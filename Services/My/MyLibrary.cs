#pragma warning disable IDE1006 // Naming Styles
namespace isr.Core.Services.My
#pragma warning restore IDE1006 // Naming Styles
{
    /// <summary>   my library info. </summary>
    /// <remarks>   David, 2020-03-18. </remarks>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1052:Static holder types should be Static or NotInheritable", Justification = "<Pending>" )]
    public partial class MyLibrary
    {

        /// <summary>   Identifier for the trace event. </summary>
        internal const int TraceEventId = ( int ) isr.Core.ProjectTraceEventId.Services;

        /// <summary>   The assembly title. </summary>
        internal const string AssemblyTitle = "ISR Core Services Library";

        /// <summary>   Information describing the assembly. </summary>
        internal const string AssemblyDescription = "ISR Core Services Library";

        /// <summary>   The assembly product. </summary>
        internal const string AssemblyProduct = "isr.Core.Services";

        /// <summary>   The assembly configuration. </summary>
        internal const string AssemblyConfiguration = "Debug";

        /// <summary> The Strong Name of the test assembly. </summary>
        internal const string TestAssemblyStrongName = "isr.Core.ServicesTests,PublicKey=" + isr.Core.My.SolutionInfo.PublicKey;

    }
}
