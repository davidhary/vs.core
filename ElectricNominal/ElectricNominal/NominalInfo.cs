using System;
using System.Linq;

namespace isr.Core.ElectricNominal
{

    /// <summary> A parser for electrical component <see cref="NominalInfo.ValueCode"/>. </summary>
    /// <remarks>
    /// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-05-19 </para>
    /// </remarks>
    public class NominalInfo
    {

        #region " CONSTRUCTION "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        public NominalInfo() : base()
        {
            this.ValueCode = EmptyCode;
            this.ParseDetails = string.Empty;
            this.ParseFailed = false;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="valueCode"> The component value code. </param>
        public NominalInfo( string valueCode ) : this()
        {
            this.ValueCode = valueCode;
            this.ParseThis();
        }

        #endregion

        #region " VALUES (properties) "

        /// <summary> Gets the parse details. </summary>
        /// <value> The parse details. </value>
        public string ParseDetails { get; set; }

        /// <summary> Gets a value indicating whether the parse failed. </summary>
        /// <value> <c>true</c> if parse failed; otherwise <c>false</c>. </value>
        public bool ParseFailed { get; set; }

        /// <summary> Gets the nominal Component. </summary>
        /// <value> The nominal Component. </value>
        public double NominalValue { get; set; }

        /// <summary> Gets the nominal value code. </summary>
        /// <value> The value code. </value>
        public string ValueCode { get; set; }

        #endregion

        #region " EMPTY IMPLEMENTATION "

        /// <summary> Gets the unknown code. </summary>
        /// <value> The unknown code. </value>
        public static string EmptyCode { get; set; } = string.Empty;

        /// <summary> Gets the sentinel indicating if the entered code is empty. </summary>
        /// <value> <c>True</c> if empty code. </value>
        public bool IsEmptyCode => string.Equals( this.ValueCode, EmptyCode );

        /// <summary> Gets the empty value. </summary>
        /// <value> The empty. </value>
        public static NominalInfo Empty => new();

        #endregion

        #region " PARSE "

        /// <summary> Query if 'valueCode' is valid. </summary>
        /// <remarks> David, 2020-04-17. </remarks>
        /// <param name="valueCode"> The component value code. </param>
        /// <returns> True if valid, false if not. </returns>
        public static bool IsValid( string valueCode )
        {
            return TryParse( valueCode ).Success;
        }

        /// <summary> Parses the <see cref="ValueCode">Component code</see>. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        private void ParseThis()
        {
            var (Success, Value, Details) = TryParse( this.ValueCode );
            this.ParseDetails = Details;
            this.ParseFailed = !Success;
            this.NominalValue = Value;
        }

        /// <summary> Parses the <see cref="ValueCode">Component code</see>. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        public void Parse()
        {
            this.ParseThis();
        }

        /// <summary> Parses the <see cref="ValueCode">component value code</see>. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="valueCode"> The component value code. </param>
        /// <returns> A Double. </returns>
        public static double Parse( string valueCode )
        {
            return TryParse( valueCode ).Value;
        }

        /// <summary> Tries to parse the Component code. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="valueCode"> The Component value code. </param>
        /// <returns> <c>true</c> if Component code can be parsed. </returns>
        public static (bool Success, double Value, string Details) TryParse( string valueCode )
        {
            (bool Success, double Value, string Details) result = (true, 0d, string.Empty);
            if ( string.IsNullOrWhiteSpace( valueCode ) )
            {
                result.Success = false;
                result.Details = "Value code is empty.";
            }
            else
            {
                var scaleFactors = new double[] { 1d, 1d, 1000d, 1000000d, 1000000000d };
                var scaleCodes = new string[] { ".", "R", "K", "M", "G" };
                string scaleCode = string.Empty;
                double scaleFactor = 0d;
                string unscaledValue = string.Empty;

                // lookup the scale code:
                for ( int i = 0, loopTo = scaleCodes.Length - 1; i <= loopTo; i++ )
                {
                    scaleCode = scaleCodes[i];
                    if ( valueCode.ToLower().Contains( scaleCode.ToLower() ) )
                    {
                        scaleFactor = scaleFactors[i];
                        break;
                    }
                }

                if ( scaleFactor > 0d )
                {
                    // found scale code in Component code, get the unscaled value
                    unscaledValue = valueCode.Replace( scaleCode, "." );
                    // remove trailing periods to use cases such as 100.0R
                    if ( unscaledValue.EndsWith( ".", StringComparison.OrdinalIgnoreCase ) )
                    {
                        unscaledValue = unscaledValue.Substring(0, unscaledValue.Length -1);
                    }
                }
                else if ( valueCode.Length >= 3 )
                {
                    // no scale code in Component code; check if the Component code has at least 4 Components.
                    // then the scale power is the last digit.
                    if ( int.TryParse( valueCode.Substring( valueCode.Length -1, 1 ), out int scaler ) )
                    {
                        scaleFactor = Math.Pow( 10d, scaler );
                        unscaledValue = valueCode.Substring(0, valueCode.Length-1);
                    }
                }

                if ( string.IsNullOrWhiteSpace( unscaledValue ) )
                {
                    result.Details = $"Unknown Component code '{valueCode}'";
                }
                else if ( double.TryParse( unscaledValue, out result.Value ) )
                {
                    result.Value *= scaleFactor;
                    result.Success = true;
                    result.Details = string.Empty;
                }
                else
                {
                    result.Details = $"Failed converting '{unscaledValue}' created from '{valueCode}' to nominal Component";
                }
            }

            return result;
        }

        #endregion

    }
}
