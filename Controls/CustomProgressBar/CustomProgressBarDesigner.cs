using System;
using System.Collections;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Windows.Forms.Design.Behavior;

using Microsoft.VisualBasic;

namespace isr.Core.Controls
{
    internal class CustomProgressBarDesigner : System.Windows.Forms.Design.ControlDesigner
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:System.Windows.Forms.Design.ControlDesigner" /> class.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public CustomProgressBarDesigner()
        {
        }

        /// <summary>
        /// Gets a list of <see cref="T:System.Windows.Forms.Design.Behavior.SnapLine" /> objects
        /// representing significant alignment points for this control.
        /// </summary>
        /// <remarks>
        /// A list of <see cref="T:System.Windows.Forms.Design.Behavior.SnapLine" /> objects representing
        /// significant alignment points for this control.
        /// </remarks>
        public override IList SnapLines
        {
            get {
                // Get the SnapLines collection from the base class
                ArrayList snapList = base.SnapLines as ArrayList;

                // Calculate the Baseline for the Font used by the Control and add it to the SnapLines
                int textBaseline = GetBaseline( base.Control, ContentAlignment.MiddleCenter );
                if ( textBaseline > 0 )
                {
                    _ = snapList.Add( new SnapLine( SnapLineType.Baseline, textBaseline, SnapLinePriority.Medium ) );
                }

                return snapList;
            }
        }

        /// <summary> Gets a baseline. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="ctrl">      The control. </param>
        /// <param name="alignment"> The alignment. </param>
        /// <returns> The baseline. </returns>
        private static int GetBaseline( Control ctrl, ContentAlignment alignment )
        {
            int textAscent = 0;
            int textHeight = 0;

            // Retrieve the ClientRect of the Control
            var clientRect = ctrl.ClientRectangle;

            // Create a Graphics object for the Control
            using ( var graphics = ctrl.CreateGraphics() )
            {
                // Retrieve the device context Handle
                var hDC = graphics.GetHdc();

                // Create a wrapper for the Font of the Control
                var controlFont = ctrl.Font;
                var tempFontHandle = new HandleRef( controlFont, controlFont.ToHfont() );
                try
                {
                    // Create a wrapper for the device context
                    var deviceContextHandle = new HandleRef( ctrl, hDC );

                    // Select the Font into the device context
                    var originalFont = NativeMethods.SelectObject( deviceContextHandle, tempFontHandle );

                    // Create a TEXTMETRIC and calculate metrics for the selected Font
                    var tEXTMETRIC = new NativeMethods.TEXTMETRIC();
                    if ( NativeMethods.GetTextMetrics( deviceContextHandle, ref tEXTMETRIC ) != 0 )
                    {
                        textAscent = tEXTMETRIC.tmAscent + 1;
                        textHeight = tEXTMETRIC.tmHeight;
                    }

                    // Restore original Font
                    var originalFontHandle = new HandleRef( ctrl, originalFont );
                    _ = NativeMethods.SelectObject( deviceContextHandle, originalFontHandle );
                }
                finally
                {
                    // Cleanup tempFont
                    _ = NativeMethods.DeleteObject( tempFontHandle );

                    // Release device context
                    graphics.ReleaseHdc( hDC );
                }
            }

            // Calculate return value based on the specified alignment; first check top alignment
            if ( (alignment & (ContentAlignment.TopLeft | ContentAlignment.TopCenter | ContentAlignment.TopRight)) != 0 )
            {
                return clientRect.Top + textAscent;
            }

            // Check middle alignment
            if ( (alignment & (ContentAlignment.MiddleLeft | ContentAlignment.MiddleCenter | ContentAlignment.MiddleRight)) == 0 )
            {
                return clientRect.Bottom - textHeight + textAscent;
            }

            // Assume bottom alignment
            return ( int ) Conversion.Fix( Math.Round( clientRect.Top + clientRect.Height / 2d - textHeight / 2d + textAscent ) );
        }
    }
}
