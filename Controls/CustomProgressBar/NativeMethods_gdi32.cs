using System;
using System.Runtime.InteropServices;
using System.Security;

namespace isr.Core.Controls
{
    [SuppressUnmanagedCodeSecurity]
    internal sealed partial class NativeMethods
    {

        /// <summary> Deletes the object described by hObject. </summary>
        /// <remarks>
        /// Deletes a logical pen, brush, font, bitmap, region, or palette, freeing all system resources
        /// associated with the object. After the object has been deleted, the specified handle is no
        /// longer valid.
        /// </remarks>
        /// <param name="hObject"> The object. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        [DllImport( "gdi32.dll", CharSet = CharSet.Auto, ExactSpelling = false )]
        internal static extern bool DeleteObject( HandleRef hObject );

        /// <summary> Select object. </summary>
        /// <remarks>
        /// Selects an object into the specified device context (DC). The new object replaces the
        /// previous object of the same type.
        /// </remarks>
        /// <param name="hDC">     The device-context. </param>
        /// <param name="hObject"> The object. </param>
        /// <returns> An IntPtr. </returns>
        [DllImport( "gdi32.dll", CharSet = CharSet.Auto, ExactSpelling = false )]
        internal static extern IntPtr SelectObject( HandleRef hDC, HandleRef hObject );

        /// <summary> Gets text metrics. </summary>
        /// <remarks>
        /// Fills the specified buffer with the metrics for the currently selected font.
        /// </remarks>
        /// <param name="hDC">  The device-context. </param>
        /// <param name="lptm"> [in,out] The metrics. </param>
        /// <returns> The text metrics. </returns>
        internal static int GetTextMetrics( HandleRef hDC, ref TEXTMETRIC lptm )
        {
            if ( Marshal.SystemDefaultCharSize != 1 )
            {
                // Handle Unicode
                return GetTextMetricsW( hDC, ref lptm );
            }

            // Handle ANSI; call GetTextMetricsA and translate to Unicode structure
            var tEXTMETRICA = new TEXTMETRICA();
            int result = GetTextMetricsA( hDC, ref tEXTMETRICA );
            lptm.tmHeight = tEXTMETRICA.tmHeight;
            lptm.tmAscent = tEXTMETRICA.tmAscent;
            lptm.tmDescent = tEXTMETRICA.tmDescent;
            lptm.tmInternalLeading = tEXTMETRICA.tmInternalLeading;
            lptm.tmExternalLeading = tEXTMETRICA.tmExternalLeading;
            lptm.tmAveCharWidth = tEXTMETRICA.tmAveCharWidth;
            lptm.tmMaxCharWidth = tEXTMETRICA.tmMaxCharWidth;
            lptm.tmWeight = tEXTMETRICA.tmWeight;
            lptm.tmOverhang = tEXTMETRICA.tmOverhang;
            lptm.tmDigitizedAspectX = tEXTMETRICA.tmDigitizedAspectX;
            lptm.tmDigitizedAspectY = tEXTMETRICA.tmDigitizedAspectY;
            lptm.tmFirstChar = Convert.ToChar( tEXTMETRICA.tmFirstChar );
            lptm.tmLastChar = Convert.ToChar( tEXTMETRICA.tmLastChar );
            lptm.tmDefaultChar = Convert.ToChar( tEXTMETRICA.tmDefaultChar );
            lptm.tmBreakChar = Convert.ToChar( tEXTMETRICA.tmBreakChar );
            lptm.tmItalic = tEXTMETRICA.tmItalic;
            lptm.tmUnderlined = tEXTMETRICA.tmUnderlined;
            lptm.tmStruckOut = tEXTMETRICA.tmStruckOut;
            lptm.tmPitchAndFamily = tEXTMETRICA.tmPitchAndFamily;
            lptm.tmCharSet = tEXTMETRICA.tmCharSet;
            return result;
        }

        /// <summary> Gets text metrics a. </summary>
        /// <remarks>
        /// Fills the specified buffer with the metrics for the currently selected font. This is the ANSI
        /// version of the function.
        /// </remarks>
        /// <param name="hDC">  The device-context. </param>
        /// <param name="lptm"> [in,out] The metrics. </param>
        /// <returns> The text metrics a. </returns>
        [DllImport( "gdi32.dll", CharSet = CharSet.Ansi, ExactSpelling = false )]
        private static extern int GetTextMetricsA( HandleRef hDC, ref TEXTMETRICA lptm );

        /// <summary> Gets text metrics w. </summary>
        /// <remarks>
        /// Fills the specified buffer with the metrics for the currently selected font. This is the
        /// Unicode version of the function.
        /// </remarks>
        /// <param name="hDC">  The device-context. </param>
        /// <param name="lptm"> [in,out] The metrics. </param>
        /// <returns> The text metrics w. </returns>
        [DllImport( "gdi32.dll", CharSet = CharSet.Unicode, ExactSpelling = false )]
        private static extern int GetTextMetricsW( HandleRef hDC, ref TEXTMETRIC lptm );
    }
}
