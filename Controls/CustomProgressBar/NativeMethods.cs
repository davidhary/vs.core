using System;
using System.Runtime.InteropServices;
using System.Security;

namespace isr.Core.Controls
{
    [SuppressUnmanagedCodeSecurity]
    internal sealed partial class NativeMethods
    {

        /// <summary> Begins a paint. </summary>
        /// <remarks>
        /// Prepares the specified window for painting and fills a PAINTSTRUCT structure with information
        /// about the painting.
        /// </remarks>
        /// <param name="hWnd">    The window. </param>
        /// <param name="lpPaint"> The paint structure. </param>
        /// <returns> An IntPtr. </returns>
        [DllImport( "user32.dll", CharSet = CharSet.Auto, ExactSpelling = false )]
        internal static extern IntPtr BeginPaint( HandleRef hWnd, ref PAINTSTRUCT lpPaint );

        /// <summary> Ends a paint. </summary>
        /// <remarks>
        /// Marks the end of painting in the specified window. This function is required for each call to
        /// the BeginPaint function, but only after painting is complete.
        /// </remarks>
        /// <param name="hWnd">    The window. </param>
        /// <param name="lpPaint"> [in,out] The paint structure. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        [DllImport( "user32.dll", CharSet = CharSet.Auto, ExactSpelling = false )]
        internal static extern bool EndPaint( HandleRef hWnd, ref PAINTSTRUCT lpPaint );
    }
}
