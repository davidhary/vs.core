using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace isr.Core.Controls
{

    /// <summary> Tool strip custom progress bar. </summary>
    /// <remarks>
    /// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-04-16 </para>
    /// </remarks>
    [ToolStripItemDesignerAvailability( ToolStripItemDesignerAvailability.ToolStrip )]
    public class ToolStripCustomProgressBar : ToolStripControlHost
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> Call the base constructor passing in a CustomProgressBar instance. </remarks>
        public ToolStripCustomProgressBar() : base( new CustomProgressBar() )
        {
        }

        #endregion

        #region " CONTROL "

        /// <summary> Gets the numeric up down control. </summary>
        /// <value> The numeric up down control. </value>
        public CustomProgressBar ProgressBar => this.Control as CustomProgressBar;

        #endregion

        #region " CONTROL PROPERTIES "

        /// <summary> Gets or sets the selected text. </summary>
        /// <value> The selected text. </value>
        [DefaultValue( "" )]
        [Description( "text" )]
        [Category( "Appearance" )]
        public override string Text
        {
            get => this.ProgressBar.Text;

            set => this.ProgressBar.Text = value;
        }

        /// <summary> Gets or sets the value. </summary>
        /// <value> The value. </value>
        [DefaultValue( "" )]
        [Description( "Value" )]
        [Category( "Appearance" )]
        public int Value
        {
            get => this.ProgressBar.Value;

            set => this.ProgressBar.Value = value;
        }

        /// <summary> Minimum. </summary>
        /// <value> The minimum value. </value>
        [DefaultValue( 0 )]
        [Description( "Minimum" )]
        [Category( "Appearance" )]
        public int Minimum
        {
            get => this.ProgressBar.Minimum;

            set => this.ProgressBar.Minimum = value;
        }

        /// <summary> Maximum. </summary>
        /// <value> The Maximum value. </value>
        [DefaultValue( 0 )]
        [Description( "Maximum" )]
        [Category( "Appearance" )]
        public int Maximum
        {
            get => this.ProgressBar.Maximum;

            set => this.ProgressBar.Maximum = value;
        }

        /// <summary>
        /// Gets or sets the opacity of the white overlay brush which fades the background colors of the
        /// <see cref="CustomProgressBar" />.
        /// </summary>
        /// <remarks>
        /// You can use this property to manipulate the density of the background coloring of this
        /// control, to allow for better readability of any text within the
        /// <see cref="CustomProgressBar" />. You can use the <see cref="CustomProgressBar.Font" /> and
        /// <see cref="CustomProgressBar.ForeColor" /> properties to further optimize the display of text.
        /// 
        /// <para>Acceptable values for this property are between 0 and 255 inclusive. The default is 150;
        /// lower values make the background darker; higher values make the background lighter.</para>
        /// </remarks>
        /// <value>
        /// An <see cref="System.Int32" /> representing the alpha value of the overlay color. The default
        /// is <b>150</b>.
        /// </value>
        [Category( "Appearance" )]
        [DefaultValue( 150 )]
        [Description( "Specifies the opacity of the white overlay brush which fades the background colors of the CustomProgressBar." )]
        public int Fade
        {
            get => this.ProgressBar.Fade;

            set => this.ProgressBar.Fade = value;
        }

        /// <summary>
        /// Gets or sets the <see cref="System.Drawing.Font" /> of the text displayed by the
        /// <see cref="CustomProgressBar" />.
        /// </summary>
        /// <remarks>
        /// You can use the Font property to change the <see cref="System.Drawing.Font" />
        /// to use when drawing text. To change the text <see cref="Color" />, use the
        /// <see cref="CustomProgressBar.ForeColor" /> property.
        /// 
        /// <para>The Font property is an ambient property. An ambient property is a control property
        /// that, if not set, is retrieved from the parent control.</para>
        /// 
        /// <para>Because the <see cref="System.Drawing.Font" /> class is immutable (meaning that you
        /// cannot adjust any of its properties), you can only assign the Font property a new Font.
        /// However, you can base the new font on the existing font.</para>
        /// 
        /// <para><note type="inherit">When overriding the Font property in a derived class, use the base
        /// class's Font property to extend the base implementation. Otherwise, you must provide all the
        /// implementation.</note></para>
        /// </remarks>
        /// <value>
        /// The <see cref="System.Drawing.Font" /> to apply to the text displayed by the control.
        /// </value>
        [Browsable( true )]
        [EditorBrowsable( EditorBrowsableState.Always )]
        public override Font Font
        {
            get => this.ProgressBar.Font;

            set => this.ProgressBar.Font = value;
        }

        /// <summary>
        /// Gets or sets the color of the text displayed by <see cref="CustomProgressBar" />.
        /// </summary>
        /// <remarks>
        /// You can use the ForeColor property to change the color of the text within the
        /// <see cref="CustomProgressBar" /> to match the text of other controls on your form.
        /// To change the <see cref="System.Drawing.Font" /> to use when drawing text, use the
        /// <see cref="CustomProgressBar.Font">CustomProgressBar.Font</see> property.
        /// 
        /// <para><note type="inherit">When overriding the ForeColor property in a derived class, use the
        /// base class's ForeColor property to extend the base implementation. Otherwise, you must
        /// provide all the implementation.</note></para>
        /// </remarks>
        /// <value>
        /// A <see cref="Color" /> that represents the control's foreground color. The default is
        /// <b>ControlText</b>.
        /// </value>
        [DefaultValue( typeof( Color ), "ControlText" )]
        public override Color ForeColor
        {
            get => this.ProgressBar.ForeColor;

            set => this.ProgressBar.ForeColor = value;
        }

        /// <summary> Specifies the format of the overlay. </summary>
        /// <value> The caption format. </value>
        [Category( "Appearance" )]
        [DefaultValue( "{0} %" )]
        [Description( "Specifies the format of the overlay." )]
        public string CaptionFormat
        {
            get => this.ProgressBar.CaptionFormat;

            set => this.ProgressBar.CaptionFormat = value;
        }

        #endregion

    }
}
