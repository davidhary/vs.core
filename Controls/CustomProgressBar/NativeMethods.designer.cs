namespace isr.Core.Controls
{
    [Microsoft.VisualBasic.CompilerServices.DesignerGenerated()]
    internal sealed partial class NativeMethods
    {

        #region " PARAMETERS "

        /// <summary> The windows message print client. </summary>
        /// <remarks> Sent to a window to request that it draw its client area
        /// in the specified device context, most commonly in a printer device context.
        /// </remarks>
        public const int WM_PRINTCLIENT = 0x318;

        /// <summary> The WS EX composited. </summary>
        /// <remarks> Paints all descendants of a window in bottom-to-top painting order using double-buffering. </remarks>
        public const int WS_EX_COMPOSITED = 0x2000000;

        #endregion

        #region " STRUCTURES "

        /// <summary> Contains basic information about a physical font. This is the Unicode version of the structure. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        [System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential, CharSet = System.Runtime.InteropServices.CharSet.Unicode)]
        public struct TEXTMETRIC
        {

            /// <summary> The height (ascent + descent) of characters. . </summary>
            public int tmHeight;

            /// <summary> The ascent (units above the base line) of characters. </summary>
            public int tmAscent;

            /// <summary> The descent (units below the base line) of characters. . </summary>
            public int tmDescent;

            /// <summary> The amount of leading (space) inside the bounds set by the tmHeight member. . </summary>
            public int tmInternalLeading;

            /// <summary> The amount of extra leading (space) that the application adds between rows. </summary>
            public int tmExternalLeading;

            /// <summary> The average width of characters in the font (generally defined as the width of the letter x). </summary>
            public int tmAveCharWidth;

            /// <summary> The width of the widest character in the font.  </summary>
            public int tmMaxCharWidth;

            /// <summary> The weight of the font.  </summary>
            public int tmWeight;

            /// <summary> The extra width per string that may be added to some synthesized fonts.  </summary>
            public int tmOverhang;

            /// <summary> The horizontal aspect of the device for which the font was designed. </summary>
            public int tmDigitizedAspectX;

            /// <summary> The vertical aspect of the device for which the font was designed. </summary>
            public int tmDigitizedAspectY;

            /// <summary> The value of the first character defined in the font. </summary>
            public char tmFirstChar;

            /// <summary> The value of the last character defined in the font. </summary>
            public char tmLastChar;

            /// <summary> The value of the character to be substituted for characters not in the font. </summary>
            public char tmDefaultChar;

            /// <summary> The value of the character that will be used to define word breaks for text justification. </summary>
            public char tmBreakChar;

            /// <summary> Specifies an italic font if it is nonzero. </summary>
            public byte tmItalic;

            /// <summary> Specifies an underlined font if it is nonzero. </summary>
            public byte tmUnderlined;

            /// <summary> A strikeout font if it is nonzero. </summary>
            public byte tmStruckOut;

            /// <summary> Specifies information about the pitch, the technology, and the family of a physical font. </summary>
            public byte tmPitchAndFamily;

            /// <summary> The character set of the font. </summary>
            public byte tmCharSet;
        }

        /// <summary> Contains basic information about a physical font. This is the ANSI version of the structure. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        [System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential, CharSet = System.Runtime.InteropServices.CharSet.Ansi)]
        public struct TEXTMETRICA
        {

            /// <summary> The height (ascent + descent) of characters. </summary>
            public int tmHeight;

            /// <summary> The ascent (units above the base line) of characters. </summary>
            public int tmAscent;
            /// <summary> The descent (units below the base line) of characters. </summary>
            public int tmDescent;

            /// <summary> The amount of leading (space) inside the bounds set by the tmHeight member. </summary>
            public int tmInternalLeading;

            /// <summary> The amount of extra leading (space) that the application adds between rows. </summary>
            public int tmExternalLeading;

            /// <summary> The average width of characters in the font (generally defined as the width of the letter x). </summary>
            public int tmAveCharWidth;

            /// <summary> The width of the widest character in the font. </summary>
            public int tmMaxCharWidth;

            /// <summary> The weight of the font. </summary>
            public int tmWeight;

            /// <summary> The extra width per string that may be added to some synthesized fonts. </summary>
            public int tmOverhang;

            /// <summary> The horizontal aspect of the device for which the font was designed. </summary>
            public int tmDigitizedAspectX;

            /// <summary> The vertical aspect of the device for which the font was designed. </summary>
            public int tmDigitizedAspectY;

            /// <summary> The value of the first character defined in the font. </summary>
            public byte tmFirstChar;

            /// <summary> The value of the last character defined in the font. </summary>
            public byte tmLastChar;

            /// <summary> The value of the character to be substituted for characters not in the font. </summary>
            public byte tmDefaultChar;

            /// <summary> The value of the character that will be used to define word breaks for text justification. </summary>
            public byte tmBreakChar;

            /// <summary> Specifies an italic font if it is nonzero. </summary>
            public byte tmItalic;

            /// <summary> Specifies an underlined font if it is nonzero. </summary>
            public byte tmUnderlined;

            /// <summary> A strikeout font if it is nonzero. </summary>
            public byte tmStruckOut;

            /// <summary> Specifies information about the pitch, the technology, and the family of a physical font. </summary>
            public byte tmPitchAndFamily;

            /// <summary> The character set of the font. </summary>
            public byte tmCharSet;
        }

        #endregion

    }
}
