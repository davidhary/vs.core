using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

using isr.Core.Controls.ExceptionExtensions;

using Microsoft.VisualBasic;

namespace isr.Core.Controls
{

    /// <summary> A toggle switch. </summary>
    /// <remarks>
    /// (c) 2015 Johnny J.. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-09-11 from Johnny J </para><para>
    /// http://www.codeproject.com/Articles/1029499/ToggleSwitch-Winforms-Control. </para>
    /// </remarks>
    [DefaultValue( "Checked" )]
    [DefaultEvent( "CheckedChanged" )]
    [ToolboxBitmap( typeof( CheckBox ) )]
    public class ToggleSwitch : Control
    {

        #region " Constructor Etc."

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Windows.Forms.Control" /> class with
        /// default settings.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public ToggleSwitch() : base()
        {
            this.SetStyle( ControlStyles.ResizeRedraw | ControlStyles.SupportsTransparentBackColor | ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.OptimizedDoubleBuffer | ControlStyles.DoubleBuffer, true );
            this._OnFont = base.Font;
            this._OffFont = base.Font;
            this.SetRendererThis( new ToggleSwitchMetroRenderer() );
            this._AnimationTimer.Enabled = false;
            this._AnimationTimer.Interval = this._AnimationInterval;
            this._AnimationTimer.Tick += this.AnimationTimer_Tick;
        }

        /// <summary> Sets renderer this. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="renderer"> The renderer. </param>
        private void SetRendererThis( ToggleSwitchRendererBase renderer )
        {
            renderer?.SetToggleSwitch( this );
            this._Renderer = renderer;
            if ( this._Renderer is object )
            {
                this.Refresh();
            }
        }

        /// <summary> Sets a renderer. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="renderer"> The renderer. </param>
        public void SetRenderer( ToggleSwitchRendererBase renderer )
        {
            this.SetRendererThis( renderer );
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.RemoveCheckedChangedEventHandler( CheckedChanged );
                    if ( this._OffFont is object )
                    {
                        this._OffFont.Dispose();
                        this._OffFont = null;
                    }

                    if ( this._OnFont is object )
                    {
                        this._OnFont.Dispose();
                        this._OffFont = null;
                    }

                    if ( this._AnimationTimer is object )
                    {
                        this._AnimationTimer.Dispose();
                    }

                    if ( this._ButtonImage is object )
                    {
                        this._ButtonImage.Dispose();
                        this._ButtonImage = null;
                    }

                    if ( this._OffButtonImage is object )
                    {
                        this._OffButtonImage.Dispose();
                        this._OffButtonImage = null;
                    }

                    if ( this._OffSideImage is object )
                    {
                        this._OffSideImage.Dispose();
                        this._OffSideImage = null;
                    }

                    if ( this._OnButtonImage is object )
                    {
                        this._OnButtonImage.Dispose();
                        this._OnButtonImage = null;
                    }

                    if ( this._OnSideImage is object )
                    {
                        this._OnSideImage.Dispose();
                        this._OnSideImage = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion ' Constructor Etc.

        #region " Public Properties "

        /// <summary> Gets or sets the style of the ToggleSwitch. </summary>
        /// <value> The style. </value>
        [Bindable( false )]
        [DefaultValue( typeof( ToggleSwitchStyle ), "Metro" )]
        [Category( "Appearance" )]
        [Description( "Gets or sets the style of the ToggleSwitch" )]
        public ToggleSwitchStyle Style
        {
            get => this._Style;

            set {
                if ( value != this._Style )
                {
                    this._Style = value;
                    switch ( this._Style )
                    {
                        case ToggleSwitchStyle.Metro:
                            {
                                this.SetRenderer( new ToggleSwitchMetroRenderer() );
                                break;
                            }

                        case ToggleSwitchStyle.Android:
                            {
                                this.SetRenderer( new ToggleSwitchAndroidRenderer() );
                                break;
                            }

                        case ToggleSwitchStyle.Ios5:
                            {
                                this.SetRenderer( new ToggleSwitchIos5Renderer() );
                                break;
                            }

                        case ToggleSwitchStyle.BrushedMetal:
                            {
                                this.SetRenderer( new ToggleSwitchBrushedMetalRenderer() );
                                break;
                            }

                        case ToggleSwitchStyle.OS10:
                            {
                                this.SetRenderer( new ToggleSwitchOS10Renderer() );
                                break;
                            }

                        case ToggleSwitchStyle.Carbon:
                            {
                                this.SetRenderer( new ToggleSwitchCarbonRenderer() );
                                break;
                            }

                        case ToggleSwitchStyle.IPhone:
                            {
                                this.SetRenderer( new ToggleSwitchIPhoneRenderer() );
                                break;
                            }

                        case ToggleSwitchStyle.Fancy:
                            {
                                this.SetRenderer( new ToggleSwitchFancyRenderer() );
                                break;
                            }

                        case ToggleSwitchStyle.Modern:
                            {
                                this.SetRenderer( new ToggleSwitchModernRenderer() );
                                break;
                            }
                    }
                }

                this.Refresh();
            }
        }

        /// <summary> Gets or sets the Checked value of the ToggleSwitch. </summary>
        /// <value> The checked. </value>
        [Bindable( true )]
        [DefaultValue( false )]
        [Category( "Data" )]
        [Description( "Gets or sets the Checked value of the ToggleSwitch" )]
        public bool Checked
        {
            get => this._Checked;

            set {
                if ( value != this._Checked )
                {
                    while ( this._Animating )
                    {
                        Application.DoEvents();
                    }

                    if ( value == true )
                    {
                        int buttonWidth = this._Renderer.GetButtonWidth();
                        this._AnimationTarget = this.Width - buttonWidth;
                        this.BeginAnimation( true );
                    }
                    else
                    {
                        this._AnimationTarget = 0;
                        this.BeginAnimation( false );
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets whether the user can change the value of the button or not.
        /// </summary>
        /// <value> The allow user change. </value>
        [Bindable( true )]
        [DefaultValue( true )]
        [Category( "Behavior" )]
        [Description( "Gets or sets whether the user can change the value of the button or not" )]
        public bool AllowUserChange { get; set; } = true;

        /// <summary> Gets the checked string. </summary>
        /// <value> The checked string. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public string CheckedString => this.Checked ? string.IsNullOrEmpty( this.OnText ) ? "ON" : this.OnText : string.IsNullOrEmpty( this.OffText ) ? "OFF" : this.OffText;

        /// <summary> Gets the button rectangle. </summary>
        /// <value> The button rectangle. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public Rectangle ButtonRectangle => this._Renderer.GetButtonRectangle();

        /// <summary> Gets or sets if the ToggleSwitch should be grayed out when disabled. </summary>
        /// <value> The gray when disabled. </value>
        [Bindable( false )]
        [DefaultValue( true )]
        [Category( "Appearance" )]
        [Description( "Gets or sets if the ToggleSwitch should be grayed out when disabled" )]
        public bool GrayWhenDisabled
        {
            get => this._GrayWhenDisabled;

            set {
                if ( value != this._GrayWhenDisabled )
                {
                    this._GrayWhenDisabled = value;
                    if ( !this.Enabled )
                    {
                        this.Refresh();
                    }
                }
            }
        }

        /// <summary> Gets or sets if the ToggleSwitch should toggle when the button is clicked. </summary>
        /// <value> The toggle on button click. </value>
        [Bindable( false )]
        [DefaultValue( true )]
        [Category( "Behavior" )]
        [Description( "Gets or sets if the ToggleSwitch should toggle when the button is clicked" )]
        public bool ToggleOnButtonClick { get; set; } = true;

        /// <summary>
        /// Gets or sets if the ToggleSwitch should toggle when the track besides the button is clicked.
        /// </summary>
        /// <value> The toggle on side click. </value>
        [Bindable( false )]
        [DefaultValue( true )]
        [Category( "Behavior" )]
        [Description( "Gets or sets if the ToggleSwitch should toggle when the track besides the button is clicked" )]
        public bool ToggleOnSideClick { get; set; } = true;

        /// <summary>
        /// Gets or sets how much the button need to be on the other side (in percent) before it snaps.
        /// </summary>
        /// <value> The threshold percentage. </value>
        [Bindable( false )]
        [DefaultValue( 50 )]
        [Category( "Behavior" )]
        [Description( "Gets or sets how much the button need to be on the other side (in percent) before it snaps" )]
        public int ThresholdPercentage { get; set; } = 50;

        /// <summary> Gets or sets the fore-color of the text when Checked=false. </summary>
        /// <value> The color of the off foreground. </value>
        [Bindable( false )]
        [DefaultValue( typeof( Color ), "Black" )]
        [Category( "Appearance" )]
        [Description( "Gets or sets the fore-color of the text when Checked=false" )]
        public Color OffForeColor
        {
            get => this._OffForeColor;

            set {
                if ( value != this._OffForeColor )
                {
                    this._OffForeColor = value;
                    this.Refresh();
                }
            }
        }

        /// <summary> Gets or sets the font of the text when Checked=false. </summary>
        /// <value> The off font. </value>
        [Bindable( false )]
        [DefaultValue( typeof( Font ), "Microsoft Sans Serif, 8.25pt" )]
        [Category( "Appearance" )]
        [Description( "Gets or sets the font of the text when Checked=false" )]
        public Font OffFont
        {
            get => this._OffFont;

            set {
                if ( !Equals( value, this.OffFont ) )
                {
                    this._OffFont = value;
                    this.Refresh();
                }
            }
        }

        /// <summary> Gets or sets the text when Checked=true. </summary>
        /// <value> The off text. </value>
        [Bindable( false )]
        [DefaultValue( "" )]
        [Category( "Appearance" )]
        [Description( "Gets or sets the text when Checked=true" )]
        public string OffText
        {
            get => this._OffText;

            set {
                if ( !string.Equals( value, this._OffText ) )
                {
                    this._OffText = value;
                    this.Refresh();
                }
            }
        }

        /// <summary>
        /// Gets or sets the side image when Checked=false - Note: Settings the OffSideImage overrules
        /// the OffText property. Only the image will be shown.
        /// </summary>
        /// <value> The off side image. </value>
        [Bindable( false )]
        [DefaultValue( ( object ) default )]
        [Category( "Appearance" )]
        [Description( "Gets or sets the side image when Checked=false - Note: Settings the OffSideImage overrules the OffText property. Only the image will be shown" )]
        public Image OffSideImage
        {
            get => this._OffSideImage;

            set {
                if ( !Equals( value, this.OffSideImage ) )
                {
                    this._OffSideImage = value;
                    this.Refresh();
                }
            }
        }

        /// <summary>
        /// Gets or sets whether the side image visible when Checked=false should be scaled to fit.
        /// </summary>
        /// <value> The off side scale image to fit. </value>
        [Bindable( false )]
        [DefaultValue( false )]
        [Category( "Behavior" )]
        [Description( "Gets or sets whether the side image visible when Checked=false should be scaled to fit" )]
        public bool OffSideScaleImageToFit
        {
            get => this._OffSideScaleImage;

            set {
                if ( value != this._OffSideScaleImage )
                {
                    this._OffSideScaleImage = value;
                    this.Refresh();
                }
            }
        }

        /// <summary>
        /// Gets or sets how the text or side image visible when Checked=false should be aligned.
        /// </summary>
        /// <value> The off side alignment. </value>
        [Bindable( false )]
        [DefaultValue( typeof( ToggleSwitchAlignment ), "Center" )]
        [Category( "Appearance" )]
        [Description( "Gets or sets how the text or side image visible when Checked=false should be aligned" )]
        public ToggleSwitchAlignment OffSideAlignment
        {
            get => this._OffSideAlignment;

            set {
                if ( value != this._OffSideAlignment )
                {
                    this._OffSideAlignment = value;
                    this.Refresh();
                }
            }
        }

        /// <summary>
        /// Gets or sets the button image when Checked=false and ButtonImage is not set.
        /// </summary>
        /// <value> The off button image. </value>
        [Bindable( false )]
        [DefaultValue( ( object ) default )]
        [Category( "Appearance" )]
        [Description( "Gets or sets the button image when Checked=false and ButtonImage is not set" )]
        public Image OffButtonImage
        {
            get => this._OffButtonImage;

            set {
                if ( !Equals( value, this.OffButtonImage ) )
                {
                    this._OffButtonImage = value;
                    this.Refresh();
                }
            }
        }

        /// <summary>
        /// Gets or sets whether the button image visible when Checked=false should be scaled to fit.
        /// </summary>
        /// <value> The off button scale image to fit. </value>
        [Bindable( false )]
        [DefaultValue( false )]
        [Category( "Behavior" )]
        [Description( "Gets or sets whether the button image visible when Checked=false should be scaled to fit" )]
        public bool OffButtonScaleImageToFit
        {
            get => this._OffButtonScaleImage;

            set {
                if ( value != this._OffButtonScaleImage )
                {
                    this._OffButtonScaleImage = value;
                    this.Refresh();
                }
            }
        }

        /// <summary>
        /// Gets or sets how the button image visible when Checked=false should be aligned.
        /// </summary>
        /// <value> The off button alignment. </value>
        [Bindable( false )]
        [DefaultValue( typeof( ToggleSwitchButtonAlignment ), "Center" )]
        [Category( "Appearance" )]
        [Description( "Gets or sets how the button image visible when Checked=false should be aligned" )]
        public ToggleSwitchButtonAlignment OffButtonAlignment
        {
            get => this._OffButtonAlignment;

            set {
                if ( value != this._OffButtonAlignment )
                {
                    this._OffButtonAlignment = value;
                    this.Refresh();
                }
            }
        }

        /// <summary> Gets or sets the fore-color of the text when Checked=true. </summary>
        /// <value> The color of the on foreground. </value>
        [Bindable( false )]
        [DefaultValue( typeof( Color ), "Black" )]
        [Category( "Appearance" )]
        [Description( "Gets or sets the fore-color of the text when Checked=true" )]
        public Color OnForeColor
        {
            get => this._OnForeColor;

            set {
                if ( value != this._OnForeColor )
                {
                    this._OnForeColor = value;
                    this.Refresh();
                }
            }
        }

        /// <summary> Gets or sets the font of the text when Checked=true. </summary>
        /// <value> The on font. </value>
        [Bindable( false )]
        [DefaultValue( typeof( Font ), "Microsoft Sans Serif, 8,25pt" )]
        [Category( "Appearance" )]
        [Description( "Gets or sets the font of the text when Checked=true" )]
        public Font OnFont
        {
            get => this._OnFont;

            set {
                if ( !Equals( value, this.OnFont ) )
                {
                    this._OnFont = value;
                    this.Refresh();
                }
            }
        }

        /// <summary> Gets or sets the text when Checked=true. </summary>
        /// <value> The on text. </value>
        [Bindable( false )]
        [DefaultValue( "" )]
        [Category( "Appearance" )]
        [Description( "Gets or sets the text when Checked=true" )]
        public string OnText
        {
            get => this._OnText;

            set {
                if ( !string.Equals( value, this.OnText ) )
                {
                    this._OnText = value;
                    this.Refresh();
                }
            }
        }

        /// <summary>
        /// Gets or sets the side image visible when Checked=true - Note: Settings the OnSideImage
        /// overrules the OnText property. Only the image will be shown.
        /// </summary>
        /// <value> The on side image. </value>
        [Bindable( false )]
        [DefaultValue( ( object ) default )]
        [Category( "Appearance" )]
        [Description( "Gets or sets the side image visible when Checked=true - Note: Settings the OnSideImage overrules the OnText property. Only the image will be shown." )]
        public Image OnSideImage
        {
            get => this._OnSideImage;

            set {
                if ( !ReferenceEquals( value, this._OnSideImage ) )
                {
                    this._OnSideImage = value;
                    this.Refresh();
                }
            }
        }

        /// <summary>
        /// Gets or sets whether the side image visible when Checked=true should be scaled to fit.
        /// </summary>
        /// <value> The on side scale image to fit. </value>
        [Bindable( false )]
        [DefaultValue( false )]
        [Category( "Behavior" )]
        [Description( "Gets or sets whether the side image visible when Checked=true should be scaled to fit" )]
        public bool OnSideScaleImageToFit
        {
            get => this._OnSideScaleImage;

            set {
                if ( value != this._OnSideScaleImage )
                {
                    this._OnSideScaleImage = value;
                    this.Refresh();
                }
            }
        }

        /// <summary> Gets or sets the button image. </summary>
        /// <value> The button image. </value>
        [Bindable( false )]
        [DefaultValue( ( object ) default )]
        [Category( "Appearance" )]
        [Description( "Gets or sets the button image" )]
        public Image ButtonImage
        {
            get => this._ButtonImage;

            set {
                if ( !ReferenceEquals( value, this._ButtonImage ) )
                {
                    this._ButtonImage = value;
                    this.Refresh();
                }
            }
        }

        /// <summary> Gets or sets whether the button image should be scaled to fit. </summary>
        /// <value> The button scale image to fit. </value>
        [Bindable( false )]
        [DefaultValue( false )]
        [Category( "Behavior" )]
        [Description( "Gets or sets whether the button image should be scaled to fit" )]
        public bool ButtonScaleImageToFit
        {
            get => this._ButtonScaleImage;

            set {
                if ( value != this._ButtonScaleImage )
                {
                    this._ButtonScaleImage = value;
                    this.Refresh();
                }
            }
        }

        /// <summary> Gets or sets how the button image should be aligned. </summary>
        /// <value> The button alignment. </value>
        [Bindable( false )]
        [DefaultValue( typeof( ToggleSwitchButtonAlignment ), "Center" )]
        [Category( "Appearance" )]
        [Description( "Gets or sets how the button image should be aligned" )]
        public ToggleSwitchButtonAlignment ButtonAlignment
        {
            get => this._ButtonAlignment;

            set {
                if ( value != this._ButtonAlignment )
                {
                    this._ButtonAlignment = value;
                    this.Refresh();
                }
            }
        }

        /// <summary>
        /// Gets or sets how the text or side image visible when Checked=true should be aligned.
        /// </summary>
        /// <value> The on side alignment. </value>
        [Bindable( false )]
        [DefaultValue( typeof( ToggleSwitchAlignment ), "Center" )]
        [Category( "Appearance" )]
        [Description( "Gets or sets how the text or side image visible when Checked=true should be aligned" )]
        public ToggleSwitchAlignment OnSideAlignment
        {
            get => this._OnSideAlignment;

            set {
                if ( value != this._OnSideAlignment )
                {
                    this._OnSideAlignment = value;
                    this.Refresh();
                }
            }
        }

        /// <summary>
        /// Gets or sets the button image visible when Checked=true and ButtonImage is not set.
        /// </summary>
        /// <value> The on button image. </value>
        [Bindable( false )]
        [DefaultValue( ( object ) default )]
        [Category( "Appearance" )]
        [Description( "Gets or sets the button image visible when Checked=true and ButtonImage is not set" )]
        public Image OnButtonImage
        {
            get => this._OnButtonImage;

            set {
                if ( !ReferenceEquals( value, this._OnButtonImage ) )
                {
                    this._OnButtonImage = value;
                    this.Refresh();
                }
            }
        }

        /// <summary>
        /// Gets or sets whether the button image visible when Checked=true should be scaled to fit.
        /// </summary>
        /// <value> The on button scale image to fit. </value>
        [Bindable( false )]
        [DefaultValue( false )]
        [Category( "Behavior" )]
        [Description( "Gets or sets whether the button image visible when Checked=true should be scaled to fit" )]
        public bool OnButtonScaleImageToFit
        {
            get => this._OnButtonScaleImage;

            set {
                if ( value != this._OnButtonScaleImage )
                {
                    this._OnButtonScaleImage = value;
                    this.Refresh();
                }
            }
        }

        /// <summary>
        /// Gets or sets how the button image visible when Checked=true should be aligned.
        /// </summary>
        /// <value> The on button alignment. </value>
        [Bindable( false )]
        [DefaultValue( typeof( ToggleSwitchButtonAlignment ), "Center" )]
        [Category( "Appearance" )]
        [Description( "Gets or sets how the button image visible when Checked=true should be aligned" )]
        public ToggleSwitchButtonAlignment OnButtonAlignment
        {
            get => this._OnButtonAlignment;

            set {
                if ( value != this._OnButtonAlignment )
                {
                    this._OnButtonAlignment = value;
                    this.Refresh();
                }
            }
        }

        /// <summary> Gets or sets whether the toggle change should be animated or not. </summary>
        /// <value> The use animation. </value>
        [Bindable( false )]
        [DefaultValue( true )]
        [Category( "Behavior" )]
        [Description( "Gets or sets whether the toggle change should be animated or not" )]
        public bool UseAnimation { get; set; } = true;

        /// <summary> Gets or sets the interval in ms between animation frames. </summary>
        /// <value> The animation interval. </value>
        [Bindable( false )]
        [DefaultValue( 1 )]
        [Category( "Behavior" )]
        [Description( "Gets or sets the interval in ms between animation frames" )]
        public int AnimationInterval
        {
            get => this._AnimationInterval;

            set {
                if ( value != this.AnimationInterval && value > 0 )
                {
                    this._AnimationInterval = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the step in pixes the button should be moved between each animation interval.
        /// </summary>
        /// <value> The animation step. </value>
        [Bindable( false )]
        [DefaultValue( 10 )]
        [Category( "Behavior" )]
        [Description( "Gets or sets the step in pixes the button should be moved between each animation interval" )]
        public int AnimationStep
        {
            get => this._AnimationStep;

            set {
                if ( value != this.AnimationStep && value > 0 )
                {
                    this._AnimationStep = value;
                }
            }
        }

        #region " Hidden Base Properties"

        /// <summary> Gets or sets the text. </summary>
        /// <value> The text. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new string Text
        {
            get => string.Empty;

            set => base.Text = string.Empty;
        }

        /// <summary> Gets or sets the color of the foreground. </summary>
        /// <value> The color of the foreground. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new Color ForeColor
        {
            get => Color.Black;

            set => base.ForeColor = Color.Black;
        }

        /// <summary> Gets or sets the font. </summary>
        /// <value> The font. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new Font Font
        {
            get => base.Font;

            set => base.Font = new Font( base.Font, FontStyle.Regular );
        }

        #endregion ' Hidden Base Properties

        #endregion ' Public Properties

        #region " Internal Properties"

        /// <summary> Gets the is button hovered. </summary>
        /// <value> The is button hovered. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        internal bool IsButtonHovered => this._IsButtonHovered && !this.IsButtonPressed;

        /// <summary> Gets the is button pressed. </summary>
        /// <value> The is button pressed. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        internal bool IsButtonPressed { get; private set; } = false;

        /// <summary> Gets the is left side hovered. </summary>
        /// <value> The is left side hovered. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        internal bool IsLeftSideHovered => this._IsLeftFieldHovered && !this.IsLeftSidePressed;

        /// <summary> Gets the is left side pressed. </summary>
        /// <value> The is left side pressed. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        internal bool IsLeftSidePressed { get; private set; } = false;

        /// <summary> Gets the is right side hovered. </summary>
        /// <value> The is right side hovered. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        internal bool IsRightSideHovered => this._IsRightFieldHovered && !this.IsRightSidePressed;

        /// <summary> Gets the is right side pressed. </summary>
        /// <value> The is right side pressed. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        internal bool IsRightSidePressed { get; private set; } = false;

        /// <summary> Gets the button value. </summary>
        /// <value> The button value. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        internal int ButtonValue
        {
            get => this._Animating || this._Moving ? this._ButtonValue : this._Checked ? this.Width - this._Renderer.GetButtonWidth() : 0;

            set {
                if ( value != this._ButtonValue )
                {
                    this._ButtonValue = value;
                    this.Refresh();
                }
            }
        }

        /// <summary> Gets the is button left side. </summary>
        /// <value> The is button on left side. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        internal bool IsButtonOnLeftSide => this.ButtonValue <= 0;

        /// <summary> Gets the is button right side. </summary>
        /// <value> The is button on right side. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        internal bool IsButtonOnRightSide => this.ButtonValue >= this.Width - this._Renderer.GetButtonWidth();

        /// <summary> Gets the is button moving left. </summary>
        /// <value> The is button moving left. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        internal bool IsButtonMovingLeft => this._Animating && !this.IsButtonOnLeftSide && this.AnimationResult == false;

        /// <summary> Gets the is button moving right. </summary>
        /// <value> The is button moving right. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        internal bool IsButtonMovingRight => this._Animating && !this.IsButtonOnRightSide && this.AnimationResult == true;

        /// <summary> Gets the animation result. </summary>
        /// <value> The animation result. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        internal bool AnimationResult { get; private set; } = false;

        #endregion ' Private Properties

        #region " Overridden Control Methods"

        /// <summary> Gets the default size of the control. </summary>
        /// <value> The default <see cref="T:System.Drawing.Size" /> of the control. </value>
        protected override Size DefaultSize => new( 50, 19 );

        /// <summary> Paints the background of the control. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="pevent"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains
        /// information about the control to paint. </param>
        protected override void OnPaintBackground( PaintEventArgs pevent )
        {
            if ( pevent is null )
            {
                return;
            }

            pevent.Graphics.ResetClip();
            base.OnPaintBackground( pevent );
            if ( this._Renderer is object )
            {
                this._Renderer.RenderBackground( pevent );
            }
        }

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.Paint" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnPaint( PaintEventArgs e )
        {
            if ( e is null )
            {
                return;
            }

            e.Graphics.ResetClip();
            base.OnPaint( e );
            if ( this._Renderer is object )
            {
                this._Renderer.RenderControl( e );
            }
        }

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.MouseMove" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnMouseMove( MouseEventArgs e )
        {
            if ( e is null )
            {
                return;
            }

            this._LastMouseEventArgs = e;
            int buttonWidth = this._Renderer.GetButtonWidth();
            var rectangle = this._Renderer.GetButtonRectangle( buttonWidth );
            if ( this._Moving )
            {
                int val = this._XValue + (e.Location.X - this._XOffset);
                if ( val < 0 )
                {
                    val = 0;
                }

                if ( val > this.Width - buttonWidth )
                {
                    val = this.Width - buttonWidth;
                }

                this.ButtonValue = val;
                this.Refresh();
                return;
            }

            if ( rectangle.Contains( e.Location ) )
            {
                this._IsButtonHovered = true;
                this._IsLeftFieldHovered = false;
                this._IsRightFieldHovered = false;
            }
            else if ( e.Location.X > rectangle.X + rectangle.Width )
            {
                this._IsButtonHovered = false;
                this._IsLeftFieldHovered = false;
                this._IsRightFieldHovered = true;
            }
            else if ( e.Location.X < rectangle.X )
            {
                this._IsButtonHovered = false;
                this._IsLeftFieldHovered = true;
                this._IsRightFieldHovered = false;
            }

            this.Refresh();
        }

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.MouseDown" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnMouseDown( MouseEventArgs e )
        {
            if ( e is null )
            {
                return;
            }

            if ( this._Animating || !this.AllowUserChange )
            {
                return;
            }

            int buttonWidth = this._Renderer.GetButtonWidth();
            // INSTANT VB NOTE: The variable buttonRectangle was renamed since Visual Basic does not handle local variables named the same as class members well:
            var buttonRectangle_Renamed = this._Renderer.GetButtonRectangle( buttonWidth );
            this._SavedButtonValue = this.ButtonValue;
            if ( buttonRectangle_Renamed.Contains( e.Location ) )
            {
                this.IsButtonPressed = true;
                this.IsLeftSidePressed = false;
                this.IsRightSidePressed = false;
                this._Moving = true;
                this._XOffset = e.Location.X;
                this._ButtonValue = buttonRectangle_Renamed.X;
                this._XValue = this.ButtonValue;
            }
            else if ( e.Location.X > buttonRectangle_Renamed.X + buttonRectangle_Renamed.Width )
            {
                this.IsButtonPressed = false;
                this.IsLeftSidePressed = false;
                this.IsRightSidePressed = true;
            }
            else if ( e.Location.X < buttonRectangle_Renamed.X )
            {
                this.IsButtonPressed = false;
                this.IsLeftSidePressed = true;
                this.IsRightSidePressed = false;
            }

            this.Refresh();
        }

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.MouseUp" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnMouseUp( MouseEventArgs e )
        {
            if ( this._Animating || !this.AllowUserChange )
            {
                return;
            }

            int buttonWidth = this._Renderer.GetButtonWidth();
            bool wasLeftSidePressed = this.IsLeftSidePressed;
            bool wasRightSidePressed = this.IsRightSidePressed;
            this.IsButtonPressed = false;
            this.IsLeftSidePressed = false;
            this.IsRightSidePressed = false;
            if ( this._Moving )
            {
                int percentage = ( int ) Conversion.Fix( 100d * this.ButtonValue / (this.Width - ( double ) buttonWidth) );
                if ( this._Checked )
                {
                    if ( percentage <= 100 - this.ThresholdPercentage )
                    {
                        this._AnimationTarget = 0;
                        this.BeginAnimation( false );
                    }
                    else if ( this.ToggleOnButtonClick && this._SavedButtonValue == this.ButtonValue )
                    {
                        this._AnimationTarget = 0;
                        this.BeginAnimation( false );
                    }
                    else
                    {
                        this._AnimationTarget = this.Width - buttonWidth;
                        this.BeginAnimation( true );
                    }
                }
                else if ( percentage >= this.ThresholdPercentage )
                {
                    this._AnimationTarget = this.Width - buttonWidth;
                    this.BeginAnimation( true );
                }
                else if ( this.ToggleOnButtonClick && this._SavedButtonValue == this.ButtonValue )
                {
                    this._AnimationTarget = this.Width - buttonWidth;
                    this.BeginAnimation( true );
                }
                else
                {
                    this._AnimationTarget = 0;
                    this.BeginAnimation( false );
                }

                this._Moving = false;
                return;
            }

            if ( this.IsButtonOnRightSide )
            {
                this._ButtonValue = this.Width - buttonWidth;
                this._AnimationTarget = 0;
            }
            else
            {
                this._ButtonValue = 0;
                this._AnimationTarget = this.Width - buttonWidth;
            }

            if ( wasLeftSidePressed && this.ToggleOnSideClick )
            {
                this.SetValueInternal( false );
            }
            else if ( wasRightSidePressed && this.ToggleOnSideClick )
            {
                this.SetValueInternal( true );
            }

            this.Refresh();
        }

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.MouseLeave" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnMouseLeave( EventArgs e )
        {
            this._IsButtonHovered = false;
            this._IsLeftFieldHovered = false;
            this._IsRightFieldHovered = false;
            this.IsButtonPressed = false;
            this.IsLeftSidePressed = false;
            this.IsRightSidePressed = false;
            this.Refresh();
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.EnabledChanged" /> event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnEnabledChanged( EventArgs e )
        {
            base.OnEnabledChanged( e );
            this.Refresh();
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.RegionChanged" /> event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnRegionChanged( EventArgs e )
        {
            base.OnRegionChanged( e );
            this.Refresh();
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.SizeChanged" /> event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnSizeChanged( EventArgs e )
        {
            if ( this._AnimationTarget > 0 )
            {
                int buttonWidth = this._Renderer.GetButtonWidth();
                this._AnimationTarget = this.Width - buttonWidth;
            }

            base.OnSizeChanged( e );
        }

        #endregion ' Overridden Control Methods

        #region " Private Methods"

        /// <summary> Sets value internal. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="checkedValue"> True to checked value. </param>
        private void SetValueInternal( bool checkedValue )
        {
            if ( checkedValue == this._Checked )
            {
                return;
            }

            while ( this._Animating )
            {
                Application.DoEvents();
            }

            this.BeginAnimation( checkedValue );
        }

        /// <summary> Begins an animation. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="checkedValue"> True to checked value. </param>
        private void BeginAnimation( bool checkedValue )
        {
            this._Animating = true;
            this.AnimationResult = checkedValue;
            if ( this._AnimationTimer is object && this.UseAnimation )
            {
                this._AnimationTimer.Interval = this._AnimationInterval;
                this._AnimationTimer.Enabled = true;
            }
            else
            {
                this.AnimationComplete();
            }
        }

        /// <summary> Event handler. Called by AnimationTimer for tick events. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void AnimationTimer_Tick( object sender, EventArgs e )
        {
            this._AnimationTimer.Enabled = false;
            int newButtonValue;
            bool animationDone;
            if ( this.IsButtonMovingRight )
            {
                newButtonValue = this.ButtonValue + this._AnimationStep;
                if ( newButtonValue > this._AnimationTarget )
                {
                    newButtonValue = this._AnimationTarget;
                }

                this.ButtonValue = newButtonValue;
                animationDone = this.ButtonValue >= this._AnimationTarget;
            }
            else
            {
                newButtonValue = this.ButtonValue - this._AnimationStep;
                if ( newButtonValue < this._AnimationTarget )
                {
                    newButtonValue = this._AnimationTarget;
                }

                this.ButtonValue = newButtonValue;
                animationDone = this.ButtonValue <= this._AnimationTarget;
            }

            if ( animationDone )
            {
                this.AnimationComplete();
            }
            else
            {
                this._AnimationTimer.Enabled = true;
            }
        }

        /// <summary> Animation complete. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void AnimationComplete()
        {
            this._Animating = false;
            this._Moving = false;
            this._Checked = this.AnimationResult;
            this._IsButtonHovered = false;
            this.IsButtonPressed = false;
            this._IsLeftFieldHovered = false;
            this.IsLeftSidePressed = false;
            this._IsRightFieldHovered = false;
            this.IsRightSidePressed = false;
            this.Refresh();
            CheckedChanged?.Invoke( this, new EventArgs() );
            if ( this._LastMouseEventArgs is object )
            {
                this.OnMouseMove( this._LastMouseEventArgs );
            }

            this._LastMouseEventArgs = null;
        }

        #endregion ' Private Methods

        #region " EVENTS "

        /// <summary> Event queue for all listeners interested in CheckedChanged events. </summary>
        [Description( "Raised when the ToggleSwitch has changed state" )]
        public event EventHandler<EventArgs> CheckedChanged;

        /// <summary> Removes event handler. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The handler. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void RemoveCheckedChangedEventHandler( EventHandler<EventArgs> value )
        {
            foreach ( Delegate d in value is null ? (Array.Empty<Delegate>()) : value.GetInvocationList() )
            {
                try
                {
                    CheckedChanged -= ( EventHandler<EventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }
        }

        #endregion

        #region " Private Members"

        /// <summary> The animation timer. </summary>
        private readonly Timer _AnimationTimer = new();

        /// <summary> The renderer. </summary>
        private ToggleSwitchRendererBase _Renderer;

        /// <summary> The style. </summary>
        private ToggleSwitchStyle _Style = ToggleSwitchStyle.Metro;

        /// <summary> True if checked. </summary>
        private bool _Checked = false;

        /// <summary> True to moving. </summary>
        private bool _Moving = false;

        /// <summary> True to animating. </summary>
        private bool _Animating = false;

        /// <summary> The animation target. </summary>
        private int _AnimationTarget = 0;

        /// <summary> The animation interval. </summary>
        private int _AnimationInterval = 1;

        /// <summary> The animation step. </summary>
        private int _AnimationStep = 10;

        /// <summary> True if is left field hovered, false if not. </summary>
        private bool _IsLeftFieldHovered = false;

        /// <summary> True if is button hovered, false if not. </summary>
        private bool _IsButtonHovered = false;

        /// <summary> True if is right field hovered, false if not. </summary>
        private bool _IsRightFieldHovered = false;

        /// <summary> The button value. </summary>
        private int _ButtonValue = 0;

        /// <summary> The saved button value. </summary>
        private int _SavedButtonValue = 0;

        /// <summary> The offset. </summary>
        private int _XOffset = 0;

        /// <summary> The value. </summary>
        private int _XValue = 0;

        /// <summary> True to disable, false to enable the gray when. </summary>
        private bool _GrayWhenDisabled = true;

        /// <summary> Mouse event information. </summary>
        private MouseEventArgs _LastMouseEventArgs = null;

        /// <summary> True to button scale image. </summary>
        private bool _ButtonScaleImage;

        /// <summary> The button alignment. </summary>
        private ToggleSwitchButtonAlignment _ButtonAlignment = ToggleSwitchButtonAlignment.Center;

        /// <summary> The button image. </summary>
        private Image _ButtonImage = null;

        /// <summary> The off text. </summary>
        private string _OffText = string.Empty;

        /// <summary> The off foreground color. </summary>
        private Color _OffForeColor = Color.Black;

        /// <summary> The off font. </summary>
        private Font _OffFont;

        /// <summary> The off side image. </summary>
        private Image _OffSideImage = null;

        /// <summary> True to off side scale image. </summary>
        private bool _OffSideScaleImage;

        /// <summary> The off side alignment. </summary>
        private ToggleSwitchAlignment _OffSideAlignment = ToggleSwitchAlignment.Center;

        /// <summary> The off button image. </summary>
        private Image _OffButtonImage = null;

        /// <summary> True to off button scale image. </summary>
        private bool _OffButtonScaleImage;

        /// <summary> The off button alignment. </summary>
        private ToggleSwitchButtonAlignment _OffButtonAlignment = ToggleSwitchButtonAlignment.Center;

        /// <summary> The on text. </summary>
        private string _OnText = string.Empty;

        /// <summary> The on foreground color. </summary>
        private Color _OnForeColor = Color.Black;

        /// <summary> The on font. </summary>
        private Font _OnFont;

        /// <summary> The on side image. </summary>
        private Image _OnSideImage = null;

        /// <summary> True to on side scale image. </summary>
        private bool _OnSideScaleImage;

        /// <summary> The on side alignment. </summary>
        private ToggleSwitchAlignment _OnSideAlignment = ToggleSwitchAlignment.Center;

        /// <summary> The on button image. </summary>
        private Image _OnButtonImage = null;

        /// <summary> True to on button scale image. </summary>
        private bool _OnButtonScaleImage;

        /// <summary> The on button alignment. </summary>
        private ToggleSwitchButtonAlignment _OnButtonAlignment = ToggleSwitchButtonAlignment.Center;

        #endregion ' Private Members

    }

    #region " Enums"

    /// <summary> Values that represent toggle switch styles. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public enum ToggleSwitchStyle
    {

        /// <summary> An enum constant representing the metro option. </summary>
        Metro,

        /// <summary> An enum constant representing the android option. </summary>
        Android,

        /// <summary> An enum constant representing the ios 5 option. </summary>
        Ios5,

        /// <summary> An enum constant representing the brushed metal option. </summary>
        BrushedMetal,

        /// <summary> An enum constant representing the Operating system 10 option. </summary>
        OS10,

        /// <summary> An enum constant representing the carbon option. </summary>
        Carbon,

        /// <summary> An enum constant representing the iPhone option. </summary>
        IPhone,

        /// <summary> An enum constant representing the fancy option. </summary>
        Fancy,

        /// <summary> An enum constant representing the modern option. </summary>
        Modern
    }

    /// <summary> Values that represent toggle switch alignments. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public enum ToggleSwitchAlignment
    {

        /// <summary> An enum constant representing the near option. </summary>
        Near,

        /// <summary> An enum constant representing the center option. </summary>
        Center,

        /// <summary> An enum constant representing the far option. </summary>
        Far
    }

    /// <summary> Values that represent toggle switch button alignments. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public enum ToggleSwitchButtonAlignment
    {

        /// <summary> An enum constant representing the left option. </summary>
        Left,

        /// <summary> An enum constant representing the center option. </summary>
        Center,

        /// <summary> An enum constant representing the right option. </summary>
        Right
    }
}

#endregion ' Enums

