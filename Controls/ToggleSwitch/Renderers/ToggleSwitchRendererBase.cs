using System.Drawing;
using System.Windows.Forms;

using isr.Core.ColorExtensions;

namespace isr.Core.Controls
{

    /// <summary> A toggle switch renderer base. </summary>
    /// <remarks>
    /// (c) 2015 Johnny J.. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-12-04 </para>
    /// </remarks>
    public abstract class ToggleSwitchRendererBase
    {

        #region " Constructor"

        /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        protected ToggleSwitchRendererBase()
        {
        }

        /// <summary> Sets toggle switch. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="toggleSwitch"> The toggle switch. </param>
        internal void SetToggleSwitch( ToggleSwitch toggleSwitch )
        {
            this.ToggleSwitch = toggleSwitch;
        }

        /// <summary> Gets the toggle switch. </summary>
        /// <value> The toggle switch. </value>
        internal ToggleSwitch ToggleSwitch { get; private set; }

        #endregion ' Constructor

        #region " Render Methods"

        /// <summary> Renders the background described by e. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> Paint event information. </param>
        public void RenderBackground( PaintEventArgs e )
        {
            if ( e is null )
            {
                return;
            }

            if ( this.ToggleSwitch is null )
            {
                return;
            }

            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            var controlRectangle = new Rectangle( 0, 0, this.ToggleSwitch.Width, this.ToggleSwitch.Height );
            this.FillBackground( e.Graphics, controlRectangle );
            this.RenderBorder( e.Graphics, controlRectangle );
        }

        /// <summary> Renders the control described by e. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> Paint event information. </param>
        public void RenderControl( PaintEventArgs e )
        {
            if ( e is null )
            {
                return;
            }

            if ( this.ToggleSwitch is null )
            {
                return;
            }

            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            var buttonRectangle = this.GetButtonRectangle();
            int totalToggleFieldWidth = this.ToggleSwitch.Width - buttonRectangle.Width;
            if ( buttonRectangle.X > 0 )
            {
                var leftRectangle = new Rectangle( 0, 0, buttonRectangle.X, this.ToggleSwitch.Height );
                if ( leftRectangle.Width > 0 )
                {
                    this.RenderLeftToggleField( e.Graphics, leftRectangle, totalToggleFieldWidth );
                }
            }

            if ( buttonRectangle.X + buttonRectangle.Width < e.ClipRectangle.Width )
            {
                var rightRectangle = new Rectangle( buttonRectangle.X + buttonRectangle.Width, 0, this.ToggleSwitch.Width - buttonRectangle.X - buttonRectangle.Width, this.ToggleSwitch.Height );
                if ( rightRectangle.Width > 0 )
                {
                    this.RenderRightToggleField( e.Graphics, rightRectangle, totalToggleFieldWidth );
                }
            }

            this.RenderButton( e.Graphics, buttonRectangle );
        }

        /// <summary> Fill background. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="g">                The Graphics to process. </param>
        /// <param name="controlRectangle"> The control rectangle. </param>
        public void FillBackground( Graphics g, Rectangle controlRectangle )
        {
            if ( g is null )
            {
                return;
            }

            var backColor = !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled ? this.ToggleSwitch.BackColor.ToGrayscale() : this.ToggleSwitch.BackColor;
            using Brush backBrush = new SolidBrush( backColor );
            g.FillRectangle( backBrush, controlRectangle );
        }

        /// <summary> Renders the border. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="g">               The Graphics to process. </param>
        /// <param name="borderRectangle"> The border rectangle. </param>
        public abstract void RenderBorder( Graphics g, Rectangle borderRectangle );

        /// <summary> Renders the left toggle field. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="g">                     The Graphics to process. </param>
        /// <param name="leftRectangle">         The left rectangle. </param>
        /// <param name="totalToggleFieldWidth"> Width of the total toggle field. </param>
        public abstract void RenderLeftToggleField( Graphics g, Rectangle leftRectangle, int totalToggleFieldWidth );

        /// <summary> Renders the right toggle field. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="g">                     The Graphics to process. </param>
        /// <param name="rightRectangle">        The right rectangle. </param>
        /// <param name="totalToggleFieldWidth"> Width of the total toggle field. </param>
        public abstract void RenderRightToggleField( Graphics g, Rectangle rightRectangle, int totalToggleFieldWidth );

        /// <summary> Renders the button. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="g">               The Graphics to process. </param>
        /// <param name="buttonRectangle"> The button rectangle. </param>
        public abstract void RenderButton( Graphics g, Rectangle buttonRectangle );

        #endregion ' Render Methods

        #region " Helper Methods"

        /// <summary> Gets button width. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> The button width. </returns>
        public abstract int GetButtonWidth();

        /// <summary> Gets button rectangle. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> The button rectangle. </returns>
        public abstract Rectangle GetButtonRectangle();

        /// <summary> Gets button rectangle. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="buttonWidth"> Width of the button. </param>
        /// <returns> The button rectangle. </returns>
        public abstract Rectangle GetButtonRectangle( int buttonWidth );

        #endregion ' Helper Methods
    }
}
