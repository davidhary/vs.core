using System;
using System.Drawing;

using isr.Core.ColorExtensions;

using Microsoft.VisualBasic;

namespace isr.Core.Controls
{

    /// <summary> A toggle switch android renderer. </summary>
    /// <remarks>
    /// (c) 2015 Johnny J.. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-12-04 </para>
    /// </remarks>
    public class ToggleSwitchAndroidRenderer : ToggleSwitchRendererBase
    {

        #region " Constructor"

        /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public ToggleSwitchAndroidRenderer()
        {
            this.BorderColor = Color.FromArgb( 255, 166, 166, 166 );
            this.BackColor = Color.FromArgb( 255, 32, 32, 32 );
            this.LeftSideColor = Color.FromArgb( 255, 32, 32, 32 );
            this.RightSideColor = Color.FromArgb( 255, 32, 32, 32 );
            this.OffButtonColor = Color.FromArgb( 255, 70, 70, 70 );
            this.OnButtonColor = Color.FromArgb( 255, 27, 161, 226 );
            this.OffButtonBorderColor = Color.FromArgb( 255, 70, 70, 70 );
            this.OnButtonBorderColor = Color.FromArgb( 255, 27, 161, 226 );
            this.SlantAngle = 8;
        }

        #endregion ' Constructor

        #region " Public Properties"

        /// <summary> Gets or sets the color of the border. </summary>
        /// <value> The color of the border. </value>
        public Color BorderColor { get; set; }

        /// <summary> Gets or sets the color of the back. </summary>
        /// <value> The color of the back. </value>
        public Color BackColor { get; set; }

        /// <summary> Gets or sets the color of the left side. </summary>
        /// <value> The color of the left side. </value>
        public Color LeftSideColor { get; set; }

        /// <summary> Gets or sets the color of the right side. </summary>
        /// <value> The color of the right side. </value>
        public Color RightSideColor { get; set; }

        /// <summary> Gets or sets the color of the off button. </summary>
        /// <value> The color of the off button. </value>
        public Color OffButtonColor { get; set; }

        /// <summary> Gets or sets the color of the on button. </summary>
        /// <value> The color of the on button. </value>
        public Color OnButtonColor { get; set; }

        /// <summary> Gets or sets the color of the off button border. </summary>
        /// <value> The color of the off button border. </value>
        public Color OffButtonBorderColor { get; set; }

        /// <summary> Gets or sets the color of the on button border. </summary>
        /// <value> The color of the on button border. </value>
        public Color OnButtonBorderColor { get; set; }

        /// <summary> Gets or sets the slant angle. </summary>
        /// <value> The slant angle. </value>
        public int SlantAngle { get; set; }

        #endregion ' Public Properties

        #region " Render Method Implementations"

        /// <summary> Renders the border. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="g">               The Graphics to process. </param>
        /// <param name="borderRectangle"> The border rectangle. </param>
        public override void RenderBorder( Graphics g, Rectangle borderRectangle )
        {
            if ( g is null )
            {
                return;
            }

            var renderColor = !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled ? this.BorderColor.ToGrayscale() : this.BorderColor;
            g.SetClip( borderRectangle );
            using var borderPen = new Pen( renderColor );
            g.DrawRectangle( borderPen, borderRectangle.X, borderRectangle.Y, borderRectangle.Width - 1, borderRectangle.Height - 1 );
        }

        /// <summary> Renders the left toggle field. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="g">                     The Graphics to process. </param>
        /// <param name="leftRectangle">         The left rectangle. </param>
        /// <param name="totalToggleFieldWidth"> Width of the total toggle field. </param>
        public override void RenderLeftToggleField( Graphics g, Rectangle leftRectangle, int totalToggleFieldWidth )
        {
            if ( g is null )
            {
                return;
            }

            var leftColor = this.LeftSideColor;
            if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
            {
                leftColor = leftColor.ToGrayscale();
            }

            var controlRectangle = this.GetInnerControlRectangle();
            g.SetClip( controlRectangle );
            int halfCathetusLength = this.GetHalfCathetusLengthBasedOnAngle();
            var adjustedLeftRect = new Rectangle( leftRectangle.X, leftRectangle.Y, leftRectangle.Width + halfCathetusLength, leftRectangle.Height );
            g.IntersectClip( adjustedLeftRect );
            using ( Brush leftBrush = new SolidBrush( leftColor ) )
            {
                g.FillRectangle( leftBrush, adjustedLeftRect );
            }

            g.ResetClip();
        }

        /// <summary> Renders the right toggle field. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="g">                     The Graphics to process. </param>
        /// <param name="rightRectangle">        The right rectangle. </param>
        /// <param name="totalToggleFieldWidth"> Width of the total toggle field. </param>
        public override void RenderRightToggleField( Graphics g, Rectangle rightRectangle, int totalToggleFieldWidth )
        {
            if ( g is null )
            {
                return;
            }

            var rightColor = this.RightSideColor;
            if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
            {
                rightColor = rightColor.ToGrayscale();
            }

            var controlRectangle = this.GetInnerControlRectangle();
            g.SetClip( controlRectangle );
            int halfCathetusLength = this.GetHalfCathetusLengthBasedOnAngle();
            var adjustedRightRect = new Rectangle( rightRectangle.X - halfCathetusLength, rightRectangle.Y, rightRectangle.Width + halfCathetusLength, rightRectangle.Height );
            g.IntersectClip( adjustedRightRect );
            using ( Brush rightBrush = new SolidBrush( rightColor ) )
            {
                g.FillRectangle( rightBrush, adjustedRightRect );
            }

            g.ResetClip();
        }

        /// <summary> Renders the button. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="g">               The Graphics to process. </param>
        /// <param name="buttonRectangle"> The button rectangle. </param>
        public override void RenderButton( Graphics g, Rectangle buttonRectangle )
        {
            if ( g is null )
            {
                return;
            }

            var controlRectangle = this.GetInnerControlRectangle();
            g.SetClip( controlRectangle );
            int fullCathetusLength = this.GetCathetusLengthBasedOnAngle();
            int dblFullCathetusLength = 2 * fullCathetusLength;
            var polygonPoints = new Point[4];
            var adjustedButtonRect = new Rectangle( buttonRectangle.X - fullCathetusLength, controlRectangle.Y, buttonRectangle.Width + dblFullCathetusLength, controlRectangle.Height );
            if ( this.SlantAngle > 0 )
            {
                polygonPoints[0] = new Point( adjustedButtonRect.X + fullCathetusLength, adjustedButtonRect.Y );
                polygonPoints[1] = new Point( adjustedButtonRect.X + adjustedButtonRect.Width - 1, adjustedButtonRect.Y );
                polygonPoints[2] = new Point( adjustedButtonRect.X + adjustedButtonRect.Width - fullCathetusLength - 1, adjustedButtonRect.Y + adjustedButtonRect.Height - 1 );
                polygonPoints[3] = new Point( adjustedButtonRect.X, adjustedButtonRect.Y + adjustedButtonRect.Height - 1 );
            }
            else
            {
                polygonPoints[0] = new Point( adjustedButtonRect.X, adjustedButtonRect.Y );
                polygonPoints[1] = new Point( adjustedButtonRect.X + adjustedButtonRect.Width - fullCathetusLength - 1, adjustedButtonRect.Y );
                polygonPoints[2] = new Point( adjustedButtonRect.X + adjustedButtonRect.Width - 1, adjustedButtonRect.Y + adjustedButtonRect.Height - 1 );
                polygonPoints[3] = new Point( adjustedButtonRect.X + fullCathetusLength, adjustedButtonRect.Y + adjustedButtonRect.Height - 1 );
            }

            g.IntersectClip( adjustedButtonRect );
            var buttonColor = this.ToggleSwitch.Checked ? this.OnButtonColor : this.OffButtonColor;
            var buttonBorderColor = this.ToggleSwitch.Checked ? this.OnButtonBorderColor : this.OffButtonBorderColor;
            if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
            {
                buttonColor = buttonColor.ToGrayscale();
                buttonBorderColor = buttonBorderColor.ToGrayscale();
            }

            using ( var buttonPen = new Pen( buttonBorderColor ) )
            {
                g.DrawPolygon( buttonPen, polygonPoints );
            }

            using ( Brush buttonBrush = new SolidBrush( buttonColor ) )
            {
                g.FillPolygon( buttonBrush, polygonPoints );
            }

            var buttonImage = this.ToggleSwitch.ButtonImage ?? (this.ToggleSwitch.Checked ? this.ToggleSwitch.OnButtonImage : this.ToggleSwitch.OffButtonImage);
            string buttonText = this.ToggleSwitch.Checked ? this.ToggleSwitch.OnText : this.ToggleSwitch.OffText;
            if ( buttonImage is object || !string.IsNullOrEmpty( buttonText ) )
            {
                var alignment = this.ToggleSwitch.ButtonImage is object ? this.ToggleSwitch.ButtonAlignment : this.ToggleSwitch.Checked ? this.ToggleSwitch.OnButtonAlignment : this.ToggleSwitch.OffButtonAlignment;
                if ( buttonImage is object )
                {
                    var imageSize = buttonImage.Size;
                    Rectangle imageRectangle;
                    int imageXPos = adjustedButtonRect.X;
                    bool scaleImage = this.ToggleSwitch.ButtonImage is object ? this.ToggleSwitch.ButtonScaleImageToFit : this.ToggleSwitch.Checked ? this.ToggleSwitch.OnButtonScaleImageToFit : this.ToggleSwitch.OffButtonScaleImageToFit;
                    if ( scaleImage )
                    {
                        var canvasSize = adjustedButtonRect.Size;
                        var resizedImageSize = ImageHelper.RescaleImageToFit( imageSize, canvasSize );
                        if ( alignment == ToggleSwitchButtonAlignment.Center )
                        {
                            imageXPos = ( int ) Conversion.Fix( adjustedButtonRect.X + (adjustedButtonRect.Width - ( float ) resizedImageSize.Width) / 2f );
                        }
                        else if ( alignment == ToggleSwitchButtonAlignment.Right )
                        {
                            imageXPos = ( int ) Conversion.Fix( adjustedButtonRect.X + ( float ) adjustedButtonRect.Width - resizedImageSize.Width );
                        }

                        imageRectangle = new Rectangle( imageXPos, ( int ) Conversion.Fix( adjustedButtonRect.Y + (adjustedButtonRect.Height - ( float ) resizedImageSize.Height) / 2f ), resizedImageSize.Width, resizedImageSize.Height );
                        if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
                        {
                            g.DrawImage( buttonImage, imageRectangle, 0, 0, buttonImage.Width, buttonImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes() );
                        }
                        else
                        {
                            g.DrawImage( buttonImage, imageRectangle );
                        }
                    }
                    else
                    {
                        if ( alignment == ToggleSwitchButtonAlignment.Center )
                        {
                            imageXPos = ( int ) Conversion.Fix( adjustedButtonRect.X + (adjustedButtonRect.Width - ( float ) imageSize.Width) / 2f );
                        }
                        else if ( alignment == ToggleSwitchButtonAlignment.Right )
                        {
                            imageXPos = ( int ) Conversion.Fix( adjustedButtonRect.X + ( float ) adjustedButtonRect.Width - imageSize.Width );
                        }

                        imageRectangle = new Rectangle( imageXPos, ( int ) Conversion.Fix( adjustedButtonRect.Y + (adjustedButtonRect.Height - ( float ) imageSize.Height) / 2f ), imageSize.Width, imageSize.Height );
                        if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
                        {
                            g.DrawImage( buttonImage, imageRectangle, 0, 0, buttonImage.Width, buttonImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes() );
                        }
                        else
                        {
                            g.DrawImageUnscaled( buttonImage, imageRectangle );
                        }
                    }
                }
                else if ( !string.IsNullOrEmpty( buttonText ) )
                {
                    var buttonFont = this.ToggleSwitch.Checked ? this.ToggleSwitch.OnFont : this.ToggleSwitch.OffFont;
                    var buttonForeColor = this.ToggleSwitch.Checked ? this.ToggleSwitch.OnForeColor : this.ToggleSwitch.OffForeColor;
                    if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
                    {
                        buttonForeColor = buttonForeColor.ToGrayscale();
                    }

                    var textSize = g.MeasureString( buttonText, buttonFont );
                    float textXPos = adjustedButtonRect.X;
                    if ( alignment == ToggleSwitchButtonAlignment.Center )
                    {
                        textXPos = adjustedButtonRect.X + (adjustedButtonRect.Width - textSize.Width) / 2f;
                    }
                    else if ( alignment == ToggleSwitchButtonAlignment.Right )
                    {
                        textXPos = adjustedButtonRect.X + ( float ) adjustedButtonRect.Width - textSize.Width;
                    }

                    var textRectangle = new RectangleF( textXPos, adjustedButtonRect.Y + (adjustedButtonRect.Height - textSize.Height) / 2f, textSize.Width, textSize.Height );
                    using Brush textBrush = new SolidBrush( buttonForeColor );
                    g.DrawString( buttonText, buttonFont, textBrush, textRectangle );
                }
            }

            g.ResetClip();
        }

        #endregion ' Render Method Implementations

        #region " Helper Method Implementations"

        /// <summary> Gets inner control rectangle. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> The inner control rectangle. </returns>
        public Rectangle GetInnerControlRectangle()
        {
            return new Rectangle( 1, 1, this.ToggleSwitch.Width - 2, this.ToggleSwitch.Height - 2 );
        }

        /// <summary> Gets cathetus length based on angle. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> The cathetus length based on angle. </returns>
        public int GetCathetusLengthBasedOnAngle()
        {
            if ( this.SlantAngle == 0 )
            {
                return 0;
            }

            double degrees = Math.Abs( this.SlantAngle );
            double radians = degrees * (Math.PI / 180d);
            double length = Math.Tan( radians ) * this.ToggleSwitch.Height;
            return ( int ) Conversion.Fix( length );
        }

        /// <summary> Gets half cathetus length based on angle. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> The half cathetus length based on angle. </returns>
        public int GetHalfCathetusLengthBasedOnAngle()
        {
            if ( this.SlantAngle == 0 )
            {
                return 0;
            }

            double degrees = Math.Abs( this.SlantAngle );
            double radians = degrees * (Math.PI / 180d);
            double length = Math.Tan( radians ) * this.ToggleSwitch.Height / 2d;
            return ( int ) Conversion.Fix( length );
        }

        /// <summary> Gets button width. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> The button width. </returns>
        public override int GetButtonWidth()
        {
            double buttonWidth = this.ToggleSwitch.Width / 2d;
            return ( int ) Conversion.Fix( buttonWidth );
        }

        /// <summary> Gets button rectangle. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> The button rectangle. </returns>
        public override Rectangle GetButtonRectangle()
        {
            int buttonWidth = this.GetButtonWidth();
            return this.GetButtonRectangle( buttonWidth );
        }

        /// <summary> Gets button rectangle. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="buttonWidth"> Width of the button. </param>
        /// <returns> The button rectangle. </returns>
        public override Rectangle GetButtonRectangle( int buttonWidth )
        {
            var buttonRect = new Rectangle( this.ToggleSwitch.ButtonValue, 0, buttonWidth, this.ToggleSwitch.Height );
            return buttonRect;
        }

        #endregion ' Helper Method Implementations
    }
}
