using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;

using isr.Core.ColorExtensions;

using Microsoft.VisualBasic;

namespace isr.Core.Controls
{

    /// <summary> A toggle switch fancy renderer. </summary>
    /// <remarks>
    /// (c) 2015 Johnny J.. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-12-04 </para>
    /// </remarks>
    public class ToggleSwitchFancyRenderer : ToggleSwitchRendererBase, IDisposable
    {

        #region " Constructor"

        /// <summary> Full pathname of the inner control file. </summary>
        private GraphicsPath _InnerControlPath = null;

        /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public ToggleSwitchFancyRenderer()
        {
            this.OuterBorderColor1 = Color.FromArgb( 255, 197, 199, 201 );
            this.OuterBorderColor2 = Color.FromArgb( 255, 207, 209, 212 );
            this.InnerBorderColor1 = Color.FromArgb( 200, 205, 208, 207 );
            this.InnerBorderColor2 = Color.FromArgb( 255, 207, 209, 212 );
            this.LeftSideBackColor1 = Color.FromArgb( 255, 61, 110, 6 );
            this.LeftSideBackColor2 = Color.FromArgb( 255, 93, 170, 9 );
            this.RightSideBackColor1 = Color.FromArgb( 255, 149, 0, 0 );
            this.RightSideBackColor2 = Color.FromArgb( 255, 198, 0, 0 );
            this.ButtonNormalBorderColor1 = Color.FromArgb( 255, 212, 209, 211 );
            this.ButtonNormalBorderColor2 = Color.FromArgb( 255, 197, 199, 201 );
            this.ButtonNormalUpperSurfaceColor1 = Color.FromArgb( 255, 252, 251, 252 );
            this.ButtonNormalUpperSurfaceColor2 = Color.FromArgb( 255, 247, 247, 247 );
            this.ButtonNormalLowerSurfaceColor1 = Color.FromArgb( 255, 233, 233, 233 );
            this.ButtonNormalLowerSurfaceColor2 = Color.FromArgb( 255, 225, 225, 225 );
            this.ButtonHoverBorderColor1 = Color.FromArgb( 255, 212, 207, 209 );
            this.ButtonHoverBorderColor2 = Color.FromArgb( 255, 223, 223, 223 );
            this.ButtonHoverUpperSurfaceColor1 = Color.FromArgb( 255, 240, 239, 240 );
            this.ButtonHoverUpperSurfaceColor2 = Color.FromArgb( 255, 235, 235, 235 );
            this.ButtonHoverLowerSurfaceColor1 = Color.FromArgb( 255, 221, 221, 221 );
            this.ButtonHoverLowerSurfaceColor2 = Color.FromArgb( 255, 214, 214, 214 );
            this.ButtonPressedBorderColor1 = Color.FromArgb( 255, 176, 176, 176 );
            this.ButtonPressedBorderColor2 = Color.FromArgb( 255, 176, 176, 176 );
            this.ButtonPressedUpperSurfaceColor1 = Color.FromArgb( 255, 189, 188, 189 );
            this.ButtonPressedUpperSurfaceColor2 = Color.FromArgb( 255, 185, 185, 185 );
            this.ButtonPressedLowerSurfaceColor1 = Color.FromArgb( 255, 175, 175, 175 );
            this.ButtonPressedLowerSurfaceColor2 = Color.FromArgb( 255, 169, 169, 169 );
            this.ButtonShadowColor1 = Color.FromArgb( 50, 0, 0, 0 );
            this.ButtonShadowColor2 = Color.FromArgb( 0, 0, 0, 0 );
            this.ButtonShadowWidth = 7;
            this.CornerRadius = 6;
        }

        #region " Disposable Support "

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose( true );

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary>
        /// Gets or sets the dispose status sentinel of the base class.  This applies to the derived
        /// class provided proper implementation.
        /// </summary>
        /// <value> <c>True</c> if disposed; otherwise, <c>False</c>. </value>
        protected bool IsDisposed { get; set; }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this._InnerControlPath?.Dispose();
                    this._InnerControlPath = null;
                }
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        #endregion


        #endregion ' Constructor

        #region " Public Properties"

        /// <summary> Gets or sets the outer border color 1. </summary>
        /// <value> The outer border color 1. </value>
        public Color OuterBorderColor1 { get; set; }

        /// <summary> Gets or sets the outer border color 2. </summary>
        /// <value> The outer border color 2. </value>
        public Color OuterBorderColor2 { get; set; }

        /// <summary> Gets or sets the inner border color 1. </summary>
        /// <value> The inner border color 1. </value>
        public Color InnerBorderColor1 { get; set; }

        /// <summary> Gets or sets the inner border color 2. </summary>
        /// <value> The inner border color 2. </value>
        public Color InnerBorderColor2 { get; set; }

        /// <summary> Gets or sets the left side back color 1. </summary>
        /// <value> The left side back color 1. </value>
        public Color LeftSideBackColor1 { get; set; }

        /// <summary> Gets or sets the left side back color 2. </summary>
        /// <value> The left side back color 2. </value>
        public Color LeftSideBackColor2 { get; set; }

        /// <summary> Gets or sets the right side back color 1. </summary>
        /// <value> The right side back color 1. </value>
        public Color RightSideBackColor1 { get; set; }

        /// <summary> Gets or sets the right side back color 2. </summary>
        /// <value> The right side back color 2. </value>
        public Color RightSideBackColor2 { get; set; }

        /// <summary> Gets or sets the button normal border color 1. </summary>
        /// <value> The button normal border color 1. </value>
        public Color ButtonNormalBorderColor1 { get; set; }

        /// <summary> Gets or sets the button normal border color 2. </summary>
        /// <value> The button normal border color 2. </value>
        public Color ButtonNormalBorderColor2 { get; set; }

        /// <summary> Gets or sets the button normal upper surface color 1. </summary>
        /// <value> The button normal upper surface color 1. </value>
        public Color ButtonNormalUpperSurfaceColor1 { get; set; }

        /// <summary> Gets or sets the button normal upper surface color 2. </summary>
        /// <value> The button normal upper surface color 2. </value>
        public Color ButtonNormalUpperSurfaceColor2 { get; set; }

        /// <summary> Gets or sets the button normal lower surface color 1. </summary>
        /// <value> The button normal lower surface color 1. </value>
        public Color ButtonNormalLowerSurfaceColor1 { get; set; }

        /// <summary> Gets or sets the button normal lower surface color 2. </summary>
        /// <value> The button normal lower surface color 2. </value>
        public Color ButtonNormalLowerSurfaceColor2 { get; set; }

        /// <summary> Gets or sets the button hover border color 1. </summary>
        /// <value> The button hover border color 1. </value>
        public Color ButtonHoverBorderColor1 { get; set; }

        /// <summary> Gets or sets the button hover border color 2. </summary>
        /// <value> The button hover border color 2. </value>
        public Color ButtonHoverBorderColor2 { get; set; }

        /// <summary> Gets or sets the button hover upper surface color 1. </summary>
        /// <value> The button hover upper surface color 1. </value>
        public Color ButtonHoverUpperSurfaceColor1 { get; set; }

        /// <summary> Gets or sets the button hover upper surface color 2. </summary>
        /// <value> The button hover upper surface color 2. </value>
        public Color ButtonHoverUpperSurfaceColor2 { get; set; }

        /// <summary> Gets or sets the button hover lower surface color 1. </summary>
        /// <value> The button hover lower surface color 1. </value>
        public Color ButtonHoverLowerSurfaceColor1 { get; set; }

        /// <summary> Gets or sets the button hover lower surface color 2. </summary>
        /// <value> The button hover lower surface color 2. </value>
        public Color ButtonHoverLowerSurfaceColor2 { get; set; }

        /// <summary> Gets or sets the button pressed border color 1. </summary>
        /// <value> The button pressed border color 1. </value>
        public Color ButtonPressedBorderColor1 { get; set; }

        /// <summary> Gets or sets the button pressed border color 2. </summary>
        /// <value> The button pressed border color 2. </value>
        public Color ButtonPressedBorderColor2 { get; set; }

        /// <summary> Gets or sets the button pressed upper surface color 1. </summary>
        /// <value> The button pressed upper surface color 1. </value>
        public Color ButtonPressedUpperSurfaceColor1 { get; set; }

        /// <summary> Gets or sets the button pressed upper surface color 2. </summary>
        /// <value> The button pressed upper surface color 2. </value>
        public Color ButtonPressedUpperSurfaceColor2 { get; set; }

        /// <summary> Gets or sets the button pressed lower surface color 1. </summary>
        /// <value> The button pressed lower surface color 1. </value>
        public Color ButtonPressedLowerSurfaceColor1 { get; set; }

        /// <summary> Gets or sets the button pressed lower surface color 2. </summary>
        /// <value> The button pressed lower surface color 2. </value>
        public Color ButtonPressedLowerSurfaceColor2 { get; set; }

        /// <summary> Gets or sets the button shadow color 1. </summary>
        /// <value> The button shadow color 1. </value>
        public Color ButtonShadowColor1 { get; set; }

        /// <summary> Gets or sets the button shadow color 2. </summary>
        /// <value> The button shadow color 2. </value>
        public Color ButtonShadowColor2 { get; set; }

        /// <summary> Gets or sets the width of the button shadow. </summary>
        /// <value> The width of the button shadow. </value>
        public int ButtonShadowWidth { get; set; }

        /// <summary> Gets or sets the corner radius. </summary>
        /// <value> The corner radius. </value>
        public int CornerRadius { get; set; }

        #endregion ' Public Properties

        #region " Render Method Implementations"

        /// <summary> Renders the border. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="g">               The Graphics to process. </param>
        /// <param name="borderRectangle"> The border rectangle. </param>
        public override void RenderBorder( Graphics g, Rectangle borderRectangle )
        {
            if ( g is null )
            {
                return;
            }

            g.SmoothingMode = SmoothingMode.HighQuality;
            g.PixelOffsetMode = PixelOffsetMode.HighQuality;
            g.InterpolationMode = InterpolationMode.HighQualityBilinear;

            // Draw outer border
            using ( var outerBorderPath = this.GetRoundedRectanglePath( borderRectangle, this.CornerRadius ) )
            {
                g.SetClip( outerBorderPath );

                // INSTANT VB NOTE: The variable outerBorderColor1 was renamed since Visual Basic does not handle local variables named the same as class members well:
                var outerBorderColor1_Renamed = !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled ? this.OuterBorderColor1.ToGrayscale() : this.OuterBorderColor1;
                // INSTANT VB NOTE: The variable outerBorderColor2 was renamed since Visual Basic does not handle local variables named the same as class members well:
                var outerBorderColor2_Renamed = !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled ? this.OuterBorderColor2.ToGrayscale() : this.OuterBorderColor2;
                using ( Brush outerBorderBrush = new LinearGradientBrush( borderRectangle, outerBorderColor1_Renamed, outerBorderColor2_Renamed, LinearGradientMode.Vertical ) )
                {
                    g.FillPath( outerBorderBrush, outerBorderPath );
                }

                g.ResetClip();
            }

            // Draw inner border
            var innerborderRectangle = new Rectangle( borderRectangle.X + 1, borderRectangle.Y + 1, borderRectangle.Width - 2, borderRectangle.Height - 2 );
            using ( var innerBorderPath = this.GetRoundedRectanglePath( innerborderRectangle, this.CornerRadius ) )
            {
                g.SetClip( innerBorderPath );

                // INSTANT VB NOTE: The variable innerBorderColor1 was renamed since Visual Basic does not handle local variables named the same as class members well:
                var innerBorderColor1_Renamed = !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled ? this.InnerBorderColor1.ToGrayscale() : this.InnerBorderColor1;
                // INSTANT VB NOTE: The variable innerBorderColor2 was renamed since Visual Basic does not handle local variables named the same as class members well:
                var innerBorderColor2_Renamed = !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled ? this.InnerBorderColor2.ToGrayscale() : this.InnerBorderColor2;
                using ( Brush borderBrush = new LinearGradientBrush( borderRectangle, innerBorderColor1_Renamed, innerBorderColor2_Renamed, LinearGradientMode.Vertical ) )
                {
                    g.FillPath( borderBrush, innerBorderPath );
                }

                g.ResetClip();
            }

            var backgroundRectangle = new Rectangle( borderRectangle.X + 2, borderRectangle.Y + 2, borderRectangle.Width - 4, borderRectangle.Height - 4 );
            this._InnerControlPath = this.GetRoundedRectanglePath( backgroundRectangle, this.CornerRadius );
        }

        /// <summary> Renders the left toggle field. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="g">                     The Graphics to process. </param>
        /// <param name="leftRectangle">         The left rectangle. </param>
        /// <param name="totalToggleFieldWidth"> Width of the total toggle field. </param>
        public override void RenderLeftToggleField( Graphics g, Rectangle leftRectangle, int totalToggleFieldWidth )
        {
            if ( g is null )
            {
                return;
            }

            g.SmoothingMode = SmoothingMode.HighQuality;
            g.PixelOffsetMode = PixelOffsetMode.HighQuality;
            g.InterpolationMode = InterpolationMode.HighQualityBilinear;
            int buttonWidth = this.GetButtonWidth();

            // Draw inner background
            int gradientRectWidth = leftRectangle.Width + buttonWidth / 2;
            var gradientRectangle = new Rectangle( leftRectangle.X, leftRectangle.Y, gradientRectWidth, leftRectangle.Height );

            // INSTANT VB NOTE: The variable leftSideBackColor1 was renamed since Visual Basic does not handle local variables named the same as class members well:
            var leftSideBackColor1_Renamed = !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled ? this.LeftSideBackColor1.ToGrayscale() : this.LeftSideBackColor1;
            // INSTANT VB NOTE: The variable leftSideBackColor2 was renamed since Visual Basic does not handle local variables named the same as class members well:
            var leftSideBackColor2_Renamed = !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled ? this.LeftSideBackColor2.ToGrayscale() : this.LeftSideBackColor2;
            if ( this._InnerControlPath is object )
            {
                g.SetClip( this._InnerControlPath );
                g.IntersectClip( gradientRectangle );
            }
            else
            {
                g.SetClip( gradientRectangle );
            }

            using ( Brush backgroundBrush = new LinearGradientBrush( gradientRectangle, leftSideBackColor1_Renamed, leftSideBackColor2_Renamed, LinearGradientMode.Vertical ) )
            {
                g.FillRectangle( backgroundBrush, gradientRectangle );
            }

            g.ResetClip();
            var leftShadowRectangle = new Rectangle() {
                X = leftRectangle.X + leftRectangle.Width - this.ButtonShadowWidth,
                Y = leftRectangle.Y,
                Width = this.ButtonShadowWidth + this.CornerRadius,
                Height = leftRectangle.Height
            };
            if ( this._InnerControlPath is object )
            {
                g.SetClip( this._InnerControlPath );
                g.IntersectClip( leftShadowRectangle );
            }
            else
            {
                g.SetClip( leftShadowRectangle );
            }

            using ( Brush buttonShadowBrush = new LinearGradientBrush( leftShadowRectangle, this.ButtonShadowColor2, this.ButtonShadowColor1, LinearGradientMode.Horizontal ) )
            {
                g.FillRectangle( buttonShadowBrush, leftShadowRectangle );
            }

            g.ResetClip();

            // Draw image or text
            if ( this.ToggleSwitch.OnSideImage is object || !string.IsNullOrEmpty( this.ToggleSwitch.OnText ) )
            {
                var fullRectangle = new RectangleF( leftRectangle.X + 1 - (totalToggleFieldWidth - leftRectangle.Width), 1f, totalToggleFieldWidth - 1, this.ToggleSwitch.Height - 2 );
                if ( this._InnerControlPath is object )
                {
                    g.SetClip( this._InnerControlPath );
                    g.IntersectClip( fullRectangle );
                }
                else
                {
                    g.SetClip( fullRectangle );
                }

                if ( this.ToggleSwitch.OnSideImage is object )
                {
                    var imageSize = this.ToggleSwitch.OnSideImage.Size;
                    Rectangle imageRectangle;
                    int imageXPos = ( int ) Conversion.Fix( fullRectangle.X );
                    if ( this.ToggleSwitch.OnSideScaleImageToFit )
                    {
                        var canvasSize = new Size( ( int ) fullRectangle.Width, ( int ) fullRectangle.Height );
                        var resizedImageSize = ImageHelper.RescaleImageToFit( imageSize, canvasSize );
                        if ( this.ToggleSwitch.OnSideAlignment == ToggleSwitchAlignment.Center )
                        {
                            imageXPos = ( int ) Conversion.Fix( fullRectangle.X + (fullRectangle.Width - resizedImageSize.Width) / 2f );
                        }
                        else if ( this.ToggleSwitch.OnSideAlignment == ToggleSwitchAlignment.Near )
                        {
                            imageXPos = ( int ) Conversion.Fix( fullRectangle.X + fullRectangle.Width - resizedImageSize.Width );
                        }

                        imageRectangle = new Rectangle( imageXPos, ( int ) Conversion.Fix( fullRectangle.Y + (fullRectangle.Height - resizedImageSize.Height) / 2f ), resizedImageSize.Width, resizedImageSize.Height );
                        if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
                        {
                            g.DrawImage( this.ToggleSwitch.OnSideImage, imageRectangle, 0, 0, this.ToggleSwitch.OnSideImage.Width, this.ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes() );
                        }
                        else
                        {
                            g.DrawImage( this.ToggleSwitch.OnSideImage, imageRectangle );
                        }
                    }
                    else
                    {
                        if ( this.ToggleSwitch.OnSideAlignment == ToggleSwitchAlignment.Center )
                        {
                            imageXPos = ( int ) Conversion.Fix( fullRectangle.X + (fullRectangle.Width - imageSize.Width) / 2f );
                        }
                        else if ( this.ToggleSwitch.OnSideAlignment == ToggleSwitchAlignment.Near )
                        {
                            imageXPos = ( int ) Conversion.Fix( fullRectangle.X + fullRectangle.Width - imageSize.Width );
                        }

                        imageRectangle = new Rectangle( imageXPos, ( int ) Conversion.Fix( fullRectangle.Y + (fullRectangle.Height - imageSize.Height) / 2f ), imageSize.Width, imageSize.Height );
                        if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
                        {
                            g.DrawImage( this.ToggleSwitch.OnSideImage, imageRectangle, 0, 0, this.ToggleSwitch.OnSideImage.Width, this.ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes() );
                        }
                        else
                        {
                            g.DrawImageUnscaled( this.ToggleSwitch.OnSideImage, imageRectangle );
                        }
                    }
                }
                else if ( !string.IsNullOrEmpty( this.ToggleSwitch.OnText ) )
                {
                    var textSize = g.MeasureString( this.ToggleSwitch.OnText, this.ToggleSwitch.OnFont );
                    float textXPos = fullRectangle.X;
                    if ( this.ToggleSwitch.OnSideAlignment == ToggleSwitchAlignment.Center )
                    {
                        textXPos = fullRectangle.X + (fullRectangle.Width - textSize.Width) / 2f;
                    }
                    else if ( this.ToggleSwitch.OnSideAlignment == ToggleSwitchAlignment.Near )
                    {
                        textXPos = fullRectangle.X + fullRectangle.Width - textSize.Width;
                    }

                    var textRectangle = new RectangleF( textXPos, fullRectangle.Y + (fullRectangle.Height - textSize.Height) / 2f, textSize.Width, textSize.Height );
                    var textForeColor = this.ToggleSwitch.OnForeColor;
                    if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
                    {
                        textForeColor = textForeColor.ToGrayscale();
                    }

                    using Brush textBrush = new SolidBrush( textForeColor );
                    g.DrawString( this.ToggleSwitch.OnText, this.ToggleSwitch.OnFont, textBrush, textRectangle );
                }

                g.ResetClip();
            }
        }

        /// <summary> Renders the right toggle field. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="g">                     The Graphics to process. </param>
        /// <param name="rightRectangle">        The right rectangle. </param>
        /// <param name="totalToggleFieldWidth"> Width of the total toggle field. </param>
        public override void RenderRightToggleField( Graphics g, Rectangle rightRectangle, int totalToggleFieldWidth )
        {
            if ( g is null )
            {
                return;
            }

            g.SmoothingMode = SmoothingMode.HighQuality;
            g.PixelOffsetMode = PixelOffsetMode.HighQuality;
            g.InterpolationMode = InterpolationMode.HighQualityBilinear;
            int buttonWidth = this.GetButtonWidth();

            // Draw inner background
            int gradientRectWidth = rightRectangle.Width + buttonWidth / 2;
            var gradientRectangle = new Rectangle( rightRectangle.X - buttonWidth / 2, rightRectangle.Y, gradientRectWidth, rightRectangle.Height );

            // INSTANT VB NOTE: The variable rightSideBackColor1 was renamed since Visual Basic does not handle local variables named the same as class members well:
            var rightSideBackColor1_Renamed = !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled ? this.RightSideBackColor1.ToGrayscale() : this.RightSideBackColor1;
            // INSTANT VB NOTE: The variable rightSideBackColor2 was renamed since Visual Basic does not handle local variables named the same as class members well:
            var rightSideBackColor2_Renamed = !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled ? this.RightSideBackColor2.ToGrayscale() : this.RightSideBackColor2;
            if ( this._InnerControlPath is object )
            {
                g.SetClip( this._InnerControlPath );
                g.IntersectClip( gradientRectangle );
            }
            else
            {
                g.SetClip( gradientRectangle );
            }

            using ( Brush backgroundBrush = new LinearGradientBrush( gradientRectangle, rightSideBackColor1_Renamed, rightSideBackColor2_Renamed, LinearGradientMode.Vertical ) )
            {
                g.FillRectangle( backgroundBrush, gradientRectangle );
            }

            g.ResetClip();
            var rightShadowRectangle = new Rectangle() {
                X = rightRectangle.X - this.CornerRadius,
                Y = rightRectangle.Y,
                Width = this.ButtonShadowWidth + this.CornerRadius,
                Height = rightRectangle.Height
            };
            if ( this._InnerControlPath is object )
            {
                g.SetClip( this._InnerControlPath );
                g.IntersectClip( rightShadowRectangle );
            }
            else
            {
                g.SetClip( rightShadowRectangle );
            }

            using ( Brush buttonShadowBrush = new LinearGradientBrush( rightShadowRectangle, this.ButtonShadowColor1, this.ButtonShadowColor2, LinearGradientMode.Horizontal ) )
            {
                g.FillRectangle( buttonShadowBrush, rightShadowRectangle );
            }

            g.ResetClip();

            // Draw image or text
            if ( this.ToggleSwitch.OffSideImage is object || !string.IsNullOrEmpty( this.ToggleSwitch.OffText ) )
            {
                var fullRectangle = new RectangleF( rightRectangle.X, 1f, totalToggleFieldWidth - 1, this.ToggleSwitch.Height - 2 );
                if ( this._InnerControlPath is object )
                {
                    g.SetClip( this._InnerControlPath );
                    g.IntersectClip( fullRectangle );
                }
                else
                {
                    g.SetClip( fullRectangle );
                }

                if ( this.ToggleSwitch.OffSideImage is object )
                {
                    var imageSize = this.ToggleSwitch.OffSideImage.Size;
                    Rectangle imageRectangle;
                    int imageXPos = ( int ) Conversion.Fix( fullRectangle.X );
                    if ( this.ToggleSwitch.OffSideScaleImageToFit )
                    {
                        var canvasSize = new Size( ( int ) fullRectangle.Width, ( int ) fullRectangle.Height );
                        var resizedImageSize = ImageHelper.RescaleImageToFit( imageSize, canvasSize );
                        if ( this.ToggleSwitch.OffSideAlignment == ToggleSwitchAlignment.Center )
                        {
                            imageXPos = ( int ) Conversion.Fix( fullRectangle.X + (fullRectangle.Width - resizedImageSize.Width) / 2f );
                        }
                        else if ( this.ToggleSwitch.OffSideAlignment == ToggleSwitchAlignment.Far )
                        {
                            imageXPos = ( int ) Conversion.Fix( fullRectangle.X + fullRectangle.Width - resizedImageSize.Width );
                        }

                        imageRectangle = new Rectangle( imageXPos, ( int ) Conversion.Fix( fullRectangle.Y + (fullRectangle.Height - resizedImageSize.Height) / 2f ), resizedImageSize.Width, resizedImageSize.Height );
                        if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
                        {
                            g.DrawImage( this.ToggleSwitch.OnSideImage, imageRectangle, 0, 0, this.ToggleSwitch.OnSideImage.Width, this.ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes() );
                        }
                        else
                        {
                            g.DrawImage( this.ToggleSwitch.OnSideImage, imageRectangle );
                        }
                    }
                    else
                    {
                        if ( this.ToggleSwitch.OffSideAlignment == ToggleSwitchAlignment.Center )
                        {
                            imageXPos = ( int ) Conversion.Fix( fullRectangle.X + (fullRectangle.Width - imageSize.Width) / 2f );
                        }
                        else if ( this.ToggleSwitch.OffSideAlignment == ToggleSwitchAlignment.Far )
                        {
                            imageXPos = ( int ) Conversion.Fix( fullRectangle.X + fullRectangle.Width - imageSize.Width );
                        }

                        imageRectangle = new Rectangle( imageXPos, ( int ) Conversion.Fix( fullRectangle.Y + (fullRectangle.Height - imageSize.Height) / 2f ), imageSize.Width, imageSize.Height );
                        if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
                        {
                            g.DrawImage( this.ToggleSwitch.OnSideImage, imageRectangle, 0, 0, this.ToggleSwitch.OnSideImage.Width, this.ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes() );
                        }
                        else
                        {
                            g.DrawImageUnscaled( this.ToggleSwitch.OffSideImage, imageRectangle );
                        }
                    }
                }
                else if ( !string.IsNullOrEmpty( this.ToggleSwitch.OffText ) )
                {
                    var textSize = g.MeasureString( this.ToggleSwitch.OffText, this.ToggleSwitch.OffFont );
                    float textXPos = fullRectangle.X;
                    if ( this.ToggleSwitch.OffSideAlignment == ToggleSwitchAlignment.Center )
                    {
                        textXPos = fullRectangle.X + (fullRectangle.Width - textSize.Width) / 2f;
                    }
                    else if ( this.ToggleSwitch.OffSideAlignment == ToggleSwitchAlignment.Far )
                    {
                        textXPos = fullRectangle.X + fullRectangle.Width - textSize.Width;
                    }

                    var textRectangle = new RectangleF( textXPos, fullRectangle.Y + (fullRectangle.Height - textSize.Height) / 2f, textSize.Width, textSize.Height );
                    var textForeColor = this.ToggleSwitch.OffForeColor;
                    if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
                    {
                        textForeColor = textForeColor.ToGrayscale();
                    }

                    using Brush textBrush = new SolidBrush( textForeColor );
                    g.DrawString( this.ToggleSwitch.OffText, this.ToggleSwitch.OffFont, textBrush, textRectangle );
                }

                g.ResetClip();
            }
        }

        /// <summary> Renders the button. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="g">               The Graphics to process. </param>
        /// <param name="buttonRectangle"> The button rectangle. </param>
        public override void RenderButton( Graphics g, Rectangle buttonRectangle )
        {
            if ( g is null )
            {
                return;
            }

            g.SmoothingMode = SmoothingMode.HighQuality;
            g.PixelOffsetMode = PixelOffsetMode.HighQuality;
            g.InterpolationMode = InterpolationMode.HighQualityBilinear;

            // Draw button surface
            var buttonUpperSurfaceColor1 = this.ButtonNormalUpperSurfaceColor1;
            var buttonUpperSurfaceColor2 = this.ButtonNormalUpperSurfaceColor2;
            var buttonLowerSurfaceColor1 = this.ButtonNormalLowerSurfaceColor1;
            var buttonLowerSurfaceColor2 = this.ButtonNormalLowerSurfaceColor2;
            if ( this.ToggleSwitch.IsButtonPressed )
            {
                buttonUpperSurfaceColor1 = this.ButtonPressedUpperSurfaceColor1;
                buttonUpperSurfaceColor2 = this.ButtonPressedUpperSurfaceColor2;
                buttonLowerSurfaceColor1 = this.ButtonPressedLowerSurfaceColor1;
                buttonLowerSurfaceColor2 = this.ButtonPressedLowerSurfaceColor2;
            }
            else if ( this.ToggleSwitch.IsButtonHovered )
            {
                buttonUpperSurfaceColor1 = this.ButtonHoverUpperSurfaceColor1;
                buttonUpperSurfaceColor2 = this.ButtonHoverUpperSurfaceColor2;
                buttonLowerSurfaceColor1 = this.ButtonHoverLowerSurfaceColor1;
                buttonLowerSurfaceColor2 = this.ButtonHoverLowerSurfaceColor2;
            }

            if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
            {
                buttonUpperSurfaceColor1 = buttonUpperSurfaceColor1.ToGrayscale();
                buttonUpperSurfaceColor2 = buttonUpperSurfaceColor2.ToGrayscale();
                buttonLowerSurfaceColor1 = buttonLowerSurfaceColor1.ToGrayscale();
                buttonLowerSurfaceColor2 = buttonLowerSurfaceColor2.ToGrayscale();
            }

            buttonRectangle.Inflate( -1, -1 );
            int upperHeight = buttonRectangle.Height / 2;
            var upperGradientRect = new Rectangle( buttonRectangle.X, buttonRectangle.Y, buttonRectangle.Width, upperHeight );
            var lowerGradientRect = new Rectangle( buttonRectangle.X, buttonRectangle.Y + upperHeight, buttonRectangle.Width, buttonRectangle.Height - upperHeight );
            using var buttonPath = this.GetRoundedRectanglePath( buttonRectangle, this.CornerRadius );
            g.SetClip( buttonPath );
            g.IntersectClip( upperGradientRect );

            // Draw upper button surface gradient
            using ( Brush buttonUpperSurfaceBrush = new LinearGradientBrush( buttonRectangle, buttonUpperSurfaceColor1, buttonUpperSurfaceColor2, LinearGradientMode.Vertical ) )
            {
                g.FillPath( buttonUpperSurfaceBrush, buttonPath );
            }

            g.ResetClip();
            g.SetClip( buttonPath );
            g.IntersectClip( lowerGradientRect );

            // Draw lower button surface gradient
            using ( Brush buttonLowerSurfaceBrush = new LinearGradientBrush( buttonRectangle, buttonLowerSurfaceColor1, buttonLowerSurfaceColor2, LinearGradientMode.Vertical ) )
            {
                g.FillPath( buttonLowerSurfaceBrush, buttonPath );
            }

            g.ResetClip();
            g.SetClip( buttonPath );

            // Draw button border
            var buttonBorderColor1 = this.ButtonNormalBorderColor1;
            var buttonBorderColor2 = this.ButtonNormalBorderColor2;
            if ( this.ToggleSwitch.IsButtonPressed )
            {
                buttonBorderColor1 = this.ButtonPressedBorderColor1;
                buttonBorderColor2 = this.ButtonPressedBorderColor2;
            }
            else if ( this.ToggleSwitch.IsButtonHovered )
            {
                buttonBorderColor1 = this.ButtonHoverBorderColor1;
                buttonBorderColor2 = this.ButtonHoverBorderColor2;
            }

            if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
            {
                buttonBorderColor1 = buttonBorderColor1.ToGrayscale();
                buttonBorderColor2 = buttonBorderColor2.ToGrayscale();
            }

            using ( Brush buttonBorderBrush = new LinearGradientBrush( buttonRectangle, buttonBorderColor1, buttonBorderColor2, LinearGradientMode.Vertical ) )
            {
                using var buttonBorderPen = new Pen( buttonBorderBrush );
                g.DrawPath( buttonBorderPen, buttonPath );
            }

            g.ResetClip();

            // Draw button image
            var buttonImage = this.ToggleSwitch.ButtonImage ?? (this.ToggleSwitch.Checked ? this.ToggleSwitch.OnButtonImage : this.ToggleSwitch.OffButtonImage);
            if ( buttonImage is object )
            {
                g.SetClip( buttonPath );
                var alignment = this.ToggleSwitch.ButtonImage is object ? this.ToggleSwitch.ButtonAlignment : this.ToggleSwitch.Checked ? this.ToggleSwitch.OnButtonAlignment : this.ToggleSwitch.OffButtonAlignment;
                var imageSize = buttonImage.Size;
                Rectangle imageRectangle;
                int imageXPos = buttonRectangle.X;
                bool scaleImage = this.ToggleSwitch.ButtonImage is object ? this.ToggleSwitch.ButtonScaleImageToFit : this.ToggleSwitch.Checked ? this.ToggleSwitch.OnButtonScaleImageToFit : this.ToggleSwitch.OffButtonScaleImageToFit;
                if ( scaleImage )
                {
                    var canvasSize = buttonRectangle.Size;
                    var resizedImageSize = ImageHelper.RescaleImageToFit( imageSize, canvasSize );
                    if ( alignment == ToggleSwitchButtonAlignment.Center )
                    {
                        imageXPos = ( int ) Conversion.Fix( buttonRectangle.X + (buttonRectangle.Width - ( float ) resizedImageSize.Width) / 2f );
                    }
                    else if ( alignment == ToggleSwitchButtonAlignment.Right )
                    {
                        imageXPos = ( int ) Conversion.Fix( buttonRectangle.X + ( float ) buttonRectangle.Width - resizedImageSize.Width );
                    }

                    imageRectangle = new Rectangle( imageXPos, ( int ) Conversion.Fix( buttonRectangle.Y + (buttonRectangle.Height - ( float ) resizedImageSize.Height) / 2f ), resizedImageSize.Width, resizedImageSize.Height );
                    if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
                    {
                        g.DrawImage( buttonImage, imageRectangle, 0, 0, buttonImage.Width, buttonImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes() );
                    }
                    else
                    {
                        g.DrawImage( buttonImage, imageRectangle );
                    }
                }
                else
                {
                    if ( alignment == ToggleSwitchButtonAlignment.Center )
                    {
                        imageXPos = ( int ) Conversion.Fix( buttonRectangle.X + (buttonRectangle.Width - ( float ) imageSize.Width) / 2f );
                    }
                    else if ( alignment == ToggleSwitchButtonAlignment.Right )
                    {
                        imageXPos = ( int ) Conversion.Fix( buttonRectangle.X + ( float ) buttonRectangle.Width - imageSize.Width );
                    }

                    imageRectangle = new Rectangle( imageXPos, ( int ) Conversion.Fix( buttonRectangle.Y + (buttonRectangle.Height - ( float ) imageSize.Height) / 2f ), imageSize.Width, imageSize.Height );
                    if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
                    {
                        g.DrawImage( buttonImage, imageRectangle, 0, 0, buttonImage.Width, buttonImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes() );
                    }
                    else
                    {
                        g.DrawImageUnscaled( buttonImage, imageRectangle );
                    }
                }

                g.ResetClip();
            }
        }

        #endregion ' Render Method Implementations

        #region " Helper Method Implementations"

        /// <summary> Gets rounded rectangle path. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="rectangle"> The rectangle. </param>
        /// <param name="radius">    The radius. </param>
        /// <returns> The rounded rectangle path. </returns>
        public GraphicsPath GetRoundedRectanglePath( Rectangle rectangle, int radius )
        {
            var gp = new GraphicsPath();
            int diameter = 2 * radius;
            if ( diameter > this.ToggleSwitch.Height )
            {
                diameter = this.ToggleSwitch.Height;
            }

            if ( diameter > this.ToggleSwitch.Width )
            {
                diameter = this.ToggleSwitch.Width;
            }

            gp.AddArc( rectangle.X, rectangle.Y, diameter, diameter, 180f, 90f );
            gp.AddArc( rectangle.X + rectangle.Width - diameter, rectangle.Y, diameter, diameter, 270f, 90f );
            gp.AddArc( rectangle.X + rectangle.Width - diameter, rectangle.Y + rectangle.Height - diameter, diameter, diameter, 0f, 90f );
            gp.AddArc( rectangle.X, rectangle.Y + rectangle.Height - diameter, diameter, diameter, 90f, 90f );
            gp.CloseFigure();
            return gp;
        }

        /// <summary> Gets button width. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> The button width. </returns>
        public override int GetButtonWidth()
        {
            float buttonWidth = 1.61f * this.ToggleSwitch.Height;
            return ( int ) Conversion.Fix( buttonWidth );
        }

        /// <summary> Gets button rectangle. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> The button rectangle. </returns>
        public override Rectangle GetButtonRectangle()
        {
            int buttonWidth = this.GetButtonWidth();
            return this.GetButtonRectangle( buttonWidth );
        }

        /// <summary> Gets button rectangle. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="buttonWidth"> Width of the button. </param>
        /// <returns> The button rectangle. </returns>
        public override Rectangle GetButtonRectangle( int buttonWidth )
        {
            var buttonRect = new Rectangle( this.ToggleSwitch.ButtonValue, 0, buttonWidth, this.ToggleSwitch.Height );
            return buttonRect;
        }

        #endregion ' Helper Method Implementations
    }
}
