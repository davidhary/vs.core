using System;
using System.Drawing;
using System.Drawing.Drawing2D;

using isr.Core.ColorExtensions;

using Microsoft.VisualBasic;

namespace isr.Core.Controls
{

    /// <summary> A toggle switch brushed metal renderer. </summary>
    /// <remarks>
    /// (c) 2015 Johnny J.. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-12-04 </para>
    /// </remarks>
    public class ToggleSwitchBrushedMetalRenderer : ToggleSwitchRendererBase
    {

        #region " Constructor"

        /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public ToggleSwitchBrushedMetalRenderer()
        {
            this.BorderColor1 = Color.FromArgb( 255, 145, 146, 149 );
            this.BorderColor2 = Color.FromArgb( 255, 227, 229, 232 );
            this.BackColor1 = Color.FromArgb( 255, 125, 126, 128 );
            this.BackColor2 = Color.FromArgb( 255, 224, 226, 228 );
            this.UpperShadowColor1 = Color.FromArgb( 150, 0, 0, 0 );
            this.UpperShadowColor2 = Color.FromArgb( 5, 0, 0, 0 );
            this.ButtonNormalBorderColor = Color.FromArgb( 255, 144, 144, 145 );
            this.ButtonNormalSurfaceColor = Color.FromArgb( 255, 251, 251, 251 );
            this.ButtonHoverBorderColor = Color.FromArgb( 255, 166, 167, 168 );
            this.ButtonHoverSurfaceColor = Color.FromArgb( 255, 238, 238, 238 );
            this.ButtonPressedBorderColor = Color.FromArgb( 255, 123, 123, 123 );
            this.ButtonPressedSurfaceColor = Color.FromArgb( 255, 184, 184, 184 );
            this.UpperShadowHeight = 8;
        }

        #endregion ' Constructor

        #region " Public Properties"

        /// <summary> Gets or sets the border color 1. </summary>
        /// <value> The border color 1. </value>
        public Color BorderColor1 { get; set; }

        /// <summary> Gets or sets the border color 2. </summary>
        /// <value> The border color 2. </value>
        public Color BorderColor2 { get; set; }

        /// <summary> Gets or sets the back color 1. </summary>
        /// <value> The back color 1. </value>
        public Color BackColor1 { get; set; }

        /// <summary> Gets or sets the back color 2. </summary>
        /// <value> The back color 2. </value>
        public Color BackColor2 { get; set; }

        /// <summary> Gets or sets the upper shadow color 1. </summary>
        /// <value> The upper shadow color 1. </value>
        public Color UpperShadowColor1 { get; set; }

        /// <summary> Gets or sets the upper shadow color 2. </summary>
        /// <value> The upper shadow color 2. </value>
        public Color UpperShadowColor2 { get; set; }

        /// <summary> Gets or sets the color of the button normal border. </summary>
        /// <value> The color of the button normal border. </value>
        public Color ButtonNormalBorderColor { get; set; }

        /// <summary> Gets or sets the color of the button normal surface. </summary>
        /// <value> The color of the button normal surface. </value>
        public Color ButtonNormalSurfaceColor { get; set; }

        /// <summary> Gets or sets the color of the button hover border. </summary>
        /// <value> The color of the button hover border. </value>
        public Color ButtonHoverBorderColor { get; set; }

        /// <summary> Gets or sets the color of the button hover surface. </summary>
        /// <value> The color of the button hover surface. </value>
        public Color ButtonHoverSurfaceColor { get; set; }

        /// <summary> Gets or sets the color of the button pressed border. </summary>
        /// <value> The color of the button pressed border. </value>
        public Color ButtonPressedBorderColor { get; set; }

        /// <summary> Gets or sets the color of the button pressed surface. </summary>
        /// <value> The color of the button pressed surface. </value>
        public Color ButtonPressedSurfaceColor { get; set; }

        /// <summary> Gets or sets the height of the upper shadow. </summary>
        /// <value> The height of the upper shadow. </value>
        public int UpperShadowHeight { get; set; }

        #endregion ' Public Properties

        #region " Render Method Implementations"

        /// <summary> Renders the border. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="g">               The Graphics to process. </param>
        /// <param name="borderRectangle"> The border rectangle. </param>
        public override void RenderBorder( Graphics g, Rectangle borderRectangle )
        {
            if ( g is null )
            {
                return;
            }

            g.SmoothingMode = SmoothingMode.HighQuality;
            g.PixelOffsetMode = PixelOffsetMode.HighQuality;
            g.InterpolationMode = InterpolationMode.HighQualityBilinear;

            // Draw outer border
            using ( var outerControlPath = GetRectangleClipPath( borderRectangle ) )
            {
                g.SetClip( outerControlPath );
                var color1 = !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled ? this.BorderColor1.ToGrayscale() : this.BorderColor1;
                var color2 = !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled ? this.BorderColor2.ToGrayscale() : this.BorderColor2;
                using ( Brush borderBrush = new LinearGradientBrush( borderRectangle, color1, color2, LinearGradientMode.Vertical ) )
                {
                    g.FillPath( borderBrush, outerControlPath );
                }

                g.ResetClip();
            }

            // Draw inner background
            var innercontrolRectangle = new Rectangle( borderRectangle.X + 1, borderRectangle.Y + 1, borderRectangle.Width - 1, borderRectangle.Height - 2 );
            using var innerControlPath = GetRectangleClipPath( innercontrolRectangle );
            g.SetClip( innerControlPath );
            var backColor1_Renamed = !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled ? this.BackColor1.ToGrayscale() : this.BackColor1;
            var backColor2_Renamed = !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled ? this.BackColor2.ToGrayscale() : this.BackColor2;
            using ( Brush backgroundBrush = new LinearGradientBrush( borderRectangle, backColor1_Renamed, backColor2_Renamed, LinearGradientMode.Horizontal ) )
            {
                g.FillPath( backgroundBrush, innerControlPath );
            }

            // Draw inner top shadow
            var upperShadowRectangle = new Rectangle( innercontrolRectangle.X, innercontrolRectangle.Y, innercontrolRectangle.Width, this.UpperShadowHeight );
            g.IntersectClip( upperShadowRectangle );
            using ( Brush shadowBrush = new LinearGradientBrush( upperShadowRectangle, this.UpperShadowColor1, this.UpperShadowColor2, LinearGradientMode.Vertical ) )
            {
                g.FillRectangle( shadowBrush, upperShadowRectangle );
            }

            g.ResetClip();
        }

        /// <summary> Renders the left toggle field. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="g">                     The Graphics to process. </param>
        /// <param name="leftRectangle">         The left rectangle. </param>
        /// <param name="totalToggleFieldWidth"> Width of the total toggle field. </param>
        public override void RenderLeftToggleField( Graphics g, Rectangle leftRectangle, int totalToggleFieldWidth )
        {
            if ( g is null )
            {
                return;
            }

            g.SmoothingMode = SmoothingMode.HighQuality;
            g.PixelOffsetMode = PixelOffsetMode.HighQuality;
            g.InterpolationMode = InterpolationMode.HighQualityBilinear;
            var innercontrolRectangle = new Rectangle( 1, 1, this.ToggleSwitch.Width - 1, this.ToggleSwitch.Height - 2 );
            using var innerControlPath = GetRectangleClipPath( innercontrolRectangle );
            g.SetClip( innerControlPath );

            // Draw image or text
            if ( this.ToggleSwitch.OnSideImage is object || !string.IsNullOrEmpty( this.ToggleSwitch.OnText ) )
            {
                var fullRectangle = new RectangleF( leftRectangle.X + 2 - (totalToggleFieldWidth - leftRectangle.Width), 2f, totalToggleFieldWidth - 2, this.ToggleSwitch.Height - 4 );
                g.IntersectClip( fullRectangle );
                if ( this.ToggleSwitch.OnSideImage is object )
                {
                    var imageSize = this.ToggleSwitch.OnSideImage.Size;
                    Rectangle imageRectangle;
                    int imageXPos = ( int ) Conversion.Fix( fullRectangle.X );
                    if ( this.ToggleSwitch.OnSideScaleImageToFit )
                    {
                        var canvasSize = new Size( ( int ) fullRectangle.Width, ( int ) fullRectangle.Height );
                        var resizedImageSize = ImageHelper.RescaleImageToFit( imageSize, canvasSize );
                        if ( this.ToggleSwitch.OnSideAlignment == ToggleSwitchAlignment.Center )
                        {
                            imageXPos = ( int ) Conversion.Fix( fullRectangle.X + (fullRectangle.Width - resizedImageSize.Width) / 2f );
                        }
                        else if ( this.ToggleSwitch.OnSideAlignment == ToggleSwitchAlignment.Near )
                        {
                            imageXPos = ( int ) Conversion.Fix( fullRectangle.X + fullRectangle.Width - resizedImageSize.Width );
                        }

                        imageRectangle = new Rectangle( imageXPos, ( int ) Conversion.Fix( fullRectangle.Y + (fullRectangle.Height - resizedImageSize.Height) / 2f ), resizedImageSize.Width, resizedImageSize.Height );
                        if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
                        {
                            g.DrawImage( this.ToggleSwitch.OnSideImage, imageRectangle, 0, 0, this.ToggleSwitch.OnSideImage.Width, this.ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes() );
                        }
                        else
                        {
                            g.DrawImage( this.ToggleSwitch.OnSideImage, imageRectangle );
                        }
                    }
                    else
                    {
                        if ( this.ToggleSwitch.OnSideAlignment == ToggleSwitchAlignment.Center )
                        {
                            imageXPos = ( int ) Conversion.Fix( fullRectangle.X + (fullRectangle.Width - imageSize.Width) / 2f );
                        }
                        else if ( this.ToggleSwitch.OnSideAlignment == ToggleSwitchAlignment.Near )
                        {
                            imageXPos = ( int ) Conversion.Fix( fullRectangle.X + fullRectangle.Width - imageSize.Width );
                        }

                        imageRectangle = new Rectangle( imageXPos, ( int ) Conversion.Fix( fullRectangle.Y + (fullRectangle.Height - imageSize.Height) / 2f ), imageSize.Width, imageSize.Height );
                        if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
                        {
                            g.DrawImage( this.ToggleSwitch.OnSideImage, imageRectangle, 0, 0, this.ToggleSwitch.OnSideImage.Width, this.ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes() );
                        }
                        else
                        {
                            g.DrawImageUnscaled( this.ToggleSwitch.OnSideImage, imageRectangle );
                        }
                    }
                }
                else if ( !string.IsNullOrEmpty( this.ToggleSwitch.OnText ) )
                {
                    var textSize = g.MeasureString( this.ToggleSwitch.OnText, this.ToggleSwitch.OnFont );
                    float textXPos = fullRectangle.X;
                    if ( this.ToggleSwitch.OnSideAlignment == ToggleSwitchAlignment.Center )
                    {
                        textXPos = fullRectangle.X + (fullRectangle.Width - textSize.Width) / 2f;
                    }
                    else if ( this.ToggleSwitch.OnSideAlignment == ToggleSwitchAlignment.Near )
                    {
                        textXPos = fullRectangle.X + fullRectangle.Width - textSize.Width;
                    }

                    var textRectangle = new RectangleF( textXPos, fullRectangle.Y + (fullRectangle.Height - textSize.Height) / 2f, textSize.Width, textSize.Height );
                    var textForeColor = this.ToggleSwitch.OnForeColor;
                    if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
                    {
                        textForeColor = textForeColor.ToGrayscale();
                    }

                    using Brush textBrush = new SolidBrush( textForeColor );
                    g.DrawString( this.ToggleSwitch.OnText, this.ToggleSwitch.OnFont, textBrush, textRectangle );
                }
            }

            g.ResetClip();
        }

        /// <summary> Renders the right toggle field. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="g">                     The Graphics to process. </param>
        /// <param name="rightRectangle">        The right rectangle. </param>
        /// <param name="totalToggleFieldWidth"> Width of the total toggle field. </param>
        public override void RenderRightToggleField( Graphics g, Rectangle rightRectangle, int totalToggleFieldWidth )
        {
            if ( g is null )
            {
                return;
            }

            g.SmoothingMode = SmoothingMode.HighQuality;
            g.PixelOffsetMode = PixelOffsetMode.HighQuality;
            g.InterpolationMode = InterpolationMode.HighQualityBilinear;
            var innercontrolRectangle = new Rectangle( 1, 1, this.ToggleSwitch.Width - 1, this.ToggleSwitch.Height - 2 );
            using var innerControlPath = GetRectangleClipPath( innercontrolRectangle );
            g.SetClip( innerControlPath );

            // Draw image or text
            if ( this.ToggleSwitch.OffSideImage is object || !string.IsNullOrEmpty( this.ToggleSwitch.OffText ) )
            {
                var fullRectangle = new RectangleF( rightRectangle.X, 2f, totalToggleFieldWidth - 2, this.ToggleSwitch.Height - 4 );
                g.IntersectClip( fullRectangle );
                if ( this.ToggleSwitch.OffSideImage is object )
                {
                    var imageSize = this.ToggleSwitch.OffSideImage.Size;
                    Rectangle imageRectangle;
                    int imageXPos = ( int ) Conversion.Fix( fullRectangle.X );
                    if ( this.ToggleSwitch.OffSideScaleImageToFit )
                    {
                        var canvasSize = new Size( ( int ) fullRectangle.Width, ( int ) fullRectangle.Height );
                        var resizedImageSize = ImageHelper.RescaleImageToFit( imageSize, canvasSize );
                        if ( this.ToggleSwitch.OffSideAlignment == ToggleSwitchAlignment.Center )
                        {
                            imageXPos = ( int ) Conversion.Fix( fullRectangle.X + (fullRectangle.Width - resizedImageSize.Width) / 2f );
                        }
                        else if ( this.ToggleSwitch.OffSideAlignment == ToggleSwitchAlignment.Far )
                        {
                            imageXPos = ( int ) Conversion.Fix( fullRectangle.X + fullRectangle.Width - resizedImageSize.Width );
                        }

                        imageRectangle = new Rectangle( imageXPos, ( int ) Conversion.Fix( fullRectangle.Y + (fullRectangle.Height - resizedImageSize.Height) / 2f ), resizedImageSize.Width, resizedImageSize.Height );
                        if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
                        {
                            g.DrawImage( this.ToggleSwitch.OnSideImage, imageRectangle, 0, 0, this.ToggleSwitch.OnSideImage.Width, this.ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes() );
                        }
                        else
                        {
                            g.DrawImage( this.ToggleSwitch.OnSideImage, imageRectangle );
                        }
                    }
                    else
                    {
                        if ( this.ToggleSwitch.OffSideAlignment == ToggleSwitchAlignment.Center )
                        {
                            imageXPos = ( int ) Conversion.Fix( fullRectangle.X + (fullRectangle.Width - imageSize.Width) / 2f );
                        }
                        else if ( this.ToggleSwitch.OffSideAlignment == ToggleSwitchAlignment.Far )
                        {
                            imageXPos = ( int ) Conversion.Fix( fullRectangle.X + fullRectangle.Width - imageSize.Width );
                        }

                        imageRectangle = new Rectangle( imageXPos, ( int ) Conversion.Fix( fullRectangle.Y + (fullRectangle.Height - imageSize.Height) / 2f ), imageSize.Width, imageSize.Height );
                        if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
                        {
                            g.DrawImage( this.ToggleSwitch.OnSideImage, imageRectangle, 0, 0, this.ToggleSwitch.OnSideImage.Width, this.ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes() );
                        }
                        else
                        {
                            g.DrawImageUnscaled( this.ToggleSwitch.OffSideImage, imageRectangle );
                        }
                    }
                }
                else if ( !string.IsNullOrEmpty( this.ToggleSwitch.OffText ) )
                {
                    var textSize = g.MeasureString( this.ToggleSwitch.OffText, this.ToggleSwitch.OffFont );
                    float textXPos = fullRectangle.X;
                    if ( this.ToggleSwitch.OffSideAlignment == ToggleSwitchAlignment.Center )
                    {
                        textXPos = fullRectangle.X + (fullRectangle.Width - textSize.Width) / 2f;
                    }
                    else if ( this.ToggleSwitch.OffSideAlignment == ToggleSwitchAlignment.Far )
                    {
                        textXPos = fullRectangle.X + fullRectangle.Width - textSize.Width;
                    }

                    var textRectangle = new RectangleF( textXPos, fullRectangle.Y + (fullRectangle.Height - textSize.Height) / 2f, textSize.Width, textSize.Height );
                    var textForeColor = this.ToggleSwitch.OffForeColor;
                    if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
                    {
                        textForeColor = textForeColor.ToGrayscale();
                    }

                    using Brush textBrush = new SolidBrush( textForeColor );
                    g.DrawString( this.ToggleSwitch.OffText, this.ToggleSwitch.OffFont, textBrush, textRectangle );
                }
            }

            g.ResetClip();
        }

        /// <summary> Renders the button. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="g">               The Graphics to process. </param>
        /// <param name="buttonRectangle"> The button rectangle. </param>
        public override void RenderButton( Graphics g, Rectangle buttonRectangle )
        {
            if ( g is null )
            {
                return;
            }

            g.SmoothingMode = SmoothingMode.HighQuality;
            g.PixelOffsetMode = PixelOffsetMode.HighQuality;
            g.InterpolationMode = InterpolationMode.HighQualityBilinear;
            g.SetClip( buttonRectangle );

            // Draw button surface
            var buttonSurfaceColor = this.ButtonNormalSurfaceColor;
            if ( this.ToggleSwitch.IsButtonPressed )
            {
                buttonSurfaceColor = this.ButtonPressedSurfaceColor;
            }
            else if ( this.ToggleSwitch.IsButtonHovered )
            {
                buttonSurfaceColor = this.ButtonHoverSurfaceColor;
            }

            if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
            {
                buttonSurfaceColor = buttonSurfaceColor.ToGrayscale();
            }

            using ( Brush buttonSurfaceBrush = new SolidBrush( buttonSurfaceColor ) )
            {
                g.FillEllipse( buttonSurfaceBrush, buttonRectangle );
            }

            // Draw "metal" surface
            var centerPoint1 = new PointF( buttonRectangle.X + buttonRectangle.Width / 2.0f, buttonRectangle.Y + 1.2f * (buttonRectangle.Height / 2.0f) );
            using ( var firstMetalBrush = GetBrush( new Color[] { Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.FromArgb( 255, 110, 110, 110 ), Color.Transparent, Color.Transparent, Color.Transparent }, buttonRectangle, centerPoint1 ) )
            {
                g.FillEllipse( firstMetalBrush, buttonRectangle );
            }

            var centerPoint2 = new PointF( buttonRectangle.X + 0.8f * (buttonRectangle.Width / 2.0f), buttonRectangle.Y + buttonRectangle.Height / 2.0f );
            using ( var secondMetalBrush = GetBrush( new Color[] { Color.FromArgb( 255, 110, 110, 110 ), Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.FromArgb( 255, 110, 110, 110 ) }, buttonRectangle, centerPoint2 ) )
            {
                g.FillEllipse( secondMetalBrush, buttonRectangle );
            }

            var centerPoint3 = new PointF( buttonRectangle.X + 1.2f * (buttonRectangle.Width / 2.0f), buttonRectangle.Y + buttonRectangle.Height / 2.0f );
            using ( var thirdMetalBrush = GetBrush( new Color[] { Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.FromArgb( 255, 98, 98, 98 ), Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent }, buttonRectangle, centerPoint3 ) )
            {
                g.FillEllipse( thirdMetalBrush, buttonRectangle );
            }

            var centerPoint4 = new PointF( buttonRectangle.X + 0.9f * (buttonRectangle.Width / 2.0f), buttonRectangle.Y + 0.9f * (buttonRectangle.Height / 2.0f) );
            using ( var fourthMetalBrush = GetBrush( new Color[] { Color.Transparent, Color.FromArgb( 255, 188, 188, 188 ), Color.FromArgb( 255, 110, 110, 110 ), Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent }, buttonRectangle, centerPoint4 ) )
            {
                g.FillEllipse( fourthMetalBrush, buttonRectangle );
            }

            // Draw button border
            var buttonBorderColor = this.ButtonNormalBorderColor;
            if ( this.ToggleSwitch.IsButtonPressed )
            {
                buttonBorderColor = this.ButtonPressedBorderColor;
            }
            else if ( this.ToggleSwitch.IsButtonHovered )
            {
                buttonBorderColor = this.ButtonHoverBorderColor;
            }

            if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
            {
                buttonBorderColor = buttonBorderColor.ToGrayscale();
            }

            using ( var buttonBorderPen = new Pen( buttonBorderColor ) )
            {
                g.DrawEllipse( buttonBorderPen, buttonRectangle );
            }

            g.ResetClip();
        }

        #endregion ' Render Method Implementations

        #region " Helper Method Implementations"

        /// <summary> Gets rectangle clip path. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="rect"> The rectangle. </param>
        /// <returns> The rectangle clip path. </returns>
        public static GraphicsPath GetRectangleClipPath( Rectangle rect )
        {
            var borderPath = new GraphicsPath();
            borderPath.AddArc( rect.X, rect.Y, rect.Height, rect.Height, 90f, 180f );
            borderPath.AddArc( rect.Width - rect.Height, rect.Y, rect.Height, rect.Height, 270f, 180f );
            borderPath.CloseFigure();
            return borderPath;
        }

        /// <summary> Gets button clip path. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> The button clip path. </returns>
        public GraphicsPath GetButtonClipPath()
        {
            var buttonRectangle = this.GetButtonRectangle();
            var buttonPath = new GraphicsPath();
            buttonPath.AddArc( buttonRectangle.X, buttonRectangle.Y, buttonRectangle.Height, buttonRectangle.Height, 0f, 360f );
            return buttonPath;
        }

        /// <summary> Gets button width. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> The button width. </returns>
        public override int GetButtonWidth()
        {
            return this.ToggleSwitch.Height - 2;
        }

        /// <summary> Gets button rectangle. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> The button rectangle. </returns>
        public override Rectangle GetButtonRectangle()
        {
            int buttonWidth = this.GetButtonWidth();
            return this.GetButtonRectangle( buttonWidth );
        }

        /// <summary> Gets button rectangle. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="buttonWidth"> Width of the button. </param>
        /// <returns> The button rectangle. </returns>
        public override Rectangle GetButtonRectangle( int buttonWidth )
        {
            var buttonRect = new Rectangle( this.ToggleSwitch.ButtonValue, 1, buttonWidth, buttonWidth );
            return buttonRect;
        }

        /// <summary> Gets a brush. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="colors">      The colors. </param>
        /// <param name="r">           A RectangleF to process. </param>
        /// <param name="centerPoint"> The center point. </param>
        /// <returns> The brush. </returns>
        private static PathGradientBrush GetBrush( Color[] colors, RectangleF r, PointF centerPoint )
        {
            int i = colors.Length - 1;
            var points = new PointF[i + 1];
            float a = 0f;
            int n = 0;
            float cx = r.Width / 2.0f;
            float cy = r.Height / 2.0f;
            int w = ( int ) Conversion.Fix( Math.Floor( 180.0d * (i - 2.0d) / i / 2.0d ) );
            double wi = w * Math.PI / 180.0d;
            double faktor = 1.0d / Math.Sin( wi );
            float radx = ( float ) (cx * faktor);
            float rady = ( float ) (cy * faktor);
            while ( a <= Math.PI * 2d )
            {
                points[n] = new PointF( ( float ) (cx + Math.Cos( a ) * radx) + r.Left, ( float ) (cy + Math.Sin( a ) * rady) + r.Top );
                n += 1;
                a += ( float ) (Math.PI * 2d / i);
                // a += CSng(Math.PI * 2 \ i)
            }

            points[i] = points[0];
            var graphicsPath = new GraphicsPath();
            graphicsPath.AddLines( points );
            var fBrush = new PathGradientBrush( graphicsPath ) {
                CenterPoint = centerPoint,
                CenterColor = Color.Transparent,
                SurroundColors = new Color[] { Color.White }
            };
            try
            {
                fBrush.SurroundColors = colors;
            }
            catch ( Exception ex )
            {
                throw new InvalidOperationException( "Too may colors!", ex );
            }

            return fBrush;
        }

        #endregion ' Helper Method Implementations
    }
}
