using System.Drawing;

using isr.Core.ColorExtensions;

using Microsoft.VisualBasic;

namespace isr.Core.Controls
{

    /// <summary> A toggle switch metro renderer. </summary>
    /// <remarks>
    /// ((c) 2015 Johnny J.. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-12-04 </para>
    /// </remarks>
    public class ToggleSwitchMetroRenderer : ToggleSwitchRendererBase
    {

        #region " Constructor"

        /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public ToggleSwitchMetroRenderer()
        {
            this.BackColor = Color.White;
            this.LeftSideColor = Color.FromArgb( 255, 23, 153, 0 );
            this.LeftSideColorHovered = Color.FromArgb( 255, 36, 182, 9 );
            this.LeftSideColorPressed = Color.FromArgb( 255, 121, 245, 100 );
            this.RightSideColor = Color.FromArgb( 255, 166, 166, 166 );
            this.RightSideColorHovered = Color.FromArgb( 255, 181, 181, 181 );
            this.RightSideColorPressed = Color.FromArgb( 255, 189, 189, 189 );
            this.BorderColor = Color.FromArgb( 255, 166, 166, 166 );
            this.ButtonColor = Color.Black;
            this.ButtonColorHovered = Color.Black;
            this.ButtonColorPressed = Color.Black;
        }

        #endregion ' Constructor

        #region " Public Properties"

        /// <summary> Gets or sets the color of the back. </summary>
        /// <value> The color of the back. </value>
        public Color BackColor { get; set; }

        /// <summary> Gets or sets the color of the left side. </summary>
        /// <value> The color of the left side. </value>
        public Color LeftSideColor { get; set; }

        /// <summary> Gets or sets the left side color hovered. </summary>
        /// <value> The left side color hovered. </value>
        public Color LeftSideColorHovered { get; set; }

        /// <summary> Gets or sets the left side color pressed. </summary>
        /// <value> The left side color pressed. </value>
        public Color LeftSideColorPressed { get; set; }

        /// <summary> Gets or sets the color of the right side. </summary>
        /// <value> The color of the right side. </value>
        public Color RightSideColor { get; set; }

        /// <summary> Gets or sets the right side color hovered. </summary>
        /// <value> The right side color hovered. </value>
        public Color RightSideColorHovered { get; set; }

        /// <summary> Gets or sets the right side color pressed. </summary>
        /// <value> The right side color pressed. </value>
        public Color RightSideColorPressed { get; set; }

        /// <summary> Gets or sets the color of the border. </summary>
        /// <value> The color of the border. </value>
        public Color BorderColor { get; set; }

        /// <summary> Gets or sets the color of the button. </summary>
        /// <value> The color of the button. </value>
        public Color ButtonColor { get; set; }

        /// <summary> Gets or sets the button color hovered. </summary>
        /// <value> The button color hovered. </value>
        public Color ButtonColorHovered { get; set; }

        /// <summary> Gets or sets the button color pressed. </summary>
        /// <value> The button color pressed. </value>
        public Color ButtonColorPressed { get; set; }

        #endregion ' Public Properties

        #region " Render Method Implementations"

        /// <summary> Renders the border. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="g">               The Graphics to process. </param>
        /// <param name="borderRectangle"> The border rectangle. </param>
        public override void RenderBorder( Graphics g, Rectangle borderRectangle )
        {
            if ( g is null )
            {
                return;
            }

            var renderColor = !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled ? this.BorderColor.ToGrayscale() : this.BorderColor;
            g.SetClip( borderRectangle );
            using ( var borderPen = new Pen( renderColor ) )
            {
                g.DrawRectangle( borderPen, borderRectangle.X, borderRectangle.Y, borderRectangle.Width - 1, borderRectangle.Height - 1 );
            }

            g.ResetClip();
        }

        /// <summary> Renders the left toggle field. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="g">                     The Graphics to process. </param>
        /// <param name="leftRectangle">         The left rectangle. </param>
        /// <param name="totalToggleFieldWidth"> Width of the total toggle field. </param>
        public override void RenderLeftToggleField( Graphics g, Rectangle leftRectangle, int totalToggleFieldWidth )
        {
            if ( g is null )
            {
                return;
            }

            var adjustedLeftRect = new Rectangle( leftRectangle.X + 2, 2, leftRectangle.Width - 2, leftRectangle.Height - 4 );
            if ( adjustedLeftRect.Width > 0 )
            {
                var leftColor = this.LeftSideColor;
                if ( this.ToggleSwitch.IsLeftSidePressed )
                {
                    leftColor = this.LeftSideColorPressed;
                }
                else if ( this.ToggleSwitch.IsLeftSideHovered )
                {
                    leftColor = this.LeftSideColorHovered;
                }

                if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
                {
                    leftColor = leftColor.ToGrayscale();
                }

                g.SetClip( adjustedLeftRect );
                using ( Brush leftBrush = new SolidBrush( leftColor ) )
                {
                    g.FillRectangle( leftBrush, adjustedLeftRect );
                }

                if ( this.ToggleSwitch.OnSideImage is object || !string.IsNullOrEmpty( this.ToggleSwitch.OnText ) )
                {
                    var fullRectangle = new RectangleF( leftRectangle.X + 2 - (totalToggleFieldWidth - leftRectangle.Width), 2f, totalToggleFieldWidth - 2, this.ToggleSwitch.Height - 4 );
                    g.IntersectClip( fullRectangle );
                    if ( this.ToggleSwitch.OnSideImage is object )
                    {
                        var imageSize = this.ToggleSwitch.OnSideImage.Size;
                        Rectangle imageRectangle;
                        int imageXPos = ( int ) Conversion.Fix( fullRectangle.X );
                        if ( this.ToggleSwitch.OnSideScaleImageToFit )
                        {
                            var canvasSize = new Size( ( int ) fullRectangle.Width, ( int ) fullRectangle.Height );
                            var resizedImageSize = ImageHelper.RescaleImageToFit( imageSize, canvasSize );
                            if ( this.ToggleSwitch.OnSideAlignment == ToggleSwitchAlignment.Center )
                            {
                                imageXPos = ( int ) Conversion.Fix( fullRectangle.X + (fullRectangle.Width - resizedImageSize.Width) / 2f );
                            }
                            else if ( this.ToggleSwitch.OnSideAlignment == ToggleSwitchAlignment.Near )
                            {
                                imageXPos = ( int ) Conversion.Fix( fullRectangle.X + fullRectangle.Width - resizedImageSize.Width );
                            }

                            imageRectangle = new Rectangle( imageXPos, ( int ) Conversion.Fix( fullRectangle.Y + (fullRectangle.Height - resizedImageSize.Height) / 2f ), resizedImageSize.Width, resizedImageSize.Height );
                            if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
                            {
                                g.DrawImage( this.ToggleSwitch.OnSideImage, imageRectangle, 0, 0, this.ToggleSwitch.OnSideImage.Width, this.ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes() );
                            }
                            else
                            {
                                g.DrawImage( this.ToggleSwitch.OnSideImage, imageRectangle );
                            }
                        }
                        else
                        {
                            if ( this.ToggleSwitch.OnSideAlignment == ToggleSwitchAlignment.Center )
                            {
                                imageXPos = ( int ) Conversion.Fix( fullRectangle.X + (fullRectangle.Width - imageSize.Width) / 2f );
                            }
                            else if ( this.ToggleSwitch.OnSideAlignment == ToggleSwitchAlignment.Near )
                            {
                                imageXPos = ( int ) Conversion.Fix( fullRectangle.X + fullRectangle.Width - imageSize.Width );
                            }

                            imageRectangle = new Rectangle( imageXPos, ( int ) Conversion.Fix( fullRectangle.Y + (fullRectangle.Height - imageSize.Height) / 2f ), imageSize.Width, imageSize.Height );
                            if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
                            {
                                g.DrawImage( this.ToggleSwitch.OnSideImage, imageRectangle, 0, 0, this.ToggleSwitch.OnSideImage.Width, this.ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes() );
                            }
                            else
                            {
                                g.DrawImageUnscaled( this.ToggleSwitch.OnSideImage, imageRectangle );
                            }
                        }
                    }
                    else if ( !string.IsNullOrEmpty( this.ToggleSwitch.OnText ) )
                    {
                        var textSize = g.MeasureString( this.ToggleSwitch.OnText, this.ToggleSwitch.OnFont );
                        float textXPos = fullRectangle.X;
                        if ( this.ToggleSwitch.OnSideAlignment == ToggleSwitchAlignment.Center )
                        {
                            textXPos = fullRectangle.X + (fullRectangle.Width - textSize.Width) / 2f;
                        }
                        else if ( this.ToggleSwitch.OnSideAlignment == ToggleSwitchAlignment.Near )
                        {
                            textXPos = fullRectangle.X + fullRectangle.Width - textSize.Width;
                        }

                        var textRectangle = new RectangleF( textXPos, fullRectangle.Y + (fullRectangle.Height - textSize.Height) / 2f, textSize.Width, textSize.Height );
                        var textForeColor = this.ToggleSwitch.OnForeColor;
                        if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
                        {
                            textForeColor = textForeColor.ToGrayscale();
                        }

                        using Brush textBrush = new SolidBrush( textForeColor );
                        g.DrawString( this.ToggleSwitch.OnText, this.ToggleSwitch.OnFont, textBrush, textRectangle );
                    }
                }

                g.ResetClip();
            }
        }

        /// <summary> Renders the right toggle field. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="g">                     The Graphics to process. </param>
        /// <param name="rightRectangle">        The right rectangle. </param>
        /// <param name="totalToggleFieldWidth"> Width of the total toggle field. </param>
        public override void RenderRightToggleField( Graphics g, Rectangle rightRectangle, int totalToggleFieldWidth )
        {
            if ( g is null )
            {
                return;
            }

            var adjustedRightRect = new Rectangle( rightRectangle.X, 2, rightRectangle.Width - 2, rightRectangle.Height - 4 );
            if ( adjustedRightRect.Width > 0 )
            {
                var rightColor = this.RightSideColor;
                if ( this.ToggleSwitch.IsRightSidePressed )
                {
                    rightColor = this.RightSideColorPressed;
                }
                else if ( this.ToggleSwitch.IsRightSideHovered )
                {
                    rightColor = this.RightSideColorHovered;
                }

                if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
                {
                    rightColor = rightColor.ToGrayscale();
                }

                g.SetClip( adjustedRightRect );
                using ( Brush rightBrush = new SolidBrush( rightColor ) )
                {
                    g.FillRectangle( rightBrush, adjustedRightRect );
                }

                if ( this.ToggleSwitch.OffSideImage is object || !string.IsNullOrEmpty( this.ToggleSwitch.OffText ) )
                {
                    var fullRectangle = new RectangleF( rightRectangle.X, 2f, totalToggleFieldWidth - 2, this.ToggleSwitch.Height - 4 );
                    g.IntersectClip( fullRectangle );
                    if ( this.ToggleSwitch.OffSideImage is object )
                    {
                        var imageSize = this.ToggleSwitch.OffSideImage.Size;
                        Rectangle imageRectangle;
                        int imageXPos = ( int ) Conversion.Fix( fullRectangle.X );
                        if ( this.ToggleSwitch.OffSideScaleImageToFit )
                        {
                            var canvasSize = new Size( ( int ) fullRectangle.Width, ( int ) fullRectangle.Height );
                            var resizedImageSize = ImageHelper.RescaleImageToFit( imageSize, canvasSize );
                            if ( this.ToggleSwitch.OffSideAlignment == ToggleSwitchAlignment.Center )
                            {
                                imageXPos = ( int ) Conversion.Fix( fullRectangle.X + (fullRectangle.Width - resizedImageSize.Width) / 2f );
                            }
                            else if ( this.ToggleSwitch.OffSideAlignment == ToggleSwitchAlignment.Far )
                            {
                                imageXPos = ( int ) Conversion.Fix( fullRectangle.X + fullRectangle.Width - resizedImageSize.Width );
                            }

                            imageRectangle = new Rectangle( imageXPos, ( int ) Conversion.Fix( fullRectangle.Y + (fullRectangle.Height - resizedImageSize.Height) / 2f ), resizedImageSize.Width, resizedImageSize.Height );
                            if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
                            {
                                g.DrawImage( this.ToggleSwitch.OnSideImage, imageRectangle, 0, 0, this.ToggleSwitch.OnSideImage.Width, this.ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes() );
                            }
                            else
                            {
                                g.DrawImage( this.ToggleSwitch.OnSideImage, imageRectangle );
                            }
                        }
                        else
                        {
                            if ( this.ToggleSwitch.OffSideAlignment == ToggleSwitchAlignment.Center )
                            {
                                imageXPos = ( int ) Conversion.Fix( fullRectangle.X + (fullRectangle.Width - imageSize.Width) / 2f );
                            }
                            else if ( this.ToggleSwitch.OffSideAlignment == ToggleSwitchAlignment.Far )
                            {
                                imageXPos = ( int ) Conversion.Fix( fullRectangle.X + fullRectangle.Width - imageSize.Width );
                            }

                            imageRectangle = new Rectangle( imageXPos, ( int ) Conversion.Fix( fullRectangle.Y + (fullRectangle.Height - imageSize.Height) / 2f ), imageSize.Width, imageSize.Height );
                            if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
                            {
                                g.DrawImage( this.ToggleSwitch.OnSideImage, imageRectangle, 0, 0, this.ToggleSwitch.OnSideImage.Width, this.ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes() );
                            }
                            else
                            {
                                g.DrawImageUnscaled( this.ToggleSwitch.OffSideImage, imageRectangle );
                            }
                        }
                    }
                    else if ( !string.IsNullOrEmpty( this.ToggleSwitch.OffText ) )
                    {
                        var textSize = g.MeasureString( this.ToggleSwitch.OffText, this.ToggleSwitch.OffFont );
                        float textXPos = fullRectangle.X;
                        if ( this.ToggleSwitch.OffSideAlignment == ToggleSwitchAlignment.Center )
                        {
                            textXPos = fullRectangle.X + (fullRectangle.Width - textSize.Width) / 2f;
                        }
                        else if ( this.ToggleSwitch.OffSideAlignment == ToggleSwitchAlignment.Far )
                        {
                            textXPos = fullRectangle.X + fullRectangle.Width - textSize.Width;
                        }

                        var textRectangle = new RectangleF( textXPos, fullRectangle.Y + (fullRectangle.Height - textSize.Height) / 2f, textSize.Width, textSize.Height );
                        var textForeColor = this.ToggleSwitch.OffForeColor;
                        if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
                        {
                            textForeColor = textForeColor.ToGrayscale();
                        }

                        using Brush textBrush = new SolidBrush( textForeColor );
                        g.DrawString( this.ToggleSwitch.OffText, this.ToggleSwitch.OffFont, textBrush, textRectangle );
                    }
                }
            }
        }

        /// <summary> Renders the button. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="g">               The Graphics to process. </param>
        /// <param name="buttonRectangle"> The button rectangle. </param>
        public override void RenderButton( Graphics g, Rectangle buttonRectangle )
        {
            if ( g is null )
            {
                return;
            }

            var renderColor = this.ButtonColor;
            if ( this.ToggleSwitch.IsButtonPressed )
            {
                renderColor = this.ButtonColorPressed;
            }
            else if ( this.ToggleSwitch.IsButtonHovered )
            {
                renderColor = this.ButtonColorHovered;
            }

            if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
            {
                renderColor = renderColor.ToGrayscale();
            }

            g.SetClip( buttonRectangle );
            using ( Brush backBrush = new SolidBrush( renderColor ) )
            {
                g.FillRectangle( backBrush, buttonRectangle );
            }

            g.ResetClip();
        }

        #endregion ' Render Method Implementations

        #region " Helper Method Implementations"

        /// <summary> Gets button width. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> The button width. </returns>
        public override int GetButtonWidth()
        {
            return ( int ) (this.ToggleSwitch.Height / 3d * 2d);
        }

        /// <summary> Gets button rectangle. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> The button rectangle. </returns>
        public override Rectangle GetButtonRectangle()
        {
            int buttonWidth = this.GetButtonWidth();
            return this.GetButtonRectangle( buttonWidth );
        }

        /// <summary> Gets button rectangle. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="buttonWidth"> Width of the button. </param>
        /// <returns> The button rectangle. </returns>
        public override Rectangle GetButtonRectangle( int buttonWidth )
        {
            var buttonRect = new Rectangle( this.ToggleSwitch.ButtonValue, 0, buttonWidth, this.ToggleSwitch.Height );
            return buttonRect;
        }

        #endregion ' Helper Method Implementations
    }
}
