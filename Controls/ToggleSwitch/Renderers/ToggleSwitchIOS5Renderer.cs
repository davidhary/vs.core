using System;
using System.Drawing;
using System.Drawing.Drawing2D;

using isr.Core.ColorExtensions;

using Microsoft.VisualBasic;

namespace isr.Core.Controls
{

    /// <summary> A toggle switch ios 5 renderer. </summary>
    /// <remarks>
    /// (c) 2015 Johnny J.. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-12-04 </para>
    /// </remarks>
    public class ToggleSwitchIos5Renderer : ToggleSwitchRendererBase
    {

        #region " Constructor"

        /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public ToggleSwitchIos5Renderer()
        {
            this.BorderColor = Color.FromArgb( 255, 202, 202, 202 );
            this.LeftSideUpperColor1 = Color.FromArgb( 255, 48, 115, 189 );
            this.LeftSideUpperColor2 = Color.FromArgb( 255, 17, 123, 220 );
            this.LeftSideLowerColor1 = Color.FromArgb( 255, 65, 143, 218 );
            this.LeftSideLowerColor2 = Color.FromArgb( 255, 130, 190, 243 );
            this.LeftSideUpperBorderColor = Color.FromArgb( 150, 123, 157, 196 );
            this.LeftSideLowerBorderColor = Color.FromArgb( 150, 174, 208, 241 );
            this.RightSideUpperColor1 = Color.FromArgb( 255, 191, 191, 191 );
            this.RightSideUpperColor2 = Color.FromArgb( 255, 229, 229, 229 );
            this.RightSideLowerColor1 = Color.FromArgb( 255, 232, 232, 232 );
            this.RightSideLowerColor2 = Color.FromArgb( 255, 251, 251, 251 );
            this.RightSideUpperBorderColor = Color.FromArgb( 150, 175, 175, 175 );
            this.RightSideLowerBorderColor = Color.FromArgb( 150, 229, 230, 233 );
            this.ButtonShadowColor = Color.Transparent;
            this.ButtonNormalOuterBorderColor = Color.FromArgb( 255, 149, 172, 194 );
            this.ButtonNormalInnerBorderColor = Color.FromArgb( 255, 235, 235, 235 );
            this.ButtonNormalSurfaceColor1 = Color.FromArgb( 255, 216, 215, 216 );
            this.ButtonNormalSurfaceColor2 = Color.FromArgb( 255, 251, 250, 251 );
            this.ButtonHoverOuterBorderColor = Color.FromArgb( 255, 141, 163, 184 );
            this.ButtonHoverInnerBorderColor = Color.FromArgb( 255, 223, 223, 223 );
            this.ButtonHoverSurfaceColor1 = Color.FromArgb( 255, 205, 204, 205 );
            this.ButtonHoverSurfaceColor2 = Color.FromArgb( 255, 239, 238, 239 );
            this.ButtonPressedOuterBorderColor = Color.FromArgb( 255, 111, 128, 145 );
            this.ButtonPressedInnerBorderColor = Color.FromArgb( 255, 176, 176, 176 );
            this.ButtonPressedSurfaceColor1 = Color.FromArgb( 255, 162, 161, 162 );
            this.ButtonPressedSurfaceColor2 = Color.FromArgb( 255, 187, 187, 187 );
        }

        #endregion ' Constructor

        #region " Public Properties"

        /// <summary> Gets or sets the color of the border. </summary>
        /// <value> The color of the border. </value>
        public Color BorderColor { get; set; }

        /// <summary> Gets or sets the left side upper color 1. </summary>
        /// <value> The left side upper color 1. </value>
        public Color LeftSideUpperColor1 { get; set; }

        /// <summary> Gets or sets the left side upper color 2. </summary>
        /// <value> The left side upper color 2. </value>
        public Color LeftSideUpperColor2 { get; set; }

        /// <summary> Gets or sets the left side lower color 1. </summary>
        /// <value> The left side lower color 1. </value>
        public Color LeftSideLowerColor1 { get; set; }

        /// <summary> Gets or sets the left side lower color 2. </summary>
        /// <value> The left side lower color 2. </value>
        public Color LeftSideLowerColor2 { get; set; }

        /// <summary> Gets or sets the color of the left side upper border. </summary>
        /// <value> The color of the left side upper border. </value>
        public Color LeftSideUpperBorderColor { get; set; }

        /// <summary> Gets or sets the color of the left side lower border. </summary>
        /// <value> The color of the left side lower border. </value>
        public Color LeftSideLowerBorderColor { get; set; }

        /// <summary> Gets or sets the right side upper color 1. </summary>
        /// <value> The right side upper color 1. </value>
        public Color RightSideUpperColor1 { get; set; }

        /// <summary> Gets or sets the right side upper color 2. </summary>
        /// <value> The right side upper color 2. </value>
        public Color RightSideUpperColor2 { get; set; }

        /// <summary> Gets or sets the right side lower color 1. </summary>
        /// <value> The right side lower color 1. </value>
        public Color RightSideLowerColor1 { get; set; }

        /// <summary> Gets or sets the right side lower color 2. </summary>
        /// <value> The right side lower color 2. </value>
        public Color RightSideLowerColor2 { get; set; }

        /// <summary> Gets or sets the color of the right side upper border. </summary>
        /// <value> The color of the right side upper border. </value>
        public Color RightSideUpperBorderColor { get; set; }

        /// <summary> Gets or sets the color of the right side lower border. </summary>
        /// <value> The color of the right side lower border. </value>
        public Color RightSideLowerBorderColor { get; set; }

        /// <summary> Gets or sets the color of the button shadow. </summary>
        /// <value> The color of the button shadow. </value>
        public Color ButtonShadowColor { get; set; }

        /// <summary> Gets or sets the color of the button normal outer border. </summary>
        /// <value> The color of the button normal outer border. </value>
        public Color ButtonNormalOuterBorderColor { get; set; }

        /// <summary> Gets or sets the color of the button normal inner border. </summary>
        /// <value> The color of the button normal inner border. </value>
        public Color ButtonNormalInnerBorderColor { get; set; }

        /// <summary> Gets or sets the button normal surface color 1. </summary>
        /// <value> The button normal surface color 1. </value>
        public Color ButtonNormalSurfaceColor1 { get; set; }

        /// <summary> Gets or sets the button normal surface color 2. </summary>
        /// <value> The button normal surface color 2. </value>
        public Color ButtonNormalSurfaceColor2 { get; set; }

        /// <summary> Gets or sets the color of the button hover outer border. </summary>
        /// <value> The color of the button hover outer border. </value>
        public Color ButtonHoverOuterBorderColor { get; set; }

        /// <summary> Gets or sets the color of the button hover inner border. </summary>
        /// <value> The color of the button hover inner border. </value>
        public Color ButtonHoverInnerBorderColor { get; set; }

        /// <summary> Gets or sets the button hover surface color 1. </summary>
        /// <value> The button hover surface color 1. </value>
        public Color ButtonHoverSurfaceColor1 { get; set; }

        /// <summary> Gets or sets the button hover surface color 2. </summary>
        /// <value> The button hover surface color 2. </value>
        public Color ButtonHoverSurfaceColor2 { get; set; }

        /// <summary> Gets or sets the color of the button pressed outer border. </summary>
        /// <value> The color of the button pressed outer border. </value>
        public Color ButtonPressedOuterBorderColor { get; set; }

        /// <summary> Gets or sets the color of the button pressed inner border. </summary>
        /// <value> The color of the button pressed inner border. </value>
        public Color ButtonPressedInnerBorderColor { get; set; }

        /// <summary> Gets or sets the button pressed surface color 1. </summary>
        /// <value> The button pressed surface color 1. </value>
        public Color ButtonPressedSurfaceColor1 { get; set; }

        /// <summary> Gets or sets the button pressed surface color 2. </summary>
        /// <value> The button pressed surface color 2. </value>
        public Color ButtonPressedSurfaceColor2 { get; set; }

        #endregion ' Public Properties

        #region " Render Method Implementations"

        /// <summary> Renders the border. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="g">               The Graphics to process. </param>
        /// <param name="borderRectangle"> The border rectangle. </param>
        public override void RenderBorder( Graphics g, Rectangle borderRectangle )
        {
            // Draw this one AFTER the button is drawn in the RenderButton method
        }

        /// <summary> Renders the left toggle field. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="g">                     The Graphics to process. </param>
        /// <param name="leftRectangle">         The left rectangle. </param>
        /// <param name="totalToggleFieldWidth"> Width of the total toggle field. </param>
        public override void RenderLeftToggleField( Graphics g, Rectangle leftRectangle, int totalToggleFieldWidth )
        {
            if ( g is null )
            {
                return;
            }

            g.SmoothingMode = SmoothingMode.HighQuality;
            g.PixelOffsetMode = PixelOffsetMode.HighQuality;
            int buttonWidth = this.GetButtonWidth();

            // Draw upper gradient field
            int gradientRectWidth = leftRectangle.Width + buttonWidth / 2;
            int upperGradientRectHeight = ( int ) Conversion.Fix( 0.8d * (leftRectangle.Height - 2) );
            var controlRectangle = new Rectangle( 0, 0, this.ToggleSwitch.Width, this.ToggleSwitch.Height );
            var controlClipPath = GetControlClipPath( controlRectangle );
            var upperGradientRectangle = new Rectangle( leftRectangle.X, leftRectangle.Y + 1, gradientRectWidth, upperGradientRectHeight - 1 );
            g.SetClip( controlClipPath );
            g.IntersectClip( upperGradientRectangle );
            using ( var upperGradientPath = new GraphicsPath() )
            {
                upperGradientPath.AddArc( upperGradientRectangle.X, upperGradientRectangle.Y, this.ToggleSwitch.Height, this.ToggleSwitch.Height, 135f, 135f );
                upperGradientPath.AddLine( upperGradientRectangle.X, upperGradientRectangle.Y, upperGradientRectangle.X + upperGradientRectangle.Width, upperGradientRectangle.Y );
                upperGradientPath.AddLine( upperGradientRectangle.X + upperGradientRectangle.Width, upperGradientRectangle.Y, upperGradientRectangle.X + upperGradientRectangle.Width, upperGradientRectangle.Y + upperGradientRectangle.Height );
                upperGradientPath.AddLine( upperGradientRectangle.X, upperGradientRectangle.Y + upperGradientRectangle.Height, upperGradientRectangle.X + upperGradientRectangle.Width, upperGradientRectangle.Y + upperGradientRectangle.Height );
                var upperColor1 = !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled ? this.LeftSideUpperColor1.ToGrayscale() : this.LeftSideUpperColor1;
                var upperColor2 = !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled ? this.LeftSideUpperColor2.ToGrayscale() : this.LeftSideUpperColor2;
                using Brush upperGradientBrush = new LinearGradientBrush( upperGradientRectangle, upperColor1, upperColor2, LinearGradientMode.Vertical );
                g.FillPath( upperGradientBrush, upperGradientPath );
            }

            g.ResetClip();

            // Draw lower gradient field
            int lowerGradientRectHeight = ( int ) Conversion.Fix( Math.Ceiling( 0.5d * (leftRectangle.Height - 2) ) );
            var lowerGradientRectangle = new Rectangle( leftRectangle.X, leftRectangle.Y + leftRectangle.Height / 2, gradientRectWidth, lowerGradientRectHeight );
            g.SetClip( controlClipPath );
            g.IntersectClip( lowerGradientRectangle );
            using ( var lowerGradientPath = new GraphicsPath() )
            {
                lowerGradientPath.AddArc( 1, lowerGradientRectangle.Y, ( int ) Conversion.Fix( 0.75d * (this.ToggleSwitch.Height - 1) ), this.ToggleSwitch.Height - 1, 215f, 45f ); // Arc from side to top
                lowerGradientPath.AddLine( lowerGradientRectangle.X + buttonWidth / 2, lowerGradientRectangle.Y, lowerGradientRectangle.X + lowerGradientRectangle.Width, lowerGradientRectangle.Y );
                lowerGradientPath.AddLine( lowerGradientRectangle.X + lowerGradientRectangle.Width, lowerGradientRectangle.Y, lowerGradientRectangle.X + lowerGradientRectangle.Width, lowerGradientRectangle.Y + lowerGradientRectangle.Height );
                lowerGradientPath.AddLine( lowerGradientRectangle.X + buttonWidth / 4, lowerGradientRectangle.Y + lowerGradientRectangle.Height, lowerGradientRectangle.X + lowerGradientRectangle.Width, lowerGradientRectangle.Y + lowerGradientRectangle.Height );
                lowerGradientPath.AddArc( 1, 1, this.ToggleSwitch.Height - 1, this.ToggleSwitch.Height - 1, 90f, 70f ); // Arc from side to bottom
                var lowerColor1 = !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled ? this.LeftSideLowerColor1.ToGrayscale() : this.LeftSideLowerColor1;
                var lowerColor2 = !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled ? this.LeftSideLowerColor2.ToGrayscale() : this.LeftSideLowerColor2;
                using Brush lowerGradientBrush = new LinearGradientBrush( lowerGradientRectangle, lowerColor1, lowerColor2, LinearGradientMode.Vertical );
                g.FillPath( lowerGradientBrush, lowerGradientPath );
            }

            g.ResetClip();
            controlRectangle = new Rectangle( 0, 0, this.ToggleSwitch.Width, this.ToggleSwitch.Height );
            controlClipPath = GetControlClipPath( controlRectangle );
            g.SetClip( controlClipPath );

            // Draw upper inside border
            var upperBordercolor = this.LeftSideUpperBorderColor;
            if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
            {
                upperBordercolor = upperBordercolor.ToGrayscale();
            }

            using ( var upperBorderPen = new Pen( upperBordercolor ) )
            {
                g.DrawLine( upperBorderPen, leftRectangle.X, leftRectangle.Y + 1, leftRectangle.X + leftRectangle.Width + buttonWidth / 2, leftRectangle.Y + 1 );
            }

            // Draw lower inside border
            var lowerBordercolor = this.LeftSideLowerBorderColor;
            if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
            {
                lowerBordercolor = lowerBordercolor.ToGrayscale();
            }

            using ( var lowerBorderPen = new Pen( lowerBordercolor ) )
            {
                g.DrawLine( lowerBorderPen, leftRectangle.X, leftRectangle.Y + leftRectangle.Height - 1, leftRectangle.X + leftRectangle.Width + buttonWidth / 2, leftRectangle.Y + leftRectangle.Height - 1 );
            }

            // Draw image or text
            if ( this.ToggleSwitch.OnSideImage is object || !string.IsNullOrEmpty( this.ToggleSwitch.OnText ) )
            {
                var fullRectangle = new RectangleF( leftRectangle.X + 2 - (totalToggleFieldWidth - leftRectangle.Width), 2f, totalToggleFieldWidth - 2, this.ToggleSwitch.Height - 4 );
                g.IntersectClip( fullRectangle );
                if ( this.ToggleSwitch.OnSideImage is object )
                {
                    var imageSize = this.ToggleSwitch.OnSideImage.Size;
                    Rectangle imageRectangle;
                    int imageXPos = ( int ) Conversion.Fix( fullRectangle.X );
                    if ( this.ToggleSwitch.OnSideScaleImageToFit )
                    {
                        var canvasSize = new Size( ( int ) fullRectangle.Width, ( int ) fullRectangle.Height );
                        var resizedImageSize = ImageHelper.RescaleImageToFit( imageSize, canvasSize );
                        if ( this.ToggleSwitch.OnSideAlignment == ToggleSwitchAlignment.Center )
                        {
                            imageXPos = ( int ) Conversion.Fix( fullRectangle.X + (fullRectangle.Width - resizedImageSize.Width) / 2f );
                        }
                        else if ( this.ToggleSwitch.OnSideAlignment == ToggleSwitchAlignment.Near )
                        {
                            imageXPos = ( int ) Conversion.Fix( fullRectangle.X + fullRectangle.Width - resizedImageSize.Width );
                        }

                        imageRectangle = new Rectangle( imageXPos, ( int ) Conversion.Fix( fullRectangle.Y + (fullRectangle.Height - resizedImageSize.Height) / 2f ), resizedImageSize.Width, resizedImageSize.Height );
                        if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
                        {
                            g.DrawImage( this.ToggleSwitch.OnSideImage, imageRectangle, 0, 0, this.ToggleSwitch.OnSideImage.Width, this.ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes() );
                        }
                        else
                        {
                            g.DrawImage( this.ToggleSwitch.OnSideImage, imageRectangle );
                        }
                    }
                    else
                    {
                        if ( this.ToggleSwitch.OnSideAlignment == ToggleSwitchAlignment.Center )
                        {
                            imageXPos = ( int ) Conversion.Fix( fullRectangle.X + (fullRectangle.Width - imageSize.Width) / 2f );
                        }
                        else if ( this.ToggleSwitch.OnSideAlignment == ToggleSwitchAlignment.Near )
                        {
                            imageXPos = ( int ) Conversion.Fix( fullRectangle.X + fullRectangle.Width - imageSize.Width );
                        }

                        imageRectangle = new Rectangle( imageXPos, ( int ) Conversion.Fix( fullRectangle.Y + (fullRectangle.Height - imageSize.Height) / 2f ), imageSize.Width, imageSize.Height );
                        if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
                        {
                            g.DrawImage( this.ToggleSwitch.OnSideImage, imageRectangle, 0, 0, this.ToggleSwitch.OnSideImage.Width, this.ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes() );
                        }
                        else
                        {
                            g.DrawImageUnscaled( this.ToggleSwitch.OnSideImage, imageRectangle );
                        }
                    }
                }
                else if ( !string.IsNullOrEmpty( this.ToggleSwitch.OnText ) )
                {
                    var textSize = g.MeasureString( this.ToggleSwitch.OnText, this.ToggleSwitch.OnFont );
                    float textXPos = fullRectangle.X;
                    if ( this.ToggleSwitch.OnSideAlignment == ToggleSwitchAlignment.Center )
                    {
                        textXPos = fullRectangle.X + (fullRectangle.Width - textSize.Width) / 2f;
                    }
                    else if ( this.ToggleSwitch.OnSideAlignment == ToggleSwitchAlignment.Near )
                    {
                        textXPos = fullRectangle.X + fullRectangle.Width - textSize.Width;
                    }

                    var textRectangle = new RectangleF( textXPos, fullRectangle.Y + (fullRectangle.Height - textSize.Height) / 2f, textSize.Width, textSize.Height );
                    var textForeColor = this.ToggleSwitch.OnForeColor;
                    if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
                    {
                        textForeColor = textForeColor.ToGrayscale();
                    }

                    using Brush textBrush = new SolidBrush( textForeColor );
                    g.DrawString( this.ToggleSwitch.OnText, this.ToggleSwitch.OnFont, textBrush, textRectangle );
                }
            }

            g.ResetClip();
        }

        /// <summary> Renders the right toggle field. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="g">                     The Graphics to process. </param>
        /// <param name="rightRectangle">        The right rectangle. </param>
        /// <param name="totalToggleFieldWidth"> Width of the total toggle field. </param>
        public override void RenderRightToggleField( Graphics g, Rectangle rightRectangle, int totalToggleFieldWidth )
        {
            if ( g is null )
            {
                return;
            }

            g.SmoothingMode = SmoothingMode.HighQuality;
            g.PixelOffsetMode = PixelOffsetMode.HighQuality;
            var buttonRectangle = this.GetButtonRectangle();
            var controlRectangle = new Rectangle( 0, 0, this.ToggleSwitch.Width, this.ToggleSwitch.Height );
            var controlClipPath = GetControlClipPath( controlRectangle );

            // Draw upper gradient field
            int gradientRectWidth = rightRectangle.Width + buttonRectangle.Width / 2;
            int upperGradientRectHeight = ( int ) Conversion.Fix( 0.8d * (rightRectangle.Height - 2) );
            var upperGradientRectangle = new Rectangle( rightRectangle.X - buttonRectangle.Width / 2, rightRectangle.Y + 1, gradientRectWidth - 1, upperGradientRectHeight - 1 );
            g.SetClip( controlClipPath );
            g.IntersectClip( upperGradientRectangle );
            using ( var upperGradientPath = new GraphicsPath() )
            {
                upperGradientPath.AddLine( upperGradientRectangle.X, upperGradientRectangle.Y, upperGradientRectangle.X + upperGradientRectangle.Width, upperGradientRectangle.Y );
                upperGradientPath.AddArc( upperGradientRectangle.X + upperGradientRectangle.Width - this.ToggleSwitch.Height + 1, upperGradientRectangle.Y - 1, this.ToggleSwitch.Height, this.ToggleSwitch.Height, 270f, 115f );
                upperGradientPath.AddLine( upperGradientRectangle.X + upperGradientRectangle.Width, upperGradientRectangle.Y + upperGradientRectangle.Height, upperGradientRectangle.X, upperGradientRectangle.Y + upperGradientRectangle.Height );
                upperGradientPath.AddLine( upperGradientRectangle.X, upperGradientRectangle.Y + upperGradientRectangle.Height, upperGradientRectangle.X, upperGradientRectangle.Y );
                var upperColor1 = !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled ? this.RightSideUpperColor1.ToGrayscale() : this.RightSideUpperColor1;
                var upperColor2 = !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled ? this.RightSideUpperColor2.ToGrayscale() : this.RightSideUpperColor2;
                using Brush upperGradientBrush = new LinearGradientBrush( upperGradientRectangle, upperColor1, upperColor2, LinearGradientMode.Vertical );
                g.FillPath( upperGradientBrush, upperGradientPath );
            }

            g.ResetClip();

            // Draw lower gradient field
            int lowerGradientRectHeight = ( int ) Conversion.Fix( Math.Ceiling( 0.5d * (rightRectangle.Height - 2) ) );
            var lowerGradientRectangle = new Rectangle( rightRectangle.X - buttonRectangle.Width / 2, rightRectangle.Y + rightRectangle.Height / 2, gradientRectWidth - 1, lowerGradientRectHeight );
            g.SetClip( controlClipPath );
            g.IntersectClip( lowerGradientRectangle );
            using ( var lowerGradientPath = new GraphicsPath() )
            {
                lowerGradientPath.AddLine( lowerGradientRectangle.X, lowerGradientRectangle.Y, lowerGradientRectangle.X + lowerGradientRectangle.Width, lowerGradientRectangle.Y );
                lowerGradientPath.AddArc( lowerGradientRectangle.X + lowerGradientRectangle.Width - ( int ) Conversion.Fix( 0.75d * (this.ToggleSwitch.Height - 1) ), lowerGradientRectangle.Y, ( int ) Conversion.Fix( 0.75d * (this.ToggleSwitch.Height - 1) ), this.ToggleSwitch.Height - 1, 270f, 45f ); // Arc from top to side
                lowerGradientPath.AddArc( this.ToggleSwitch.Width - this.ToggleSwitch.Height, 0, this.ToggleSwitch.Height, this.ToggleSwitch.Height, 20f, 70f ); // Arc from side to bottom
                lowerGradientPath.AddLine( lowerGradientRectangle.X + lowerGradientRectangle.Width, lowerGradientRectangle.Y + lowerGradientRectangle.Height, lowerGradientRectangle.X, lowerGradientRectangle.Y + lowerGradientRectangle.Height );
                lowerGradientPath.AddLine( lowerGradientRectangle.X, lowerGradientRectangle.Y + lowerGradientRectangle.Height, lowerGradientRectangle.X, lowerGradientRectangle.Y );
                var lowerColor1 = !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled ? this.RightSideLowerColor1.ToGrayscale() : this.RightSideLowerColor1;
                var lowerColor2 = !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled ? this.RightSideLowerColor2.ToGrayscale() : this.RightSideLowerColor2;
                using Brush lowerGradientBrush = new LinearGradientBrush( lowerGradientRectangle, lowerColor1, lowerColor2, LinearGradientMode.Vertical );
                g.FillPath( lowerGradientBrush, lowerGradientPath );
            }

            g.ResetClip();
            controlRectangle = new Rectangle( 0, 0, this.ToggleSwitch.Width, this.ToggleSwitch.Height );
            controlClipPath = GetControlClipPath( controlRectangle );
            g.SetClip( controlClipPath );

            // Draw upper inside border
            var upperBordercolor = this.RightSideUpperBorderColor;
            if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
            {
                upperBordercolor = upperBordercolor.ToGrayscale();
            }

            using ( var upperBorderPen = new Pen( upperBordercolor ) )
            {
                g.DrawLine( upperBorderPen, rightRectangle.X - buttonRectangle.Width / 2, rightRectangle.Y + 1, rightRectangle.X + rightRectangle.Width, rightRectangle.Y + 1 );
            }

            // Draw lower inside border
            var lowerBordercolor = this.RightSideLowerBorderColor;
            if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
            {
                lowerBordercolor = lowerBordercolor.ToGrayscale();
            }

            using ( var lowerBorderPen = new Pen( lowerBordercolor ) )
            {
                g.DrawLine( lowerBorderPen, rightRectangle.X - buttonRectangle.Width / 2, rightRectangle.Y + rightRectangle.Height - 1, rightRectangle.X + rightRectangle.Width, rightRectangle.Y + rightRectangle.Height - 1 );
            }

            // Draw image or text
            if ( this.ToggleSwitch.OffSideImage is object || !string.IsNullOrEmpty( this.ToggleSwitch.OffText ) )
            {
                var fullRectangle = new RectangleF( rightRectangle.X, 2f, totalToggleFieldWidth - 2, this.ToggleSwitch.Height - 4 );
                g.IntersectClip( fullRectangle );
                if ( this.ToggleSwitch.OffSideImage is object )
                {
                    var imageSize = this.ToggleSwitch.OffSideImage.Size;
                    Rectangle imageRectangle;
                    int imageXPos = ( int ) Conversion.Fix( fullRectangle.X );
                    if ( this.ToggleSwitch.OffSideScaleImageToFit )
                    {
                        var canvasSize = new Size( ( int ) fullRectangle.Width, ( int ) fullRectangle.Height );
                        var resizedImageSize = ImageHelper.RescaleImageToFit( imageSize, canvasSize );
                        if ( this.ToggleSwitch.OffSideAlignment == ToggleSwitchAlignment.Center )
                        {
                            imageXPos = ( int ) Conversion.Fix( fullRectangle.X + (fullRectangle.Width - resizedImageSize.Width) / 2f );
                        }
                        else if ( this.ToggleSwitch.OffSideAlignment == ToggleSwitchAlignment.Far )
                        {
                            imageXPos = ( int ) Conversion.Fix( fullRectangle.X + fullRectangle.Width - resizedImageSize.Width );
                        }

                        imageRectangle = new Rectangle( imageXPos, ( int ) Conversion.Fix( fullRectangle.Y + (fullRectangle.Height - resizedImageSize.Height) / 2f ), resizedImageSize.Width, resizedImageSize.Height );
                        if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
                        {
                            g.DrawImage( this.ToggleSwitch.OnSideImage, imageRectangle, 0, 0, this.ToggleSwitch.OnSideImage.Width, this.ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes() );
                        }
                        else
                        {
                            g.DrawImage( this.ToggleSwitch.OnSideImage, imageRectangle );
                        }
                    }
                    else
                    {
                        if ( this.ToggleSwitch.OffSideAlignment == ToggleSwitchAlignment.Center )
                        {
                            imageXPos = ( int ) Conversion.Fix( fullRectangle.X + (fullRectangle.Width - imageSize.Width) / 2f );
                        }
                        else if ( this.ToggleSwitch.OffSideAlignment == ToggleSwitchAlignment.Far )
                        {
                            imageXPos = ( int ) Conversion.Fix( fullRectangle.X + fullRectangle.Width - imageSize.Width );
                        }

                        imageRectangle = new Rectangle( imageXPos, ( int ) Conversion.Fix( fullRectangle.Y + (fullRectangle.Height - imageSize.Height) / 2f ), imageSize.Width, imageSize.Height );
                        if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
                        {
                            g.DrawImage( this.ToggleSwitch.OnSideImage, imageRectangle, 0, 0, this.ToggleSwitch.OnSideImage.Width, this.ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes() );
                        }
                        else
                        {
                            g.DrawImageUnscaled( this.ToggleSwitch.OffSideImage, imageRectangle );
                        }
                    }
                }
                else if ( !string.IsNullOrEmpty( this.ToggleSwitch.OffText ) )
                {
                    var textSize = g.MeasureString( this.ToggleSwitch.OffText, this.ToggleSwitch.OffFont );
                    float textXPos = fullRectangle.X;
                    if ( this.ToggleSwitch.OffSideAlignment == ToggleSwitchAlignment.Center )
                    {
                        textXPos = fullRectangle.X + (fullRectangle.Width - textSize.Width) / 2f;
                    }
                    else if ( this.ToggleSwitch.OffSideAlignment == ToggleSwitchAlignment.Far )
                    {
                        textXPos = fullRectangle.X + fullRectangle.Width - textSize.Width;
                    }

                    var textRectangle = new RectangleF( textXPos, fullRectangle.Y + (fullRectangle.Height - textSize.Height) / 2f, textSize.Width, textSize.Height );
                    var textForeColor = this.ToggleSwitch.OffForeColor;
                    if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
                    {
                        textForeColor = textForeColor.ToGrayscale();
                    }

                    using Brush textBrush = new SolidBrush( textForeColor );
                    g.DrawString( this.ToggleSwitch.OffText, this.ToggleSwitch.OffFont, textBrush, textRectangle );
                }
            }

            g.ResetClip();
        }

        /// <summary> Renders the button. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="g">               The Graphics to process. </param>
        /// <param name="buttonRectangle"> The button rectangle. </param>
        public override void RenderButton( Graphics g, Rectangle buttonRectangle )
        {
            if ( g is null )
            {
                return;
            }

            if ( this.ToggleSwitch.IsButtonOnLeftSide )
            {
                buttonRectangle.X += 1;
            }
            else if ( this.ToggleSwitch.IsButtonOnRightSide )
            {
                buttonRectangle.X -= 1;
            }

            g.SmoothingMode = SmoothingMode.HighQuality;
            g.PixelOffsetMode = PixelOffsetMode.HighQuality;

            // Draw button shadow
            buttonRectangle.Inflate( 1, 1 );
            var shadowClipRectangle = new Rectangle( buttonRectangle.Location, buttonRectangle.Size );
            shadowClipRectangle.Inflate( 0, -1 );
            if ( this.ToggleSwitch.IsButtonOnLeftSide )
            {
                shadowClipRectangle.X += shadowClipRectangle.Width / 2;
                shadowClipRectangle.Width /= 2;
            }
            else if ( this.ToggleSwitch.IsButtonOnRightSide )
            {
                shadowClipRectangle.Width /= 2;
            }

            g.SetClip( shadowClipRectangle );

            // INSTANT VB NOTE: The variable buttonShadowColor was renamed since Visual Basic does not handle local variables named the same as class members well:
            var buttonShadowColor_Renamed = !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled ? this.ButtonShadowColor.ToGrayscale() : this.ButtonShadowColor;
            using ( var buttonShadowPen = new Pen( buttonShadowColor_Renamed ) )
            {
                g.DrawEllipse( buttonShadowPen, buttonRectangle );
            }

            g.ResetClip();
            buttonRectangle.Inflate( -1, -1 );

            // Draw outer button border
            var buttonOuterBorderColor = this.ButtonNormalOuterBorderColor;
            if ( this.ToggleSwitch.IsButtonPressed )
            {
                buttonOuterBorderColor = this.ButtonPressedOuterBorderColor;
            }
            else if ( this.ToggleSwitch.IsButtonHovered )
            {
                buttonOuterBorderColor = this.ButtonHoverOuterBorderColor;
            }

            if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
            {
                buttonOuterBorderColor = buttonOuterBorderColor.ToGrayscale();
            }

            using ( Brush outerBorderBrush = new SolidBrush( buttonOuterBorderColor ) )
            {
                g.FillEllipse( outerBorderBrush, buttonRectangle );
            }

            // Draw inner button border
            buttonRectangle.Inflate( -1, -1 );
            var buttonInnerBorderColor = this.ButtonNormalInnerBorderColor;
            if ( this.ToggleSwitch.IsButtonPressed )
            {
                buttonInnerBorderColor = this.ButtonPressedInnerBorderColor;
            }
            else if ( this.ToggleSwitch.IsButtonHovered )
            {
                buttonInnerBorderColor = this.ButtonHoverInnerBorderColor;
            }

            if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
            {
                buttonInnerBorderColor = buttonInnerBorderColor.ToGrayscale();
            }

            using ( Brush innerBorderBrush = new SolidBrush( buttonInnerBorderColor ) )
            {
                g.FillEllipse( innerBorderBrush, buttonRectangle );
            }

            // Draw button surface
            buttonRectangle.Inflate( -1, -1 );
            var buttonUpperSurfaceColor = this.ButtonNormalSurfaceColor1;
            if ( this.ToggleSwitch.IsButtonPressed )
            {
                buttonUpperSurfaceColor = this.ButtonPressedSurfaceColor1;
            }
            else if ( this.ToggleSwitch.IsButtonHovered )
            {
                buttonUpperSurfaceColor = this.ButtonHoverSurfaceColor1;
            }

            if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
            {
                buttonUpperSurfaceColor = buttonUpperSurfaceColor.ToGrayscale();
            }

            var buttonLowerSurfaceColor = this.ButtonNormalSurfaceColor2;
            if ( this.ToggleSwitch.IsButtonPressed )
            {
                buttonLowerSurfaceColor = this.ButtonPressedSurfaceColor2;
            }
            else if ( this.ToggleSwitch.IsButtonHovered )
            {
                buttonLowerSurfaceColor = this.ButtonHoverSurfaceColor2;
            }

            if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
            {
                buttonLowerSurfaceColor = buttonLowerSurfaceColor.ToGrayscale();
            }

            using ( Brush buttonSurfaceBrush = new LinearGradientBrush( buttonRectangle, buttonUpperSurfaceColor, buttonLowerSurfaceColor, LinearGradientMode.Vertical ) )
            {
                g.FillEllipse( buttonSurfaceBrush, buttonRectangle );
            }

            g.CompositingMode = CompositingMode.SourceOver;
            g.CompositingQuality = CompositingQuality.HighQuality;

            // Draw outer control border
            var controlRectangle = new Rectangle( 0, 0, this.ToggleSwitch.Width, this.ToggleSwitch.Height );
            using ( var borderPath = GetControlClipPath( controlRectangle ) )
            {
                var controlBorderColor = !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled ? this.BorderColor.ToGrayscale() : this.BorderColor;
                using var borderPen = new Pen( controlBorderColor );
                g.DrawPath( borderPen, borderPath );
            }

            g.ResetClip();

            // Draw button image
            var buttonImage = this.ToggleSwitch.ButtonImage ?? (this.ToggleSwitch.Checked ? this.ToggleSwitch.OnButtonImage : this.ToggleSwitch.OffButtonImage);
            if ( buttonImage is object )
            {
                g.SetClip( this.GetButtonClipPath() );
                var alignment = this.ToggleSwitch.ButtonImage is object ? this.ToggleSwitch.ButtonAlignment : this.ToggleSwitch.Checked ? this.ToggleSwitch.OnButtonAlignment : this.ToggleSwitch.OffButtonAlignment;
                var imageSize = buttonImage.Size;
                Rectangle imageRectangle;
                int imageXPos = buttonRectangle.X;
                bool scaleImage = this.ToggleSwitch.ButtonImage is object ? this.ToggleSwitch.ButtonScaleImageToFit : this.ToggleSwitch.Checked ? this.ToggleSwitch.OnButtonScaleImageToFit : this.ToggleSwitch.OffButtonScaleImageToFit;
                if ( scaleImage )
                {
                    var canvasSize = buttonRectangle.Size;
                    var resizedImageSize = ImageHelper.RescaleImageToFit( imageSize, canvasSize );
                    if ( alignment == ToggleSwitchButtonAlignment.Center )
                    {
                        imageXPos = ( int ) Conversion.Fix( buttonRectangle.X + (buttonRectangle.Width - ( float ) resizedImageSize.Width) / 2f );
                    }
                    else if ( alignment == ToggleSwitchButtonAlignment.Right )
                    {
                        imageXPos = ( int ) Conversion.Fix( buttonRectangle.X + ( float ) buttonRectangle.Width - resizedImageSize.Width );
                    }

                    imageRectangle = new Rectangle( imageXPos, ( int ) Conversion.Fix( buttonRectangle.Y + (buttonRectangle.Height - ( float ) resizedImageSize.Height) / 2f ), resizedImageSize.Width, resizedImageSize.Height );
                    if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
                    {
                        g.DrawImage( buttonImage, imageRectangle, 0, 0, buttonImage.Width, buttonImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes() );
                    }
                    else
                    {
                        g.DrawImage( buttonImage, imageRectangle );
                    }
                }
                else
                {
                    if ( alignment == ToggleSwitchButtonAlignment.Center )
                    {
                        imageXPos = ( int ) Conversion.Fix( buttonRectangle.X + (buttonRectangle.Width - ( float ) imageSize.Width) / 2f );
                    }
                    else if ( alignment == ToggleSwitchButtonAlignment.Right )
                    {
                        imageXPos = ( int ) Conversion.Fix( buttonRectangle.X + ( float ) buttonRectangle.Width - imageSize.Width );
                    }

                    imageRectangle = new Rectangle( imageXPos, ( int ) Conversion.Fix( buttonRectangle.Y + (buttonRectangle.Height - ( float ) imageSize.Height) / 2f ), imageSize.Width, imageSize.Height );
                    if ( !this.ToggleSwitch.Enabled && this.ToggleSwitch.GrayWhenDisabled )
                    {
                        g.DrawImage( buttonImage, imageRectangle, 0, 0, buttonImage.Width, buttonImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes() );
                    }
                    else
                    {
                        g.DrawImageUnscaled( buttonImage, imageRectangle );
                    }
                }

                g.ResetClip();
            }
        }

        #endregion ' Render Method Implementations

        #region " Helper Method Implementations"

        /// <summary> Gets control clip path. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="controlRectangle"> The control rectangle. </param>
        /// <returns> The control clip path. </returns>
        public static GraphicsPath GetControlClipPath( Rectangle controlRectangle )
        {
            var borderPath = new GraphicsPath();
            borderPath.AddArc( controlRectangle.X, controlRectangle.Y, controlRectangle.Height, controlRectangle.Height, 90f, 180f );
            borderPath.AddArc( controlRectangle.Width - controlRectangle.Height, controlRectangle.Y, controlRectangle.Height, controlRectangle.Height, 270f, 180f );
            borderPath.CloseFigure();
            return borderPath;
        }

        /// <summary> Gets button clip path. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> The button clip path. </returns>
        public GraphicsPath GetButtonClipPath()
        {
            var buttonRectangle = this.GetButtonRectangle();
            var buttonPath = new GraphicsPath();
            buttonPath.AddArc( buttonRectangle.X, buttonRectangle.Y, buttonRectangle.Height, buttonRectangle.Height, 0f, 360f );
            return buttonPath;
        }

        /// <summary> Gets button width. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> The button width. </returns>
        public override int GetButtonWidth()
        {
            return this.ToggleSwitch.Height - 2;
        }

        /// <summary> Gets button rectangle. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> The button rectangle. </returns>
        public override Rectangle GetButtonRectangle()
        {
            int buttonWidth = this.GetButtonWidth();
            return this.GetButtonRectangle( buttonWidth );
        }

        /// <summary> Gets button rectangle. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="buttonWidth"> Width of the button. </param>
        /// <returns> The button rectangle. </returns>
        public override Rectangle GetButtonRectangle( int buttonWidth )
        {
            var buttonRect = new Rectangle( this.ToggleSwitch.ButtonValue, 1, buttonWidth, buttonWidth );
            return buttonRect;
        }

        #endregion ' Helper Method Implementations
    }
}
