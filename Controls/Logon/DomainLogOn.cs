using System;
using System.Collections;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices.ActiveDirectory;

using isr.Core.Controls.ExceptionExtensions;

namespace isr.Core.Controls
{

    /// <summary> Domain log on. </summary>
    /// <remarks>
    /// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-10-10, 2.1.5396. </para>
    /// </remarks>
    public class DomainLogOn : LogOnBase
    {

        #region " SHARED "

        /// <summary> Enumerate user roles. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="user"> The user. </param>
        /// <returns> <c>true</c> if in role; otherwise <c>false</c>. </returns>
        public static ArrayList EnumerateUserRoles( UserPrincipal user )
        {
            var list = new ArrayList();
            if ( user is object )
            {
                foreach ( Principal group in user.GetAuthorizationGroups() )
                {
                    _ = list.Add( group.Name );
                }
            }

            return list;
        }

        /// <summary> Enumerate user roles. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="user">             The user. </param>
        /// <param name="allowedUserRoles"> Specifies the list of valid roles. This could be compared to
        /// the name of enumeration flags. </param>
        /// <returns> <c>true</c> if in role; otherwise <c>false</c>. </returns>
        public static ArrayList EnumerateUserRoles( UserPrincipal user, ArrayList allowedUserRoles )
        {
            if ( user is null )
            {
                throw new ArgumentNullException( nameof( user ) );
            }

            if ( allowedUserRoles is null )
            {
                throw new ArgumentNullException( nameof( allowedUserRoles ) );
            }

            var roles = new ArrayList();
            foreach ( Principal group in user.GetAuthorizationGroups() )
            {
                if ( allowedUserRoles.Contains( group.Name ) )
                {
                    _ = roles.Add( group.Name );
                }
            }

            return roles;
        }

        /// <summary> Enumerate user roles. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="userRoles">        The user roles. </param>
        /// <param name="allowedUserRoles"> Specifies the list of valid roles. This could be compared to
        /// the name of enumeration flags. </param>
        /// <returns> <c>true</c> if in role; otherwise <c>false</c>. </returns>
        public static ArrayList EnumerateUserRoles( ArrayList userRoles, ArrayList allowedUserRoles )
        {
            if ( userRoles is null )
            {
                throw new ArgumentNullException( nameof( userRoles ) );
            }

            if ( allowedUserRoles is null )
            {
                throw new ArgumentNullException( nameof( allowedUserRoles ) );
            }

            var roles = new ArrayList();
            foreach ( string role in userRoles )
            {
                if ( allowedUserRoles.Contains( role ) )
                {
                    _ = roles.Add( role );
                }
            }

            return roles;
        }


        /// <summary> Enumerate domains. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> An ArrayList. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        public static ArrayList EnumerateDomains()
        {
            var domainNames = new ArrayList();
            try
            {
                var currentForest = Forest.GetCurrentForest();
                var myDomains = currentForest.Domains;
                foreach ( Domain domain in myDomains )
                {
                    _ = domainNames.Add( domain.Name );
                }
            }
            catch ( ActiveDirectoryOperationException )
            {
                // this exception will occur if the Current security context is not associated with an Active Directory domain or forest.
                // so an empty list is returned.
            }

            return domainNames;
        }

        /// <summary> Attempts to find user on the machine. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="userName"> Specifies a user name. </param>
        /// <param name="e">        Cancel details event information. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static UserPrincipal FindUser( string userName, ActionEventArgs e )
        {
            if ( e is null )
            {
                throw new ArgumentNullException( nameof( e ) );
            }

            UserPrincipal user = null;
            if ( string.IsNullOrWhiteSpace( userName ) )
            {
                e.RegisterFailure( "User name is empty" );
            }
            else
            {
                PrincipalContext ctx = null;
                try
                {
                    ctx = new PrincipalContext( ContextType.Machine );
                }
                catch ( Exception ex )
                {
                    ctx?.Dispose();
                    ctx = null;
                    e.RegisterError( $"Exception occurred;. {ex.ToFullBlownString()}" );
                }

                if ( ctx is object )
                {
                    user = UserPrincipal.FindByIdentity( ctx, userName );
                    if ( user is null )
                    {
                        e.RegisterFailure( "User '{0}' not found @'{1}'", userName, My.MyProject.Computer.Name );
                    }
                }
            }

            return user;
        }

        /// <summary> Attempts to find user with the allowed roles on the machine. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="userName">         Specifies a user name. </param>
        /// <param name="allowedUserRoles"> The allowed roles. </param>
        /// <param name="e">                Cancel details event information. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static UserPrincipal FindUser( string userName, ArrayList allowedUserRoles, ActionEventArgs e )
        {
            if ( e is null )
            {
                throw new ArgumentNullException( nameof( e ) );
            }

            UserPrincipal user = null;
            if ( string.IsNullOrWhiteSpace( userName ) )
            {
                e.RegisterFailure( "User name is empty" );
            }
            else
            {
                try
                {
                    user = FindUser( userName, e );
                    if ( user is null )
                    {
                    }
                    else if ( EnumerateUserRoles( user, allowedUserRoles ).Count == 0 )
                    {
                        e.RegisterFailure( "User '{0}' found @'{1}' is not a member in any of the allowed groups. ", userName, My.MyProject.Computer.Name );
                        user.Dispose();
                        user = null;
                    }
                }
                catch ( Exception ex )
                {
                    e.RegisterError( $"Exception occurred;. {ex.ToFullBlownString()}" );
                }
            }

            return user;
        }

        /// <summary> Attempts to find user on the machine. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="userName"> Specifies a user name. </param>
        /// <param name="e">        Action event information. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static bool TryFindUser( string userName, ActionEventArgs e )
        {
            if ( e is null )
            {
                throw new ArgumentNullException( nameof( e ) );
            }

            bool result = false;
            if ( string.IsNullOrWhiteSpace( userName ) )
            {
                e.RegisterFailure( "User name is empty" );
            }
            else
            {
                try
                {
                    using var user = FindUser( userName, e );
                    if ( user is null )
                    {
                        e.RegisterFailure( "User '{0}' not found @'{1}'", userName, My.MyProject.Computer.Name );
                    }
                    else
                    {
                        result = true;
                    }
                }
                catch ( Exception ex )
                {
                    e.RegisterError( $"Exception occurred;. {ex.ToFullBlownString()}" );
                }
            }

            return result;
        }

        /// <summary> Attempts to find user on the domain. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="userName">   Specifies a user name. </param>
        /// <param name="domainName"> Name of the domain. </param>
        /// <param name="e">          Action event information. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static UserPrincipal FindUser( string userName, string domainName, ActionEventArgs e )
        {
            if ( e is null )
            {
                throw new ArgumentNullException( nameof( e ) );
            }

            UserPrincipal user = null;
            if ( string.IsNullOrWhiteSpace( userName ) )
            {
                e.RegisterFailure( "User name is empty" );
            }
            else if ( string.IsNullOrWhiteSpace( domainName ) )
            {
                e.RegisterFailure( "Domain name is empty" );
            }
            else
            {
                PrincipalContext ctx = null;
                try
                {
                    ctx = new PrincipalContext( ContextType.Domain, domainName );
                    user = UserPrincipal.FindByIdentity( ctx, userName );
                    if ( user is null )
                    {
                        e.RegisterFailure( "User '{0}' not identified @'{1}'", userName, domainName );
                    }
                }
                catch ( Exception ex )
                {
                    e.RegisterError( "Exception occurred;. {0}", ex.ToFullBlownString() );
                }
                finally
                {
                    if ( user is null )
                    {
                        ctx?.Dispose();
                    }
                }
            }

            return user;
        }

        /// <summary> Attempts to find user on the domain with an allowed role. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="userName">         Specifies a user name. </param>
        /// <param name="domainName">       Name of the domain. </param>
        /// <param name="allowedUserRoles"> The allowed roles. </param>
        /// <param name="e">                Action event information. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static UserPrincipal FindUser( string userName, string domainName, ArrayList allowedUserRoles, ActionEventArgs e )
        {
            if ( e is null )
            {
                throw new ArgumentNullException( nameof( e ) );
            }

            UserPrincipal user = null;
            if ( string.IsNullOrWhiteSpace( userName ) )
            {
                e.RegisterFailure( "User name is empty" );
            }
            else if ( string.IsNullOrWhiteSpace( domainName ) )
            {
                e.RegisterFailure( "Domain name is empty" );
            }
            else
            {
                PrincipalContext ctx = null;
                try
                {
                    user = FindUser( userName, domainName, e );
                    if ( user is null )
                    {
                    }
                    else if ( EnumerateUserRoles( user, allowedUserRoles ).Count == 0 )
                    {
                        e.RegisterFailure( "User '{0}' found @'{1}' is not a member in any of the allowed groups.", userName, domainName );
                        user.Dispose();
                        user = null;
                    }

                    ctx = new PrincipalContext( ContextType.Domain, domainName );
                    user = UserPrincipal.FindByIdentity( ctx, userName );
                    if ( user is null )
                    {
                        e.RegisterFailure( "User '{0}' not identified @'{1}'", userName, domainName );
                    }
                }
                catch ( Exception ex )
                {
                    e.RegisterError( $"Exception occurred;. {ex.ToFullBlownString()}" );
                }
                finally
                {
                    if ( user is null )
                    {
                        ctx?.Dispose();
                    }
                }
            }

            return user;
        }

        /// <summary> Attempts to find user on the machine. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="userName">   Specifies a user name. </param>
        /// <param name="domainName"> [in,out] Name of the domain. </param>
        /// <param name="e">          Cancel details event information. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static UserPrincipal FindNextUser( string userName, ref string domainName, ActionEventArgs e )
        {
            if ( e is null )
            {
                throw new ArgumentNullException( nameof( e ) );
            }

            UserPrincipal user = null;
            if ( string.IsNullOrWhiteSpace( userName ) )
            {
                e.RegisterFailure( "User name is empty" );
            }
            else
            {
                try
                {
                    var domainNames = EnumerateDomains();
                    if ( domainNames is null || domainNames.Count == 0 )
                    {
                        e.RegisterFailure( "No domains found; Most likely, the current security context is not associated with an Active Directory domain or forest." );
                    }
                    else
                    {
                        bool foundDomain = string.IsNullOrEmpty( domainName );
                        foreach ( string name in domainNames )
                        {
                            if ( foundDomain )
                            {
                                // skip until the domain is found or empty.
                                user = FindUser( userName, name, e );
                                if ( user is object )
                                {
                                    domainName = name;
                                    break;
                                }
                            }
                            else
                            {
                                foundDomain = string.Equals( name, domainName, StringComparison.OrdinalIgnoreCase );
                            }
                        }
                    }
                }
                catch ( Exception ex )
                {
                    e.RegisterError( $"Exception occurred;. {ex.ToFullBlownString()}" );
                }
            }

            return user;
        }

        /// <summary> Attempts to find user on the machine. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="userName">         Specifies a user name. </param>
        /// <param name="allowedUserRoles"> The allowed roles. </param>
        /// <param name="domainName">       [in,out] Name of the domain. </param>
        /// <param name="e">                Cancel details event information. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static UserPrincipal FindNextUser( string userName, ArrayList allowedUserRoles, ref string domainName, ActionEventArgs e )
        {
            if ( e is null )
            {
                throw new ArgumentNullException( nameof( e ) );
            }

            UserPrincipal user = null;
            if ( string.IsNullOrWhiteSpace( userName ) )
            {
                e.RegisterFailure( "User name is empty" );
            }
            else
            {
                try
                {
                    var domainNames = EnumerateDomains();
                    if ( domainNames is null || domainNames.Count == 0 )
                    {
                        e.RegisterFailure( "No domains found; Most likely, the current security context is not associated with an Active Directory domain or forest." );
                    }
                    else
                    {
                        bool foundDomain = string.IsNullOrEmpty( domainName );
                        foreach ( string name in domainNames )
                        {
                            if ( foundDomain )
                            {
                                // skip until the domain is found or empty.
                                user = FindUser( userName, name, allowedUserRoles, e );
                                if ( user is object )
                                {
                                    domainName = name;
                                    break;
                                }
                            }
                            else
                            {
                                foundDomain = string.Equals( name, domainName, StringComparison.OrdinalIgnoreCase );
                            }
                        }
                    }
                }
                catch ( Exception ex )
                {
                    e.RegisterError( $"Exception occurred;. {ex.ToFullBlownString()}" );
                }
            }

            return user;
        }

        /// <summary> Attempts to find user on the machine. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="userName">   Specifies a user name. </param>
        /// <param name="domainName"> Name of the domain. </param>
        /// <param name="e">          Cancel details event information. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static bool TryFindUser( string userName, string domainName, ActionEventArgs e )
        {
            if ( e is null )
            {
                throw new ArgumentNullException( nameof( e ) );
            }

            bool result = false;
            if ( string.IsNullOrWhiteSpace( userName ) )
            {
                e.RegisterFailure( "User name is empty" );
            }
            else
            {
                try
                {
                    using var ctx = new PrincipalContext( ContextType.Domain, domainName );
                    using var user = UserPrincipal.FindByIdentity( ctx, userName );
                    if ( user is null )
                    {
                        e.RegisterFailure( "User '{0}' not found @{1}", userName, domainName );
                    }
                    else
                    {
                        result = true;
                    }
                }
                catch ( Exception ex )
                {
                    e.RegisterError( $"Exception occurred;. {ex.ToFullBlownString()}" );
                }
            }

            return result;
        }

        /// <summary> Attempts to find user on the machine. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="userName">    Specifies a user name. </param>
        /// <param name="contextType"> [in,out] Type of the context. </param>
        /// <param name="e">           Cancel details event information. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
        public static bool TryFindUser( string userName, ref ContextType contextType, ActionEventArgs e )
        {

            // first try the machine.
            bool result = TryFindUser( userName, e );
            if ( result )
            {
                contextType = ContextType.Machine;
            }
            else
            {
                var domainNames = EnumerateDomains();
                if ( domainNames is null || domainNames.Count == 0 )
                {
                    e.RegisterFailure( "No domains found; Most likely, the current security context is not associated with an Active Directory domain or forest." );
                }
                else
                {
                    contextType = ContextType.Domain;
                    foreach ( string name in domainNames )
                    {
                        result = TryFindUser( userName, name, e );
                        if ( result )
                        {
                            break;
                        }
                    }
                }
            }

            return result;
        }

        #endregion

        #region " AUTHENTICATE "

        /// <summary>
        /// Authenticates a user name and password on the machine and then on the domain.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="userName"> Specifies a user name. </param>
        /// <param name="password"> Specifies a password. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public override bool Authenticate( string userName, string password )
        {
            bool result = false;
            if ( string.IsNullOrWhiteSpace( userName ) )
            {
                this.ValidationMessage = "User name is empty";
            }
            else if ( string.IsNullOrWhiteSpace( password ) )
            {
                this.ValidationMessage = "User password is empty";
            }
            else
            {
                var e = new ActionEventArgs();
                try
                {
                    using ( var user = FindUser( userName, e ) )
                    {
                        this.ValidationMessage = e.Details;
                        result = user is object && user.Context.ValidateCredentials( userName, password );
                        if ( result )
                        {
                        }

                        if ( !result && user is object )
                        {
                            this.ValidationMessage = $"User '{userName}' not authenticated @'{My.MyProject.Computer.Name}";
                        }
                    }

                    if ( !result )
                    {
                        UserPrincipal user = null;
                        try
                        {
                            string domainName = string.Empty;
                            do
                            {
                                user = FindNextUser( userName, ref domainName, e );
                                this.ValidationMessage = e.Details;
                                result = user is object && user.Context.ValidateCredentials( userName, password );
                                if ( !result && user is object )
                                {
                                    this.ValidationMessage = $"User '{userName}' not authenticated @'{domainName}";
                                }
                            }
                            while ( !result && !(user is null) );
                            if ( result )
                            {
                                this.UserRoles = EnumerateUserRoles( user );
                                this.UserName = userName;
                            }
                        }
                        catch
                        {
                            throw;
                        }
                        finally
                        {
                            user?.Dispose();
                            user = null;
                        }
                    }
                }
                catch ( Exception ex )
                {
                    this.ValidationMessage = ex.ToFullBlownString();
                }
            }

            this.IsAuthenticated = result;
            this.Failed = !result;
            return result;
        }

        /// <summary> Authenticates a user name and password. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="userName">         Specifies a user name. </param>
        /// <param name="password">         Specifies a password. </param>
        /// <param name="allowedUserRoles"> Specifies the list of valid roles. This could be compared to
        /// the name of enumeration flags. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public override bool Authenticate( string userName, string password, ArrayList allowedUserRoles )
        {
            bool result = false;
            if ( string.IsNullOrWhiteSpace( userName ) )
            {
                this.ValidationMessage = "User name is empty";
            }
            else if ( string.IsNullOrWhiteSpace( password ) )
            {
                this.ValidationMessage = "User password is empty";
            }
            else
            {
                var e = new ActionEventArgs();
                try
                {
                    using ( var user = FindUser( userName, allowedUserRoles, e ) )
                    {
                        this.ValidationMessage = e.Details;
                        result = user is object && user.Context.ValidateCredentials( userName, password );
                        if ( !result && user is object )
                        {
                            this.ValidationMessage = $"User '{userName}' not authenticated @'{My.MyProject.Computer.Name}";
                        }
                    }

                    if ( !result )
                    {
                        UserPrincipal user = null;
                        try
                        {
                            string domainName = string.Empty;
                            do
                            {
                                user = FindNextUser( userName, allowedUserRoles, ref domainName, e );
                                this.ValidationMessage = e.Details;
                                result = user is object && user.Context.ValidateCredentials( userName, password );
                                if ( !result && user is object )
                                {
                                    this.ValidationMessage = $"User '{userName}' not authenticated @'{domainName}";
                                }
                            }
                            while ( !result && !(user is null) );
                            if ( result )
                            {
                                this.UserRoles = EnumerateUserRoles( user );
                                this.UserName = userName;
                            }
                        }
                        catch
                        {
                            throw;
                        }
                        finally
                        {
                            user?.Dispose();
                            user = null;
                        }
                    }
                }
                catch ( Exception ex )
                {
                    this.ValidationMessage = ex.ToFullBlownString();
                }
            }

            this.IsAuthenticated = result;
            this.Failed = !result;
            return result;
        }

        /// <summary> Tries to find a user in a group role. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="userName">         Specifies a user name. </param>
        /// <param name="allowedUserRoles"> Specifies the list of valid roles. This could be compared to
        /// the name of enumeration flags. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public override bool TryFindUser( string userName, ArrayList allowedUserRoles )
        {
            bool result = false;
            this.Failed = false;
            if ( string.IsNullOrWhiteSpace( userName ) )
            {
                this.ValidationMessage = "User name is empty";
            }
            else if ( allowedUserRoles is null )
            {
                this.ValidationMessage = "User roles not specified";
            }
            else if ( allowedUserRoles.Count == 0 )
            {
                this.ValidationMessage = "Expected user roles not set";
            }
            else
            {
                var e = new ActionEventArgs();
                try
                {
                    using ( var user = FindUser( userName, e ) )
                    {
                        this.ValidationMessage = e.Details;
                        result = user is object && EnumerateUserRoles( user, allowedUserRoles ).Count > 0;
                        if ( !result && user is object )
                        {
                            this.ValidationMessage = $"User '{userName}' is not a member in any of the allowed groups @'{My.MyProject.Computer.Name}";
                        }
                    }

                    if ( !result )
                    {
                        UserPrincipal user = null;
                        try
                        {
                            string domainName = string.Empty;
                            do
                            {
                                user = FindNextUser( userName, ref domainName, e );
                                this.ValidationMessage = e.Details;
                                result = user is object && EnumerateUserRoles( user, allowedUserRoles ).Count > 0;
                                if ( !result && user is object )
                                {
                                    this.ValidationMessage = $"User '{userName}' is not a member in any of the allowed groups @'{domainName}";
                                }
                            }
                            while ( !result && !(user is null) );
                            if ( result )
                            {
                                this.UserRoles = EnumerateUserRoles( user );
                                if ( EnumerateUserRoles( user, allowedUserRoles ).Count > 0 )
                                {
                                    this.UserName = userName;
                                }
                            }
                        }
                        catch
                        {
                            throw;
                        }
                        finally
                        {
                            user?.Dispose();
                            user = null;
                        }
                    }
                }
                catch ( Exception ex )
                {
                    this.ValidationMessage = ex.ToFullBlownString();
                }
            }

            this.Failed = !result;
            return result;
        }
        #endregion

    }
}
