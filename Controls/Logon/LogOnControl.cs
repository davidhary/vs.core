using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace isr.Core.Controls
{

    /// <summary> User log on/off interface. </summary>
    /// <remarks>
    /// (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2008-10-30, 1.00.3225 </para>
    /// </remarks>
    [Description( "User Log On Control" )]
    [ToolboxBitmap( typeof( LogOnControl ) )]
    [System.Runtime.InteropServices.ComVisible( false )]
    public partial class LogOnControl : Forma.ModelViewBase
    {

        #region " CONSTRUCTION "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public LogOnControl()
        {

            // This call is required by the designer.
            this.InitializeComponent();

            // Add any initialization after the InitializeComponent() call.
            this._ErrorProvider = new ErrorProvider( this.components );
            (( ISupportInitialize ) this._ErrorProvider).BeginInit();
            this._ErrorProvider.ContainerControl = this;
            (( ISupportInitialize ) this._ErrorProvider).EndInit();
            this.__LogOnLinkLabel.Name = "_LogOnLinkLabel";
        }

        /// <summary>
        /// Releases the unmanaged resources used by the isr.Core.Controls.LogOnControl and optionally
        /// releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this._Popup is object )
                    {
                        this._Popup.Dispose();
                        this._Popup = null;
                    }

                    this._UserLogOn = null;
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " USER ACTIONS "

        /// <summary> True if is authenticated, false if not. </summary>
        private bool _IsAuthenticated;

        /// <summary> Gets or sets a value indicating whether this object is authenticated. </summary>
        /// <value> <c>true</c> if this object is Authenticated; otherwise <c>false</c>. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public bool IsAuthenticated
        {
            get => this._IsAuthenticated;

            set {
                this._IsAuthenticated = value;
                this.UpdateCaptions();
            }
        }

        /// <summary> The user log on. </summary>
        private LogOnBase _UserLogOn;

        /// <summary> Gets or sets reference to the user log on implementation object. </summary>
        /// <value> The user log on. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public LogOnBase UserLogOn
        {
            get => this._UserLogOn;

            set {
                this._UserLogOn = value;
                this.IsAuthenticated = false;
            }
        }

        #endregion

        #region " ALLOWED USER ROLES "

        /// <summary> The allowed user roles. </summary>

        /// <summary> Gets the allowed user roles. </summary>
        /// <value> The allowed user roles. </value>
        public ArrayList AllowedUserRoles { get; private set; }

        /// <summary> Adds a user role to the list of allowed user roles. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> Specifies a user role to add to the list of allowed user roles. </param>
        public void AddAllowedUserRole( string value )
        {
            if ( this.AllowedUserRoles is null )
            {
                this.AllowedUserRoles = new ArrayList();
            }

            if ( !this.AllowedUserRoles.Contains( value ) )
            {
                _ = this.AllowedUserRoles.Add( value );
            }
        }

        /// <summary> Adds a user roles. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="values"> The values. </param>
        public void AddAllowedUserRoles( string[] values )
        {
            if ( this.AllowedUserRoles is null )
            {
                this.AllowedUserRoles = new ArrayList();
            }

            this.AllowedUserRoles.AddRange( values );
        }

        /// <summary> Clears the allowed user roles. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public void ClearAllowedUserRoles()
        {
            this.AllowedUserRoles = new ArrayList();
        }

        /// <summary> Authenticated roles. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> An ArrayList. </returns>
        public ArrayList AuthenticatedRoles()
        {
            return DomainLogOn.EnumerateUserRoles( this.UserLogOn.UserRoles, this.AllowedUserRoles );
        }

        /// <summary> Returns true if the specified role is allowed. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> Specifies a user role to check against the list of allowed user roles. </param>
        /// <returns> <c>true</c> if user role allowed; otherwise <c>false</c>. </returns>
        public bool IsUserRoleAllowed( string value )
        {
            return this.AllowedUserRoles is object && this.AllowedUserRoles.Contains( value );
        }

        #endregion

        #region " GUI "

        /// <summary> Logs the user on. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void LogOnThis()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.IsAuthenticated = this.AllowedUserRoles is null || this.AllowedUserRoles.Count == 0 ? this.UserLogOn.Authenticate( this._UserNameTextBox.Text, this._PasswordTextBox.Text.Trim() ) : this.UserLogOn.Authenticate( this._UserNameTextBox.Text, this._PasswordTextBox.Text.Trim(), this.AllowedUserRoles );
                if ( !this.IsAuthenticated )
                {
                    this._ErrorProvider.SetError( this._LogOnLinkLabel, this.UserLogOn.ValidationMessage );
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Logs on link label link clicked. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Link label link clicked event information. </param>
        private void LogOnLinkLabel_LinkClicked( object sender, LinkLabelLinkClickedEventArgs e )
        {
            this._ErrorProvider.SetError( this._LogOnLinkLabel, "" );
            if ( this.IsAuthenticated )
            {
                this.IsAuthenticated = false;
                this.UserLogOn.Invalidate();
            }
            else
            {
                this.LogOnThis();
            }

            if ( this._Popup is object )
            {
                this._Popup.Hide();
            }
        }

        /// <summary> The log on caption. </summary>
        private string _LogOnCaption = "Log In";

        /// <summary> Gets or sets the log on caption. </summary>
        /// <value> The log on caption. </value>
        [Category( "Appearance" )]
        [Description( "Caption for log on" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( "Log In" )]
        public string LogOnCaption
        {
            get => this._LogOnCaption;

            set {
                this._LogOnCaption = value;
                this.UpdateCaptions();
            }
        }

        /// <summary> The log off caption. </summary>
        private string _LogOffCaption = "Log Out";

        /// <summary> Gets or sets the log off caption. </summary>
        /// <value> The log off caption. </value>
        [Category( "Appearance" )]
        [Description( "Caption for log off" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( "Log Out" )]
        public string LogOffCaption
        {
            get => this._LogOffCaption;

            set {
                this._LogOffCaption = value;
                this.UpdateCaptions();
            }
        }

        /// <summary> Initializes the user interface and tool tips. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void UserLogOnControl_Load( object sender, EventArgs e )
        {

            // disable when loaded.
            this._UserNameTextBox.Enabled = false;
            this._UserNameTextBox.Text = string.Empty;
            this._PasswordTextBox.Enabled = false;
            this._UserNameTextBox.Text = string.Empty;
            this._LogOnLinkLabel.Enabled = false;
            this._LogOnLinkLabel.Text = this.LogOnCaption;
        }

        /// <summary> Logs on control visible changed. </summary>
        /// <remarks> This in fact enables the control when in a pop-up container. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void LogOnControl_VisibleChanged( object sender, EventArgs e )
        {
            this.UpdateCaptions();
        }

        /// <summary> Updates the captions. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void UpdateCaptions()
        {
            this._LogOnLinkLabel.Text = this._IsAuthenticated ? this.LogOffCaption : this.LogOnCaption;
            if ( this._UserLogOn is object )
            {
                // enable the user name and password boxes
                this._UserNameTextBox.Enabled = true;
                this._PasswordTextBox.Enabled = true;
                this._LogOnLinkLabel.Enabled = true;
            }
        }

        #endregion

        #region " POPUP "

        /// <summary> The popup. </summary>
        private PopupContainer _Popup;

        /// <summary> Shows the pop-up. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="control">  The control. </param>
        /// <param name="location"> The location. </param>
        public void ShowPopup( Control control, Point location )
        {
            this._Popup = new PopupContainer( this );
            this._Popup.Show( control, location );
        }

        #endregion

    }
}
