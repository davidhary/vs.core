using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;

using isr.Core.Controls.ExceptionExtensions;

namespace isr.Core.Controls
{

    /// <summary> Interfaces user log on/off functionality. </summary>
    /// <remarks>
    /// (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2008-10-30, 1.00.3225 </para>
    /// </remarks>
    [System.Runtime.InteropServices.ComVisible( false )]
    public abstract class LogOnBase : IDisposable, INotifyPropertyChanged
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="LogOnBase" /> class. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        protected LogOnBase() : base()
        {
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose( true );

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary>
        /// Gets or sets the dispose status sentinel of the base class.  This applies to the derived
        /// class provided proper implementation.
        /// </summary>
        /// <value> <c>True</c> if disposed; otherwise, <c>False</c>. </value>
        protected bool IsDisposed { get; set; }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter:<para>
        /// If True, the method has been called directly or indirectly by a user's code--managed and
        /// unmanaged resources can be disposed.</para><para>
        /// If False, the method has been called by the runtime from inside the finalizer and you should
        /// not reference other objects--only unmanaged resources can be disposed.</para>
        /// </remarks>
        /// <param name="disposing"> <c>True</c> if this method releases both managed and unmanaged
        /// resources;
        /// False if this method releases only unmanaged
        /// resources. </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.RemoveEventHandler( PropertyChanged );
                }
            }
            finally
            {
                // set the sentinel indicating that the class was disposed.
                this.IsDisposed = true;
            }
        }

        #endregion

        #region " EVENTS "

        /// <summary> Event that is raised when a property value changes. </summary>
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Delegate for handling PropertyChanged events. </summary>
        /// <remarks>   David, 2020-09-24. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Property changed event information. </param>
        public delegate void PropertyChangedEventHandler( object sender, PropertyChangedEventArgs e );

        /// <summary> Removes the event handler. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The value. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void RemoveEventHandler( System.ComponentModel.PropertyChangedEventHandler value )
        {
            foreach ( Delegate d in value is null ? (Array.Empty<Delegate>()) : value.GetInvocationList() )
            {
                try
                {
                    PropertyChanged -= ( System.ComponentModel.PropertyChangedEventHandler ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }
        }

        #endregion

        #region " VALIDATION METHODS "

        /// <summary> Authenticates a user name and password. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="userName"> Specifies a user name. </param>
        /// <param name="password"> Specifies a password. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
        public abstract bool Authenticate( string userName, string password );

        /// <summary> Authenticates a user name and password. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="userName">         Specifies a user name. </param>
        /// <param name="password">         Specifies a password. </param>
        /// <param name="allowedUserRoles"> Specifies the list of valid roles. This could be compared to
        /// the name of enumeration flags. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
        public virtual bool Authenticate( string userName, string password, ArrayList allowedUserRoles )
        {
            this.IsAuthenticated = this.TryFindUser( userName, allowedUserRoles ) && this.Authenticate( userName, password );
            return this.IsAuthenticated;
        }

        /// <summary> Tries to find a user in a group role. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="userName">         Specifies a user name. </param>
        /// <param name="allowedUserRoles"> Specifies the list of valid roles. This could be compared to
        /// the name of enumeration flags. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
        public abstract bool TryFindUser( string userName, ArrayList allowedUserRoles );

        /// <summary> Invalidates the last login user. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public void Invalidate()
        {
            this.IsAuthenticated = false;
            this.Failed = false;
        }

        #endregion

        #region " VALIDATION OUTCOMES "

        /// <summary> Name of the user. </summary>
        private string _UserName;

        /// <summary> Gets or sets (protected) the name of the user. </summary>
        /// <value> The name of the user. </value>
        public string UserName
        {
            get => this._UserName;

            protected set {
                this._UserName = value;
                var evt = PropertyChanged;
                evt?.Invoke( this, new PropertyChangedEventArgs( nameof( this.UserName ) ) );
            }
        }

        /// <summary> The user roles. </summary>
        private ArrayList _UserRoles;

        /// <summary> Gets or sets (protected) the roles of the user. </summary>
        /// <value> The Role of the user. </value>
        public ArrayList UserRoles
        {
            get => this._UserRoles;

            protected set {
                this._UserRoles = value;
                var evt = PropertyChanged;
                evt?.Invoke( this, new PropertyChangedEventArgs( nameof( this.UserRoles ) ) );
            }
        }

        /// <summary> True if is authenticated, false if not. </summary>
        private bool _IsAuthenticated;

        /// <summary>
        /// Gets or sets (protected) a value indicating whether the user is authenticated.
        /// </summary>
        /// <value> <c>true</c> if the user authenticated is valid; otherwise <c>false</c>. </value>
        public bool IsAuthenticated
        {
            get => this._IsAuthenticated;

            protected set {
                this._IsAuthenticated = value;
                var evt = PropertyChanged;
                evt?.Invoke( this, new PropertyChangedEventArgs( nameof( this.IsAuthenticated ) ) );
            }
        }

        /// <summary> The validation details. </summary>
        private string _ValidationDetails;

        /// <summary> Gets or sets (protected) the validation message. </summary>
        /// <value> A message describing the validation. </value>
        public string ValidationMessage
        {
            get => this._ValidationDetails;

            protected set {
                this._ValidationDetails = value;
                var evt = PropertyChanged;
                evt?.Invoke( this, new PropertyChangedEventArgs( nameof( this.ValidationMessage ) ) );
            }
        }

        /// <summary> True if failed. </summary>
        private bool _Failed;

        /// <summary> Gets or sets a value indicating whether the login failed. </summary>
        /// <value> <c>true</c> if failed; otherwise <c>false</c>. </value>
        public bool Failed
        {
            get => this._Failed;

            protected set {
                this._Failed = value;
                var evt = PropertyChanged;
                evt?.Invoke( this, new PropertyChangedEventArgs( nameof( this.Failed ) ) );
            }
        }

        #endregion

    }
}
