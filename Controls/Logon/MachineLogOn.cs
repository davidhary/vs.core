using System;
using System.Collections;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;

using isr.Core.Controls.ExceptionExtensions;

namespace isr.Core.Controls
{

    /// <summary> Machine log on. </summary>
    /// <remarks>
    /// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-10-06, 2.1.5392. </para>
    /// </remarks>
    public class MachineLogOn : LogOnBase
    {

        /// <summary>   Try authenticate. </summary>
        /// <remarks>   David, 2021-05-13. </remarks>
        /// <param name="userName">         Specifies a user name. </param>
        /// <param name="password">         Specifies a password. </param>
        /// <param name="expectedUserRole"> The expected user role. </param>
        /// <returns>   A Tuple. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string Details) TryAuthenticate( string userName, string password, string expectedUserRole )
        {
            try
            {
                using var ctx = new PrincipalContext( ContextType.Machine );
                using var user = UserPrincipal.FindByIdentity( ctx, userName );
                if ( user is null )
                {
                    return (false, $"User '{userName}' not identified on this machine");
                }
                else
                {
                    if ( ctx.ValidateCredentials( userName, password ) )
                    {
                        bool found = false;
                        foreach ( Principal group in user.GetAuthorizationGroups() )
                        {
                            if ( string.Equals( group.Name, expectedUserRole, StringComparison.OrdinalIgnoreCase ) )
                            {
                                found = true;
                                break;
                            }
                        }
                        return (found, found ? string.Empty : $"User '{userName }' does not have the expected '{expectedUserRole}' role");
                    }
                    else
                    {
                        return (false, $"Failed validating credentials for user '{userName}'");
                    }

                }
            }
            catch ( Exception ex )
            {
                return (false, ex.ToFullBlownString());
            }
        }


        /// <summary> Validates a user name and password. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="userName"> Specifies a user name. </param>
        /// <param name="password"> Specifies a password. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public override bool Authenticate( string userName, string password )
        {
            bool result = false;
            if ( string.IsNullOrWhiteSpace( userName ) )
            {
                this.ValidationMessage = "User name is empty";
            }
            else if ( string.IsNullOrWhiteSpace( password ) )
            {
                this.ValidationMessage = "User password is empty";
            }
            else
            {
                try
                {
                    using var ctx = new PrincipalContext( ContextType.Machine );
                    using var user = UserPrincipal.FindByIdentity( ctx, userName );
                    if ( user is null )
                    {
                        this.ValidationMessage = $"User '{userName}' not identified on this machine";
                    }
                    else
                    {
                        this.UserRoles = DomainLogOn.EnumerateUserRoles( user );
                        result = ctx.ValidateCredentials( userName, password );
                        if ( result )
                        {
                            this.UserName = userName;
                        }
                        else
                        {
                            this.UserName = string.Empty;
                            this.ValidationMessage = $"Failed validating credentials for user '{userName }'";
                        }
                    }
                }
                catch ( Exception ex )
                {
                    this.ValidationMessage = ex.ToFullBlownString();
                }
            }

            this.IsAuthenticated = result;
            this.Failed = !result;
            return result;
        }

        /// <summary> Tries to find a user in a group role. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="userName">         Specifies a user name. </param>
        /// <param name="allowedUserRoles"> Specifies the list of valid roles. This could be compared to
        /// the name of enumeration flags. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public override bool TryFindUser( string userName, ArrayList allowedUserRoles )
        {
            bool result = false;
            this.Failed = false;
            if ( string.IsNullOrWhiteSpace( userName ) )
            {
                this.ValidationMessage = "User name is empty";
                this.Failed = true;
            }
            else if ( allowedUserRoles is null )
            {
                this.ValidationMessage = "User roles not specified";
                this.Failed = true;
            }
            else if ( allowedUserRoles.Count == 0 )
            {
                this.ValidationMessage = "Expected user roles not set";
                this.Failed = true;
            }
            else
            {
                try
                {
                    using var ctx = new PrincipalContext( ContextType.Machine );
                    using var user = UserPrincipal.FindByIdentity( ctx, userName );
                    if ( user is null )
                    {
                        this.ValidationMessage = $"User '{userName}' not identified on this machine";
                    }
                    else
                    {
                        this.UserRoles = DomainLogOn.EnumerateUserRoles( user );
                        if ( DomainLogOn.EnumerateUserRoles( user, allowedUserRoles ).Count > 0 )
                        {
                            this.UserName = userName;
                            result = true;
                        }

                        if ( !result )
                        {
                            this.ValidationMessage = $"User '{userName}' role was not found among the allowed user roles.";
                        }
                    }
                }
                catch ( Exception ex )
                {
                    this.ValidationMessage = ex.ToFullBlownString();
                }
            }

            return result;
        }
    }
}
