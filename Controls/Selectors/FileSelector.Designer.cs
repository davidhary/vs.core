﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace isr.Core.Controls
{
    [Microsoft.VisualBasic.CompilerServices.DesignerGenerated()]
    public partial class FileSelector
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;
        internal ToolTip _toolTip;
        private System.Windows.Forms.TextBox __FilePathTextBox;

        private System.Windows.Forms.TextBox _FilePathTextBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __FilePathTextBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__FilePathTextBox != null)
                {
                    __FilePathTextBox.Validating -= FilePathTextBox_Validating;
                }

                __FilePathTextBox = value;
                if (__FilePathTextBox != null)
                {
                    __FilePathTextBox.Validating += FilePathTextBox_Validating;
                }
            }
        }

        private Button __BrowseButton;

        private Button _BrowseButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __BrowseButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__BrowseButton != null)
                {
                    __BrowseButton.Click -= BrowseButton_Click;
                }

                __BrowseButton = value;
                if (__BrowseButton != null)
                {
                    __BrowseButton.Click += BrowseButton_Click;
                }
            }
        }
        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _toolTip = new ToolTip(components);
            __BrowseButton = new Button();
            __BrowseButton.Click += new EventHandler(BrowseButton_Click);
            __FilePathTextBox = new System.Windows.Forms.TextBox();
            __FilePathTextBox.Validating += new System.ComponentModel.CancelEventHandler(FilePathTextBox_Validating);
            SuspendLayout();
            // 
            // _BrowseButton
            // 
            __BrowseButton.BackColor = SystemColors.Control;
            __BrowseButton.Cursor = Cursors.Default;
            __BrowseButton.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold);
            __BrowseButton.ForeColor = SystemColors.ControlText;
            __BrowseButton.Location = new Point(516, 1);
            __BrowseButton.Name = "__BrowseButton";
            __BrowseButton.RightToLeft = RightToLeft.No;
            __BrowseButton.Size = new Size(29, 22);
            __BrowseButton.TabIndex = 0;
            __BrowseButton.Text = "...";
            __BrowseButton.TextAlign = ContentAlignment.TopCenter;
            _toolTip.SetToolTip(__BrowseButton, "Browses for a file");
            __BrowseButton.UseMnemonic = false;
            __BrowseButton.UseVisualStyleBackColor = true;
            // 
            // _FilePathTextBox
            // 
            __FilePathTextBox.AcceptsReturn = true;
            __FilePathTextBox.BackColor = SystemColors.Window;
            __FilePathTextBox.Cursor = Cursors.IBeam;
            __FilePathTextBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold);
            __FilePathTextBox.ForeColor = SystemColors.WindowText;
            __FilePathTextBox.Location = new Point(0, 0);
            __FilePathTextBox.MaxLength = 0;
            __FilePathTextBox.Name = "__FilePathTextBox";
            __FilePathTextBox.RightToLeft = RightToLeft.No;
            __FilePathTextBox.Size = new Size(510, 25);
            __FilePathTextBox.TabIndex = 1;
            // 
            // FileSelector
            // 
            BackColor = Color.Transparent;
            Controls.Add(__FilePathTextBox);
            Controls.Add(__BrowseButton);
            Name = "FileSelector";
            Size = new Size(547, 25);
            Resize += new EventHandler(UserControl_Resize);
            ResumeLayout(false);
            PerformLayout();
        }
    }
}