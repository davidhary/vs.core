using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.Design;

using isr.Core.Controls.ExceptionExtensions;

namespace isr.Core.Controls
{

    /// <summary> Tool strip selector base. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-03-02 </para>
    /// </remarks>
    [ToolStripItemDesignerAvailability( ToolStripItemDesignerAvailability.ToolStrip )]
    public class ToolStripSelectorBase : ToolStripControlHost
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> Call the base constructor passing in a base selector instance. </remarks>
        /// <param name="selectorControl"> The selector control. </param>
        public ToolStripSelectorBase( SelectorControlBase selectorControl ) : base( selectorControl )
        {
            this.AutoSize = false;
        }

        /// <summary>
        /// Releases the unmanaged resources used by the
        /// <see cref="T:System.Windows.Forms.ToolStripControlHost" /> and optionally releases the
        /// managed resources.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> <see langword="true" /> to release both managed and unmanaged
        /// resources; <see langword="false" /> to release only unmanaged
        /// resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.RemoveDirtyChangedEventHandler( DirtyChanged );
                    this.RemoveValueSelectedEventHandler( ValueSelected );
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #region " COLORS "

        /// <summary> Gets or sets the color of the read only back. </summary>
        /// <value> The color of the read only back. </value>
        [DefaultValue( typeof( Color ), "SystemColors.orange" )]
        [Description( "Back color when dirty" )]
        [Category( "Appearance" )]
        public Color DirtyBackColor
        {
            get => this.SelectorControlBase.DirtyBackColor;

            set => this.SelectorControlBase.DirtyBackColor = value;
        }

        /// <summary> Gets or sets the color of the read only foreground. </summary>
        /// <value> The color of the read only foreground. </value>
        [DefaultValue( typeof( Color ), "SystemColors.InactiveCaption" )]
        [Description( "Fore color when dirty" )]
        [Category( "Appearance" )]
        public Color DirtyForeColor
        {
            get => this.SelectorControlBase.DirtyForeColor;

            set => this.SelectorControlBase.DirtyForeColor = value;
        }

        #endregion

        #region " UNDERLYING CONTROL PROPERTIES "

        /// <summary> Gets the numeric up down control. </summary>
        /// <value> The numeric up down control. </value>
        public SelectorControlBase SelectorControlBase => this.Control as SelectorControlBase;

        /// <summary> Gets a value indicating whether this object has value. </summary>
        /// <value> <c>true</c> if this object has value; otherwise <c>false</c> </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public bool HasValue => this.SelectorControlBase.HasValue;

        /// <summary> The watermark text. </summary>
        /// <value> The water mark text with this control. </value>
        [DefaultValue( typeof( string ), "Watermark" )]
        [Description( "Watermark Text" )]
        [Category( "Appearance" )]
        public string Watermark
        {
            get => this.SelectorControlBase.Watermark;

            set => this.SelectorControlBase.Watermark = value;
        }

        /// <summary> Gets or sets the selected text. </summary>
        /// <value> The selected text. </value>
        [DefaultValue( "" )]
        [Description( "Selected text" )]
        [Category( "Appearance" )]
        public string SelectedText
        {
            get => this.SelectorControlBase.SelectedText;

            set => this.SelectorControlBase.SelectedText = value;
        }

        /// <summary> Gets or sets the selected text. </summary>
        /// <value> The selected text. </value>
        [DefaultValue( "" )]
        [Description( "text" )]
        [Category( "Appearance" )]
        public override string Text
        {
            get => this.SelectorControlBase.Text;

            set => this.SelectorControlBase.Text = value;
        }

        /// <summary> Selector icon image. </summary>
        /// <value> The selector icon. </value>
        [Description( "Selector icon image" )]
        [Category( "Appearance" )]
        public Image SelectorIcon
        {
            get => this.SelectorControlBase.SelectorIcon;

            set => this.SelectorControlBase.SelectorIcon = value;
        }

        /// <summary> Gets or sets the read only property. </summary>
        /// <value> The read only. </value>
        [DefaultValue( false )]
        [Category( "Behavior" )]
        [Description( "Indicates whether the check box is read only." )]
        public bool ReadOnly
        {
            get => this.SelectorControlBase.ReadOnly;

            set => this.SelectorControlBase.ReadOnly = value;
        }

        #endregion

        #region " UNDERLYING CONTROL METHODS "

        /// <summary> Select value. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public void SelectValue()
        {
            this.SelectorControlBase.SelectValue();
        }

        #endregion

        #region " EVENT HANDLERS "

        /// <summary> Subscribes events from the hosted control. </summary>
        /// <remarks> Subscribe the control events to expose. </remarks>
        /// <param name="control"> The control from which to subscribe events. </param>
        protected override void OnSubscribeControlEvents( Control control )
        {
            if ( control is object )
            {
                // Call the base so the base events are connected.
                base.OnSubscribeControlEvents( control );

                // Cast the control to a Selector control base.
                if ( control is SelectorControlBase ctrl )
                {
                    // Add the event.
                    ctrl.DirtyChanged += this.OnDirtyChanged;
                    ctrl.ValueSelected += this.OnValueSelected;
                }
            }
        }

        /// <summary> Unsubscribes events from the hosted control. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="control"> The control from which to unsubscribe events. </param>
        protected override void OnUnsubscribeControlEvents( Control control )
        {
            // Call the base method so the basic events are unsubscribed.
            base.OnUnsubscribeControlEvents( control );

            // Cast the control to a Selector control base.
            if ( control is SelectorControlBase ctrl )
            {
                // Remove the events.
                ctrl.DirtyChanged -= this.OnDirtyChanged;
                ctrl.ValueSelected -= this.OnValueSelected;
            }
        }

        /// <summary> Event queue for all listeners interested in DirtyChanged events. </summary>
        public event EventHandler<EventArgs> DirtyChanged;

        /// <summary> Removes event handler. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The handler. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void RemoveDirtyChangedEventHandler( EventHandler<EventArgs> value )
        {
            foreach ( Delegate d in value is null ? (Array.Empty<Delegate>()) : value.GetInvocationList() )
            {
                try
                {
                    DirtyChanged -= ( EventHandler<EventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }
        }

        /// <summary> Raises the value selected event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information to send to registered event handlers. </param>
        private void OnDirtyChanged( object sender, EventArgs e )
        {
            var evt = DirtyChanged;
            evt?.Invoke( this, e );
        }

        /// <summary> Event queue for all listeners interested in ValueSelected events. </summary>
        public event EventHandler<EventArgs> ValueSelected;

        /// <summary> Removes event handler. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The handler. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void RemoveValueSelectedEventHandler( EventHandler<EventArgs> value )
        {
            foreach ( Delegate d in value is null ? (Array.Empty<Delegate>()) : value.GetInvocationList() )
            {
                try
                {
                    ValueSelected -= ( EventHandler<EventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }
        }

        /// <summary> Raises the value selected event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information to send to registered event handlers. </param>
        private void OnValueSelected( object sender, EventArgs e )
        {
            var evt = ValueSelected;
            evt?.Invoke( this, e );
        }

        #endregion

    }
}
