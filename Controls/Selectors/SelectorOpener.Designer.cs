﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Controls
{
    [DesignerGenerated()]
    public partial class SelectorOpener
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _ErrorProvider = new ErrorProvider(components);
            __ToolStrip = new ToolStrip();
            __ToolStrip.DoubleClick += new EventHandler(ToolStripDoubleClick);
            __ClearButton = new ToolStripButton();
            __ClearButton.Click += new EventHandler(ClearButtonClick);
            __ResourceNamesComboBox = new ToolStripSpringComboBox();
            __ResourceNamesComboBox.DoubleClick += new EventHandler(ToolStripDoubleClick);
            __ToggleOpenButton = new ToolStripButton();
            __ToggleOpenButton.Click += new EventHandler(ToggleOpenButtonClick);
            __EnumerateButton = new ToolStripButton();
            __EnumerateButton.Click += new EventHandler(EnumerateButtonClick);
            _OverflowLabel = new System.Windows.Forms.ToolStripLabel();
            ((System.ComponentModel.ISupportInitialize)_ErrorProvider).BeginInit();
            __ToolStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _ErrorProvider
            // 
            _ErrorProvider.ContainerControl = this;
            // 
            // _ToolStrip
            // 
            __ToolStrip.GripStyle = ToolStripGripStyle.Hidden;
            __ToolStrip.Items.AddRange(new ToolStripItem[] { __ClearButton, __ResourceNamesComboBox, __ToggleOpenButton, __EnumerateButton, _OverflowLabel });
            __ToolStrip.Location = new Point(0, 0);
            __ToolStrip.Name = "__ToolStrip";
            __ToolStrip.Size = new Size(474, 29);
            __ToolStrip.TabIndex = 0;
            __ToolStrip.Text = "Selector Opener";
            // 
            // _ClearButton
            // 
            __ClearButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            __ClearButton.Image = My.Resources.Resources.Clear_22x22;
            __ClearButton.ImageScaling = ToolStripItemImageScaling.None;
            __ClearButton.ImageTransparentColor = Color.Magenta;
            __ClearButton.Name = "__ClearButton";
            __ClearButton.Overflow = ToolStripItemOverflow.Never;
            __ClearButton.Size = new Size(26, 26);
            __ClearButton.Text = "Clear known state";
            // 
            // _ResourceNamesComboBox
            // 
            __ResourceNamesComboBox.Name = "__ResourceNamesComboBox";
            __ResourceNamesComboBox.Overflow = ToolStripItemOverflow.Never;
            __ResourceNamesComboBox.Size = new Size(362, 29);
            __ResourceNamesComboBox.ToolTipText = "Available resource names";
            // 
            // _ToggleOpenButton
            // 
            __ToggleOpenButton.Alignment = ToolStripItemAlignment.Right;
            __ToggleOpenButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            __ToggleOpenButton.Image = My.Resources.Resources.Disconnected_22x22;
            __ToggleOpenButton.ImageScaling = ToolStripItemImageScaling.None;
            __ToggleOpenButton.ImageTransparentColor = Color.Magenta;
            __ToggleOpenButton.Name = "__ToggleOpenButton";
            __ToggleOpenButton.Overflow = ToolStripItemOverflow.Never;
            __ToggleOpenButton.Size = new Size(26, 26);
            __ToggleOpenButton.ToolTipText = "Click to open";
            // 
            // _EnumerateButton
            // 
            __EnumerateButton.Alignment = ToolStripItemAlignment.Right;
            __EnumerateButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            __EnumerateButton.Image = My.Resources.Resources.Find_22x22;
            __EnumerateButton.ImageScaling = ToolStripItemImageScaling.None;
            __EnumerateButton.ImageTransparentColor = Color.Magenta;
            __EnumerateButton.Name = "__EnumerateButton";
            __EnumerateButton.Overflow = ToolStripItemOverflow.Never;
            __EnumerateButton.Size = new Size(26, 26);
            __EnumerateButton.ToolTipText = "Enumerate resources";
            // 
            // _OverflowLabel
            // 
            _OverflowLabel.Name = "_OverflowLabel";
            _OverflowLabel.Size = new Size(0, 26);
            // 
            // SelectorOpener
            // 
            BackColor = Color.Transparent;
            Controls.Add(__ToolStrip);
            Margin = new Padding(0);
            Name = "SelectorOpener";
            Size = new Size(474, 29);
            ((System.ComponentModel.ISupportInitialize)_ErrorProvider).EndInit();
            __ToolStrip.ResumeLayout(false);
            __ToolStrip.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        private ErrorProvider _ErrorProvider;
        private ToolStripButton __ClearButton;

        private ToolStripButton _ClearButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ClearButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ClearButton != null)
                {
                    __ClearButton.Click -= ClearButtonClick;
                }

                __ClearButton = value;
                if (__ClearButton != null)
                {
                    __ClearButton.Click += ClearButtonClick;
                }
            }
        }

        private ToolStripSpringComboBox __ResourceNamesComboBox;

        private ToolStripSpringComboBox _ResourceNamesComboBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ResourceNamesComboBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ResourceNamesComboBox != null)
                {
                    __ResourceNamesComboBox.DoubleClick -= ToolStripDoubleClick;
                }

                __ResourceNamesComboBox = value;
                if (__ResourceNamesComboBox != null)
                {
                    __ResourceNamesComboBox.DoubleClick += ToolStripDoubleClick;
                }
            }
        }

        private ToolStripButton __EnumerateButton;

        private ToolStripButton _EnumerateButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __EnumerateButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__EnumerateButton != null)
                {
                    __EnumerateButton.Click -= EnumerateButtonClick;
                }

                __EnumerateButton = value;
                if (__EnumerateButton != null)
                {
                    __EnumerateButton.Click += EnumerateButtonClick;
                }
            }
        }

        private ToolStripButton __ToggleOpenButton;

        private ToolStripButton _ToggleOpenButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ToggleOpenButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ToggleOpenButton != null)
                {
                    __ToggleOpenButton.Click -= ToggleOpenButtonClick;
                }

                __ToggleOpenButton = value;
                if (__ToggleOpenButton != null)
                {
                    __ToggleOpenButton.Click += ToggleOpenButtonClick;
                }
            }
        }

        private ToolStrip __ToolStrip;

        private ToolStrip _ToolStrip
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ToolStrip;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ToolStrip != null)
                {
                    __ToolStrip.DoubleClick -= ToolStripDoubleClick;
                }

                __ToolStrip = value;
                if (__ToolStrip != null)
                {
                    __ToolStrip.DoubleClick += ToolStripDoubleClick;
                }
            }
        }

        private System.Windows.Forms.ToolStripLabel _OverflowLabel;
    }
}