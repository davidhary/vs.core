using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace isr.Core.Controls
{

    /// <summary> Tool strip selector numeric up down. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-03-02 </para>
    /// </remarks>
    [ToolStripItemDesignerAvailability( ToolStripItemDesignerAvailability.ToolStrip )]
    public class ToolStripSelectorNumeric : ToolStripSelectorBase
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> Call the base constructor passing in a SelectorNumeric instance. </remarks>
        public ToolStripSelectorNumeric() : base( new SelectorNumeric() )
        {
        }

        #region " UNDERLYING CONTROL PROPERTIES "

        /// <summary> Gets the numeric up down control. </summary>
        /// <value> The numeric up down control. </value>
        public SelectorNumeric SelectorNumericControl => this.Control as SelectorNumeric;

        /// <summary> Gets or sets the value. </summary>
        /// <value> The value. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public decimal? SelectedValue
        {
            get => this.SelectorNumericControl.SelectedValue;

            set => this.SelectorNumericControl.SelectedValue = value;
        }

        /// <summary> Gets or sets the selected Value. </summary>
        /// <value> The selected Value. </value>
        [DefaultValue( "" )]
        [Description( "Value" )]
        [Category( "Appearance" )]
        public decimal Value
        {
            get => this.SelectorNumericControl.Value;

            set => this.SelectorNumericControl.Value = value;
        }

        /// <summary> Gets or sets the maximum value. </summary>
        /// <value> The maximum value. </value>
        [DefaultValue( 100 )]
        [Description( "Maximum" )]
        [Category( "Appearance" )]
        public decimal Maximum
        {
            get => this.SelectorNumericControl.Maximum;

            set => this.SelectorNumericControl.Maximum = value;
        }

        /// <summary> Gets or sets the minimum value. </summary>
        /// <value> The minimum value. </value>
        [DefaultValue( 0 )]
        [Description( "Minimum" )]
        [Category( "Appearance" )]
        public decimal Minimum
        {
            get => this.SelectorNumericControl.Minimum;

            set => this.SelectorNumericControl.Minimum = value;
        }

        /// <summary> Decimal Places. </summary>
        /// <value> The decimal places. </value>
        [DefaultValue( 0 )]
        [Description( "Decimal Places" )]
        [Category( "Appearance" )]
        public int DecimalPlaces
        {
            get => this.SelectorNumericControl.DecimalPlaces;

            set => this.SelectorNumericControl.DecimalPlaces = value;
        }

        /// <summary> Increment. </summary>
        /// <value> The amount to increment by. </value>
        [DefaultValue( 1 )]
        [Description( "Increment" )]
        [Category( "Appearance" )]
        public decimal Increment
        {
            get => this.SelectorNumericControl.NumericUpDown.Increment;

            set => this.SelectorNumericControl.NumericUpDown.Increment = value;
        }

        /// <summary> Gets the numeric up down. </summary>
        /// <value> The numeric up down. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public NumericUpDown NumericUpDown => this.SelectorNumericControl.NumericUpDown;

        /// <summary> Gets the button. </summary>
        /// <value> The button. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public Button Button => this.SelectorNumericControl.Button;

        #endregion

        #region " UNDERLYING CONTROL METHODS "

        /// <summary> Select value. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The selected Value. </param>
        public void SelectValue( decimal value )
        {
            this.SelectorNumericControl.SelectValue( value );
        }

        #endregion

    }
}
