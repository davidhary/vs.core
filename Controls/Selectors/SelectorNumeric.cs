using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;

using isr.Core.WinForms.NumericUpDownExtensions;

namespace isr.Core.Controls
{

    /// <summary> Selector numeric. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-03-06 </para>
    /// </remarks>
    [Description( "Selector Numeric" )]
    [ToolboxBitmap( typeof( SelectorNumeric ) )]
    [System.Runtime.InteropServices.ComVisible( false )]
    public partial class SelectorNumeric
    {

        #region " CONSTRUCTION "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public SelectorNumeric()
        {

            // This call is required by the designer.
            this.InitializeComponent();
            this.SelectorButton = this.Button;
            this.SelectorTextBox = this.NumericUpDown;
            this._NumericUpDown.Name = "NumericUpDown";
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " EXPOSED PROPERTIES "

        /// <summary> The watermark text. </summary>
        /// <value> The water mark text with this control. </value>
        [DefaultValue( typeof( string ), "Watermark" )]
        [Description( "Watermark Text" )]
        [Category( "Appearance" )]
        public override string Watermark
        {
            get => base.Watermark;

            set {
                if ( !string.Equals( value, this.Watermark ) )
                {
                    if ( System.Text.RegularExpressions.Regex.IsMatch( value, "^[0-9 ]+$" ) )
                    {
                        base.Watermark = value;
                        base.OnDirtyChanged();
                    }
                }
            }
        }

        /// <summary> Hexadecimal. </summary>
        /// <value> <c>true</c> if hexadecimal; otherwise <c>false</c> </value>
        [DefaultValue( false )]
        [Description( "Hexadecimal" )]
        [Category( "Appearance" )]
        public bool Hexadecimal
        {
            get => this.NumericUpDown.Hexadecimal;

            set => this.NumericUpDown.Hexadecimal = value;
        }

        /// <summary> Numeric Value. </summary>
        /// <value> The Value associated with this control. </value>
        [DefaultValue( typeof( decimal ), "1" )]
        [Description( "Numeric Value" )]
        [Category( "Appearance" )]
        public decimal Value
        {
            get => this.NumericUpDown.Value;

            set => this.NumericUpDown.Value = value;
        }

        /// <summary> Decimal Places. </summary>
        /// <value> The number of decimal places. </value>
        [DefaultValue( typeof( int ), "0" )]
        [Description( "Decimal Places" )]
        [Category( "Appearance" )]
        public int DecimalPlaces
        {
            get => this.NumericUpDown.DecimalPlaces;

            set => this.NumericUpDown.DecimalPlaces = value;
        }

        /// <summary> Maximum numeric Value. </summary>
        /// <value> The maximum value allowed with this control. </value>
        [DefaultValue( typeof( decimal ), "100" )]
        [Description( "Maximum Value" )]
        [Category( "Appearance" )]
        public decimal Maximum
        {
            get => this.NumericUpDown.Maximum;

            set => this.NumericUpDown.Maximum = value;
        }

        /// <summary> Minimum numeric Value. </summary>
        /// <value> The minimum value allowed with this control. </value>
        [DefaultValue( typeof( decimal ), "100" )]
        [Description( "Minimum Value" )]
        [Category( "Appearance" )]
        public decimal Minimum
        {
            get => this.NumericUpDown.Minimum;

            set => this.NumericUpDown.Minimum = value;
        }

        /// <summary> Gets a value indicating whether this object has value. </summary>
        /// <value> <c>true</c> if this object has value; otherwise <c>false</c> </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public override bool HasValue => this.NumericUpDown.HasValue();

        /// <summary> Gets or sets the read only property. </summary>
        /// <value> The read only. </value>
        [DefaultValue( false )]
        [Category( "Behavior" )]
        [Description( "Indicates whether the check box is read only." )]
        public override bool ReadOnly
        {
            get => this.NumericUpDown.ReadOnly;

            set {
                this.NumericUpDown.ReadOnly = value;
                this.Button.Visible = !value;
            }
        }

        #endregion

        #region " CAPTION HANDLERS "

        /// <summary> Gets the selected value. </summary>
        /// <value> The selected value. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public decimal? SelectedValue { get; set; }

        /// <summary> Gets a value indicating whether this object has a selected value. </summary>
        /// <value> <c>true</c> if this object is new; otherwise <c>false</c> </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public bool HasSelectedValue => this.SelectedValue.HasValue;

        /// <summary> Select value. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public override void SelectValue()
        {
            this.SelectedValue = this.NumericUpDown.NullValue;
            base.SelectValue();
        }

        #endregion

        #region " SELECT VALUE "

        /// <summary> Select text. </summary>
        /// <remarks> David, 2015-11-27: Make the current selection current. </remarks>
        public override void SelectText()
        {
            this.SelectedValue = this.NumericUpDown.NullValue;
            base.SelectText();
        }

        /// <summary> Select value. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The Value associated with this control. </param>
        public void SelectValue( decimal value )
        {
            this.Value = value;
            this.SelectValue();
        }

        #endregion

        #region " EVENT HANDLERS "

        /// <summary> Value changed. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void NumericUpDown_ValueChanged( object sender, EventArgs e )
        {
            this.OnDirtyChanged();
        }

        /// <summary> Text changed. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void NumericUpDown_NumericTextChanged( object sender, EventArgs e )
        {
            this.OnDirtyChanged();
        }

        #endregion

        #region " VALUE SETTER "

        /// <summary> value setter. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The Value associated with this control. </param>
        public void ValueSetter( decimal value )
        {
            _ = this.NumericUpDown.ValueSetter( value );
        }

        /// <summary> value setter. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The Value associated with this control. </param>
        public void ValueSetter( decimal? value )
        {
            _ = this.NumericUpDown.ValueSetter( value );
        }

        #endregion

    }
}
