using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

using isr.Core.Controls.ExceptionExtensions;

namespace isr.Core.Controls
{

    /// <summary> Selector control base. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-03-06 </para>
    /// </remarks>
    [DefaultEvent( "ValueSelected" )]
    public partial class SelectorControlBase : Forma.ModelViewBase
    {

        #region " CONSTRUCTION "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public SelectorControlBase()
        {

            // This call is required by the designer.
            this.InitializeComponent();
            this.SelectorButtonThis = new Button();
            this.SelectorTextBoxThis = new Control();
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                        this.components = null;
                    }

                    this.SelectorButtonThis?.Dispose();
                    this.SelectorButtonThis = null;
                    this.SelectorTextBoxThis?.Dispose();
                    this.SelectorTextBoxThis = null;
                    this.RemoveDirtyChangedEventHandler( DirtyChanged );
                    this.RemoveValueSelectedEventHandler( ValueSelected );
                    this.RemoveValueSelectingEventHandler( ValueSelecting );
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " BASE CONTROLS "

        private Button _SelectorButtonThis;

        private Button SelectorButtonThis
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._SelectorButtonThis;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._SelectorButtonThis != null )
                {

                    this._SelectorButtonThis.Click -= this.SelectorButton_Click;
                }

                this._SelectorButtonThis = value;
                if ( this._SelectorButtonThis != null )
                {
                    this._SelectorButtonThis.Click += this.SelectorButton_Click;
                }
            }
        }

        /// <summary> Gets or sets the selector button. </summary>
        /// <value> The selector button. </value>
        protected Button SelectorButton
        {
            get => this.SelectorButtonThis;

            set => this.SelectorButtonThis = value;
        }

        private Control _SelectorTextBoxThis;

        private Control SelectorTextBoxThis
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._SelectorTextBoxThis;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._SelectorTextBoxThis != null )
                {
                    this._SelectorTextBoxThis.TextChanged -= this.SelectorTextBox_TextChanged;
                }

                this._SelectorTextBoxThis = value;
                if ( this._SelectorTextBoxThis != null )
                {
                    this._SelectorTextBoxThis.TextChanged += this.SelectorTextBox_TextChanged;
                }
            }
        }

        /// <summary> Gets or sets the selector text box. </summary>
        /// <value> The selector text box. </value>
        protected Control SelectorTextBox
        {
            get => this.SelectorTextBoxThis;

            set {
                this.SelectorTextBoxThis = value;
                this._IsDirty = !this.IsDirty;
            }
        }

        #endregion

        #region " EXPOSED PROPERTIES "

        /// <summary> The watermark. </summary>
        private string _Watermark;

        /// <summary> The watermark text. </summary>
        /// <value> The water mark text with this control. </value>
        [DefaultValue( typeof( string ), "Watermark" )]
        [Description( "Watermark Text" )]
        [Category( "Appearance" )]
        public virtual string Watermark
        {
            get => this._Watermark;

            set {
                if ( !string.Equals( value, this.Watermark ) )
                {
                    this._Watermark = value;
                    this.OnDirtyChangedThis();
                }
            }
        }

        /// <summary> Combo box text. </summary>
        /// <value> The text associated with this control. </value>
        [DefaultValue( typeof( string ), "" )]
        [Description( "Combo box text" )]
        [Category( "Appearance" )]
        public override string Text
        {
            get => this.SelectorTextBox.Text;

            set => this.SelectorTextBox.Text = value;
        }

        /// <summary> Selector icon image. </summary>
        /// <value> The selector icon. </value>
        [Description( "Selector icon image" )]
        [Category( "Appearance" )]
        public Image SelectorIcon
        {
            get => this.SelectorButton.Image;

            set => this.SelectorButton.Image = value;
        }

        /// <summary> Gets or sets the read only property. </summary>
        /// <value> The read only. </value>
        [DefaultValue( false )]
        [Category( "Behavior" )]
        [Description( "Indicates whether the selector is read only." )]
        public virtual bool ReadOnly { get; set; }

        /// <summary> Indicates whether an empty value can be selected. </summary>
        /// <value> The can select empty value. </value>
        [DefaultValue( false )]
        [Category( "Behavior" )]
        [Description( "Indicates whether an empty value can be selected." )]
        public bool CanSelectEmptyValue { get; set; }

        #endregion

        #region " CAPTION HANDLERS "

        /// <summary> Gets the selected text. </summary>
        /// <value> The selected text. </value>
        [DefaultValue( "" )]
        [Description( "Selected text" )]
        [Category( "Appearance" )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public string SelectedText { get; set; }

        /// <summary> Gets the previous selected text. </summary>
        /// <value> The previous selected text. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public string PreviousSelectedText { get; private set; }

        /// <summary> Gets a value indicating whether a new value was selected. </summary>
        /// <value> <c>true</c> if a new value was selected; otherwise <c>false</c> </value>
        public bool IsSelectedNew => !string.Equals( this.PreviousSelectedText, this.SelectedText, StringComparison.OrdinalIgnoreCase );

        /// <summary> Gets a value indicating whether the selector text is empty. </summary>
        /// <value> <c>true</c> if the selector test is empty; otherwise <c>false</c> </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public virtual bool IsEmpty => string.IsNullOrWhiteSpace( this.Text );

        /// <summary> Gets a value indicating whether this object has value. </summary>
        /// <value> <c>true</c> if this object has value; otherwise <c>false</c> </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public virtual bool HasValue => !string.IsNullOrWhiteSpace( this.Text );

        /// <summary> True if is dirty, false if not. </summary>
        private bool _IsDirty;

        /// <summary> Gets a value indicating whether this object is dirty. </summary>
        /// <value> <c>true</c> if this object is dirty; otherwise <c>false</c> </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public bool IsDirty => !string.Equals( this.SelectedText, this.Text );

        /// <summary> Executes the dirty changed action. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void OnDirtyChangedThis()
        {
            if ( string.IsNullOrEmpty( this.Text ) && !string.IsNullOrWhiteSpace( this.Watermark ) )
            {
                this.Text = this.Watermark;
            }

            if ( !string.IsNullOrEmpty( this.Text ) && !string.IsNullOrWhiteSpace( this.Watermark ) && string.Equals( this.Text, this.Watermark ) )
            {
                this.SelectorTextBox.ForeColor = SystemColors.InactiveCaption;
            }
            else if ( this.IsDirty != this._IsDirty )
            {
                this._IsDirty = this.IsDirty;
                var evt = DirtyChanged;
                evt?.Invoke( this, EventArgs.Empty );
            }

            if ( !string.IsNullOrEmpty( this.Text ) && !string.IsNullOrWhiteSpace( this.Watermark ) && string.Equals( this.Text, this.Watermark ) )
            {
                this.SelectorTextBox.ForeColor = SystemColors.InactiveCaption;
            }
            else if ( !this.Enabled )
            {
                this.SelectorButton.BackColor = this.BackColor;
                this.SelectorTextBox.ForeColor = SystemColors.InactiveCaption;
            }
            else if ( !this.IsDirty && this.SelectorTextBox.ForeColor != this.ForeColor )
            {
                this.SelectorButton.BackColor = this.BackColor;
                this.SelectorTextBox.ForeColor = this.ForeColor;
            }
            else if ( this.IsDirty && this.SelectorTextBox.ForeColor != this._DirtyForeColor )
            {
                this.SelectorButton.BackColor = this._DirtyBackColor;
                this.SelectorTextBox.ForeColor = this._DirtyForeColor;
            }

            this.OnContentChanged();
        }

        /// <summary> Executes the content changed action. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        protected virtual void OnContentChanged()
        {
            if ( this.SelectorButton.Enabled != this.CanSelectItem() )
            {
                this.SelectorButton.Enabled = this.CanSelectItem();
                if ( !this.SelectorButton.Enabled )
                {
                    this.SelectorButton.BackColor = this.BackColor;
                    this.SelectorTextBox.ForeColor = SystemColors.InactiveCaption;
                }
            }
        }

        /// <summary> Determine if we can select item. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> <c>true</c> if we can select item; otherwise <c>false</c> </returns>
        protected virtual bool CanSelectItem()
        {
            return this.CanSelectEmptyValue || this.HasValue;
        }

        /// <summary> Executes the dirty changed action. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public virtual void OnDirtyChanged()
        {
            this.OnDirtyChangedThis();
        }

        #endregion

        #region " COLORS "

        /// <summary> The dirty back color. </summary>
        private Color _DirtyBackColor;

        /// <summary> Gets or sets the color of the read only back. </summary>
        /// <value> The color of the read only back. </value>
        [DefaultValue( typeof( Color ), "SystemColors.orange" )]
        [Description( "Back color when dirty" )]
        [Category( "Appearance" )]
        public Color DirtyBackColor
        {
            get {
                if ( this._DirtyBackColor.IsEmpty || this._DirtyBackColor == this.BackColor )
                {
                    this._DirtyBackColor = Color.Orange;
                }

                return this._DirtyBackColor;
            }

            set => this._DirtyBackColor = value;
        }

        /// <summary> The dirty foreground color. </summary>
        private Color _DirtyForeColor;

        /// <summary> Gets or sets the color of the read only foreground. </summary>
        /// <value> The color of the read only foreground. </value>
        [DefaultValue( typeof( Color ), "SystemColors.ActiveCaption" )]
        [Description( "Fore color when dirty" )]
        [Category( "Appearance" )]
        public Color DirtyForeColor
        {
            get {
                if ( this._DirtyForeColor.IsEmpty || this._DirtyForeColor == this.ForeColor )
                {
                    this._DirtyForeColor = SystemColors.ActiveCaption;
                }

                return this._DirtyForeColor;
            }

            set => this._DirtyForeColor = value;
        }

        #endregion

        #region " SELECT FUNCTION "

        /// <summary> Select text. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void SelectTextThis()
        {
            this.PreviousSelectedText = this.SelectedText;
            this.SelectedText = this.SelectorTextBox.Text;
            this.OnDirtyChanged();
        }

        /// <summary> Select text. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public virtual void SelectText()
        {
            this.SelectTextThis();
        }

        /// <summary> Select value. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void SelectValueThis()
        {
            var e = new CancelEventArgs();
            this.OnValueSelecting( this, e );
            if ( e is object && !e.Cancel )
            {
                this.SelectTextThis();
                this.OnValueSelected( this, EventArgs.Empty );
            }
        }

        /// <summary> Select value. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public virtual void SelectValue()
        {
            this.SelectValueThis();
        }

        private void SelectorTextBox_TextChanged( object sender, EventArgs e )
        {
            this.OnDirtyChanged();
        }

        /// <summary> Select value. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        private void SelectValue( Control sender )
        {
            var cursor = sender.Cursor;
            try
            {
                sender.Cursor = Cursors.WaitCursor;
                this.SelectValue();
            }
            finally
            {
                sender.Cursor = cursor;
            }
        }

        private void SelectorButton_Click( object sender, EventArgs e )
        {
            if ( sender is not Control ctrl )
            {
                this.SelectValue();
            }
            else
            {
                this.SelectValue( ctrl );
            }
        }

        /// <summary> Event queue for all listeners interested in Value-Selected events. </summary>
        public event EventHandler<EventArgs> DirtyChanged;

        /// <summary> Removes event handler. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The handler. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void RemoveDirtyChangedEventHandler( EventHandler<EventArgs> value )
        {
            foreach ( Delegate d in value is null ? (Array.Empty<Delegate>()) : value.GetInvocationList() )
            {
                try
                {
                    DirtyChanged -= ( EventHandler<EventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }
        }

        /// <summary> Event queue for all listeners interested in Value-Selecting events. </summary>
        public event EventHandler<CancelEventArgs> ValueSelecting;

        /// <summary> Removes event handler. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The handler. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void RemoveValueSelectingEventHandler( EventHandler<CancelEventArgs> value )
        {
            foreach ( Delegate d in value is null ? (Array.Empty<Delegate>()) : value.GetInvocationList() )
            {
                try
                {
                    ValueSelecting -= ( EventHandler<CancelEventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }
        }

        /// <summary> Raises the Value-Selecting event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information to send to registered event handlers. </param>
        public virtual void OnValueSelecting( object sender, CancelEventArgs e )
        {
            var evt = ValueSelecting;
            evt?.Invoke( this, e );
        }

        /// <summary> Event queue for all listeners interested in Value-Selected events. </summary>
        public event EventHandler<EventArgs> ValueSelected;

        /// <summary> Removes event handler. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The handler. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void RemoveValueSelectedEventHandler( EventHandler<EventArgs> value )
        {
            foreach ( Delegate d in value is null ? (Array.Empty<Delegate>()) : value.GetInvocationList() )
            {
                try
                {
                    ValueSelected -= ( EventHandler<EventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }
        }

        /// <summary> Raises the Value-Selected event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information to send to registered event handlers. </param>
        public virtual void OnValueSelected( object sender, EventArgs e )
        {
            var evt = ValueSelected;
            evt?.Invoke( this, e );
        }

        #endregion

    }
}
