using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

using isr.Core.Controls.ExceptionExtensions;
using isr.Core.WinForms.CompactExtensions;

namespace isr.Core.Controls
{

    /// <summary> A simple text box and browse button file selector. </summary>
    /// <remarks>
    /// (c) 2007 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2007-03-14, 1.2.3405 </para><para>  
    /// Add default event and description and move bitmap setting
    /// from the designer file. </para>
    /// </remarks>
    [Description( "File Selector" )]
    [DefaultEvent( "SelectedChanged" )]
    public partial class FileSelector : Forma.ModelViewBase
    {

        #region " CONSTRUCTION "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public FileSelector() : base()
        {
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this._TextBoxContents = string.Empty;
            this._FilePath = string.Empty;
            this.DefaultExtension = ".TXT";
            this.DialogFilter = "Text files (*.txt; *.lst)|*.txt;*.lst|Batch files (*.bat)|*.bat|All Files (*.*)|*.*";
            this.DialogTitle = "SELECT A TEXT FILE";
            this._FileExtension = string.Empty;
            this._FileName = string.Empty;
            this.FileTitle = string.Empty;
            this.__BrowseButton.Name = "_BrowseButton";
            this.__FilePathTextBox.Name = "_FilePathTextBox";
        }

        /// <summary>
        /// Releases the unmanaged resources used by the isr.Core.Forma.ModelViewBase and optionally
        /// releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.RemoveSelectedChangedEventHandler( SelectedChanged );
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " FILE NAME PROPERTIES "

        /// <summary> The default extension. </summary>
        private string _DefaultExtension;

        /// <summary> Gets or sets the default extension. </summary>
        /// <value> The default extension. </value>
        [Category( "Appearance" )]
        [Description( "Default extension" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( ".TXT" )]
        public string DefaultExtension
        {
            get => this._DefaultExtension;

            set {
                if ( string.IsNullOrWhiteSpace( value ) )
                {
                    value = string.Empty;
                }

                if ( !string.Equals( value, this.DefaultExtension, StringComparison.OrdinalIgnoreCase ) )
                {
                    this._DefaultExtension = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> A filter specifying the dialog. </summary>
        private string _DialogFilter;

        /// <summary>
        /// Gets or sets the filename filter string, which determines the choices of files that appear in
        /// the file type selection drop down list.
        /// </summary>
        /// <value> The dialog filter. </value>
        [Category( "Appearance" )]
        [Description( "Filename filter string determining the choices of files that appear in the file type selection drop down list." )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( "Text files (*.txt; *.lst)|*.txt;*.lst|Batch files (*.bat)|*.bat|All Files (*.*)|*.*" )]
        public string DialogFilter
        {
            get => this._DialogFilter;

            set {
                if ( string.IsNullOrWhiteSpace( value ) )
                {
                    value = string.Empty;
                }

                if ( !string.Equals( value, this.DialogFilter, StringComparison.OrdinalIgnoreCase ) )
                {
                    this._DialogFilter = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The dialog title. </summary>
        private string _DialogTitle;

        /// <summary> Gets or sets the dialog title. </summary>
        /// <value> The dialog title. </value>
        [Category( "Appearance" )]
        [Description( "Dialog caption" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( "SELECT A TEXT FILE" )]
        public string DialogTitle
        {
            get => this._DialogTitle;

            set {
                if ( string.IsNullOrWhiteSpace( value ) )
                {
                    value = string.Empty;
                }

                if ( !string.Equals( value, this.DialogTitle, StringComparison.OrdinalIgnoreCase ) )
                {
                    this._DialogTitle = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The file extension. </summary>
        private string _FileExtension;

        /// <summary> Gets or sets the file extension. </summary>
        /// <value> The name of the extension. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public string FileExtension
        {
            get => this._FileExtension;

            protected set {
                this._FileExtension = value;
                this.FileTitle = ParseFileTitle( this.FileName, this.FileExtension );
            }
        }

        /// <summary> Filename of the file. </summary>
        private string _FileName;

        /// <summary> Gets or sets the file name. </summary>
        /// <value> The name of the file. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public string FileName
        {
            get => this._FileName;

            set {
                this._FileName = value;
                this.FileTitle = ParseFileTitle( this.FileName, this.FileExtension );
            }
        }

        /// <summary> Parses file title. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="name">      The name. </param>
        /// <param name="extension"> The extension. </param>
        /// <returns> A String. </returns>
        public static string ParseFileTitle( string name, string extension )
        {
            return string.IsNullOrWhiteSpace( name ) ? string.Empty : string.IsNullOrWhiteSpace( extension ) ? name : name.Substring( 0, name.LastIndexOf( extension, StringComparison.OrdinalIgnoreCase ) );
        }

        /// <summary> The text box contents. </summary>

        private string _TextBoxContents;

        /// <summary> Full pathname of the file. </summary>
        private string _FilePath;

        /// <summary> Gets or sets a new file path. </summary>
        /// <value> The full pathname of the file. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public string FilePath
        {
            get => this._FilePath;

            set {
                if ( string.IsNullOrWhiteSpace( value ) )
                {
                    value = string.Empty;
                }

                value = value.Trim();
                if ( this._FilePath is null )
                {
                    this._FilePath = string.Empty;
                }

                if ( !(this._FilePath.Equals( value, comparisonType: StringComparison.OrdinalIgnoreCase ) && this._TextBoxContents.Equals( value.Compact( this._FilePathTextBox ), comparisonType: StringComparison.OrdinalIgnoreCase )) )
                {
                    if ( string.IsNullOrWhiteSpace( value ) )
                    {
                        this._TextBoxContents = string.Empty;
                        this._FilePathTextBox.Text = this._TextBoxContents;
                        this._FileName = this._TextBoxContents;
                        this._FileExtension = string.Empty;
                        this._toolTip.SetToolTip( this._FilePathTextBox, "Enter or browse for a file name" );
                    }
                    else if ( System.IO.File.Exists( value ) )
                    {
                        var fileInfo = My.MyProject.Computer.FileSystem.GetFileInfo( value );
                        value = fileInfo.FullName;
                        this._TextBoxContents = value.Compact( this._FilePathTextBox );
                        this._FilePathTextBox.Text = this._TextBoxContents;
                        this._toolTip.SetToolTip( this._FilePathTextBox, value );
                        this._FileName = fileInfo.Name;
                        this._FileExtension = fileInfo.Extension;
                    }
                    else
                    {
                        this._TextBoxContents = "<file not found: " + value + ">";
                        this._FilePathTextBox.Text = this._TextBoxContents;
                        this._toolTip.SetToolTip( this._FilePathTextBox, "Enter or browse for a file name" );
                        this._FileName = string.Empty;
                        this._FileExtension = string.Empty;
                    }

                    this.FileTitle = ParseFileTitle( this.FileName, this.FileExtension );
                    this._FilePath = value;
                    this.OnSelectedChanged();
                }

                this._FilePathTextBox.Refresh();
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> Gets the file title. </summary>
        /// <value> The file title. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public string FileTitle { get; private set; }

        #endregion

        #region " REFRESH ; SELECT FILE "

        /// <summary> Refreshes the control. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public override void Refresh()
        {
            base.Refresh();
        }

        /// <summary> Selects a file. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
        public bool SelectFile()
        {
            using var dialog = new OpenFileDialog {
                RestoreDirectory = true,
                Multiselect = false,
                CheckFileExists = true,
                CheckPathExists = true,
                ReadOnlyChecked = false,
                DefaultExt = this._DefaultExtension,
                Title = this._DialogTitle,
                FilterIndex = 0,
                FileName = string.IsNullOrWhiteSpace( this._FilePath ) ? My.MyProject.Computer.FileSystem.CurrentDirectory : this.FilePath,
                Filter = this._DialogFilter
            };

            // Open the Open dialog
            if ( dialog.ShowDialog( this ) == DialogResult.OK )
            {

                // if file selected,
                this.FilePath = dialog.FileName;

                // return true for pass
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region " CONTROL EVENT HANDLERS "

        /// <summary>
        /// Event handler. Called by browseButton for click events. Browses for a file name.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="eventSender"> The event sender. </param>
        /// <param name="eventArgs">   Event information. </param>
        private void BrowseButton_Click( object eventSender, EventArgs eventArgs )
        {

            // Select a file
            _ = this.SelectFile();
        }

        /// <summary>
        /// Event handler. Called by filePathTextBox for validating events. Enters a file name.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Cancel event information. </param>
        private void FilePathTextBox_Validating( object sender, CancelEventArgs e )
        {
            this.FilePath = this._FilePathTextBox.Text;
        }

        /// <summary>
        /// Event handler. Called by UserControl for resize events. Resized the frame and sets the width
        /// of the text boxes.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="eventSender"> The event sender. </param>
        /// <param name="eventArgs">   Event information. </param>
        private void UserControl_Resize( object eventSender, EventArgs eventArgs )
        {
            this.Height = Math.Max( this._BrowseButton.Height, this._FilePathTextBox.Height ) + 2;
            this._BrowseButton.Top = (this.Height - this._BrowseButton.Height) / 2;
            this._BrowseButton.Left = this.Width - this._BrowseButton.Width - 1;
            this._BrowseButton.Refresh();
            this._FilePathTextBox.Top = (this.Height - this._FilePathTextBox.Height) / 2;
            this._FilePathTextBox.Width = this._BrowseButton.Left - 1;
            this._FilePathTextBox.Refresh();
            this.Refresh();
        }

        #endregion

        #region " EVENTS "

        /// <summary> Occurs when a new file was selected. </summary>
        public event EventHandler<EventArgs> SelectedChanged;

        /// <summary> Removes event handler. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The handler. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void RemoveSelectedChangedEventHandler( EventHandler<EventArgs> value )
        {
            foreach ( Delegate d in value is null ? (Array.Empty<Delegate>()) : value.GetInvocationList() )
            {
                try
                {
                    SelectedChanged -= ( EventHandler<EventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }
        }

        /// <summary> Raises the selected changed event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public void OnSelectedChanged()
        {
            var evt = SelectedChanged;
            evt?.Invoke( this, EventArgs.Empty );
            Application.DoEvents();
        }

        #endregion

    }
}
