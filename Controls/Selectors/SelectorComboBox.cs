using System;
using System.ComponentModel;
using System.Drawing;

namespace isr.Core.Controls
{

    /// <summary> Selector combo box. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-03-06 </para>
    /// </remarks>
    [Description( "Selector Combo Box" )]
    [ToolboxBitmap( typeof( SelectorComboBox ) )]
    [System.Runtime.InteropServices.ComVisible( false )]
    public partial class SelectorComboBox : SelectorControlBase
    {

        #region " CONSTRUCTION "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public SelectorComboBox()
        {

            // This call is required by the designer.
            this.InitializeComponent();
            this.SelectorButton = this.Button;
            this.SelectorTextBox = this.ComboBox;
            base.OnDirtyChanged();
            this._ComboBox.Name = "ComboBox";
        }

        /// <summary>
        /// Releases the unmanaged resources used by the isr.Core.Controls.SelectorComboBox and
        /// optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " EXPOSED PROPERTIES "

        /// <summary> Gets or sets the read only property. </summary>
        /// <value> The read only. </value>
        [DefaultValue( false )]
        [Category( "Behavior" )]
        [Description( "Indicates whether the check box is read only." )]
        public override bool ReadOnly
        {
            get => this.ComboBox.ReadOnly;

            set {
                base.ReadOnly = value;
                this.ComboBox.ReadOnly = value;
                this.Button.Visible = !value;
            }
        }

        /// <summary> Gets or sets the enabled. </summary>
        /// <remarks>
        /// Had some issues. Setting enabled on the control toggles the combo box. But if the combo
        /// enabled is set, the link breaks.
        /// </remarks>
        /// <value> The enabled. </value>
        public new bool Enabled
        {
            get => base.Enabled;

            set {
                if ( base.Enabled != value )
                {
                    base.Enabled = value;
                }

                if ( this.ComboBox.Enabled != value )
                {
                    this.ComboBox.Enabled = value;
                }
            }
        }

        #endregion

        #region " CAPTION HANDLERS "

        /// <summary> Gets the selected item. </summary>
        /// <value> The selected item. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public object SelectedItem { get; set; }

        /// <summary> Gets the selected value. </summary>
        /// <value> The selected value. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public object SelectedValue { get; set; }

        /// <summary> Gets a value indicating whether a value was selected. </summary>
        /// <value> <c>true</c> if the selector has a selected value; otherwise <c>false</c> </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public bool HasSelectedValue => this.SelectedValue is object;

        #endregion

        #region " SELECT VALUE "

        /// <summary> Select value. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The value. </param>
        public void SelectValue( string value )
        {
            this.Text = value;
            this.SelectValue();
        }

        /// <summary> Searches for the first match for the given string. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> An Integer. </returns>
        public int FindIndex( string value )
        {
            return this.ComboBox.FindStringExact( value );
        }

        /// <summary> Displays an existing value described by value. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The value. </param>
        public void DisplayExistingValue( string value )
        {
            this.Text = this.ComboBox.FindString( value ) >= 0 ? value : this.Watermark;
        }

        #endregion

        #region " EVENT HANDLERS "

        /// <summary> Combo box data source changed. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ComboBox_DataSourceChanged( object sender, EventArgs e )
        {
            this.OnContentChanged();
        }

        /// <summary> Combo box selected value changed. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ComboBox_SelectedValueChanged( object sender, EventArgs e )
        {
            this.OnDirtyChanged();
        }

        #endregion

    }
}
