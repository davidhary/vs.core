using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace isr.Core.Controls
{
    [Microsoft.VisualBasic.CompilerServices.DesignerGenerated()]
    public partial class SelectorComboBox
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            _ComboBox = new ComboBox();
            _ComboBox.DataSourceChanged += new EventHandler(ComboBox_DataSourceChanged);
            _ComboBox.SelectedValueChanged += new EventHandler(ComboBox_SelectedValueChanged);
            Button = new Button();
            SuspendLayout();
            // 
            // ComboBox
            // 
            _ComboBox.Dock = DockStyle.Top;
            _ComboBox.FormattingEnabled = true;
            _ComboBox.Location = new Point(0, 0);
            _ComboBox.Name = "_ComboBox";
            _ComboBox.ReadOnlyBackColor = SystemColors.Control;
            _ComboBox.ReadOnlyForeColor = SystemColors.WindowText;
            _ComboBox.ReadWriteBackColor = SystemColors.Window;
            _ComboBox.ReadWriteForeColor = SystemColors.ControlText;
            _ComboBox.Size = new Size(66, 25);
            _ComboBox.TabIndex = 0;
            // 
            // Button
            // 
            Button.Dock = DockStyle.Right;
            Button.Image = My.Resources.Resources.dialog_ok_4;
            Button.Location = new Point(66, 0);
            Button.MaximumSize = new Size(25, 25);
            Button.Name = "Button";
            Button.Size = new Size(25, 25);
            Button.TabIndex = 1;
            Button.UseVisualStyleBackColor = true;
            // 
            // SelectorComboBox
            // 
            AutoScaleDimensions = new SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(_ComboBox);
            Controls.Add(Button);
            Name = "SelectorComboBox";
            Size = new Size(91, 25);
            ResumeLayout(false);
        }

        private ComboBox _ComboBox;

        /// <summary>   Gets or sets the combo box. </summary>
        /// <value> The combo box. </value>
        public ComboBox ComboBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _ComboBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_ComboBox != null)
                {
                    _ComboBox.DataSourceChanged -= ComboBox_DataSourceChanged;
                    _ComboBox.SelectedValueChanged -= ComboBox_SelectedValueChanged;
                }

                _ComboBox = value;
                if (_ComboBox != null)
                {
                    _ComboBox.DataSourceChanged += ComboBox_DataSourceChanged;
                    _ComboBox.SelectedValueChanged += ComboBox_SelectedValueChanged;
                }
            }
        }

        /// <summary>   The button control. </summary>
        public Button Button;
    }
}
