using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace isr.Core.Controls
{

    /// <summary> Tool strip selector combo box. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-03-02 </para>
    /// </remarks>
    [ToolStripItemDesignerAvailability( ToolStripItemDesignerAvailability.ToolStrip )]
    public class ToolStripSelectorComboBox : ToolStripSelectorBase
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> Call the base constructor passing in a SelectorComboBox instance. </remarks>
        public ToolStripSelectorComboBox() : base( new SelectorComboBox() )
        {
        }

        #region " UNDERLYING CONTROL PROPERTIES "

        /// <summary> Gets the numeric up down control. </summary>
        /// <value> The numeric up down control. </value>
        public SelectorComboBox SelectorComboBoxControl => this.Control as SelectorComboBox;

        /// <summary> Gets the combo box. </summary>
        /// <value> The combo box. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public ComboBox ComboBox => this.SelectorComboBoxControl.ComboBox;

        /// <summary> Gets the button. </summary>
        /// <value> The button. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public Button Button => this.SelectorComboBoxControl.Button;

        #endregion

        #region " UNDERLYING CONTROL METHODS "

        /// <summary> Select value. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The value. </param>
        public void SelectValue( string value )
        {
            this.SelectorComboBoxControl.SelectValue( value );
        }

        #endregion

    }
}
