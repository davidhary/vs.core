using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Controls
{
    [DesignerGenerated()]
    public partial class SelectorNumeric : SelectorControlBase
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            Button = new Button();
            _NumericUpDown = new NumericUpDown();
            _NumericUpDown.ValueChanged += new EventHandler(NumericUpDown_ValueChanged);
            _NumericUpDown.NumericTextChanged += new EventHandler<EventArgs>(NumericUpDown_NumericTextChanged);
            ((System.ComponentModel.ISupportInitialize)_NumericUpDown).BeginInit();
            SuspendLayout();
            // 
            // Button
            // 
            Button.Dock = DockStyle.Right;
            Button.Image = My.Resources.Resources.dialog_ok_4;
            Button.Location = new Point(49, 0);
            Button.MaximumSize = new Size(25, 25);
            Button.Name = "Button";
            Button.Size = new Size(25, 25);
            Button.TabIndex = 1;
            Button.UseVisualStyleBackColor = true;
            // 
            // NumericUpDown
            // 
            _NumericUpDown.Dock = DockStyle.Bottom;
            _NumericUpDown.Location = new Point(0, 0);
            _NumericUpDown.Name = "_NumericUpDown";
            _NumericUpDown.NullValue = new decimal(new int[] { 0, 0, 0, 0 });
            _NumericUpDown.ReadOnlyBackColor = SystemColors.Control;
            _NumericUpDown.ReadOnlyForeColor = SystemColors.WindowText;
            _NumericUpDown.ReadWriteBackColor = SystemColors.Window;
            _NumericUpDown.ReadWriteForeColor = SystemColors.ControlText;
            _NumericUpDown.Size = new Size(49, 25);
            _NumericUpDown.TabIndex = 0;
            _NumericUpDown.Value = new decimal(new int[] { 0, 0, 0, 0 });
            // 
            // SelectorNumeric
            // 
            AutoScaleDimensions = new SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(_NumericUpDown);
            Controls.Add(Button);
            Name = "SelectorNumeric";
            Size = new Size(74, 25);
            ((System.ComponentModel.ISupportInitialize)_NumericUpDown).EndInit();
            ResumeLayout(false);
        }

        /// <summary>   The button control. </summary>
        public Button Button;

        private NumericUpDown _NumericUpDown;

        /// <summary>   Gets or sets the numeric up down. </summary>
        /// <value> The numeric up down. </value>
        public NumericUpDown NumericUpDown
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _NumericUpDown;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_NumericUpDown != null)
                {
                    _NumericUpDown.ValueChanged -= NumericUpDown_ValueChanged;
                    _NumericUpDown.NumericTextChanged -= NumericUpDown_NumericTextChanged;
                }

                _NumericUpDown = value;
                if (_NumericUpDown != null)
                {
                    _NumericUpDown.ValueChanged += NumericUpDown_ValueChanged;
                    _NumericUpDown.NumericTextChanged += NumericUpDown_NumericTextChanged;
                }
            }
        }
    }
}
