using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace isr.Core.Controls
{

    /// <summary> A tab style vs 2010 provider. </summary>
    /// <remarks>
    /// (c) 2010 The Man from U.N.C.L.E. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-09-26 </para><para>
    /// http://www.codeproject.com/Articles/91387/Painting-Your-Own-Tabs-Second-Edition </para>
    /// </remarks>
    [System.ComponentModel.ToolboxItem( false )]
    public class TabStyleVS2010Provider : TabStyleRoundedProvider
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="tabControl"> The tab control. </param>
        public TabStyleVS2010Provider( CustomTabControl tabControl ) : base( tabControl, Color.Transparent, Color.FromArgb( 155, 167, 183 ), Color.White, Color.WhiteSmoke, new Point( 6, 5 ), 3, true, Color.Black, Color.FromArgb( 117, 99, 61 ) )
        {
        }

        /// <summary> Gets tab background brush. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="index"> Zero-based index of the. </param>
        /// <returns> The tab background brush. </returns>
        protected override Brush GetTabBackgroundBrush( int index )
        {
            LinearGradientBrush fillBrush = null;

            // Capture the colors dependent on selection state of the tab
            var dark = Color.Transparent;
            var light = Color.Transparent;
            if ( this.TabControl.SelectedIndex == index )
            {
                dark = Color.FromArgb( 229, 195, 101 );
                light = SystemColors.Window;
            }
            else if ( !this.TabControl.TabPages[index].Enabled )
            {
                light = dark;
            }
            else if ( this.HotTrack && index == this.TabControl.ActiveIndex )
            {
                // Enable hot tracking
                dark = Color.FromArgb( 108, 116, 118 );
                light = dark;
            }

            // Get the correctly aligned gradient
            var tabBounds = this.GetTabRect( index );
            tabBounds.Inflate( 3, 3 );
            tabBounds.X -= 1;
            tabBounds.Y -= 1;
            switch ( this.TabControl.Alignment )
            {
                case TabAlignment.Top:
                    {
                        fillBrush = new LinearGradientBrush( tabBounds, light, dark, LinearGradientMode.Vertical );
                        break;
                    }

                case TabAlignment.Bottom:
                    {
                        fillBrush = new LinearGradientBrush( tabBounds, dark, light, LinearGradientMode.Vertical );
                        break;
                    }

                case TabAlignment.Left:
                    {
                        fillBrush = new LinearGradientBrush( tabBounds, light, dark, LinearGradientMode.Horizontal );
                        break;
                    }

                case TabAlignment.Right:
                    {
                        fillBrush = new LinearGradientBrush( tabBounds, dark, light, LinearGradientMode.Horizontal );
                        break;
                    }
            }

            // Add the blend
            fillBrush.Blend = GetBackgroundBlend();
            return fillBrush;
        }

        /// <summary> Gets background blend. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> The background blend. </returns>
        private static Blend GetBackgroundBlend()
        {
            var relativeIntensities = new float[] { 0f, 0.5f, 1.0f, 1.0f };
            var relativePositions = new float[] { 0f, 0.5f, 0.51f, 1.0f };
            var blend = new Blend() { Factors = relativeIntensities, Positions = relativePositions };
            return blend;
        }

        /// <summary> Gets page background brush. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="index"> Zero-based index of the. </param>
        /// <returns> The page background brush. </returns>
        public override Brush GetPageBackgroundBrush( int index )
        {

            // Capture the colors dependent on selection state of the tab
            var light = Color.Transparent;
            if ( this.TabControl.SelectedIndex == index )
            {
                light = Color.FromArgb( 229, 195, 101 );
            }
            else if ( !this.TabControl.TabPages[index].Enabled )
            {
                light = Color.Transparent;
            }
            else if ( this.HotTrack && index == this.TabControl.ActiveIndex )
            {
                // Enable hot tracking
                light = Color.Transparent;
            }

            return new SolidBrush( light );
        }

        /// <summary> Draw tab closer. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="index">    Zero-based index of the. </param>
        /// <param name="graphics"> The graphics. </param>
        protected override void DrawTabCloser( int index, Graphics graphics )
        {
            if ( graphics is null )
            {
                throw new ArgumentNullException( nameof( graphics ) );
            }

            if ( this.ShowTabCloser && graphics is object )
            {
                var closerRect = this.TabControl.GetTabCloserRect( index );
                graphics.SmoothingMode = SmoothingMode.AntiAlias;
                if ( closerRect.Contains( this.TabControl.MousePosition ) )
                {
                    using ( var closerPath = GetCloserButtonPath( closerRect ) )
                    {
                        graphics.FillPath( Brushes.White, closerPath );
                        using var closerPen = new Pen( Color.FromArgb( 229, 195, 101 ) );
                        graphics.DrawPath( closerPen, closerPath );
                    }

                    using ( var closerPath = GetCloserPath( closerRect ) )
                    {
                        using var closerPen = new Pen( this.CloserColorActive ) {
                            Width = 2f
                        };
                        graphics.DrawPath( closerPen, closerPath );
                    }
                }
                else if ( index == this.TabControl.SelectedIndex )
                {
                    using var closerPath = GetCloserPath( closerRect );
                    using var closerPen = new Pen( this.CloserColor ) {
                        Width = 2f
                    };
                    graphics.DrawPath( closerPen, closerPath );
                }
                else if ( index == this.TabControl.ActiveIndex )
                {
                    using var closerPath = GetCloserPath( closerRect );
                    using var closerPen = new Pen( Color.FromArgb( 155, 167, 183 ) ) {
                        Width = 2f
                    };
                    graphics.DrawPath( closerPen, closerPath );
                }
            }
        }

        /// <summary> Gets closer button path. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="closerRect"> The closer rectangle. </param>
        /// <returns> The closer button path. </returns>
        private static GraphicsPath GetCloserButtonPath( Rectangle closerRect )
        {
            var closerPath = new GraphicsPath();
            closerPath.AddLine( closerRect.X - 1, closerRect.Y - 2, closerRect.Right + 1, closerRect.Y - 2 );
            closerPath.AddLine( closerRect.Right + 2, closerRect.Y - 1, closerRect.Right + 2, closerRect.Bottom + 1 );
            closerPath.AddLine( closerRect.Right + 1, closerRect.Bottom + 2, closerRect.X - 1, closerRect.Bottom + 2 );
            closerPath.AddLine( closerRect.X - 2, closerRect.Bottom + 1, closerRect.X - 2, closerRect.Y - 1 );
            closerPath.CloseFigure();
            return closerPath;
        }
    }
}
