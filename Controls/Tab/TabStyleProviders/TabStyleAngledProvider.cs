using System;
using System.Drawing;
using System.Windows.Forms;

namespace isr.Core.Controls
{

    /// <summary> A tab style angled provider. </summary>
    /// <remarks>
    /// (c) 2010 The Man from U.N.C.L.E. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-09-26 </para><para>
    /// http://www.codeproject.com/Articles/91387/Painting-Your-Own-Tabs-Second-Edition </para>
    /// </remarks>
    [System.ComponentModel.ToolboxItem( false )]
    public class TabStyleAngledProvider : TabStyleProvider
    {

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="tabControl"> The tab control. </param>
        public TabStyleAngledProvider( CustomTabControl tabControl ) : base( tabControl, ContentAlignment.MiddleRight, 10, 7, new Point( 10, 3 ) )
        {
        }

        /// <summary> Adds a tab border to 'tabBounds'. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="path">      Full pathname of the file. </param>
        /// <param name="tabBounds"> The tab bounds. </param>
        public override void AddTabBorder( System.Drawing.Drawing2D.GraphicsPath path, Rectangle tabBounds )
        {
            if ( path is null )
            {
                throw new ArgumentNullException( nameof( path ) );
            }

            switch ( this.TabControl.Alignment )
            {
                case TabAlignment.Top:
                    {
                        path.AddLine( tabBounds.X, tabBounds.Bottom, tabBounds.X + this.Radius - 2, tabBounds.Y + 2 );
                        path.AddLine( tabBounds.X + this.Radius, tabBounds.Y, tabBounds.Right - this.Radius, tabBounds.Y );
                        path.AddLine( tabBounds.Right - this.Radius + 2, tabBounds.Y + 2, tabBounds.Right, tabBounds.Bottom );
                        break;
                    }

                case TabAlignment.Bottom:
                    {
                        path.AddLine( tabBounds.Right, tabBounds.Y, tabBounds.Right - this.Radius + 2, tabBounds.Bottom - 2 );
                        path.AddLine( tabBounds.Right - this.Radius, tabBounds.Bottom, tabBounds.X + this.Radius, tabBounds.Bottom );
                        path.AddLine( tabBounds.X + this.Radius - 2, tabBounds.Bottom - 2, tabBounds.X, tabBounds.Y );
                        break;
                    }

                case TabAlignment.Left:
                    {
                        path.AddLine( tabBounds.Right, tabBounds.Bottom, tabBounds.X + 2, tabBounds.Bottom - this.Radius + 2 );
                        path.AddLine( tabBounds.X, tabBounds.Bottom - this.Radius, tabBounds.X, tabBounds.Y + this.Radius );
                        path.AddLine( tabBounds.X + 2, tabBounds.Y + this.Radius - 2, tabBounds.Right, tabBounds.Y );
                        break;
                    }

                case TabAlignment.Right:
                    {
                        path.AddLine( tabBounds.X, tabBounds.Y, tabBounds.Right - 2, tabBounds.Y + this.Radius - 2 );
                        path.AddLine( tabBounds.Right, tabBounds.Y + this.Radius, tabBounds.Right, tabBounds.Bottom - this.Radius );
                        path.AddLine( tabBounds.Right - 2, tabBounds.Bottom - this.Radius + 2, tabBounds.X, tabBounds.Bottom );
                        break;
                    }
            }
        }
    }
}
