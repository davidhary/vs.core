using System;
using System.Drawing;
using System.Windows.Forms;

namespace isr.Core.Controls
{

    /// <summary> A tab style visual studio provider. </summary>
    /// <remarks>
    /// (c) 2010 The Man from U.N.C.L.E. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-09-26 </para><para>
    /// http://www.codeproject.com/Articles/91387/Painting-Your-Own-Tabs-Second-Edition </para>
    /// </remarks>
    [System.ComponentModel.ToolboxItem( false )]
    public class TabStyleVisualStudioProvider : TabStyleProvider
    {

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="tabControl"> The tab control. </param>
        public TabStyleVisualStudioProvider( CustomTabControl tabControl ) : base( tabControl, ContentAlignment.MiddleRight, 1, 7, new Point( 14, 1 ) )
        {
        }

        /// <summary> Adds a tab border to 'tabBounds'. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="path">      Full pathname of the file. </param>
        /// <param name="tabBounds"> The tab bounds. </param>
        public override void AddTabBorder( System.Drawing.Drawing2D.GraphicsPath path, Rectangle tabBounds )
        {
            if ( path is null )
            {
                throw new ArgumentNullException( nameof( path ) );
            }

            switch ( this.TabControl.Alignment )
            {
                case TabAlignment.Top:
                    {
                        path.AddLine( tabBounds.X, tabBounds.Bottom, tabBounds.X + tabBounds.Height - 4, tabBounds.Y + 2 );
                        path.AddLine( tabBounds.X + tabBounds.Height, tabBounds.Y, tabBounds.Right - 3, tabBounds.Y );
                        path.AddArc( tabBounds.Right - 6, tabBounds.Y, 6, 6, 270f, 90f );
                        path.AddLine( tabBounds.Right, tabBounds.Y + 3, tabBounds.Right, tabBounds.Bottom );
                        break;
                    }

                case TabAlignment.Bottom:
                    {
                        path.AddLine( tabBounds.Right, tabBounds.Y, tabBounds.Right, tabBounds.Bottom - 3 );
                        path.AddArc( tabBounds.Right - 6, tabBounds.Bottom - 6, 6, 6, 0f, 90f );
                        path.AddLine( tabBounds.Right - 3, tabBounds.Bottom, tabBounds.X + tabBounds.Height, tabBounds.Bottom );
                        path.AddLine( tabBounds.X + tabBounds.Height - 4, tabBounds.Bottom - 2, tabBounds.X, tabBounds.Y );
                        break;
                    }

                case TabAlignment.Left:
                    {
                        path.AddLine( tabBounds.Right, tabBounds.Bottom, tabBounds.X + 3, tabBounds.Bottom );
                        path.AddArc( tabBounds.X, tabBounds.Bottom - 6, 6, 6, 90f, 90f );
                        path.AddLine( tabBounds.X, tabBounds.Bottom - 3, tabBounds.X, tabBounds.Y + tabBounds.Width );
                        path.AddLine( tabBounds.X + 2, tabBounds.Y + tabBounds.Width - 4, tabBounds.Right, tabBounds.Y );
                        break;
                    }

                case TabAlignment.Right:
                    {
                        path.AddLine( tabBounds.X, tabBounds.Y, tabBounds.Right - 2, tabBounds.Y + tabBounds.Width - 4 );
                        path.AddLine( tabBounds.Right, tabBounds.Y + tabBounds.Width, tabBounds.Right, tabBounds.Bottom - 3 );
                        path.AddArc( tabBounds.Right - 6, tabBounds.Bottom - 6, 6, 6, 0f, 90f );
                        path.AddLine( tabBounds.Right - 3, tabBounds.Bottom, tabBounds.X, tabBounds.Bottom );
                        break;
                    }
            }
        }
    }
}
