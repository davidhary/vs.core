using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace isr.Core.Controls
{

    /// <summary> A tab style IE 8 provider. </summary>
    /// <remarks>
    /// (c) 2010 The Man from U.N.C.L.E. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-09-26 </para><para>
    /// http://www.codeproject.com/Articles/91387/Painting-Your-Own-Tabs-Second-Edition </para>
    /// </remarks>
    [System.ComponentModel.ToolboxItem( false )]
    public class TabStyleIE8Provider : TabStyleRoundedProvider
    {

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="tabControl"> The tab control. </param>
        public TabStyleIE8Provider( CustomTabControl tabControl ) : base( tabControl, 3, true, Color.Red, new Point( 6, 5 ) )
        {
        }

        /// <summary> Gets tab rectangle. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="index"> Zero-based index of the. </param>
        /// <returns> The tab rectangle. </returns>
        public override Rectangle GetTabRect( int index )
        {
            if ( index < 0 )
            {
                return new Rectangle();
            }

            var tabBounds = base.GetTabRect( index );
            bool firstTabinRow = this.TabControl.IsFirstTabInRow( index );

            // Make non-SelectedTabs smaller and selected tab bigger
            if ( index != this.TabControl.SelectedIndex )
            {
                switch ( this.TabControl.Alignment )
                {
                    case TabAlignment.Top:
                        {
                            tabBounds.Y += 1;
                            tabBounds.Height -= 1;
                            break;
                        }

                    case TabAlignment.Bottom:
                        {
                            tabBounds.Height -= 1;
                            break;
                        }

                    case TabAlignment.Left:
                        {
                            tabBounds.X += 1;
                            tabBounds.Width -= 1;
                            break;
                        }

                    case TabAlignment.Right:
                        {
                            tabBounds.Width -= 1;
                            break;
                        }
                }
            }
            else
            {
                switch ( this.TabControl.Alignment )
                {
                    case TabAlignment.Top:
                        {
                            tabBounds.Y -= 1;
                            tabBounds.Height += 1;
                            if ( firstTabinRow )
                            {
                                tabBounds.Width += 1;
                            }
                            else
                            {
                                tabBounds.X -= 1;
                                tabBounds.Width += 2;
                            }

                            break;
                        }

                    case TabAlignment.Bottom:
                        {
                            tabBounds.Height += 1;
                            if ( firstTabinRow )
                            {
                                tabBounds.Width += 1;
                            }
                            else
                            {
                                tabBounds.X -= 1;
                                tabBounds.Width += 2;
                            }

                            break;
                        }

                    case TabAlignment.Left:
                        {
                            tabBounds.X -= 1;
                            tabBounds.Width += 1;
                            if ( firstTabinRow )
                            {
                                tabBounds.Height += 1;
                            }
                            else
                            {
                                tabBounds.Y -= 1;
                                tabBounds.Height += 2;
                            }

                            break;
                        }

                    case TabAlignment.Right:
                        {
                            tabBounds.Width += 1;
                            if ( firstTabinRow )
                            {
                                tabBounds.Height += 1;
                            }
                            else
                            {
                                tabBounds.Y -= 1;
                                tabBounds.Height += 2;
                            }

                            break;
                        }
                }
            }

            // Adjust first tab in the row to align with tab page
            this.EnsureFirstTabIsInView( ref tabBounds, index );
            return tabBounds;
        }

        /// <summary> Gets tab background brush. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="index"> Zero-based index of the. </param>
        /// <returns> The tab background brush. </returns>
        protected override Brush GetTabBackgroundBrush( int index )
        {
            LinearGradientBrush fillBrush = null;

            // Capture the colors dependent on selection state of the tab
            var dark = Color.FromArgb( 227, 238, 251 );
            var light = Color.FromArgb( 227, 238, 251 );
            if ( this.TabControl.SelectedIndex == index )
            {
                dark = Color.FromArgb( 196, 222, 251 );
                light = SystemColors.Window;
            }
            else if ( !this.TabControl.TabPages[index].Enabled )
            {
                light = dark;
            }
            else if ( this.HotTrack && index == this.TabControl.ActiveIndex )
            {
                // Enable hot tracking
                light = SystemColors.Window;
                dark = Color.FromArgb( 166, 203, 248 );
            }

            // Get the correctly aligned gradient
            var tabBounds = this.GetTabRect( index );
            tabBounds.Inflate( 3, 3 );
            tabBounds.X -= 1;
            tabBounds.Y -= 1;
            switch ( this.TabControl.Alignment )
            {
                case TabAlignment.Top:
                    {
                        fillBrush = new LinearGradientBrush( tabBounds, dark, light, LinearGradientMode.Vertical );
                        break;
                    }

                case TabAlignment.Bottom:
                    {
                        fillBrush = new LinearGradientBrush( tabBounds, light, dark, LinearGradientMode.Vertical );
                        break;
                    }

                case TabAlignment.Left:
                    {
                        fillBrush = new LinearGradientBrush( tabBounds, dark, light, LinearGradientMode.Horizontal );
                        break;
                    }

                case TabAlignment.Right:
                    {
                        fillBrush = new LinearGradientBrush( tabBounds, light, dark, LinearGradientMode.Horizontal );
                        break;
                    }
            }

            // Add the blend
            fillBrush.Blend = this.GetBackgroundBlend( index );
            return fillBrush;
        }

        /// <summary> Gets background blend. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="index"> Zero-based index of the. </param>
        /// <returns> The background blend. </returns>
        private Blend GetBackgroundBlend( int index )
        {
            var relativeIntensities = new float[] { 0f, 0.7f, 1.0f };
            var relativePositions = new float[] { 0f, 0.8f, 1.0f };
            if ( this.TabControl.SelectedIndex != index )
            {
                relativeIntensities = new float[] { 0f, 0.3f, 1.0f };
                relativePositions = new float[] { 0f, 0.2f, 1.0f };
            }

            var blend = new Blend() { Factors = relativeIntensities, Positions = relativePositions };
            return blend;
        }

        /// <summary> Draw tab closer. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="index">    Zero-based index of the. </param>
        /// <param name="graphics"> The graphics. </param>
        protected override void DrawTabCloser( int index, Graphics graphics )
        {
            if ( graphics is null )
            {
                throw new ArgumentNullException( nameof( graphics ) );
            }

            if ( this.ShowTabCloser )
            {
                var closerRect = this.TabControl.GetTabCloserRect( index );
                graphics.SmoothingMode = SmoothingMode.AntiAlias;
                if ( closerRect.Contains( this.TabControl.MousePosition ) )
                {
                    using ( var closerPath = GetCloserButtonPath( closerRect ) )
                    {
                        graphics.FillPath( Brushes.White, closerPath );
                        using var closerPen = new Pen( this.BorderColor );
                        graphics.DrawPath( closerPen, closerPath );
                    }

                    using ( var closerPath = GetCloserPath( closerRect ) )
                    {
                        using var closerPen = new Pen( this.CloserColorActive ) {
                            Width = 2f
                        };
                        graphics.DrawPath( closerPen, closerPath );
                    }
                }
                else
                {
                    using var closerPath = GetCloserPath( closerRect );
                    using var closerPen = new Pen( this.CloserColor ) {
                        Width = 2f
                    };
                    graphics.DrawPath( closerPen, closerPath );
                }
            }
        }

        /// <summary> Gets closer button path. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="closerRect"> The closer rectangle. </param>
        /// <returns> The closer button path. </returns>
        private static GraphicsPath GetCloserButtonPath( Rectangle closerRect )
        {
            var closerPath = new GraphicsPath();
            closerPath.AddLine( closerRect.X - 1, closerRect.Y - 2, closerRect.Right + 1, closerRect.Y - 2 );
            closerPath.AddLine( closerRect.Right + 2, closerRect.Y - 1, closerRect.Right + 2, closerRect.Bottom + 1 );
            closerPath.AddLine( closerRect.Right + 1, closerRect.Bottom + 2, closerRect.X - 1, closerRect.Bottom + 2 );
            closerPath.AddLine( closerRect.X - 2, closerRect.Bottom + 1, closerRect.X - 2, closerRect.Y - 1 );
            closerPath.CloseFigure();
            return closerPath;
        }

        /// <summary> Gets page background brush. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="index"> Zero-based index of the. </param>
        /// <returns> The page background brush. </returns>
        public override Brush GetPageBackgroundBrush( int index )
        {

            // Capture the colors dependent on selection state of the tab
            var light = Color.FromArgb( 227, 238, 251 );
            if ( this.TabControl.SelectedIndex == index )
            {
                light = SystemColors.Window;
            }
            else if ( !this.TabControl.TabPages[index].Enabled )
            {
                light = Color.FromArgb( 207, 207, 207 );
            }
            else if ( this.HotTrack && index == this.TabControl.ActiveIndex )
            {
                // Enable hot tracking
                light = Color.FromArgb( 234, 246, 253 );
            }

            return new SolidBrush( light );
        }
    }
}
