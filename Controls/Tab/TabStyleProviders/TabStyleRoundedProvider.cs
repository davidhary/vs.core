using System;
using System.Drawing;
using System.Windows.Forms;

namespace isr.Core.Controls
{

    /// <summary> A tab style rounded provider. </summary>
    /// <remarks>
    /// (c) 2010 The Man from U.N.C.L.E. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-09-26 </para><para>
    /// http://www.codeproject.com/Articles/91387/Painting-Your-Own-Tabs-Second-Edition </para>
    /// </remarks>
    [System.ComponentModel.ToolboxItem( false )]
    public class TabStyleRoundedProvider : TabStyleProvider
    {

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="tabControl"> The tab control. </param>
        public TabStyleRoundedProvider( CustomTabControl tabControl ) : base( tabControl, 10 )
        {
        }

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="tabControl">        The tab control. </param>
        /// <param name="borderColor">       The border color. </param>
        /// <param name="borderColorHot">    The border color hot. </param>
        /// <param name="textColor">         The text color. </param>
        /// <param name="textColorDisabled"> The text color disabled. </param>
        /// <param name="padding">           The padding. </param>
        /// <param name="radius">            The radius. </param>
        /// <param name="showTabCloser">     True to show, false to hide the tab closer. </param>
        /// <param name="closerColorActive"> The closer color active. </param>
        /// <param name="closerColor">       The closer color. </param>
        public TabStyleRoundedProvider( CustomTabControl tabControl, Color borderColor, Color borderColorHot, Color textColor, Color textColorDisabled, Point padding, int radius, bool showTabCloser, Color closerColorActive, Color closerColor ) : base( tabControl, borderColor, borderColorHot, textColor, textColorDisabled, padding, radius, showTabCloser, closerColorActive, closerColor )
        {
        }

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="tabControl">        The tab control. </param>
        /// <param name="radius">            The radius. </param>
        /// <param name="showTabCloser">     True to show, false to hide the tab closer. </param>
        /// <param name="closerColorActive"> The closer color active. </param>
        /// <param name="padding">           The padding. </param>
        public TabStyleRoundedProvider( CustomTabControl tabControl, int radius, bool showTabCloser, Color closerColorActive, Point padding ) : base( tabControl, radius, showTabCloser, closerColorActive, padding )
        {
        }

        /// <summary> Adds a tab border to 'tabBounds'. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="path">      Full pathname of the file. </param>
        /// <param name="tabBounds"> The tab bounds. </param>
        public override void AddTabBorder( System.Drawing.Drawing2D.GraphicsPath path, Rectangle tabBounds )
        {
            if ( path is null )
            {
                throw new ArgumentNullException( nameof( path ) );
            }

            switch ( this.TabControl.Alignment )
            {
                case TabAlignment.Top:
                    {
                        path.AddLine( tabBounds.X, tabBounds.Bottom, tabBounds.X, tabBounds.Y + this.Radius );
                        path.AddArc( tabBounds.X, tabBounds.Y, this.Radius * 2, this.Radius * 2, 180f, 90f );
                        path.AddLine( tabBounds.X + this.Radius, tabBounds.Y, tabBounds.Right - this.Radius, tabBounds.Y );
                        path.AddArc( tabBounds.Right - this.Radius * 2, tabBounds.Y, this.Radius * 2, this.Radius * 2, 270f, 90f );
                        path.AddLine( tabBounds.Right, tabBounds.Y + this.Radius, tabBounds.Right, tabBounds.Bottom );
                        break;
                    }

                case TabAlignment.Bottom:
                    {
                        path.AddLine( tabBounds.Right, tabBounds.Y, tabBounds.Right, tabBounds.Bottom - this.Radius );
                        path.AddArc( tabBounds.Right - this.Radius * 2, tabBounds.Bottom - this.Radius * 2, this.Radius * 2, this.Radius * 2, 0f, 90f );
                        path.AddLine( tabBounds.Right - this.Radius, tabBounds.Bottom, tabBounds.X + this.Radius, tabBounds.Bottom );
                        path.AddArc( tabBounds.X, tabBounds.Bottom - this.Radius * 2, this.Radius * 2, this.Radius * 2, 90f, 90f );
                        path.AddLine( tabBounds.X, tabBounds.Bottom - this.Radius, tabBounds.X, tabBounds.Y );
                        break;
                    }

                case TabAlignment.Left:
                    {
                        path.AddLine( tabBounds.Right, tabBounds.Bottom, tabBounds.X + this.Radius, tabBounds.Bottom );
                        path.AddArc( tabBounds.X, tabBounds.Bottom - this.Radius * 2, this.Radius * 2, this.Radius * 2, 90f, 90f );
                        path.AddLine( tabBounds.X, tabBounds.Bottom - this.Radius, tabBounds.X, tabBounds.Y + this.Radius );
                        path.AddArc( tabBounds.X, tabBounds.Y, this.Radius * 2, this.Radius * 2, 180f, 90f );
                        path.AddLine( tabBounds.X + this.Radius, tabBounds.Y, tabBounds.Right, tabBounds.Y );
                        break;
                    }

                case TabAlignment.Right:
                    {
                        path.AddLine( tabBounds.X, tabBounds.Y, tabBounds.Right - this.Radius, tabBounds.Y );
                        path.AddArc( tabBounds.Right - this.Radius * 2, tabBounds.Y, this.Radius * 2, this.Radius * 2, 270f, 90f );
                        path.AddLine( tabBounds.Right, tabBounds.Y + this.Radius, tabBounds.Right, tabBounds.Bottom - this.Radius );
                        path.AddArc( tabBounds.Right - this.Radius * 2, tabBounds.Bottom - this.Radius * 2, this.Radius * 2, this.Radius * 2, 0f, 90f );
                        path.AddLine( tabBounds.Right - this.Radius, tabBounds.Bottom, tabBounds.X, tabBounds.Bottom );
                        break;
                    }
            }
        }
    }
}
