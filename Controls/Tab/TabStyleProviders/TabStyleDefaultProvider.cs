using System;
using System.Drawing;
using System.Windows.Forms;

namespace isr.Core.Controls
{

    /// <summary> A tab style default provider. </summary>
    /// <remarks>
    /// (c) 2010 The Man from U.N.C.L.E. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-09-26 </para><para>
    /// http://www.codeproject.com/Articles/91387/Painting-Your-Own-Tabs-Second-Edition </para>
    /// </remarks>
    [System.ComponentModel.ToolboxItem( false )]
    public class TabStyleDefaultProvider : TabStyleProvider
    {

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="tabControl"> The tab control. </param>
        public TabStyleDefaultProvider( CustomTabControl tabControl ) : base( tabControl, 2, true )
        {
        }

        /// <summary> Adds a tab border to 'tabBounds'. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="path">      Full pathname of the file. </param>
        /// <param name="tabBounds"> The tab bounds. </param>
        public override void AddTabBorder( System.Drawing.Drawing2D.GraphicsPath path, Rectangle tabBounds )
        {
            if ( path is null )
            {
                throw new ArgumentNullException( nameof( path ) );
            }

            switch ( this.TabControl.Alignment )
            {
                case TabAlignment.Top:
                    {
                        path.AddLine( tabBounds.X, tabBounds.Bottom, tabBounds.X, tabBounds.Y );
                        path.AddLine( tabBounds.X, tabBounds.Y, tabBounds.Right, tabBounds.Y );
                        path.AddLine( tabBounds.Right, tabBounds.Y, tabBounds.Right, tabBounds.Bottom );
                        break;
                    }

                case TabAlignment.Bottom:
                    {
                        path.AddLine( tabBounds.Right, tabBounds.Y, tabBounds.Right, tabBounds.Bottom );
                        path.AddLine( tabBounds.Right, tabBounds.Bottom, tabBounds.X, tabBounds.Bottom );
                        path.AddLine( tabBounds.X, tabBounds.Bottom, tabBounds.X, tabBounds.Y );
                        break;
                    }

                case TabAlignment.Left:
                    {
                        path.AddLine( tabBounds.Right, tabBounds.Bottom, tabBounds.X, tabBounds.Bottom );
                        path.AddLine( tabBounds.X, tabBounds.Bottom, tabBounds.X, tabBounds.Y );
                        path.AddLine( tabBounds.X, tabBounds.Y, tabBounds.Right, tabBounds.Y );
                        break;
                    }

                case TabAlignment.Right:
                    {
                        path.AddLine( tabBounds.X, tabBounds.Y, tabBounds.Right, tabBounds.Y );
                        path.AddLine( tabBounds.Right, tabBounds.Y, tabBounds.Right, tabBounds.Bottom );
                        path.AddLine( tabBounds.Right, tabBounds.Bottom, tabBounds.X, tabBounds.Bottom );
                        break;
                    }
            }
        }

        /// <summary> Gets tab rectangle. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="index"> Zero-based index of the. </param>
        /// <returns> The tab rectangle. </returns>
        public override Rectangle GetTabRect( int index )
        {
            if ( index < 0 )
            {
                return new Rectangle();
            }

            var tabBounds = base.GetTabRect( index );
            bool firstTabinRow = this.TabControl.IsFirstTabInRow( index );

            // Make non-SelectedTabs smaller and selected tab bigger
            if ( index != this.TabControl.SelectedIndex )
            {
                switch ( this.TabControl.Alignment )
                {
                    case TabAlignment.Top:
                        {
                            tabBounds.Y += 1;
                            tabBounds.Height -= 1;
                            break;
                        }

                    case TabAlignment.Bottom:
                        {
                            tabBounds.Height -= 1;
                            break;
                        }

                    case TabAlignment.Left:
                        {
                            tabBounds.X += 1;
                            tabBounds.Width -= 1;
                            break;
                        }

                    case TabAlignment.Right:
                        {
                            tabBounds.Width -= 1;
                            break;
                        }
                }
            }
            else
            {
                switch ( this.TabControl.Alignment )
                {
                    case TabAlignment.Top:
                        {
                            if ( tabBounds.Y > 0 )
                            {
                                tabBounds.Y -= 1;
                                tabBounds.Height += 1;
                            }

                            if ( firstTabinRow )
                            {
                                tabBounds.Width += 1;
                            }
                            else
                            {
                                tabBounds.X -= 1;
                                tabBounds.Width += 2;
                            }

                            break;
                        }

                    case TabAlignment.Bottom:
                        {
                            if ( tabBounds.Bottom < this.TabControl.Bottom )
                            {
                                tabBounds.Height += 1;
                            }

                            if ( firstTabinRow )
                            {
                                tabBounds.Width += 1;
                            }
                            else
                            {
                                tabBounds.X -= 1;
                                tabBounds.Width += 2;
                            }

                            break;
                        }

                    case TabAlignment.Left:
                        {
                            if ( tabBounds.X > 0 )
                            {
                                tabBounds.X -= 1;
                                tabBounds.Width += 1;
                            }

                            if ( firstTabinRow )
                            {
                                tabBounds.Height += 1;
                            }
                            else
                            {
                                tabBounds.Y -= 1;
                                tabBounds.Height += 2;
                            }

                            break;
                        }

                    case TabAlignment.Right:
                        {
                            if ( tabBounds.Right < this.TabControl.Right )
                            {
                                tabBounds.Width += 1;
                            }

                            if ( firstTabinRow )
                            {
                                tabBounds.Height += 1;
                            }
                            else
                            {
                                tabBounds.Y -= 1;
                                tabBounds.Height += 2;
                            }

                            break;
                        }
                }
            }

            // Adjust first tab in the row to align with tab page
            this.EnsureFirstTabIsInView( ref tabBounds, index );
            return tabBounds;
        }
    }
}
