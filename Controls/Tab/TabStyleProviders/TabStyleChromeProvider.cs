using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace isr.Core.Controls
{

    /// <summary> A tab style chrome provider. </summary>
    /// <remarks>
    /// (c) 2010 The Man from U.N.C.L.E. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-09-26 </para><para>
    /// http://www.codeproject.com/Articles/91387/Painting-Your-Own-Tabs-Second-Edition </para>
    /// </remarks>
    [System.ComponentModel.ToolboxItem( false )]
    public class TabStyleChromeProvider : TabStyleProvider
    {

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="tabControl"> The tab control. </param>
        public TabStyleChromeProvider( CustomTabControl tabControl ) : base( tabControl, 16, 16, true, Color.White, new Point( 7, 5 ) )
        {
        }

        /// <summary> Adds a tab border to 'tabBounds'. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="path">      Full pathname of the file. </param>
        /// <param name="tabBounds"> The tab bounds. </param>
        public override void AddTabBorder( GraphicsPath path, Rectangle tabBounds )
        {
            if ( path is null )
            {
                throw new ArgumentNullException( nameof( path ) );
            }

            int spread;
            int eigth;
            int sixth;
            int quarter;
            if ( this.TabControl.Alignment <= TabAlignment.Bottom )
            {
                spread = ( int ) Math.Truncate( Math.Floor( tabBounds.Height * 2m / 3m ) );
                eigth = ( int ) Math.Truncate( Math.Floor( tabBounds.Height * 1m / 8m ) );
                sixth = ( int ) Math.Truncate( Math.Floor( tabBounds.Height * 1m / 6m ) );
                quarter = ( int ) Math.Truncate( Math.Floor( tabBounds.Height * 1m / 4m ) );
            }
            else
            {
                spread = ( int ) Math.Truncate( Math.Floor( tabBounds.Width * 2m / 3m ) );
                eigth = ( int ) Math.Truncate( Math.Floor( tabBounds.Width * 1m / 8m ) );
                sixth = ( int ) Math.Truncate( Math.Floor( tabBounds.Width * 1m / 6m ) );
                quarter = ( int ) Math.Truncate( Math.Floor( tabBounds.Width * 1m / 4m ) );
            }

            switch ( this.TabControl.Alignment )
            {
                case TabAlignment.Top:
                    {
                        path.AddCurve( new Point[] { new Point( tabBounds.X, tabBounds.Bottom ), new Point( tabBounds.X + sixth, tabBounds.Bottom - eigth ), new Point( tabBounds.X + spread - quarter, tabBounds.Y + eigth ), new Point( tabBounds.X + spread, tabBounds.Y ) } );
                        path.AddLine( tabBounds.X + spread, tabBounds.Y, tabBounds.Right - spread, tabBounds.Y );
                        path.AddCurve( new Point[] { new Point( tabBounds.Right - spread, tabBounds.Y ), new Point( tabBounds.Right - spread + quarter, tabBounds.Y + eigth ), new Point( tabBounds.Right - sixth, tabBounds.Bottom - eigth ), new Point( tabBounds.Right, tabBounds.Bottom ) } );
                        break;
                    }

                case TabAlignment.Bottom:
                    {
                        path.AddCurve( new Point[] { new Point( tabBounds.Right, tabBounds.Y ), new Point( tabBounds.Right - sixth, tabBounds.Y + eigth ), new Point( tabBounds.Right - spread + quarter, tabBounds.Bottom - eigth ), new Point( tabBounds.Right - spread, tabBounds.Bottom ) } );
                        path.AddLine( tabBounds.Right - spread, tabBounds.Bottom, tabBounds.X + spread, tabBounds.Bottom );
                        path.AddCurve( new Point[] { new Point( tabBounds.X + spread, tabBounds.Bottom ), new Point( tabBounds.X + spread - quarter, tabBounds.Bottom - eigth ), new Point( tabBounds.X + sixth, tabBounds.Y + eigth ), new Point( tabBounds.X, tabBounds.Y ) } );
                        break;
                    }

                case TabAlignment.Left:
                    {
                        path.AddCurve( new Point[] { new Point( tabBounds.Right, tabBounds.Bottom ), new Point( tabBounds.Right - eigth, tabBounds.Bottom - sixth ), new Point( tabBounds.X + eigth, tabBounds.Bottom - spread + quarter ), new Point( tabBounds.X, tabBounds.Bottom - spread ) } );
                        path.AddLine( tabBounds.X, tabBounds.Bottom - spread, tabBounds.X, tabBounds.Y + spread );
                        path.AddCurve( new Point[] { new Point( tabBounds.X, tabBounds.Y + spread ), new Point( tabBounds.X + eigth, tabBounds.Y + spread - quarter ), new Point( tabBounds.Right - eigth, tabBounds.Y + sixth ), new Point( tabBounds.Right, tabBounds.Y ) } );
                        break;
                    }

                case TabAlignment.Right:
                    {
                        path.AddCurve( new Point[] { new Point( tabBounds.X, tabBounds.Y ), new Point( tabBounds.X + eigth, tabBounds.Y + sixth ), new Point( tabBounds.Right - eigth, tabBounds.Y + spread - quarter ), new Point( tabBounds.Right, tabBounds.Y + spread ) } );
                        path.AddLine( tabBounds.Right, tabBounds.Y + spread, tabBounds.Right, tabBounds.Bottom - spread );
                        path.AddCurve( new Point[] { new Point( tabBounds.Right, tabBounds.Bottom - spread ), new Point( tabBounds.Right - eigth, tabBounds.Bottom - spread + quarter ), new Point( tabBounds.X + eigth, tabBounds.Bottom - sixth ), new Point( tabBounds.X, tabBounds.Bottom ) } );
                        break;
                    }
            }
        }

        /// <summary> Draw tab closer. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="index">    Zero-based index of the. </param>
        /// <param name="graphics"> The graphics. </param>
        protected override void DrawTabCloser( int index, Graphics graphics )
        {
            if ( graphics is null )
            {
                throw new ArgumentNullException( nameof( graphics ) );
            }

            if ( this.ShowTabCloser )
            {
                var closerRect = this.TabControl.GetTabCloserRect( index );
                graphics.SmoothingMode = SmoothingMode.AntiAlias;
                if ( closerRect.Contains( this.TabControl.MousePosition ) )
                {
                    using ( var closerPath = GetCloserButtonPath( closerRect ) )
                    {
                        using var closerBrush = new SolidBrush( Color.FromArgb( 193, 53, 53 ) );
                        graphics.FillPath( closerBrush, closerPath );
                    }

                    using ( var closerPath = GetCloserPath( closerRect ) )
                    {
                        using var closerPen = new Pen( this.CloserColorActive );
                        graphics.DrawPath( closerPen, closerPath );
                    }
                }
                else
                {
                    using var closerPath = GetCloserPath( closerRect );
                    using var closerPen = new Pen( this.CloserColor );
                    graphics.DrawPath( closerPen, closerPath );
                }
            }
        }

        /// <summary> Gets closer button path. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="closerRect"> The closer rectangle. </param>
        /// <returns> The closer button path. </returns>
        private static GraphicsPath GetCloserButtonPath( Rectangle closerRect )
        {
            var closerPath = new GraphicsPath();
            closerPath.AddEllipse( new Rectangle( closerRect.X - 2, closerRect.Y - 2, closerRect.Width + 4, closerRect.Height + 4 ) );
            closerPath.CloseFigure();
            return closerPath;
        }
    }
}
