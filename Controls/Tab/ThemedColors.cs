using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace isr.Core.Controls
{

    /// <summary> A themed colors. </summary>
    /// <remarks> (c) 2010 The Man from U.N.C.L.E. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-09-26,Created.
    /// http://www.codeproject.com/Articles/91387/Painting-Your-Own-Tabs-Second-Edition </para></remarks>
    internal sealed class ThemedColors
    {

        #region "   Variables and Constants "

        /// <summary> The normal color. </summary>
        private const string _NormalColor = "NormalColor";

        /// <summary> The home stead. </summary>
        private const string _HomeStead = "HomeStead";

        /// <summary> The metallic. </summary>
        private const string _Metallic = "Metallic";
        // Private Constant NoTheme As String = "NoTheme"

        #endregion

        #region "   Properties "

        /// <summary> Gets the current theme index. </summary>
        /// <value> The current theme index. </value>
        public static ColorScheme CurrentThemeIndex => GetCurrentThemeIndex();

        /// <summary> The tool border. </summary>
#pragma warning disable IDE1006 // Naming Styles
        private static readonly Color[] _ToolBorder = new Color[] { Color.FromArgb( 127, 157, 185 ), Color.FromArgb( 164, 185, 127 ), Color.FromArgb( 165, 172, 178 ), Color.FromArgb( 132, 130, 132 ) };
#pragma warning restore IDE1006 // Naming Styles

        /// <summary> Gets the tool border. </summary>
        /// <value> The tool border. </value>
        public static Color ToolBorder => _ToolBorder[( int ) CurrentThemeIndex];

        #endregion

        #region "   Constructors "

        /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private ThemedColors()
        {
        }

        #endregion

        /// <summary> Gets the zero-based index of the current theme. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> The current theme index. </returns>
        private static ColorScheme GetCurrentThemeIndex()
        {
            var theme = ColorScheme.NoTheme;
            if ( VisualStyleInformation.IsSupportedByOS && VisualStyleInformation.IsEnabledByUser && Application.RenderWithVisualStyles )
            {
                switch ( VisualStyleInformation.ColorScheme ?? "" )
                {
                    case _NormalColor:
                        {
                            theme = ColorScheme.NormalColor;
                            break;
                        }

                    case _HomeStead:
                        {
                            theme = ColorScheme.HomeStead;
                            break;
                        }

                    case _Metallic:
                        {
                            theme = ColorScheme.Metallic;
                            break;
                        }

                    default:
                        {
                            theme = ColorScheme.NoTheme;
                            break;
                        }
                }
            }

            return theme;
        }

        /// <summary> Values that represent color schemes. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public enum ColorScheme
        {
            /// <summary> An enum constant representing the normal color option. </summary>
            NormalColor = 0,

            /// <summary> An enum constant representing the home stead option. </summary>
            HomeStead = 1,

            /// <summary> An enum constant representing the metallic option. </summary>
            Metallic = 2,

            /// <summary> An enum constant representing the no theme option. </summary>
            NoTheme = 3
        }
    }
}
