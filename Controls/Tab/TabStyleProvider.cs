using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace isr.Core.Controls
{

    /// <summary> A tab style provider. </summary>
    /// <remarks>
    /// (c) 2010 The Man from U.N.C.L.E. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-09-26 </para><para>
    /// http://www.codeproject.com/Articles/91387/Painting-Your-Own-Tabs-Second-Edition </para>
    /// </remarks>
    [ToolboxItem( false )]
    public abstract class TabStyleProvider : Component
    {

        #region " Constructor"

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="tabControl">          The tab control. </param>
        /// <param name="imageAlignment">      The image alignment. </param>
        /// <param name="borderColor">         The border color. </param>
        /// <param name="borderColorSelected"> The border color selected. </param>
        /// <param name="borderColorHot">      The border color hot. </param>
        /// <param name="focusColor">          The focus color. </param>
        /// <param name="textColor">           The text color. </param>
        /// <param name="textColorDisabled">   The text color disabled. </param>
        /// <param name="hotTrack">            True to hot track. </param>
        /// <param name="padding">             The padding. </param>
        /// <param name="radius">              The radius. </param>
        /// <param name="showTabCloser">       True to show, false to hide the tab closer. </param>
        /// <param name="closerColorActive">   The closer color active. </param>
        /// <param name="closerColor">         The closer color. </param>
        protected TabStyleProvider( CustomTabControl tabControl, ContentAlignment imageAlignment, Color borderColor, Color borderColorSelected, Color borderColorHot, Color focusColor, Color textColor, Color textColorDisabled, bool hotTrack, Point padding, int radius, bool showTabCloser, Color closerColorActive, Color closerColor ) : base()
        {
            this.TabControl = tabControl;
            this._BorderColor = borderColor;
            this._BorderColorSelected = borderColorSelected;
            this._BorderColorHot = borderColorHot;
            this._FocusColor = focusColor;
            this._ImageAlign = imageAlignment;
            this._HotTrack = hotTrack;
            this._Radius = radius;
            this._ShowTabCloser = showTabCloser;
            this._CloserColorActive = closerColorActive;
            this._CloserColor = closerColor;
            this._TextColor = textColor;
            this._TextColorDisabled = textColorDisabled;
            this._Opacity = 1f;
            this._Padding = padding;
            this.AdjustPadding( padding );
        }

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="tabControl">          The tab control. </param>
        /// <param name="borderColor">         The border color. </param>
        /// <param name="borderColorSelected"> The border color selected. </param>
        /// <param name="borderColorHot">      The border color hot. </param>
        /// <param name="focusColor">          The focus color. </param>
        /// <param name="textColor">           The text color. </param>
        /// <param name="textColorDisabled">   The text color disabled. </param>
        /// <param name="hotTrack">            True to hot track. </param>
        /// <param name="padding">             The padding. </param>
        /// <param name="radius">              The radius. </param>
        /// <param name="showTabCloser">       True to show, false to hide the tab closer. </param>
        /// <param name="closerColorActive">   The closer color active. </param>
        /// <param name="closerColor">         The closer color. </param>
        protected TabStyleProvider( CustomTabControl tabControl, Color borderColor, Color borderColorSelected, Color borderColorHot, Color focusColor, Color textColor, Color textColorDisabled, bool hotTrack, Point padding, int radius, bool showTabCloser, Color closerColorActive, Color closerColor ) : this( tabControl, tabControl?.RightToLeftLayout == true ? ContentAlignment.MiddleRight : ContentAlignment.MiddleLeft, borderColor, borderColorSelected, borderColorHot, focusColor, textColor, textColorDisabled, hotTrack, padding, radius, showTabCloser, closerColorActive, closerColor )
        {
        }

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="tabControl">        The tab control. </param>
        /// <param name="borderColor">       The border color. </param>
        /// <param name="borderColorHot">    The border color hot. </param>
        /// <param name="textColor">         The text color. </param>
        /// <param name="textColorDisabled"> The text color disabled. </param>
        /// <param name="padding">           The padding. </param>
        /// <param name="radius">            The radius. </param>
        /// <param name="showTabCloser">     True to show, false to hide the tab closer. </param>
        /// <param name="closerColorActive"> The closer color active. </param>
        /// <param name="closerColor">       The closer color. </param>
        protected TabStyleProvider( CustomTabControl tabControl, Color borderColor, Color borderColorHot, Color textColor, Color textColorDisabled, Point padding, int radius, bool showTabCloser, Color closerColorActive, Color closerColor ) : this( tabControl, tabControl?.RightToLeftLayout == true ? ContentAlignment.MiddleRight : ContentAlignment.MiddleLeft, borderColor, Color.Empty, borderColorHot, Color.Orange, textColor, textColorDisabled, true, padding, radius, showTabCloser, closerColorActive, closerColor )
        {
        }

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="tabControl">     The tab control. </param>
        /// <param name="imageAlignment"> The image alignment. </param>
        protected TabStyleProvider( CustomTabControl tabControl, ContentAlignment imageAlignment ) : base()
        {
            this.TabControl = tabControl;
            this._BorderColor = Color.Empty;
            this._BorderColorSelected = Color.Empty;
            this._BorderColorHot = Color.Empty;
            this._FocusColor = Color.Orange;
            this._ImageAlign = imageAlignment;
            this._HotTrack = true;
            this._Radius = 1;
            this._CloserColorActive = Color.Black;
            this._CloserColor = Color.DarkGray;
            this._TextColor = Color.Empty;
            this._TextColorDisabled = Color.Empty;
            this._Opacity = 1f;
            this._Padding = new Point( 6, 3 );
            this.AdjustPadding( this.Padding );
        }

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="tabControl"> The tab control. </param>
        protected TabStyleProvider( CustomTabControl tabControl ) : this( tabControl, tabControl?.RightToLeftLayout == true ? ContentAlignment.MiddleRight : ContentAlignment.MiddleLeft )
        {
        }

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="tabControl"> The tab control. </param>
        /// <param name="radius">     The radius. </param>
        /// <param name="focusTrack"> True to focus track. </param>
        protected TabStyleProvider( CustomTabControl tabControl, int radius, bool focusTrack ) : this( tabControl, tabControl?.RightToLeftLayout == true ? ContentAlignment.MiddleRight : ContentAlignment.MiddleLeft )
        {
            this._Radius = radius;
            this._FocusTrack = focusTrack;
            this._Padding = new Point( 6, 3 );
            this.AdjustPadding( this.Padding );
        }

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="tabControl"> The tab control. </param>
        /// <param name="radius">     The radius. </param>
        protected TabStyleProvider( CustomTabControl tabControl, int radius ) : this( tabControl, tabControl?.RightToLeftLayout == true ? ContentAlignment.MiddleRight : ContentAlignment.MiddleLeft )
        {
            this._Radius = radius;
            this._Padding = new Point( 6, 3 );
            this.AdjustPadding( this.Padding );
        }

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="tabControl">        The tab control. </param>
        /// <param name="radius">            The radius. </param>
        /// <param name="showTabCloser">     True to show, false to hide the tab closer. </param>
        /// <param name="closerColorActive"> The closer color active. </param>
        /// <param name="padding">           The padding. </param>
        protected TabStyleProvider( CustomTabControl tabControl, int radius, bool showTabCloser, Color closerColorActive, Point padding ) : this( tabControl, tabControl?.RightToLeftLayout == true ? ContentAlignment.MiddleRight : ContentAlignment.MiddleLeft )
        {
            this._ShowTabCloser = showTabCloser;
            this._CloserColorActive = closerColorActive;
            this._Radius = radius;
            this._Padding = padding;
            this.AdjustPadding( padding );
        }

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="tabControl">        The tab control. </param>
        /// <param name="radius">            The radius. </param>
        /// <param name="overlap">           The overlap. </param>
        /// <param name="showTabCloser">     True to show, false to hide the tab closer. </param>
        /// <param name="closerColorActive"> The closer color active. </param>
        /// <param name="padding">           The padding. </param>
        protected TabStyleProvider( CustomTabControl tabControl, int radius, int overlap, bool showTabCloser, Color closerColorActive, Point padding ) : this( tabControl, tabControl?.RightToLeftLayout == true ? ContentAlignment.MiddleRight : ContentAlignment.MiddleLeft )
        {
            this._ShowTabCloser = showTabCloser;
            this._CloserColorActive = closerColorActive;
            this._Overlap = overlap;
            this._Radius = radius;
            this._Padding = padding;
            this.AdjustPadding( padding );
        }

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="tabControl">     The tab control. </param>
        /// <param name="imageAlignment"> The image alignment. </param>
        /// <param name="radius">         The radius. </param>
        /// <param name="overlap">        The overlap. </param>
        /// <param name="padding">        The padding. </param>
        protected TabStyleProvider( CustomTabControl tabControl, ContentAlignment imageAlignment, int radius, int overlap, Point padding ) : this( tabControl, imageAlignment )
        {
            this._Overlap = overlap;
            this._Radius = radius;
            this._Padding = padding;
            this.AdjustPadding( padding );
        }

        #endregion

        #region " Factory Methods"

        /// <summary> Creates a provider. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="tabControl"> The tab control. </param>
        /// <returns> The new provider. </returns>
        public static TabStyleProvider CreateProvider( CustomTabControl tabControl )
        {
            if ( tabControl is null )
            {
                throw new ArgumentNullException( nameof( tabControl ) );
            }

            TabStyleProvider provider;

            // Depending on the display style of the tabControl generate an appropriate provider.
            switch ( tabControl.DisplayStyle )
            {
                case TabStyle.None:
                    {
                        provider = new TabStyleNoneProvider( tabControl );
                        break;
                    }

                case TabStyle.Default:
                    {
                        provider = new TabStyleDefaultProvider( tabControl );
                        break;
                    }

                case TabStyle.Angled:
                    {
                        provider = new TabStyleAngledProvider( tabControl );
                        break;
                    }

                case TabStyle.Rounded:
                    {
                        provider = new TabStyleRoundedProvider( tabControl );
                        break;
                    }

                case TabStyle.VisualStudio:
                    {
                        provider = new TabStyleVisualStudioProvider( tabControl );
                        break;
                    }

                case TabStyle.Chrome:
                    {
                        provider = new TabStyleChromeProvider( tabControl );
                        break;
                    }

                case TabStyle.IE8:
                    {
                        provider = new TabStyleIE8Provider( tabControl );
                        break;
                    }

                case TabStyle.VS2010:
                    {
                        provider = new TabStyleVS2010Provider( tabControl );
                        break;
                    }

                default:
                    {
                        provider = new TabStyleDefaultProvider( tabControl );
                        break;
                    }
            }

            provider.DisplayStyle = tabControl.DisplayStyle;
            return provider;
        }

        #endregion

        #region " overridable Methods"

        /// <summary> Adds a tab border to 'tabBounds'. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="path">      Full pathname of the file. </param>
        /// <param name="tabBounds"> The tab bounds. </param>
        public abstract void AddTabBorder( GraphicsPath path, Rectangle tabBounds );

        /// <summary> Gets tab rectangle. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="index"> Zero-based index of the. </param>
        /// <returns> The tab rectangle. </returns>
        public virtual Rectangle GetTabRect( int index )
        {
            if ( index < 0 )
            {
                return new Rectangle();
            }

            var tabBounds = this.TabControl.GetTabRect( index );
            if ( this.TabControl.RightToLeftLayout )
            {
                tabBounds.X = this.TabControl.Width - tabBounds.Right;
            }

            bool firstTabinRow = this.TabControl.IsFirstTabInRow( index );

            // Expand to overlap the tab page
            switch ( this.TabControl.Alignment )
            {
                case TabAlignment.Top:
                    {
                        tabBounds.Height += 2;
                        break;
                    }

                case TabAlignment.Bottom:
                    {
                        tabBounds.Height += 2;
                        tabBounds.Y -= 2;
                        break;
                    }

                case TabAlignment.Left:
                    {
                        tabBounds.Width += 2;
                        break;
                    }

                case TabAlignment.Right:
                    {
                        tabBounds.X -= 2;
                        tabBounds.Width += 2;
                        break;
                    }
            }


            // Create Overlap unless first tab in the row to align with tab page
            if ( (!firstTabinRow || this.TabControl.RightToLeftLayout) && this.Overlap > 0 )
            {
                if ( this.TabControl.Alignment <= TabAlignment.Bottom )
                {
                    tabBounds.X -= this.Overlap;
                    tabBounds.Width += this.Overlap;
                }
                else
                {
                    tabBounds.Y -= this.Overlap;
                    tabBounds.Height += this.Overlap;
                }
            }

            // Adjust first tab in the row to align with tab page
            this.EnsureFirstTabIsInView( ref tabBounds, index );
            return tabBounds;
        }

        /// <summary> Ensures that first tab is in view. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="tabBounds"> [in,out] The tab bounds. </param>
        /// <param name="index">     Zero-based index of the. </param>
        protected virtual void EnsureFirstTabIsInView( ref Rectangle tabBounds, int index )
        {
            // Adjust first tab in the row to align with tab page
            // Make sure we only reposition visible tabs, as we may have scrolled out of view.

            bool firstTabinRow = this.TabControl.IsFirstTabInRow( index );
            if ( firstTabinRow )
            {
                if ( this.TabControl.Alignment <= TabAlignment.Bottom )
                {
                    if ( this.TabControl.RightToLeftLayout )
                    {
                        if ( tabBounds.Left < this.TabControl.Right )
                        {
                            int tabPageRight = this.TabControl.GetPageBounds( index ).Right;
                            if ( tabBounds.Right > tabPageRight )
                            {
                                tabBounds.Width -= tabBounds.Right - tabPageRight;
                            }
                        }
                    }
                    else if ( tabBounds.Right > 0 )
                    {
                        int tabPageX = this.TabControl.GetPageBounds( index ).X;
                        if ( tabBounds.X < tabPageX )
                        {
                            tabBounds.Width -= tabPageX - tabBounds.X;
                            tabBounds.X = tabPageX;
                        }
                    }
                }
                else if ( this.TabControl.RightToLeftLayout )
                {
                    if ( tabBounds.Top < this.TabControl.Bottom )
                    {
                        int tabPageBottom = this.TabControl.GetPageBounds( index ).Bottom;
                        if ( tabBounds.Bottom > tabPageBottom )
                        {
                            tabBounds.Height -= tabBounds.Bottom - tabPageBottom;
                        }
                    }
                }
                else if ( tabBounds.Bottom > 0 )
                {
                    int tabPageY = this.TabControl.GetPageBounds( index ).Location.Y;
                    if ( tabBounds.Y < tabPageY )
                    {
                        tabBounds.Height -= tabPageY - tabBounds.Y;
                        tabBounds.Y = tabPageY;
                    }
                }
            }
        }

        /// <summary> Gets tab background brush. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="index"> Zero-based index of the. </param>
        /// <returns> The tab background brush. </returns>
        protected virtual Brush GetTabBackgroundBrush( int index )
        {
            LinearGradientBrush fillBrush = null;

            // Capture the colors dependent on selection state of the tab
            var dark = Color.FromArgb( 207, 207, 207 );
            var light = Color.FromArgb( 242, 242, 242 );
            if ( this.TabControl.SelectedIndex == index )
            {
                dark = SystemColors.ControlLight;
                light = SystemColors.Window;
            }
            else if ( !this.TabControl.TabPages[index].Enabled )
            {
                light = dark;
            }
            else if ( this.HotTrack && index == this.TabControl.ActiveIndex )
            {
                // Enable hot tracking
                light = Color.FromArgb( 234, 246, 253 );
                dark = Color.FromArgb( 167, 217, 245 );
            }

            // Get the correctly aligned gradient
            var tabBounds = this.GetTabRect( index );
            tabBounds.Inflate( 3, 3 );
            tabBounds.X -= 1;
            tabBounds.Y -= 1;
            switch ( this.TabControl.Alignment )
            {
                case TabAlignment.Top:
                    {
                        if ( this.TabControl.SelectedIndex == index )
                        {
                            dark = light;
                        }

                        fillBrush = new LinearGradientBrush( tabBounds, light, dark, LinearGradientMode.Vertical );
                        break;
                    }

                case TabAlignment.Bottom:
                    {
                        fillBrush = new LinearGradientBrush( tabBounds, light, dark, LinearGradientMode.Vertical );
                        break;
                    }

                case TabAlignment.Left:
                    {
                        fillBrush = new LinearGradientBrush( tabBounds, dark, light, LinearGradientMode.Horizontal );
                        break;
                    }

                case TabAlignment.Right:
                    {
                        fillBrush = new LinearGradientBrush( tabBounds, light, dark, LinearGradientMode.Horizontal );
                        break;
                    }
            }

            // Add the blend
            fillBrush.Blend = this.GetBackgroundBlend();
            return fillBrush;
        }

        #endregion

        #region " Base Properties"

        /// <summary> Gets or sets the tab control. </summary>
        /// <value> The tab control. </value>
        protected CustomTabControl TabControl { get; set; }

        /// <summary> Gets or sets the display style. </summary>
        /// <value> The display style. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public TabStyle DisplayStyle { get; set; }

        /// <summary> The image align. </summary>
        private ContentAlignment _ImageAlign;

        /// <summary> Gets or sets the image align. </summary>
        /// <value> The image align. </value>
        [Category( "Appearance" )]
        public ContentAlignment ImageAlign
        {
            get => this._ImageAlign;

            set {
                this._ImageAlign = value;
                this.TabControl.Invalidate();
            }
        }

        /// <summary> The padding. </summary>
        private Point _Padding;

        /// <summary> Adjust padding. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The value. </param>
        private void AdjustPadding( Point value )
        {
            // This line will trigger the handle to recreate, therefore invalidating the control
            (( TabControl ) this.TabControl).Padding = this.ShowTabCloser ? value.X + this.Radius / 2 < -6 ? new Point( 0, value.Y ) : new Point( value.X + this.Radius / 2 + 6, value.Y ) : value.X + this.Radius / 2 < 1 ? new Point( 0, value.Y ) : new Point( value.X + this.Radius / 2 - 1, value.Y );
        }

        /// <summary> Gets or sets the padding. </summary>
        /// <value> The padding. </value>
        [Category( "Appearance" )]
        public Point Padding
        {
            get => this._Padding;

            set {
                this._Padding = value;
                this.AdjustPadding( value );
            }
        }

        /// <summary> The radius. </summary>

        private int _Radius;

        /// <summary> Gets or sets the radius. </summary>
        /// <value> The radius. </value>
        [Category( "Appearance" )]
        [DefaultValue( 1 )]
        [Browsable( true )]
        public int Radius
        {
            get => this._Radius;

            set {
                if ( value < 1 )
                {
                    value = 1;
                }

                this._Radius = value;
                this.AdjustPadding( this._Padding );
            }
        }

        /// <summary> The overlap. </summary>
        private int _Overlap;

        /// <summary> Gets or sets the overlap. </summary>
        /// <value> The overlap. </value>
        [Category( "Appearance" )]
        public int Overlap
        {
            get => this._Overlap;

            set {
                if ( value < 0 )
                {
                    value = 0;
                }

                this._Overlap = value;
            }
        }

        /// <summary> True to focus track. </summary>
        private bool _FocusTrack;

        /// <summary> Gets or sets the focus track. </summary>
        /// <value> The focus track. </value>
        [Category( "Appearance" )]
        public bool FocusTrack
        {
            get => this._FocusTrack;

            set {
                this._FocusTrack = value;
                this.TabControl.Invalidate();
            }
        }

        /// <summary> True to hot track. </summary>
        private bool _HotTrack;

        /// <summary> Gets or sets the hot track. </summary>
        /// <value> The hot track. </value>
        [Category( "Appearance" )]
        public bool HotTrack
        {
            get => this._HotTrack;

            set {
                this._HotTrack = value;
                (( TabControl ) this.TabControl).HotTrack = value;
            }
        }

        /// <summary> True to show, false to hide the tab closer. </summary>
        private bool _ShowTabCloser;

        /// <summary> Gets or sets the show tab closer. </summary>
        /// <value> The show tab closer. </value>
        [Category( "Appearance" )]
        public bool ShowTabCloser
        {
            get => this._ShowTabCloser;

            set {
                this._ShowTabCloser = value;
                this.AdjustPadding( this._Padding );
            }
        }

        /// <summary> The opacity. </summary>
        private float _Opacity = 1f;

        /// <summary> Gets or sets the opacity. </summary>
        /// <value> The opacity. </value>
        [Category( "Appearance" )]
        public float Opacity
        {
            get => this._Opacity;

            set {
                if ( value < 0f )
                {
                    value = 0f;
                }
                else if ( value > 1f )
                {
                    value = 1f;
                }

                this._Opacity = value;
                this.TabControl.Invalidate();
            }
        }

        /// <summary> The border color selected. </summary>
        private Color _BorderColorSelected;

        /// <summary> Gets or sets the border color selected. </summary>
        /// <value> The border color selected. </value>
        [Category( "Appearance" )]
        [DefaultValue( typeof( Color ), "" )]
        public Color BorderColorSelected
        {
            get => this._BorderColorSelected.IsEmpty ? ThemedColors.ToolBorder : this._BorderColorSelected;

            set {
                this._BorderColorSelected = value.Equals( ThemedColors.ToolBorder ) ? Color.Empty : value;
                this.TabControl.Invalidate();
            }
        }

        /// <summary> The border color hot. </summary>
        private Color _BorderColorHot;

        /// <summary> Gets or sets the border color hot. </summary>
        /// <value> The border color hot. </value>
        [Category( "Appearance" )]
        [DefaultValue( typeof( Color ), "" )]
        public Color BorderColorHot
        {
            get => this._BorderColorHot.IsEmpty ? SystemColors.ControlDark : this._BorderColorHot;

            set {
                this._BorderColorHot = value.Equals( SystemColors.ControlDark ) ? Color.Empty : value;
                this.TabControl.Invalidate();
            }
        }

        /// <summary> The border color. </summary>
        private Color _BorderColor;

        /// <summary> Gets or sets the color of the border. </summary>
        /// <value> The color of the border. </value>
        [Category( "Appearance" )]
        [DefaultValue( typeof( Color ), "" )]
        public Color BorderColor
        {
            get => this._BorderColor.IsEmpty ? SystemColors.ControlDark : this._BorderColor;

            set {
                this._BorderColor = value.Equals( SystemColors.ControlDark ) ? Color.Empty : value;
                this.TabControl.Invalidate();
            }
        }

        /// <summary> The text color. </summary>
        private Color _TextColor;

        /// <summary> Gets or sets the color of the text. </summary>
        /// <value> The color of the text. </value>
        [Category( "Appearance" )]
        [DefaultValue( typeof( Color ), "" )]
        public Color TextColor
        {
            get => this._TextColor.IsEmpty ? SystemColors.ControlText : this._TextColor;

            set {
                this._TextColor = value.Equals( SystemColors.ControlText ) ? Color.Empty : value;
                this.TabControl.Invalidate();
            }
        }

        /// <summary> The text color selected. </summary>
        private Color _TextColorSelected = Color.Empty;

        /// <summary> Gets or sets the text color selected. </summary>
        /// <value> The text color selected. </value>
        [Category( "Appearance" )]
        [DefaultValue( typeof( Color ), "" )]
        public Color TextColorSelected
        {
            get => this._TextColorSelected.IsEmpty ? SystemColors.ControlText : this._TextColorSelected;

            set {
                this._TextColorSelected = value.Equals( SystemColors.ControlText ) ? Color.Empty : value;
                this.TabControl.Invalidate();
            }
        }

        /// <summary> The text color disabled. </summary>
        private Color _TextColorDisabled;

        /// <summary> Gets or sets the text color disabled. </summary>
        /// <value> The text color disabled. </value>
        [Category( "Appearance" )]
        [DefaultValue( typeof( Color ), "" )]
        public Color TextColorDisabled
        {
            get => this._TextColorDisabled.IsEmpty ? SystemColors.ControlDark : this._TextColorDisabled;

            set {
                this._TextColorDisabled = value.Equals( SystemColors.ControlDark ) ? Color.Empty : value;
                this.TabControl.Invalidate();
            }
        }

        /// <summary> The focus color. </summary>
        private Color _FocusColor;

        /// <summary> Gets or sets the color of the focus. </summary>
        /// <value> The color of the focus. </value>
        [Category( "Appearance" )]
        [DefaultValue( typeof( Color ), "Orange" )]
        public Color FocusColor
        {
            get => this._FocusColor;

            set {
                this._FocusColor = value;
                this.TabControl.Invalidate();
            }
        }

        /// <summary> The closer color active. </summary>
        private Color _CloserColorActive;

        /// <summary> Gets or sets the closer color active. </summary>
        /// <value> The closer color active. </value>
        [Category( "Appearance" )]
        [DefaultValue( typeof( Color ), "Black" )]
        public Color CloserColorActive
        {
            get => this._CloserColorActive;

            set {
                this._CloserColorActive = value;
                this.TabControl.Invalidate();
            }
        }

        /// <summary> The closer color. </summary>
        private Color _CloserColor;

        /// <summary> Gets or sets the color of the closer. </summary>
        /// <value> The color of the closer. </value>
        [Category( "Appearance" )]
        [DefaultValue( typeof( Color ), "DarkGrey" )]
        public Color CloserColor
        {
            get => this._CloserColor;

            set {
                this._CloserColor = value;
                this.TabControl.Invalidate();
            }
        }

        #endregion

        #region " Painting"

        /// <summary> Paints the tab. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="index">    Zero-based index of the. </param>
        /// <param name="graphics"> The graphics. </param>
        public void PaintTab( int index, Graphics graphics )
        {
            if ( graphics is null )
            {
                throw new ArgumentNullException( nameof( graphics ) );
            }

            using var tabpath = this.GetTabBorder( index );
            using var fillBrush = this.GetTabBackgroundBrush( index );
            // Paint the background
            graphics.FillPath( fillBrush, tabpath );
            // Paint a focus indication
            if ( this.TabControl.Focused )
            {
                this.DrawTabFocusIndicator( tabpath, index, graphics );
            }
            // Paint the closer
            this.DrawTabCloser( index, graphics );
        }

        /// <summary> Draw tab closer. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="index">    Zero-based index of the. </param>
        /// <param name="graphics"> The graphics. </param>
        protected virtual void DrawTabCloser( int index, Graphics graphics )
        {
            if ( graphics is null )
            {
                throw new ArgumentNullException( nameof( graphics ) );
            }

            if ( this.ShowTabCloser )
            {
                var closerRect = this.TabControl.GetTabCloserRect( index );
                graphics.SmoothingMode = SmoothingMode.AntiAlias;
                using var closerPath = GetCloserPath( closerRect );
                if ( closerRect.Contains( this.TabControl.MousePosition ) )
                {
                    using var closerPen = new Pen( this.CloserColorActive );
                    graphics.DrawPath( closerPen, closerPath );
                }
                else
                {
                    using var closerPen = new Pen( this.CloserColor );
                    graphics.DrawPath( closerPen, closerPath );
                }
            }
        }

        /// <summary> Gets closer path. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="closerRect"> The closer rectangle. </param>
        /// <returns> The closer path. </returns>
        protected static GraphicsPath GetCloserPath( Rectangle closerRect )
        {
            var closerPath = new GraphicsPath();
            closerPath.AddLine( closerRect.X, closerRect.Y, closerRect.Right, closerRect.Bottom );
            closerPath.CloseFigure();
            closerPath.AddLine( closerRect.Right, closerRect.Y, closerRect.X, closerRect.Bottom );
            closerPath.CloseFigure();
            return closerPath;
        }

        /// <summary> Draw tab focus indicator. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="tabpath">  The tabpath. </param>
        /// <param name="index">    Zero-based index of the. </param>
        /// <param name="graphics"> The graphics. </param>
        private void DrawTabFocusIndicator( GraphicsPath tabpath, int index, Graphics graphics )
        {
            if ( this.FocusTrack && this.TabControl.Focused && index == this.TabControl.SelectedIndex )
            {
                var pathRect = tabpath.GetBounds();
                Rectangle focusRect;
                switch ( this.TabControl.Alignment )
                {
                    case TabAlignment.Top:
                        {
                            focusRect = new Rectangle( ( int ) Math.Truncate( pathRect.X ), ( int ) Math.Truncate( pathRect.Y ), ( int ) Math.Truncate( pathRect.Width ), 4 );
                            using Brush focusBrush = new LinearGradientBrush( focusRect, this.FocusColor, SystemColors.Window, LinearGradientMode.Vertical );
                            using var focusRegion = new Region( focusRect );
                            focusRegion.Intersect( tabpath );
                            graphics.FillRegion( focusBrush, focusRegion );

                            break;
                        }

                    case TabAlignment.Bottom:
                        {
                            focusRect = new Rectangle( ( int ) Math.Truncate( pathRect.X ), ( int ) Math.Truncate( pathRect.Bottom ) - 4, ( int ) Math.Truncate( pathRect.Width ), 4 );
                            using Brush focusBrush = new LinearGradientBrush( focusRect, SystemColors.ControlLight, this.FocusColor, LinearGradientMode.Vertical );
                            using var focusRegion = new Region( focusRect );
                            focusRegion.Intersect( tabpath );
                            graphics.FillRegion( focusBrush, focusRegion );

                            break;
                        }

                    case TabAlignment.Left:
                        {
                            focusRect = new Rectangle( ( int ) Math.Truncate( pathRect.X ), ( int ) Math.Truncate( pathRect.Y ), 4, ( int ) Math.Truncate( pathRect.Height ) );
                            using Brush focusBrush = new LinearGradientBrush( focusRect, this.FocusColor, SystemColors.ControlLight, LinearGradientMode.Horizontal );
                            using var focusRegion = new Region( focusRect );
                            focusRegion.Intersect( tabpath );
                            graphics.FillRegion( focusBrush, focusRegion );

                            break;
                        }

                    case TabAlignment.Right:
                        {
                            focusRect = new Rectangle( ( int ) Math.Truncate( pathRect.Right ) - 4, ( int ) Math.Truncate( pathRect.Y ), 4, ( int ) Math.Truncate( pathRect.Height ) );
                            using Brush focusBrush = new LinearGradientBrush( focusRect, SystemColors.ControlLight, this.FocusColor, LinearGradientMode.Horizontal );
                            using var focusRegion = new Region( focusRect );
                            focusRegion.Intersect( tabpath );
                            graphics.FillRegion( focusBrush, focusRegion );

                            break;
                        }
                }
            }
        }

        #endregion

        #region " Background brushes"

        /// <summary> Gets background blend. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> The background blend. </returns>
        private Blend GetBackgroundBlend()
        {
            var relativeIntensities = new float[] { 0f, 0.7f, 1.0f };
            var relativePositions = new float[] { 0f, 0.6f, 1.0f };

            // Glass look to top aligned tabs
            if ( this.TabControl.Alignment == TabAlignment.Top )
            {
                relativeIntensities = new float[] { 0f, 0.5f, 1.0f, 1.0f };
                relativePositions = new float[] { 0f, 0.5f, 0.51f, 1.0f };
            }

            var blend = new Blend() { Factors = relativeIntensities, Positions = relativePositions };
            return blend;
        }

        /// <summary> Gets page background brush. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="index"> Zero-based index of the. </param>
        /// <returns> The page background brush. </returns>
        public virtual Brush GetPageBackgroundBrush( int index )
        {

            // Capture the colors dependent on selection state of the tab
            var light = Color.FromArgb( 242, 242, 242 );
            if ( this.TabControl.Alignment == TabAlignment.Top )
            {
                light = Color.FromArgb( 207, 207, 207 );
            }

            if ( this.TabControl.SelectedIndex == index )
            {
                light = SystemColors.Window;
            }
            else if ( !this.TabControl.TabPages[index].Enabled )
            {
                light = Color.FromArgb( 207, 207, 207 );
            }
            else if ( this.HotTrack && index == this.TabControl.ActiveIndex )
            {
                // Enable hot tracking
                light = Color.FromArgb( 234, 246, 253 );
            }

            return new SolidBrush( light );
        }

        #endregion

        #region " Tab border and rectangle"

        /// <summary> Gets tab border. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="index"> Zero-based index of the. </param>
        /// <returns> The tab border. </returns>
        public GraphicsPath GetTabBorder( int index )
        {
            var path = new GraphicsPath();
            var tabBounds = this.GetTabRect( index );
            this.AddTabBorder( path, tabBounds );
            path.CloseFigure();
            return path;
        }

        #endregion

    }

    /// <summary> Values that represent tab styles. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public enum TabStyle
    {

        /// <summary> An enum constant representing the none option. </summary>
        None,

        /// <summary> An enum constant representing the default] option. </summary>
        Default,

        /// <summary> An enum constant representing the visual studio option. </summary>
        VisualStudio,

        /// <summary> An enum constant representing the rounded option. </summary>
        Rounded,

        /// <summary> An enum constant representing the angled option. </summary>
        Angled,

        /// <summary> An enum constant representing the chrome option. </summary>
        Chrome,

        /// <summary> An enum constant representing the IE 8 option. </summary>
        IE8,

        /// <summary> An enum constant representing the vs 2010 option. </summary>
        VS2010
    }
}
