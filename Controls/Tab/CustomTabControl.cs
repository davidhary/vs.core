using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Security.Permissions;
using System.Windows.Forms;

using isr.Core.Controls.ExceptionExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Controls
{

    /// <summary> A custom tab control. </summary>
    /// <remarks>
    /// (c) 2010 The Man from U.N.C.L.E. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-09-26 </para><para>
    /// http://www.codeproject.com/Articles/91387/Painting-Your-Own-Tabs-Second-Edition </para>
    /// </remarks>
    [ToolboxBitmap( typeof( TabControl ) )]
    public class CustomTabControl : TabControl
    {

        #region " Construction"

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Windows.Forms.TabControl" /> class.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public CustomTabControl() : base()
        {
            this.SetStyle( ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint | ControlStyles.Opaque | ControlStyles.ResizeRedraw, true );
            this._BackBuffer = new Bitmap( this.Width, this.Height );
            this._BackBufferGraphics = Graphics.FromImage( this._BackBuffer );
            this._TabBuffer = new Bitmap( this.Width, this.Height );
            this._TabBufferGraphics = Graphics.FromImage( this._TabBuffer );
            this.DisplayStyle = TabStyle.Default;
        }

        /// <summary>
        /// Raises the <see cref="M:System.Windows.Forms.Control.CreateControl" /> method.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        protected override void OnCreateControl()
        {
            base.OnCreateControl();
            this.OnFontChanged( EventArgs.Empty );
        }

        /// <summary>
        /// This member overrides <see cref="P:System.Windows.Forms.Control.CreateParams" />.
        /// </summary>
        /// <value>
        /// A <see cref="T:System.Windows.Forms.CreateParams" /> that contains the required creation
        /// parameters when the handle to the control is created.
        /// </value>
        protected override CreateParams CreateParams
        {
            [SecurityPermission( SecurityAction.Demand, Flags = SecurityPermissionFlag.UnmanagedCode )]
            get {
                var cp = base.CreateParams;
                if ( this.RightToLeftLayout )
                {
                    cp.ExStyle = cp.ExStyle | NativeMethods.WS_EX_LAYOUTRTL | NativeMethods.WS_EX_NOINHERITLAYOUT;
                }

                return cp;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.RemoveHScrollToggledEventHandler( HScroll );
                    this.RemoveTabClosingEventHandler( TabClosing );
                    this.RemoveTabImageClickEventHandler( TabImageClick );
                    if ( this._BackImage is object )
                    {
                        this._BackImage.Dispose();
                        this._BackImage = null;
                    }

                    if ( this._BackBufferGraphics is object )
                    {
                        this._BackBufferGraphics.Dispose();
                        this._BackBufferGraphics = null;
                    }

                    if ( this._BackBuffer is object )
                    {
                        this._BackBuffer.Dispose();
                        this._BackBuffer = null;
                    }

                    if ( this._TabBufferGraphics is object )
                    {
                        this._TabBufferGraphics.Dispose();
                        this._TabBufferGraphics = null;
                    }

                    if ( this._TabBuffer is object )
                    {
                        this._TabBuffer.Dispose();
                        this._TabBuffer = null;
                    }

                    if ( this._StyleProvider is object )
                    {
                        this._StyleProvider.Dispose();
                        this._StyleProvider = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " Private variables"

        /// <summary> The back image. </summary>
        private Bitmap _BackImage;

        /// <summary> Buffer for back data. </summary>
        private Bitmap _BackBuffer;

        /// <summary> The back buffer graphics. </summary>
        private Graphics _BackBufferGraphics;

        /// <summary> Buffer for tab data. </summary>
        private Bitmap _TabBuffer;

        /// <summary> The tab buffer graphics. </summary>
        private Graphics _TabBufferGraphics;

        /// <summary> The old value. </summary>
        private int _OldValue;

        /// <summary> The drag start position. </summary>
        private Point _DragStartPosition = Point.Empty;

        /// <summary> The tab pages. </summary>
        private List<TabPage> _TabPages;

        #endregion

        #region " Public properties"

        /// <summary> The style provider. </summary>
        private TabStyleProvider _StyleProvider;

        /// <summary> Gets or sets the display style provider. </summary>
        /// <value> The display style provider. </value>
        [Category( "Appearance" )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Content )]
        public TabStyleProvider DisplayStyleProvider
        {
            get {
                if ( this._StyleProvider is null )
                {
                    this.DisplayStyle = TabStyle.Default;
                }

                return this._StyleProvider;
            }

            set => this._StyleProvider = value;
        }

        /// <summary> The style. </summary>
        private TabStyle _Style;

        /// <summary> Gets or sets the display style. </summary>
        /// <value> The display style. </value>
        [Category( "Appearance" )]
        [DefaultValue( typeof( TabStyle ), "Default" )]
        [RefreshProperties( RefreshProperties.All )]
        public TabStyle DisplayStyle
        {
            get => this._Style;

            set {
                if ( this._Style != value )
                {
                    this._Style = value;
                    this._StyleProvider = TabStyleProvider.CreateProvider( this );
                    this.Invalidate();
                }
            }
        }

        /// <summary> Gets or sets the multiline. </summary>
        /// <value> The multiline. </value>
        [Category( "Appearance" )]
        [RefreshProperties( RefreshProperties.All )]
        public new bool Multiline
        {
            get => base.Multiline;

            set {
                base.Multiline = value;
                this.Invalidate();
            }
        }

        /// <summary> Gets or sets the padding. </summary>
        /// <value> The padding. </value>
        /// <remarks>
        /// Hide the Padding attribute so it can not be changed	We are handling this on the Style Provider
        /// </remarks>
        [Browsable( false )]
        [EditorBrowsable( EditorBrowsableState.Never )]
        public new Point Padding
        {
            get => this.DisplayStyleProvider.Padding;

            set => this.DisplayStyleProvider.Padding = value;
        }

        /// <summary>
        /// Gets or sets a value indicating whether right-to-left mirror placement is turned on.
        /// </summary>
        /// <value>
        /// <see langword="true" /> if right-to-left mirror placement is turned on;
        /// <see langword="false" /> for standard child control placement. The default is
        /// <see langword="false" />.
        /// </value>
        public override bool RightToLeftLayout
        {
            get => base.RightToLeftLayout;

            set {
                base.RightToLeftLayout = value;
                this.UpdateStyles();
            }
        }

        /// <summary> Gets or sets the hot track. </summary>
        /// <remarks>
        /// Hide the HotTrack attribute so it can not be changed.   We are handling this on the Style
        /// Provider.
        /// </remarks>
        /// <value> The hot track. </value>
        [Browsable( false )]
        [EditorBrowsable( EditorBrowsableState.Never )]
        public new bool HotTrack
        {
            get => this.DisplayStyleProvider.HotTrack;

            set => this.DisplayStyleProvider.HotTrack = value;
        }

        /// <summary> Gets or sets the alignment. </summary>
        /// <value> The alignment. </value>
        [Category( "Appearance" )]
        public new TabAlignment Alignment
        {
            get => base.Alignment;

            set {
                base.Alignment = value;
                switch ( value )
                {
                    case TabAlignment.Top:
                    case TabAlignment.Bottom:
                        {
                            this.Multiline = false;
                            break;
                        }

                    case TabAlignment.Left:
                    case TabAlignment.Right:
                        {
                            this.Multiline = true;
                            break;
                        }
                }
            }
        }

        /// <summary> Gets or sets the appearance. </summary>
        /// <remarks>
        /// Hide the Appearance attribute so it can not be changed  We don't want it as we are doing all
        /// the painting.
        /// </remarks>
        /// <value> The appearance. </value>
        [Browsable( false )]
        [EditorBrowsable( EditorBrowsableState.Never )]
        public new TabAppearance Appearance
        {
            get => base.Appearance;

            set =>
                // Don't permit setting to other appearances as we are doing all the painting
                base.Appearance = TabAppearance.Normal;
        }

        /// <summary> Gets the display area of the control's tab pages. </summary>
        /// <value>
        /// A <see cref="T:System.Drawing.Rectangle" /> that represents the display area of the tab pages.
        /// </value>
        public override Rectangle DisplayRectangle { get; }

        /// <summary> Gets the active index. </summary>
        /// <value> The active index. </value>
        [Browsable( false )]
        public int ActiveIndex
        {
            get {
                var hitTestInfo = new NativeMethods.TCHITTESTINFO( this.PointToClient( Control.MousePosition ) );
                int index = NativeMethods.SendMessage( this.Handle, NativeMethods.TCM_HITTEST, IntPtr.Zero, NativeMethods.ToIntPtr( hitTestInfo ) ).ToInt32();
                return index == -1 ? -1 : this.TabPages[index].Enabled ? index : -1;
            }
        }

        /// <summary> Gets the active tab. </summary>
        /// <value> The active tab. </value>
        [Browsable( false )]
        public TabPage ActiveTab
        {
            get {
                int activeIndex = this.ActiveIndex;
                return activeIndex > -1 ? this.TabPages[activeIndex] : null;
            }
        }

        #endregion

        #region " Extension methods"

        /// <summary> Hides the tab. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="page"> The page. </param>
        public void HideTab( TabPage page )
        {
            if ( page is object && this.TabPages.Contains( page ) )
            {
                this.BackupTabPages();
                this.TabPages.Remove( page );
            }
        }

        /// <summary> Hides the tab. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="index"> Zero-based index of the. </param>
        public void HideTab( int index )
        {
            if ( this.IsValidTabIndex( index ) )
            {
                this.HideTab( this._TabPages[index] );
            }
        }

        /// <summary> Hides the tab. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="key"> The key. </param>
        public void HideTab( string key )
        {
            if ( this.TabPages.ContainsKey( key ) )
            {
                this.HideTab( this.TabPages[key] );
            }
        }

        /// <summary> Shows the tab. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="page"> The page. </param>
        public void ShowTab( TabPage page )
        {
            if ( page is object )
            {
                if ( this._TabPages is object )
                {
                    if ( !this.TabPages.Contains( page ) && this._TabPages.Contains( page ) )
                    {

                        // Get insert point from backup of pages
                        int pageIndex = this._TabPages.IndexOf( page );
                        if ( pageIndex > 0 )
                        {
                            int start = pageIndex - 1;

                            // Check for presence of earlier pages in the visible tabs
                            for ( int index = start; index >= 0; index -= 1 )
                            {
                                if ( this.TabPages.Contains( this._TabPages[index] ) )
                                {

                                    // Set insert point to the right of the last present tab
                                    pageIndex = this.TabPages.IndexOf( this._TabPages[index] ) + 1;
                                    break;
                                }
                            }
                        }

                        // Insert the page, or add to the end
                        if ( pageIndex >= 0 && pageIndex < this.TabPages.Count )
                        {
                            this.TabPages.Insert( pageIndex, page );
                        }
                        else
                        {
                            this.TabPages.Add( page );
                        }
                    }
                }

                // If the page is not found at all then just add it
                else if ( !this.TabPages.Contains( page ) )
                {
                    this.TabPages.Add( page );
                }
            }
        }

        /// <summary> Shows the tab. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="index"> Zero-based index of the. </param>
        public void ShowTab( int index )
        {
            if ( this.IsValidTabIndex( index ) )
            {
                this.ShowTab( this._TabPages[index] );
            }
        }

        /// <summary> Shows the tab. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="key"> The key. </param>
        public void ShowTab( string key )
        {
            if ( this._TabPages is object )
            {
                var tab = this._TabPages.Find( ( page ) => page.Name.Equals( key, StringComparison.OrdinalIgnoreCase ) );
                this.ShowTab( tab );
            }
        }

        /// <summary> Query if 'index' is valid tab index. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="index"> Zero-based index of the. </param>
        /// <returns> True if valid tab index, false if not. </returns>
        private bool IsValidTabIndex( int index )
        {
            this.BackupTabPages();
            return index >= 0 && index < this._TabPages.Count;
        }

        /// <summary> Backup tab pages. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void BackupTabPages()
        {
            if ( this._TabPages is null )
            {
                this._TabPages = new List<TabPage>();
                foreach ( TabPage page in this.TabPages )
                {
                    this._TabPages.Add( page );
                }
            }
        }

        #endregion

        #region " Drag 'n' Drop"

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.MouseDown" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnMouseDown( MouseEventArgs e )
        {
            base.OnMouseDown( e );
            if ( e is object && this.AllowDrop )
            {
                this._DragStartPosition = new Point( e.X, e.Y );
            }
        }

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.MouseUp" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnMouseUp( MouseEventArgs e )
        {
            base.OnMouseUp( e );
            if ( this.AllowDrop )
            {
                this._DragStartPosition = Point.Empty;
            }
        }

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.DragOver" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="drgevent"> A <see cref="T:System.Windows.Forms.DragEventArgs" /> that contains
        /// the event data. </param>
        protected override void OnDragOver( DragEventArgs drgevent )
        {
            base.OnDragOver( drgevent );
            if ( drgevent is null )
            {
            }
            else
            {
                drgevent.Effect = drgevent.Data.GetDataPresent( typeof( TabPage ) ) ? DragDropEffects.Move : DragDropEffects.None;
            }
        }

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.DragDrop" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="drgevent"> A <see cref="T:System.Windows.Forms.DragEventArgs" /> that contains
        /// the event data. </param>
        protected override void OnDragDrop( DragEventArgs drgevent )
        {
            base.OnDragDrop( drgevent );
            if ( drgevent is object && drgevent.Data.GetDataPresent( typeof( TabPage ) ) )
            {
                drgevent.Effect = DragDropEffects.Move;
                TabPage dragTab = ( TabPage ) drgevent.Data.GetData( typeof( TabPage ) );
                if ( ReferenceEquals( this.ActiveTab, dragTab ) )
                {
                    return;
                }

                // Capture insert point and adjust for removal of tab
                // We cannot assess this after removal as differing tab sizes will cause
                // inaccuracies in the activeTab at insert point.
                int insertPoint = this.ActiveIndex;
                if ( dragTab.Parent.Equals( this ) && this.TabPages.IndexOf( dragTab ) < insertPoint )
                {
                    insertPoint -= 1;
                }

                if ( insertPoint < 0 )
                {
                    insertPoint = 0;

                    // Remove from current position (could be another tab control)
                } (( TabControl ) dragTab.Parent).TabPages.Remove( dragTab );

                // Add to current position
                this.TabPages.Insert( insertPoint, dragTab );

                // deal with hidden tab handling?
                this.SelectedTab = dragTab;
            }
        }

        /// <summary> Starts drag drop. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void StartDragDrop()
        {
            if ( !this._DragStartPosition.IsEmpty )
            {
                var dragTab = this.SelectedTab;
                if ( dragTab is object )
                {
                    // Test for movement greater than the drag activation trigger area
                    var dragTestRect = new Rectangle( this._DragStartPosition, Size.Empty );
                    dragTestRect.Inflate( SystemInformation.DragSize );
                    var pt = this.PointToClient( Control.MousePosition );
                    if ( !dragTestRect.Contains( pt ) )
                    {
                        _ = this.DoDragDrop( dragTab, DragDropEffects.All );
                        this._DragStartPosition = Point.Empty;
                    }
                }
            }
        }

        #endregion

        #region " Events"

        /// <summary> Event queue for all listeners interested in Scroll events. </summary>
        [Category( "Action" )]
        public event EventHandler<ScrollEventArgs> HScroll;

        /// <summary> Removes event handler. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The handler. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void RemoveHScrollToggledEventHandler( EventHandler<ScrollEventArgs> value )
        {
            foreach ( Delegate d in value is null ? (Array.Empty<Delegate>()) : value.GetInvocationList() )
            {
                try
                {
                    HScroll -= ( EventHandler<ScrollEventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }
        }

        /// <summary> Event queue for all listeners interested in TabImageClick events. </summary>
        [Category( "Action" )]
        public event EventHandler<TabControlEventArgs> TabImageClick;

        /// <summary> Removes event handler. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The handler. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void RemoveTabImageClickEventHandler( EventHandler<TabControlEventArgs> value )
        {
            foreach ( Delegate d in value is null ? (Array.Empty<Delegate>()) : value.GetInvocationList() )
            {
                try
                {
                    TabImageClick -= ( EventHandler<TabControlEventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }
        }

        /// <summary> Event queue for all listeners interested in TabClosing events. </summary>

        [Category( "Action" )]
        public event EventHandler<TabControlCancelEventArgs> TabClosing;

        /// <summary> Removes event handler. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The handler. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void RemoveTabClosingEventHandler( EventHandler<TabControlCancelEventArgs> value )
        {
            foreach ( Delegate d in value is null ? (Array.Empty<Delegate>()) : value.GetInvocationList() )
            {
                try
                {
                    TabClosing -= ( EventHandler<TabControlCancelEventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }
        }



        #endregion

        #region " Base class event processing"

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.FontChanged" /> event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnFontChanged( EventArgs e )
        {
            var hFont = this.Font.ToHfont();
            _ = NativeMethods.SendMessage( this.Handle, NativeMethods.WM_SETFONT, hFont, ( IntPtr ) (-1) );
            _ = NativeMethods.SendMessage( this.Handle, NativeMethods.WM_FONTCHANGE, IntPtr.Zero, IntPtr.Zero );
            this.UpdateStyles();
            if ( this.Visible )
            {
                this.Invalidate();
            }
        }

        /// <summary>
        /// This member overrides
        /// <see cref="M:System.Windows.Forms.Control.OnResize(System.EventArgs)" />.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnResize( EventArgs e )
        {
            // Recreate the buffer for manual double buffering
            if ( this.Width > 0 && this.Height > 0 )
            {
                this._BackImage?.Dispose();
                this._BackImage = null;
                this._BackBufferGraphics?.Dispose();
                this._BackBuffer?.Dispose();
                this._BackBuffer = new Bitmap( this.Width, this.Height );
                this._BackBufferGraphics = Graphics.FromImage( this._BackBuffer );
                this._TabBufferGraphics?.Dispose();
                this._TabBuffer?.Dispose();
                this._TabBuffer = new Bitmap( this.Width, this.Height );
                this._TabBufferGraphics = Graphics.FromImage( this._TabBuffer );
                this._BackImage?.Dispose();
                this._BackImage = null;
            }

            base.OnResize( e );
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.BackColorChanged" /> event when the
        /// <see cref="P:System.Windows.Forms.Control.BackColor" /> property value of the control's
        /// container changes.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnParentBackColorChanged( EventArgs e )
        {
            this._BackImage?.Dispose();
            this._BackImage = null;
            base.OnParentBackColorChanged( e );
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.BackgroundImageChanged" /> event when
        /// the <see cref="P:System.Windows.Forms.Control.BackgroundImage" /> property value of the
        /// control's container changes.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnParentBackgroundImageChanged( EventArgs e )
        {
            this._BackImage?.Dispose();
            this._BackImage = null;
            base.OnParentBackgroundImageChanged( e );
        }

        /// <summary> Raises the parent resize event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information to send to registered event handlers. </param>
        private void OnParentResize( object sender, EventArgs e )
        {
            if ( this.Visible )
            {
                this.Invalidate();
            }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.ParentChanged" /> event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnParentChanged( EventArgs e )
        {
            base.OnParentChanged( e );
            if ( this.Parent is object )
            {
                this.Parent.Resize += this.OnParentResize;
            }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.TabControl.Selecting" /> event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.TabControlCancelEventArgs" /> that
        /// contains the event data. </param>
        protected override void OnSelecting( TabControlCancelEventArgs e )
        {
            base.OnSelecting( e );

            // Do not allow selecting of disabled tabs
            if ( e is object && e.Action == TabControlAction.Selecting && e.TabPage is object && !e.TabPage.Enabled )
            {
                e.Cancel = true;
            }
        }

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.Move" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnMove( EventArgs e )
        {
            if ( this.Width > 0 && this.Height > 0 )
            {
                this._BackImage?.Dispose();
                this._BackImage = null;
            }

            base.OnMove( e );
            this.Invalidate();
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.ControlAdded" /> event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.ControlEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnControlAdded( ControlEventArgs e )
        {
            base.OnControlAdded( e );
            if ( this.Visible )
            {
                this.Invalidate();
            }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.ControlRemoved" /> event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.ControlEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnControlRemoved( ControlEventArgs e )
        {
            base.OnControlRemoved( e );
            if ( this.Visible )
            {
                this.Invalidate();
            }
        }

        /// <summary> Processes a mnemonic character. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="charCode"> The character to process. </param>
        /// <returns>
        /// <see langword="true" /> if the character was processed as a mnemonic by the control;
        /// otherwise, <see langword="false" />.
        /// </returns>
        [UIPermission( SecurityAction.Demand, Window = UIPermissionWindow.AllWindows )]
        protected override bool ProcessMnemonic( char charCode )
        {
            foreach ( TabPage page in this.TabPages )
            {
                if ( IsMnemonic( charCode, page.Text ) )
                {
                    this.SelectedTab = page;
                    return true;
                }
            }

            return base.ProcessMnemonic( charCode );
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.TabControl.SelectedIndexChanged" /> event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnSelectedIndexChanged( EventArgs e )
        {
            base.OnSelectedIndexChanged( e );
        }

        /// <summary>
        /// This member overrides
        /// <see cref="M:System.Windows.Forms.Control.WndProc(System.Windows.Forms.Message@)" />.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="m"> [in,out] A Windows Message Object. </param>
        [SecurityPermission( SecurityAction.Demand, Flags = SecurityPermissionFlag.UnmanagedCode )]
        [DebuggerStepThrough]
        protected override void WndProc( ref Message m )
        {
            switch ( m.Msg )
            {
                case NativeMethods.WM_HSCROLL:
                    {

                        // Raise the scroll event when the scroller is scrolled
                        base.WndProc( ref m );
                        this.OnHScroll( new ScrollEventArgs( ( ScrollEventType ) Conversions.ToInteger( NativeMethods.LoWord( m.WParam ) ), this._OldValue, NativeMethods.HiWord( m.WParam ), ScrollOrientation.HorizontalScroll ) );
                        break;
                    }

                default:
                    {
                        // case NativeMethods.WM_PAINT:
                        // 
                        // //	Handle painting ourselves rather than call the base OnPaint.
                        // CustomPaint(ref m);
                        // break;

                        base.WndProc( ref m );
                        break;
                    }
            }
        }

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.MouseClick" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnMouseClick( MouseEventArgs e )
        {
            int index = this.ActiveIndex;

            // If we are clicking on an image then raise the ImageClicked event before raising the standard mouse click event
            // if there if a handler.
            if ( index > -1 && TabImageClick is object && (this.TabPages[index].ImageIndex > -1 || !string.IsNullOrEmpty( this.TabPages[index].ImageKey )) && this.GetTabImageRect( index ).Contains( this.MousePosition ) )
            {
                this.OnTabImageClick( new TabControlEventArgs( this.TabPages[index], index, TabControlAction.Selected ) );

                // Fire the base event

                base.OnMouseClick( e );
            }
            else if ( !this.DesignMode && index > -1 && this._StyleProvider.ShowTabCloser && this.GetTabCloserRect( index ).Contains( this.MousePosition ) )
            {

                // If we are clicking on a closer then remove the tab instead of raising the standard mouse click event
                // But raise the tab closing event first
                var tab = this.ActiveTab;
                var args = new TabControlCancelEventArgs( tab, index, false, TabControlAction.Deselecting );
                this.OnTabClosing( args );
                if ( !args.Cancel )
                {
                    this.TabPages.Remove( tab );
                    tab.Dispose();
                }
            }
            else
            {
                // Fire the base event
                base.OnMouseClick( e );
            }
        }

        /// <summary> Raises the tab control event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void OnTabImageClick( TabControlEventArgs e )
        {
            TabImageClick?.Invoke( this, e );
        }

        /// <summary> Raises the tab control cancel event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void OnTabClosing( TabControlCancelEventArgs e )
        {
            TabClosing?.Invoke( this, e );
        }

        /// <summary> Raises the scroll event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void OnHScroll( ScrollEventArgs e )
        {
            // repaint the moved tabs
            this.Invalidate();

            // Raise the event
            HScroll?.Invoke( this, e );
            if ( ( int? ) (e?.Type) == ( int? ) ScrollEventType.EndScroll == true )
            {
                this._OldValue = e.NewValue;
            }
        }

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.MouseMove" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnMouseMove( MouseEventArgs e )
        {
            base.OnMouseMove( e );
            if ( this._StyleProvider.ShowTabCloser )
            {
                var tabRect = this._StyleProvider.GetTabRect( this.ActiveIndex );
                if ( tabRect.Contains( this.MousePosition ) )
                {
                    this.Invalidate();
                }
            }

            // Initialize Drag Drop
            if ( e is object && this.AllowDrop && e.Button == MouseButtons.Left )
            {
                this.StartDragDrop();
            }
        }

        #endregion

        #region " Basic drawing methods"

        // private void CustomPaint(ref Message m){
        // NativeMethods.PAINTSTRUCT paintStruct = new NativeMethods.PAINTSTRUCT();
        // NativeMethods.BeginPaint(m.HWnd, ref paintStruct);
        // using (Graphics screenGraphics = this.CreateGraphics()) {
        // this.CustomPaint(screenGraphics);
        // }
        // NativeMethods.EndPaint(m.HWnd, ref paintStruct);
        // }

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.Paint" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnPaint( PaintEventArgs e )
        {
            // We must always paint the entire area of the tab control
            if ( e is object && e.ClipRectangle.Equals( this.ClientRectangle ) )
            {
                this.CustomPaint( e.Graphics );
            }
            else
            {
                // it is less intensive to just re-invoke the paint with the whole surface available to draw on.
                this.Invalidate();
            }
        }

        /// <summary> Custom paint. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="screenGraphics"> The screen graphics. </param>
        private void CustomPaint( Graphics screenGraphics )
        {
            // We render into a bitmap that is then drawn in one shot rather than using
            // double buffering built into the control as the built in buffering
            // messes up the background painting.
            // Equally the .Net 2.0 BufferedGraphics object causes the background painting
            // to mess up, which is why we use this .Net 1.1 buffering technique.

            // Buffer code from Gil. Schmidt http://www.codeproject.com/KB/graphics/DoubleBuffering.aspx

            if ( this.Width > 0 && this.Height > 0 )
            {
                if ( this._BackImage is null )
                {
                    // Cached Background Image
                    this._BackImage = new Bitmap( this.Width, this.Height );
                    var backGraphics = Graphics.FromImage( this._BackImage );
                    backGraphics.Clear( Color.Transparent );
                    this.PaintTransparentBackground( backGraphics, this.ClientRectangle );
                }

                this._BackBufferGraphics.Clear( Color.Transparent );
                this._BackBufferGraphics.DrawImageUnscaled( this._BackImage, 0, 0 );
                this._TabBufferGraphics.Clear( Color.Transparent );
                if ( this.TabCount > 0 )
                {

                    // When top or bottom and scrollable we need to clip the sides from painting the tabs.
                    // Left and right are always multi line.
                    if ( this.Alignment <= TabAlignment.Bottom && !this.Multiline )
                    {
                        this._TabBufferGraphics.Clip = new Region( new RectangleF( this.ClientRectangle.X + 3, this.ClientRectangle.Y, this.ClientRectangle.Width - 6, this.ClientRectangle.Height ) );
                    }

                    // Draw each tab page from right to left.  We do it this way to handle
                    // the overlap correctly.
                    if ( this.Multiline )
                    {
                        for ( int row = 0, loopTo = this.RowCount - 1; row <= loopTo; row++ )
                        {
                            for ( int index = this.TabCount - 1; index >= 0; index -= 1 )
                            {
                                if ( index != this.SelectedIndex && (this.RowCount == 1 || this.GetTabRow( index ) == row) )
                                {
                                    this.DrawTabPage( index, this._TabBufferGraphics );
                                }
                            }
                        }
                    }
                    else
                    {
                        for ( int index = this.TabCount - 1; index >= 0; index -= 1 )
                        {
                            if ( index != this.SelectedIndex )
                            {
                                this.DrawTabPage( index, this._TabBufferGraphics );
                            }
                        }
                    }

                    // The selected tab must be drawn last so it appears on top.
                    if ( this.SelectedIndex > -1 )
                    {
                        this.DrawTabPage( this.SelectedIndex, this._TabBufferGraphics );
                    }
                }

                this._TabBufferGraphics.Flush();

                // Paint the tabs on top of the background

                // Create a new color matrix and set the alpha value to 0.5
                var alphaMatrix = new ColorMatrix();
#if false
                float localInlineAssignHelper() { float argtarget = alphaMatrix.Matrix44;
                                                  var ret = InlineAssignHelper(ref argtarget, 1f);
                                                    alphaMatrix.Matrix44 = argtarget; return ret; }

                float localInlineAssignHelper1() { float argtarget = alphaMatrix.Matrix22;
                                                   var ret = InlineAssignHelper(ref argtarget, hs0accc824c5424d699037dd2c314d03d9());
                                                        alphaMatrix.Matrix22 = argtarget; return ret; }

                float localInlineAssignHelper2() { float argtarget = alphaMatrix.Matrix11;
                                                   var ret = InlineAssignHelper(ref argtarget, hs285b48731b72444c933a346516a0cc3e()); alphaMatrix.Matrix11 = argtarget; return ret; }
                alphaMatrix.Matrix00 = InlineAssignHelper( alphaMatrix.Matrix11, InlineAssignHelper(  alphaMatrix.Matrix22, InlineAssignHelper( alphaMatrix.Matrix44 , 1 ) ) );
#endif

                float temp44 = alphaMatrix.Matrix44;
                float temp22 = alphaMatrix.Matrix22;
                float temp11 = alphaMatrix.Matrix11;

                alphaMatrix.Matrix00 = InlineAssignHelper( ref temp11, InlineAssignHelper( ref temp22, InlineAssignHelper( ref temp44, 1 ) ) );

                alphaMatrix.Matrix44 = temp44;
                alphaMatrix.Matrix22 = temp22;
                alphaMatrix.Matrix11 = temp11;

                alphaMatrix.Matrix33 = this._StyleProvider.Opacity;

                // Create a new image attribute object and set the color matrix to
                // the one just created
                using ( var alphaAttributes = new ImageAttributes() )
                {
                    alphaAttributes.SetColorMatrix( alphaMatrix );

                    // Draw the original image with the image attributes specified
                    this._BackBufferGraphics.DrawImage( this._TabBuffer, new Rectangle( 0, 0, this._TabBuffer.Width, this._TabBuffer.Height ), 0, 0, this._TabBuffer.Width, this._TabBuffer.Height, GraphicsUnit.Pixel, alphaAttributes );
                }

                this._BackBufferGraphics.Flush();

                // Now paint this to the screen


                // We want to paint the whole tab strip and border every time
                // so that the hot areas update correctly, along with any overlaps

                // paint the tabs etc.
                if ( this.RightToLeftLayout )
                {
                    screenGraphics.DrawImageUnscaled( this._BackBuffer, -1, 0 );
                }
                else
                {
                    screenGraphics.DrawImageUnscaled( this._BackBuffer, 0, 0 );
                }
            }
        }

        /// <summary> Paints the transparent background. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphics"> The graphics. </param>
        /// <param name="clipRect"> The clip rectangle. </param>
        protected void PaintTransparentBackground( Graphics graphics, Rectangle clipRect )
        {
            if ( graphics is null )
            {
                throw new ArgumentNullException( nameof( graphics ) );
            }

            if ( this.Parent is object )
            {

                // Set the clip rectangle to be relative to the parent
                clipRect.Offset( this.Location );

                // Save the current state before we do anything.
                var state = graphics.Save();

                // Set the graphics object to be relative to the parent
                graphics.TranslateTransform( -this.Location.X, -this.Location.Y );
                graphics.SmoothingMode = SmoothingMode.HighSpeed;

                // Paint the parent
                try
                {
                    using var e = new PaintEventArgs( graphics, clipRect );
                    this.InvokePaintBackground( this.Parent, e );
                    this.InvokePaint( this.Parent, e );
                }
                finally
                {
                    // Restore the graphics state and the clipRect to their original locations
                    graphics.Restore( state );
                    clipRect.Offset( -this.Location.X, -this.Location.Y );
                }
            }
        }

        /// <summary> Draw tab page. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="index">    Zero-based index of the. </param>
        /// <param name="graphics"> The graphics. </param>
        private void DrawTabPage( int index, Graphics graphics )
        {
            graphics.SmoothingMode = SmoothingMode.HighSpeed;

            // Get TabPageBorder
            using var tabPageBorderPath = this.GetTabPageBorder( index );

            // Paint the background
            using ( var fillBrush = this._StyleProvider.GetPageBackgroundBrush( index ) )
            {
                graphics.FillPath( fillBrush, tabPageBorderPath );
            }

            if ( this._Style != TabStyle.None )
            {

                // Paint the tab
                this._StyleProvider.PaintTab( index, graphics );

                // Draw any image
                this.DrawTabImage( index, graphics );

                // Draw the text

                this.DrawTabText( index, graphics );
            }

            // Paint the border

            this.DrawTabBorder( tabPageBorderPath, index, graphics );
        }

        /// <summary> Draw tab border. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="path">     Full pathname of the file. </param>
        /// <param name="index">    Zero-based index of the. </param>
        /// <param name="graphics"> The graphics. </param>
        private void DrawTabBorder( GraphicsPath path, int index, Graphics graphics )
        {
            graphics.SmoothingMode = SmoothingMode.HighQuality;
            Color borderColor = index == this.SelectedIndex
                ? this._StyleProvider.BorderColorSelected
                : this._StyleProvider.HotTrack && index == this.ActiveIndex ? this._StyleProvider.BorderColorHot : this._StyleProvider.BorderColor;
            using var borderPen = new Pen( borderColor );
            graphics.DrawPath( borderPen, path );
        }

        /// <summary> Draw tab text. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="index">    Zero-based index of the. </param>
        /// <param name="graphics"> The graphics. </param>
        private void DrawTabText( int index, Graphics graphics )
        {
            graphics.SmoothingMode = SmoothingMode.HighQuality;
            graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            var tabBounds = this.GetTabTextRect( index );
            if ( this.SelectedIndex == index )
            {
                using Brush textBrush = new SolidBrush( this._StyleProvider.TextColorSelected );
                graphics.DrawString( this.TabPages[index].Text, this.Font, textBrush, tabBounds, this.GetStringFormat() );
            }
            else if ( this.TabPages[index].Enabled )
            {
                using Brush textBrush = new SolidBrush( this._StyleProvider.TextColor );
                graphics.DrawString( this.TabPages[index].Text, this.Font, textBrush, tabBounds, this.GetStringFormat() );
            }
            else
            {
                using Brush textBrush = new SolidBrush( this._StyleProvider.TextColorDisabled );
                graphics.DrawString( this.TabPages[index].Text, this.Font, textBrush, tabBounds, this.GetStringFormat() );
            }
        }

        /// <summary> Draw tab image. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="index">    Zero-based index of the. </param>
        /// <param name="graphics"> The graphics. </param>
        private void DrawTabImage( int index, Graphics graphics )
        {
            Image tabImage = null;
            if ( this.TabPages[index].ImageIndex > -1 && this.ImageList is object && this.ImageList.Images.Count > this.TabPages[index].ImageIndex )
            {
                tabImage = this.ImageList.Images[this.TabPages[index].ImageIndex];
            }
            else if ( !string.IsNullOrEmpty( this.TabPages[index].ImageKey ) && !this.TabPages[index].ImageKey.Equals( "(none)", StringComparison.OrdinalIgnoreCase ) && this.ImageList is object && this.ImageList.Images.ContainsKey( this.TabPages[index].ImageKey ) )
            {
                tabImage = this.ImageList.Images[this.TabPages[index].ImageKey];
            }

            if ( tabImage is object )
            {
                if ( this.RightToLeftLayout )
                {
                    tabImage.RotateFlip( RotateFlipType.RotateNoneFlipX );
                }

                var imageRect = this.GetTabImageRect( index );
                if ( this.TabPages[index].Enabled )
                {
                    graphics.DrawImage( tabImage, imageRect );
                }
                else
                {
                    ControlPaint.DrawImageDisabled( graphics, tabImage, imageRect.X, imageRect.Y, Color.Transparent );
                }
            }
        }

        #endregion

        #region " String formatting"

        /// <summary> Gets string format. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> The string format. </returns>
        private StringFormat GetStringFormat()
        {
            StringFormat format = null;

            // Rotate Text by 90 degrees for left and right tabs
            switch ( this.Alignment )
            {
                case TabAlignment.Top:
                case TabAlignment.Bottom:
                    {
                        format = new StringFormat();
                        break;
                    }

                case TabAlignment.Left:
                case TabAlignment.Right:
                    {
                        format = new StringFormat( StringFormatFlags.DirectionVertical );
                        break;
                    }
            }

            format.Alignment = StringAlignment.Center;
            format.LineAlignment = StringAlignment.Center;
            format.HotkeyPrefix = this.FindForm() is object && this.FindForm().KeyPreview ? System.Drawing.Text.HotkeyPrefix.Show : System.Drawing.Text.HotkeyPrefix.Hide;
            if ( this.RightToLeft == RightToLeft.Yes )
            {
                format.FormatFlags |= StringFormatFlags.DirectionRightToLeft;
            }

            return format;
        }

        #endregion

        #region " Tab borders and bounds properties"

        /// <summary> Gets tab page border. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="index"> Zero-based index of the. </param>
        /// <returns> The tab page border. </returns>
        private GraphicsPath GetTabPageBorder( int index )
        {
            var path = new GraphicsPath();
            var pageBounds = this.GetPageBounds( index );
            var tabBounds = this._StyleProvider.GetTabRect( index );
            this._StyleProvider.AddTabBorder( path, tabBounds );
            this.AddPageBorder( path, pageBounds, tabBounds );
            path.CloseFigure();
            return path;
        }

        /// <summary> Gets page bounds. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="index"> Zero-based index of the. </param>
        /// <returns> The page bounds. </returns>
        public Rectangle GetPageBounds( int index )
        {
            var pageBounds = this.TabPages[index].Bounds;
            pageBounds.Width += 1;
            pageBounds.Height += 1;
            pageBounds.X -= 1;
            pageBounds.Y -= 1;
            if ( pageBounds.Bottom > this.Height - 4 )
            {
                pageBounds.Height -= pageBounds.Bottom - this.Height + 4;
            }

            return pageBounds;
        }

        /// <summary> Gets tab text rectangle. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="index"> Zero-based index of the. </param>
        /// <returns> The tab text rectangle. </returns>
        private Rectangle GetTabTextRect( int index )
        {
            var textRect = new Rectangle();
            using ( var path = this._StyleProvider.GetTabBorder( index ) )
            {
                var tabBounds = path.GetBounds();
                textRect = new Rectangle( ( int ) Math.Truncate( tabBounds.X ), ( int ) Math.Truncate( tabBounds.Y ), ( int ) Math.Truncate( tabBounds.Width ), ( int ) Math.Truncate( tabBounds.Height ) );

                // Make it shorter or thinner to fit the height or width because of the padding added to the tab for painting
                switch ( this.Alignment )
                {
                    case TabAlignment.Top:
                        {
                            textRect.Y += 4;
                            textRect.Height -= 6;
                            break;
                        }

                    case TabAlignment.Bottom:
                        {
                            textRect.Y += 2;
                            textRect.Height -= 6;
                            break;
                        }

                    case TabAlignment.Left:
                        {
                            textRect.X += 4;
                            textRect.Width -= 6;
                            break;
                        }

                    case TabAlignment.Right:
                        {
                            textRect.X += 2;
                            textRect.Width -= 6;
                            break;
                        }
                }

                // If there is an image allow for it
                if ( this.ImageList is object && (this.TabPages[index].ImageIndex > -1 || !string.IsNullOrEmpty( this.TabPages[index].ImageKey ) && !this.TabPages[index].ImageKey.Equals( "(none)", StringComparison.OrdinalIgnoreCase )) )
                {
                    var imageRect = this.GetTabImageRect( index );
                    if ( (this._StyleProvider.ImageAlign & NativeMethods.AnyLeftAlign) != ( ContentAlignment ) Conversions.ToInteger( 0 ) )
                    {
                        if ( this.Alignment <= TabAlignment.Bottom )
                        {
                            textRect.X = imageRect.Right + 4;
                            textRect.Width -= textRect.Right - ( int ) Math.Truncate( tabBounds.Right );
                        }
                        else
                        {
                            textRect.Y = imageRect.Y + 4;
                            textRect.Height -= textRect.Bottom - ( int ) Math.Truncate( tabBounds.Bottom );
                        }
                        // If there is a closer allow for it
                        if ( this._StyleProvider.ShowTabCloser )
                        {
                            var closerRect = this.GetTabCloserRect( index );
                            if ( this.Alignment <= TabAlignment.Bottom )
                            {
                                if ( this.RightToLeftLayout )
                                {
                                    textRect.Width -= closerRect.Right + 4 - textRect.X;
                                    textRect.X = closerRect.Right + 4;
                                }
                                else
                                {
                                    textRect.Width -= ( int ) Math.Truncate( tabBounds.Right ) - closerRect.X + 4;
                                }
                            }
                            else if ( this.RightToLeftLayout )
                            {
                                textRect.Height -= closerRect.Bottom + 4 - textRect.Y;
                                textRect.Y = closerRect.Bottom + 4;
                            }
                            else
                            {
                                textRect.Height -= ( int ) Math.Truncate( tabBounds.Bottom ) - closerRect.Y + 4;
                            }
                        }
                    }
                    else if ( (this._StyleProvider.ImageAlign & NativeMethods.AnyCenterAlign) != ( ContentAlignment ) Conversions.ToInteger( 0 ) )
                    {
                        // If there is a closer allow for it
                        if ( this._StyleProvider.ShowTabCloser )
                        {
                            var closerRect = this.GetTabCloserRect( index );
                            if ( this.Alignment <= TabAlignment.Bottom )
                            {
                                if ( this.RightToLeftLayout )
                                {
                                    textRect.Width -= closerRect.Right + 4 - textRect.X;
                                    textRect.X = closerRect.Right + 4;
                                }
                                else
                                {
                                    textRect.Width -= ( int ) Math.Truncate( tabBounds.Right ) - closerRect.X + 4;
                                }
                            }
                            else if ( this.RightToLeftLayout )
                            {
                                textRect.Height -= closerRect.Bottom + 4 - textRect.Y;
                                textRect.Y = closerRect.Bottom + 4;
                            }
                            else
                            {
                                textRect.Height -= ( int ) Math.Truncate( tabBounds.Bottom ) - closerRect.Y + 4;
                            }
                        }
                    }
                    else
                    {
                        if ( this.Alignment <= TabAlignment.Bottom )
                        {
                            textRect.Width -= ( int ) Math.Truncate( tabBounds.Right ) - imageRect.X + 4;
                        }
                        else
                        {
                            textRect.Height -= ( int ) Math.Truncate( tabBounds.Bottom ) - imageRect.Y + 4;
                        }
                        // If there is a closer allow for it
                        if ( this._StyleProvider.ShowTabCloser )
                        {
                            var closerRect = this.GetTabCloserRect( index );
                            if ( this.Alignment <= TabAlignment.Bottom )
                            {
                                if ( this.RightToLeftLayout )
                                {
                                    textRect.Width -= closerRect.Right + 4 - textRect.X;
                                    textRect.X = closerRect.Right + 4;
                                }
                                else
                                {
                                    textRect.Width -= ( int ) Math.Truncate( tabBounds.Right ) - closerRect.X + 4;
                                }
                            }
                            else if ( this.RightToLeftLayout )
                            {
                                textRect.Height -= closerRect.Bottom + 4 - textRect.Y;
                                textRect.Y = closerRect.Bottom + 4;
                            }
                            else
                            {
                                textRect.Height -= ( int ) Math.Truncate( tabBounds.Bottom ) - closerRect.Y + 4;
                            }
                        }
                    }
                }
                // If there is a closer allow for it
                else if ( this._StyleProvider.ShowTabCloser )
                {
                    var closerRect = this.GetTabCloserRect( index );
                    if ( this.Alignment <= TabAlignment.Bottom )
                    {
                        if ( this.RightToLeftLayout )
                        {
                            textRect.Width -= closerRect.Right + 4 - textRect.X;
                            textRect.X = closerRect.Right + 4;
                        }
                        else
                        {
                            textRect.Width -= ( int ) Math.Truncate( tabBounds.Right ) - closerRect.X + 4;
                        }
                    }
                    else if ( this.RightToLeftLayout )
                    {
                        textRect.Height -= closerRect.Bottom + 4 - textRect.Y;
                        textRect.Y = closerRect.Bottom + 4;
                    }
                    else
                    {
                        textRect.Height -= ( int ) Math.Truncate( tabBounds.Bottom ) - closerRect.Y + 4;
                    }
                }


                // Ensure it fits inside the path at the center line
                if ( this.Alignment <= TabAlignment.Bottom )
                {
                    while ( !path.IsVisible( textRect.Right, textRect.Y ) && textRect.Width > 0 )
                    {
                        textRect.Width -= 1;
                    }

                    while ( !path.IsVisible( textRect.X, textRect.Y ) && textRect.Width > 0 )
                    {
                        textRect.X += 1;
                        textRect.Width -= 1;
                    }
                }
                else
                {
                    while ( !path.IsVisible( textRect.X, textRect.Bottom ) && textRect.Height > 0 )
                    {
                        textRect.Height -= 1;
                    }

                    while ( !path.IsVisible( textRect.X, textRect.Y ) && textRect.Height > 0 )
                    {
                        textRect.Y += 1;
                        textRect.Height -= 1;
                    }
                }
            }

            return textRect;
        }

        /// <summary> Gets tab row. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="index"> Zero-based index of the. </param>
        /// <returns> The tab row. </returns>
        public int GetTabRow( int index )
        {
            // All calculations will use this rectangle as the base point
            // because the item size does not return the correct width.
            var rect = this.GetTabRect( index );
            int row = -1;
            switch ( this.Alignment )
            {
                case TabAlignment.Top:
                    {
                        row = (rect.Y - 2) / rect.Height;
                        break;
                    }

                case TabAlignment.Bottom:
                    {
                        row = (this.Height - rect.Y - 2) / rect.Height - 1;
                        break;
                    }

                case TabAlignment.Left:
                    {
                        row = (rect.X - 2) / rect.Width;
                        break;
                    }

                case TabAlignment.Right:
                    {
                        row = (this.Width - rect.X - 2) / rect.Width - 1;
                        break;
                    }
            }

            return row;
        }

        /// <summary> Gets tab position. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="index"> Zero-based index of the. </param>
        /// <returns> The tab position. </returns>
        public Point GetTabPosition( int index )
        {

            // If we are not multi line then the column is the index and the row is 0.
            if ( !this.Multiline )
            {
                return new Point( 0, index );
            }

            // If there is only one row then the column is the index
            if ( this.RowCount == 1 )
            {
                return new Point( 0, index );
            }

            // We are in a true multi-row scenario
            int row = this.GetTabRow( index );
            var rect = this.GetTabRect( index );
            int column = -1;

            // Scan from left to right along rows, skipping to next row if it is not the one we want.
            for ( int testIndex = 0, loopTo = this.TabCount - 1; testIndex <= loopTo; testIndex++ )
            {
                var testRect = this.GetTabRect( testIndex );
                if ( this.Alignment <= TabAlignment.Bottom )
                {
                    if ( testRect.Y == rect.Y )
                    {
                        column += 1;
                    }
                }
                else if ( testRect.X == rect.X )
                {
                    column += 1;
                }

                if ( testRect.Location.Equals( rect.Location ) )
                {
                    return new Point( row, column );
                }
            }

            return new Point( 0, 0 );
        }

        /// <summary> Query if 'index' is first tab in row. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="index"> Zero-based index of the. </param>
        /// <returns> True if first tab in row, false if not. </returns>
        public bool IsFirstTabInRow( int index )
        {
            if ( index < 0 )
            {
                return false;
            }

            bool firstTabinRow = index == 0;
            if ( !firstTabinRow )
            {
                if ( this.Alignment <= TabAlignment.Bottom )
                {
                    if ( this.GetTabRect( index ).X == 2 )
                    {
                        firstTabinRow = true;
                    }
                }
                else if ( this.GetTabRect( index ).Y == 2 )
                {
                    firstTabinRow = true;
                }
            }

            return firstTabinRow;
        }

        /// <summary> Adds a page border. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="path">       Full pathname of the file. </param>
        /// <param name="pageBounds"> The page bounds. </param>
        /// <param name="tabBounds">  The tab bounds. </param>
        private void AddPageBorder( GraphicsPath path, Rectangle pageBounds, Rectangle tabBounds )
        {
            switch ( this.Alignment )
            {
                case TabAlignment.Top:
                    {
                        path.AddLine( tabBounds.Right, pageBounds.Y, pageBounds.Right, pageBounds.Y );
                        path.AddLine( pageBounds.Right, pageBounds.Y, pageBounds.Right, pageBounds.Bottom );
                        path.AddLine( pageBounds.Right, pageBounds.Bottom, pageBounds.X, pageBounds.Bottom );
                        path.AddLine( pageBounds.X, pageBounds.Bottom, pageBounds.X, pageBounds.Y );
                        path.AddLine( pageBounds.X, pageBounds.Y, tabBounds.X, pageBounds.Y );
                        break;
                    }

                case TabAlignment.Bottom:
                    {
                        path.AddLine( tabBounds.X, pageBounds.Bottom, pageBounds.X, pageBounds.Bottom );
                        path.AddLine( pageBounds.X, pageBounds.Bottom, pageBounds.X, pageBounds.Y );
                        path.AddLine( pageBounds.X, pageBounds.Y, pageBounds.Right, pageBounds.Y );
                        path.AddLine( pageBounds.Right, pageBounds.Y, pageBounds.Right, pageBounds.Bottom );
                        path.AddLine( pageBounds.Right, pageBounds.Bottom, tabBounds.Right, pageBounds.Bottom );
                        break;
                    }

                case TabAlignment.Left:
                    {
                        path.AddLine( pageBounds.X, tabBounds.Y, pageBounds.X, pageBounds.Y );
                        path.AddLine( pageBounds.X, pageBounds.Y, pageBounds.Right, pageBounds.Y );
                        path.AddLine( pageBounds.Right, pageBounds.Y, pageBounds.Right, pageBounds.Bottom );
                        path.AddLine( pageBounds.Right, pageBounds.Bottom, pageBounds.X, pageBounds.Bottom );
                        path.AddLine( pageBounds.X, pageBounds.Bottom, pageBounds.X, tabBounds.Bottom );
                        break;
                    }

                case TabAlignment.Right:
                    {
                        path.AddLine( pageBounds.Right, tabBounds.Bottom, pageBounds.Right, pageBounds.Bottom );
                        path.AddLine( pageBounds.Right, pageBounds.Bottom, pageBounds.X, pageBounds.Bottom );
                        path.AddLine( pageBounds.X, pageBounds.Bottom, pageBounds.X, pageBounds.Y );
                        path.AddLine( pageBounds.X, pageBounds.Y, pageBounds.Right, pageBounds.Y );
                        path.AddLine( pageBounds.Right, pageBounds.Y, pageBounds.Right, tabBounds.Y );
                        break;
                    }
            }
        }

        /// <summary> Gets tab image rectangle. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="index"> Zero-based index of the. </param>
        /// <returns> The tab image rectangle. </returns>
        private Rectangle GetTabImageRect( int index )
        {
            using var tabBorderPath = this._StyleProvider.GetTabBorder( index );
            return this.GetTabImageRect( tabBorderPath );
        }

        /// <summary> Gets tab image rectangle. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="tabBorderPath"> Full pathname of the tab border file. </param>
        /// <returns> The tab image rectangle. </returns>
        private Rectangle GetTabImageRect( GraphicsPath tabBorderPath )
        {
            var rect = tabBorderPath.GetBounds();

            // Make it shorter or thinner to fit the height or width because of the padding added to the tab for painting
            switch ( this.Alignment )
            {
                case TabAlignment.Top:
                    {
                        rect.Y += 4f;
                        rect.Height -= 6f;
                        break;
                    }

                case TabAlignment.Bottom:
                    {
                        rect.Y += 2f;
                        rect.Height -= 6f;
                        break;
                    }

                case TabAlignment.Left:
                    {
                        rect.X += 4f;
                        rect.Width -= 6f;
                        break;
                    }

                case TabAlignment.Right:
                    {
                        rect.X += 2f;
                        rect.Width -= 6f;
                        break;
                    }
            }

            Rectangle imageRect;
            // Ensure image is fully visible
            if ( this.Alignment <= TabAlignment.Bottom )
            {
                if ( (this._StyleProvider.ImageAlign & NativeMethods.AnyLeftAlign) != ( ContentAlignment ) Conversions.ToInteger( 0 ) )
                {
                    imageRect = new Rectangle( ( int ) Math.Truncate( rect.X ), ( int ) Math.Truncate( rect.Y ) + ( int ) Math.Truncate( Math.Floor( (( int ) Math.Truncate( rect.Height ) - 16) / 2d ) ), 16, 16 );
                    while ( !tabBorderPath.IsVisible( imageRect.X, imageRect.Y ) )
                    {
                        imageRect.X += 1;
                    }

                    imageRect.X += 4;
                }
                else if ( (this._StyleProvider.ImageAlign & NativeMethods.AnyCenterAlign) != ( ContentAlignment ) Conversions.ToInteger( 0 ) )
                {
                    imageRect = new Rectangle( ( int ) Math.Truncate( rect.X ) + ( int ) Math.Truncate( Math.Floor( ( double ) ((( int ) Math.Truncate( rect.Right ) - ( int ) Math.Truncate( rect.X ) - ( int ) Math.Truncate( rect.Height ) + 2) / 2) ) ), ( int ) Math.Truncate( rect.Y ) + ( int ) Math.Truncate( Math.Floor( (( int ) Math.Truncate( rect.Height ) - 16) / 2d ) ), 16, 16 );
                }
                else
                {
                    imageRect = new Rectangle( ( int ) Math.Truncate( rect.Right ), ( int ) Math.Truncate( rect.Y ) + ( int ) Math.Truncate( Math.Floor( (( int ) Math.Truncate( rect.Height ) - 16) / 2d ) ), 16, 16 );
                    while ( !tabBorderPath.IsVisible( imageRect.Right, imageRect.Y ) )
                    {
                        imageRect.X -= 1;
                    }

                    imageRect.X -= 4;

                    // Move it in further to allow for the tab closer
                    if ( this._StyleProvider.ShowTabCloser && !this.RightToLeftLayout )
                    {
                        imageRect.X -= 10;
                    }
                }
            }
            else if ( (this._StyleProvider.ImageAlign & NativeMethods.AnyLeftAlign) != ( ContentAlignment ) Conversions.ToInteger( 0 ) )
            {
                imageRect = new Rectangle( ( int ) Math.Truncate( rect.X ) + ( int ) Math.Truncate( Math.Floor( (( int ) Math.Truncate( rect.Width ) - 16) / 2d ) ), ( int ) Math.Truncate( rect.Y ), 16, 16 );
                while ( !tabBorderPath.IsVisible( imageRect.X, imageRect.Y ) )
                {
                    imageRect.Y += 1;
                }

                imageRect.Y += 4;
            }
            else if ( (this._StyleProvider.ImageAlign & NativeMethods.AnyCenterAlign) != ( ContentAlignment ) Conversions.ToInteger( 0 ) )
            {
                imageRect = new Rectangle( ( int ) Math.Truncate( rect.X ) + ( int ) Math.Truncate( Math.Floor( (( int ) Math.Truncate( rect.Width ) - 16) / 2d ) ), ( int ) Math.Truncate( rect.Y ) + ( int ) Math.Truncate( Math.Floor( ( double ) ((( int ) Math.Truncate( rect.Bottom ) - ( int ) Math.Truncate( rect.Y ) - ( int ) Math.Truncate( rect.Width ) + 2) / 2) ) ), 16, 16 );
            }
            else
            {
                imageRect = new Rectangle( ( int ) Math.Truncate( rect.X ) + ( int ) Math.Truncate( Math.Floor( (( int ) Math.Truncate( rect.Width ) - 16) / 2d ) ), ( int ) Math.Truncate( rect.Bottom ), 16, 16 );
                while ( !tabBorderPath.IsVisible( imageRect.X, imageRect.Bottom ) )
                {
                    imageRect.Y -= 1;
                }

                imageRect.Y -= 4;

                // Move it in further to allow for the tab closer
                if ( this._StyleProvider.ShowTabCloser && !this.RightToLeftLayout )
                {
                    imageRect.Y -= 10;
                }
            }

            return imageRect;
        }

        /// <summary> Gets tab closer rectangle. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="index"> Zero-based index of the. </param>
        /// <returns> The tab closer rectangle. </returns>
        public Rectangle GetTabCloserRect( int index )
        {
            var closerRect = new Rectangle();
            using ( var path = this._StyleProvider.GetTabBorder( index ) )
            {
                var rect = path.GetBounds();

                // Make it shorter or thinner to fit the height or width because of the padding added to the tab for painting
                switch ( this.Alignment )
                {
                    case TabAlignment.Top:
                        {
                            rect.Y += 4f;
                            rect.Height -= 6f;
                            break;
                        }

                    case TabAlignment.Bottom:
                        {
                            rect.Y += 2f;
                            rect.Height -= 6f;
                            break;
                        }

                    case TabAlignment.Left:
                        {
                            rect.X += 4f;
                            rect.Width -= 6f;
                            break;
                        }

                    case TabAlignment.Right:
                        {
                            rect.X += 2f;
                            rect.Width -= 6f;
                            break;
                        }
                }

                if ( this.Alignment <= TabAlignment.Bottom )
                {
                    if ( this.RightToLeftLayout )
                    {
                        closerRect = new Rectangle( ( int ) Math.Truncate( rect.Left ), ( int ) Math.Truncate( rect.Y ) + ( int ) Math.Truncate( Math.Floor( (( int ) Math.Truncate( rect.Height ) - 6) / 2d ) ), 6, 6 );
                        while ( !path.IsVisible( closerRect.Left, closerRect.Y ) && closerRect.Right < this.Width )
                        {
                            closerRect.X += 1;
                        }

                        closerRect.X += 4;
                    }
                    else
                    {
                        closerRect = new Rectangle( ( int ) Math.Truncate( rect.Right ), ( int ) Math.Truncate( rect.Y ) + ( int ) Math.Truncate( Math.Floor( (( int ) Math.Truncate( rect.Height ) - 6) / 2d ) ), 6, 6 );
                        while ( !path.IsVisible( closerRect.Right, closerRect.Y ) && closerRect.Right > -6 )
                        {
                            closerRect.X -= 1;
                        }

                        closerRect.X -= 4;
                    }
                }
                else if ( this.RightToLeftLayout )
                {
                    closerRect = new Rectangle( ( int ) Math.Truncate( rect.X ) + ( int ) Math.Truncate( Math.Floor( (( int ) Math.Truncate( rect.Width ) - 6) / 2d ) ), ( int ) Math.Truncate( rect.Top ), 6, 6 );
                    while ( !path.IsVisible( closerRect.X, closerRect.Top ) && closerRect.Bottom < this.Height )
                    {
                        closerRect.Y += 1;
                    }

                    closerRect.Y += 4;
                }
                else
                {
                    closerRect = new Rectangle( ( int ) Math.Truncate( rect.X ) + ( int ) Math.Truncate( Math.Floor( (( int ) Math.Truncate( rect.Width ) - 6) / 2d ) ), ( int ) Math.Truncate( rect.Bottom ), 6, 6 );
                    while ( !path.IsVisible( closerRect.X, closerRect.Bottom ) && closerRect.Top > -6 )
                    {
                        closerRect.Y -= 1;
                    }

                    closerRect.Y -= 4;
                }
            }

            return closerRect;
        }

        /// <summary> Gets the mouse position. </summary>
        /// <value> The mouse position. </value>
        public new Point MousePosition
        {
            get {
                var loc = this.PointToClient( Control.MousePosition );
                if ( this.RightToLeftLayout )
                {
                    loc.X = this.Width - loc.X;
                }

                return loc;
            }
        }

        /// <summary> Helper method that inline assign. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="target"> [in,out] Target for the. </param>
        /// <param name="value">  The handler. </param>
        /// <returns> A T. </returns>
        private static T InlineAssignHelper<T>( ref T target, T value )
        {
            target = value;
            return value;
        }

        #endregion

    }
}
