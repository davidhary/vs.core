using System;
using System.Drawing;
using System.Runtime.InteropServices;

namespace isr.Core.Controls
{
    internal partial class NativeMethods
    {

        #region " WINDOWS STRUCTURES AND ENUMS "

        /// <summary> A bit-field of flags for specifying tchittestflags. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        [Flags]
        public enum TCHITTESTFLAGS
        {
            /// <summary> An enum constant representing the tcht nowhere option. </summary>
            TCHT_NOWHERE = 1,
            /// <summary> An enum constant representing the tcht onitemicon option. </summary>
            TCHT_ONITEMICON = 2,
            /// <summary> An enum constant representing the tcht onitemlabel option. </summary>
            TCHT_ONITEMLABEL = 4,
            /// <summary> An enum constant representing the tcht onitem option. </summary>
            TCHT_ONITEM = TCHT_ONITEMICON | TCHT_ONITEMLABEL
        }

        /// <summary> A TCHITTESTINFO. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        [StructLayout(LayoutKind.Sequential)]
        public struct TCHITTESTINFO
        {

            /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="location"> The location. </param>
            public TCHITTESTINFO(Point location)
            {
                pt = location;
                flags = TCHITTESTFLAGS.TCHT_ONITEM;
            }
            /// <summary> The point. </summary>
            public Point pt;
            /// <summary> The flags. </summary>
            public TCHITTESTFLAGS flags;
        }

        /// <summary> A paint structure. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        [StructLayout(LayoutKind.Sequential, Pack = 4)]
        public struct PAINTSTRUCT
        {

            /// <summary> A handle to the display DC to use for painting. </summary>
            public IntPtr hdc;
            /// <summary> Indicates whether the background should be erased. </summary>
            public int fErase;

            /// <summary> A RECT structure that specifies the upper left and lower right
        /// corners of the rectangle in which the painting is requested, </summary>
            public RECT rcPaint;

            /// <summary> Reserved; used internally by the system.  The restore. </summary>
            public int fRestore;

            /// <summary> Reserved; used internally by the system.  The restore. </summary>
            public int fIncUpdate;

            /// <summary> The RGB reserved. </summary>
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
            public byte[] rgbReserved;
        }

        /// <summary> A rectangle. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {

            /// <summary> The x-coordinate of the upper-left corner of the rectangle.  The left. </summary>
            public int left;

            /// <summary> The y-coordinate of the upper-left corner of the rectangle.  The left. </summary>
            public int top;

            /// <summary> The x-coordinate of the lower-right corner of the rectangle.  The left. </summary>
            public int right;

            /// <summary> The y-coordinate of the lower-right corner of the rectangle.  The left. </summary>
            public int bottom;

            /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="left">   The left. </param>
        /// <param name="top">    The top. </param>
        /// <param name="right">  The right. </param>
        /// <param name="bottom"> The bottom. </param>
            public RECT(int left, int top, int right, int bottom)
            {
                this.left = left;
                this.top = top;
                this.right = right;
                this.bottom = bottom;
            }

            /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="r"> A Rectangle to process. </param>
            public RECT(Rectangle r)
            {
                left = r.Left;
                top = r.Top;
                right = r.Right;
                bottom = r.Bottom;
            }

            /// <summary> From X, Y, W, H. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="x">      The x coordinate. </param>
        /// <param name="y">      The y coordinate. </param>
        /// <param name="width">  The width. </param>
        /// <param name="height"> The height. </param>
        /// <returns> A RECT. </returns>
            public static RECT FromXYWH(int x, int y, int width, int height)
            {
                return new RECT(x, y, x + width, y + height);
            }

            /// <summary> Initializes this object from the given int pointer. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="ptr"> The pointer. </param>
        /// <returns> A RECT. </returns>
            public static RECT FromIntPtr(IntPtr ptr)
            {
                RECT rect = (RECT)Marshal.PtrToStructure(ptr, typeof(RECT));
                return rect;
            }

            /// <summary> Gets the size. </summary>
        /// <value> The size. </value>
            public Size Size
            {
                get
                {
                    return new Size(right - left, bottom - top);
                }
            }
        }

        #endregion

        #region " WINDOWS CONSTANTS "

        /// <summary> The windows message gettabrect. </summary>
        public const int WM_GETTABRECT = 0x130A;

        /// <summary> The ws exception transparent. </summary>
        public const int WS_EX_TRANSPARENT = 0x20;

        /// <summary> The windows message setfont. </summary>
        public const int WM_SETFONT = 0x30;

        /// <summary> The windows message fontchange. </summary>
        public const int WM_FONTCHANGE = 0x1D;

        /// <summary> The windows message hscroll. </summary>
        public const int WM_HSCROLL = 0x114;

        /// <summary> The tcm hittest. </summary>
        public const int TCM_HITTEST = 0x130D;

        /// <summary> The windows message paint. Sent when the system makes a request to paint (a portion of) a window
        /// </summary>
        public const int WM_PAINT = 0xF;

        /// <summary> The ws exception layoutrtl. </summary>
        public const int WS_EX_LAYOUTRTL = 0x400000;

        /// <summary> The ws exception noinheritlayout. </summary>
        public const int WS_EX_NOINHERITLAYOUT = 0x100000;

        #endregion

        #region " CONTENT ALIGNMENT "

        /// <summary> any right align. </summary>
        public const ContentAlignment AnyRightAlign = ContentAlignment.BottomRight | ContentAlignment.MiddleRight | ContentAlignment.TopRight;

        /// <summary> any left align. </summary>
        public const ContentAlignment AnyLeftAlign = ContentAlignment.BottomLeft | ContentAlignment.MiddleLeft | ContentAlignment.TopLeft;

        /// <summary> any top align. </summary>
        public const ContentAlignment AnyTopAlign = ContentAlignment.TopRight | ContentAlignment.TopCenter | ContentAlignment.TopLeft;

        /// <summary> any bottom align. </summary>
        public const ContentAlignment AnyBottomAlign = ContentAlignment.BottomRight | ContentAlignment.BottomCenter | ContentAlignment.BottomLeft;

        /// <summary> any middle align. </summary>
        public const ContentAlignment AnyMiddleAlign = ContentAlignment.MiddleRight | ContentAlignment.MiddleCenter | ContentAlignment.MiddleLeft;

        /// <summary> any center align. </summary>
        public const ContentAlignment AnyCenterAlign = ContentAlignment.BottomCenter | ContentAlignment.MiddleCenter | ContentAlignment.TopCenter;

        #endregion

    }
}
