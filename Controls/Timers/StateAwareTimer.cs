using System;
using System.Diagnostics;

using isr.Core.Controls.ExceptionExtensions;

namespace isr.Core.Controls
{

    /// <summary> A timer with state management. </summary>
    /// <remarks>
    /// (c) 2008 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2013-11-08 Added to the Core Controls.  </para><para>
    /// David, 2009-05-07, 1.1.3414 Sync locks timer and dispose methods.  </para><para>
    /// David, 2008-01-30, 1.0.2951. </para>
    /// </remarks>
    public class StateAwareTimer : System.Timers.Timer
    {

        /// <summary> Constructs this class. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public StateAwareTimer() : base()
        {

            base.Elapsed += ( object sender, System.Timers.ElapsedEventArgs e ) => this.OnElapsed();
            this.TimerState = TimerStates.None;
            this._OnTickLocker = new object();
        }

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructor specifying a <paramref name="synchronizer">synchronizing object.</paramref>
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="synchronizer"> The form where this class resides. </param>
        public StateAwareTimer( System.ComponentModel.ISynchronizeInvoke synchronizer ) : this()
        {
            this.SynchronizingObject = synchronizer;
            // prefixing Synchronizing object with MyBase causes warning code AD0001
        }

        /// <summary>
        /// Gets the dispose status sentinel of the base class.  This applies to the derived class
        /// provided proper implementation.
        /// </summary>
        /// <value> The is disposed. </value>
        protected bool IsDisposed { get; set; }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        protected override void Dispose( bool disposing )
        {
            lock ( this._OnTickLocker )
            {
                try
                {
                    if ( !this.IsDisposed && disposing )
                    {
                        this.RemoveTickEventHandler( Tick );
                        if ( this.SynchronizingObject is object )
                        {
                            this.SynchronizingObject = null;
                        }
                    }
                }
                finally
                {
                    this.IsDisposed = true;
                    base.Dispose( disposing );
                }
            }
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> Gets the timer state. </summary>
        /// <value> The timer state. </value>
        public TimerStates TimerState { get; private set; }

        /// <summary> Gets a True if the timer is processing an event. </summary>
        /// <value> The is in tick. </value>
        public bool IsInTick => (this.TimerState & TimerStates.InTick) != 0;

        /// <summary> Gets a True if timer is in Running state. </summary>
        /// <value> The is running. </value>
        public bool IsRunning => (this.TimerState & TimerStates.Running) != 0;

        /// <summary> Gets a True if timer is in Stopping state. </summary>
        /// <value> The is stopping. </value>
        public bool IsStopping => (this.TimerState & TimerStates.Stopping) != 0;

        /// <summary>
        /// Gets or sets a condition for invoking the call back tick event asynchronously.  Must have a
        /// <see cref="System.Timers.Timer.SynchronizingObject">synchronizing object</see>
        /// for using asynchronous invocation.
        /// </summary>
        /// <value> The asynchronous invoke. </value>
        public bool AsynchronousInvoke { get; set; }

        /// <summary> Start or stops event raising. </summary>
        /// <value> The enabled. </value>
        public new bool Enabled
        {
            get => base.Enabled;

            set => _ = value ? this.Start() : this.Stop();
        }

        #endregion

        #region " METHODS "

        /// <summary> Starts this instance. </summary>
        /// <returns> <c>True</c> if started (enabled), <c>False</c> otherwise. </returns>
        public new bool Start()
        {
            lock ( this._OnTickLocker )
            {
                if ( this.IsDisposed )
                {
                    return false;
                }

                base.Start();
                this.TimerState = TimerStates.Running;
                return base.Enabled;
            }
        }

        /// <summary> Stops the timer. </summary>
        /// <returns> <c>True</c> if stopped (not enabled), <c>False</c> otherwise. </returns>
        public new bool Stop()
        {
            lock ( this._OnTickLocker )
            {
                if ( this.IsDisposed )
                {
                    return true;
                }

                if ( this.IsRunning )
                {
                    this.TimerState |= TimerStates.Stopping;
                }

                base.Stop();
                this.TimerState = TimerStates.None;
                return !base.Enabled;
            }
        }

        #endregion

        #region " TICK EVENT "

        /// <summary> Handles tick events </summary>
        public event EventHandler<EventArgs> Tick;

        /// <summary> Removes event handler. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The handler. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void RemoveTickEventHandler( EventHandler<EventArgs> value )
        {
            foreach ( Delegate d in value is null ? (Array.Empty<Delegate>()) : value.GetInvocationList() )
            {
                try
                {
                    Tick -= ( EventHandler<EventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }
        }

        /// <summary>   Raises the tick event. </summary>
        /// <remarks>   David, 2020-09-24. </remarks>
        /// <param name="e">    Event information to send to registered event handlers. </param>
        private void OnTickThis( EventArgs e )
        {
            var evt = Tick;
            evt?.Invoke( this, e );
        }

        /// <summary> Raises the Notify event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> Specifies the event <see cref="EventArgs">arguments</see>. </param>
        protected void OnTick( EventArgs e )
        {
            if ( this.SynchronizingObject is object )
            {
                if ( this.AsynchronousInvoke )
                {
                    _ = this.SynchronizingObject.BeginInvoke( new Action<EventArgs>( this.OnTickThis ), new object[] { e } );
                }
                else if ( this.SynchronizingObject is object && this.SynchronizingObject.InvokeRequired )
                {
                    _ = this.SynchronizingObject.Invoke( new Action<EventArgs>( this.OnTickThis ), new object[] { e } );
                }
                else
                {
                    this.OnTickThis( e );
                }
            }
        }

        /// <summary> The on tick locker. </summary>
        private readonly object _OnTickLocker;

        /// <summary>   Executes the 'elapsed' action. </summary>
        /// <remarks>   David, 2020-09-24. </remarks>
        protected void OnElapsed()
        {
            base.Stop();
            lock ( this._OnTickLocker )
            {
                if ( this.IsDisposed )
                {
                    return;
                }

                this.TimerState |= TimerStates.InTick;
                this.OnTick( EventArgs.Empty );
                this.TimerState = this.IsStopping ? TimerStates.None : TimerStates.Running;
            }

            if ( this.TimerState == TimerStates.Running )
            {
                base.Start();
            }
        }

        #endregion

    }

    /// <summary> Enumerates the timer states. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    [Flags()]
    public enum TimerStates
    {
        /// <summary>   A binary constant representing the none flag. </summary>
        None = 0,

        /// <summary> An enum constant representing the running option. </summary>
        Running = 1 << 0,

        /// <summary> An enum constant representing the stopping option. </summary>
        Stopping = 1 << 1,

        /// <summary> An enum constant representing the in tick option. </summary>
        InTick = 1 << 2
    }
}
