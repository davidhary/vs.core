﻿using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Controls
{
    [DesignerGenerated()]
    public partial class TreePanel
    {
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            __NavigatorTreeView = new TreeView();
            __NavigatorTreeView.BeforeSelect += new TreeViewCancelEventHandler(NavigatorTreeView_BeforeSelect);
            __NavigatorTreeView.AfterSelect += new TreeViewEventHandler(NavigatorTreeView_AfterSelect);
            ((System.ComponentModel.ISupportInitialize)this).BeginInit();
            Panel1.SuspendLayout();
            SuspendLayout();
            // 
            // _NavigatorTreeView
            // 
            __NavigatorTreeView.Dock = DockStyle.Fill;
            __NavigatorTreeView.LineColor = Color.Empty;
            __NavigatorTreeView.Location = new Point(0, 0);
            __NavigatorTreeView.Name = "__NavigatorTreeView";
            __NavigatorTreeView.Size = new Size(50, 100);
            __NavigatorTreeView.TabIndex = 0;
            // 
            // TreePanel
            // 
            Font = new Font("Segoe UI", 9.75f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            // 
            // 
            // 
            Panel1.Controls.Add(__NavigatorTreeView);
            Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)this).EndInit();
            ResumeLayout(false);
        }

        private TreeView __NavigatorTreeView;

        private TreeView _NavigatorTreeView
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __NavigatorTreeView;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__NavigatorTreeView != null)
                {
                    __NavigatorTreeView.BeforeSelect -= NavigatorTreeView_BeforeSelect;
                    __NavigatorTreeView.AfterSelect -= NavigatorTreeView_AfterSelect;
                }

                __NavigatorTreeView = value;
                if (__NavigatorTreeView != null)
                {
                    __NavigatorTreeView.BeforeSelect += NavigatorTreeView_BeforeSelect;
                    __NavigatorTreeView.AfterSelect += NavigatorTreeView_AfterSelect;
                }
            }
        }
    }
}