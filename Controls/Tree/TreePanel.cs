using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

using isr.Core.Controls.ExceptionExtensions;

namespace isr.Core.Controls
{

    /// <summary> A tree split panel for holding multiple controls. </summary>
    /// <remarks> David, 2020-09-02. </remarks>
    public partial class TreePanel : SplitContainer
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public TreePanel() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this._NavigatorTreeView.Nodes.Clear();
            this.TreeNodes = new Dictionary<string, TreeNode>();
            this.NodeControls = new Dictionary<string, Control>();
            this.NodesVisited = new List<string>();
            this.InitializingComponents = false;
            this.__NavigatorTreeView.Name = "_NavigatorTreeView";
        }

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Gets the initializing components. </summary>
        /// <value> The initializing components. </value>
        protected bool InitializingComponents { get; set; }

        /// <summary> Creates a new <see cref="TreePanel"/> </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> A <see cref="TreePanel"/>. </returns>
        public static TreePanel Create()
        {
            TreePanel control = null;
            try
            {
                control = new TreePanel();
                return control;
            }
            catch
            {
                control?.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    this._NavigatorTreeView?.Dispose();
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " TREE NODES "

        /// <summary> Clears the nodes. </summary>
        /// <remarks> David, 2020-09-03. </remarks>
        public void ClearNodes()
        {
            this._NavigatorTreeView.Nodes.Clear();
            this.TreeNodes.Clear();
            this.NodeControls.Clear();
            this.NodesVisited.Clear();
        }

        /// <summary> Gets the number of nodes. </summary>
        /// <value> The number of nodes. </value>
        public int NodeCount => this._NavigatorTreeView.Nodes.Count;

        /// <summary> Removes the node. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="nodeName"> Name of the node. </param>
        public void RemoveNode( string nodeName )
        {
            _ = this.TreeNodes.Remove( nodeName );
            _ = this.NodeControls.Remove( nodeName );
            this._NavigatorTreeView.Nodes.RemoveByKey( nodeName );
        }

        /// <summary> Adds a node. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="nodeName">    Name of the node. </param>
        /// <param name="nodeCaption"> The node caption. </param>
        /// <param name="nodeControl"> The node control. </param>
        /// <returns> A TreeNode. </returns>
        public TreeNode AddNode( string nodeName, string nodeCaption, Control nodeControl )
        {
            var newNode = new TreeNode() { Name = nodeName, Text = nodeCaption };
            this.TreeNodes.Add( nodeName, newNode );
            _ = this._NavigatorTreeView.Nodes.Add( this.TreeNodes[nodeName] );
            this.NodeControls.Add( nodeName, nodeControl );
            return newNode;
        }

        /// <summary> Inserts a node. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="index">       Zero-based index of the. </param>
        /// <param name="nodeName">    Name of the node. </param>
        /// <param name="nodeCaption"> The node caption. </param>
        /// <param name="nodeControl"> The node control. </param>
        /// <returns> A TreeNode. </returns>
        public TreeNode InsertNode( int index, string nodeName, string nodeCaption, Control nodeControl )
        {
            var newNode = new TreeNode() { Name = nodeName, Text = nodeCaption };
            this.TreeNodes.Add( nodeName, newNode );
            this._NavigatorTreeView.Nodes.Insert( index, this.TreeNodes[nodeName] );
            this.NodeControls.Add( nodeName, nodeControl );
            return newNode;
        }

        /// <summary> Gets a node. </summary>
        /// <remarks> David, 2020-09-03. </remarks>
        /// <param name="nodeName"> Name of the node. </param>
        /// <returns> The node. </returns>
        public TreeNode GetNode( string nodeName )
        {
            return this.TreeNodes.ContainsKey( nodeName ) ? this.TreeNodes[nodeName] : new TreeNode();
        }

        #endregion

        #region " NAVIGATION "

        /// <summary> Gets a dictionary of node controls. </summary>
        /// <value> A dictionary of node controls. </value>
        public IDictionary<string, Control> NodeControls { get; private set; }

        /// <summary> Gets a dictionary of <see cref="TreeNode"/>. </summary>
        /// <value> A dictionary of nodes. </value>
        public IDictionary<string, TreeNode> TreeNodes { get; private set; }

        /// <summary> Gets the last node selected. </summary>
        /// <value> The last node selected. </value>
        public TreeNode LastNodeSelected { get; private set; }

        /// <summary> Gets the last tree view node selected. </summary>
        /// <value> The last tree view node selected. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        protected string LastTreeViewNodeSelected => this.LastNodeSelected is null ? string.Empty : this.LastNodeSelected.Name;

        /// <summary> Gets or sets the nodes visited. </summary>
        /// <value> The nodes visited. </value>
        private IList<string> NodesVisited { get; set; }

        /// <summary> Select active control. </summary>
        /// <remarks> David, 2020-09-10. </remarks>
        /// <param name="nodeName"> Name of the node. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        protected virtual bool SelectActiveControl( string nodeName )
        {
            if ( this.NodeControls.ContainsKey( nodeName ) )
            {
                var activeControl = this.NodeControls[nodeName];

                // turn off the visibility of the current panel, this turns off the visibility of the
                // contained controls, which will now be removed from the panel.
                this.Panel2.Hide();
                this.Panel2.Controls.Clear();
                if ( activeControl is object )
                {
                    activeControl.Dock = DockStyle.None;
                    this.Panel2.Controls.Add( activeControl );
                    activeControl.Dock = DockStyle.Fill;
                    activeControl.Visible = true;
                }

                // turn on visibility on the panel -- this toggles the visibility of the contained controls,
                // which is required for the messages boxes.
                this.Panel2.Show();
                if ( !this.NodesVisited.Contains( nodeName ) )
                {
                    this.NodesVisited.Add( nodeName );
                }

                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary> Called after a node is selected. Displays to relevant screen. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="nodeName"> The node. </param>
        protected virtual void OnNodeSelected( string nodeName )
        {
            _ = this.SelectActiveControl( nodeName );
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="Console"/> is navigating.
        /// </summary>
        /// <remarks>
        /// Used to ignore changes in grids during the navigation. The grids go through selecting their
        /// rows when navigating.
        /// </remarks>
        /// <value> <c>True</c> if navigating; otherwise, <c>False</c>. </value>
        public bool Navigating { get; private set; }

        /// <summary> Handles the BeforeSelect event of the _NavigatorTreeView control. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="TreeViewCancelEventArgs"/> instance
        /// containing the event data. </param>
        private void NavigatorTreeView_BeforeSelect( object sender, TreeViewCancelEventArgs e )
        {
            if ( e is object )
            {
                if ( this.NodeControls.ContainsKey( e.Node.Name ) )
                {
                    var activeControl = this.NodeControls[e.Node.Name];
                    if ( activeControl is object )
                    {
                        activeControl.Visible = false;
                    }

                    this.Navigating = true;
                }
            }
        }

        /// <summary> Handles the AfterSelect event of the Me._NavigatorTreeView control. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="TreeViewEventArgs" /> instance
        /// containing the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void NavigatorTreeView_AfterSelect( object sender, TreeViewEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
            {
                return;
            }

            string activity;
            try
            {
                activity = $"{this.Name} handling navigator after select event";
                if ( this.LastNodeSelected is object )
                {
                    this.LastNodeSelected.BackColor = this._NavigatorTreeView.BackColor;
                }

                if ( (e.Node?.IsSelected).GetValueOrDefault( false ) )
                {
                    this.LastNodeSelected = e.Node;
                    e.Node.BackColor = SystemColors.Highlight;
                    this.OnNodeSelected( this.LastTreeViewNodeSelected );
                    var evt = AfterNodeSelected;
                    evt?.Invoke( this, e );
                }
            }
            catch ( Exception ex )
            {
                this.OnNavigationError( true, new System.IO.ErrorEventArgs( ex ) );
            }
            finally
            {
                this.Navigating = false;
            }
        }

        /// <summary> Select navigator tree view first node. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public void SelectNavigatorTreeViewFirstNode()
        {
            if ( this.NodeCount > 0 )
            {
                this._NavigatorTreeView.SelectedNode = this._NavigatorTreeView.Nodes[0];
            }
        }

        /// <summary> Selects the navigator tree view node. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="nodeName"> The node. </param>
        public void SelectNavigatorTreeViewNode( string nodeName )
        {
            this._NavigatorTreeView.SelectedNode = this._NavigatorTreeView.Nodes[nodeName];
        }

        /// <summary> Event queue for all listeners interested in AfterNodeSelected events. </summary>
        public event EventHandler<TreeViewEventArgs> AfterNodeSelected;

        #endregion

        #region " NAVIGATION ERROR "

        /// <summary> Event queue for all listeners interested in NavigationError events. </summary>
        public event EventHandler<System.IO.ErrorEventArgs> NavigationError;

        /// <summary> Raises the <see cref="System.IO.ErrorEventArgs"/>. </summary>
        /// <remarks> David, 2020-09-01. </remarks>
        /// <param name="displayErrorDialogIfNoHandler"> True to display error dialog if no handler. </param>
        /// <param name="e">                             Event information to send to registered event
        /// handlers. </param>
        protected virtual void OnNavigationError( bool displayErrorDialogIfNoHandler, System.IO.ErrorEventArgs e )
        {
            var evt = NavigationError;
            if ( evt is null )
            {
                if ( displayErrorDialogIfNoHandler )
                {
                    _ = MessageBox.Show( "Navigation Exception", e.GetException().ToFullBlownString() );
                }
            }
            else
            {
                evt.Invoke( this, new System.IO.ErrorEventArgs( e.GetException() ) );
            }
        }

        #endregion

    }
}
