using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Management;
using System.Windows.Forms;

using isr.Core.Controls.ExceptionExtensions;

namespace isr.Core.Controls
{

    /// <summary> A folder tree view. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-07-04 </para>
    /// </remarks>
    [Description( "Folder Tree View" )]
    [ToolboxBitmap( typeof( FolderTreeViewPassive ), "FolderTreeViewPassive.gif" )]
    public class FolderTreeViewPassive : TreeView
    {

        #region " CONSTRUCTION "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public FolderTreeViewPassive() : base()
        {
            var resources = new System.Resources.ResourceManager( typeof( FolderTreeViewPassive ) );
            this.ImageList = new ImageList() {
                ColorDepth = ColorDepth.Depth8Bit,
                ImageSize = new Size( 16, 16 ),
                ImageStream = ( ImageListStreamer ) resources.GetObject( "ImageStream" ),
                TransparentColor = Color.Transparent
            };

            // Populate TreeView with Drive list
            this.PopulateDriveList();
        }

        #endregion

        #region " BEHAVIOR "

        /// <summary> Populate drive list. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public void PopulateDriveList()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.PopulateDriveListThis();
            }
            catch
            {
                throw;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Populate drive list. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void PopulateDriveListThis()
        {
            TreeNode nodeTreeNode;
            const int Removable = 2;
            const int LocalDisk = 3;
            const int Network = 4;
            const int CD = 5;

            // clear TreeView
            this.Nodes.Clear();
            nodeTreeNode = new TreeNode( "My Computer", 0, 0 ) { Name = string.Empty };
            _ = this.Nodes.Add( nodeTreeNode );
            this.CurrentNode = nodeTreeNode;

            // set node collection
            var nodeCollection = nodeTreeNode.Nodes;

            // Get Drive list
            var queryCollection = SelectDrives();
            foreach ( ManagementObject mo in queryCollection )
            {
                int imageIndex;
                int selectIndex;
                switch ( int.Parse( mo["DriveType"].ToString() ) )
                {
                    case Removable: // removable drives
                        {
                            imageIndex = 5;
                            selectIndex = 5;
                            break;
                        }

                    case LocalDisk: // Local drives
                        {
                            imageIndex = 6;
                            selectIndex = 6;
                            break;
                        }

                    case CD: // CD-Rom drives
                        {
                            imageIndex = 7;
                            selectIndex = 7;
                            break;
                        }

                    case Network: // Network drives
                        {
                            imageIndex = 8;
                            selectIndex = 8; // defaults to folder image
                            break;
                        }

                    default:
                        {
                            imageIndex = 2;
                            selectIndex = 3;
                            break;
                        }
                }
                // create new drive node
                nodeTreeNode = new TreeNode( mo["Name"].ToString() + @"\", imageIndex, selectIndex );

                // add new node
                _ = nodeCollection.Add( nodeTreeNode );
            }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.TreeView.AfterSelect" /> event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.TreeViewEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnAfterSelect( TreeViewEventArgs e )
        {
            if ( e is null )
            {
                return;
            }
            // get current selected drive or folder
            var selectedNode = e.Node;

            // clear all sub-folders
            selectedNode.Nodes.Clear();
            if ( selectedNode.SelectedImageIndex == 0 )
            {
                // Selected My Computer - repopulate drive list
                this.PopulateDriveList();
            }
            else
            {
                // populate sub-folders and folder files
                PopulateDirectory( selectedNode, selectedNode.Nodes );
            }

            this.CurrentNode = selectedNode;
            base.OnAfterSelect( e );
        }

        /// <summary> Populate directory. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="currentNode">           The current node. </param>
        /// <param name="currentNodeCollection"> current Collection of nodes. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private static void PopulateDirectory( TreeNode currentNode, TreeNodeCollection currentNodeCollection )
        {
            if ( currentNode is null || currentNodeCollection is null )
            {
                return;
            }

            TreeNode nodeDir;
            int imageIndex = 2; // unselected image index
            int selectIndex = 3; // selected image index
            if ( currentNode.SelectedImageIndex != 0 )
            {
                // populate tree view with folders
                try
                {
                    // check path
                    if ( Directory.Exists( ParseFullPath( currentNode.FullPath ) ) == false )
                    {
                        _ = MessageBox.Show( $"Directory or path {currentNode.Name} does not exist." );
                    }
                    else
                    {
                        var stringDirectories = Directory.GetDirectories( ParseFullPath( currentNode.FullPath ) );
                        string fullPath = string.Empty;
                        string pathName = string.Empty;

                        // loop through all directories
                        foreach ( string directoryName in stringDirectories )
                        {
                            fullPath = directoryName;
                            pathName = ParsePathName( fullPath );

                            // create node for directories
                            nodeDir = new TreeNode( pathName, imageIndex, selectIndex ) { Name = fullPath };
                            _ = currentNodeCollection.Add( nodeDir );
                        }
                    }
                }
                catch ( IOException )
                {
                    _ = MessageBox.Show( "Error: Drive not ready or directory does not exist." );
                }
                catch ( UnauthorizedAccessException )
                {
                    _ = MessageBox.Show( "Error: Drive or directory access denied." );
                }
                catch ( Exception ex )
                {
                    _ = MessageBox.Show( $"Error: {ex.ToFullBlownString()}" );
                }
            }
        }

        /// <summary> Parse path name. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="filePath"> Full pathname of the file. </param>
        /// <returns> A String. </returns>
        private static string ParsePathName( string filePath )
        {
            return string.IsNullOrWhiteSpace( filePath ) ? filePath : filePath.Split( '\\' ).Last();
        }

        /// <summary> Parse full path removing 'My Computer\'. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="compositeFolderPath"> The composite folder path including 'My Computer'. </param>
        /// <returns> A String. </returns>
        public static string ParseFullPath( string compositeFolderPath )
        {
            return string.IsNullOrWhiteSpace( compositeFolderPath ) ? compositeFolderPath : compositeFolderPath.Replace( @"My Computer\", "" );
        }

        /// <summary> Select drives. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> A ManagementObjectCollection. </returns>
        private static ManagementObjectCollection SelectDrives()
        {
            var query = new ManagementObjectSearcher( "SELECT * From Win32_LogicalDisk " );
            var queryCollection = query.Get();
            return queryCollection;
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> Searches for the first node. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="path"> The full pathname of the file. </param>
        public void FindNode( string path )
        {
            if ( Directory.Exists( path ) )
            {
                IEnumerable<TreeNode> nodes = this.Nodes.Find( path, true );
                this.CurrentNode = nodes.Any() ? nodes.First() : this.Nodes[0];
            }
        }

        /// <summary> The node. </summary>
        private TreeNode _Node;

        /// <summary> Gets or sets the current node. </summary>
        /// <value> The current node. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public TreeNode CurrentNode
        {
            get => this._Node;

            set {
                if ( value is object && (this.CurrentNode is null || !string.Equals( value.Name, this.CurrentNode.Name )) )
                {
                    this._Node = value;
                    this.Path = value.Name;
                }
            }
        }

        /// <summary> Gets or sets the full pathname of the file. </summary>
        /// <value> The full pathname of the file. </value>
        [DefaultValue( false )]
        [Description( "Current Path" )]
        [Category( "Appearance" )]
        public string Path
        {
            get => this.CurrentNode.Name;

            set {
                if ( !string.Equals( value, this.Path ) )
                {
                    this.FindNode( value );
                }
            }
        }

        #endregion


    }
}
