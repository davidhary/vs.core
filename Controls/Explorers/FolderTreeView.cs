using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace isr.Core.Controls
{

    /// <summary> Implements a tree view for exploring folders. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-07-06 </para><para>
    /// David, 2019-07-03, Uses tree view from
    /// https://www.codeproject.com/Articles/14570/A-Windows-Explorer-in-a-user-control
    /// </para>
    /// </remarks>
    [ToolboxBitmap( typeof( FolderTreeView ), "FolderTreeView.gif" )]
    [DefaultEvent( "PathChanged" )]
    public partial class FolderTreeView : TreeView
    {

        #region " CONSTRUCTION And CLEANUP "

        /// <summary> Gets or sets the initlizing components. </summary>
        /// <value> The initlizing components. </value>
        private bool InitlizingComponents { get; set; }

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public FolderTreeView() : base()
        {
            this.InitlizingComponents = true;
            this.ShowLines = false;
            this.ShowRootLines = false;
            var resources = new ComponentResourceManager( typeof( FolderTreeView ) );
            this.ImageList = new ImageList() {
                ImageStream = ( ImageListStreamer ) resources.GetObject( "_ImageList.ImageStream" ),
                TransparentColor = Color.Transparent
            };
            for ( int i = 0; i <= 28; i++ )
            {
                this.ImageList.Images.SetKeyName( i, string.Empty );
            }

            // List view managers the contiguous list of paths.
            this._PathColumnHeader = new ColumnHeader();
            this._StatusColumnHeader = new ColumnHeader();
            this._ListView = new System.Windows.Forms.ListView() {
                Name = "_ListView",
                HideSelection = false,
                UseCompatibleStateImageBehavior = false,
                View = View.Details,
                Visible = false
            };
            this._ListView.Columns.AddRange( new ColumnHeader[] { this._PathColumnHeader, this._StatusColumnHeader } );

            // Context menu
            base.ContextMenuStrip = this.CreateContextMenuStrip();
            this.ImageIndex = 0;
            this.SelectedImageIndex = 2;
            this.InitlizingComponents = false;
        }

        /// <summary> Clean up any resources being used. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> <see langword="true" /> to release both managed and unmanaged
        /// resources; <see langword="false" /> to release only unmanaged
        /// resources. </param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing )
            {
                this.ContextMenuStrip = null;
                if ( this._MyContextMenuStrip is object )
                {
                    this._MyContextMenuStrip.Dispose();
                }

                if ( this._ListView is object )
                {
                    this._ListView.Dispose();
                }

                this._ListView = null;
            }

            base.Dispose( disposing );
        }

        /// <summary> The list view control. </summary>
        private System.Windows.Forms.ListView _ListView;

        /// <summary> The path column header. </summary>
        private readonly ColumnHeader _PathColumnHeader;

        /// <summary> The status column header. </summary>
        private readonly ColumnHeader _StatusColumnHeader;

        #endregion

        #region " APPEARANCE "

        /// <summary> True to show, false to hide my documents. </summary>
        private bool _ShowMyDocuments = true;

        /// <summary> Show my documents. </summary>
        /// <value> The show my documents. </value>
        [Category( "Appearance" )]
        [Description( "Show my documents" )]
        [DefaultValue( true )]
        public bool ShowMyDocuments
        {
            get => this._ShowMyDocuments;

            set {
                if ( value != this.ShowMyDocuments )
                {
                    this._ShowMyDocuments = value;
                    if ( this.IsInitialized )
                    {
                        this.Refresh();
                    }
                }
            }
        }

        /// <summary> True to show, false to hide my favorites. </summary>
        private bool _ShowMyFavorites = true;

        /// <summary> True to show, false to hide my favorites. </summary>
        /// <value> The show my favorites. </value>
        [Category( "Appearance" )]
        [Description( "Show my favorites" )]
        [DefaultValue( true )]
        public bool ShowMyFavorites
        {
            get => this._ShowMyFavorites;

            set {
                if ( value != this.ShowMyFavorites )
                {
                    this._ShowMyFavorites = value;
                    if ( this.IsInitialized )
                    {
                        this.Refresh();
                    }
                }
            }
        }

        /// <summary> True to show, false to hide my network. </summary>
        private bool _ShowMyNetwork = true;

        /// <summary> True to show, false to hide my network. </summary>
        /// <value> The show my network. </value>
        [Category( "Appearance" )]
        [Description( "Show my network" )]
        [DefaultValue( true )]
        public bool ShowMyNetwork
        {
            get => this._ShowMyNetwork;

            set {
                if ( value != this.ShowMyNetwork )
                {
                    this._ShowMyNetwork = value;
                    if ( this.IsInitialized )
                    {
                        this.Refresh();
                    }
                }
            }
        }

        #endregion

        #region " EVENTS "

        /// <summary> Event queue for all listeners interested in PathChanged events. </summary>
        public event EventHandler<EventArgs> PathChanged;

        /// <summary> Notifies the path changed. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        protected void NotifyPathChanged()
        {
            if ( !this.InitlizingComponents )
            {
                var evt = PathChanged;
                evt?.Invoke( this, EventArgs.Empty );
            }
        }

        #endregion

        #region " BEHAVIOR "

        /// <summary> Name of the 'Home' path </summary>
        public const string HomePathName = "home";

        /// <summary> Full pathname of the selected file. </summary>
        private string _SelectedPath;

        /// <summary> Gets or sets the full pathname of the selected directory. </summary>
        /// <value> The full pathname of the selected directory. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public string SelectedPath
        {
            get => this._SelectedPath;

            protected set {
                if ( !string.Equals( value, this.SelectedPath, StringComparison.OrdinalIgnoreCase ) )
                {
                    this._CandidatePath = value;
                    this._SelectedPath = value;
                    this.NotifyPathChanged();
                }
            }
        }

        /// <summary> Select path. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="pathName"> Full pathname of the file. </param>
        public void SelectPath( string pathName )
        {
            this.SetCandidatePath( pathName );
            this.SelectMyComputerTreeNode( this.CandidatePath );
        }

        /// <summary> Full pathname of the candidate file. </summary>
        private string _CandidatePath;

        /// <summary> Gets or sets the full pathname of the Candidate path. </summary>
        /// <value> The full pathname of the Candidate path. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public string CandidatePath
        {
            get => this._CandidatePath;

            protected set {
                if ( !string.Equals( value, this.CandidatePath, StringComparison.OrdinalIgnoreCase ) )
                {
                    this._CandidatePath = value;
                }
            }
        }

        /// <summary> Sets candidate path. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="pathName"> Full pathname of the file. </param>
        public void SetCandidatePath( string pathName )
        {
            this.CandidatePath = string.Equals( pathName, HomePathName, StringComparison.OrdinalIgnoreCase ) ? Application.StartupPath : Directory.Exists( pathName ) ? pathName : Application.StartupPath;
        }

        /// <summary> my computer tree node. </summary>
        private TreeNode _MyComputerTreeNode;

        /// <summary> The root tree node. </summary>
        private TreeNode _RootTreeNode;

        /// <summary> Gets the is initialized. </summary>
        /// <value> The is initialized. </value>
        public bool IsInitialized => this._RootTreeNode is object;

        /// <summary> Initializes the nodes and explores the desktop folder. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public void InitializeKnownState()
        {
            this.Nodes.Clear();

            // Dim drives() As String = Environment.GetLogicalDrives()

            // Environment.UserDomainName .GetFolderPath( 
            // Environment.GetFolderPath (Environment.SystemDirectory);

            var desktopNode = new TreeNode() {
                Tag = Environment.GetFolderPath( Environment.SpecialFolder.Desktop ),
                Text = "Desktop",
                ImageIndex = 10,
                SelectedImageIndex = 10
            };
            _ = this.Nodes.Add( desktopNode );
            this._RootTreeNode = desktopNode;
            if ( this.ShowMyDocuments )
            {
                // Add My Documents and Desktop folder outside
                var myDocumentsNode = new TreeNode() {
                    Tag = Environment.GetFolderPath( Environment.SpecialFolder.Personal ),
                    Text = "My Documents",
                    ImageIndex = 9,
                    SelectedImageIndex = 9
                };
                _ = desktopNode.Nodes.Add( myDocumentsNode );
                _ = TryFillFilesAndDirectories( myDocumentsNode );
            }

            var myComputerNode = new TreeNode() {
                Tag = "My Computer",
                Text = "My Computer",
                ImageIndex = 12,
                SelectedImageIndex = 12
            };
            _ = desktopNode.Nodes.Add( myComputerNode );
            myComputerNode.EnsureVisible();
            this._MyComputerTreeNode = myComputerNode;
            var myNode = new TreeNode() {
                Tag = "my Node",
                Text = "my Node",
                ImageIndex = 12,
                SelectedImageIndex = 12
            };
            _ = myComputerNode.Nodes.Add( myNode );
            if ( this.ShowMyNetwork )
            {
                var myNetworkPlacesNode = new TreeNode() {
                    Tag = "My Network Places",
                    Text = "My Network Places",
                    ImageIndex = 13,
                    SelectedImageIndex = 13
                };
                _ = desktopNode.Nodes.Add( myNetworkPlacesNode );
                myNetworkPlacesNode.EnsureVisible();
                var entireNetworkNode = new TreeNode() {
                    Tag = "Entire Network",
                    Text = "Entire Network",
                    ImageIndex = 14,
                    SelectedImageIndex = 14
                };
                _ = myNetworkPlacesNode.Nodes.Add( entireNetworkNode );
                var networkNode = new TreeNode() {
                    Tag = "Network Node",
                    Text = "Network Node",
                    ImageIndex = 15,
                    SelectedImageIndex = 15
                };
                _ = entireNetworkNode.Nodes.Add( networkNode );
                entireNetworkNode.EnsureVisible();
            }

            if ( this.ShowMyFavorites )
            {
                var myFevoritesNode = new TreeNode() {
                    Tag = Environment.GetFolderPath( Environment.SpecialFolder.Favorites ),
                    Text = "My Favorites",
                    ImageIndex = 26,
                    SelectedImageIndex = 26
                };
                _ = desktopNode.Nodes.Add( myFevoritesNode );
                _ = TryFillFilesAndDirectories( myFevoritesNode );
            }

            ExploreTreeNode( desktopNode );
        }

        /// <summary> Explore tree node. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="parentNode"> The node. </param>
        private static void ExploreTreeNode( TreeNode parentNode )
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                // get directories
                _ = TryFillFilesAndDirectories( parentNode );
                // get directories one more level deep in current directory so user can see there is
                // more directories underneath current directory 
                foreach ( TreeNode subNode in parentNode.Nodes )
                {
                    if ( string.Equals( parentNode.Text, "Desktop", StringComparison.OrdinalIgnoreCase ) )
                    {
                        if ( string.Equals( subNode.Text, "My Documents", StringComparison.OrdinalIgnoreCase ) || string.Equals( subNode.Text, "My Computer", StringComparison.OrdinalIgnoreCase ) || string.Equals( subNode.Text, "Microsoft Windows Network", StringComparison.OrdinalIgnoreCase ) || string.Equals( subNode.Text, "My Network Places", StringComparison.OrdinalIgnoreCase ) )
                        {
                        }
                        else
                        {
                            _ = TryFillFilesAndDirectories( subNode );
                        }
                    }
                    else
                    {
                        _ = TryFillFilesAndDirectories( subNode );
                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        /// <summary> Gets the directories. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="parentNode"> The parent node. </param>
        private static void GetDirectories( TreeNode parentNode )
        {
            var dirList = Directory.GetDirectories( parentNode.Tag.ToString() );
            Array.Sort( dirList );

            // check if directory already exists in case click same directory twice
            if ( dirList.Length != parentNode.Nodes.Count )
            {
                // add each directory in selected director
                foreach ( string path in dirList )
                {
                    _ = parentNode.Nodes.Add( new TreeNode() {
                        Tag = path, // store path in tag
                        Text = path.Substring( path.LastIndexOf( @"\", StringComparison.OrdinalIgnoreCase ) + 1 ),
                        ImageIndex = 1
                    } );
                }
            }
        }

        /// <summary> Fill files and directories. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="communalNode"> The communal node. </param>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private static (bool Success, string Details) TryFillFilesAndDirectories( TreeNode communalNode )
        {
            try
            {
                GetDirectories( communalNode );
                return (true, string.Empty);
            }
            catch ( Exception ex )
            {
                return (false, ex.ToString());
            }
        }

        /// <summary> Refresh folders. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public void RefreshFolders()
        {
            this._ListView.Items.Clear();
            this.Nodes.Clear();
            // Me.SetCurrentPath(Environment.GetFolderPath(Environment.SpecialFolder.Personal))
            this.InitializeKnownState();
        }

        /// <summary> Select my computer tree node. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="searchPath"> Full pathname of the search file. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SelectMyComputerTreeNode( string searchPath )
        {
            if ( !this.IsInitialized )
            {
                this.InitializeKnownState();
            }

            int h = 1;
            var tn = this._MyComputerTreeNode;
            TreeNode selectedNode = null;
            this.ExploreMyComputer();
            StartAgain:
            ;
            do
            {
                foreach ( TreeNode t in tn.Nodes )
                {
                    string mypath = t.Tag.ToString();
                    string mypathf = mypath;
                    if ( !mypath.EndsWith( @"\", StringComparison.OrdinalIgnoreCase ) )
                    {
                        if ( searchPath.Length > mypathf.Length )
                        {
                            mypathf = $@"{mypath}\";
                        }
                    }

                    if ( searchPath.StartsWith( mypathf, StringComparison.OrdinalIgnoreCase ) )
                    {
                        _ = t.TreeView.Focus();
                        // t.TreeView.SelectedNode = t
                        t.EnsureVisible();
                        t.Expand();
                        if ( t.Nodes.Count >= 1 )
                        {
                            t.Expand();
                            tn = t;
                        }
                        else if ( string.Equals( searchPath, mypath, StringComparison.OrdinalIgnoreCase ) )
                        {
                            selectedNode = t;
                            h = -1;
                            break;
                        }
                        else
                        {
                            continue;
                        }

                        if ( mypathf.StartsWith( searchPath, StringComparison.OrdinalIgnoreCase ) )
                        {
                            selectedNode = t;
                            h = -1;
                            break;
                        }
                        else
                        {
                            goto StartAgain;
                            // return;
                        }
                    }
                }

                try
                {
                    tn = tn.NextNode;
                }
                catch
                {
                }
            }
            while ( h >= 0 );
            if ( selectedNode is object )
            {
                selectedNode.TreeView.SelectedNode = selectedNode;
            }
        }

        /// <summary> Adds a folder node to 'path'. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="name"> The name. </param>
        /// <param name="path"> Full pathname of the file. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void AddFolderNode( string name, string path )
        {
            try
            {
                var nodemyC = new TreeNode() {
                    Tag = path,
                    Text = name,
                    ImageIndex = 18,
                    SelectedImageIndex = 18
                };
                _ = this._RootTreeNode.Nodes.Add( nodemyC );
                try
                {
                    // add directories under drive
                    if ( Directory.Exists( path ) )
                    {
                        foreach ( string dir in Directory.GetDirectories( path ) )
                        {
                            var newNode = new TreeNode() {
                                Tag = dir,
                                Text = dir.Substring( dir.LastIndexOf( @"\", StringComparison.OrdinalIgnoreCase ) + 1 ),
                                ImageIndex = 1
                            };
                            _ = nodemyC.Nodes.Add( newNode );
                        }
                    }
                }
                catch ( Exception ex ) // error just add blank directory
                {
                    _ = MessageBox.Show( "Error while Filling the Explorer:" + ex.Message );
                }
            }
            catch ( Exception e )
            {
                _ = MessageBox.Show( e.Message );
            }
        }

        /// <summary> Explore my computer. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ExploreMyComputer()
        {
            if ( !this.IsInitialized )
            {
                this.RefreshFolders();
            }

            var drives = Environment.GetLogicalDrives();
            TreeNode nodeDrive;
            if ( this._MyComputerTreeNode.GetNodeCount( true ) < 2 )
            {
                this._MyComputerTreeNode.FirstNode.Remove();
                foreach ( string drive in drives )
                {
                    nodeDrive = new TreeNode() { Tag = drive };
                    UpdateDriveImage( drive, nodeDrive );
                    _ = this._MyComputerTreeNode.Nodes.Add( nodeDrive );
                    try
                    {
                        // add directories under drive
                        if ( Directory.Exists( drive ) )
                        {
                            foreach ( string dir in Directory.GetDirectories( drive ) )
                            {
                                _ = nodeDrive.Nodes.Add( new TreeNode() {
                                    Tag = dir,
                                    Text = dir.Substring( dir.LastIndexOf( @"\", StringComparison.OrdinalIgnoreCase ) + 1 ),
                                    ImageIndex = 1
                                } );
                            }
                        }
                    }
                    catch ( Exception ex )
                    {
                        _ = MessageBox.Show( "Error while Filling the Explorer:" + ex.Message );
                    }
                }
            }

            this._MyComputerTreeNode.Expand();
        }

        #endregion

        #region " FOLDER LIST "

        /// <summary> Updates the list add current. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void UpdateListAddCurrent()
        {
            int i = 0;
            int j = 0;
            var loopTo = this._ListView.Items.Count - 2;
            for ( i = 0; i <= loopTo; i++ )
            {
                if ( string.Equals( this._ListView.Items[i].SubItems[1].Text, "Selected", StringComparison.OrdinalIgnoreCase ) )
                {
                    var loopTo1 = i + 2;
                    for ( j = this._ListView.Items.Count - 1; j >= loopTo1; j -= 1 )
                    {
                        this._ListView.Items[j].Remove();
                    }

                    break;
                }
            }
        }

        /// <summary> Updates the list go back. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void UpdateListGoBack()
        {
            if ( this._ListView.Items.Count > 0 && string.Equals( this._ListView.Items[0].SubItems[1].Text, "Selected", StringComparison.OrdinalIgnoreCase ) )
            {
                return;
            }

            int i = 0;
            var loopTo = this._ListView.Items.Count - 1;
            for ( i = 0; i <= loopTo; i++ )
            {
                if ( string.Equals( this._ListView.Items[i].SubItems[1].Text, "Selected", StringComparison.OrdinalIgnoreCase ) )
                {
                    if ( i != 0 )
                    {
                        this._ListView.Items[i - 1].SubItems[1].Text = "Selected";
                        this.CandidatePath = this._ListView.Items[i - 1].Text;
                    }
                }

                if ( i != 0 )
                {
                    this._ListView.Items[i].SubItems[1].Text = " -/- ";
                }
            }
        }

        /// <summary> Updates the list go forward. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void UpdateListGoFwd()
        {
            if ( this._ListView.Items.Count > 0 && string.Equals( this._ListView.Items[this._ListView.Items.Count - 1].SubItems[1].Text, "Selected", StringComparison.OrdinalIgnoreCase ) )
            {
                return;
            }

            int i = 0;
            for ( i = this._ListView.Items.Count - 1; i >= 0; i -= 1 )
            {
                if ( string.Equals( this._ListView.Items[i].SubItems[1].Text, "Selected", StringComparison.OrdinalIgnoreCase ) )
                {
                    if ( i != this._ListView.Items.Count )
                    {
                        this._ListView.Items[i + 1].SubItems[1].Text = "Selected";
                        this.CandidatePath = this._ListView.Items[i + 1].Text;
                    }
                }

                if ( i != this._ListView.Items.Count - 1 )
                {
                    this._ListView.Items[i].SubItems[1].Text = " -/- ";
                }
            }
        }

        /// <summary> Updates the list described by f. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="f"> The format string. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void UpdateList( string f )
        {
            ListViewItem listViewItem; // Used for creating list view items.
            this.UpdateListAddCurrent();
            try
            {
                if ( this._ListView.Items.Count > 0 )
                {
                    if ( string.Equals( this._ListView.Items[this._ListView.Items.Count - 1].Text, f, StringComparison.OrdinalIgnoreCase ) )
                    {
                        return;
                    }
                }

                int i;
                var loopTo = this._ListView.Items.Count - 1;
                for ( i = 0; i <= loopTo; i++ )
                {
                    this._ListView.Items[i].SubItems[1].Text = " -/- ";
                }

                listViewItem = new ListViewItem( f );
                _ = listViewItem.SubItems.Add( "Selected" );
                listViewItem.Tag = f;
                _ = this._ListView.Items.Add( listViewItem );
            }
            catch ( Exception e )
            {
                _ = MessageBox.Show( e.Message );
            }
        }

        /// <summary> Updates the drive image. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="drive">     The drive. </param>
        /// <param name="nodeDrive"> The node drive. </param>
        private static void UpdateDriveImage( string drive, TreeNode nodeDrive )
        {
            if ( nodeDrive is object )
            {
                nodeDrive.Text = drive;
                var di = new DriveInfo( drive );
                switch ( di.DriveType )
                {
                    case DriveType.Removable: // 2
                        {
                            nodeDrive.ImageIndex = 17;
                            nodeDrive.SelectedImageIndex = 17;
                            break;
                        }

                    case DriveType.Fixed: // 3
                        {
                            nodeDrive.ImageIndex = 0;
                            nodeDrive.SelectedImageIndex = 0;
                            break;
                        }

                    case DriveType.Network: // 4
                        {
                            nodeDrive.ImageIndex = 8;
                            nodeDrive.SelectedImageIndex = 8;
                            break;
                        }

                    case DriveType.CDRom: // 5
                        {
                            nodeDrive.ImageIndex = 7;
                            nodeDrive.SelectedImageIndex = 7;
                            break;
                        }

                    default:
                        {
                            nodeDrive.ImageIndex = 0;
                            nodeDrive.SelectedImageIndex = 0;
                            break;
                        }
                }
            }
        }

        #endregion

        #region " TREE VIEW CONTROL EVENTS "

        /// <summary> Expand my computer node. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> Tree view event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ExpandMyComputerNode( TreeViewEventArgs e )
        {
            var drives = Environment.GetLogicalDrives();
            TreeNode n;
            TreeNode nodemyC;
            TreeNode nodeDrive;
            nodemyC = e.Node;
            n = e.Node;
            if ( string.Equals( n.Text, "My Computer", StringComparison.OrdinalIgnoreCase ) && nodemyC.GetNodeCount( true ) < 2 )
            {
                // /////////
                // add each drive and files and directories
                nodemyC.FirstNode.Remove();
                foreach ( string drive in drives )
                {
                    nodeDrive = new TreeNode() { Tag = drive };
                    UpdateDriveImage( drive, nodeDrive );
                    _ = nodemyC.Nodes.Add( nodeDrive );
                    nodeDrive.EnsureVisible();
                    base.Refresh();
                    try
                    {
                        // add directories under drive
                        if ( Directory.Exists( drive ) )
                        {
                            foreach ( string dir in Directory.GetDirectories( drive ) )
                            {
                                _ = nodeDrive.Nodes.Add( new TreeNode() {
                                    Tag = dir,
                                    Text = dir.Substring( dir.LastIndexOf( @"\", StringComparison.OrdinalIgnoreCase ) + 1 ),
                                    ImageIndex = 1
                                } );
                            }
                        }
                    }
                    catch
                    {
                    }

                    nodemyC.Expand();
                }
            }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.TreeView.AfterExpand" /> event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.TreeViewEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnAfterExpand( TreeViewEventArgs e )
        {
            base.OnAfterExpand( e );
            if ( e is null )
            {
                return;
            }

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var n = e.Node;
                if ( n.Text.IndexOf( ":", 1, StringComparison.OrdinalIgnoreCase ) > 0 )
                {
                    ExploreTreeNode( n );
                }
                else if ( string.Equals( n.Text, "Desktop", StringComparison.OrdinalIgnoreCase ) || string.Equals( n.Text, "Microsoft Windows Network", StringComparison.OrdinalIgnoreCase ) || string.Equals( n.Text, "My Computer", StringComparison.OrdinalIgnoreCase ) || string.Equals( n.Text, "My Network Places", StringComparison.OrdinalIgnoreCase ) || string.Equals( n.Text, "Entire Network", StringComparison.OrdinalIgnoreCase ) || n.Parent is object && string.Equals( n.Parent.Text, "Microsoft Windows Network", StringComparison.OrdinalIgnoreCase ) )
                {
                    this.ExpandMyComputerNode( e );
                }
                // Me.ExpandEntireNetworkNode(e) ' TO_DO: Fix
                // Me.ExpandMicrosoftWindowsNetworkNode(e)  TO_DO: Fix
                else
                {
                    ExploreTreeNode( n );
                }
            }
            catch ( UnauthorizedAccessException )
            {
                _ = MessageBox.Show( $"Access denied to folder {e.Node.Tag}", "Access denied", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly );
            }
            catch
            {
                throw;
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.TreeView.AfterSelect" /> event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.TreeViewEventArgs" /> that contains the
        /// event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnAfterSelect( TreeViewEventArgs e )
        {
            base.OnAfterSelect( e );
            if ( e is null )
            {
                return;
            }

            TreeNode n;
            n = e.Node;
            try
            {
                if ( string.Equals( n.Text, "My Computer", StringComparison.OrdinalIgnoreCase ) || string.Equals( n.Text, "My Network Places", StringComparison.OrdinalIgnoreCase ) || string.Equals( n.Text, "Entire Network", StringComparison.OrdinalIgnoreCase ) )
                {
                }
                else
                {
                    this.SelectedPath = n.Tag.ToString();
                }
            }
            catch
            {
            }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.DoubleClick" /> event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnDoubleClick( EventArgs e )
        {
            base.OnDoubleClick( e );
            if ( e is null )
            {
                return;
            }

            TreeNode n;
            n = this.SelectedNode;
            if ( !this.SelectedNode.IsExpanded )
            {
                this.SelectedNode.Collapse();
            }
            else
            {
                ExploreTreeNode( n );
            }
        }

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.MouseUp" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnMouseUp( MouseEventArgs e )
        {
            this.UpdateList( this.SelectedPath );
            base.OnMouseUp( e );
        }

        /// <summary>
        /// Forces the control to invalidate its client area and immediately redraw itself and any child
        /// controls.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public override void Refresh()
        {
            base.Refresh();
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                this.RefreshFolders();
                if ( string.IsNullOrWhiteSpace( this.CandidatePath ) )
                {
                    this.SetCandidatePath( HomePathName );
                }

                this.SelectMyComputerTreeNode( this.CandidatePath );
            }
            catch
            {
                throw;
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region " TOOL BAR "

        /// <summary> Try select directory. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="path"> Full pathname of the file. </param>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public (bool Success, string Details) TrySelectPath( string path )
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                this.SelectPath( path );
                return (false, string.Empty);
            }
            catch ( Exception ex )
            {
                return (false, ex.ToString());
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        /// <summary> Try select home. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public (bool Success, string Details) TrySelectHome()
        {
            return this.TrySelectPath( HomePathName );
        }

        /// <summary> Try go forward. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public (bool Success, string Details) TryGoingForward()
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                this.UpdateListGoFwd();
                if ( !string.Equals( this.SelectedPath, this.CandidatePath, StringComparison.OrdinalIgnoreCase ) )
                {
                    this.SelectMyComputerTreeNode( this.CandidatePath );
                }

                return (false, string.Empty);
            }
            catch ( Exception ex )
            {
                return (false, ex.ToString());
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        /// <summary> Try go back. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public (bool Success, string Details) TryGoingBack()
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                this.UpdateListGoBack();
                if ( !string.Equals( this.SelectedPath, this.CandidatePath, StringComparison.OrdinalIgnoreCase ) )
                {
                    this.SelectMyComputerTreeNode( this.CandidatePath );
                }

                return (false, string.Empty);
            }
            catch ( Exception ex )
            {
                return (false, ex.ToString());
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        /// <summary> Try go up. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public (bool Success, string Details) TryGoingUp()
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var di = new DirectoryInfo( this.SelectedPath );
                if ( di.Parent.Exists )
                {
                    this.CandidatePath = di.Parent.FullName;
                    this.UpdateList( this.CandidatePath );
                    this.SelectMyComputerTreeNode( this.CandidatePath );
                }

                return (false, string.Empty);
            }
            catch ( Exception ex )
            {
                return (false, ex.ToString());
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        /// <summary> Try add folder. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public (bool Success, string Details) TryAddFolder()
        {
            try
            {
                using ( var dialog = new FolderBrowserDialog() {
                    Description = "Add Folder in Explorer Tree",
                    ShowNewFolderButton = true,
                    SelectedPath = SelectedPath
                } )
                {
                    if ( dialog.ShowDialog() == DialogResult.OK )
                    {
                        string newPath = dialog.SelectedPath;
                        string pathTitle = newPath.Substring( newPath.LastIndexOf( @"\", StringComparison.OrdinalIgnoreCase ) + 1 );
                        this.AddFolderNode( pathTitle, newPath );
                    }
                }

                return (false, string.Empty);
            }
            catch ( Exception ex )
            {
                return (false, ex.ToString());
            }
            finally
            {
            }
        }

        #endregion

        #region " CONTEXT MENU STRIP "

        /// <summary> my context menu strip. </summary>
        private ContextMenuStrip _MyContextMenuStrip;

        /// <summary> Creates a context menu strip. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> The new context menu strip. </returns>
        private ContextMenuStrip CreateContextMenuStrip()
        {

            // Create a new ContextMenuStrip control.
            this._MyContextMenuStrip = new ContextMenuStrip();

            // Attach an event handler for the 
            // ContextMenuStrip control's Opening event.
            this._MyContextMenuStrip.Opening += this.ContectMenuOpeningHandler;
            return this._MyContextMenuStrip;
        }

        /// <summary> Adds menu items. </summary>
        /// <remarks>
        /// This event handler is invoked when the <see cref="ContextMenuStrip"/> control's Opening event
        /// is raised.
        /// </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Cancel event information. </param>
        private void ContectMenuOpeningHandler( object sender, CancelEventArgs e )
        {
            this._MyContextMenuStrip = sender as ContextMenuStrip;

            // Clear the ContextMenuStrip control's Items collection.
            this._MyContextMenuStrip.Items.Clear();

            // Populate the ContextMenuStrip control with its default items.
            // myContextMenuStrip.Items.Add("-")
            _ = this._MyContextMenuStrip.Items.Add( new System.Windows.Forms.ToolStripMenuItem( "&Refresh", null, this.RefreshHandler, "Refresh" ) );
            if ( this.SelectedNode.ImageIndex == 18 )
            {
                _ = this._MyContextMenuStrip.Items.Add( new System.Windows.Forms.ToolStripMenuItem( "Remove &Shortcut", null, this.RemoveShortcutHandler, "RemoveShortcut" ) );
            }

            // Set Cancel to false. 
            // It is optimized to true based on empty entry.
            e.Cancel = false;
        }

        /// <summary> Removes the shortcut handler. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void RemoveShortcutHandler( object sender, EventArgs e )
        {
            if ( this.SelectedNode.ImageIndex == 18 )
            {
                this.SelectedNode.Remove();
            }
        }

        /// <summary> Handler, called when the refresh. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void RefreshHandler( object sender, EventArgs e )
        {
            this.Refresh();
        }

        #endregion

    }
}
