using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace isr.Core.Controls
{

    /// <summary> Summary description for ExplorerTree. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-07-06 </para>
    /// </remarks>
    [ToolboxBitmap( typeof( FolderTreeViewControl ), "FolderTreeViewControl.gif" )]
    public partial class FolderTreeViewControl : UserControl
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Windows.Forms.UserControl" /> class.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public FolderTreeViewControl()
        {
            this.IsInitializingComponents = true;
            // This call is required by the Windows.Forms Form Designer.
            this.InitializeComponent();
            this.IsInitializingComponents = false;
            this._HomeToolStripButton.Image = this._ImageList.Images[23];
            this._AddFolderButton.Image = this._ImageList.Images[27];
            this._BackToolStripButton.Image = this._ImageList.Images[20];
            this._ForwardToolStripButton.Image = this._ImageList.Images[19];
            this._FolderUpButton.Image = this._ImageList.Images[24];
            this._RefreshToolStripButton.Image = this._ImageList.Images[25];
            this.__PathTextBox.Name = "_PathTextBox";
            this.__GoToDirectoryButton.Name = "_GoToDirectoryButton";
            this.__FolderTreeView.Name = "_FolderTreeView";
            this.__HomeToolStripButton.Name = "_HomeToolStripButton";
            this.__FolderUpButton.Name = "_FolderUpButton";
            this.__BackToolStripButton.Name = "_BackToolStripButton";
            this.__ForwardToolStripButton.Name = "_ForwardToolStripButton";
            this.__AddFolderButton.Name = "_AddFolderButton";
            this.__RefreshToolStripButton.Name = "_RefreshToolStripButton";
        }

        #region " CONSTRUCTION And CLEANUP "

        /// <summary> Gets the is initializing components. </summary>
        /// <value> The is initializing components. </value>
        private bool IsInitializingComponents { get; set; }

        /// <summary> Clean up any resources being used. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> <see langword="true" /> to release both managed and unmanaged
        /// resources; <see langword="false" /> to release only unmanaged
        /// resources. </param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing )
            {
                if ( this.components is object )
                {
                    this.components.Dispose();
                }
            }

            base.Dispose( disposing );
        }

        #endregion

        #region " APPEARANCE "

        /// <summary> Gets the folder tree view. </summary>
        /// <value> The folder tree view. </value>
        public FolderTreeView FolderTreeView => this._FolderTreeView;

        /// <summary> Gets or sets the show current path text box. </summary>
        /// <value> The show current path text box. </value>
        public bool ShowCurrentPathTextBox
        {
            get => this._CurrentPathLayoutPanel.Visible;

            set => this._CurrentPathLayoutPanel.Visible = value;
        }

        /// <summary> Gets or sets the show tool bar. </summary>
        /// <value> The show tool bar. </value>
        public bool ShowToolBar
        {
            get => this._ToolStrip.Visible;

            set => this._ToolStrip.Visible = value;
        }

        /// <summary> Gets or sets the show my documents. </summary>
        /// <value> The show my documents. </value>
        public bool ShowMyDocuments
        {
            get => this.FolderTreeView.ShowMyDocuments;

            set => this.FolderTreeView.ShowMyDocuments = value;
        }

        /// <summary> Gets or sets the show my favorites. </summary>
        /// <value> The show my favorites. </value>
        public bool ShowMyFavorites
        {
            get => this.FolderTreeView.ShowMyFavorites;

            set => this.FolderTreeView.ShowMyFavorites = value;
        }

        /// <summary> Gets or sets the show my network. </summary>
        /// <value> The show my network. </value>
        public bool ShowMyNetwork
        {
            get => this.FolderTreeView.ShowMyNetwork;

            set => this.FolderTreeView.ShowMyNetwork = value;
        }

        #endregion

        #region " BEHAVIOR "

        /// <summary> The selected path. </summary>
        /// <value> The full pathname of the folder tree view. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public string SelectedPath
        {
            get => this._FolderTreeView.SelectedPath;

            set {
                if ( !string.Equals( value, this.SelectedPath, StringComparison.OrdinalIgnoreCase ) )
                {
                    this.SelectCurrentPath( value );
                }
            }
        }

        /// <summary> Select current path. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The value. </param>
        private void SelectCurrentPath( string value )
        {
            var (success, _) = this.FolderTreeView.TrySelectPath( value );
            if ( success )
            {
                this._PathTextBox.Text = value;
            }
        }

        /// <summary> Refresh view. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public void RefreshView()
        {
            this.FolderTreeView.Refresh();
        }

        #endregion

        #region " TOOL BAR "

        /// <summary> Event handler. Called by _RefreshToolStripButton for click events. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void RefreshButtonClick( object sender, EventArgs e )
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                this.RefreshView();
            }
            catch ( Exception e1 )
            {
                _ = MessageBox.Show( "Error: " + e1.Message );
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        /// <summary> Event handler. Called by _GoToDirectoryButton for click events. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void GoToDirectoryButtonClick( object sender, EventArgs e )
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                this.SelectCurrentPath( this._PathTextBox.Text.Trim() );
            }
            catch ( Exception e1 )
            {
                _ = MessageBox.Show( "Error: " + e1.Message );
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        /// <summary> Home tool strip button click. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void HomeToolStripButton_Click( object sender, EventArgs e )
        {
            _ = this.FolderTreeView.TrySelectHome();
        }

        /// <summary> Forward tool strip button click. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ForwardToolStripButton_Click( object sender, EventArgs e )
        {
            _ = this.FolderTreeView.TryGoingForward();
        }

        /// <summary> Back tool strip button click. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void BackToolStripButton_Click( object sender, EventArgs e )
        {
            _ = this.FolderTreeView.TryGoingBack();
        }

        /// <summary> Folder up button click. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void FolderUpButton_Click( object sender, EventArgs e )
        {
            _ = this.FolderTreeView.TryGoingUp();
        }

        /// <summary> Adds a folder button click to 'e'. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void AddFolderButton_Click( object sender, EventArgs e )
        {
            _ = this.FolderTreeView.TryAddFolder();
        }

        #endregion

        #region " PATH TEXT BOX "

        /// <summary> Folder tree view path changed. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void FolderTreeView_PathChanged( object sender, EventArgs e )
        {
            if ( this.IsInitializingComponents )
            {
                return;
            }

            this._PathTextBox.Text = this._FolderTreeView.SelectedPath;
        }

        /// <summary> Path text box key up. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Key event information. </param>
        private void PathTextBox_KeyUp( object sender, KeyEventArgs e )
        {
            if ( e.KeyValue == 13 )
            {
                string value = this._PathTextBox.Text.Trim();
                if ( Directory.Exists( value ) && !string.Equals( this.FolderTreeView.SelectedPath, value, StringComparison.OrdinalIgnoreCase ) )
                {
                    this.FolderTreeView.SelectPath( value );
                }

                _ = this._PathTextBox.Focus();
            }
        }


        #endregion

    }
}
