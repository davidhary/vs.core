using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Controls
{
    [DesignerGenerated()]
    public partial class FileListViewControl
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _ToolStrip = new ToolStrip();
            _SearchPatternComboBox = new System.Windows.Forms.ToolStripComboBox();
            _RefreshButton = new System.Windows.Forms.ToolStripButton();
            FileListView = new FileListView();
            _Split = new SplitContainer();
            _FileInfoRichTextBox = new System.Windows.Forms.RichTextBox();
            _ToolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_Split).BeginInit();
            _Split.Panel1.SuspendLayout();
            _Split.Panel2.SuspendLayout();
            _Split.SuspendLayout();
            SuspendLayout();
            // 
            // _ToolStrip
            // 
            _ToolStrip.GripMargin = new Padding(0);
            _ToolStrip.GripStyle = ToolStripGripStyle.Hidden;
            _ToolStrip.Items.AddRange(new ToolStripItem[] { _SearchPatternComboBox, _RefreshButton });
            _ToolStrip.Location = new Point(0, 0);
            _ToolStrip.Name = "_ToolStrip";
            _ToolStrip.Size = new Size(301, 25);
            _ToolStrip.Stretch = true;
            _ToolStrip.TabIndex = 0;
            _ToolStrip.Text = "Tool Strip";
            // 
            // _SearchPatternComboBox
            // 
            _SearchPatternComboBox.Items.AddRange(new object[] { "*.*", "*.bmp, *.jpg, *.png", "*.csv, *.txt" });
            _SearchPatternComboBox.Name = "_SearchPatternComboBox";
            _SearchPatternComboBox.Size = new Size(140, 25);
            _SearchPatternComboBox.Text = "*.*";
            _SearchPatternComboBox.ToolTipText = "Search patterns";
            // 
            // _RefreshButton
            // 
            _RefreshButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            _RefreshButton.Image = My.Resources.Resources.arrow_refresh_small;
            _RefreshButton.ImageScaling = ToolStripItemImageScaling.None;
            _RefreshButton.ImageTransparentColor = Color.Magenta;
            _RefreshButton.Name = "_RefreshButton";
            _RefreshButton.Size = new Size(23, 22);
            _RefreshButton.Text = "Refresh";
            _RefreshButton.Click += new EventHandler( RefreshButtonClick );
            // 
            // _FileListView
            // 
            FileListView.Dock = DockStyle.Fill;
            FileListView.HideSelection = false;
            FileListView.Location = new Point(0, 0);
            FileListView.Name = "_FileListView";
            FileListView.Size = new Size(301, 238);
            FileListView.TabIndex = 0;
            FileListView.UseCompatibleStateImageBehavior = false;
            FileListView.View = View.Details;
            // 
            // _Split
            // 
            _Split.Dock = DockStyle.Fill;
            _Split.Location = new Point(0, 25);
            _Split.Name = "_Split";
            _Split.Orientation = Orientation.Horizontal;
            // 
            // _Split.Panel1
            // 
            _Split.Panel1.Controls.Add(FileListView);
            // 
            // _Split.Panel2
            // 
            _Split.Panel2.Controls.Add(_FileInfoRichTextBox);
            _Split.Panel2MinSize = 5;
            _Split.Size = new Size(301, 472);
            _Split.SplitterDistance = 238;
            _Split.SplitterWidth = 5;
            _Split.TabIndex = 1;
            // 
            // _FileInfoRichTextBox
            // 
            _FileInfoRichTextBox.Dock = DockStyle.Fill;
            _FileInfoRichTextBox.Location = new Point(0, 0);
            _FileInfoRichTextBox.Name = "_FileInfoRichTextBox";
            _FileInfoRichTextBox.Size = new Size(301, 229);
            _FileInfoRichTextBox.TabIndex = 0;
            _FileInfoRichTextBox.Text = "file info";
            // 
            // FileListViewControl
            // 
            AutoScaleDimensions = new SizeF(7.0f, 15.0f);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(_Split);
            Controls.Add(_ToolStrip);
            Font = new Font("Segoe UI", 9.0f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            Name = "FileListViewControl";
            Size = new Size(301, 497);
            _ToolStrip.ResumeLayout(false);
            _ToolStrip.PerformLayout();
            _Split.Panel1.ResumeLayout(false);
            _Split.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)_Split).EndInit();
            _Split.ResumeLayout(false);
            ResumeLayout(false);
            PerformLayout();
        }

        private ToolStrip _ToolStrip;
        private System.Windows.Forms.ToolStripComboBox _SearchPatternComboBox;
        private System.Windows.Forms.ToolStripButton _RefreshButton;
        private SplitContainer _Split;
        private System.Windows.Forms.RichTextBox _FileInfoRichTextBox;
    }
}
