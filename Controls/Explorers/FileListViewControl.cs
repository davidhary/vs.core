using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace isr.Core.Controls
{

    /// <summary> A file list view control. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-07-06 </para>
    /// </remarks>
    [Description( "File list view control" )]
    [ToolboxBitmap( typeof( FileListView ), "FileListViewControl.gif" )]
    public partial class FileListViewControl : UserControl
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Windows.Forms.UserControl" /> class.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public FileListViewControl() : base()
        {
            this.IsInitializingComponents = true;
            this.InitializeComponent();
            this.IsInitializingComponents = false;
            this._RefreshButton.Name = "_RefreshButton";
        }

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Gets the is initializing components. </summary>
        /// <value> The is initializing components. </value>
        private bool IsInitializingComponents { get; set; }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> <see langword="true" /> to release both managed and unmanaged
        /// resources; <see langword="false" /> to release only unmanaged
        /// resources. </param>
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( disposing && this.components is object )
                {
                    this.components.Dispose();
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " EVENT SOURCE "

        /// <summary> Gets the file list view. </summary>
        /// <value> The file list view. </value>
        public FileListView FileListView { get; private set; }

        #endregion

        #region " APPEARANCE "

        /// <summary> Gets or sets information describing the hide file. </summary>
        /// <value> Information describing the hide file. </value>
        [Category( "Appearance" )]
        [Description( "Hide file info" )]
        [DefaultValue( true )]
        public bool HideFileInfo
        {
            get => this._Split.Panel2Collapsed;

            set {
                if ( value != this.HideFileInfo )
                {
                    this._Split.Panel2Collapsed = value;
                }
            }
        }

        #endregion

        #region " BEHAVIOR "

        /// <summary> Full pathname of the directory file. </summary>
        private string _DirectoryPath;

        /// <summary> The current directory path. </summary>
        /// <value> The full pathname of the folder tree view. </value>
        [Category( "Behavior" )]
        [Description( "Current directory path" )]
        public string DirectoryPath
        {
            get => this._DirectoryPath;

            set {
                if ( !string.Equals( value, this.DirectoryPath, StringComparison.OrdinalIgnoreCase ) )
                {
                    this._DirectoryPath = value;
                }
            }
        }

        /// <summary> Search pattern. </summary>
        /// <value> The search pattern. </value>
        [Category( "Behavior" )]
        [Description( "Search pattern" )]
        [DefaultValue( "*.*" )]
        public string SearchPattern
        {
            get => this._SearchPatternComboBox.Text;

            set {
                if ( !string.Equals( value, this.SearchPattern ) )
                {
                    this._SearchPatternComboBox.Text = this.SearchPattern;
                }
            }
        }

        /// <summary> Gets or sets the search pattern delimiter. </summary>
        /// <value> The search pattern delimiter. </value>
        public char SearchPatternDelimiter { get; set; } = ';';

        /// <summary> Gets or sets the search patterns. </summary>
        /// <value> The search patterns. </value>
        [Category( "Behavior" )]
        [Description( "Search patterns" )]
        [DefaultValue( "*.*;*.bmp;*.png;*.csv;*.txt" )]
        public string SearchPatterns
        {
            get {
                var values = new System.Text.StringBuilder();
                foreach ( object item in this._SearchPatternComboBox.Items )
                {
                    _ = values.Append( $"{item}{this.SearchPatternDelimiter}" );
                }

                return values.ToString().TrimEnd( this.SearchPatternDelimiter );
            }

            set {
                if ( string.IsNullOrWhiteSpace( value ) )
                {
                    this._SearchPatternComboBox.Items.Clear();
                    _ = this._SearchPatternComboBox.Items.Add( "*.*" );
                }
                else
                {
                    this._SearchPatternComboBox.Items.Clear();
                    foreach ( string pattern in value.Split( this.SearchPatternDelimiter ) )
                    {
                        _ = this._SearchPatternComboBox.Items.Add( pattern );
                    }
                }

                this._SearchPatternComboBox.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// Forces the control to invalidate its client area and immediately redraw itself and any child
        /// controls.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public override void Refresh()
        {
            base.Refresh();
            if ( this.IsInitializingComponents )
            {
                return;
            }

            this.FileListView.PopulateFiles( this.DirectoryPath, this._SearchPatternComboBox.Text );
        }

        #endregion

        #region " CONTROL EVENT HANDLERS "

        /// <summary> Refresh button click. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void RefreshButtonClick( object sender, EventArgs e )
        {
            this.Refresh();
        }

        #endregion

    }
}
