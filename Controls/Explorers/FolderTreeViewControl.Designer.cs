﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Controls
{
    [DesignerGenerated()]
    public partial class FolderTreeViewControl
    {
        private System.ComponentModel.IContainer components;

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(FolderTreeViewControl));
            __PathTextBox = new System.Windows.Forms.TextBox();
            __PathTextBox.KeyUp += new KeyEventHandler(PathTextBox_KeyUp);
            __GoToDirectoryButton = new Button();
            __GoToDirectoryButton.Click += new EventHandler(GoToDirectoryButtonClick);
            _ImageList = new ImageList(components);
            _ToolTip = new ToolTip(components);
            __FolderTreeView = new FolderTreeView();
            __FolderTreeView.PathChanged += new EventHandler<EventArgs>(FolderTreeView_PathChanged);
            _CurrentPathLayoutPanel = new TableLayoutPanel();
            _ToolStrip = new ToolStrip();
            __HomeToolStripButton = new System.Windows.Forms.ToolStripButton();
            __HomeToolStripButton.Click += new EventHandler(HomeToolStripButton_Click);
            __FolderUpButton = new System.Windows.Forms.ToolStripButton();
            __FolderUpButton.Click += new EventHandler(FolderUpButton_Click);
            __BackToolStripButton = new System.Windows.Forms.ToolStripButton();
            __BackToolStripButton.Click += new EventHandler(BackToolStripButton_Click);
            __ForwardToolStripButton = new System.Windows.Forms.ToolStripButton();
            __ForwardToolStripButton.Click += new EventHandler(ForwardToolStripButton_Click);
            __AddFolderButton = new System.Windows.Forms.ToolStripButton();
            __AddFolderButton.Click += new EventHandler(AddFolderButton_Click);
            __RefreshToolStripButton = new System.Windows.Forms.ToolStripButton();
            __RefreshToolStripButton.Click += new EventHandler(RefreshButtonClick);
            _CurrentPathLayoutPanel.SuspendLayout();
            _ToolStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _PathTextBox
            // 
            __PathTextBox.Dock = DockStyle.Fill;
            __PathTextBox.Location = new Point(3, 3);
            __PathTextBox.Name = "__PathTextBox";
            __PathTextBox.Size = new Size(276, 23);
            __PathTextBox.TabIndex = 0;
            _ToolTip.SetToolTip(__PathTextBox, "Current directory");
            // 
            // _GoToDirectoryButton
            // 
            __GoToDirectoryButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            __GoToDirectoryButton.Cursor = Cursors.Hand;
            __GoToDirectoryButton.FlatStyle = FlatStyle.Flat;
            __GoToDirectoryButton.ForeColor = Color.White;
            __GoToDirectoryButton.ImageIndex = 21;
            __GoToDirectoryButton.ImageList = _ImageList;
            __GoToDirectoryButton.Location = new Point(285, 3);
            __GoToDirectoryButton.Name = "__GoToDirectoryButton";
            __GoToDirectoryButton.Size = new Size(20, 22);
            __GoToDirectoryButton.TabIndex = 1;
            _ToolTip.SetToolTip(__GoToDirectoryButton, "Go to the directory");
            // 
            // _ImageList
            // 
            _ImageList.ImageStream = (ImageListStreamer)resources.GetObject("_ImageList.ImageStream");
            _ImageList.TransparentColor = Color.Transparent;
            _ImageList.Images.SetKeyName(0, "");
            _ImageList.Images.SetKeyName(1, "");
            _ImageList.Images.SetKeyName(2, "");
            _ImageList.Images.SetKeyName(3, "");
            _ImageList.Images.SetKeyName(4, "");
            _ImageList.Images.SetKeyName(5, "");
            _ImageList.Images.SetKeyName(6, "");
            _ImageList.Images.SetKeyName(7, "");
            _ImageList.Images.SetKeyName(8, "");
            _ImageList.Images.SetKeyName(9, "");
            _ImageList.Images.SetKeyName(10, "");
            _ImageList.Images.SetKeyName(11, "");
            _ImageList.Images.SetKeyName(12, "");
            _ImageList.Images.SetKeyName(13, "");
            _ImageList.Images.SetKeyName(14, "");
            _ImageList.Images.SetKeyName(15, "");
            _ImageList.Images.SetKeyName(16, "");
            _ImageList.Images.SetKeyName(17, "");
            _ImageList.Images.SetKeyName(18, "");
            _ImageList.Images.SetKeyName(19, "");
            _ImageList.Images.SetKeyName(20, "");
            _ImageList.Images.SetKeyName(21, "");
            _ImageList.Images.SetKeyName(22, "");
            _ImageList.Images.SetKeyName(23, "");
            _ImageList.Images.SetKeyName(24, "");
            _ImageList.Images.SetKeyName(25, "");
            _ImageList.Images.SetKeyName(26, "");
            _ImageList.Images.SetKeyName(27, "");
            _ImageList.Images.SetKeyName(28, "");
            // 
            // _FolderTreeView
            // 
            __FolderTreeView.Dock = DockStyle.Fill;
            __FolderTreeView.ImageIndex = 0;
            __FolderTreeView.Location = new Point(0, 57);
            __FolderTreeView.Name = "__FolderTreeView";
            __FolderTreeView.SelectedImageIndex = 2;
            __FolderTreeView.ShowLines = false;
            __FolderTreeView.ShowRootLines = false;
            __FolderTreeView.Size = new Size(308, 382);
            __FolderTreeView.TabIndex = 2;
            // 
            // _CurrentPathLayoutPanel
            // 
            _CurrentPathLayoutPanel.ColumnCount = 2;
            _CurrentPathLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100.0f));
            _CurrentPathLayoutPanel.ColumnStyles.Add(new ColumnStyle());
            _CurrentPathLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 20.0f));
            _CurrentPathLayoutPanel.Controls.Add(__GoToDirectoryButton, 1, 0);
            _CurrentPathLayoutPanel.Controls.Add(__PathTextBox, 0, 0);
            _CurrentPathLayoutPanel.Dock = DockStyle.Top;
            _CurrentPathLayoutPanel.Location = new Point(0, 25);
            _CurrentPathLayoutPanel.Name = "_CurrentPathLayoutPanel";
            _CurrentPathLayoutPanel.RowCount = 2;
            _CurrentPathLayoutPanel.RowStyles.Add(new RowStyle());
            _CurrentPathLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 100.0f));
            _CurrentPathLayoutPanel.Size = new Size(308, 32);
            _CurrentPathLayoutPanel.TabIndex = 1;
            // 
            // _ToolStrip
            // 
            _ToolStrip.GripMargin = new Padding(0);
            _ToolStrip.GripStyle = ToolStripGripStyle.Hidden;
            _ToolStrip.Items.AddRange(new ToolStripItem[] { __HomeToolStripButton, __FolderUpButton, __BackToolStripButton, __ForwardToolStripButton, __AddFolderButton, __RefreshToolStripButton });
            _ToolStrip.Location = new Point(0, 0);
            _ToolStrip.Name = "_ToolStrip";
            _ToolStrip.Size = new Size(308, 25);
            _ToolStrip.TabIndex = 0;
            _ToolStrip.Text = "ToolStrip1";
            // 
            // _HomeToolStripButton
            // 
            __HomeToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            __HomeToolStripButton.Image = My.Resources.Resources.go_up_8;
            __HomeToolStripButton.ImageTransparentColor = Color.Magenta;
            __HomeToolStripButton.Name = "__HomeToolStripButton";
            __HomeToolStripButton.Size = new Size(23, 22);
            __HomeToolStripButton.Text = "Home";
            // 
            // _FolderUpButton
            // 
            __FolderUpButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            __FolderUpButton.Image = My.Resources.Resources.go_up_8;
            __FolderUpButton.ImageTransparentColor = Color.Magenta;
            __FolderUpButton.Name = "__FolderUpButton";
            __FolderUpButton.Size = new Size(23, 22);
            __FolderUpButton.Text = "Up";
            // 
            // _BackToolStripButton
            // 
            __BackToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            __BackToolStripButton.Image = My.Resources.Resources.go_up_8;
            __BackToolStripButton.ImageTransparentColor = Color.Magenta;
            __BackToolStripButton.Name = "__BackToolStripButton";
            __BackToolStripButton.Size = new Size(23, 22);
            __BackToolStripButton.Text = "Back";
            // 
            // _ForwardToolStripButton
            // 
            __ForwardToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            __ForwardToolStripButton.Image = My.Resources.Resources.go_up_8;
            __ForwardToolStripButton.ImageTransparentColor = Color.Magenta;
            __ForwardToolStripButton.Name = "__ForwardToolStripButton";
            __ForwardToolStripButton.Size = new Size(23, 22);
            __ForwardToolStripButton.Text = "Forward";
            // 
            // _AddFolderButton
            // 
            __AddFolderButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            __AddFolderButton.Image = My.Resources.Resources.go_up_8;
            __AddFolderButton.ImageTransparentColor = Color.Magenta;
            __AddFolderButton.Name = "__AddFolderButton";
            __AddFolderButton.Size = new Size(23, 22);
            __AddFolderButton.Text = "Add";
            // 
            // _RefreshToolStripButton
            // 
            __RefreshToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            __RefreshToolStripButton.Image = My.Resources.Resources.go_up_8;
            __RefreshToolStripButton.ImageTransparentColor = Color.Magenta;
            __RefreshToolStripButton.Name = "__RefreshToolStripButton";
            __RefreshToolStripButton.Size = new Size(23, 22);
            __RefreshToolStripButton.Text = "Refresh";
            // 
            // FolderTreeViewControl
            // 
            BackColor = SystemColors.Control;
            Controls.Add(__FolderTreeView);
            Controls.Add(_CurrentPathLayoutPanel);
            Controls.Add(_ToolStrip);
            Font = new Font("Segoe UI", 9.0f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            Name = "FolderTreeViewControl";
            Size = new Size(308, 439);
            _CurrentPathLayoutPanel.ResumeLayout(false);
            _CurrentPathLayoutPanel.PerformLayout();
            _ToolStrip.ResumeLayout(false);
            _ToolStrip.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        private Button __GoToDirectoryButton;

        private Button _GoToDirectoryButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __GoToDirectoryButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__GoToDirectoryButton != null)
                {
                    __GoToDirectoryButton.Click -= GoToDirectoryButtonClick;
                }

                __GoToDirectoryButton = value;
                if (__GoToDirectoryButton != null)
                {
                    __GoToDirectoryButton.Click += GoToDirectoryButtonClick;
                }
            }
        }

        private ToolTip _ToolTip;
        private ImageList _ImageList;
        private System.Windows.Forms.TextBox __PathTextBox;

        private System.Windows.Forms.TextBox _PathTextBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __PathTextBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__PathTextBox != null)
                {
                    __PathTextBox.KeyUp -= PathTextBox_KeyUp;
                }

                __PathTextBox = value;
                if (__PathTextBox != null)
                {
                    __PathTextBox.KeyUp += PathTextBox_KeyUp;
                }
            }
        }

        private TableLayoutPanel _CurrentPathLayoutPanel;
        private System.Windows.Forms.ToolStripButton __FolderUpButton;

        private System.Windows.Forms.ToolStripButton _FolderUpButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __FolderUpButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__FolderUpButton != null)
                {
                    __FolderUpButton.Click -= FolderUpButton_Click;
                }

                __FolderUpButton = value;
                if (__FolderUpButton != null)
                {
                    __FolderUpButton.Click += FolderUpButton_Click;
                }
            }
        }

        private FolderTreeView __FolderTreeView;

        private FolderTreeView _FolderTreeView
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __FolderTreeView;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__FolderTreeView != null)
                {
                    __FolderTreeView.PathChanged -= FolderTreeView_PathChanged;
                }

                __FolderTreeView = value;
                if (__FolderTreeView != null)
                {
                    __FolderTreeView.PathChanged += FolderTreeView_PathChanged;
                }
            }
        }

        private ToolStrip _ToolStrip;
        private System.Windows.Forms.ToolStripButton __HomeToolStripButton;

        private System.Windows.Forms.ToolStripButton _HomeToolStripButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __HomeToolStripButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__HomeToolStripButton != null)
                {
                    __HomeToolStripButton.Click -= HomeToolStripButton_Click;
                }

                __HomeToolStripButton = value;
                if (__HomeToolStripButton != null)
                {
                    __HomeToolStripButton.Click += HomeToolStripButton_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripButton __BackToolStripButton;

        private System.Windows.Forms.ToolStripButton _BackToolStripButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __BackToolStripButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__BackToolStripButton != null)
                {
                    __BackToolStripButton.Click -= BackToolStripButton_Click;
                }

                __BackToolStripButton = value;
                if (__BackToolStripButton != null)
                {
                    __BackToolStripButton.Click += BackToolStripButton_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripButton __ForwardToolStripButton;

        private System.Windows.Forms.ToolStripButton _ForwardToolStripButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ForwardToolStripButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ForwardToolStripButton != null)
                {
                    __ForwardToolStripButton.Click -= ForwardToolStripButton_Click;
                }

                __ForwardToolStripButton = value;
                if (__ForwardToolStripButton != null)
                {
                    __ForwardToolStripButton.Click += ForwardToolStripButton_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripButton __AddFolderButton;

        private System.Windows.Forms.ToolStripButton _AddFolderButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __AddFolderButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__AddFolderButton != null)
                {
                    __AddFolderButton.Click -= AddFolderButton_Click;
                }

                __AddFolderButton = value;
                if (__AddFolderButton != null)
                {
                    __AddFolderButton.Click += AddFolderButton_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripButton __RefreshToolStripButton;

        private System.Windows.Forms.ToolStripButton _RefreshToolStripButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __RefreshToolStripButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__RefreshToolStripButton != null)
                {
                    __RefreshToolStripButton.Click -= RefreshButtonClick;
                }

                __RefreshToolStripButton = value;
                if (__RefreshToolStripButton != null)
                {
                    __RefreshToolStripButton.Click += RefreshButtonClick;
                }
            }
        }
    }
}