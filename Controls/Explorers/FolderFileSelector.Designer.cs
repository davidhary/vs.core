﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Controls
{
    [DesignerGenerated()]
    public partial class FolderFileSelector
    {
        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(FolderFileSelector));
            _FileListBoxLabel = new Label();
            _FileNamePatternCheckBoxLabel = new Label();
            _FolderTextBoxLabel = new Label();
            _FilePathNameTextBoxLabel = new Label();
            __FilePathNameTextBox = new System.Windows.Forms.TextBox();
            __FilePathNameTextBox.TextChanged += new EventHandler(FileTitleTextBox_TextChanged);
            __FilePathNameTextBox.Enter += new EventHandler(EditTextBox_Enter);
            __SelectFolderButton = new Button();
            __SelectFolderButton.Click += new EventHandler(SelectFolderButton_Click);
            __FolderTextBox = new System.Windows.Forms.TextBox();
            __FolderTextBox.Enter += new EventHandler(EditTextBox_Enter);
            __FolderTextBox.TextChanged += new EventHandler(FolderTextBox_TextChanged);
            __FilePatternComboBox = new System.Windows.Forms.ComboBox();
            __FilePatternComboBox.SelectedIndexChanged += new EventHandler(FilePatternComboBox_SelectedIndexChanged);
            _ToolTip = new ToolTip(components);
            __DataFileListBox = new FileListBox();
            __DataFileListBox.SelectedIndexChanged += new EventHandler(DataFileListBox_SelectedIndexChanged);
            __DataFileListBox.DoubleClick += new EventHandler(DataFileListBox_DoubleClick);
            _FileInfoTextBox = new RichTextBox();
            _FileInfoTextBoxLabel = new Label();
            __RefreshButton = new Button();
            __RefreshButton.Click += new EventHandler(RefreshButton_Click);
            _Annunciator = new ErrorProvider(components);
            ((System.ComponentModel.ISupportInitialize)_Annunciator).BeginInit();
            SuspendLayout();
            // 
            // _FileListBoxLabel
            // 
            _FileListBoxLabel.BackColor = Color.Transparent;
            _FileListBoxLabel.Cursor = Cursors.Default;
            _FileListBoxLabel.Font = new Font("Segoe UI", 9.75f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _FileListBoxLabel.ForeColor = SystemColors.WindowText;
            _FileListBoxLabel.Location = new Point(2, 94);
            _FileListBoxLabel.Name = "_FileListBoxLabel";
            _FileListBoxLabel.RightToLeft = RightToLeft.No;
            _FileListBoxLabel.Size = new Size(72, 18);
            _FileListBoxLabel.TabIndex = 5;
            _FileListBoxLabel.Text = "&Files:";
            _FileListBoxLabel.TextAlign = ContentAlignment.BottomLeft;
            // 
            // _FileNamePatternCheckBoxLabel
            // 
            _FileNamePatternCheckBoxLabel.BackColor = Color.Transparent;
            _FileNamePatternCheckBoxLabel.Cursor = Cursors.Default;
            _FileNamePatternCheckBoxLabel.Font = new Font("Segoe UI", 9.75f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _FileNamePatternCheckBoxLabel.ForeColor = SystemColors.WindowText;
            _FileNamePatternCheckBoxLabel.Location = new Point(3, 342);
            _FileNamePatternCheckBoxLabel.Name = "_FileNamePatternCheckBoxLabel";
            _FileNamePatternCheckBoxLabel.RightToLeft = RightToLeft.No;
            _FileNamePatternCheckBoxLabel.Size = new Size(72, 18);
            _FileNamePatternCheckBoxLabel.TabIndex = 7;
            _FileNamePatternCheckBoxLabel.Text = "File T&ype: ";
            _FileNamePatternCheckBoxLabel.TextAlign = ContentAlignment.BottomLeft;
            // 
            // _FolderTextBoxLabel
            // 
            _FolderTextBoxLabel.AutoSize = true;
            _FolderTextBoxLabel.BackColor = Color.Transparent;
            _FolderTextBoxLabel.Cursor = Cursors.Default;
            _FolderTextBoxLabel.Font = new Font("Segoe UI", 9.75f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _FolderTextBoxLabel.ForeColor = SystemColors.WindowText;
            _FolderTextBoxLabel.Location = new Point(3, 3);
            _FolderTextBoxLabel.Name = "_FolderTextBoxLabel";
            _FolderTextBoxLabel.RightToLeft = RightToLeft.No;
            _FolderTextBoxLabel.Size = new Size(48, 17);
            _FolderTextBoxLabel.TabIndex = 0;
            _FolderTextBoxLabel.Text = "F&older:";
            _FolderTextBoxLabel.TextAlign = ContentAlignment.BottomLeft;
            // 
            // _FilePathNameTextBoxLabel
            // 
            _FilePathNameTextBoxLabel.AutoSize = true;
            _FilePathNameTextBoxLabel.BackColor = Color.Transparent;
            _FilePathNameTextBoxLabel.Cursor = Cursors.Default;
            _FilePathNameTextBoxLabel.Font = new Font("Segoe UI", 9.75f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _FilePathNameTextBoxLabel.ForeColor = SystemColors.WindowText;
            _FilePathNameTextBoxLabel.Location = new Point(3, 51);
            _FilePathNameTextBoxLabel.Name = "_FilePathNameTextBoxLabel";
            _FilePathNameTextBoxLabel.RightToLeft = RightToLeft.No;
            _FilePathNameTextBoxLabel.Size = new Size(30, 17);
            _FilePathNameTextBoxLabel.TabIndex = 3;
            _FilePathNameTextBoxLabel.Text = "F&ile:";
            _FilePathNameTextBoxLabel.TextAlign = ContentAlignment.BottomLeft;
            // 
            // _FilePathNameTextBox
            // 
            __FilePathNameTextBox.AcceptsReturn = true;
            __FilePathNameTextBox.BackColor = SystemColors.Window;
            __FilePathNameTextBox.Cursor = Cursors.IBeam;
            __FilePathNameTextBox.Font = new Font("Arial", 9.75f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            __FilePathNameTextBox.ForeColor = SystemColors.WindowText;
            __FilePathNameTextBox.Location = new Point(3, 70);
            __FilePathNameTextBox.MaxLength = 0;
            __FilePathNameTextBox.Name = "__FilePathNameTextBox";
            __FilePathNameTextBox.RightToLeft = RightToLeft.No;
            __FilePathNameTextBox.Size = new Size(246, 22);
            __FilePathNameTextBox.TabIndex = 4;
            _ToolTip.SetToolTip(__FilePathNameTextBox, "The name of the file that was selected or entered");
            __FilePathNameTextBox.WordWrap = false;
            // 
            // _SelectFolderButton
            // 
            __SelectFolderButton.Font = new Font("Segoe UI Black", 9.75f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            __SelectFolderButton.Location = new Point(609, 22);
            __SelectFolderButton.Name = "__SelectFolderButton";
            __SelectFolderButton.Size = new Size(36, 26);
            __SelectFolderButton.TabIndex = 2;
            __SelectFolderButton.Text = "...";
            _ToolTip.SetToolTip(__SelectFolderButton, "Click to select a folder");
            // 
            // _FolderTextBox
            // 
            __FolderTextBox.AcceptsReturn = true;
            __FolderTextBox.BackColor = SystemColors.Window;
            __FolderTextBox.Cursor = Cursors.IBeam;
            __FolderTextBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            __FolderTextBox.ForeColor = SystemColors.WindowText;
            __FolderTextBox.Location = new Point(3, 22);
            __FolderTextBox.MaxLength = 0;
            __FolderTextBox.Name = "__FolderTextBox";
            __FolderTextBox.RightToLeft = RightToLeft.No;
            __FolderTextBox.Size = new Size(604, 25);
            __FolderTextBox.TabIndex = 1;
            _ToolTip.SetToolTip(__FolderTextBox, "The name of the file you have selected or entered");
            // 
            // _FilePatternComboBox
            // 
            __FilePatternComboBox.BackColor = SystemColors.Window;
            __FilePatternComboBox.Cursor = Cursors.Default;
            __FilePatternComboBox.Font = new Font("Arial", 9.75f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            __FilePatternComboBox.ForeColor = SystemColors.WindowText;
            __FilePatternComboBox.Items.AddRange(new object[] { "Data Files (*.dat)", "All Files (*.*)" });
            __FilePatternComboBox.Location = new Point(3, 362);
            __FilePatternComboBox.Name = "__FilePatternComboBox";
            __FilePatternComboBox.RightToLeft = RightToLeft.No;
            __FilePatternComboBox.Size = new Size(184, 24);
            __FilePatternComboBox.TabIndex = 8;
            __FilePatternComboBox.Text = "*.dat";
            _ToolTip.SetToolTip(__FilePatternComboBox, "Determines the type listed in the file list");
            // 
            // _DataFileListBox
            // 
            __DataFileListBox.BackColor = SystemColors.Window;
            __DataFileListBox.Cursor = Cursors.Default;
            __DataFileListBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            __DataFileListBox.ForeColor = Color.Black;
            __DataFileListBox.FormattingEnabled = true;
            __DataFileListBox.ItemHeight = 17;
            __DataFileListBox.Location = new Point(3, 113);
            __DataFileListBox.Name = "__DataFileListBox";
            __DataFileListBox.DirectoryPath = string.Empty;
            __DataFileListBox.SearchPattern = "*.dat";
            __DataFileListBox.ReadOnlyBackColor = SystemColors.Control;
            __DataFileListBox.ReadOnlyForeColor = SystemColors.WindowText;
            __DataFileListBox.ReadWriteBackColor = SystemColors.Window;
            __DataFileListBox.ReadWriteForeColor = SystemColors.ControlText;
            __DataFileListBox.SelectionMode = SelectionMode.MultiExtended;
            __DataFileListBox.Size = new Size(248, 225);
            __DataFileListBox.TabIndex = 6;
            _ToolTip.SetToolTip(__DataFileListBox, "Select file(s)");
            // 
            // _FileInfoTextBox
            // 
            _FileInfoTextBox.Location = new Point(273, 70);
            _FileInfoTextBox.Name = "_FileInfoTextBox";
            _FileInfoTextBox.Size = new Size(371, 316);
            _FileInfoTextBox.TabIndex = 10;
            _FileInfoTextBox.Text = string.Empty;
            _ToolTip.SetToolTip(_FileInfoTextBox, "Displays file information if available");
            // 
            // _FileInfoTextBoxLabel
            // 
            _FileInfoTextBoxLabel.AutoSize = true;
            _FileInfoTextBoxLabel.Location = new Point(272, 51);
            _FileInfoTextBoxLabel.Name = "_FileInfoTextBoxLabel";
            _FileInfoTextBoxLabel.Size = new Size(56, 17);
            _FileInfoTextBoxLabel.TabIndex = 11;
            _FileInfoTextBoxLabel.Text = "File info:";
            // 
            // _RefreshButton
            // 
            __RefreshButton.Font = new Font("Segoe UI", 9.75f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            __RefreshButton.Image = (Image)resources.GetObject("_RefreshButton.Image");
            __RefreshButton.Location = new Point(214, 352);
            __RefreshButton.Name = "__RefreshButton";
            __RefreshButton.Size = new Size(35, 34);
            __RefreshButton.TabIndex = 9;
            _ToolTip.SetToolTip(__RefreshButton, "Click to select a folder");
            __RefreshButton.UseCompatibleTextRendering = true;
            // 
            // _Annunciator
            // 
            _Annunciator.ContainerControl = this;
            // 
            // FolderFileSelector
            // 
            AutoScaleMode = AutoScaleMode.Inherit;
            Controls.Add(_FileInfoTextBoxLabel);
            Controls.Add(_FileInfoTextBox);
            Controls.Add(_FileListBoxLabel);
            Controls.Add(_FileNamePatternCheckBoxLabel);
            Controls.Add(_FolderTextBoxLabel);
            Controls.Add(_FilePathNameTextBoxLabel);
            Controls.Add(__FilePathNameTextBox);
            Controls.Add(__FilePatternComboBox);
            Controls.Add(__RefreshButton);
            Controls.Add(__SelectFolderButton);
            Controls.Add(__FolderTextBox);
            Controls.Add(__DataFileListBox);
            Name = "FolderFileSelector";
            Size = new Size(650, 390);
            ((System.ComponentModel.ISupportInitialize)_Annunciator).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        private Label _FileListBoxLabel;
        private Label _FileNamePatternCheckBoxLabel;
        private Label _FolderTextBoxLabel;
        private ToolTip _ToolTip;
        private Label _FilePathNameTextBoxLabel;
        private System.Windows.Forms.TextBox __FilePathNameTextBox;

        private System.Windows.Forms.TextBox _FilePathNameTextBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __FilePathNameTextBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__FilePathNameTextBox != null)
                {
                    __FilePathNameTextBox.TextChanged -= FileTitleTextBox_TextChanged;
                    __FilePathNameTextBox.Enter -= EditTextBox_Enter;
                }

                __FilePathNameTextBox = value;
                if (__FilePathNameTextBox != null)
                {
                    __FilePathNameTextBox.TextChanged += FileTitleTextBox_TextChanged;
                    __FilePathNameTextBox.Enter += EditTextBox_Enter;
                }
            }
        }

        private Button __RefreshButton;

        private Button _RefreshButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __RefreshButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__RefreshButton != null)
                {
                    __RefreshButton.Click -= RefreshButton_Click;
                }

                __RefreshButton = value;
                if (__RefreshButton != null)
                {
                    __RefreshButton.Click += RefreshButton_Click;
                }
            }
        }

        private Button __SelectFolderButton;

        private Button _SelectFolderButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SelectFolderButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SelectFolderButton != null)
                {
                    __SelectFolderButton.Click -= SelectFolderButton_Click;
                }

                __SelectFolderButton = value;
                if (__SelectFolderButton != null)
                {
                    __SelectFolderButton.Click += SelectFolderButton_Click;
                }
            }
        }

        private System.Windows.Forms.TextBox __FolderTextBox;

        private System.Windows.Forms.TextBox _FolderTextBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __FolderTextBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__FolderTextBox != null)
                {
                    __FolderTextBox.Enter -= EditTextBox_Enter;
                    __FolderTextBox.TextChanged -= FolderTextBox_TextChanged;
                }

                __FolderTextBox = value;
                if (__FolderTextBox != null)
                {
                    __FolderTextBox.Enter += EditTextBox_Enter;
                    __FolderTextBox.TextChanged += FolderTextBox_TextChanged;
                }
            }
        }

        private System.Windows.Forms.ComboBox __FilePatternComboBox;

        private System.Windows.Forms.ComboBox _FilePatternComboBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __FilePatternComboBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__FilePatternComboBox != null)
                {
                    __FilePatternComboBox.SelectedIndexChanged -= FilePatternComboBox_SelectedIndexChanged;
                }

                __FilePatternComboBox = value;
                if (__FilePatternComboBox != null)
                {
                    __FilePatternComboBox.SelectedIndexChanged += FilePatternComboBox_SelectedIndexChanged;
                }
            }
        }

        private FileListBox __DataFileListBox;

        private FileListBox _DataFileListBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __DataFileListBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__DataFileListBox != null)
                {
                    __DataFileListBox.SelectedIndexChanged -= DataFileListBox_SelectedIndexChanged;
                    __DataFileListBox.DoubleClick -= DataFileListBox_DoubleClick;
                }

                __DataFileListBox = value;
                if (__DataFileListBox != null)
                {
                    __DataFileListBox.SelectedIndexChanged += DataFileListBox_SelectedIndexChanged;
                    __DataFileListBox.DoubleClick += DataFileListBox_DoubleClick;
                }
            }
        }

        private ErrorProvider _Annunciator;
        private Label _FileInfoTextBoxLabel;
        private RichTextBox _FileInfoTextBox;
    }
}