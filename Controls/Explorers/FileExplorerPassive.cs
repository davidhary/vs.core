using System;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace isr.Core.Controls
{

    /// <summary> A file explorer. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-07-04 </para>
    /// </remarks>
    [Description( "File explorer" )]
    [ToolboxBitmap( typeof( FileExplorerPassive ), "FileExplorerPassive.gif" )]
    public class FileExplorerPassive : Forma.ModelViewBase
    {

        #region " CONSTRUCTION "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public FileExplorerPassive() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
        }

        /// <summary>
        /// Releases the unmanaged resources used by the isr.Core.Forma.ModelViewBase and optionally
        /// releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this._Components is object )
                    {
                        this._Components.Dispose();
                        this._Components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " DESIGNER "

        /// <summary> The splitter. </summary>
        private Splitter _Splitter;

        /// <summary> The with events. </summary>
        private FolderTreeViewPassive _FolderTreeView;

        private FolderTreeViewPassive FolderTreeView
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._FolderTreeView;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._FolderTreeView != null )
                {
                    this._FolderTreeView.AfterSelect -= this.FolderTreeViewAfterSelect;
                }

                this._FolderTreeView = value;
                if ( this._FolderTreeView != null )
                {
                    this._FolderTreeView.AfterSelect += this.FolderTreeViewAfterSelect;
                }
            }
        }

        /// <summary> The file list view. </summary>
        private FileListView _FileListView;

        /// <summary> The components. </summary>
        private IContainer _Components;

        /// <summary>
        /// Required method for Designer support - do not modify the contents of this method with the
        /// code editor.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void InitializeComponent()
        {
            this._Components = new Container();
            var TreeNode1 = new TreeNode( @"C:\", 6, 6 );
            var TreeNode2 = new TreeNode( @"D:\", 7, 7 );
            var TreeNode3 = new TreeNode( "My Computer", 0, 0, new TreeNode[] { TreeNode1, TreeNode2 } );
            this._FolderTreeView = new FolderTreeViewPassive();
            this._FolderTreeView.AfterSelect += new TreeViewEventHandler( this.FolderTreeViewAfterSelect );
            this._Splitter = new Splitter();
            this._FileListView = new FileListView();
            this.SuspendLayout();
            // 
            // _FolderTreeView
            // 
            this._FolderTreeView.Cursor = Cursors.Default;
            this._FolderTreeView.Dock = DockStyle.Left;
            this._FolderTreeView.ImageIndex = 0;
            this._FolderTreeView.Location = new Point( 0, 0 );
            this._FolderTreeView.Name = "__FolderTreeView";
            TreeNode1.ImageIndex = 6;
            TreeNode1.Name = string.Empty;
            TreeNode1.SelectedImageIndex = 6;
            TreeNode1.Text = @"C:\";
            TreeNode2.ImageIndex = 7;
            TreeNode2.Name = string.Empty;
            TreeNode2.SelectedImageIndex = 7;
            TreeNode2.Text = @"D:\";
            TreeNode3.ImageIndex = 0;
            TreeNode3.Name = string.Empty;
            TreeNode3.SelectedImageIndex = 0;
            TreeNode3.Text = "My Computer";
            this._FolderTreeView.Nodes.AddRange( new TreeNode[] { TreeNode3 } );
            this._FolderTreeView.Path = string.Empty;
            this._FolderTreeView.SelectedImageIndex = 0;
            this._FolderTreeView.Size = new Size( 168, 357 );
            this._FolderTreeView.TabIndex = 0;
            // 
            // _Splitter
            // 
            this._Splitter.Location = new Point( 168, 0 );
            this._Splitter.Name = "_Splitter";
            this._Splitter.Size = new Size( 3, 357 );
            this._Splitter.TabIndex = 3;
            this._Splitter.TabStop = false;
            // 
            // _FileListView
            // 
            this._FileListView.Dock = DockStyle.Fill;
            this._FileListView.HideSelection = false;
            this._FileListView.Location = new Point( 171, 0 );
            this._FileListView.Name = "_FileListView";
            this._FileListView.Size = new Size( 429, 357 );
            this._FileListView.TabIndex = 1;
            this._FileListView.UseCompatibleStateImageBehavior = false;
            this._FileListView.View = View.Details;
            // 
            // FileExplorerPassive
            // 
            this.Controls.Add( this._FileListView );
            this.Controls.Add( this._Splitter );
            this.Controls.Add( this._FolderTreeView );
            this.Name = "FileExplorerPassive";
            this.Size = new Size( 600, 357 );
            this.ResumeLayout( false );
        }

        #endregion

        #region " CONTROL EVENTS "

        /// <summary> Handles the after select. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Tree view event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void FolderTreeViewAfterSelect( object sender, TreeViewEventArgs e )
        {
            // Populate folders and files when a folder is selected
            this.Cursor = Cursors.WaitCursor;
            string fullPath = string.Empty;
            try
            {
                var folderTreeNode = e.Node;
                fullPath = FolderTreeViewPassive.ParseFullPath( e.Node.FullPath );
                if ( folderTreeNode is null || folderTreeNode.SelectedImageIndex == 0 )
                {
                }
                // nothing to do: leave clear file list
                else if ( System.IO.Directory.Exists( fullPath ) )
                {
                    this._FileListView.PopulateFiles( fullPath, "*.*" );
                }
                else
                {
                }
            }
            catch ( UnauthorizedAccessException )
            {
                _ = MessageBox.Show( $"Access denied to folder {fullPath}", "Access denied", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly );
            }
            catch ( Exception ex )
            {
                _ = MessageBox.Show( $"Error: {ex}" );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

    }
}
