using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;

namespace isr.Core.Controls
{
    public partial class FolderTreeView
    {

        /// <summary> Network computers. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> A list of. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private static IList<string> NetworkComputers()
        {
            var servers = new List<string>();
            // TO_DO: Use Win32_MyComputerSystem node instead of Win NT
            using ( var root = new System.DirectoryServices.DirectoryEntry( "WinNT:" ) )
            {
                foreach ( System.DirectoryServices.DirectoryEntries entries in root.Children )
                {
                    foreach ( System.DirectoryServices.DirectoryEntry entry in entries )
                    {
                        if ( entry.SchemaClassName == "Computer" )
                        {
                            servers.Add( entry.Name );
                        }
                    }
                }
            }

            return servers;
        }

        /// <summary> Expand entire network node. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> Tree view event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private static void ExpandEntireNetworkNode( TreeViewEventArgs e )
        {
            var n = e.Node;
            TreeNode nodeNN;
            TreeNode nodemN;
            if ( string.Equals( n.Text, "Entire Network", StringComparison.OrdinalIgnoreCase ) )
            {
                if ( n.FirstNode.Text == "Network Node" )
                {
                    n.FirstNode.Remove();

                    // NETRESOURCE netRoot = new NETRESOURCE();
                    using ( var shares = new System.Management.ManagementClass() )
                    {
                    }

                    var servers = new ArrayList();
                    using ( var root = new System.DirectoryServices.DirectoryEntry( "WinNT:" ) )
                    {
                        foreach ( System.DirectoryServices.DirectoryEntries entries in root.Children )
                        {
                            foreach ( System.DirectoryServices.DirectoryEntry entry in entries )
                            {
                                _ = servers.Add( entry.Name );
                            }
                        }
                    }

                    foreach ( string s1 in servers )
                    {
                        string s2 = string.Empty;
                        s2 = s1.Substring( 0, s1.IndexOf( "|", 1, StringComparison.OrdinalIgnoreCase ) );
                        if ( s1.IndexOf( "NETWORK", 1, StringComparison.OrdinalIgnoreCase ) > 0 )
                        {
                            nodeNN = new TreeNode() {
                                Tag = s2,
                                Text = s2, // dir.Substring(dir.LastIndexOf(@"\") + 1);
                                ImageIndex = 15,
                                SelectedImageIndex = 15
                            };
                            _ = n.Nodes.Add( nodeNN );
                        }
                        else
                        {
                            // need to handle the VMWare situation where 
                            nodemN = new TreeNode() {
                                Tag = s2, // "my Node";
                                Text = s2, // "my Node";//dir.Substring(dir.LastIndexOf(@"\") + 1);
                                ImageIndex = 16,
                                SelectedImageIndex = 16
                            };
                            // DH: this does not work. there are initially no nodes here
                            // so last node is null.
                            if ( n.LastNode is null )
                            {
                                _ = n.Nodes.Add( nodemN );
                            }
                            else
                            {
                                _ = n.LastNode.Nodes.Add( nodemN );
                            }

                            TreeNode nodemNc;
                            nodemNc = new TreeNode() {
                                Tag = _MyNetworkNodeName,
                                Text = _MyNetworkNodeName, // dir.Substring(dir.LastIndexOf(@"\") + 1);
                                ImageIndex = 12,
                                SelectedImageIndex = 12
                            };
                            _ = nodemN.Nodes.Add( nodemNc );
                        }
                    }
                }
            }
        }

        /// <summary> Name of my network node. </summary>
        private const string _MyNetworkNodeName = "My Node";

        /// <summary> Expand Microsoft windows network node. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> Tree view event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private static void ExpandMicrosoftWindowsNetworkNode( TreeViewEventArgs e )
        {
            var n = e.Node;
            TreeNode nodeNN;
            TreeNode nodeNNode;
            if ( n.Parent is object && string.Equals( n.Parent.Text, "Microsoft Windows Network", StringComparison.OrdinalIgnoreCase ) )
            {
                if ( string.Equals( n.FirstNode.Text, _MyNetworkNodeName, StringComparison.OrdinalIgnoreCase ) )
                {
                    n.FirstNode.Remove();

                    // NETRESOURCE netRoot = new NETRESOURCE();
                    // Dim servers As New ServerEnum(ResourceScope.RESOURCE_GLOBALNET, ResourceType.RESOURCETYPE_DISK, 
                    // ResourceUsage.RESOURCEUSAGE_ALL, ResourceDisplayType.RESOURCEDISPLAYTYPE_SERVER, n.Text)
                    var servers = Array.Empty<string>(); // ServerEnum(n.Text)
                    foreach ( string s1 in servers )
                    {
                        if ( s1.Length < 6 || !string.Equals( s1.Substring( s1.Length - 6, 6 ), "-share", StringComparison.OrdinalIgnoreCase ) )
                        {
                            string s2 = s1;
                            nodeNN = new TreeNode() {
                                Tag = s2,
                                Text = s2.Substring( 2 ),
                                ImageIndex = 12,
                                SelectedImageIndex = 12
                            };
                            _ = n.Nodes.Add( nodeNN );
                            foreach ( string s1node in servers )
                            {
                                if ( s1node.Length > 6 )
                                {
                                    if ( string.Equals( s1node.Substring( s1node.Length - 6, 6 ), "-share", StringComparison.OrdinalIgnoreCase ) )
                                    {
                                        if ( s2.Length <= s1node.Length )
                                        {
                                            try
                                            {
                                                if ( string.Equals( s1node.Substring( 0, s2.Length + 1 ), s2 + @"\", StringComparison.OrdinalIgnoreCase ) )
                                                {
                                                    nodeNNode = new TreeNode() {
                                                        Tag = s1node.Substring( 0, s1node.Length - 6 ),
                                                        Text = s1node.Substring( s2.Length + 1, s1node.Length - s2.Length - 7 ),
                                                        ImageIndex = 28,
                                                        SelectedImageIndex = 28
                                                    };
                                                    _ = nodeNN.Nodes.Add( nodeNNode );
                                                }
                                            }
                                            catch ( Exception )
                                            {
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
