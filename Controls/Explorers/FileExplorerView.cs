using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Controls
{

    /// <summary> Folder and file explorer view. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-07-04 </para>
    /// </remarks>
    [Description( "Folder and file explorer" )]
    [ToolboxBitmap( typeof( FileExplorerView ), "FileExplorerView.gif" )]
    public class FileExplorerView : Forma.ModelViewBase
    {

        #region " CONSTRUCTION "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public FileExplorerView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this._FolderExplorer.FolderTreeView.PathChanged += this.FolderTreeViewControlPathChanged;
        }

        /// <summary>
        /// Releases the unmanaged resources used by the isr.Core.Forma.ModelViewBase and optionally
        /// releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this._Components is object )
                    {
                        this._Components.Dispose();
                        this._Components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " DESIGNER "

        /// <summary> The splitter. </summary>
        private Splitter _Splitter;

        /// <summary>   The folder explorer. </summary>
        private FolderTreeViewControl _FolderExplorer;

        /// <summary>   Gets or sets the file list view control. </summary>
        /// <value> The file list view control. </value>
        internal FileListViewControl FileListViewControl { get; set; }

        /// <summary> The components. </summary>
        private IContainer _Components;

        /// <summary>
        /// Required method for Designer support - do not modify the contents of this method with the
        /// code editor.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void InitializeComponent()
        {
            this._FolderExplorer = new FolderTreeViewControl();
            this._Splitter = new Splitter();
            this.FileListViewControl = new FileListViewControl();
            this.SuspendLayout();
            // 
            // _FolderExplorer
            // 
            this._FolderExplorer.BackColor = SystemColors.Control;
            this._FolderExplorer.Dock = DockStyle.Left;
            this._FolderExplorer.Font = new Font( "Segoe UI", 9.0f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte( 0 ) );
            this._FolderExplorer.Location = new Point( 0, 0 );
            this._FolderExplorer.Name = "_FolderExplorer";
            this._FolderExplorer.ShowCurrentPathTextBox = true;
            this._FolderExplorer.ShowMyDocuments = true;
            this._FolderExplorer.ShowMyFavorites = true;
            this._FolderExplorer.ShowMyNetwork = true;
            this._FolderExplorer.ShowToolBar = true;
            this._FolderExplorer.Size = new Size( 168, 357 );
            this._FolderExplorer.TabIndex = 0;
            // 
            // _Splitter
            // 
            this._Splitter.Location = new Point( 168, 0 );
            this._Splitter.Name = "_Splitter";
            this._Splitter.Size = new Size( 3, 357 );
            this._Splitter.TabIndex = 3;
            this._Splitter.TabStop = false;
            // 
            // _FileListViewControl
            // 
            this.FileListViewControl.Dock = DockStyle.Fill;
            this.FileListViewControl.Font = new Font( "Segoe UI", 9.0f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte( 0 ) );
            this.FileListViewControl.HideFileInfo = false;
            this.FileListViewControl.Location = new Point( 171, 0 );
            this.FileListViewControl.Name = "_FileListViewControl";
            this.FileListViewControl.DirectoryPath = null;
            this.FileListViewControl.SearchPatternDelimiter = ';';
            this.FileListViewControl.SearchPatterns = "*.*;*.bmp, *.jpg, *.png;*.csv, *.txt";
            this.FileListViewControl.Size = new Size( 429, 357 );
            this.FileListViewControl.TabIndex = 1;
            // 
            // FileExplorerView
            // 
            this.Controls.Add( this.FileListViewControl );
            this.Controls.Add( this._Splitter );
            this.Controls.Add( this._FolderExplorer );
            this.Name = "FileExplorerView";
            this.Size = new Size( 600, 357 );
            this.ResumeLayout( false );
        }

        #endregion

        #region " CONTROL EVENTS "

        /// <summary> Select home. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public void SelectHome()
        {
            _ = this._FolderExplorer.FolderTreeView.TrySelectHome();
        }

        /// <summary>
        /// Forces the control to invalidate its client area and immediately redraw itself and any child
        /// controls.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public override void Refresh()
        {
            base.Refresh();
            this._FolderExplorer.Refresh();
        }

        /// <summary> Handles the path change. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Tree view event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void FolderTreeViewControlPathChanged( object sender, EventArgs e )
        {
            // Populate folders and files when a folder is selected
            this.Cursor = Cursors.WaitCursor;
            try
            {
                this.FileListViewControl.DirectoryPath = this._FolderExplorer.SelectedPath;
                this.FileListViewControl.Refresh();
            }
            // Me._FileListViewControl.FileListView.PopulateFiles(Me._FolderExplorer.SelectedPath)
            catch ( UnauthorizedAccessException )
            {
                _ = MessageBox.Show( $"Access denied to folder {this._FolderExplorer.SelectedPath}", "Access denied", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly );
            }
            catch ( Exception ex )
            {
                _ = MessageBox.Show( $"Error: {ex}" );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

    }
}
