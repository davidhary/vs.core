using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Controls
{

    /// <summary> A file list view. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-07-04 </para>
    /// </remarks>
    [Description( "File list view" )]
    [ToolboxBitmap( typeof( FileListView ), "FileListView.gif" )]
    public class FileListView : System.Windows.Forms.ListView
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public FileListView() : base()
        {
            this.SmallImageList = new ImageList();
            this.SmallImageList.Images.Add( My.Resources.Resources.file );
        }

        /// <summary>
        /// Releases the unmanaged resources used by the isr.Core.Forma.ModelViewBase and optionally
        /// releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " CONSTRUCTION "

        /// <summary> Initializes the list view. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public void InitListView()
        {
            // initialize the file list view 
            this.Clear(); // clear control
                          // create column header for ListView
            _ = this.Columns.Add( "Name", 150, HorizontalAlignment.Left );
            _ = this.Columns.Add( "Size", 75, HorizontalAlignment.Right );
            _ = this.Columns.Add( "Created", 140, HorizontalAlignment.Left );
            _ = this.Columns.Add( "Modified", 140, HorizontalAlignment.Left );
        }

        /// <summary> Parse path name. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="pathName"> Full pathname. </param>
        /// <returns> A String. </returns>
        private static string ParsePathName( string pathName )
        {
            if ( string.IsNullOrWhiteSpace( pathName ) )
            {
                return pathName;
            }
            else
            {
                var stringSplit = pathName.Split( '\\' );
                int _maxIndex = stringSplit.Length;
                return stringSplit[_maxIndex - 1];
            }
        }

        /// <summary> Populate files. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="path"> Full pathname of the file. </param>
        public void PopulateFiles( string path )
        {
            this.PopulateFiles( path, "*.*" );
        }

        /// <summary> Enumerates select files in this collection. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="path">           Full pathname of the file. </param>
        /// <param name="searchPatterns"> The search string to match against the names of files in path.
        /// This parameter can contain a combination of valid literal
        /// path and wildcard (* and ?) characters (see Remarks), but
        /// doesn't support regular expressions. </param>
        /// <param name="delimiter">      The delimiter. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process select files in this collection.
        /// </returns>
        private static IList<string> SelectFiles( string path, string searchPatterns, char delimiter )
        {
            var fileNames = new List<string>();
            if ( string.IsNullOrWhiteSpace( searchPatterns ) || searchPatterns.Contains( "*.*" ) )
            {
                fileNames = new List<string>( Directory.EnumerateFiles( path ) );
            }
            else
            {
                foreach ( string searchPattern in searchPatterns.Split( delimiter ) )
                {
                    fileNames.AddRange( Directory.EnumerateFiles( path, searchPattern ) );
                }

                fileNames.Sort();
            }

            return fileNames;
        }

        /// <summary>
        /// Converts a UTC timestamp to an explorer time, which is incorrect when daylight saving time
        /// (DST) was in a different state to now.
        /// </summary>
        /// <remarks>
        /// The file times are different between .NET and Explorer if daylight saving time (DST) was in a
        /// different state to now.
        /// .NET is processing this difference differently to Explorer and Scripting.FileSystemObject.
        /// You can see a similar issue when extracting files from a zip file when DST Is different to
        /// when the files were zipped. Via Reflector, the reason .NET differs from the non .NET code
        /// paths Is that all three local timestamps in <see cref="File"/> and
        /// <see cref="FileInfo"/>
        /// are implemented As getting the UTC timestamp and applying .ToLocalTime, which determines the
        /// offset to add based upon whether DST was happening in the local timezone For the UTC time. So,
        /// in summary, .NET Is more correct. But if you want the incorrect, but same as Explorer, date
        /// use This has been discussed On Raymond Chen's blog
        /// http://blogs.msdn.com/b/oldnewthing/archive/2003/10/24/55413.aspx. Raymond's summary is:
        /// Win32 does not attempt to guess which time zone rules were in effect at that other time. So
        /// Win32 says, “Thursday, October 17, 2002 8:45:38 AM PST”. Note: Pacific Standard Time. Even
        /// though October 17 was during Pacific Daylight Time, Win32 displays the time as standard time
        /// because that’s what time it is now.
        /// .NET says, “Well, if the rules in effect now were also in effect on October 17, 2003, then
        /// that would be daylight time” so it displays “Thursday, October 17, 2003, 9:45 AM PDT” –
        /// daylight time. So .NET gives a value which Is more intuitively correct, but Is also
        /// potentially incorrect, And which Is Not invertible. Win32 gives a value which Is intuitively
        /// incorrect, but Is strictly correct. In short: .NET is intuitively correct but not always
        /// invertible, and Win32 is strictly correct and invertible).
        /// https://stackoverflow.com/questions/4605983/io-file-getlastaccesstime-is-off-by-one-hour.
        /// </remarks>
        /// <param name="fileInfoUtcTime"> The file information UTC time. </param>
        /// <returns> Corrected local time. </returns>
        public static DateTime ToExplorerLocalTime( DateTime fileInfoUtcTime )
        {
            return fileInfoUtcTime.Add( TimeZone.CurrentTimeZone.GetUtcOffset( DateAndTime.Now ) );
        }

        /// <summary> Legacy to-explorer local time. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="fileInfoLocalTime"> The file information local time. </param>
        /// <returns> A DateTime. </returns>
        public static DateTime LegacyToExplorerLocalTime( DateTime fileInfoLocalTime )
        {
            if ( TimeZone.CurrentTimeZone.IsDaylightSavingTime( fileInfoLocalTime ) == false )
            {
                // adjust time for day light saving time 
                return fileInfoLocalTime.AddHours( 1d );
            }
            else
            {
                // not in day light saving time
                return fileInfoLocalTime;
            }
        }

        /// <summary> Populate files. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="path">           Full pathname of the file. </param>
        /// <param name="searchPatterns"> The search string to match against the names of files in path.
        /// This parameter can contain a combination of valid literal
        /// path and wildcard (* and ?) characters (see Remarks), but
        /// doesn't support regular expressions. </param>
        public void PopulateFiles( string path, string searchPatterns )
        {

            // Populate list view with files
            var columnValues = new string[4];

            // clear list
            this.InitListView();

            // check path
            if ( Directory.Exists( path ) )
            {
                var fileNames = SelectFiles( path, searchPatterns, ',' );
                // loop through all files
                foreach ( var fileName in fileNames )
                {
                    var fileInfo = new FileInfo( fileName );
                    long fileSize = fileInfo.Length;

                    // create list view data
                    columnValues[0] = ParsePathName( fileName );
                    columnValues[1] = this.ShowSizeKilobytes ? fileSize > 1024L ? $"{fileSize / 1024d:N0} K" : $"{fileSize:N0} B" : fileSize.ToString( "N0", System.Globalization.CultureInfo.CurrentCulture );

                    // file creation time
                    columnValues[2] = FormatFileDate( ToExplorerLocalTime( fileInfo.CreationTimeUtc ) );

                    // file modify time
                    columnValues[3] = FormatFileDate( ToExplorerLocalTime( fileInfo.LastWriteTimeUtc ) );

                    // Create actual list item
                    var listViewItem = new ListViewItem( columnValues, 0 ) { Tag = fileName };
                    _ = this.Items.Add( listViewItem );
                }
            }
        }

        /// <summary> Gets or sets the show size kilo bytes. </summary>
        /// <value> The show size kilo bytes. </value>
        [Category( "Options" )]
        [Description( "Show size in Kilo Bytes." )]
        [DefaultValue( false )]
        [Browsable( true )]
        public bool ShowSizeKilobytes { get; set; }

        /// <summary> Formats the date using short date and time values. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> The formatted date. </returns>
        private static string FormatFileDate( DateTime value )
        {
            return $"{value.ToShortDateString()} {value.ToShortTimeString()}";
        }

        #endregion

        #region " SELECTIONS "

        /// <summary> Enumerates selected file names in this collection. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns>
        /// An enumerator that allows for each to be used to process selected file names in this
        /// collection.
        /// </returns>
        public IList<string> EnumerateSelectedFileNames()
        {
            var result = new List<string>();
            foreach ( ListViewItem item in this.SelectedItems )
            {
                result.Add( Conversions.ToString( item.Tag ) );
            }

            return result;
        }

        #endregion

    }
}
