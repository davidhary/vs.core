using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace isr.Core.Controls
{

    /// <summary> A folder file selector. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-07-03 </para>
    /// </remarks>
    public partial class FolderFileSelector : Forma.ModelViewBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Prevents a default instance of the <see cref="FolderFileSelector" /> class from being created.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private FolderFileSelector() : base()
        {

            // This method is required by the Windows Form Designer.
            this.InitializeComponent();
            this.__FilePathNameTextBox.Name = "_FilePathNameTextBox";
            this.__SelectFolderButton.Name = "_SelectFolderButton";
            this.__FolderTextBox.Name = "_FolderTextBox";
            this.__FilePatternComboBox.Name = "_FilePatternComboBox";
            this.__DataFileListBox.Name = "_DataFileListBox";
            this.__RefreshButton.Name = "_RefreshButton";
        }

        #endregion

        #region " METHODS  and  PROPERTIES "

        /// <summary> Adds a file name pattern. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="pattern"> Specify the file type and pattern such as "Data Files (*.dat)" or
        /// "Pictures (*.bmp; *.jpg)". </param>
        public void AddFilePattern( string pattern )
        {
            if ( string.IsNullOrWhiteSpace( pattern ) )
            {
                this._FilePatternComboBox.Items.Clear();
            }
            else
            {
                // Add an item to the combo box.
                _ = this._FilePatternComboBox.Items.Add( pattern );
            }
        }

        /// <summary> Gets or sets the caption of the form. </summary>
        /// <value> The caption. </value>
        public string Caption
        {
            get => this.Text;

            set => this.Text = value;
        }

        /// <summary> Gets or sets the data file list selection mode. </summary>
        /// <value> The selection mode. </value>
        public SelectionMode SelectionMode
        {
            get => this._DataFileListBox.SelectionMode;

            set => this._DataFileListBox.SelectionMode = value;
        }

        /// <summary> Returns the number of selected files. </summary>
        /// <value> The selected count. </value>
        public int SelectedCount => this._DataFileListBox.SelectedItems.Count;

        /// <summary> Gets the array of selected files. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> System.String[][]. </returns>
        public string[] SelectedFiles()
        {
            var selected = new string[this._DataFileListBox.SelectedItems.Count];
            this._DataFileListBox.SelectedItems.CopyTo( selected, 0 );
            return selected;
        }

        /// <summary> Returns the selected file names. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> System.String[][]. </returns>
        public string[] SelectedFilePathNames()
        {
            if ( this.SelectedFiles() is null || this.SelectedFiles().Length == 0 )
            {
                return Array.Empty<string>();
            }
            else
            {
                var filePathNames = new List<string>();
                foreach ( string fileName in this.SelectedFiles() )
                {
                    filePathNames.Add( System.IO.Path.Combine( this.SelectedPathName, fileName ) );
                }

                return filePathNames.ToArray();
            }
        }

        /// <summary> Returns the path of the selected file. </summary>
        /// <value> The name of the selected path. </value>
        public string SelectedPathName
        {
            get =>
                // Set file path.
                this._DataFileListBox.DirectoryPath;

            set {
                this._FolderTextBox.Text = value;
                if ( System.IO.Directory.Exists( value ) )
                {
                    // clear the previous path so that we get a refresh
                    this._DataFileListBox.DirectoryPath = Environment.CurrentDirectory;
                    this._DataFileListBox.Invalidate();
                    for ( int i = 1; i <= 1000; i++ )
                    {
                        ApplianceBase.DoEvents();
                    }

                    this._DataFileListBox.DirectoryPath = value;
                    this._DataFileListBox.Invalidate();
                }
            }
        }

        /// <summary> Returns the name of the select file. </summary>
        /// <value> The selected filename. </value>
        public string SelectedFileName
        {
            get =>
                // return the file title.
                this._FilePathNameTextBox.Text;

            set => this._FilePathNameTextBox.Text = value;
        }

        /// <summary> Returns the title of the select file and path. </summary>
        /// <value> The name of the selected file path. </value>
        public string SelectedFilePathName =>
                // return the file title.
                System.IO.Path.Combine( this.SelectedPathName, this.SelectedFileName );

        /// <summary> Returns the title of the select file. </summary>
        /// <value> The selected file title. </value>
        public string SelectedFileTitle =>
                // return the file title.
                string.IsNullOrWhiteSpace( this.SelectedFileName ) ? string.Empty : this.SelectedFileName.Substring( 0, this.SelectedFileName.LastIndexOf( ".", StringComparison.OrdinalIgnoreCase ) );

        /// <summary> Returns the file type pattern. </summary>
        /// <value> The pattern. </value>
        public string Pattern
        {
            get => this._FilePatternComboBox.Text;

            set {
                this._FilePatternComboBox.Text = value;
                if ( !string.IsNullOrWhiteSpace( value ) )
                {
                    int firstLocation = value.IndexOf( "(", StringComparison.OrdinalIgnoreCase ) + 1;
                    int patternLength = value.IndexOf( ")", firstLocation, StringComparison.OrdinalIgnoreCase ) - firstLocation;
                    this._DataFileListBox.SearchPattern = value.Substring( firstLocation, patternLength );
                }
            }
        }

        /// <summary> Gets or sets the status message. </summary>
        /// <remarks> Use this property to get the status message generated by the object. </remarks>
        /// <value> A <see cref="System.String">String</see>. </value>
        public string StatusMessage { get; set; }

        #endregion

        #region " PRIVATE  and  PROTECTED "

        /// <summary> Verifies the file name and provides alerts as necessary. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> <c>true</c> if okay, <c>false</c> otherwise. </returns>
        public bool VerifyFileName()
        {

            // Set file name for testing (faster than using the Text property over again.
            string fileTitle = this._FilePathNameTextBox.Text.Trim();

            // clear the Annunciator
            this._Annunciator.SetError( this._FilePathNameTextBox, "" );

            // Check if file name was given.
            if ( fileTitle is null || fileTitle.Length < 2 )
            {

                // If file is not given, alert and exit:
                this.StatusMessage = "Please select or enter a file name or use Cancel to exit this dialog box.";
                this._Annunciator.SetIconAlignment( this._FilePathNameTextBox, ErrorIconAlignment.TopRight );
                this._Annunciator.SetError( this._FilePathNameTextBox, "" );
                this._Annunciator.SetError( this._FilePathNameTextBox, this.StatusMessage );
                return false;
            }
            else
            {
                try
                {

                    // Check for wild cards in the file name.
                    if ( fileTitle.IndexOf( '*' ) + fileTitle.IndexOf( '?' ) > 0 )
                    {

                        // If wild cards in file name, alert and exit:
                        this.StatusMessage = "Please enter a complete file name!";
                        this._Annunciator.SetIconAlignment( this._FilePathNameTextBox, ErrorIconAlignment.TopLeft );
                        this._Annunciator.SetError( this._FilePathNameTextBox, "" );
                        this._Annunciator.SetError( this._FilePathNameTextBox, this.StatusMessage );
                        return false;
                    }

                    // This will create an error if the file name is bad.
                    return System.IO.File.Exists( this.SelectedFilePathName );
                }
                catch ( System.IO.IOException )
                {

                    // If error in file name, alert:
                    this.StatusMessage = "Please enter a correct file name!";
                    this._Annunciator.SetIconAlignment( this._FilePathNameTextBox, ErrorIconAlignment.TopLeft );
                    this._Annunciator.SetError( this._FilePathNameTextBox, "" );
                    this._Annunciator.SetError( this._FilePathNameTextBox, this.StatusMessage );
                    return false;
                }
            }
        }

        /// <summary> Event queue for all listeners interested in FileSelected events. </summary>
        public event EventHandler<System.IO.FileSystemEventArgs> FileSelected;

        /// <summary> Update data for the specified file name. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="filePathName"> Name of the file path. </param>
        private void UpdateData( string filePathName )
        {

            // clear the Annunciator
            this._Annunciator.SetError( this._FilePathNameTextBox, "" );
            if ( !System.IO.File.Exists( filePathName ) )
            {
                this._Annunciator.SetIconAlignment( this._FilePathNameTextBox, ErrorIconAlignment.TopLeft );
                this._Annunciator.SetError( this._FilePathNameTextBox, "" );
                this.StatusMessage = "File not found";
                this._Annunciator.SetError( this._FilePathNameTextBox, this.StatusMessage );
                return;
            }

            try
            {
                var evt = FileSelected;
                evt.Invoke( this, new System.IO.FileSystemEventArgs( System.IO.WatcherChangeTypes.Changed, this.SelectedPathName, this.SelectedFileName ) );
            }
            catch
            {
                throw;
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        /// <summary> Displays a file information described by value. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The value. </param>
        public void DisplayFileInfo( string value )
        {
            this._FileInfoTextBox.Text = value;
        }

        /// <summary> Displays a file information rich text described by value. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The value. </param>
        public void DisplayFileInfoRichText( string value )
        {
            this._FileInfoTextBox.Rtf = value;
        }

        #endregion

        #region " CONTROL EVENT HANDLERS "

        /// <summary> Handles the SelectedIndexChanged event of the Me._dataFileListBox control. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="EventArgs" /> instance containing the event data. </param>
        private void DataFileListBox_SelectedIndexChanged( object sender, EventArgs e )
        {

            // Set the file name text box.
            this._FilePathNameTextBox.Text = this._DataFileListBox.SelectedFilePath;
            this.UpdateData( this.SelectedFilePathName );
        }

        /// <summary> Updates and exits. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="EventArgs" /> instance containing the event data. </param>
        private void DataFileListBox_DoubleClick( object sender, EventArgs e )
        {

            // Just update the file and try to exit.
            this.DataFileListBox_SelectedIndexChanged( this._DataFileListBox, new EventArgs() );

            // allow other threads to execute.
            ApplianceBase.DoEvents();
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the Me._filePatternComboBox control.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="EventArgs" /> instance containing the event data. </param>
        private void FilePatternComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {

            // clear the Annunciator
            this._Annunciator.SetError( this._FilePatternComboBox, "" );
            this._Annunciator.SetError( this._FilePathNameTextBox, "" );

            // Reset the file pattern upon a click on the Pattern combo box.
            this.Pattern = this._FilePatternComboBox.Text;
        }

        /// <summary> Handles the TextChanged event of the fileTitleTextBox control. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="EventArgs" /> instance containing the event data. </param>
        private void FileTitleTextBox_TextChanged( object sender, EventArgs e )
        {

            // clear the Annunciator
            this._Annunciator.SetError( this._FilePathNameTextBox, "" );
            string newFileName = this._FilePathNameTextBox.Text;

            // Check if wild cards in file name.
            if ( newFileName.IndexOf( "*", StringComparison.OrdinalIgnoreCase ) + newFileName.IndexOf( "?", StringComparison.OrdinalIgnoreCase ) > 0 )
            {

                // If so, reset the file pattern.
                this._DataFileListBox.SearchPattern = newFileName;
            }
        }

        /// <summary>
        /// Handles the Enter event of the edit text box controls. Selects the control text.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="EventArgs" /> instance containing the event data. </param>
        private void EditTextBox_Enter( object sender, EventArgs e )
        {
            TextBox editBox = ( TextBox ) sender;
            editBox.SelectionStart = 0;
            editBox.SelectionLength = editBox.Text.Length;
        }

        /// <summary> Handles the TextChanged event of the Me._folderTextBox control. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="EventArgs" /> instance containing the event data. </param>
        private void FolderTextBox_TextChanged( object sender, EventArgs e )
        {
            if ( System.IO.Directory.Exists( this._FolderTextBox.Text ) )
            {
                this.SelectedPathName = this._FolderTextBox.Text;
            }
        }

        /// <summary> Handles the Click event of the Me._refreshButton control. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="EventArgs" /> instance containing the event data. </param>
        private void RefreshButton_Click( object sender, EventArgs e )
        {

            // clear the Annunciator
            this._Annunciator.SetError( this._FilePathNameTextBox, "" );
            this.SelectedPathName = this.SelectedPathName;
        }

        /// <summary> Gets or sets the full pathname of the parent folder file. </summary>
        /// <value> The full pathname of the parent folder file. </value>
        public string ParentFolderPathName { get; set; }

        /// <summary> Handles the Click event of the Me._selectFolderButton control. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="EventArgs" /> instance containing the event data. </param>
        private void SelectFolderButton_Click( object sender, EventArgs e )
        {

            // clear the Annunciator
            this._Annunciator.SetError( this._FilePathNameTextBox, "" );
            using var browser = new FolderBrowserDialog {
                // browser.Description = "Select folder for files to retrieve"
                RootFolder = Environment.SpecialFolder.MyComputer,
                // set the default path
                SelectedPath = System.IO.Directory.Exists( this.ParentFolderPathName ) ? this.ParentFolderPathName : this._DataFileListBox.DirectoryPath,
                ShowNewFolderButton = true
            };
            if ( browser.ShowDialog() == DialogResult.OK )
            {
                // Set the path label
                this.SelectedPathName = browser.SelectedPath;
            }
        }

        #endregion

    }
}
