using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Controls
{
    [DesignerGenerated()]
    public partial class RichTextEditControl
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(RichTextEditControl));
            _RichTextBox = new isr.Core.Controls.RichTextBox();
            _MainMenu = new System.Windows.Forms.MenuStrip();
            _FileMenu = new System.Windows.Forms.ToolStripMenuItem();
            __FilePageSetupMenu = new System.Windows.Forms.ToolStripMenuItem();
            __FilePageSetupMenu.Click += new EventHandler(FilePageSetupMenu_Click);
            __FilePrintPreviewMenu = new System.Windows.Forms.ToolStripMenuItem();
            __FilePrintPreviewMenu.Click += new EventHandler(FilePrintPreviewMenu_Click);
            __FilePrintMenu = new System.Windows.Forms.ToolStripMenuItem();
            __FilePrintMenu.Click += new EventHandler(FilePrintMenu_Click);
            _FileSeparatorMenuItem = new System.Windows.Forms.ToolStripSeparator();
            _FileExitMenu = new System.Windows.Forms.ToolStripMenuItem();
            _FormatMenu = new System.Windows.Forms.ToolStripMenuItem();
            _FormatFontStyleMenu = new System.Windows.Forms.ToolStripMenuItem();
            __FormatBoldMenu = new System.Windows.Forms.ToolStripMenuItem();
            __FormatBoldMenu.Click += new EventHandler(FormatBoldMenu_Click);
            __FormatItalicMenu = new System.Windows.Forms.ToolStripMenuItem();
            __FormatItalicMenu.Click += new EventHandler(FormatItalicMenu_Click);
            __FormatUnderlinedMenu = new System.Windows.Forms.ToolStripMenuItem();
            __FormatUnderlinedMenu.Click += new EventHandler(FormatUnderlinedMenu_Click);
            _FormatFontMenu = new System.Windows.Forms.ToolStripMenuItem();
            __FormatFontArialMenu = new System.Windows.Forms.ToolStripMenuItem();
            __FormatFontArialMenu.Click += new EventHandler(FormatFontArialMenu_Click);
            __FormatFontCourierMenu = new System.Windows.Forms.ToolStripMenuItem();
            __FormatFontCourierMenu.Click += new EventHandler(FormatFontCourierMenu_Click);
            __FormatFontLucidaMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __FormatFontLucidaMenuItem.Click += new EventHandler(FormatFontLucidaMenuItem_Click);
            __FormatFontSegoeUIMenu = new System.Windows.Forms.ToolStripMenuItem();
            __FormatFontSegoeUIMenu.Click += new EventHandler(FormatFontSegoeMenuItem_Click);
            __FormatFontTimesMenu = new System.Windows.Forms.ToolStripMenuItem();
            __FormatFontTimesMenu.Click += new EventHandler(FormatFontTimesMenu_Click);
            _FormatFontSizeMenu = new System.Windows.Forms.ToolStripMenuItem();
            __FormatFontSize8Menu = new System.Windows.Forms.ToolStripMenuItem();
            __FormatFontSize8Menu.Click += new EventHandler(FormatFontSize8Menu_Click);
            __FormatFontSize10Menu = new System.Windows.Forms.ToolStripMenuItem();
            __FormatFontSize10Menu.Click += new EventHandler(FormatFontSize10Menu_Click);
            __FormatFontSize12Menu = new System.Windows.Forms.ToolStripMenuItem();
            __FormatFontSize12Menu.Click += new EventHandler(FontSize12Menu_Click);
            __FormatFontSize18Menu = new System.Windows.Forms.ToolStripMenuItem();
            __FormatFontSize18Menu.Click += new EventHandler(FormatFontSize18Menu_Click);
            __FormatFontSize24Menu = new System.Windows.Forms.ToolStripMenuItem();
            __FormatFontSize24Menu.Click += new EventHandler(FormatFontSize24Menu_Click);
            __PrintDocument = new System.Drawing.Printing.PrintDocument();
            __PrintDocument.BeginPrint += new System.Drawing.Printing.PrintEventHandler(PrintDocument_BeginPrint);
            __PrintDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(PrintDocument_PrintPage);
            __PrintDocument.EndPrint += new System.Drawing.Printing.PrintEventHandler(PrintDocument_EndPrint);
            _PageSetupDialog = new System.Windows.Forms.PageSetupDialog();
            _PrintPreviewDialog = new System.Windows.Forms.PrintPreviewDialog();
            _PrintDialog = new System.Windows.Forms.PrintDialog();
            _MainMenu.SuspendLayout();
            SuspendLayout();
            // 
            // _RichTextBox
            // 
            _RichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            _RichTextBox.Location = new Point(0, 24);
            _RichTextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            _RichTextBox.Name = "_RichTextBox";
            _RichTextBox.Size = new Size(560, 298);
            _RichTextBox.TabIndex = 0;
            _RichTextBox.Text = string.Empty;
            // 
            // _MainMenu
            // 
            _MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { _FileMenu, _FormatMenu });
            _MainMenu.Location = new Point(0, 0);
            _MainMenu.Name = "_MainMenu";
            _MainMenu.Size = new Size(560, 24);
            _MainMenu.TabIndex = 0;
            _MainMenu.Text = "MenuStrip1";
            // 
            // _FileMenu
            // 
            _FileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { __FilePageSetupMenu, __FilePrintPreviewMenu, __FilePrintMenu, _FileSeparatorMenuItem, _FileExitMenu });
            _FileMenu.Name = "_FileMenu";
            _FileMenu.Size = new Size(37, 20);
            _FileMenu.Text = "&File";
            // 
            // _FilePageSetupMenu
            // 
            __FilePageSetupMenu.Name = "__FilePageSetupMenu";
            __FilePageSetupMenu.Size = new Size(180, 22);
            __FilePageSetupMenu.Text = "Page &Setup...";
            // 
            // _FilePrintPreviewMenu
            // 
            __FilePrintPreviewMenu.Name = "__FilePrintPreviewMenu";
            __FilePrintPreviewMenu.Size = new Size(180, 22);
            __FilePrintPreviewMenu.Text = "Print Pre&view...";
            // 
            // _FilePrintMenu
            // 
            __FilePrintMenu.Name = "__FilePrintMenu";
            __FilePrintMenu.Size = new Size(180, 22);
            __FilePrintMenu.Text = "&Print...";
            // 
            // _FileSeparatorMenuItem
            // 
            _FileSeparatorMenuItem.Name = "_FileSeparatorMenuItem";
            _FileSeparatorMenuItem.Size = new Size(177, 6);
            // 
            // _FileExitMenu
            // 
            _FileExitMenu.Name = "_FileExitMenu";
            _FileExitMenu.Size = new Size(180, 22);
            _FileExitMenu.Text = "E&xit";
            // 
            // _FormatMenu
            // 
            _FormatMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { _FormatFontStyleMenu, _FormatFontMenu, _FormatFontSizeMenu });
            _FormatMenu.Name = "_FormatMenu";
            _FormatMenu.Size = new Size(57, 20);
            _FormatMenu.Text = "F&ormat";
            // 
            // _FormatFontStyleMenu
            // 
            _FormatFontStyleMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { __FormatBoldMenu, __FormatItalicMenu, __FormatUnderlinedMenu });
            _FormatFontStyleMenu.Name = "_FormatFontStyleMenu";
            _FormatFontStyleMenu.Size = new Size(180, 22);
            _FormatFontStyleMenu.Text = "Font St&yle";
            // 
            // _FormatBoldMenu
            // 
            __FormatBoldMenu.Name = "__FormatBoldMenu";
            __FormatBoldMenu.Size = new Size(132, 22);
            __FormatBoldMenu.Text = "&Bold";
            // 
            // _FormatItalicMenu
            // 
            __FormatItalicMenu.Name = "__FormatItalicMenu";
            __FormatItalicMenu.Size = new Size(132, 22);
            __FormatItalicMenu.Text = "&Italic";
            // 
            // _FormatUnderlinedMenu
            // 
            __FormatUnderlinedMenu.Name = "__FormatUnderlinedMenu";
            __FormatUnderlinedMenu.Size = new Size(132, 22);
            __FormatUnderlinedMenu.Text = "&Underlined";
            // 
            // _FormatFontMenu
            // 
            _FormatFontMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { __FormatFontArialMenu, __FormatFontCourierMenu, __FormatFontLucidaMenuItem, __FormatFontSegoeUIMenu, __FormatFontTimesMenu });
            _FormatFontMenu.Name = "_FormatFontMenu";
            _FormatFontMenu.Size = new Size(180, 22);
            _FormatFontMenu.Text = "&Font";
            // 
            // _FormatFontArialMenu
            // 
            __FormatFontArialMenu.Name = "__FormatFontArialMenu";
            __FormatFontArialMenu.Size = new Size(174, 22);
            __FormatFontArialMenu.Text = "Arial";
            // 
            // _FormatFontCourierMenu
            // 
            __FormatFontCourierMenu.Name = "__FormatFontCourierMenu";
            __FormatFontCourierMenu.Size = new Size(174, 22);
            __FormatFontCourierMenu.Text = "Courier";
            // 
            // _FormatFontLucidaMenuItem
            // 
            __FormatFontLucidaMenuItem.Name = "__FormatFontLucidaMenuItem";
            __FormatFontLucidaMenuItem.Size = new Size(174, 22);
            __FormatFontLucidaMenuItem.Text = "Lucida Console";
            // 
            // _FormatFontSegoeUIMenu
            // 
            __FormatFontSegoeUIMenu.Name = "__FormatFontSegoeUIMenu";
            __FormatFontSegoeUIMenu.Size = new Size(174, 22);
            __FormatFontSegoeUIMenu.Text = "Segoe UI";
            // 
            // _FormatFontTimesMenu
            // 
            __FormatFontTimesMenu.Name = "__FormatFontTimesMenu";
            __FormatFontTimesMenu.Size = new Size(174, 22);
            __FormatFontTimesMenu.Text = "Times New Roman";
            // 
            // _FormatFontSizeMenu
            // 
            _FormatFontSizeMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { __FormatFontSize8Menu, __FormatFontSize10Menu, __FormatFontSize12Menu, __FormatFontSize18Menu, __FormatFontSize24Menu });
            _FormatFontSizeMenu.Name = "_FormatFontSizeMenu";
            _FormatFontSizeMenu.Size = new Size(180, 22);
            _FormatFontSizeMenu.Text = "Font &Size";
            // 
            // _FormatFontSize8Menu
            // 
            __FormatFontSize8Menu.Name = "__FormatFontSize8Menu";
            __FormatFontSize8Menu.Size = new Size(86, 22);
            __FormatFontSize8Menu.Text = "8";
            // 
            // _FormatFontSize10Menu
            // 
            __FormatFontSize10Menu.Name = "__FormatFontSize10Menu";
            __FormatFontSize10Menu.Size = new Size(86, 22);
            __FormatFontSize10Menu.Text = "10";
            // 
            // _FormatFontSize12Menu
            // 
            __FormatFontSize12Menu.Name = "__FormatFontSize12Menu";
            __FormatFontSize12Menu.Size = new Size(86, 22);
            __FormatFontSize12Menu.Text = "12";
            // 
            // _FormatFontSize18Menu
            // 
            __FormatFontSize18Menu.Name = "__FormatFontSize18Menu";
            __FormatFontSize18Menu.Size = new Size(86, 22);
            __FormatFontSize18Menu.Text = "18";
            // 
            // _FormatFontSize24Menu
            // 
            __FormatFontSize24Menu.Name = "__FormatFontSize24Menu";
            __FormatFontSize24Menu.Size = new Size(86, 22);
            __FormatFontSize24Menu.Text = "24";
            // 
            // _PrintDocument
            // 
            // 
            // _PageSetupDialog
            // 
            _PageSetupDialog.Document = __PrintDocument;
            // 
            // _PrintPreviewDialog
            // 
            _PrintPreviewDialog.AutoScrollMargin = new Size(0, 0);
            _PrintPreviewDialog.AutoScrollMinSize = new Size(0, 0);
            _PrintPreviewDialog.ClientSize = new Size(400, 300);
            _PrintPreviewDialog.Document = __PrintDocument;
            _PrintPreviewDialog.Enabled = true;
            _PrintPreviewDialog.Icon = (Icon)resources.GetObject("_PrintPreviewDialog.Icon");
            _PrintPreviewDialog.Name = "_PrintPreviewDialog";
            _PrintPreviewDialog.Visible = false;
            // 
            // _PrintDialog
            // 
            _PrintDialog.Document = __PrintDocument;
            // 
            // RichTextEditControl
            // 
            AutoScaleDimensions = new SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            Controls.Add(_RichTextBox);
            Controls.Add(_MainMenu);
            Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            Name = "RichTextEditControl";
            Size = new Size(560, 322);
            _MainMenu.ResumeLayout(false);
            _MainMenu.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        private isr.Core.Controls.RichTextBox _RichTextBox;

        private System.Drawing.Printing.PrintDocument __PrintDocument;

        private System.Drawing.Printing.PrintDocument _PrintDocument
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __PrintDocument;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__PrintDocument != null)
                {
                    __PrintDocument.BeginPrint -= PrintDocument_BeginPrint;
                    __PrintDocument.PrintPage -= PrintDocument_PrintPage;
                    __PrintDocument.EndPrint -= PrintDocument_EndPrint;
                }

                __PrintDocument = value;
                if (__PrintDocument != null)
                {
                    __PrintDocument.BeginPrint += PrintDocument_BeginPrint;
                    __PrintDocument.PrintPage += PrintDocument_PrintPage;
                    __PrintDocument.EndPrint += PrintDocument_EndPrint;
                }
            }
        }

        private System.Windows.Forms.PageSetupDialog _PageSetupDialog;
        private System.Windows.Forms.PrintPreviewDialog _PrintPreviewDialog;
        private System.Windows.Forms.PrintDialog _PrintDialog;
        private System.Windows.Forms.MenuStrip _MainMenu;
        private System.Windows.Forms.ToolStripMenuItem _FileMenu;
        private System.Windows.Forms.ToolStripMenuItem __FilePageSetupMenu;

        private System.Windows.Forms.ToolStripMenuItem _FilePageSetupMenu
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __FilePageSetupMenu;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__FilePageSetupMenu != null)
                {
                    __FilePageSetupMenu.Click -= FilePageSetupMenu_Click;
                }

                __FilePageSetupMenu = value;
                if (__FilePageSetupMenu != null)
                {
                    __FilePageSetupMenu.Click += FilePageSetupMenu_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __FilePrintPreviewMenu;

        private System.Windows.Forms.ToolStripMenuItem _FilePrintPreviewMenu
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __FilePrintPreviewMenu;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__FilePrintPreviewMenu != null)
                {
                    __FilePrintPreviewMenu.Click -= FilePrintPreviewMenu_Click;
                }

                __FilePrintPreviewMenu = value;
                if (__FilePrintPreviewMenu != null)
                {
                    __FilePrintPreviewMenu.Click += FilePrintPreviewMenu_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __FilePrintMenu;

        private System.Windows.Forms.ToolStripMenuItem _FilePrintMenu
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __FilePrintMenu;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__FilePrintMenu != null)
                {
                    __FilePrintMenu.Click -= FilePrintMenu_Click;
                }

                __FilePrintMenu = value;
                if (__FilePrintMenu != null)
                {
                    __FilePrintMenu.Click += FilePrintMenu_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripSeparator _FileSeparatorMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _FileExitMenu;
        private System.Windows.Forms.ToolStripMenuItem _FormatMenu;
        private System.Windows.Forms.ToolStripMenuItem _FormatFontStyleMenu;
        private System.Windows.Forms.ToolStripMenuItem __FormatBoldMenu;

        private System.Windows.Forms.ToolStripMenuItem _FormatBoldMenu
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __FormatBoldMenu;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__FormatBoldMenu != null)
                {
                    __FormatBoldMenu.Click -= FormatBoldMenu_Click;
                }

                __FormatBoldMenu = value;
                if (__FormatBoldMenu != null)
                {
                    __FormatBoldMenu.Click += FormatBoldMenu_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __FormatItalicMenu;

        private System.Windows.Forms.ToolStripMenuItem _FormatItalicMenu
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __FormatItalicMenu;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__FormatItalicMenu != null)
                {
                    __FormatItalicMenu.Click -= FormatItalicMenu_Click;
                }

                __FormatItalicMenu = value;
                if (__FormatItalicMenu != null)
                {
                    __FormatItalicMenu.Click += FormatItalicMenu_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __FormatUnderlinedMenu;

        private System.Windows.Forms.ToolStripMenuItem _FormatUnderlinedMenu
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __FormatUnderlinedMenu;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__FormatUnderlinedMenu != null)
                {
                    __FormatUnderlinedMenu.Click -= FormatUnderlinedMenu_Click;
                }

                __FormatUnderlinedMenu = value;
                if (__FormatUnderlinedMenu != null)
                {
                    __FormatUnderlinedMenu.Click += FormatUnderlinedMenu_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem _FormatFontSizeMenu;
        private System.Windows.Forms.ToolStripMenuItem __FormatFontSize8Menu;

        private System.Windows.Forms.ToolStripMenuItem _FormatFontSize8Menu
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __FormatFontSize8Menu;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__FormatFontSize8Menu != null)
                {
                    __FormatFontSize8Menu.Click -= FormatFontSize8Menu_Click;
                }

                __FormatFontSize8Menu = value;
                if (__FormatFontSize8Menu != null)
                {
                    __FormatFontSize8Menu.Click += FormatFontSize8Menu_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __FormatFontSize10Menu;

        private System.Windows.Forms.ToolStripMenuItem _FormatFontSize10Menu
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __FormatFontSize10Menu;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__FormatFontSize10Menu != null)
                {
                    __FormatFontSize10Menu.Click -= FormatFontSize10Menu_Click;
                }

                __FormatFontSize10Menu = value;
                if (__FormatFontSize10Menu != null)
                {
                    __FormatFontSize10Menu.Click += FormatFontSize10Menu_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __FormatFontSize12Menu;

        private System.Windows.Forms.ToolStripMenuItem _FormatFontSize12Menu
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __FormatFontSize12Menu;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__FormatFontSize12Menu != null)
                {
                    __FormatFontSize12Menu.Click -= FontSize12Menu_Click;
                }

                __FormatFontSize12Menu = value;
                if (__FormatFontSize12Menu != null)
                {
                    __FormatFontSize12Menu.Click += FontSize12Menu_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __FormatFontSize18Menu;

        private System.Windows.Forms.ToolStripMenuItem _FormatFontSize18Menu
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __FormatFontSize18Menu;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__FormatFontSize18Menu != null)
                {
                    __FormatFontSize18Menu.Click -= FormatFontSize18Menu_Click;
                }

                __FormatFontSize18Menu = value;
                if (__FormatFontSize18Menu != null)
                {
                    __FormatFontSize18Menu.Click += FormatFontSize18Menu_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __FormatFontSize24Menu;

        private System.Windows.Forms.ToolStripMenuItem _FormatFontSize24Menu
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __FormatFontSize24Menu;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__FormatFontSize24Menu != null)
                {
                    __FormatFontSize24Menu.Click -= FormatFontSize24Menu_Click;
                }

                __FormatFontSize24Menu = value;
                if (__FormatFontSize24Menu != null)
                {
                    __FormatFontSize24Menu.Click += FormatFontSize24Menu_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem _FormatFontMenu;
        private System.Windows.Forms.ToolStripMenuItem __FormatFontArialMenu;

        private System.Windows.Forms.ToolStripMenuItem _FormatFontArialMenu
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __FormatFontArialMenu;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__FormatFontArialMenu != null)
                {
                    __FormatFontArialMenu.Click -= FormatFontArialMenu_Click;
                }

                __FormatFontArialMenu = value;
                if (__FormatFontArialMenu != null)
                {
                    __FormatFontArialMenu.Click += FormatFontArialMenu_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __FormatFontCourierMenu;

        private System.Windows.Forms.ToolStripMenuItem _FormatFontCourierMenu
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __FormatFontCourierMenu;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__FormatFontCourierMenu != null)
                {
                    __FormatFontCourierMenu.Click -= FormatFontCourierMenu_Click;
                }

                __FormatFontCourierMenu = value;
                if (__FormatFontCourierMenu != null)
                {
                    __FormatFontCourierMenu.Click += FormatFontCourierMenu_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __FormatFontLucidaMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _FormatFontLucidaMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __FormatFontLucidaMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__FormatFontLucidaMenuItem != null)
                {
                    __FormatFontLucidaMenuItem.Click -= FormatFontLucidaMenuItem_Click;
                }

                __FormatFontLucidaMenuItem = value;
                if (__FormatFontLucidaMenuItem != null)
                {
                    __FormatFontLucidaMenuItem.Click += FormatFontLucidaMenuItem_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __FormatFontSegoeUIMenu;

        private System.Windows.Forms.ToolStripMenuItem _FormatFontSegoeUIMenu
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __FormatFontSegoeUIMenu;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__FormatFontSegoeUIMenu != null)
                {
                    __FormatFontSegoeUIMenu.Click -= FormatFontSegoeMenuItem_Click;
                }

                __FormatFontSegoeUIMenu = value;
                if (__FormatFontSegoeUIMenu != null)
                {
                    __FormatFontSegoeUIMenu.Click += FormatFontSegoeMenuItem_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __FormatFontTimesMenu;

        private System.Windows.Forms.ToolStripMenuItem _FormatFontTimesMenu
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __FormatFontTimesMenu;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__FormatFontTimesMenu != null)
                {
                    __FormatFontTimesMenu.Click -= FormatFontTimesMenu_Click;
                }

                __FormatFontTimesMenu = value;
                if (__FormatFontTimesMenu != null)
                {
                    __FormatFontTimesMenu.Click += FormatFontTimesMenu_Click;
                }
            }
        }
    }
}
