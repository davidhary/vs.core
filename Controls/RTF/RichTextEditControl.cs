using System;
using System.Drawing;
using System.Windows.Forms;

using isr.Core.Forma;

namespace isr.Core.Controls
{

    /// <summary>
    /// Control for viewing and printing a <see cref="RichTextBox">rich text box</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-01-08 </para>
    /// </remarks>
    public partial class RichTextEditControl : ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP  "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public RichTextEditControl() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;

            // Add any initialization after the InitializeComponent() call
            this._FormatFontSegoeUIMenu.Text = SystemFonts.MessageBoxFont.Name;
            this.__FilePageSetupMenu.Name = "_FilePageSetupMenu";
            this.__FilePrintPreviewMenu.Name = "_FilePrintPreviewMenu";
            this.__FilePrintMenu.Name = "_FilePrintMenu";
            this.__FormatBoldMenu.Name = "_FormatBoldMenu";
            this.__FormatItalicMenu.Name = "_FormatItalicMenu";
            this.__FormatUnderlinedMenu.Name = "_FormatUnderlinedMenu";
            this.__FormatFontArialMenu.Name = "_FormatFontArialMenu";
            this.__FormatFontCourierMenu.Name = "_FormatFontCourierMenu";
            this.__FormatFontLucidaMenuItem.Name = "_FormatFontLucidaMenuItem";
            this.__FormatFontSegoeUIMenu.Name = "_FormatFontSegoeUIMenu";
            this.__FormatFontTimesMenu.Name = "_FormatFontTimesMenu";
            this.__FormatFontSize8Menu.Name = "_FormatFontSize8Menu";
            this.__FormatFontSize10Menu.Name = "_FormatFontSize10Menu";
            this.__FormatFontSize12Menu.Name = "_FormatFontSize12Menu";
            this.__FormatFontSize18Menu.Name = "_FormatFontSize18Menu";
            this.__FormatFontSize24Menu.Name = "_FormatFontSize24Menu";
        }

        /// <summary>
        /// Disposes of the resources (other than memory) used by the
        /// <see cref="T:System.Windows.Forms.Form" />.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        /// <summary> Creates a form. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="caption">        The caption. </param>
        /// <param name="tabTitle">       The tab title. </param>
        /// <param name="richTextEditor"> The rich text editor. </param>
        /// <param name="logListener">    The log listener. </param>
        /// <returns> The new form. </returns>
        public static ConsoleForm CreateForm( string caption, string tabTitle, ModelViewTalkerBase richTextEditor, IMessageListener logListener )
        {
            ConsoleForm result = null;
            try
            {
                result = new ConsoleForm() { Text = caption };
                result.AddTalkerControl( tabTitle, richTextEditor, true );
                result.AddListener( logListener );
            }
            catch
            {
                if ( result is object )
                {
                    result.Dispose();
                }

                throw;
            }

            return result;
        }

        #endregion

        #region " PRINT SETTINGS "

        /// <summary> Gets a reference to the print document. </summary>
        /// <value> The print document. </value>
        public System.Drawing.Printing.PrintDocument PrintDocument => this._PrintDocument;

        /// <summary> Gets the reference to the rich text box. </summary>
        /// <value> The rich text box. </value>
        public RichTextBox RichTextBox => this._RichTextBox;

        #endregion

        #region " PRINT DIALOG "

        /// <summary> The first character on page. </summary>
        private int _FirstCharOnPage;

        /// <summary> Print document begin print. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Print event information. </param>
        private void PrintDocument_BeginPrint( object sender, System.Drawing.Printing.PrintEventArgs e )
        {
            // Start at the beginning of the text
            this._FirstCharOnPage = 0;
        }

        /// <summary> Print document print page. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Print page event information. </param>
        private void PrintDocument_PrintPage( object sender, System.Drawing.Printing.PrintPageEventArgs e )
        {
            // To print the boundaries of the current page margins
            // uncomment the next line:
            e.Graphics.DrawRectangle( Pens.Blue, e.MarginBounds );

            // make the RichTextBoxEx calculate and render as much text as will
            // fit on the page and remember the last character printed for the
            // beginning of the next page
            this._FirstCharOnPage = this.RichTextBox.FormatRange( false, e, this._FirstCharOnPage, this.RichTextBox.TextLength );

            // check if there are more pages to print
            e.HasMorePages = this._FirstCharOnPage < this.RichTextBox.TextLength;
        }

        /// <summary> Print document end print. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Print event information. </param>
        private void PrintDocument_EndPrint( object sender, System.Drawing.Printing.PrintEventArgs e )
        {
            // Clean up cached information
            _ = this.RichTextBox.FormatRangeDone();
        }

        #endregion

        #region " FILE MENU "

        /// <summary> File page setup menu click. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void FilePageSetupMenu_Click( object sender, EventArgs e )
        {
            _ = this._PageSetupDialog.ShowDialog();
        }

        /// <summary> File print preview menu click. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void FilePrintPreviewMenu_Click( object sender, EventArgs e )
        {
            if ( this._PrintPreviewDialog.ShowDialog() == DialogResult.OK )
            {
                this._PrintDocument.Print();
            }
        }

        /// <summary> File print menu click. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void FilePrintMenu_Click( object sender, EventArgs e )
        {
            this._PrintDocument.Print();
        }

        #endregion

        #region " FORMAT MENU "

        /// <summary> Format bold menu click. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void FormatBoldMenu_Click( object sender, EventArgs e )
        {
            _ = this.RichTextBox.SetSelectionBold( true );
        }

        /// <summary> Format italic menu click. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void FormatItalicMenu_Click( object sender, EventArgs e )
        {
            _ = this.RichTextBox.SetSelectionItalic( true );
        }

        /// <summary> Format underlined menu click. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void FormatUnderlinedMenu_Click( object sender, EventArgs e )
        {
            _ = this.RichTextBox.SetSelectionUnderlined( true );
        }

        /// <summary> Format font size 8 menu click. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void FormatFontSize8Menu_Click( object sender, EventArgs e )
        {
            _ = this.RichTextBox.SetSelectionSize( 8 );
        }

        /// <summary> Format font size 10 menu click. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void FormatFontSize10Menu_Click( object sender, EventArgs e )
        {
            _ = this.RichTextBox.SetSelectionSize( 10 );
        }

        /// <summary> Font size 12 menu click. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void FontSize12Menu_Click( object sender, EventArgs e )
        {
            _ = this.RichTextBox.SetSelectionSize( 12 );
        }

        /// <summary> Format font size 18 menu click. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void FormatFontSize18Menu_Click( object sender, EventArgs e )
        {
            _ = this.RichTextBox.SetSelectionSize( 18 );
        }

        /// <summary> Format font size 24 menu click. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void FormatFontSize24Menu_Click( object sender, EventArgs e )
        {
            _ = this.RichTextBox.SetSelectionSize( 24 );
        }

        /// <summary> Format font arial menu click. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void FormatFontArialMenu_Click( object sender, EventArgs e )
        {
            _ = this.RichTextBox.SetSelectionFont( "Arial" );
        }

        /// <summary> Format font courier menu click. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void FormatFontCourierMenu_Click( object sender, EventArgs e )
        {
            _ = this.RichTextBox.SetSelectionFont( "Courier New" );
        }

        /// <summary> Format font lucida menu item click. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void FormatFontLucidaMenuItem_Click( object sender, EventArgs e )
        {
            _ = this.RichTextBox.SetSelectionFont( "Lucida Console" );
        }

        /// <summary> Format font segoe menu item click. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void FormatFontSegoeMenuItem_Click( object sender, EventArgs e )
        {
            _ = this.RichTextBox.SetSelectionFont( SystemFonts.MessageBoxFont.Name );
        }

        /// <summary> Format font times menu click. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void FormatFontTimesMenu_Click( object sender, EventArgs e )
        {
            _ = this.RichTextBox.SetSelectionFont( "Times New Roman" );
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary> Adds the listeners such as the top level trace messages box and log. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="listener"> The listener. </param>
        public override void AddListener( IMessageListener listener )
        {
            base.AddListener( listener );
        }

        #endregion

    }
}
