using System;
using System.ComponentModel;

namespace isr.Core.Controls
{

    /// <summary> Check box with read only capability. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2013-09-27 </para>
    /// </remarks>
    [DesignerCategory( "code" )]
    [Description( "Extended Check Box" )]
    public class CheckBox : System.Windows.Forms.CheckBox
    {

        /// <summary> Gets or sets the read only property. </summary>
        /// <value> The read only. </value>
        [DefaultValue( false )]
        [Category( "Behavior" )]
        [Description( "Indicates whether the check box is read only." )]
        public bool ReadOnly { get; set; }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.Click" /> event. Ignored if read only.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnClick( EventArgs e )
        {
            if ( !this.ReadOnly )
            {
                base.OnClick( e );
            }
        }
    }
}
