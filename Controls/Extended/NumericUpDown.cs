using System.Collections.Generic;
using System.ComponentModel;

namespace isr.Core.Controls
{

    /// <summary>
    /// Extends the standard <see cref="System.Windows.Forms.NumericUpDown">numeric up
    /// down</see>control.
    /// </summary>
    /// <remarks>
    /// (c) 2013 Claudio NiCora HTTP://CoolSoft.AlterVista.org.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2013-09-27. Created from http://www.CodeProject.com/KB/edit/NumericUpDownEx.aspx.
    /// </para>
    /// </remarks>
    [DesignerCategory( "code" )]
    [Description( "Numeric Up Down" )]
    public class NumericUpDown : NumericUpDownBase
    {

        /// <summary> object creator. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public NumericUpDown() : base()
        {
        }

        /// <summary> Embedded resource names. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> The resource names. </returns>
        public static IEnumerable<string> EmbeddedResourceNames()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceNames();
        }
    }
}
