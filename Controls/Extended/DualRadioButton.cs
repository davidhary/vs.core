using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

using isr.Core.Controls.ExceptionExtensions;

namespace isr.Core.Controls
{

    /// <summary>
    /// A dual radio button control functioning as a check box control displaying both its states.
    /// </summary>
    /// <remarks>
    /// (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2010-12-09, 1.02.3995 </para>
    /// </remarks>
    [Description( "Dual Radio Button" )]
    [DefaultEvent( "CheckToggled" )]
    [DefaultBindingProperty( "Checked" )]
    public partial class DualRadioButton : Forma.ModelViewBase
    {

        #region " CONSTRUCTION "

        /// <summary> Clears internal properties. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public DualRadioButton() : base()
        {
            this.InitializeComponent();
            this._PrimaryButtonHorizontalLocation = HorizontalLocation.Left;
            this._PrimaryButtonVerticalLocation = VerticalLocation.Center;
            this._SecondaryButtonHorizontalLocation = HorizontalLocation.Right;
            this._SecondaryButtonVerticalLocation = VerticalLocation.Center;
            this._PrimaryRadioButton.ReadOnly = false;
            this._SecondaryRadioButton.ReadOnly = false;
            this._Checked = false;
            this._PrimaryRadioButton.Checked = false;
            this._SecondaryRadioButton.Checked = false;
            this.ArrangeButtons();
            this.__PrimaryRadioButton.Name = "_PrimaryRadioButton";
            this.__SecondaryRadioButton.Name = "_SecondaryRadioButton";
        }

        /// <summary>
        /// Releases the unmanaged resources used by the isr.Core.Forma.ModelViewBase and optionally
        /// releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                        this.components = null;
                    }

                    this.RemoveCheckToggledEventHandler( CheckToggled );
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " BEHAVIOR "

        /// <summary> Gets or sets the read only property. </summary>
        /// <value> The read only. </value>
        [DefaultValue( false )]
        [Category( "Behavior" )]
        [Description( "Indicates whether the dual radio button is read only." )]
        public bool ReadOnly
        {
            get => this._PrimaryRadioButton.ReadOnly;

            set {
                if ( this.ReadOnly != value )
                {
                    this._PrimaryRadioButton.ReadOnly = value;
                    this._SecondaryRadioButton.ReadOnly = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> <c>True</c> if checked. </summary>
        private bool _Checked;

        /// <summary> Gets or sets the checked property. </summary>
        /// <value> The checked. </value>
        [DefaultValue( false )]
        [Category( "Behavior" )]
        [Description( "Indicates whether the radio button is checked or not." )]
        public bool Checked
        {
            get => this._Checked;

            set {
                if ( this.Checked != value )
                {
                    this._Checked = value;
                    this._PrimaryRadioButton.Checked = value;
                    this._SecondaryRadioButton.Checked = !value;
                    this.NotifyPropertyChanged();
                    this.OnCheckToggled();
                }
            }
        }

        #endregion

        #region " APPEARANCE "

        /// <summary> The checked color. </summary>
        private Color _CheckedColor;

        /// <summary> Checked background color. </summary>
        /// <value> The color of the checked. </value>
        [Category( "Appearance" )]
        [Description( "Checked background color" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( typeof( Color ), "LightGreen" )]
        public Color CheckedColor
        {
            get => this._CheckedColor;

            set {
                if ( !Equals( this.CheckedColor, value ) )
                {
                    this._CheckedColor = value;
                    this.NotifyPropertyChanged();
                    this.RadioButtonCheckedChanged( this._PrimaryRadioButton );
                    this.RadioButtonCheckedChanged( this._SecondaryRadioButton );
                }
            }
        }

        /// <summary> The unchecked color. </summary>
        private Color _UncheckedColor;

        /// <summary> Unchecked background color. </summary>
        /// <value> The color of the unchecked. </value>
        [Category( "Appearance" )]
        [Description( "Unchecked background color" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( typeof( Color ), "DarkSeaGreen" )]
        public Color UncheckedColor
        {
            get => this._UncheckedColor;

            set {
                if ( !Equals( this.UncheckedColor, value ) )
                {
                    this._UncheckedColor = value;
                    this.NotifyPropertyChanged();
                    this.RadioButtonCheckedChanged( this._PrimaryRadioButton );
                    this.RadioButtonCheckedChanged( this._SecondaryRadioButton );
                }
            }
        }

        #endregion

        #region " PRIMARY BUTTON "

        /// <summary> Primary Button appearance. </summary>
        /// <value> The Primary Button appearance. </value>
        [Category( "Appearance" )]
        [Description( "Primary Button appearance" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( typeof( Appearance ), "Button" )]
        public Appearance PrimaryButtonAppearance
        {
            get => this._PrimaryRadioButton.Appearance;

            set {
                if ( this.PrimaryButtonAppearance != value )
                {
                    this._PrimaryRadioButton.Appearance = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Primary Button font. </summary>
        /// <value> The Primary Button font. </value>
        [Category( "Appearance" )]
        [Description( "Primary Button font" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        public Font PrimaryButtonFont
        {
            get => this._PrimaryRadioButton.Font;

            set {
                if ( !Equals( this.PrimaryButtonFont, value ) )
                {
                    this._PrimaryRadioButton.Font = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The primary button horizontal location. </summary>
        private HorizontalLocation _PrimaryButtonHorizontalLocation;

        /// <summary> Gets or sets the primary button horizontal location. </summary>
        /// <value> The primary button horizontal location. </value>
        [DefaultValue( typeof( HorizontalLocation ), "Left" )]
        [Category( "Behavior" )]
        [Description( "Horizontal location of the primary button." )]
        public HorizontalLocation PrimaryButtonHorizontalLocation
        {
            get => this._PrimaryButtonHorizontalLocation;

            set {
                if ( value != this.PrimaryButtonHorizontalLocation )
                {
                    this._PrimaryButtonHorizontalLocation = value;
                    this.NotifyPropertyChanged();
                    this.ArrangeButtons();
                }
            }
        }

        /// <summary> The primary button vertical location. </summary>
        private VerticalLocation _PrimaryButtonVerticalLocation;

        /// <summary> Gets or sets the primary button vertical location. </summary>
        /// <value> The primary button vertical location. </value>
        [DefaultValue( typeof( VerticalLocation ), "Center" )]
        [Category( "Behavior" )]
        [Description( "Vertical location of the primary button." )]
        public VerticalLocation PrimaryButtonVerticalLocation
        {
            get => this._PrimaryButtonVerticalLocation;

            set {
                if ( value != this.PrimaryButtonVerticalLocation )
                {
                    this._PrimaryButtonVerticalLocation = value;
                    this.NotifyPropertyChanged();
                    this.ArrangeButtons();
                }
            }
        }

        /// <summary> Primary Button Text. </summary>
        /// <value> The Primary Button text. </value>
        [Category( "Appearance" )]
        [Description( "Primary Button Text" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( "O&N" )]
        public string PrimaryButtonText
        {
            get => this._PrimaryRadioButton.Text;

            set {
                if ( !string.Equals( this.PrimaryButtonText, value ) )
                {
                    this._PrimaryRadioButton.Text = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the Primary Button toolTip. </summary>
        /// <value> The Primary Button toolTip. </value>
        [Category( "Appearance" )]
        [Description( "Primary Button text alignment" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( typeof( ContentAlignment ), "MiddleCenter" )]
        public ContentAlignment PrimaryButtonTextAlign
        {
            get => this._PrimaryRadioButton.TextAlign;

            set {
                if ( this.PrimaryButtonTextAlign != value )
                {
                    this._PrimaryRadioButton.TextAlign = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The primary button tool tip. </summary>
        private string _PrimaryButtonToolTip;

        /// <summary> Gets or sets the Primary Button toolTip. </summary>
        /// <value> The Primary Button toolTip. </value>
        [Category( "Appearance" )]
        [Description( "Primary Button Tool Tip" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( "Press to turn on" )]
        public string PrimaryButtonToolTip
        {
            get => this._PrimaryButtonToolTip;

            set {
                if ( !string.Equals( this.PrimaryButtonToolTip, value ) )
                {
                    this._PrimaryButtonToolTip = value;
                    this.ToolTip.SetToolTip( this._PrimaryRadioButton, value );
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " SECONDARY BUTTON "

        /// <summary> Secondary Button appearance. </summary>
        /// <value> The Secondary Button appearance. </value>
        [Category( "Appearance" )]
        [Description( "Secondary Button appearance" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( typeof( Appearance ), "Button" )]
        public Appearance SecondaryButtonAppearance
        {
            get => this._SecondaryRadioButton.Appearance;

            set {
                if ( this.SecondaryButtonAppearance != value )
                {
                    this._SecondaryRadioButton.Appearance = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Secondary Button font. </summary>
        /// <value> The Secondary Button font. </value>
        [Category( "Appearance" )]
        [Description( "Secondary Button font" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        public Font SecondaryButtonFont
        {
            get => this._SecondaryRadioButton.Font;

            set {
                if ( !Equals( this.SecondaryButtonFont, value ) )
                {
                    this._SecondaryRadioButton.Font = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The secondary button horizontal location. </summary>
        private HorizontalLocation _SecondaryButtonHorizontalLocation;

        /// <summary> Gets or sets the secondary button horizontal location. </summary>
        /// <value> The secondary button horizontal location. </value>
        [DefaultValue( typeof( HorizontalLocation ), "Right" )]
        [Category( "Behavior" )]
        [Description( "Horizontal location of the Secondary button." )]
        public HorizontalLocation SecondaryButtonHorizontalLocation
        {
            get => this._SecondaryButtonHorizontalLocation;

            set {
                if ( value != this.SecondaryButtonHorizontalLocation )
                {
                    this._SecondaryButtonHorizontalLocation = value;
                    this.NotifyPropertyChanged();
                    this.ArrangeButtons();
                }
            }
        }

        /// <summary> Gets or sets the text associated with the secondary button. </summary>
        /// <value> The secondary button text. </value>
        [Category( "Appearance" )]
        [Description( "The text of the Secondary button" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( "O&FF" )]
        public string SecondaryButtonText
        {
            get => this._SecondaryRadioButton.Text;

            set {
                if ( !string.Equals( this.SecondaryButtonText, value ) )
                {
                    this._SecondaryRadioButton.Text = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The secondary button vertical location. </summary>
        private VerticalLocation _SecondaryButtonVerticalLocation;

        /// <summary> Gets or sets the secondary button vertical location. </summary>
        /// <value> The secondary button vertical location. </value>
        [DefaultValue( typeof( VerticalLocation ), "Center" )]
        [Category( "Behavior" )]
        [Description( "Vertical location of the Secondary button." )]
        public VerticalLocation SecondaryButtonVerticalLocation
        {
            get => this._SecondaryButtonVerticalLocation;

            set {
                if ( value != this.SecondaryButtonVerticalLocation )
                {
                    this._SecondaryButtonVerticalLocation = value;
                    this.NotifyPropertyChanged();
                    this.ArrangeButtons();
                }
            }
        }

        /// <summary> Gets or sets the Secondary Button toolTip. </summary>
        /// <value> The Secondary Button toolTip. </value>
        [Category( "Appearance" )]
        [Description( "Secondary Button text alignment" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( typeof( ContentAlignment ), "MiddleCenter" )]
        public ContentAlignment SecondaryButtonTextAlign
        {
            get => this._SecondaryRadioButton.TextAlign;

            set {
                if ( this.SecondaryButtonTextAlign != value )
                {
                    this._SecondaryRadioButton.TextAlign = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The secondary button tool tip. </summary>
        private string _SecondaryButtonToolTip;

        /// <summary> Gets or sets the Secondary Button toolTip. </summary>
        /// <value> The Secondary Button toolTip. </value>
        [Category( "Appearance" )]
        [Description( "Secondary Button Tool Tip" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( "Press to turn off" )]
        public string SecondaryButtonToolTip
        {
            get => this._SecondaryButtonToolTip;

            set {
                if ( !string.Equals( this.SecondaryButtonToolTip, value ) )
                {
                    this._SecondaryButtonToolTip = value;
                    this.ToolTip.SetToolTip( this._PrimaryRadioButton, value );
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " EVENT HANDLERS "

        /// <summary> Handles the control checked changed described by control. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="control"> The control. </param>
        private void RadioButtonCheckedChanged( System.Windows.Forms.RadioButton control )
        {
            control.BackColor = control.Checked ? this.CheckedColor : this.UncheckedColor;
        }

        /// <summary> Handles the primary radio button checked changed described by sender. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        private void PrimaryRadioButtonCheckedChanged( object sender )
        {
            if ( sender is RadioButton control )
            {
                this.Checked = control.Checked;
                this._SecondaryRadioButton.Checked = !control.Checked;
                this.RadioButtonCheckedChanged( control );
            }
        }

        /// <summary> Handles the secondary radio button checked changed described by sender. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        private void SecondaryRadioButtonCheckedChanged( object sender )
        {
            if ( sender is RadioButton control )
            {
                this.Checked = !control.Checked;
                control.BackColor = control.Checked ? this.CheckedColor : this.UncheckedColor;
                this.RadioButtonCheckedChanged( control );
            }
        }

        /// <summary> Raises the radio button checked changed event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void PrimaryRadioButtonCheckedChanged( object sender, EventArgs e )
        {
            this.PrimaryRadioButtonCheckedChanged( sender );
        }

        /// <summary> Off radio button checked changed. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void SecondaryRadioButtonCheckedChanged( object sender, EventArgs e )
        {
            this.SecondaryRadioButtonCheckedChanged( sender );
        }

        /// <summary> Gets the row and column for the button withing the control layout. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="verticalLocation">   The vertical location. </param>
        /// <param name="horizontalLocation"> The horizontal location. </param>
        /// <returns> A (Column As Integer, Row As Integer) </returns>
        private static (int Column, int Row) LocateButton( VerticalLocation verticalLocation, HorizontalLocation horizontalLocation )
        {
            int column = 1;
            int row = 1;
            switch ( verticalLocation )
            {
                case VerticalLocation.Bottom:
                    {
                        row = 2;
                        break;
                    }

                case VerticalLocation.Center:
                    {
                        row = 1;
                        break;
                    }

                case VerticalLocation.Top:
                    {
                        row = 0;
                        break;
                    }
            }

            switch ( horizontalLocation )
            {
                case HorizontalLocation.Center:
                    {
                        column = 1;
                        break;
                    }

                case HorizontalLocation.Left:
                    {
                        column = 0;
                        break;
                    }

                case HorizontalLocation.Right:
                    {
                        column = 2;
                        break;
                    }
            }

            return (column, row);
        }

        /// <summary> Arrange buttons within the control layout. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void ArrangeButtons()
        {
            var (Column, Row) = LocateButton( this.PrimaryButtonVerticalLocation, this.PrimaryButtonHorizontalLocation );
            var secondaryLocation = LocateButton( this.SecondaryButtonVerticalLocation, this.SecondaryButtonHorizontalLocation );
            float hundredPercent = 100.0f;

            // clear the styles
            foreach ( ColumnStyle colStyle in this._Layout.ColumnStyles )
            {
                colStyle.SizeType = SizeType.AutoSize;
            }

            foreach ( RowStyle rowStyle in this._Layout.RowStyles )
            {
                rowStyle.SizeType = SizeType.AutoSize;
            }

            this._Layout.RowStyles[Row].SizeType = SizeType.Percent;
            this._Layout.RowStyles[secondaryLocation.Row].SizeType = SizeType.Percent;
            this._Layout.ColumnStyles[Column].SizeType = SizeType.Percent;
            this._Layout.ColumnStyles[secondaryLocation.Column].SizeType = SizeType.Percent;
            int percentCount = 0;
            foreach ( RowStyle rowStyle in this._Layout.RowStyles )
            {
                if ( rowStyle.SizeType == SizeType.Percent )
                {
                    percentCount += 1;
                }
            }

            foreach ( RowStyle rowStyle in this._Layout.RowStyles )
            {
                if ( rowStyle.SizeType == SizeType.Percent )
                {
                    rowStyle.Height = hundredPercent / percentCount;
                }
            }

            percentCount = 0;
            foreach ( ColumnStyle columnStyle in this._Layout.ColumnStyles )
            {
                if ( columnStyle.SizeType == SizeType.Percent )
                {
                    percentCount += 1;
                }
            }

            foreach ( ColumnStyle columnStyle in this._Layout.ColumnStyles )
            {
                if ( columnStyle.SizeType == SizeType.Percent )
                {
                    columnStyle.Width = hundredPercent / percentCount;
                }
            }

            this._Layout.Controls.Clear();
            this._PrimaryRadioButton.Dock = DockStyle.None;
            this._SecondaryRadioButton.Dock = DockStyle.None;
            this._Layout.Controls.Add( this._PrimaryRadioButton, Column, Row );
            this._Layout.Controls.Add( this._SecondaryRadioButton, secondaryLocation.Column, secondaryLocation.Row );
            this._PrimaryRadioButton.Dock = DockStyle.Fill;
            this._SecondaryRadioButton.Dock = DockStyle.Fill;
        }

        #endregion

        #region " EVENTS "

        /// <summary>Occurs when the checked value changed. </summary>
        public event EventHandler<EventArgs> CheckToggled;

        /// <summary> Removes event handler. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The handler. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void RemoveCheckToggledEventHandler( EventHandler<EventArgs> value )
        {
            foreach ( Delegate d in value is null ? (Array.Empty<Delegate>()) : value.GetInvocationList() )
            {
                try
                {
                    CheckToggled -= ( EventHandler<EventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }
        }

        /// <summary> Raises the Check changed event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        protected virtual void OnCheckToggled()
        {
            var evt = CheckToggled;
            evt?.Invoke( this, EventArgs.Empty );
        }

        #endregion

    }

    /// <summary> Values that represent HorizontalLocation. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public enum HorizontalLocation
    {
        /// <summary>Button located on the left side of the control (default behavior)</summary>
        Left,
        /// <summary>Button located in the center of the control</summary>
        Center,
        /// <summary>Button located at the right side of the control</summary>
        Right
    }

    /// <summary> Values that represent VerticalLocation. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public enum VerticalLocation
    {
        /// <summary>Button located on the top side of the control (default behavior)</summary>
        Top,
        /// <summary>Button located in the center of the control</summary>
        Center,
        /// <summary>Button located at the bottom of the control</summary>
        Bottom
    }
}
