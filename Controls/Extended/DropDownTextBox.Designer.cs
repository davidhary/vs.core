using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace isr.Core.Controls
{
    [Microsoft.VisualBasic.CompilerServices.DesignerGenerated()]
    public partial class DropDownTextBox
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            __DropDownToggle = new System.Windows.Forms.CheckBox();
            __DropDownToggle.CheckedChanged += new EventHandler(DropDownToggle_CheckedChanged);
            _TextBox = new TextBox();
            SuspendLayout();
            // 
            // _dropDownToggle
            // 
            __DropDownToggle.Appearance = Appearance.Button;
            __DropDownToggle.Cursor = Cursors.Arrow;
            __DropDownToggle.Dock = DockStyle.Right;
            __DropDownToggle.Image = My.Resources.Resources.go_down_8;
            __DropDownToggle.Location = new Point(514, 0);
            __DropDownToggle.Margin = new Padding(0);
            __DropDownToggle.MaximumSize = new Size(24, 25);
            __DropDownToggle.Name = "__dropDownToggle";
            __DropDownToggle.Size = new Size(24, 25);
            __DropDownToggle.TabIndex = 0;
            __DropDownToggle.TextAlign = ContentAlignment.TopCenter;
            __DropDownToggle.UseVisualStyleBackColor = true;
            // 
            // _TextBox
            // 
            _TextBox.BackColor = SystemColors.Window;
            _TextBox.Dock = DockStyle.Fill;
            _TextBox.ForeColor = SystemColors.ControlText;
            _TextBox.Location = new Point(0, 0);
            _TextBox.Margin = new Padding(0);
            _TextBox.Name = "_TextBox";
            _TextBox.ReadOnlyBackColor = SystemColors.Control;
            _TextBox.ReadOnlyForeColor = SystemColors.WindowText;
            _TextBox.ReadWriteBackColor = SystemColors.Window;
            _TextBox.ReadWriteForeColor = SystemColors.ControlText;
            _TextBox.Size = new Size(514, 25);
            _TextBox.TabIndex = 1;
            // 
            // DropDownTextBox
            // 
            Controls.Add(_TextBox);
            Controls.Add(__DropDownToggle);
            Margin = new Padding(0);
            Name = "DropDownTextBox";
            Size = new Size(538, 25);
            ResumeLayout(false);
            PerformLayout();
        }

        private System.Windows.Forms.CheckBox __DropDownToggle;

        private System.Windows.Forms.CheckBox _DropDownToggle
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __DropDownToggle;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__DropDownToggle != null)
                {
                    __DropDownToggle.CheckedChanged -= DropDownToggle_CheckedChanged;
                }

                __DropDownToggle = value;
                if (__DropDownToggle != null)
                {
                    __DropDownToggle.CheckedChanged += DropDownToggle_CheckedChanged;
                }
            }
        }

        internal TextBox _TextBox;
    }
}
