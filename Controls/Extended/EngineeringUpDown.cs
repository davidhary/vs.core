using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace isr.Core.Controls
{

    /// <summary> Engineering up down. </summary>
    /// <remarks>
    /// Features: <para>
    /// Disables up/down events when read only.</para><para>
    /// Adds up/down cursor.</para><para>
    /// Adds engineering scaling. </para> (c) 2014 Integrated Scientific Resources, Inc. All rights
    /// reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-04-05 </para>
    /// </remarks>
    [Description( "Engineering Up Down" )]
    public class EngineeringUpDown : NumericUpDownBase
    {

        #region " CONSTRUCTION "

        /// <summary> object creator. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public EngineeringUpDown() : base()
        {
            this._Unit = string.Empty;
            this.UpdateScalingExponent( 0 );
        }

        #endregion

        #region " TEXT BOX "

        /// <summary> Strips post fix. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> A String. </returns>
        private string StripedText()
        {
            return this.TextBox is object && !string.IsNullOrWhiteSpace( this.Postfix ) && this.TextBox.Text.EndsWith( this.Postfix, StringComparison.OrdinalIgnoreCase ) ? this.TextBox.Text.Substring( 0, this.TextBox.Text.IndexOf( this.Postfix, StringComparison.OrdinalIgnoreCase ) ) : this.Value.ToString();
        }

        /// <summary> Strips post fix. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void StripPostFix()
        {
            if ( this.TextBox is object && !string.IsNullOrWhiteSpace( this.Postfix ) && this.TextBox.Text.EndsWith( this.Postfix, StringComparison.OrdinalIgnoreCase ) )
            {
                this.TextBox.Text = this.TextBox.Text.Substring( 0, this.TextBox.Text.IndexOf( this.Postfix, StringComparison.OrdinalIgnoreCase ) );
            }
        }

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.LostFocus" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnLostFocus( EventArgs e )
        {
            this.StripPostFix();
            base.OnLostFocus( e );
            this.UpdateEditText();
        }

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.Validating" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.ComponentModel.CancelEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnValidating( CancelEventArgs e )
        {
            this.StripPostFix();
            base.OnValidating( e );
            this.UpdateEditText();
        }

        /// <summary>
        /// Validates and updates the text displayed in the spin box (also known as an up-down control).
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        protected override void ValidateEditText()
        {
            this.StripPostFix();
            base.ValidateEditText();
        }

        /// <summary>
        /// Displays the current value of the spin box (also known as an up-down control) in the
        /// appropriate format.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        protected override void UpdateEditText()
        {
            base.UpdateEditText();
            if ( this.TextBox is object && !string.IsNullOrWhiteSpace( this.Postfix ) )
            {
                this.TextBox.AppendText( this.Postfix );
            }
        }

        #endregion

        #region " ENGINEERING "

        /// <summary> Builds the engineering scale to scale exponent hash. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> A Dictionary for translating trace events to trace levels. </returns>
        private static IDictionary<int, EngineeringScale> BuildEngineringUnitHash()
        {
            var dix2 = new Dictionary<int, EngineeringScale>();
            var dix3 = dix2;
            dix3.Add( -18, EngineeringScale.Atto );
            dix3.Add( 18, EngineeringScale.Exa );
            dix3.Add( 15, EngineeringScale.Femto );
            dix3.Add( -9, EngineeringScale.Giga );
            dix3.Add( -3, EngineeringScale.Kilo );
            dix3.Add( -2, EngineeringScale.Deci );
            dix3.Add( -6, EngineeringScale.Mega );
            dix3.Add( 6, EngineeringScale.Micro );
            dix3.Add( 2, EngineeringScale.Percent );
            dix3.Add( 3, EngineeringScale.Milli );
            dix3.Add( 9, EngineeringScale.Nano );
            dix3.Add( -14, EngineeringScale.Peta );
            dix3.Add( 12, EngineeringScale.Pico );
            dix3.Add( -12, EngineeringScale.Tera );
            dix3.Add( 0, EngineeringScale.Unity );
            return dix2;
        }

        /// <summary> Builds the engineering scale to scale exponent hash. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> A Dictionary for translating trace events to trace levels. </returns>
        private static IDictionary<EngineeringScale, int> BuildScaleExponentHash()
        {
            var dix2 = new Dictionary<EngineeringScale, int>();
            var dix3 = dix2;
            dix3.Add( EngineeringScale.Atto, -18 );
            dix3.Add( EngineeringScale.Exa, 18 );
            dix3.Add( EngineeringScale.Femto, 15 );
            dix3.Add( EngineeringScale.Giga, -9 );
            dix3.Add( EngineeringScale.Deci, -2 );
            dix3.Add( EngineeringScale.Kilo, -3 );
            dix3.Add( EngineeringScale.Mega, -6 );
            dix3.Add( EngineeringScale.Micro, 6 );
            dix3.Add( EngineeringScale.Milli, 3 );
            dix3.Add( EngineeringScale.Nano, 9 );
            dix3.Add( EngineeringScale.Percent, 2 );
            dix3.Add( EngineeringScale.Peta, -15 );
            dix3.Add( EngineeringScale.Pico, 12 );
            dix3.Add( EngineeringScale.Tera, -12 );
            dix3.Add( EngineeringScale.Unity, 0 );
            return dix2;
        }

        /// <summary> The engineering scale. </summary>
        private EngineeringScale _EngineeringScale;

        /// <summary> Gets or sets the engineering scale. </summary>
        /// <value> The engineering scale. </value>
        [DefaultValue( 0 )]
        [Category( "Behavior" )]
        [Description( "The engineering scale." )]
        public EngineeringScale EngineeringScale
        {
            get => this._EngineeringScale;

            set => this.UpdateEngineeringScale( value );
        }

        /// <summary> The scale exponent hash. </summary>
        private static IDictionary<EngineeringScale, int> _ScaleExponentHash;

        /// <summary> Gets the scale exponent hash. </summary>
        /// <value> The scale exponent hash. </value>
        private static IDictionary<EngineeringScale, int> ScaleExponentHash
        {
            get {
                if ( _ScaleExponentHash is null )
                {
                    _ScaleExponentHash = BuildScaleExponentHash();
                }

                return _ScaleExponentHash;
            }
        }

        /// <summary> Updates the engineering scale described by value. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The value. </param>
        private void UpdateEngineeringScale( EngineeringScale value )
        {
            this.UpdateScalingExponent( ScaleExponentHash[value] );
        }

        /// <summary> The scale value prefix hash. </summary>
        private static IDictionary<int, string> _ScaleValuePrefixHash;

        /// <summary> Gets the scale value prefix hash. </summary>
        /// <value> The scale value prefix hash. </value>
        private static IDictionary<int, string> ScaleValuePrefixHash
        {
            get {
                if ( _ScaleValuePrefixHash is null )
                {
                    _ScaleValuePrefixHash = BuildScaleValuePrefixHash();
                }

                return _ScaleValuePrefixHash;
            }
        }

        /// <summary> The engineering scales hash. </summary>
        private static IDictionary<int, EngineeringScale> _EngineeringScalesHash;

        /// <summary> Gets the engineering scales hash. </summary>
        /// <value> The engineering scales hash. </value>
        private static IDictionary<int, EngineeringScale> EngineeringScalesHash
        {
            get {
                if ( _EngineeringScalesHash is null )
                {
                    _EngineeringScalesHash = BuildEngineringUnitHash();
                }

                return _EngineeringScalesHash;
            }
        }

        /// <summary> Initializes the scale properties. </summary>
        /// <remarks> Percent units are treated as a special case. </remarks>
        /// <param name="value"> The value. </param>
        private void UpdateScalingExponent( int value )
        {
            // treat percent units as a special case
            if ( value == 2 || value == -2 )
            {
                this._ScalingExponent = 2;
            }
            else
            {
                // must be in powers of 3.
                this._ScalingExponent = 3 * (value / 3);
            }

            this.ScaleFactor = ( decimal ) Math.Pow( 10d, -this._ScalingExponent );
            this.Postfix = value == 0 && string.IsNullOrWhiteSpace( this.Unit ) ? string.Empty : $" {ScaleValuePrefixHash[value]}{this.Unit}";
            this._EngineeringScale = EngineeringScalesHash[this.ScalingExponent];
            // update the display to show the change in units, if any.
            this.UpdateEditText();
            this.OnValueChanged( EventArgs.Empty );
        }

        /// <summary> Builds a scale exponent to unit prefix hash. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> A Dictionary for translating exponents to units. </returns>
        private static IDictionary<int, string> BuildScaleValuePrefixHash()
        {
            var dix2 = new Dictionary<int, string>();
            var dix3 = dix2;
            dix3.Add( -18, "E" ); // Exa
            dix3.Add( -15, "P" ); // Peta
            dix3.Add( -12, "T" ); // Tera
            dix3.Add( -9, "G" );  // Giga
            dix3.Add( -6, "M" );  // Mega
            dix3.Add( -2, "D" );  // Deci
            dix3.Add( -3, "K" );  // Kilo
            dix3.Add( 0, "" );
            dix3.Add( 2, "%" );  // Percent
            dix3.Add( 3, "m" );  // Milli
            dix3.Add( 6, "u" );  // Micro
            dix3.Add( 9, "n" );  // Nano
            dix3.Add( 12, "p" ); // Pico
            dix3.Add( 15, "f" ); // Femto
            dix3.Add( 18, "a" ); // Atto
            return dix2;
        }

        /// <summary> The unit. </summary>
        private string _Unit;

        /// <summary> The engineering scale to display, e.g., V, A. </summary>
        /// <value> The unit. </value>
        [DefaultValue( "" )]
        [Category( "Appearance" )]
        [Description( "The engineering scale to display, e.g., V, A." )]
        public string Unit
        {
            get => this._Unit;

            set {
                this._Unit = value;
                // update the post fix.
                this.UpdateScalingExponent( this.ScalingExponent );
            }
        }

        /// <summary> Gets or sets the unscaled value. </summary>
        /// <value> The unscaled value. </value>
        public decimal UnscaledValue
        {
            get {
                if ( !decimal.TryParse( this.StripedText(), out decimal value ) )
                {
                    value = this.Value;
                }

                return value;
            }

            set => this.Value = value;
        }

        /// <summary> Gets the scaled sentinel. </summary>
        /// <value> <c>True</c> if scaled. </value>
        private bool IsScaled => this.ScaleFactor != 1m && this.ScaleFactor != 0m;

        /// <summary> Gets or sets the scaled value. </summary>
        /// <value> The scaled value. </value>
        [DefaultValue( 0 )]
        [Category( "Behavior" )]
        [Description( "The scaled value." )]
        public decimal ScaledValue
        {
            get => this.IsScaled ? this.ScaleFactor * this.UnscaledValue : this.UnscaledValue;

            set {
                if ( this.IsScaled )
                {
                    value /= this.ScaleFactor;
                }

                if ( this.TextBox is object && !string.IsNullOrWhiteSpace( this.Postfix ) )
                {
                    this.TextBox.Text = value.ToString();
                }

                this.UnscaledValue = value;
                this.TextBox.AppendText( this.Postfix );
            }
        }

        /// <summary> The scaling exponent. </summary>
        private int _ScalingExponent;

        /// <summary> Gets or sets the scaling exponent. </summary>
        /// <value> The scaling exponent. </value>
        protected int ScalingExponent
        {
            get => this._ScalingExponent;

            set => this.UpdateScalingExponent( value );
        }

        /// <summary> Gets or sets the scale factor. </summary>
        /// <value> The scale factor. </value>
        protected decimal ScaleFactor { get; set; }

        /// <summary> Gets or sets the text to append to the display value. </summary>
        /// <value> The post fix. </value>
        protected string Postfix { get; set; }

        #endregion

    }

    /// <summary> Values that represent engineering scale. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public enum EngineeringScale
    {

        /// <summary> An enum constant representing the unity option. </summary>
        [Description( "Unity (0)" )]
        Unity = 0,

        /// <summary> An enum constant representing the atto option. </summary>
        [Description( "Atto (a:-18)" )]
        Atto,

        /// <summary> An enum constant representing the femto option. </summary>
        [Description( "Femto (f:-15)" )]
        Femto,

        /// <summary> An enum constant representing the pico option. </summary>
        [Description( "Pico (p:-12)" )]
        Pico,

        /// <summary> An enum constant representing the nano option. </summary>
        [Description( "Nano (n:-9)" )]
        Nano,

        /// <summary> An enum constant representing the micro option. </summary>
        [Description( "Micro (u:-6)" )]
        Micro,

        /// <summary> An enum constant representing the milli option. </summary>
        [Description( "Milli (m:-3)" )]
        Milli,

        /// <summary> An enum constant representing the deci option. </summary>
        [Description( "Deci (D:2)" )]
        Deci,

        /// <summary> An enum constant representing the percent option. </summary>
        [Description( "Percent (D:-2)" )]
        Percent,

        /// <summary> An enum constant representing the kilo option. </summary>
        [Description( "Kilo (K:3)" )]
        Kilo,

        /// <summary> An enum constant representing the mega option. </summary>
        [Description( "Mega (M:6)" )]
        Mega,

        /// <summary> An enum constant representing the giga option. </summary>
        [Description( "Giga (G:9)" )]
        Giga,

        /// <summary> An enum constant representing the tera option. </summary>
        [Description( "Tera (T:12)" )]
        Tera,

        /// <summary> An enum constant representing the peta option. </summary>
        [Description( "Peta (P:15)" )]
        Peta,

        /// <summary> An enum constant representing the exa option. </summary>
        [Description( "Exa (E:18)" )]
        Exa
    }
}
