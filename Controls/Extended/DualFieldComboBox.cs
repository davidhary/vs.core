using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace isr.Core.Controls
{

    /// <summary> A drop down box with two fields. </summary>
    /// <remarks>
    /// (c) 2011 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2011-03-23, 1.02.4099 </para>
    /// </remarks>
    [Description( "Dual Field Combo Box" )]
    [DefaultBindingProperty( "Text" )]
    public partial class DualFieldComboBox : Forma.ModelViewBase
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-09-24. </remarks>
        public DualFieldComboBox()
        {
            this.InitializeComponent();
            this.__SecondFieldTextBox.Name = "_SecondFieldTextBox";
            this.__FirstFieldTextBox.Name = "_FirstFieldTextBox";
            this.__TextComboBox.Name = "_TextComboBox";
        }

        /// <summary>
        /// Releases the unmanaged resources used by the isr.Core.Forma.ModelViewBase and optionally
        /// releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        /// <summary> Gets the combo box. </summary>
        /// <value> The combo box. </value>
        public System.Windows.Forms.ComboBox ComboBox => this._TextComboBox;

        /// <summary> Parse record. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The value. </param>
        private void ParseRecord( string value )
        {
            if ( string.IsNullOrWhiteSpace( value ) ) // OrElse String.Is NullOrWhiteSpace(value.Trim) Then
            {
                value = string.Empty;
                this._FirstFieldTextBox.Text = value;
                this._SecondFieldTextBox.Text = value;
            }
            else
            {
                value = value.Trim();
                var names = value.Split( ' ' );
                if ( names.Length == 1 )
                {
                    this._SecondFieldTextBox.Text = value;
                    this._FirstFieldTextBox.Text = string.Empty;
                }
                else
                {
                    this._SecondFieldTextBox.Text = names[names.Length - 1];
                    this._FirstFieldTextBox.Text = value.Substring( 0, value.Length - this.SecondField.Length ).Trim();
                }
            }

            this._Text = value;
            this.NotifyPropertyChanged( nameof( DualFieldComboBox.Text ) );
        }

        /// <summary> The text. </summary>
        private string _Text;

        /// <summary> Gets or sets the text, which includes the two fields. </summary>
        /// <value> The full name. </value>
        public override string Text
        {
            get => this._Text;

            set {
                if ( string.IsNullOrWhiteSpace( value ) )
                {
                    value = string.Empty;
                }

                if ( !value.Equals( this.Text ) )
                {
                    this.ParseRecord( value );
                }
            }
        }

        /// <summary> Gets the first field. </summary>
        /// <value> The first field. </value>
        public string FirstField => this._FirstFieldTextBox.Text;

        /// <summary> Gets the second field. </summary>
        /// <value> The second field. </value>
        public string SecondField => this._SecondFieldTextBox.Text;

        /// <summary> Event handler. Called by _ValueComboBox for selected index changed events. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void TextComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            this.ParseRecord( this._TextComboBox.Text );
        }

        /// <summary>
        /// Builds a value based on the <see cref="FirstField">first</see> and
        /// <see cref="SecondField">second </see> fields.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> The built value. </returns>
        public string BuildValue()
        {
            return $"{this.FirstField} {this.SecondField}";
        }

        /// <summary> Event handler. Called by _FirstFieldTextBox for validated events. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void FirstFieldTextBox_Validated( object sender, EventArgs e )
        {
            // Me.parseRecord(Me.BuildValue)
        }

        /// <summary> Event handler. Called by _SecondFieldTextBox for validated events. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void SecondFieldTextBox_Validated( object sender, EventArgs e )
        {
            // Me.parseRecord(Me.BuildValue)
        }

        /// <summary> Event handler. Called by DualFieldComboBox for resize events. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void DualFieldComboBox_Resize( object sender, EventArgs e )
        {
            this.ResizeMe();
        }

        /// <summary> Event handler. Called by  for  events. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void ResizeMe()
        {
            const int buttonWidth = 17;
            this._Layout.Width = this.Width - buttonWidth - this.Padding.Left - this.Padding.Right;
            this._TextComboBox.Width = this._Layout.Width + 17 - this._Layout.Padding.Left;
            this._TextComboBox.Left = this._Layout.Left + this._Layout.Padding.Left;
            this._TextComboBox.Top = this.Height - this.Padding.Top - this.Padding.Bottom - this._TextComboBox.Height;
        }

        /// <summary> Event handler. Called by _TextComboBox for layout events. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Layout event information. </param>
        private void TextComboBox_Layout( object sender, LayoutEventArgs e )
        {
            this.ResizeMe();
        }

        /// <summary> Event handler. Called by DualFieldComboBox for validated events. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void DualFieldComboBox_Validated( object sender, EventArgs e )
        {
            this.ParseRecord( this.BuildValue() );
        }
    }
}
