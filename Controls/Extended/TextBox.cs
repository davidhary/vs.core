using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace isr.Core.Controls
{

    /// <summary> Text box with read only non-validation. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2013-09-27 </para>
    /// </remarks>
    [DesignerCategory( "code" )]
    [Description( "Text Box" )]
    public class TextBox : System.Windows.Forms.TextBox
    {

        #region " CONSTRUCTION "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public TextBox() : base()
        {
            base.ReadOnly = false;
            this.ApplyReadOnly();
        }

        #endregion

        #region " READ ONLY "

        /// <summary> The read only back color. </summary>
        private Color _ReadOnlyBackColor;

        /// <summary> Gets or sets the color of the read only back. </summary>
        /// <value> The color of the read only back. </value>
        [DefaultValue( typeof( Color ), "SystemColors.Control" )]
        [Description( "Back color when read only" )]
        [Category( "Appearance" )]
        public Color ReadOnlyBackColor
        {
            get {
                if ( this._ReadOnlyBackColor.IsEmpty )
                {
                    this._ReadOnlyBackColor = SystemColors.Control;
                }

                return this._ReadOnlyBackColor;
            }

            set => this._ReadOnlyBackColor = value;
        }

        /// <summary> The read only foreground color. </summary>
        private Color _ReadOnlyForeColor;

        /// <summary> Gets or sets the color of the read only foreground. </summary>
        /// <value> The color of the read only foreground. </value>
        [DefaultValue( typeof( Color ), "SystemColors.WindowText" )]
        [Description( "Fore color when read only" )]
        [Category( "Appearance" )]
        public Color ReadOnlyForeColor
        {
            get {
                if ( this._ReadOnlyForeColor.IsEmpty )
                {
                    this._ReadOnlyForeColor = SystemColors.WindowText;
                }

                return this._ReadOnlyForeColor;
            }

            set => this._ReadOnlyForeColor = value;
        }

        /// <summary> The read write back color. </summary>
        private Color _ReadWriteBackColor;

        /// <summary> Gets or sets the color of the read write back. </summary>
        /// <value> The color of the read write back. </value>
        [DefaultValue( typeof( Color ), "SystemColors.Window" )]
        [Description( "Back color when control is read/write" )]
        [Category( "Appearance" )]
        public Color ReadWriteBackColor
        {
            get {
                if ( this._ReadWriteBackColor.IsEmpty )
                {
                    this._ReadWriteBackColor = SystemColors.Window;
                }

                return this._ReadWriteBackColor;
            }

            set => this._ReadWriteBackColor = value;
        }

        /// <summary> The read write foreground color. </summary>
        private Color _ReadWriteForeColor;

        /// <summary> Gets or sets the color of the read write foreground. </summary>
        /// <value> The color of the read write foreground. </value>
        [DefaultValue( typeof( Color ), "System.Drawing.SystemColors.ControlText" )]
        [Description( "Fore color when control is read/write" )]
        [Category( "Appearance" )]
        public Color ReadWriteForeColor
        {
            get {
                if ( this._ReadWriteForeColor.IsEmpty )
                {
                    this._ReadWriteForeColor = SystemColors.ControlText;
                }

                return this._ReadWriteForeColor;
            }

            set => this._ReadWriteForeColor = value;
        }

        /// <summary> The read write context menu. </summary>
        private ContextMenu _ReadWriteContextMenu;

        /// <summary> Applies the read only. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void ApplyReadOnly()
        {
            if ( base.ReadOnly )
            {
                this._ReadWriteContextMenu = base.ContextMenu;
                this.ContextMenu = new ContextMenu();
                this.BackColor = this.ReadOnlyBackColor;
                this.ForeColor = this.ReadOnlyForeColor;
            }
            else
            {
                this.ContextMenu = this._ReadWriteContextMenu;
                this.BackColor = this.ReadWriteBackColor;
                this.ForeColor = this.ReadWriteForeColor;
            }

            this.CausesValidation = !base.ReadOnly;
        }

        /// <summary> Gets or sets the read only. </summary>
        /// <value> The read only. </value>
        [DefaultValue( false )]
        [Category( "Behavior" )]
        [Description( "Indicates whether the combo box is read only." )]
        public new bool ReadOnly
        {
            get => base.ReadOnly;

            set {
                if ( this.ReadOnly != value )
                {
                    base.ReadOnly = value;
                    this.ApplyReadOnly();
                }
            }
        }

        #endregion

    }
}
