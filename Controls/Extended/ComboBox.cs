using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

using isr.Core.WinForms.BindingExtensions;
namespace isr.Core.Controls
{

    /// <summary> Combo box with read only capability. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2013-09-27. </para><para>
    /// David, 2018-01-07. Added implementation of binding update on item changed. </para><para>
    /// Dan.A. http://www.CodeProject.com/KB/ComboBox/CSReadOnlyComboBox.aspx </para>
    /// </remarks>
    [DesignerCategory( "code" )]
    [Description( "Extended Combo Box" )]
    public class ComboBox : System.Windows.Forms.ComboBox
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public ComboBox() : base()
        {
            this._DropDownStyle = ComboBoxStyle.DropDown;
            // _ReadOnlyBackColor = SystemColors.Control
            // _ReadOnlyForeColor = SystemColors.WindowText
        }

        #region " READ ONLY IMPLEMENTATION "

        /// <summary> The read write context menu. </summary>
        private ContextMenu _ReadWriteContextMenu;

        /// <summary> true to read only. </summary>
        private bool _ReadOnly;

        /// <summary> Gets or sets the read only. </summary>
        /// <value> The read only. </value>
        [DefaultValue( false )]
        [Category( "Behavior" )]
        [Description( "Indicates whether the combo box is read only." )]
        public bool ReadOnly
        {
            get => this._ReadOnly;

            set {
                if ( this._ReadOnly != value )
                {
                    this._ReadOnly = value;
                    if ( value )
                    {
                        this._ReadWriteContextMenu = base.ContextMenu;
                        this.ContextMenu = new ContextMenu();
                        int h = this.Height;
                        base.DropDownStyle = ComboBoxStyle.Simple;
                        this.Height = h + 3;
                        this.BackColor = this.ReadOnlyBackColor;
                        this.ForeColor = this.ReadOnlyForeColor;
                    }
                    else
                    {
                        this.ContextMenu = this._ReadWriteContextMenu;
                        base.DropDownStyle = this._DropDownStyle;
                        this.BackColor = this.ReadWriteBackColor;
                        this.ForeColor = this.ReadWriteForeColor;
                    }
                }
            }
        }

        /// <summary> The read only back color. </summary>
        private Color _ReadOnlyBackColor;

        /// <summary> Gets or sets the color of the read only back. </summary>
        /// <value> The color of the read only back. </value>
        [DefaultValue( typeof( Color ), "SystemColors.Control" )]
        [Description( "Back color when read only" )]
        [Category( "Appearance" )]
        public Color ReadOnlyBackColor
        {
            get {
                if ( this._ReadOnlyBackColor.IsEmpty )
                {
                    this._ReadOnlyBackColor = SystemColors.Control;
                }

                return this._ReadOnlyBackColor;
            }

            set => this._ReadOnlyBackColor = value;
        }

        /// <summary> The read only foreground color. </summary>
        private Color _ReadOnlyForeColor;

        /// <summary> Gets or sets the color of the read only foreground. </summary>
        /// <value> The color of the read only foreground. </value>
        [DefaultValue( typeof( Color ), "SystemColors.WindowText" )]
        [Description( "Fore color when read only" )]
        [Category( "Appearance" )]
        public Color ReadOnlyForeColor
        {
            get {
                if ( this._ReadOnlyForeColor.IsEmpty )
                {
                    this._ReadOnlyForeColor = SystemColors.WindowText;
                }

                return this._ReadOnlyForeColor;
            }

            set => this._ReadOnlyForeColor = value;
        }

        /// <summary> The read write back color. </summary>
        private Color _ReadWriteBackColor;

        /// <summary> Gets or sets the color of the read write back. </summary>
        /// <value> The color of the read write back. </value>
        [DefaultValue( typeof( Color ), "SystemColors.Window" )]
        [Description( "Back color when control is read/write" )]
        [Category( "Appearance" )]
        public Color ReadWriteBackColor
        {
            get {
                if ( this._ReadWriteBackColor.IsEmpty )
                {
                    this._ReadWriteBackColor = SystemColors.Window;
                }

                return this._ReadWriteBackColor;
            }

            set => this._ReadWriteBackColor = value;
        }

        /// <summary> The read write foreground color. </summary>
        private Color _ReadWriteForeColor;

        /// <summary> Gets or sets the color of the read write foreground. </summary>
        /// <value> The color of the read write foreground. </value>
        [DefaultValue( typeof( Color ), "System.Drawing.SystemColors.ControlText" )]
        [Description( "Fore color when control is read/write" )]
        [Category( "Appearance" )]
        public Color ReadWriteForeColor
        {
            get {
                if ( this._ReadWriteForeColor.IsEmpty )
                {
                    this._ReadWriteForeColor = SystemColors.ControlText;
                }

                return this._ReadWriteForeColor;
            }

            set => this._ReadWriteForeColor = value;
        }

        /// <summary> The drop down style. </summary>
        private ComboBoxStyle _DropDownStyle;

        /// <summary> Gets or sets the drop down style. </summary>
        /// <value> The drop down style. </value>
        public new ComboBoxStyle DropDownStyle
        {
            get => this._DropDownStyle;

            set {
                if ( this._DropDownStyle != value )
                {
                    this._DropDownStyle = value;
                    if ( !this.ReadOnly )
                    {
                        base.DropDownStyle = value;
                    }
                }
            }
        }

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.KeyDown" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.KeyEventArgs" /> that contains the event
        /// data. </param>
        protected override void OnKeyDown( KeyEventArgs e )
        {
            if ( e is null )
            {
                return;
            }

            if ( this.ReadOnly && (e.KeyCode == Keys.Up || e.KeyCode == Keys.Down || e.KeyCode == Keys.Delete || e.KeyCode == Keys.F4 || e.KeyCode == Keys.PageDown || e.KeyCode == Keys.PageUp) )
            {
                e.Handled = true;
            }
            else
            {
                base.OnKeyDown( e );
            }
        }

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.KeyPress" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.KeyPressEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnKeyPress( KeyPressEventArgs e )
        {
            if ( e is null )
            {
                return;
            }

            if ( this.ReadOnly )
            {
                e.Handled = true;
            }
            else
            {
                base.OnKeyPress( e );
            }
        }

        #endregion

        #region " SELECTED ITEM CHANGE CHANGE BINDING "

        /// <summary>
        /// Overrides raising the <see cref="E:System.Windows.Forms.DomainUpDown.SelectedItemChanged" />
        /// event and updates the data source if <see cref="DataSourceUpdateMode"/> =.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        /// <seealso cref="DataSourceUpdateMode.OnPropertyChanged "/>
        protected override void OnSelectedItemChanged( EventArgs e )
        {
            base.OnSelectedItemChanged( e );
            Binding binding = this.SelectBinding( nameof( ComboBox.SelectedItem ) );
            if ( binding is object && binding.DataSourceUpdateMode == DataSourceUpdateMode.OnPropertyChanged )
            {
                binding.WriteValue();
            }
        }

        #endregion

    }
}
