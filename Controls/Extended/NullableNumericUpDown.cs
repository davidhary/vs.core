using System;
using System.ComponentModel;

namespace isr.Core.Controls
{

    /// <summary> A numeric up down with Nullable value. </summary>
    /// <remarks>
    /// (c) 2011 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2011-08-19, 1.02.4248 </para>
    /// </remarks>
    [DesignerCategory( "code" )]
    [Description( "Nullable Numeric Up Down" )]
    public class NullableNumericUpDown : NumericUpDownBase
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="NullableNumericUpDown" /> class.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public NullableNumericUpDown() : base()
        {
        }

        /// <summary> Gets or sets the Nullable value. </summary>
        /// <value> The Nullable value. </value>
        [Category( "Behavior" )]
        [Description( "The Nullable value." )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public decimal? NullableValue
        {
            get => string.IsNullOrWhiteSpace( this.TextBox.Text ) ? new decimal?() : this.Value;

            set {
                if ( value.HasValue )
                {
                    this.Value = value.Value;
                }
                else
                {
                    this.TextBox.Text = string.Empty;
                }
            }
        }

        /// <summary> Occurs when the underlying text changes. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="source"> The source of the event. </param>
        /// <param name="e">      An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnChanged( object source, EventArgs e )
        {
            // this is required so that the Nullable value is updated when spinning after it was set to null.
            this.OnValueChanged( e );
            base.OnChanged( source, e );
        }
    }
}
