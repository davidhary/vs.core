using System;
using System.ComponentModel;
using System.Windows.Forms;

using isr.Core.Controls.ExceptionExtensions;

namespace isr.Core.Controls
{

    /// <summary> Data grid view. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2013-09-27 </para>
    /// </remarks>
    [DesignerCategory( "code" )]
    [Description( "Extended Data Grid View" )]
    public class DataGridView : System.Windows.Forms.DataGridView
    {

        #region " CELL FORMATTING "

        /// <summary>
        /// Work around to a problem with the data grid view failure to handle the format provider.
        /// http://stackoverflow.com/questions/3627922/format-time-span-in-datagridview-column.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnCellFormatting( DataGridViewCellFormattingEventArgs e )
        {
            if ( e is null )
            {
                base.OnCellFormatting( e );
            }
            else
            {
                if ( !(e.CellStyle.FormatProvider is ICustomFormatter formatter) )
                {
                    base.OnCellFormatting( e );
                }
                else
                {
                    e.Value = formatter.Format( e.CellStyle.Format, e.Value, e.CellStyle.FormatProvider );
                    e.FormattingApplied = true;
                }
            }
        }

        #endregion

        #region " DATA ERROR "

        /// <summary> Ignores the data error. </summary>
        /// <remarks> Uses the collection to determine if the collection is in edit mode. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      The <see cref="DataGridViewDataErrorEventArgs"/>
        /// instance containing the event data. </param>
        /// <returns> <c>True</c> if editing error can be ignored, <c>False</c> otherwise. </returns>
        public static bool IgnoreDataError( object sender, DataGridViewDataErrorEventArgs e )
        {
            if ( e is null || sender is null )
            {
                return true;
            }
            else
            {
                if ( !(sender is DataGridView grid) )
                {
                    return true;
                }
                else if ( grid.CurrentCell is null || grid.CurrentRow is null )
                {
                    return true;
                }
                else if ( grid.CurrentCell.IsInEditMode || grid.CurrentRow.IsNewRow || grid.IsCurrentCellInEditMode || grid.IsCurrentRowDirty )
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary> Builds the data error. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="grid"> The grid. </param>
        /// <param name="e">    The <see cref="DataGridViewDataErrorEventArgs"/>
        /// instance containing the event data. </param>
        /// <returns> System.String. </returns>
        public static string BuildDataError( System.Windows.Forms.DataGridView grid, DataGridViewDataErrorEventArgs e )
        {
            // prevent error reporting when adding a new row or editing a cell
            if ( grid is null || e is null || e.Exception is null || grid.CurrentCell is null || grid.CurrentRow is null )
            {
                return string.Empty;
            }
            else
            {
                string cellValue = "nothing";
                if ( grid.CurrentCell.Value is object )
                {
                    cellValue = grid.CurrentCell.Value.ToString();
                }

                return $"Data error occurred at Grid {grid.Name}(R{e.RowIndex},C{e.ColumnIndex}):{grid.Columns[e.ColumnIndex].Name}. Cell value is '{cellValue}';. {e.Exception.ToFullBlownString()}";
            }
        }

        /// <summary> Gets or sets a value indicating whether to suppress data error. </summary>
        /// <value> <c>True</c> if suppressing data error; otherwise, <c>False</c>. </value>
        [Category( "Behavior" )]
        [Description( "Suppresses all data errors." )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( false )]
        public bool SuppressDataError { get; set; }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.DataGridView.DataError" /> event.
        /// </summary>
        /// <remarks>
        /// Allows suppression of data error. This became necessary as we are unable to prevent data
        /// error when changing the data source of a data grid with combo boxes.
        /// </remarks>
        /// <param name="displayErrorDialogIfNoHandler"> true to display an error dialog box if there is
        /// no handler for the
        /// <see cref="E:System.Windows.Forms.DataGridView.DataError" />
        /// event. </param>
        /// <param name="e">                             A <see cref="T:System.Windows.Forms.DataGridViewDat
        /// aErrorEventArgs" /> that contains the event data.
        /// </param>
        protected override void OnDataError( bool displayErrorDialogIfNoHandler, DataGridViewDataErrorEventArgs e )
        {
            if ( !this.SuppressDataError )
            {
                base.OnDataError( displayErrorDialogIfNoHandler, e );
            }
        }


        #endregion

    }
}
