using System;
using System.ComponentModel;
using System.Security.Permissions;
using System.Windows.Forms;

namespace isr.Core.Controls
{

    /// <summary> A tab control with hidden tab page titles in run time. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2012-06-28, 1.02.4562.x From Hans PASSANT </para><para>
    /// (http://StackOverflow.com/users/17034/hans-PASSANT)
    /// http://StackOverflow.com/questions/1824036/TabControl-how-can-you-remove-the-TabPage-title.
    /// </para>
    /// </remarks>
    [DesignerCategory( "code" )]
    [Description( "Title-Less Tab Control" )]
    public class TitleLessTabControl : TabControl
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public TitleLessTabControl() : base()
        {
            this._PreviousPageCount = 0;
        }

        #region " TAB PAGE TITLE HIDING ENGINE "

        /// <summary> Number of previous pages. </summary>
        private int _PreviousPageCount;

        /// <summary> Checks if a single tab page. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void CheckSingleTabPage()
        {
            if ( this.IsHandleCreated )
            {
                int pages = this._PreviousPageCount;
                this._PreviousPageCount = this.TabCount;
                if ( pages == 1 && this._PreviousPageCount > 1 || pages > 1 && this._PreviousPageCount == 1 )
                {
                    this.RecreateHandle();
                }
            }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.ControlAdded" /> event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.ControlEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnControlAdded( ControlEventArgs e )
        {
            base.OnControlAdded( e );
            this.CheckSingleTabPage();
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.ControlRemoved" /> event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.ControlEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnControlRemoved( ControlEventArgs e )
        {
            base.OnControlRemoved( e );
            this.CheckSingleTabPage();
        }

        /// <summary> Windows Procedure override to hide tab headers. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="m"> [in,out] The Windows <see cref="T:System.Windows.Forms.Message" /> to
        /// process. </param>
        [SecurityPermission( SecurityAction.Demand, Flags = SecurityPermissionFlag.UnmanagedCode )]
        protected override void WndProc( ref Message m )
        {
            // Hide tabs by trapping the message
            const int TCM_ADJUSTRECT = 0x1328;
            if ( m.Msg == TCM_ADJUSTRECT && !this.DesignMode && (this.TabCount == 1 || !this.HideSingleTabLabelOnly) )
            {
                m.Result = new IntPtr( 1 ); // CType(1, IntPtr)
            }
            else
            {
                base.WndProc( ref m );
            }
        }

        #endregion

        #region " VISIBLE PROPERTIES "

        /// <summary> true to hide, false to show the single tab page title only. </summary>
        private bool _HideSingleTabPageTitleOnly;

        /// <summary> Gets or sets the hide single tab label only. </summary>
        /// <value> The hide single tab label only. </value>
        [Category( "Behavior" )]
        [Description( "Toggle hiding all or only one tab page title." )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( false )]
        public bool HideSingleTabLabelOnly
        {
            get => this._HideSingleTabPageTitleOnly;

            set {
                this._HideSingleTabPageTitleOnly = value;
                this.CheckSingleTabPage();
            }
        }

        #endregion

    }
}
