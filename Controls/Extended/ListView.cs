using System.ComponentModel;
using System.Windows.Forms;

namespace isr.Core.Controls
{

    /// <summary>
    /// This class provides a non-flickering ListView.  A heartfelt "thank you" goes to stormenet on
    /// StackOverflow.
    /// </summary>
    /// <remarks>
    /// (c) 2011 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2011-08-19, 1.02.4248 </para>
    /// </remarks>
    [DesignerCategory( "code" )]
    [Description( "Non-Flickering List View" )]
    public class ListView : System.Windows.Forms.ListView
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public ListView() : base()
        {

            // Activate double buffering
            this.SetStyle( ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint, true );

            // Enable the OnNotifyMessage event so we get a chance to filter out 
            // Windows messages before they get to the form's WndProc
            this.SetStyle( ControlStyles.EnableNotifyMessage, true );
        }

        /// <summary> Notifies the control of Windows messages. </summary>
        /// <remarks> Filters out the erase background message. </remarks>
        /// <param name="m"> A <see cref="T:System.Windows.Forms.Message" /> that represents the Windows
        /// message. </param>
        protected override void OnNotifyMessage( Message m )
        {
            // Filter out the WM_ERASEBKGND message
            if ( m.Msg != 0x14 )
            {
                base.OnNotifyMessage( m );
            }
        }
    }
}
