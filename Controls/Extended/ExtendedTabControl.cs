using System;
using System.ComponentModel;
using System.Security.Permissions;
using System.Windows.Forms;

namespace isr.Core.Controls
{

    /// <summary> Extended Tab control with hidden tab headers. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-04-25, 2.1.5593. </para>
    /// </remarks>
    [DesignerCategory( "code" )]
    [Description( "Extended Tab Control" )]
    public class ExtendedTabControl : TabControl
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public ExtendedTabControl() : base()
        {
        }

        #region " Windows Procedure "

        /// <summary> Windows Procedure override to hide tab headers. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="m"> [in,out] The Windows <see cref="T:System.Windows.Forms.Message" /> to
        /// process. </param>
        [SecurityPermission( SecurityAction.Demand, Flags = SecurityPermissionFlag.UnmanagedCode )]
        protected override void WndProc( ref Message m )
        {
            // Hide tabs by trapping the message
            const int TCM_ADJUSTRECT = 0x1328;
            if ( this.HideTabHeaders && m.Msg == TCM_ADJUSTRECT && !this.DesignMode )
            {
                m.Result = new IntPtr( 1 );
            }
            else
            {
                base.WndProc( ref m );
            }
        }

        #endregion

        /// <summary> Gets or sets a value indicating whether the tab headers should be drawn. </summary>
        /// <value> <c>true</c> to hide tab headers; otherwise <c>false</c> </value>
        [Description( "Gets or sets a value indicating whether the tab headers should be drawn" )]
        [DefaultValue( false )]
        public bool HideTabHeaders { get; set; }
    }
}
