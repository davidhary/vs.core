﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace isr.Core.Controls
{
    [Microsoft.VisualBasic.CompilerServices.DesignerGenerated()]
    public partial class DualRadioButton
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            _Layout = new TableLayoutPanel();
            __PrimaryRadioButton = new RadioButton();
            __PrimaryRadioButton.CheckedChanged += new EventHandler(PrimaryRadioButtonCheckedChanged);
            __SecondaryRadioButton = new RadioButton();
            __SecondaryRadioButton.CheckedChanged += new EventHandler(SecondaryRadioButtonCheckedChanged);
            _Layout.SuspendLayout();
            SuspendLayout();
            // 
            // _Layout
            // 
            _Layout.BackColor = Color.Transparent;
            _Layout.ColumnCount = 3;
            _Layout.ColumnStyles.Add(new ColumnStyle());
            _Layout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100.0f));
            _Layout.ColumnStyles.Add(new ColumnStyle());
            _Layout.Controls.Add(__SecondaryRadioButton, 2, 1);
            _Layout.Controls.Add(__PrimaryRadioButton, 0, 1);
            _Layout.Dock = DockStyle.Fill;
            _Layout.Location = new Point(0, 0);
            _Layout.Name = "_Layout";
            _Layout.RowCount = 3;
            _Layout.RowStyles.Add(new RowStyle(SizeType.Percent, 50.0f));
            _Layout.RowStyles.Add(new RowStyle());
            _Layout.RowStyles.Add(new RowStyle(SizeType.Percent, 50.0f));
            _Layout.Size = new Size(107, 34);
            _Layout.TabIndex = 0;
            // 
            // _PrimaryRadioButton
            // 
            __PrimaryRadioButton.AutoSize = true;
            __PrimaryRadioButton.Checked = true;
            __PrimaryRadioButton.Dock = DockStyle.Top;
            __PrimaryRadioButton.Location = new Point(3, 8);
            __PrimaryRadioButton.Name = "__PrimaryRadioButton";
            __PrimaryRadioButton.Size = new Size(41, 17);
            __PrimaryRadioButton.TabIndex = 1;
            __PrimaryRadioButton.TabStop = true;
            __PrimaryRadioButton.Text = "ON";
            __PrimaryRadioButton.UseVisualStyleBackColor = true;
            // 
            // _SecondaryRadioButton
            // 
            __SecondaryRadioButton.AutoSize = true;
            __SecondaryRadioButton.Dock = DockStyle.Top;
            __SecondaryRadioButton.Location = new Point(59, 8);
            __SecondaryRadioButton.Name = "__SecondaryRadioButton";
            __SecondaryRadioButton.Size = new Size(45, 17);
            __SecondaryRadioButton.TabIndex = 1;
            __SecondaryRadioButton.Text = "OFF";
            __SecondaryRadioButton.UseVisualStyleBackColor = true;
            // 
            // DualRadioButton
            // 
            AutoScaleDimensions = new SizeF(6.0f, 13.0f);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = Color.Transparent;
            Controls.Add(_Layout);
            Name = "DualRadioButton";
            Size = new Size(107, 34);
            _Layout.ResumeLayout(false);
            _Layout.PerformLayout();
            ResumeLayout(false);
        }

        private TableLayoutPanel _Layout;
        private RadioButton __SecondaryRadioButton;

        private RadioButton _SecondaryRadioButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SecondaryRadioButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SecondaryRadioButton != null)
                {
                    __SecondaryRadioButton.CheckedChanged -= SecondaryRadioButtonCheckedChanged;
                }

                __SecondaryRadioButton = value;
                if (__SecondaryRadioButton != null)
                {
                    __SecondaryRadioButton.CheckedChanged += SecondaryRadioButtonCheckedChanged;
                }
            }
        }

        private RadioButton __PrimaryRadioButton;

        private RadioButton _PrimaryRadioButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __PrimaryRadioButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__PrimaryRadioButton != null)
                {
                    __PrimaryRadioButton.CheckedChanged -= PrimaryRadioButtonCheckedChanged;
                }

                __PrimaryRadioButton = value;
                if (__PrimaryRadioButton != null)
                {
                    __PrimaryRadioButton.CheckedChanged += PrimaryRadioButtonCheckedChanged;
                }
            }
        }
    }
}