using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Security.Permissions;
using System.Windows.Forms;

using isr.Core.Controls.ExceptionExtensions;

namespace isr.Core.Controls
{

    /// <summary> Numeric Up Down base control. </summary>
    /// <remarks>
    /// Features:<para>
    /// Text selection properties similar to text box control;</para><para>
    /// Controls handling mouse wheel events;</para><para>
    /// Fixes mouse enter and leave event handling;</para><para>
    /// Adds value-incrementing and -decrementing events allowing to alter the increment or decrement
    /// dynamically;</para><para>
    /// Adds Wrap Value property to allow the wrapping of the value to maximum or minimum when
    /// reaching the minimum or maximum, respectively;</para><para>
    /// Adds option to show up the up/down buttons when the control has focus regardless or mouse
    /// over.</para><para>
    /// Disables up/down events when read only.</para><para>
    /// Adds up/down cursor.</para><para>
    /// Adds engineering scaling.</para><para>
    /// Author:   Claudio NiCora</para><para>
    /// WebSite:  http://CoolSoft.AlterVista.org </para><para>
    /// CodeProject: http://www.CodeProject.com/KB/edit/NumericUpDownEx.aspx </para><para>
    /// Feel free to contribute here: HTTP://CoolSoft.AlterVista.org </para> <para>
    /// (c) 2013 Claudio NiCora.</para><para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-04-05 </para>
    /// </remarks>
    [DesignerCategory( "code" )]
    [Description( "Numeric Up Down Base Control" )]
    public abstract class NumericUpDownBase : System.Windows.Forms.NumericUpDown
    {

        #region " CONSTRUCTION "

        /// <summary> Specialized default constructor for use only by derived classes. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        protected NumericUpDownBase() : base()
        {

            // extract a reference to the underlying TextBox field
            // Me._TextBox = GetPrivateField(Of TextBox)(Me, "upDownEdit")
            this.TextBox = this.Controls[1] as System.Windows.Forms.TextBox;
            if ( this.TextBox is null || this.TextBox.GetType().FullName != "System.Windows.Forms.UpDownBase+UpDownEdit" )
            {
                throw new ArgumentNullException( this.GetType().FullName + ": Can't get a reference to the internal Text Box field." );
            }

            // extract a reference to the underlying UpDownButtons field
            this.UpDownButtons = this.Controls[0];
            // Me._UpDownButtons = GetPrivateField(Of Control)(Me, "upDownButtons")
            this.UpDownButtons.Cursor = this.UpDownButtons is null || this.UpDownButtons.GetType().FullName != "System.Windows.Forms.UpDownBase+UpDownButtons"
                ? throw new ArgumentNullException( this.GetType().FullName + ": Can't get a reference to the internal UpDown buttons field." )
                : this._UpDownCursor;

            this._HasFocus = false;
            this._UpDownDisplayMode = UpDownButtonsDisplayMode.Always;
            this.InterceptMouseWheel = InterceptMouseWheelMode.Always;

            // add handlers (MouseEnter and MouseLeave events of NumericUpDown
            // are not working properly)
            this.TextBox.MouseEnter += this.MouseEnterLeaveThis;
            this.TextBox.MouseLeave += this.MouseEnterLeaveThis;
            this.UpDownButtons.MouseEnter += this.MouseEnterLeaveThis;
            this.UpDownButtons.MouseLeave += this.MouseEnterLeaveThis;
            base.MouseEnter += this.MouseEnterLeaveThis;
            base.MouseLeave += this.MouseEnterLeaveThis;
        }

        /// <summary>
        /// Releases the unmanaged resources used by the NumericUpDown and optionally releases the
        /// managed resources.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    try
                    {
                        this.TextBox.MouseEnter -= this.MouseEnterLeaveThis;
                    }
                    catch
                    {
                    }

                    try
                    {
                        this.TextBox.MouseLeave -= this.MouseEnterLeaveThis;
                    }
                    catch
                    {
                    }

                    try
                    {
                        this.UpDownButtons.MouseEnter -= this.MouseEnterLeaveThis;
                    }
                    catch
                    {
                    }

                    try
                    {
                        this.UpDownButtons.MouseLeave -= this.MouseEnterLeaveThis;
                    }
                    catch
                    {
                    }

                    try
                    {
                        base.MouseEnter -= this.MouseEnterLeaveThis;
                    }
                    catch
                    {
                    }

                    try
                    {
                        base.MouseLeave -= this.MouseEnterLeaveThis;
                    }
                    catch
                    {
                    }

                    this.RemoveNumericTextChangedEventHandler( NumericTextChanged );
                    this.RemoveValueDecrementingEventHandler( ValueDecrementing );
                    this.RemoveValueIncrementingEventHandler( ValueIncrementing );
                    this.RemoveMouseEnterEvent( MouseEnter );
                    this.RemoveMouseLeaveEvent( MouseLeave );
                }
            }
            catch
            {
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " WINDOWS PROCEDURE "

        /// <summary> Windows Procedure override to kill wN_MouseWheel message. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="m"> [in,out] The Windows <see cref="T:System.Windows.Forms.Message" /> to
        /// process. </param>
        [SecurityPermission( SecurityAction.Demand, Flags = SecurityPermissionFlag.UnmanagedCode )]
        protected override void WndProc( ref Message m )
        {
            const int WM_MOUSEWHEEL = 0x20A;
            if ( m.Msg == WM_MOUSEWHEEL )
            {
                switch ( this.InterceptMouseWheel )
                {
                    case InterceptMouseWheelMode.Always:
                        {
                            // standard message
                            base.WndProc( ref m );
                            break;
                        }

                    case InterceptMouseWheelMode.WhenMouseOver:
                        {
                            if ( this._MouseOver )
                            {
                                // standard message
                                base.WndProc( ref m );
                            }

                            break;
                        }

                    case InterceptMouseWheelMode.Never:
                        {
                            // kill the message
                            return;
                        }
                }
            }
            else
            {
                base.WndProc( ref m );
            }
        }

        #endregion

        #region " ON EVENTS "

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.Paint" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.PaintEventArgs" />  that contains the
        /// event data. </param>
        protected override void OnPaint( PaintEventArgs e )
        {
            if ( e is null )
            {
                return;
            }

            if ( this.UpDownButtons is object && this.UpDownButtons.Visible == false )
            {
                e.Graphics.Clear( this.BackColor );
            }

            base.OnPaint( e );
        }

        #endregion

        #region " UP DOWN BUTTONS "

        /// <summary> Reference to the underlying UpDownButtons control. </summary>
        /// <value> The up down buttons. </value>
        protected Control UpDownButtons { get; set; }

        /// <summary> The up down cursor. </summary>
        private Cursor _UpDownCursor;

        /// <summary> Gets or sets the up down cursor. </summary>
        /// <value> The up down cursor. </value>
        [DefaultValue( typeof( Cursor ), "System.Windows.Forms.Cursors.Default" )]
        [Description( "The up/down cursor" )]
        [Category( "Appearance" )]
        public Cursor UpDownCursor
        {
            get => this._UpDownCursor;

            set {
                this._UpDownCursor = value;
                if ( this.UpDownButtons is object )
                {
                    this.UpDownButtons.Cursor = value;
                }
            }
        }

        #endregion

        #region " TEXT BOX "

        /// <summary> Gets or sets the text box. </summary>
        /// <value> The text box. </value>
        protected System.Windows.Forms.TextBox TextBox { get; set; }

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.GotFocus" /> event. </summary>
        /// <remarks> select all the text on focus enter. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnGotFocus( EventArgs e )
        {
            this._HasFocus = true;
            if ( this.AutoSelect )
            {
                this.TextBox.SelectAll();
            }
            // Update UpDownButtons visibility
            if ( this.ShowUpDownButtons )
            {
                this.UpdateUpDownButtonsVisibility();
            }

            base.OnGotFocus( e );
        }

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.LostFocus" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnLostFocus( EventArgs e )
        {
            this._HasFocus = false;
            // Update UpDownButtons visibility
            if ( this.ShowUpDownButtons )
            {
                this.UpdateUpDownButtonsVisibility();
            }

            base.OnLostFocus( e );
        }

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.MouseUp" /> event. </summary>
        /// <remarks>
        /// MouseUp will kill the SelectAll made on GotFocus. Will restore it, but only if user have not
        /// made a partial text selection.
        /// </remarks>
        /// <param name="mevent"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnMouseUp( MouseEventArgs mevent )
        {
            if ( this.AutoSelect && this.TextBox.SelectionLength == 0 )
            {
                this.TextBox.SelectAll();
            }

            base.OnMouseUp( mevent );
        }

        /// <summary> Gets or sets the selection start. </summary>
        /// <value> The selection start. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public int SelectionStart
        {
            get => this.TextBox.SelectionStart;

            set => this.TextBox.SelectionStart = value;
        }

        /// <summary>   Gets or sets the selection length. </summary>
        /// <value> The length of the selection. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public int SelectionLength
        {
            get => this.TextBox.SelectionLength;
            set => this.TextBox.SelectionLength = value;
        }

        /// <summary>   Gets or sets the selected text. </summary>
        /// <value> The selected text. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public string SelectedText
        {
            get => this.TextBox.SelectedText;
            set => this.TextBox.SelectedText = value;
        }

        /// <summary>
        /// Gets or sets the text to be displayed in the
        /// <see cref="T:System.Windows.Forms.NumericUpDown" /> control.
        /// </summary>
        /// <remarks>
        /// After reset this value clears not reflecting the
        /// <see cref="Value">value</see>. Use <see cref="Sync">Sync</see> to sync the
        /// <see cref="Text">text</see> and <see cref="Value">value</see>.
        /// </remarks>
        /// <value> Null. </value>
        public override string Text
        {
            get => base.Text;

            set => base.Text = value;
        }

        /// <summary>
        /// Synchronizes the <see cref="Text">text</see> and <see cref="Value">value</see>.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public void Sync()
        {
            string textValue = this.Value.ToString();
            if ( !string.Equals( this.Text, textValue ) )
            {
                this.Text = textValue;
            }
        }

        /// <summary> Event queue for all listeners interested in NumericTextChanged events. </summary>
        public event EventHandler<EventArgs> NumericTextChanged;

        /// <summary> Removes event handler. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The handler. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void RemoveNumericTextChangedEventHandler( EventHandler<EventArgs> value )
        {
            foreach ( Delegate d in value is null ? (Array.Empty<Delegate>()) : value.GetInvocationList() )
            {
                try
                {
                    NumericTextChanged -= ( EventHandler<EventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.TextChanged" /> event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnTextChanged( EventArgs e )
        {
            base.OnTextChanged( e );
            var evt = NumericTextChanged;
            evt?.Invoke( this, e );
        }

        #endregion

        #region " REFLECTION "

        /// <summary> Extracts a reference to a private underlying field. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="ctrl">      The control. </param>
        /// <param name="fieldName"> Name of the field. </param>
        /// <returns> The private field. </returns>
        protected internal static T GetPrivateField<T>( System.Windows.Forms.NumericUpDown ctrl, string fieldName ) where T : Control
        {
            // find internal TextBox
            var fi = typeof( System.Windows.Forms.NumericUpDown ).GetField( fieldName, System.Reflection.BindingFlags.FlattenHierarchy | System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance );
            // take some caution... they could change field name in the future!
            return fi is null ? null : fi.GetValue( ctrl ) as T;
        }

        #endregion

        #region " READ ONLY "

        /// <summary> The read only back color. </summary>
        private Color _ReadOnlyBackColor;

        /// <summary> Gets or sets the color of the read only back. </summary>
        /// <value> The color of the read only back. </value>
        [DefaultValue( typeof( Color ), "SystemColors.Control" )]
        [Description( "Back color when read only" )]
        [Category( "Appearance" )]
        public Color ReadOnlyBackColor
        {
            get {
                if ( this._ReadOnlyBackColor.IsEmpty )
                {
                    this._ReadOnlyBackColor = SystemColors.Control;
                }

                return this._ReadOnlyBackColor;
            }

            set => this._ReadOnlyBackColor = value;
        }

        /// <summary> The read only foreground color. </summary>
        private Color _ReadOnlyForeColor;

        /// <summary> Gets or sets the color of the read only foreground. </summary>
        /// <value> The color of the read only foreground. </value>
        [DefaultValue( typeof( Color ), "SystemColors.WindowText" )]
        [Description( "Fore color when read only" )]
        [Category( "Appearance" )]
        public Color ReadOnlyForeColor
        {
            get {
                if ( this._ReadOnlyForeColor.IsEmpty )
                {
                    this._ReadOnlyForeColor = SystemColors.WindowText;
                }

                return this._ReadOnlyForeColor;
            }

            set => this._ReadOnlyForeColor = value;
        }

        /// <summary> The read write back color. </summary>
        private Color _ReadWriteBackColor;

        /// <summary> Gets or sets the color of the read write back. </summary>
        /// <value> The color of the read write back. </value>
        [DefaultValue( typeof( Color ), "SystemColors.Window" )]
        [Description( "Back color when control is read/write" )]
        [Category( "Appearance" )]
        public Color ReadWriteBackColor
        {
            get {
                if ( this._ReadWriteBackColor.IsEmpty )
                {
                    this._ReadWriteBackColor = SystemColors.Window;
                }

                return this._ReadWriteBackColor;
            }

            set => this._ReadWriteBackColor = value;
        }

        /// <summary> The read write foreground color. </summary>
        private Color _ReadWriteForeColor;

        /// <summary> Gets or sets the color of the read write foreground. </summary>
        /// <value> The color of the read write foreground. </value>
        [DefaultValue( typeof( Color ), "System.Drawing.SystemColors.ControlText" )]
        [Description( "Fore color when control is read/write" )]
        [Category( "Appearance" )]
        public Color ReadWriteForeColor
        {
            get {
                if ( this._ReadWriteForeColor.IsEmpty )
                {
                    this._ReadWriteForeColor = SystemColors.ControlText;
                }

                return this._ReadWriteForeColor;
            }

            set => this._ReadWriteForeColor = value;
        }

        /// <summary> Gets or sets a value indicating whether the text can be changed by the use of the up
        /// or down buttons only. </summary>
        /// <value> <c>True</c> if [read only]; otherwise, <c>False</c>. </value>
        public new bool ReadOnly
        {
            get => base.ReadOnly;

            set {
                base.ReadOnly = value;
                this.UpDownButtons.Enabled = !value;
                if ( value )
                {
                    this.BackColor = this.ReadOnlyBackColor;
                    this.ForeColor = this.ReadOnlyForeColor;
                }
                else
                {
                    this.BackColor = this.ReadWriteBackColor;
                    this.ForeColor = this.ReadWriteForeColor;
                }
            }
        }

        #endregion

        #region " NEW PROPERTIES "

        /// <summary> Gets or sets the automatic select. </summary>
        /// <value> The automatic select. </value>
        [DefaultValue( false )]
        [Category( "Behavior" )]
        [Description( "Automatically select control text when it receives focus." )]
        public bool AutoSelect { get; set; }

        /// <summary> Gets or sets the intercept mouse wheel. </summary>
        /// <value> The intercept mouse wheel. </value>
        [DefaultValue( typeof( InterceptMouseWheelMode ), "Always" )]
        [Category( "Behavior" )]
        [Description( "Enables MouseWheel only under certain conditions." )]
        public InterceptMouseWheelMode InterceptMouseWheel { get; set; }

        /// <summary> The show up down buttons. </summary>
        private UpDownButtonsDisplayMode _UpDownDisplayMode;

        /// <summary> Gets or sets the mode for displaying the up down buttons. </summary>
        /// <value> The show up down buttons. </value>
        [DefaultValue( typeof( UpDownButtonsDisplayMode ), "Always" )]
        [Category( "Behavior" )]
        [Description( "Set Up/Down Buttons visibility mode." )]
        public UpDownButtonsDisplayMode UpDownDisplayMode
        {
            get => this._UpDownDisplayMode;

            set {
                this._UpDownDisplayMode = value;
                this.UpdateUpDownButtonsVisibility();
            }
        }

        /// <summary> Gets the sentinel indication the up down buttons display mode is visible. </summary>
        /// <value> The sentinel indication the up down buttons display mode is visible. </value>
        protected bool ShowUpDownButtons => this.UpDownDisplayMode == UpDownButtonsDisplayMode.WhenFocus || this._UpDownDisplayMode == UpDownButtonsDisplayMode.WhenFocusOrMouseOver;

        /// <summary>
        /// If set, incrementing value will cause it to restart from Minimum when Maximum is reached (and
        /// vice versa).
        /// </summary>
        /// <value> The wrap value. </value>
        [DefaultValue( false )]
        [Category( "Behavior" )]
        [Description( "If set, incrementing value will cause it to restart from Minimum when Maximum is reached (and vice versa)." )]
        public bool WrapValue { get; set; }

        #endregion

        #region " UP DOWN BUTTONS VISIBILITY MANAGEMENT "

        /// <summary> Tracks the mouse position. <c>True</c> if mouse is over. </summary>
        private bool _MouseOver;

        /// <summary> Tracks the focus. <c>True</c> if control has focus. </summary>
        private bool _HasFocus;

        /// <summary>
        /// Show or hide the UpDownButtons, according to ShowUpDownButtons property value.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        protected void UpdateUpDownButtonsVisibility()
        {

            // test new state
            bool newVisible;
            switch ( this._UpDownDisplayMode )
            {
                case UpDownButtonsDisplayMode.WhenMouseOver:
                    {
                        newVisible = this._MouseOver;
                        break;
                    }

                case UpDownButtonsDisplayMode.WhenFocus:
                    {
                        newVisible = this._HasFocus;
                        break;
                    }

                case UpDownButtonsDisplayMode.WhenFocusOrMouseOver:
                    {
                        newVisible = this._HasFocus || this._MouseOver;
                        break;
                    }

                case UpDownButtonsDisplayMode.Never:
                    {
                        newVisible = false;
                        break;
                    }

                default:
                    {
                        newVisible = true;
                        break;
                    }
            }


            // assign only if needed
            if ( this.UpDownButtons.Visible != newVisible )
            {
                if ( newVisible )
                {
                    if ( !this.ReadOnly )
                    {
                        this.TextBox.Width = this.ClientRectangle.Width - this.UpDownButtons.Width;
                        this.UpDownButtons.Visible = newVisible;
                        this.OnTextBoxResize( this.TextBox, EventArgs.Empty );
                        this.Invalidate();
                    }
                }
                else
                {
                    this.TextBox.Width = this.ClientRectangle.Width;
                    this.UpDownButtons.Visible = newVisible;
                    this.OnTextBoxResize( this.TextBox, EventArgs.Empty );
                    this.Invalidate();
                }
            }
        }

        /// <summary> Custom text box size management. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="source"> The source of the event. </param>
        /// <param name="e">      An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnTextBoxResize( object source, EventArgs e )
        {
            if ( this.TextBox is null )
            {
                return;
            }

            if ( this._UpDownDisplayMode == UpDownButtonsDisplayMode.Always )
            {
                // standard management
                base.OnTextBoxResize( source, e );
            }
            else
            {
                // custom management

                // change position if Right to left
                bool fixPos = this.RightToLeft == RightToLeft.Yes ^ this.UpDownAlign == LeftRightAlignment.Left;
                if ( this._MouseOver )
                {
                    if ( !this.ReadOnly )
                    {
                        this.TextBox.Width = this.ClientSize.Width - this.TextBox.Left - this.UpDownButtons.Width - 2;
                        if ( fixPos )
                        {
                            this.TextBox.Location = new Point( 16, this.TextBox.Location.Y );
                        }
                    }
                }
                else
                {
                    if ( fixPos )
                    {
                        this.TextBox.Location = new Point( 2, this.TextBox.Location.Y );
                    }

                    this.TextBox.Width = this.ClientSize.Width - this.TextBox.Left - 2;
                }
            }
        }

        #endregion

        #region " FIXED EVENTS "

        /// <summary> Event queue for all listeners interested in MouseEnter events. </summary>
        /// <remarks> Raised correctly when mouse enters the text box. </remarks>
        public new event EventHandler<EventArgs> MouseEnter;

        /// <summary> Removes mouse enter event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The value. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void RemoveMouseEnterEvent( EventHandler<EventArgs> value )
        {
            foreach ( Delegate d in value is null ? (Array.Empty<Delegate>()) : value.GetInvocationList() )
            {
                try
                {
                    MouseEnter -= ( EventHandler<EventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }
        }

        /// <summary> Event queue for all listeners interested in MouseLeave events. </summary>
        /// <remarks> Raised correctly when mouse leaves the text box. </remarks>
        public new event EventHandler<EventArgs> MouseLeave;

        /// <summary> Removes mouse leave event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The value. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void RemoveMouseLeaveEvent( EventHandler<EventArgs> value )
        {
            foreach ( Delegate d in value is null ? (Array.Empty<Delegate>()) : value.GetInvocationList() )
            {
                try
                {
                    MouseLeave -= ( EventHandler<EventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }
        }

        /// <summary> Event handler. Called by  for mouse enter leave events. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void MouseEnterLeaveThis( object sender, EventArgs e )
        {
            var cr = this.RectangleToScreen( this.ClientRectangle );
            var mp = MousePosition;

            // actual state
            bool isOver = cr.Contains( mp );

            // test if status changed
            if ( this._MouseOver ^ isOver )
            {
                // update state
                this._MouseOver = isOver;
                if ( this._MouseOver )
                {
                    var evt = MouseEnter;
                    evt?.Invoke( this, e );
                }
                else
                {
                    var evt = MouseLeave;
                    evt?.Invoke( this, e );
                }
            }

            // update UpDownButtons visibility
            if ( this._UpDownDisplayMode != UpDownButtonsDisplayMode.Always )
            {
                this.UpdateUpDownButtonsVisibility();
            }
        }

        #endregion

        #region " NEW EVENTS "

        /// <summary> Event queue for all listeners interested in ValueDecrementing events. </summary>
        /// <remarks> Raised BEFORE value decrements. </remarks>
        public event EventHandler<CancelEventArgs> ValueDecrementing;

        /// <summary> Removes event handler. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The handler. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void RemoveValueDecrementingEventHandler( EventHandler<CancelEventArgs> value )
        {
            foreach ( Delegate d in value is null ? (Array.Empty<Delegate>()) : value.GetInvocationList() )
            {
                try
                {
                    ValueDecrementing -= ( EventHandler<CancelEventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }
        }

        /// <summary> Event queue for all listeners interested in ValueIncrementing events. </summary>
        /// <remarks> Raised BEFORE value increments. </remarks>
        public event EventHandler<CancelEventArgs> ValueIncrementing;

        /// <summary> Removes event handler. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The handler. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void RemoveValueIncrementingEventHandler( EventHandler<CancelEventArgs> value )
        {
            foreach ( Delegate d in value is null ? (Array.Empty<Delegate>()) : value.GetInvocationList() )
            {
                try
                {
                    ValueIncrementing -= ( EventHandler<CancelEventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }
        }

        /// <summary>
        /// Decrements the value of the spin box (also known as an up-down control). Raises the new
        /// <see cref="ValueDecrementing">event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public override void DownButton()
        {
            if ( this.ReadOnly )
            {
                return;
            }

            var e = new CancelEventArgs();
            var evt = ValueDecrementing;
            evt?.Invoke( this, e );
            if ( e.Cancel )
            {
                return;
            }
            // decrement with wrap
            if ( this.WrapValue && this.Value - this.Increment < this.Minimum )
            {
                this.Value = this.Maximum;
            }
            else
            {
                base.DownButton();
            }
        }

        /// <summary>
        /// Increments the value of the spin box (also known as an up-down control). Raises the new
        /// <see cref="ValueIncrementing">event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public override void UpButton()
        {
            if ( this.ReadOnly )
            {
                return;
            }

            var e = new CancelEventArgs();
            var evt = ValueIncrementing;
            evt?.Invoke( this, e );
            if ( e.Cancel )
            {
                return;
            }
            // increment with wrap
            if ( this.WrapValue && this.Value + this.Increment > this.Maximum )
            {
                this.Value = this.Minimum;
            }
            else
            {
                base.UpButton();
            }
        }

        #endregion

        #region " VALUE "

        /// <summary> Query if this object has value. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> <c>true</c> if value; otherwise <c>false</c> </returns>
        public bool HasValue()
        {
            return !string.IsNullOrWhiteSpace( this.Text );
        }

        /// <summary> Gets or sets the value. </summary>
        /// <value> The value. </value>
        [DefaultValue( "" )]
        [Description( "Value" )]
        [Category( "Appearance" )]
        public new decimal Value
        {
            get => base.Value;

            set {
                base.Value = Math.Max( this.Minimum, Math.Min( this.Maximum, value ) );
                this.Sync();
            }
        }

        /// <summary> Gets or sets the null value. </summary>
        /// <value> The null value. </value>
        public decimal? NullValue
        {
            get => this.HasValue() ? this.Value : new decimal?();

            set {
                if ( value.HasValue )
                {
                    this.Value = value.Value;
                }
                else
                {
                    this.Text = string.Empty;
                }
            }
        }

        /// <summary>
        /// Determines whether the specified value is within the minimum/maximum range.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns>
        /// <c>True</c> if [is in range] [the specified value]; otherwise, <c>False</c>.
        /// </returns>
        public bool IsInRange( decimal value )
        {
            return value >= this.Minimum && value <= this.Maximum;
        }

        #endregion

        #region " RANGE "

        /// <summary> Gets or sets the range. </summary>
        /// <value> The range. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public Primitives.RangeD Range
        {
            get => new( this.Minimum, this.Maximum );

            set {
                if ( value is object && value != this.Range )
                {
                    if ( this.Value > value.Max )
                    {
                        this.Value = value.Max;
                    }

                    if ( this.Value < value.Min )
                    {
                        this.Value = value.Min;
                    }

                    this.Minimum = value.Min;
                    this.Maximum = value.Max;
                }
            }
        }

        #endregion

    }

    /// <summary> Values that represent Up Down Buttons Display Mode. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public enum UpDownButtonsDisplayMode
    {

        /// <summary> Up-down buttons are always visible (default behavior). </summary>
        [Description( "Up-down buttons are always visible (default behavior). " )]
        Always,

        /// <summary> Up-down buttons are visible only when mouse is over the control. </summary>
        [Description( "Up-down buttons are visible only when mouse is over the control." )]
        WhenMouseOver,

        /// <summary> Up-down buttons are visible only when control has the focus. </summary>
        [Description( "Up-down buttons are visible only when control has the focus." )]
        WhenFocus,

        /// <summary> Up-down buttons are visible when control has focus or mouse is over the control. </summary>
        [Description( "Up-down buttons are visible when control has focus or mouse is over the control." )]
        WhenFocusOrMouseOver,

        /// <summary>UpDownButtons are never visible</summary>
        Never,

    }

    /// <summary> Values that represent Intercept Mouse Wheel Mode. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public enum InterceptMouseWheelMode
    {

        /// <summary> Mouse Wheel always works (default behavior). </summary>
        [Description( "Mouse Wheel always works (default behavior)" )]
        Always,

        /// <summary> Mouse Wheel works only when mouse is over the (focused) control. </summary>
        [Description( "Mouse Wheel works only when mouse is over the (focused) control" )]
        WhenMouseOver,

        /// <summary> Mouse Wheel never works. </summary>
        [Description( "Mouse Wheel never works" )]
        Never
    }
}
