using System;
using System.ComponentModel;
using System.Drawing.Printing;
using System.Runtime.InteropServices;

namespace isr.Core.Controls
{

    /// <summary>
    /// An extension to <see cref="System.Windows.Forms.RichTextBox">windows rich text box</see>
    /// suitable for printing.
    /// </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-12-02 </para><para>
    /// David, 2015-01-08. </para>
    /// </remarks>
    [DesignerCategory( "code" )]
    [Description( "Extended Rich Text Box" )]
    public class RichTextBox : System.Windows.Forms.RichTextBox
    {

        #region " STRUCTURES AND IMPORTS "

        /// <summary> A structure rectangle. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        [StructLayout( LayoutKind.Sequential )]
        private struct STRUCT_RECT
        {

            /// <summary> Gets or sets the left. </summary>
            /// <value> The left. </value>
            public int Left { get; set; }

            /// <summary> Gets or sets the top. </summary>
            /// <value> The top. </value>
            public int Top { get; set; }

            /// <summary> Gets or sets the right. </summary>
            /// <value> The right. </value>
            public int Right { get; set; }

            /// <summary> Gets or sets the bottom. </summary>
            /// <value> The bottom. </value>
            public int Bottom { get; set; }
        }

        /// <summary> A structure charrange. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        [StructLayout( LayoutKind.Sequential )]
        private struct STRUCT_CHARRANGE
        {

            /// <summary> Gets or sets the cp minimum. </summary>
            /// <value> The cp minimum. </value>
            public int CpMin { get; set; }

            /// <summary> Gets or sets the cp maximum. </summary>
            /// <value> The cp maximum. </value>
            public int CpMax { get; set; }
        }

        /// <summary> A structure formatrange. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        [StructLayout( LayoutKind.Sequential )]
        private struct STRUCT_FORMATRANGE
        {

            /// <summary> Gets or sets the hdc. </summary>
            /// <value> The hdc. </value>
            public IntPtr Hdc { get; set; }

            /// <summary> Gets or sets the hdc target. </summary>
            /// <value> The hdc target. </value>
            public IntPtr HdcTarget { get; set; }

            /// <summary> Gets or sets the rectangle. </summary>
            /// <value> The rectangle. </value>
            public STRUCT_RECT Rc { get; set; }

            /// <summary> Gets or sets the rectangle page. </summary>
            /// <value> The rectangle page. </value>
            public STRUCT_RECT RcPage { get; set; }

            /// <summary> Gets or sets the chrg. </summary>
            /// <value> The chrg. </value>
            public STRUCT_CHARRANGE Chrg { get; set; }
        }

        /// <summary> A structure charformat. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        [StructLayout( LayoutKind.Sequential )]
        private struct STRUCT_CHARFORMAT
        {

            /// <summary> Gets or sets the size. </summary>
            /// <value> The cb size. </value>
            public int CbSize { get; set; }

            /// <summary> Gets or sets the mask. </summary>
            /// <value> The double-word mask. </value>
            public uint DwMask { get; set; }

            /// <summary> Gets or sets the effects. </summary>
            /// <value> The double-word effects. </value>
            public uint DwEffects { get; set; }

            /// <summary> Gets or sets the height. </summary>
            /// <value> The y coordinate height. </value>
            public int YHeight { get; set; }

            /// <summary> Gets or sets the offset. </summary>
            /// <value> The y coordinate offset. </value>
            public int YOffset { get; set; }

            /// <summary> Gets or sets the color of the carriage return text. </summary>
            /// <value> The color of the carriage return text. </value>
            public int CrTextColor { get; set; }

            /// <summary> Gets or sets the set the character belongs to. </summary>
            /// <value> The b character set. </value>
            public byte BCharSet { get; set; }

            /// <summary> Gets or sets the pitch and family. </summary>
            /// <value> The b pitch and family. </value>
            public byte BPitchAndFamily { get; set; }
            /// <summary> Name of the face. </summary>
            [MarshalAs( UnmanagedType.ByValArray, SizeConst = 32 )]
            [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "<Pending>" )]
            public char[] SzFaceName;
        }

        /// <summary> A safe native methods. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private sealed class SafeNativeMethods
        {

            /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
            /// <remarks> David, 2020-09-24. </remarks>
            private SafeNativeMethods() : base()
            {
            }

            /// <summary> Sends a message. </summary>
            /// <remarks> David, 2020-09-24. </remarks>
            /// <param name="hWnd">   The window. </param>
            /// <param name="msg">    The message. </param>
            /// <param name="wParam"> The parameter. </param>
            /// <param name="lParam"> The parameter. </param>
            /// <returns> An Int32. </returns>
            [DllImport( "user32.dll" )]
            internal static extern IntPtr SendMessage( IntPtr hWnd, uint msg, UIntPtr wParam, IntPtr lParam );
        }

        private const int _WM_USER = 0x400;
        private const int _EM_FORMATRANGE = _WM_USER + 57;

        // Private Constant _EM_GETCHARFORMAT As Int32 = _WM_USER + 58

        private const int _EM_SETCHARFORMAT = _WM_USER + 68;
        private readonly UIntPtr _SCF_SELECTION = new( 0x1U );

        // not used Private SCF_WORD As Int32 = &H2
        // not used Private SCF_ALL As Int32 = &H4

        // Defines for STRUCT_CHARFORMAT member dwMask
        // (Long because UInt32 is not an intrinsic type)
        private const long _CFM_BOLD = 0x1L;
        private const long _CFM_ITALIC = 0x2;
        private const long _CFM_UNDERLINE = 0x4;

        // Private Constant CFM_STRIKEOUT As Long = &H8
        // Private Constant CFM_PROTECTED As Long = &H10
        // Private Constant CFM_LINK As Long = &H20
        private const long _CFM_SIZE = 0x80000000L;

        // Private Constant CFM_COLOR As Long = &H40000000
        private const long _CFM_FACE = 0x20000000L;

        // Private Constant CFM_OFFSET As Long = &H10000000
        // Private Constant CFM_CHARSET As Long = &H8000000

        // Defines for STRUCT_CHARFORMAT member dwEffects
        private const long _CFE_BOLD = 0x1;

        /// <summary> The cfe italic. </summary>
        private const long _CFE_ITALIC = 0x2;
        private const long _CFE_UNDERLINE = 0x4;
        // Private Constant CFE_STRIKEOUT As Long = &H8
        // Private Constant CFE_PROTECTED As Long = &H10
        // Private Constant CFE_LINK As Long = &H20
        // Private Constant CFE_AUTOCOLOR As Long = &H40000000

        #endregion

        #region " EXTENDED FUNCTIONS "

        /// <summary>
        /// Calculate or render the contents of the <see cref="RichTextBox">rich text box</see> for
        /// printing Parameter.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="measureOnly"> If true, only the calculation is performed;
        /// otherwise the text is rendered as well. </param>
        /// <param name="e">           The PrintPageEventArgs object from the PrintPage event. </param>
        /// <param name="charFrom">    Index of first character to be printed. </param>
        /// <param name="charTo">      Index of last character to be printed. </param>
        /// <returns> (Index of last character that fitted on the page) + 1. </returns>
        public int FormatRange( bool measureOnly, PrintPageEventArgs e, int charFrom, int charTo )
        {
            if ( e is null )
            {
                return 0;
            }

            // Specify which characters to print
            var cr = default( STRUCT_CHARRANGE );
            cr.CpMin = charFrom;
            cr.CpMax = charTo;

            // Specify the area inside page margins
            var rc = default( STRUCT_RECT );
            rc.Top = HundredthInchToTwips( e.MarginBounds.Top );
            rc.Bottom = HundredthInchToTwips( e.MarginBounds.Bottom );
            rc.Left = HundredthInchToTwips( e.MarginBounds.Left );
            rc.Right = HundredthInchToTwips( e.MarginBounds.Right );

            // Specify the page area
            var rcPage = default( STRUCT_RECT );
            rcPage.Top = HundredthInchToTwips( e.PageBounds.Top );
            rcPage.Bottom = HundredthInchToTwips( e.PageBounds.Bottom );
            rcPage.Left = HundredthInchToTwips( e.PageBounds.Left );
            rcPage.Right = HundredthInchToTwips( e.PageBounds.Right );

            // Get device context of output device
            IntPtr hdc;
            hdc = e.Graphics.GetHdc();

            // Fill in the FORMATRANGE structure
            var fr = default( STRUCT_FORMATRANGE );
            fr.Chrg = cr;
            fr.Hdc = hdc;
            fr.HdcTarget = hdc;
            fr.Rc = rc;
            fr.RcPage = rcPage;

            // Non-Zero wParam means render, Zero means measure
            var wParam = measureOnly ? UIntPtr.Zero : new UIntPtr( 1U );

            // Allocate memory for the FORMATRANGE structure and
            // copy the contents of our structure to this memory
            IntPtr lParam;
            lParam = Marshal.AllocCoTaskMem( Marshal.SizeOf( fr ) );
            Marshal.StructureToPtr( fr, lParam, false );

            // Send the actual Win32 message
            IntPtr res;
            res = SafeNativeMethods.SendMessage( this.Handle, _EM_FORMATRANGE, wParam, lParam );

            // Free allocated memory
            Marshal.FreeCoTaskMem( lParam );

            // and release the device context
            e.Graphics.ReleaseHdc( hdc );
            return ( int ) res;
        }

        /// <summary>
        /// Converts between 1/100 inch (unit used by the .NET framework)
        /// and twips (1/1440 inch, used by Win32 API calls).
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="n"> Value in 1/100 inch Return value: Value in twips. </param>
        /// <returns> An Int32. </returns>
        private static int HundredthInchToTwips( int n )
        {
            return Convert.ToInt32( n * 14.4d );
        }

        /// <summary> Frees cached data from rich edit control after printing. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> The formatted range done. </returns>
        public int FormatRangeDone()
        {
            return ( int ) SafeNativeMethods.SendMessage( this.Handle, _EM_FORMATRANGE, UIntPtr.Zero, IntPtr.Zero );
        }

        /// <summary>
        /// Sets the font only for the selected part of the rich text box without modifying the other
        /// properties like size or style.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="face"> Name of the font to use. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool SetSelectionFont( string face )
        {
            if ( string.IsNullOrEmpty( face ) )
            {
                return false;
            }

            var cf = new STRUCT_CHARFORMAT();
            cf.CbSize = Marshal.SizeOf( cf );
            cf.DwMask = Convert.ToUInt32( _CFM_FACE );

            // ReDim face name to relevant size
            cf.SzFaceName = new char[33];
            face.CopyTo( 0, cf.SzFaceName, 0, Math.Min( 31, face.Length ) );
            IntPtr lParam;
            lParam = Marshal.AllocCoTaskMem( Marshal.SizeOf( cf ) );
            Marshal.StructureToPtr( cf, lParam, false );
            IntPtr res;
            res = SafeNativeMethods.SendMessage( this.Handle, _EM_SETCHARFORMAT, this._SCF_SELECTION, lParam );
            return res == IntPtr.Zero;
        }

        /// <summary>
        /// Sets the font size only for the selected part of the rich text box without modifying the
        /// other properties like font or style.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="size"> new point size to use. </param>
        /// <returns> true on success, false on failure. </returns>
        public bool SetSelectionSize( int size )
        {
            var cf = new STRUCT_CHARFORMAT();
            cf.CbSize = Marshal.SizeOf( cf );
            cf.DwMask = Convert.ToUInt32( _CFM_SIZE );
            // yHeight is in 1/20 pt
            cf.YHeight = size * 20;
            IntPtr lParam;
            lParam = Marshal.AllocCoTaskMem( Marshal.SizeOf( cf ) );
            Marshal.StructureToPtr( cf, lParam, false );
            IntPtr res;
            res = SafeNativeMethods.SendMessage( this.Handle, _EM_SETCHARFORMAT, this._SCF_SELECTION, lParam );
            return res == IntPtr.Zero;
        }

        /// <summary>
        /// Sets the bold style only for the selected part of the rich text box without modifying the
        /// other properties like font or size.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="bold"> make selection bold (true) or regular (false) </param>
        /// <returns> true on success, false on failure. </returns>
        public bool SetSelectionBold( bool bold )
        {
            return bold ? this.SetSelectionStyle( ( int ) _CFM_BOLD, ( int ) _CFE_BOLD ) : this.SetSelectionStyle( ( int ) _CFM_BOLD, 0 );
        }

        /// <summary>
        /// Sets the italic style only for the selected part of the rich text box without modifying the
        /// other properties like font or size.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="italic"> make selection italic (true) or regular (false) </param>
        /// <returns> true on success, false on failure. </returns>
        public bool SetSelectionItalic( bool italic )
        {
            return italic ? this.SetSelectionStyle( ( int ) _CFM_ITALIC, ( int ) _CFE_ITALIC ) : this.SetSelectionStyle( ( int ) _CFM_ITALIC, 0 );
        }

        /// <summary>
        /// Sets the underlined style only for the selected part of the rich text box ' without modifying
        /// the other properties like font or size.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="underlined"> make selection underlined (true) or regular (false) </param>
        /// <returns> true on success, false on failure. </returns>
        public bool SetSelectionUnderlined( bool underlined )
        {
            return underlined ? this.SetSelectionStyle( ( int ) _CFM_UNDERLINE, ( int ) _CFE_UNDERLINE ) : this.SetSelectionStyle( ( int ) _CFM_UNDERLINE, 0 );
        }

        /// <summary>
        /// Set the style only for the selected part of the rich text box with the possibility to mask
        /// out some styles that are not to be modified.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="mask">   modify which styles? </param>
        /// <param name="effect"> new values for the styles. </param>
        /// <returns> true on success, false on failure. </returns>
        private bool SetSelectionStyle( int mask, int effect )
        {
            var cf = new STRUCT_CHARFORMAT();
            cf.CbSize = Marshal.SizeOf( cf );
            cf.DwMask = Convert.ToUInt32( mask );
            cf.DwEffects = Convert.ToUInt32( effect );
            IntPtr lParam;
            lParam = Marshal.AllocCoTaskMem( Marshal.SizeOf( cf ) );
            Marshal.StructureToPtr( cf, lParam, false );
            IntPtr res;
            res = SafeNativeMethods.SendMessage( this.Handle, _EM_SETCHARFORMAT, this._SCF_SELECTION, lParam );
            return res == IntPtr.Zero;
        }
        #endregion

    }
}
