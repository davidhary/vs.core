using System.Drawing;
using System.Windows.Forms;

namespace isr.Core.Controls
{

    /// <summary> Pop-up container. </summary>
    /// <remarks>
    /// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-10-06, 2.1.5392. </para>
    /// </remarks>
    public class PopupContainer : ToolStripDropDown
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public PopupContainer() : base()
        {
            this.BackColor = Color.Transparent;
            this.Margin = Padding.Empty;
            this.Padding = Padding.Empty;
        }

        /// <summary> Gets or sets the host. </summary>
        private readonly ToolStripControlHost _Host;

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="containedControl"> The contained control. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public PopupContainer( Control containedControl ) : this()
        {
            this._Host = null;
            try
            {
                this._Host = new ToolStripControlHost( containedControl );
            }
            catch
            {
                this._Host?.Dispose();
                this._Host = null;
                throw;
            }

            try
            {
                // no way to check if the control supports transparency
                this._Host.BackColor = Color.Transparent;
            }
            catch
            {
            }

            this._Host.Margin = Padding.Empty;
            this._Host.Padding = Padding.Empty;
            _ = this.Items.Add( this._Host );
        }

        /// <summary>
        /// Releases the unmanaged resources used by the
        /// <see cref="T:System.Windows.Forms.ToolStripDropDown" /> and optionally releases the managed
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    // this causes stack overflow.
                    // If Me._host IsNot Nothing Me._host.Dispose(): Me._host = Nothing
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        /// <summary>
        /// Positions the System.Windows.Forms.ToolStripDropDown relative to the specified control
        /// location.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="referencePointControl"> The control (typically, a
        /// System.Windows.Forms.ToolStripDropDownButton) that is the
        /// reference point for the
        /// System.Windows.Forms.ToolStripDropDown position. </param>
        /// <param name="position">              The horizontal and vertical location of the reference
        /// control's upper-left corner, in pixels. </param>
        /// <param name="size">                  The maximum size of the drop-down. </param>
        public void Show( Control referencePointControl, Point position, Size size )
        {
            this.Show( referencePointControl, position );
            this.Size = size;
        }

        /// <summary> Shows the information. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="referencePointControl"> The control (typically, a
        /// System.Windows.Forms.ToolStripDropDownButton) that is the
        /// reference point for the
        /// System.Windows.Forms.ToolStripDropDown position. </param>
        /// <param name="info">                  The information. </param>
        /// <param name="position">              The horizontal and vertical location of the reference
        /// control's upper-left corner, in pixels. </param>
        /// <param name="size">                  The maximum size of the drop-down. </param>
        public static void PopupInfo( Control referencePointControl, string info, Point position, Size size )
        {
            var containedControl = new Label() { Size = size, Text = info };
            var Popup = new PopupContainer( containedControl );
            Popup.Show( referencePointControl, position );
            Popup.Size = size;
        }
    }
}
