using System;
using System.ComponentModel;

namespace isr.Core.Controls
{

    /// <summary> Drop down text box. </summary>
    /// <remarks>
    /// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-12-06 </para>
    /// </remarks>
    public partial class DropDownTextBox : Forma.ModelViewBase
    {

        /// <summary>
        /// A private constructor for this class making it not publicly creatable. This ensure using the
        /// class as a singleton.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public DropDownTextBox()
        {

            // This call is required by the designer.
            this.InitializeComponent();

            // Add any initialization after the InitializeComponent() call.
            this.InitialHeight = this.Height;
            if ( this.DropDownHeight <= this.InitialHeight )
            {
                this.DropDownHeight = 300;
            }

            this.__DropDownToggle.Name = "_dropDownToggle";
        }

        /// <summary>
        /// Releases the unmanaged resources used by the isr.Core.Controls.DropDownTextBox and optionally
        /// releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        /// <summary> Gets the text box. </summary>
        /// <value> The text box. </value>
        public TextBox TextBox => this._TextBox;

        /// <summary> Toggle multi line. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="turnOn"> true to enable, false to disable the turn. </param>
        private void ToggleMultiLine( bool turnOn )
        {
            if ( this._TextBox is object )
            {
                if ( turnOn && !this._TextBox.Multiline )
                {
                    if ( this.DropDownHeight > this.InitialHeight )
                    {
                        this._TextBox.Multiline = true;
                        this.Height = this.DropDownHeight;
                    }
                }
                else if ( !turnOn && this._TextBox.Multiline )
                {
                    this._TextBox.Multiline = false;
                    this.Height = this.InitialHeight;
                }
            }
        }

        /// <summary> Drop down toggle checked changed. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void DropDownToggle_CheckedChanged( object sender, EventArgs e )
        {
            if ( sender is System.Windows.Forms.CheckBox checkBox )
            {
                this.ToggleMultiLine( checkBox.Checked );
                if ( this._TextBox is object )
                {
                    checkBox.Image = this._TextBox.Multiline ? My.Resources.Resources.go_up_8 : My.Resources.Resources.go_down_8;
                }
            }
        }

        /// <summary> Drop Down Height. </summary>
        /// <value> The height of the drop down. </value>
        [DefaultValue( 300 )]
        [Description( "Drop Down Height" )]
        [Category( "Appearance" )]
        public int DropDownHeight { get; set; }

        /// <summary> Gets or sets the height of the initial. </summary>
        /// <value> The height of the initial. </value>
        private int InitialHeight { get; set; }
    }
}
