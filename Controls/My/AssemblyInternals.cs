[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "isr.VI.DeviceTests,PublicKey=" + isr.Core.Controls.SolutionInfo.PublicKey )]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "isr.VI.Facade,PublicKey=" + isr.Core.Controls.SolutionInfo.PublicKey )]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "isr.VI.FacadeTests,PublicKey=" + isr.Core.Controls.SolutionInfo.PublicKey )]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "isr.VI.E4990Tests,PublicKey=" + isr.Core.Controls.SolutionInfo.PublicKey )]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "isr.VI.K2002Tests,PublicKey=" + isr.Core.Controls.SolutionInfo.PublicKey )]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "isr.VI.K3700Tests,PublicKey=" + isr.Core.Controls.SolutionInfo.PublicKey )]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "isr.VI.K2450Tests,PublicKey=" + isr.Core.Controls.SolutionInfo.PublicKey )]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "isr.VI.K3458Tests,PublicKey=" + isr.Core.Controls.SolutionInfo.PublicKey )]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "isr.VI.K7500Tests,PublicKey=" + isr.Core.Controls.SolutionInfo.PublicKey )]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "isr.VI.Tsp2.K7500Tests,PublicKey=" + isr.Core.Controls.SolutionInfo.PublicKey )]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "isr.OhmniTests,PublicKey=" + isr.Core.Controls.SolutionInfo.PublicKey )]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "isr.Ohmni.CincoTests,PublicKey=" + isr.Core.Controls.SolutionInfo.PublicKey )]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "isr.Ohmni.MorpheTests,PublicKey=" + isr.Core.Controls.SolutionInfo.PublicKey )]


namespace isr.Core.Controls
{

    /// <summary> Information about this and related projects in this solution. </summary>
    /// <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para> </remarks>
    internal static class SolutionInfo
    {
        /// <summary> The public key. </summary>
        internal const string PublicKey = "0024000004800000940000000602000000240000525341310004000001000100dba29c0e6dba927c84e79ce36609bb871da7c63c8ef88addafa51ccc967604e03c21afeb0cb5f5e74028e1f151bf92e9fe73166bfab3a61c4db81e8dad98c020f06368cce665735e9a2e42741b80ea60a3dcc6b4ad212928baf132b99c0ce9a2d0eae0b14d4fb2b7d2e4d5eba898b61e1e69c1d47db03a92eb9324137084bffb";
    }
}

