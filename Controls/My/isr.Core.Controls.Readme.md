## ISR Core Controls<sub>&trade;</sub>: Core Controls Class Library
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Revision History [](#){name=Revision-History}

*5.0.7445 2020-05-20*  
Relaxes selector name validations to allow opening resources 
not registered with a resource manager.

*5.0.7280 2019-12-07*  
Uses now Services, Model and Constructs created from
agnostic.

*3.4.6944 2019-01-05*  
Adds binding management to base controls.

*3.2.6421 2017-07-31*  
Adds read only functionality to the toggle image.

*3.2.6414 2017-07-24*  
Adds image toggle. Render disabled state for the image
buttons.

*3.1.6120 2016-10-03*  
Adds custom progress with text and percent value
display.

*3.0.6034 2016-07-09*  
Adds the CP Wizard controls. Adds the console control.

*3.0.5906 2016-03-03*  
Adds a custom progress bar with text overlay.

*3.0.5866 2016-01-23*  
Updates to .NET 4.6.1

*2.1.5747 2015-09-26 Adds custom tab control.

*2.1.5733 2015-09-12 Adds toggle switch control.

*2.1.5661 2015-07-02 Updates images and icons of drop down text box and
info provider.

*2.1.5600 2015-05-02 Adds Cool Print Preview dialog.

*2.1.5593 2015-04-25 Adds extended tab control with hidden tab headers.

*2.1.5572 2015-04-03 Adds Info Provider.

*2.1.5538 2015-03-01 Adds Selector combo box control.

*2.1.5399 2014-10-14*  
Adds shape control.

*2.1.5394 2014-10-08*  
Adds logon control.

*2.1.5288 2014-06-24*  
Nullable Up Down: Force the nullable value text when
the control text is not set.

*2.1.5253 2014-05-20*  
Engineering numeric up down treats percent units as a
special case253

*2.1.5239 2014-05-06*  
Access the numeric up down control collections when
addressing the text box and up down buttons.

*2.1.5219 2014-04-16*  
Adds tool strip numeric up down control.

*2.1.5213 2014-04-10*  
Fixes and simplifies the nullable numeric up down
control. Adds tester for the control.

*2.1.5207 2014-04-05*  
Adds engineering up down control. Adds engineering
scaling to numeric up down control.

*2.1.5163 2014-02-19*  
Uses local sync and async safe event handlers.

*2.0.5138 2014-01-25*  
Uses system font for forms and controls.

*2.0.5126 2014-01-13*  
Tagged as 2014.

*2.0.5065 2013-11-13*  
Fixes state aware timer dispose to prevent stack
overflow. Adds safe setters for three state check box.

*2.0.5064 2013-11-12*  
Adds file selector user control.

*2.0.5018 2013-09-27*  
Created based on the legacy Controls structures.

\(C\) 2007 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Core Controls Library](https://bitbucket.org/davidhary/vs.core)
