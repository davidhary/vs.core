using System.Security;
using System.Security.Permissions;

namespace isr.Core.Controls
{

    /// <summary> An unsafe native methods. </summary>
    /// <remarks>
    /// Potentially dangerous P/Invoke method declarations; be careful not to expose these publicly. <para>
    /// (c) 2002 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    [SecurityPermission( SecurityAction.Assert, Flags = SecurityPermissionFlag.UnmanagedCode )]
    [SuppressUnmanagedCodeSecurity]
    internal sealed partial class NativeMethods
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private NativeMethods()
        {
        }
        #endregion

    }
}
