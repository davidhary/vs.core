using System;

namespace isr.Core.Controls.ExceptionExtensions
{

    /// <summary> Adds exception data for building the exception full blown report. </summary>
    /// <remarks> (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para> </remarks>
    public static class ExceptionExtensionMethods
    {

        /// <summary> Adds exception data from the specified exception. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="exception"> The exception. </param>
        /// <returns> <c>true</c> if exception was added; otherwise <c>false</c> </returns>
        public static bool AddExceptionData( Exception exception )
        {
            // presently this project add no project-specific extensions.
            return false;
        }

        /// <summary> Adds an exception data. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="exception"> The exception. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        private static bool AddExceptionDataThis( Exception exception )
        {
            return AddExceptionData( exception ) || Core.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( exception );
        }

        /// <summary> Converts a value to a full blown string. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> Value as a String. </returns>
        internal static string ToFullBlownString( this Exception value )
        {
            return value.ToFullBlownString( int.MaxValue );
        }

        /// <summary> Converts this object to a full blown string. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The value. </param>
        /// <param name="level"> The level. </param>
        /// <returns> The given data converted to a String. </returns>
        internal static string ToFullBlownString( this Exception value, int level )
        {
            return Core.ExceptionExtensions.ExceptionExtensionMethods.ToFullBlownString( value, level, AddExceptionDataThis );
        }
    }
}
