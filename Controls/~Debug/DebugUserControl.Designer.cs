namespace isr.Core.Controls
{
    partial class DebugUserControl
    {

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.engineeringUpDown1 = new isr.Core.Controls.EngineeringUpDown();
            (( System.ComponentModel.ISupportInitialize ) (this.engineeringUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // engineeringUpDown1
            // 
            this.engineeringUpDown1.Location = new System.Drawing.Point( 17, 14 );
            this.engineeringUpDown1.Name = "engineeringUpDown1";
            this.engineeringUpDown1.NullValue = new decimal( new int[] {
            0,
            0,
            0,
            0} );
            this.engineeringUpDown1.ReadOnlyBackColor = System.Drawing.SystemColors.Control;
            this.engineeringUpDown1.ReadOnlyForeColor = System.Drawing.SystemColors.WindowText;
            this.engineeringUpDown1.ReadWriteBackColor = System.Drawing.SystemColors.Window;
            this.engineeringUpDown1.ReadWriteForeColor = System.Drawing.SystemColors.ControlText;
            this.engineeringUpDown1.ScaledValue = new decimal( new int[] {
            0,
            0,
            0,
            0} );
            this.engineeringUpDown1.Size = new System.Drawing.Size( 120, 20 );
            this.engineeringUpDown1.TabIndex = 0;
            this.engineeringUpDown1.UnscaledValue = new decimal( new int[] {
            0,
            0,
            0,
            0} );
            this.engineeringUpDown1.Value = new decimal( new int[] {
            0,
            0,
            0,
            0} );
            // 
            // DebugUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add( this.engineeringUpDown1 );
            this.Name = "DebugUserControl";
            this.Size = new System.Drawing.Size( 401, 97 );
            (( System.ComponentModel.ISupportInitialize ) (this.engineeringUpDown1)).EndInit();
            this.ResumeLayout( false );

        }

        #endregion

        private EngineeringUpDown engineeringUpDown1;
    }
}
