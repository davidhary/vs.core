using System.Windows.Forms;

namespace isr.Core.Controls
{
    /// <summary>   A debug user control. </summary>
    /// <remarks>   David, 2020-09-24. </remarks>
    public partial class DebugUserControl : UserControl
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-09-24. </remarks>
        public DebugUserControl()
        {
            this.InitializeComponent();
        }
    }
}
