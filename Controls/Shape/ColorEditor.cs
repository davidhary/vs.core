using System;
using System.Drawing;
using System.Drawing.Design;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace isr.Core.Controls
{

    /// <summary> A color editor control. </summary>
    /// <remarks>
    /// (c) 2016 Yang Kok Wah. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-05-14 </para><para>
    /// http://www.codeproject.com/Articles/10558/Shape-Control-for-NET. </para>
    /// </remarks>
    internal class ColorEditorControl : UserControl
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="initial_color"> The initial color. </param>
        public ColorEditorControl( Color initial_color )
        {
            this.NewColor = initial_color;
            this.OldColor = initial_color;
            this.InitializeComponent();
        }

        #endregion

        #region " DESIGNER "

        private TrackBar _TrackBarAlpha;
        private TrackBar _TrackBarRed;
        private TrackBar _TrackBarGreen;
        private TrackBar _TrackBarBlue;
        private Label _Label1;
        private Label _Label2;
        private Label _Label3;
        private Label _Label4;

        public Color NewColor { get; set; }
        public Color OldColor { get; set; }

        private Label _LabelColor;
        private Panel _PanelColor;

        /// <summary> Initializes the component. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void InitializeComponent()
        {
            this._TrackBarAlpha = new TrackBar();
            this._TrackBarRed = new TrackBar();
            this._TrackBarGreen = new TrackBar();
            this._TrackBarBlue = new TrackBar();
            this._Label1 = new Label();
            this._Label2 = new Label();
            this._Label3 = new Label();
            this._Label4 = new Label();
            this._PanelColor = new Panel();
            this._LabelColor = new Label();
            (( System.ComponentModel.ISupportInitialize ) this._TrackBarAlpha).BeginInit();
            (( System.ComponentModel.ISupportInitialize ) this._TrackBarRed).BeginInit();
            (( System.ComponentModel.ISupportInitialize ) this._TrackBarGreen).BeginInit();
            (( System.ComponentModel.ISupportInitialize ) this._TrackBarBlue).BeginInit();
            this._PanelColor.SuspendLayout();
            this.SuspendLayout();
            // 
            // trackBarAlpha
            // 
            this._TrackBarAlpha.Location = new Point( 40, 3 );
            this._TrackBarAlpha.Maximum = 255;
            this._TrackBarAlpha.Name = "trackBarAlpha";
            this._TrackBarAlpha.Size = new Size( 94, 45 );
            this._TrackBarAlpha.TabIndex = 0;
            this._TrackBarAlpha.TickFrequency = 20;
            this._TrackBarAlpha.ValueChanged += new EventHandler( this.TrackBar_ValueChanged );
            // 
            // trackBarRed
            // 
            this._TrackBarRed.Location = new Point( 40, 33 );
            this._TrackBarRed.Maximum = 255;
            this._TrackBarRed.Name = "trackBarRed";
            this._TrackBarRed.Size = new Size( 94, 45 );
            this._TrackBarRed.TabIndex = 1;
            this._TrackBarRed.TickFrequency = 20;
            this._TrackBarRed.ValueChanged += new EventHandler( this.TrackBar_ValueChanged );
            // 
            // trackBarGreen
            // 
            this._TrackBarGreen.Location = new Point( 40, 65 );
            this._TrackBarGreen.Maximum = 255;
            this._TrackBarGreen.Name = "trackBarGreen";
            this._TrackBarGreen.Size = new Size( 94, 45 );
            this._TrackBarGreen.TabIndex = 2;
            this._TrackBarGreen.TickFrequency = 20;
            this._TrackBarGreen.ValueChanged += new EventHandler( this.TrackBar_ValueChanged );
            // 
            // trackBarBlue
            // 
            this._TrackBarBlue.Location = new Point( 40, 97 );
            this._TrackBarBlue.Maximum = 255;
            this._TrackBarBlue.Name = "trackBarBlue";
            this._TrackBarBlue.Size = new Size( 94, 45 );
            this._TrackBarBlue.TabIndex = 3;
            this._TrackBarBlue.TickFrequency = 20;
            this._TrackBarBlue.ValueChanged += new EventHandler( this.TrackBar_ValueChanged );
            // 
            // label1
            // 
            this._Label1.Location = new Point( 8, 6 );
            this._Label1.Name = "label1";
            this._Label1.Size = new Size( 40, 24 );
            this._Label1.TabIndex = 5;
            this._Label1.Text = "Alpha";
            // 
            // label2
            // 
            this._Label2.Location = new Point( 8, 38 );
            this._Label2.Name = "label2";
            this._Label2.Size = new Size( 48, 24 );
            this._Label2.TabIndex = 6;
            this._Label2.Text = "Red";
            // 
            // label3
            // 
            this._Label3.Location = new Point( 8, 70 );
            this._Label3.Name = "label3";
            this._Label3.Size = new Size( 48, 24 );
            this._Label3.TabIndex = 7;
            this._Label3.Text = "Green";
            // 
            // label4
            // 
            this._Label4.Location = new Point( 8, 104 );
            this._Label4.Name = "label4";
            this._Label4.Size = new Size( 48, 24 );
            this._Label4.TabIndex = 8;
            this._Label4.Text = "Blue";
            // 
            // panelColor
            // 
            this._PanelColor.BorderStyle = BorderStyle.FixedSingle;
            this._PanelColor.Controls.Add( this._LabelColor );
            this._PanelColor.Location = new Point( 136, 11 );
            this._PanelColor.Name = "panelColor";
            this._PanelColor.Size = new Size( 31, 106 );
            this._PanelColor.TabIndex = 9;
            // 
            // labelColor
            // 
            this._LabelColor.Location = new Point( -10, -3 );
            this._LabelColor.Name = "labelColor";
            this._LabelColor.Size = new Size( 56, 160 );
            this._LabelColor.TabIndex = 10;
            // 
            // ColorEditorControl
            // 
            this.BackColor = Color.LightGray;
            this.Controls.Add( this._TrackBarBlue );
            this.Controls.Add( this._TrackBarGreen );
            this.Controls.Add( this._TrackBarRed );
            this.Controls.Add( this._PanelColor );
            this.Controls.Add( this._TrackBarAlpha );
            this.Controls.Add( this._Label4 );
            this.Controls.Add( this._Label3 );
            this.Controls.Add( this._Label2 );
            this.Controls.Add( this._Label1 );
            this.Name = "ColorEditorControl";
            this.Size = new Size( 171, 135 );
            KeyPress += new KeyPressEventHandler( this.ColorEditorControl_KeyPress );
            Load += new EventHandler( this.ColorEditorControl_Load );
            (( System.ComponentModel.ISupportInitialize ) this._TrackBarAlpha).EndInit();
            (( System.ComponentModel.ISupportInitialize ) this._TrackBarRed).EndInit();
            (( System.ComponentModel.ISupportInitialize ) this._TrackBarGreen).EndInit();
            (( System.ComponentModel.ISupportInitialize ) this._TrackBarBlue).EndInit();
            this._PanelColor.ResumeLayout( false );
            this.ResumeLayout( false );
        }

        #endregion

        #region " EVENT HANDLERS "

        /// <summary> Event handler. Called by ColorEditorControl for load events. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ColorEditorControl_Load( object sender, EventArgs e )
        {
            int argb = this.NewColor.ToArgb();
            // the colors are store in the argb value as 4 byte : AARRGGBB
            byte alpha = ( byte ) (argb >> 24);
            byte red = ( byte ) (argb >> 16);
            byte green = ( byte ) (argb >> 8);
            byte blue = ( byte ) argb;
            this._TrackBarAlpha.Value = alpha;
            this._TrackBarRed.Value = red;
            this._TrackBarGreen.Value = green;
            this._TrackBarBlue.Value = blue;

            // Foreground Label on the Color Panel
            this._LabelColor.BackColor = Color.FromArgb( alpha, red, green, blue );

            // Create the Background Image for Color Panel 
            // The Color Panel is to allow the user to check on Alpha transparency
            var bm = new Bitmap( this._PanelColor.Width, this._PanelColor.Height );
            this._PanelColor.BackgroundImage = bm;
            var g = Graphics.FromImage( this._PanelColor.BackgroundImage );
            g.FillRectangle( Brushes.White, 0, 0, this._PanelColor.Width, this._PanelColor.Height );

            // For formating the string
            using var sf = new StringFormat {
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Center
            };

            // For rotating the string 
            // If you want the text to be rotated uncomment the 5 lines below and comment off the last line

            using var font = new Font( "Arial", 16f, FontStyle.Bold );
            // Matrix m=new Matrix ();
            // m.Rotate(90);
            // m.Translate(this.panelColor.Width ,0 ,MatrixOrder.Append);
            // g.Transform=m;
            // g.DrawString("TEST",new Font("Arial",16,FontStyle.Bold),Brushes.Black,new Rectangle(0,0,panelColor.Height ,panelColor.Width  ),sf);
            g.DrawString( "TEST", font, Brushes.Black, new Rectangle( 0, 0, this._PanelColor.Width, this._PanelColor.Height ), sf );
        }

        /// <summary> Updates the color. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="a"> An Integer to process. </param>
        /// <param name="r"> An Integer to process. </param>
        /// <param name="g"> An Integer to process. </param>
        /// <param name="b"> An Integer to process. </param>
        private void UpdateColor( int a, int r, int g, int b )
        {
            this.NewColor = Color.FromArgb( a, r, g, b );
            this._LabelColor.BackColor = this.NewColor;
        }

        /// <summary> Event handler. Called by TrackBar for value changed events. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void TrackBar_ValueChanged( object sender, EventArgs e )
        {
            this.UpdateColor( this._TrackBarAlpha.Value, this._TrackBarRed.Value, this._TrackBarGreen.Value, this._TrackBarBlue.Value );
        }

        /// <summary> Event handler. Called by ColorEditorControl for key press events. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Key press event information. </param>
        private void ColorEditorControl_KeyPress( object sender, KeyPressEventArgs e )
        {
            // If the user hit the escape key we restore back the old color
            if ( e.KeyChar.Equals( Keys.Escape ) )
            {
                this.NewColor = this.OldColor;
            }
        }
        #endregion

    }

    /// <summary> Editor for color. </summary>
    /// <remarks>
    /// (c) 2016 Yang Kok Wah. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-05-14 </para><para>
    /// http://www.codeproject.com/Articles/10558/Shape-Control-for-NET. </para>
    /// </remarks>
    public class ColorEditor : UITypeEditor
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Drawing.Design.UITypeEditor" /> class.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public ColorEditor()
        {
        }

        /// <summary>
        /// Gets the editor style used by the
        /// <see cref="M:System.Drawing.Design.UITypeEditor.EditValue(System.IServiceProvider,System.Object)" />
        /// method.
        /// </summary>
        /// <remarks>
        /// David, 2020-09-24. Indicates whether the UITypeEditor provides a form-based (modal) dialog,
        /// drop down dialog, or no UI outside of the properties window.
        /// </remarks>
        /// <param name="context"> An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that
        /// can be used to gain additional context information. </param>
        /// <returns>
        /// A <see cref="T:System.Drawing.Design.UITypeEditorEditStyle" /> value that indicates the style
        /// of editor used by the
        /// <see cref="M:System.Drawing.Design.UITypeEditor.EditValue(System.IServiceProvider,System.Object)" />
        /// method. If the <see cref="T:System.Drawing.Design.UITypeEditor" /> does not support this
        /// method, then <see cref="M:System.Drawing.Design.UITypeEditor.GetEditStyle" /> will return
        /// <see cref="F:System.Drawing.Design.UITypeEditorEditStyle.None" />.
        /// </returns>
        [System.Security.Permissions.PermissionSet( System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust" )]
        public override UITypeEditorEditStyle GetEditStyle( System.ComponentModel.ITypeDescriptorContext context )
        {
            return UITypeEditorEditStyle.DropDown;
        }

        /// <summary>
        /// Edits the specified object's value using the editor style indicated by the
        /// <see cref="M:System.Drawing.Design.UITypeEditor.GetEditStyle" /> method.
        /// </summary>
        /// <remarks> David, 2020-09-24. Displays the UI for value selection. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="context">  An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that
        /// can be used to gain additional context information. </param>
        /// <param name="provider"> An <see cref="T:System.IServiceProvider" /> that this editor can use
        /// to obtain services. </param>
        /// <param name="value">    The object to edit. </param>
        /// <returns>
        /// The new value of the object. If the value of the object has not changed, this should return
        /// the same object it was passed.
        /// </returns>
        [System.Security.Permissions.PermissionSet( System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust" )]
        public override object EditValue( System.ComponentModel.ITypeDescriptorContext context, IServiceProvider provider, object value )
        {
            if ( value is null )
            {
                throw new ArgumentNullException( nameof( value ) );
            }

            if ( provider is null )
            {
                throw new ArgumentNullException( nameof( provider ) );
            }

            if ( !ReferenceEquals( value.GetType(), typeof( Color ) ) )
            {
                return value;
            }

            // Uses the IWindowsFormsEditorService to display a 
            // drop-down UI in the Properties window.
            IWindowsFormsEditorService edSvc = ( IWindowsFormsEditorService ) provider.GetService( typeof( IWindowsFormsEditorService ) );
            if ( edSvc is object )
            {
                using var editor = new ColorEditorControl( ( Color ) value );
                edSvc.DropDownControl( editor );
                // Return the value in the appropriate data format.
                if ( ReferenceEquals( value.GetType(), typeof( Color ) ) )
                {
                    var result = editor.NewColor;
                    return result;
                }
            }

            return value;
        }

        /// <summary>
        /// Paints a representation of the value of an object using the specified
        /// <see cref="T:System.Drawing.Design.PaintValueEventArgs" />.
        /// </summary>
        /// <remarks> David, 2020-09-24. Draws a representation of the property's value. </remarks>
        /// <param name="e"> A <see cref="T:System.Drawing.Design.PaintValueEventArgs" /> that indicates
        /// what to paint and where to paint it. </param>
        [System.Security.Permissions.PermissionSet( System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust" )]
        public override void PaintValue( PaintValueEventArgs e )
        {
            if ( e is null )
            {
                return;
            }

            using var br = new SolidBrush( ( Color ) e.Value );
            e.Graphics.FillRectangle( br, 1, 1, e.Bounds.Width, e.Bounds.Height );
        }

        /// <summary>
        /// Indicates whether the specified context supports painting a representation of an object's
        /// value within the specified context.
        /// </summary>
        /// <remarks>
        /// David, 2020-09-24. Indicates whether the UITypeEditor supports painting a representation of a
        /// property's value.
        /// </remarks>
        /// <param name="context"> An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that
        /// can be used to gain additional context information. </param>
        /// <returns>
        /// <see langword="true" /> if
        /// <see cref="M:System.Drawing.Design.UITypeEditor.PaintValue(System.Object,System.Drawing.Graphics,System.Drawing.Rectangle)" />
        /// is implemented; otherwise, <see langword="false" />.
        /// </returns>
        [System.Security.Permissions.PermissionSet( System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust" )]
        public override bool GetPaintValueSupported( System.ComponentModel.ITypeDescriptorContext context )
        {
            return true;
        }
    }
}
