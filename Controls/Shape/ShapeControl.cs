using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace isr.Core.Controls
{

    /// <summary> A shape control. </summary>
    /// <remarks>
    /// (c) 2016 Yang Kok Wah. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-05-14 </para><para>
    /// http://www.codeproject.com/Articles/10558/Shape-Control-for-NET. </para>
    /// </remarks>
    public class ShapeControl : Control
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Windows.Forms.Control" /> class with
        /// default settings.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public ShapeControl() : base()
        {
            // This call is required by the Windows.Forms Form Designer.
            this.InitializeComponent();
            this.DoubleBuffered = true;
            // Using Double Buffer allow for smooth rendering 
            // minimizing flickering
            this.SetStyle( ControlStyles.SupportsTransparentBackColor | ControlStyles.DoubleBuffer | ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true );

            // set the default back color and font
            this.BackColor = Color.FromArgb( 0, 255, 255, 255 );
            this.Font = new Font( "Arial", 12f, FontStyle.Bold );
            this.Font = new Font( "Arial", 12f, FontStyle.Bold );
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> <see langword="true" /> to release both managed and unmanaged
        /// resources; <see langword="false" /> to release only unmanaged
        /// resources. </param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing )
            {
                if ( this._Outline is object )
                {
                    this._Outline.Dispose();
                    this._Outline = null;
                }

                if ( this._Components is object )
                {
                    this._Components.Dispose();
                }
            }

            base.Dispose( disposing );
        }

        #endregion


        #region " Component Designer generated code"

        /// <summary> Initializes the component. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void InitializeComponent()
        {
            // Me.DoubleBuffered = True
            this._Components = new Container();
            this._Timer1 = new Timer( this._Components );
            this._Timer2 = new Timer( this._Components );
            this.SuspendLayout();
            // 
            // timer1
            // 
            this._Timer1.Interval = 200;
            this._Timer1.Tick += new EventHandler( this.Timer1_Tick );
            // 
            // timer2
            // 
            this._Timer2.Interval = 200;
            this._Timer2.Tick += new EventHandler( this.Timer2_Tick );
            // 
            // CustomControl1
            // 
            TextChanged += new EventHandler( this.ShapeControl_TextChanged );
            this.ResumeLayout( false );
        }

        #endregion

        private IContainer _Components;
        private bool _Istextset = false;
        private ShapeType _Shape = ShapeType.Rectangle;
        private DashStyle _Borderstyle = DashStyle.Solid;
        private Color _Bordercolor = Color.FromArgb( 255, 255, 0, 0 );
        private int _Borderwidth = 3;
        private GraphicsPath _Outline = new();
        private bool _Usegradient = false;
        private bool _Vibrate = false;
        private bool _Voffseted = false;
        private Color _Centercolor = Color.FromArgb( 100, 255, 0, 0 );
        private Color _Surroundcolor = Color.FromArgb( 100, 0, 255, 255 );
        private Timer _Timer1;
        private Timer _Timer2;
        private Bitmap _Bm;

        /// <summary> Additional user-defined data. </summary>
        /// <value> The tag 2. </value>
        [Category( "Shape" )]
        [Description( "Additional user-defined data" )]
        public string Tag2 { get; set; }

        /// <summary> True to blink. </summary>
        private bool _Blink = false;

        /// <summary> Causes the control to blink. </summary>
        /// <value> The blink. </value>
        [Category( "Shape" )]
        [Description( "Causes the control to blink" )]
        public bool Blink
        {
            get => this._Blink;

            set {
                this._Blink = value;
                this._Timer1.Enabled = this._Blink;
                if ( !this._Blink )
                {
                    this.Visible = true;
                }
            }
        }

        /// <summary> Causes the control to vibrate. </summary>
        /// <value> The vibrate. </value>
        [Category( "Shape" )]
        [Description( "Causes the control to vibrate" )]
        public bool Vibrate
        {
            get => this._Vibrate;

            set {
                this._Vibrate = value;
                this._Timer2.Enabled = this._Vibrate;
                if ( !this._Vibrate )
                {
                    if ( this._Voffseted )
                    {
                        this.Top += 5;
                        this._Voffseted = false;
                    }
                }
            }
        }

        /// <summary> Background Image to define outline. </summary>
        /// <value> The shape image. </value>
        [Category( "Shape" )]
        [Description( "Background Image to define outline" )]
        public Image ShapeImage
        {
            get => this._Bm;

            set {
                if ( value is object )
                {
                    this._Bm = ( Bitmap ) value.Clone();
                    this.Width = 150;
                    this.Height = 150;
                    this.OnResize( null );
                }
                else
                {
                    if ( this._Bm is object )
                    {
                        this._Bm = null;
                    }

                    this.OnResize( null );
                }
            }
        }

        /// <summary> Gets or sets the text associated with this control. </summary>
        /// <value> The text associated with this control. </value>
        [Category( "Shape" )]
        [Description( "Text to display" )]
        public override string Text
        {
            get => base.Text;

            set {
                base.Text = value;

                // When Visual Studio first create a new control, text=name
                // we do not want any default text, thus we override it with blank
                if ( !this._Istextset && base.Text.Equals( this.Name ) )
                {
                    base.Text = string.Empty;
                }

                this._Istextset = true;
            }
        }

        /// <summary> Gets or sets the background color for the control. </summary>
        /// <value>
        /// A <see cref="T:System.Drawing.Color" /> that represents the background color of the control.
        /// The default is the value of the
        /// <see cref="P:System.Windows.Forms.Control.DefaultBackColor" /> property.
        /// </value>
        [Category( "Shape" )]
        [Description( "Back Color" )]
        [Browsable( true )]
        [Editor( typeof( ColorEditor ), typeof( System.Drawing.Design.UITypeEditor ) )]
        public override Color BackColor
        {
            get => base.BackColor;

            set {
                base.BackColor = value;
                this.Refresh();
            }
        }

        /// <summary> Using Gradient to fill Shape. </summary>
        /// <value> The use gradient. </value>
        [Category( "Shape" )]
        [Description( "Using Gradient to fill Shape" )]
        public bool UseGradient
        {
            get => this._Usegradient;

            set {
                this._Usegradient = value;
                this.Refresh();
            }
        }

        /// <summary> Color at center. </summary>
        /// <value> The color of the center. </value>
        /// <remarks> For Gradient Rendering, this is the color at the center of the shape </remarks>
        [Category( "Shape" )]
        [Description( "Color at center" )]
        [Browsable( true )]
        [Editor( typeof( ColorEditor ), typeof( System.Drawing.Design.UITypeEditor ) )]
        public Color CenterColor
        {
            get => this._Centercolor;

            set {
                this._Centercolor = value;
                this.Refresh();
            }
        }

        /// <summary> Color at the edges of the Shape. </summary>
        /// <value> The color of the surround. </value>
        /// <remarks> For Gradient Rendering, this is the color at the edges of the shape</remarks>
        [Category( "Shape" )]
        [Description( "Color at the edges of the Shape" )]
        [Browsable( true )]
        [Editor( typeof( ColorEditor ), typeof( System.Drawing.Design.UITypeEditor ) )]
        public Color SurroundColor
        {
            get => this._Surroundcolor;

            set {
                this._Surroundcolor = value;
                this.Refresh();
            }
        }

        /// <summary> Border Width. </summary>
        /// <value> The width of the border. </value>
        [Category( "Shape" )]
        [Description( "Border Width" )]
        public int BorderWidth
        {
            get => this._Borderwidth;

            set {
                this._Borderwidth = value;
                if ( this._Borderwidth < 0 )
                {
                    this._Borderwidth = 0;
                }

                this.Refresh();
            }
        }

        /// <summary> Border Color. </summary>
        /// <value> The color of the border. </value>
        [Category( "Shape" )]
        [Description( "Border Color" )]
        [Browsable( true )]
        [Editor( typeof( ColorEditor ), typeof( System.Drawing.Design.UITypeEditor ) )]
        public Color BorderColor
        {
            get => this._Bordercolor;

            set {
                this._Bordercolor = value;
                this.Refresh();
            }
        }

        /// <summary> Border Style. </summary>
        /// <value> The border style. </value>
        [Category( "Shape" )]
        [Description( "Border Style" )]
        public DashStyle BorderStyle
        {
            get => this._Borderstyle;

            set {
                this._Borderstyle = value;
                this.Refresh();
            }
        }

        /// <summary> Select Shape. </summary>
        /// <value> The shape. </value>
        [Category( "Shape" )]
        [Description( "Select Shape" )]
        [Browsable( true )]
        [Editor( typeof( ShapeTypeEditor ), typeof( System.Drawing.Design.UITypeEditor ) )]
        public ShapeType Shape
        {
            get => this._Shape;

            set {
                this._Shape = value;
                this.OnResize( null );
            }
        }

        /// <summary> Updates the outline. </summary>
        /// <remarks> David, 2020-09-24.
        /// This function creates the path for each shape It is also being used by the
        /// ShapeTypeEditor to create the various shapes for the Shape property editor UI.
        /// </remarks>
        /// <param name="outline"> [in,out] The outline. </param>
        /// <param name="shape">   The shape. </param>
        /// <param name="width">   The width. </param>
        /// <param name="height">  The height. </param>
        internal static void UpdateOutline( ref GraphicsPath outline, ShapeType shape, int width, int height )
        {
            switch ( shape )
            {
                case ShapeType.CustomPie:
                    {
                        outline.AddPie( 0, 0, width, height, 180f, 270f );
                        break;
                    }

                case ShapeType.CustomPolygon:
                    {
                        outline.AddPolygon( new Point[] { new Point( 0, 0 ), new Point( width / 2, height / 4 ), new Point( width, 0 ), new Point( width * 3 / 4, height / 2 ), new Point( width, height ), new Point( width / 2, height * 3 / 4 ), new Point( 0, height ), new Point( width / 4, height / 2 ) } );
                        break;
                    }

                case ShapeType.Diamond:
                    {
                        outline.AddPolygon( new Point[] { new Point( 0, height / 2 ), new Point( width / 2, 0 ), new Point( width, height / 2 ), new Point( width / 2, height ) } );
                        break;
                    }

                case ShapeType.Rectangle:
                    {
                        outline.AddRectangle( new Rectangle( 0, 0, width, height ) );
                        break;
                    }

                case ShapeType.Ellipse:
                    {
                        outline.AddEllipse( 0, 0, width, height );
                        break;
                    }

                case ShapeType.TriangleUp:
                    {
                        outline.AddPolygon( new Point[] { new Point( 0, height ), new Point( width, height ), new Point( width / 2, 0 ) } );
                        break;
                    }

                case ShapeType.TriangleDown:
                    {
                        outline.AddPolygon( new Point[] { new Point( 0, 0 ), new Point( width, 0 ), new Point( width / 2, height ) } );
                        break;
                    }

                case ShapeType.TriangleLeft:
                    {
                        outline.AddPolygon( new Point[] { new Point( width, 0 ), new Point( 0, height / 2 ), new Point( width, height ) } );
                        break;
                    }

                case ShapeType.TriangleRight:
                    {
                        outline.AddPolygon( new Point[] { new Point( 0, 0 ), new Point( width, height / 2 ), new Point( 0, height ) } );
                        break;
                    }

                case ShapeType.RoundedRectangle:
                    {
                        outline.AddArc( 0, 0, width / 4, width / 4, 180f, 90f );
                        outline.AddLine( width / 8, 0, width - width / 8, 0 );
                        outline.AddArc( width - width / 4, 0, width / 4, width / 4, 270f, 90f );
                        outline.AddLine( width, width / 8, width, height - width / 8 );
                        outline.AddArc( width - width / 4, height - width / 4, width / 4, width / 4, 0f, 90f );
                        outline.AddLine( width - width / 8, height, width / 8, height );
                        outline.AddArc( 0, height - width / 4, width / 4, width / 4, 90f, 90f );
                        outline.AddLine( 0, height - width / 8, 0, width / 8 );
                        break;
                    }

                case ShapeType.BalloonSW:
                    {
                        outline.AddArc( 0, 0, width / 4, width / 4, 180f, 90f );
                        outline.AddLine( width / 8, 0, width - width / 8, 0 );
                        outline.AddArc( width - width / 4, 0, width / 4, width / 4, 270f, 90f );
                        outline.AddLine( width, width / 8, width, height * 0.75f - width / 8 );
                        outline.AddArc( width - width / 4, height * 0.75f - width / 4, width / 4, width / 4, 0f, 90f );
                        outline.AddLine( width - width / 8, height * 0.75f, width / 8 + width / 4, height * 0.75f );
                        outline.AddLine( width / 8 + width / 4, height * 0.75f, width / 8 + width / 8, height );
                        outline.AddLine( width / 8 + width / 8, height, width / 8 + width / 8, height * 0.75f );
                        outline.AddLine( width / 8 + width / 8, height * 0.75f, width / 8, height * 0.75f );
                        outline.AddArc( 0f, height * 0.75f - width / 4, width / 4, width / 4, 90f, 90f );
                        outline.AddLine( 0f, height * 0.75f - width / 8, 0f, width / 8 );
                        break;
                    }

                case ShapeType.BalloonSE:
                    {
                        outline.AddArc( 0, 0, width / 4, width / 4, 180f, 90f );
                        outline.AddLine( width / 8, 0, width - width / 8, 0 );
                        outline.AddArc( width - width / 4, 0, width / 4, width / 4, 270f, 90f );
                        outline.AddLine( width, width / 8, width, height * 0.75f - width / 8 );
                        outline.AddArc( width - width / 4, height * 0.75f - width / 4, width / 4, width / 4, 0f, 90f );
                        outline.AddLine( width - width / 8, height * 0.75f, width - width / 4, height * 0.75f );
                        outline.AddLine( width - width / 4, height * 0.75f, width - width / 4, height );
                        outline.AddLine( width - width / 4, height, width - 3 * width / 8, height * 0.75f );
                        outline.AddLine( width - 3 * width / 8, height * 0.75f, width / 8, height * 0.75f );
                        outline.AddArc( 0f, height * 0.75f - width / 4, width / 4, width / 4, 90f, 90f );
                        outline.AddLine( 0f, height * 0.75f - width / 8, 0f, width / 8 );
                        break;
                    }

                case ShapeType.BalloonNW:
                    {
                        outline.AddArc( width - width / 4, height - width / 4, width / 4, width / 4, 0f, 90f );
                        outline.AddLine( width - width / 8, height, width - width / 4, height );
                        outline.AddArc( 0, height - width / 4, width / 4, width / 4, 90f, 90f );
                        outline.AddLine( 0f, height - width / 8, 0f, height * 0.25f + width / 8 );
                        outline.AddArc( 0f, height * 0.25f, width / 4, width / 4, 180f, 90f );
                        outline.AddLine( width / 8, height * 0.25f, width / 4, height * 0.25f );
                        outline.AddLine( width / 4, height * 0.25f, width / 4, 0f );
                        outline.AddLine( width / 4, 0f, 3 * width / 8, height * 0.25f );
                        outline.AddLine( 3 * width / 8, height * 0.25f, width - width / 8, height * 0.25f );
                        outline.AddArc( width - width / 4, height * 0.25f, width / 4, width / 4, 270f, 90f );
                        outline.AddLine( width, width / 8 + height * 0.25f, width, height - width / 8 );
                        break;
                    }

                case ShapeType.BalloonNE:
                    {
                        outline.AddArc( width - width / 4, height - width / 4, width / 4, width / 4, 0f, 90f );
                        outline.AddLine( width - width / 8, height, width - width / 4, height );
                        outline.AddArc( 0, height - width / 4, width / 4, width / 4, 90f, 90f );
                        outline.AddLine( 0f, height - width / 8, 0f, height * 0.25f + width / 8 );
                        outline.AddArc( 0f, height * 0.25f, width / 4, width / 4, 180f, 90f );
                        outline.AddLine( width / 8, height * 0.25f, 5 * width / 8, height * 0.25f );
                        outline.AddLine( 5 * width / 8, height * 0.25f, 3 * width / 4, 0f );
                        outline.AddLine( 3 * width / 4, 0f, 3 * width / 4, height * 0.25f );
                        outline.AddLine( 3 * width / 4, height * 0.25f, width - width / 8, height * 0.25f );
                        outline.AddArc( width - width / 4, height * 0.25f, width / 4, width / 4, 270f, 90f );
                        outline.AddLine( width, width / 8 + height * 0.25f, width, height - width / 8 );
                        break;
                    }

                default:
                    {
                        break;
                    }
            }
        }

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.Resize" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnResize( EventArgs e )
        {
            if ( this.Width < 0 || this.Height <= 0 )
            {
                return;
            }

            if ( this._Bm is null )
            {
                this._Outline = new GraphicsPath();
                UpdateOutline( ref this._Outline, this._Shape, this.Width, this.Height );
            }
            else
            {
                using Bitmap bm = ( Bitmap ) this._Bm.Clone();
                using var bm2 = new Bitmap( this.Width, this.Height );
                Debug.WriteLine( bm2.Width + "," + bm2.Height );
                Graphics.FromImage( bm2 ).DrawImage( bm, new RectangleF( 0f, 0f, bm2.Width, bm2.Height ), new RectangleF( 0f, 0f, bm.Width, bm.Height ), GraphicsUnit.Pixel );
                var trace = new OutlineTrace();
                string s = trace.TraceOutlineN( bm2, 0, bm2.Height / 2, bm2.Width / 2, Color.Black, Color.White, true, 1 );
                var p = OutlineTrace.StringOutline2Polygon( s );
                this._Outline = new GraphicsPath();
                this._Outline.AddPolygon( p );
            }

            if ( this._Outline is object )
            {
                this.Region = new Region( this._Outline );
            }

            this.Refresh();
            base.OnResize( e );
        }

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.Paint" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnPaint( PaintEventArgs e )
        {
            if ( e is null )
            {
                return;
            }
            // Rendering with Gradient
            if ( this._Usegradient )
            {
                using var br = new PathGradientBrush( this._Outline ) {
                    CenterColor = this._Centercolor,
                    SurroundColors = new Color[] { this._Surroundcolor }
                };
                e.Graphics.FillPath( br, this._Outline );
            }

            // Rendering with Border
            if ( this._Borderwidth > 0 )
            {
                using var p = new Pen( this._Bordercolor, this._Borderwidth * 2 ) {
                    DashStyle = this._Borderstyle
                };
                e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
                e.Graphics.DrawPath( p, this._Outline );
            }

            // Rendering the text to be at the center of the shape
            using ( var sf = new StringFormat() )
            {
                sf.Alignment = StringAlignment.Center;
                sf.LineAlignment = StringAlignment.Center;
                switch ( this._Shape )
                {
                    case ShapeType.BalloonNE:
                    case ShapeType.BalloonNW:
                        {
                            using var br = new SolidBrush( this.ForeColor );
                            e.Graphics.DrawString( this.Text, this.Font, br, new RectangleF( 0f, this.Height * 0.25f, this.Width, this.Height * 0.75f ), sf );

                            break;
                        }

                    case ShapeType.BalloonSE:
                    case ShapeType.BalloonSW:
                        {
                            using var br = new SolidBrush( this.ForeColor );
                            e.Graphics.DrawString( this.Text, this.Font, br, new RectangleF( 0f, 0f, this.Width, this.Height * 0.75f ), sf );

                            break;
                        }

                    default:
                        {
                            using var br = new SolidBrush( this.ForeColor );
                            e.Graphics.DrawString( this.Text, this.Font, br, new Rectangle( 0, 0, this.Width, this.Height ), sf );

                            break;
                        }
                }
            }
            // Calling the base class OnPaint
            base.OnPaint( e );
        }

        /// <summary> Event handler. Called by ShapeControl for text changed events. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ShapeControl_TextChanged( object sender, EventArgs e )
        {
            this.Refresh();
        }

        /// <summary> Event handler. Called by Timer1 for tick events. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void Timer1_Tick( object sender, EventArgs e )
        {
            this.Visible = !this.Visible;
        }

        /// <summary> Event handler. Called by Timer2 for tick events. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void Timer2_Tick( object sender, EventArgs e )
        {
            if ( !this._Vibrate )
            {
                return;
            }

            this._Voffseted = !this._Voffseted;
            this.Top = this._Voffseted ? this.Top - 5 : this.Top + 5;
        }
    }

    /// <summary> Values that represent shape types. </summary>
    /// <remarks> David, 2020-09-24. All the defined shape type </remarks>
    public enum ShapeType
    {

        /// <summary> An enum constant representing the rectangle option. </summary>
        Rectangle,

        /// <summary> An enum constant representing the rounded rectangle option. </summary>
        RoundedRectangle,

        /// <summary> An enum constant representing the diamond option. </summary>
        Diamond,

        /// <summary> An enum constant representing the ellipse option. </summary>
        Ellipse,

        /// <summary> An enum constant representing the triangle up option. </summary>
        TriangleUp,

        /// <summary> An enum constant representing the triangle down option. </summary>
        TriangleDown,

        /// <summary> An enum constant representing the triangle left option. </summary>
        TriangleLeft,

        /// <summary> An enum constant representing the triangle right option. </summary>
        TriangleRight,

        /// <summary> An enum constant representing the balloon NW option. </summary>
        BalloonNE,

        /// <summary> An enum constant representing the balloon nw option. </summary>
        BalloonNW,

        /// <summary> An enum constant representing the balloon Software option. </summary>
        BalloonSW,

        /// <summary> An enum constant representing the balloon se option. </summary>
        BalloonSE,

        /// <summary> An enum constant representing the custom polygon option. </summary>
        CustomPolygon,

        /// <summary> An enum constant representing the custom pie option. </summary>
        CustomPie
    }
}
