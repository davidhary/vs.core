using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace isr.Core.Controls
{

    /// <summary> An outline trace. </summary>
    /// <remarks>
    /// (c) 2016 Yang Kok Wah. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-05-14 </para><para>
    /// http://www.codeproject.com/Articles/10558/Shape-Control-for-NET. </para>
    /// </remarks>
    internal class OutlineTrace
    {

        /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public OutlineTrace() : base()
        {
        }

        /// <summary> Gets or sets the color threshold. </summary>
        /// <value> The color threshold. </value>
        public int ColorThreshold { get; set; } = 200;

        /// <summary> Gets or sets the use red. </summary>
        /// <value> The use red. </value>
        public bool UseRed { get; set; } = true;

        /// <summary> Gets or sets the use green. </summary>
        /// <value> The use green. </value>
        public bool UseGreen { get; set; } = true;

        /// <summary> Gets or sets the use blue. </summary>
        /// <value> The use blue. </value>
        public bool UseBlue { get; set; } = true;

        /// <summary> Coordinates to index for ARGB color pixel format. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="x">      The x coordinate. </param>
        /// <param name="y">      The y coordinate. </param>
        /// <param name="stride"> The stride. </param>
        /// <returns> An Integer. </returns>
        private static int CoordsToIndex( int x, int y, int stride )
        {
            return stride * y + x * 4;
        }

        /// <summary> Gets gray scale color. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="c"> A Color to process. </param>
        /// <returns> The gray scale color. </returns>
        public int GetGrayScaleColor( Color c )
        {
            int numcolorplane = 3;
            numcolorplane = !this.UseBlue ? numcolorplane - 1 : numcolorplane;
            numcolorplane = !this.UseGreen ? numcolorplane - 1 : numcolorplane;
            numcolorplane = !this.UseRed ? numcolorplane - 1 : numcolorplane;
            if ( numcolorplane == 0 )
            {
                return (c.B + c.G + c.R) / 3;
            }

            int accvalue = 0;
            accvalue = this.UseBlue ? accvalue + c.B : accvalue;
            accvalue = this.UseGreen ? accvalue + c.G : accvalue;
            accvalue = this.UseRed ? accvalue + c.R : accvalue;
            return accvalue / numcolorplane;
        }

        /// <summary> Gets mono color. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="c"> A Color to process. </param>
        /// <returns> The mono color. </returns>
        private int GetMonoColor( Color c )
        {
            int i = this.GetGrayScaleColor( c );
            return i < this.ColorThreshold ? 0 : 1;
        }

        /// <summary> String outline 2 polygon. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="outline"> The outline. </param>
        /// <returns> A Point() </returns>
        public static Point[] StringOutline2Polygon( string outline )
        {
            var s = outline.Split( ';' );
            if ( s.Length < 5 )
            {
                return null;
            }

            var p = new Point[s.Length - 2 + 1];
            string[] s1; // = s(0).Split(","c)
            for ( int i = 0, loopTo = s.Length - 2; i <= loopTo; i++ )
            {
                s1 = s[i].Split( ',' );
                p[i].X = int.Parse( s1[0] );
                p[i].Y = int.Parse( s1[1] );
            }

            return p;
        }

        /// <summary> Trace outline n. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="bm">              The bm. </param>
        /// <param name="x0">              The x coordinate 0. </param>
        /// <param name="y0">              The y coordinate 0. </param>
        /// <param name="probe_width">     Width of the probe. </param>
        /// <param name="fg">              The foreground. </param>
        /// <param name="bg">              The background. </param>
        /// <param name="bauto_threshold"> True to bauto threshold. </param>
        /// <param name="n">               An Integer to process. </param>
        /// <returns> A String. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public string TraceOutlineN( Bitmap bm, int x0, int y0, int probe_width, Color fg, Color bg, bool bauto_threshold, int n )
        {
            string s = string.Empty;
            int y = y0;
            int x1 = 0;
            int y1 = 0;
            Color c1;
            Color c2;
            int start_direction = 0;
            int current_direction = 0;
            bool hitborder = false;
            bool hitstart = false;
            int max_width = bm.Width;
            int max_height = bm.Height;

            // direct bit manipulation
            var rect = new Rectangle( 0, 0, bm.Width, bm.Height );
            var bmpData = bm.LockBits( rect, ImageLockMode.ReadOnly, bm.PixelFormat );
            var ptr = bmpData.Scan0;
            int bytes = bm.Width * bm.Height * 4;
            var rgbValues = new byte[bytes];

            // Copy the RGB values into the array.
            System.Runtime.InteropServices.Marshal.Copy( ptr, rgbValues, 0, bytes );
            bm.UnlockBits( bmpData );
            int x;
            int gc1;
            int gc2;
            if ( bauto_threshold )
            {
                // get max pix value difference
                int maxpixdiff = 0;
                int maxpixvalue = 0;
                for ( int i = 0, loopTo = probe_width * 2 - 1; i <= loopTo; i++ )
                {
                    try
                    {
                        x = i % 2 == 1 ? x0 + i / 2 : x0 - i / 2;
                        if ( x < 0 )
                        {
                            continue;
                        }

                        if ( x >= max_width )
                        {
                            continue;
                        }

                        int index = CoordsToIndex( x, y0, bmpData.Stride );
                        if ( index < 0 || index > bytes - 1 )
                        {
                            break;
                        }

                        c1 = Color.FromArgb( rgbValues[index + 2], rgbValues[index + 1], rgbValues[index] );
                        gc1 = this.GetGrayScaleColor( c1 );
                        index = CoordsToIndex( x + 1, y0, bmpData.Stride );
                        if ( index < 0 || index > bytes - 1 )
                        {
                            break;
                        }

                        c2 = Color.FromArgb( rgbValues[index + 2], rgbValues[index + 1], rgbValues[index] );
                        gc2 = this.GetGrayScaleColor( c2 );
                        if ( maxpixdiff < Math.Abs( gc1 - gc2 ) )
                        {
                            maxpixdiff = Math.Abs( gc1 - gc2 );
                        }

                        if ( gc1 > maxpixvalue )
                        {
                            maxpixvalue = gc1;
                        }

                        if ( gc2 > maxpixvalue )
                        {
                            maxpixvalue = gc2;
                        }
                    }
                    catch ( Exception )
                    {
                        break;
                    }
                }

                if ( maxpixdiff > 0 )
                {
                    this.ColorThreshold = maxpixvalue - ( int ) Math.Truncate( 0.3d * maxpixdiff );
                }

                if ( this.ColorThreshold < 0 )
                {
                    this.ColorThreshold = 0;
                }
            }

            int gfg = this.GetMonoColor( fg );
            int gbg = this.GetMonoColor( bg );
            for ( int i = 0, loopTo1 = probe_width * 2 - 1; i <= loopTo1; i++ )
            {
                try
                {
                    x = i % 2 == 1 ? x0 + i / 2 : x0 - i / 2;
                    if ( x < 0 )
                    {
                        continue;
                    }

                    if ( x >= max_width )
                    {
                        continue;
                    }

                    int index = CoordsToIndex( x, y0, bmpData.Stride );
                    if ( index < 0 || index > bytes - 1 )
                    {
                        break;
                    }

                    c1 = Color.FromArgb( rgbValues[index + 2], rgbValues[index + 1], rgbValues[index] );
                    gc1 = this.GetMonoColor( c1 );
                    index = CoordsToIndex( x + 1, y0, bmpData.Stride );
                    if ( index < 0 || index > bytes - 1 )
                    {
                        break;
                    }

                    c2 = Color.FromArgb( rgbValues[index + 2], rgbValues[index + 1], rgbValues[index] );
                    gc2 = this.GetMonoColor( c2 );
                    if ( gc1 == gfg && gc2 == gbg || gc1 == gbg && gc2 == gfg )
                    {
                        if ( gc1 == gfg && gc2 == gbg )
                        {
                            start_direction = 4;
                        }

                        if ( gc1 == gbg && gc2 == gfg )
                        {
                            start_direction = 0;
                        }

                        hitborder = true;
                        x1 = x;
                        y1 = y;
                        break;
                    }
                }
                catch ( Exception )
                {
                    break;
                }
            }

            if ( !hitborder )
            {
                return string.Empty;
            }

            var cn = new Color[(8 * n)];
            int count = 0;
            int countlimit = 10000;
            x = x1;
            y = y1;
            while ( !hitstart )
            {
                count += 1;

                // fall back to prevent infinite loop
                if ( count > countlimit )
                {
                    return string.Empty;
                }

                int diffx;
                int diffy;
                // getting all the neighbors' pixel color
                try
                {
                    int index1;

                    // processing top neighbors left to right
                    for ( int i = 0, loopTo2 = 2 * n; i <= loopTo2; i++ )
                    {
                        diffx = i - n;
                        index1 = CoordsToIndex( x + diffx, y - n, bmpData.Stride );
                        cn[i] = x + diffx >= 0 && x + diffx < max_width && y - n >= 0 && y - n < max_height ? Color.FromArgb( rgbValues[index1 + 2], rgbValues[index1 + 1], rgbValues[index1] ) : Color.Empty;
                    }

                    // processing right neighbors top to bottom
                    for ( int i = 2 * n + 1, loopTo3 = 4 * n - 1; i <= loopTo3; i++ )
                    {
                        diffy = i - 3 * n;
                        index1 = CoordsToIndex( x + n, y + diffy, bmpData.Stride );
                        cn[i] = x + n >= 0 && x + n < max_width && y + diffy >= 0 && y + diffy < max_height ? Color.FromArgb( rgbValues[index1 + 2], rgbValues[index1 + 1], rgbValues[index1] ) : Color.Empty;
                    }

                    // processing bottom neighbors right to left
                    for ( int i = 4 * n, loopTo4 = 6 * n; i <= loopTo4; i++ )
                    {
                        diffx = i - 5 * n;
                        index1 = CoordsToIndex( x - diffx, y + n, bmpData.Stride );
                        cn[i] = x - diffx >= 0 && x - diffx < max_width && y + n >= 0 && y + n < max_height ? Color.FromArgb( rgbValues[index1 + 2], rgbValues[index1 + 1], rgbValues[index1] ) : Color.Empty;
                    }

                    // processing left neighbors bottom to top
                    for ( int i = 6 * n + 1, loopTo5 = 8 * n - 1; i <= loopTo5; i++ )
                    {
                        diffy = i - 7 * n;
                        index1 = CoordsToIndex( x - n, y - diffy, bmpData.Stride );
                        cn[i] = x - n >= 0 && x - n < max_width && y - diffy >= 0 && y - diffy < max_height ? Color.FromArgb( rgbValues[index1 + 2], rgbValues[index1 + 1], rgbValues[index1] ) : Color.Empty;
                    }
                }
                catch ( Exception e )
                {
                    _ = MessageBox.Show( e.ToString() );
                    return string.Empty;
                }

                int index = 0;
                bool dir_found = false;

                // find the first valid foreground pixel				
                for ( int i = start_direction, loopTo6 = start_direction + (8 * n - 1); i <= loopTo6; i++ )
                {
                    index = i % (8 * n);
                    if ( !cn[index].Equals( Color.Empty ) )
                    {
                        if ( this.GetMonoColor( cn[index] ) == gfg )
                        {
                            current_direction = index;
                            dir_found = true;
                            break;
                        }
                    }
                }


                // if no foreground pixel found, just find the next valid pixel 

                if ( !dir_found )
                {
                    for ( int i = start_direction, loopTo7 = start_direction + (8 * n - 1); i <= loopTo7; i++ )
                    {
                        index = i % (8 * n);
                        if ( !cn[index].Equals( Color.Empty ) )
                        {
                            current_direction = index;
                            break;
                        }
                    }
                }


                // find the next direction to look for foreground pixels
                if ( index >= 0 && index <= 2 * n )
                {
                    diffx = index - n;
                    x += diffx;
                    y -= n;
                }

                if ( index > 2 * n && index < 4 * n )
                {
                    diffy = index - 3 * n;
                    x += n;
                    y += diffy;
                }

                if ( index >= 4 * n && index <= 6 * n )
                {
                    diffx = index - 5 * n;
                    x -= diffx;
                    y += n;
                }

                if ( index > 6 * n && index < 8 * n )
                {
                    diffy = index - 7 * n;
                    x -= n;
                    y -= diffy;
                }



                // store the found outline
                string tests = x + "," + y + ";";
                s += tests;
                start_direction = (current_direction + 4 * n + 1) % (8 * n);

                // adaptive stop condition
                bool bMinCountOK = n > 1 ? count > max_height / 5 : count > 10;
                if ( bMinCountOK && Math.Abs( x - x1 ) < n + 1 && Math.Abs( y - y1 ) < n + 1 )
                {
                    hitstart = true;
                }
            }

            return s;
        }
    }
}
