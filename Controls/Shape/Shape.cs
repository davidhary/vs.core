using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace isr.Core.Controls
{

    /// <summary> Shape. </summary>
    /// <remarks>
    /// Supports VB6 Shape controls. (c) 2014 Integrated Scientific Resources, Inc. All rights
    /// reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-10-13, </para><para>
    /// David, 2014-10-13, 2.1.5399. </para>
    /// </remarks>
    public partial class Shape : Forma.ModelViewBase
    {

        #region " CONSTRUCTION "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public Shape() : base()
        {
            this.InitializeComponent();
            this.SetStyle( ControlStyles.UserPaint, true );
            this.BackColor = Color.Transparent;
            this._FillBrush = new HatchBrush( HatchStyle.Horizontal, Color.Transparent, Color.Transparent );
            this._BorderPen = new Pen( SystemColors.WindowText );
            this._BorderColor = SystemColors.WindowText;
            this._ShapeStyle = ShapeStyle.Rectangle;
            this._BackStyle = ShapeBackStyle.Transparent;
            this._FillStyle = ShapeFillStyle.Solid;
            this._BorderStyle = ShapeBorderStyle.Solid;
            this._BorderWidth = 1;
            this._FillColor = Color.Black;
            this._RoundPercent = 0.15d;
            this.SelectBrush();
            this.SelectPen();
        }

        /// <summary> Cleans up any resources being used. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> <c>true</c> if managed resources should be disposed; otherwise,
        /// false. </param>
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                        this.components = null;
                    }

                    if ( this._BorderPen is object )
                    {
                        this._BorderPen.Dispose();
                        this._BorderPen = null;
                    }

                    if ( this._FillBrush is object )
                    {
                        this._FillBrush.Dispose();
                        this._FillBrush = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " SHAPE PROPERTIES "

        /// <summary>
        /// Brush used to paint the Shape control.
        /// </summary>
        private HatchBrush _FillBrush;

        // Private _BackColor As Color

        /// <summary> Background Color to display text and graphics. </summary>
        /// <value> The color of the back. </value>
        [Description( "Returns/sets the background color used to display text and graphics in an object." )]
        [Category( "Appearance" )]
        public override Color BackColor
        {
            get => base.BackColor;

            set => base.BackColor = value;// Me.SelectBrush()// Me.Refresh()
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.BackColorChanged" /> event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnBackColorChanged( EventArgs e )
        {
            base.OnBackColorChanged( e );
            this.SelectBrush();
            this.Refresh();
        }

        /// <summary>
        /// Stores the Back Style property.
        /// </summary>
        private ShapeBackStyle _BackStyle;

        /// <summary>
        /// Indicates whether a Label or the background of a Shape is transparent or opaque.
        /// </summary>
        /// <value> The back style. </value>
        [Description( "Indicates whether a Label or the background of a Shape is transparent or opaque." )]
        [Category( "Appearance" )]
        public ShapeBackStyle BackStyle
        {
            get => this._BackStyle;

            set {
                this._BackStyle = value;
                this.SelectBrush();
                this.Refresh();
            }
        }

        /// <summary>
        /// Pen used to paint the border of the Shape Control.
        /// </summary>
        private Pen _BorderPen;

        /// <summary> Stores the BorderColor property. </summary>
        private Color _BorderColor;

        /// <summary> Color of the Shape border. </summary>
        /// <value> The color of the border. </value>
        [Description( "Returns/sets the color of an object's border." )]
        [Category( "Appearance" )]
        public Color BorderColor
        {
            get => this._BorderColor;

            set {
                this._BorderColor = value;
                this.SelectPen();
                this.Refresh();
            }
        }

        /// <summary> Select pen. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void SelectPen()
        {
            this._BorderPen.Width = this.BorderWidth;
            this._BorderPen.DashOffset = 1500f;
            switch ( this.BorderStyle )
            {
                case ShapeBorderStyle.None:
                    {
                        this._BorderPen.Color = Color.Transparent;
                        break;
                    }

                case ShapeBorderStyle.Solid:
                    {
                        this._BorderPen.Color = this.BorderColor;
                        this._BorderPen.DashStyle = DashStyle.Solid;
                        break;
                    }

                case ShapeBorderStyle.Dash:
                    {
                        this._BorderPen.Color = this.BorderColor;
                        this._BorderPen.DashStyle = DashStyle.Dash;
                        break;
                    }

                case ShapeBorderStyle.Dot:
                    {
                        this._BorderPen.Color = this.BorderColor;
                        this._BorderPen.DashStyle = DashStyle.Dot;
                        break;
                    }

                case ShapeBorderStyle.DashDot:
                    {
                        this._BorderPen.Color = this.BorderColor;
                        this._BorderPen.DashStyle = DashStyle.DashDot;
                        break;
                    }

                case ShapeBorderStyle.DashDotDot:
                    {
                        this._BorderPen.Color = this.BorderColor;
                        this._BorderPen.DashStyle = DashStyle.DashDotDot;
                        break;
                    }

                default:
                    {
                        this._BorderStyle = ShapeBorderStyle.Solid;
                        this._BorderPen.Color = this.BorderColor;
                        this._BorderPen.DashStyle = DashStyle.Solid;
                        break;
                    }
            }
        }

        /// <summary> Stores the Border Style property. </summary>
        private ShapeBorderStyle _BorderStyle;

        /// <summary> Border style of the Shape control. </summary>
        /// <value> The border style. </value>
        [Description( "Returns/sets the border style for an object." )]
        [Category( "Appearance" )]
        public new ShapeBorderStyle BorderStyle
        {
            get => this._BorderStyle;

            set {
                this._BorderStyle = value;
                this.SelectPen();
                this.Refresh();
            }
        }


        /// <summary> Stores the BorderWidth property. </summary>
        private int _BorderWidth;

        /// <summary> Width of the Shape border. </summary>
        /// <value> The width of the border. </value>
        [Description( "Returns or sets the width of a control's border." )]
        [Category( "Appearance" )]
        public int BorderWidth
        {
            get => this._BorderWidth;

            set {
                this._BorderWidth = value;
                this._BorderPen.Width = this.BorderWidth;
                this.Refresh();
            }
        }

        /// <summary>
        /// Stores FillColor property.
        /// </summary>
        private Color _FillColor;

        /// <summary> Color to fill in Shape control. </summary>
        /// <value> The color of the fill. </value>
        [Description( "Returns/sets the color used to fill in shapes, circles, and boxes" )]
        [Category( "Appearance" )]
        public Color FillColor
        {
            get => this._FillColor;

            set {
                this._FillColor = value;
                this.SelectBrush();
                this.Refresh();
            }
        }

        /// <summary> Select brush. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void SelectBrush()
        {
            switch ( this.FillStyle )
            {
                case ShapeFillStyle.Solid:
                    {
                        this._FillBrush = new HatchBrush( HatchStyle.Horizontal, this.FillColor, this.FillColor );
                        break;
                    }

                case ShapeFillStyle.HorizontalLine:
                    {
                        this._FillBrush = this.BackStyle == ShapeBackStyle.Opaque ? new HatchBrush( HatchStyle.Horizontal, this.FillColor, this.BackColor ) : new HatchBrush( HatchStyle.Horizontal, this.FillColor, Color.Transparent );
                        break;
                    }

                case ShapeFillStyle.VerticalLine:
                    {
                        this._FillBrush = this.BackStyle == ShapeBackStyle.Opaque ? new HatchBrush( HatchStyle.Vertical, this.FillColor, this.BackColor ) : new HatchBrush( HatchStyle.Vertical, this.FillColor, Color.Transparent );
                        break;
                    }

                case ShapeFillStyle.DownwardDiagonal:
                    {
                        this._FillBrush = this.BackStyle == ShapeBackStyle.Opaque ? new HatchBrush( HatchStyle.WideDownwardDiagonal, this.FillColor, this.BackColor ) : new HatchBrush( HatchStyle.WideDownwardDiagonal, this.FillColor, Color.Transparent );
                        break;
                    }

                case ShapeFillStyle.UpwardDiagonal:
                    {
                        this._FillBrush = this.BackStyle == ShapeBackStyle.Opaque ? new HatchBrush( HatchStyle.WideUpwardDiagonal, this.FillColor, this.BackColor ) : new HatchBrush( HatchStyle.WideUpwardDiagonal, this.FillColor, Color.Transparent );
                        break;
                    }

                case ShapeFillStyle.Cross:
                    {
                        this._FillBrush = this.BackStyle == ShapeBackStyle.Opaque ? new HatchBrush( HatchStyle.Cross, this.FillColor, this.BackColor ) : new HatchBrush( HatchStyle.Cross, this.FillColor, Color.Transparent );
                        break;
                    }

                case ShapeFillStyle.DiagonalCross:
                    {
                        this._FillBrush = this.BackStyle == ShapeBackStyle.Opaque ? new HatchBrush( HatchStyle.DiagonalCross, this.FillColor, this.BackColor ) : new HatchBrush( HatchStyle.DiagonalCross, this.FillColor, Color.Transparent );
                        break;
                    }

                case ShapeFillStyle.Transparent:
                    {
                        this._FillBrush = this.BackStyle == ShapeBackStyle.Transparent ? new HatchBrush( HatchStyle.Horizontal, Color.Transparent, Color.Transparent ) : new HatchBrush( HatchStyle.Horizontal, this.BackColor, this.BackColor );
                        break;
                    }

                default:
                    {
                        this._FillBrush = this.BackStyle == ShapeBackStyle.Opaque ? new HatchBrush( HatchStyle.Horizontal, this.FillColor, this.BackColor ) : new HatchBrush( HatchStyle.DiagonalCross, this.FillColor, Color.Transparent );
                        break;
                    }
            }
        }

        /// <summary>
        /// Stores FillStyle property.
        /// </summary>
        private ShapeFillStyle _FillStyle;

        /// <summary> FillStyle in Shape control. </summary>
        /// <value> The fill style. </value>
        [Description( "Returns/sets the fill style of a shape" )]
        [Category( "Appearance" )]
        public ShapeFillStyle FillStyle
        {
            get => this._FillStyle;

            set {
                this._FillStyle = value;
                this.SelectBrush();
                this.Refresh();
            }
        }

        /// <summary>
        /// Stores the Shape style property.
        /// </summary>
        private ShapeStyle _ShapeStyle;

        /// <summary> The kind of Shape. </summary>
        /// <value> The shape. </value>
        [Description( "Returns/sets a value indicating the appearance of a control" )]
        [Category( "Appearance" )]
        public ShapeStyle ShapeStyle
        {
            get => this._ShapeStyle;

            set {
                this._ShapeStyle = value;
                this.Refresh();
            }
        }

        /// <summary>
        /// Stores the RoundPercent property.
        /// </summary>
        private double _RoundPercent;

        /// <summary>
        /// Adds a property to specify the percent used to round the corners in round rectangles and
        /// round squares.
        /// </summary>
        /// <value> The round percent. </value>
        [Description( "Allows to specify the percent used to round the corners of round rectangles and round squares" )]
        public int RoundPercent
        {
            get => ( int ) Math.Floor( this._RoundPercent * 100d );

            set {
                value = value < 1 ? 1 : value > 50 ? 50 : value;
                this._RoundPercent = value / 100d;
                this.Refresh();
            }
        }

        #endregion

        #region " PAINT "

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.Paint" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnPaint( PaintEventArgs e )
        {
            base.OnPaint( e );
            if ( e is object )
            {
                var _ClientRectangle = new Rectangle( 0, 0, this.ClientRectangle.Width - 1, this.ClientRectangle.Height - 1 );
                switch ( this.ShapeStyle )
                {
                    case ShapeStyle.Rectangle:
                        {
                            this.DrawRectangle( _ClientRectangle, e.Graphics );
                            break;
                        }

                    case ShapeStyle.Square:
                        {
                            this.DrawSquare( _ClientRectangle, e.Graphics );
                            break;
                        }

                    case ShapeStyle.Oval:
                        {
                            this.DrawOval( _ClientRectangle, e.Graphics );
                            break;
                        }

                    case ShapeStyle.Circle:
                        {
                            this.DrawCircle( _ClientRectangle, e.Graphics );
                            break;
                        }

                    case ShapeStyle.RoundRectangle:
                        {
                            this.DrawRoundRectangle( _ClientRectangle, e.Graphics );
                            break;
                        }

                    case ShapeStyle.RoundSquare:
                        {
                            this.DrawRoundSquare( _ClientRectangle, e.Graphics );
                            break;
                        }
                }
            }
        }

        /// <summary> Raises the system. event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnResize( EventArgs e )
        {
            base.OnResize( e );
            // Me.Refresh()
        }

        /// <summary> Draws a round square. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="clientRectangle"> . </param>
        /// <param name="g">               . </param>
        private void DrawRoundSquare( Rectangle clientRectangle, Graphics g )
        {
            int maxDiameter = Math.Min( clientRectangle.Height, clientRectangle.Width );
            var newClientRectangle = new Rectangle( clientRectangle.Location.X + (clientRectangle.Width - maxDiameter) / 2, clientRectangle.Location.Y + (clientRectangle.Height - maxDiameter) / 2, maxDiameter, maxDiameter );
            this.DrawRoundRectangle( newClientRectangle, g );
        }

        /// <summary> Draws a round rectangle. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="clientRectangle"> The region where to draw. </param>
        /// <param name="g">               The GDI used to draw the rectangle. </param>
        private void DrawRoundRectangle( Rectangle clientRectangle, Graphics g )
        {
            double percentX = clientRectangle.Width * this.RoundPercent;
            double percentY = clientRectangle.Height * this.RoundPercent;
            double minPercent = Math.Min( percentX, percentY );
            double halfPercentX = percentX / 2d;
            double halfPercentY = percentY / 2d;
            double minHalfPercent = Math.Min( halfPercentX, halfPercentY );
            var pUp1 = new PointF( ( float ) (clientRectangle.X + minPercent), clientRectangle.Y );
            var pUp2 = new PointF( ( float ) (clientRectangle.X + clientRectangle.Width - minPercent), clientRectangle.Y );
            var pDown1 = new PointF( ( float ) (clientRectangle.X + clientRectangle.Width - minPercent), clientRectangle.Y + clientRectangle.Height );
            var pDown2 = new PointF( ( float ) (clientRectangle.X + minPercent), clientRectangle.Y + clientRectangle.Height );
            var pLeft1 = new PointF( clientRectangle.X, ( float ) (clientRectangle.Y + clientRectangle.Height - minPercent) );
            var pLeft2 = new PointF( clientRectangle.X, ( float ) (clientRectangle.Y + minPercent) );
            var pRight1 = new PointF( clientRectangle.X + clientRectangle.Width, ( float ) (clientRectangle.Y + minPercent) );
            var pRight2 = new PointF( clientRectangle.X + clientRectangle.Width, ( float ) (clientRectangle.Y + clientRectangle.Height - minPercent) );
            var pCornerA1 = new PointF( clientRectangle.X, ( float ) (clientRectangle.Y + minHalfPercent) );
            var pCornerA2 = new PointF( ( float ) (clientRectangle.X + minHalfPercent), clientRectangle.Y );
            var pCornerB1 = new PointF( ( float ) (clientRectangle.X + clientRectangle.Width - minHalfPercent), clientRectangle.Y );
            var pCornerB2 = new PointF( clientRectangle.X + clientRectangle.Width, ( float ) (clientRectangle.Y + minHalfPercent) );
            var pCornerC1 = new PointF( clientRectangle.X + clientRectangle.Width, ( float ) (clientRectangle.Y + clientRectangle.Height - minHalfPercent) );
            var pCornerC2 = new PointF( ( float ) (clientRectangle.X + clientRectangle.Width - minHalfPercent), clientRectangle.Y + clientRectangle.Height );
            var pCornerD1 = new PointF( ( float ) (clientRectangle.X + minHalfPercent), clientRectangle.Y + clientRectangle.Height );
            var pCornerD2 = new PointF( clientRectangle.X, ( float ) (clientRectangle.Y + clientRectangle.Height - minHalfPercent) );
            if ( this._BackStyle != ShapeBackStyle.Transparent || this._FillStyle != ShapeFillStyle.Transparent )
            {
                using var gPath = new GraphicsPath();
                gPath.AddLine( pUp1, pUp2 );
                gPath.AddBezier( pUp2, pCornerB1, pCornerB2, pRight1 );
                gPath.AddLine( pRight1, pRight2 );
                gPath.AddBezier( pRight2, pCornerC1, pCornerC2, pDown1 );
                gPath.AddLine( pDown1, pDown2 );
                gPath.AddBezier( pDown2, pCornerD1, pCornerD2, pLeft1 );
                gPath.AddLine( pLeft1, pLeft2 );
                gPath.AddBezier( pLeft2, pCornerA1, pCornerA2, pUp1 );
                using var region = new Region( gPath );
                g.FillRegion( this._FillBrush, region );
            }

            g.DrawLine( this._BorderPen, pUp1, pUp2 );
            g.DrawLine( this._BorderPen, pDown1, pDown2 );
            g.DrawLine( this._BorderPen, pLeft1, pLeft2 );
            g.DrawLine( this._BorderPen, pRight1, pRight2 );
            g.DrawBezier( this._BorderPen, pLeft2, pCornerA1, pCornerA2, pUp1 );
            g.DrawBezier( this._BorderPen, pUp2, pCornerB1, pCornerB2, pRight1 );
            g.DrawBezier( this._BorderPen, pRight2, pCornerC1, pCornerC2, pDown1 );
            g.DrawBezier( this._BorderPen, pDown2, pCornerD1, pCornerD2, pLeft1 );
        }

        /// <summary> Draws a circle. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="clientRectangle"> The region where to draw. </param>
        /// <param name="g">               The GDI used to draw the rectangle. </param>
        private void DrawCircle( Rectangle clientRectangle, Graphics g )
        {
            int maxDiameter = Math.Min( clientRectangle.Height, clientRectangle.Width );
            var newClientRectangle = new Rectangle( clientRectangle.Location.X + (clientRectangle.Width - maxDiameter) / 2, clientRectangle.Location.Y + (clientRectangle.Height - maxDiameter) / 2, maxDiameter, maxDiameter );
            if ( this._BackStyle != ShapeBackStyle.Transparent || this._FillStyle != ShapeFillStyle.Transparent )
            {
                g.FillEllipse( this._FillBrush, newClientRectangle );
            }

            g.DrawEllipse( this._BorderPen, newClientRectangle );
        }

        /// <summary> Draws an oval. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="clientRectangle"> The region where to draw. </param>
        /// <param name="g">               The GDI used to draw the rectangle. </param>
        private void DrawOval( Rectangle clientRectangle, Graphics g )
        {
            if ( this.BackStyle != ShapeBackStyle.Transparent || this.FillStyle != ShapeFillStyle.Transparent )
            {
                g.FillEllipse( this._FillBrush, clientRectangle );
            }

            g.DrawEllipse( this._BorderPen, clientRectangle );
        }

        /// <summary> Draws a square. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="clientRectangle"> The region where to draw. </param>
        /// <param name="g">               The GDI used to draw the rectangle. </param>
        private void DrawSquare( Rectangle clientRectangle, Graphics g )
        {
            int maxDiameter = Math.Min( clientRectangle.Height, clientRectangle.Width );
            var newClientRectangle = new Rectangle( clientRectangle.Location.X + (clientRectangle.Width - maxDiameter) / 2, clientRectangle.Location.Y + (clientRectangle.Height - maxDiameter) / 2, maxDiameter, maxDiameter );
            if ( this._BackStyle != ShapeBackStyle.Transparent || this._FillStyle != ShapeFillStyle.Transparent )
            {
                g.FillRectangle( this._FillBrush, newClientRectangle );
            }

            g.DrawRectangle( this._BorderPen, newClientRectangle );
        }

        /// <summary> Draws a rectangle. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="clientRectangle"> The region where to draw. </param>
        /// <param name="g">               The GDI used to draw the rectangle. </param>
        private void DrawRectangle( Rectangle clientRectangle, Graphics g )
        {
            if ( this.BackStyle != ShapeBackStyle.Transparent || this.FillStyle != ShapeFillStyle.Transparent )
            {
                g.FillRectangle( this._FillBrush, clientRectangle );
            }

            g.DrawRectangle( this._BorderPen, clientRectangle );
        }

        /// <summary> Overriding <see cref="CreateParams"/> method from UserControl. </summary>
        /// <value> Options that control the create. </value>
        protected override CreateParams CreateParams
        {
            get {
                var cp = base.CreateParams;
                cp.ExStyle |= 0x20; // WS_EX_TRANSPARENT
                return cp;
            }
        }

        /// <summary> Overriding OnPaintBackground method from UserControl. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="pevent"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnPaintBackground( PaintEventArgs pevent )
        {
            // do not allow the background to be painted  
        }
        #endregion

    }

    /// <summary> Values that represent Shape Style. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public enum ShapeStyle
    {

        /// <summary> An enum constant representing the rectangle option. </summary>
        Rectangle = 0,

        /// <summary> An enum constant representing the square option. </summary>
        Square = 1,

        /// <summary> An enum constant representing the oval option. </summary>
        Oval = 2,

        /// <summary> An enum constant representing the circle option. </summary>
        Circle = 3,

        /// <summary> An enum constant representing the round rectangle option. </summary>
        RoundRectangle = 4,

        /// <summary> An enum constant representing the round square option. </summary>
        RoundSquare = 5
    }

    /// <summary> Values that represent BackStyle. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public enum ShapeBackStyle
    {

        /// <summary> An enum constant representing the transparent option. </summary>
        Transparent = 0,

        /// <summary> An enum constant representing the opaque option. </summary>
        Opaque = 1
    }

    /// <summary> Values that represent ShapeBorderStyle. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public enum ShapeBorderStyle
    {

        /// <summary> An enum constant representing the none option. </summary>
        None,

        /// <summary> An enum constant representing the solid option. </summary>
        Solid,

        /// <summary> An enum constant representing the dash option. </summary>
        Dash,

        /// <summary> An enum constant representing the dot option. </summary>
        Dot,

        /// <summary> An enum constant representing the dash dot option. </summary>
        DashDot,

        /// <summary> An enum constant representing the dash dot option. </summary>
        DashDotDot
    }

    /// <summary> Values that represent ShapeFillStyle. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public enum ShapeFillStyle
    {

        /// <summary> An enum constant representing the solid option. </summary>
        Solid = 0,

        /// <summary> An enum constant representing the transparent option. </summary>
        Transparent = 1,

        /// <summary> An enum constant representing the horizontal line option. </summary>
        HorizontalLine = 2,

        /// <summary> An enum constant representing the vertical line option. </summary>
        VerticalLine = 3,

        /// <summary> An enum constant representing the downward diagonal option. </summary>
        DownwardDiagonal = 4,

        /// <summary> An enum constant representing the upward diagonal option. </summary>
        UpwardDiagonal = 5,

        /// <summary> An enum constant representing the cross option. </summary>
        Cross = 6,

        /// <summary> An enum constant representing the diagonal cross option. </summary>
        DiagonalCross = 7
    }
}
