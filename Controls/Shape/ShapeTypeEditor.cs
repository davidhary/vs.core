using System;
using System.Drawing;
using System.Drawing.Design;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Windows.Forms.Design;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Controls
{

    /// <summary> A shape type editor control. </summary>
    /// <remarks>
    /// (c) 2016 Yang Kok Wah. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-05-14 </para><para>
    /// http://www.codeproject.com/Articles/10558/Shape-Control-for-NET. </para>
    /// </remarks>
    internal class ShapeTypeEditorControl : UserControl
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="initial_shape"> The initial shape. </param>
        public ShapeTypeEditorControl( ShapeType initial_shape ) : base()
        {
            this.InitializeComponent();
            this.Shape = initial_shape;

            // Find the number of shapes in the enumeration
            int numshape = Enum.GetValues( typeof( ShapeType ) ).GetLength( 0 );
            int numrow = ( int ) Math.Truncate( Math.Sqrt( numshape ) );
            // Find the number of rows and columns to accommodate the shapes
            int numcol = numshape / numrow;
            if ( numshape % numcol > 0 )
            {
                numcol += 1;
            }

            // Record the specifications
            this._Numrow = numrow;
            this._Numcol = numcol;
            this._Valid_width = this._Numcol * this._Width + (this._Numcol - 1) * 6 + 2 * 4;
            this._Valid_height = this._Numrow * this._Height + (this._Numrow - 1) * 6 + 2 * 4;
        }

        /// <summary> Initializes the component. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void InitializeComponent()
        {
            // 
            // ShapeTypeEditorControl
            // 
            this.BackColor = Color.LightGray;
            this.Name = "ShapeTypeEditorControl";
        }

        /// <summary> Gets or sets the shape type. </summary>
        /// <value> The shape. </value>
        public ShapeType Shape { get; set; }

        // Specification for the UI

        // number of shapes
        // Private _Numshape As Integer

#pragma warning disable IDE0044 // Add readonly modifier
        // number of rows
        private int _Numrow;
        // number of columns
        private int _Numcol;
        // width of each shape
        private int _Valid_width;
        // height of each shape
        private int _Valid_height;
        private int _Width = 20;
        private int _Height = 20;
#pragma warning restore IDE0044 // Add readonly modifier

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.Paint" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnPaint( PaintEventArgs e )
        {
            if ( e is null )
            {
                return;
            }

            using var bm = new Bitmap( this.Width, this.Height, e.Graphics );
            var g = Graphics.FromImage( bm );
            g.FillRectangle( Brushes.LightGray, new Rectangle( 0, 0, bm.Width, bm.Height ) );
            e.Graphics.FillRectangle( Brushes.LightGray, new Rectangle( 0, 0, this.Width, this.Height ) );
            int x = 4;
            int y = 4;
            int n = 0;
            foreach ( ShapeType shape in Enum.GetValues( typeof( ShapeType ) ) )
            {
                using var path = new GraphicsPath();
                var argoutline = path;
                ShapeControl.UpdateOutline( ref argoutline, shape, this._Width, this._Height );
                g.FillRectangle( Brushes.LightGray, 0, 0, bm.Width, bm.Height );
                g.FillPath( Brushes.Yellow, path );
                g.DrawPath( Pens.Red, path );
                e.Graphics.DrawImage( bm, x, y, new Rectangle( new Point( 0, 0 ), new Size( this._Width + 1, this._Height + 1 ) ), GraphicsUnit.Pixel );
                if ( this.Shape.Equals( shape ) )
                {
                    e.Graphics.DrawRectangle( Pens.Red, new Rectangle( new Point( x - 2, y - 2 ), new Size( this._Width + 4, this._Height + 4 ) ) );
                }

                n += 1;
                x = n % this._Numcol * this._Width;
                x = x + n % this._Numcol * 6 + 4;
                y = n / this._Numcol * this._Height;
                y = y + n / this._Numcol * 6 + 4;
            }
        }

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.MouseDown" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnMouseDown( MouseEventArgs e )
        {
            if ( e is null )
            {
                return;
            }

            if ( e.Button.Equals( MouseButtons.Left ) )
            {
                if ( e.X > this._Valid_width )
                {
                    return;
                }

                if ( e.Y > this._Valid_height )
                {
                    return;
                }

                int x;
                int y;
                int n;
                x = e.X;
                y = e.Y;
                n = y / (this._Valid_height / this._Numrow) * this._Numcol + x / (this._Valid_width / this._Numcol) % this._Numcol;
                int count = 0;
                foreach ( ShapeType shape in Enum.GetValues( typeof( ShapeType ) ) )
                {
                    if ( count == n )
                    {
                        this.Shape = shape;
                        // close the editor immediately
                        SendKeys.Send( "{ENTER}" );
                    }

                    count += 1;
                }
            }
        }
    }

    /// <summary> Editor for shape type. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public class ShapeTypeEditor : UITypeEditor
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Drawing.Design.UITypeEditor" /> class.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public ShapeTypeEditor()
        {
        }

        /// <summary>
        /// Gets the editor style used by the
        /// <see cref="M:System.Drawing.Design.UITypeEditor.EditValue(System.IServiceProvider,System.Object)" />
        /// method.
        /// </summary>
        /// <remarks>
        /// David, 2020-09-24. Indicates whether the UITypeEditor provides a form-based (modal) dialog,
        /// drop down dialog, or no UI outside of the properties window.
        /// </remarks>
        /// <param name="context"> An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that
        /// can be used to gain additional context information. </param>
        /// <returns>
        /// A <see cref="T:System.Drawing.Design.UITypeEditorEditStyle" /> value that indicates the style
        /// of editor used by the
        /// <see cref="M:System.Drawing.Design.UITypeEditor.EditValue(System.IServiceProvider,System.Object)" />
        /// method. If the <see cref="T:System.Drawing.Design.UITypeEditor" /> does not support this
        /// method, then <see cref="M:System.Drawing.Design.UITypeEditor.GetEditStyle" /> will return
        /// <see cref="F:System.Drawing.Design.UITypeEditorEditStyle.None" />.
        /// </returns>
        [System.Security.Permissions.PermissionSet( System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust" )]
        public override UITypeEditorEditStyle GetEditStyle( System.ComponentModel.ITypeDescriptorContext context )
        {
            return UITypeEditorEditStyle.DropDown;
        }

        /// <summary>
        /// Edits the specified object's value using the editor style indicated by the
        /// <see cref="M:System.Drawing.Design.UITypeEditor.GetEditStyle" /> method.
        /// </summary>
        /// <remarks> David, 2020-09-24. Displays the UI for value selection. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="context">  An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that
        /// can be used to gain additional context information. </param>
        /// <param name="provider"> An <see cref="T:System.IServiceProvider" /> that this editor can use
        /// to obtain services. </param>
        /// <param name="value">    The object to edit. </param>
        /// <returns>
        /// The new value of the object. If the value of the object has not changed, this should return
        /// the same object it was passed.
        /// </returns>
        [System.Security.Permissions.PermissionSet( System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust" )]
        public override object EditValue( System.ComponentModel.ITypeDescriptorContext context, IServiceProvider provider, object value )
        {
            if ( provider is null )
            {
                throw new ArgumentNullException( nameof( provider ) );
            }
            // Return the value if the value is not of type ShapeType
            if ( value is null || !ReferenceEquals( value.GetType(), typeof( ShapeType ) ) )
            {
                return value;
            }

            // Uses the IWindowsFormsEditorService to display a 
            // drop-down UI in the Properties window.
            IWindowsFormsEditorService edSvc = ( IWindowsFormsEditorService ) provider.GetService( typeof( IWindowsFormsEditorService ) );
            if ( edSvc is object )
            {
                // Display an Shape Type Editor Control and retrieve the value.
                using var editor = new ShapeTypeEditorControl( ( ShapeType ) Conversions.ToInteger( value ) );
                edSvc.DropDownControl( editor );
                // Return the value in the appropriate data format.
                if ( ReferenceEquals( value.GetType(), typeof( ShapeType ) ) )
                {
                    var result = editor.Shape;
                    return result;
                }
            }

            return value;
        }

        /// <summary>
        /// Paints a representation of the value of an object using the specified
        /// <see cref="T:System.Drawing.Design.PaintValueEventArgs" />.
        /// </summary>
        /// <remarks> David, 2020-09-24. Draws a representation of the property's value. </remarks>
        /// <param name="e"> A <see cref="T:System.Drawing.Design.PaintValueEventArgs" /> that indicates
        /// what to paint and where to paint it. </param>
        [System.Security.Permissions.PermissionSet( System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust" )]
        public override void PaintValue( PaintValueEventArgs e )
        {
            if ( e is null )
            {
                return;
            }

            using var bm = new Bitmap( e.Bounds.Width + 4, e.Bounds.Height + 4, e.Graphics );
            using var g = Graphics.FromImage( bm );
            ShapeType shape = ( ShapeType ) Conversions.ToInteger( e.Value );
            using var path = new GraphicsPath();
            var argoutline = path;
            ShapeControl.UpdateOutline( ref argoutline, shape, e.Bounds.Width - 5, e.Bounds.Height - 5 );
            g.FillPath( Brushes.Yellow, path );
            g.DrawPath( Pens.Red, path );
            e.Graphics.DrawImage( bm, 3, 3, new Rectangle( new Point( 0, 0 ), new Size( e.Bounds.Width, e.Bounds.Height ) ), GraphicsUnit.Pixel );
        }

        /// <summary>
        /// Indicates whether the specified context supports painting a representation of an object's
        /// value within the specified context.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="context"> An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that
        /// can be used to gain additional context information. </param>
        /// <returns>
        /// <see langword="true" /> if
        /// <see cref="M:System.Drawing.Design.UITypeEditor.PaintValue(System.Object,System.Drawing.Graphics,System.Drawing.Rectangle)" />
        /// is implemented; otherwise, <see langword="false" />.
        /// </returns>
        [System.Security.Permissions.PermissionSet( System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust" )]
        public override bool GetPaintValueSupported( System.ComponentModel.ITypeDescriptorContext context )
        {
            return true;
        }
    }
}
