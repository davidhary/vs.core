using System.Diagnostics;
using System.Drawing;

namespace isr.Core.Controls
{
    public partial class Shape
    {
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            SuspendLayout();
            // 
            // Shape
            // 
            Name = "FlashLed";
            Size = new Size(17, 17);
            ResumeLayout(false);
        }
    }
}
