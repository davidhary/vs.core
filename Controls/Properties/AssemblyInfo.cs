using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.Core.Controls.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.Core.Controls.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.Core.Controls.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]

[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( isr.Core.Controls.My.MyLibrary.TestAssemblyStrongName )]
