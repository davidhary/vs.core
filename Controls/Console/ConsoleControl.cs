using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

using isr.Core.Controls.ExceptionExtensions;

using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Controls
{

    /// <summary> A console control. </summary>
    /// <remarks>
    /// This controls displays provides a 80x25 character display and entry console. The console
    /// cannot be resized at this time nor can the text scroll. <para>
    /// (c) 2012 icemanind. All rights reserved.</para><para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-07-09, http://www.codeproject.com/Articles/1053951/Console-Control.
    /// </para>
    /// </remarks>
    public class ConsoleControl : UserControl
    {

        #region " CONSTRUCTION "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public ConsoleControl() : base()
        {
            this._RenderFont = new Font( this.RenderFontName, 10f, FontStyle.Regular );
            this.KeyPress += this.ConsoleControlKeyPress;
            this.RestoreDefaults();
        }

        /// <summary> Restore defaults. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void RestoreDefaults()
        {
            this.RenderFontName = "Courier New";
            this.ControlScreenSize = new Size( 80, 25 );
            this.CharacterSize = new Size( 8, 15 );
            this.ControlSize = new Size( this.ControlScreenSize.Width * this.CharacterSize.Width + 6, this.ControlScreenSize.Height * this.CharacterSize.Height + 2 );
            this.ScreenSize = this.ControlScreenSize.Width * this.ControlScreenSize.Height;
            this.OnResizeThis();
            this._CursorX = 0;
            this._CursorY = 0;
            this._IsCursorOn = false;
            this.CursorType = CursorType.Underline;
            this.ConsoleBackgroundColor = Color.Black;
            this.ConsoleForegroundColor = Color.LightGray;
            this.CurrentForegroundColor = Color.LightGray;
            this.CurrentBackgroundColor = Color.Black;
            try
            {
                this.CursorFlashTimer = new Timer();
            }
            catch
            {
                this.CursorFlashTimer?.Dispose();
                throw;
            }

            this.CursorFlashTimer.Enabled = false;
            this.CursorFlashTimer.Interval = 500;
            this.TextBlockArray = new TextBlock[this.ScreenSize]; // 80 x 25
            for ( int i = 0, loopTo = this.ScreenSize - 1; i <= loopTo; i++ )
            {
                this.TextBlockArray[i].BackgroundColor = this.ConsoleBackgroundColor;
                this.TextBlockArray[i].ForegroundColor = this.ConsoleForegroundColor;
                this.TextBlockArray[i].Character = ControlChars.NullChar;
            }

            this._KeysBuffer = new List<char>();
            this._CommandBuffer = new List<string>();
            this._CommandBufferIndex = 0;
            this.AllowInput = true;
            this.EchoInput = true;
            this.ShowCursor = true;
        }

        /// <summary> Gets options for controlling the create. </summary>
        /// <value>
        /// A <see cref="T:System.Windows.Forms.CreateParams" />
        /// that contains the required creation parameters when the handle to the control is created.
        /// </value>
        protected override CreateParams CreateParams
        {
            get {
                var cp = base.CreateParams;
                cp.ExStyle |= 0x2000000;
                return cp;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> <see langword="true" /> to release both managed and unmanaged
        /// resources; <see langword="false" /> to release only unmanaged
        /// resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing )
                    {
                        if ( this.CursorFlashTimer is object )
                        {
                            this.CursorFlashTimer.Dispose();
                            this.CursorFlashTimer = null;
                        }

                        if ( this._RenderFont is object )
                        {
                            this._RenderFont.Dispose();
                        }

                        this.RemoveEventHandler( LineEntered );
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " EVENTS "

        /// <summary>
        /// This event is raised whenever text is entered into the console control
        /// </summary>
        public event EventHandler<LineEnteredEventArgs> LineEntered;

        /// <summary> Removes event handler. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The value. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void RemoveEventHandler( EventHandler<LineEnteredEventArgs> value )
        {
            foreach ( Delegate d in value is null ? (Array.Empty<Delegate>()) : value.GetInvocationList() )
            {
                try
                {
                    LineEntered -= ( EventHandler<LineEnteredEventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }
        }

        #endregion

        #region " APPERANCE "

        /// <summary> Name of the render font. </summary>
        /// <remarks>
        /// to_do: allow changing the font and building a new screen and character sizes.
        /// </remarks>
        /// <value> The name of the render font. </value>
        private string RenderFontName { get; set; } = "Courier New";

        /// <summary> Fixed Control Size. </summary>
        /// <value> The size of the control. </value>
        private Size ControlSize { get; set; }

        /// <summary> Gets or sets the size of the control screen. </summary>
        /// <value> The size of the control screen. </value>
        private Size ControlScreenSize { get; set; }

        /// <summary> Gets or sets the size of the character. </summary>
        /// <value> The size of the character. </value>
        private Size CharacterSize { get; set; }

        /// <summary> Size of the screen. </summary>
        /// <remarks> to_do: allow changing the screen size. </remarks>
        /// <value> The size of the screen. </value>
        private int ScreenSize { get; set; }

        /// <summary> True to show, false to hide the cursor. </summary>
        private bool _ShowCursor;

        /// <summary>
        /// Gets or sets or Sets a value indicating whether the console should show the cursor.
        /// </summary>
        /// <value> The show cursor. </value>
        public bool ShowCursor
        {
            get => this._ShowCursor;

            set {
                this._ShowCursor = value;
                this.CursorFlashTimer.Enabled = value;
            }
        }

        /// <summary>
        /// Gets or sets or Sets a value indicating whether the console should allow keyboard input.
        /// </summary>
        /// <value> The allow input. </value>
        public bool AllowInput { get; set; }

        /// <summary>
        /// Gets or sets or Sets a value indicating whether the console should echo any input it receives.
        /// </summary>
        /// <value> The echo input. </value>
        public bool EchoInput { get; set; }

        /// <summary> Type of the cursor. </summary>
        private CursorType _CursorType;

        /// <summary> Gets or sets or Sets the current cursor type. </summary>
        /// <value> The type of the cursor. </value>
        public CursorType CursorType
        {
            get => this._CursorType;

            set {
                this._CursorType = value;
                this.Invalidate();
            }
        }

        /// <summary> Gets or sets the current background color. </summary>
        /// <value> The color of the current background. </value>
        public Color CurrentBackgroundColor { get; set; }

        /// <summary> The console background color. </summary>
        private Color _ConsoleBackgroundColor;

        /// <summary>
        /// Gets or sets or Sets the background color of the console. Default is Black.
        /// </summary>
        /// <value> The color of the console background. </value>
        public Color ConsoleBackgroundColor
        {
            get => this._ConsoleBackgroundColor;

            set {
                this._ConsoleBackgroundColor = value;
                this.BackColor = value;
                this.Invalidate();
            }
        }

        /// <summary> Gets or sets the current foreground color. </summary>
        /// <value> The color of the current foreground. </value>
        public Color CurrentForegroundColor { get; set; }

        /// <summary> The console foreground color. </summary>
        private Color _ConsoleForegroundColor;

        /// <summary>
        /// Gets or sets or Sets the foreground color of the console. Default is light gray.
        /// </summary>
        /// <value> The color of the console foreground. </value>
        public Color ConsoleForegroundColor
        {
            get => this._ConsoleForegroundColor;

            set {
                this._ConsoleForegroundColor = value;
                this.ForeColor = value;
                this.Invalidate();
            }
        }

        #endregion

        #region " RENDER "

        /// <summary> Buffer for keys data. </summary>
        private List<char> _KeysBuffer;

        /// <summary> Buffer for command data. </summary>
        private List<string> _CommandBuffer;

        /// <summary> Zero-based index of the command buffer. </summary>
        private int _CommandBufferIndex;

        /// <summary> True if is cursor on, false if not. </summary>
        private bool _IsCursorOn;

        /// <summary> Process the command key. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="msg">     [in,out] A <see cref="T:System.Windows.Forms.Message" />
        /// , passed by reference, that represents the window message to process.
        /// </param>
        /// <param name="keyData"> One of the <see cref="T:System.Windows.Forms.Keys" />
        /// values that represents the key to process. </param>
        /// <returns> true if the character was processed by the control; otherwise, false. </returns>
        protected override bool ProcessCmdKey( ref Message msg, Keys keyData )
        {
            if ( keyData == Keys.Up )
            {
                if ( this._CommandBufferIndex <= 0 || !this.AllowInput )
                {
                    return true;
                }

                int len = this._KeysBuffer.Count;
                for ( int i = 0, loopTo = len - 1; i <= loopTo; i++ )
                {
                    this.ConsoleControlKeyPress( this, new KeyPressEventArgs( '\b' ) );
                }

                this._KeysBuffer.Clear();
                foreach ( char c in this._CommandBuffer[this._CommandBufferIndex - 1] )
                {
                    this._KeysBuffer.Add( c );
                    if ( this.EchoInput )
                    {
                        this.Write( c );
                    }
                }

                this._CommandBufferIndex -= 1;
                return true;
            }

            if ( keyData == Keys.Down )
            {
                if ( this._CommandBufferIndex + 1 >= this._CommandBuffer.Count || !this.AllowInput )
                {
                    return true;
                }

                int len = this._KeysBuffer.Count;
                for ( int i = 0, loopTo1 = len - 1; i <= loopTo1; i++ )
                {
                    this.ConsoleControlKeyPress( this, new KeyPressEventArgs( '\b' ) );
                }

                this._KeysBuffer.Clear();
                foreach ( char c in this._CommandBuffer[this._CommandBufferIndex + 1] )
                {
                    this._KeysBuffer.Add( c );
                    if ( this.EchoInput )
                    {
                        this.Write( c );
                    }
                }

                this._CommandBufferIndex += 1;
                return true;
            }

            return base.ProcessCmdKey( ref msg, keyData );
        }

        /// <summary> The cursor x coordinate. </summary>
        private int _CursorX;

        /// <summary> The cursor y coordinate. </summary>
        private int _CursorY;

        /// <summary> Gets an array of text blocks. </summary>
        /// <value> An array of text blocks. </value>
        private TextBlock[] TextBlockArray { get; set; }

        private void ConsoleControlKeyPress( object sender, KeyPressEventArgs e )
        {
            if ( !this.AllowInput )
            {
                return;
            }

            if ( Strings.AscW( e.KeyChar ) == 8 )
            {
                if ( this._KeysBuffer.Count == 0 )
                {
                    return;
                }

                if ( this.EchoInput )
                {
                    this.TextBlockArray[this.GetIndex()].Character = ControlChars.NullChar;
                    this._CursorX -= 1;
                    if ( this._CursorX < 0 )
                    {
                        this._CursorY -= 1;
                        this._CursorX = 79;
                        if ( this._CursorY < 0 )
                        {
                            this._CursorY += 1;
                            this._CursorX = 0;
                        }
                    }

                    this.TextBlockArray[this.GetIndex()].Character = ControlChars.NullChar;
                    this.Invalidate();
                }

                this._KeysBuffer.RemoveAt( this._KeysBuffer.Count - 1 );
                return;
            }

            this._KeysBuffer.Add( e.KeyChar );
            if ( this.EchoInput )
            {
                this.Write( e.KeyChar );
                if ( e.KeyChar == ControlChars.Cr )
                {
                    this.Write( ControlChars.Lf );
                }
            }

            if ( e.KeyChar == ControlChars.Cr )
            {
                if ( Environment.NewLine.Length == 2 )
                {
                    this._KeysBuffer.Add( ControlChars.Lf );
                }

                string s = this._KeysBuffer.Aggregate( "", ( current, c ) => current + Conversions.ToString( c ) );
                this._KeysBuffer.Clear();
                this._CommandBuffer.Add( s.Trim( ControlChars.Cr, ControlChars.Lf ) );
                this._CommandBufferIndex = this._CommandBuffer.Count;
                var evt = LineEntered;
                if ( evt is object )
                {
                    evt.Invoke( this, new LineEnteredEventArgs( s ) );
                }
            }

            this.Invalidate();
        }

        /// <summary> The with events. </summary>
        private Timer _CursorFlashTimer;

        private Timer CursorFlashTimer
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._CursorFlashTimer;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._CursorFlashTimer != null )
                {
                    this._CursorFlashTimer.Tick -= this.CursorFlashTimerTick;
                }

                this._CursorFlashTimer = value;
                if ( this._CursorFlashTimer != null )
                {
                    this._CursorFlashTimer.Tick += this.CursorFlashTimerTick;
                }
            }
        }

        private void CursorFlashTimerTick( object sender, EventArgs e )
        {
            if ( !this.ShowCursor )
            {
                return;
            }

            this._IsCursorOn = !this._IsCursorOn;
            char c;
            switch ( this.CursorType )
            {
                case CursorType.Block:
                    {
                        c = '█';
                        break;
                    }

                case CursorType.Invisible:
                    {
                        c = ' ';
                        break;
                    }

                default:
                    {
                        c = '_';
                        break;
                    }
            }

            this.TextBlockArray[this.GetIndex()].Character = this._IsCursorOn ? c : ControlChars.NullChar;
            this.Invalidate();
        }

        private readonly Font _RenderFont;

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.Paint" />
        /// event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.PaintEventArgs" />
        /// that contains the event data. </param>
        protected override void OnPaint( PaintEventArgs e )
        {
            if ( e is null )
            {
                return;
            }

            int x = 0;
            int y = 0;
            int charWidth = this.CharacterSize.Width;
            int charHeight = this.CharacterSize.Height;
            using ( var bitmap = new Bitmap( this.Width, this.Height ) )
            {
                using var g = Graphics.FromImage( bitmap );
                for ( int i = 0, loopTo = this.ScreenSize - 1; i <= loopTo; i++ )
                {
                    var fc = this.TextBlockArray[i].Character == ControlChars.NullChar ? this.ConsoleForegroundColor : this.TextBlockArray[i].ForegroundColor;
                    var bc = this.TextBlockArray[i].Character == ControlChars.NullChar ? this.ConsoleBackgroundColor : this.TextBlockArray[i].BackgroundColor;
                    using ( Brush bgBrush = new SolidBrush( bc ) )
                    {
                        g.FillRectangle( bgBrush, new Rectangle( x + 2, y + 1, charWidth, charHeight ) );
                        using Brush fgBrush = new SolidBrush( fc );
                        g.DrawString( this.TextBlockArray[i].Character == ControlChars.NullChar ? " " : this.TextBlockArray[i].Character.ToString(), this._RenderFont, fgBrush, new PointF( x, y ) );
                    }

                    x += charWidth;
                    if ( x > 79 * charWidth )
                    {
                        y += charHeight;
                        x = 0;
                    }
                }

                e.Graphics.DrawImage( bitmap, e.ClipRectangle, e.ClipRectangle, GraphicsUnit.Pixel );
            }

            base.OnPaint( e );
        }

        /// <summary> Executes the 'resize this' action. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void OnResizeThis()
        {
            this.Width = this.ControlSize.Width;
            this.Height = this.ControlSize.Height;
        }

        /// <summary> Raises the resize event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" />
        /// that contains the event data. </param>
        protected override void OnResize( EventArgs e )
        {
            base.OnResize( e );
            this.OnResizeThis();
        }

        #endregion

        #region " WRITING "

        /// <summary> Sets the position of the cursor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="row">    The row of the cursor position. </param>
        /// <param name="column"> The column of the cursor position. </param>
        public void SetCursorPosition( int row, int column )
        {
            if ( this.ShowCursor )
            {
                this.TextBlockArray[this.GetIndex()].Character = ControlChars.NullChar;
            }

            this._CursorX = column;
            this._CursorY = row;
            this.Invalidate();
        }

        /// <summary> Sets the position of the cursor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="location"> The location of the cursor position. </param>
        public void SetCursorPosition( Location location )
        {
            if ( location is object )
            {
                this.SetCursorPosition( location.Row, location.Column );
            }
        }

        /// <summary> Gets the position of the cursor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> The location of the cursor. </returns>
        public Location GetCursorPosition()
        {
            return new Location() { Column = _CursorX, Row = _CursorY };
        }

        /// <summary> Writes a newline (carriage return) to the console. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public void Write()
        {
            this.Write( Environment.NewLine );
        }

        /// <summary>
        /// Writes a character to the console using the current foreground color and background color.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The character to write to the console. </param>
        public void Write( char value )
        {
            this.Write( value, this.CurrentForegroundColor, this.CurrentBackgroundColor );
        }

        /// <summary>
        /// Writes a character to the console using the specified foreground color and background color.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value">     The character to write to the console. </param>
        /// <param name="foreColor"> The foreground color. </param>
        /// <param name="backColor"> The background color. </param>
        public void Write( char value, Color foreColor, Color backColor )
        {
            if ( Strings.AscW( value ) == 7 )
            {
                Console.Beep( 1000, 500 );
                return;
            }

            if ( Strings.AscW( value ) == 13 )
            {
                this.SetCursorPosition( this.GetCursorPosition().Row, 0 );
                return;
            }

            if ( Strings.AscW( value ) == 10 )
            {
                if ( Environment.NewLine.Length == 1 )
                {
                    this.SetCursorPosition( this.GetCursorPosition().Row, 0 );
                }

                this._CursorY += 1;
                if ( this._CursorY > 24 )
                {
                    this.ScrollUp();
                    this._CursorY = 24;
                }

                return;
            }

            this.TextBlockArray[this.GetIndex()].Character = value;
            this.TextBlockArray[this.GetIndex()].BackgroundColor = backColor;
            this.TextBlockArray[this.GetIndex()].ForegroundColor = foreColor;
            this.MoveCursorPosition();
            this.Invalidate();
        }

        /// <summary>
        /// Writes a string to the console using the current foreground color and background color.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="text"> The string to write to the console. </param>
        public void Write( string text )
        {
            this.Write( text, this.CurrentForegroundColor, this.CurrentBackgroundColor );
        }

        /// <summary>
        /// Writes a string to the console using the specified foreground color and background color.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="text">      The string to write to the console. </param>
        /// <param name="foreColor"> The foreground color. </param>
        /// <param name="backColor"> The background color. </param>
        public void Write( string text, Color foreColor, Color backColor )
        {
            if ( string.IsNullOrWhiteSpace( text ) )
            {
                return;
            }

            foreach ( char c in text )
            {
                this.Write( c, foreColor, backColor );
            }

            this.Invalidate();
        }

        /// <summary> Move cursor position. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void MoveCursorPosition()
        {
            this._CursorX += 1;
            if ( this._CursorX > 79 )
            {
                this._CursorX = 0;
                this._CursorY += 1;
            }

            if ( this._CursorY > 24 )
            {
                this.ScrollUp();
                this._CursorY = 24;
            }
        }

        /// <summary> Gets an index. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="row"> The row of the cursor position. </param>
        /// <param name="col"> The col. </param>
        /// <returns> The index. </returns>
        private static int GetIndex( int row, int col )
        {
            return 80 * row + col;
        }

        /// <summary> Gets an index. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> The index. </returns>
        private int GetIndex()
        {
            return GetIndex( this._CursorY, this._CursorX );
        }

        /// <summary> Scrolls the console screen window up the given. number of lines. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="lines"> The number of lines to scroll up. </param>
        public void ScrollUp( int lines )
        {
            while ( lines > 0 )
            {
                for ( int i = 0, loopTo = this.ScreenSize - 80 - 1; i <= loopTo; i++ )
                {
                    this.TextBlockArray[i] = this.TextBlockArray[i + 80];
                }

                for ( int i = this.ScreenSize - 80, loopTo1 = this.ScreenSize - 1; i <= loopTo1; i++ )
                {
                    this.TextBlockArray[i].Character = ControlChars.NullChar;
                    this.TextBlockArray[i].BackgroundColor = this.ConsoleBackgroundColor;
                    this.TextBlockArray[i].ForegroundColor = this.ConsoleForegroundColor;
                }

                lines -= 1;
            }

            this.Invalidate();
        }

        /// <summary> Scrolls the console screen window up one line. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public void ScrollUp()
        {
            this.ScrollUp( 1 );
        }

        /// <summary> Clears the console screen. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public void Clear()
        {
            for ( int i = 0, loopTo = this.ScreenSize - 1; i <= loopTo; i++ )
            {
                this.TextBlockArray[i].BackgroundColor = this.ConsoleBackgroundColor;
                this.TextBlockArray[i].ForegroundColor = this.ConsoleForegroundColor;
                this.TextBlockArray[i].Character = ControlChars.NullChar;
            }

            this._CursorX = 0;
            this._CursorY = 0;
            this.Invalidate();
        }

        /// <summary> Sets the background color at the specified location. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="color">  The background color to set. </param>
        /// <param name="row">    The row at which to set the background color. </param>
        /// <param name="column"> The column at which to set the background color. </param>
        public void SetBackgroundColorAt( Color color, int row, int column )
        {
            this.TextBlockArray[GetIndex( row, column )].BackgroundColor = color;
            this.Invalidate();
        }

        /// <summary> Sets the foreground color at the specified location. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="color">  The foreground color to set. </param>
        /// <param name="row">    The row at which to set the foreground color. </param>
        /// <param name="column"> The column at which to set the foreground color. </param>
        public void SetForegroundColorAt( Color color, int row, int column )
        {
            this.TextBlockArray[GetIndex( row, column )].ForegroundColor = color;
            this.Invalidate();
        }

        /// <summary> Sets the character at the specified location. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="character"> The character to set. </param>
        /// <param name="row">       The row at which to place the character. </param>
        /// <param name="column">    The column at which to place the character. </param>
        public void SetCharacterAt( char character, int row, int column )
        {
            this.TextBlockArray[GetIndex( row, column )].Character = character;
            this.Invalidate();
        }

        #endregion

    }

    /// <summary> Values that represent cursor types. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public enum CursorType
    {

        /// <summary> An enum constant representing the invisible option. </summary>
        Invisible = 0,

        /// <summary> An enum constant representing the block option. </summary>
        Block = 1,

        /// <summary> An enum constant representing the underline option. </summary>
        Underline = 2
    }

    /// <summary> A location. </summary>
    /// <remarks>
    /// (c) 2012 icemanind. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-07-09 </para>
    /// </remarks>
    public class Location
    {

        /// <summary> Gets the row. </summary>
        /// <value> The row. </value>
        public int Row { get; set; }

        /// <summary> Gets the column. </summary>
        /// <value> The column. </value>
        public int Column { get; set; }
    }

    /// <summary> A text block. </summary>
    /// <remarks>
    /// (c) 2012 icemanind. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    internal struct TextBlock
    {

        /// <summary> Gets the color of the background. </summary>
        /// <value> The color of the background. </value>
        public Color BackgroundColor { get; set; }

        /// <summary> Gets the color of the foreground. </summary>
        /// <value> The color of the foreground. </value>
        public Color ForegroundColor { get; set; }

        /// <summary> Gets the character. </summary>
        /// <value> The character. </value>
        public char Character { get; set; }
    }

    /// <summary> Additional information for line entered events. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-07-09 </para>
    /// </remarks>
    public class LineEnteredEventArgs : EventArgs
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="line"> The line. </param>
        public LineEnteredEventArgs( string line ) : base()
        {
            this.Line = line;
        }

        /// <summary> Gets the empty. </summary>
        /// <value> The empty. </value>
        public static new LineEnteredEventArgs Empty => new( "" );

        /// <summary> Gets or sets the line. </summary>
        /// <value> The line. </value>
        public string Line { get; private set; }
    }
}
