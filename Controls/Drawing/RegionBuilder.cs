using System;
using System.Drawing;
using System.Runtime.InteropServices;

namespace isr.Core.Controls
{

    /// <summary> A region builder. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public sealed class RegionBuilder
    {

        /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private RegionBuilder() : base()
        {
        }

        /// <summary> Builds round region. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="rect">   The rectangle. </param>
        /// <param name="radius"> The radius. </param>
        /// <returns> An Integer. </returns>
        public static int BuildRoundRegion( Rectangle rect, int radius )
        {
            return SafeNativeMethods.CreateRoundRectRgn( rect.X, rect.Y, rect.X + rect.Width, rect.Y + rect.Height, radius, radius ).ToInt32();
        }

        /// <summary> A safe native methods. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private sealed class SafeNativeMethods
        {

            /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
            /// <remarks> David, 2020-09-24. </remarks>
            private SafeNativeMethods() : base()
            {
            }

            /// <summary> Creates round rectangle region. </summary>
            /// <remarks> David, 2020-09-24. </remarks>
            /// <param name="nLeftRect">      The left rectangle. </param>
            /// <param name="nTopRect">       The top rectangle. </param>
            /// <param name="nRightRect">     The right rectangle. </param>
            /// <param name="nBottomRect">    The bottom rectangle. </param>
            /// <param name="nWidthEllipse">  The width ellipse. </param>
            /// <param name="nHeightEllipse"> The height ellipse. </param>
            /// <returns> The new round rectangle region. </returns>
            [DllImport( "Gdi32.dll", EntryPoint = "CreateRoundRectRgn" )]
            public static extern IntPtr CreateRoundRectRgn( int nLeftRect, int nTopRect, int nRightRect, int nBottomRect, int nWidthEllipse, int nHeightEllipse );
        }
    }
}
