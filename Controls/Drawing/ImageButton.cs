using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Controls
{

    /// <summary> An image button. </summary>
    /// <remarks>
    /// (c) 2008 Vartan Simonian. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-03-31 </para>
    /// </remarks>
    public class ImageButton : PictureBox, IButtonControl
    {

        #region " IButtonControl Members "

        /// <summary>
        /// Gets or sets the value returned to the parent form when the button is clicked.
        /// </summary>
        /// <value> One of the <see cref="T:System.Windows.Forms.DialogResult" /> values. </value>
        public DialogResult DialogResult { get; set; }

        /// <summary> Gets or sets the is default. </summary>
        /// <value> The is default. </value>
        public bool IsDefault { get; private set; } = false;

        /// <summary>
        /// Notifies a control that it is the default button so that its appearance and behavior is
        /// adjusted accordingly.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> true if the control should behave as a default button; otherwise false. </param>
        public void NotifyDefault( bool value )
        {
            this.IsDefault = value;
        }

        /// <summary>
        /// Generates a <see cref="E:System.Windows.Forms.Control.Click" /> event for the control.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public void PerformClick()
        {
            base.OnClick( EventArgs.Empty );
        }

        #endregion

        #region " IMAGES "

        /// <summary> True to hover. </summary>
        private bool _Hover = false;

        /// <summary> True to down. </summary>
        private bool _Down = false;

        /// <summary> The hover image. </summary>
        private Image _HoverImage;

        /// <summary> Image to show when the button is hovered over. </summary>
        /// <value> The hover image. </value>
        [Category( "Appearance" )]
        [Description( "Image to show when the button is hovered over." )]
        public Image HoverImage
        {
            get => this._HoverImage;

            set {
                this._HoverImage = value;
                if ( this._Hover )
                {
                    this.Image = value;
                }
            }
        }

        /// <summary> The down image. </summary>
        private Image _DownImage;

        /// <summary> Image to show when the button is depressed. </summary>
        /// <value> The down image. </value>
        [Category( "Appearance" )]
        [Description( "Image to show when the button is depressed." )]
        public Image DownImage
        {
            get => this._DownImage;

            set {
                this._DownImage = value;
                if ( this._Down )
                {
                    this.Image = value;
                }
            }
        }

        /// <summary> The normal image. </summary>
        private Image _NormalImage;

        /// <summary> Image to show when the button is not in any other state. </summary>
        /// <value> The normal image. </value>
        [Category( "Appearance" )]
        [Description( "Image to show when the button is not in any other state." )]
        public Image NormalImage
        {
            get => this._NormalImage;

            set {
                this._NormalImage = value;
                if ( !(this._Hover || this._Down) )
                {
                    this.Image = value;
                }
            }
        }

        #endregion

        #region " Overrides "

        /// <summary>
        /// Gets or sets the text of the <see cref="T:System.Windows.Forms.PictureBox" />.
        /// </summary>
        /// <value> The text of the <see cref="T:System.Windows.Forms.PictureBox" />. </value>
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [Category( "Appearance" )]
        [Description( "The text associated with the control." )]
        public override string Text
        {
            get => base.Text;

            set => base.Text = value;
        }

        /// <summary> Gets or sets the font of the text displayed by the control. </summary>
        /// <value>
        /// The <see cref="T:System.Drawing.Font" /> to apply to the text displayed by the control. The
        /// default is the value of the <see cref="P:System.Windows.Forms.Control.DefaultFont" />
        /// property.
        /// </value>
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [Category( "Appearance" )]
        [Description( "The font used to display text in the control." )]
        public override Font Font
        {
            get => base.Font;

            set => base.Font = value;
        }

        #endregion

        #region " Description Changes "

        /// <summary>
        /// Controls how the ImageButton will handle image placement and control sizing.
        /// </summary>
        /// <value> The size mode. </value>
        [Description( "Controls how the ImageButton will handle image placement and control sizing." )]
        public new PictureBoxSizeMode SizeMode
        {
            get => base.SizeMode;

            set => base.SizeMode = value;
        }

        /// <summary> Controls what type of border the ImageButton should have. </summary>
        /// <value> The border style. </value>
        [Description( "Controls what type of border the ImageButton should have." )]
        public new BorderStyle BorderStyle
        {
            get => base.BorderStyle;

            set => base.BorderStyle = value;
        }

        #endregion

        #region " HIDING "

        /// <summary> Gets or sets the image. </summary>
        /// <value> The image. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new Image Image
        {
            get => base.Image;

            set => base.Image = value;
        }

        /// <summary> Gets or sets the background image layout. </summary>
        /// <value> The background image layout. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new ImageLayout BackgroundImageLayout
        {
            get => base.BackgroundImageLayout;

            set => base.BackgroundImageLayout = value;
        }

        /// <summary> Gets or sets the background image. </summary>
        /// <value> The background image. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new Image BackgroundImage
        {
            get => base.BackgroundImage;

            set => base.BackgroundImage = value;
        }

        /// <summary> Gets or sets the image location. </summary>
        /// <value> The image location. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new string ImageLocation
        {
            get => base.ImageLocation;

            set => base.ImageLocation = value;
        }

        /// <summary> Gets or sets the error image. </summary>
        /// <value> The error image. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new Image ErrorImage
        {
            get => base.ErrorImage;

            set => base.ErrorImage = value;
        }

        /// <summary> Gets or sets the initial image. </summary>
        /// <value> The initial image. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new Image InitialImage
        {
            get => base.InitialImage;

            set => base.InitialImage = value;
        }

        /// <summary> Gets or sets the wait on load. </summary>
        /// <value> The wait on load. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new bool WaitOnLoad
        {
            get => base.WaitOnLoad;

            set => base.WaitOnLoad = value;
        }

        #endregion

        #region " MESSAGE PROCESS "

        /// <summary> The windows message keydown. </summary>
        private const int _WM_KEYDOWN = 0x100;

        /// <summary> The windows message keyup. </summary>
        private const int _WM_KEYUP = 0x101;

        /// <summary> True to holding space. </summary>
        private bool _HoldingSpace = false;

        /// <summary>
        /// Preprocesses keyboard or input messages within the message loop before they are dispatched.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="msg"> [in,out] A <see cref="T:System.Windows.Forms.Message" />, passed by
        /// reference, that represents the message to process. The possible values
        /// are WM_KEYDOWN, WM_SYSKEYDOWN, WM_CHAR, and WM_SYSCHAR. </param>
        /// <returns>
        /// <see langword="true" /> if the message was processed by the control; otherwise,
        /// <see langword="false" />.
        /// </returns>
        public override bool PreProcessMessage( ref Message msg )
        {
            if ( msg.Msg == _WM_KEYUP )
            {
                if ( this._HoldingSpace )
                {
                    if ( Conversions.ToInteger( Conversion.Fix( msg.WParam ) ) == ( int ) Keys.Space )
                    {
                        this.OnMouseUp( null );
                        this.PerformClick();
                    }
                    else if ( Conversions.ToInteger( Conversion.Fix( msg.WParam ) ) == ( int ) Keys.Escape || Conversions.ToInteger( Conversion.Fix( msg.WParam ) ) == ( int ) Keys.Tab )
                    {
                        this._HoldingSpace = false;
                        this.OnMouseUp( null );
                    }
                }

                return true;
            }
            else if ( msg.Msg == _WM_KEYDOWN )
            {
                if ( Conversions.ToInteger( Conversion.Fix( msg.WParam ) ) == ( int ) Keys.Space )
                {
                    this._HoldingSpace = true;
                    this.OnMouseDown( null );
                }
                else if ( Conversions.ToInteger( Conversion.Fix( msg.WParam ) ) == ( int ) Keys.Enter )
                {
                    this.PerformClick();
                }

                return true;
            }
            else
            {
                return base.PreProcessMessage( ref msg );
            }
        }

        #endregion

        #region " MOUSE EVENTS"

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.MouseMove" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnMouseMove( MouseEventArgs e )
        {
            this._Hover = true;
            if ( this._Down )
            {
                if ( this._DownImage is object && !ReferenceEquals( this.Image, this._DownImage ) )
                {
                    this.Image = this._DownImage;
                }
            }
            else
            {
                this.Image = this._HoverImage ?? this._NormalImage;
            }

            base.OnMouseMove( e );
        }

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.MouseLeave" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnMouseLeave( EventArgs e )
        {
            this._Hover = false;
            this.Image = this._NormalImage;
            base.OnMouseLeave( e );
        }

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.MouseDown" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnMouseDown( MouseEventArgs e )
        {
            _ = this.Focus();
            this.OnMouseUp( null );
            this._Down = true;
            if ( this._DownImage is object )
            {
                this.Image = this._DownImage;
            }

            base.OnMouseDown( e );
        }

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.MouseUp" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnMouseUp( MouseEventArgs e )
        {
            this._Down = false;
            if ( this._Hover )
            {
                if ( this._HoverImage is object )
                {
                    this.Image = this._HoverImage;
                }
            }
            else
            {
                this.Image = this._NormalImage;
            }

            base.OnMouseUp( e );
        }

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.LostFocus" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnLostFocus( EventArgs e )
        {
            this._HoldingSpace = false;
            this.OnMouseUp( null );
            base.OnLostFocus( e );
        }

        #endregion

        #region " GRAPHICS EVENTS"

        /// <summary> Renders the image described by event arguments. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="pe"> Paint event information. </param>
        private void RenderImage( PaintEventArgs pe )
        {
            if ( this.Image is object )
            {
                var matrix = new ColorMatrix();
                float value = this.Enabled ? 1.0f : 0.6f;
                matrix.Matrix33 = value;
                var g = pe.Graphics;
                g.Clear( this.BackColor );
                using var bmp = new Bitmap( this.Image, new Size( this.Width, this.Height ) );
                using var attributes = new ImageAttributes();
                attributes.SetColorMatrix( matrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap );
                g.DrawImage( bmp, new Rectangle( 0, 0, bmp.Width, bmp.Height ), 0, 0, bmp.Width, bmp.Height, GraphicsUnit.Pixel, attributes );
            }
            else
            {
                base.OnPaint( pe );
            }
        }

        /// <summary> Draw text. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="pe"> Paint event information. </param>
        private void DrawText( PaintEventArgs pe )
        {
            if ( !string.IsNullOrEmpty( base.Text ) && pe is object && base.Font is object )
            {
                var drawStringSize = pe.Graphics.MeasureString( base.Text, base.Font );
                var drawPoint = base.Image is object ? new PointF( base.Image.Width / 2 - ( int ) drawStringSize.Width / 2, base.Image.Height / 2 - ( int ) drawStringSize.Height / 2 ) : new PointF( this.Width / 2 - ( int ) drawStringSize.Width / 2, this.Height / 2 - ( int ) drawStringSize.Height / 2 );
                using var drawBrush = new SolidBrush( base.ForeColor );
                pe.Graphics.DrawString( base.Text, base.Font, drawBrush, drawPoint );
            }
        }

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.Paint" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="pe"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnPaint( PaintEventArgs pe )
        {
            this.RenderImage( pe );
            this.DrawText( pe );
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.TextChanged" /> event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnTextChanged( EventArgs e )
        {
            this.Refresh();
            base.OnTextChanged( e );
        }

        #endregion

    }
}
