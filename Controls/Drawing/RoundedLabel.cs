using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

using isr.Core.Controls.DrawingExtensions;

namespace isr.Core.Controls
{

    /// <summary> A rounded label. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-04-02 </para>
    /// </remarks>
    [DesignerCategory( "code" )]
    [Description( "Rounded Label" )]
    public class RoundedLabel : Label
    {

        #region " CONTRUCTOR "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public RoundedLabel() : base()
        {
            this.CornerRadii = new CornerRadius();
        }

        #endregion

        #region " HIDING "

        /// <summary> Gets or sets the border style for the control. </summary>
        /// <value>
        /// One of the <see cref="T:System.Windows.Forms.BorderStyle" /> values. The default is
        /// <see langword="BorderStyle.None" />.
        /// </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public override BorderStyle BorderStyle
        {
            get => base.BorderStyle;

            set => base.BorderStyle = BorderStyle.None;
        }

        #endregion

        #region " BORDER STYLE "

        /// <summary> Size of the border. </summary>
        private int _BorderSize;

        /// <summary> Specifies the border size. </summary>
        /// <value> The size of the border. </value>
        [Category( "Appearance" )]
        [DefaultValue( 1 )]
        [Description( "Specifies the border size." )]
        public int BorderSize
        {
            get => this._BorderSize;

            set {
                this._BorderSize = value;
                this.Invalidate();
            }
        }

        /// <summary> The shadow angle. </summary>
        private int _ShadowAngle;

        /// <summary> Specifies the shadow angle. </summary>
        /// <value> The shadow angle. </value>
        [Category( "Appearance" )]
        [DefaultValue( 270 )]
        [Description( "Specifies the shadow angle." )]
        public int ShadowAngle
        {
            get => this._ShadowAngle;

            set {
                this._ShadowAngle = value;
                this.Invalidate();
            }
        }

        /// <summary> Specifies the top left radius in percent of height or width. </summary>
        /// <value> The top left radius. </value>
        [Category( "Appearance" )]
        [DefaultValue( 0 )]
        [Description( "Specifies the top left radius in percent of height or width." )]
        public int TopLeftRadius
        {
            get => this.Corners.TopLeft;

            set {
                this._Corners = new CornerRadius( this.BottomLeftRadius, this.BottomRightRadius, value, this.TopRightRadius );
                this.CornerRadii = new CornerRadius( this.BottomLeftRadius * this.Height / 100, this.BottomRightRadius * this.Height / 100, this.TopLeftRadius * this.Height / 100, this.TopRightRadius * this.Height / 100 );
                this.Invalidate();
            }
        }

        /// <summary> Specifies the top right radius. </summary>
        /// <value> The top right radius. </value>
        [Category( "Appearance" )]
        [DefaultValue( 0 )]
        [Description( "Specifies the top right radius." )]
        public int TopRightRadius
        {
            get => this.Corners.TopRight;

            set {
                this._Corners = new CornerRadius( this.BottomLeftRadius, this.BottomRightRadius, this.TopLeftRadius, value );
                this.CornerRadii = new CornerRadius( this.BottomLeftRadius * this.Height / 100, this.BottomRightRadius * this.Height / 100, this.TopLeftRadius * this.Height / 100, this.TopRightRadius * this.Height / 100 );
                this.Invalidate();
            }
        }

        /// <summary> Specifies the bottom left radius. </summary>
        /// <value> The bottom left radius. </value>
        [Category( "Appearance" )]
        [DefaultValue( 0 )]
        [Description( "Specifies the bottom left radius." )]
        public int BottomLeftRadius
        {
            get => this.Corners.BottomLeft;

            set {
                this._Corners = new CornerRadius( value, this.BottomRightRadius, this.TopLeftRadius, this.TopRightRadius );
                this.CornerRadii = new CornerRadius( this.BottomLeftRadius * this.Height / 100, this.BottomRightRadius * this.Height / 100, this.TopLeftRadius * this.Height / 100, this.TopRightRadius * this.Height / 100 );
                this.Invalidate();
            }
        }

        /// <summary> Specifies the bottom right radius. </summary>
        /// <value> The bottom right radius. </value>
        [Category( "Appearance" )]
        [DefaultValue( 0 )]
        [Description( "Specifies the bottom right radius." )]
        public int BottomRightRadius
        {
            get => this.Corners.BottomRight;

            set {
                this._Corners = new CornerRadius( this.BottomLeftRadius, value, this.TopLeftRadius, this.TopRightRadius );
                this.CornerRadii = new CornerRadius( this.BottomLeftRadius * this.Height / 100, this.BottomRightRadius * this.Height / 100, this.TopLeftRadius * this.Height / 100, this.TopRightRadius * this.Height / 100 );
                this.Invalidate();
            }
        }

        #endregion

        #region " CORNERS "

        /// <summary> Default corners. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> The CornerRadius. </returns>
        private static CornerRadius DefaultCorners()
        {
            return new CornerRadius( 0 );
        }

        /// <summary> The corners. </summary>
        private CornerRadius _Corners = DefaultCorners();

        /// <summary> Gets or sets the relative corner radii. </summary>
        /// <value> The relative corner radii. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public CornerRadius Corners
        {
            get => this._Corners;

            set {
                this._Corners = value;
                this.CornerRadii = new CornerRadius( this.BottomLeftRadius * this.Height / 100, this.BottomRightRadius * this.Height / 100, this.TopLeftRadius * this.Height / 100, this.TopRightRadius * this.Height / 100 );
                this.Invalidate();
            }
        }

        /// <summary> Gets or sets the corner radii. </summary>
        /// <value> The corner radii. </value>
        private CornerRadius CornerRadii { get; set; }

        /// <summary> Resets the corners. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public void ResetCorners()
        {
            this.Corners = DefaultCorners();
        }

        /// <summary> Determine if we should serialize corners. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        private bool ShouldSerializeCorners()
        {
            return !this._Corners.Equals( DefaultCorners() );
        }

        #endregion

        #region " PAINT "

        /// <summary> Clip region. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="surface">         The surface. </param>
        /// <param name="borderRectangle"> The border rectangle. </param>
        public void ClipRegion( Graphics surface, Rectangle borderRectangle )
        {
            if ( surface is null )
            {
                return;
            }

            surface.PixelOffsetMode = PixelOffsetMode.HighQuality;
            surface.InterpolationMode = InterpolationMode.HighQualityBilinear;
            surface.SmoothingMode = SmoothingMode.AntiAlias;
            using var path = new GraphicsPath();
            _ = path.BuildRoundClipPath( this.Size, borderRectangle, this.CornerRadii );
            this.Region = new Region( path );
        }

        /// <summary> Gets shadow brushes. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="shadowSize"> Size of the shadow. </param>
        /// <returns> The shadow brushes. </returns>
        private IList<Brush> GetShadowBrushes( int shadowSize )
        {
            var brushes = new List<Brush>();
            int alphaOffset = 35;
            int maxAlpha = shadowSize * alphaOffset;
            for ( int shadowIndex = 0, loopTo = shadowSize - 1; shadowIndex <= loopTo; shadowIndex++ )
            {
                int alpha = maxAlpha - shadowIndex * alphaOffset;
                alpha = alpha < 0 ? 0 : alpha > 255 ? 255 : alpha;
                brushes.Add( new SolidBrush( Color.FromArgb( alpha, ControlPaint.Dark( this.BackColor ) ) ) );
                // brushes.Add(New SolidBrush(Color.FromArgb(alpha, Color.Black)))
            }

            return brushes;
        }

        /// <summary> Renders the border. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="surface">         The surface. </param>
        /// <param name="borderRectangle"> The border rectangle. </param>
        public void RenderBorder( Graphics surface, Rectangle borderRectangle )
        {
            if ( surface is null )
            {
                return;
            }

            surface.PixelOffsetMode = PixelOffsetMode.HighQuality;
            surface.InterpolationMode = InterpolationMode.HighQualityBilinear;
            surface.SmoothingMode = SmoothingMode.AntiAlias;
            using ( var path = new GraphicsPath() )
            {
                _ = path.BuildRoundClipPath( this.Size, borderRectangle, this.CornerRadii );
                surface.SetClip( path );
                using ( var pen = new Pen( ControlPaint.Dark( this.BackColor ), 1f ) )
                {
                    surface.SmoothingMode = SmoothingMode.AntiAlias;
                    pen.Alignment = PenAlignment.Inset;
                    surface.DrawPath( pen, path );
                }

                surface.ResetClip();
            }

            var innerRectangle = new Rectangle( borderRectangle.X, borderRectangle.Y, borderRectangle.Width, borderRectangle.Height );
            using ( var path = new GraphicsPath() )
            {
                if ( this.ShadowAngle < 0 )
                {
                    foreach ( Brush brush in this.GetShadowBrushes( this.BorderSize ) )
                    {
                        _ = path.BuildRoundClipPath( this.Size, innerRectangle, this.CornerRadii );
                        surface.SetClip( path );
                        using ( var pen = new Pen( brush, this.BorderSize ) )
                        {
                            pen.Alignment = PenAlignment.Inset;
                            surface.SmoothingMode = SmoothingMode.AntiAlias;
                            surface.DrawPath( pen, path );
                        }

                        surface.ResetClip();
                        innerRectangle.Inflate( -1, -1 );
                    }
                }
                else
                {
                    _ = path.BuildRoundClipPath( this.Size, innerRectangle, this.CornerRadii );
                    surface.SetClip( path );
                    // TO_DO: Try transparent color.
                    var color1 = ControlPaint.LightLight( this.BackColor );
                    // color1 = Color.Transparent
                    var color2 = ControlPaint.Dark( this.BackColor );
                    using ( var brush = new LinearGradientBrush( innerRectangle, color1, color2, this.ShadowAngle ) )
                    {
                        brush.GammaCorrection = true;
                        using var pen = new Pen( brush, this.BorderSize ) {
                            Alignment = PenAlignment.Inset
                        };
                        surface.SmoothingMode = SmoothingMode.AntiAlias;
                        surface.DrawPath( pen, path );
                    }

                    surface.ResetClip();
                }
            }
        }

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.Paint" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnPaint( PaintEventArgs e )
        {
            if ( e is null )
            {
                return;
            }

            base.OnPaint( e );
            if ( this.DisplayRectangle.Width < 4 || this.DisplayRectangle.Height < 4 )
            {
                return;
            }

            this.CornerRadii = new CornerRadius( this.BottomLeftRadius * this.Height / 100, this.BottomRightRadius * this.Height / 100, this.TopLeftRadius * this.Height / 100, this.TopRightRadius * this.Height / 100 );
            this.ClipRegion( e.Graphics, this.DisplayRectangle );
            if ( this.BorderSize > 0 )
            {
                this.RenderBorder( e.Graphics, this.DisplayRectangle );
            }
        }

        #endregion

    }
}
