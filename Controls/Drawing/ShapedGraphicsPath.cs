using System;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace isr.Core.Controls
{

    /// <summary> A shaped graphics path. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-03-26 </para>
    /// </remarks>
    public sealed class ShapedGraphicsPath
    {

        /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private ShapedGraphicsPath() : base()
        {
        }

        /// <summary> Builds round path. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="rect"> The rectangle. </param>
        /// <returns> A GraphicsPath. </returns>
        public static GraphicsPath BuildRoundPath( Rectangle rect )
        {
            GraphicsPath path = null;
            try
            {
                path = new GraphicsPath();
                // path.AddEllipse(0, 0, Me.ClientSize.Width, Me.ClientSize.Height)
                path.AddEllipse( rect.X, rect.Y, rect.Width, rect.Height );
            }
            catch
            {
                path?.Dispose();
                throw;
            }

            return path;
        }

        /// <summary> Builds rounded path. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="rect">   The rectangle. </param>
        /// <param name="radius"> The radius. </param>
        /// <returns> A GraphicsPath. </returns>
        public static GraphicsPath BuildRoundedPath( Rectangle rect, int radius )
        {
            radius = Math.Min( Math.Min( rect.Height, rect.Width ), radius );
            int r2 = radius / 2;
            radius = r2 + r2;
            GraphicsPath path = null;
            try
            {
                path = new GraphicsPath();
                path.StartFigure();
                path.AddArc( rect.X, rect.Y, radius, radius, 180f, 90f );
                path.AddLine( rect.X + r2, rect.Y, rect.Width - r2, rect.Y );
                int value = rect.Width - radius;
                value = value < 0 ? 0 : value;
                path.AddArc( rect.X + value, rect.Y, radius, radius, 270f, 90f );
                path.AddLine( rect.Width, rect.Y + r2, rect.Width, rect.Height - r2 );
                value = rect.Height - radius;
                value = value < 0 ? 0 : value;
                path.AddArc( rect.X + rect.Width - radius, rect.Y + value, radius, radius, 0f, 90f );
                path.AddLine( rect.Width - r2, rect.Height, rect.X + r2, rect.Height );
                path.AddArc( rect.X, rect.Y + value, radius, radius, 90f, 90f );
                path.AddLine( rect.X, rect.Height - r2, rect.X, rect.Y + r2 );
                path.CloseFigure();
            }
            catch
            {
                path?.Dispose();
                throw;
            }

            return path;
        }
    }
}
