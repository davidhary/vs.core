using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace isr.Core.Controls.DrawingExtensions
{
    /// <summary>   <see cref="DrawingExtensions"/> methods. </summary>
    /// <remarks>   David, 2020-09-24. </remarks>
    public static class DrawingExtensionMethods
    {

        #region " CLIP REGION "

        /// <summary> Clip region. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="control"> The control. </param>
        /// <param name="surface"> The surface. </param>
        /// <param name="corners"> The corners. </param>
        public static void ClipRegion( Control control, Graphics surface, CornerRadius corners )
        {
            if ( control is null )
            {
                throw new ArgumentNullException( nameof( control ) );
            }

            if ( surface is null )
            {
                throw new ArgumentNullException( nameof( surface ) );
            }

            ClipRegion( control, surface, control.Size, control.DisplayRectangle, corners );
        }

        /// <summary> Clip region. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="control">         The control. </param>
        /// <param name="surface">         The surface. </param>
        /// <param name="size">            The size. </param>
        /// <param name="regionRectangle"> The region rectangle. </param>
        /// <param name="corners">         The corners. </param>
        public static void ClipRegion( Control control, Graphics surface, Size size, Rectangle regionRectangle, CornerRadius corners )
        {
            if ( control is null )
            {
                throw new ArgumentNullException( nameof( control ) );
            }

            if ( surface is null )
            {
                throw new ArgumentNullException( nameof( surface ) );
            }

            surface.PixelOffsetMode = PixelOffsetMode.HighQuality;
            surface.InterpolationMode = InterpolationMode.HighQualityBilinear;
            surface.SmoothingMode = SmoothingMode.AntiAlias;
            using var path = new GraphicsPath();
            _ = path.BuildRoundClipPath( size, regionRectangle, corners );
            surface.SetClip( path );
            control.Region = new Region( path );
            surface.ResetClip();
        }

        #endregion

        #region " PATH BUILDER "

        /// <summary> Builds round clip path. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="path"> Full pathname of the file. </param>
        /// <param name="rect"> The rectangle. </param>
        /// <returns> The rectangle clip path. </returns>
        public static GraphicsPath BuildRoundClipPath( this GraphicsPath path, Rectangle rect )
        {
            if ( path is object )
            {
                path.AddArc( rect.X, rect.Y, rect.Height, rect.Height, 90f, 180f );
                path.AddArc( rect.Width - rect.Height, rect.Y, rect.Height, rect.Height, 270f, 180f );
                path.CloseFigure();
            }

            return path;
        }

        /// <summary> Builds round clip path. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="path">      Full pathname of the file. </param>
        /// <param name="size">      The size. </param>
        /// <param name="rectangle"> The rectangle. </param>
        /// <param name="radius">    The radius. </param>
        /// <returns> The rectangle clip path. </returns>
        public static GraphicsPath BuildRoundClipPath( this GraphicsPath path, Size size, Rectangle rectangle, int radius )
        {
            var radii = new CornerRadius( radius );
            return path.BuildRoundClipPath( size, rectangle, radii );
        }

        /// <summary> Clip diameter. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="size">     The size. </param>
        /// <param name="diameter"> The diameter. </param>
        /// <returns> An Integer. </returns>
        private static int ClipDiameter( Size size, int diameter )
        {
            if ( diameter > size.Height )
            {
                diameter = size.Height;
            }

            if ( diameter > size.Width )
            {
                diameter = size.Width;
            }

            if ( diameter < _MinimumDiameter )
            {
                diameter = _MinimumDiameter;
            }

            return diameter;
        }

        /// <summary> The minimum diameter. </summary>
        private const int _MinimumDiameter = 1;

        /// <summary> Builds round clip path. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="path">      Full pathname of the file. </param>
        /// <param name="size">      The size. </param>
        /// <param name="rectangle"> The rectangle. </param>
        /// <param name="radii">     The radii. </param>
        /// <returns> The rectangle clip path. </returns>
        public static GraphicsPath BuildRoundClipPath( this GraphicsPath path, Size size, Rectangle rectangle, CornerRadius radii )
        {
            if ( path is null )
            {
                throw new ArgumentNullException( nameof( path ) );
            }

            if ( size.Width == 0 || size.Height == 0 )
            {
                throw new InvalidOperationException( "Size must be non zero" );
            }

            int diameter; // = Methods.ClipDiameter(size, CInt(2 * radii.TopLeft))
            // TO_DO: Check this.  
            int x = rectangle.X;
            int y = rectangle.Y;
            int h = rectangle.Height;
            int w = rectangle.Width;
            diameter = ClipDiameter( size, 2 * radii.BottomLeft );
            path.AddArc( x, y + h - diameter, diameter, diameter, 90f, 90f );
            diameter = ClipDiameter( size, 2 * radii.TopLeft );
            path.AddArc( x, y, diameter, diameter, 180f, 90f );
            diameter = ClipDiameter( size, 2 * radii.TopRight );
            path.AddArc( x + w - diameter, y, diameter, diameter, 270f, 90f );
            diameter = ClipDiameter( size, 2 * radii.BottomRight );
            path.AddArc( x + w - diameter, y + h - diameter, diameter, diameter, 0f, 90f );
            path.CloseFigure();
            return path;
        }

        /// <summary> (This method is obsolete) builds zope round clip path. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="path">      Full pathname of the file. </param>
        /// <param name="size">      The size. </param>
        /// <param name="rectangle"> The rectangle. </param>
        /// <param name="radii">     The radii. </param>
        /// <returns> A GraphicsPath. </returns>
        [Obsolete( "Use Build Round Clip Path instead" )]
        public static GraphicsPath BuildZopeRoundClipPath( this GraphicsPath path, Size size, Rectangle rectangle, CornerRadius radii )
        {
            if ( path is null )
            {
                throw new ArgumentNullException( nameof( path ) );
            }

            if ( size.Width == 0 || size.Height == 0 )
            {
                throw new InvalidOperationException( "Size must be non zero" );
            }

            int diameter = ClipDiameter( size, 2 * radii.TopLeft );
            // TO_DO: Check this.  
            int x = rectangle.X;
            int y = rectangle.Y;
            int h = rectangle.Height;
            int w = rectangle.Width;
            // this works with Zope shaped button.
            path.AddArc( x, y, diameter, diameter, 180f, 90f );
            path.AddArc( w - diameter - x, y, diameter, diameter, 270f, 90f );
            path.AddArc( w - diameter - x, h - diameter - y, diameter, diameter, 0f, 90f );
            path.AddArc( x, h - diameter - y, diameter, diameter, 90f, 90f );

            // This works for the Shaped Button but not for the rounded button.
            diameter = ClipDiameter( size, 2 * radii.TopLeft );
            path.AddArc( x, y, diameter, diameter, 180f, 90f );
            diameter = ClipDiameter( size, 2 * radii.TopRight );
            path.AddArc( w - diameter - x, y, diameter, diameter, 270f, 90f );
            diameter = ClipDiameter( size, 2 * radii.BottomRight );
            path.AddArc( w - diameter - x, h - diameter - y, diameter, diameter, 0f, 90f );
            diameter = ClipDiameter( size, 2 * radii.BottomLeft );
            path.AddArc( x, h - diameter - y, diameter, diameter, 90f, 90f );
            path.CloseFigure();
            return path;
        }

        #endregion

    }
}
