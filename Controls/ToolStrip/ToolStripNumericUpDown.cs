using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;
using System.Windows.Forms.Design;

using isr.Core.Controls.ExceptionExtensions;

namespace isr.Core.Controls
{

    /// <summary> Tool strip numeric up down. </summary>
    /// <remarks> David, 2014-04-16. </remarks>
    [ToolStripItemDesignerAvailability( ToolStripItemDesignerAvailability.ToolStrip )]
    public class ToolStripNumericUpDown : ToolStripControlHost, IBindableComponent
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> Call the base constructor passing in a NumericUpDown instance. </remarks>
        public ToolStripNumericUpDown() : base( new NumericUpDown() )
        {
        }

        /// <summary>
        /// Releases the unmanaged resources used by the
        /// <see cref="T:System.Windows.Forms.ToolStripControlHost" /> and optionally releases the
        /// managed resources.
        /// </summary>
        /// <remarks> David, 2014-04-16. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.RemoveValueChangedEventHandler( ValueChanged );
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " BINDABLE "

        /// <summary> The binding context. </summary>
        private BindingContext _BindingContext = null;

        /// <summary>
        /// Gets or sets the collection of currency managers for the
        /// <see cref="T:System.Windows.Forms.IBindableComponent" />.
        /// </summary>
        /// <value>
        /// The collection of <see cref="T:System.Windows.Forms.BindingManagerBase" /> objects for this
        /// <see cref="T:System.Windows.Forms.IBindableComponent" />.
        /// </value>
        public BindingContext BindingContext
        {
            get {
                if ( this._BindingContext is null )
                {
                    this._BindingContext = new BindingContext();
                }

                return this._BindingContext;
            }

            set => this._BindingContext = value;
        }

        /// <summary> The bindings. </summary>
        private ControlBindingsCollection _Bindings;

        /// <summary>
        /// Gets the collection of data-binding objects for this
        /// <see cref="T:System.Windows.Forms.IBindableComponent" />.
        /// </summary>
        /// <value>
        /// The <see cref="T:System.Windows.Forms.ControlBindingsCollection" /> for this
        /// <see cref="T:System.Windows.Forms.IBindableComponent" />.
        /// </value>
        public ControlBindingsCollection DataBindings
        {
            get {
                if ( this._Bindings is null )
                {
                    this._Bindings = new ControlBindingsCollection( this );
                }

                return this._Bindings;
            }
        }

        #endregion

        #region " VALIDATING "

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.ToolStripControlHost.LostFocus" /> event.
        /// </summary>
        /// <remarks>   David, 2021-03-30. </remarks>
        /// <param name="e">    A <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnLostFocus( EventArgs e )
        {
            base.OnLostFocus( e );
            this.OnValidating( new CancelEventArgs() );
        }

        #endregion

        #region " CONTROL "

        /// <summary> Gets the numeric up down control. </summary>
        /// <value> The numeric up down control. </value>
        public NumericUpDown NumericUpDownControl => this.Control as NumericUpDown;

        #endregion

        #region " CONTROL PROPERTIES "

        /// <summary> Gets or sets the selected text. </summary>
        /// <value> The selected text. </value>
        [DefaultValue( "" )]
        [Description( "text" )]
        [Category( "Appearance" )]
        public override string Text
        {
            get => this.NumericUpDownControl.Text;

            set => SafeSetter( this.NumericUpDownControl, () => this.NumericUpDownControl.Text = value );
        }

        /// <summary> Safe setter. </summary>
        /// <remarks> David, 2014-04-16. </remarks>
        /// <param name="control"> The control from which to subscribe events. </param>
        /// <param name="setter">  The setter. </param>
        private static void SafeSetter( Control control, Action setter )
        {
            if ( control is object && control.Parent is object )
            {
                if ( control.Parent.InvokeRequired )
                {
                    _ = control.Parent.Invoke( new Action<Control, Action>( SafeSetter ), new object[] { control, setter } );
                }
                else if ( control.Parent.IsHandleCreated )
                {
                    setter.Invoke();
                }
            }
        }

        /// <summary> Gets or sets the value. </summary>
        /// <value> The value. </value>
        [DefaultValue( "" )]
        [Description( "Value" )]
        [Category( "Appearance" )]
        public decimal Value
        {
            get => this.NumericUpDownControl.Value;

            set => SafeSetter( this.NumericUpDownControl, () => this.NumericUpDownControl.Value = value );
        }

        /// <summary> Gets a value indicating whether this object has value. </summary>
        /// <value> <c>true</c> if this object has value; otherwise <c>false</c> </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public bool HasValue => this.NumericUpDownControl.HasValue();

        #endregion

        #region " EVENTS "

        /// <summary> Event queue for all listeners interested in ValueChanged events. </summary>
        public event EventHandler<EventArgs> ValueChanged;

        /// <summary> Removes event handler. </summary>
        /// <remarks> David, 2014-04-16. </remarks>
        /// <param name="value"> The handler. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void RemoveValueChangedEventHandler( EventHandler<EventArgs> value )
        {
            foreach ( Delegate d in value is null ? (Array.Empty<Delegate>()) : value.GetInvocationList() )
            {
                try
                {
                    ValueChanged -= ( EventHandler<EventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }
        }

        /// <summary> Raises the value changed event. </summary>
        /// <remarks> David, 2014-04-16. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information to send to registered event handlers. </param>
        private void OnValueChanged( object sender, EventArgs e )
        {
            var evt = ValueChanged;
            evt?.Invoke( this, e );
        }

        #endregion

        #region " TOOL STRIP MANAGEMENT "

        /// <summary> Subscribes events from the hosted control. </summary>
        /// <remarks> Subscribe the control events to expose. </remarks>
        /// <param name="control"> The control from which to subscribe events. </param>
        protected override void OnSubscribeControlEvents( Control control )
        {
            if ( control is object )
            {
                // Call the base so the base events are connected.
                base.OnSubscribeControlEvents( control );

                // Cast the control to a NumericUpDown control.
                if ( control is NumericUpDown numericControl )
                {
                    // Add the event.
                    numericControl.ValueChanged += this.OnValueChanged;
                }
            }
        }

        /// <summary> Unsubscribes events from the hosted control. </summary>
        /// <remarks> David, 2014-04-16. </remarks>
        /// <param name="control"> The control from which to unsubscribe events. </param>
        protected override void OnUnsubscribeControlEvents( Control control )
        {
            // Call the base method so the basic events are unsubscribed.
            base.OnUnsubscribeControlEvents( control );

            // Cast the control to a NumericUpDown control.
            if ( control is NumericUpDown numericControl )
            {
                // Remove the event.
                numericControl.ValueChanged -= this.OnValueChanged;
            }
        }

        #endregion

    }
}
