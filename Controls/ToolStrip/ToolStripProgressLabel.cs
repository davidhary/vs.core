using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace isr.Core.Controls
{

    /// <summary> A tool strip progress label. </summary>
    /// <remarks>
    /// (c) 2007 Hypercubed. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-09-19. http://www.codeproject.com/script/Membership/View.aspx?mid=722189
    /// http://www.codeproject.com/Articles/21419/Label-with-ProgressBar-in-a-StatusStrip.
    /// </para>
    /// </remarks>
    [DesignerCategory( "code" )]
    [Description( "Status label with progress bar" )]
    [ToolStripItemDesignerAvailability( ToolStripItemDesignerAvailability.StatusStrip )]
    public class ToolStripProgressLabel : System.Windows.Forms.ToolStripStatusLabel, IBindableComponent
    {

        #region " BINDABLE "

        /// <summary> The context. </summary>
        private BindingContext _Context = null;

        /// <summary>
        /// Gets or sets the collection of currency managers for the
        /// <see cref="T:System.Windows.Forms.IBindableComponent" />.
        /// </summary>
        /// <value>
        /// The collection of <see cref="T:System.Windows.Forms.BindingManagerBase" /> objects for this
        /// <see cref="T:System.Windows.Forms.IBindableComponent" />.
        /// </value>
        public BindingContext BindingContext
        {
            get {
                if ( this._Context is null )
                {
                    this._Context = new BindingContext();
                }

                return this._Context;
            }

            set => this._Context = value;
        }

        /// <summary> The bindings. </summary>
        private ControlBindingsCollection _Bindings;

        /// <summary>
        /// Gets the collection of data-binding objects for this
        /// <see cref="T:System.Windows.Forms.IBindableComponent" />.
        /// </summary>
        /// <value>
        /// The <see cref="T:System.Windows.Forms.ControlBindingsCollection" /> for this
        /// <see cref="T:System.Windows.Forms.IBindableComponent" />.
        /// </value>
        public ControlBindingsCollection DataBindings
        {
            get {
                if ( this._Bindings is null )
                {
                    this._Bindings = new ControlBindingsCollection( this );
                }

                return this._Bindings;
            }
        }

        #endregion

        #region " PROGRESS IMPLEMENTATION "

        /// <summary> Paints this window. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.PaintEventArgs" />
        /// that contains the event data. </param>
        protected override void OnPaint( PaintEventArgs e )
        {
            if ( e is null )
            {
                return;
            }

            double percent = this.Value / 100d;
            var rect = e.ClipRectangle;
            int height = this.BarHeight;
            if ( height == 0 )
            {
                height = rect.Height;
            }

            rect.Width = ( int ) (rect.Width * percent);
            rect.Y = ( int ) (0.5d * (rect.Height - height));
            rect.Height = height;
            using ( var brush = new SolidBrush( this.BarColor ) )
            {
                // Draw bar
                var g = e.Graphics;
                g.FillRectangle( brush, rect );
            }

            base.OnPaint( e );
        }

        /// <summary> The value. </summary>
        private int _Value = 0;               // Current progress

        /// <summary> Progress Value. </summary>
        /// <value> The value. </value>
        [Category( "Behavior" )]
        [Description( "Progress Value" )]
        [DefaultValue( 0 )]
        public int Value
        {
            get => this._Value;

            set {
                // Make sure that the value does not stray outside the valid range.
                value = value < 0 ? 0 : value > 100 ? 100 : value;
                if ( this.Value != value )
                {
                    this._Value = value;
                    this.Invalidate();
                }
            }
        }

        /// <summary> The bar color. </summary>
        private Color _BarColor = Color.Blue;   // Color of progress meter

        /// <summary> Progress color. </summary>
        /// <value> The color of the bar. </value>
        [Category( "Behavior" )]
        [Description( "Progress color" )]
        [DefaultValue( typeof( Color ), "Blue" )]
        public Color BarColor
        {
            get => this._BarColor;

            set {
                this._BarColor = value;

                // Invalidate the control to get a repaint.
                this.Invalidate();
            }
        }

        /// <summary> Height of the bar. </summary>
        private int _BarHeight = 0;

        /// <summary> Progress height. </summary>
        /// <value> The height of the bar. </value>
        [Category( "Behavior" )]
        [Description( "Progress height" )]
        [DefaultValue( 0 )]
        public int BarHeight
        {
            get => this._BarHeight;

            set {
                switch ( value )
                {
                    case var @case when @case > this.Size.Height:
                    case var case1 when case1 < 0:
                        {
                            this._BarHeight = this.Size.Height;
                            break;
                        }

                    default:
                        {
                            this._BarHeight = value;
                            break;
                        }
                }

                // Invalidate the control to get a repaint.
                this.Invalidate();
            }
        }

        /// <summary> The caption format. </summary>
        private string _CaptionFormat;

        /// <summary> Specifies the format of the overlay. </summary>
        /// <value> The caption format. </value>
        [Category( "Appearance" )]
        [DefaultValue( "{0} %" )]
        [Description( "Specifies the format of the overlay." )]
        public string CaptionFormat
        {
            get => string.IsNullOrEmpty( this._CaptionFormat ) ? "{0} %" : this._CaptionFormat;

            set => this._CaptionFormat = value;
        }

        /// <summary> The default caption format. </summary>
        public const string DefaultCaptionFormat = "{0} %";

        /// <summary> Updates the progress described by value. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The value. </param>
        public void UpdateProgress( int value )
        {
            string format = this.CaptionFormat;
            if ( string.IsNullOrEmpty( format ) )
            {
                format = DefaultCaptionFormat;
            }

            this.UpdateProgress( value, string.Format( System.Globalization.CultureInfo.CurrentCulture, format, value ) );
        }

        /// <summary> Updates the progress described by arguments. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value">   The value. </param>
        /// <param name="caption"> The caption. </param>
        public void UpdateProgress( int value, string caption )
        {
            if ( value >= 0 ^ this.Visible )
            {
                this.Visible = value >= 0;
            }

            if ( this.Visible )
            {
                this.Text = caption;
                this.Value = value;
            }
        }
        #endregion

    }
}
