using System;
using System.Diagnostics;
using System.Windows.Forms;
using System.Windows.Forms.Design;

using isr.Core.Controls.ExceptionExtensions;

namespace isr.Core.Controls
{

    /// <summary> Tool strip check box. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-01-23 </para>
    /// </remarks>
    [ToolStripItemDesignerAvailability( ToolStripItemDesignerAvailability.ToolStrip )]
    public class ToolStripCheckBox : ToolStripControlHost, IBindableComponent
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> Call the base constructor passing in a CheckBox instance. </remarks>
        public ToolStripCheckBox() : base( new CheckBox() )
        {
        }

        /// <summary>
        /// Releases the unmanaged resources used by the
        /// <see cref="T:System.Windows.Forms.ToolStripControlHost" /> and optionally releases the
        /// managed resources.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.RemoveCheckedChangedEventHandler( CheckedChanged );
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " CHECK BOX "

        /// <summary> Gets the check box control. </summary>
        /// <value> The check box control. </value>
        public CheckBox CheckBoxControl => this.Control as CheckBox;

        /// <summary> Gets or sets the value. </summary>
        /// <value> The value. </value>
        public bool Checked
        {
            get => this.CheckBoxControl.Checked;

            set => this.CheckBoxControl.Checked = value;
        }

        /// <summary> Subscribes events from the hosted control. </summary>
        /// <remarks> Subscribe the control events to expose. </remarks>
        /// <param name="control"> The control from which to subscribe events. </param>
        protected override void OnSubscribeControlEvents( Control control )
        {
            if ( control is object )
            {
                // Call the base so the base events are connected.
                base.OnSubscribeControlEvents( control );

                // Cast the control to a CheckBox control.
                if ( control is CheckBox containedControl )
                {
                    // Add the event.
                    containedControl.CheckedChanged += this.OnCheckedChanged;
                }
            }
        }

        /// <summary> Unsubscribe events from the hosted control. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="control"> The control from which to unsubscribe events. </param>
        protected override void OnUnsubscribeControlEvents( Control control )
        {
            // Call the base method so the basic events are unsubscribed.
            base.OnUnsubscribeControlEvents( control );

            // Cast the control to a CheckBox control.
            if ( control is CheckBox containedControl )
            {
                // Remove the event.
                containedControl.CheckedChanged -= this.OnCheckedChanged;
            }
        }

        /// <summary> Event queue for all listeners interested in CheckChanged events. </summary>
        public event EventHandler<EventArgs> CheckedChanged;

        /// <summary> Removes event handler. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The handler. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void RemoveCheckedChangedEventHandler( EventHandler<EventArgs> value )
        {
            foreach ( Delegate d in value is null ? (Array.Empty<Delegate>()) : value.GetInvocationList() )
            {
                try
                {
                    CheckedChanged -= ( EventHandler<EventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }
        }

        /// <summary> Raises the checked changed event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information to send to registered event handlers. </param>
        private void OnCheckedChanged( object sender, EventArgs e )
        {
            var evt = CheckedChanged;
            evt?.Invoke( this, e );
        }

        #endregion

        #region " BINDABLE "

        /// <summary> The context. </summary>
        private BindingContext _Context = null;

        /// <summary>
        /// Gets or sets the collection of currency managers for the
        /// <see cref="T:System.Windows.Forms.IBindableComponent" />.
        /// </summary>
        /// <value>
        /// The collection of <see cref="T:System.Windows.Forms.BindingManagerBase" /> objects for this
        /// <see cref="T:System.Windows.Forms.IBindableComponent" />.
        /// </value>
        public BindingContext BindingContext
        {
            get {
                if ( this._Context is null )
                {
                    this._Context = new BindingContext();
                }

                return this._Context;
            }

            set => this._Context = value;
        }

        /// <summary> The bindings. </summary>
        private ControlBindingsCollection _Bindings;

        /// <summary>
        /// Gets the collection of data-binding objects for this
        /// <see cref="T:System.Windows.Forms.IBindableComponent" />.
        /// </summary>
        /// <value>
        /// The <see cref="T:System.Windows.Forms.ControlBindingsCollection" /> for this
        /// <see cref="T:System.Windows.Forms.IBindableComponent" />.
        /// </value>
        public ControlBindingsCollection DataBindings
        {
            get {
                if ( this._Bindings is null )
                {
                    this._Bindings = new ControlBindingsCollection( this );
                }

                return this._Bindings;
            }
        }
        #endregion

        #region " LOST FOCUS and VALIDATING EVENT HANDLING "

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.ToolStripControlHost.LostFocus" /> event.
        /// </summary>
        /// <remarks>   David, 2021-04-19. <para>
        /// Raises the validating event. </para></remarks>
        /// <param name="e">    A <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnLostFocus( EventArgs e )
        {
            base.OnLostFocus( e );
            this.OnValidating( new System.ComponentModel.CancelEventArgs() );
        }

        #endregion

    }
}
