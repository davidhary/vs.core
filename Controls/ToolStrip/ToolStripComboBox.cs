using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace isr.Core.Controls
{

    /// <summary> A tool strip spring Combo box. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-03-22 </para>
    /// </remarks>
    public class ToolStripComboBox : System.Windows.Forms.ToolStripComboBox, ISpringable, IBindableComponent
    {

        #region " BINDABLE "

        /// <summary> The context. </summary>
        private BindingContext _Context = null;

        /// <summary>
        /// Gets or sets the collection of currency managers for the
        /// <see cref="T:System.Windows.Forms.IBindableComponent" />.
        /// </summary>
        /// <value>
        /// The collection of <see cref="T:System.Windows.Forms.BindingManagerBase" /> objects for this
        /// <see cref="T:System.Windows.Forms.IBindableComponent" />.
        /// </value>
        public BindingContext BindingContext
        {
            get {
                if ( this._Context is null )
                {
                    this._Context = new BindingContext();
                }

                return this._Context;
            }

            set => this._Context = value;
        }

        /// <summary> The bindings. </summary>
        private ControlBindingsCollection _Bindings;

        /// <summary>
        /// Gets the collection of data-binding objects for this
        /// <see cref="T:System.Windows.Forms.IBindableComponent" />.
        /// </summary>
        /// <value>
        /// The <see cref="T:System.Windows.Forms.ControlBindingsCollection" /> for this
        /// <see cref="T:System.Windows.Forms.IBindableComponent" />.
        /// </value>
        public ControlBindingsCollection DataBindings
        {
            get {
                if ( this._Bindings is null )
                {
                    this._Bindings = new ControlBindingsCollection( this );
                }

                return this._Bindings;
            }
        }

        #endregion

        #region " LOST FOCUS and VALIDATING EVENT HANDLING "

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.ToolStripControlHost.LostFocus" /> event.
        /// </summary>
        /// <remarks>   David, 2021-04-19. <para>
        /// Raises the validating event. </para></remarks>
        /// <param name="e">    A <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnLostFocus( EventArgs e )
        {
            base.OnLostFocus( e );
            this.OnValidating( new CancelEventArgs() );
        }

        #endregion
		
        #region " I SPRINGABLE "

        /// <summary> Gets the sentinel indicating if the tool strip item can spring. </summary>
        /// <value> The sentinel indicating if the tool strip item can spring. </value>
        public bool CanSpring => this.AutoSize && this.Spring;

        /// <summary> Gets or sets the spring. </summary>
        /// <value>
        /// <c>true</c> if the control stretches to fill the remaining space in the owner control.
        /// </value>
        [DefaultValue( false )]
        [Description( "Spring" )]
        [Category( "Appearance" )]
        public bool Spring { get; set; }

        #endregion

        #region " SPRING IMPLEMENTATION "

        /// <summary> Gets preferred size. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="constrainingSize"> Size of the constraining. </param>
        /// <returns> The preferred size. </returns>
        public override Size GetPreferredSize( Size constrainingSize )
        {

            // Use the default size if the tool strip item is on the overflow menu,
            // is on a vertical ToolStrip, or cannot spring.
            if ( this.IsOnOverflow || this.Owner.Orientation == Orientation.Vertical || !this.CanSpring )
            {
                return this.DefaultSize;
            }

            // Declare a variable to store the total available width as 
            // it is calculated, starting with the display width of the 
            // owning ToolStrip.
            int width = this.Owner.DisplayRectangle.Width;

            // Subtract the width of the overflow button if it is displayed. 
            if ( this.Owner.OverflowButton.Visible )
            {
                width = width - this.Owner.OverflowButton.Width - this.Owner.OverflowButton.Margin.Horizontal;
            }

            if ( this.Owner.GripStyle == ToolStripGripStyle.Visible )
            {
                width -= this.Owner.GripMargin.Horizontal;
            }

            // Declare a variable to maintain a count of Spring items
            // currently displayed in the owning ToolStrip. 
            int springItemCount = 0;
            foreach ( ToolStripItem item in this.Owner.Items )
            {

                // Ignore items on the overflow menu.
                if ( item.IsOnOverflow )
                {
                    continue;
                }

                if ( (item as ISpringable)?.CanSpring == true )
                {
                    // For Spring items, increment the count and 
                    // subtract the margin width from the total available width.
                    springItemCount += 1;
                    width -= item.Margin.Horizontal;
                }
                else
                {
                    // For all other items, subtract the full width from the total
                    // available width.
                    width = width - item.Width - item.Margin.Horizontal;
                }
            }

            // If there are multiple spring items in the owning
            // ToolStrip, divide the total available width between them. 
            if ( springItemCount > 1 )
            {
                width /= springItemCount;
            }

            // If the available width is less than the default width, use the
            // default width, forcing one or more items onto the overflow menu.
            if ( width < this.DefaultSize.Width )
            {
                width = this.DefaultSize.Width;
            }

            // Retrieve the preferred size from the base class, but change the
            // width to the calculated width. 
            var preferredSize = base.GetPreferredSize( constrainingSize );
            preferredSize.Width = width;
            return preferredSize;
        }

        #endregion

    }
}
