namespace isr.Core.Controls
{

    /// <summary> Interface for springable. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public interface ISpringable
    {

        /// <summary> Gets or sets the sentinel indicating if the tool strip item can spring. </summary>
        /// <value> The sentinel indicating if the tool strip item can spring. </value>
        bool CanSpring { get; }

        /// <summary> Gets or sets the spring. </summary>
        /// <value>
        /// <c>true</c> if the control stretches to fill the remaining space in the owner control.
        /// </value>
        bool Spring { get; set; }
    }
}
