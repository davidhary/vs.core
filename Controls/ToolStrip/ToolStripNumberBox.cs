using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;
using System.Windows.Forms.Design;

using isr.Core.Controls.ExceptionExtensions;

namespace isr.Core.Controls
{
    /// <summary> Tool strip number box. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-04-08. </para>
    /// </remarks>
    [ToolStripItemDesignerAvailability( ToolStripItemDesignerAvailability.ToolStrip )]
    public class ToolStripNumberBox : ToolStripControlHost, IBindableComponent
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> Call the base constructor passing in a NumberBox instance. </remarks>
        public ToolStripNumberBox() : base( new NumberBox() )
        {
        }

        /// <summary>
        /// Releases the unmanaged resources used by the
        /// <see cref="T:System.Windows.Forms.ToolStripControlHost" /> and optionally releases the
        /// managed resources.
        /// </summary>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.RemoveLeaveEventHandler( Leave );
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " NUMBER BOX "

        /// <summary> Gets the numeric up down control. </summary>
        /// <value> The numeric up down control. </value>
        public NumberBox NumberBoxControl => this.Control as NumberBox;

        /// <summary> Standard Numeric Format String D,E,F,N,X, B for binary. </summary>
        /// <value> The Standard numeric format string </value>
        [Category( "NumberBox" ), Description( "Standard Numeric Format String D,E,F,N,X, B for binary" )]
        public string Snfs
        {
            get => this.NumberBoxControl.Snfs;
            set => this.NumberBoxControl.Snfs = value;
        }

        /// <summary>   Prefix for X and B formats. </summary>
        /// <value> The prefix. </value>
        [Category( "NumberBox" ), Description( "Prefix for X and B formats" )]
        public string Prefix
        {
            get => this.NumberBoxControl.Prefix;
            set => this.NumberBoxControl.Prefix = value;
        }

        /// <summary>   Suffix for X and B formats. </summary>
        /// <value> The suffix. </value>
        [Category( "NumberBox" ), Description( "Suffix for X and B formats" )]
        public string Suffix
        {
            get => this.NumberBoxControl.Suffix;
            set => this.NumberBoxControl.Suffix = value;
        }

        /// <summary>   Gets/sets Max Value...double. </summary>
        /// <value> The maximum value. </value>
        [Category( "NumberBox" ), Description( "gets/sets Max Value...double" )]
        public double MaxValue
        {
            get => this.NumberBoxControl.MaxValue;
            set => this.NumberBoxControl.MaxValue = value;
        }


        /// <summary>   Gets/sets Min Value...double. </summary>
        /// <value> The minimum value. </value>
        [Category( "NumberBox" ), Description( "gets/sets Min Value...double" )]
        public double MinValue
        {
            get => this.NumberBoxControl.MinValue;
            set => this.NumberBoxControl.MinValue = value;
        }

        /// <summary>   Gets/sets Value As Double. </summary>
        /// <value> The value as double. </value>
        [Category( "NumberBox" ), Description( "gets/sets Value As Double" ), Browsable( true )]
        public double ValueAsDouble
        {
            get => this.NumberBoxControl.ValueAsDouble;
            set => this.NumberBoxControl.ValueAsDouble = value;
        }

        /// <summary>   Gets/sets Value As Float. </summary>
        /// <value> The value as float. </value>
        [Category( "NumberBox" ), Description( "gets/sets Value As Float" ), Browsable( true )]
        public float ValueAsFloat
        {
            get => this.NumberBoxControl.ValueAsFloat;
            set => this.NumberBoxControl.ValueAsFloat = value;
        }

        /// <summary>   Gets/sets Value As Byte. </summary>
        /// <value> The value as byte. </value>
        [Category( "NumberBox" ), Description( "gets/sets Value As Byte" ), Browsable( true )]
        public byte ValueAsByte
        {
            get => this.NumberBoxControl.ValueAsByte;
            set => this.NumberBoxControl.ValueAsByte = value;
        }

        /// <summary>   Gets/sets Value As SByte. </summary>
        /// <value> The value as s byte. </value>
        [Category( "NumberBox" ), Description( "gets/sets Value As SByte" ), Browsable( true ), CLSCompliant( false )]
        public sbyte ValueAsSByte
        {
            get => this.NumberBoxControl.ValueAsSByte;
            set => this.NumberBoxControl.ValueAsSByte = value;
        }

        /// <summary>   Gets/sets Value As UInt64. </summary>
        /// <value> The value as u int 64. </value>
        [Category( "NumberBox" ), Description( "gets/sets Value As UInt64" ), Browsable( true ), CLSCompliant( false )]
        public ulong ValueAsUInt64
        {
            get => this.NumberBoxControl.ValueAsUInt64;
            set => this.NumberBoxControl.ValueAsUInt64 = value;
        }

        /// <summary>   Gets/sets Value As UInt32. </summary>
        /// <value> The value as u int 32. </value>
        [Category( "NumberBox" ), Description( "gets/sets Value As UInt32" ), Browsable( true ), CLSCompliant( false )]
        public uint ValueAsUInt32
        {
            get => this.NumberBoxControl.ValueAsUInt32;
            set => this.NumberBoxControl.ValueAsUInt32 = value;
        }

        /// <summary>   Gets/sets Value As UInt16. </summary>
        /// <value> The value as u int 16. </value>
        [Category( "NumberBox" ), Description( "gets/sets Value As UInt16" ), Browsable( true ), CLSCompliant( false )]
        public ushort ValueAsUInt16
        {
            get => this.NumberBoxControl.ValueAsUInt16;
            set => this.NumberBoxControl.ValueAsUInt16 = value;
        }

        /// <summary>   Gets/sets Value As Int64. </summary>
        /// <value> The value as int 64. </value>
        [Category( "NumberBox" ), Description( "gets/sets Value As Int64" ), Browsable( true )]
        public long ValueAsInt64
        {
            get => this.NumberBoxControl.ValueAsInt64;
            set => this.NumberBoxControl.ValueAsInt64 = value;
        }

        /// <summary>   Gets/sets Value As Int32. </summary>
        /// <value> The value as int 32. </value>
        [Category( "NumberBox" ), Description( "gets/sets Value As Int32" ), Browsable( true )]
        public int ValueAsInt32
        {
            get => this.NumberBoxControl.ValueAsInt32;
            set => this.NumberBoxControl.ValueAsInt32 = value;
        }

        /// <summary>   Gets/sets Value As Int16. </summary>
        /// <value> The value as int 16. </value>
        [Category( "NumberBox" ), Description( "gets/sets Value As Int16" ), Browsable( true )]
        public short ValueAsInt16
        {
            get => this.NumberBoxControl.ValueAsInt16;
            set => this.NumberBoxControl.ValueAsInt16 = value;
        }

        /// <summary> Subscribes events from the hosted control. </summary>
        /// <remarks> Subscribe the control events to expose. </remarks>
        /// <param name="control"> The control from which to subscribe events. </param>
        protected override void OnSubscribeControlEvents( Control control )
        {
            if ( control is object )
            {
                // Call the base so the base events are connected.
                base.OnSubscribeControlEvents( control );

                // Cast the control to a NumberBox control.
                if ( control is NumberBox containedControl )
                {
                    // Add the event.
                    containedControl.Leave += this.OnLeave;
                }
            }
        }

        /// <summary> Unsubscribe events from the hosted control. </summary>
        /// <param name="control"> The control from which to unsubscribe events. </param>
        protected override void OnUnsubscribeControlEvents( Control control )
        {
            // Call the base method so the basic events are unsubscribed.
            base.OnUnsubscribeControlEvents( control );

            // Cast the control to a NumberBox control.
            if ( control is NumberBox containedControl )
            {
                // Remove the event.
                containedControl.Leave -= this.OnLeave;
            }
        }

        /// <summary> Event queue for all listeners interested in Leave events. </summary>
        public new event EventHandler<EventArgs> Leave;

        /// <summary> Removes event handler. </summary>
        /// <param name="value"> The handler. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void RemoveLeaveEventHandler( EventHandler<EventArgs> value )
        {
            foreach ( Delegate d in value is null ? (Array.Empty<Delegate>()) : value.GetInvocationList() )
            {
                try
                {
                    Leave -= ( EventHandler<EventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }
        }

        /// <summary> Raises the checked changed event. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information to send to registered event handlers. </param>
        private void OnLeave( object sender, EventArgs e )
        {
            var evt = Leave;
            evt?.Invoke( this, e );
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.ToolStripControlHost.LostFocus" /> event.
        /// </summary>
        /// <remarks>   David, 2021-03-30. </remarks>
        /// <param name="e">    A <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnLostFocus( EventArgs e )
        {
            base.OnLostFocus( e );
            this.OnValidating( new CancelEventArgs() );
        }

        #endregion

        #region " BINDING "

        private BindingContext _Context = null;

        /// <summary>
        /// Gets or sets the collection of currency managers for the
        /// <see cref="T:System.Windows.Forms.IBindableComponent" />.
        /// </summary>
        /// <value>
        /// The collection of <see cref="T:System.Windows.Forms.BindingManagerBase" /> objects for this
        /// <see cref="T:System.Windows.Forms.IBindableComponent" />.
        /// </value>
        public BindingContext BindingContext
        {
            get {
                if ( this._Context is null )
                {
                    this._Context = new BindingContext();
                }

                return this._Context;
            }

            set => this._Context = value;
        }

        private ControlBindingsCollection _Bindings;

        /// <summary>
        /// Gets the collection of data-binding objects for this
        /// <see cref="T:System.Windows.Forms.IBindableComponent" />.
        /// </summary>
        /// <value>
        /// The <see cref="T:System.Windows.Forms.ControlBindingsCollection" /> for this
        /// <see cref="T:System.Windows.Forms.IBindableComponent" />.
        /// </value>
        public ControlBindingsCollection DataBindings
        {
            get {
                if ( this._Bindings is null )
                {
                    this._Bindings = new ControlBindingsCollection( this );
                }

                return this._Bindings;
            }
        }

        #endregion

    }
}
