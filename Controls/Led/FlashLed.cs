using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace isr.Core.Controls
{

    /// <summary> A flashing LED Control. </summary>
    /// <remarks>
    /// (c) 2007 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2007-09-04" by="HORIA TUDOSIE" revision="1.0.2803.x </para><para>
    /// http://www.CodeProject.com/cs/MISCCTRL/FlashLED.asp. </para><para>
    /// David, 2007-09-05, 1.0.2804 Added Flush Color property. </para>
    /// </remarks>
    [Description( "Flashing LED Control" )]
    public partial class FlashLed : Forma.ModelViewBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public FlashLed() : base()
        {

            // Initialize user components that might be affected by resize or paint actions

            // This call is required by the Windows Form Designer.
            this.InitializeComponent();

            // Add any initialization after the InitializeComponent() call
            this.SetStyle( ControlStyles.AllPaintingInWmPaint, true );
            this.SetStyle( ControlStyles.DoubleBuffer, true );
            this.SetStyle( ControlStyles.UserPaint, true );
            this.SetStyle( ControlStyles.ResizeRedraw, true );

            // These too lines are required to make the control transparent.
            this.SetStyle( ControlStyles.SupportsTransparentBackColor, true );
            this._FlashColor = Color.Red;
            // _flash = False
            this._FlashColorsArray = new Color[5] { Color.Red, Color.Empty, Color.Yellow, Color.Empty, Color.Blue };
            this._FlashColors = "Red,,Yellow,,Blue";
            this._FlashIntervalsArray = new int[6] { 500, 250, 500, 250, 500, 250 };
            this._FlashIntervals = "500,250,500,250,500,250";
            this._SparklePenWidth = 2f;
            this.Active = true;
            this.BackColor = Color.Transparent;
            this.OffColor = SystemColors.Control;
            this.OnColor = Color.Red;

            // Set the Width and Height to an odd number for the convenience of having a pixel in the center 
            // of the control. 17 is a good size for a perfect circle for a circular LED.
            // Width = 17
            // Height = 17
            try
            {
                this._TickTimer = new Timer();
            }
            catch
            {
                this._TickTimer?.Dispose();
                throw;
            }

            this._TickTimer.Enabled = false;
            this._TickTimer.Tick += this.TickTimerTick;
        }

        /// <summary>
        /// Releases the unmanaged resources used by the isr.Core.Forma.ModelViewBase and optionally
        /// releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                        this.components = null;
                    }

                    if ( this._TickTimer is object )
                    {
                        this._TickTimer.Dispose();
                        this._TickTimer = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> true to active. </summary>
        private bool _Active;

        /// <summary>
        /// Turns on the <see cref="OnColor">on color</see> when True or the
        /// <see cref="OffColor">off color</see> when False.
        /// </summary>
        /// <value> The active. </value>
        [Category( "Behavior" )]
        [DefaultValue( true )]
        public bool Active
        {
            get => this._Active;

            set {
                this._Active = value;
                this.Invalidate();
            }
        }

        /// <summary> true to flash. </summary>
        private bool _Flash;

        /// <summary> Gets or sets the Flash condition.  When True, the Led flashes. </summary>
        /// <value> The flash. </value>
        [Category( "Behavior" )]
        [DefaultValue( false )]
        public bool Flash
        {
            get => this._Flash;

            set {
                this._Flash = value; // AndAlso (Me._flashIntervalsArray.Length > 0)
                this._TickIndex = 0;
                this._TickTimer.Interval = this._FlashIntervalsArray[this._TickIndex];
                this._TickTimer.Enabled = this._Flash;
                this.Active = this.Active;
            }
        }

        /// <summary> The flash color. </summary>
        private Color _FlashColor;

        /// <summary> Gets or sets the Flash color. </summary>
        /// <value> The color of the flash. </value>
        [Category( "Appearance" )]
        public Color FlashColor
        {
            get => this._FlashColor;

            set {
                this._FlashColor = value;
                this.Invalidate();
            }
        }

        /// <summary>
        /// Holds the array of flash colors in case a set of colors is used.
        /// </summary>
        private Color[] _FlashColorsArray;

        /// <summary> List of colors of the flashes. </summary>
        private string _FlashColors;

        /// <summary> Gets or sets the set of flash colors. </summary>
        /// <remarks>
        /// Accepts a delimited string that is converted to a color array for setting the flash color.
        /// Any reasonable delimiter will break the input string, and error items will default to
        /// Color.Empty for colors that will switch the LED Off.
        /// </remarks>
        /// <value> A list of colors of the flashes. </value>
        [Category( "Appearance" )]
        [DefaultValue( "Red,,Yellow,,Blue" )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public string FlashColors
        {
            get => this._FlashColors;

            set {
                this._FlashColors = string.IsNullOrWhiteSpace( value ) ? string.Empty : value;
                if ( string.IsNullOrWhiteSpace( this._FlashColors ) )
                {
                    this._FlashColorsArray = null;
                }
                else
                {
                    var fc = this._FlashColors.Split( new char[] { ',', '/', '|', ' ', Environment.NewLine.ToCharArray()[0], Environment.NewLine.ToCharArray()[1] } );
                    this._FlashColorsArray = new Color[fc.Length];
                    for ( int i = 0, loopTo = fc.Length - 1; i <= loopTo; i++ )
                    {
                        try
                        {
                            this._FlashColorsArray[i] = string.IsNullOrEmpty( fc[i] ) ? Color.Empty : Color.FromName( fc[i] );
                        }
                        catch
                        {
                            this._FlashColorsArray[i] = Color.Empty;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Holds the array of flash intervals.
        /// </summary>
        private int[] _FlashIntervalsArray;

        /// <summary> The flash intervals. </summary>
        private string _FlashIntervals;

        /// <summary> Gets or sets the set of flash intervals. </summary>
        /// <remarks>
        /// Accepts a delimited string that is converted to a interval array for setting the flash
        /// intervals. Any reasonable delimiter will break the input string, and error items will default
        /// to 25 ms for the interval.
        /// </remarks>
        /// <value> The flash intervals. </value>
        [Category( "Appearance" )]
        [DefaultValue( "500,250,500,250,500,250" )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public string FlashIntervals
        {
            get => this._FlashIntervals;

            set {
                this._FlashIntervals = string.IsNullOrWhiteSpace( value ) ? string.Empty : value;
                if ( string.IsNullOrWhiteSpace( this._FlashIntervals ) )
                {
                    this._FlashIntervalsArray = null;
                }
                else
                {
                    var fi = this._FlashIntervals.Split( new char[] { ',', '/', '|', ' ', Environment.NewLine.ToCharArray()[0], Environment.NewLine.ToCharArray()[1] } );
                    this._FlashIntervalsArray = new int[fi.Length];
                    for ( int i = 0, loopTo = fi.Length - 1; i <= loopTo; i++ )
                    {
                        try
                        {
                            this._FlashIntervalsArray[i] = int.Parse( fi[i], System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.CurrentCulture );
                        }
                        catch
                        {
                            this._FlashIntervalsArray[i] = 25;
                        }
                    }
                }
            }
        }

        /// <summary> The off color. </summary>
        private Color _OffColor;

        /// <summary> Gets or sets the OFF color. </summary>
        /// <value> The color of the off. </value>
        [Category( "Appearance" )]
        public Color OffColor
        {
            get => this._OffColor;

            set {
                this._OffColor = value;
                this.Invalidate();
            }
        }

        /// <summary> The on color. </summary>
        private Color _OnColor;

        /// <summary> Gets or sets the ON color. </summary>
        /// <value> The color of the on. </value>
        [Category( "Appearance" )]
        public Color OnColor
        {
            get => this._OnColor;

            set {
                this._OnColor = value;
                this.Invalidate();
            }
        }

        #endregion

        #region " HELPER COLOR FUNCTIONS "

        /// <summary>
        /// Blends the <para>firstColor</para> over the <para>secondColor</para> with a weighted blend of
        /// <para>firstWeight</para> and <para>secondWeight</para>.
        /// </summary>
        /// <remarks>
        /// The function works by splitting the two colors in R, G and B components, applying the ratio,
        /// and returning the recomposed color. This function blends the LED color in the margin of the
        /// bubble, and adds the sparkle that will give the bubble's volume. When the LED is On, the
        /// margin is enlightened with White, while - when Off is darkened with Black. The same for the
        /// sparkle, but with different ratios.
        /// </remarks>
        /// <param name="firstColor">   The first color. </param>
        /// <param name="secondColor">  The second color. </param>
        /// <param name="firstWeight">  The first weight. </param>
        /// <param name="secondWeight"> The second weight. </param>
        /// <returns> null if it fails, else a Color. </returns>
        public static Color FadeColor( Color firstColor, Color secondColor, int firstWeight, int secondWeight )
        {
            int r = ( int ) ((firstWeight * firstColor.R + secondWeight * secondColor.R) / ( double ) (firstWeight + secondWeight));
            int g = ( int ) ((firstWeight * firstColor.G + secondWeight * secondColor.G) / ( double ) (firstWeight + secondWeight));
            int b = ( int ) ((firstWeight * firstColor.B + secondWeight * secondColor.B) / ( double ) (firstWeight + secondWeight));
            return Color.FromArgb( r, g, b );
        }

        /// <summary> Blends two colors with equal weights. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="firstColor">  The first color. </param>
        /// <param name="secondColor"> The second color. </param>
        /// <returns> A Color. </returns>
        public static Color FadeColor( Color firstColor, Color secondColor )
        {
            return FadeColor( firstColor, secondColor, 1, 1 );
        }

        /// <summary> Width of the sparkle pen. </summary>
        private readonly float _SparklePenWidth;

        /// <summary> Draws the LED. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="graphics"> The graphics. </param>
        /// <param name="color">    The color. </param>
        /// <param name="sparkle">  true to sparkle. </param>
        private void DrawLed( Graphics graphics, Color color, bool sparkle )
        {
            float halfPenWidth = this._SparklePenWidth / 2f;
            int imageWidth = this.Width - this.Margin.Left;
            int imageHeight = this.Height - this.Margin.Top;
            if ( sparkle )
            {
                using ( var b = new SolidBrush( color ) )
                {
                    graphics.FillEllipse( b, 1, 1, imageWidth, imageHeight );
                }

                using ( var p = new Pen( FadeColor( color, Color.White, 1, 2 ), this._SparklePenWidth ) )
                {
                    graphics.DrawArc( p, halfPenWidth + 2f, halfPenWidth + 2f, this.Width - 6 - halfPenWidth, this.Height - 6 - halfPenWidth, -90.0f, -90.0f );
                }

                using ( var p = new Pen( FadeColor( color, Color.White ), 1f ) )
                {
                    graphics.DrawEllipse( p, 1, 1, imageWidth, imageHeight );
                }
            }
            else
            {
                using ( var b = new SolidBrush( color ) )
                {
                    graphics.FillEllipse( b, 1, 1, imageWidth, imageHeight );
                }

                using ( var p = new Pen( FadeColor( color, Color.Black, 2, 1 ), this._SparklePenWidth ) )
                {
                    graphics.DrawArc( p, 3f, 3f, this.Width - 7, this.Height - 7, 0.0f, 90.0f );
                }

                using ( var p = new Pen( FadeColor( color, Color.Black ), 1f ) )
                {
                    graphics.DrawEllipse( p, 1, 1, imageWidth, imageHeight );
                }
            }
        }

        #endregion

        #region " EVENT HANDLERS "

        /// <summary>
        /// Event for adding external features.
        /// </summary>
        public new event PaintEventHandler Paint;

        /// <summary> Draws the control. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnPaint( PaintEventArgs e )
        {
            if ( e is null )
            {
                return;
            }

            if ( Paint is object )
            {
                Paint( this, e );
            }
            else
            {
                base.OnPaint( e );
                // e.Graphics.Clear(BackColor);
                if ( this.Enabled )
                {
                    if ( this._Flash )
                    {
                        this.DrawLed( e.Graphics, this._FlashColor, true );
                    }
                    // e.Graphics.FillEllipse(New SolidBrush(Me.FlashColor), 1, 1, Width - 3, Height - 3)
                    // e.Graphics.DrawArc(New Pen(FadeColor(FlashColor, Color.White, 1, 2), 2), 3, 3, Width - 7, Height - 7, -90.0F, -90.0F)
                    // e.Graphics.DrawEllipse(New Pen(FadeColor(FlashColor, Color.White), 1), 1, 1, Width - 3, Height - 3)
                    // if non flash mode, set the on or off color.
                    else if ( this.Active )
                    {
                        this.DrawLed( e.Graphics, this._OnColor, true );
                    }
                    // e.Graphics.FillEllipse(New SolidBrush(Me.OnColor), 1, 1, Width - 3, Height - 3)
                    // e.Graphics.DrawArc(New Pen(FadeColor(OnColor, Color.White, 1, 2), 2), 3, 3, Width - 7, Height - 7, -90.0F, -90.0F)
                    // e.Graphics.DrawEllipse(New Pen(FadeColor(OnColor, Color.White), 1), 1, 1, Width - 3, Height - 3)
                    else
                    {
                        this.DrawLed( e.Graphics, this._OffColor, false );
                        // e.Graphics.FillEllipse(New SolidBrush(OffColor), 1, 1, Width - 3, Height - 3)
                        // e.Graphics.DrawArc(New Pen(FadeColor(OffColor, Color.Black, 2, 1), 2), 3, 3, Width - 7, Height - 7, 0.0F, 90.0F)
                        // e.Graphics.DrawEllipse(New Pen(FadeColor(OffColor, Color.Black), 1), 1, 1, Width - 3, Height - 3)
                    }
                }
                else
                {
                    using var p = new Pen( SystemColors.ControlDark, 1f );
                    e.Graphics.DrawEllipse( p, 1, 1, this.Width - 3, this.Height - 3 );
                }
            }
        }

        /// <summary> Zero-based index of the tick. </summary>
        private int _TickIndex;

        /// <summary> Handles the change of colors in flash mode. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void TickTimerTick( object sender, EventArgs e )
        {
            this._TickIndex += 1;
            if ( this._TickIndex >= this._FlashIntervalsArray.Length )
            {
                this._TickIndex = 0;
            }

            this._TickTimer.Interval = this._FlashIntervalsArray[this._TickIndex];
            if ( this._FlashColorsArray is null )
            {
                // if no color array defined or array color index out of bounds,
                // use the on/off colors to flash
                this.FlashColor = this._FlashColor.Equals( this._OnColor ) ? this._OffColor : this._OnColor;
            }
            else
            {
                this.FlashColor = this._FlashColorsArray.Length <= this._TickIndex || this._FlashColorsArray[this._TickIndex].Equals( Color.Empty )
                    ? this._OffColor
                    : this._FlashColorsArray[this._TickIndex];
            }
        }

        #endregion

    }
}
