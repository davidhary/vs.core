﻿using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace isr.Core.Controls
{
    [Microsoft.VisualBasic.CompilerServices.DesignerGenerated()]
    public partial class FlashLed
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _TickTimer = new Timer(components);
            SuspendLayout();
            // 
            // FlashLed
            // 
            AutoScaleMode = AutoScaleMode.Inherit;
            Margin = new Padding(3, 4, 3, 4);
            Name = "FlashLed";
            Size = new Size(20, 22);
            ResumeLayout(false);
        }

        private Timer _TickTimer;
    }
}