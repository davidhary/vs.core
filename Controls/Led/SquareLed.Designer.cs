﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace isr.Core.Controls
{
    [Microsoft.VisualBasic.CompilerServices.DesignerGenerated()]
    public partial class SquareLed
    {

        /// <summary> Default constructor. </summary>
        public SquareLed() : base()
        {
            // This call is required by the Windows Form Designer.
            InitializeComponent();
        }

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;
        private Label _IndicatorLabel;

        private Label IndicatorLabel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _IndicatorLabel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_IndicatorLabel != null)
                {
                    _IndicatorLabel.Click -= IndicatorLabel_Click;
                }

                _IndicatorLabel = value;
                if (_IndicatorLabel != null)
                {
                    _IndicatorLabel.Click += IndicatorLabel_Click;
                }
            }
        }

        private Label CaptionLabel;
        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            CaptionLabel = new Label();
            _IndicatorLabel = new Label();
            _IndicatorLabel.Click += new EventHandler(IndicatorLabel_Click);
            SuspendLayout();
            // 
            // captionLabel
            // 
            CaptionLabel.BackColor = Color.Transparent;
            CaptionLabel.Cursor = Cursors.Default;
            CaptionLabel.ForeColor = SystemColors.ControlText;
            CaptionLabel.Location = new Point(0, 0);
            CaptionLabel.Name = "captionLabel";
            CaptionLabel.RightToLeft = RightToLeft.No;
            CaptionLabel.Size = new Size(33, 13);
            CaptionLabel.TabIndex = 0;
            CaptionLabel.Text = "TITLE";
            CaptionLabel.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // indicatorLabel
            // 
            _IndicatorLabel.BackColor = Color.Green;
            _IndicatorLabel.BorderStyle = BorderStyle.FixedSingle;
            _IndicatorLabel.Cursor = Cursors.Default;
            _IndicatorLabel.ForeColor = Color.Red;
            _IndicatorLabel.Location = new Point(8, 16);
            _IndicatorLabel.Name = "_indicatorLabel";
            _IndicatorLabel.RightToLeft = RightToLeft.No;
            _IndicatorLabel.Size = new Size(13, 13);
            _IndicatorLabel.TabIndex = 1;
            // 
            // SquareLed
            // 
            Controls.Add(_IndicatorLabel);
            Controls.Add(CaptionLabel);
            Name = "SquareLed";
            Size = new Size(33, 33);
            Resize += new EventHandler(SquareLed_Resize);
            ResumeLayout(false);
        }
    }
}