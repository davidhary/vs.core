using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

using isr.Core.Controls.ExceptionExtensions;

namespace isr.Core.Controls
{

    /// <summary> Defines a simple LED display with a caption. </summary>
    /// <remarks>
    /// (c) 2006 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2006-09-05, 2.0.2439 </para>
    /// </remarks>
    [System.ComponentModel.Description( "Squared Led Control" )]
    public partial class SquareLed : Forma.ModelViewBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Releases the unmanaged resources used by the isr.Core.Forma.ModelViewBase and optionally
        /// releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.RemoveClickEvent( Click );
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " APPEARANCE "

        /// <summary> The background color. </summary>
        private int _BackgroundColor;

        /// <summary> Gets or sets the color of the background. </summary>
        /// <value> The color of the background. </value>
        [System.ComponentModel.Browsable( true )]
        [System.ComponentModel.Category( "Appearance" )]
        [System.ComponentModel.Description( "Background Color." )]
        public int BackgroundColor
        {
            get => this._BackgroundColor;

            set {
                if ( value != this.BackgroundColor )
                {
                    this._BackgroundColor = value;
                    this.BackColor = ColorTranslator.FromOle( value );
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The caption. </summary>
        private string _Caption;

        /// <summary> Gets or sets the caption. </summary>
        /// <value> The caption. </value>
        [System.ComponentModel.Browsable( true )]
        [System.ComponentModel.Category( "Appearance" )]
        [System.ComponentModel.Description( "Caption." )]
        public string Caption
        {
            get => this._Caption;

            set {
                if ( !string.Equals( value, this.Caption ) )
                {
                    this._Caption = value;
                    this.CaptionLabel.Text = this.Caption;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The indicator color. </summary>
        private int _IndicatorColor;

        /// <summary> Gets or sets the color of the indicator. </summary>
        /// <value> The color of the indicator. </value>
        [System.ComponentModel.Browsable( true )]
        [System.ComponentModel.Category( "Appearance" )]
        [System.ComponentModel.Description( "Indicator color." )]
        public int IndicatorColor
        {
            get => this._IndicatorColor;

            set {
                if ( value != this.IndicatorColor )
                {
                    this._IndicatorColor = value;
                    this.IndicatorLabel.BackColor = ColorTranslator.FromOle( this._IndicatorColor );
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The state. </summary>
        private IndicatorState _State;

        /// <summary> Gets or sets the is on. </summary>
        /// <value> The is on. </value>
        [System.ComponentModel.Browsable( true )]
        [System.ComponentModel.Category( "Appearance" )]
        [System.ComponentModel.Description( "Is On." )]
        public bool IsOn
        {
            get => this._State == IndicatorState.On;

            set {
                if ( value != this.IsOn )
                {
                    this.State = value ? IndicatorState.On : IndicatorState.Off;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The no color. </summary>
        private int _NoColor;

        /// <summary> Gets or sets the color of the no. </summary>
        /// <value> The color of the no. </value>
        [System.ComponentModel.Browsable( true )]
        [System.ComponentModel.Category( "Appearance" )]
        [System.ComponentModel.Description( "No Color." )]
        public int NoColor
        {
            get => this._NoColor;

            set {
                if ( value != this.NoColor )
                {
                    this._NoColor = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The off color. </summary>
        private int _OffColor;

        /// <summary> Gets or sets the color of the off. </summary>
        /// <value> The color of the off. </value>
        [System.ComponentModel.Browsable( true )]
        [System.ComponentModel.Category( "Appearance" )]
        [System.ComponentModel.Description( "Off Color." )]
        public int OffColor
        {
            get => this._OffColor;

            set {
                if ( value != this.OffColor )
                {
                    this._OffColor = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The on color. </summary>
        private int _OnColor;

        /// <summary> Gets or sets the color of the on. </summary>
        /// <value> The color of the on. </value>
        [System.ComponentModel.Browsable( true )]
        [System.ComponentModel.Category( "Appearance" )]
        [System.ComponentModel.Description( "On Color." )]
        public int OnColor
        {
            get => this._OnColor;

            set {
                if ( value != this.OnColor )
                {
                    this._OnColor = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the indicator color based on the state. </summary>
        /// <value> The state. </value>
        [System.ComponentModel.Browsable( true )]
        [System.ComponentModel.Category( "Appearance" )]
        [System.ComponentModel.Description( "Current State." )]
        public IndicatorState State
        {
            get => this._State;

            set {
                if ( value != this.State )
                {
                    this._State = value;
                    if ( this.Visible )
                    {
                        switch ( true )
                        {
                            case object _ when value == IndicatorState.None:
                                {
                                    this.IndicatorColor = this.NoColor;
                                    break;
                                }

                            case object _ when value == IndicatorState.On:
                                {
                                    this.IndicatorColor = this.OnColor;
                                    break;
                                }

                            case object _ when value == IndicatorState.Off:
                                {
                                    this.IndicatorColor = this.OffColor;
                                    break;
                                }
                        }
                    }

                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " EVENTS HANDLERS "

        /// <summary>Occurs when the user clicks the indicator.
        /// </summary>
        public new event EventHandler<EventArgs> Click;

        /// <summary> Removes click event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The value. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void RemoveClickEvent( EventHandler<EventArgs> value )
        {
            foreach ( Delegate d in value is null ? (Array.Empty<Delegate>()) : value.GetInvocationList() )
            {
                try
                {
                    Click -= ( EventHandler<EventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }
        }

        /// <summary> Raises the click event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="eventSender"> The source of the event. </param>
        /// <param name="e">           The <see cref="EventArgs" /> instance containing the event
        /// data. </param>
        private void IndicatorLabel_Click( object eventSender, EventArgs e )
        {
            var evt = Click;
            evt?.Invoke( this, e );
        }

        /// <summary> Position the controls. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="eventSender"> The source of the event. </param>
        /// <param name="eventArgs">   Event information. </param>
        private void SquareLed_Resize( object eventSender, EventArgs eventArgs )
        {
            this.CaptionLabel.SetBounds( (this.Width - this.CaptionLabel.Width) / 2, 0, this.Width, this.CaptionLabel.Height, BoundsSpecified.All );
            this.IndicatorLabel.SetBounds( (this.Width - this.IndicatorLabel.Width) / 2, this.Height - this.IndicatorLabel.Height, 0, 0, BoundsSpecified.X | BoundsSpecified.Y );
        }

        #endregion

    }

    /// <summary> Enumerates the indicator states. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public enum IndicatorState
    {

        /// <summary>   An enum constant representing the none option. </summary>
        [System.ComponentModel.Description( "None" )]
        None,

        /// <summary>   An enum constant representing the on option. </summary>
        [System.ComponentModel.Description( "On" )]
        On,

        /// <summary>   An enum constant representing the off option. </summary>
        [System.ComponentModel.Description( "Off" )]
        Off
    }
}
