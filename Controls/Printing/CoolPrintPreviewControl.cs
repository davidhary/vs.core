using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.Windows.Forms;

using isr.Core.Controls.ExceptionExtensions;

using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Controls
{

    /// <summary> Cool print preview control. </summary>
    /// <remarks>
    /// (c) 2009 Bernardo Castillo. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2009-08-05 from Bernardo Castilho </para><para>
    /// http://www.codeproject.com/Articles/38758/An-Enhanced-PrintPreviewDialog. </para>
    /// </remarks>
    [Description( "Print Preview Control" )]
    public class CoolPrintPreviewControl : UserControl
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public CoolPrintPreviewControl() : base()
        {
            this._Himm2pix = new PointF( -1.0f, -1.0f );
            this._PageImages = new List<Image>();
            this.BackColor = SystemColors.AppWorkspace;
            this.ZoomMode = ZoomMode.FullPage;
            this.StartPage = 0;
            this.SetStyle( ControlStyles.OptimizedDoubleBuffer, true );
        }

        /// <summary>
        /// Releases the unmanaged resources used by the CoolPrintPreviewVB.CoolPrintPreviewControl and
        /// optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this._BackBrush is object )
                    {
                        this._BackBrush.Dispose();
                        this._BackBrush = null;
                    }

                    this.RemovePageCountChangedEventHandler( PageCountChanged );
                    this.RemoveStartPageChangedEventHandler( StartPageChanged );
                    this.RemoveZoomModeChangedEventHandler( ZoomModeChanged );
                    this._PageImages?.Clear();
                    this._PageImages = null;
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " EVENT HANDLERS "

        /// <summary> Print document end print. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Print event information. </param>
        private void PrintDocumentEndPrint( object sender, PrintEventArgs e )
        {
            this.SyncPageImages( true );
        }

        /// <summary> Print document print page. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Print page event information. </param>
        private void PrintDocumentPrintPage( object sender, PrintPageEventArgs e )
        {
            if ( e is null )
            {
                return;
            }

            this.SyncPageImages( false );
            if ( this._Cancel )
            {
                e.Cancel = true;
            }
        }

        #endregion

        #region " FUNCTIONS "

        /// <summary> True to cancel. </summary>
        private bool _Cancel;

        /// <summary> Cancels printing. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public void Cancel()
        {
            this._Cancel = true;
        }

        /// <summary> Gets an image. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="page"> The page. </param>
        /// <returns> The image. </returns>
        private Image GetImage( int page )
        {
            return page > -1 && page < this.PageCount ? this._PageImages[page] : null;
        }

        /// <summary> Gets image rectangle. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="img"> The image. </param>
        /// <returns> The image rectangle. </returns>
        private Rectangle GetImageRectangle( Image img )
        {
            var sz = this.GetImageSizeInPixels( img );
            var rc = new Rectangle( 0, 0, sz.Width, sz.Height );
            var rcCli = this.ClientRectangle;
            switch ( this._ZoomMode )
            {
                case ZoomMode.ActualSize:
                    {
                        this._Zoom = 1d;
                        break;
                    }

                case ZoomMode.FullPage:
                    {
                        this._Zoom = Math.Min( Conversions.ToDouble( Interaction.IIf( rc.Width > 0, rcCli.Width / ( double ) rc.Width, 0 ) ), Conversions.ToDouble( Interaction.IIf( rc.Height > 0, rcCli.Height / ( double ) rc.Height, 0 ) ) );
                        break;
                    }

                case ZoomMode.PageWidth:
                    {
                        this._Zoom = Conversions.ToDouble( Interaction.IIf( rc.Width > 0, rcCli.Width / ( double ) rc.Width, 0 ) );
                        break;
                    }

                case ZoomMode.TwoPages:
                    {
                        rc.Width *= 2;
                        this._Zoom = Math.Min( Conversions.ToDouble( Interaction.IIf( rc.Width > 0, rcCli.Width / ( double ) rc.Width, 0 ) ), Conversions.ToDouble( Interaction.IIf( rc.Height > 0, rcCli.Height / ( double ) rc.Height, 0 ) ) );
                        break;
                    }

                default:
                    {
                        break;
                    }
            }
            // not used Dim zoomX As Double = CDbl(IIf((rc.Width > 0), (CDbl(rcCli.Width) / CDbl(rc.Width)), 0))
            // not used Dim zoomY As Double = CDbl(IIf((rc.Height > 0), (CDbl(rcCli.Height) / CDbl(rc.Height)), 0))
            rc.Width = ( int ) (rc.Width * this._Zoom);
            rc.Height = ( int ) (rc.Height * this._Zoom);
            int dx = ( int ) ((rcCli.Width - rc.Width) / 2d);
            if ( dx > 0 )
            {
                rc.X += dx;
            }

            int dy = ( int ) ((rcCli.Height - rc.Height) / 2d);
            if ( dy > 0 )
            {
                rc.Y += dy;
            }

            rc.Inflate( -4, -4 );
            if ( this._ZoomMode == ZoomMode.TwoPages )
            {
                rc.Inflate( -2, 0 );
            }

            return rc;
        }

        /// <summary> The himm 2pix. </summary>
        private PointF _Himm2pix;

        /// <summary> Gets image size in pixels. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="img"> The image. </param>
        /// <returns> The image size in pixels. </returns>
        private Size GetImageSizeInPixels( Image img )
        {
            var szf = img.PhysicalDimension;
            if ( img is Metafile )
            {
                if ( this._Himm2pix.X < 0.0f )
                {
                    using var g = this.CreateGraphics();
                    this._Himm2pix.X = g.DpiX / 2540.0f;
                    this._Himm2pix.Y = g.DpiY / 2540.0f;
                }

                szf.Width *= this._Himm2pix.X;
                szf.Height *= this._Himm2pix.Y;
            }

            return Size.Truncate( szf );
        }

        /// <summary>
        /// Determines whether the specified key is a regular input key or a special key that requires
        /// preprocessing.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="keyData"> One of the <see cref="T:System.Windows.Forms.Keys" /> values. </param>
        /// <returns> true if the specified key is a regular input key; otherwise, false. </returns>
        protected override bool IsInputKey( Keys keyData )
        {
            switch ( keyData )
            {
                case Keys.Prior:
                case Keys.Next:
                case Keys.End:
                case Keys.Home:
                case Keys.Left:
                case Keys.Up:
                case Keys.Right:
                case Keys.Down:
                    {
                        return true;
                    }
            }

            return base.IsInputKey( keyData );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.KeyDown" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.KeyEventArgs" /> that contains the event
        /// data. </param>
        protected override void OnKeyDown( KeyEventArgs e )
        {
            if ( e is null )
            {
                return;
            }

            base.OnKeyDown( e );
            if ( e.Handled )
            {
                return;
            }

            switch ( e.KeyCode )
            {

                // arrow keys scroll or browse, depending on ZoomMode
                case Keys.Left:
                case Keys.Up:
                case Keys.Right:
                case Keys.Down:
                    {

                        // browse
                        if ( this.ZoomMode == ZoomMode.FullPage || this.ZoomMode == ZoomMode.TwoPages )
                        {
                            switch ( e.KeyCode )
                            {
                                case Keys.Left:
                                case Keys.Up:
                                    {
                                        this.StartPage -= 1;
                                        break;
                                    }

                                case Keys.Right:
                                case Keys.Down:
                                    {
                                        this.StartPage += 1;
                                        break;
                                    }
                            }
                        }

                        // scroll
                        var pt = this.AutoScrollPosition;
                        switch ( e.KeyCode )
                        {
                            case Keys.Left:
                                {
                                    pt.X += 20;
                                    break;
                                }

                            case Keys.Right:
                                {
                                    pt.X -= 20;
                                    break;
                                }

                            case Keys.Up:
                                {
                                    pt.Y += 20;
                                    break;
                                }

                            case Keys.Down:
                                {
                                    pt.Y -= 20;
                                    break;
                                }
                        }

                        this.AutoScrollPosition = new Point( -pt.X, -pt.Y );
                        break;
                    }

                // page up/down browse pages
                case Keys.PageUp:
                    {
                        this.StartPage -= 1;
                        break;
                    }

                case Keys.PageDown:
                    {
                        this.StartPage += 1;
                        break;
                    }

                // home/end 
                case Keys.Home:
                    {
                        this.AutoScrollPosition = Point.Empty;
                        this.StartPage = 0;
                        break;
                    }

                case Keys.End:
                    {
                        this.AutoScrollPosition = Point.Empty;
                        this.StartPage = this.PageCount - 1;
                        break;
                    }

                default:
                    {
                        return;
                    }
            }

            // if we got here, the event was handled
            e.Handled = true;
        }

        /// <summary> The point last. </summary>
        private Point _PtLast;

        /// <summary> Raises the mouse event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnMouseDown( MouseEventArgs e )
        {
            if ( e is null )
            {
                return;
            }

            base.OnMouseDown( e );
            if ( e.Button == MouseButtons.Left && this.AutoScrollMinSize != Size.Empty )
            {
                this.Cursor = Cursors.NoMove2D;
                this._PtLast = new Point( e.X, e.Y );
            }
        }

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.MouseMove" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnMouseMove( MouseEventArgs e )
        {
            if ( e is null )
            {
                return;
            }

            base.OnMouseMove( e );
            if ( ReferenceEquals( this.Cursor, Cursors.NoMove2D ) )
            {
                int dx = e.X - this._PtLast.X;
                int dy = e.Y - this._PtLast.Y;
                if ( dx != 0 || dy != 0 )
                {
                    var pt = this.AutoScrollPosition;
                    this.AutoScrollPosition = new Point( -(pt.X + dx), -(pt.Y + dy) );
                    this._PtLast = new Point( e.X, e.Y );
                }
            }
        }

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.MouseUp" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnMouseUp( MouseEventArgs e )
        {
            if ( e is null )
            {
                return;
            }

            base.OnMouseUp( e );
            if ( e.Button == MouseButtons.Left && ReferenceEquals( this.Cursor, Cursors.NoMove2D ) )
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Event queue for all listeners interested in PageCountChanged events. </summary>
        public event EventHandler<EventArgs> PageCountChanged;

        /// <summary> Removes event handler. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The handler. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void RemovePageCountChangedEventHandler( EventHandler<EventArgs> value )
        {
            foreach ( Delegate d in value is null ? (Array.Empty<Delegate>()) : value.GetInvocationList() )
            {
                try
                {
                    PageCountChanged -= ( EventHandler<EventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }
        }

        /// <summary> Raises the page count changed event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected void OnPageCountChanged( EventArgs e )
        {
            PageCountChanged?.Invoke( this, e );
        }

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.Paint" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnPaint( PaintEventArgs e )
        {
            if ( e is null )
            {
                return;
            }

            var img = this.GetImage( this.StartPage );
            if ( img is object )
            {
                var rc = this.GetImageRectangle( img );
                if ( rc.Width > 2 && rc.Height > 2 )
                {
                    rc.Offset( this.AutoScrollPosition );
                    if ( this._ZoomMode != ZoomMode.TwoPages )
                    {
                        RenderPage( e.Graphics, img, rc );
                    }
                    else
                    {
                        rc.Width = ( int ) ((rc.Width - 4) / 2d);
                        RenderPage( e.Graphics, img, rc );
                        img = this.GetImage( this.StartPage + 1 );
                        if ( img is object )
                        {
                            rc = this.GetImageRectangle( img );
                            rc.Width = ( int ) ((rc.Width - 4) / 2d);
                            rc.Offset( rc.Width + 4, 0 );
                            RenderPage( e.Graphics, img, rc );
                        }
                    }
                }
            }

            e.Graphics.FillRectangle( this._BackBrush, this.ClientRectangle );
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.SizeChanged" /> event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnSizeChanged( EventArgs e )
        {
            this.UpdateScrollBars();
            base.OnSizeChanged( e );
        }

        /// <summary> Event queue for all listeners interested in StartPageChanged events. </summary>
        public event EventHandler<EventArgs> StartPageChanged;

        /// <summary> Removes event handler. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The handler. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void RemoveStartPageChangedEventHandler( EventHandler<EventArgs> value )
        {
            foreach ( Delegate d in value is null ? (Array.Empty<Delegate>()) : value.GetInvocationList() )
            {
                try
                {
                    StartPageChanged -= ( EventHandler<EventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }
        }

        /// <summary> Raises the start page changed event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected void OnStartPageChanged( EventArgs e )
        {
            StartPageChanged?.Invoke( this, e );
        }

        /// <summary> Event queue for all listeners interested in ZoomModeChanged events. </summary>
        public event EventHandler<EventArgs> ZoomModeChanged;

        /// <summary> Removes event handler. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The handler. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void RemoveZoomModeChangedEventHandler( EventHandler<EventArgs> value )
        {
            foreach ( Delegate d in value is null ? (Array.Empty<Delegate>()) : value.GetInvocationList() )
            {
                try
                {
                    ZoomModeChanged -= ( EventHandler<EventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }
        }

        /// <summary> Raises the zoom mode changed event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected void OnZoomModeChanged( EventArgs e )
        {
            ZoomModeChanged?.Invoke( this, e );
        }

        #endregion

        #region " PRINT FUNCTIONS "

        /// <summary> Prints this object. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public void Print()
        {
            var ps = this._PrintDocument.PrinterSettings;
            int first = ps.MinimumPage - 1;
            int last = ps.MaximumPage - 1;
            switch ( ps.PrintRange )
            {
                case PrintRange.AllPages:
                    {
                        this.Document.Print();
                        return;
                    }

                case PrintRange.Selection:
                    {
                        first = this.StartPage;
                        last = this.StartPage;
                        if ( this.ZoomMode == ZoomMode.TwoPages )
                        {
                            last = Math.Min( first + 1, this.PageCount - 1 );
                        }

                        break;
                    }

                case PrintRange.SomePages:
                    {
                        first = ps.FromPage - 1;
                        last = ps.ToPage - 1;
                        break;
                    }

                case PrintRange.CurrentPage:
                    {
                        first = this.StartPage;
                        last = this.StartPage;
                        break;
                    }
            }

            using var dp = new DocumentPrinter( this, first, last );
            dp.Print();
        }

        /// <summary> Refresh preview. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public void RefreshPreview()
        {
            if ( this._PrintDocument is object )
            {
                this._PageImages.Clear();
                var savePC = this._PrintDocument.PrintController;
                try
                {
                    this._Cancel = false;
                    this.IsRendering = true;
                    this._PrintDocument.PrintController = new PreviewPrintController();
                    this._PrintDocument.PrintPage += new PrintPageEventHandler( this.PrintDocumentPrintPage );
                    this._PrintDocument.EndPrint += new PrintEventHandler( this.PrintDocumentEndPrint );
                    this._PrintDocument.Print();
                }
                finally
                {
                    this._Cancel = false;
                    this.IsRendering = false;
                    this._PrintDocument.PrintPage -= new PrintPageEventHandler( this.PrintDocumentPrintPage );
                    this._PrintDocument.EndPrint -= new PrintEventHandler( this.PrintDocumentEndPrint );
                    this._PrintDocument.PrintController = savePC;
                }
            }

            this.OnPageCountChanged( EventArgs.Empty );
            this.UpdatePreview();
            this.UpdateScrollBars();
        }

        /// <summary> Renders the page. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="g">   The Graphics to process. </param>
        /// <param name="img"> The image. </param>
        /// <param name="rc">  The rectangle. </param>
        private static void RenderPage( Graphics g, Image img, Rectangle rc )
        {
            if ( g is null )
            {
                return;
            }

            rc.Offset( 1, 1 );
            g.DrawRectangle( Pens.Black, rc );
            rc.Offset( -1, -1 );
            g.FillRectangle( Brushes.White, rc );
            g.DrawImage( img, rc );
            g.DrawRectangle( Pens.Black, rc );
            rc.Width += 1;
            rc.Height += 1;
            g.ExcludeClip( rc );
            rc.Offset( 1, 1 );
            g.ExcludeClip( rc );
        }

        /// <summary> Synchronization page images. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="lastPageReady"> true to last page ready. </param>
        private void SyncPageImages( bool lastPageReady )
        {
            PreviewPrintController pv = ( PreviewPrintController ) this._PrintDocument.PrintController;
            if ( pv is object )
            {
                var pageInfo = pv.GetPreviewPageInfo();
                int count = Conversions.ToInteger( Interaction.IIf( lastPageReady, pageInfo.Length, pageInfo.Length - 1 ) );
                int i;
                var loopTo = count - 1;
                for ( i = this._PageImages.Count; i <= loopTo; i++ )
                {
                    var img = pageInfo[i].Image;
                    this._PageImages.Add( img );
                    this.OnPageCountChanged( EventArgs.Empty );
                    if ( this.StartPage < 0 )
                    {
                        this.StartPage = 0;
                    }

                    if ( i == this.StartPage || i == this.StartPage + 1 )
                    {
                        this.Refresh();
                    }

                    Application.DoEvents();
                }
            }
        }

        /// <summary> Updates the preview. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void UpdatePreview()
        {
            if ( this._StartPage < 0 )
            {
                this._StartPage = 0;
            }

            if ( this._StartPage > this.PageCount - 1 )
            {
                this._StartPage = this.PageCount - 1;
            }

            this.Invalidate();
        }

        /// <summary> Updates the scroll bars. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void UpdateScrollBars()
        {
            var rc = Rectangle.Empty;
            var img = this.GetImage( this.StartPage );
            if ( img is object )
            {
                rc = this.GetImageRectangle( img );
            }

            var scrollSize = new Size( 0, 0 );
            switch ( this._ZoomMode )
            {
                case ZoomMode.ActualSize:
                case ZoomMode.Custom:
                    {
                        scrollSize = new Size( rc.Width + 8, rc.Height + 8 );
                        break;
                    }

                case ZoomMode.PageWidth:
                    {
                        scrollSize = new Size( 0, rc.Height + 8 );
                        break;
                    }
            }

            if ( scrollSize != this.AutoScrollMinSize )
            {
                this.AutoScrollMinSize = scrollSize;
            }

            this.UpdatePreview();
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> The back brush. </summary>
        private Brush _BackBrush;

        /// <summary> Gets or sets the background color for the control. </summary>
        /// <value>
        /// A <see cref="T:System.Drawing.Color" /> that represents the background color of the control.
        /// The default is the value of the
        /// <see cref="P:System.Windows.Forms.Control.DefaultBackColor" /> property.
        /// </value>
        [DefaultValue( typeof( Color ), "AppWorkspace" )]
        public override Color BackColor
        {
            get => base.BackColor;

            set {
                base.BackColor = value;
                this._BackBrush = new SolidBrush( value );
            }
        }

        /// <summary> The print document. </summary>
        private PrintDocument _PrintDocument;

        /// <summary> Gets or sets the document. </summary>
        /// <value> The document. </value>
        public PrintDocument Document
        {
            get => this._PrintDocument;

            set {
                if ( !ReferenceEquals( value, this._PrintDocument ) )
                {
                    this._PrintDocument = value;
                    this.RefreshPreview();
                }
            }
        }

        /// <summary> Gets a value indicating whether this object is rendering. </summary>
        /// <value> <c>true</c> if this object is rendering; otherwise <c>false</c> </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public bool IsRendering { get; private set; }

        /// <summary> Gets the number of pages. </summary>
        /// <value> The number of pages. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public int PageCount => this._PageImages.Count;

        /// <summary> The page images. </summary>
        private List<Image> _PageImages;

        /// <summary> Gets the page images. </summary>
        /// <value> The page images. </value>
        [Browsable( false )]
        public PageImageCollection PageImages => new( this._PageImages.ToArray() );

        /// <summary> The start page. </summary>
        private int _StartPage;

        /// <summary> Gets or sets the start page number. </summary>
        /// <value> The Zero-Based start page number. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public int StartPage
        {
            get => this._StartPage;

            set {
                if ( value > this.PageCount - 1 )
                {
                    value = this.PageCount - 1;
                }

                if ( value < 0 )
                {
                    value = 0;
                }

                if ( value != this._StartPage )
                {
                    this._StartPage = value;
                    this.UpdateScrollBars();
                    this.OnStartPageChanged( EventArgs.Empty );
                }
            }
        }

        /// <summary> The zoom. </summary>
        private double _Zoom;

        /// <summary> Gets or sets the zoom. </summary>
        /// <value> The zoom. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public double Zoom
        {
            get => this._Zoom;

            set {
                if ( value != this._Zoom || this.ZoomMode != ZoomMode.Custom )
                {
                    this.ZoomMode = ZoomMode.Custom;
                    this._Zoom = value;
                    this.UpdateScrollBars();
                    this.OnZoomModeChanged( EventArgs.Empty );
                }
            }
        }

        /// <summary> The zoom mode. </summary>
        private ZoomMode _ZoomMode;

        /// <summary> Gets or sets the zoom mode. </summary>
        /// <value> The zoom mode. </value>
        [DefaultValue( 1 )]
        public ZoomMode ZoomMode
        {
            get => this._ZoomMode;

            set {
                if ( value != this._ZoomMode )
                {
                    this._ZoomMode = value;
                    this.UpdateScrollBars();
                    this.OnZoomModeChanged( EventArgs.Empty );
                }
            }
        }

        #endregion

        #region " DOCUMENT PRINTER CLASS "

        /// <summary> Document printer. </summary>
        /// <remarks>
        /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
        /// Licensed under The MIT License.</para><para>
        /// David, 2015-05-02 </para>
        /// </remarks>
        internal class DocumentPrinter : PrintDocument
        {

            /// <summary> Constructor. </summary>
            /// <remarks> David, 2020-09-24. </remarks>
            /// <param name="preview"> The preview. </param>
            /// <param name="first">   The first. </param>
            /// <param name="last">    The last. </param>
            public DocumentPrinter( CoolPrintPreviewControl preview, int first, int last )
            {
                this.First = first;
                this.Last = last;
                this.PageImages = preview.PageImages;
                this.DefaultPageSettings = preview.Document.DefaultPageSettings;
                this.PrinterSettings = preview.Document.PrinterSettings;
            }

            /// <summary> Gets or sets the first. </summary>
            /// <value> The first. </value>
            public int First { get; private set; }

            /// <summary> Gets or sets the zero-based index of this object. </summary>
            /// <value> The index. </value>
            public int Index { get; private set; }

            /// <summary>
            /// Raises the <see cref="E:System.Drawing.Printing.PrintDocument.BeginPrint" /> event. It is
            /// called after the <see cref="M:System.Drawing.Printing.PrintDocument.Print" /> method is
            /// called and before the first page of the document prints.
            /// </summary>
            /// <remarks> David, 2020-09-24. </remarks>
            /// <param name="e"> A <see cref="T:System.Drawing.Printing.PrintEventArgs" /> that contains the
            /// event data. </param>
            protected override void OnBeginPrint( PrintEventArgs e )
            {
                this.Index = this.First;
            }

            /// <summary> Gets or sets the page images. </summary>
            /// <value> The page images. </value>
            public PageImageCollection PageImages { get; private set; }

            /// <summary> Gets or sets the last. </summary>
            /// <value> The last. </value>
            public int Last { get; private set; }

            /// <summary>
            /// Raises the <see cref="E:System.Drawing.Printing.PrintDocument.PrintPage" /> event. It is
            /// called before a page prints.
            /// </summary>
            /// <remarks> David, 2020-09-24. </remarks>
            /// <param name="e"> A <see cref="T:System.Drawing.Printing.PrintPageEventArgs" /> that contains
            /// the event data. </param>
            protected override void OnPrintPage( PrintPageEventArgs e )
            {
                if ( e is null )
                {
                    return;
                }

                e.Graphics.PageUnit = GraphicsUnit.Display;
                e.Graphics.DrawImage( this.PageImages[this.Index], e.PageBounds );
                this.Index += 1;
                e.HasMorePages = this.Index <= this.Last;
            }
        }
        #endregion

    }

    /// <summary> Collection of page images. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-05-02 </para>
    /// </remarks>
    public class PageImageCollection : System.Collections.ObjectModel.ReadOnlyCollection<Image>
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="images"> The images. </param>
        public PageImageCollection( Image[] images ) : base( images )
        {
        }
    }

    /// <summary> Values that represent zoom modes. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public enum ZoomMode
    {

        /// <summary> An enum constant representing the actual size option. </summary>
        ActualSize = 0,

        /// <summary> An enum constant representing the full page option. </summary>
        FullPage = 1,

        /// <summary> An enum constant representing the page width option. </summary>
        PageWidth = 2,

        /// <summary> An enum constant representing the two pages option. </summary>
        TwoPages = 3,

        /// <summary> An enum constant representing the custom option. </summary>
        Custom = 4
    }
}
