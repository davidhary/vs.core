﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Controls
{
    [DesignerGenerated()]
    public partial class CoolPrintPreviewDialog
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(CoolPrintPreviewDialog));
            _ToolStrip = new ToolStrip();
            __PrintButton = new System.Windows.Forms.ToolStripButton();
            __PrintButton.Click += new EventHandler(PrintButton_Click);
            __PageSetupButton = new System.Windows.Forms.ToolStripButton();
            __PageSetupButton.Click += new EventHandler(PageSetupButton_Click);
            _Separator2 = new ToolStripSeparator();
            __ZoomButton = new System.Windows.Forms.ToolStripSplitButton();
            __ZoomButton.ButtonClick += new EventHandler(ZoomButton_ButtonClick);
            __ZoomButton.DropDownItemClicked += new ToolStripItemClickedEventHandler(ZoomButton_DropDownItemClicked);
            _ActualSizeMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _FullPageMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _PageWidthMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _TwoPagesMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _ZoomSeparatorMenuItem = new ToolStripSeparator();
            _Zoom500MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _Zomm200MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _Zoom150MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _Zoom100MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _Zoom75MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _Zoom50MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _Zoom25MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _Zoom10MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __FirstButton = new System.Windows.Forms.ToolStripButton();
            __FirstButton.Click += new EventHandler(FirstButton_Click);
            __PreviousButton = new System.Windows.Forms.ToolStripButton();
            __PreviousButton.Click += new EventHandler(PreviousButton_Click);
            __StartPageTextBox = new System.Windows.Forms.ToolStripTextBox();
            __StartPageTextBox.Enter += new EventHandler(StartPageTextBox_Enter);
            __StartPageTextBox.KeyPress += new KeyPressEventHandler(StartPageTextBox_KeyPress);
            __StartPageTextBox.Validating += new System.ComponentModel.CancelEventHandler(StartPageTextBox_Validating);
            _PageCountLabel = new System.Windows.Forms.ToolStripLabel();
            __NextButton = new System.Windows.Forms.ToolStripButton();
            __NextButton.Click += new EventHandler(NextButton_Click);
            __LastButton = new System.Windows.Forms.ToolStripButton();
            __LastButton.Click += new EventHandler(LastButton_Click);
            _Separator1 = new ToolStripSeparator();
            __CancelButton = new System.Windows.Forms.ToolStripButton();
            __CancelButton.Click += new EventHandler(CancelButton_Click);
            __Preview = new CoolPrintPreviewControl();
            __Preview.PageCountChanged += new EventHandler<EventArgs>(Preview_PageCountChanged);
            __Preview.StartPageChanged += new EventHandler<EventArgs>(Preview_StartPageChanged);
            _ToolStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _toolStrip
            // 
            _ToolStrip.GripStyle = ToolStripGripStyle.Hidden;
            _ToolStrip.Items.AddRange(new ToolStripItem[] { __PrintButton, __PageSetupButton, _Separator2, __ZoomButton, __FirstButton, __PreviousButton, __StartPageTextBox, _PageCountLabel, __NextButton, __LastButton, _Separator1, __CancelButton });
            _ToolStrip.Location = new Point(0, 0);
            _ToolStrip.Name = "_toolStrip";
            _ToolStrip.Size = new Size(532, 25);
            _ToolStrip.TabIndex = 1;
            _ToolStrip.Text = "toolStrip1";
            // 
            // _PrintButton
            // 
            __PrintButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            __PrintButton.Image = (Image)resources.GetObject("PrinterImage");
            __PrintButton.ImageTransparentColor = Color.Magenta;
            __PrintButton.Name = "__PrintButton";
            __PrintButton.Size = new Size(23, 22);
            __PrintButton.Text = "Print Document";
            // 
            // _PageSetupButton
            // 
            __PageSetupButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            __PageSetupButton.Image = (Image)resources.GetObject("PageSetupImage");
            __PageSetupButton.ImageTransparentColor = Color.Magenta;
            __PageSetupButton.Name = "__PageSetupButton";
            __PageSetupButton.Size = new Size(23, 22);
            __PageSetupButton.Text = "Page Setup";
            // 
            // _Separator2
            // 
            _Separator2.Name = "_Separator2";
            _Separator2.Size = new Size(6, 25);
            // 
            // _ZoomButton
            // 
            __ZoomButton.AutoToolTip = false;
            __ZoomButton.DropDownItems.AddRange(new ToolStripItem[] { _ActualSizeMenuItem, _FullPageMenuItem, _PageWidthMenuItem, _TwoPagesMenuItem, _ZoomSeparatorMenuItem, _Zoom500MenuItem, _Zomm200MenuItem, _Zoom150MenuItem, _Zoom100MenuItem, _Zoom75MenuItem, _Zoom50MenuItem, _Zoom25MenuItem, _Zoom10MenuItem });
            __ZoomButton.Image = (Image)resources.GetObject("ZoomImage");
            __ZoomButton.ImageTransparentColor = Color.Magenta;
            __ZoomButton.Name = "__ZoomButton";
            __ZoomButton.Size = new Size(77, 22);
            __ZoomButton.Text = "&Zoom";
            // 
            // _ActualSizeMenuItem
            // 
            _ActualSizeMenuItem.Image = (Image)resources.GetObject("ActualSizeImage");
            _ActualSizeMenuItem.Name = "_ActualSizeMenuItem";
            _ActualSizeMenuItem.Size = new Size(150, 22);
            _ActualSizeMenuItem.Text = "Actual Size";
            // 
            // _FullPageMenuItem
            // 
            _FullPageMenuItem.Image = (Image)resources.GetObject("FullPageImage");
            _FullPageMenuItem.Name = "_FullPageMenuItem";
            _FullPageMenuItem.Size = new Size(150, 22);
            _FullPageMenuItem.Text = "Full Page";
            // 
            // _PageWidthMenuItem
            // 
            _PageWidthMenuItem.Image = (Image)resources.GetObject("PageWidthImage");
            _PageWidthMenuItem.Name = "_PageWidthMenuItem";
            _PageWidthMenuItem.Size = new Size(150, 22);
            _PageWidthMenuItem.Text = "Page Width";
            // 
            // _TwoPagesMenuItem
            // 
            _TwoPagesMenuItem.Image = (Image)resources.GetObject("TwoPagesImage");
            _TwoPagesMenuItem.Name = "_TwoPagesMenuItem";
            _TwoPagesMenuItem.Size = new Size(150, 22);
            _TwoPagesMenuItem.Text = "Two Pages";
            // 
            // _ZoomSeparatorMenuItem
            // 
            _ZoomSeparatorMenuItem.Name = "_ZoomSeparatorMenuItem";
            _ZoomSeparatorMenuItem.Size = new Size(147, 6);
            // 
            // _Zoom500MenuItem
            // 
            _Zoom500MenuItem.Name = "_Zoom500MenuItem";
            _Zoom500MenuItem.Size = new Size(150, 22);
            _Zoom500MenuItem.Text = "500%";
            // 
            // _Zomm200MenuItem
            // 
            _Zomm200MenuItem.Name = "_Zomm200MenuItem";
            _Zomm200MenuItem.Size = new Size(150, 22);
            _Zomm200MenuItem.Text = "200%";
            // 
            // _Zoom150MenuItem
            // 
            _Zoom150MenuItem.Name = "_Zoom150MenuItem";
            _Zoom150MenuItem.Size = new Size(150, 22);
            _Zoom150MenuItem.Text = "150%";
            // 
            // _Zoom100MenuItem
            // 
            _Zoom100MenuItem.Name = "_Zoom100MenuItem";
            _Zoom100MenuItem.Size = new Size(150, 22);
            _Zoom100MenuItem.Text = "100%";
            // 
            // _Zoom75MenuItem
            // 
            _Zoom75MenuItem.Name = "_Zoom75MenuItem";
            _Zoom75MenuItem.Size = new Size(150, 22);
            _Zoom75MenuItem.Text = "75%";
            // 
            // _Zoom50MenuItem
            // 
            _Zoom50MenuItem.Name = "_Zoom50MenuItem";
            _Zoom50MenuItem.Size = new Size(150, 22);
            _Zoom50MenuItem.Text = "50%";
            // 
            // _Zoom25MenuItem
            // 
            _Zoom25MenuItem.Name = "_Zoom25MenuItem";
            _Zoom25MenuItem.Size = new Size(150, 22);
            _Zoom25MenuItem.Text = "25%";
            // 
            // _Zoom10MenuItem
            // 
            _Zoom10MenuItem.Name = "_Zoom10MenuItem";
            _Zoom10MenuItem.Size = new Size(150, 22);
            _Zoom10MenuItem.Text = "10%";
            // 
            // _FirstButton
            // 
            __FirstButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            __FirstButton.Image = (Image)resources.GetObject("FirstImage");
            __FirstButton.ImageTransparentColor = Color.Red;
            __FirstButton.Name = "__FirstButton";
            __FirstButton.Size = new Size(23, 22);
            __FirstButton.Text = "First Page";
            // 
            // _PreviousButton
            // 
            __PreviousButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            __PreviousButton.Image = (Image)resources.GetObject("PreviousImage");
            __PreviousButton.ImageTransparentColor = Color.Red;
            __PreviousButton.Name = "__PreviousButton";
            __PreviousButton.Size = new Size(23, 22);
            __PreviousButton.Text = "Previous Page";
            // 
            // _StartPageTextBox
            // 
            __StartPageTextBox.AutoSize = false;
            __StartPageTextBox.Name = "__StartPageTextBox";
            __StartPageTextBox.Size = new Size(39, 24);
            __StartPageTextBox.TextBoxTextAlign = HorizontalAlignment.Center;
            // 
            // _PageCountLabel
            // 
            _PageCountLabel.Name = "_PageCountLabel";
            _PageCountLabel.Size = new Size(13, 22);
            _PageCountLabel.Text = " ";
            // 
            // _NextButton
            // 
            __NextButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            __NextButton.Image = (Image)resources.GetObject("NextImage");
            __NextButton.ImageTransparentColor = Color.Red;
            __NextButton.Name = "__NextButton";
            __NextButton.Size = new Size(23, 22);
            __NextButton.Text = "Next Page";
            // 
            // _LastButton
            // 
            __LastButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            __LastButton.Image = (Image)resources.GetObject("LastImage");
            __LastButton.ImageTransparentColor = Color.Red;
            __LastButton.Name = "__LastButton";
            __LastButton.Size = new Size(23, 22);
            __LastButton.Text = "Last Page";
            // 
            // _Separator1
            // 
            _Separator1.Name = "_Separator1";
            _Separator1.Size = new Size(6, 25);
            _Separator1.Visible = false;
            // 
            // _CancelButton
            // 
            __CancelButton.AutoToolTip = false;
            __CancelButton.Image = (Image)resources.GetObject("CancelImage");
            __CancelButton.ImageTransparentColor = Color.Magenta;
            __CancelButton.Name = "__CancelButton";
            __CancelButton.Size = new Size(70, 22);
            __CancelButton.Text = "Cancel";
            // 
            // _Preview
            // 
            __Preview.Dock = DockStyle.Fill;
            __Preview.Document = null;
            __Preview.Location = new Point(0, 25);
            __Preview.Name = "__Preview";
            __Preview.Size = new Size(532, 410);
            __Preview.TabIndex = 2;
            __Preview.ZoomMode = ZoomMode.FullPage;
            // 
            // CoolPrintPreviewDialog
            // 
            AutoScaleDimensions = new SizeF(120.0f, 120.0f);
            AutoScaleMode = AutoScaleMode.Dpi;
            ClientSize = new Size(532, 435);
            Controls.Add(__Preview);
            Controls.Add(_ToolStrip);
            Name = "CoolPrintPreviewDialog";
            ShowIcon = false;
            ShowInTaskbar = false;
            StartPosition = FormStartPosition.CenterParent;
            Text = "Print Preview";
            _ToolStrip.ResumeLayout(false);
            _ToolStrip.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        private ToolStrip _ToolStrip;
        private System.Windows.Forms.ToolStripButton __PrintButton;

        private System.Windows.Forms.ToolStripButton _PrintButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __PrintButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__PrintButton != null)
                {
                    __PrintButton.Click -= PrintButton_Click;
                }

                __PrintButton = value;
                if (__PrintButton != null)
                {
                    __PrintButton.Click += PrintButton_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripButton __PageSetupButton;

        private System.Windows.Forms.ToolStripButton _PageSetupButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __PageSetupButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__PageSetupButton != null)
                {
                    __PageSetupButton.Click -= PageSetupButton_Click;
                }

                __PageSetupButton = value;
                if (__PageSetupButton != null)
                {
                    __PageSetupButton.Click += PageSetupButton_Click;
                }
            }
        }

        private ToolStripSeparator _Separator2;
        private System.Windows.Forms.ToolStripSplitButton __ZoomButton;

        private System.Windows.Forms.ToolStripSplitButton _ZoomButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ZoomButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ZoomButton != null)
                {
                    __ZoomButton.ButtonClick -= ZoomButton_ButtonClick;
                    __ZoomButton.DropDownItemClicked -= ZoomButton_DropDownItemClicked;
                }

                __ZoomButton = value;
                if (__ZoomButton != null)
                {
                    __ZoomButton.ButtonClick += ZoomButton_ButtonClick;
                    __ZoomButton.DropDownItemClicked += ZoomButton_DropDownItemClicked;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem _ActualSizeMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _FullPageMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _PageWidthMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _TwoPagesMenuItem;
        private ToolStripSeparator _ZoomSeparatorMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _Zoom500MenuItem;
        private System.Windows.Forms.ToolStripMenuItem _Zomm200MenuItem;
        private System.Windows.Forms.ToolStripMenuItem _Zoom150MenuItem;
        private System.Windows.Forms.ToolStripMenuItem _Zoom100MenuItem;
        private System.Windows.Forms.ToolStripMenuItem _Zoom75MenuItem;
        private System.Windows.Forms.ToolStripMenuItem _Zoom50MenuItem;
        private System.Windows.Forms.ToolStripMenuItem _Zoom25MenuItem;
        private System.Windows.Forms.ToolStripMenuItem _Zoom10MenuItem;
        private System.Windows.Forms.ToolStripButton __FirstButton;

        private System.Windows.Forms.ToolStripButton _FirstButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __FirstButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__FirstButton != null)
                {
                    __FirstButton.Click -= FirstButton_Click;
                }

                __FirstButton = value;
                if (__FirstButton != null)
                {
                    __FirstButton.Click += FirstButton_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripButton __PreviousButton;

        private System.Windows.Forms.ToolStripButton _PreviousButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __PreviousButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__PreviousButton != null)
                {
                    __PreviousButton.Click -= PreviousButton_Click;
                }

                __PreviousButton = value;
                if (__PreviousButton != null)
                {
                    __PreviousButton.Click += PreviousButton_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripTextBox __StartPageTextBox;

        private System.Windows.Forms.ToolStripTextBox _StartPageTextBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __StartPageTextBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__StartPageTextBox != null)
                {
                    __StartPageTextBox.Enter -= StartPageTextBox_Enter;
                    __StartPageTextBox.KeyPress -= StartPageTextBox_KeyPress;
                    __StartPageTextBox.Validating -= StartPageTextBox_Validating;
                }

                __StartPageTextBox = value;
                if (__StartPageTextBox != null)
                {
                    __StartPageTextBox.Enter += StartPageTextBox_Enter;
                    __StartPageTextBox.KeyPress += StartPageTextBox_KeyPress;
                    __StartPageTextBox.Validating += StartPageTextBox_Validating;
                }
            }
        }

        private System.Windows.Forms.ToolStripLabel _PageCountLabel;
        private System.Windows.Forms.ToolStripButton __NextButton;

        private System.Windows.Forms.ToolStripButton _NextButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __NextButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__NextButton != null)
                {
                    __NextButton.Click -= NextButton_Click;
                }

                __NextButton = value;
                if (__NextButton != null)
                {
                    __NextButton.Click += NextButton_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripButton __LastButton;

        private System.Windows.Forms.ToolStripButton _LastButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __LastButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__LastButton != null)
                {
                    __LastButton.Click -= LastButton_Click;
                }

                __LastButton = value;
                if (__LastButton != null)
                {
                    __LastButton.Click += LastButton_Click;
                }
            }
        }

        private ToolStripSeparator _Separator1;
        private System.Windows.Forms.ToolStripButton __CancelButton;

        private System.Windows.Forms.ToolStripButton _CancelButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __CancelButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__CancelButton != null)
                {
                    __CancelButton.Click -= CancelButton_Click;
                }

                __CancelButton = value;
                if (__CancelButton != null)
                {
                    __CancelButton.Click += CancelButton_Click;
                }
            }
        }

        private CoolPrintPreviewControl __Preview;

        private CoolPrintPreviewControl _Preview
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __Preview;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__Preview != null)
                {
                    __Preview.PageCountChanged -= Preview_PageCountChanged;
                    __Preview.StartPageChanged -= Preview_StartPageChanged;
                }

                __Preview = value;
                if (__Preview != null)
                {
                    __Preview.PageCountChanged += Preview_PageCountChanged;
                    __Preview.StartPageChanged += Preview_StartPageChanged;
                }
            }
        }
    }
}