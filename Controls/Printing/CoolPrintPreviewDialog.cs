using System;
using System.ComponentModel;
using System.Drawing.Printing;
using System.Windows.Forms;

using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Controls
{

    /// <summary> Dialog for setting the cool print preview. </summary>
    /// <remarks>
    /// (c) 2009 Bernardo Castillo. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2009-08-05 from Bernardo Castilho </para><para>
    /// http://www.codeproject.com/Articles/38758/An-Enhanced-PrintPreviewDialog. </para>
    /// </remarks>
    public partial class CoolPrintPreviewDialog : Form
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="parentForm"> The parent form. </param>
        public CoolPrintPreviewDialog( Control parentForm ) : base()
        {
            this.components = null;
            this.InitializeComponent();
            if ( parentForm is object )
            {
                this.Size = parentForm.Size;
            }

            this.__PrintButton.Name = "_PrintButton";
            this.__PageSetupButton.Name = "_PageSetupButton";
            this.__ZoomButton.Name = "_ZoomButton";
            this.__FirstButton.Name = "_FirstButton";
            this.__PreviousButton.Name = "_PreviousButton";
            this.__StartPageTextBox.Name = "_StartPageTextBox";
            this.__NextButton.Name = "_NextButton";
            this.__LastButton.Name = "_LastButton";
            this.__CancelButton.Name = "_CancelButton";
            this.__Preview.Name = "_Preview";
        }

        #region " CONSTRUCTION "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public CoolPrintPreviewDialog() : this( null )
        {
        }

        /// <summary>
        /// Disposes of the resources (other than memory) used by the
        /// <see cref="T:System.Windows.Forms.Form" />.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " FORM OVERLOADS "

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Form.FormClosing" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.FormClosingEventArgs" /> that contains
        /// the event data. </param>
        protected override void OnFormClosing( FormClosingEventArgs e )
        {
            if ( e is null )
            {
                return;
            }

            base.OnFormClosing( e );
            if ( !(!this._Preview.IsRendering || e.Cancel) )
            {
                this._Preview.Cancel();
            }
        }

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Form.Shown" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnShown( EventArgs e )
        {
            if ( e is null )
            {
                return;
            }

            base.OnShown( e );
            this._Preview.Document = this.Document;
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> The document. </summary>
        private PrintDocument _Doc;

        /// <summary> Gets or sets the document. </summary>
        /// <value> The document. </value>
        public PrintDocument Document
        {
            get => this._Doc;

            set {
                if ( this._Doc is object )
                {
                    this._Doc.BeginPrint -= new PrintEventHandler( this.Doc_BeginPrint );
                    this._Doc.EndPrint -= new PrintEventHandler( this.Doc_EndPrint );
                }

                this._Doc = value;
                if ( this._Doc is object )
                {
                    this._Doc.BeginPrint += new PrintEventHandler( this.Doc_BeginPrint );
                    this._Doc.EndPrint += new PrintEventHandler( this.Doc_EndPrint );
                }

                if ( this.Visible )
                {
                    this._Preview.Document = this.Document;
                }
            }
        }

        #endregion

        #region " EVENT HANDLERS "

        /// <summary> Cancel button click. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void CancelButton_Click( object sender, EventArgs e )
        {
            if ( this._Preview.IsRendering )
            {
                this._Preview.Cancel();
            }
            else
            {
                this.Close();
            }
        }

        /// <summary> First button click. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void FirstButton_Click( object sender, EventArgs e )
        {
            this._Preview.StartPage = 0;
        }

        /// <summary> Last button click. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void LastButton_Click( object sender, EventArgs e )
        {
            this._Preview.StartPage = this._Preview.PageCount - 1;
        }

        /// <summary> Next button click. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void NextButton_Click( object sender, EventArgs e )
        {
            this._Preview.StartPage += 1;
        }

        /// <summary> Previous button click. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void PreviousButton_Click( object sender, EventArgs e )
        {
            this._Preview.StartPage -= 1;
        }

        /// <summary> Page setup button click. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void PageSetupButton_Click( object sender, EventArgs e )
        {
            using var dlg = new PageSetupDialog {
                Document = this.Document
            };
            if ( dlg.ShowDialog( this ) == DialogResult.OK )
            {
                this._Preview.RefreshPreview();
            }
        }

        /// <summary> Print button click. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void PrintButton_Click( object sender, EventArgs e )
        {
            using var dlg = new PrintDialog {
                AllowSomePages = true,
                AllowSelection = true,
                UseEXDialog = true,
                Document = this.Document
            };
            var ps = dlg.PrinterSettings;
            ps.MinimumPage = 1;
            ps.MaximumPage = this._Preview.PageCount;
            ps.FromPage = 1;
            ps.ToPage = this._Preview.PageCount;
            if ( dlg.ShowDialog( this ) == DialogResult.OK )
            {
                this._Preview.Print();
            }
        }

        /// <summary> Zoom button click. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ZoomButton_ButtonClick( object sender, EventArgs e )
        {
            this._Preview.ZoomMode = ( ZoomMode ) Conversions.ToInteger( Interaction.IIf( this._Preview.ZoomMode == ZoomMode.ActualSize, ZoomMode.FullPage, ZoomMode.ActualSize ) );
        }

        /// <summary> Zoom button drop down item clicked. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Tool strip item clicked event information. </param>
        private void ZoomButton_DropDownItemClicked( object sender, ToolStripItemClickedEventArgs e )
        {
            if ( e is null )
            {
                return;
            }

            if ( ReferenceEquals( e.ClickedItem, this._ActualSizeMenuItem ) )
            {
                this._Preview.ZoomMode = ZoomMode.ActualSize;
            }
            else if ( ReferenceEquals( e.ClickedItem, this._FullPageMenuItem ) )
            {
                this._Preview.ZoomMode = ZoomMode.FullPage;
            }
            else if ( ReferenceEquals( e.ClickedItem, this._PageWidthMenuItem ) )
            {
                this._Preview.ZoomMode = ZoomMode.PageWidth;
            }
            else if ( ReferenceEquals( e.ClickedItem, this._TwoPagesMenuItem ) )
            {
                this._Preview.ZoomMode = ZoomMode.TwoPages;
            }

            if ( ReferenceEquals( e.ClickedItem, this._Zoom10MenuItem ) )
            {
                this._Preview.Zoom = 0.1d;
            }
            else if ( ReferenceEquals( e.ClickedItem, this._Zoom100MenuItem ) )
            {
                this._Preview.Zoom = 1d;
            }
            else if ( ReferenceEquals( e.ClickedItem, this._Zoom150MenuItem ) )
            {
                this._Preview.Zoom = 1.5d;
            }
            else if ( ReferenceEquals( e.ClickedItem, this._Zomm200MenuItem ) )
            {
                this._Preview.Zoom = 2d;
            }
            else if ( ReferenceEquals( e.ClickedItem, this._Zoom25MenuItem ) )
            {
                this._Preview.Zoom = 0.25d;
            }
            else if ( ReferenceEquals( e.ClickedItem, this._Zoom50MenuItem ) )
            {
                this._Preview.Zoom = 0.5d;
            }
            else if ( ReferenceEquals( e.ClickedItem, this._Zoom500MenuItem ) )
            {
                this._Preview.Zoom = 5d;
            }
            else if ( ReferenceEquals( e.ClickedItem, this._Zoom75MenuItem ) )
            {
                this._Preview.Zoom = 0.75d;
            }
        }

        /// <summary> Event handler. Called by Doc for begin print events. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Print event information. </param>
        private void Doc_BeginPrint( object sender, PrintEventArgs e )
        {
            this._CancelButton.Text = "&Cancel";
            this._PrintButton.Enabled = this._PageSetupButton.Enabled == false;
        }

        /// <summary> Event handler. Called by Doc for end print events. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Print event information. </param>
        private void Doc_EndPrint( object sender, PrintEventArgs e )
        {
            this._CancelButton.Text = "&Close";
            this._PrintButton.Enabled = this._PageSetupButton.Enabled == true;
        }

        /// <summary> Preview page count changed. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void Preview_PageCountChanged( object sender, EventArgs e )
        {
            this.Update();
            Application.DoEvents();
            this._PageCountLabel.Text = $"of {this._Preview.PageCount}";
        }

        /// <summary> Preview start page changed. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void Preview_StartPageChanged( object sender, EventArgs e )
        {
            this._StartPageTextBox.Text = (this._Preview.StartPage + 1).ToString();
        }

        /// <summary> Starts page text box enter. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void StartPageTextBox_Enter( object sender, EventArgs e )
        {
            this._StartPageTextBox.SelectAll();
        }

        /// <summary> Starts page text box key press. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Key press event information. </param>
        private void StartPageTextBox_KeyPress( object sender, KeyPressEventArgs e )
        {
            if ( e is null )
            {
                return;
            }

            char c = e.KeyChar;
            if ( c == '\r' )
            {
                this.CommitPageNumber();
                e.Handled = true;
            }
            else if ( !(c <= ' ' || char.IsDigit( c )) )
            {
                e.Handled = true;
            }
        }

        /// <summary> Starts page text box validating. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Cancel event information. </param>
        private void StartPageTextBox_Validating( object sender, CancelEventArgs e )
        {
            this.CommitPageNumber();
        }

        /// <summary> Commits page number. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void CommitPageNumber()
        {
            if ( int.TryParse( this._StartPageTextBox.Text, out int page ) )
            {
                this._Preview.StartPage = page - 1;
            }
        }

        #endregion

    }
}
