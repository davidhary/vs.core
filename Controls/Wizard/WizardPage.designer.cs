using System.ComponentModel;

namespace isr.Core.Controls
{
    [DefaultEvent("Click")]
    [Designer(typeof(WizardPageDesigner))]
    public partial class WizardPage
    {
    }
}
