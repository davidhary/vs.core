using System;
using System.ComponentModel;

namespace isr.Core.Controls
{

    /// <summary> Provides data for the Page Changed Event of the Wizard control. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public class PageChangedEventArgs : EventArgs
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Creates a new instance of the <see cref="PageChangedEventArgs"/> class. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="oldIndex"> An integer value representing the index of the old page. </param>
        /// <param name="newIndex"> An integer value representing the index of the new page. </param>
        internal PageChangedEventArgs( int oldIndex, int newIndex ) : base()
        {
            this.OldIndex = oldIndex;
            this.NewIndex = newIndex;
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> Gets or sets the index of the old page. </summary>
        /// <value> The old index. </value>
        public int OldIndex { get; private set; }

        /// <summary> Gets or sets the index of the new page. </summary>
        /// <value> The new index. </value>
        public int NewIndex { get; protected set; }

        /// <summary> Gets the direction. </summary>
        /// <value> The direction. </value>
        public Direction Direction => this.NewIndex == this.OldIndex ? Direction.None : this.NewIndex > this.OldIndex ? Direction.Next : Direction.Previous;

        #endregion

    }

    /// <summary> Provides data for the Page Changing Event of the Wizard control. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public class PageChangingEventArgs : PageChangedEventArgs
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Creates a new instance of the <see cref="PageChangingEventArgs"/> class. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="oldIndex"> An integer value representing the index of the old page. </param>
        /// <param name="newIndex"> An integer value representing the index of the new page. </param>
        internal PageChangingEventArgs( int oldIndex, int newIndex ) : base( oldIndex, newIndex )
        {
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> Indicates whether the page change should be canceled. </summary>
        /// <value> The cancel. </value>
        public bool Cancel { get; set; }

        /// <summary> Gets or sets the index of the new page. </summary>
        /// <value> The new index. </value>
        public new int NewIndex
        {
            get => base.NewIndex;

            set => base.NewIndex = value;
        }

        #endregion

    }

    /// <summary> Values that represent directions. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public enum Direction
    {

        /// <summary> An enum constant representing the none option. </summary>
        [Description( "None" )]
        None,

        /// <summary> An enum constant representing the next] option. </summary>
        [Description( "Next" )]
        Next,

        /// <summary> An enum constant representing the previous] option. </summary>
        [Description( "Previous" )]
        Previous
    }
}
