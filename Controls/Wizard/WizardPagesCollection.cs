using System.Collections;

namespace isr.Core.Controls
{
    /// <summary> Represents a collection of wizard pages. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc., All Rights Reserved <para>
    /// (c) 2005 Cristi Potlog - All Rights Reserved </para><para>
    /// Licensed under the MIT License. </para><para>
    /// David, 2012-09-19, 1.05.4645 </para><para>
    /// Based on http://www.CodeProject.com/Articles/10808/Cristi-Potlog-s-Wizard-Control-for-NET
    /// </para>
    /// </remarks>
    public class WizardPagesCollection : CollectionBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> The owner. </summary>
        private readonly Wizard _Owner;

        /// <summary> Creates a new instance of the <see cref="WizardPagesCollection"/> class. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="owner"> A Wizard object that owns the collection. </param>
        internal WizardPagesCollection( Wizard owner ) : base()
        {
            this._Owner = owner;
        }
        #endregion

        #region " INDEXER "

        /// <summary> Gets or sets the item. </summary>
        /// <value> The item. </value>
        public WizardPage this[int index]
        {
            get => ( WizardPage ) this.List[index];

            set => this.List[index] = value;
        }
        #endregion

        #region " METHODS "

        /// <summary> Adds an object to the end of the WizardPagesCollection. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The WizardPage to be added. The value can be null. </param>
        /// <returns> An Integer value representing the index at which the value has been added. </returns>
        public int Add( WizardPage value )
        {
            int result = this.List.Add( value );
            return result;
        }

        /// <summary>
        /// Adds the elements of an array of WizardPage objects to the end of the WizardPagesCollection.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="pages"> An array on WizardPage objects to be added. The array itself cannot be
        /// null, but it can contain elements that are null. </param>
        public void AddRange( WizardPage[] pages )
        {
            // Use external to validate and add each entry
            if ( pages is object )
            {
                foreach ( WizardPage page in pages )
                {
                    _ = this.Add( page );
                }
            }
        }

        /// <summary>
        /// Searches for the specified WizardPage and returns the zero-based index of the first
        /// occurrence in the entire WizardPagesCollection.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> A WizardPage object to locate in the WizardPagesCollection. The value can
        /// be null. </param>
        /// <returns> An Integer. </returns>
        public int IndexOf( WizardPage value )
        {
            return value is null ? -1 : this.List.IndexOf( value );
        }

        /// <summary> Inserts an element into the WizardPagesCollection at the specified index. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="index"> An Integer value representing the zero-based index at which value should
        /// be inserted. </param>
        /// <param name="value"> A WizardPage object to insert. The value can be null. </param>
        public void Insert( int index, WizardPage value )
        {
            if ( value is object )
            {
                // insert the item
                this.List.Insert( index, value );
            }
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the WizardPagesCollection.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> A WizardPage object to remove. The value can be null. </param>
        public void Remove( WizardPage value )
        {
            if ( value is object )
            {
                // remove the item
                this.List.Remove( value );
            }
        }

        /// <summary> Determines whether an element is in the WizardPagesCollection.   </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The WizardPage object to locate. The value can be null. </param>
        /// <returns> true if item is found in the WizardPagesCollection; otherwise, false. </returns>
        public bool Contains( WizardPage value )
        {
            return !(value is null) && this.List.Contains( value );
        }

        /// <summary>
        /// Performs additional custom processes after inserting a new element into the
        /// WizardPagesCollection instance.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="index"> The zero-based index at which to insert value. </param>
        /// <param name="value"> The new value of the element at index. </param>
        protected override void OnInsertComplete( int index, object value )
        {
            // call base class
            base.OnInsertComplete( index, value );

            // reset current page index
            this._Owner.SelectedIndex = index;
        }

        /// <summary>
        /// Performs additional custom processes after removing an element from the
        /// System.Collections.CollectionBase instance.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="index"> The zero-based index at which value can be found. </param>
        /// <param name="value"> The value of the element to remove from index. </param>
        protected override void OnRemoveComplete( int index, object value )
        {
            // call base class
            base.OnRemoveComplete( index, value );

            // check if removing current page
            if ( this._Owner.SelectedIndex == index )
            {
                // check if at the end of the list
                this._Owner.SelectedIndex = index < this.InnerList.Count ? index : this.InnerList.Count - 1;
            }
        }

        /// <summary> Provide the strongly typed member for ICollection. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="array"> The array. </param>
        /// <param name="index"> An Integer value representing the zero-based index at which value should
        /// be inserted. </param>
        public void CopyTo( WizardPage array, int index )
        {
            this.CopyTo( array, index );
        }

        #endregion

    }
}
