using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace isr.Core.Controls
{

    /// <summary> Represents a wizard page control with basic layout functionality. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc., All Rights Reserved <para>
    /// (c) 2005 Cristi Potlog - All Rights Reserved </para><para>
    /// Licensed under the MIT License. </para><para>
    /// David, 2012-09-19, 1.05.4645 </para><para>
    /// Based on http://www.CodeProject.com/Articles/10808/Cristi-Potlog-s-Wizard-Control-for-NET
    /// </para>
    /// </remarks>
    public partial class WizardPage : Panel
    {


        #region " CONSTANTS "

        /// <summary> Height of the header area. </summary>
        private const int _HeaderAreaHeight = 64;

        /// <summary> Size of the header glyph. </summary>
        private const int _HeaderGlyphSize = 48;

        /// <summary> The header text padding. </summary>
        private const int _HeaderTextPadding = 8;

        /// <summary> Width of the welcome glyph. </summary>
        private const int _WelcomeGlyphWidth = 164;
        #endregion

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Creates a new instance of the <see cref="WizardPage"/> class. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public WizardPage() : base()
        {
            this._Description = string.Empty;
            this._Title = string.Empty;
            this._WizardPageStyle = WizardPageStyle.Standard;

            // reset control style to improve rendering (reduce flicker)
            this.SetStyle( ControlStyles.AllPaintingInWmPaint, true );
            this.SetStyle( ControlStyles.DoubleBuffer, true );
            this.SetStyle( ControlStyles.ResizeRedraw, true );
            this.SetStyle( ControlStyles.UserPaint, true );
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> The wizard page style. </summary>
        private WizardPageStyle _WizardPageStyle;

        /// <summary> Gets or sets the style of the wizard page. </summary>
        /// <value> The wizard page style. </value>
        [DefaultValue( WizardPageStyle.Standard )]
        [Category( "Wizard" )]
        [Description( "Gets or sets the style of the wizard page." )]
        public WizardPageStyle WizardPageStyle
        {
            get => this._WizardPageStyle;

            set {
                if ( this._WizardPageStyle != value )
                {
                    this._WizardPageStyle = value;
                    // get the parent wizard control
                    if ( this.Parent is object && this.Parent is Wizard wizard )
                    {
                        Wizard parentWizard = wizard;
                        // check if page is selected
                        if ( ReferenceEquals( parentWizard.SelectedPage, this ) )
                        {
                            // reactivate the selected page (performs redraw too)
                            parentWizard.SelectedPage = this;
                        }
                    }
                    else
                    {
                        // just redraw the page
                        this.Invalidate();
                    }
                }
            }
        }

        /// <summary> The title. </summary>
        private string _Title;

        /// <summary> Gets or sets the title of the wizard page. </summary>
        /// <value> The title. </value>
        [DefaultValue( "" )]
        [Category( "Wizard" )]
        [Description( "Gets or sets the title of the wizard page." )]
        public string Title
        {
            get => this._Title;

            set {
                if ( value is null )
                {
                    value = string.Empty;
                }

                if ( (this._Title ?? "") != (value ?? "") )
                {
                    this._Title = value;
                    this.Invalidate();
                }
            }
        }

        /// <summary> The description. </summary>
        private string _Description;

        /// <summary> Gets or sets the description of the wizard page. </summary>
        /// <value> The description. </value>
        [DefaultValue( "" )]
        [Category( "Wizard" )]
        [Description( "Gets or sets the description of the wizard page." )]
        public string Description
        {
            get => this._Description;

            set {
                if ( value is null )
                {
                    value = string.Empty;
                }

                if ( (this._Description ?? "") != (value ?? "") )
                {
                    this._Description = value;
                    this.Invalidate();
                }
            }
        }
        #endregion

        #region " METHODS "

        /// <summary> Provides custom drawing to the wizard page. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnPaint( PaintEventArgs e )
        {
            // raise paint event
            base.OnPaint( e );
            if ( e is null )
            {
                return;
            }

            // check if custom style
            if ( this._WizardPageStyle == WizardPageStyle.Custom )
            {
                // filter out
                return;
            }

            // initialize graphic resources
            var headerRect = this.ClientRectangle;
            var glyphRect = Rectangle.Empty;
            var titleRect = Rectangle.Empty;
            var descriptionRect = Rectangle.Empty;

            // determine text format
            var textFormat = StringFormat.GenericDefault;
            textFormat.LineAlignment = StringAlignment.Near;
            textFormat.Alignment = StringAlignment.Near;
            textFormat.Trimming = StringTrimming.EllipsisCharacter;
            switch ( this._WizardPageStyle )
            {
                case WizardPageStyle.Standard:
                    {
                        // adjust height for header
                        headerRect.Height = _HeaderAreaHeight;
                        // draw header border
                        ControlPaint.DrawBorder3D( e.Graphics, headerRect, Border3DStyle.Etched, Border3DSide.Bottom );
                        // adjust header rect not to overwrite the border
                        headerRect.Height -= SystemInformation.Border3DSize.Height;
                        // fill header with window color
                        e.Graphics.FillRectangle( SystemBrushes.Window, headerRect );

                        // determine header image rectangle
                        int headerPadding = ( int ) Math.Floor( (_HeaderAreaHeight - _HeaderGlyphSize) / 2.0d );
                        glyphRect.Location = new Point( this.Width - _HeaderGlyphSize - headerPadding, headerPadding );
                        glyphRect.Size = new Size( _HeaderGlyphSize, _HeaderGlyphSize );

                        // determine the header content
                        Image headerImage = null;
                        var headerFont = this.Font;
                        var headerTitleFont = this.Font;
                        if ( this.Parent is object && this.Parent is Wizard wizard )
                        {
                            // get content from parent wizard, if exists
                            Wizard parentWizard = wizard;
                            headerImage = parentWizard.HeaderImage;
                            headerFont = parentWizard.HeaderFont;
                            headerTitleFont = parentWizard.HeaderTitleFont;
                        }

                        // check if we have an image
                        if ( headerImage is null )
                        {
                            // display a focus rect as a place holder
                            ControlPaint.DrawFocusRectangle( e.Graphics, glyphRect );
                        }
                        else
                        {
                            // draw header image
                            e.Graphics.DrawImage( headerImage, glyphRect );
                        }

                        // determine title height
                        int headerTitleHeight = ( int ) Math.Ceiling( e.Graphics.MeasureString( this._Title, headerTitleFont, 0, textFormat ).Height );

                        // calculate text sizes
                        titleRect.Location = new Point( _HeaderTextPadding, _HeaderTextPadding );
                        titleRect.Size = new Size( glyphRect.Left - _HeaderTextPadding, headerTitleHeight );
                        descriptionRect.Location = titleRect.Location;
                        descriptionRect.Y += headerTitleHeight + _HeaderTextPadding / 2;
                        descriptionRect.Size = new Size( titleRect.Width, _HeaderAreaHeight - descriptionRect.Y );

                        // draw title text (single line, truncated with ellipsis)
                        e.Graphics.DrawString( this._Title, headerTitleFont, SystemBrushes.WindowText, titleRect, textFormat );
                        // draw description text (multiple lines if needed)
                        e.Graphics.DrawString( this._Description, headerFont, SystemBrushes.WindowText, descriptionRect, textFormat );
                        break;
                    }

                case WizardPageStyle.Welcome:
                case WizardPageStyle.Finish:
                    {
                        // fill whole page with window color
                        e.Graphics.FillRectangle( SystemBrushes.Window, headerRect );

                        // determine welcome image rectangle
                        glyphRect.Location = Point.Empty;
                        glyphRect.Size = new Size( _WelcomeGlyphWidth, this.Height );

                        // determine the icon that should appear on the welcome page
                        Image welcomeImage = null;
                        var welcomeFont = this.Font;
                        var welcomeTitleFont = this.Font;
                        if ( this.Parent is object && this.Parent is Wizard wizard )
                        {
                            // get content from parent wizard, if exists
                            Wizard parentWizard = wizard;
                            welcomeImage = parentWizard.WelcomeImage;
                            welcomeFont = parentWizard.WelcomeFont;
                            welcomeTitleFont = parentWizard.WelcomeTitleFont;
                        }

                        // check if we have an image
                        if ( welcomeImage is null )
                        {
                            // display a focus rect as a place holder
                            ControlPaint.DrawFocusRectangle( e.Graphics, glyphRect );
                        }
                        else
                        {
                            // draw welcome page image
                            e.Graphics.DrawImage( welcomeImage, glyphRect );
                        }

                        // calculate text sizes
                        titleRect.Location = new Point( _WelcomeGlyphWidth + _HeaderTextPadding, _HeaderTextPadding );
                        titleRect.Width = this.Width - titleRect.Left - _HeaderTextPadding;
                        // determine title height
                        int welcomeTitleHeight = ( int ) Math.Ceiling( e.Graphics.MeasureString( this._Title, welcomeTitleFont, titleRect.Width, textFormat ).Height );
                        descriptionRect.Location = titleRect.Location;
                        descriptionRect.Y += welcomeTitleHeight + _HeaderTextPadding;
                        descriptionRect.Size = new Size( this.Width - descriptionRect.Left - _HeaderTextPadding, this.Height - descriptionRect.Y );

                        // draw title text (multiple lines if needed)
                        e.Graphics.DrawString( this._Title, welcomeTitleFont, SystemBrushes.WindowText, titleRect, textFormat );
                        // draw description text (multiple lines if needed)
                        e.Graphics.DrawString( this._Description, welcomeFont, SystemBrushes.WindowText, descriptionRect, textFormat );
                        break;
                    }
            }
        }
        #endregion

        #region " INNER CLASSES "

        /// <summary>
        /// This is a designer for the Banner. This designer locks the control vertical sizing.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        internal class WizardPageDesigner : ParentControlDesigner
        {

            /// <summary>
            /// Gets the selection rules that indicate the movement capabilities of a component.
            /// </summary>
            /// <value>
            /// A bitwise combination of <see cref="T:System.Windows.Forms.Design.SelectionRules" /> values.
            /// </value>
            public override SelectionRules SelectionRules =>
                    // lock the control
                    SelectionRules.Visible | SelectionRules.Locked;
        }

        #endregion

    }
}
