#region " Copyright ©2005, Cristi Potlog - All Rights Reserved"
// THIS SOURCE CODE IS PROVIDED "AS IS" WITH NO WARRANTIES OF ANY KIND, 
// EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE        
// WARRANTIES OF DESIGN, MERCHANTIBILITY AND FITNESS FOR A PARTICULAR   
// PURPOSE, NONINFRINGEMENT, OR ARISING FROM A COURSE OF DEALING,       
// USAGE OR TRADE PRACTICE.                                             
// THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.             
#endregion

using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Controls
{
    [DesignerGenerated()]
    public partial class Wizard : UserControl
    {

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks>   David, 2020-09-24. </remarks>
        /// <param name="disposing">    <see langword="true" /> to release both managed and unmanaged
        ///                             resources; <see langword="false" /> to release only unmanaged
        ///                             resources. </param>
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (components is object)
                    {
                        components.Dispose();
                    }
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            __CancelButton = new Button();
            __CancelButton.Click += new EventHandler(CancelButtonClick);
            __NextButton = new Button();
            __NextButton.Click += new EventHandler(NextButtonClick);
            __BackButton = new Button();
            __BackButton.Click += new EventHandler(BackButtonClick);
            __HelpButton = new Button();
            __HelpButton.Click += new EventHandler(HelpButtonClick);
            SuspendLayout();
            // 
            // Me._CancelButton
            // 
            __CancelButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            __CancelButton.DialogResult = DialogResult.Cancel;
            __CancelButton.FlatStyle = FlatStyle.System;
            __CancelButton.Location = new Point(344, 224);
            __CancelButton.Name = "__CancelButton";
            __CancelButton.TabIndex = 8;
            __CancelButton.Text = "Cancel";
            // Me.buttonCancel.Click += New System.EventHandler(Me.buttonCancel_Click)
            // 
            // Me._NextButton
            // 
            __NextButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            __NextButton.FlatStyle = FlatStyle.System;
            __NextButton.Location = new Point(260, 224);
            __NextButton.Name = "__NextButton";
            __NextButton.TabIndex = 7;
            __NextButton.Text = "&Next >";
            // Me.buttonNext.Click += New System.EventHandler(Me.buttonNext_Click)
            // 
            // Me._BackButton
            // 
            __BackButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            __BackButton.FlatStyle = FlatStyle.System;
            __BackButton.Location = new Point(184, 224);
            __BackButton.Name = "__BackButton";
            __BackButton.TabIndex = 6;
            __BackButton.Text = "< &Back";
            // Me.buttonBack.Click += New System.EventHandler(Me.buttonBack_Click)
            // 
            // Me._HelpButton
            // 
            __HelpButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            __HelpButton.FlatStyle = FlatStyle.System;
            __HelpButton.Location = new Point(8, 224);
            __HelpButton.Name = "__HelpButton";
            __HelpButton.TabIndex = 9;
            __HelpButton.Text = "&Help";
            __HelpButton.Visible = false;
            // Me.buttonHelp.Click += New System.EventHandler(Me.buttonHelp_Click)
            // 
            // Wizard
            // 
            Controls.Add(__HelpButton);
            Controls.Add(__CancelButton);
            Controls.Add(__NextButton);
            Controls.Add(__BackButton);
            AutoScaleDimensions = new SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Inherit;
            Font = new Font(SystemFonts.MessageBoxFont.FontFamily, 9.75f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            AutoSizeMode = AutoSizeMode.GrowAndShrink;
            Name = "Wizard";
            Size = new Size(428, 256);
            ResumeLayout(false);
        }

        private Button __CancelButton;

        private Button _CancelButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __CancelButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__CancelButton != null)
                {
                    __CancelButton.Click -= CancelButtonClick;
                }

                __CancelButton = value;
                if (__CancelButton != null)
                {
                    __CancelButton.Click += CancelButtonClick;
                }
            }
        }

        private Button __NextButton;

        private Button _NextButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __NextButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__NextButton != null)
                {
                    __NextButton.Click -= NextButtonClick;
                }

                __NextButton = value;
                if (__NextButton != null)
                {
                    __NextButton.Click += NextButtonClick;
                }
            }
        }

        private Button __BackButton;

        private Button _BackButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __BackButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__BackButton != null)
                {
                    __BackButton.Click -= BackButtonClick;
                }

                __BackButton = value;
                if (__BackButton != null)
                {
                    __BackButton.Click += BackButtonClick;
                }
            }
        }

        private Button __HelpButton;

        private Button _HelpButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __HelpButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__HelpButton != null)
                {
                    __HelpButton.Click -= HelpButtonClick;
                }

                __HelpButton = value;
                if (__HelpButton != null)
                {
                    __HelpButton.Click += HelpButtonClick;
                }
            }
        }
    }
}
