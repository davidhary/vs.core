﻿using System.Drawing;
using System.Windows.Forms;

namespace isr.Core.Controls
{
    public partial class MetroProgressBar
    {

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region " Component Designer generated code"

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(MetroProgressBar));
            _PictureBox = new PictureBox();
            ((System.ComponentModel.ISupportInitialize)_PictureBox).BeginInit();
            SuspendLayout();
            // 
            // _PictureBox
            // 
            _PictureBox.BackColor = Color.Transparent;
            _PictureBox.Dock = DockStyle.Fill;
            _PictureBox.Image = (Image)resources.GetObject("_pictureBox.Image");
            _PictureBox.Location = new Point(0, 0);
            _PictureBox.Name = "_PictureBox";
            _PictureBox.Size = new Size(308, 20);
            _PictureBox.TabIndex = 0;
            _PictureBox.TabStop = false;
            // 
            // MetroProgressBar
            // 
            AutoScaleDimensions = new SizeF(6.0f, 13.0f);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = Color.Transparent;
            Controls.Add(_PictureBox);
            Name = "MetroProgressBar";
            Size = new Size(308, 20);
            ((System.ComponentModel.ISupportInitialize)_PictureBox).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private PictureBox _PictureBox;
    }
}