﻿using System.Drawing;
using System.Windows.Forms;

namespace isr.Core.Controls
{
    public partial class MetroLoading
    {

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region " Component Designer generated code"

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(MetroLoading));
            _PictureBox = new PictureBox();
            ((System.ComponentModel.ISupportInitialize)_PictureBox).BeginInit();
            SuspendLayout();
            // 
            // _pictureBox
            // 
            _PictureBox.BackColor = Color.Transparent;
            _PictureBox.Dock = DockStyle.Fill;
            _PictureBox.Image = (Image)resources.GetObject("_pictureBox.Image");
            _PictureBox.Location = new Point(0, 0);
            _PictureBox.Name = "_pictureBox";
            _PictureBox.Size = new Size(76, 76);
            _PictureBox.TabIndex = 0;
            _PictureBox.TabStop = false;
            // 
            // MetroLoading
            // 
            AutoScaleDimensions = new SizeF(6.0f, 13.0f);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = Color.Transparent;
            Controls.Add(_PictureBox);
            Name = "MetroLoading";
            Size = new Size(76, 76);
            ((System.ComponentModel.ISupportInitialize)_PictureBox).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private PictureBox _PictureBox;
    }
}