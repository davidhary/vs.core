namespace isr.Core.Controls.ToolStripExtensions
{

    /// <summary> Includes extensions for <see cref="System.Windows.Forms.ToolStrip">Tool Strip</see>. </summary>
    /// <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2010-11-19, 1.2.3975 </para></remarks>
    public static class ToolStripExtensionMethods
    {

        #region " STATUS STRIP CUSTOM PROGRESS BAR "

        /// <summary>
        /// Updates the <see cref="StatusStripCustomProgressBar">control</see> value to
        /// the
        /// <paramref name="value">value</paramref> limited by the control range.
        /// This setter is thread safe.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="control"> The tool strip progress bar control. </param>
        /// <param name="value">   The candidate value. </param>
        /// <returns> The value limited within the range of the progress bar. </returns>
        public static int ValueUpdater( this StatusStripCustomProgressBar control, int value )
        {
            if ( control is object )
            {
                if ( control.Value != value )
                {
                    return control.ValueSetter( value );
                }
            }

            return value;
        }

        /// <summary>
        /// Sets the <see cref="CustomProgressBar">control</see> value to the
        /// <paramref name="value">value</paramref> limited by the control range.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="control"> The tool strip progress bar control. </param>
        /// <param name="value">   The candidate value. </param>
        /// <returns> The value limited within the range of the progress bar. </returns>
        public static int ValueSetter( this StatusStripCustomProgressBar control, int value )
        {
            if ( control is object )
            {
                value = value < control.Minimum ? control.Minimum : value > control.Maximum ? control.Maximum : value;
                if ( control.Value != value )
                {
                    control.Value = value;
                }

                return control.Value;
            }

            return value;
        }

        /// <summary>
        /// Sets the <see cref="StatusStripCustomProgressBar">
        /// control</see>
        /// value to the.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="control"> The tool strip progress bar control. </param>
        /// <param name="value">   The candidate value. </param>
        /// <returns> The value limited within the range of the progress bar. </returns>
        public static int ValueSetter( this StatusStripCustomProgressBar control, double value )
        {
            return control.ValueSetter( ( int ) value );
        }

        #endregion

    }
}
