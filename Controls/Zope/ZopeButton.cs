using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Controls
{

    /// <summary> A Zope button. </summary>
    /// <remarks>
    /// (c) 2017 Pritam Zope, All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-03-08, 3.1.6276 </para><para>
    /// https://www.codeproject.com/Articles/1068043/Creating-Custom-Windows-Forms-in-Csharp-using-Pane.
    /// </para>
    /// </remarks>
    public class ZopeButton : Button
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Windows.Forms.Button" /> class.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public ZopeButton() : base()
        {
            this.Size = new Size( 31, 24 );
            this.ForeColor = Color.White;
            this.FlatStyle = FlatStyle.Flat;
            this.Font = new Font( SystemFonts.MessageBoxFont.FontFamily, 20.25f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte( 0 ) );
            this.Text = "_";
            this._DisplayText = this.Text;
        }

        #region " TEXT "

        /// <summary> The display text. </summary>
        private string _DisplayText = "_";

        /// <summary> Gets or sets the display text. </summary>
        /// <value> The display text. </value>
        public string DisplayText
        {
            get => this._DisplayText;

            set {
                this._DisplayText = value;
                this.Invalidate();
            }
        }

        /// <summary> The text location left. </summary>
        private int _TextLocationLeft = 6;

        /// <summary> Gets or sets the text location left. </summary>
        /// <value> The text location left. </value>
        public int TextLocationLeft
        {
            get => this._TextLocationLeft;

            set {
                this._TextLocationLeft = value;
                this.Invalidate();
            }
        }

        /// <summary> The text location top. </summary>
        private int _TextLocationTop = -20;

        /// <summary> Gets or sets the text location top. </summary>
        /// <value> The text location top. </value>
        public int TextLocationTop
        {
            get => this._TextLocationTop;

            set {
                this._TextLocationTop = value;
                this.Invalidate();
            }
        }

        #endregion

        #region " BORDER "

        private int _BorderWidth = 2;

        /// <summary> Gets or sets the width of the border. </summary>
        /// <value> The width of the border. </value>
        [Category( "Appearance" )]
        [Description( "Width of button border." )]
        public int BorderWidth
        {
            get => this._BorderWidth;

            set {
                this._BorderWidth = value;
                this.Invalidate();
            }
        }

        /// <summary> Sets border color. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="borderColor"> The border color. </param>
        private void SetBorderColor( Color borderColor )
        {
            int red = borderColor.R - 40;
            int green = borderColor.G - 40;
            int blue = borderColor.B - 40;
            if ( red < 0 )
            {
                red = 0;
            }

            if ( green < 0 )
            {
                green = 0;
            }

            if ( blue < 0 )
            {
                blue = 0;
            }

            this._ButtonBorderColor = Color.FromArgb( red, green, blue );
        }

        /// <summary> The button border color. </summary>

        private Color _ButtonBorderColor = Color.FromArgb( 220, 220, 220 );

        private Color _BorderColor = Color.Transparent;

        /// <summary>   Gets or sets the color of the focus border. </summary>
        /// <value> The color of the focus border. </value>
        [Category( "Appearance" )]
        [Description( "Border color to show when the button is in focus." )]
        public Color BorderColorFocus { get; set; } = Color.FromArgb( 220, 220, 220 );

        /// <summary>   Gets or sets the color of the normal border. </summary>
        /// <value> The color of the normal border. </value>
        [Category( "Appearance" )]
        [Description( "Border color when button is not in focus." )]
        public Color BorderColorNormal { get; set; } = Color.Transparent;

        /// <summary> Gets or sets the color of the border. </summary>
        /// <value> The color of the border. </value>
        [Category( "Appearance" )]
        [Description( "Border color." )]
        public Color BorderColor
        {
            get => this._BorderColor;

            set {
                this._BorderColor = value;
                if ( this._BorderColor == Color.Transparent )
                {
                    this._ButtonBorderColor = Color.FromArgb( 220, 220, 220 );
                }
                else
                {
                    this.SetBorderColor( this._BorderColor );
                }
            }
        }

        #endregion

        #region " BACK COLORS "

        private Color _BackColorActive = Color.Teal;

        /// <summary> Gets or sets the active background color. </summary>
        /// <value> The color of the busy back. </value>
        public Color BackColorActive
        {
            get => this._BackColorActive;

            set {
                this._BackColorActive = value;
                this.Invalidate();
            }
        }

        private Color _BackColorNormal = Color.Teal;

        /// <summary> Gets or sets the Normal background color. </summary>
        /// <value> The color of the busy back. </value>
        public Color BackColorNormal
        {
            get => this._BackColorNormal;

            set {
                this._BackColorNormal = value;
                this.Invalidate();
            }
        }

        private Color _BackColorMouseHover = Color.FromArgb( 0, 0, 140 );

        /// <summary> Gets or sets the back color during mouse hover. </summary>
        /// <value> The back color during mouse hover. </value>
        public Color BackColorMouseHover
        {
            get => this._BackColorMouseHover;

            set {
                this._BackColorMouseHover = value;
                this.Invalidate();
            }
        }

        private Color _BackColorMouseClick = Color.FromArgb( 160, 180, 200 );

        /// <summary> Gets or sets the background color of the mouse click. </summary>
        /// <value> The background color of the mouse click. </value>
        public Color BackColorMouseClick
        {
            get => this._BackColorMouseClick;

            set {
                this._BackColorMouseClick = value;
                this.Invalidate();
            }
        }

        /// <summary> True to enable, false to disable the mouse colors. </summary>
        private bool _MouseColorsEnabled = true;

        /// <summary> Gets or sets the mouse colors enabled. </summary>
        /// <value> The mouse colors enabled. </value>
        public bool MouseColorsEnabled
        {
            get => this._MouseColorsEnabled;

            set {
                this._MouseColorsEnabled = value;
                this.Invalidate();
            }
        }

        #endregion

        #region " EVENT HANDLING "

        /// <summary> True to hover. </summary>
        private bool _Hover = false;

        /// <summary> True to down. </summary>
        private bool _Down = false;

        /// <summary>
        /// Raises the
        /// <see cref="M:System.Windows.Forms.Control.OnMouseMove(System.Windows.Forms.MouseEventArgs)" />
        /// event.
        /// </summary>
        /// <remarks>   David, 2021-04-19. </remarks>
        /// <param name="e">    A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
        ///                     event data. </param>
        protected override void OnMouseMove( MouseEventArgs e )
        {
            this._Hover = true;
            if ( this._MouseColorsEnabled )
                this._BackColorActive = this._Down ? this._BackColorMouseClick : this._BackColorMouseHover;
            base.OnMouseMove( e );
        }

        /// <summary>
        /// Raises the <see cref="M:System.Windows.Forms.Control.OnMouseEnter(System.EventArgs)" /> event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnMouseEnter( EventArgs e )
        {
            this._Hover = true;
            if ( this._MouseColorsEnabled )
                this._BackColorActive = this.BackColorMouseHover;
            base.OnMouseEnter( e );
        }

        /// <summary>
        /// Raises the <see cref="M:System.Windows.Forms.Control.OnMouseLeave(System.EventArgs)" /> event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnMouseLeave( EventArgs e )
        {
            this._Hover = false;
            if ( this._MouseColorsEnabled )
                this._BackColorActive = this.BackColorNormal;
            this.SetBorderColor( this._BorderColor );
            base.OnMouseLeave( e );
        }

        /// <summary>
        /// Raises the
        /// <see cref="M:System.Windows.Forms.Control.OnMouseDown(System.Windows.Forms.MouseEventArgs)" />
        /// event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="mevent"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnMouseDown( MouseEventArgs mevent )
        {
            _ = this.Focus();
            this.OnMouseUp( null );
            this._Down = true;
            if ( this.MouseColorsEnabled )
                this._BackColorActive = this._BackColorMouseClick;
            this._ButtonBorderColor = this._BorderColor;
            base.OnMouseDown( mevent );
            this.SetBorderColor( this._BorderColor );
            this.Invalidate();
        }

        /// <summary>
        /// Raises the
        /// <see cref="M:System.Windows.Forms.ButtonBase.OnMouseUp(System.Windows.Forms.MouseEventArgs)" />
        /// event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="mevent"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnMouseUp( MouseEventArgs mevent )
        {
            if ( mevent is not object )
                // otherwise an exception on exit.
                return;
            this._Down = false;
            if ( this.MouseColorsEnabled )
                this._BackColorActive = this._Hover ? this._BackColorMouseHover : this._BackColorNormal;
            base.OnMouseUp( mevent );
            this.OnMouseLeave( mevent );
            this.SetBorderColor( this._BorderColor );
            this.Invalidate();
        }

        /// <summary>
        /// Raises the
        /// <see cref="M:System.Windows.Forms.ButtonBase.OnPaint(System.Windows.Forms.PaintEventArgs)" />
        /// event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="pevent"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnPaint( PaintEventArgs pevent )
        {
            if ( pevent is null )
            {
                return;
            }

            base.OnPaint( pevent );
            this._DisplayText = this.Text;
            if ( this._TextLocationLeft == 100 && this._TextLocationTop == 25 )
            {
                this._TextLocationLeft = this.Width / 3 + 10;
                this._TextLocationTop = this.Height / 2 - 1;
            }

            var p = new Point( this.TextLocationLeft, this.TextLocationTop );
            using ( var b = new SolidBrush( this._BackColorActive ) )
            {
                pevent.Graphics.FillRectangle( b, this.ClientRectangle );
            }

            using ( var b = new Pen( this._ButtonBorderColor ) )
            {
                int x = this.ClientRectangle.X;
                int y = this.ClientRectangle.Y;
                int w = this.ClientRectangle.Width;
                int h = this.ClientRectangle.Height;
                for ( int i = 0, loopTo = this.BorderWidth - 1; i <= loopTo; i++ )
                {
                    x += 1;
                    y += 1;
                    w -= (2);
                    h -= (2);
                    Rectangle rec = new( x, y, w, h );
                    pevent.Graphics.DrawRectangle( b, rec );
                }
            }

            using ( var b = new SolidBrush( this.ForeColor ) )
            {
                pevent.Graphics.DrawString( this.DisplayText, this.Font, b, p );
            }
        }

        #endregion

    }
}
