using System;
using System.Drawing;
using System.Windows.Forms;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Controls
{

    /// <summary> A zope check box. </summary>
    /// <remarks>
    /// (c) 2017 Pritam Zope, All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-03-08, 3.1.6276 </para><para>
    /// https://www.codeproject.com/Articles/1068043/Creating-Custom-Windows-Forms-in-Csharp-using-Pane.
    /// </para>
    /// </remarks>
    public class ZopeCheckBox : System.Windows.Forms.CheckBox
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Windows.Forms.CheckBox" /> class.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public ZopeCheckBox() : base()
        {
            this.ForeColor = Color.White;
            this.AutoSize = false;
            this.Font = new Font( SystemFonts.MessageBoxFont.FontFamily, 9.75f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte( 0 ) );
        }

        /// <summary> The display text. </summary>
        private string _DisplayText = string.Empty;

        /// <summary> Gets or sets the display text. </summary>
        /// <value> The display text. </value>
        public string DisplayText
        {
            get => this._DisplayText;

            set {
                this._DisplayText = value;
                this.Invalidate();
            }
        }

        /// <summary> The start color. </summary>
        private Color _StartColor = Color.SteelBlue;

        /// <summary> Gets or sets the color of the start. </summary>
        /// <value> The color of the start. </value>
        public Color StartColor
        {
            get => this._StartColor;

            set {
                this._StartColor = value;
                this.Invalidate();
            }
        }

        /// <summary> The end color. </summary>
        private Color _EndColor = Color.DarkBlue;

        /// <summary> Gets or sets the color of the end. </summary>
        /// <value> The color of the end. </value>
        public Color EndColor
        {
            get => this._EndColor;

            set {
                this._EndColor = value;
                this.Invalidate();
            }
        }

        /// <summary> The mouse hover start color. </summary>
        private Color _MouseHoverStartColor = Color.Yellow;

        /// <summary> Gets or sets the color of the mouse hover start. </summary>
        /// <value> The color of the mouse hover start. </value>
        public Color MouseHoverStartColor
        {
            get => this._MouseHoverStartColor;

            set {
                this._MouseHoverStartColor = value;
                this.Invalidate();
            }
        }

        /// <summary> The mouse hover end color. </summary>
        private Color _MouseHoverEndColor = Color.DarkOrange;

        /// <summary> Gets or sets the color of the mouse hover end. </summary>
        /// <value> The color of the mouse hover end. </value>
        public Color MouseHoverEndColor
        {
            get => this._MouseHoverEndColor;

            set {
                this._MouseHoverEndColor = value;
                this.Invalidate();
            }
        }

        /// <summary> The start opacity. </summary>
        private int _StartOpacity = 150;

        /// <summary> Gets or sets the start opacity. </summary>
        /// <value> The start opacity. </value>
        public int StartOpacity
        {
            get => this._StartOpacity;

            set {
                this._StartOpacity = value;
                if ( this._StartOpacity > 255 )
                {
                    this._StartOpacity = 255;
                    this.Invalidate();
                }
                else
                {
                    this.Invalidate();
                }
            }
        }

        /// <summary> The end opacity. </summary>
        private int _EndOpacity = 150;

        /// <summary> Gets or sets the end opacity. </summary>
        /// <value> The end opacity. </value>
        public int EndOpacity
        {
            get => this._EndOpacity;

            set {
                this._EndOpacity = value;
                if ( this._EndOpacity > 255 )
                {
                    this._EndOpacity = 255;
                    this.Invalidate();
                }
                else
                {
                    this.Invalidate();
                }
            }
        }

        /// <summary> The gradient angle. </summary>
        private int _GradientAngle = 90;

        /// <summary> Gets or sets the gradient angle. </summary>
        /// <value> The gradient angle. </value>
        public int GradientAngle
        {
            get => this._GradientAngle;

            set {
                this._GradientAngle = value;
                this.Invalidate();
            }
        }

        /// <summary> The text location. </summary>
        private Point _TextLocation = new( 14, 4 );

        /// <summary> Gets or sets the text location. </summary>
        /// <value> The text location. </value>
        public Point TextLocation
        {
            get => this._TextLocation;

            set {
                this._TextLocation = value;
                this.Invalidate();
            }
        }

        /// <summary> Size of the box. </summary>
        private Size _BoxSize = new( 18, 18 );

        /// <summary> Gets or sets the box size. </summary>
        /// <value> The size of the box. </value>
        public Size BoxSize
        {
            get => this._BoxSize;

            set {
                this._BoxSize = value;
                this.Invalidate();
            }
        }

        /// <summary> The box location. </summary>
        private Point _BoxLocation = new( 0, 0 );

        /// <summary> Gets or sets the box location. </summary>
        /// <value> The box location. </value>
        public Point BoxLocation
        {
            get => this._BoxLocation;

            set {
                this._BoxLocation = value;
                this.Invalidate();
            }
        }

        /// <summary> The cached start color. </summary>
        private Color _CachedStartColor, _CachedEndColor;

        /// <summary>
        /// Raises the <see cref="M:System.Windows.Forms.Control.OnMouseEnter(System.EventArgs)" /> event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnMouseEnter( EventArgs e )
        {
            base.OnMouseEnter( e );
            this._CachedStartColor = this.StartColor;
            this._CachedEndColor = this.EndColor;
            this._StartColor = this._MouseHoverStartColor;
            this._EndColor = this._MouseHoverEndColor;
        }

        /// <summary>
        /// Raises the <see cref="M:System.Windows.Forms.Control.OnMouseLeave(System.EventArgs)" /> event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnMouseLeave( EventArgs e )
        {
            base.OnMouseLeave( e );
            this._StartColor = this._CachedStartColor;
            this._EndColor = this._CachedEndColor;
        }

        /// <summary>
        /// Raises the
        /// <see cref="M:System.Windows.Forms.ButtonBase.OnPaint(System.Windows.Forms.PaintEventArgs)" />
        /// event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnPaint( PaintEventArgs e )
        {
            if ( e is null )
            {
                return;
            }

            base.OnPaint( e );
            this.AutoSize = false;
            // _DisplayText = _DisplayText;
            if ( this.TextLocation.X == 100 && this.TextLocation.Y == 25 )
            {
                this.TextLocation = new Point( this.Width / 3 + 10, this.Height / 2 - 1 );
            }
            // drawing string & filling gradient rectangle
            var c1 = Color.FromArgb( this._StartOpacity, this._StartColor );
            var c2 = Color.FromArgb( this._EndOpacity, this._EndColor );
            using ( Brush b = new System.Drawing.Drawing2D.LinearGradientBrush( this.ClientRectangle, c1, c2, this._GradientAngle ) )
            {
                e.Graphics.FillRectangle( b, this.ClientRectangle );
            }

            var p = new Point( this.TextLocation.X, this.TextLocation.Y );
            using ( var frcolor = new SolidBrush( this.ForeColor ) )
            {
                e.Graphics.DrawString( this._DisplayText, this.Font, frcolor, p );
            }

            var rc = new Rectangle( this.BoxLocation, this.BoxSize );
            // drawing check box
            ControlPaint.DrawCheckBox( e.Graphics, rc, this.Checked ? ButtonState.Checked : ButtonState.Normal );
        }
    }
}
