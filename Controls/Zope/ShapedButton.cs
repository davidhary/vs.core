using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Controls
{

    /// <summary> A shaped button. </summary>
    /// <remarks>
    /// (c) 2017 Pritam Zope, All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-03-08, 3.1.6276 </para><para>
    /// https://www.codeproject.com/Articles/1068043/Creating-Custom-Windows-Forms-in-Csharp-using-Pane.
    /// </para>
    /// </remarks>
    public class ShapedButton : Button
    {

        #region " CONSTRUCTION "

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Windows.Forms.Button" /> class.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public ShapedButton()
        {
            this.Size = new Size( 100, 40 );
            this.BackColor = Color.Transparent;
            this.FlatStyle = FlatStyle.Flat;
            this.FlatAppearance.BorderSize = 0;
            this.FlatAppearance.MouseOverBackColor = Color.Transparent;
            this.FlatAppearance.MouseDownBackColor = Color.Transparent;
            this._ButtonText = this.Text;
            this.Font = new Font( SystemFonts.MessageBoxFont.FontFamily, 20.25f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte( 0 ) );
        }

        #endregion

        #region " SHAPE "

        /// <summary> The radius percent. </summary>
        private int _RadiusPercent = 25;

        /// <summary> Gets or sets the radius percent. </summary>
        /// <value> The radius percent. </value>
        [Category( "Appearance" )]
        [Description( "Relative radius." )]
        public int RadiusPercent
        {
            get => this._RadiusPercent;

            set {
                this._RadiusPercent = value;
                this.Invalidate();
            }
        }

        /// <summary> The button shape. </summary>
        private ButtonShape _ButtonShape;

        /// <summary> Gets or sets the button shape. </summary>
        /// <value> The button shape. </value>
        [Category( "Appearance" )]
        [Description( "Button shape" )]
        public ButtonShape ButtonShape
        {
            get => this._ButtonShape;

            set {
                this._ButtonShape = value;
                this.Invalidate();
            }
        }

        #endregion

        #region " TEXT "

        /// <summary> The button text. </summary>
        private string _ButtonText = string.Empty;

        /// <summary> Gets or sets the button text. </summary>
        /// <value> The button text. </value>
        [Category( "Appearance" )]
        [Description( "Text to show." )]
        public string ButtonText
        {
            get => this._ButtonText;
            set {
                this._ButtonText = value;
                this.Invalidate();
            }
        }

        /// <summary> The text location. </summary>
        private Point _TextLocation = new( 100, 25 );

        /// <summary> Gets or sets the text location. </summary>
        /// <value> The text location. </value>
        [Category( "Appearance" )]
        [Description( "Text location." )]
        public Point TextLocation
        {
            get => this._TextLocation;

            set {
                this._TextLocation = value;
                this.Invalidate();
            }
        }

        /// <summary> True to show, false to hide the button text. </summary>
        private bool _ShowButtonText = true;

        /// <summary> Gets or sets the show button text. </summary>
        /// <value> The show button text. </value>
        [Category( "Appearance" )]
        [Description( "Toggles appearance of button text." )]
        public bool ShowButtonText
        {
            get => this._ShowButtonText;

            set {
                this._ShowButtonText = value;
                this.Invalidate();
            }
        }

        #endregion

        #region " BORDER "

        private int _BorderWidth = 2;

        /// <summary> Gets or sets the width of the border. </summary>
        /// <value> The width of the border. </value>
        [Category( "Appearance" )]
        [Description( "Width of button border." )]
        public int BorderWidth
        {
            get => this._BorderWidth;

            set {
                this._BorderWidth = value;
                this.Invalidate();
            }
        }

        /// <summary> Sets border color. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="borderColor"> The border color. </param>
        private void SetBorderColor( Color borderColor )
        {
            int red = borderColor.R - 40;
            int green = borderColor.G - 40;
            int blue = borderColor.B - 40;
            if ( red < 0 )
            {
                red = 0;
            }

            if ( green < 0 )
            {
                green = 0;
            }

            if ( blue < 0 )
            {
                blue = 0;
            }

            this._ButtonBorderColor = Color.FromArgb( red, green, blue );
        }

        /// <summary> The button border color. </summary>

        private Color _ButtonBorderColor = Color.FromArgb( 220, 220, 220 );

        private Color _BorderColor = Color.Transparent;

        /// <summary>   Gets or sets the color of the focus border. </summary>
        /// <value> The color of the focus border. </value>
        [Category( "Appearance" )]
        [Description( "Border color to show when the button is in focus." )]
        public Color BorderColorFocus { get; set; } = Color.FromArgb( 220, 220, 220 );

        /// <summary>   Gets or sets the color of the normal border. </summary>
        /// <value> The color of the normal border. </value>
        [Category( "Appearance" )]
        [Description( "Border color when button is not in focus." )]
        public Color BorderColorNormal { get; set; } = Color.Transparent;

        /// <summary> Gets or sets the color of the border. </summary>
        /// <value> The color of the border. </value>
        [Category( "Appearance" )]
        [Description( "Border color." )]
        public Color BorderColor
        {
            get => this._BorderColor;

            set {
                this._BorderColor = value;
                if ( this._BorderColor == Color.Transparent )
                {
                    this._ButtonBorderColor = Color.FromArgb( 220, 220, 220 );
                }
                else
                {
                    this.SetBorderColor( this._BorderColor );
                }
            }
        }

        #endregion

        #region " BUTTON COLORS "

        private Color _BackStartColor = Color.DodgerBlue;

        /// <summary> Gets or sets the start of the background . </summary>
        /// <value> The start of the background color. </value>
        [Category( "Appearance" )]
        [Description( "Background  Start color" )]
        public Color BackStartColor
        {
            get => this._BackStartColor;

            set {
                this._BackStartColor = value;
                this.Invalidate();
            }
        }

        private Color _BackEndColor = Color.MidnightBlue;

        /// <summary> Gets or sets the end of the background color. </summary>
        /// <value> The end of the background color. </value>
        [Category( "Appearance" )]
        [Description( "Background  end color." )]
        public Color BackEndColor
        {
            get => this._BackEndColor;

            set {
                this._BackEndColor = value;
                this.Invalidate();
            }
        }

        private Color _BackStartColorNormal = Color.DodgerBlue;

        /// <summary> Gets or sets the start of the normal background color. </summary>
        /// <value> The start of the normal background color. </value>
        [Category( "Appearance" )]
        [Description( "Start color to show when the button is not in focus or hovered over." )]
        public Color BackStartColorNormal
        {
            get => this._BackStartColorNormal;

            set {
                this._BackStartColorNormal = value;
                this.Invalidate();
            }
        }

        private Color _BackEndColorNormal = Color.MidnightBlue;

        /// <summary> Gets or sets the end of the normal background color. </summary>
        /// <value> The end of the normal background color. </value>
        [Category( "Appearance" )]
        [Description( "End color to show when the button is not in focus or hovered over." )]
        public Color BackEndColorNormal
        {
            get => this._BackEndColorNormal;

            set {
                this._BackEndColorNormal = value;
                this.Invalidate();
            }
        }

        private Color _BackStartColorMouseHover = Color.Turquoise;

        /// <summary> Gets or sets the start of the mouse hover background color. </summary>
        /// <value> The start of the mouse hover background color. </value>
        [Category( "Appearance" )]
        [Description( "Start color to show when the button is hovered over." )]
        public Color BackStartColorMouseHover
        {
            get => this._BackStartColorMouseHover;

            set {
                this._BackStartColorMouseHover = value;
                this.Invalidate();
            }
        }

        private Color _BackEndColorMouseHover = Color.DarkSlateGray;

        /// <summary> Gets or sets the end of the mouse hover background color. </summary>
        /// <value> The end of the mouse hover background color. </value>
        [Category( "Appearance" )]
        [Description( "End color to show when the button is hovered over." )]
        public Color BackEndColorMouseHover
        {
            get => this._BackEndColorMouseHover;

            set {
                this._BackEndColorMouseHover = value;
                this.Invalidate();
            }
        }

        private Color _BackStartColorMouseClick = Color.Yellow;

        /// <summary> Gets or sets the start of the mouse click background color. </summary>
        /// <value> The start of the mouse click background color. </value>
        [Category( "Appearance" )]
        [Description( "Start color to show when the button is clicked." )]
        public Color BackStartColorMouseClick
        {
            get => this._BackStartColorMouseClick;

            set {
                this._BackStartColorMouseClick = value;
                this.Invalidate();
            }
        }

        private Color _BackEndColorMouseClick = Color.Red;

        /// <summary> Gets or sets the end of the mouse click background color. </summary>
        /// <value> The end of the mouse click background color. </value>
        [Category( "Appearance" )]
        [Description( "End color to show when the button is hovered over." )]
        public Color BackEndColorMouseClick
        {
            get => this._BackEndColorMouseClick;

            set {
                this._BackEndColorMouseClick = value;
                this.Invalidate();
            }
        }

        private int _OpacityStart = 250;

        /// <summary> Gets or sets the start opacity. </summary>
        /// <value> The start opacity. </value>
        [Category( "Appearance" )]
        [Description( "Start opacity." )]
        public int OpacityStart
        {
            get => this._OpacityStart;

            set {
                this._OpacityStart = value;
                if ( this._OpacityStart > 255 )
                {
                    this._OpacityStart = 255;
                    this.Invalidate();
                }
                else
                {
                    this.Invalidate();
                }
            }
        }

        private int _OpacityEnd = 250;

        /// <summary> Gets or sets the end opacity. </summary>
        /// <value> The end opacity. </value>
        [Category( "Appearance" )]
        [Description( "End opacity." )]
        public int OpacityEnd
        {
            get => this._OpacityEnd;

            set {
                this._OpacityEnd = value;
                if ( this._OpacityEnd > 255 )
                {
                    this._OpacityEnd = 255;
                    this.Invalidate();
                }
                else
                {
                    this.Invalidate();
                }
            }
        }

        /// <summary> The gradient angle. </summary>
        private int _GradientAngle = 90;

        /// <summary> Gets or sets the gradient angle. </summary>
        /// <value> The gradient angle. </value>
        [Category( "Appearance" )]
        [Description( "Gradient angle." )]
        public int GradientAngle
        {
            get => this._GradientAngle;

            set {
                this._GradientAngle = value;
                this.Invalidate();
            }
        }

        /// <summary> True to hover. </summary>
        private bool _Hover = false;

        /// <summary> True to down. </summary>
        private bool _Down = false;

        /// <summary>
        /// Raises the
        /// <see cref="M:System.Windows.Forms.Control.OnMouseMove(System.Windows.Forms.MouseEventArgs)" />
        /// event.
        /// </summary>
        /// <remarks>   David, 2021-04-19. </remarks>
        /// <param name="e">    A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
        ///                     event data. </param>
        protected override void OnMouseMove( MouseEventArgs e )
        {
            this._Hover = true;
            if ( this._Down )
            {
                this._BackStartColor = this._BackStartColorMouseClick;
                this._BackEndColor = this._BackEndColorMouseClick;
            }
            else
            {
                this._BackStartColor = this._BackStartColorMouseHover;
                this._BackEndColor = this._BackEndColorMouseHover;
            }
            base.OnMouseMove( e );
        }

        /// <summary>
        /// Raises the <see cref="M:System.Windows.Forms.Control.OnMouseEnter(System.EventArgs)" /> event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnMouseEnter( EventArgs e )
        {
            this._Hover = true;
            this._BackStartColor = this._BackStartColorMouseHover;
            this._BackEndColor = this._BackEndColorMouseHover;
            base.OnMouseEnter( e );
        }

        /// <summary>
        /// Raises the <see cref="M:System.Windows.Forms.Control.OnMouseLeave(System.EventArgs)" /> event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnMouseLeave( EventArgs e )
        {
            this._Hover = false;
            this._BackStartColor = this._BackStartColorNormal;
            this._BackEndColor = this._BackEndColorNormal;
            this.SetBorderColor( this._BorderColor );
            base.OnMouseLeave( e );
        }

        /// <summary>
        /// Raises the
        /// <see cref="M:System.Windows.Forms.Control.OnMouseDown(System.Windows.Forms.MouseEventArgs)" />
        /// event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="mevent"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnMouseDown( MouseEventArgs mevent )
        {
            _ = this.Focus();
            this.OnMouseUp( null );
            this._Down = true;
            this._BackStartColor = this._BackStartColorMouseClick;
            this._BackEndColor = this._BackEndColorMouseClick;
            this._ButtonBorderColor = this._BorderColor;
            base.OnMouseDown( mevent );
            this.SetBorderColor( this._BorderColor );
            this.Invalidate();
        }

        /// <summary>
        /// Raises the
        /// <see cref="M:System.Windows.Forms.ButtonBase.OnMouseUp(System.Windows.Forms.MouseEventArgs)" />
        /// event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="mevent"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnMouseUp( MouseEventArgs mevent )
        {
            if ( mevent is not object )
                // otherwise an exception on exit.
                return;
            this._Down = false;
            if ( this._Hover )
            {
                this._BackStartColor = this._BackStartColorMouseHover;
                this._BackEndColor = this._BackEndColorMouseHover;
            }
            else
            {
                this._BackStartColor = this._BackStartColorNormal;
                this._BackEndColor = this._BackEndColorNormal;
            }
            base.OnMouseUp( mevent );
            this.OnMouseLeave( mevent );
            this.SetBorderColor( this._BorderColor );
            this.Invalidate();
        }

        /// <summary>
        /// Raises the <see cref="M:System.Windows.Forms.ButtonBase.OnLostFocus(System.EventArgs)" />
        /// event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnLostFocus( EventArgs e )
        {
            this._BackStartColor = this._BackStartColorNormal;
            this._BackEndColor = this._BackEndColorNormal;
            this.SetBorderColor( this.BorderColorNormal );
            base.OnLostFocus( e );
            this.Invalidate();
        }


        /// <summary>   Raises the <see cref="E:System.Windows.Forms.Control.GotFocus" /> event. </summary>
        /// <remarks>   David, 2021-04-19. </remarks>
        /// <param name="e">    An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnGotFocus( EventArgs e )
        {
            this.SetBorderColor( this.BorderColorFocus );
            base.OnGotFocus( e );
            this.Invalidate();
        }

        /// <summary>   Raises the <see cref="E:System.Windows.Forms.Control.Click" /> event. </summary>
        /// <remarks>   David, 2021-04-19. </remarks>
        /// <param name="e">    An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnClick( EventArgs e )
        {
            this._BackStartColor = this._BackStartColorMouseClick;
            this._BackEndColor = this._BackEndColorMouseClick;
            this.SetBorderColor( this.BorderColorNormal );
            base.OnClick( e );
            this._BackStartColor = this._BackStartColorNormal;
            this._BackEndColor = this._BackEndColorNormal;
            this.SetBorderColor( this.BorderColorFocus );
        }


        #endregion

        #region " RENDER "

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.Resize" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnResize( EventArgs e )
        {
            base.OnResize( e );
            this.TextLocation = new Point( this.Width / 3 - 1, this.Height / 3 + 5 );
        }

        /// <summary> Draw circular button. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="g"> The Graphics to process. </param>
        private void DrawCircularButton( Graphics g )
        {
            var c1 = Color.FromArgb( this._OpacityStart, this._BackStartColor );
            var c2 = Color.FromArgb( this._OpacityEnd, this._BackEndColor );
            using ( Brush b = new LinearGradientBrush( this.ClientRectangle, c1, c2, this._GradientAngle ) )
            {
                g.FillEllipse( b, 5, 5, this.Width - 10, this.Height - 10 );
            }

            using ( var b = new SolidBrush( this._ButtonBorderColor ) )
            {
                using var p = new Pen( b );
                for ( int i = 0, loopTo = this._BorderWidth - 1; i <= loopTo; i++ )
                {
                    g.DrawEllipse( p, 5 + i, 5, this.Width - 10, this.Height - 10 );
                }
            }

            if ( this._ShowButtonText )
            {
                var p = new Point( this.TextLocation.X, this.TextLocation.Y );
                using var sb = new SolidBrush( this.ForeColor );
                g.DrawString( this._ButtonText, this.Font, sb, p );
            }
        }

        /// <summary> Draw round rectangular button. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="g"> The Graphics to process. </param>
        private void DrawRoundRectangularButton( Graphics g )
        {
            var c1 = Color.FromArgb( this._OpacityStart, this._BackStartColor );
            var c2 = Color.FromArgb( this._OpacityEnd, this._BackEndColor );
            var rec = new Rectangle( 5, 5, this.Width, this.Height );

            // create the radius variable and set it equal to 20% the height of the rectangle
            // this will determine the amount of bend at the corners
            int radius = ( int ) Conversion.Fix( 0.01d * Math.Min( rec.Height, rec.Height ) * this.RadiusPercent );

            // make sure that we have a valid radius, too small and we have a problem
            if ( radius < 1 )
            {
                radius = 2;
            }

            int diameter = radius * 2;

            // create an x and y variable so that we can reduce the length of our code lines
            int x = rec.Left;
            int y = rec.Top;
            int h = rec.Height;
            int w = rec.Width;
            using ( var _region = new Region( rec ) )
            {
                using ( var path = new GraphicsPath() )
                {
                    path.AddArc( x, y, diameter, diameter, 180f, 90f );
                    path.AddArc( w - diameter - x, y, diameter, diameter, 270f, 90f );
                    path.AddArc( w - diameter - x, h - diameter - y, diameter, diameter, 0f, 90f );
                    path.AddArc( x, h - diameter - y, diameter, diameter, 90f, 90f );
                    path.CloseFigure();
                    _region.Intersect( path );
                }

                using Brush b = new LinearGradientBrush( this.ClientRectangle, c1, c2, this._GradientAngle );
                g.FillRegion( b, _region );
            }

            using ( var p = new Pen( this._ButtonBorderColor ) )
            {
                for ( int i = 0, loopTo = this._BorderWidth - 1; i <= loopTo; i++ )
                {
                    g.DrawArc( p, x + i, y + i, diameter, diameter, 180, 90 );
                    g.DrawLine( p, x + radius, y + i, this.Width - x - radius, y + i );
                    g.DrawArc( p, this.Width - diameter - x - i, y + i, diameter, diameter, 270, 90 );
                    g.DrawLine( p, x + i, y + radius, x + i, this.Height - y - radius );
                    g.DrawLine( p, this.Width - x - i, y + radius, this.Width - x - i, this.Height - y - radius );
                    g.DrawArc( p, this.Width - diameter - x - i, this.Height - diameter - y - i, diameter, diameter, 0, 90 );
                    g.DrawLine( p, x + radius, this.Height - y - i, this.Width - x - radius, this.Height - y - i );
                    g.DrawArc( p, x + i, this.Height - diameter - y - i, diameter, diameter, 90, 90 );
                }
            }

            if ( this._ShowButtonText )
            {
                using var sb = new SolidBrush( this.ForeColor );
                g.DrawString( this.ButtonText, this.Font, sb, this.TextLocation );
            }
        }

        /// <summary>
        /// Raises the
        /// <see cref="M:System.Windows.Forms.ButtonBase.OnPaint(System.Windows.Forms.PaintEventArgs)" />
        /// event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnPaint( PaintEventArgs e )
        {
            if ( e is null )
            {
                return;
            }

            e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
            base.OnPaint( e );
            switch ( this._ButtonShape )
            {
                case ButtonShape.Circle:
                    {
                        this.DrawCircularButton( e.Graphics );
                        break;
                    }

                case ButtonShape.RoundRect:
                    {
                        this.DrawRoundRectangularButton( e.Graphics );
                        break;
                    }
            }
        }

        #endregion

    }

    /// <summary> Values that represent button shapes. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public enum ButtonShape
    {

        /// <summary> An enum constant representing the round Rectangle option. </summary>
        RoundRect,

        /// <summary> An enum constant representing the circle option. </summary>
        Circle
    }
}
