using System.Drawing;
using System.Windows.Forms;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Controls
{

    /// <summary> Panel Control. </summary>
    /// <remarks>
    /// (c) 2017 Pritam Zope, All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-03-08, 3.1.6276 </para><para>
    /// https://www.codeproject.com/Articles/1068043/Creating-Custom-Windows-Forms-in-Csharp-using-Pane.
    /// </para>
    /// </remarks>
    public class ZopePanel : Panel
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Windows.Forms.Panel" /> class.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public ZopePanel() : base()
        {
            this.Font = new Font( SystemFonts.MessageBoxFont.FontFamily, 9.75f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte( 0 ) );
        }

        /// <summary> The start color. </summary>
        private Color _StartColor = Color.SteelBlue;

        /// <summary> Gets or sets the color of the start. </summary>
        /// <value> The color of the start. </value>
        public Color StartColor
        {
            get => this._StartColor;

            set {
                this._StartColor = value;
                this.Invalidate();
            }
        }

        /// <summary> The end color. </summary>
        private Color _EndColor = Color.DarkBlue;

        /// <summary> Gets or sets the color of the end. </summary>
        /// <value> The color of the end. </value>
        public Color EndColor
        {
            get => this._EndColor;

            set {
                this._EndColor = value;
                this.Invalidate();
            }
        }

        /// <summary> The start opacity. </summary>
        private int _StartOpacity = 150;

        /// <summary> Gets or sets the start opacity. </summary>
        /// <value> The start opacity. </value>
        public int StartOpacity
        {
            get => this._StartOpacity;

            set {
                this._StartOpacity = value;
                if ( this._StartOpacity > 255 )
                {
                    this._StartOpacity = 255;
                    this.Invalidate();
                }
                else
                {
                    this.Invalidate();
                }
            }
        }

        /// <summary> The end opacity. </summary>
        private int _EndOpacity = 150;

        /// <summary> Gets or sets the end opacity. </summary>
        /// <value> The end opacity. </value>
        public int EndOpacity
        {
            get => this._EndOpacity;

            set {
                this._EndOpacity = value;
                if ( this._EndOpacity > 255 )
                {
                    this._EndOpacity = 255;
                    this.Invalidate();
                }
                else
                {
                    this.Invalidate();
                }
            }
        }

        /// <summary> The gradient angle. </summary>
        private int _GradientAngle = 90;

        /// <summary> Gets or sets the gradient angle. </summary>
        /// <value> The gradient angle. </value>
        public int GradientAngle
        {
            get => this._GradientAngle;

            set {
                this._GradientAngle = value;
                this.Invalidate();
            }
        }

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.Paint" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnPaint( PaintEventArgs e )
        {
            if ( e is null )
            {
                return;
            }

            base.OnPaint( e );
            var startingColor = Color.FromArgb( this._StartOpacity, this._StartColor );
            var endingColor = Color.FromArgb( this._EndOpacity, this._EndColor );
            using Brush b = new System.Drawing.Drawing2D.LinearGradientBrush( this.ClientRectangle, startingColor, endingColor, this._GradientAngle );
            e.Graphics.FillRectangle( b, this.ClientRectangle );
        }
    }
}
