using System;
using System.Drawing;
using System.Windows.Forms;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Controls
{

    /// <summary> A minimum maximum button. </summary>
    /// <remarks>
    /// (c) 2017 Pritam Zope, All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-03-08, 3.1.6276 </para><para>
    /// https://www.codeproject.com/Articles/1068043/Creating-Custom-Windows-Forms-in-Csharp-using-Pane.
    /// </para>
    /// </remarks>
    public class MinMaxButton : Button
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Windows.Forms.Button" /> class.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public MinMaxButton() : base()
        {
            this.Size = new Size( 31, 24 );
            this.Font = new Font( SystemFonts.MessageBoxFont.FontFamily, 9.75f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte( 0 ) );
            this.ForeColor = Color.White;
            this.FlatStyle = FlatStyle.Flat;
            this.Text = "_";
            this._DisplayText = this.Text;
        }

        /// <summary> State of the custom form. </summary>
        private CustomFormState _CustomFormState;

        /// <summary> Gets or sets the state of the custom form. </summary>
        /// <value> The custom form state. </value>
        public CustomFormState CustomFormState
        {
            get => this._CustomFormState;

            set {
                this._CustomFormState = value;
                this.Invalidate();
            }
        }

        /// <summary> The display text. </summary>
        private string _DisplayText = "_";

        /// <summary> Gets or sets the display text. </summary>
        /// <value> The display text. </value>
        public string DisplayText
        {
            get => this._DisplayText;

            set {
                this._DisplayText = value;
                this.Invalidate();
            }
        }

        /// <summary> The busy back color. </summary>
        private Color _BusyBackColor = Color.Gray;

        /// <summary> Gets or sets the color of the busy back. </summary>
        /// <value> The color of the busy back. </value>
        public Color BusyBackColor
        {
            get => this._BusyBackColor;

            set {
                this._BusyBackColor = value;
                this.Invalidate();
            }
        }

        /// <summary> The mouse hover color. </summary>
        private Color _MouseHoverColor = Color.FromArgb( 180, 200, 240 );

        /// <summary> Gets or sets the color of the mouse hover. </summary>
        /// <value> The color of the mouse hover. </value>
        public Color MouseHoverColor
        {
            get => this._MouseHoverColor;

            set {
                this._MouseHoverColor = value;
                this.Invalidate();
            }
        }

        /// <summary> The mouse click color. </summary>
        private Color _MouseClickColor = Color.FromArgb( 160, 180, 200 );

        /// <summary> Gets or sets the color of the mouse click. </summary>
        /// <value> The color of the mouse click. </value>
        public Color MouseClickColor
        {
            get => this._MouseClickColor;

            set {
                this._MouseClickColor = value;
                this.Invalidate();
            }
        }

        /// <summary> The text location. </summary>
        private Point _TextLocation = new( 6, -20 );

        /// <summary> Gets or sets the text location. </summary>
        /// <value> The text location. </value>
        public Point TextLocation
        {
            get => this._TextLocation;

            set {
                this._TextLocation = value;
                this.Invalidate();
            }
        }

        /// <summary> The cached back color. </summary>
        private Color _CachedBackColor;

        /// <summary>
        /// Raises the <see cref="M:System.Windows.Forms.Control.OnMouseEnter(System.EventArgs)" /> event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnMouseEnter( EventArgs e )
        {
            base.OnMouseEnter( e );
            this._CachedBackColor = this._BusyBackColor;
            this._BusyBackColor = this._MouseHoverColor;
        }

        /// <summary>
        /// Raises the <see cref="M:System.Windows.Forms.Control.OnMouseLeave(System.EventArgs)" /> event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnMouseLeave( EventArgs e )
        {
            base.OnMouseLeave( e );
            this._BusyBackColor = this._CachedBackColor;
        }

        /// <summary>
        /// Raises the
        /// <see cref="M:System.Windows.Forms.Control.OnMouseDown(System.Windows.Forms.MouseEventArgs)" />
        /// event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="mevent"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnMouseDown( MouseEventArgs mevent )
        {
            base.OnMouseDown( mevent );
            this._BusyBackColor = this._MouseClickColor;
        }

        /// <summary>
        /// Raises the
        /// <see cref="M:System.Windows.Forms.ButtonBase.OnMouseUp(System.Windows.Forms.MouseEventArgs)" />
        /// event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="mevent"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnMouseUp( MouseEventArgs mevent )
        {
            base.OnMouseUp( mevent );
            this._BusyBackColor = this._CachedBackColor;
        }

        /// <summary>
        /// Raises the
        /// <see cref="M:System.Windows.Forms.ButtonBase.OnPaint(System.Windows.Forms.PaintEventArgs)" />
        /// event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="pevent"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnPaint( PaintEventArgs pevent )
        {
            if ( pevent is null )
            {
                return;
            }

            base.OnPaint( pevent );
            switch ( this._CustomFormState )
            {
                case CustomFormState.Normal:
                    {
                        using ( var brush = new SolidBrush( this.BusyBackColor ) )
                        {
                            pevent.Graphics.FillRectangle( brush, this.ClientRectangle );
                        }

                        // draw and fill the rectangles of maximized window     
                        using var p = new Pen( this.ForeColor );
                        using var b = new SolidBrush( this.ForeColor );
                        for ( int i = 0; i <= 1; i++ )
                        {
                            pevent.Graphics.DrawRectangle( p, this.TextLocation.X + i + 1, this.TextLocation.Y, 10, 10 );
                            pevent.Graphics.FillRectangle( b, this.TextLocation.X + 1, this.TextLocation.Y - 1, 12, 4 );
                        }

                        break;
                    }

                case CustomFormState.Maximize:
                    {
                        using ( var brush = new SolidBrush( this.BusyBackColor ) )
                        {
                            pevent.Graphics.FillRectangle( brush, this.ClientRectangle );
                        }

                        using var p = new Pen( this.ForeColor );
                        using var b = new SolidBrush( this.ForeColor );
                        // draw and fill the rectangles of maximized window     
                        for ( int i = 0; i <= 1; i++ )
                        {
                            pevent.Graphics.DrawRectangle( p, this.TextLocation.X + 5, this.TextLocation.Y, 8, 8 );
                            pevent.Graphics.FillRectangle( b, this.TextLocation.X + 5, this.TextLocation.Y - 1, 9, 4 );
                            pevent.Graphics.DrawRectangle( p, this.TextLocation.X + 2, this.TextLocation.Y + 5, 8, 8 );
                            pevent.Graphics.FillRectangle( b, this.TextLocation.X + 2, this.TextLocation.Y + 4, 9, 4 );
                        }

                        break;
                    }
            }
        }
    }

    /// <summary> Values that represent custom form states. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public enum CustomFormState
    {

        /// <summary> An enum constant representing the normal option. </summary>
        Normal,

        /// <summary> An enum constant representing the maximize option. </summary>
        Maximize
    }
}
