using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Controls
{

    /// <summary> A Zope tab control. </summary>
    /// <remarks>
    /// (c) 2017 Pritam Zope, All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-03-08, 3.1.6276 </para><para>
    /// https://www.codeproject.com/Articles/1068043/Creating-Custom-Windows-Forms-in-Csharp-using-Pane.
    /// </para>
    /// </remarks>
    public class ZopeTabControl : TabControl
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Windows.Forms.TabControl" /> class.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public ZopeTabControl() : base()
        {
            this.DrawMode = TabDrawMode.OwnerDrawFixed;
            this.Padding = new Point( 22, 4 );
            this.Font = new Font( SystemFonts.MessageBoxFont.FontFamily, 9.75f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte( 0 ) );
        }

        /// <summary> The active tab start color. </summary>

        private Color _ActiveTabStartColor = Color.Yellow;

        /// <summary> Gets or sets the active tab start color. </summary>
        /// <value> The color of the active tab start. </value>
        public Color ActiveTabStartColor
        {
            get => this._ActiveTabStartColor;

            set {
                this._ActiveTabStartColor = value;
                this.Invalidate();
            }
        }

        /// <summary> The active tab end color. </summary>

        private Color _ActiveTabEndColor = Color.DarkOrange;

        /// <summary> Gets or sets the active tab end color. </summary>
        /// <value> The color of the active tab end. </value>
        public Color ActiveTabEndColor
        {
            get => this._ActiveTabEndColor;

            set {
                this._ActiveTabEndColor = value;
                this.Invalidate();
            }
        }

        /// <summary> The inactive tab start color. </summary>

        private Color _InactiveTabStartColor = Color.LightGreen;

        /// <summary> Gets or sets the color of the inactive tab start. </summary>
        /// <value> The color of the inactive tab start. </value>
        public Color InactiveTabStartColor
        {
            get => this._InactiveTabStartColor;

            set {
                this._InactiveTabStartColor = value;
                this.Invalidate();
            }
        }

        /// <summary> The inactive tab end color. </summary>
        private Color _InactiveTabEndColor = Color.DarkBlue;

        /// <summary> Gets or sets the color of the inactive tab end. </summary>
        /// <value> The color of the inactive tab end. </value>
        public Color InactiveTabEndColor
        {
            get => this._InactiveTabEndColor;

            set {
                this._InactiveTabEndColor = value;
                this.Invalidate();
            }
        }

        /// <summary> The start opacity. </summary>
        private int _StartOpacity = 150;

        /// <summary> Gets or sets the start opacity. </summary>
        /// <value> The start opacity. </value>
        public int StartOpacity
        {
            get => this._StartOpacity;

            set {
                this._StartOpacity = value;
                if ( this._StartOpacity > 255 )
                {
                    this._StartOpacity = 255;
                    this.Invalidate();
                }
                else
                {
                    this.Invalidate();
                }
            }
        }

        /// <summary> The end opacity. </summary>

        private int _EndOpacity = 150;

        /// <summary> Gets or sets the end opacity. </summary>
        /// <value> The end opacity. </value>
        public int EndOpacity
        {
            get => this._EndOpacity;

            set {
                this._EndOpacity = value;
                if ( this._EndOpacity > 255 )
                {
                    this._EndOpacity = 255;
                    this.Invalidate();
                }
                else
                {
                    this.Invalidate();
                }
            }
        }

        /// <summary> The gradient angle. </summary>
        private int _GradientAngle = 90;

        /// <summary> Gets or sets the gradient angle. </summary>
        /// <value> The gradient angle. </value>
        public int GradientAngle
        {
            get => this._GradientAngle;

            set {
                this._GradientAngle = value;
                this.Invalidate();
            }
        }

        /// <summary> The text color. </summary>
        private Color _TextColor = Color.Navy;

        /// <summary> Gets or sets the color of the text. </summary>
        /// <value> The color of the text. </value>
        public Color TextColor
        {
            get => this._TextColor;

            set {
                this._TextColor = value;
                this.Invalidate();
            }
        }

        /// <summary> The close button color. </summary>
        private Color _CloseButtonColor = Color.Red;

        /// <summary> Gets or sets the color of the close button. </summary>
        /// <value> The color of the close button. </value>
        public Color CloseButtonColor
        {
            get => this._CloseButtonColor;

            set {
                this._CloseButtonColor = value;
                this.Invalidate();
            }
        }

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.Paint" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnPaint( PaintEventArgs e )
        {
            base.OnPaint( e );
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.TabControl.DrawItem" /> event.
        /// Draws tab items.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.DrawItemEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnDrawItem( DrawItemEventArgs e )
        {
            if ( e is null )
            {
                return;
            }

            base.OnDrawItem( e );
            var rc = this.GetTabRect( e.Index );

            // if tab is selected
            if ( string.Equals( this.SelectedTab.Text, this.TabPages[e.Index].Text ) )
            {
                var c1 = Color.FromArgb( this._StartOpacity, this._ActiveTabStartColor );
                var c2 = Color.FromArgb( this._EndOpacity, this._ActiveTabEndColor );
                using var br = new LinearGradientBrush( rc, c1, c2, this._GradientAngle );
                e.Graphics.FillRectangle( br, rc );
            }
            else
            {
                var c1 = Color.FromArgb( this._StartOpacity, this._InactiveTabStartColor );
                var c2 = Color.FromArgb( this._EndOpacity, this._InactiveTabEndColor );
                using var br = new LinearGradientBrush( rc, c1, c2, this._GradientAngle );
                e.Graphics.FillRectangle( br, rc );
            }

            this.TabPages[e.Index].BorderStyle = BorderStyle.FixedSingle;
            this.TabPages[e.Index].ForeColor = SystemColors.ControlText;

            // draw close button on tabs

            var paddedBounds = e.Bounds;
            paddedBounds.Inflate( -5, -4 );
            using ( var b = new SolidBrush( this._TextColor ) )
            {
                e.Graphics.DrawString( this.TabPages[e.Index].Text, this.Font, b, paddedBounds );
            }

            var pad = this.Padding;

            // drawing close button to tab items
            using ( var b = new SolidBrush( this._CloseButtonColor ) )
            {
                using var f = new Font( this.Font.FontFamily, 10f, FontStyle.Bold );
                e.Graphics.DrawString( "X", f, b, e.Bounds.Right + 1 - 18, e.Bounds.Top + pad.Y - 2 );
            }

            e.DrawFocusRectangle();
        }

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.MouseDown" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnMouseDown( MouseEventArgs e )
        {
            if ( e is null )
            {
                return;
            }

            base.OnMouseDown( e );
            int i = 0;
            while ( i < this.TabPages.Count )
            {
                var r = this.GetTabRect( i );
                var closeButton = new Rectangle( r.Right + 1 - 15, r.Top + 4, 12, 12 );
                if ( closeButton.Contains( e.Location ) )
                {
                    if ( this.IsTabClosing() )
                    {
                        this.TabPages.RemoveAt( i );
                    }

                    break;
                }

                i += 1;
            }
        }

        /// <summary> Event queue for all listeners interested in TabClosing events. </summary>
        public event EventHandler<System.ComponentModel.CancelEventArgs> TabClosing;

        /// <summary> Raises the system. component model. cancel event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected void OnTabClosing( System.ComponentModel.CancelEventArgs e )
        {
            var evt = TabClosing;
            evt?.Invoke( this, e );
        }

        /// <summary> Query if this object is tab closing. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> True if tab closing, false if not. </returns>
        private bool IsTabClosing()
        {
            var cancelArgs = new System.ComponentModel.CancelEventArgs();
            this.OnTabClosing( cancelArgs );
            return !cancelArgs.Cancel;
        }
    }
}
