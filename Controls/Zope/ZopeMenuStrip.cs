using System.Drawing;
using System.Windows.Forms;

namespace isr.Core.Controls
{

    /// <summary> A zope menu strip. </summary>
    /// <remarks>
    /// (c) 2017 Pritam Zope, All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-03-08, 3.1.6276 </para><para>
    /// https://www.codeproject.com/Articles/1068043/Creating-Custom-Windows-Forms-in-Csharp-using-Pane.
    /// </para>
    /// </remarks>
    public class ZopeMenuStrip : MenuStrip
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Windows.Forms.MenuStrip" /> class.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public ZopeMenuStrip()
        {
            this.Renderer = new ZopeDarkMenuRenderer();
        }
    }

    /// <summary> Zope Dark Menu renderer. </summary>
    /// <remarks>
    /// (c) 2017 Pritam Zope, All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-03-08, 3.1.6276 </para><para>
    /// https://www.codeproject.com/Articles/1068043/Creating-Custom-Windows-Forms-in-Csharp-using-Pane.
    /// </para>
    /// </remarks>
    public class ZopeDarkMenuRenderer : ToolStripRenderer
    {

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.ToolStripRenderer.RenderMenuItemBackground" />
        /// event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.ToolStripItemRenderEventArgs" /> that
        /// contains the event data. </param>
        protected override void OnRenderMenuItemBackground( ToolStripItemRenderEventArgs e )
        {
            if ( e is null )
            {
                return;
            }

            base.OnRenderMenuItemBackground( e );
            if ( e.Item.Enabled )
            {
                if ( e.Item.IsOnDropDown == false && e.Item.Selected )
                {
                    var rect = new Rectangle( 0, 0, e.Item.Width - 1, e.Item.Height - 1 );
                    var rect2 = new Rectangle( 0, 0, e.Item.Width - 1, e.Item.Height - 1 );
                    using ( var b = new SolidBrush( Color.FromArgb( 60, 60, 60 ) ) )
                    {
                        e.Graphics.FillRectangle( b, rect );
                    }

                    using ( var b = new SolidBrush( Color.Black ) )
                    {
                        using var p = new Pen( b );
                        e.Graphics.DrawRectangle( p, rect2 );
                    }

                    e.Item.ForeColor = Color.White;
                }
                else
                {
                    e.Item.ForeColor = Color.White;
                }

                if ( e.Item.IsOnDropDown && e.Item.Selected )
                {
                    var rect = new Rectangle( 0, 0, e.Item.Width - 1, e.Item.Height - 1 );
                    using ( var b = new SolidBrush( Color.FromArgb( 60, 60, 60 ) ) )
                    {
                        e.Graphics.FillRectangle( b, rect );
                    }

                    using ( var b = new SolidBrush( Color.Black ) )
                    {
                        using var p = new Pen( b );
                        e.Graphics.DrawRectangle( p, rect );
                    }

                    e.Item.ForeColor = Color.White;
                }

                if ( (e.Item as System.Windows.Forms.ToolStripMenuItem).DropDown.Visible && e.Item.IsOnDropDown == false )
                {
                    var rect = new Rectangle( 0, 0, e.Item.Width - 1, e.Item.Height - 1 );
                    var rect2 = new Rectangle( 0, 0, e.Item.Width - 1, e.Item.Height - 1 );
                    using ( var b = new SolidBrush( Color.FromArgb( 20, 20, 20 ) ) )
                    {
                        e.Graphics.FillRectangle( b, rect );
                    }

                    using ( var b = new SolidBrush( Color.Black ) )
                    {
                        using var p = new Pen( b );
                        e.Graphics.DrawRectangle( p, rect2 );
                    }

                    e.Item.ForeColor = Color.White;
                }
            }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.ToolStripRenderer.RenderSeparator" /> event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.ToolStripSeparatorRenderEventArgs" />
        /// that contains the event data. </param>
        protected override void OnRenderSeparator( ToolStripSeparatorRenderEventArgs e )
        {
            if ( e is null )
            {
                return;
            }

            base.OnRenderSeparator( e );
            var rect = new Rectangle( 30, 3, e.Item.Width - 30, 1 );
            using var darkLine = new SolidBrush( Color.FromArgb( 30, 30, 30 ) );
            e.Graphics.FillRectangle( darkLine, rect );
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.ToolStripRenderer.RenderItemCheck" /> event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.ToolStripItemImageRenderEventArgs" />
        /// that contains the event data. </param>
        protected override void OnRenderItemCheck( ToolStripItemImageRenderEventArgs e )
        {
            if ( e is null )
            {
                return;
            }

            base.OnRenderItemCheck( e );
            if ( e.Item.Selected )
            {
                var rect = new Rectangle( 4, 2, 18, 18 );
                var rect2 = new Rectangle( 5, 3, 16, 16 );
                using ( var b = new SolidBrush( Color.Black ) )
                {
                    e.Graphics.FillRectangle( b, rect );
                }

                using ( var b2 = new SolidBrush( Color.FromArgb( 220, 220, 220 ) ) )
                {
                    e.Graphics.FillRectangle( b2, rect2 );
                }

                e.Graphics.DrawImage( e.Image, new Point( 5, 3 ) );
            }
            else
            {
                var rect = new Rectangle( 4, 2, 18, 18 );
                var rect2 = new Rectangle( 5, 3, 16, 16 );
                using ( var b = new SolidBrush( Color.White ) )
                {
                    e.Graphics.FillRectangle( b, rect );
                }

                using ( var b2 = new SolidBrush( Color.FromArgb( 255, 80, 90, 90 ) ) )
                {
                    e.Graphics.FillRectangle( b2, rect2 );
                }

                e.Graphics.DrawImage( e.Image, new Point( 5, 3 ) );
            }
        }

        /// <summary> Draws the item background. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.ToolStripRenderEventArgs" /> that
        /// contains the event data. </param>
        protected override void OnRenderImageMargin( ToolStripRenderEventArgs e )
        {
            if ( e is null )
            {
                return;
            }

            base.OnRenderImageMargin( e );
            var rect = new Rectangle( 0, 0, e.ToolStrip.Width, e.ToolStrip.Height );
            var rect3 = new Rectangle( 0, 0, 26, e.AffectedBounds.Height );
            using ( var darkLine = new SolidBrush( Color.FromArgb( 20, 20, 20 ) ) )
            {
                e.Graphics.FillRectangle( darkLine, rect );
                e.Graphics.FillRectangle( darkLine, rect3 );
            }

            using ( var b = new SolidBrush( Color.FromArgb( 20, 20, 20 ) ) )
            {
                using var p = new Pen( b );
                e.Graphics.DrawLine( p, 28, 0, 28, e.AffectedBounds.Height );
            }

            var rect2 = new Rectangle( 0, 0, e.ToolStrip.Width - 1, e.ToolStrip.Height - 1 );
            using ( var b = new SolidBrush( Color.Black ) )
            {
                using var p = new Pen( b );
                e.Graphics.DrawRectangle( p, rect2 );
            }
        }
    }
}
