using System;
using System.Drawing;
using System.Windows.Forms;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core
{

    /// <summary> Form with drop shadow. </summary>
    /// <remarks>
    /// Nicholas Seward, 2007-08-13. http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx
    /// David, 2007-08-13. Converted from C#.
    /// </remarks>
    public class FormBase : Form
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Gets the initializing components sentinel. </summary>
        /// <value> The initializing components sentinel. </value>
        protected bool InitializingComponents { get; set; }

        /// <summary> Specialized default constructor for use only by derived classes. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        protected FormBase() : base()
        {
            this.InitializingComponents = true;
            this.InitializeComponent();
            this.InitializingComponents = false;
        }

        /// <summary> Initializes the component. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            this.AutoScaleDimensions = new SizeF( 7.0f, 17.0f );
            this.AutoScaleMode = AutoScaleMode.Font;
            this.BackColor = SystemColors.Control;
            this.ClientSize = new Size( 331, 341 );
            this.Cursor = Cursors.Default;
            this.Font = new Font( SystemFonts.MessageBoxFont.FontFamily, 9.75f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte( 0 ) );
            this.Icon = My.Resources.Resources.favicon;
            this.Name = "FormBase";
            this.ResumeLayout( false );
        }


        #region " CLASS STYLE "

        /// <summary> The enable drop shadow version. </summary>
        public const int EnableDropShadowVersion = 5;

        /// <summary> Gets the class style. </summary>
        /// <value> The class style. </value>
        protected ClassStyleConstants ClassStyle { get; set; } = ClassStyleConstants.None;

        /// <summary> Adds a drop shadow parameter. </summary>
        /// <remarks>
        /// From Code Project: http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx.
        /// </remarks>
        /// <value> Options that control the create. </value>
        protected override CreateParams CreateParams
        {
            [System.Security.Permissions.SecurityPermission( System.Security.Permissions.SecurityAction.Demand, Flags = System.Security.Permissions.SecurityPermissionFlag.UnmanagedCode )]
            get {
                var cp = base.CreateParams;
                cp.ClassStyle = cp.ClassStyle | ( int ) this.ClassStyle;
                return cp;
            }
        }

        #endregion 

        #endregion 

    }

    /// <summary> Values that represent class style constants. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    [Flags]
    public enum ClassStyleConstants
    {

        /// <summary> . </summary>
        [System.ComponentModel.Description( "Not Specified" )]
        None = 0,

        /// <summary> . </summary>
        [System.ComponentModel.Description( "No close button" )]
        HideCloseButton = 0x200,

        /// <summary> . </summary>
        [System.ComponentModel.Description( "Drop Shadow" )]
        DropShadow = 0x20000 // 131072
    }
}
