using System;
using System.Threading.Tasks;

namespace isr.Core
{
    /// <summary> The windows forms. </summary>
    /// <remarks> David, 2019-01-19. </remarks>
    public sealed class WindowsForms
    {

        #region " CONSTRUCTION "

        /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        private WindowsForms() : base()
        {
        }

        #endregion 

        #region " ABOUT "

        /// <summary> Displays an about. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        public static void DisplayAbout()
        {
            // display the application information
            using var aboutScreen = new About {
                TopMost = true,
                Icon = My.Resources.Resources.favicon
            };
            _ = aboutScreen.ShowDialog( System.Reflection.Assembly.GetExecutingAssembly(), "Integrated Scientific Resources, Inc.", "ABC=123=ZXCA", "My System ID", "Licensed to:" );
        }

        #endregion

        #region " CONFIGURATION EDITOR "

        /// <summary> Opens the settings editor. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="caption">  The caption. </param>
        /// <param name="settings"> Options for controlling the operation. </param>
        public static void EditConfiguration( string caption, System.Configuration.ApplicationSettingsBase settings )
        {
            ConfigurationEditPad.Open( caption, settings );
        }

        #endregion

        #region " INPUT "

        /// <summary> Enter input. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="caption"> The caption. </param>
        /// <returns> A String. </returns>
        public static string EnterInput( string caption )
        {
            return Core.MyTextInputBox.EnterDialog( caption ).DialogValue;
        }

        /// <summary> Enter input. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="caption">                    The caption. </param>
        /// <param name="useSystemPasswordCharacter"> True to use system password character. </param>
        /// <returns> <see cref="MyDialogResult">Dialog result</see>. </returns>
        public static string EnterInput( string caption, bool useSystemPasswordCharacter )
        {
            return Core.MyTextInputBox.EnterDialog( caption, useSystemPasswordCharacter ).DialogValue;
        }

        /// <summary> Enter input. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="caption">               The caption. </param>
        /// <param name="prompt">                The prompt. </param>
        /// <param name="defaultValue">          The default value. </param>
        /// <param name="useSystemPasswordChar"> True to use system password character. </param>
        /// <returns> <see cref="MyDialogResult">Dialog result</see>. </returns>
        public static string EnterInput( string caption, string prompt, string defaultValue, bool useSystemPasswordChar )
        {
            return Core.MyTextInputBox.EnterDialog( caption, prompt, defaultValue, useSystemPasswordChar ).DialogValue;
        }

        #endregion

        #region " MESSAGE BOX "

        /// <summary>
        /// Synchronously Invokes the exception display on the apartment thread to permit using the
        /// clipboard.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="exception"> The exception. </param>
        /// <returns> <see cref="MyDialogResult">Dialog result</see>. </returns>
        public static MyDialogResult ShowDialog( Exception exception )
        {
            return Core.MyMessageBox.ShowDialog( exception );
        }

        /// <summary>
        /// Synchronously Invokes the exception display on the apartment thread to permit using the
        /// clipboard.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="text">    The text. </param>
        /// <param name="caption"> The caption. </param>
        /// <returns> <see cref="MyDialogResult">Dialog result</see>. </returns>
        public static MyDialogResult ShowDialog( string text, string caption )
        {
            return Core.MyMessageBox.ShowDialog( text, caption );
        }

        /// <summary>
        /// Synchronously Invokes the exception display on the apartment thread to permit using the
        /// clipboard.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="exception">     The exception. </param>
        /// <param name="icon">          The icon. </param>
        /// <param name="dialogResults"> The dialog results. </param>
        /// <returns> <see cref="MyDialogResult">Dialog result</see>. </returns>
        public static MyDialogResult ShowDialog( Exception exception, MyMessageBoxIcon icon, MyDialogResult[] dialogResults )
        {
            return Core.MyMessageBox.ShowDialog( exception, icon, dialogResults );
        }

        /// <summary>
        /// Synchronously Invokes the exception display on the apartment thread to permit using the
        /// clipboard.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="text">          The exception. </param>
        /// <param name="caption">       The caption. </param>
        /// <param name="icon">          The icon. </param>
        /// <param name="dialogResults"> The dialog results. </param>
        /// <returns> <see cref="MyDialogResult">Dialog result</see>. </returns>
        public static MyDialogResult ShowDialog( string text, string caption, MyMessageBoxIcon icon, MyDialogResult[] dialogResults )
        {
            return Core.MyMessageBox.ShowDialog( text, caption, icon, dialogResults );
        }

        /// <summary> Displays the message box. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="exception"> The exception. </param>
        /// <param name="icon">      The icon. </param>
        /// <returns> <see cref="MyDialogResult">Dialog result</see>. </returns>
        public static MyDialogResult ShowDialogAbortIgnore( Exception exception, MyMessageBoxIcon icon )
        {
            return Core.MyMessageBox.ShowDialogAbortIgnore( exception, icon );
        }

        /// <summary> Displays the message box. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="exception"> The exception. </param>
        /// <returns> <see cref="MyDialogResult">Dialog result</see>. </returns>
        public static MyDialogResult ShowDialogAbortIgnore( Exception exception )
        {
            return Core.MyMessageBox.ShowDialog( exception, MyMessageBoxIcon.Error, new Core.MyDialogResult[] { Core.MyDialogResult.Abort, Core.MyDialogResult.Ignore } );
        }

        /// <summary> Displays the message box. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="text">    The text. </param>
        /// <param name="caption"> The caption. </param>
        /// <param name="icon">    The icon. </param>
        /// <returns> <see cref="MyDialogResult">Dialog result</see>. </returns>
        public static MyDialogResult ShowDialogAbortIgnore( string text, string caption, MyMessageBoxIcon icon )
        {
            return Core.MyMessageBox.ShowDialog( text, caption, icon, new Core.MyDialogResult[] { Core.MyDialogResult.Abort, Core.MyDialogResult.Ignore } );
        }

        /// <summary> Displays the message box. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="exception"> The exception. </param>
        /// <param name="icon">      The icon. </param>
        /// <returns>
        /// Either <see cref="MyDialogResult.Ignore">ignore</see> or
        /// <see cref="MyDialogResult.Ok">Okay</see>.
        /// </returns>
        public static MyDialogResult ShowDialogIgnoreExit( Exception exception, MyMessageBoxIcon icon )
        {
            return Core.MyMessageBox.ShowDialogIgnoreExit( exception, icon );
        }

        /// <summary> Displays the message box. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="text">    The text. </param>
        /// <param name="caption"> The caption. </param>
        /// <param name="icon">    The icon. </param>
        /// <returns>
        /// Either <see cref="MyDialogResult.Ignore">ignore</see> or
        /// <see cref="MyDialogResult.Ok">Okay</see>.
        /// </returns>
        public static MyDialogResult ShowDialogIgnoreExit( string text, string caption, MyMessageBoxIcon icon )
        {
            return Core.MyMessageBox.ShowDialogIgnoreExit( text, caption, icon );
        }

        /// <summary> Displays the message box. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="exception"> The exception. </param>
        /// <param name="icon">      The icon. </param>
        /// <returns> <see cref="MyDialogResult.Ok">Okay</see>. </returns>
        public static MyDialogResult ShowDialogExit( Exception exception, MyMessageBoxIcon icon )
        {
            return Core.MyMessageBox.ShowDialogExit( exception, icon );
        }

        /// <summary> Displays the message box. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="text">    The text. </param>
        /// <param name="caption"> The caption. </param>
        /// <param name="icon">    The icon. </param>
        /// <returns> <see cref="MyDialogResult.Ok">Okay</see>. </returns>
        public static MyDialogResult ShowDialogExit( string text, string caption, MyMessageBoxIcon icon )
        {
            return Core.MyMessageBox.ShowDialogExit( text, caption, icon );
        }

        /// <summary> Shows the dialog okay. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="text">    The text. </param>
        /// <param name="caption"> The caption. </param>
        /// <param name="icon">    The icon. </param>
        /// <returns> A DialogResult. </returns>
        public static MyDialogResult ShowDialogOkay( string text, string caption, MyMessageBoxIcon icon )
        {
            return Core.MyMessageBox.ShowDialogOkay( text, caption, icon );
        }

        /// <summary> Shows the dialog okay. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="text">    The text. </param>
        /// <param name="caption"> The caption. </param>
        /// <returns> A DialogResult. </returns>
        public static MyDialogResult ShowDialogOkay( string text, string caption )
        {
            return Core.MyMessageBox.ShowDialogOkay( text, caption, MyMessageBoxIcon.Information );
        }

        /// <summary> Shows the okay/cancel dialog. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="text">    The text. </param>
        /// <param name="caption"> The caption. </param>
        /// <param name="icon">    The icon. </param>
        /// <returns> A DialogResult. </returns>
        public static MyDialogResult ShowDialogOkayCancel( string text, string caption, MyMessageBoxIcon icon )
        {
            return Core.MyMessageBox.ShowDialogOkayCancel( text, caption, icon );
        }

        /// <summary> Shows the okay/cancel dialog. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="text">    The text. </param>
        /// <param name="caption"> The caption. </param>
        /// <returns> A DialogResult. </returns>
        public static MyDialogResult ShowDialogOkayCancel( string text, string caption )
        {
            return Core.MyMessageBox.ShowDialogOkayCancel( text, caption );
        }

        /// <summary> Shows the cancel/okay dialog. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="text">    The text. </param>
        /// <param name="caption"> The caption. </param>
        /// <param name="icon">    The icon. </param>
        /// <returns> A DialogResult. </returns>
        public static MyDialogResult ShowDialogCancelOkay( string text, string caption, MyMessageBoxIcon icon )
        {
            return Core.MyMessageBox.ShowDialogCancelOkay( text, caption, icon );
        }

        /// <summary> Shows the cancel/okay dialog. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="text">    The text. </param>
        /// <param name="caption"> The caption. </param>
        /// <returns> A DialogResult. </returns>
        public static MyDialogResult ShowDialogCancelOkay( string text, string caption )
        {
            return Core.MyMessageBox.ShowDialogCancelOkay( text, caption );
        }

        /// <summary> Shows the Yes/No dialog. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="text">    The text. </param>
        /// <param name="caption"> The caption. </param>
        /// <param name="icon">    The icon. </param>
        /// <returns> A DialogResult. </returns>
        public static MyDialogResult ShowDialogYesNo( string text, string caption, MyMessageBoxIcon icon )
        {
            return Core.MyMessageBox.ShowDialogYesNo( text, caption, icon );
        }

        /// <summary> Shows the Yes/No dialog. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="text">    The text. </param>
        /// <param name="caption"> The caption. </param>
        /// <returns> A DialogResult. </returns>
        public static MyDialogResult ShowDialogYesNo( string text, string caption )
        {
            return Core.MyMessageBox.ShowDialogYesNo( text, caption );
        }

        /// <summary> Shows the No/Yes dialog. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="text">    The text. </param>
        /// <param name="caption"> The caption. </param>
        /// <param name="icon">    The icon. </param>
        /// <returns> A DialogResult. </returns>
        public static MyDialogResult ShowDialogNoYes( string text, string caption, MyMessageBoxIcon icon )
        {
            return Core.MyMessageBox.ShowDialogNoYes( text, caption, icon );
        }

        /// <summary> Shows the No/Yes dialog. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="text">    The text. </param>
        /// <param name="caption"> The caption. </param>
        /// <returns> A DialogResult. </returns>
        public static MyDialogResult ShowDialogNoYes( string text, string caption )
        {
            return Core.MyMessageBox.ShowDialogNoYes( text, caption );
        }

        #endregion

        #region " MESAGE FORM "

        /// <summary> Prompt while waiting for action to complete. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="timeout"> The timeout. </param>
        /// <param name="caption"> The caption. </param>
        /// <param name="details"> The details. </param>
        /// <param name="action">  The action. </param>
        public static void ShowMessageForm( TimeSpan timeout, string caption, string details, Action action )
        {
            if ( action is null )
            {
                throw new ArgumentNullException( nameof( action ) );
            }

            using var msg = MessageDialog.Get();
            msg.Show( null, caption, details, timeout );
            action.Invoke();
        }

        /// <summary> Prompt while waiting for a task to complete. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="timeout"> The timeout. </param>
        /// <param name="caption"> The caption. </param>
        /// <param name="details"> The details. </param>
        /// <param name="task">    The task. </param>
        public static void ShowMessageForm( TimeSpan timeout, string caption, string details, Task task )
        {
            if ( task is null )
            {
                throw new ArgumentNullException( nameof( task ) );
            }

            using var msg = MessageDialog.Get();
            msg.Show( null, caption, details );
            _ = task.Wait( timeout );
        }

        #endregion

        #region " SPLASH "

        /// <summary> Gets or sets the splash screen. </summary>
        /// <value> The splash screen. </value>
        private static SplashScreen SplashScreen { get; set; }

        /// <summary> Displays a splash screen. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        public static void ShowSplashScreen()
        {
            SplashScreen = new SplashScreen();
            SplashScreen.Show();
            SplashScreen.TopmostSetter( true );
        }

        /// <summary> Displays a splash message described by value. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="value"> The value. </param>
        public static void DisplaySplashMessage( string value )
        {
            SplashScreen?.DisplayMessage( value );
        }

        /// <summary> Closes splash screen. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        public static void CloseSplashScreen()
        {
            if ( SplashScreen is object )
            {
                SplashScreen.Close();
                SplashScreen.Dispose();
            }

            SplashScreen = null;
        }

        /// <summary> Displays a splash screen. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        public static void DisplaySplashScreen()
        {
            using var splashScreen = new SplashScreen();
            splashScreen.Show();
            splashScreen.TopmostSetter( true );
            Display( splashScreen );
        }

        /// <summary> Displays the given target. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="splashScreen"> The splash screen. </param>
        private static void Display( SplashScreen splashScreen )
        {
            splashScreen.DisplayMessage( $"Splash message {DateTimeOffset.Now:F)}" );
            ApplianceBase.DoEvents();
            ApplianceBase.Delay( TimeSpan.FromMilliseconds( 200d ) );
            ApplianceBase.DoEvents();
        }

        #endregion

        #region " SEND KEYS "

        /// <summary> Send key strokes to the active application. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="keys"> The keys. </param>
        public static void SendKeys( string keys )
        {
            System.Windows.Forms.SendKeys.Send( keys );
        }

        /// <summary>
        /// Send the given keys to the active application and then wait for the message to be processed.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="keys"> The keys. </param>
        public static void SendWait( string keys )
        {
            System.Windows.Forms.SendKeys.SendWait( keys );
        }

        #endregion

    }
}
