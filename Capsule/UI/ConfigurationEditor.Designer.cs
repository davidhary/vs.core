﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace isr.Core
{
    [Microsoft.VisualBasic.CompilerServices.DesignerGenerated()]
    public partial class ConfigurationEditor
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            _PropertyGrid = new System.Windows.Forms.PropertyGrid();
            _MenuStrip = new System.Windows.Forms.MenuStrip();
            _FileMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __ResetMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __ResetMenuItem.Click += new EventHandler(ResetMenuItem_Click);
            __ReadMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __ReadMenuItem.Click += new EventHandler(ReadMenuItem_Click);
            __SaveMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __SaveMenuItem.Click += new EventHandler(SaveMenuItem_Click);
            __SaveExitMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __SaveExitMenuItem.Click += new EventHandler(SaveExitMenuItem_Click);
            _FileSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            __ExitMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __ExitMenuItem.Click += new EventHandler(ExitMenuItem_Click);
            __AcceptButton = new System.Windows.Forms.Button();
            __AcceptButton.Click += new EventHandler(AcceptButton_Click);
            __IgnoreButton = new System.Windows.Forms.Button();
            __IgnoreButton.Click += new EventHandler(IgnoreButton_Click);
            _MenuStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _PropertyGrid
            // 
            _PropertyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            _PropertyGrid.Location = new System.Drawing.Point(0, 24);
            _PropertyGrid.Name = "_PropertyGrid";
            _PropertyGrid.Size = new System.Drawing.Size(427, 383);
            _PropertyGrid.TabIndex = 0;
            // 
            // _MenuStrip
            // 
            _MenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { _FileMenuItem });
            _MenuStrip.Location = new System.Drawing.Point(0, 0);
            _MenuStrip.Name = "_MenuStrip";
            _MenuStrip.Size = new System.Drawing.Size(427, 24);
            _MenuStrip.TabIndex = 1;
            _MenuStrip.Text = "Menu Strip";
            // 
            // _FileMenuItem
            // 
            _FileMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { __ResetMenuItem, __ReadMenuItem, __SaveMenuItem, __SaveExitMenuItem, _FileSeparator1, __ExitMenuItem });
            _FileMenuItem.Name = "_FileMenuItem";
            _FileMenuItem.Size = new System.Drawing.Size(37, 20);
            _FileMenuItem.Text = "&File";
            // 
            // _ResetMenuItem
            // 
            __ResetMenuItem.Name = "__ResetMenuItem";
            __ResetMenuItem.Size = new System.Drawing.Size(180, 22);
            __ResetMenuItem.Text = "R&eset";
            __ResetMenuItem.ToolTipText = "Restores the persisted application settings values to their corresponding default" + " properties";
            // 
            // _ReadMenuItem
            // 
            __ReadMenuItem.Name = "__ReadMenuItem";
            __ReadMenuItem.Size = new System.Drawing.Size(180, 22);
            __ReadMenuItem.Text = "&Read";
            // 
            // _SaveMenuItem
            // 
            __SaveMenuItem.Name = "__SaveMenuItem";
            __SaveMenuItem.Size = new System.Drawing.Size(180, 22);
            __SaveMenuItem.Text = "&Save";
            // 
            // _SaveExitMenuItem
            // 
            __SaveExitMenuItem.Name = "__SaveExitMenuItem";
            __SaveExitMenuItem.Size = new System.Drawing.Size(180, 22);
            __SaveExitMenuItem.Text = "S&ave and Exit";
            // 
            // _FileSeparator1
            // 
            _FileSeparator1.Name = "_FileSeparator1";
            _FileSeparator1.Size = new System.Drawing.Size(177, 6);
            // 
            // _ExitMenuItem
            // 
            __ExitMenuItem.Name = "__ExitMenuItem";
            __ExitMenuItem.Size = new System.Drawing.Size(180, 22);
            __ExitMenuItem.Text = "E&xit";
            // 
            // _AcceptButton
            // 
            __AcceptButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            __AcceptButton.Location = new System.Drawing.Point(305, 79);
            __AcceptButton.Name = "__AcceptButton";
            __AcceptButton.Size = new System.Drawing.Size(75, 34);
            __AcceptButton.TabIndex = 2;
            __AcceptButton.Text = "OK";
            __AcceptButton.UseVisualStyleBackColor = true;
            // 
            // _IgnoreButton
            // 
            __IgnoreButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            __IgnoreButton.Location = new System.Drawing.Point(305, 79);
            __IgnoreButton.Name = "__IgnoreButton";
            __IgnoreButton.Size = new System.Drawing.Size(75, 34);
            __IgnoreButton.TabIndex = 2;
            __IgnoreButton.Text = "OK";
            __IgnoreButton.UseVisualStyleBackColor = true;
            // 
            // ConfigurationEditor
            // 
            AcceptButton = __AcceptButton;
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            CancelButton = __IgnoreButton;
            ClientSize = new System.Drawing.Size(427, 407);
            Controls.Add(_PropertyGrid);
            Controls.Add(_MenuStrip);
            Controls.Add(__AcceptButton);
            Controls.Add(__IgnoreButton);
            MainMenuStrip = _MenuStrip;
            Name = "ConfigurationEditor";
            Text = "Configuration Editor";
            _MenuStrip.ResumeLayout(false);
            _MenuStrip.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        private System.Windows.Forms.PropertyGrid _PropertyGrid;
        private System.Windows.Forms.MenuStrip _MenuStrip;
        private System.Windows.Forms.ToolStripMenuItem _FileMenuItem;
        private System.Windows.Forms.ToolStripMenuItem __ReadMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _ReadMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadMenuItem != null)
                {
                    __ReadMenuItem.Click -= ReadMenuItem_Click;
                }

                __ReadMenuItem = value;
                if (__ReadMenuItem != null)
                {
                    __ReadMenuItem.Click += ReadMenuItem_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripSeparator _FileSeparator1;
        private System.Windows.Forms.ToolStripMenuItem __ExitMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _ExitMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ExitMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ExitMenuItem != null)
                {
                    __ExitMenuItem.Click -= ExitMenuItem_Click;
                }

                __ExitMenuItem = value;
                if (__ExitMenuItem != null)
                {
                    __ExitMenuItem.Click += ExitMenuItem_Click;
                }
            }
        }

        private System.Windows.Forms.Button __AcceptButton;

        private System.Windows.Forms.Button _AcceptButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __AcceptButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__AcceptButton != null)
                {
                    __AcceptButton.Click -= AcceptButton_Click;
                }

                __AcceptButton = value;
                if (__AcceptButton != null)
                {
                    __AcceptButton.Click += AcceptButton_Click;
                }
            }
        }

        private System.Windows.Forms.Button __IgnoreButton;

        private System.Windows.Forms.Button _IgnoreButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __IgnoreButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__IgnoreButton != null)
                {
                    __IgnoreButton.Click -= IgnoreButton_Click;
                }

                __IgnoreButton = value;
                if (__IgnoreButton != null)
                {
                    __IgnoreButton.Click += IgnoreButton_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __SaveMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _SaveMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SaveMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SaveMenuItem != null)
                {
                    __SaveMenuItem.Click -= SaveMenuItem_Click;
                }

                __SaveMenuItem = value;
                if (__SaveMenuItem != null)
                {
                    __SaveMenuItem.Click += SaveMenuItem_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __SaveExitMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _SaveExitMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SaveExitMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SaveExitMenuItem != null)
                {
                    __SaveExitMenuItem.Click -= SaveExitMenuItem_Click;
                }

                __SaveExitMenuItem = value;
                if (__SaveExitMenuItem != null)
                {
                    __SaveExitMenuItem.Click += SaveExitMenuItem_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __ResetMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _ResetMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ResetMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ResetMenuItem != null)
                {
                    __ResetMenuItem.Click -= ResetMenuItem_Click;
                }

                __ResetMenuItem = value;
                if (__ResetMenuItem != null)
                {
                    __ResetMenuItem.Click += ResetMenuItem_Click;
                }
            }
        }
    }
}