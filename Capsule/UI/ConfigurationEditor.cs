using System;
using System.Diagnostics;

using isr.Core.Capsule.ExceptionExtensions;

namespace isr.Core
{

    /// <summary> Editor for configuration. </summary>
    /// <remarks> David, 2016-03-31. </remarks>
    public partial class ConfigurationEditor : FormBase
    {


        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Prevents a default instance of the <see cref="ConfigurationEditor" /> class from being
        /// created.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        private ConfigurationEditor() : base()
        {

            // This method is required by the Windows Form Designer.
            this.InitializeComponent();
            this.__ResetMenuItem.Name = "_ResetMenuItem";
            this.__ReadMenuItem.Name = "_ReadMenuItem";
            this.__SaveMenuItem.Name = "_SaveMenuItem";
            this.__SaveExitMenuItem.Name = "_SaveExitMenuItem";
            this.__ExitMenuItem.Name = "_ExitMenuItem";
            this.__AcceptButton.Name = "_AcceptButton";
            this.__IgnoreButton.Name = "_IgnoreButton";
        }

        /// <summary>
        /// Gets the locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        /// <value> The sync locker. </value>
        private static object SyncLocker { get; set; } = new object();

        /// <summary> Gets the instance. </summary>
        /// <value> The instance. </value>
        private static ConfigurationEditor Instance { get; set; }

        /// <summary> Instantiates the class. </summary>
        /// <remarks> Use this property to instantiate a single instance of this class. This class uses
        /// lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
        /// <returns> A new or existing instance of the class. </returns>
        public static ConfigurationEditor Get()
        {
            if ( !Instantiated )
            {
                lock ( SyncLocker )
                {
                    Instance = new ConfigurationEditor();
                }
            }

            return Instance;
        }

        /// <summary> Returns true if an instance of the class was created and not disposed. </summary>
        /// <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
        internal static bool Instantiated
        {
            get {
                lock ( SyncLocker )
                {
                    return Instance is object && !Instance.IsDisposed;
                }
            }
        }

        /// <summary> Dispose instance. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        public static void DisposeInstance()
        {
            lock ( SyncLocker )
            {
                if ( Instance is object && !Instance.IsDisposed )
                {
                    Instance.Dispose();
                    Instance = null;
                }
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion 


        #region " SHOW "

        /// <summary> Hides the dialog. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="owner"> The owner. </param>
        /// <returns> A DialogResult. </returns>
        [Obsolete( "Illegal Call" )]
        public new System.Windows.Forms.DialogResult ShowDialog( System.Windows.Forms.IWin32Window owner )
        {
            this.Text = "Illegal Call";
            return base.ShowDialog( owner );
        }

        /// <summary> Hides the dialog. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <returns> A System.Windows.Forms.DialogResult. </returns>
        [Obsolete( "Illegal Call" )]
        public new System.Windows.Forms.DialogResult ShowDialog()
        {
            this.Text = "Illegal Call";
            return base.ShowDialog();
        }

        /// <summary> Shows the dialog. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="settings"> Options for controlling the operation. </param>
        /// <returns> A DialogResult. </returns>
        public System.Windows.Forms.DialogResult ShowDialog( System.Configuration.ApplicationSettingsBase settings )
        {
            return this.ShowDialog( null, settings );
        }

        /// <summary> Shows the dialog. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="mdiForm">  The MDI form. </param>
        /// <param name="settings"> Options for controlling the operation. </param>
        /// <returns> A DialogResult. </returns>
        public System.Windows.Forms.DialogResult ShowDialog( System.Windows.Forms.Form mdiForm, System.Configuration.ApplicationSettingsBase settings )
        {
            this.Show( mdiForm, settings );
            while ( this.Visible )
            {
                System.Windows.Forms.Application.DoEvents();
            }

            return this.DialogResult;
        }

        /// <summary> Displays this dialog with title 'illegal call'. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        [Obsolete( "Illegal Call" )]
        public new void Show()
        {
            this.Text = "Illegal Call";
            base.Show();
        }

        /// <summary> Shows this form. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="settings"> Options for controlling the operation. </param>
        public void Show( System.Configuration.ApplicationSettingsBase settings )
        {
            this.Show( null, settings );
        }

        /// <summary> Shows this form. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="mdiForm">  The MDI form. </param>
        /// <param name="settings"> Options for controlling the operation. </param>
        public void Show( System.Windows.Forms.Form mdiForm, System.Configuration.ApplicationSettingsBase settings )
        {
            if ( mdiForm is object && mdiForm.IsMdiContainer )
            {
                this.MdiParent = mdiForm;
                mdiForm.Show();
            }

            this._Settings = settings;
            base.Show();
        }

        #endregion 


        #region " FORM EVENTS "

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Form.Shown" /> event. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnShown( EventArgs e )
        {
            try
            {
                this._PropertyGrid.SelectedObject = this._Settings;
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
            }
            finally
            {
                base.OnShown( e );
            }
        }

        #endregion 


        #region " HANDLERS "

        /// <summary> Cancel button click. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void IgnoreButton_Click( object sender, EventArgs e )
        {
            this.Close();
        }

        /// <summary> Accept button click. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void AcceptButton_Click( object sender, EventArgs e )
        {
            this.SaveExit( sender, e );
        }

        /// <summary> Options for controlling the operation. </summary>
        private System.Configuration.ApplicationSettingsBase _Settings;

        /// <summary> Resets the menu item click. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ResetMenuItem_Click( object sender, EventArgs e )
        {
            try
            {
                // Me._PropertyGrid.SelectedObject = Nothing
                System.Windows.Forms.Application.DoEvents();
                if ( System.Windows.Forms.MessageBox.Show( "Restoring the persisted application settings values to their corresponding default properties; Are you sure?", "Reset Setting; Are you sure?!", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Question, System.Windows.Forms.MessageBoxDefaultButton.Button2, System.Windows.Forms.MessageBoxOptions.DefaultDesktopOnly ) == System.Windows.Forms.DialogResult.Yes )
                {
                    this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
                    this._Settings.Reset();
                    this._PropertyGrid.SelectedObject = this._Settings;
                }
            }
            catch ( Exception ex )
            {
                _ = System.Windows.Forms.MessageBox.Show( ex.ToFullBlownString(), "Exception Resetting Settings", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error, System.Windows.Forms.MessageBoxDefaultButton.Button1, System.Windows.Forms.MessageBoxOptions.DefaultDesktopOnly );
            }
            finally
            {
                this.Cursor = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary> Reads menu item click. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ReadMenuItem_Click( object sender, EventArgs e )
        {
            try
            {
                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
                this._PropertyGrid.SelectedObject = null;
                System.Windows.Forms.Application.DoEvents();
                System.Threading.Thread.Sleep( 100 );
                this._PropertyGrid.SelectedObject = this._Settings;
            }
            catch ( Exception ex )
            {
                _ = System.Windows.Forms.MessageBox.Show( ex.ToFullBlownString(), "Exception reading Settings", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error, System.Windows.Forms.MessageBoxDefaultButton.Button1, System.Windows.Forms.MessageBoxOptions.DefaultDesktopOnly );
            }
            finally
            {
                this.Cursor = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary> Saves a menu item click. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SaveMenuItem_Click( object sender, EventArgs e )
        {
            try
            {
                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
                this._Settings.Save();
                System.Windows.Forms.Application.DoEvents();
                System.Threading.Thread.Sleep( 100 );
            }
            catch ( Exception ex )
            {
                _ = System.Windows.Forms.MessageBox.Show( ex.ToFullBlownString(), "Exception saving Settings", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error, System.Windows.Forms.MessageBoxDefaultButton.Button1, System.Windows.Forms.MessageBoxOptions.DefaultDesktopOnly );
            }
            finally
            {
                this.Cursor = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary> Saves an exit. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SaveExit( object sender, EventArgs e )
        {
            try
            {
                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
                this._Settings.Save();
                System.Windows.Forms.Application.DoEvents();
                System.Threading.Thread.Sleep( 100 );
                this.Close();
            }
            catch ( Exception ex )
            {
                _ = System.Windows.Forms.MessageBox.Show( ex.ToFullBlownString(), "Exception saving Settings", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error, System.Windows.Forms.MessageBoxDefaultButton.Button1, System.Windows.Forms.MessageBoxOptions.DefaultDesktopOnly );
            }
            finally
            {
                this.Cursor = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary> Saves an exit menu item click. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void SaveExitMenuItem_Click( object sender, EventArgs e )
        {
            this.SaveExit( sender, e );
        }

        /// <summary> Exit menu item click. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ExitMenuItem_Click( object sender, EventArgs e )
        {
            this.Close();
        }

        #endregion 

    }

    /// <summary> A configuration edit pad. </summary>
    /// <remarks> David, 2019-01-19. </remarks>
    public sealed class ConfigurationEditPad
    {

        #region " CONSTRUCTION "

        /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        private ConfigurationEditPad() : base()
        {
        }

        #endregion 

        #region " CONFIGURATION EDITOR "

        /// <summary> Opens the settings editor. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="caption">  The caption. </param>
        /// <param name="settings"> Options for controlling the operation. </param>
        public static void Open( string caption, System.Configuration.ApplicationSettingsBase settings )
        {
            using var f = ConfigurationEditor.Get();
            f.Text = caption;
            _ = f.ShowDialog( settings );
        }

        #endregion 

    }
}
