using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core
{
    [DesignerGenerated()]
    public partial class CompoundInputBox
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            _TextBox = new System.Windows.Forms.TextBox();
            _TextBox.Validated += new EventHandler(TextBox_Validated);
            __CancelButton = new System.Windows.Forms.Button();
            __CancelButton.Click += new EventHandler(CancelButton_Click);
            __AcceptButton = new System.Windows.Forms.Button();
            __AcceptButton.Click += new EventHandler(AcceptButton_Click);
            TextBoxLabel = new System.Windows.Forms.Label();
            _Layout = new System.Windows.Forms.TableLayoutPanel();
            _ButtonLayout = new System.Windows.Forms.TableLayoutPanel();
            NumericUpDownLabel = new System.Windows.Forms.Label();
            DropDownBoxLabel = new System.Windows.Forms.Label();
            NumericUpDown = new System.Windows.Forms.NumericUpDown();
            DropDownBox = new System.Windows.Forms.ComboBox();
            _Layout.SuspendLayout();
            _ButtonLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)NumericUpDown).BeginInit();
            SuspendLayout();
            // 
            // TextBox
            // 
            _TextBox.Dock = System.Windows.Forms.DockStyle.Top;
            _TextBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _TextBox.Location = new System.Drawing.Point(9, 35);
            _TextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            _TextBox.Name = "_TextBox";
            _TextBox.Size = new System.Drawing.Size(252, 25);
            _TextBox.TabIndex = 28;
            _TextBox.Text = "TextBox1";
            // 
            // _cancelButton
            // 
            __CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            __CancelButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            __CancelButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            __CancelButton.Location = new System.Drawing.Point(36, 4);
            __CancelButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            __CancelButton.Name = "__CancelButton";
            __CancelButton.Size = new System.Drawing.Size(87, 30);
            __CancelButton.TabIndex = 27;
            __CancelButton.Text = "&Cancel";
            // 
            // _acceptButton
            // 
            __AcceptButton.Enabled = false;
            __AcceptButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            __AcceptButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            __AcceptButton.Location = new System.Drawing.Point(129, 4);
            __AcceptButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            __AcceptButton.Name = "__AcceptButton";
            __AcceptButton.Size = new System.Drawing.Size(87, 30);
            __AcceptButton.TabIndex = 26;
            __AcceptButton.Text = "&OK";
            // 
            // TextBoxLabel
            // 
            TextBoxLabel.AutoSize = true;
            TextBoxLabel.BackColor = System.Drawing.SystemColors.Control;
            TextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default;
            TextBoxLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            TextBoxLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            TextBoxLabel.ForeColor = System.Drawing.SystemColors.WindowText;
            TextBoxLabel.Location = new System.Drawing.Point(9, 14);
            TextBoxLabel.Name = "TextBoxLabel";
            TextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            TextBoxLabel.Size = new System.Drawing.Size(252, 17);
            TextBoxLabel.TabIndex = 25;
            TextBoxLabel.Text = "Enter text: ";
            TextBoxLabel.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // _Layout
            // 
            _Layout.ColumnCount = 3;
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 6.0f));
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0f));
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 6.0f));
            _Layout.Controls.Add(DropDownBoxLabel, 1, 7);
            _Layout.Controls.Add(_ButtonLayout, 1, 10);
            _Layout.Controls.Add(_TextBox, 1, 2);
            _Layout.Controls.Add(TextBoxLabel, 1, 1);
            _Layout.Controls.Add(NumericUpDownLabel, 1, 4);
            _Layout.Controls.Add(NumericUpDown, 1, 5);
            _Layout.Controls.Add(DropDownBox, 1, 8);
            _Layout.Dock = System.Windows.Forms.DockStyle.Fill;
            _Layout.Location = new System.Drawing.Point(0, 0);
            _Layout.Name = "_Layout";
            _Layout.RowCount = 12;
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0f));
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0f));
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0f));
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0f));
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 44.0f));
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0f));
            _Layout.Size = new System.Drawing.Size(270, 261);
            _Layout.TabIndex = 29;
            // 
            // _ButtonLayout
            // 
            _ButtonLayout.ColumnCount = 4;
            _ButtonLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _ButtonLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _ButtonLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _ButtonLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _ButtonLayout.Controls.Add(__AcceptButton, 2, 1);
            _ButtonLayout.Controls.Add(__CancelButton, 1, 1);
            _ButtonLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            _ButtonLayout.Location = new System.Drawing.Point(9, 205);
            _ButtonLayout.Name = "_ButtonLayout";
            _ButtonLayout.RowCount = 3;
            _ButtonLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _ButtonLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _ButtonLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _ButtonLayout.Size = new System.Drawing.Size(252, 38);
            _ButtonLayout.TabIndex = 30;
            // 
            // NumericUpDownLabel
            // 
            NumericUpDownLabel.AutoSize = true;
            NumericUpDownLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            NumericUpDownLabel.Location = new System.Drawing.Point(9, 78);
            NumericUpDownLabel.Name = "NumericUpDownLabel";
            NumericUpDownLabel.Size = new System.Drawing.Size(252, 17);
            NumericUpDownLabel.TabIndex = 31;
            NumericUpDownLabel.Text = "Enter a number:";
            // 
            // DropDownBoxLabel
            // 
            DropDownBoxLabel.AutoSize = true;
            DropDownBoxLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            DropDownBoxLabel.Location = new System.Drawing.Point(9, 140);
            DropDownBoxLabel.Name = "DropDownBoxLabel";
            DropDownBoxLabel.Size = new System.Drawing.Size(252, 17);
            DropDownBoxLabel.TabIndex = 30;
            DropDownBoxLabel.Text = "Select item:";
            // 
            // NumericUpDown
            // 
            NumericUpDown.Dock = System.Windows.Forms.DockStyle.Top;
            NumericUpDown.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            NumericUpDown.Location = new System.Drawing.Point(9, 98);
            NumericUpDown.Name = "NumericUpDown";
            NumericUpDown.Size = new System.Drawing.Size(252, 25);
            NumericUpDown.TabIndex = 32;
            // 
            // DropDownBox
            // 
            DropDownBox.Dock = System.Windows.Forms.DockStyle.Top;
            DropDownBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            DropDownBox.FormattingEnabled = true;
            DropDownBox.Location = new System.Drawing.Point(9, 160);
            DropDownBox.Name = "DropDownBox";
            DropDownBox.Size = new System.Drawing.Size(252, 25);
            DropDownBox.TabIndex = 33;
            // 
            // CompoundInputBox
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            ClientSize = new System.Drawing.Size(270, 261);
            ControlBox = false;
            Controls.Add(_Layout);
            Name = "CompoundInputBox";
            Text = "Compound Input Box";
            _Layout.ResumeLayout(false);
            _Layout.PerformLayout();
            _ButtonLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)NumericUpDown).EndInit();
            ResumeLayout(false);
        }

        private System.Windows.Forms.TextBox _TextBox;

        private System.Windows.Forms.TextBox TextBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _TextBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_TextBox != null)
                {
                    _TextBox.Validated -= TextBox_Validated;
                }

                _TextBox = value;
                if (_TextBox != null)
                {
                    _TextBox.Validated += TextBox_Validated;
                }
            }
        }

        private System.Windows.Forms.Button __CancelButton;

        private System.Windows.Forms.Button _CancelButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __CancelButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__CancelButton != null)
                {
                    __CancelButton.Click -= CancelButton_Click;
                }

                __CancelButton = value;
                if (__CancelButton != null)
                {
                    __CancelButton.Click += CancelButton_Click;
                }
            }
        }

        private System.Windows.Forms.Button __AcceptButton;

        private System.Windows.Forms.Button _AcceptButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __AcceptButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__AcceptButton != null)
                {
                    __AcceptButton.Click -= AcceptButton_Click;
                }

                __AcceptButton = value;
                if (__AcceptButton != null)
                {
                    __AcceptButton.Click += AcceptButton_Click;
                }
            }
        }

        private System.Windows.Forms.TableLayoutPanel _ButtonLayout;

        /// <summary>   The text box label. </summary>
        public System.Windows.Forms.Label TextBoxLabel;

        /// <summary>   The drop down box label. </summary>
        public System.Windows.Forms.Label DropDownBoxLabel;

        /// <summary>   The numeric up down label. </summary>
        public System.Windows.Forms.Label NumericUpDownLabel;

        /// <summary>   The numeric up down control. </summary>
        public System.Windows.Forms.NumericUpDown NumericUpDown;

        /// <summary>   The drop down box. </summary>
        public System.Windows.Forms.ComboBox DropDownBox;
        private System.Windows.Forms.TableLayoutPanel _Layout;
    }
}
