﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core
{
    [DesignerGenerated()]
    public partial class SplashScreen
    {

        /// <summary> The components. </summary>
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(SplashScreen));
            _RightPanel = new System.Windows.Forms.Panel();
            _ProductNameLabel = new System.Windows.Forms.Label();
            _ProductTitleLabel = new System.Windows.Forms.Label();
            _ProductVersionLabel = new System.Windows.Forms.Label();
            _LicenseeLabel = new System.Windows.Forms.Label();
            _PlatformTitleLabel = new System.Windows.Forms.Label();
            _ProductFamilyLabel = new System.Windows.Forms.Label();
            _CompanyNameLabel = new System.Windows.Forms.Label();
            _LeftPanel = new System.Windows.Forms.Panel();
            _DisksPictureBox = new System.Windows.Forms.PictureBox();
            _CompanyLogoPictureBox = new System.Windows.Forms.PictureBox();
            __CancelButton = new System.Windows.Forms.Button();
            __CancelButton.Click += new EventHandler(CancelButtonClick);
            _StatusLabel = new System.Windows.Forms.Label();
            _CopyrightLabel = new System.Windows.Forms.Label();
            _CopyrightWarningLabel = new System.Windows.Forms.Label();
            _RightPanel.SuspendLayout();
            _LeftPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_DisksPictureBox).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_CompanyLogoPictureBox).BeginInit();
            SuspendLayout();
            // 
            // _RightPanel
            // 
            _RightPanel.Controls.Add(_ProductNameLabel);
            _RightPanel.Controls.Add(_ProductTitleLabel);
            _RightPanel.Controls.Add(_ProductVersionLabel);
            _RightPanel.Controls.Add(_LicenseeLabel);
            _RightPanel.Controls.Add(_PlatformTitleLabel);
            _RightPanel.Controls.Add(_ProductFamilyLabel);
            _RightPanel.Controls.Add(_CompanyNameLabel);
            _RightPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            _RightPanel.Location = new System.Drawing.Point(95, 0);
            _RightPanel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            _RightPanel.Name = "_RightPanel";
            _RightPanel.Size = new System.Drawing.Size(426, 262);
            _RightPanel.TabIndex = 33;
            // 
            // _ProductNameLabel
            // 
            _ProductNameLabel.BackColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(232)), Conversions.ToInteger(Conversions.ToByte(243)), Conversions.ToInteger(Conversions.ToByte(255)));
            _ProductNameLabel.Cursor = System.Windows.Forms.Cursors.Default;
            _ProductNameLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            _ProductNameLabel.Font = new System.Drawing.Font("Segoe UI", 18.0f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _ProductNameLabel.ForeColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(214)), Conversions.ToInteger(Conversions.ToByte(159)), Conversions.ToInteger(Conversions.ToByte(66)));
            _ProductNameLabel.Location = new System.Drawing.Point(0, 90);
            _ProductNameLabel.Name = "_ProductNameLabel";
            _ProductNameLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _ProductNameLabel.Size = new System.Drawing.Size(426, 43);
            _ProductNameLabel.TabIndex = 13;
            _ProductNameLabel.Tag = "Product";
            _ProductNameLabel.Text = "Product";
            _ProductNameLabel.UseMnemonic = false;
            // 
            // _ProductTitleLabel
            // 
            _ProductTitleLabel.BackColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(232)), Conversions.ToInteger(Conversions.ToByte(243)), Conversions.ToInteger(Conversions.ToByte(255)));
            _ProductTitleLabel.Cursor = System.Windows.Forms.Cursors.Default;
            _ProductTitleLabel.Dock = System.Windows.Forms.DockStyle.Top;
            _ProductTitleLabel.Font = new System.Drawing.Font("Segoe UI", 14.25f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _ProductTitleLabel.ForeColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(69)), Conversions.ToInteger(Conversions.ToByte(138)), Conversions.ToInteger(Conversions.ToByte(214)));
            _ProductTitleLabel.Location = new System.Drawing.Point(0, 59);
            _ProductTitleLabel.Name = "_ProductTitleLabel";
            _ProductTitleLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _ProductTitleLabel.Size = new System.Drawing.Size(426, 31);
            _ProductTitleLabel.TabIndex = 26;
            _ProductTitleLabel.Text = "Title";
            _ProductTitleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            _ProductTitleLabel.UseMnemonic = false;
            // 
            // _ProductVersionLabel
            // 
            _ProductVersionLabel.BackColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(232)), Conversions.ToInteger(Conversions.ToByte(243)), Conversions.ToInteger(Conversions.ToByte(255)));
            _ProductVersionLabel.Cursor = System.Windows.Forms.Cursors.Default;
            _ProductVersionLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            _ProductVersionLabel.Font = new System.Drawing.Font("Segoe UI", 12.0f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _ProductVersionLabel.ForeColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(148)));
            _ProductVersionLabel.Location = new System.Drawing.Point(0, 133);
            _ProductVersionLabel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            _ProductVersionLabel.Name = "_ProductVersionLabel";
            _ProductVersionLabel.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            _ProductVersionLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _ProductVersionLabel.Size = new System.Drawing.Size(426, 39);
            _ProductVersionLabel.TabIndex = 16;
            _ProductVersionLabel.Tag = "Version";
            _ProductVersionLabel.Text = "Version {0}.{1:00}.{2}";
            _ProductVersionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            _ProductVersionLabel.UseMnemonic = false;
            // 
            // _LicenseeLabel
            // 
            _LicenseeLabel.BackColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(232)), Conversions.ToInteger(Conversions.ToByte(243)), Conversions.ToInteger(Conversions.ToByte(255)));
            _LicenseeLabel.Cursor = System.Windows.Forms.Cursors.Default;
            _LicenseeLabel.Dock = System.Windows.Forms.DockStyle.Top;
            _LicenseeLabel.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _LicenseeLabel.ForeColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(69)), Conversions.ToInteger(Conversions.ToByte(138)), Conversions.ToInteger(Conversions.ToByte(214)));
            _LicenseeLabel.Location = new System.Drawing.Point(0, 17);
            _LicenseeLabel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            _LicenseeLabel.Name = "_LicenseeLabel";
            _LicenseeLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _LicenseeLabel.Size = new System.Drawing.Size(426, 42);
            _LicenseeLabel.TabIndex = 25;
            _LicenseeLabel.Text = "Licensee";
            _LicenseeLabel.UseMnemonic = false;
            // 
            // _PlatformTitleLabel
            // 
            _PlatformTitleLabel.BackColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(232)), Conversions.ToInteger(Conversions.ToByte(243)), Conversions.ToInteger(Conversions.ToByte(255)));
            _PlatformTitleLabel.Cursor = System.Windows.Forms.Cursors.Default;
            _PlatformTitleLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            _PlatformTitleLabel.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _PlatformTitleLabel.ForeColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(148)));
            _PlatformTitleLabel.Location = new System.Drawing.Point(0, 172);
            _PlatformTitleLabel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            _PlatformTitleLabel.Name = "_PlatformTitleLabel";
            _PlatformTitleLabel.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            _PlatformTitleLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _PlatformTitleLabel.Size = new System.Drawing.Size(426, 27);
            _PlatformTitleLabel.TabIndex = 20;
            _PlatformTitleLabel.Tag = "Platform";
            _PlatformTitleLabel.Text = "Platform";
            _PlatformTitleLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            _PlatformTitleLabel.UseMnemonic = false;
            // 
            // _ProductFamilyLabel
            // 
            _ProductFamilyLabel.BackColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(232)), Conversions.ToInteger(Conversions.ToByte(243)), Conversions.ToInteger(Conversions.ToByte(255)));
            _ProductFamilyLabel.Cursor = System.Windows.Forms.Cursors.Default;
            _ProductFamilyLabel.Dock = System.Windows.Forms.DockStyle.Top;
            _ProductFamilyLabel.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _ProductFamilyLabel.ForeColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(69)), Conversions.ToInteger(Conversions.ToByte(138)), Conversions.ToInteger(Conversions.ToByte(214)));
            _ProductFamilyLabel.Location = new System.Drawing.Point(0, 0);
            _ProductFamilyLabel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            _ProductFamilyLabel.Name = "_ProductFamilyLabel";
            _ProductFamilyLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _ProductFamilyLabel.Size = new System.Drawing.Size(426, 17);
            _ProductFamilyLabel.TabIndex = 20;
            _ProductFamilyLabel.Text = "An Integrated Scientific Resources Product";
            _ProductFamilyLabel.UseMnemonic = false;
            // 
            // _CompanyNameLabel
            // 
            _CompanyNameLabel.BackColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(232)), Conversions.ToInteger(Conversions.ToByte(243)), Conversions.ToInteger(Conversions.ToByte(255)));
            _CompanyNameLabel.Cursor = System.Windows.Forms.Cursors.Default;
            _CompanyNameLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            _CompanyNameLabel.Font = new System.Drawing.Font("Segoe UI", 11.25f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _CompanyNameLabel.ForeColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(214)), Conversions.ToInteger(Conversions.ToByte(159)), Conversions.ToInteger(Conversions.ToByte(66)));
            _CompanyNameLabel.Location = new System.Drawing.Point(0, 199);
            _CompanyNameLabel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            _CompanyNameLabel.Name = "_CompanyNameLabel";
            _CompanyNameLabel.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            _CompanyNameLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _CompanyNameLabel.Size = new System.Drawing.Size(426, 63);
            _CompanyNameLabel.TabIndex = 28;
            _CompanyNameLabel.Tag = "Company";
            _CompanyNameLabel.Text = "Company";
            _CompanyNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            _CompanyNameLabel.UseMnemonic = false;
            // 
            // _LeftPanel
            // 
            _LeftPanel.Controls.Add(_DisksPictureBox);
            _LeftPanel.Controls.Add(_CompanyLogoPictureBox);
            _LeftPanel.Controls.Add(__CancelButton);
            _LeftPanel.Dock = System.Windows.Forms.DockStyle.Left;
            _LeftPanel.Location = new System.Drawing.Point(0, 0);
            _LeftPanel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            _LeftPanel.Name = "_LeftPanel";
            _LeftPanel.Size = new System.Drawing.Size(95, 262);
            _LeftPanel.TabIndex = 34;
            // 
            // _DisksPictureBox
            // 
            _DisksPictureBox.BackColor = System.Drawing.Color.Transparent;
            _DisksPictureBox.Cursor = System.Windows.Forms.Cursors.Default;
            _DisksPictureBox.Dock = System.Windows.Forms.DockStyle.Top;
            _DisksPictureBox.Image = (System.Drawing.Image)resources.GetObject("_DisksPictureBox.Image");
            _DisksPictureBox.Location = new System.Drawing.Point(0, 90);
            _DisksPictureBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            _DisksPictureBox.Name = "_DisksPictureBox";
            _DisksPictureBox.Size = new System.Drawing.Size(95, 112);
            _DisksPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            _DisksPictureBox.TabIndex = 23;
            _DisksPictureBox.TabStop = false;
            // 
            // _CompanyLogoPictureBox
            // 
            _CompanyLogoPictureBox.BackColor = System.Drawing.Color.Transparent;
            _CompanyLogoPictureBox.Cursor = System.Windows.Forms.Cursors.Default;
            _CompanyLogoPictureBox.Dock = System.Windows.Forms.DockStyle.Top;
            _CompanyLogoPictureBox.Image = (System.Drawing.Image)resources.GetObject("_CompanyLogoPictureBox.Image");
            _CompanyLogoPictureBox.Location = new System.Drawing.Point(0, 0);
            _CompanyLogoPictureBox.Margin = new System.Windows.Forms.Padding(3, 13, 3, 4);
            _CompanyLogoPictureBox.Name = "_CompanyLogoPictureBox";
            _CompanyLogoPictureBox.Padding = new System.Windows.Forms.Padding(0, 4, 0, 0);
            _CompanyLogoPictureBox.Size = new System.Drawing.Size(95, 90);
            _CompanyLogoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            _CompanyLogoPictureBox.TabIndex = 23;
            _CompanyLogoPictureBox.TabStop = false;
            // 
            // _CancelButton
            // 
            __CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            __CancelButton.Location = new System.Drawing.Point(4, 130);
            __CancelButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            __CancelButton.Name = "__CancelButton";
            __CancelButton.Size = new System.Drawing.Size(87, 30);
            __CancelButton.TabIndex = 0;
            __CancelButton.Text = "Cancel";
            __CancelButton.UseVisualStyleBackColor = true;
            // 
            // _StatusLabel
            // 
            _StatusLabel.BackColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(232)), Conversions.ToInteger(Conversions.ToByte(243)), Conversions.ToInteger(Conversions.ToByte(255)));
            _StatusLabel.Cursor = System.Windows.Forms.Cursors.Default;
            _StatusLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            _StatusLabel.ForeColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(148)));
            _StatusLabel.Location = new System.Drawing.Point(0, 262);
            _StatusLabel.Name = "_StatusLabel";
            _StatusLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _StatusLabel.Size = new System.Drawing.Size(521, 35);
            _StatusLabel.TabIndex = 32;
            _StatusLabel.Tag = "status";
            _StatusLabel.Text = "status";
            _StatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            _StatusLabel.UseMnemonic = false;
            // 
            // _CopyrightLabel
            // 
            _CopyrightLabel.BackColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(232)), Conversions.ToInteger(Conversions.ToByte(243)), Conversions.ToInteger(Conversions.ToByte(255)));
            _CopyrightLabel.Cursor = System.Windows.Forms.Cursors.Default;
            _CopyrightLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            _CopyrightLabel.ForeColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(148)));
            _CopyrightLabel.Location = new System.Drawing.Point(0, 297);
            _CopyrightLabel.Name = "_CopyrightLabel";
            _CopyrightLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _CopyrightLabel.Size = new System.Drawing.Size(521, 35);
            _CopyrightLabel.TabIndex = 31;
            _CopyrightLabel.Tag = "Copyright";
            _CopyrightLabel.Text = "Copyright";
            _CopyrightLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            _CopyrightLabel.UseMnemonic = false;
            // 
            // _CopyrightWarningLabel
            // 
            _CopyrightWarningLabel.BackColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(232)), Conversions.ToInteger(Conversions.ToByte(243)), Conversions.ToInteger(Conversions.ToByte(255)));
            _CopyrightWarningLabel.Cursor = System.Windows.Forms.Cursors.Default;
            _CopyrightWarningLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            _CopyrightWarningLabel.ForeColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(148)));
            _CopyrightWarningLabel.Location = new System.Drawing.Point(0, 332);
            _CopyrightWarningLabel.Name = "_CopyrightWarningLabel";
            _CopyrightWarningLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _CopyrightWarningLabel.Size = new System.Drawing.Size(521, 35);
            _CopyrightWarningLabel.TabIndex = 30;
            _CopyrightWarningLabel.Tag = "trademarks";
            _CopyrightWarningLabel.Text = "trademarks";
            _CopyrightWarningLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            _CopyrightWarningLabel.UseMnemonic = false;
            // 
            // SplashScreen
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            BackColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(232)), Conversions.ToInteger(Conversions.ToByte(243)), Conversions.ToInteger(Conversions.ToByte(255)));
            CancelButton = __CancelButton;
            ClientSize = new System.Drawing.Size(521, 367);
            ControlBox = false;
            Controls.Add(_RightPanel);
            Controls.Add(_LeftPanel);
            Controls.Add(_StatusLabel);
            Controls.Add(_CopyrightLabel);
            Controls.Add(_CopyrightWarningLabel);
            FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "SplashScreen";
            ShowIcon = false;
            ShowInTaskbar = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            _RightPanel.ResumeLayout(false);
            _LeftPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)_DisksPictureBox).EndInit();
            ((System.ComponentModel.ISupportInitialize)_CompanyLogoPictureBox).EndInit();
            Shown += new EventHandler(Form_Shown);
            ResumeLayout(false);
        }

        /// <summary> The with events. </summary>
        private System.Windows.Forms.Panel _RightPanel;

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.Label _ProductNameLabel;

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.Label _ProductTitleLabel;

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.Label _ProductVersionLabel;

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.Label _LicenseeLabel;

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.Label _PlatformTitleLabel;

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.Label _ProductFamilyLabel;

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.Label _CompanyNameLabel;

        /// <summary> The with events. </summary>
        private System.Windows.Forms.Panel _LeftPanel;

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.PictureBox _DisksPictureBox;

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.PictureBox _CompanyLogoPictureBox;

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.Label _StatusLabel;

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.Label _CopyrightLabel;

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.Label _CopyrightWarningLabel;

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.Button __CancelButton;

        private System.Windows.Forms.Button _CancelButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __CancelButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__CancelButton != null)
                {
                    __CancelButton.Click -= CancelButtonClick;
                }

                __CancelButton = value;
                if (__CancelButton != null)
                {
                    __CancelButton.Click += CancelButtonClick;
                }
            }
        }
    }
}