using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace isr.Core
{

    /// <summary> Form for displaying a message. </summary>
    /// <remarks> David, 2014-09-17. </remarks>
    public sealed partial class MessageDialog : FormBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Prevents a default instance of the <see cref="MessageDialog" /> class from being created.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        private MessageDialog() : base()
        {

            // This method is required by the Windows Form Designer.
            this.InitializeComponent();
        }

        /// <summary>
        /// Gets the locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        /// <value> The sync locker. </value>
        private static object SyncLocker { get; set; } = new object();

        /// <summary> Gets the instance. </summary>
        /// <value> The instance. </value>
        private static MessageDialog Instance { get; set; }

        /// <summary> Instantiates the class. </summary>
        /// <remarks> Use this property to instantiate a single instance of this class. This class uses
        /// lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
        /// <returns> A new or existing instance of the class. </returns>
        public static MessageDialog Get()
        {
            if ( !Instantiated )
            {
                lock ( SyncLocker )
                {
                    Instance = new MessageDialog();
                }
            }

            return Instance;
        }

        /// <summary> Returns true if an instance of the class was created and not disposed. </summary>
        /// <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
        internal static bool Instantiated
        {
            get {
                lock ( SyncLocker )
                {
                    return Instance is object && !Instance.IsDisposed;
                }
            }
        }

        /// <summary> Dispose instance. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        public static void DisposeInstance()
        {
            lock ( SyncLocker )
            {
                if ( Instance is object && !Instance.IsDisposed )
                {
                    Instance.Dispose();
                    Instance = null;
                }
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion 

        #region " FORM EVENTS "

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Form.Shown" /> event. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnShown( EventArgs e )
        {
            base.OnShown( e );
        }

        /// <summary> Displays this dialog with title 'illegal call'. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <returns> A MyDialogResult. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private new MyDialogResult ShowDialog()
        {
            this.Text = "Illegal Call";
            return MessageBox.ToDialogResult( base.ShowDialog() );
        }

        /// <summary> Shows the message box with 'illegal call' in the caption. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private new void Show()
        {
            this.Status = string.Empty;
            this.Text = "Illegal Call";
            this._RichTextBox.Text = "Illegal Call";
            this.DialogResult = DialogResult.None;
            base.Show();
        }

        /// <summary> Shows the message box with these messages. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="mdiForm"> The MDI form. </param>
        /// <param name="caption"> The caption. </param>
        /// <param name="details"> The details. </param>
        public void Show( Form mdiForm, string caption, string details )
        {
            if ( mdiForm is object && mdiForm.IsMdiContainer )
            {
                this.MdiParent = mdiForm;
                mdiForm.Show();
            }

            this._ProgressBar.Visible = false;
            this.Status = string.Empty;
            this.Text = caption;
            this._RichTextBox.Text = details;
            this.DialogResult = DialogResult.None;
            Application.DoEvents();
            base.Show();
            Application.DoEvents();
        }

        /// <summary> Shows the message box with these messages. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="mdiForm">  The MDI form. </param>
        /// <param name="caption">  The caption. </param>
        /// <param name="details">  The details. </param>
        /// <param name="duration"> The duration. </param>
        public void Show( Form mdiForm, string caption, string details, TimeSpan duration )
        {
            this.Show( mdiForm, caption, details );
            var sw = Stopwatch.StartNew();
            this._ProgressBar.Value = 100;
            this._ProgressBar.Visible = true;
            while ( sw.Elapsed <= duration )
            {
                Application.DoEvents();
                int value = ( int ) (100d * (1d - sw.Elapsed.Ticks / ( double ) duration.Ticks));
                value = value < 0 ? 0 : value > 100 ? 100 : value;
                this._ProgressBar.Value = value;
                Application.DoEvents();
            }

            this._ProgressBar.Visible = false;
            this.Close();
        }

        /// <summary> Gets the rich text box. </summary>
        /// <value> The rich text box. </value>
        [System.ComponentModel.Browsable( false )]
        [System.ComponentModel.DesignerSerializationVisibility( System.ComponentModel.DesignerSerializationVisibility.Hidden )]
        public RichTextBox RichTextBox => this._RichTextBox;

        /// <summary> Gets or sets the status. </summary>
        /// <value> The status. </value>
        [System.ComponentModel.Browsable( false )]
        [System.ComponentModel.DesignerSerializationVisibility( System.ComponentModel.DesignerSerializationVisibility.Hidden )]
        public string Status
        {
            get => this._StatusLabel.Text;

            set {
                this._StatusLabel.Text = isr.Core.WinForms.CompactExtensions.CompactExtensionMethods.Compact( value, this._StatusLabel );
                this._StatusLabel.ToolTipText = value;
                Application.DoEvents();
            }
        }

        /// <summary> Gets the progress. </summary>
        /// <value> The progress. </value>
        [System.ComponentModel.Browsable( false )]
        [System.ComponentModel.DesignerSerializationVisibility( System.ComponentModel.DesignerSerializationVisibility.Hidden )]
        public ToolStripProgressBar Progress => this._ProgressBar;

        /// <summary> Gets or sets the custom button text. </summary>
        /// <value> The custom button text. </value>
        public string CustomButtonText
        {
            get => this._CustomButton.Text;

            set {
                this._CustomButton.Text = value;
                Application.DoEvents();
            }
        }

        /// <summary> Custom button click. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void CustomButton_Click( object sender, EventArgs e )
        {
            this.DialogResult = DialogResult.OK;
            Application.DoEvents();
            this.Status = $"{this._CustomButton.Text} requested";
        }

        #endregion 

    }
}
