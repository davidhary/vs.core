using System;
using System.Diagnostics;
using System.Security.Permissions;

namespace isr.Core
{

    /// <summary> Displays assembly information. </summary>
    /// <remarks>
    /// David, 2002-09-21. This is the information form for assemblies. To open, instantiate the form
    /// passing the new instance the module file version information reference.
    /// </remarks>
    public partial class About : FormBase

    {


        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        public About() : base()
        {

            // This call is required by the Windows Form Designer.
            this.InitializeComponent();

            // Add any initialization after the InitializeComponent() call
            // onInstantiate()
            if ( Version.Parse( My.MyProject.Computer.Info.OSVersion ).Major <= EnableDropShadowVersion )
            {
                this.ClassStyle = ClassStyleConstants.DropShadow;
            }

            this.__ExitButton.Name = "_ExitButton";
        }

        /// <summary>
        /// Disposes of the resources (other than memory) used by the
        /// <see cref="T:System.Windows.Forms.Form" />.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }


        #endregion 


        #region " METHODS "

        /// <summary> Displays this module. </summary>
        /// <remarks>
        /// Use this method to display the application main form. You must also call Application.Run with
        /// reference to this form because the method shows the form modeless.
        /// </remarks>
        /// <exception cref="ArgumentNullException" guarantee="strong"> Failed to show main form. </exception>
        /// <param name="executionAssembly"> Specifies the
        /// <see cref="System.Reflection.Assembly">calling
        /// assembly</see>. </param>
        /// <param name="licenseeName">      A <see cref="System.String">String</see> expression that
        /// specifies the name of the licensee. </param>
        /// <param name="licenseCode">       A <see cref="System.String">String</see> expression that
        /// specifies the license string (serial number) </param>
        /// <param name="systemId">          A <see cref="System.String">String</see> expression that
        /// specifies the system id, e.g., the Product Name. </param>
        /// <param name="licenseHeader">     A <see cref="System.String">String</see> expression that
        /// specifies the license heading under which the licensee name
        /// and code are displayed.  For example:  "This product is
        /// licensed to:". </param>
        /// <example>
        /// This example displays the About form.
        /// <code>
        /// Private Sub DisplayAboutForm()
        /// ' display the application information
        /// Dim aboutScreen As New isr.WindowsForms.About
        /// aboutScreen.ShowDialog(System.Reflection.Assembly.GetExecutingAssembly,
        /// String.Empty, String.Empty, String.Empty, String.Empty)
        /// End Sub
        /// </code>
        /// To run this example, paste the code fragment into the method region of a Visual Basic form.
        /// Run the program by pressing F5.
        /// </example>
        [FileIOPermission( SecurityAction.Demand, Unrestricted = true )]
        public void Show( System.Reflection.Assembly executionAssembly, string licenseeName, string licenseCode, string systemId, string licenseHeader )
        {
            if ( executionAssembly is null )
            {
                throw new ArgumentNullException( nameof( executionAssembly ) );
            }

            if ( licenseeName is null )
            {
                throw new ArgumentNullException( nameof( licenseeName ) );
            }

            if ( licenseCode is null )
            {
                throw new ArgumentNullException( nameof( licenseCode ) );
            }

            if ( systemId is null )
            {
                throw new ArgumentNullException( nameof( systemId ) );
            }

            if ( licenseHeader is null )
            {
                throw new ArgumentNullException( nameof( licenseHeader ) );
            }

            // process the form show methods
            this.BeforeFormLoad( executionAssembly, licenseeName, licenseCode, systemId, licenseHeader, "Product Information" );

            // show the form
            this.Show();
        }

        /// <summary> Displays this module. </summary>
        /// <remarks>
        /// Use this method to display the form. If you prefer to use the Show Form statement, make sure
        /// that all the errors trapped in the Form_Load event are handled within the form as these
        /// errors cannot be raised to the calling function.
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="executionAssembly"> Specifies the
        /// <see cref="System.Reflection.Assembly">calling
        /// assembly</see>. </param>
        /// <param name="licenseeName">      is an optional String expression that specifies the name of
        /// the licensee. </param>
        /// <param name="licenseCode">       is an optional String expression that specifies the license
        /// string (serial number) </param>
        /// <param name="systemId">          is an optional String expression that specifies the system id,
        /// e.g., the Product Name. </param>
        /// <param name="licenseHeader">     A <see cref="System.String">String</see> expression that
        /// specifies the license heading under which the licensee name
        /// and code are displayed.  For example:  "This product Is
        /// licensed to". </param>
        /// <returns> A Dialog Result. </returns>
        /// <example>
        /// This example displays the About form.
        /// <code>
        /// Private Sub DisplayAboutForm()
        /// ' display the application information
        /// Dim aboutScreen As New isr.WindowsForms.About
        /// aboutScreen.ShowDialog(support.GetFileVersionInfo(),
        /// String.Empty, String.Empty, String.Empty, String.Empty)
        /// End Sub
        /// </code>
        /// To run this example, paste the code fragment into the method region of a Visual Basic form.
        /// Run the program by pressing F5.
        /// </example>
        [FileIOPermission( SecurityAction.Demand, Unrestricted = true )]
        public System.Windows.Forms.DialogResult ShowDialog( System.Reflection.Assembly executionAssembly, string licenseeName, string licenseCode, string systemId, string licenseHeader )
        {
            if ( executionAssembly is null )
            {
                throw new ArgumentNullException( nameof( executionAssembly ) );
            }

            if ( licenseeName is null )
            {
                throw new ArgumentNullException( nameof( licenseeName ) );
            }

            if ( licenseCode is null )
            {
                throw new ArgumentNullException( nameof( licenseCode ) );
            }

            if ( systemId is null )
            {
                throw new ArgumentNullException( nameof( systemId ) );
            }

            if ( licenseHeader is null )
            {
                throw new ArgumentNullException( nameof( licenseHeader ) );
            }

            // process the form show methods
            this.BeforeFormLoad( executionAssembly, licenseeName, licenseCode, systemId, licenseHeader, "Product Information" );

            // show the form
            return this.ShowDialog();
        }

        #endregion 


        #region " PROPERTIES "

        /// <summary> Gets or sets the status message. </summary>
        /// <remarks> Use this property to get the status message generated by the object. </remarks>
        /// <value> A <see cref="System.String">String</see>. </value>
        public string StatusMessage { get; private set; }

        /// <summary> Gets or sets the image. </summary>
        /// <value> The image. </value>
        public System.Drawing.Icon Image
        {
            get => this.Icon;

            set {
                if ( value is object )
                {
                    this._IconPictureBox.Image = value.ToBitmap();
                    this._IconPictureBox.Invalidate();
                    if ( this._IconPictureBox.Left + this._IconPictureBox.Width > this.Width )
                    {
                        if ( this._IconPictureBox.Width < 0.2d * this.Width )
                        {
                            this._IconPictureBox.Left = this.ClientSize.Width - this._IconPictureBox.Width - 5;
                        }
                    }
                }
            }
        }

        #endregion 


        #region " CONTROL EVENT HANDLERS "

        /// <summary> Occurs when the user selects the exit button. </summary>
        /// <remarks> Use this method to exit. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      Event information. </param>
        private void ExitButtonClick( object sender, EventArgs e )
        {
            // Close this form
            this.Close();
        }

        #endregion 


        #region " PRIVATE  and  PROTECTED "

        /// <summary> Initializes the user interface and tool tips. </summary>
        /// <remarks> Call this method from the form load method to set the user interface. </remarks>
        private void InitializeUserInterface()
        {
            this._IconPictureBox.Image = this.Icon.ToBitmap();
        }

        /// <summary> Processes the display of this module. </summary>
        /// <remarks>
        /// David, 2008-02-09. Use File Description (Application Title Property) for the product name and
        /// remove it from the comments section. Use product version instead of file version.
        /// </remarks>
        /// <exception cref="ArgumentNullException" guarantee="strong"> Failed to show main form. </exception>
        /// <param name="executionAssembly"> Specifies the
        /// <see cref="System.Reflection.Assembly">calling
        /// assembly</see>. </param>
        /// <param name="licenseeName">      A <see cref="System.String">String</see> expression that
        /// specifies the name of the licensee. </param>
        /// <param name="licenseCode">       A <see cref="System.String">String</see> expression that
        /// specifies the license string (serial number) </param>
        /// <param name="systemId">          A <see cref="System.String">String</see> expression that
        /// specifies the system id, e.g., the Product Name. </param>
        /// <param name="licenseHeader">     A <see cref="System.String">String</see> expression that
        /// specifies the license heading under which the licensee name
        /// and code are displayed.  For example:  "This product is
        /// licensed to:". </param>
        /// <param name="caption">           The caption. </param>
        [FileIOPermission( SecurityAction.Demand, Unrestricted = true )]
        private void BeforeFormLoad( System.Reflection.Assembly executionAssembly, string licenseeName, string licenseCode, string systemId, string licenseHeader, string caption )
        {
            if ( executionAssembly is null )
            {
                throw new ArgumentNullException( nameof( executionAssembly ) );
            }

            if ( licenseeName is null )
            {
                throw new ArgumentNullException( nameof( licenseeName ) );
            }

            if ( licenseCode is null )
            {
                throw new ArgumentNullException( nameof( licenseCode ) );
            }

            if ( systemId is null )
            {
                throw new ArgumentNullException( nameof( systemId ) );
            }

            if ( licenseHeader is null )
            {
                throw new ArgumentNullException( nameof( licenseHeader ) );
            }

            try
            {

                // Turn on the form hourglass cursor
                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

                // hide the cancel button
                this._ExitButton.Top = -this._ExitButton.Height;
                var fileVersionInfo = FileVersionInfo.GetVersionInfo( executionAssembly.Location );
                int rowTop;
                var rowText = new System.Text.StringBuilder( "" );
                int topMargin = 1;
                // Dim leftMargin As Int32 = 2
                int rowSpace;

                // set the caption
                this.Text = caption.Length > 0 ? caption : "Registration Information";
                rowSpace = this._ProductTitleLabel.Height / 2;
                this._ProductTitleLabel.Text = executionAssembly.GetName().Name;
                this._ProductTitleLabel.Refresh();
                this._ProductTitleLabel.Top = topMargin;
                // Me._productTitleLabel.Left = leftMargin
                // Me._productTitleLabel.Width = 1 + Int32.parse(Me.CreateGraphics().MeasureString(Me._productTitleLabel.text, Me._productTitleLabel.Font).Width, System.Globalization.CultureInfo.CurrentCulture)
                this._ProductTitleLabel.Height = Convert.ToInt32( this.CreateGraphics().MeasureString( this._ProductTitleLabel.Text, this._ProductTitleLabel.Font ).Height, System.Globalization.CultureInfo.CurrentCulture );
                this._ProductTitleLabel.Invalidate();
                rowTop = this._ProductTitleLabel.Top + this._ProductTitleLabel.Height + rowSpace;
                this._ProductNameLabel.Text = fileVersionInfo.FileDescription;
                this._ProductNameLabel.Refresh();
                // Me._productNameLabel.Width = 1 + Int32.parse(Me._productNameLabel.CreateGraphics().MeasureString(Me._productNameLabel.text, Me._productNameLabel.Font).Width, System.Globalization.CultureInfo.CurrentCulture)
                this._ProductNameLabel.Height = Convert.ToInt32( this._ProductNameLabel.CreateGraphics().MeasureString( this._ProductNameLabel.Text, this._ProductNameLabel.Font ).Height, System.Globalization.CultureInfo.CurrentCulture );
                // Me._productNameLabel.Left = leftMargin
                this._ProductNameLabel.Top = rowTop;
                this._ProductNameLabel.Invalidate();
                rowTop = this._ProductNameLabel.Top + this._ProductNameLabel.Height + rowSpace;
                this._IconPictureBox.Top = topMargin;
                if ( fileVersionInfo.Comments.Trim().Length > 0 )
                {
                    _ = rowText.Append( fileVersionInfo.Comments.Trim() );
                }

                if ( systemId.Trim().Length > 0 )
                {
                    _ = rowText.AppendLine();
                    _ = rowText.Append( $"{systemId.Trim()}" );
                }

                _ = rowText.AppendLine();
                _ = rowText.Append( $"Version: {fileVersionInfo.ProductVersion} updated {System.IO.File.GetLastWriteTime( System.Windows.Forms.Application.StartupPath )}" );
                this._DescriptionLabel.Text = rowText.ToString();
                this._DescriptionLabel.Refresh();
                // Me._descriptionLabel.Width = 1 + Int32.parse(Me.CreateGraphics().MeasureString(Me._descriptionLabel.text, Me._descriptionLabel.Font).Width, System.Globalization.CultureInfo.CurrentCulture)
                this._DescriptionLabel.Height = Convert.ToInt32( this.CreateGraphics().MeasureString( this._DescriptionLabel.Text, this._DescriptionLabel.Font ).Height, System.Globalization.CultureInfo.CurrentCulture );
                // Me._descriptionLabel.Left = leftMargin
                this._DescriptionLabel.Top = rowTop;
                this._DescriptionLabel.Invalidate();
                rowTop = this._DescriptionLabel.Top + this._DescriptionLabel.Height + rowSpace;

                // Display license information
                rowText = new System.Text.StringBuilder();
                if ( licenseeName.Trim().Length > 0 | licenseCode.Trim().Length > 0 )
                {
                    if ( licenseHeader.Trim().Length > 0 )
                    {
                        _ = rowText.Append( licenseHeader );
                    }

                    if ( licenseeName.Trim().Length > 0 )
                    {
                        if ( rowText.Length > 0 )
                        {
                            _ = rowText.AppendLine();
                        }

                        _ = rowText.Append( $" {licenseeName.Trim()}" );
                    }

                    if ( licenseCode.Trim().Length > 0 )
                    {
                        if ( rowText.Length > 0 )
                        {
                            _ = rowText.AppendLine();
                        }

                        _ = rowText.Append( $"  {licenseCode.Trim()}" );
                    }
                }
                else if ( licenseHeader.Trim().Length > 0 )
                {
                    _ = rowText.Append( licenseHeader );
                }

                this._LicenseLabel.Text = rowText.ToString();
                this._LicenseLabel.Refresh();
                this._LicenseLabel.Height = Convert.ToInt32( this.CreateGraphics().MeasureString( this._LicenseLabel.Text, this._LicenseLabel.Font ).Height, System.Globalization.CultureInfo.CurrentCulture );

                this._LicenseLabel.Top = rowTop;
                this._LicenseLabel.Invalidate();
                rowTop = this._LicenseLabel.Top + this._LicenseLabel.Height + rowSpace;
                this._CopyrightLabel.Text = fileVersionInfo.LegalCopyright.Trim();
                this._CopyrightLabel.Refresh();

                this._CopyrightLabel.Top = rowTop;
                this._CopyrightLabel.Invalidate();
                rowTop = this._CopyrightLabel.Top + this._CopyrightLabel.Height + rowSpace / 2;

                // set form height to fit data.
                this.Height = rowTop + (this.Height - this.ClientSize.Height);
            }
            catch
            {
                throw;
            }
            finally
            {

                // Turn off the form hourglass cursor
                this.Cursor = System.Windows.Forms.Cursors.Default;
            }
        }

        #endregion 


        #region " FORM EVENT HANDLERS "

        /// <summary>
        /// Occurs when the form is loaded. Does all the processing before the form controls are rendered
        /// as the user expects them.
        /// </summary>
        /// <remarks>
        /// Use this method for doing any final initialization right before the form is shown. This is a
        /// good place to change the Visible and ShowInTaskbar properties to start the form as hidden.
        /// Starting a form as hidden is useful for forms that need to be running but that should not
        /// show themselves right away, such as forms with a notify icon in the task bar.
        /// </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      <see cref="System.EventArgs"/> </param>
        private void Form_Load( object sender, EventArgs e )
        {
            try
            {

                // Turn on the form hourglass cursor
                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

                // center the form
                this.CenterToScreen();
            }
            catch
            {

                // Use throw without an argument in order to preserve the stack location 
                // where the exception was initially raised.
                throw;
            }
            finally
            {

                // Turn off the form hourglass cursor
                this.Cursor = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary>
        /// Does all the post processing after all the form controls are rendered as the user expects
        /// them.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void Form_Shown( object sender, EventArgs e )
        {

            // allow form rendering time to complete: process all messages currently in the queue.
            System.Windows.Forms.Application.DoEvents();
            try
            {

                // Turn on the form hourglass cursor
                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

                // instantiate form objects
                // Me.instantiateObjects()

                // set the form caption
                this.Text = $"{My.MyProject.Application.Info.AssemblyName}.{My.MyProject.Application.Info.Version}";

                // set tool tips
                this.InitializeUserInterface();

                // allow some events to occur for refreshing the display.
                System.Windows.Forms.Application.DoEvents();
            }
            catch ( Exception ex )
            {
                _ = MyMessageBox.ShowDialog( ex );
            }
            finally
            {
                this.Cursor = System.Windows.Forms.Cursors.Default;
            }
        }

        #endregion 

    }
}
