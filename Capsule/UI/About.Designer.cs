﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core
{
    [DesignerGenerated()]
    public partial class About
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            _IconPictureBox = new System.Windows.Forms.PictureBox();
            _ProductTitleLabel = new System.Windows.Forms.Label();
            _DescriptionLabel = new System.Windows.Forms.Label();
            _LicenseLabel = new System.Windows.Forms.Label();
            _CopyrightLabel = new System.Windows.Forms.Label();
            __ExitButton = new System.Windows.Forms.Button();
            __ExitButton.Click += new EventHandler(ExitButtonClick);
            _ProductNameLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)_IconPictureBox).BeginInit();
            SuspendLayout();
            // 
            // _iconPictureBox
            // 
            _IconPictureBox.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            _IconPictureBox.BackColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(232)), Conversions.ToInteger(Conversions.ToByte(243)), Conversions.ToInteger(Conversions.ToByte(255)));
            _IconPictureBox.Location = new System.Drawing.Point(367, 5);
            _IconPictureBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            _IconPictureBox.Name = "_iconPictureBox";
            _IconPictureBox.Size = new System.Drawing.Size(16, 16);
            _IconPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            _IconPictureBox.TabIndex = 8;
            _IconPictureBox.TabStop = false;
            // 
            // _productTitleLabel
            // 
            _ProductTitleLabel.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            _ProductTitleLabel.BackColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(232)), Conversions.ToInteger(Conversions.ToByte(243)), Conversions.ToInteger(Conversions.ToByte(255)));
            _ProductTitleLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            _ProductTitleLabel.Font = new System.Drawing.Font(Font, System.Drawing.FontStyle.Bold);
            _ProductTitleLabel.ForeColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(148)));
            _ProductTitleLabel.Location = new System.Drawing.Point(3, 0);
            _ProductTitleLabel.Name = "_productTitleLabel";
            _ProductTitleLabel.Size = new System.Drawing.Size(341, 24);
            _ProductTitleLabel.TabIndex = 14;
            _ProductTitleLabel.Text = "Product Title";
            _ProductTitleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _descriptionLabel
            // 
            _DescriptionLabel.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            _DescriptionLabel.BackColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(232)), Conversions.ToInteger(Conversions.ToByte(243)), Conversions.ToInteger(Conversions.ToByte(255)));
            _DescriptionLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            _DescriptionLabel.ForeColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(148)));
            _DescriptionLabel.Location = new System.Drawing.Point(3, 76);
            _DescriptionLabel.Name = "_descriptionLabel";
            _DescriptionLabel.Size = new System.Drawing.Size(387, 47);
            _DescriptionLabel.TabIndex = 10;
            _DescriptionLabel.Text = "File description, copyrights, trademarks, revision";
            // 
            // _licenseLabel
            // 
            _LicenseLabel.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            _LicenseLabel.BackColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(232)), Conversions.ToInteger(Conversions.ToByte(243)), Conversions.ToInteger(Conversions.ToByte(255)));
            _LicenseLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            _LicenseLabel.ForeColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(148)));
            _LicenseLabel.Location = new System.Drawing.Point(3, 180);
            _LicenseLabel.Name = "_licenseLabel";
            _LicenseLabel.Size = new System.Drawing.Size(387, 43);
            _LicenseLabel.TabIndex = 12;
            _LicenseLabel.Text = "This product is licensed to";
            // 
            // _copyrightLabel
            // 
            _CopyrightLabel.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            _CopyrightLabel.BackColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(232)), Conversions.ToInteger(Conversions.ToByte(243)), Conversions.ToInteger(Conversions.ToByte(255)));
            _CopyrightLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            _CopyrightLabel.ForeColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(148)));
            _CopyrightLabel.Location = new System.Drawing.Point(3, 233);
            _CopyrightLabel.Name = "_copyrightLabel";
            _CopyrightLabel.Size = new System.Drawing.Size(387, 38);
            _CopyrightLabel.TabIndex = 11;
            _CopyrightLabel.Text = "This program is protected by US and international copyright laws";
            _CopyrightLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // _ExitButton
            // 
            __ExitButton.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            __ExitButton.BackColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(232)), Conversions.ToInteger(Conversions.ToByte(243)), Conversions.ToInteger(Conversions.ToByte(255)));
            __ExitButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            __ExitButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            __ExitButton.Location = new System.Drawing.Point(335, 123);
            __ExitButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            __ExitButton.Name = "__ExitButton";
            __ExitButton.Size = new System.Drawing.Size(37, 42);
            __ExitButton.TabIndex = 13;
            __ExitButton.UseVisualStyleBackColor = false;
            // 
            // _productNameLabel
            // 
            _ProductNameLabel.BackColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(232)), Conversions.ToInteger(Conversions.ToByte(243)), Conversions.ToInteger(Conversions.ToByte(255)));
            _ProductNameLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            _ProductNameLabel.Font = new System.Drawing.Font(Font, System.Drawing.FontStyle.Bold);
            _ProductNameLabel.ForeColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(148)));
            _ProductNameLabel.Location = new System.Drawing.Point(3, 41);
            _ProductNameLabel.Name = "_productNameLabel";
            _ProductNameLabel.Size = new System.Drawing.Size(385, 24);
            _ProductNameLabel.TabIndex = 9;
            _ProductNameLabel.Text = "Product Name";
            _ProductNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // About
            // 
            BackColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(232)), Conversions.ToInteger(Conversions.ToByte(243)), Conversions.ToInteger(Conversions.ToByte(255)));
            CancelButton = __ExitButton;
            ClientSize = new System.Drawing.Size(394, 282);
            Controls.Add(_IconPictureBox);
            Controls.Add(_ProductTitleLabel);
            Controls.Add(_DescriptionLabel);
            Controls.Add(_LicenseLabel);
            Controls.Add(_CopyrightLabel);
            Controls.Add(__ExitButton);
            Controls.Add(_ProductNameLabel);
            FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            Name = "About";
            Text = "About";
            ((System.ComponentModel.ISupportInitialize)_IconPictureBox).EndInit();
            Load += new EventHandler(Form_Load);
            Shown += new EventHandler(Form_Shown);
            ResumeLayout(false);
            PerformLayout();
        }

        private System.Windows.Forms.PictureBox _IconPictureBox;
        private System.Windows.Forms.Label _ProductTitleLabel;
        private System.Windows.Forms.Label _DescriptionLabel;
        private System.Windows.Forms.Label _LicenseLabel;
        private System.Windows.Forms.Label _CopyrightLabel;
        private System.Windows.Forms.Button __ExitButton;

        private System.Windows.Forms.Button _ExitButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ExitButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ExitButton != null)
                {
                    __ExitButton.Click -= ExitButtonClick;
                }

                __ExitButton = value;
                if (__ExitButton != null)
                {
                    __ExitButton.Click += ExitButtonClick;
                }
            }
        }

        private System.Windows.Forms.Label _ProductNameLabel;
    }
}