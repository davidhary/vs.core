using System;

namespace isr.Core
{
    /// <summary> A data entry form for text with a numeric validator. </summary>
    /// <remarks> David, 2006-02-20. </remarks>
    public partial class TextInputBox : FormBase
    {


        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of this class. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        public TextInputBox() : base()
        {

            // This call is required by the Windows Form Designer.
            this.InitializeComponent();

            // Add any initialization after the InitializeComponent() call
            if ( Version.Parse( My.MyProject.Computer.Info.OSVersion ).Major <= EnableDropShadowVersion )
            {
                this.ClassStyle = ClassStyleConstants.DropShadow;
            }

            this.NumberStyle = System.Globalization.NumberStyles.None;
            this.__EnteredValueTextBox.Name = "_EnteredValueTextBox";
            this.__CancelButton.Name = "_CancelButton";
            this.__AcceptButton.Name = "_AcceptButton";
        }

        /// <summary>
        /// Disposes of the resources (other than memory) used by the
        /// <see cref="T:System.Windows.Forms.Form" />.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion 


        #region " PROPERTIES "

        /// <summary> Gets or sets the use system password character. </summary>
        /// <value> The use system password character. </value>
        public bool UseSystemPasswordChar
        {
            get => this._EnteredValueTextBox.UseSystemPasswordChar;

            set => this._EnteredValueTextBox.UseSystemPasswordChar = value;
        }

        /// <summary> Gets or sets the prompt. </summary>
        /// <value> The prompt. </value>
        public string Prompt
        {
            get => this._EnteredValueTextBoxLabel.Text;

            set => this._EnteredValueTextBoxLabel.Text = value;
        }

        /// <summary> Returns the entered value. </summary>
        /// <value> The entered value. </value>
        public string EnteredValue
        {
            get => this._EnteredValueTextBox.Text;

            set => this._EnteredValueTextBox.Text = value;
        }

        /// <summary> Gets or sets the default value. </summary>
        /// <value> The default value. </value>
        public string DefaultValue { get; set; } = string.Empty;

        /// <summary> Gets or sets the number style requested. </summary>
        /// <value> The total number of style. </value>
        public System.Globalization.NumberStyles NumberStyle { get; set; }

        #endregion 


        #region " FORM AND CONTROL EVENT HANDLERS "

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Form.Shown" /> event. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnShown( EventArgs e )
        {
            try
            {
                _ = this._EnteredValueTextBox.Focus();
            }
            catch
            {
            }
            finally
            {
                base.OnShown( e );
            }
        }

        /// <summary>
        /// Closes and returns the <see cref="System.Windows.Forms.DialogResult.OK">OK</see>
        /// dialog result.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void AcceptButtonClick( object sender, EventArgs e )
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// Closes and returns the <see cref="System.Windows.Forms.DialogResult.Cancel">Cancel</see>
        /// dialog result.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void CancelButtonClick( object sender, EventArgs e )
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        /// <summary> Event handler. Called by _EnteredValueTextBox for text changed events. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void EnteredValueTextBoxTextChanged( object sender, EventArgs e )
        {
            if ( this.NumberStyle == System.Globalization.NumberStyles.None )
            {
                this._AcceptButton.Enabled = !string.IsNullOrEmpty( this._EnteredValueTextBox.Text );
            }
        }

        /// <summary> Validates the entered value. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Cancel event information. </param>
        private void EnteredValueTextBoxValidating( object sender, System.ComponentModel.CancelEventArgs e )
        {
            this._AcceptButton.Enabled = false;
            this._ValidationErrorProvider.SetError( sender as System.Windows.Forms.Control, string.Empty );
            if ( this.NumberStyle != System.Globalization.NumberStyles.None )
            {
                e.Cancel = !double.TryParse( this._EnteredValueTextBox.Text, this.NumberStyle, System.Globalization.CultureInfo.CurrentCulture, out _ );
            }
        }

        /// <summary> Enables the OK button. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void EnteredValueTextBoxValidated( object sender, EventArgs e )
        {
            this._AcceptButton.Enabled = true;
        }

        #endregion 

    }

    /// <summary> my text input box. </summary>
    /// <remarks> David, 2019-09-13. </remarks>
    public sealed class MyTextInputBox
    {


        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        private MyTextInputBox() : base()
        {
        }


        #endregion 


        #region " SHARED "

        /// <summary> Enter dialog. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="caption"> The caption. </param>
        /// <returns> A (DialogResult As MyDialogResult, DialogValue As String) </returns>
        public static (MyDialogResult DialogResult, string DialogValue) EnterDialog( string caption )
        {
            return EnterDialog( caption, "Enter a value:", string.Empty, false );
        }

        /// <summary> Enter dialog. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="caption">               The caption. </param>
        /// <param name="useSystemPasswordChar"> True to use system password character. </param>
        /// <returns> A (DialogResult As MyDialogResult, DialogValue As String) </returns>
        public static (MyDialogResult DialogResult, string DialogValue) EnterDialog( string caption, bool useSystemPasswordChar )
        {
            return EnterDialog( caption, "Enter a value:", string.Empty, useSystemPasswordChar );
        }

        /// <summary> Enter dialog. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="caption">               The caption. </param>
        /// <param name="prompt">                The prompt. </param>
        /// <param name="defaultValue">          The default value. </param>
        /// <param name="useSystemPasswordChar"> True to use system password character. </param>
        /// <returns> A (DialogResult As MyDialogResult, DialogValue As String) </returns>
        public static (MyDialogResult DialogResult, string DialogValue) EnterDialog( string caption, string prompt, string defaultValue, bool useSystemPasswordChar )
        {
            (MyDialogResult DialogResult, string DialogValue) outcome = (MyDialogResult.Ok, string.Empty);
            using ( var box = new TextInputBox() )
            {
                box.UseSystemPasswordChar = useSystemPasswordChar;
                box.DefaultValue = defaultValue;
                box.Text = prompt;
                box.Text = caption;
                // toggling visibility ensures that the form displays in unit testing debug mode
                box.Visible = true;
                System.Windows.Forms.Application.DoEvents();
                box.Visible = false;
                _ = box.ShowDialog();
                outcome = (MessageBox.ToDialogResult( box.DialogResult ), box.EnteredValue);
            }

            return outcome;
        }

        #endregion 

    }
}
