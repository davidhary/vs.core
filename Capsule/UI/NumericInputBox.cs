using System;

#pragma warning disable IDE1006 // Naming Styles
namespace isr.Core
#pragma warning restore IDE1006 // Naming Styles
{
    /// <summary> A data entry form. </summary>
    /// <remarks> David, 2006-02-20. </remarks>
    public partial class NumericInputBox : FormBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of this class. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        public NumericInputBox() : base()
        {

            // This call is required by the Windows Form Designer.
            this.InitializeComponent();

            // Add any initialization after the InitializeComponent() call
            if ( Version.Parse( My.MyProject.Computer.Info.OSVersion ).Major <= EnableDropShadowVersion )
            {
                this.ClassStyle = ClassStyleConstants.DropShadow;
            }

            this._AcceptButton.Enabled = true;
            this.__CancelButton.Name = "_CancelButton";
            this.__AcceptButton.Name = "_AcceptButton";
        }

        /// <summary>
        /// Disposes of the resources (other than memory) used by the
        /// <see cref="T:System.Windows.Forms.Form" />.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion 

        #region " FORM AND CONTROL EVENT HANDLERS "

        /// <summary>
        /// Closes and returns the <see cref="System.Windows.Forms.DialogResult.OK">OK</see>
        /// dialog result.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void AcceptButton_Click( object sender, EventArgs e )
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// Closes and returns the <see cref="System.Windows.Forms.DialogResult.Cancel">Cancel</see>
        /// dialog result.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void CancelButton_Click( object sender, EventArgs e )
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        #endregion 

    }
}
