using System;

namespace isr.Core
{
    /// <summary> Displays a splash screen. </summary>
    /// <remarks> David, 2004-02-17. </remarks>
    public partial class SplashScreen : FormBase
    {


        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of this class. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        public SplashScreen() : base()
        {

            // This call is required by the Windows Form Designer.
            this.InitializeComponent();

            // Add any initialization after the InitializeComponent() call
            if ( Version.Parse( My.MyProject.Computer.Info.OSVersion ).Major <= EnableDropShadowVersion )
            {
                this.ClassStyle = ClassStyleConstants.DropShadow;
            }

            this._Status = string.Empty;
            this.LicenseHeader = "Licensed to:";
            this._LicenseeName = "Integrated Scientific Resources, Inc.";
            this.ProductFamilyName = "An Integrated Scientific Resources Product";
            this.__CancelButton.Name = "_CancelButton";
        }

        /// <summary>
        /// Disposes of the resources (other than memory) used by the
        /// <see cref="T:System.Windows.Forms.Form" />.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion 


        #region " PROPERTIES and METHODS "

        /// <summary> true to topmost. </summary>
        private bool _Topmost;

        /// <summary> Sets the top most status in a thread safe way. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="value"> true to value. </param>
        public void TopmostSetter( bool value )
        {
            this._Topmost = value;
            SafeTopMostSetter( this, value );
        }

        /// <summary> Gets or sets the header for displaying the licensee name. </summary>
        /// <value> <c>LicenseHeader</c>is a String property. </value>
        public string LicenseHeader { get; set; }

        /// <summary> Gets or sets the license code. </summary>
        /// <value> <c>LicenseCode</c>is a String property. </value>
        public string LicenseCode { get; set; }

        /// <summary> Name of the licensee. </summary>
        private string _LicenseeName;

        /// <summary> Gets or sets the licensee name. </summary>
        /// <value> <c>LicenseeName</c>is a String property. </value>
        public string LicenseeName
        {
            get => this._LicenseeName;

            set {
                this._LicenseeName = value;
                // Display license information
                SafeTextSetter( this._LicenseeLabel, this.BuildLicenseInfo() );
                SafeHeightSetter( this._LicenseeLabel, Convert.ToInt32( this.CreateGraphics().MeasureString( this._LicenseeLabel.Text, this._LicenseeLabel.Font ).Height ) );
            }
        }

        /// <summary> The platform title. </summary>
        private string _PlatformTitle = string.Empty;

        /// <summary> Gets or sets the platform title, e.g., 'For 32 Bit operating Systems'. </summary>
        /// <value> <c>PlatformTitle</c>is a String property. </value>
        public string PlatformTitle
        {
            get {
                if ( string.IsNullOrWhiteSpace( this._PlatformTitle ) )
                {
                    this._PlatformTitle = "for 32/64-Bit Operating Systems";
                }

                return this._PlatformTitle;
            }

            set => this._PlatformTitle = value;
        }

        /// <summary> Gets reference to the primary Picture Box. </summary>
        /// <value> The primary picture box. </value>
        public System.Windows.Forms.PictureBox PrimaryPictureBox => this._CompanyLogoPictureBox;

        /// <summary> Gets reference to the secondary Picture Box. </summary>
        /// <value> The secondary picture box. </value>
        public System.Windows.Forms.PictureBox SecondaryPictureBox => this._DisksPictureBox;

        /// <summary> Gets or sets the product family name, such as MyCompany Components. </summary>
        /// <value> <c>ProductFamilyName</c>is a String property. </value>
        public string ProductFamilyName { get; set; }

        /// <summary> Updates all information. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        public void UpdateAllInfo()
        {
            this.StartUpdateInfo();
        }

        /// <summary> The status. </summary>
        private string _Status;

        /// <summary> Displays a message on the splash screen. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="value"> The message. </param>
        public void DisplayMessage( string value )
        {
            this._Status = value;
            SafeTextSetter( this._StatusLabel, value );
        }

        /// <summary> Builds license information. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <returns> The license information. </returns>
        private string BuildLicenseInfo()
        {
            // Display license information
            var licenseBuilder = new System.Text.StringBuilder( "" );
            if ( !((string.IsNullOrWhiteSpace( this.LicenseeName ) || string.IsNullOrWhiteSpace( this.LicenseeName.Trim() )) && (string.IsNullOrWhiteSpace( this.LicenseCode ) || string.IsNullOrWhiteSpace( this.LicenseCode.Trim() ))) )
            {
                if ( !(string.IsNullOrWhiteSpace( this.LicenseHeader ) || string.IsNullOrWhiteSpace( this.LicenseHeader.Trim() )) )
                {
                    _ = licenseBuilder.AppendLine( this.LicenseHeader.Trim() );
                }

                if ( !(string.IsNullOrWhiteSpace( this.LicenseeName ) || string.IsNullOrWhiteSpace( this.LicenseeName.Trim() )) )
                {
                    _ = licenseBuilder.AppendLine( $"  {this.LicenseeName.Trim()}" );
                }

                if ( !(string.IsNullOrWhiteSpace( this.LicenseCode ) || string.IsNullOrWhiteSpace( this.LicenseCode.Trim() )) )
                {
                    _ = licenseBuilder.AppendLine( $"  {this.LicenseCode.Trim()}" );
                }
            }
            else
            {
                _ = !(string.IsNullOrWhiteSpace( this.LicenseHeader ) || string.IsNullOrWhiteSpace( this.LicenseHeader.Trim() ))
                    ? licenseBuilder.AppendLine( $"{this.LicenseHeader.Trim()}" )
                    : licenseBuilder.AppendLine( $"{My.MyProject.Application.Info.CompanyName}" );
            }

            return licenseBuilder.ToString();
        }

        /// <summary> Update the information on screen. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        private void UpdateInfo()
        {
            this._ProductFamilyLabel.Text = this.ProductFamilyName;
            this._ProductFamilyLabel.Invalidate();
            this._ProductTitleLabel.Text = My.MyProject.Application.Info.Title;
            this._ProductTitleLabel.Invalidate();
            this._ProductNameLabel.Text = My.MyProject.Application.Info.ProductName;
            this._ProductNameLabel.Invalidate();
            this._ProductVersionLabel.Text = $"Version {My.MyProject.Application.Info.Version}";
            this._ProductVersionLabel.Invalidate();
            this._PlatformTitleLabel.Text = this.PlatformTitle;
            this._PlatformTitleLabel.Invalidate();
            this._CompanyNameLabel.Text = My.MyProject.Application.Info.CompanyName;
            this._CompanyNameLabel.Invalidate();
            this._CopyrightLabel.Text = My.MyProject.Application.Info.Copyright;
            this._CopyrightLabel.Invalidate();
            this._CopyrightWarningLabel.Text = My.MyProject.Application.Info.Trademark;
            this._CopyrightWarningLabel.Invalidate();
            System.Windows.Forms.Application.DoEvents();

            // Display license information
            this._LicenseeLabel.Text = this.BuildLicenseInfo();
            this._LicenseeLabel.Height = Convert.ToInt32( this.CreateGraphics().MeasureString( this._LicenseeLabel.Text, this._LicenseeLabel.Font ).Height );
            this._LicenseeLabel.Invalidate();
            System.Windows.Forms.Application.DoEvents();
            this._StatusLabel.Text = this._Status;
            this._StatusLabel.Invalidate();
            this.TopMost = this._Topmost;
            System.Windows.Forms.Application.DoEvents();
        }

        #endregion 


        #region " THREAD SAFE METHODS "

        /// <summary> Safe height setter. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="control"> The control. </param>
        /// <param name="value">   true to value. </param>
        private static void SafeHeightSetter( System.Windows.Forms.Control control, int value )
        {
            if ( control is object )
            {
                if ( control.InvokeRequired )
                {
                    _ = control.Invoke( new Action<System.Windows.Forms.Control, int>( SafeHeightSetter ), new object[] { control, value } );
                }
                else
                {
                    control.Height = value;
                    control.Invalidate();
                    System.Windows.Forms.Application.DoEvents();
                }
            }
        }

        /// <summary>
        /// Sets the <see cref="System.Windows.Forms.Control">control</see> text to the
        /// <paramref name="value">value</paramref>.
        /// This setter is thread safe.
        /// </summary>
        /// <remarks> The value is set to empty if null or empty. </remarks>
        /// <param name="control"> The control. </param>
        /// <param name="value">   The value. </param>
        /// <returns> value. </returns>
        private static void SafeTextSetter( System.Windows.Forms.Control control, string value )
        {
            if ( control is object )
            {
                if ( string.IsNullOrWhiteSpace( value ) )
                {
                    value = string.Empty;
                }

                if ( control.InvokeRequired )
                {
                    _ = control.Invoke( new Action<System.Windows.Forms.Control, string>( SafeTextSetter ), new object[] { control, value } );
                }
                else
                {
                    control.Text = value;
                    control.Invalidate();
                    System.Windows.Forms.Application.DoEvents();
                }
            }
        }

        /// <summary> Safe top most setter. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="form">  The form. </param>
        /// <param name="value"> true to value. </param>
        private static void SafeTopMostSetter( System.Windows.Forms.Form form, bool value )
        {
            if ( form is object )
            {
                if ( form.InvokeRequired )
                {
                    _ = form.Invoke( new Action<System.Windows.Forms.Form, bool>( SafeTopMostSetter ), new object[] { form, value } );
                }
                else
                {
                    form.TopMost = value;
                    System.Windows.Forms.Application.DoEvents();
                }
            }
        }

        /// <summary> Start the information updating thread. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        private void StartUpdateInfo()
        {
            if ( this.InvokeRequired )
            {
                _ = this.Invoke( new Action( this.UpdateInfo ) );
            }
            else
            {
                this.UpdateInfo();
            }
        }

        #endregion 


        #region " FORM EVENT HANDLERS "

        /// <summary>
        /// Does all the post processing after all the form controls are rendered as the user expects
        /// them.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void Form_Shown( object sender, EventArgs e )
        {
            System.Windows.Forms.Application.DoEvents();
            try
            {
                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
                // instantiate form objects
                this.StartUpdateInfo();
            }
            catch ( Exception ex )
            {
                _ = MyMessageBox.ShowDialog( ex );
            }
            finally
            {
                this.Cursor = System.Windows.Forms.Cursors.Default;
                System.Windows.Forms.Application.DoEvents();
            }
        }

        #endregion 


        #region " CONTROL EVENT HANDLERS "

        /// <summary> Hides the splash screen. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void CancelButtonClick( object sender, EventArgs e )
        {
            this.Close();
        }

        #endregion 

    }
}
