﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core
{
    [DesignerGenerated()]
    public partial class TextInputBox
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            __EnteredValueTextBox = new System.Windows.Forms.TextBox();
            __EnteredValueTextBox.TextChanged += new EventHandler(EnteredValueTextBoxTextChanged);
            __EnteredValueTextBox.Validating += new System.ComponentModel.CancelEventHandler(EnteredValueTextBoxValidating);
            __EnteredValueTextBox.Validated += new EventHandler(EnteredValueTextBoxValidated);
            __CancelButton = new System.Windows.Forms.Button();
            __CancelButton.Click += new EventHandler(CancelButtonClick);
            __AcceptButton = new System.Windows.Forms.Button();
            __AcceptButton.Click += new EventHandler(AcceptButtonClick);
            _EnteredValueTextBoxLabel = new System.Windows.Forms.Label();
            _ValidationErrorProvider = new System.Windows.Forms.ErrorProvider(components);
            ((System.ComponentModel.ISupportInitialize)_ValidationErrorProvider).BeginInit();
            SuspendLayout();
            // 
            // _EnteredValueTextBox
            // 
            __EnteredValueTextBox.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;

            __EnteredValueTextBox.Font = new Font("Segoe UI", 9.0f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            __EnteredValueTextBox.Location = new Point(9, 27);
            __EnteredValueTextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            __EnteredValueTextBox.Multiline = true;
            __EnteredValueTextBox.Name = "__EnteredValueTextBox";
            __EnteredValueTextBox.Size = new Size(186, 20);
            __EnteredValueTextBox.TabIndex = 28;
            // 
            // _CancelButton
            // 
            __CancelButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left;
            __CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            __CancelButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            __CancelButton.Font = new Font("Segoe UI", 9.0f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            __CancelButton.Location = new Point(9, 59);
            __CancelButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            __CancelButton.Name = "__CancelButton";
            __CancelButton.Size = new Size(87, 30);
            __CancelButton.TabIndex = 27;
            __CancelButton.Text = "&Cancel";
            // 
            // _AcceptButton
            // 
            __AcceptButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            __AcceptButton.Enabled = false;
            __AcceptButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            __AcceptButton.Font = new Font("Segoe UI", 9.0f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            __AcceptButton.Location = new Point(108, 59);
            __AcceptButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            __AcceptButton.Name = "__AcceptButton";
            __AcceptButton.Size = new Size(87, 30);
            __AcceptButton.TabIndex = 26;
            __AcceptButton.Text = "&OK";
            // 
            // _EnteredValueTextBoxLabel
            // 
            _EnteredValueTextBoxLabel.AutoSize = true;
            _EnteredValueTextBoxLabel.BackColor = SystemColors.Control;
            _EnteredValueTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default;
            _EnteredValueTextBoxLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            _EnteredValueTextBoxLabel.ForeColor = SystemColors.WindowText;
            _EnteredValueTextBoxLabel.Location = new Point(9, 7);
            _EnteredValueTextBoxLabel.Name = "_EnteredValueTextBoxLabel";
            _EnteredValueTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _EnteredValueTextBoxLabel.Size = new Size(91, 17);
            _EnteredValueTextBoxLabel.TabIndex = 25;
            _EnteredValueTextBoxLabel.Text = "Enter a Value: ";
            _EnteredValueTextBoxLabel.TextAlign = ContentAlignment.BottomLeft;
            // 
            // _ValidationErrorProvider
            // 
            _ValidationErrorProvider.ContainerControl = this;
            // 
            // TextInputBox
            // 
            AcceptButton = __AcceptButton;
            AutoScaleDimensions = new SizeF(7.0f, 17.0f);
            CancelButton = __CancelButton;
            ClientSize = new Size(203, 95);
            ControlBox = false;
            Controls.Add(__EnteredValueTextBox);
            Controls.Add(__CancelButton);
            Controls.Add(__AcceptButton);
            Controls.Add(_EnteredValueTextBoxLabel);
            Name = "TextInputBox";
            Text = "InputBox";
            ((System.ComponentModel.ISupportInitialize)_ValidationErrorProvider).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        private System.Windows.Forms.TextBox __EnteredValueTextBox;

        private System.Windows.Forms.TextBox _EnteredValueTextBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __EnteredValueTextBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__EnteredValueTextBox != null)
                {
                    __EnteredValueTextBox.TextChanged -= EnteredValueTextBoxTextChanged;
                    __EnteredValueTextBox.Validating -= EnteredValueTextBoxValidating;
                    __EnteredValueTextBox.Validated -= EnteredValueTextBoxValidated;
                }

                __EnteredValueTextBox = value;
                if (__EnteredValueTextBox != null)
                {
                    __EnteredValueTextBox.TextChanged += EnteredValueTextBoxTextChanged;
                    __EnteredValueTextBox.Validating += EnteredValueTextBoxValidating;
                    __EnteredValueTextBox.Validated += EnteredValueTextBoxValidated;
                }
            }
        }

        private System.Windows.Forms.Button __CancelButton;

        private System.Windows.Forms.Button _CancelButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __CancelButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__CancelButton != null)
                {
                    __CancelButton.Click -= CancelButtonClick;
                }

                __CancelButton = value;
                if (__CancelButton != null)
                {
                    __CancelButton.Click += CancelButtonClick;
                }
            }
        }

        private System.Windows.Forms.Button __AcceptButton;

        private System.Windows.Forms.Button _AcceptButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __AcceptButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__AcceptButton != null)
                {
                    __AcceptButton.Click -= AcceptButtonClick;
                }

                __AcceptButton = value;
                if (__AcceptButton != null)
                {
                    __AcceptButton.Click += AcceptButtonClick;
                }
            }
        }

        private System.Windows.Forms.Label _EnteredValueTextBoxLabel;
        private System.Windows.Forms.ErrorProvider _ValidationErrorProvider;
    }
}