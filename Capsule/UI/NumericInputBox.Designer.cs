using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core
{
    [DesignerGenerated()]
    public partial class NumericInputBox
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            __CancelButton = new System.Windows.Forms.Button();
            __CancelButton.Click += new EventHandler(CancelButton_Click);
            __AcceptButton = new System.Windows.Forms.Button();
            __AcceptButton.Click += new EventHandler(AcceptButton_Click);
            _NumericUpDownLabel = new System.Windows.Forms.Label();
            NumericUpDown = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)NumericUpDown).BeginInit();
            SuspendLayout();
            // 
            // _CancelButton
            // 
            __CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            __CancelButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            __CancelButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            __CancelButton.Location = new System.Drawing.Point(11, 59);
            __CancelButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            __CancelButton.Name = "__CancelButton";
            __CancelButton.Size = new System.Drawing.Size(87, 30);
            __CancelButton.TabIndex = 27;
            __CancelButton.Text = "&Cancel";
            // 
            // _AcceptButton
            // 
            __AcceptButton.Enabled = false;
            __AcceptButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            __AcceptButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            __AcceptButton.Location = new System.Drawing.Point(108, 59);
            __AcceptButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            __AcceptButton.Name = "__AcceptButton";
            __AcceptButton.Size = new System.Drawing.Size(87, 30);
            __AcceptButton.TabIndex = 26;
            __AcceptButton.Text = "&OK";
            // 
            // _NumericUpDownLabel
            // 
            _NumericUpDownLabel.BackColor = System.Drawing.SystemColors.Control;
            _NumericUpDownLabel.Cursor = System.Windows.Forms.Cursors.Default;
            _NumericUpDownLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            _NumericUpDownLabel.ForeColor = System.Drawing.SystemColors.WindowText;
            _NumericUpDownLabel.Location = new System.Drawing.Point(9, 5);
            _NumericUpDownLabel.Name = "_NumericUpDownLabel";
            _NumericUpDownLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _NumericUpDownLabel.Size = new System.Drawing.Size(156, 21);
            _NumericUpDownLabel.TabIndex = 25;
            _NumericUpDownLabel.Text = "Enter a Value: ";
            _NumericUpDownLabel.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // NumericUpDown
            // 
            NumericUpDown.Location = new System.Drawing.Point(11, 27);
            NumericUpDown.Name = "NumericUpDown";
            NumericUpDown.Size = new System.Drawing.Size(183, 25);
            NumericUpDown.TabIndex = 28;
            // 
            // NumericInputBox
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            ClientSize = new System.Drawing.Size(209, 98);
            ControlBox = false;
            Controls.Add(NumericUpDown);
            Controls.Add(__CancelButton);
            Controls.Add(__AcceptButton);
            Controls.Add(_NumericUpDownLabel);
            Name = "NumericInputBox";
            Text = "Numeric Input Box";
            ((System.ComponentModel.ISupportInitialize)NumericUpDown).EndInit();
            ResumeLayout(false);
        }

        private System.Windows.Forms.Button __CancelButton;

        private System.Windows.Forms.Button _CancelButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __CancelButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__CancelButton != null)
                {
                    __CancelButton.Click -= CancelButton_Click;
                }

                __CancelButton = value;
                if (__CancelButton != null)
                {
                    __CancelButton.Click += CancelButton_Click;
                }
            }
        }

        private System.Windows.Forms.Button __AcceptButton;

        private System.Windows.Forms.Button _AcceptButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __AcceptButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__AcceptButton != null)
                {
                    __AcceptButton.Click -= AcceptButton_Click;
                }

                __AcceptButton = value;
                if (__AcceptButton != null)
                {
                    __AcceptButton.Click += AcceptButton_Click;
                }
            }
        }

        private System.Windows.Forms.Label _NumericUpDownLabel;

        /// <summary>   The numeric up down control. </summary>
        public System.Windows.Forms.NumericUpDown NumericUpDown;
    }
}
