using System;

namespace isr.Core
{
    /// <summary> Dialog for print preview and print. </summary>
    /// <remarks> David, 2020-09-10. </remarks>
    public class PrintPreviewDialog : FileDialogBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs this class. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        public PrintPreviewDialog() : base()
        {
        }

        #endregion 

        #region " PRINT PREVIEW DIALOG IMPLEMENTATION "

        /// <summary> Opens the print preview dialog. </summary>
        /// <remarks> David, 2020-09-10. </remarks>
        /// <param name="document"> The document. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        public virtual void ShowDialog( System.Drawing.Printing.PrintDocument document )
        {
            string activity = string.Empty;
            try
            {
                activity = "preparing print preview dialog";
                using var ppd = new MyPrintPreviewDialog() { Document = document };
                activity = "showing print preview dialog";
                _ = ppd.ShowDialog();
            }
            catch ( Exception ex )
            {
                _ = MyMessageBox.ShowDialog( $"Exception {activity};. {ex}", "Print Dialog Exception", MyMessageBoxIcon.Error, new MyDialogResult[] { MyDialogResult.Ok } );
            }
        }

        #endregion 

    }
}
