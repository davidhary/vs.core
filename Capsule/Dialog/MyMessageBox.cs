using System;
using System.Collections.Generic;

using Microsoft.SqlServer.MessageBox;

namespace isr.Core
{

    /// <summary>
    /// Extends the <see cref="ExceptionMessageBox">Exception message dialog</see>.
    /// </summary>
    /// <remarks> David, 2014-02-14. </remarks>
    public class MyMessageBox
    {

        #region " CONSTRUCTION "

        /// <summary> Gets the exception message box. </summary>
        /// <value> The exception message box. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private MessageBox ExceptionMessageBox { get; }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="exception"> The exception. </param>
        public MyMessageBox( Exception exception ) : base()
        {
            this.ExceptionMessageBox = new MessageBox( exception );
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="text">    The text. </param>
        /// <param name="caption"> The caption. </param>
        public MyMessageBox( string text, string caption ) : base()
        {
            this.ExceptionMessageBox = new MessageBox( text, caption );
        }

        #endregion 

        #region " FRIEND SHARED "

        /// <summary> Builds message box icon symbol hash. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <returns>
        /// A <see cref="Dictionary{MyMessageBoxIcon, ExceptionMessageBoxSymbol}">dictionary</see>.
        /// </returns>
        private static IDictionary<MyMessageBoxIcon, ExceptionMessageBoxSymbol> BuildMessageBoxIconSymbolHash()
        {
            var dix2 = new Dictionary<MyMessageBoxIcon, ExceptionMessageBoxSymbol>();
            var dix3 = dix2;
            // same as information: dix3.Add(MessageBoxIcon.Asterisk, ExceptionMessageBoxSymbol.Asterisk)
            // same as warning: dix3.Add(MessageBoxIcon.Exclamation, ExceptionMessageBoxSymbol.Exclamation)
            dix3.Add( MyMessageBoxIcon.Error, ExceptionMessageBoxSymbol.Error );
            // same as error: dix3.Add(MessageBoxIcon.Hand, ExceptionMessageBoxSymbol.Hand)
            dix3.Add( MyMessageBoxIcon.Information, ExceptionMessageBoxSymbol.Information );
            dix3.Add( MyMessageBoxIcon.None, ExceptionMessageBoxSymbol.None );
            dix3.Add( MyMessageBoxIcon.Question, ExceptionMessageBoxSymbol.Question );
            // same as error: dix3.Add(MessageBoxIcon.Stop, ExceptionMessageBoxSymbol.Stop)
            dix3.Add( MyMessageBoxIcon.Warning, ExceptionMessageBoxSymbol.Warning );
            return dix2;
        }

        private static System.Collections.Generic.IDictionary<isr.Core.MyMessageBoxIcon, Microsoft.SqlServer.MessageBox.ExceptionMessageBoxSymbol> _Hash;

        /// <summary> Converts a value to a symbol. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> Symbol. </returns>
        public static ExceptionMessageBoxSymbol ToSymbol( MyMessageBoxIcon value )
        {
            if ( _Hash is null )
            {
                _Hash = BuildMessageBoxIconSymbolHash();
            }

            return _Hash.ContainsKey( value ) ? _Hash[value] : ExceptionMessageBoxSymbol.Information;
        }

        #endregion 

        #region " SHARED "

        /// <summary>
        /// Synchronously Invokes the exception display on the apartment thread to permit using the
        /// clipboard.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="exception"> The exception. </param>
        /// <returns> <see cref="MyDialogResult">Dialog result</see>. </returns>
        public static MyDialogResult ShowDialog( Exception exception )
        {
            var box = new MessageBox( exception );
            return box.ShowDialog();
        }

        /// <summary>
        /// Synchronously Invokes the exception display on the apartment thread to permit using the
        /// clipboard.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="text">    The text. </param>
        /// <param name="caption"> The caption. </param>
        /// <returns> <see cref="MyDialogResult">Dialog result</see>. </returns>
        public static MyDialogResult ShowDialog( string text, string caption )
        {
            var box = new MessageBox( text, caption );
            return box.ShowDialog();
        }

        /// <summary>
        /// Synchronously Invokes the exception display on the apartment thread to permit using the
        /// clipboard.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="exception">     The exception. </param>
        /// <param name="icon">          The icon. </param>
        /// <param name="dialogResults"> The dialog results. </param>
        /// <returns> <see cref="MyDialogResult">Dialog result</see>. </returns>
        public static MyDialogResult ShowDialog( Exception exception, MyMessageBoxIcon icon, MyDialogResult[] dialogResults )
        {
            if ( dialogResults is null )
            {
                throw new ArgumentNullException( nameof( dialogResults ) );
            }

            var box = new MessageBox( exception );
            var buttonText = new List<string>();
            foreach ( MyDialogResult d in dialogResults )
            {
                buttonText.Add( d.ToString() );
            }

            return box.ShowDialog( buttonText.ToArray(), ToSymbol( icon ), ExceptionMessageBoxDefaultButton.Button1, dialogResults );
        }

        /// <summary>
        /// Synchronously Invokes the exception display on the apartment thread to permit using the
        /// clipboard.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="text">          The exception. </param>
        /// <param name="caption">       The caption. </param>
        /// <param name="icon">          The icon. </param>
        /// <param name="dialogResults"> The dialog results. </param>
        /// <returns> <see cref="MyDialogResult">Dialog result</see>. </returns>
        public static MyDialogResult ShowDialog( string text, string caption, MyMessageBoxIcon icon, MyDialogResult[] dialogResults )
        {
            if ( dialogResults is null )
            {
                throw new ArgumentNullException( nameof( dialogResults ) );
            }

            var box = new MessageBox( text, caption );
            var buttonText = new List<string>();
            foreach ( MyDialogResult d in dialogResults )
            {
                buttonText.Add( d.ToString() );
            }

            return box.ShowDialog( buttonText.ToArray(), ToSymbol( icon ), ExceptionMessageBoxDefaultButton.Button1, dialogResults );
        }

        /// <summary> Displays the message box. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="exception"> The exception. </param>
        /// <param name="icon">      The icon. </param>
        /// <returns> <see cref="MyDialogResult">Dialog result</see>. </returns>
        public static MyDialogResult ShowDialogAbortIgnore( Exception exception, MyMessageBoxIcon icon )
        {
            return ShowDialog( exception, icon, new MyDialogResult[] { MyDialogResult.Abort, MyDialogResult.Ignore } );
        }

        /// <summary> Displays the message box. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="exception"> The exception. </param>
        /// <returns> <see cref="MyDialogResult">Dialog result</see>. </returns>
        public static MyDialogResult ShowDialogAbortIgnore( Exception exception )
        {
            return ShowDialog( exception, MyMessageBoxIcon.Error, new MyDialogResult[] { MyDialogResult.Abort, MyDialogResult.Ignore } );
        }

        /// <summary> Displays the message box. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="text">    The text. </param>
        /// <param name="caption"> The caption. </param>
        /// <param name="icon">    The icon. </param>
        /// <returns> <see cref="MyDialogResult">Dialog result</see>. </returns>
        public static MyDialogResult ShowDialogAbortIgnore( string text, string caption, MyMessageBoxIcon icon )
        {
            return ShowDialog( text, caption, icon, new MyDialogResult[] { MyDialogResult.Abort, MyDialogResult.Ignore } );
        }

        /// <summary> Displays the message box. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="exception"> The exception. </param>
        /// <param name="icon">      The icon. </param>
        /// <returns>
        /// Either <see cref="MyDialogResult.Ignore">ignore</see> or
        /// <see cref="MyDialogResult.Ok">Okay</see>.
        /// </returns>
        public static MyDialogResult ShowDialogIgnoreExit( Exception exception, MyMessageBoxIcon icon )
        {
            var box = new MyMessageBox( exception );
            return box.ShowDialogIgnoreExit( icon );
        }

        /// <summary> Displays the message box. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="text">    The text. </param>
        /// <param name="caption"> The caption. </param>
        /// <param name="icon">    The icon. </param>
        /// <returns>
        /// Either <see cref="MyDialogResult.Ignore">ignore</see> or
        /// <see cref="MyDialogResult.Ok">Okay</see>.
        /// </returns>
        public static MyDialogResult ShowDialogIgnoreExit( string text, string caption, MyMessageBoxIcon icon )
        {
            var box = new MyMessageBox( text, caption );
            return box.ShowDialogIgnoreExit( icon );
        }

        /// <summary> Displays the message box. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="exception"> The exception. </param>
        /// <param name="icon">      The icon. </param>
        /// <returns> <see cref="MyDialogResult.Ok">Okay</see>. </returns>
        public static MyDialogResult ShowDialogExit( Exception exception, MyMessageBoxIcon icon )
        {
            var box = new MyMessageBox( exception );
            return box.ShowDialogExit( icon );
        }

        /// <summary> Displays the message box. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="text">    The text. </param>
        /// <param name="caption"> The caption. </param>
        /// <param name="icon">    The icon. </param>
        /// <returns> <see cref="MyDialogResult.Ok">Okay</see>. </returns>
        public static MyDialogResult ShowDialogExit( string text, string caption, MyMessageBoxIcon icon )
        {
            var box = new MyMessageBox( text, caption );
            return box.ShowDialogExit( icon );
        }

        /// <summary> Shows the okay dialog. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="text">    The text. </param>
        /// <param name="caption"> The caption. </param>
        /// <param name="icon">    The icon. </param>
        /// <returns> A DialogResult. </returns>
        public static MyDialogResult ShowDialogOkay( string text, string caption, MyMessageBoxIcon icon )
        {
            var box = new MyMessageBox( text, caption );
            return box.ShowDialogExit( icon );
        }

        /// <summary> Shows the okay dialog. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="text">    The text. </param>
        /// <param name="caption"> The caption. </param>
        /// <returns> A DialogResult. </returns>
        public static MyDialogResult ShowDialogOkay( string text, string caption )
        {
            return ShowDialogOkay( text, caption, MyMessageBoxIcon.Information );
        }

        /// <summary> Shows the okay/cancel dialog. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="text">    The text. </param>
        /// <param name="caption"> The caption. </param>
        /// <param name="icon">    The icon. </param>
        /// <returns> A DialogResult. </returns>
        public static MyDialogResult ShowDialogOkayCancel( string text, string caption, MyMessageBoxIcon icon )
        {
            return ShowDialog( text, caption, icon, new MyDialogResult[] { MyDialogResult.Ok, MyDialogResult.Cancel } );
        }

        /// <summary> Shows the okay/cancel dialog. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="text">    The text. </param>
        /// <param name="caption"> The caption. </param>
        /// <returns> A DialogResult. </returns>
        public static MyDialogResult ShowDialogOkayCancel( string text, string caption )
        {
            return ShowDialogOkayCancel( text, caption, MyMessageBoxIcon.Information );
        }

        /// <summary> Shows the Cancel/Okay dialog. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="text">    The text. </param>
        /// <param name="caption"> The caption. </param>
        /// <param name="icon">    The icon. </param>
        /// <returns> A DialogResult. </returns>
        public static MyDialogResult ShowDialogCancelOkay( string text, string caption, MyMessageBoxIcon icon )
        {
            return ShowDialog( text, caption, icon, new MyDialogResult[] { MyDialogResult.Cancel, MyDialogResult.Ok } );
        }

        /// <summary> Shows the Cancel/Okay dialog. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="text">    The text. </param>
        /// <param name="caption"> The caption. </param>
        /// <returns> A DialogResult. </returns>
        public static MyDialogResult ShowDialogCancelOkay( string text, string caption )
        {
            return ShowDialogCancelOkay( text, caption, MyMessageBoxIcon.Information );
        }

        /// <summary> Shows the yes/no dialog. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="text">    The text. </param>
        /// <param name="caption"> The caption. </param>
        /// <param name="icon">    The icon. </param>
        /// <returns> A DialogResult. </returns>
        public static MyDialogResult ShowDialogYesNo( string text, string caption, MyMessageBoxIcon icon )
        {
            return ShowDialog( text, caption, icon, new MyDialogResult[] { MyDialogResult.Yes, MyDialogResult.No } );
        }

        /// <summary> Shows the yes/no dialog. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="text">    The text. </param>
        /// <param name="caption"> The caption. </param>
        /// <returns> A DialogResult. </returns>
        public static MyDialogResult ShowDialogYesNo( string text, string caption )
        {
            return ShowDialogYesNo( text, caption, MyMessageBoxIcon.Question );
        }

        /// <summary> Shows the No/Yes dialog. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="text">    The text. </param>
        /// <param name="caption"> The caption. </param>
        /// <param name="icon">    The icon. </param>
        /// <returns> A DialogResult. </returns>
        public static MyDialogResult ShowDialogNoYes( string text, string caption, MyMessageBoxIcon icon )
        {
            return ShowDialog( text, caption, icon, new MyDialogResult[] { MyDialogResult.No, MyDialogResult.Yes } );
        }

        /// <summary> Shows the No/Yes dialog. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="text">    The text. </param>
        /// <param name="caption"> The caption. </param>
        /// <returns> A DialogResult. </returns>
        public static MyDialogResult ShowDialogNoYes( string text, string caption )
        {
            return ShowDialogNoYes( text, caption, MyMessageBoxIcon.Question );
        }

        #endregion 

        #region " SHOW DIALOG "

        /// <summary> Shows the exception display. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <returns> <see cref="MyDialogResult">Dialog result</see>. </returns>
        /// <example> Example 1: Simple Message <code>
        /// Dim box As MyMessageBox = New MyMessageBox(message)
        /// me.ShowDialog(owner)      </code></example>
        /// <example> Example 2: Message box with exception message. <code>
        /// Dim box As MyMessageBox = New MyMessageBox(exception)
        /// me.ShowDialog(owner)      </code></example>
        public MyDialogResult ShowDialog()
        {
            return this.ExceptionMessageBox.ShowDialog();
        }

        #endregion 


        #region " SHOW DIALOG - PREDEFINED BUTTONS "

        /// <summary> Displays the message box. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="icon"> The icon. </param>
        /// <returns> <see cref="MyDialogResult">Dialog result</see>. </returns>
        public MyDialogResult ShowDialogAbortIgnore( MyMessageBoxIcon icon )
        {
            return this.ExceptionMessageBox.ShowDialog( new string[] { "Abort", "Ignore" }, ToSymbol( icon ), ExceptionMessageBoxDefaultButton.Button1, new MyDialogResult[] { MyDialogResult.Abort, MyDialogResult.Ignore } );
        }

        /// <summary> Displays the message box. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="icon"> The icon. </param>
        /// <returns>
        /// Either <see cref="MyDialogResult.Ignore">ignore</see> or
        /// <see cref="MyDialogResult.Ok">Okay</see>.
        /// </returns>
        public MyDialogResult ShowDialogIgnoreExit( MyMessageBoxIcon icon )
        {
            return this.ExceptionMessageBox.ShowDialog( new string[] { "Ignore", "Exit" }, ToSymbol( icon ), ExceptionMessageBoxDefaultButton.Button1, new MyDialogResult[] { MyDialogResult.Ignore, MyDialogResult.Ok } );
        }

        /// <summary> Displays the message box. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="icon"> The icon. </param>
        /// <returns> <see cref="MyDialogResult.Ok">Okay</see>. </returns>
        public MyDialogResult ShowDialogExit( MyMessageBoxIcon icon )
        {
            return this.ExceptionMessageBox.ShowDialog( new string[] { "Exit" }, ToSymbol( icon ), ExceptionMessageBoxDefaultButton.Button1, new MyDialogResult[] { MyDialogResult.Ok } );
        }

        #endregion 

    }

    /// <summary> Values that represent dialog results. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    public enum MyDialogResult
    {

        /// <summary> An enum constant representing the none option. </summary>
        None = System.Windows.Forms.DialogResult.None,

        /// <summary> An enum constant representing the ok option. </summary>
        Ok = System.Windows.Forms.DialogResult.OK,

        /// <summary> An enum constant representing the cancel option. </summary>
        Cancel = System.Windows.Forms.DialogResult.Cancel,

        /// <summary> An enum constant representing the abort option. </summary>
        Abort = System.Windows.Forms.DialogResult.Abort,

        /// <summary> An enum constant representing the retry option. </summary>
        Retry = System.Windows.Forms.DialogResult.Retry,

        /// <summary> An enum constant representing the ignore option. </summary>
        Ignore = System.Windows.Forms.DialogResult.Ignore,

        /// <summary> An enum constant representing the yes option. </summary>
        Yes = System.Windows.Forms.DialogResult.Yes,

        /// <summary> An enum constant representing the no option. </summary>
        No = System.Windows.Forms.DialogResult.No
    }

    /// <summary> Values that represent my message box icons. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1027:Mark enums with FlagsAttribute", Justification = "<Pending>" )]
    public enum MyMessageBoxIcon
    {

        /// <summary> An enum constant representing the none option. </summary>
        None = System.Windows.Forms.MessageBoxIcon.None,

        /// <summary> An enum constant representing the asterisk option. </summary>
        Asterisk = System.Windows.Forms.MessageBoxIcon.Asterisk,

        /// <summary> An enum constant representing the error] option. </summary>
        Error = System.Windows.Forms.MessageBoxIcon.Error,

        /// <summary> An enum constant representing the exclamation option. </summary>
        Exclamation = System.Windows.Forms.MessageBoxIcon.Exclamation,

        /// <summary> An enum constant representing the hand option. </summary>
        Hand = System.Windows.Forms.MessageBoxIcon.Hand,

        /// <summary> An enum constant representing the information option. </summary>
        Information = System.Windows.Forms.MessageBoxIcon.Information,

        /// <summary> An enum constant representing the question option. </summary>
        Question = System.Windows.Forms.MessageBoxIcon.Question,

        /// <summary> An enum constant representing the stop] option. </summary>
        Stop = System.Windows.Forms.MessageBoxIcon.Stop,

        /// <summary> An enum constant representing the warning option. </summary>
        Warning = System.Windows.Forms.MessageBoxIcon.Warning
    }
}
