using System;
using System.Linq;

using Microsoft.SqlServer.MessageBox;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core
{

    /// <summary>
    /// Encapsulates the <see cref="Microsoft.SqlServer.MessageBox.ExceptionMessageBox"/> exception
    /// message box.
    /// </summary>
    /// <remarks> David, 2019-01-17. </remarks>
    public class MessageBox : ExceptionMessageBox
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 202-09-12. </remarks>
        public MessageBox()
        {
            this.OnCopyToClipboard += this.MyExceptionMessageBox_OnCopyToClipboard;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="text">    The text. </param>
        /// <param name="caption"> The caption. </param>
        public MessageBox( string text, string caption ) : base( text, caption )
        {
            this.OnCopyToClipboard += this.MyExceptionMessageBox_OnCopyToClipboard;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="exception"> The exception. </param>
        public MessageBox( Exception exception ) : base( exception )
        {
            this.OnCopyToClipboard += this.MyExceptionMessageBox_OnCopyToClipboard;
        }

        private void MyExceptionMessageBox_OnCopyToClipboard( object sender, CopyToClipboardEventArgs e )
        {
            SafeClipboardSetDataObject.SetDataObject( e.ClipboardText );
            e.EventHandled = true;
        }

        /// <summary> Converts a value to a dialog result. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> Value as a DialogResult. </returns>
        internal static MyDialogResult ToDialogResult( System.Windows.Forms.DialogResult value )
        {
            return ( MyDialogResult ) Conversions.ToInteger( value );
        }

        /// <summary> Converts a value to a message box icon. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> Value as a MessageBoxIcon. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private static MyMessageBoxIcon ToMessageBoxIcon( System.Windows.Forms.MessageBoxIcon value )
        {
            return ( MyMessageBoxIcon ) Conversions.ToInteger( value );
        }

        /// <summary> Displays the message box. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <returns> <see cref="MyDialogResult">Dialog result</see>. </returns>
        public MyDialogResult ShowDialog()
        {
            var r = MyDialogResult.Abort;
            System.Threading.Thread oThread;
            oThread = new System.Threading.Thread( new System.Threading.ThreadStart( () => r = MessageBox.ToDialogResult( this.Show( default ) ) ) );
            oThread.SetApartmentState( System.Threading.ApartmentState.STA );
            oThread.Start();
            oThread.Join();
            while ( oThread.IsAlive )
            {
                System.Threading.Thread.Sleep( 1000 );
            }

            return r;
        }

        /// <summary> Displays the message box. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <exception cref="InvalidOperationException">   Thrown when the requested operation is
        /// invalid. </exception>
        /// <param name="buttonText">    The buttons text. </param>
        /// <param name="symbol">        The symbol. </param>
        /// <param name="defaultButton"> The default button. </param>
        /// <param name="dialogResults"> The dialog results corresponding to the
        /// <paramref name="buttonText">buttons</paramref>. </param>
        /// <returns> <see cref="MyDialogResult">Dialog result</see>. </returns>
        /// <example>
        /// Example 1: Simple Message
        /// <code>
        /// Dim box As ExceptionMessageBox = New ExceptionMessageBox(exception)
        /// box.InvokeShow(owner, New String(){"A","B"},ExceptionMessageBoxSymbol.Asterisk,
        /// ExceptionMessageBoxDefaultButton.Button1)
        /// </code>
        /// </example>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public MyDialogResult ShowDialog( string[] buttonText, ExceptionMessageBoxSymbol symbol, ExceptionMessageBoxDefaultButton defaultButton, MyDialogResult[] dialogResults )
        {
            if ( buttonText is null )
            {
                throw new ArgumentNullException( nameof( buttonText ) );
            }
            else if ( buttonText.Count() == 0 || buttonText.Count() > 5 )
            {
                throw new ArgumentOutOfRangeException( nameof( buttonText ), "Must have between 1 and 5 values" );
            }
            else if ( dialogResults is null )
            {
                throw new ArgumentNullException( nameof( dialogResults ) );
            }
            else if ( dialogResults.Count() == 0 || dialogResults.Count() > 5 )
            {
                throw new ArgumentOutOfRangeException( nameof( dialogResults ), "Must have between 1 and 5 values" );
            }
            else if ( dialogResults.Count() != buttonText.Count() )
            {
                throw new ArgumentOutOfRangeException( nameof( dialogResults ), "Must have the same count as button text" );
            }

            // Set the names of the custom buttons.
            switch ( buttonText.Count() )
            {
                case 1:
                    {
                        this.SetButtonText( buttonText[0] );
                        break;
                    }

                case 2:
                    {
                        this.SetButtonText( buttonText[0], buttonText[1] );
                        break;
                    }

                case 3:
                    {
                        this.SetButtonText( buttonText[0], buttonText[1], buttonText[2] );
                        break;
                    }

                case 4:
                    {
                        this.SetButtonText( buttonText[0], buttonText[1], buttonText[2], buttonText[3] );
                        break;
                    }

                case 5:
                    {
                        this.SetButtonText( buttonText[0], buttonText[1], buttonText[2], buttonText[4] );
                        break;
                    }
            }

            this.DefaultButton = defaultButton;
            this.Symbol = symbol;
            this.Buttons = ExceptionMessageBoxButtons.Custom;
            _ = this.ShowDialog();
            switch ( this.CustomDialogResult )
            {
                case ExceptionMessageBoxDialogResult.Button1:
                    {
                        return dialogResults[0];
                    }

                case ExceptionMessageBoxDialogResult.Button2:
                    {
                        return dialogResults[1];
                    }

                case ExceptionMessageBoxDialogResult.Button3:
                    {
                        return dialogResults[2];
                    }

                case ExceptionMessageBoxDialogResult.Button4:
                    {
                        return dialogResults[3];
                    }

                case ExceptionMessageBoxDialogResult.Button5:
                    {
                        return dialogResults[4];
                    }

                default:
                    {
                        throw new InvalidOperationException(
                            $"Failed displaying the message box with {nameof( Microsoft.SqlServer.MessageBox.ExceptionMessageBox.CustomDialogResult )}={this.CustomDialogResult}" );
                    }
            }
        }
    }
}
