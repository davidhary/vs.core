
namespace isr.Core
{
    /// <summary> Dialog for selecting a file to open. </summary>
    /// <remarks> David, 2019-01-22. </remarks>
    public class OpenFileDialog : FileDialogBase
    {


        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs this class. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        public OpenFileDialog() : base()
        {
        }

        #endregion 


        #region " OPEN DIALOG IMPLEMENTATION "

        /// <summary> Opens the File Open dialog box and selects a file name. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <returns> A Windows.Forms.DialogResult. </returns>
        public virtual bool TrySelectFile()
        {
            using var fileDialog = new System.Windows.Forms.OpenFileDialog {
                // Use the common dialog box
                CheckFileExists = true,
                CheckPathExists = true,
                Title = this.FileDialogTitle,
                FileName = this.FilePathName
            };
            var fi = new System.IO.FileInfo( this.FilePathName );
            if ( fi.Directory.Exists )
            {
                fileDialog.InitialDirectory = fi.DirectoryName;
            }

            fileDialog.RestoreDirectory = true;
            fileDialog.Filter = this.FileDialogFilter;
            fileDialog.DefaultExt = this.FileExtension;
            bool result = System.Windows.Forms.DialogResult.OK == fileDialog.ShowDialog();
            if ( result )
            {
                this.FilePathName = fileDialog.FileName;
            }

            return result;
        }

        /// <summary> Try find file. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="fileName"> Filename of the file. </param>
        /// <returns> A String if file found or empty if not. </returns>
        public static string TryFindFile( string fileName )
        {
            var dialog = new OpenFileDialog() { FilePathName = fileName };
            if ( !dialog.FileExists() )
            {
                fileName = dialog.TrySelectFile() ? dialog.FilePathName : string.Empty;
            }

            return fileName;
        }

        #endregion 

    }
}
