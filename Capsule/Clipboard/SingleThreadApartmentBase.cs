using System;
using System.Threading;

namespace isr.Core
{

    /// <summary> Single thread apartment base. </summary>
    /// <remarks>
    /// David, 2015-03-24
    /// http://stackoverflow.com/questions/899350/how-to-copy-the-contents-of-a-string-to-the-clipboard-in-c.
    /// </remarks>
    public abstract class SingleThreadApartmentBase
    {

        #region " CONSTRUCTION "

        /// <summary> Specialized default constructor for use only by derived class. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        protected SingleThreadApartmentBase() : base()
        {
        }

        #endregion 

        #region " WORKERS "

        /// <summary> Executes the work as defined by the inheriting class. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        public void Go()
        {
            var thread = new Thread( new ThreadStart( this.DoWork ) );
            thread.SetApartmentState( ApartmentState.STA );
            thread.Start();
        }

        /// <summary> The complete reset event. Notifies one or more waiting threads that an event has occurred. </summary>
        /// <remarks> is used to block and release threads manually. It is created in the non-signaled state.
        /// <c>
        /// https://msdn.microsoft.com/en-us/library/system.threading.manualresetevent%28v=vs.110%29.aspx
        /// </c>
        /// It seems that this does not do anything in this case as the event is not set to block, e.g., Wait One.
        /// </remarks>
        private readonly ManualResetEvent _Complete = new ManualResetEvent( false );

        /// <summary> The thread entry method. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void DoWork()
        {
            try
            {
                // sets the state of the event to not signaled causing threads to block
                _ = this._Complete.Reset();
                this.Work();
            }
            catch ( Exception ex )
            {
                if ( this.RetryWorkOnFailed )
                {
                    try
                    {
                        Thread.Sleep( 1000 );
                        this.Work();
                    }
                    catch
                    {
                        // ex from first exception
                        My.MyProject.Application.Log.WriteException( ex );
                    }
                }
                else
                {
                    throw;
                }
            }
            finally
            {
                // sets the state of the event to signaled, allowing waiting events to proceed 
                _ = this._Complete.Set();
            }
        }

        /// <summary> Gets or sets a value indicating whether to retry work on failed. </summary>
        /// <value> <c>true</c> if retry work on failed; otherwise <c>false</c> </value>
        public bool RetryWorkOnFailed { get; set; }

        /// <summary> Implemented in the inheriting class to do actual work. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        protected abstract void Work();

        #endregion 

    }
}
