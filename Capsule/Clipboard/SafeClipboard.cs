using System.Windows.Forms;

namespace isr.Core
{

    /// <summary> Safe clipboard set data object. </summary>
    /// <remarks>
    /// David, 2015-03-24
    /// http://stackoverflow.com/questions/899350/how-to-copy-the-contents-of-a-string-to-the-clipboard-in-c.
    /// </remarks>
    public class SafeClipboardSetDataObject : SingleThreadApartmentBase

    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="format"> Describes the <see cref="DataFormats">format</see> of the specified data. </param>
        /// <param name="data">   The data. </param>
        public SafeClipboardSetDataObject( string format, object data ) : base()
        {
            this._Format = format;
            this._Data = data;
        }

        /// <summary> Describes the format to use. </summary>
        private readonly string _Format;

        /// <summary> The data. </summary>
        private readonly object _Data;

        /// <summary> Implemented in this class to do actual work. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        protected override void Work()
        {
            object obj = new DataObject( this._Format, this._Data );
            Clipboard.SetDataObject( obj, true );
        }

        /// <summary> Copies text to the clipboard. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="text"> The text. </param>
        public static void SetDataObject( string text )
        {
            var scr = new SafeClipboardSetDataObject( DataFormats.Text, text );
            scr.Go();
        }
    }
}
