using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.Core.Capsule.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.Core.Capsule.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.Core.Capsule.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]

[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( isr.Core.Capsule.My.MyLibrary.TestAssemblyStrongName )]
