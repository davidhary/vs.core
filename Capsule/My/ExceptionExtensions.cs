using System;

namespace isr.Core.Capsule.ExceptionExtensions
{
    /// <summary> Adds exception data for building the exception full blown report. </summary>
    /// <license>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    /// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    /// NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    /// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    /// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    /// </license>
    public static class ExceptionExtensionMethods
    {

        /// <summary> Adds the <paramref name="exception"/> data to <paramref name="value"/> exception. </summary>
        /// <remarks>
        /// For more info on the external exceptions see:
        /// http://msdn.microsoft.com/en-us/library/system.runtime.interopStandard.sehexception.aspx.
        /// </remarks>
        /// <param name="value">     The value. </param>
        /// <param name="exception"> The exception. </param>
        /// <returns> <c>true</c> if it <see cref="Exception"/> is not nothing; otherwise <c>false</c> </returns>
        public static bool AddExceptionData( Exception value, System.Runtime.InteropServices.ExternalException exception )
        {
            if ( value is object && exception is object )
            {
                value.Data.Add( $"{value.Data.Count}-External.Error.Code", $"{exception.ErrorCode}" );
            }

            return exception is object;
        }

        /// <summary> Adds the <paramref name="exception"/> data to <paramref name="value"/> exception. </summary>
        /// <param name="value">     The value. </param>
        /// <param name="exception"> The exception. </param>
        /// <returns> <c>true</c> if it <see cref="Exception"/> is not nothing; otherwise <c>false</c> </returns>
        public static bool AddExceptionData( Exception value, ArgumentOutOfRangeException exception )
        {
            if ( value is object && exception is object )
            {
                value.Data.Add( $"{value.Data.Count}-Name+Value", $"{exception.ParamName}={exception.ActualValue}" );
            }

            return exception is object;
        }

        /// <summary> Adds the <paramref name="exception"/> data to <paramref name="value"/> exception. </summary>
        /// <param name="value">     The value. </param>
        /// <param name="exception"> The exception. </param>
        /// <returns> <c>true</c> if it <see cref="Exception"/> is not nothing; otherwise <c>false</c> </returns>
        public static bool AddExceptionData( Exception value, ArgumentException exception )
        {
            if ( value is object && exception is object )
            {
                value.Data.Add( $"{value.Data.Count}-Name", exception.ParamName );
            }

            return exception is object;
        }

        /// <summary> Adds exception data from the specified exception. </summary>
        /// <param name="exception"> The exception. </param>
        /// <returns> <c>true</c> if exception was added; otherwise <c>false</c> </returns>
        public static bool AddExceptionData( Exception exception )
        {
            return AddExceptionData( exception, exception as ArgumentOutOfRangeException ) ||
                   AddExceptionData( exception, exception as ArgumentException ) ||
                   AddExceptionData( exception, exception as System.Runtime.InteropServices.ExternalException );
        }

        private static bool AddExceptionDataThis( Exception exception )
        {
            return AddExceptionData( exception ) || isr.Core.WinForms.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( exception );
        }

        /// <summary> Converts a value to a full blown string. </summary>
        /// <param name="value"> The value. </param>
        /// <returns> Value as a String. </returns>
        public static string ToFullBlownString( this Exception value )
        {
            return value.ToFullBlownString( int.MaxValue );
        }

        /// <summary> Converts this object to a full blown string. </summary>
        /// <param name="value"> The value. </param>
        /// <param name="level"> The level. </param>
        /// <returns> The given data converted to a String. </returns>
        public static string ToFullBlownString( this Exception value, int level )
        {
            return isr.Core.ExceptionExtensions.ExceptionExtensionMethods.ToFullBlownString( value, level, AddExceptionDataThis );
        }

    }

}
