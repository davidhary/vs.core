
namespace isr.Core.Capsule.My
{

    /// <summary> Provides assembly information for the class library. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    public sealed partial class MyLibrary
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        private MyLibrary() : base()
        {
        }

        /// <summary> Identifier for the trace event. </summary>
        public const int TraceEventId = 0x103;

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "Core Message Box Library";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "Core Message Box Library";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "Core.Message.Box";

        /// <summary> The Strong Name of the test assembly. </summary>
        public const string TestAssemblyStrongName = "isr.Core.CapsuleTests,PublicKey=" + isr.Core.My.SolutionInfo.PublicKey;

    }
}
