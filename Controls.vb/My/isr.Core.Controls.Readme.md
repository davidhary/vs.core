## ISR Core Controls<sub>&trade;</sub>: Core Controls Class Library
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Revision History [](#){name=Revision-History}

*5.0.7445 05/20/20*  
Relaxes selector name validations to allow opening resources 
not registered with a resource manager.

*5.0.7280 12/07/19*  
Uses now Services, Model and Constructs created from
agnostic.

*3.4.6944 01/05/19*  
Adds binding management to base controls.

*3.2.6421 07/31/17*  
Adds read only functionality to the toggle image.

*3.2.6414 07/24/17*  
Adds image toggle. Render disabled state for the image
buttons.

*3.1.6120 10/03/16*  
Adds custom progress with text and percent value
display.

*3.0.6034 07/09/16*  
Adds the CP Wizard controls. Adds the console control.

*3.0.5906 03/03/16*  
Adds a custom progress bar with text overlay.

*3.0.5866 01/23/16*  
Updates to .NET 4.6.1

*2.1.5747 09/26/15 Adds custom tab control.

*2.1.5733 09/12/15 Adds toggle switch control.

*2.1.5661 07/02/15 Updates images and icons of drop down text box and
info provider.

*2.1.5600 05/02/15 Adds Cool Print Preview dialog.

*2.1.5593 04/25/15 Adds extended tab control with hidden tab headers.

*2.1.5572 04/03/15 Adds Info Provider.

*2.1.5538 03/01/15 Adds Selector combo box control.

*2.1.5399 10/14/14*  
Adds shape control.

*2.1.5394 10/08/14*  
Adds logon control.

*2.1.5288 06/24/14*  
Nullable Up Down: Force the nullable value text when
the control text is not set.

*2.1.5253 05/20/14*  
Engineering numeric up down treats percent units as a
special case253

*2.1.5239 05/06/14*  
Access the numeric up down control collections when
addressing the text box and up down buttons.

*2.1.5219 04/16/14*  
Adds tool strip numeric up down control.

*2.1.5213 04/10/14*  
Fixes and simplifies the nullable numeric up down
control. Adds tester for the control.

*2.1.5207 04/05/14*  
Adds engineering up down control. Adds engineering
scaling to numeric up down control.

*2.1.5163 02/19/14*  
Uses local sync and async safe event handlers.

*2.0.5138 01/25/14*  
Uses system font for forms and controls.

*2.0.5126 01/13/14*  
Tagged as 2014.

*2.0.5065 11/13/13*  
Fixes state aware timer dispose to prevent stack
overflow. Adds safe setters for three state check box.

*2.0.5064 11/12/13*  
Adds file selector user control.

*2.0.5018 09/27/13*  
Created based on the legacy Controls structures.

\(C\) 2007 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Core Controls Library](https://bitbucket.org/davidhary/vs.core)
