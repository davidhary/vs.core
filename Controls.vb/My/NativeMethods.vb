Imports System.Security
Imports System.Security.Permissions

''' <summary> An unsafe native methods. </summary>
''' <remarks>
''' Potentially dangerous P/Invoke method declarations; be careful not to expose these publicly. <para>
''' (c) 2002 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License.</para>
''' </remarks>
<SecurityPermission(SecurityAction.Assert, Flags:=SecurityPermissionFlag.UnmanagedCode)>
<SuppressUnmanagedCodeSecurity>
Partial Friend NotInheritable Class NativeMethods

#Region " CONSTRUCTOR "

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Sub New()
    End Sub
#End Region

End Class
