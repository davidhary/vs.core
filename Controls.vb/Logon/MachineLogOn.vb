﻿Imports System.DirectoryServices.AccountManagement

Imports isr.Core.Controls.ExceptionExtensions

''' <summary> Machine log on. </summary>
''' <remarks>
''' (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 10/6/2014, 2.1.5392. </para>
''' </remarks>
Public Class MachineLogOn
    Inherits LogOnBase

    ''' <summary> Validates a user name and password. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="userName"> Specifies a user name. </param>
    ''' <param name="password"> Specifies a password. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Overloads Overrides Function Authenticate(ByVal userName As String, ByVal password As String) As Boolean
        Dim result As Boolean = False
        If String.IsNullOrWhiteSpace(userName) Then
            Me.ValidationMessage = String.Format("User name is empty")
        ElseIf String.IsNullOrWhiteSpace(password) Then
            Me.ValidationMessage = String.Format("User password is empty")
        Else
            Try
                Using ctx As PrincipalContext = New PrincipalContext(ContextType.Machine)
                    Using user As UserPrincipal = UserPrincipal.FindByIdentity(ctx, userName)
                        If user Is Nothing Then
                            Me.ValidationMessage = String.Format("User '{0}' not identified on this machine", userName)
                        Else
                            Me.UserRoles = DomainLogOn.EnumerateUserRoles(user)
                            result = ctx.ValidateCredentials(userName, password)
                            If result Then
                                MyBase.UserName = userName
                            Else
                                MyBase.UserName = String.Empty
                                Me.ValidationMessage = String.Format("Failed validating credentials for user '{0}'", userName)
                            End If
                        End If
                    End Using
                End Using
            Catch ex As Exception
                Me.ValidationMessage = ex.ToFullBlownString
            End Try
        End If
        Me.IsAuthenticated = result
        Me.Failed = Not result
        Return result
    End Function

    ''' <summary> Tries to find a user in a group role. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="userName">         Specifies a user name. </param>
    ''' <param name="allowedUserRoles"> Specifies the list of valid roles. This could be compared to
    '''                                 the name of enumeration flags. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Overloads Overrides Function TryFindUser(userName As String, ByVal allowedUserRoles As ArrayList) As Boolean
        Dim result As Boolean = False
        Me.Failed = False
        If String.IsNullOrWhiteSpace(userName) Then
            Me.ValidationMessage = String.Format("User name is empty")
            Me.Failed = True
        ElseIf allowedUserRoles Is Nothing Then
            Me.ValidationMessage = String.Format("User roles not specified")
            Me.Failed = True
        ElseIf allowedUserRoles.Count = 0 Then
            Me.ValidationMessage = String.Format("Expected user roles not set")
            Me.Failed = True
        Else
            Try
                Using ctx As PrincipalContext = New PrincipalContext(ContextType.Machine)
                    Using user As UserPrincipal = UserPrincipal.FindByIdentity(ctx, userName)
                        If user Is Nothing Then
                            Me.ValidationMessage = String.Format("User '{0}' not identified on this machine", userName)
                        Else
                            Me.UserRoles = DomainLogOn.EnumerateUserRoles(user)
                            If DomainLogOn.EnumerateUserRoles(user, allowedUserRoles).Count > 0 Then
                                MyBase.UserName = userName
                                result = True
                            End If
                            If Not result Then
                                Me.ValidationMessage = String.Format("User '{0}' role was not found among the allowed user roles.", userName)
                            End If
                        End If
                    End Using
                End Using
            Catch ex As Exception
                Me.ValidationMessage = ex.ToFullBlownString
            End Try
        End If
        Return result
    End Function

End Class
