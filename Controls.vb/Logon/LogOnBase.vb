Imports System.ComponentModel

Imports isr.Core.Controls.ExceptionExtensions

''' <summary> Interfaces user log on/off functionality. </summary>
''' <remarks>
''' (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License.</para><para>
''' David, 10/30/08, 1.00.3225.x. </para>
''' </remarks>
<System.Runtime.InteropServices.ComVisible(False)>
Public MustInherit Class LogOnBase
    Implements IDisposable, INotifyPropertyChanged

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="LogOnBase" /> class. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Protected Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Me.Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived
    ''' class provided proper implementation.
    ''' </summary>
    ''' <value> <c>True</c> if disposed; otherwise, <c>False</c>. </value>
    Protected Property IsDisposed() As Boolean

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks>
    ''' Executes in two distinct scenarios as determined by its disposing parameter:<para>
    ''' If True, the method has been called directly or indirectly by a user's code--managed and
    ''' unmanaged resources can be disposed.</para><para>
    ''' If False, the method has been called by the runtime from inside the finalizer and you should
    ''' not reference other objects--only unmanaged resources can be disposed.</para>
    ''' </remarks>
    ''' <param name="disposing"> <c>True</c> if this method releases both managed and unmanaged
    '''                                                   resources;
    '''                                                   False if this method releases only unmanaged
    '''                                                   resources. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.RemoveEventHandler(Me.PropertyChangedEvent)
            End If
        Finally
            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True
        End Try
    End Sub

#End Region

#Region " EVENTS "

    ''' <summary> Event that is raised when a property value changes. </summary>
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As PropertyChangedEventArgs) Implements INotifyPropertyChanged.PropertyChanged

    ''' <summary> Removes the event handler. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The value. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub RemoveEventHandler(ByVal value As PropertyChangedEventHandler)
#Disable Warning CA1825 ' Avoid zero-length array allocations.
        For Each d As [Delegate] In If(value Is Nothing, New [Delegate]() {}, value.GetInvocationList)
#Enable Warning CA1825 ' Avoid zero-length array allocations.
            Try
                RemoveHandler Me.PropertyChanged, CType(d, PropertyChangedEventHandler)
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

#End Region

#Region " VALIDATION METHODS "

    ''' <summary> Authenticates a user name and password. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="userName"> Specifies a user name. </param>
    ''' <param name="password"> Specifies a password. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
    Public MustOverride Function Authenticate(ByVal userName As String, ByVal password As String) As Boolean

    ''' <summary> Authenticates a user name and password. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="userName">         Specifies a user name. </param>
    ''' <param name="password">         Specifies a password. </param>
    ''' <param name="allowedUserRoles"> Specifies the list of valid roles. This could be compared to
    '''                                 the name of enumeration flags. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
    Public Overridable Function Authenticate(userName As String, password As String, ByVal allowedUserRoles As ArrayList) As Boolean
        Me.IsAuthenticated = If(Me.TryFindUser(userName, allowedUserRoles), Me.Authenticate(userName, password), False)
        Return Me.IsAuthenticated
    End Function

    ''' <summary> Tries to find a user in a group role. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="userName">         Specifies a user name. </param>
    ''' <param name="allowedUserRoles"> Specifies the list of valid roles. This could be compared to
    '''                                 the name of enumeration flags. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
    Public MustOverride Function TryFindUser(ByVal userName As String, ByVal allowedUserRoles As ArrayList) As Boolean

    ''' <summary> Invalidates the last login user. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub Invalidate()
        Me.IsAuthenticated = False
        Me.Failed = False
    End Sub

#End Region

#Region " VALIDATION OUTCOMES "

    ''' <summary> Name of the user. </summary>
    Private _UserName As String

    ''' <summary> Gets or sets (protected) the name of the user. </summary>
    ''' <value> The name of the user. </value>
    Public Property UserName As String
        Get
            Return Me._UserName
        End Get
        Protected Set(ByVal value As String)
            Me._UserName = value
            Dim evt As PropertyChangedEventHandler = Me.PropertyChangedEvent
            evt?.Invoke(Me, New PropertyChangedEventArgs(NameOf(UserName)))
        End Set
    End Property

    ''' <summary> The user roles. </summary>
    Private _UserRoles As ArrayList

    ''' <summary> Gets or sets (protected) the roles of the user. </summary>
    ''' <value> The Role of the user. </value>
    Public Property UserRoles As ArrayList
        Get
            Return Me._UserRoles
        End Get
        Protected Set(ByVal value As ArrayList)
            Me._UserRoles = value
            Dim evt As PropertyChangedEventHandler = Me.PropertyChangedEvent
            evt?.Invoke(Me, New PropertyChangedEventArgs(NameOf(UserRoles)))
        End Set
    End Property

    ''' <summary> True if is authenticated, false if not. </summary>
    Private _IsAuthenticated As Boolean

    ''' <summary>
    ''' Gets or sets (protected) a value indicating whether the user is authenticated.
    ''' </summary>
    ''' <value> <c>true</c> if the user authenticated is valid; otherwise <c>false</c>. </value>
    Public Property IsAuthenticated As Boolean
        Get
            Return Me._IsAuthenticated
        End Get
        Protected Set(ByVal value As Boolean)
            Me._IsAuthenticated = value
            Dim evt As PropertyChangedEventHandler = Me.PropertyChangedEvent
            evt?.Invoke(Me, New PropertyChangedEventArgs(NameOf(IsAuthenticated)))
        End Set
    End Property

    ''' <summary> The validation details. </summary>
    Private _ValidationDetails As String

    ''' <summary> Gets or sets (protected) the validation message. </summary>
    ''' <value> A message describing the validation. </value>
    Public Property ValidationMessage() As String
        Get
            Return Me._ValidationDetails
        End Get
        Protected Set(ByVal value As String)
            Me._ValidationDetails = value
            Dim evt As PropertyChangedEventHandler = Me.PropertyChangedEvent
            evt?.Invoke(Me, New PropertyChangedEventArgs(NameOf(ValidationMessage)))
        End Set
    End Property

    ''' <summary> True if failed. </summary>
    Private _Failed As Boolean

    ''' <summary> Gets or sets a value indicating whether the login failed. </summary>
    ''' <value> <c>true</c> if failed; otherwise <c>false</c>. </value>
    Public Property Failed As Boolean
        Get
            Return Me._Failed
        End Get
        Protected Set(value As Boolean)
            Me._Failed = value
            Dim evt As PropertyChangedEventHandler = Me.PropertyChangedEvent
            evt?.Invoke(Me, New PropertyChangedEventArgs(NameOf(Failed)))
        End Set
    End Property

#End Region

End Class
