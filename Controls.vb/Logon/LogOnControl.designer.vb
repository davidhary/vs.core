
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class LogOnControl

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me._PasswordTextBox = New System.Windows.Forms.TextBox()
        Me._PasswordTextBoxLabel = New System.Windows.Forms.Label()
        Me._UserNameTextBox = New System.Windows.Forms.TextBox()
        Me._UserNameTextBoxLabel = New System.Windows.Forms.Label()
        Me._ErrorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        Me._LogOnLinkLabel = New System.Windows.Forms.LinkLabel()
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_passwordTextBox
        '
        Me._PasswordTextBox.Location = New System.Drawing.Point(95, 31)
        Me._PasswordTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._PasswordTextBox.Name = "_passwordTextBox"
        Me._PasswordTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me._PasswordTextBox.Size = New System.Drawing.Size(81, 25)
        Me._PasswordTextBox.TabIndex = 3
        '
        '_passwordTextBoxLabel
        '
        Me._PasswordTextBoxLabel.Location = New System.Drawing.Point(5, 30)
        Me._PasswordTextBoxLabel.Name = "_passwordTextBoxLabel"
        Me._PasswordTextBoxLabel.Size = New System.Drawing.Size(90, 26)
        Me._PasswordTextBoxLabel.TabIndex = 2
        Me._PasswordTextBoxLabel.Text = "Password: "
        Me._PasswordTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_userNameTextBox
        '
        Me._UserNameTextBox.Location = New System.Drawing.Point(96, 4)
        Me._UserNameTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._UserNameTextBox.Name = "_userNameTextBox"
        Me._UserNameTextBox.Size = New System.Drawing.Size(81, 25)
        Me._UserNameTextBox.TabIndex = 1
        Me._UserNameTextBox.Text = "David"
        '
        '_userNameTextBoxLabel
        '
        Me._UserNameTextBoxLabel.Location = New System.Drawing.Point(6, 3)
        Me._UserNameTextBoxLabel.Name = "_userNameTextBoxLabel"
        Me._UserNameTextBoxLabel.Size = New System.Drawing.Size(89, 26)
        Me._UserNameTextBoxLabel.TabIndex = 0
        Me._UserNameTextBoxLabel.Text = "User Name: "
        Me._UserNameTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_ErrorProvider
        '
        Me._ErrorProvider.ContainerControl = Me
        '
        '_LogOnLinkLabel
        '
        Me._LogOnLinkLabel.Location = New System.Drawing.Point(113, 60)
        Me._LogOnLinkLabel.Name = "_LogOnLinkLabel"
        Me._LogOnLinkLabel.Size = New System.Drawing.Size(59, 17)
        Me._LogOnLinkLabel.TabIndex = 4
        Me._LogOnLinkLabel.TabStop = True
        Me._LogOnLinkLabel.Text = "Log In"
        Me._LogOnLinkLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'LogOnControl
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Transparent
        Me.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Controls.Add(Me._LogOnLinkLabel)
        Me.Controls.Add(Me._PasswordTextBox)
        Me.Controls.Add(Me._PasswordTextBoxLabel)
        Me.Controls.Add(Me._UserNameTextBox)
        Me.Controls.Add(Me._UserNameTextBoxLabel)
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "LogOnControl"
        Me.Size = New System.Drawing.Size(179, 81)
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    ''' <summary> The with events control. </summary>
    Private WithEvents _PasswordTextBox As System.Windows.Forms.TextBox

    ''' <summary> The with events control. </summary>
    Private WithEvents _PasswordTextBoxLabel As System.Windows.Forms.Label

    ''' <summary> The with events control. </summary>
    Private WithEvents _UserNameTextBox As System.Windows.Forms.TextBox

    ''' <summary> The with events control. </summary>
    Private WithEvents _UserNameTextBoxLabel As System.Windows.Forms.Label

        Private WithEvents _ErrorProvider As System.Windows.Forms.ErrorProvider

    ''' <summary> The with events control. </summary>
    Private WithEvents _LogOnLinkLabel As System.Windows.Forms.LinkLabel

End Class
