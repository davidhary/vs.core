Imports System.ComponentModel

''' <summary> User log on/off interface. </summary>
''' <remarks>
''' (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License.</para><para>
''' David, 10/30/08, 1.00.3225.x. </para>
''' </remarks>
<Description("User Log On Control"), System.Drawing.ToolboxBitmap(GetType(LogOnControl))>
<System.Runtime.InteropServices.ComVisible(False)>
Public Class LogOnControl
    Inherits Forma.ModelViewBase

#Region " CONSTRUCTION "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()

        ' This call is required by the designer.
        Me.InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me._ErrorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._ErrorProvider.ContainerControl = Me
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the isr.Core.Controls.LogOnControl and optionally
    ''' releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me._Popup IsNot Nothing Then Me._Popup.Dispose() : Me._Popup = Nothing
                Me._UserLogOn = Nothing
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " USER ACTIONS "

    ''' <summary> True if is authenticated, false if not. </summary>
    Private _IsAuthenticated As Boolean

    ''' <summary> Gets or sets a value indicating whether this object is authenticated. </summary>
    ''' <value> <c>true</c> if this object is Authenticated; otherwise <c>false</c>. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property IsAuthenticated() As Boolean
        Get
            Return Me._IsAuthenticated
        End Get
        Set(ByVal value As Boolean)
            Me._IsAuthenticated = value
            Me.UpdateCaptions()
        End Set
    End Property

    ''' <summary> The user log on. </summary>
    Private _UserLogOn As LogOnBase

    ''' <summary> Gets or sets reference to the user log on implementation object. </summary>
    ''' <value> The user log on. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property UserLogOn() As LogOnBase
        Get
            Return Me._UserLogOn
        End Get
        Set(value As LogOnBase)
            Me._UserLogOn = value
            Me.IsAuthenticated = False
        End Set
    End Property

#End Region

#Region " ALLOWED USER ROLES "

    ''' <summary> The allowed user roles. </summary>
    Private _AllowedUserRoles As ArrayList

    ''' <summary> Gets the allowed user roles. </summary>
    ''' <value> The allowed user roles. </value>
    Public ReadOnly Property AllowedUserRoles As ArrayList
        Get
            Return Me._AllowedUserRoles
        End Get
    End Property

    ''' <summary> Adds a user role to the list of allowed user roles. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> Specifies a user role to add to the list of allowed user roles. </param>
    Public Sub AddAllowedUserRole(ByVal value As String)
        If Me._AllowedUserRoles Is Nothing Then
            Me._AllowedUserRoles = New ArrayList
        End If
        If Not Me._AllowedUserRoles.Contains(value) Then
            Me._AllowedUserRoles.Add(value)
        End If
    End Sub

    ''' <summary> Adds a user roles. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="values"> The values. </param>
    Public Sub AddAllowedUserRoles(ByVal values() As String)
        If Me._AllowedUserRoles Is Nothing Then
            Me._AllowedUserRoles = New ArrayList
        End If
        Me._AllowedUserRoles.AddRange(values)
    End Sub

    ''' <summary> Clears the allowed user roles. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub ClearAllowedUserRoles()
        Me._AllowedUserRoles = New ArrayList
    End Sub

    ''' <summary> Authenticated roles. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> An ArrayList. </returns>
    Public Function AuthenticatedRoles() As ArrayList
        Return isr.Core.Controls.DomainLogOn.EnumerateUserRoles(Me.UserLogOn.UserRoles, Me.AllowedUserRoles)
    End Function

    ''' <summary> Returns true if the specified role is allowed. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> Specifies a user role to check against the list of allowed user roles. </param>
    ''' <returns> <c>true</c> if user role allowed; otherwise <c>false</c>. </returns>
    Public Function IsUserRoleAllowed(ByVal value As String) As Boolean
        Return Me._AllowedUserRoles IsNot Nothing AndAlso Me._AllowedUserRoles.Contains(value)
    End Function

#End Region

#Region " GUI "

    ''' <summary> Logs the user on. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Sub LogOnThis()
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.IsAuthenticated = If(Me.AllowedUserRoles Is Nothing OrElse Me.AllowedUserRoles.Count = 0,
                Me.UserLogOn.Authenticate(Me._UserNameTextBox.Text, Me._PasswordTextBox.Text.Trim),
                Me.UserLogOn.Authenticate(Me._UserNameTextBox.Text, Me._PasswordTextBox.Text.Trim, Me._AllowedUserRoles))
            If Not Me.IsAuthenticated Then
                Me._ErrorProvider.SetError(Me._LogOnLinkLabel, Me.UserLogOn.ValidationMessage)
            End If
        Catch
            Throw
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Logs on link label link clicked. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Link label link clicked event information. </param>
    Private Sub LogOnLinkLabel_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles _LogOnLinkLabel.LinkClicked
        Me._ErrorProvider.SetError(Me._LogOnLinkLabel, "")
        If Me.IsAuthenticated Then
            Me.IsAuthenticated = False
            Me.UserLogOn.Invalidate()
        Else
            Me.LogOnThis()
        End If
        If Me._Popup IsNot Nothing Then
            Me._Popup.Hide()
        End If
    End Sub

    ''' <summary> The log on caption. </summary>
    Private _LogOnCaption As String = "Log In"

    ''' <summary> Gets or sets the log on caption. </summary>
    ''' <value> The log on caption. </value>
    <Category("Appearance"), Description("Caption for log on"),
      Browsable(True),
      DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
      DefaultValue("Log In")>
    Public Property LogOnCaption() As String
        Get

            Return Me._LogOnCaption
        End Get
        Set(ByVal value As String)
            Me._LogOnCaption = value
            Me.UpdateCaptions()
        End Set
    End Property

    ''' <summary> The log off caption. </summary>
    Private _LogOffCaption As String = "Log Out"

    ''' <summary> Gets or sets the log off caption. </summary>
    ''' <value> The log off caption. </value>
    <Category("Appearance"), Description("Caption for log off"),
      Browsable(True),
      DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
      DefaultValue("Log Out")>
    Public Property LogOffCaption() As String
        Get
            Return Me._LogOffCaption
        End Get
        Set(ByVal value As String)
            Me._LogOffCaption = value
            Me.UpdateCaptions()
        End Set
    End Property

    ''' <summary> Initializes the user interface and tool tips. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub UserLogOnControl_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        ' disable when loaded.
        Me._UserNameTextBox.Enabled = False
        Me._UserNameTextBox.Text = String.Empty
        Me._PasswordTextBox.Enabled = False
        Me._UserNameTextBox.Text = String.Empty
        Me._LogOnLinkLabel.Enabled = False
        Me._LogOnLinkLabel.Text = Me.LogOnCaption

    End Sub

    ''' <summary> Logs on control visible changed. </summary>
    ''' <remarks> This in fact enables the control when in a pop-up container. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub LogOnControl_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.VisibleChanged
        Me.UpdateCaptions()
    End Sub

    ''' <summary> Updates the captions. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Sub UpdateCaptions()
        Me._LogOnLinkLabel.Text = If(Me._IsAuthenticated, Me.LogOffCaption, Me.LogOnCaption)
        If Me._UserLogOn IsNot Nothing Then
            ' enable the user name and password boxes
            Me._UserNameTextBox.Enabled = True
            Me._PasswordTextBox.Enabled = True
            Me._LogOnLinkLabel.Enabled = True
        End If
    End Sub

#End Region

#Region " POPUP "

    ''' <summary> The popup. </summary>
    Private _Popup As PopupContainer

    ''' <summary> Shows the pop-up. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="control">  The control. </param>
    ''' <param name="location"> The location. </param>
    Public Sub ShowPopup(ByVal control As Control, ByVal location As Drawing.Point)
        Me._Popup = New PopupContainer(Me)
        Me._Popup.Show(control, location)
    End Sub

#End Region

End Class

