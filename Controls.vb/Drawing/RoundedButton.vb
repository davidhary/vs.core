﻿Imports System.ComponentModel
Imports System.Drawing.Drawing2D

Imports isr.Core.Controls.DrawingExtensions

''' <summary> A button with rounded option. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 3/26/2016 </para>
''' </remarks>
<DesignerCategory("code"), System.ComponentModel.Description("Round Button")>
Public Class RoundedButton
    Inherits System.Windows.Forms.Button

#Region " CONTRUCTOR "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        MyBase.New
        Me.CornerRadii = New CornerRadius
    End Sub

#End Region

#Region " BORDER STYLE "

    ''' <summary> Size of the border. </summary>
    Private _BorderSize As Integer

    ''' <summary> Specifies the border size. </summary>
    ''' <value> The size of the border. </value>
    <Category("Appearance"), DefaultValue(1),
        Description("Specifies the border size.")>
    Public Property BorderSize As Integer
        Get
            Return Me._BorderSize
        End Get
        Set(value As Integer)
            Me._BorderSize = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The shadow angle. </summary>
    Private _ShadowAngle As Integer

    ''' <summary> Specifies the shadow angle. </summary>
    ''' <value> The shadow angle. </value>
    <Category("Appearance"), DefaultValue(270),
        Description("Specifies the shadow angle.")>
    Public Property ShadowAngle As Integer
        Get
            Return Me._ShadowAngle
        End Get
        Set(value As Integer)
            Me._ShadowAngle = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> Specifies the top left radius in percent of height or width. </summary>
    ''' <value> The top left radius. </value>
    <Category("Appearance"), DefaultValue(0),
        Description("Specifies the top left radius in percent of height or width.")>
    Public Property TopLeftRadius As Integer
        Get
            Return Me.Corners.TopLeft
        End Get
        Set(value As Integer)
            Me._Corners = New CornerRadius(Me.BottomLeftRadius, Me.BottomRightRadius, value, Me.TopRightRadius)
            Me._CornerRadii = New CornerRadius(Me.BottomLeftRadius * Me.Height \ 100,
                                               Me.BottomRightRadius * Me.Height \ 100,
                                               Me.TopLeftRadius * Me.Height \ 100,
                                               Me.TopRightRadius * Me.Height \ 100)
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> Specifies the top right radius. </summary>
    ''' <value> The top right radius. </value>
    <Category("Appearance"), DefaultValue(0),
        Description("Specifies the top right radius.")>
    Public Property TopRightRadius As Integer
        Get
            Return Me.Corners.TopRight
        End Get
        Set(value As Integer)
            Me._Corners = New CornerRadius(Me.BottomLeftRadius, Me.BottomRightRadius, Me.TopLeftRadius, value)
            Me._CornerRadii = New CornerRadius(Me.BottomLeftRadius * Me.Height \ 100,
                                               Me.BottomRightRadius * Me.Height \ 100,
                                               Me.TopLeftRadius * Me.Height \ 100,
                                               Me.TopRightRadius * Me.Height \ 100)
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> Specifies the bottom left radius. </summary>
    ''' <value> The bottom left radius. </value>
    <Category("Appearance"), DefaultValue(0),
        Description("Specifies the bottom left radius.")>
    Public Property BottomLeftRadius As Integer
        Get
            Return Me.Corners.BottomLeft
        End Get
        Set(value As Integer)
            Me._Corners = New CornerRadius(value, Me.BottomRightRadius, Me.TopLeftRadius, Me.TopRightRadius)
            Me._CornerRadii = New CornerRadius(Me.BottomLeftRadius * Me.Height \ 100,
                                               Me.BottomRightRadius * Me.Height \ 100,
                                               Me.TopLeftRadius * Me.Height \ 100,
                                               Me.TopRightRadius * Me.Height \ 100)
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> Specifies the bottom right radius. </summary>
    ''' <value> The bottom right radius. </value>
    <Category("Appearance"), DefaultValue(0),
        Description("Specifies the bottom right radius.")>
    Public Property BottomRightRadius As Integer
        Get
            Return Me.Corners.BottomRight
        End Get
        Set(value As Integer)
            Me._Corners = New CornerRadius(Me.BottomLeftRadius, value, Me.TopLeftRadius, Me.TopRightRadius)
            Me._CornerRadii = New CornerRadius(Me.BottomLeftRadius * Me.Height \ 100,
                                               Me.BottomRightRadius * Me.Height \ 100,
                                               Me.TopLeftRadius * Me.Height \ 100,
                                               Me.TopRightRadius * Me.Height \ 100)
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> Default corners. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> The CornerRadius. </returns>
    Private Shared Function DefaultCorners() As CornerRadius
        Return New CornerRadius(0)
    End Function

    ''' <summary> The corners. </summary>
    Private _Corners As CornerRadius = RoundedButton.DefaultCorners

    ''' <summary> Gets or sets the relative corner radii. </summary>
    ''' <value> The relative corner radii. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property Corners As CornerRadius
        Get
            Return Me._Corners
        End Get
        Set(value As CornerRadius)
            Me._Corners = value
            Me._CornerRadii = New CornerRadius(Me.BottomLeftRadius * Me.Height \ 100,
                                               Me.BottomRightRadius * Me.Height \ 100,
                                               Me.TopLeftRadius * Me.Height \ 100,
                                               Me.TopRightRadius * Me.Height \ 100)
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> Gets or sets the corner radii. </summary>
    ''' <value> The corner radii. </value>
    Private ReadOnly Property CornerRadii As CornerRadius

#End Region

#Region " PAINT "

    ''' <summary> Clip region. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="surface">         The surface. </param>
    ''' <param name="borderRectangle"> The border rectangle. </param>
    Public Sub ClipRegion(ByVal surface As Graphics, ByVal borderRectangle As Rectangle)
        If surface Is Nothing Then Return
        surface.PixelOffsetMode = PixelOffsetMode.HighQuality
        surface.InterpolationMode = InterpolationMode.HighQualityBilinear
        surface.SmoothingMode = SmoothingMode.AntiAlias
        Using path As New GraphicsPath
            path.BuildRoundClipPath(Me.Size, borderRectangle, Me.CornerRadii)
            Me.Region = New System.Drawing.Region(path)
        End Using
    End Sub

    ''' <summary> Renders the border. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="surface">         The surface. </param>
    ''' <param name="borderRectangle"> The border rectangle. </param>
    Public Sub RenderBorder(ByVal surface As Graphics, ByVal borderRectangle As Rectangle)
        If surface Is Nothing Then Return
        surface.PixelOffsetMode = PixelOffsetMode.HighQuality
        surface.InterpolationMode = InterpolationMode.HighQualityBilinear
        surface.SmoothingMode = SmoothingMode.AntiAlias
        Using path As New GraphicsPath
            path.BuildRoundClipPath(Me.Size, borderRectangle, Me.CornerRadii)
            surface.SetClip(path)
            Using pen As New Pen(ControlPaint.Dark(Me.BackColor), 1)
                surface.SmoothingMode = SmoothingMode.AntiAlias
                surface.DrawPath(pen, path)
            End Using
            surface.ResetClip()
        End Using

        Dim innerRectangle As Rectangle = borderRectangle
        Using path As New GraphicsPath
            path.BuildRoundClipPath(Me.Size, innerRectangle, Me.CornerRadii)
            surface.SetClip(path)
            ' TO_DO: Try transparent color.
            Dim color1 As Color = ControlPaint.LightLight(Me.BackColor)
            ' color1 = Color.Transparent
            Dim color2 As Color = ControlPaint.Dark(Me.BackColor)
            If Me.ShadowAngle < 0 Then
                Using brush As LinearGradientBrush = New LinearGradientBrush(innerRectangle, color2, color2, 0)
                    brush.GammaCorrection = True
                    Using pen As New Pen(brush, Me.BorderSize)
                        pen.Alignment = PenAlignment.Inset
                        surface.SmoothingMode = SmoothingMode.AntiAlias
                        surface.DrawPath(pen, path)
                    End Using
                End Using
            Else
                Using brush As LinearGradientBrush = New LinearGradientBrush(innerRectangle, color1, color2, Me.ShadowAngle)
                    brush.GammaCorrection = True
                    Using pen As New Pen(brush, Me.BorderSize)
                        pen.Alignment = PenAlignment.Inset
                        surface.SmoothingMode = SmoothingMode.AntiAlias
                        surface.DrawPath(pen, path)
                    End Using
                End Using
            End If
            surface.ResetClip()
        End Using

    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.Paint" /> event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnPaint(ByVal e As System.Windows.Forms.PaintEventArgs)
        If e Is Nothing Then Return
        e.Graphics.SmoothingMode = SmoothingMode.AntiAlias
        MyBase.OnPaint(e)
        If Me.DisplayRectangle.Width < 4 OrElse Me.DisplayRectangle.Height < 4 Then Return
        Me._CornerRadii = New CornerRadius(Me.BottomLeftRadius * Me.Height \ 100,
                                           Me.BottomRightRadius * Me.Height \ 100,
                                           Me.TopLeftRadius * Me.Height \ 100,
                                           Me.TopRightRadius * Me.Height \ 100)
        Dim rect As Rectangle = Me.DisplayRectangle
        ' this removes the default border.
        rect.Inflate(-3, -3)
        Me.ClipRegion(e.Graphics, rect)
        If Me.BorderSize > 0 Then Me.RenderBorder(e.Graphics, rect)
    End Sub

#End Region

#Region " RESIZE "

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.Resize" /> event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnResize(e As EventArgs)
        MyBase.OnResize(e)
        Using paintArgs As PaintEventArgs = New PaintEventArgs(Me.CreateGraphics, Me.DisplayRectangle)
            Me.InvokePaint(Me, paintArgs)
        End Using
    End Sub

#End Region

End Class




