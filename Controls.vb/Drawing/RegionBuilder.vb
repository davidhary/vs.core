﻿Imports System.Runtime.InteropServices

''' <summary> A region builder. </summary>
''' <remarks> David, 2020-09-24. </remarks>
Public NotInheritable Class RegionBuilder

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Sub New()
        MyBase.New
    End Sub

    ''' <summary> Builds round region. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="rect">   The rectangle. </param>
    ''' <param name="radius"> The radius. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function BuildRoundRegion(ByVal rect As Rectangle, ByVal radius As Integer) As Integer
        Return SafeNativeMethods.CreateRoundRectRgn(rect.X, rect.Y, rect.X + rect.Width, rect.Y + rect.Height, radius, radius).ToInt32
    End Function

    ''' <summary> A safe native methods. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private NotInheritable Class SafeNativeMethods

        ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        ''' <remarks> David, 2020-09-24. </remarks>
        Private Sub New()
            MyBase.New()
        End Sub

        ''' <summary> Creates round rectangle region. </summary>
        ''' <remarks> David, 2020-09-24. </remarks>
        ''' <param name="nLeftRect">      The left rectangle. </param>
        ''' <param name="nTopRect">       The top rectangle. </param>
        ''' <param name="nRightRect">     The right rectangle. </param>
        ''' <param name="nBottomRect">    The bottom rectangle. </param>
        ''' <param name="nWidthEllipse">  The width ellipse. </param>
        ''' <param name="nHeightEllipse"> The height ellipse. </param>
        ''' <returns> The new round rectangle region. </returns>
        <DllImport("Gdi32.dll", EntryPoint:="CreateRoundRectRgn")>
        Public Shared Function CreateRoundRectRgn(ByVal nLeftRect As Integer, ByVal nTopRect As Integer,
                                                  ByVal nRightRect As Integer, ByVal nBottomRect As Integer,
                                                  ByVal nWidthEllipse As Integer, ByVal nHeightEllipse As Integer) As IntPtr
        End Function

    End Class


End Class
