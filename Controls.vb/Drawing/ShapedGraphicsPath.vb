﻿Imports System.Drawing.Drawing2D

''' <summary> A shaped graphics path. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 3/26/2016 </para>
''' </remarks>
Public NotInheritable Class ShapedGraphicsPath

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Sub New()
        MyBase.New
    End Sub

    ''' <summary> Builds round path. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="rect"> The rectangle. </param>
    ''' <returns> A GraphicsPath. </returns>
    Public Shared Function BuildRoundPath(ByVal rect As Rectangle) As GraphicsPath
        Dim path As GraphicsPath = Nothing
        Try
            path = New GraphicsPath()
            'path.AddEllipse(0, 0, Me.ClientSize.Width, Me.ClientSize.Height)
            path.AddEllipse(rect.X, rect.Y, rect.Width, rect.Height)
        Catch
            path?.Dispose()
            Throw
        End Try
        Return path
    End Function

    ''' <summary> Builds rounded path. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="rect">   The rectangle. </param>
    ''' <param name="radius"> The radius. </param>
    ''' <returns> A GraphicsPath. </returns>
    Public Shared Function BuildRoundedPath(ByVal rect As Rectangle, ByVal radius As Integer) As GraphicsPath
        radius = Math.Min(Math.Min(rect.Height, rect.Width), radius)
        Dim r2 As Integer = radius \ 2
        radius = r2 + r2
        Dim path As GraphicsPath = Nothing
        Try
            path = New GraphicsPath()
            path.StartFigure()
            path.AddArc(rect.X, rect.Y, radius, radius, 180, 90)
            path.AddLine(rect.X + r2, rect.Y, rect.Width - r2, rect.Y)
            Dim value As Integer = rect.Width - radius : value = If(value < 0, 0, value)
            path.AddArc(rect.X + value, rect.Y, radius, radius, 270, 90)
            path.AddLine(rect.Width, rect.Y + r2, rect.Width, rect.Height - r2)
            value = rect.Height - radius : value = If(value < 0, 0, value)
            path.AddArc(rect.X + rect.Width - radius, rect.Y + value, radius, radius, 0, 90)
            path.AddLine(rect.Width - r2, rect.Height, rect.X + r2, rect.Height)
            path.AddArc(rect.X, rect.Y + value, radius, radius, 90, 90)
            path.AddLine(rect.X, rect.Height - r2, rect.X, rect.Y + r2)
            path.CloseFigure()
        Catch
            path?.Dispose()
            Throw
        End Try
        Return path
    End Function

End Class

