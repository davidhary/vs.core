Imports System.ComponentModel
#Disable Warning CA2231 ' Overload operator equals on overriding value type Equals

''' <summary> A corner radius. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
<CodeAnalysis.SuppressMessage("Performance", "CA1815:Override equals and operator equals on value types", Justification:="<Pending>")>
Public Structure CornerRadius
#Enable Warning CA2231 ' Overload operator equals on overriding value type Equals

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="radius"> The radius. </param>
    Public Sub New(ByVal radius As Integer)
        Me._BottomLeft = radius
        Me._BottomRight = radius
        Me._TopLeft = radius
        Me._TopRight = radius
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="bottomLeft">  The bottom left. </param>
    ''' <param name="bottomRight"> The bottom right. </param>
    ''' <param name="topLeft">     The top left. </param>
    ''' <param name="topRight">    The top right. </param>
    Public Sub New(ByVal bottomLeft As Integer, ByVal bottomRight As Integer, ByVal topLeft As Integer, ByVal topRight As Integer)
        Me._BottomLeft = bottomLeft
        Me._BottomRight = bottomRight
        Me._TopLeft = topLeft
        Me._TopRight = topRight
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As CornerRadius)
        Me._BottomLeft = value.BottomLeft
        Me._BottomRight = value.BottomRight
        Me._TopLeft = value.TopLeft
        Me._TopRight = value.TopRight
    End Sub

    ''' <summary> Parses. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> The CornerRadius. </returns>
    Public Shared Function Parse(ByVal value As String) As CornerRadius
        If String.IsNullOrWhiteSpace(value) Then
            Return CornerRadius.Empty
        Else
            Dim values As String() = value.Split(","c)
            Return If(value.Count < 4,
                CornerRadius.Empty,
                New CornerRadius(Convert.ToInt32(values(0), Globalization.CultureInfo.InvariantCulture),
                                        Convert.ToInt32(values(1), Globalization.CultureInfo.InvariantCulture),
                                        Convert.ToInt32(values(2), Globalization.CultureInfo.InvariantCulture),
                                        Convert.ToInt32(values(3), Globalization.CultureInfo.InvariantCulture)))
        End If
    End Function

    ''' <summary> Gets the unity. </summary>
    ''' <value> The unity. </value>
    Public Shared ReadOnly Property Unity As CornerRadius
        Get
            Return New CornerRadius(1, 1, 1, 1)
        End Get
    End Property

    ''' <summary> Gets the empty. </summary>
    ''' <value> The empty. </value>
    Public Shared ReadOnly Property Empty As CornerRadius
        Get
            Return New CornerRadius(0, 0, 0, 0)
        End Get
    End Property

    ''' <summary> Gets the is empty. </summary>
    ''' <value> The is empty. </value>
    Public ReadOnly Property IsEmpty As Boolean


        Get
            Return Me.BottomLeft <= 0 OrElse Me.BottomRight <= 0 OrElse Me.TopLeft <= 0 OrElse Me.TopRight <= 0
        End Get
    End Property

    ''' <summary> Set the Radius of the Lower Left Corner. </summary>
    ''' <value> The bottom left. </value>
    <DescriptionAttribute("Set the Radius of the Lower Left Corner")>
    <RefreshProperties(RefreshProperties.Repaint)>
    <NotifyParentProperty(True)>
    <DefaultValue(0)>
    Public Property BottomLeft As Integer

    ''' <summary> Set the Radius of the Lower Right Corner. </summary>
    ''' <value> The bottom right. </value>
    <DescriptionAttribute("Set the Radius of the Lower Right Corner")>
    <RefreshProperties(RefreshProperties.Repaint)>
    <NotifyParentProperty(True)>
    <DefaultValue(0)>
    Public Property BottomRight As Integer

    ''' <summary> Set the Radius of the Upper Left Corner. </summary>
    ''' <value> The top left. </value>
    <DescriptionAttribute("Set the Radius of the Upper Left Corner")>
    <RefreshProperties(RefreshProperties.Repaint)>
    <NotifyParentProperty(True)>
    <DefaultValue(0)>
    Public Property TopLeft As Integer

    ''' <summary> Set the Radius of the Upper Right Corner. </summary>
    ''' <value> The top right. </value>
    <DescriptionAttribute("Set the Radius of the Upper Right Corner")>
    <RefreshProperties(RefreshProperties.Repaint)>
    <NotifyParentProperty(True)>
    <DefaultValue(0)>
    Public Property TopRight As Integer

    ''' <summary> Returns the fully qualified type name of this instance. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> The fully qualified type name. </returns>
    Public Overrides Function ToString() As String
        Return String.Format("{0}, {1}, {2}, {3}", Me.BottomLeft, Me.BottomRight, Me.TopLeft, Me.TopRight)
    End Function

    ''' <summary> Indicates whether this instance and a specified object are equal. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="obj"> The object to compare with the current instance. </param>
    ''' <returns>
    ''' true if <paramref name="obj" /> and this instance are the same type and represent the same
    ''' value; otherwise, false.
    ''' </returns>
    Public Overrides Function Equals(ByVal obj As Object) As Boolean
        Return obj IsNot Nothing AndAlso Me.GetType() Is obj.GetType() AndAlso Me.Equals(CType(obj, CornerRadius))
    End Function

    ''' <summary> Indicates whether this instance and a specified object are equal. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="other"> The corner radius to compare to this object. </param>
    ''' <returns>
    ''' true if <paramref name="other" /> and this instance are the same type and represent the same
    ''' value; otherwise, false.
    ''' </returns>
    Public Overloads Function Equals(ByVal other As CornerRadius) As Boolean
        Return Me.BottomLeft = other.BottomLeft AndAlso Me.BottomRight = other.BottomRight AndAlso
               Me.TopLeft = other.TopLeft AndAlso Me.TopRight = other.TopRight
    End Function

End Structure

''' <summary> A corner radius converter. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 4/4/2016 </para>
''' </remarks>
Friend Class CornerRadiusConverter
    Inherits ExpandableObjectConverter

    ''' <summary>
    ''' Returns whether changing a value on this object requires a call to
    ''' <see cref="M:System.ComponentModel.TypeConverter.CreateInstance(System.Collections.IDictionary)" />
    ''' to create a new value, using the specified context.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="context"> An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that
    '''                        provides a format context. </param>
    ''' <returns>
    ''' true if changing a property on this object requires a call to
    ''' <see cref="M:System.ComponentModel.TypeConverter.CreateInstance(System.Collections.IDictionary)" />
    ''' to create a new value; otherwise, false.
    ''' </returns>
    Public Overrides Function GetCreateInstanceSupported(ByVal context As ITypeDescriptorContext) As Boolean
        Return True
    End Function

    ''' <summary>
    ''' Creates an instance of the type that this
    ''' <see cref="T:System.ComponentModel.TypeConverter" /> is associated with, using the specified
    ''' context, given a set of property values for the object.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="context">        An <see cref="T:System.ComponentModel.ITypeDescriptorContext" />
    '''                               that provides a format context. </param>
    ''' <param name="propertyValues"> An <see cref="T:System.Collections.IDictionary" /> of new
    '''                               property values. </param>
    ''' <returns>
    ''' An <see cref="T:System.Object" /> representing the given
    ''' <see cref="T:System.Collections.IDictionary" />, or null if the object cannot be created.
    ''' This method always returns null.
    ''' </returns>
    Public Overrides Function CreateInstance(ByVal context As ITypeDescriptorContext,
                                             ByVal propertyValues As System.Collections.IDictionary) As Object
        If propertyValues Is Nothing Then Throw New ArgumentNullException(NameOf(propertyValues))
        Dim lL As Int32 = CType(propertyValues("BottomLeft"), Int32)
        Dim lR As Int32 = CType(propertyValues("BottomRight"), Int32)
        Dim uL As Int32 = CType(propertyValues("TopLeft"), Int32)
        Dim uR As Int32 = CType(propertyValues("TopRight"), Int32)
        Dim crn As New CornerRadius(lL, lR, uL, uR)
        Return crn
    End Function

    ''' <summary>
    ''' Returns whether this converter can convert an object of the given type to the type of this
    ''' converter, using the specified context.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="context">    An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that
    '''                           provides a format context. </param>
    ''' <param name="sourceType"> A <see cref="T:System.Type" /> that represents the type you want to
    '''                           convert from. </param>
    ''' <returns> true if this converter can perform the conversion; otherwise, false. </returns>
    Public Overloads Overrides Function CanConvertFrom(ByVal context As ITypeDescriptorContext,
      ByVal sourceType As System.Type) As Boolean
        Return sourceType Is GetType(String) OrElse MyBase.CanConvertFrom(context, sourceType)
    End Function

    ''' <summary>
    ''' Converts the given object to the type of this converter, using the specified context and
    ''' culture information.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="context"> An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that
    '''                        provides a format context. </param>
    ''' <param name="culture"> The <see cref="T:System.Globalization.CultureInfo" /> to use as the
    '''                        current culture. </param>
    ''' <param name="value">   The <see cref="T:System.Object" /> to convert. </param>
    ''' <returns> An <see cref="T:System.Object" /> that represents the converted value. </returns>
    Public Overloads Overrides Function ConvertFrom(ByVal context As ITypeDescriptorContext,
                                                    ByVal culture As System.Globalization.CultureInfo, ByVal value As Object) As Object
        If TypeOf value Is String Then
            Try
                Dim s As String = CType(value, String)
                Dim cornerParts(4) As String
                cornerParts = Split(s, ",")
                If Not IsNothing(cornerParts) Then
                    If IsNothing(cornerParts(0)) Then cornerParts(0) = "0"
                    If IsNothing(cornerParts(1)) Then cornerParts(1) = "0"
                    If IsNothing(cornerParts(2)) Then cornerParts(2) = "0"
                    If IsNothing(cornerParts(3)) Then cornerParts(3) = "0"
                    Return New CornerRadius(CInt(cornerParts(0).Trim),
                                               CInt(cornerParts(1).Trim),
                                               CInt(cornerParts(2).Trim),
                                               CInt(cornerParts(3).Trim))
                End If
            Catch ex As Exception
                Throw New ArgumentException(FormattableString.Invariant($"Can not convert '{value}' to type CornerRadius"))
            End Try
        Else
            Return New CornerRadius()
        End If
        Return MyBase.ConvertFrom(context, culture, value)
    End Function

    ''' <summary>
    ''' Converts the given value object to the specified type, using the specified context and
    ''' culture information.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="context">         An <see cref="T:System.ComponentModel.ITypeDescriptorContext"/>
    '''                                that provides a format context. </param>
    ''' <param name="culture">         A <see cref="T:System.Globalization.CultureInfo" />. If
    '''                                <see langword="null" /> is passed, the current culture is
    '''                                assumed. </param>
    ''' <param name="value">           The <see cref="T:System.Object" /> to convert. </param>
    ''' <param name="destinationType"> The <see cref="T:System.Type" /> to convert the
    '''                                <paramref name="value" /> parameter to. </param>
    ''' <returns> An <see cref="T:System.Object" /> that represents the converted value. </returns>
    Public Overloads Overrides Function ConvertTo(ByVal context As ITypeDescriptorContext,
                                                  ByVal culture As System.Globalization.CultureInfo,
                                                  ByVal value As Object, ByVal destinationType As System.Type) As Object
        Dim _Corners As CornerRadius = CType(value, CornerRadius)
        Return If(destinationType Is GetType(System.String) AndAlso TypeOf value Is CornerRadius,
            _Corners.ToString,
            MyBase.ConvertTo(context, culture, value, destinationType))
    End Function

End Class





