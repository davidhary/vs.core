﻿Imports System.Runtime.CompilerServices
Imports System.Drawing.Drawing2D

Namespace DrawingExtensions

    Public Module Methods

#Region " CLIP REGION "

        ''' <summary> Clip region. </summary>
        ''' <remarks> David, 2020-09-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="control"> The control. </param>
        ''' <param name="surface"> The surface. </param>
        ''' <param name="corners"> The corners. </param>
        Public Sub ClipRegion(ByVal control As Control, ByVal surface As Graphics, ByVal corners As CornerRadius)
            If control Is Nothing Then Throw New ArgumentNullException(NameOf(control))
            If surface Is Nothing Then Throw New ArgumentNullException(NameOf(surface))
            ClipRegion(control, surface, control.Size, control.DisplayRectangle, corners)
        End Sub

        ''' <summary> Clip region. </summary>
        ''' <remarks> David, 2020-09-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="control">         The control. </param>
        ''' <param name="surface">         The surface. </param>
        ''' <param name="size">            The size. </param>
        ''' <param name="regionRectangle"> The region rectangle. </param>
        ''' <param name="corners">         The corners. </param>
        Public Sub ClipRegion(ByVal control As Control, ByVal surface As Graphics, ByVal size As Size,
                              ByVal regionRectangle As Rectangle, ByVal corners As CornerRadius)
            If control Is Nothing Then Throw New ArgumentNullException(NameOf(control))
            If surface Is Nothing Then Throw New ArgumentNullException(NameOf(surface))
            surface.PixelOffsetMode = PixelOffsetMode.HighQuality
            surface.InterpolationMode = InterpolationMode.HighQualityBilinear
            surface.SmoothingMode = SmoothingMode.AntiAlias
            Using path As New GraphicsPath
                path.BuildRoundClipPath(size, regionRectangle, corners)
                surface.SetClip(path)
                control.Region = New System.Drawing.Region(path)
                surface.ResetClip()
            End Using
        End Sub

#End Region

#Region " PATH BUILDER "

        ''' <summary> Builds round clip path. </summary>
        ''' <remarks> David, 2020-09-24. </remarks>
        ''' <param name="path"> Full pathname of the file. </param>
        ''' <param name="rect"> The rectangle. </param>
        ''' <returns> The rectangle clip path. </returns>
        <Extension()>
        Public Function BuildRoundClipPath(ByVal path As GraphicsPath, ByVal rect As Rectangle) As GraphicsPath
            If path IsNot Nothing Then
                path.AddArc(rect.X, rect.Y, rect.Height, rect.Height, 90, 180)
                path.AddArc(rect.Width - rect.Height, rect.Y, rect.Height, rect.Height, 270, 180)
                path.CloseFigure()
            End If
            Return path
        End Function

        ''' <summary> Builds round clip path. </summary>
        ''' <remarks> David, 2020-09-24. </remarks>
        ''' <param name="path">      Full pathname of the file. </param>
        ''' <param name="size">      The size. </param>
        ''' <param name="rectangle"> The rectangle. </param>
        ''' <param name="radius">    The radius. </param>
        ''' <returns> The rectangle clip path. </returns>
        <Extension()>
        Public Function BuildRoundClipPath(ByVal path As GraphicsPath, ByVal size As Size, ByVal rectangle As Rectangle, ByVal radius As Integer) As GraphicsPath
            Dim radii As New CornerRadius(radius)
            Return BuildRoundClipPath(path, size, rectangle, radii)
        End Function

        ''' <summary> Clip diameter. </summary>
        ''' <remarks> David, 2020-09-24. </remarks>
        ''' <param name="size">     The size. </param>
        ''' <param name="diameter"> The diameter. </param>
        ''' <returns> An Integer. </returns>
        Private Function ClipDiameter(ByVal size As Size, ByVal diameter As Integer) As Integer
            If diameter > size.Height Then diameter = size.Height
            If diameter > size.Width Then diameter = size.Width
            If diameter < _MinimumDiameter Then diameter = _MinimumDiameter
            Return diameter
        End Function

        ''' <summary> The minimum diameter. </summary>
        Private Const _MinimumDiameter As Integer = 1

        ''' <summary> Builds round clip path. </summary>
        ''' <remarks> David, 2020-09-24. </remarks>
        ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        '''                                              null. </exception>
        ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        ''' <param name="path">      Full pathname of the file. </param>
        ''' <param name="size">      The size. </param>
        ''' <param name="rectangle"> The rectangle. </param>
        ''' <param name="radii">     The radii. </param>
        ''' <returns> The rectangle clip path. </returns>
        <Extension()>
        Public Function BuildRoundClipPath(ByVal path As GraphicsPath, ByVal size As Size,
                                           ByVal rectangle As Rectangle, ByVal radii As CornerRadius) As GraphicsPath
            If path Is Nothing Then Throw New ArgumentNullException(NameOf(path))
            If size.Width = 0 OrElse size.Height = 0 Then Throw New InvalidOperationException("Size must be non zero")
            Dim diameter As Integer ' = Methods.ClipDiameter(size, CInt(2 * radii.TopLeft))
            ' TO_DO: Check this.  
            Dim x As Integer = rectangle.X
            Dim y As Integer = rectangle.Y
            Dim h As Integer = rectangle.Height
            Dim w As Integer = rectangle.Width
            diameter = Methods.ClipDiameter(size, CInt(2 * radii.BottomLeft))
            path.AddArc(x, y + h - diameter, diameter, diameter, 90, 90)
            diameter = Methods.ClipDiameter(size, CInt(2 * radii.TopLeft))
            path.AddArc(x, y, diameter, diameter, 180, 90)
            diameter = Methods.ClipDiameter(size, CInt(2 * radii.TopRight))
            path.AddArc(x + w - diameter, y, diameter, diameter, 270, 90)
            diameter = Methods.ClipDiameter(size, CInt(2 * radii.BottomRight))
            path.AddArc(x + w - diameter, y + h - diameter, diameter, diameter, 0, 90)
            path.CloseFigure()
            Return path
        End Function

        ''' <summary> (This method is obsolete) builds zope round clip path. </summary>
        ''' <remarks> David, 2020-09-24. </remarks>
        ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        '''                                              null. </exception>
        ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        ''' <param name="path">      Full pathname of the file. </param>
        ''' <param name="size">      The size. </param>
        ''' <param name="rectangle"> The rectangle. </param>
        ''' <param name="radii">     The radii. </param>
        ''' <returns> A GraphicsPath. </returns>
        <Extension(), Obsolete("Use Build Round Clip Path instead")>
        Public Function BuildZopeRoundClipPath(ByVal path As GraphicsPath, ByVal size As Size,
                                           ByVal rectangle As Rectangle, ByVal radii As CornerRadius) As GraphicsPath
            If path Is Nothing Then Throw New ArgumentNullException(NameOf(path))
            If size.Width = 0 OrElse size.Height = 0 Then Throw New InvalidOperationException("Size must be non zero")
            Dim diameter As Integer = Methods.ClipDiameter(size, CInt(2 * radii.TopLeft))
            ' TO_DO: Check this.  
            Dim x As Integer = rectangle.X
            Dim y As Integer = rectangle.Y
            Dim h As Integer = rectangle.Height
            Dim w As Integer = rectangle.Width
            ' this works with Zope shaped button.
            path.AddArc(x, y, diameter, diameter, 180, 90)
            path.AddArc(w - diameter - x, y, diameter, diameter, 270, 90)
            path.AddArc(w - diameter - x, h - diameter - y, diameter, diameter, 0, 90)
            path.AddArc(x, h - diameter - y, diameter, diameter, 90, 90)

            ' This works for the Shaped Button but not for the rounded button.
            diameter = Methods.ClipDiameter(size, CInt(2 * radii.TopLeft))
            path.AddArc(x, y, diameter, diameter, 180, 90)
            diameter = Methods.ClipDiameter(size, CInt(2 * radii.TopRight))
            path.AddArc(w - diameter - x, y, diameter, diameter, 270, 90)
            diameter = Methods.ClipDiameter(size, CInt(2 * radii.BottomRight))
            path.AddArc(w - diameter - x, h - diameter - y, diameter, diameter, 0, 90)
            diameter = Methods.ClipDiameter(size, CInt(2 * radii.BottomLeft))
            path.AddArc(x, h - diameter - y, diameter, diameter, 90, 90)
            path.CloseFigure()
            Return path
        End Function

#End Region

    End Module

End Namespace

