﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class RichTextEditControl

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(RichTextEditControl))
        Me._RichTextBox = New isr.Core.Controls.RichTextBox()
        Me._MainMenu = New System.Windows.Forms.MenuStrip()
        Me._FileMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me._FilePageSetupMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me._FilePrintPreviewMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me._FilePrintMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me._FileSeparatorMenuItem = New System.Windows.Forms.ToolStripSeparator()
        Me._FileExitMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me._FormatMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me._FormatFontStyleMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me._FormatBoldMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me._FormatItalicMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me._FormatUnderlinedMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me._FormatFontMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me._FormatFontArialMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me._FormatFontCourierMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me._FormatFontLucidaMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._FormatFontSegoeUIMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me._FormatFontTimesMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me._FormatFontSizeMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me._FormatFontSize8Menu = New System.Windows.Forms.ToolStripMenuItem()
        Me._FormatFontSize10Menu = New System.Windows.Forms.ToolStripMenuItem()
        Me._FormatFontSize12Menu = New System.Windows.Forms.ToolStripMenuItem()
        Me._FormatFontSize18Menu = New System.Windows.Forms.ToolStripMenuItem()
        Me._FormatFontSize24Menu = New System.Windows.Forms.ToolStripMenuItem()
        Me._PrintDocument = New System.Drawing.Printing.PrintDocument()
        Me._PageSetupDialog = New System.Windows.Forms.PageSetupDialog()
        Me._PrintPreviewDialog = New System.Windows.Forms.PrintPreviewDialog()
        Me._PrintDialog = New System.Windows.Forms.PrintDialog()
        Me._MainMenu.SuspendLayout()
        Me.SuspendLayout()
        '
        '_RichTextBox
        '
        Me._RichTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._RichTextBox.Location = New System.Drawing.Point(0, 24)
        Me._RichTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._RichTextBox.Name = "_RichTextBox"
        Me._RichTextBox.Size = New System.Drawing.Size(560, 298)
        Me._RichTextBox.TabIndex = 0
        Me._RichTextBox.Text = String.Empty
        '
        '_MainMenu
        '
        Me._MainMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._FileMenu, Me._FormatMenu})
        Me._MainMenu.Location = New System.Drawing.Point(0, 0)
        Me._MainMenu.Name = "_MainMenu"
        Me._MainMenu.Size = New System.Drawing.Size(560, 24)
        Me._MainMenu.TabIndex = 0
        Me._MainMenu.Text = "MenuStrip1"
        '
        '_FileMenu
        '
        Me._FileMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._FilePageSetupMenu, Me._FilePrintPreviewMenu, Me._FilePrintMenu, Me._FileSeparatorMenuItem, Me._FileExitMenu})
        Me._FileMenu.Name = "_FileMenu"
        Me._FileMenu.Size = New System.Drawing.Size(37, 20)
        Me._FileMenu.Text = "&File"
        '
        '_FilePageSetupMenu
        '
        Me._FilePageSetupMenu.Name = "_FilePageSetupMenu"
        Me._FilePageSetupMenu.Size = New System.Drawing.Size(180, 22)
        Me._FilePageSetupMenu.Text = "Page &Setup..."
        '
        '_FilePrintPreviewMenu
        '
        Me._FilePrintPreviewMenu.Name = "_FilePrintPreviewMenu"
        Me._FilePrintPreviewMenu.Size = New System.Drawing.Size(180, 22)
        Me._FilePrintPreviewMenu.Text = "Print Pre&view..."
        '
        '_FilePrintMenu
        '
        Me._FilePrintMenu.Name = "_FilePrintMenu"
        Me._FilePrintMenu.Size = New System.Drawing.Size(180, 22)
        Me._FilePrintMenu.Text = "&Print..."
        '
        '_FileSeparatorMenuItem
        '
        Me._FileSeparatorMenuItem.Name = "_FileSeparatorMenuItem"
        Me._FileSeparatorMenuItem.Size = New System.Drawing.Size(177, 6)
        '
        '_FileExitMenu
        '
        Me._FileExitMenu.Name = "_FileExitMenu"
        Me._FileExitMenu.Size = New System.Drawing.Size(180, 22)
        Me._FileExitMenu.Text = "E&xit"
        '
        '_FormatMenu
        '
        Me._FormatMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._FormatFontStyleMenu, Me._FormatFontMenu, Me._FormatFontSizeMenu})
        Me._FormatMenu.Name = "_FormatMenu"
        Me._FormatMenu.Size = New System.Drawing.Size(57, 20)
        Me._FormatMenu.Text = "F&ormat"
        '
        '_FormatFontStyleMenu
        '
        Me._FormatFontStyleMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._FormatBoldMenu, Me._FormatItalicMenu, Me._FormatUnderlinedMenu})
        Me._FormatFontStyleMenu.Name = "_FormatFontStyleMenu"
        Me._FormatFontStyleMenu.Size = New System.Drawing.Size(180, 22)
        Me._FormatFontStyleMenu.Text = "Font St&yle"
        '
        '_FormatBoldMenu
        '
        Me._FormatBoldMenu.Name = "_FormatBoldMenu"
        Me._FormatBoldMenu.Size = New System.Drawing.Size(132, 22)
        Me._FormatBoldMenu.Text = "&Bold"
        '
        '_FormatItalicMenu
        '
        Me._FormatItalicMenu.Name = "_FormatItalicMenu"
        Me._FormatItalicMenu.Size = New System.Drawing.Size(132, 22)
        Me._FormatItalicMenu.Text = "&Italic"
        '
        '_FormatUnderlinedMenu
        '
        Me._FormatUnderlinedMenu.Name = "_FormatUnderlinedMenu"
        Me._FormatUnderlinedMenu.Size = New System.Drawing.Size(132, 22)
        Me._FormatUnderlinedMenu.Text = "&Underlined"
        '
        '_FormatFontMenu
        '
        Me._FormatFontMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._FormatFontArialMenu, Me._FormatFontCourierMenu, Me._FormatFontLucidaMenuItem, Me._FormatFontSegoeUIMenu, Me._FormatFontTimesMenu})
        Me._FormatFontMenu.Name = "_FormatFontMenu"
        Me._FormatFontMenu.Size = New System.Drawing.Size(180, 22)
        Me._FormatFontMenu.Text = "&Font"
        '
        '_FormatFontArialMenu
        '
        Me._FormatFontArialMenu.Name = "_FormatFontArialMenu"
        Me._FormatFontArialMenu.Size = New System.Drawing.Size(174, 22)
        Me._FormatFontArialMenu.Text = "Arial"
        '
        '_FormatFontCourierMenu
        '
        Me._FormatFontCourierMenu.Name = "_FormatFontCourierMenu"
        Me._FormatFontCourierMenu.Size = New System.Drawing.Size(174, 22)
        Me._FormatFontCourierMenu.Text = "Courier"
        '
        '_FormatFontLucidaMenuItem
        '
        Me._FormatFontLucidaMenuItem.Name = "_FormatFontLucidaMenuItem"
        Me._FormatFontLucidaMenuItem.Size = New System.Drawing.Size(174, 22)
        Me._FormatFontLucidaMenuItem.Text = "Lucida Console"
        '
        '_FormatFontSegoeUIMenu
        '
        Me._FormatFontSegoeUIMenu.Name = "_FormatFontSegoeUIMenu"
        Me._FormatFontSegoeUIMenu.Size = New System.Drawing.Size(174, 22)
        Me._FormatFontSegoeUIMenu.Text = "Segoe UI"
        '
        '_FormatFontTimesMenu
        '
        Me._FormatFontTimesMenu.Name = "_FormatFontTimesMenu"
        Me._FormatFontTimesMenu.Size = New System.Drawing.Size(174, 22)
        Me._FormatFontTimesMenu.Text = "Times New Roman"
        '
        '_FormatFontSizeMenu
        '
        Me._FormatFontSizeMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._FormatFontSize8Menu, Me._FormatFontSize10Menu, Me._FormatFontSize12Menu, Me._FormatFontSize18Menu, Me._FormatFontSize24Menu})
        Me._FormatFontSizeMenu.Name = "_FormatFontSizeMenu"
        Me._FormatFontSizeMenu.Size = New System.Drawing.Size(180, 22)
        Me._FormatFontSizeMenu.Text = "Font &Size"
        '
        '_FormatFontSize8Menu
        '
        Me._FormatFontSize8Menu.Name = "_FormatFontSize8Menu"
        Me._FormatFontSize8Menu.Size = New System.Drawing.Size(86, 22)
        Me._FormatFontSize8Menu.Text = "8"
        '
        '_FormatFontSize10Menu
        '
        Me._FormatFontSize10Menu.Name = "_FormatFontSize10Menu"
        Me._FormatFontSize10Menu.Size = New System.Drawing.Size(86, 22)
        Me._FormatFontSize10Menu.Text = "10"
        '
        '_FormatFontSize12Menu
        '
        Me._FormatFontSize12Menu.Name = "_FormatFontSize12Menu"
        Me._FormatFontSize12Menu.Size = New System.Drawing.Size(86, 22)
        Me._FormatFontSize12Menu.Text = "12"
        '
        '_FormatFontSize18Menu
        '
        Me._FormatFontSize18Menu.Name = "_FormatFontSize18Menu"
        Me._FormatFontSize18Menu.Size = New System.Drawing.Size(86, 22)
        Me._FormatFontSize18Menu.Text = "18"
        '
        '_FormatFontSize24Menu
        '
        Me._FormatFontSize24Menu.Name = "_FormatFontSize24Menu"
        Me._FormatFontSize24Menu.Size = New System.Drawing.Size(86, 22)
        Me._FormatFontSize24Menu.Text = "24"
        '
        '_PrintDocument
        '
        '
        '_PageSetupDialog
        '
        Me._PageSetupDialog.Document = Me._PrintDocument
        '
        '_PrintPreviewDialog
        '
        Me._PrintPreviewDialog.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me._PrintPreviewDialog.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me._PrintPreviewDialog.ClientSize = New System.Drawing.Size(400, 300)
        Me._PrintPreviewDialog.Document = Me._PrintDocument
        Me._PrintPreviewDialog.Enabled = True
        Me._PrintPreviewDialog.Icon = CType(resources.GetObject("_PrintPreviewDialog.Icon"), System.Drawing.Icon)
        Me._PrintPreviewDialog.Name = "_PrintPreviewDialog"
        Me._PrintPreviewDialog.Visible = False
        '
        '_PrintDialog
        '
        Me._PrintDialog.Document = Me._PrintDocument
        '
        'RichTextEditControl
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me._RichTextBox)
        Me.Controls.Add(Me._MainMenu)
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "RichTextEditControl"
        Me.Size = New System.Drawing.Size(560, 322)
        Me._MainMenu.ResumeLayout(False)
        Me._MainMenu.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _RichTextBox As isr.Core.Controls.RichTextBox
    Private WithEvents _PrintDocument As System.Drawing.Printing.PrintDocument
    Private WithEvents _PageSetupDialog As System.Windows.Forms.PageSetupDialog
    Private WithEvents _PrintPreviewDialog As System.Windows.Forms.PrintPreviewDialog
    Private WithEvents _PrintDialog As System.Windows.Forms.PrintDialog

    Private WithEvents _MainMenu As System.Windows.Forms.MenuStrip
    Private WithEvents _FileMenu As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _FilePageSetupMenu As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _FilePrintPreviewMenu As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _FilePrintMenu As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _FileSeparatorMenuItem As System.Windows.Forms.ToolStripSeparator
    Private WithEvents _FileExitMenu As System.Windows.Forms.ToolStripMenuItem

    Private WithEvents _FormatMenu As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _FormatFontStyleMenu As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _FormatBoldMenu As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _FormatItalicMenu As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _FormatUnderlinedMenu As System.Windows.Forms.ToolStripMenuItem

    Private WithEvents _FormatFontSizeMenu As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _FormatFontSize8Menu As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _FormatFontSize10Menu As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _FormatFontSize12Menu As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _FormatFontSize18Menu As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _FormatFontSize24Menu As System.Windows.Forms.ToolStripMenuItem

    Private WithEvents _FormatFontMenu As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _FormatFontArialMenu As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _FormatFontCourierMenu As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _FormatFontLucidaMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _FormatFontSegoeUIMenu As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _FormatFontTimesMenu As System.Windows.Forms.ToolStripMenuItem
End Class
