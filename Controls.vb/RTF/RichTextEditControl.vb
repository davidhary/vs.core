Imports isr.Core.Forma

''' <summary>
''' Control for viewing and printing a <see cref="RichTextBox">rich text box</see>.
''' </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 1/8/2015 </para>
''' </remarks>
Public Class RichTextEditControl
    Inherits ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP  "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        MyBase.New()

        Me.InitializingComponents = True
        'This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False

        'Add any initialization after the InitializeComponent() call
        Me._FormatFontSegoeUIMenu.Text = Drawing.SystemFonts.MessageBoxFont.Name

    End Sub

    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the
    ''' <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    ''' <summary> Creates a form. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="caption">        The caption. </param>
    ''' <param name="tabTitle">       The tab title. </param>
    ''' <param name="richTextEditor"> The rich text editor. </param>
    ''' <param name="logListener">    The log listener. </param>
    ''' <returns> The new form. </returns>
    Public Shared Function CreateForm(ByVal caption As String, ByVal tabTitle As String,
                                      ByVal richTextEditor As ModelViewTalkerBase,
                                      ByVal logListener As IMessageListener) As ConsoleForm
        Dim result As ConsoleForm = Nothing
        Try
            result = New ConsoleForm With {.Text = caption}
            result.AddTalkerControl(tabTitle, richTextEditor, True)

            result.AddListener(logListener)
        Catch
            If result IsNot Nothing Then
                result.Dispose()
            End If
            Throw
        End Try
        Return result
    End Function

#End Region

#Region " PRINT SETTINGS "

    ''' <summary> Gets a reference to the print document. </summary>
    ''' <value> The print document. </value>
    Public ReadOnly Property PrintDocument As System.Drawing.Printing.PrintDocument
        Get
            Return Me._PrintDocument
        End Get
    End Property

    ''' <summary> Gets the reference to the rich text box. </summary>
    ''' <value> The rich text box. </value>
    Public ReadOnly Property RichTextBox As RichTextBox
        Get
            Return Me._RichTextBox
        End Get
    End Property

#End Region

#Region " PRINT DIALOG "

    ''' <summary> The first character on page. </summary>
    Private _FirstCharOnPage As Integer

    ''' <summary> Print document begin print. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Print event information. </param>
    Private Sub PrintDocument_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles _PrintDocument.BeginPrint
        ' Start at the beginning of the text
        Me._FirstCharOnPage = 0
    End Sub

    ''' <summary> Print document print page. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Print page event information. </param>
    Private Sub PrintDocument_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles _PrintDocument.PrintPage
        ' To print the boundaries of the current page margins
        ' uncomment the next line:
        e.Graphics.DrawRectangle(System.Drawing.Pens.Blue, e.MarginBounds)

        ' make the RichTextBoxEx calculate and render as much text as will
        ' fit on the page and remember the last character printed for the
        ' beginning of the next page
        Me._FirstCharOnPage = Me._RichTextBox.FormatRange(False, e, Me._FirstCharOnPage, Me._RichTextBox.TextLength)

        ' check if there are more pages to print
        e.HasMorePages = Me._FirstCharOnPage < Me._RichTextBox.TextLength
    End Sub

    ''' <summary> Print document end print. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Print event information. </param>
    Private Sub PrintDocument_EndPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles _PrintDocument.EndPrint
        ' Clean up cached information
        Me._RichTextBox.FormatRangeDone()
    End Sub

#End Region

#Region " FILE MENU "

    ''' <summary> File page setup menu click. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub FilePageSetupMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _FilePageSetupMenu.Click
        Me._PageSetupDialog.ShowDialog()
    End Sub

    ''' <summary> File print preview menu click. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub FilePrintPreviewMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _FilePrintPreviewMenu.Click
        If Me._PrintPreviewDialog.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            Me._PrintDocument.Print()
        End If
    End Sub

    ''' <summary> File print menu click. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub FilePrintMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _FilePrintMenu.Click
        Me._PrintDocument.Print()
    End Sub

#End Region

#Region " FORMAT MENU "

    ''' <summary> Format bold menu click. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub FormatBoldMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _FormatBoldMenu.Click
        Me._RichTextBox.SetSelectionBold(True)
    End Sub

    ''' <summary> Format italic menu click. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub FormatItalicMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _FormatItalicMenu.Click
        Me._RichTextBox.SetSelectionItalic(True)
    End Sub

    ''' <summary> Format underlined menu click. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub FormatUnderlinedMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _FormatUnderlinedMenu.Click
        Me._RichTextBox.SetSelectionUnderlined(True)
    End Sub

    ''' <summary> Format font size 8 menu click. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub FormatFontSize8Menu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _FormatFontSize8Menu.Click
        Me._RichTextBox.SetSelectionSize(8)
    End Sub

    ''' <summary> Format font size 10 menu click. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub FormatFontSize10Menu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _FormatFontSize10Menu.Click
        Me._RichTextBox.SetSelectionSize(10)
    End Sub

    ''' <summary> Font size 12 menu click. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub FontSize12Menu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _FormatFontSize12Menu.Click
        Me._RichTextBox.SetSelectionSize(12)
    End Sub

    ''' <summary> Format font size 18 menu click. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub FormatFontSize18Menu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _FormatFontSize18Menu.Click
        Me._RichTextBox.SetSelectionSize(18)
    End Sub

    ''' <summary> Format font size 24 menu click. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub FormatFontSize24Menu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _FormatFontSize24Menu.Click
        Me._RichTextBox.SetSelectionSize(24)
    End Sub

    ''' <summary> Format font arial menu click. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub FormatFontArialMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _FormatFontArialMenu.Click
        Me._RichTextBox.SetSelectionFont("Arial")
    End Sub

    ''' <summary> Format font courier menu click. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub FormatFontCourierMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _FormatFontCourierMenu.Click
        Me._RichTextBox.SetSelectionFont("Courier New")
    End Sub

    ''' <summary> Format font lucida menu item click. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub FormatFontLucidaMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles _FormatFontLucidaMenuItem.Click
        Me._RichTextBox.SetSelectionFont("Lucida Console")
    End Sub

    ''' <summary> Format font segoe menu item click. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub FormatFontSegoeMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles _FormatFontSegoeUIMenu.Click
        Me._RichTextBox.SetSelectionFont(Drawing.SystemFonts.MessageBoxFont.Name)
    End Sub

    ''' <summary> Format font times menu click. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub FormatFontTimesMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _FormatFontTimesMenu.Click
        Me._RichTextBox.SetSelectionFont("Times New Roman")
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Protected Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Identify(Me.Talker)
    End Sub

    ''' <summary> Adds the listeners such as the top level trace messages box and log. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="listener"> The listener. </param>
    Public Overrides Sub AddListener(ByVal listener As IMessageListener)
        MyBase.AddListener(listener)
    End Sub

#End Region

End Class
