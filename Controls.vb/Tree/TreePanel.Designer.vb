<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Public Class TreePanel

    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me._NavigatorTreeView = New System.Windows.Forms.TreeView()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        '_NavigatorTreeView
        '
        Me._NavigatorTreeView.Dock = System.Windows.Forms.DockStyle.Fill
        Me._NavigatorTreeView.LineColor = System.Drawing.Color.Empty
        Me._NavigatorTreeView.Location = New System.Drawing.Point(0, 0)
        Me._NavigatorTreeView.Name = "_NavigatorTreeView"
        Me._NavigatorTreeView.Size = New System.Drawing.Size(50, 100)
        Me._NavigatorTreeView.TabIndex = 0
        '
        'TreePanel
        '
        Me.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        '
        '
        Me.Panel1.Controls.Add(Me._NavigatorTreeView)
        Me.Panel1.ResumeLayout(False)
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _NavigatorTreeView As Windows.Forms.TreeView

End Class
