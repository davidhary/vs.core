Imports System.ComponentModel

Imports isr.Core.Controls.ExceptionExtensions

''' <summary> A tree split panel for holding multiple controls. </summary>
''' <remarks> David, 2020-09-02. </remarks>
Public Class TreePanel
    Inherits SplitContainer

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Gets the initializing components. </summary>
    ''' <value> The initializing components. </value>
    Protected Property InitializingComponents As Boolean

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me._NavigatorTreeView.Nodes.Clear()
        Me._TreeNodes = New Dictionary(Of String, TreeNode)
        Me._NodeControls = New Dictionary(Of String, Control)
        Me._NodesVisited = New List(Of String)
        Me.InitializingComponents = False
    End Sub

    ''' <summary> Creates a new <see cref="TreePanel"/> </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> A <see cref="TreePanel"/>. </returns>
    Public Shared Function Create() As TreePanel
        Dim control As TreePanel = Nothing
        Try
            control = New TreePanel
            Return control
        Catch
            control?.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.InitializingComponents = True
                Me._NavigatorTreeView?.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " TREE NODES "

    ''' <summary> Clears the nodes. </summary>
    ''' <remarks> David, 2020-09-03. </remarks>
    Public Sub ClearNodes()
        Me._NavigatorTreeView.Nodes.Clear()
        Me.TreeNodes.Clear()
        Me.NodeControls.Clear()
        Me.NodesVisited.Clear()
    End Sub

    ''' <summary> Gets the number of nodes. </summary>
    ''' <value> The number of nodes. </value>
    Public ReadOnly Property NodeCount As Integer
        Get
            Return Me._NavigatorTreeView.Nodes.Count
        End Get
    End Property

    ''' <summary> Removes the node. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="nodeName"> Name of the node. </param>
    Public Sub RemoveNode(ByVal nodeName As String)
        Me.TreeNodes.Remove(nodeName)
        Me.NodeControls.Remove(nodeName)
        Me._NavigatorTreeView.Nodes.RemoveByKey(nodeName)
    End Sub

    ''' <summary> Adds a node. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="nodeName">    Name of the node. </param>
    ''' <param name="nodeCaption"> The node caption. </param>
    ''' <param name="nodeControl"> The node control. </param>
    ''' <returns> A TreeNode. </returns>
    Public Function AddNode(ByVal nodeName As String, ByVal nodeCaption As String, ByVal nodeControl As Control) As TreeNode
        Dim newNode As New TreeNode With {.Name = nodeName, .Text = nodeCaption}
        Me.TreeNodes.Add(nodeName, newNode)
        Me._NavigatorTreeView.Nodes.Add(Me._TreeNodes(nodeName))
        Me.NodeControls.Add(nodeName, nodeControl)
        Return newNode
    End Function

    ''' <summary> Inserts a node. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="index">       Zero-based index of the. </param>
    ''' <param name="nodeName">    Name of the node. </param>
    ''' <param name="nodeCaption"> The node caption. </param>
    ''' <param name="nodeControl"> The node control. </param>
    ''' <returns> A TreeNode. </returns>
    Public Function InsertNode(ByVal index As Integer, ByVal nodeName As String, ByVal nodeCaption As String, ByVal nodeControl As Control) As TreeNode
        Dim newNode As New TreeNode With {.Name = nodeName, .Text = nodeCaption}
        Me.TreeNodes.Add(nodeName, newNode)
        Me._NavigatorTreeView.Nodes.Insert(index, Me._TreeNodes(nodeName))
        Me.NodeControls.Add(nodeName, nodeControl)
        Return newNode
    End Function

    ''' <summary> Gets a node. </summary>
    ''' <remarks> David, 2020-09-03. </remarks>
    ''' <param name="nodeName"> Name of the node. </param>
    ''' <returns> The node. </returns>
    Public Function GetNode(ByVal nodeName As String) As TreeNode
        Return If(Me.TreeNodes.ContainsKey(nodeName), Me.TreeNodes(nodeName), New TreeNode)
    End Function

#End Region

#Region " NAVIGATION "

    ''' <summary> Gets a dictionary of node controls. </summary>
    ''' <value> A dictionary of node controls. </value>
    Public ReadOnly Property NodeControls As IDictionary(Of String, Control)

    ''' <summary> Gets a dictionary of <see cref="TreeNode"/>. </summary>
    ''' <value> A dictionary of nodes. </value>
    Public ReadOnly Property TreeNodes As IDictionary(Of String, TreeNode)

    ''' <summary> Gets the last node selected. </summary>
    ''' <value> The last node selected. </value>
    Public ReadOnly Property LastNodeSelected As Windows.Forms.TreeNode

    ''' <summary> Gets the last tree view node selected. </summary>
    ''' <value> The last tree view node selected. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Protected ReadOnly Property LastTreeViewNodeSelected As String
        Get
            Return If(Me.LastNodeSelected Is Nothing, String.Empty, Me.LastNodeSelected.Name)
        End Get
    End Property

    ''' <summary> Gets or sets the nodes visited. </summary>
    ''' <value> The nodes visited. </value>
    Private ReadOnly Property NodesVisited As IList(Of String)

    ''' <summary> Select active control. </summary>
    ''' <remarks> David, 2020-09-10. </remarks>
    ''' <param name="nodeName"> Name of the node. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Protected Overridable Function SelectActiveControl(ByVal nodeName As String) As Boolean
        If Me.NodeControls.ContainsKey(nodeName) Then

            Dim activeControl As Control = Me._NodeControls(nodeName)

            ' turn off the visibility of the current panel, this turns off the visibility of the
            ' contained controls, which will now be removed from the panel.
            Me.Panel2.Hide()
            Me.Panel2.Controls.Clear()

            If activeControl IsNot Nothing Then
                activeControl.Dock = DockStyle.None
                Me.Panel2.Controls.Add(activeControl)
                activeControl.Dock = DockStyle.Fill
                activeControl.Visible = True
            End If

            ' turn on visibility on the panel -- this toggles the visibility of the contained controls,
            ' which is required for the messages boxes.
            Me.Panel2.Show()

            If Not Me.NodesVisited.Contains(nodeName) Then Me.NodesVisited.Add(nodeName)
            Return True
        Else
            Return False
        End If

    End Function

    ''' <summary> Called after a node is selected. Displays to relevant screen. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="nodeName"> The node. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overridable Sub OnNodeSelected(ByVal nodeName As String)
        Me.SelectActiveControl(nodeName)
    End Sub

    ''' <summary>
    ''' Gets or sets a value indicating whether this <see cref="Console"/> is navigating.
    ''' </summary>
    ''' <remarks>
    ''' Used to ignore changes in grids during the navigation. The grids go through selecting their
    ''' rows when navigating.
    ''' </remarks>
    ''' <value> <c>True</c> if navigating; otherwise, <c>False</c>. </value>
    Public ReadOnly Property Navigating As Boolean

    ''' <summary> Handles the BeforeSelect event of the _NavigatorTreeView control. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.Windows.Forms.TreeViewCancelEventArgs"/> instance
    '''                       containing the event data. </param>
    Private Sub NavigatorTreeView_BeforeSelect(sender As Object, e As System.Windows.Forms.TreeViewCancelEventArgs) Handles _NavigatorTreeView.BeforeSelect
        If e IsNot Nothing Then
            If Me._NodeControls.ContainsKey(e.Node.Name) Then
                Dim activeControl As Control = Me._NodeControls(e.Node.Name)
                If activeControl IsNot Nothing Then activeControl.Visible = False
                Me._Navigating = True
            End If
        End If
    End Sub

    ''' <summary> Handles the AfterSelect event of the Me._NavigatorTreeView control. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.Windows.Forms.TreeViewEventArgs" /> instance
    '''                       containing the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub NavigatorTreeView_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles _NavigatorTreeView.AfterSelect
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String
        Try
            activity = $"{Me.Name} handling navigator after select event"
            If Me.LastNodeSelected IsNot Nothing Then
                Me.LastNodeSelected.BackColor = Me._NavigatorTreeView.BackColor
            End If
            If (e.Node?.IsSelected).GetValueOrDefault(False) Then
                Me._LastNodeSelected = e.Node
                e.Node.BackColor = System.Drawing.SystemColors.Highlight
                Me.OnNodeSelected(Me.LastTreeViewNodeSelected)
                Dim evt As EventHandler(Of System.Windows.Forms.TreeViewEventArgs) = Me.AfterNodeSelectedEvent
                evt?.Invoke(Me, e)
            End If
        Catch ex As Exception
            Me.OnNavigationError(True, New System.IO.ErrorEventArgs(ex))
        Finally
            Me._Navigating = False
        End Try
    End Sub

    ''' <summary> Select navigator tree view first node. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub SelectNavigatorTreeViewFirstNode()
        If Me.NodeCount > 0 Then Me._NavigatorTreeView.SelectedNode = Me._NavigatorTreeView.Nodes(0)
    End Sub

    ''' <summary> Selects the navigator tree view node. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="nodeName"> The node. </param>
    Public Sub SelectNavigatorTreeViewNode(ByVal nodeName As String)
        Me._NavigatorTreeView.SelectedNode = Me._NavigatorTreeView.Nodes(nodeName)
    End Sub

    ''' <summary> Event queue for all listeners interested in AfterNodeSelected events. </summary>
    Public Event AfterNodeSelected As EventHandler(Of System.Windows.Forms.TreeViewEventArgs)

#End Region

#Region " NAVIGATION ERROR "

    ''' <summary> Event queue for all listeners interested in NavigationError events. </summary>
    Public Event NavigationError As EventHandler(Of System.IO.ErrorEventArgs)

    ''' <summary> Raises the <see cref="System.IO.ErrorEventArgs"/>. </summary>
    ''' <remarks> David, 2020-09-01. </remarks>
    ''' <param name="displayErrorDialogIfNoHandler"> True to display error dialog if no handler. </param>
    ''' <param name="e">                             Event information to send to registered event
    '''                                              handlers. </param>
    Protected Overridable Sub OnNavigationError(displayErrorDialogIfNoHandler As Boolean, e As System.IO.ErrorEventArgs)
        Dim evt As EventHandler(Of System.IO.ErrorEventArgs) = Me.NavigationErrorEvent
        If evt Is Nothing Then
            If displayErrorDialogIfNoHandler Then
                MessageBox.Show("Navigation Exception", e.GetException.ToFullBlownString)
            End If
        Else
            evt.Invoke(Me, New System.IO.ErrorEventArgs(e.GetException))
        End If
    End Sub

#End Region

End Class
