Imports System.Windows.Forms.Design
Imports System.ComponentModel

''' <summary> A tool strip spring text box. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 3/22/2017 </para>
''' </remarks>
<ToolStripItemDesignerAvailability(ToolStripItemDesignerAvailability.ToolStrip)>
Public Class ToolStripTextBox
    Inherits System.Windows.Forms.ToolStripTextBox
    Implements ISpringable, IBindableComponent

#Region " BINDABLE "

    ''' <summary> The context. </summary>
    Private _Context As BindingContext = Nothing

    ''' <summary>
    ''' Gets or sets the collection of currency managers for the
    ''' <see cref="T:System.Windows.Forms.IBindableComponent" />.
    ''' </summary>
    ''' <value>
    ''' The collection of <see cref="T:System.Windows.Forms.BindingManagerBase" /> objects for this
    ''' <see cref="T:System.Windows.Forms.IBindableComponent" />.
    ''' </value>
    Public Property BindingContext() As BindingContext Implements IBindableComponent.BindingContext
        Get
            If Nothing Is Me._Context Then Me._Context = New BindingContext()
            Return Me._Context
        End Get
        Set(ByVal value As BindingContext)
            Me._Context = value
        End Set
    End Property

    ''' <summary> The bindings. </summary>
    Private _Bindings As ControlBindingsCollection

    ''' <summary>
    ''' Gets the collection of data-binding objects for this
    ''' <see cref="T:System.Windows.Forms.IBindableComponent" />.
    ''' </summary>
    ''' <value>
    ''' The <see cref="T:System.Windows.Forms.ControlBindingsCollection" /> for this
    ''' <see cref="T:System.Windows.Forms.IBindableComponent" />.
    ''' </value>
    Public ReadOnly Property DataBindings() As ControlBindingsCollection Implements IBindableComponent.DataBindings
        Get
            If Me._Bindings Is Nothing Then Me._Bindings = New ControlBindingsCollection(Me)
            Return Me._Bindings
        End Get
    End Property

#End Region

#Region " I SPRINGABLE "

    ''' <summary> Gets the sentinel indicating if the tool strip item can spring. </summary>
    ''' <value> The sentinel indicating if the tool strip item can spring. </value>
    Public ReadOnly Property CanSpring As Boolean Implements ISpringable.CanSpring
        Get
            Return Me.AutoSize AndAlso Me.Spring
        End Get
    End Property

    ''' <summary> Gets or sets the spring. </summary>
    ''' <value>
    ''' <c>true</c> if the control stretches to fill the remaining space in the owner control.
    ''' </value>
    <DefaultValue(False), Description("Spring"), Category("Appearance")>
    Public Property Spring As Boolean Implements ISpringable.Spring

#End Region

#Region " SPRING IMPLEMENTATION "

    ''' <summary>
    ''' Retrieves the size of a rectangular area into which a control can be fitted.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="constrainingSize"> The custom-sized area for a control. </param>
    ''' <returns>
    ''' An ordered pair of type <see cref="T:System.Drawing.Size" /> representing the width and
    ''' height of a rectangle.
    ''' </returns>
    Public Overrides Function GetPreferredSize(ByVal constrainingSize As Size) As Size

        ' Use the default size if the tool strip item is on the overflow menu,
        ' is on a vertical ToolStrip, or cannot spring.
        If Me.IsOnOverflow OrElse Me.Owner.Orientation = Orientation.Vertical OrElse Not Me.CanSpring Then
            Return Me.DefaultSize
        End If

        ' Declare a variable to store the total available width as 
        ' it is calculated, starting with the display width of the 
        ' owning ToolStrip.
        Dim width As Int32 = Me.Owner.DisplayRectangle.Width

        ' Subtract the width of the overflow button if it is displayed. 
        If Me.Owner.OverflowButton.Visible Then
            width = width - Me.Owner.OverflowButton.Width - Me.Owner.OverflowButton.Margin.Horizontal()
        End If
        If Me.Owner.GripStyle = ToolStripGripStyle.Visible Then
            width -= Me.Owner.GripMargin.Horizontal
        End If

        ' Declare a variable to maintain a count of Spring items
        ' currently displayed in the owning ToolStrip. 
        Dim springItemCount As Int32 = 0

        For Each item As ToolStripItem In Me.Owner.Items

            ' Ignore items on the overflow menu.
            If item.IsOnOverflow Then Continue For

            If TryCast(item, ISpringable)?.CanSpring Then
                ' For Spring items, increment the count and 
                ' subtract the margin width from the total available width.
                springItemCount += 1
                width -= item.Margin.Horizontal
            Else
                ' For all other items, subtract the full width from the total
                ' available width.
                width = width - item.Width - item.Margin.Horizontal
            End If

        Next

        ' If there are multiple spring  items in the owning
        ' ToolStrip, divide the total available width between them. 
        If springItemCount > 1 Then width \= springItemCount

        ' If the available width is less than the default width, use the
        ' default width, forcing one or more items onto the overflow menu.
        If width < Me.DefaultSize.Width Then width = Me.DefaultSize.Width

        ' Retrieve the preferred size from the base class, but change the
        ' width to the calculated width. 
        Dim preferredSize As Size = MyBase.GetPreferredSize(constrainingSize)
        preferredSize.Width = width
        Return preferredSize

    End Function

#End Region

End Class
