Imports System.ComponentModel
Imports System.Windows.Forms.Design

Imports isr.Core.Controls.ExceptionExtensions

''' <summary> Tool strip numeric up down. </summary>
''' <remarks> David, 4/16/2014. </remarks>
<ToolStripItemDesignerAvailability(ToolStripItemDesignerAvailability.ToolStrip)>
Public Class ToolStripNumericUpDown
    Inherits ToolStripControlHost
    Implements IBindableComponent

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> Call the base constructor passing in a NumericUpDown instance. </remarks>
    Public Sub New()
        MyBase.New(New NumericUpDown())
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the
    ''' <see cref="T:System.Windows.Forms.ToolStripControlHost" /> and optionally releases the
    ''' managed resources.
    ''' </summary>
    ''' <remarks> David, 4/16/2014. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.RemoveValueChangedEventHandler(Me.ValueChangedEvent)
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " BINDABLE "

    ''' <summary> The binding context. </summary>
    Private _BindingContext As BindingContext = Nothing

    ''' <summary>
    ''' Gets or sets the collection of currency managers for the
    ''' <see cref="T:System.Windows.Forms.IBindableComponent" />.
    ''' </summary>
    ''' <value>
    ''' The collection of <see cref="T:System.Windows.Forms.BindingManagerBase" /> objects for this
    ''' <see cref="T:System.Windows.Forms.IBindableComponent" />.
    ''' </value>
    Public Property BindingContext() As BindingContext Implements IBindableComponent.BindingContext
        Get
            If Nothing Is Me._BindingContext Then Me._BindingContext = New BindingContext()
            Return Me._BindingContext
        End Get
        Set(ByVal value As BindingContext)
            Me._BindingContext = value
        End Set
    End Property

    ''' <summary> The bindings. </summary>
    Private _Bindings As ControlBindingsCollection

    ''' <summary>
    ''' Gets the collection of data-binding objects for this
    ''' <see cref="T:System.Windows.Forms.IBindableComponent" />.
    ''' </summary>
    ''' <value>
    ''' The <see cref="T:System.Windows.Forms.ControlBindingsCollection" /> for this
    ''' <see cref="T:System.Windows.Forms.IBindableComponent" />.
    ''' </value>
    Public ReadOnly Property DataBindings() As ControlBindingsCollection Implements IBindableComponent.DataBindings
        Get
            If Me._Bindings Is Nothing Then Me._Bindings = New ControlBindingsCollection(Me)
            Return Me._Bindings
        End Get
    End Property

#End Region

#Region " CONTROL "

    ''' <summary> Gets the numeric up down control. </summary>
    ''' <value> The numeric up down control. </value>
    Public ReadOnly Property NumericUpDownControl() As NumericUpDown
        Get
            Return TryCast(Me.Control, NumericUpDown)
        End Get
    End Property

#End Region

#Region " CONTROL PROPERTIES "

    ''' <summary> Gets or sets the selected text. </summary>
    ''' <value> The selected text. </value>
    <DefaultValue("")>
    <Description("text"), Category("Appearance")>
    Public Overrides Property Text() As String
        Get
            Return Me.NumericUpDownControl.Text
        End Get
        Set(ByVal value As String)
            ToolStripNumericUpDown.SafeSetter(Me.NumericUpDownControl, Sub()
                                                                           Me.NumericUpDownControl.Text = value
                                                                       End Sub)
        End Set
    End Property

    ''' <summary> Safe setter. </summary>
    ''' <remarks> David, 4/16/2014. </remarks>
    ''' <param name="control"> The control from which to subscribe events. </param>
    ''' <param name="setter">  The setter. </param>
    Private Shared Sub SafeSetter(ByVal control As Control, ByVal setter As Action)
        If control IsNot Nothing AndAlso control.Parent IsNot Nothing Then
            If control.Parent.InvokeRequired Then
                control.Parent.Invoke(New Action(Of Control, Action)(AddressOf ToolStripNumericUpDown.SafeSetter), New Object() {control, setter})
            ElseIf control.Parent.IsHandleCreated Then
                setter.Invoke
            End If
        End If
    End Sub

    ''' <summary> Gets or sets the value. </summary>
    ''' <value> The value. </value>
    <DefaultValue("")>
    <Description("Value"), Category("Appearance")>
    Public Property Value() As Decimal
        Get
            Return Me.NumericUpDownControl.Value
        End Get
        Set(ByVal value As Decimal)
            ToolStripNumericUpDown.SafeSetter(Me.NumericUpDownControl, Sub()
                                                                           Me.NumericUpDownControl.Value = value
                                                                       End Sub)
        End Set
    End Property

    ''' <summary> Gets a value indicating whether this object has value. </summary>
    ''' <value> <c>true</c> if this object has value; otherwise <c>false</c> </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property HasValue As Boolean
        Get
            Return Me.NumericUpDownControl.HasValue
        End Get
    End Property

#End Region

#Region " EVENTS "

    ''' <summary> Event queue for all listeners interested in ValueChanged events. </summary>
    Public Event ValueChanged As EventHandler(Of System.EventArgs)

    ''' <summary> Removes event handler. </summary>
    ''' <remarks> David, 4/16/2014. </remarks>
    ''' <param name="value"> The handler. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub RemoveValueChangedEventHandler(ByVal value As EventHandler(Of EventArgs))
#Disable Warning CA1825 ' Avoid zero-length array allocations.
        For Each d As [Delegate] In If(value Is Nothing, New [Delegate]() {}, value.GetInvocationList)
#Enable Warning CA1825 ' Avoid zero-length array allocations.
            Try
                RemoveHandler Me.ValueChanged, CType(d, EventHandler(Of EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

    ''' <summary> Raises the value changed event. </summary>
    ''' <remarks> David, 4/16/2014. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information to send to registered event handlers. </param>
    Private Sub OnValueChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim evt As EventHandler(Of System.EventArgs) = Me.ValueChangedEvent
        evt?.Invoke(Me, e)
    End Sub

#End Region

#Region " TOOL STRIP MANAGEMENT "

    ''' <summary> Subscribes events from the hosted control. </summary>
    ''' <remarks> Subscribe the control events to expose. </remarks>
    ''' <param name="control"> The control from which to subscribe events. </param>
    Protected Overrides Sub OnSubscribeControlEvents(ByVal control As Control)

        If control IsNot Nothing Then

            ' Call the base so the base events are connected.
            MyBase.OnSubscribeControlEvents(control)

            ' Cast the control to a NumericUpDown control.
            Dim numericControl As NumericUpDown = TryCast(control, NumericUpDown)

            If numericControl IsNot Nothing Then
                ' Add the event.
                AddHandler numericControl.ValueChanged, AddressOf Me.OnValueChanged
            End If

        End If
    End Sub

    ''' <summary> Unsubscribes events from the hosted control. </summary>
    ''' <remarks> David, 4/16/2014. </remarks>
    ''' <param name="control"> The control from which to unsubscribe events. </param>
    Protected Overrides Sub OnUnsubscribeControlEvents(ByVal control As Control)

        ' Call the base method so the basic events are unsubscribed.
        MyBase.OnUnsubscribeControlEvents(control)

        ' Cast the control to a NumericUpDown control.
        Dim numericControl As NumericUpDown = TryCast(control, NumericUpDown)

        If numericControl IsNot Nothing Then
            ' Remove the event.
            RemoveHandler numericControl.ValueChanged, AddressOf Me.OnValueChanged
        End If

    End Sub

#End Region

End Class

