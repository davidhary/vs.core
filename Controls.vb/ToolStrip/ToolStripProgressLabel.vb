Imports System.ComponentModel
Imports System.Windows.Forms.Design

''' <summary> A tool strip progress label. </summary>
''' <remarks>
''' (c) 2007 Hypercubed. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/19/2016. http://www.codeproject.com/script/Membership/View.aspx?mid=722189
''' http://www.codeproject.com/Articles/21419/Label-with-ProgressBar-in-a-StatusStrip.
''' </para>
''' </remarks>
<DesignerCategory("code"), Description("Status label with progress bar")>
<ToolStripItemDesignerAvailability(ToolStripItemDesignerAvailability.StatusStrip)>
Public Class ToolStripProgressLabel
    Inherits Windows.Forms.ToolStripStatusLabel
    Implements IBindableComponent

#Region " BINDABLE "

    ''' <summary> The context. </summary>
    Private _Context As BindingContext = Nothing

    ''' <summary>
    ''' Gets or sets the collection of currency managers for the
    ''' <see cref="T:System.Windows.Forms.IBindableComponent" />.
    ''' </summary>
    ''' <value>
    ''' The collection of <see cref="T:System.Windows.Forms.BindingManagerBase" /> objects for this
    ''' <see cref="T:System.Windows.Forms.IBindableComponent" />.
    ''' </value>
    Public Property BindingContext() As BindingContext Implements IBindableComponent.BindingContext
        Get
            If Nothing Is Me._Context Then Me._Context = New BindingContext()
            Return Me._Context
        End Get
        Set(ByVal value As BindingContext)
            Me._Context = value
        End Set
    End Property

    ''' <summary> The bindings. </summary>
    Private _Bindings As ControlBindingsCollection

    ''' <summary>
    ''' Gets the collection of data-binding objects for this
    ''' <see cref="T:System.Windows.Forms.IBindableComponent" />.
    ''' </summary>
    ''' <value>
    ''' The <see cref="T:System.Windows.Forms.ControlBindingsCollection" /> for this
    ''' <see cref="T:System.Windows.Forms.IBindableComponent" />.
    ''' </value>
    Public ReadOnly Property DataBindings() As ControlBindingsCollection Implements IBindableComponent.DataBindings
        Get
            If Me._Bindings Is Nothing Then Me._Bindings = New ControlBindingsCollection(Me)
            Return Me._Bindings
        End Get
    End Property

#End Region

#Region " PROGRESS IMPLEMENTATION "

    ''' <summary> Paints this window. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.PaintEventArgs" />
    '''                   that contains the event data. </param>
    Protected Overrides Sub OnPaint(ByVal e As PaintEventArgs)
        If e Is Nothing Then Return
        Dim percent As Double = Me.Value / 100
        Dim rect As Rectangle = e.ClipRectangle

        Dim height As Integer = Me.BarHeight
        If height = 0 Then height = rect.Height

        rect.Width = CInt(rect.Width * percent)
        rect.Y = CInt(0.5 * (rect.Height - height))
        rect.Height = height

        Using brush As SolidBrush = New SolidBrush(Me.BarColor)
            ' Draw bar
            Dim g As Graphics = e.Graphics
            g.FillRectangle(brush, rect)
        End Using
        MyBase.OnPaint(e)
    End Sub

    ''' <summary> The value. </summary>
    Private _Value As Integer = 0               ' Current progress

    ''' <summary> Progress Value. </summary>
    ''' <value> The value. </value>
    <Category("Behavior"), Description("Progress Value"), DefaultValue(0)>
    Public Property Value() As Integer
        Get
            Return Me._Value
        End Get
        Set(ByVal value As Integer)
            ' Make sure that the value does not stray outside the valid range.
            value = If(value < 0, 0, If(value > 100, 100, value))
            If Me.Value <> value Then
                Me._Value = value
                Me.Invalidate()
            End If
        End Set
    End Property

    ''' <summary> The bar color. </summary>
    Private _BarColor As Color = Color.Blue   ' Color of progress meter

    ''' <summary> Progress color. </summary>
    ''' <value> The color of the bar. </value>
    <Category("Behavior"), Description("Progress color"), DefaultValue(GetType(Color), "Blue")>
    Public Property BarColor() As Color
        Get
            Return Me._BarColor
        End Get

        Set(ByVal value As Color)
            Me._BarColor = value

            ' Invalidate the control to get a repaint.
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> Height of the bar. </summary>
    Private _BarHeight As Integer = 0

    ''' <summary> Progress height. </summary>
    ''' <value> The height of the bar. </value>
    <Category("Behavior"), Description("Progress height"), DefaultValue(0)>
    Public Property BarHeight() As Integer
        Get
            Return Me._BarHeight
        End Get
        Set(ByVal value As Integer)
            Select Case value
                Case Is > Me.Size.Height, Is < 0
                    Me._BarHeight = Me.Size.Height
                Case Else
                    Me._BarHeight = value
            End Select

            ' Invalidate the control to get a repaint.
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The caption format. </summary>
    Private _CaptionFormat As String

    ''' <summary> Specifies the format of the overlay. </summary>
    ''' <value> The caption format. </value>
    <Category("Appearance"), DefaultValue("{0} %"),
        Description("Specifies the format of the overlay.")>
    Public Property CaptionFormat As String
        Get
            Return If(String.IsNullOrEmpty(Me._CaptionFormat), "{0} %", Me._CaptionFormat)
        End Get
        Set(value As String)
            Me._CaptionFormat = value
        End Set
    End Property

    ''' <summary> The default caption format. </summary>
    Public Const DefaultCaptionFormat As String = "{0} %"

    ''' <summary> Updates the progress described by value. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub UpdateProgress(ByVal value As Integer)
        Dim format As String = Me.CaptionFormat
        If String.IsNullOrEmpty(format) Then format = DefaultCaptionFormat
        Me.UpdateProgress(value, String.Format(Globalization.CultureInfo.CurrentCulture, format, value))
    End Sub

    ''' <summary> Updates the progress described by arguments. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value">   The value. </param>
    ''' <param name="caption"> The caption. </param>
    Public Sub UpdateProgress(ByVal value As Integer, ByVal caption As String)
        If value >= 0 Xor Me.Visible Then Me.Visible = value >= 0
        If Me.Visible Then
            Me.Text = caption
            Me.Value = value
        End If
    End Sub
#End Region

End Class
