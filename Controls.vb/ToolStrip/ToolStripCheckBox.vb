Imports System.Windows.Forms.Design

Imports isr.Core.Controls.ExceptionExtensions

''' <summary> Tool strip check box. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 01/23/2015 </para>
''' </remarks>
<ToolStripItemDesignerAvailability(ToolStripItemDesignerAvailability.ToolStrip)>
Public Class ToolStripCheckBox
    Inherits ToolStripControlHost
    Implements IBindableComponent

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> Call the base constructor passing in a CheckBox instance. </remarks>
    Public Sub New()
        MyBase.New(New CheckBox())
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the
    ''' <see cref="T:System.Windows.Forms.ToolStripControlHost" /> and optionally releases the
    ''' managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.RemoveCheckedChangedEventHandler(Me.CheckedChangedEvent)
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try

    End Sub

#End Region

#Region " CHECK BOX "

    ''' <summary> Gets the numeric up down control. </summary>
    ''' <value> The numeric up down control. </value>
    Public ReadOnly Property CheckBoxControl() As CheckBox
        Get
            Return TryCast(Me.Control, CheckBox)
        End Get
    End Property

    ''' <summary> Gets or sets the value. </summary>
    ''' <value> The value. </value>
    Public Property Checked() As Boolean
        Get
            Return Me.CheckBoxControl.Checked
        End Get
        Set(ByVal value As Boolean)
            Me.CheckBoxControl.Checked = value
        End Set
    End Property

    ''' <summary> Subscribes events from the hosted control. </summary>
    ''' <remarks> Subscribe the control events to expose. </remarks>
    ''' <param name="control"> The control from which to subscribe events. </param>
    Protected Overrides Sub OnSubscribeControlEvents(ByVal control As Control)

        If control IsNot Nothing Then

            ' Call the base so the base events are connected.
            MyBase.OnSubscribeControlEvents(control)

            ' Cast the control to a CheckBox control.
            Dim containedControl As CheckBox = TryCast(control, CheckBox)

            If containedControl IsNot Nothing Then
                ' Add the event.
                AddHandler containedControl.CheckedChanged, AddressOf Me.OnCheckedChanged
            End If

        End If
    End Sub

    ''' <summary> Unsubscribe events from the hosted control. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="control"> The control from which to unsubscribe events. </param>
    Protected Overrides Sub OnUnsubscribeControlEvents(ByVal control As Control)

        ' Call the base method so the basic events are unsubscribed.
        MyBase.OnUnsubscribeControlEvents(control)

        ' Cast the control to a CheckBox control.
        Dim containedControl As CheckBox = TryCast(control, CheckBox)

        If containedControl IsNot Nothing Then
            ' Remove the event.
            RemoveHandler containedControl.CheckedChanged, AddressOf Me.OnCheckedChanged
        End If

    End Sub

    ''' <summary> Event queue for all listeners interested in CheckChanged events. </summary>
    Public Event CheckedChanged As EventHandler(Of System.EventArgs)

    ''' <summary> Removes event handler. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The handler. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub RemoveCheckedChangedEventHandler(ByVal value As EventHandler(Of EventArgs))
#Disable Warning CA1825 ' Avoid zero-length array allocations.
        For Each d As [Delegate] In If(value Is Nothing, New [Delegate]() {}, value.GetInvocationList)
#Enable Warning CA1825 ' Avoid zero-length array allocations.
            Try
                RemoveHandler Me.CheckedChanged, CType(d, EventHandler(Of EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

    ''' <summary> Raises the checked changed event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information to send to registered event handlers. </param>
    Private Sub OnCheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim evt As EventHandler(Of System.EventArgs) = Me.CheckedChangedEvent
        evt?.Invoke(Me, e)
    End Sub

#End Region

#Region " BINDABLE "

    ''' <summary> The context. </summary>
    Private _Context As BindingContext = Nothing

    ''' <summary>
    ''' Gets or sets the collection of currency managers for the
    ''' <see cref="T:System.Windows.Forms.IBindableComponent" />.
    ''' </summary>
    ''' <value>
    ''' The collection of <see cref="T:System.Windows.Forms.BindingManagerBase" /> objects for this
    ''' <see cref="T:System.Windows.Forms.IBindableComponent" />.
    ''' </value>
    Public Property BindingContext() As BindingContext Implements IBindableComponent.BindingContext
        Get
            If Nothing Is Me._Context Then Me._Context = New BindingContext()
            Return Me._Context
        End Get
        Set(ByVal value As BindingContext)
            Me._Context = value
        End Set
    End Property

    ''' <summary> The bindings. </summary>
    Private _Bindings As ControlBindingsCollection

    ''' <summary>
    ''' Gets the collection of data-binding objects for this
    ''' <see cref="T:System.Windows.Forms.IBindableComponent" />.
    ''' </summary>
    ''' <value>
    ''' The <see cref="T:System.Windows.Forms.ControlBindingsCollection" /> for this
    ''' <see cref="T:System.Windows.Forms.IBindableComponent" />.
    ''' </value>
    Public ReadOnly Property DataBindings() As ControlBindingsCollection Implements IBindableComponent.DataBindings
        Get
            If Me._Bindings Is Nothing Then Me._Bindings = New ControlBindingsCollection(Me)
            Return Me._Bindings
        End Get
    End Property
#End Region


End Class

