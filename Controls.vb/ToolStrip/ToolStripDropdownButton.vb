Imports System.Windows.Forms.Design

''' <summary> A bindable tool strip drop down button. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/27/2018, </para><para>
''' http://forums.devx.com/showthread.php?153607-Making-ToolStripStatusLabel-data-bindable.
''' </para>
''' </remarks>
<ToolStripItemDesignerAvailability(ToolStripItemDesignerAvailability.ToolStrip)>
Public Class ToolStripDropDownButton
    Inherits Windows.Forms.ToolStripDropDownButton
    Implements IBindableComponent

#Region " BINDABLE "

    ''' <summary> The context. </summary>
    Private _Context As BindingContext = Nothing

    ''' <summary>
    ''' Gets or sets the collection of currency managers for the
    ''' <see cref="T:System.Windows.Forms.IBindableComponent" />.
    ''' </summary>
    ''' <value>
    ''' The collection of <see cref="T:System.Windows.Forms.BindingManagerBase" /> objects for this
    ''' <see cref="T:System.Windows.Forms.IBindableComponent" />.
    ''' </value>
    Public Property BindingContext() As BindingContext Implements IBindableComponent.BindingContext
        Get
            If Nothing Is Me._Context Then Me._Context = New BindingContext()
            Return Me._Context
        End Get
        Set(ByVal value As BindingContext)
            Me._Context = value
        End Set
    End Property

    ''' <summary> The bindings. </summary>
    Private _Bindings As ControlBindingsCollection

    ''' <summary>
    ''' Gets the collection of data-binding objects for this
    ''' <see cref="T:System.Windows.Forms.IBindableComponent" />.
    ''' </summary>
    ''' <value>
    ''' The <see cref="T:System.Windows.Forms.ControlBindingsCollection" /> for this
    ''' <see cref="T:System.Windows.Forms.IBindableComponent" />.
    ''' </value>
    Public ReadOnly Property DataBindings() As ControlBindingsCollection Implements IBindableComponent.DataBindings
        Get
            If Me._Bindings Is Nothing Then Me._Bindings = New ControlBindingsCollection(Me)
            Return Me._Bindings
        End Get
    End Property

#End Region

End Class

