Imports isr.Core.Controls.ExceptionExtensions

''' <summary> A timer with state management. </summary>
''' <remarks>
''' (c) 2008 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 11/08/2013, Added to the Core Controls. </para><para>
''' David, 05/07/2009, 1.1.3414.Sync locks timer and dispose methods. </para><para>
''' David, 01/30/2008, 1.0.2951. </para>
''' </remarks>
Public Class StateAwareTimer
    Inherits System.Timers.Timer

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Constructor specifying a <paramref name="synchronizer">synchronizing object.</paramref>
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="synchronizer"> The form where this class resides. </param>
    Public Sub New(ByVal synchronizer As System.ComponentModel.ISynchronizeInvoke)
        Me.New()
        Me.SynchronizingObject = synchronizer
        ' prefixing Synchronizing object with MyBase causes warning code AD0001
    End Sub

    ''' <summary> Constructs this class. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        MyBase.New()
        Me._TimerState = TimerStates.None
        Me._OnTickLocker = New Object
    End Sub

    ''' <summary>
    ''' Gets the dispose status sentinel of the base class.  This applies to the derived class
    ''' provided proper implementation.
    ''' </summary>
    ''' <value> The is disposed. </value>
    Protected Property IsDisposed() As Boolean

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "CA2215:Dispose methods should call base class dispose",
        Justification:="There seems to be a problem with the base class, which keeps calling this method causing a stack overflow.")>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        SyncLock Me._OnTickLocker
            Try
                If Not Me.IsDisposed AndAlso disposing Then
                    Me.RemoveTickEventHandler(Me.TickEvent)
                    If Me.SynchronizingObject IsNot Nothing Then Me.SynchronizingObject = Nothing
                End If
            Finally
                Me.IsDisposed = True
                MyBase.Dispose(disposing)
            End Try
        End SyncLock

    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary> State of the timer. </summary>
    Private _TimerState As TimerStates

    ''' <summary> Gets the timer state. </summary>
    ''' <value> The timer state. </value>
    Public ReadOnly Property TimerState() As TimerStates
        Get
            Return Me._TimerState
        End Get
    End Property

    ''' <summary> Gets a True if the timer is processing an event. </summary>
    ''' <value> The is in tick. </value>
    Public ReadOnly Property IsInTick() As Boolean
        Get
            Return (Me.TimerState And TimerStates.InTick) <> 0
        End Get
    End Property

    ''' <summary> Gets a True if timer is in Running state. </summary>
    ''' <value> The is running. </value>
    Public ReadOnly Property IsRunning() As Boolean
        Get
            Return (Me.TimerState And TimerStates.Running) <> 0
        End Get
    End Property

    ''' <summary> Gets a True if timer is in Stopping state. </summary>
    ''' <value> The is stopping. </value>
    Public ReadOnly Property IsStopping() As Boolean
        Get
            Return (Me.TimerState And TimerStates.Stopping) <> 0
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets a condition for invoking the call back tick event asynchronously.  Must have a
    ''' <see cref="SynchronizingObject">synchronizing object</see>
    ''' for using asynchronous invocation.
    ''' </summary>
    ''' <value> The asynchronous invoke. </value>
    Public Property AsynchronousInvoke() As Boolean

    ''' <summary> Start or stops event raising. </summary>
    ''' <value> The enabled. </value>
    Public Shadows Property Enabled() As Boolean
        Get
            Return MyBase.Enabled
        End Get
        Set(ByVal value As Boolean)
            If value Then
                Me.Start()
            Else
                Me.Stop()
            End If
        End Set
    End Property

#End Region

#Region " METHODS "

    ''' <summary> Starts this instance. </summary>
    ''' <returns> <c>True</c> if started (enabled), <c>False</c> otherwise. </returns>
    Public Shadows Function [Start]() As Boolean
        SyncLock Me._OnTickLocker
            If Me.IsDisposed Then Return False
            MyBase.Start()
            Me._TimerState = TimerStates.Running
            Return MyBase.Enabled
        End SyncLock
    End Function

    ''' <summary> Stops the timer. </summary>
    ''' <returns> <c>True</c> if stopped (not enabled), <c>False</c> otherwise. </returns>
    Public Shadows Function [Stop]() As Boolean
        SyncLock Me._OnTickLocker
            If Me._IsDisposed Then
                Return True
            End If
            If Me.IsRunning Then
                Me._TimerState = Me._TimerState Or TimerStates.Stopping
            End If
            MyBase.Stop()
            Me._TimerState = TimerStates.None
            Return Not MyBase.Enabled
        End SyncLock
    End Function

#End Region

#Region " TICK EVENT "

    ''' <summary> Handles tick events </summary>
    Public Event Tick As EventHandler(Of System.EventArgs)

    ''' <summary> Removes event handler. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The handler. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub RemoveTickEventHandler(ByVal value As EventHandler(Of EventArgs))
#Disable Warning CA1825 ' Avoid zero-length array allocations.
        For Each d As [Delegate] In If(value Is Nothing, New [Delegate]() {}, value.GetInvocationList)
#Enable Warning CA1825 ' Avoid zero-length array allocations.
            Try
                RemoveHandler Me.Tick, CType(d, EventHandler(Of EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

    ''' <summary> Raises the <see cref="system.EventHandler">tick</see> event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Private Sub OnTickThis(ByVal e As System.EventArgs)
        Dim evt As EventHandler(Of EventArgs) = Me.TickEvent
        evt?.Invoke(Me, e)
    End Sub

    ''' <summary> Raises the Notify event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> Specifies the event <see cref="System.EventArgs">arguments</see>. </param>
    Protected Sub OnTick(ByVal e As System.EventArgs)
        If Me.SynchronizingObject IsNot Nothing Then
            If Me.AsynchronousInvoke Then
                Me.SynchronizingObject.BeginInvoke(New Action(Of EventArgs)(AddressOf Me.OnTickThis), New Object() {e})
            Else
                If Me.SynchronizingObject IsNot Nothing AndAlso Me.SynchronizingObject.InvokeRequired Then
                    Me.SynchronizingObject.Invoke(New Action(Of EventArgs)(AddressOf Me.OnTickThis), New Object() {e})
                Else
                    Me.OnTickThis(e)
                End If
            End If
        End If
    End Sub

    ''' <summary> The on tick locker. </summary>
    Private ReadOnly _OnTickLocker As Object

    ''' <summary> Raises the tick event. </summary>
    ''' <remarks>
    ''' David, 05/07/2009, 1.1.3414.x Add a sync lock to facilitate the orchestrating of close and
    ''' dispose. Remove exception handling - this should be done within the timer event handler in
    ''' the parent instance.
    ''' </remarks>
    Protected Sub OnElapsed() Handles MyBase.Elapsed
        MyBase.Stop()
        SyncLock Me._OnTickLocker
            If Me._IsDisposed Then
                Return
            End If
            Me._TimerState = Me.TimerState Or TimerStates.InTick
            Me.OnTick(System.EventArgs.Empty)
            Me._TimerState = If(Me.IsStopping, TimerStates.None, TimerStates.Running)
        End SyncLock
        If Me.TimerState = TimerStates.Running Then
            MyBase.Start()
        End If
    End Sub

#End Region

End Class

''' <summary> Enumerates the timer states. </summary>
''' <remarks> David, 2020-09-24. </remarks>
<Flags()> Public Enum TimerStates
    None = 0

    ''' <summary> An enum constant representing the running option. </summary>
    Running = 1

    ''' <summary> An enum constant representing the stopping option. </summary>
    Stopping = 2

    ''' <summary> An enum constant representing the in tick option. </summary>
    InTick = 4
End Enum
