Imports isr.Core.Controls.ExceptionExtensions

''' <summary> A console control. </summary>
''' <remarks>
''' This controls displays provides a 80x25 character display and entry console. The console
''' cannot be resized at this time nor can the text scroll. <para>
''' (c) 2012 icemanind. All rights reserved.</para><para>
''' Licensed under The MIT License.</para><para>
''' David, 7/9/2016, http://www.codeproject.com/Articles/1053951/Console-Control.
''' </para>
''' </remarks>
Public Class ConsoleControl
    Inherits UserControl

#Region " CONSTRUCTION "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        MyBase.New()
        Me.RestoreDefaults()
    End Sub

    ''' <summary> Restore defaults. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Sub RestoreDefaults()
        Me._RenderFontName = "Courier New"
        Me._ControlScreenSize = New Size(80, 25)
        Me._CharacterSize = New Size(8, 15)
        Me._ControlSize = New Size(Me.ControlScreenSize.Width * Me.CharacterSize.Width + 6,
                                   Me.ControlScreenSize.Height * Me.CharacterSize.Height + 2)
        Me._ScreenSize = Me.ControlScreenSize.Width * Me.ControlScreenSize.Height
        Me.OnResizeThis()

        Me._CursorX = 0
        Me._CursorY = 0
        Me._IsCursorOn = False
        Me.CursorType = CursorType.Underline

        Me.ConsoleBackgroundColor = Color.Black
        Me.ConsoleForegroundColor = Color.LightGray
        Me.CurrentForegroundColor = Color.LightGray
        Me.CurrentBackgroundColor = Color.Black

        Try
            Me.CursorFlashTimer = New Timer()
        Catch
            Me.CursorFlashTimer?.Dispose()
            Throw
        End Try
        Me.CursorFlashTimer.Enabled = False
        Me.CursorFlashTimer.Interval = 500

        Me._TextBlockArray = New TextBlock(Me.ScreenSize - 1) {} ' 80 x 25

        For i As Integer = 0 To Me.ScreenSize - 1
            Me.TextBlockArray(i).BackgroundColor = Me.ConsoleBackgroundColor
            Me.TextBlockArray(i).ForegroundColor = Me.ConsoleForegroundColor
            Me.TextBlockArray(i).Character = ControlChars.NullChar
        Next i

        Me._KeysBuffer = New List(Of Char)()
        Me._CommandBuffer = New List(Of String)()
        Me._CommandBufferIndex = 0

        Me.AllowInput = True
        Me.EchoInput = True
        Me.ShowCursor = True

    End Sub

    ''' <summary> Gets options for controlling the create. </summary>
    ''' <value>
    ''' A <see cref="T:System.Windows.Forms.CreateParams" />
    '''  that contains the required creation parameters when the handle to the control is created.
    ''' </value>
    Protected Overrides ReadOnly Property CreateParams() As CreateParams
        Get
            Dim cp As CreateParams = MyBase.CreateParams
            cp.ExStyle = cp.ExStyle Or &H2000000
            Return cp
        End Get
    End Property

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="disposing"> <see langword="true" /> to release both managed and unmanaged
    '''                          resources; <see langword="false" /> to release only unmanaged
    '''                          resources. </param>
    Protected Overrides Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed Then
                If disposing Then
                    If Me.CursorFlashTimer IsNot Nothing Then Me.CursorFlashTimer.Dispose() : Me.CursorFlashTimer = Nothing
                    If Me._RenderFont IsNot Nothing Then Me._RenderFont.Dispose()
                    Me.RemoveEventHandler(Me.LineEnteredEvent)
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " EVENTS "

    ''' <summary>
    ''' This event is raised whenever text is entered into the console control
    ''' </summary>
    Public Event LineEntered As EventHandler(Of LineEnteredEventArgs)

    ''' <summary> Removes event handler. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The value. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub RemoveEventHandler(ByVal value As EventHandler(Of LineEnteredEventArgs))
#Disable Warning CA1825 ' Avoid zero-length array allocations.
        For Each d As [Delegate] In If(value Is Nothing, New [Delegate]() {}, value.GetInvocationList)
#Enable Warning CA1825 ' Avoid zero-length array allocations.
            Try
                RemoveHandler Me.LineEntered, CType(d, EventHandler(Of LineEnteredEventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

#End Region

#Region " APPERANCE "

    ''' <summary> Name of the render font. </summary>
    ''' <remarks>
    ''' to_do: allow changing the font and building a new screen and character sizes.
    ''' </remarks>
    ''' <value> The name of the render font. </value>
    Private ReadOnly Property RenderFontName As String = "Courier New"

    ''' <summary> Fixed Control Size. </summary>
    ''' <value> The size of the control. </value>
    Private ReadOnly Property ControlSize As Size

    ''' <summary> Gets or sets the size of the control screen. </summary>
    ''' <value> The size of the control screen. </value>
    Private ReadOnly Property ControlScreenSize As Size

    ''' <summary> Gets or sets the size of the character. </summary>
    ''' <value> The size of the character. </value>
    Private ReadOnly Property CharacterSize As Size

    ''' <summary> Size of the screen. </summary>
    ''' <remarks> to_do: allow changing the screen size. </remarks>
    ''' <value> The size of the screen. </value>
    Private ReadOnly Property ScreenSize As Integer

    ''' <summary> True to show, false to hide the cursor. </summary>
    Private _ShowCursor As Boolean

    ''' <summary>
    ''' Gets or sets or Sets a value indicating whether the console should show the cursor.
    ''' </summary>
    ''' <value> The show cursor. </value>
    Public Property ShowCursor() As Boolean
        Get
            Return Me._ShowCursor
        End Get
        Set(value As Boolean)
            Me._ShowCursor = value
            Me.CursorFlashTimer.Enabled = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets or Sets a value indicating whether the console should allow keyboard input.
    ''' </summary>
    ''' <value> The allow input. </value>
    Public Property AllowInput() As Boolean

    ''' <summary>
    ''' Gets or sets or Sets a value indicating whether the console should echo any input it receives.
    ''' </summary>
    ''' <value> The echo input. </value>
    Public Property EchoInput() As Boolean

    ''' <summary> Type of the cursor. </summary>
    Private _CursorType As CursorType

    ''' <summary> Gets or sets or Sets the current cursor type. </summary>
    ''' <value> The type of the cursor. </value>
    Public Property CursorType() As CursorType
        Get
            Return Me._CursorType
        End Get
        Set(ByVal value As CursorType)
            Me._CursorType = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> Gets or sets the current background color. </summary>
    ''' <value> The color of the current background. </value>
    Public Property CurrentBackgroundColor() As Color

    ''' <summary> The console background color. </summary>
    Private _ConsoleBackgroundColor As Color

    ''' <summary>
    ''' Gets or sets or Sets the background color of the console. Default is Black.
    ''' </summary>
    ''' <value> The color of the console background. </value>
    Public Property ConsoleBackgroundColor() As Color
        Get
            Return Me._ConsoleBackgroundColor
        End Get
        Set(ByVal value As Color)
            Me._ConsoleBackgroundColor = value
            Me.BackColor = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> Gets or sets the current foreground color. </summary>
    ''' <value> The color of the current foreground. </value>
    Public Property CurrentForegroundColor() As Color

    ''' <summary> The console foreground color. </summary>
    Private _ConsoleForegroundColor As Color

    ''' <summary>
    ''' Gets or sets or Sets the foreground color of the console. Default is light gray.
    ''' </summary>
    ''' <value> The color of the console foreground. </value>
    Public Property ConsoleForegroundColor() As Color
        Get
            Return Me._ConsoleForegroundColor
        End Get
        Set(ByVal value As Color)
            Me._ConsoleForegroundColor = value
            Me.ForeColor = value
            Me.Invalidate()
        End Set
    End Property

#End Region

#Region " RENDER "

    ''' <summary> Buffer for keys data. </summary>
    Private _KeysBuffer As List(Of Char)

    ''' <summary> Buffer for command data. </summary>
    Private _CommandBuffer As List(Of String)

    ''' <summary> Zero-based index of the command buffer. </summary>
    Private _CommandBufferIndex As Integer

    ''' <summary> True if is cursor on, false if not. </summary>
    Private _IsCursorOn As Boolean

    ''' <summary> Process the command key. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="msg">     [in,out] A <see cref="T:System.Windows.Forms.Message" />
    '''                        , passed by reference, that represents the window message to process. 
    ''' </param>
    ''' <param name="keyData"> One of the <see cref="T:System.Windows.Forms.Keys" />
    '''                         values that represents the key to process. </param>
    ''' <returns> true if the character was processed by the control; otherwise, false. </returns>
    Protected Overrides Function ProcessCmdKey(ByRef msg As Message, ByVal keyData As Keys) As Boolean
        If keyData = Keys.Up Then
            If Me._CommandBufferIndex <= 0 OrElse Not Me.AllowInput Then
                Return True
            End If

            Dim len As Integer = Me._KeysBuffer.Count
            For i As Integer = 0 To len - 1
                Me.ConsoleControlKeyPress(Me, New KeyPressEventArgs(ChrW(8)))
            Next i
            Me._KeysBuffer.Clear()
            For Each c As Char In Me._CommandBuffer(Me._CommandBufferIndex - 1)
                Me._KeysBuffer.Add(c)
                If Me.EchoInput Then
                    Me.Write(c)
                End If
            Next c
            Me._CommandBufferIndex -= 1
            Return True
        End If
        If keyData = Keys.Down Then
            If (Me._CommandBufferIndex + 1) >= Me._CommandBuffer.Count OrElse Not Me.AllowInput Then
                Return True
            End If

            Dim len As Integer = Me._KeysBuffer.Count
            For i As Integer = 0 To len - 1
                Me.ConsoleControlKeyPress(Me, New KeyPressEventArgs(ChrW(8)))
            Next i
            Me._KeysBuffer.Clear()

            For Each c As Char In Me._CommandBuffer(Me._CommandBufferIndex + 1)
                Me._KeysBuffer.Add(c)
                If Me.EchoInput Then
                    Me.Write(c)
                End If
            Next c
            Me._CommandBufferIndex += 1
            Return True
        End If
        Return MyBase.ProcessCmdKey(msg, keyData)
    End Function

    ''' <summary> The cursor x coordinate. </summary>
    Private _CursorX As Integer

    ''' <summary> The cursor y coordinate. </summary>
    Private _CursorY As Integer

    ''' <summary> Gets an array of text blocks. </summary>
    ''' <value> An array of text blocks. </value>
    Private ReadOnly Property TextBlockArray() As TextBlock()

    ''' <summary> Console control key press. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Key press event information. </param>
    Private Sub ConsoleControlKeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles Me.KeyPress
        If Not Me.AllowInput Then
            Return
        End If

        If AscW(e.KeyChar) = 8 Then
            If Me._KeysBuffer.Count = 0 Then
                Return
            End If
            If Me.EchoInput Then
                Me._TextBlockArray(Me.GetIndex()).Character = ControlChars.NullChar
                Me._CursorX -= 1
                If Me._CursorX < 0 Then
                    Me._CursorY -= 1
                    Me._CursorX = 79
                    If Me._CursorY < 0 Then
                        Me._CursorY += 1
                        Me._CursorX = 0
                    End If
                End If
                Me._TextBlockArray(Me.GetIndex()).Character = ControlChars.NullChar
                Me.Invalidate()
            End If
            Me._KeysBuffer.RemoveAt(Me._KeysBuffer.Count - 1)
            Return
        End If
        Me._KeysBuffer.Add(e.KeyChar)
        If Me.EchoInput Then
            Me.Write(e.KeyChar)
            If e.KeyChar = ControlChars.Cr Then
                Me.Write(ControlChars.Lf)
            End If
        End If

        If e.KeyChar = ControlChars.Cr Then
            If Environment.NewLine.Length = 2 Then
                Me._KeysBuffer.Add(ControlChars.Lf)
            End If
            Dim s As String = Me._KeysBuffer.Aggregate("", Function(current, c) current + c)
            Me._KeysBuffer.Clear()

            Me._CommandBuffer.Add(s.Trim(ControlChars.Cr, ControlChars.Lf))
            Me._CommandBufferIndex = Me._CommandBuffer.Count
            Dim evt As EventHandler(Of LineEnteredEventArgs) = Me.LineEnteredEvent
            If evt IsNot Nothing Then evt.Invoke(Me, New LineEnteredEventArgs(s))
        End If
        Me.Invalidate()
    End Sub

        Private WithEvents CursorFlashTimer As Timer

    ''' <summary> Cursor flash timer tick. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub CursorFlashTimerTick(sender As Object, e As EventArgs) Handles CursorFlashTimer.Tick
        If Not Me.ShowCursor Then Return
        Me._IsCursorOn = Not Me._IsCursorOn
        Dim c As Char
        Select Case Me.CursorType
            Case CursorType.Block
                c = ChrW(&H2588)
            Case CursorType.Invisible
                c = " "c
            Case Else
                c = "_"c
        End Select
        Me._TextBlockArray(Me.GetIndex()).Character = If(Me._IsCursorOn, c, ControlChars.NullChar)
        Me.Invalidate()
    End Sub

    ''' <summary> The render font. </summary>
    Private ReadOnly _RenderFont As New Font(Me.RenderFontName, 10, FontStyle.Regular)

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.Control.Paint" />
    '''  event.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.PaintEventArgs" />
    '''                   that contains the event data. </param>
    Protected Overrides Sub OnPaint(ByVal e As PaintEventArgs)
        If e Is Nothing Then Return
        Dim x As Integer = 0
        Dim y As Integer = 0
        Dim charWidth As Integer = Me.CharacterSize.Width
        Dim charHeight As Integer = Me.CharacterSize.Height

        Using bitmap As Bitmap = New Bitmap(Me.Width, Me.Height)
            Using g As Graphics = Graphics.FromImage(bitmap)
                For i As Integer = 0 To Me.ScreenSize - 1
                    Dim fc As Color = If(Me.TextBlockArray(i).Character = ControlChars.NullChar,
                                            Me.ConsoleForegroundColor, Me.TextBlockArray(i).ForegroundColor)
                    Dim bc As Color = If(Me.TextBlockArray(i).Character = ControlChars.NullChar,
                                            Me.ConsoleBackgroundColor, Me.TextBlockArray(i).BackgroundColor)

                    Using bgBrush As Brush = New SolidBrush(bc)
                        g.FillRectangle(bgBrush, New Rectangle(x + 2, y + 1, charWidth, charHeight))
                        Using fgBrush As Brush = New SolidBrush(fc)
                            g.DrawString(If(Me.TextBlockArray(i).Character = ControlChars.NullChar, " ", Me.TextBlockArray(i).Character.ToString()),
                                            Me._RenderFont, fgBrush, New Drawing.PointF(x, y))
                        End Using
                    End Using
                    x += charWidth
                    If x > 79 * charWidth Then
                        y += charHeight
                        x = 0
                    End If
                Next i
                e.Graphics.DrawImage(bitmap, e.ClipRectangle, e.ClipRectangle, GraphicsUnit.Pixel)
            End Using
        End Using
        MyBase.OnPaint(e)
    End Sub

    ''' <summary> Executes the 'resize this' action. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Sub OnResizeThis()
        Me.Width = Me.ControlSize.Width
        Me.Height = Me.ControlSize.Height
    End Sub

    ''' <summary> Raises the resize event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" />
    '''                   that contains the event data. </param>
    Protected Overrides Sub OnResize(ByVal e As EventArgs)
        MyBase.OnResize(e)
        Me.OnResizeThis()
    End Sub

#End Region

#Region " WRITING "

    ''' <summary> Sets the position of the cursor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="row">    The row of the cursor position. </param>
    ''' <param name="column"> The column of the cursor position. </param>
    Public Sub SetCursorPosition(ByVal row As Integer, ByVal column As Integer)
        If Me.ShowCursor Then
            Me.TextBlockArray(Me.GetIndex()).Character = ControlChars.NullChar
        End If
        Me._CursorX = column
        Me._CursorY = row

        Me.Invalidate()
    End Sub

    ''' <summary> Sets the position of the cursor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="location"> The location of the cursor position. </param>
    Public Sub SetCursorPosition(ByVal location As Location)
        If location IsNot Nothing Then Me.SetCursorPosition(location.Row, location.Column)
    End Sub

    ''' <summary> Gets the position of the cursor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> The location of the cursor. </returns>
    Public Function GetCursorPosition() As Location
        Return New Location With {.Column = Me._CursorX, .Row = Me._CursorY}
    End Function

    ''' <summary> Writes a newline (carriage return) to the console. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub Write()
        Me.Write(Environment.NewLine)
    End Sub

    ''' <summary>
    ''' Writes a character to the console using the current foreground color and background color.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The character to write to the console. </param>
    Public Sub Write(ByVal value As Char)
        Me.Write(value, Me.CurrentForegroundColor, Me.CurrentBackgroundColor)
    End Sub

    ''' <summary>
    ''' Writes a character to the console using the specified foreground color and background color.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value">     The character to write to the console. </param>
    ''' <param name="foreColor"> The foreground color. </param>
    ''' <param name="backColor"> The background color. </param>
    Public Sub Write(ByVal value As Char, ByVal foreColor As Color, ByVal backColor As Color)
        If AscW(value) = 7 Then
            Console.Beep(1000, 500)
            Return
        End If

        If AscW(value) = 13 Then
            Me.SetCursorPosition(Me.GetCursorPosition().Row, 0)
            Return
        End If
        If AscW(value) = 10 Then
            If Environment.NewLine.Length = 1 Then
                Me.SetCursorPosition(Me.GetCursorPosition().Row, 0)
            End If
            Me._CursorY += 1
            If Me._CursorY > 24 Then
                Me.ScrollUp()
                Me._CursorY = 24
            End If
            Return
        End If
        Me.TextBlockArray(Me.GetIndex()).Character = value
        Me.TextBlockArray(Me.GetIndex()).BackgroundColor = backColor
        Me.TextBlockArray(Me.GetIndex()).ForegroundColor = foreColor
        Me.MoveCursorPosition()
        Me.Invalidate()
    End Sub

    ''' <summary>
    ''' Writes a string to the console using the current foreground color and background color.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="text"> The string to write to the console. </param>
    Public Sub Write(ByVal text As String)
        Me.Write(text, Me.CurrentForegroundColor, Me.CurrentBackgroundColor)
    End Sub

    ''' <summary>
    ''' Writes a string to the console using the specified foreground color and background color.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="text">      The string to write to the console. </param>
    ''' <param name="foreColor"> The foreground color. </param>
    ''' <param name="backColor"> The background color. </param>
    Public Sub Write(ByVal text As String, ByVal foreColor As Color, ByVal backColor As Color)
        If String.IsNullOrWhiteSpace(text) Then Return
        For Each c As Char In text
            Me.Write(c, foreColor, backColor)
        Next c
        Me.Invalidate()
    End Sub

    ''' <summary> Move cursor position. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Sub MoveCursorPosition()
        Me._CursorX += 1
        If Me._CursorX > 79 Then
            Me._CursorX = 0
            Me._CursorY += 1
        End If
        If Me._CursorY > 24 Then
            Me.ScrollUp()
            Me._CursorY = 24
        End If
    End Sub

    ''' <summary> Gets an index. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="row"> The row of the cursor position. </param>
    ''' <param name="col"> The col. </param>
    ''' <returns> The index. </returns>
    Private Shared Function GetIndex(ByVal row As Integer, ByVal col As Integer) As Integer
        Return 80 * row + col
    End Function

    ''' <summary> Gets an index. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> The index. </returns>
    Private Function GetIndex() As Integer
        Return ConsoleControl.GetIndex(Me._CursorY, Me._CursorX)
    End Function

    ''' <summary> Scrolls the console screen window up the given. number of lines. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="lines"> The number of lines to scroll up. </param>
    Public Sub ScrollUp(ByVal lines As Integer)
        Do While lines > 0
            For i As Integer = 0 To Me.ScreenSize - 80 - 1
                Me._TextBlockArray(i) = Me._TextBlockArray(i + 80)
            Next i
            For i As Integer = Me.ScreenSize - 80 To Me.ScreenSize - 1
                Me._TextBlockArray(i).Character = ControlChars.NullChar
                Me._TextBlockArray(i).BackgroundColor = Me.ConsoleBackgroundColor
                Me._TextBlockArray(i).ForegroundColor = Me.ConsoleForegroundColor
            Next i
            lines -= 1
        Loop
        Me.Invalidate()
    End Sub

    ''' <summary> Scrolls the console screen window up one line. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub ScrollUp()
        Me.ScrollUp(1)
    End Sub

    ''' <summary> Clears the console screen. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub Clear()
        For i As Integer = 0 To Me.ScreenSize - 1
            Me._TextBlockArray(i).BackgroundColor = Me.ConsoleBackgroundColor
            Me._TextBlockArray(i).ForegroundColor = Me.ConsoleForegroundColor
            Me._TextBlockArray(i).Character = ControlChars.NullChar
        Next i
        Me._CursorX = 0
        Me._CursorY = 0
        Me.Invalidate()
    End Sub

    ''' <summary> Sets the background color at the specified location. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="color">  The background color to set. </param>
    ''' <param name="row">    The row at which to set the background color. </param>
    ''' <param name="column"> The column at which to set the background color. </param>
    Public Sub SetBackgroundColorAt(ByVal color As Color, ByVal row As Integer, ByVal column As Integer)
        Me._TextBlockArray(GetIndex(row, column)).BackgroundColor = color
        Me.Invalidate()
    End Sub

    ''' <summary> Sets the foreground color at the specified location. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="color">  The foreground color to set. </param>
    ''' <param name="row">    The row at which to set the foreground color. </param>
    ''' <param name="column"> The column at which to set the foreground color. </param>
    Public Sub SetForegroundColorAt(ByVal color As Color, ByVal row As Integer, ByVal column As Integer)
        Me._TextBlockArray(GetIndex(row, column)).ForegroundColor = color
        Me.Invalidate()
    End Sub

    ''' <summary> Sets the character at the specified location. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="character"> The character to set. </param>
    ''' <param name="row">       The row at which to place the character. </param>
    ''' <param name="column">    The column at which to place the character. </param>
    Public Sub SetCharacterAt(ByVal character As Char, ByVal row As Integer, ByVal column As Integer)
        Me._TextBlockArray(GetIndex(row, column)).Character = character
        Me.Invalidate()
    End Sub

#End Region

End Class

''' <summary> Values that represent cursor types. </summary>
''' <remarks> David, 2020-09-24. </remarks>
Public Enum CursorType

    ''' <summary> An enum constant representing the invisible option. </summary>
    Invisible = 0

    ''' <summary> An enum constant representing the block option. </summary>
    Block = 1

    ''' <summary> An enum constant representing the underline option. </summary>
    Underline = 2
End Enum

''' <summary> A location. </summary>
''' <remarks>
''' (c) 2012 icemanind. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 7/9/2016 </para>
''' </remarks>
Public Class Location

    ''' <summary> Gets the row. </summary>
    ''' <value> The row. </value>
    Public Property Row() As Integer

    ''' <summary> Gets the column. </summary>
    ''' <value> The column. </value>
    Public Property Column() As Integer
End Class

''' <summary> A text block. </summary>
''' <remarks>
''' (c) 2012 icemanind. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Friend Structure TextBlock

    ''' <summary> Gets the color of the background. </summary>
    ''' <value> The color of the background. </value>
    Public Property BackgroundColor() As Color

    ''' <summary> Gets the color of the foreground. </summary>
    ''' <value> The color of the foreground. </value>
    Public Property ForegroundColor() As Color

    ''' <summary> Gets the character. </summary>
    ''' <value> The character. </value>
    Public Property Character() As Char
End Structure

''' <summary> Additional information for line entered events. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 7/9/2016 </para>
''' </remarks>
Public Class LineEnteredEventArgs
    Inherits System.EventArgs

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="line"> The line. </param>
    Public Sub New(ByVal line As String)
        MyBase.New
        Me._Line = line
    End Sub

    ''' <summary> Gets the empty. </summary>
    ''' <value> The empty. </value>
    Public Shared Shadows ReadOnly Property Empty As LineEnteredEventArgs
        Get
            Return New LineEnteredEventArgs("")
        End Get
    End Property

    ''' <summary> Gets or sets the line. </summary>
    ''' <value> The line. </value>
    Public ReadOnly Property Line As String

End Class
