﻿''' <summary> Metro progress bar. </summary>
''' <remarks>
''' (c) 2014 Magyar Andras, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/08/2012 from Magyar András </para><para>
''' http://www.codeproject.com/Articles/804316/Office-Style-Splash-Screen. </para>
''' </remarks>
Partial Public Class MetroProgressBar
    Inherits UserControl

    ''' <summary> Default constructor. </summary>

    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        Me.InitializeComponent()
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the isr.Core.WindowsForms.MetroProgressBar and
    ''' optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

End Class
