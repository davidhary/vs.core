Imports System.Windows.Forms.VisualStyles
''' <summary> A themed colors. </summary>
''' <remarks> (c) 2010 The Man from U.N.C.L.E. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 09/26/2015,Created.
''' http://www.codeproject.com/Articles/91387/Painting-Your-Own-Tabs-Second-Edition </para></remarks>
Friend NotInheritable Class ThemedColors

#Region "    Variables and Constants "

    ''' <summary> The normal color. </summary>
    Private Const _NormalColor As String = "NormalColor"

    ''' <summary> The home stead. </summary>
    Private Const _HomeStead As String = "HomeStead"

    ''' <summary> The metallic. </summary>
    Private Const _Metallic As String = "Metallic"
    'Private Constant NoTheme As String = "NoTheme"

#End Region

#Region "    Properties "

    ''' <summary> Gets the current theme index. </summary>
    ''' <value> The current theme index. </value>
    <System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
    Public Shared ReadOnly Property CurrentThemeIndex() As ColorScheme
        Get
            Return ThemedColors.GetCurrentThemeIndex()
        End Get
    End Property

#Disable Warning IDE1006 ' Naming Styles
    ''' <summary> The tool border. </summary>
    Private Shared ReadOnly _ToolBorder As Color() = New Color() {Color.FromArgb(127, 157, 185), Color.FromArgb(164, 185, 127), Color.FromArgb(165, 172, 178), Color.FromArgb(132, 130, 132)}
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets the tool border. </summary>
    ''' <value> The tool border. </value>
    <System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
    Public Shared ReadOnly Property ToolBorder() As Color
        Get
            Return ThemedColors._ToolBorder(CInt(ThemedColors.CurrentThemeIndex))
        End Get
    End Property

#End Region

#Region "    Constructors "

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Sub New()
    End Sub

#End Region

    ''' <summary> Gets the zero-based index of the current theme. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> The current theme index. </returns>
    Private Shared Function GetCurrentThemeIndex() As ColorScheme
        Dim theme As ColorScheme = ColorScheme.NoTheme

        If VisualStyleInformation.IsSupportedByOS AndAlso VisualStyleInformation.IsEnabledByUser AndAlso Application.RenderWithVisualStyles Then


            Select Case VisualStyleInformation.ColorScheme
                Case _NormalColor
                    theme = ColorScheme.NormalColor
                    Exit Select
                Case _HomeStead
                    theme = ColorScheme.HomeStead
                    Exit Select
                Case _Metallic
                    theme = ColorScheme.Metallic
                    Exit Select
                Case Else
                    theme = ColorScheme.NoTheme
                    Exit Select
            End Select
        End If

        Return theme
    End Function

    ''' <summary> Values that represent color schemes. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Enum ColorScheme
        ''' <summary> An enum constant representing the normal color option. </summary>
        NormalColor = 0
        ''' <summary> An enum constant representing the home stead option. </summary>
        HomeStead = 1
        ''' <summary> An enum constant representing the metallic option. </summary>
        Metallic = 2
        ''' <summary> An enum constant representing the no theme option. </summary>
        NoTheme = 3
    End Enum

End Class

