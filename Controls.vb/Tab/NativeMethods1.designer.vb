Imports System.Runtime.InteropServices
Imports System.Security

Partial Friend Class NativeMethods

#Region " WINDOWS STRUCTURES AND ENUMS "

    ''' <summary> A bit-field of flags for specifying tchittestflags. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    <Flags>
    Public Enum TCHITTESTFLAGS
        ''' <summary> An enum constant representing the tcht nowhere option. </summary>
        TCHT_NOWHERE = 1
        ''' <summary> An enum constant representing the tcht onitemicon option. </summary>
        TCHT_ONITEMICON = 2
        ''' <summary> An enum constant representing the tcht onitemlabel option. </summary>
        TCHT_ONITEMLABEL = 4
        ''' <summary> An enum constant representing the tcht onitem option. </summary>
        TCHT_ONITEM = TCHT_ONITEMICON Or TCHT_ONITEMLABEL
    End Enum

    ''' <summary> A TCHITTESTINFO. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    <StructLayout(LayoutKind.Sequential)>
    Public Structure TCHITTESTINFO

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 2020-09-24. </remarks>
        ''' <param name="location"> The location. </param>
        Public Sub New(location As Point)
            pt = location
            flags = TCHITTESTFLAGS.TCHT_ONITEM
        End Sub
        ''' <summary> The point. </summary>
        Public pt As Point
        ''' <summary> The flags. </summary>
        Public flags As TCHITTESTFLAGS
    End Structure

    ''' <summary> A paint structure. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    <StructLayout(LayoutKind.Sequential, Pack:=4)>
    Public Structure PAINTSTRUCT

        ''' <summary> A handle to the display DC to use for painting. </summary>
        Public hdc As IntPtr
        ''' <summary> Indicates whether the background should be erased. </summary>
        Public fErase As Integer

        ''' <summary> A RECT structure that specifies the upper left and lower right 
        '''           corners of the rectangle in which the painting is requested, </summary>
        Public rcPaint As RECT

        ''' <summary> Reserved; used internally by the system.  The restore. </summary>
        '''
        Public fRestore As Integer

        ''' <summary> Reserved; used internally by the system.  The restore. </summary>
        '''
        Public fIncUpdate As Integer

        ''' <summary> The RGB reserved. </summary>
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=32)>
        Public rgbReserved As Byte()
    End Structure

    ''' <summary> A rectangle. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    <StructLayout(LayoutKind.Sequential)>
    Public Structure RECT

        ''' <summary> The x-coordinate of the upper-left corner of the rectangle.  The left. </summary>
        '''
        Public left As Integer

        ''' <summary> The y-coordinate of the upper-left corner of the rectangle.  The left. </summary>
        '''
        Public top As Integer

        ''' <summary> The x-coordinate of the lower-right corner of the rectangle.  The left. </summary>
        '''
        Public right As Integer

        ''' <summary> The y-coordinate of the lower-right corner of the rectangle.  The left. </summary>
        '''
        Public bottom As Integer

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 2020-09-24. </remarks>
        ''' <param name="left">   The left. </param>
        ''' <param name="top">    The top. </param>
        ''' <param name="right">  The right. </param>
        ''' <param name="bottom"> The bottom. </param>
        Public Sub New(left As Integer, top As Integer, right As Integer, bottom As Integer)
            Me.left = left
            Me.top = top
            Me.right = right
            Me.bottom = bottom
        End Sub

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 2020-09-24. </remarks>
        ''' <param name="r"> A Rectangle to process. </param>
        Public Sub New(r As Rectangle)
            Me.left = r.Left
            Me.top = r.Top
            Me.right = r.Right
            Me.bottom = r.Bottom
        End Sub

        ''' <summary> From X, Y, W, H. </summary>
        ''' <remarks> David, 2020-09-24. </remarks>
        ''' <param name="x">      The x coordinate. </param>
        ''' <param name="y">      The y coordinate. </param>
        ''' <param name="width">  The width. </param>
        ''' <param name="height"> The height. </param>
        ''' <returns> A RECT. </returns>
        Public Shared Function FromXYWH(x As Integer, y As Integer, width As Integer, height As Integer) As RECT
            Return New RECT(x, y, x + width, y + height)
        End Function

        ''' <summary> Initializes this object from the given int pointer. </summary>
        ''' <remarks> David, 2020-09-24. </remarks>
        ''' <param name="ptr"> The pointer. </param>
        ''' <returns> A RECT. </returns>
        Public Shared Function FromIntPtr(ptr As IntPtr) As RECT
            Dim rect As RECT = CType(Marshal.PtrToStructure(ptr, GetType(RECT)), RECT)
            Return rect
        End Function

        ''' <summary> Gets the size. </summary>
        ''' <value> The size. </value>
        Public ReadOnly Property Size() As Size
            Get
                Return New Size(Me.right - Me.left, Me.bottom - Me.top)
            End Get
        End Property
    End Structure

#End Region

#Region " WINDOWS CONSTANTS "

    ''' <summary> The windows message gettabrect. </summary>
    Public Const WM_GETTABRECT As Integer = &H130A

    ''' <summary> The ws exception transparent. </summary>
    Public Const WS_EX_TRANSPARENT As Integer = &H20

    ''' <summary> The windows message setfont. </summary>
    Public Const WM_SETFONT As Integer = &H30

    ''' <summary> The windows message fontchange. </summary>
    Public Const WM_FONTCHANGE As Integer = &H1D

    ''' <summary> The windows message hscroll. </summary>
    Public Const WM_HSCROLL As Integer = &H114

    ''' <summary> The tcm hittest. </summary>
    Public Const TCM_HITTEST As Integer = &H130D

    ''' <summary> The windows message paint. Sent when the system makes a request to paint (a portion of) a window
    ''' </summary>
    Public Const WM_PAINT As Integer = &HF

    ''' <summary> The ws exception layoutrtl. </summary>
    Public Const WS_EX_LAYOUTRTL As Integer = &H400000

    ''' <summary> The ws exception noinheritlayout. </summary>
    Public Const WS_EX_NOINHERITLAYOUT As Integer = &H100000

#End Region

#Region " CONTENT ALIGNMENT "

    ''' <summary> any right align. </summary>
    Public Const AnyRightAlign As ContentAlignment = ContentAlignment.BottomRight Or ContentAlignment.MiddleRight Or ContentAlignment.TopRight

    ''' <summary> any left align. </summary>
    Public Const AnyLeftAlign As ContentAlignment = ContentAlignment.BottomLeft Or ContentAlignment.MiddleLeft Or ContentAlignment.TopLeft

    ''' <summary> any top align. </summary>
    Public Const AnyTopAlign As ContentAlignment = ContentAlignment.TopRight Or ContentAlignment.TopCenter Or ContentAlignment.TopLeft

    ''' <summary> any bottom align. </summary>
    Public Const AnyBottomAlign As ContentAlignment = ContentAlignment.BottomRight Or ContentAlignment.BottomCenter Or ContentAlignment.BottomLeft

    ''' <summary> any middle align. </summary>
    Public Const AnyMiddleAlign As ContentAlignment = ContentAlignment.MiddleRight Or ContentAlignment.MiddleCenter Or ContentAlignment.MiddleLeft

    ''' <summary> any center align. </summary>
    Public Const AnyCenterAlign As ContentAlignment = ContentAlignment.BottomCenter Or ContentAlignment.MiddleCenter Or ContentAlignment.TopCenter

#End Region

End Class
