Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.Security.Permissions


<SecurityPermission(SecurityAction.Assert, Flags:=SecurityPermissionFlag.UnmanagedCode)>
Partial Friend NotInheritable Class NativeMethods

#Region " SEND MESSAGE "

    ''' <summary> Sends a message. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="hWnd">   The window. </param>
    ''' <param name="msg">    The message. </param>
    ''' <param name="wParam"> The parameter. </param>
    ''' <param name="lParam"> The parameter. </param>
    ''' <returns> An IntPtr. </returns>
    Public Shared Function SendMessage(hWnd As IntPtr, msg As Integer, wParam As IntPtr, lParam As IntPtr) As IntPtr
        '	This Method replaces the User32 method SendMessage, but will only work for sending
        '	messages to Managed controls.
        Dim control__1 As Control = Control.FromHandle(hWnd)
        If control__1 Is Nothing Then
            Return IntPtr.Zero
        End If
        Dim message As New Message() With {.HWnd = hWnd, .LParam = lParam, .WParam = wParam, .Msg = msg}
        Dim wproc As MethodInfo = control__1.[GetType]().GetMethod("WndProc", BindingFlags.NonPublic Or BindingFlags.InvokeMethod Or
                                                                   BindingFlags.FlattenHierarchy Or BindingFlags.IgnoreCase Or BindingFlags.Instance)
        Dim args As Object() = New Object() {message}
        wproc.Invoke(control__1, args)
        Return CType(args(0), Message).Result
    End Function

#End Region

#Region " MISC FUNCTIONS "

    ''' <summary> Lower word. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="dWord"> The word. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function LoWord(dWord As IntPtr) As Integer
        Return dWord.ToInt32() And &HFFFF
    End Function

    ''' <summary> Higher word. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="dWord"> The word. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function HiWord(dWord As IntPtr) As Integer
        Return If((dWord.ToInt32() And &H80000000UI) = &H80000000UI, dWord.ToInt32() >> 16, (dWord.ToInt32() >> 16) And &HFFFF)
    End Function

    ''' <summary> Converts a Structure Object to an int pointer. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="structureObject"> The structure object. </param>
    ''' <returns> Structure Object as an IntPtr. </returns>
    Public Shared Function ToIntPtr(ByVal structureObject As Object) As IntPtr
        Dim lparam As IntPtr = Marshal.AllocCoTaskMem(Marshal.SizeOf(structureObject))
        Marshal.StructureToPtr(structureObject, lparam, False)
        Return lparam
    End Function

#End Region

End Class

