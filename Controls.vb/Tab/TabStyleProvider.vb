Imports System.ComponentModel
Imports System.Drawing.Drawing2D

''' <summary> A tab style provider. </summary>
''' <remarks>
''' (c) 2010 The Man from U.N.C.L.E. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 09/26/2015, Created.</para><para>
''' http://www.codeproject.com/Articles/91387/Painting-Your-Own-Tabs-Second-Edition </para>
''' </remarks>
<System.ComponentModel.ToolboxItem(False)>
Public MustInherit Class TabStyleProvider
    Inherits Component

#Region "Constructor"

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="tabControl">          The tab control. </param>
    ''' <param name="imageAlignment">      The image alignment. </param>
    ''' <param name="borderColor">         The border color. </param>
    ''' <param name="borderColorSelected"> The border color selected. </param>
    ''' <param name="borderColorHot">      The border color hot. </param>
    ''' <param name="focusColor">          The focus color. </param>
    ''' <param name="textColor">           The text color. </param>
    ''' <param name="textColorDisabled">   The text color disabled. </param>
    ''' <param name="hotTrack">            True to hot track. </param>
    ''' <param name="padding">             The padding. </param>
    ''' <param name="radius">              The radius. </param>
    ''' <param name="showTabCloser">       True to show, false to hide the tab closer. </param>
    ''' <param name="closerColorActive">   The closer color active. </param>
    ''' <param name="closerColor">         The closer color. </param>
    Protected Sub New(tabControl As CustomTabControl, ByVal imageAlignment As ContentAlignment,
                      ByVal borderColor As Color, ByVal borderColorSelected As Color, ByVal borderColorHot As Color,
                      ByVal focusColor As Color, ByVal textColor As Color, ByVal textColorDisabled As Color,
                      ByVal hotTrack As Boolean, ByVal padding As Point, ByVal radius As Integer,
                      ByVal showTabCloser As Boolean, ByVal closerColorActive As Color, ByVal closerColor As Color)
        MyBase.New()
        Me._TabControl = tabControl
        Me._BorderColor = borderColor
        Me._BorderColorSelected = borderColorSelected
        Me._BorderColorHot = borderColorHot

        Me._FocusColor = focusColor
        Me._ImageAlign = imageAlignment
        Me._HotTrack = hotTrack

        Me._Radius = radius
        Me._ShowTabCloser = showTabCloser
        Me._CloserColorActive = closerColorActive
        Me._CloserColor = closerColor
        Me._TextColor = textColor
        Me._TextColorDisabled = textColorDisabled
        Me._Opacity = 1

        Me._Padding = padding
        Me.AdjustPadding(padding)
    End Sub

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="tabControl">          The tab control. </param>
    ''' <param name="borderColor">         The border color. </param>
    ''' <param name="borderColorSelected"> The border color selected. </param>
    ''' <param name="borderColorHot">      The border color hot. </param>
    ''' <param name="focusColor">          The focus color. </param>
    ''' <param name="textColor">           The text color. </param>
    ''' <param name="textColorDisabled">   The text color disabled. </param>
    ''' <param name="hotTrack">            True to hot track. </param>
    ''' <param name="padding">             The padding. </param>
    ''' <param name="radius">              The radius. </param>
    ''' <param name="showTabCloser">       True to show, false to hide the tab closer. </param>
    ''' <param name="closerColorActive">   The closer color active. </param>
    ''' <param name="closerColor">         The closer color. </param>
    Protected Sub New(tabControl As CustomTabControl,
                      ByVal borderColor As Color, ByVal borderColorSelected As Color, ByVal borderColorHot As Color,
                      ByVal focusColor As Color, ByVal textColor As Color, ByVal textColorDisabled As Color,
                      ByVal hotTrack As Boolean, ByVal padding As Point, ByVal radius As Integer,
                      ByVal showTabCloser As Boolean, ByVal closerColorActive As Color, ByVal closerColor As Color)
        Me.New(tabControl, If(tabControl?.RightToLeftLayout, ContentAlignment.MiddleRight, ContentAlignment.MiddleLeft),
                   borderColor, borderColorSelected, borderColorHot,
                   focusColor, textColor, textColorDisabled,
                   hotTrack, padding, radius,
               showTabCloser, closerColorActive, closerColor)
    End Sub

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="tabControl">        The tab control. </param>
    ''' <param name="borderColor">       The border color. </param>
    ''' <param name="borderColorHot">    The border color hot. </param>
    ''' <param name="textColor">         The text color. </param>
    ''' <param name="textColorDisabled"> The text color disabled. </param>
    ''' <param name="padding">           The padding. </param>
    ''' <param name="radius">            The radius. </param>
    ''' <param name="showTabCloser">     True to show, false to hide the tab closer. </param>
    ''' <param name="closerColorActive"> The closer color active. </param>
    ''' <param name="closerColor">       The closer color. </param>
    Protected Sub New(tabControl As CustomTabControl,
                      ByVal borderColor As Color, ByVal borderColorHot As Color,
                      ByVal textColor As Color, ByVal textColorDisabled As Color,
                      ByVal padding As Point, ByVal radius As Integer,
                      ByVal showTabCloser As Boolean, ByVal closerColorActive As Color, ByVal closerColor As Color)
        Me.New(tabControl, If(tabControl?.RightToLeftLayout, ContentAlignment.MiddleRight, ContentAlignment.MiddleLeft),
                   borderColor, Color.Empty, borderColorHot,
                   Color.Orange, textColor, textColorDisabled,
                   True, padding, radius,
               showTabCloser, closerColorActive, closerColor)
    End Sub

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="tabControl">     The tab control. </param>
    ''' <param name="imageAlignment"> The image alignment. </param>
    Protected Sub New(tabControl As CustomTabControl, ByVal imageAlignment As ContentAlignment)
        MyBase.New()
        Me._TabControl = tabControl

        Me._BorderColor = Color.Empty
        Me._BorderColorSelected = Color.Empty
        Me._BorderColorHot = Color.Empty

        Me._FocusColor = Color.Orange
        Me._ImageAlign = imageAlignment
        Me._HotTrack = True

        Me._Radius = 1
        Me._CloserColorActive = Color.Black
        Me._CloserColor = Color.DarkGray

        Me._TextColor = Color.Empty
        Me._TextColorDisabled = Color.Empty
        Me._Opacity = 1

        Me._Padding = New Point(6, 3)
        Me.AdjustPadding(Me.Padding)
    End Sub

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="tabControl"> The tab control. </param>
    Protected Sub New(tabControl As CustomTabControl)
        Me.New(tabControl, If(tabControl?.RightToLeftLayout, ContentAlignment.MiddleRight, ContentAlignment.MiddleLeft))
    End Sub

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="tabControl"> The tab control. </param>
    ''' <param name="radius">     The radius. </param>
    ''' <param name="focusTrack"> True to focus track. </param>
    Protected Sub New(tabControl As CustomTabControl, ByVal radius As Integer, ByVal focusTrack As Boolean)
        Me.New(tabControl, If(tabControl?.RightToLeftLayout, ContentAlignment.MiddleRight, ContentAlignment.MiddleLeft))
        Me._Radius = radius
        Me._FocusTrack = focusTrack
        Me._Padding = New Point(6, 3)
        Me.AdjustPadding(Me.Padding)
    End Sub

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="tabControl"> The tab control. </param>
    ''' <param name="radius">     The radius. </param>
    Protected Sub New(tabControl As CustomTabControl, ByVal radius As Integer)
        Me.New(tabControl, If(tabControl?.RightToLeftLayout, ContentAlignment.MiddleRight, ContentAlignment.MiddleLeft))
        Me._Radius = radius
        Me._Padding = New Point(6, 3)
        Me.AdjustPadding(Me.Padding)
    End Sub

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="tabControl">        The tab control. </param>
    ''' <param name="radius">            The radius. </param>
    ''' <param name="showTabCloser">     True to show, false to hide the tab closer. </param>
    ''' <param name="closerColorActive"> The closer color active. </param>
    ''' <param name="padding">           The padding. </param>
    Protected Sub New(tabControl As CustomTabControl, ByVal radius As Integer,
                      ByVal showTabCloser As Boolean, ByVal closerColorActive As Color,
                      ByVal padding As Point)
        Me.New(tabControl, If(tabControl?.RightToLeftLayout, ContentAlignment.MiddleRight, ContentAlignment.MiddleLeft))
        Me._ShowTabCloser = showTabCloser
        Me._CloserColorActive = closerColorActive
        Me._Radius = radius
        Me._Padding = padding
        Me.AdjustPadding(padding)
    End Sub

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="tabControl">        The tab control. </param>
    ''' <param name="radius">            The radius. </param>
    ''' <param name="overlap">           The overlap. </param>
    ''' <param name="showTabCloser">     True to show, false to hide the tab closer. </param>
    ''' <param name="closerColorActive"> The closer color active. </param>
    ''' <param name="padding">           The padding. </param>
    Protected Sub New(tabControl As CustomTabControl,
                      ByVal radius As Integer, ByVal overlap As Integer,
                      ByVal showTabCloser As Boolean, ByVal closerColorActive As Color,
                      ByVal padding As Point)
        Me.New(tabControl, If(tabControl?.RightToLeftLayout, ContentAlignment.MiddleRight, ContentAlignment.MiddleLeft))
        Me._ShowTabCloser = showTabCloser
        Me._CloserColorActive = closerColorActive
        Me._Overlap = overlap
        Me._Radius = radius
        Me._Padding = padding
        Me.AdjustPadding(padding)
    End Sub

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="tabControl">     The tab control. </param>
    ''' <param name="imageAlignment"> The image alignment. </param>
    ''' <param name="radius">         The radius. </param>
    ''' <param name="overlap">        The overlap. </param>
    ''' <param name="padding">        The padding. </param>
    Protected Sub New(tabControl As CustomTabControl, ByVal imageAlignment As ContentAlignment,
                      ByVal radius As Integer, ByVal overlap As Integer,
                      ByVal padding As Point)
        Me.New(tabControl, imageAlignment)
        Me._Overlap = overlap
        Me._Radius = radius
        Me._Padding = padding
        Me.AdjustPadding(padding)
    End Sub

#End Region

#Region "Factory Methods"

    ''' <summary> Creates a provider. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="tabControl"> The tab control. </param>
    ''' <returns> The new provider. </returns>
    Public Shared Function CreateProvider(tabControl As CustomTabControl) As TabStyleProvider
        If tabControl Is Nothing Then Throw New ArgumentNullException(NameOf(tabControl))
        Dim provider As TabStyleProvider

        '	Depending on the display style of the tabControl generate an appropriate provider.
        Select Case tabControl.DisplayStyle
            Case TabStyle.None
                provider = New TabStyleNoneProvider(tabControl)
                Exit Select

            Case TabStyle.[Default]
                provider = New TabStyleDefaultProvider(tabControl)
                Exit Select

            Case TabStyle.Angled
                provider = New TabStyleAngledProvider(tabControl)
                Exit Select

            Case TabStyle.Rounded
                provider = New TabStyleRoundedProvider(tabControl)
                Exit Select

            Case TabStyle.VisualStudio
                provider = New TabStyleVisualStudioProvider(tabControl)
                Exit Select

            Case TabStyle.Chrome
                provider = New TabStyleChromeProvider(tabControl)
                Exit Select

            Case TabStyle.IE8
                provider = New TabStyleIE8Provider(tabControl)
                Exit Select

            Case TabStyle.VS2010
                provider = New TabStyleVS2010Provider(tabControl)
                Exit Select
            Case Else

                provider = New TabStyleDefaultProvider(tabControl)
                Exit Select
        End Select

        provider.DisplayStyle = tabControl.DisplayStyle
        Return provider
    End Function

#End Region

#Region "overridable Methods"

    ''' <summary> Adds a tab border to 'tabBounds'. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="path">      Full pathname of the file. </param>
    ''' <param name="tabBounds"> The tab bounds. </param>
    Public MustOverride Sub AddTabBorder(path As GraphicsPath, tabBounds As Rectangle)

    ''' <summary> Gets tab rectangle. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="index"> Zero-based index of the. </param>
    ''' <returns> The tab rectangle. </returns>
    Public Overridable Function GetTabRect(index As Integer) As Rectangle

        If index < 0 Then
            Return New Rectangle()
        End If
        Dim tabBounds As Rectangle = Me.TabControl.GetTabRect(index)
        If Me.TabControl.RightToLeftLayout Then
            tabBounds.X = Me.TabControl.Width - tabBounds.Right
        End If
        Dim firstTabinRow As Boolean = Me.TabControl.IsFirstTabInRow(index)

        '	Expand to overlap the tab page
        Select Case Me.TabControl.Alignment
            Case TabAlignment.Top
                tabBounds.Height += 2
                Exit Select
            Case TabAlignment.Bottom
                tabBounds.Height += 2
                tabBounds.Y -= 2
                Exit Select
            Case TabAlignment.Left
                tabBounds.Width += 2
                Exit Select
            Case TabAlignment.Right
                tabBounds.X -= 2
                tabBounds.Width += 2
                Exit Select
        End Select


        '	Create Overlap unless first tab in the row to align with tab page
        If (Not firstTabinRow OrElse Me.TabControl.RightToLeftLayout) AndAlso Me.Overlap > 0 Then
            If Me.TabControl.Alignment <= TabAlignment.Bottom Then
                tabBounds.X -= Me.Overlap
                tabBounds.Width += Me.Overlap
            Else
                tabBounds.Y -= Me.Overlap
                tabBounds.Height += Me.Overlap
            End If
        End If

        '	Adjust first tab in the row to align with tab page
        Me.EnsureFirstTabIsInView(tabBounds, index)

        Return tabBounds
    End Function

    ''' <summary> Ensures that first tab is in view. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="tabBounds"> [in,out] The tab bounds. </param>
    ''' <param name="index">     Zero-based index of the. </param>
    Protected Overridable Sub EnsureFirstTabIsInView(ByRef tabBounds As Rectangle, index As Integer)
        '	Adjust first tab in the row to align with tab page
        '	Make sure we only reposition visible tabs, as we may have scrolled out of view.

        Dim firstTabinRow As Boolean = Me.TabControl.IsFirstTabInRow(index)

        If firstTabinRow Then
            If Me.TabControl.Alignment <= TabAlignment.Bottom Then
                If Me.TabControl.RightToLeftLayout Then
                    If tabBounds.Left < Me.TabControl.Right Then
                        Dim tabPageRight As Integer = Me.TabControl.GetPageBounds(index).Right
                        If tabBounds.Right > tabPageRight Then
                            tabBounds.Width -= tabBounds.Right - tabPageRight
                        End If
                    End If
                Else
                    If tabBounds.Right > 0 Then
                        Dim tabPageX As Integer = Me.TabControl.GetPageBounds(index).X
                        If tabBounds.X < tabPageX Then
                            tabBounds.Width -= tabPageX - tabBounds.X
                            tabBounds.X = tabPageX
                        End If
                    End If
                End If
            Else
                If Me.TabControl.RightToLeftLayout Then
                    If tabBounds.Top < Me.TabControl.Bottom Then
                        Dim tabPageBottom As Integer = Me.TabControl.GetPageBounds(index).Bottom
                        If tabBounds.Bottom > tabPageBottom Then
                            tabBounds.Height -= tabBounds.Bottom - tabPageBottom
                        End If
                    End If
                Else
                    If tabBounds.Bottom > 0 Then
                        Dim tabPageY As Integer = Me.TabControl.GetPageBounds(index).Location.Y
                        If tabBounds.Y < tabPageY Then
                            tabBounds.Height -= tabPageY - tabBounds.Y
                            tabBounds.Y = tabPageY
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    ''' <summary> Gets tab background brush. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="index"> Zero-based index of the. </param>
    ''' <returns> The tab background brush. </returns>
    Protected Overridable Function GetTabBackgroundBrush(index As Integer) As Brush
        Dim fillBrush As LinearGradientBrush = Nothing

        '	Capture the colors dependent on selection state of the tab
        Dim dark As Color = Color.FromArgb(207, 207, 207)
        Dim light As Color = Color.FromArgb(242, 242, 242)

        If Me.TabControl.SelectedIndex = index Then
            dark = SystemColors.ControlLight
            light = SystemColors.Window
        ElseIf Not Me.TabControl.TabPages(index).Enabled Then
            light = dark
        ElseIf Me.HotTrack AndAlso index = Me.TabControl.ActiveIndex Then
            '	Enable hot tracking
            light = Color.FromArgb(234, 246, 253)
            dark = Color.FromArgb(167, 217, 245)
        End If

        '	Get the correctly aligned gradient
        Dim tabBounds As Rectangle = Me.GetTabRect(index)
        tabBounds.Inflate(3, 3)
        tabBounds.X -= 1
        tabBounds.Y -= 1
        Select Case Me.TabControl.Alignment
            Case TabAlignment.Top
                If Me.TabControl.SelectedIndex = index Then
                    dark = light
                End If
                fillBrush = New LinearGradientBrush(tabBounds, light, dark, LinearGradientMode.Vertical)
                Exit Select
            Case TabAlignment.Bottom
                fillBrush = New LinearGradientBrush(tabBounds, light, dark, LinearGradientMode.Vertical)
                Exit Select
            Case TabAlignment.Left
                fillBrush = New LinearGradientBrush(tabBounds, dark, light, LinearGradientMode.Horizontal)
                Exit Select
            Case TabAlignment.Right
                fillBrush = New LinearGradientBrush(tabBounds, light, dark, LinearGradientMode.Horizontal)
                Exit Select
        End Select

        '	Add the blend
        fillBrush.Blend = Me.GetBackgroundBlend()

        Return fillBrush
    End Function

#End Region

#Region "Base Properties"

    ''' <summary> Gets or sets the tab control. </summary>
    ''' <value> The tab control. </value>
    Protected Property TabControl As CustomTabControl

    ''' <summary> Gets or sets the display style. </summary>
    ''' <value> The display style. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property DisplayStyle() As TabStyle

    ''' <summary> The image align. </summary>
    Private _ImageAlign As ContentAlignment

    ''' <summary> Gets or sets the image align. </summary>
    ''' <value> The image align. </value>
    <Category("Appearance")>
    Public Property ImageAlign() As ContentAlignment
        Get
            Return Me._ImageAlign
        End Get
        Set
            Me._ImageAlign = Value
            Me.TabControl.Invalidate()
        End Set
    End Property

    ''' <summary> The padding. </summary>
    Private _Padding As Point

    ''' <summary> Adjust padding. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub AdjustPadding(ByVal value As Point)
        '	This line will trigger the handle to recreate, therefore invalidating the control
        DirectCast(Me.TabControl, TabControl).Padding = If(Me.ShowTabCloser,
            If(value.X + CInt(Me.Radius \ 2) < -6, New Point(0, value.Y), New Point(value.X + CInt(Me.Radius \ 2) + 6, value.Y)),
            If(value.X + CInt(Me.Radius \ 2) < 1, New Point(0, value.Y), New Point(value.X + CInt(Me.Radius \ 2) - 1, value.Y)))
    End Sub

    ''' <summary> Gets or sets the padding. </summary>
    ''' <value> The padding. </value>
    <Category("Appearance")>
    Public Property Padding() As Point
        Get
            Return Me._Padding
        End Get
        Set
            Me._Padding = Value
            Me.AdjustPadding(Value)
        End Set
    End Property

    ''' <summary> The radius. </summary>

    Private _Radius As Integer

    ''' <summary> Gets or sets the radius. </summary>
    ''' <value> The radius. </value>
    <Category("Appearance"), DefaultValue(1), Browsable(True)>
    Public Property Radius() As Integer
        Get
            Return Me._Radius
        End Get
        Set
            If Value < 1 Then Value = 1
            Me._Radius = Value
            Me.AdjustPadding(Me._Padding)
        End Set
    End Property

    ''' <summary> The overlap. </summary>
    Private _Overlap As Integer

    ''' <summary> Gets or sets the overlap. </summary>
    ''' <value> The overlap. </value>
    <Category("Appearance")>
    Public Property Overlap() As Integer
        Get
            Return Me._Overlap
        End Get
        Set
            If Value < 0 Then Value = 0
            Me._Overlap = Value
        End Set
    End Property

    ''' <summary> True to focus track. </summary>
    Private _FocusTrack As Boolean

    ''' <summary> Gets or sets the focus track. </summary>
    ''' <value> The focus track. </value>
    <Category("Appearance")>
    Public Property FocusTrack() As Boolean
        Get
            Return Me._FocusTrack
        End Get
        Set
            Me._FocusTrack = Value
            Me.TabControl.Invalidate()
        End Set
    End Property

    ''' <summary> True to hot track. </summary>
    Private _HotTrack As Boolean

    ''' <summary> Gets or sets the hot track. </summary>
    ''' <value> The hot track. </value>
    <Category("Appearance")>
    Public Property HotTrack() As Boolean
        Get
            Return Me._HotTrack
        End Get
        Set
            Me._HotTrack = Value
            DirectCast(Me.TabControl, TabControl).HotTrack = Value
        End Set
    End Property

    ''' <summary> True to show, false to hide the tab closer. </summary>
    Private _ShowTabCloser As Boolean

    ''' <summary> Gets or sets the show tab closer. </summary>
    ''' <value> The show tab closer. </value>
    <Category("Appearance")>
    Public Property ShowTabCloser() As Boolean
        Get
            Return Me._ShowTabCloser
        End Get
        Set
            Me._ShowTabCloser = Value
            Me.AdjustPadding(Me._Padding)
        End Set
    End Property

    ''' <summary> The opacity. </summary>
    Private _Opacity As Single = 1

    ''' <summary> Gets or sets the opacity. </summary>
    ''' <value> The opacity. </value>
    <Category("Appearance")>
    Public Property Opacity() As Single
        Get
            Return Me._Opacity
        End Get
        Set
            If Value < 0 Then
                Value = 0
            ElseIf Value > 1 Then
                Value = 1
            End If
            Me._Opacity = Value
            Me.TabControl.Invalidate()
        End Set
    End Property

    ''' <summary> The border color selected. </summary>
    Private _BorderColorSelected As Color

    ''' <summary> Gets or sets the border color selected. </summary>
    ''' <value> The border color selected. </value>
    <Category("Appearance"), DefaultValue(GetType(Color), "")>
    Public Property BorderColorSelected() As Color
        Get
            Return If(Me._BorderColorSelected.IsEmpty, ThemedColors.ToolBorder, Me._BorderColorSelected)
        End Get
        Set
            Me._BorderColorSelected = If(Value.Equals(ThemedColors.ToolBorder), Color.Empty, Value)
            Me.TabControl.Invalidate()
        End Set
    End Property

    ''' <summary> The border color hot. </summary>
    Private _BorderColorHot As Color

    ''' <summary> Gets or sets the border color hot. </summary>
    ''' <value> The border color hot. </value>
    <Category("Appearance"), DefaultValue(GetType(Color), "")>
    Public Property BorderColorHot() As Color
        Get
            Return If(Me._BorderColorHot.IsEmpty, SystemColors.ControlDark, Me._BorderColorHot)
        End Get
        Set
            Me._BorderColorHot = If(Value.Equals(SystemColors.ControlDark), Color.Empty, Value)
            Me.TabControl.Invalidate()
        End Set
    End Property

    ''' <summary> The border color. </summary>
    Private _BorderColor As Color

    ''' <summary> Gets or sets the color of the border. </summary>
    ''' <value> The color of the border. </value>
    <Category("Appearance"), DefaultValue(GetType(Color), "")>
    Public Property BorderColor() As Color
        Get
            Return If(Me._BorderColor.IsEmpty, SystemColors.ControlDark, Me._BorderColor)
        End Get
        Set
            Me._BorderColor = If(Value.Equals(SystemColors.ControlDark), Color.Empty, Value)
            Me.TabControl.Invalidate()
        End Set
    End Property

    ''' <summary> The text color. </summary>
    Private _TextColor As Color

    ''' <summary> Gets or sets the color of the text. </summary>
    ''' <value> The color of the text. </value>
    <Category("Appearance"), DefaultValue(GetType(Color), "")>
    Public Property TextColor() As Color
        Get
            Return If(Me._TextColor.IsEmpty, SystemColors.ControlText, Me._TextColor)
        End Get
        Set
            Me._TextColor = If(Value.Equals(SystemColors.ControlText), Color.Empty, Value)
            Me.TabControl.Invalidate()
        End Set
    End Property

    ''' <summary> The text color selected. </summary>
    Private _TextColorSelected As Color = Color.Empty

    ''' <summary> Gets or sets the text color selected. </summary>
    ''' <value> The text color selected. </value>
    <Category("Appearance"), DefaultValue(GetType(Color), "")>
    Public Property TextColorSelected() As Color
        Get
            Return If(Me._TextColorSelected.IsEmpty, SystemColors.ControlText, Me._TextColorSelected)
        End Get
        Set
            Me._TextColorSelected = If(Value.Equals(SystemColors.ControlText), Color.Empty, Value)
            Me.TabControl.Invalidate()
        End Set
    End Property

    ''' <summary> The text color disabled. </summary>
    Private _TextColorDisabled As Color

    ''' <summary> Gets or sets the text color disabled. </summary>
    ''' <value> The text color disabled. </value>
    <Category("Appearance"), DefaultValue(GetType(Color), "")>
    Public Property TextColorDisabled() As Color
        Get
            Return If(Me._TextColorDisabled.IsEmpty, SystemColors.ControlDark, Me._TextColorDisabled)
        End Get
        Set
            Me._TextColorDisabled = If(Value.Equals(SystemColors.ControlDark), Color.Empty, Value)
            Me.TabControl.Invalidate()
        End Set
    End Property

    ''' <summary> The focus color. </summary>
    Private _FocusColor As Color

    ''' <summary> Gets or sets the color of the focus. </summary>
    ''' <value> The color of the focus. </value>
    <Category("Appearance"), DefaultValue(GetType(Color), "Orange")>
    Public Property FocusColor() As Color
        Get
            Return Me._FocusColor
        End Get
        Set
            Me._FocusColor = Value
            Me.TabControl.Invalidate()
        End Set
    End Property

    ''' <summary> The closer color active. </summary>
    Private _CloserColorActive As Color

    ''' <summary> Gets or sets the closer color active. </summary>
    ''' <value> The closer color active. </value>
    <Category("Appearance"), DefaultValue(GetType(Color), "Black")>
    Public Property CloserColorActive() As Color
        Get
            Return Me._CloserColorActive
        End Get
        Set
            Me._CloserColorActive = Value
            Me.TabControl.Invalidate()
        End Set
    End Property

    ''' <summary> The closer color. </summary>
    Private _CloserColor As Color

    ''' <summary> Gets or sets the color of the closer. </summary>
    ''' <value> The color of the closer. </value>
    <Category("Appearance"), DefaultValue(GetType(Color), "DarkGrey")>
    Public Property CloserColor() As Color
        Get
            Return Me._CloserColor
        End Get
        Set
            Me._CloserColor = Value
            Me.TabControl.Invalidate()
        End Set
    End Property

#End Region

#Region "Painting"

    ''' <summary> Paints the tab. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="index">    Zero-based index of the. </param>
    ''' <param name="graphics"> The graphics. </param>
    Public Sub PaintTab(index As Integer, graphics As Graphics)
        If graphics Is Nothing Then Throw New ArgumentNullException(NameOf(graphics))
        Using tabpath As GraphicsPath = Me.GetTabBorder(index)
            Using fillBrush As Brush = Me.GetTabBackgroundBrush(index)
                '	Paint the background
                graphics.FillPath(fillBrush, tabpath)
                '	Paint a focus indication
                If Me.TabControl.Focused Then
                    Me.DrawTabFocusIndicator(tabpath, index, graphics)
                End If
                '	Paint the closer
                Me.DrawTabCloser(index, graphics)
            End Using
        End Using
    End Sub

    ''' <summary> Draw tab closer. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="index">    Zero-based index of the. </param>
    ''' <param name="graphics"> The graphics. </param>
    Protected Overridable Sub DrawTabCloser(index As Integer, graphics As Graphics)
        If graphics Is Nothing Then Throw New ArgumentNullException(NameOf(graphics))
        If Me.ShowTabCloser Then
            Dim closerRect As Rectangle = Me.TabControl.GetTabCloserRect(index)
            graphics.SmoothingMode = SmoothingMode.AntiAlias
            Using closerPath As GraphicsPath = TabStyleProvider.GetCloserPath(closerRect)
                If closerRect.Contains(Me.TabControl.MousePosition) Then
                    Using closerPen As New Pen(Me.CloserColorActive)
                        graphics.DrawPath(closerPen, closerPath)
                    End Using
                Else
                    Using closerPen As New Pen(Me.CloserColor)
                        graphics.DrawPath(closerPen, closerPath)
                    End Using

                End If
            End Using
        End If
    End Sub

    ''' <summary> Gets closer path. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="closerRect"> The closer rectangle. </param>
    ''' <returns> The closer path. </returns>
    Protected Shared Function GetCloserPath(closerRect As Rectangle) As GraphicsPath
        Dim closerPath As New GraphicsPath()
        closerPath.AddLine(closerRect.X, closerRect.Y, closerRect.Right, closerRect.Bottom)
        closerPath.CloseFigure()
        closerPath.AddLine(closerRect.Right, closerRect.Y, closerRect.X, closerRect.Bottom)
        closerPath.CloseFigure()

        Return closerPath
    End Function

    ''' <summary> Draw tab focus indicator. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="tabpath">  The tabpath. </param>
    ''' <param name="index">    Zero-based index of the. </param>
    ''' <param name="graphics"> The graphics. </param>
    Private Sub DrawTabFocusIndicator(tabpath As GraphicsPath, index As Integer, graphics As Graphics)
        If Me.FocusTrack AndAlso Me.TabControl.Focused AndAlso index = Me.TabControl.SelectedIndex Then
            Dim pathRect As RectangleF = tabpath.GetBounds()
            Dim focusRect As Rectangle

            Select Case Me.TabControl.Alignment
                Case TabAlignment.Top
                    focusRect = New Rectangle(CInt(Math.Truncate(pathRect.X)), CInt(Math.Truncate(pathRect.Y)), CInt(Math.Truncate(pathRect.Width)), 4)
                    Using focusBrush As Brush = New LinearGradientBrush(focusRect, Me.FocusColor, SystemColors.Window, LinearGradientMode.Vertical)
                        Using focusRegion As New Region(focusRect)
                            focusRegion.Intersect(tabpath)
                            graphics.FillRegion(focusBrush, focusRegion)
                        End Using
                    End Using
                Case TabAlignment.Bottom
                    focusRect = New Rectangle(CInt(Math.Truncate(pathRect.X)), CInt(Math.Truncate(pathRect.Bottom)) - 4, CInt(Math.Truncate(pathRect.Width)), 4)
                    Using focusBrush As Brush = New LinearGradientBrush(focusRect, SystemColors.ControlLight, Me.FocusColor, LinearGradientMode.Vertical)
                        Using focusRegion As New Region(focusRect)
                            focusRegion.Intersect(tabpath)
                            graphics.FillRegion(focusBrush, focusRegion)
                        End Using
                    End Using
                Case TabAlignment.Left
                    focusRect = New Rectangle(CInt(Math.Truncate(pathRect.X)), CInt(Math.Truncate(pathRect.Y)), 4, CInt(Math.Truncate(pathRect.Height)))
                    Using focusBrush As Brush = New LinearGradientBrush(focusRect, Me.FocusColor, SystemColors.ControlLight, LinearGradientMode.Horizontal)
                        Using focusRegion As New Region(focusRect)
                            focusRegion.Intersect(tabpath)
                            graphics.FillRegion(focusBrush, focusRegion)
                        End Using
                    End Using
                Case TabAlignment.Right
                    focusRect = New Rectangle(CInt(Math.Truncate(pathRect.Right)) - 4, CInt(Math.Truncate(pathRect.Y)), 4, CInt(Math.Truncate(pathRect.Height)))
                    Using focusBrush As Brush = New LinearGradientBrush(focusRect, SystemColors.ControlLight, Me.FocusColor, LinearGradientMode.Horizontal)
                        Using focusRegion As New Region(focusRect)
                            focusRegion.Intersect(tabpath)
                            graphics.FillRegion(focusBrush, focusRegion)
                        End Using
                    End Using
            End Select

        End If
    End Sub

#End Region

#Region "Background brushes"

    ''' <summary> Gets background blend. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> The background blend. </returns>
    Private Function GetBackgroundBlend() As Blend
        Dim relativeIntensities As Single() = New Single() {0F, 0.7F, 1.0F}
        Dim relativePositions As Single() = New Single() {0F, 0.6F, 1.0F}

        '	Glass look to top aligned tabs
        If Me.TabControl.Alignment = TabAlignment.Top Then
            relativeIntensities = New Single() {0F, 0.5F, 1.0F, 1.0F}
            relativePositions = New Single() {0F, 0.5F, 0.51F, 1.0F}
        End If

        Dim blend As New Blend() With {.Factors = relativeIntensities, .Positions = relativePositions}
        Return blend
    End Function

    ''' <summary> Gets page background brush. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="index"> Zero-based index of the. </param>
    ''' <returns> The page background brush. </returns>
    Public Overridable Function GetPageBackgroundBrush(index As Integer) As Brush

        '	Capture the colors dependent on selection state of the tab
        Dim light As Color = Color.FromArgb(242, 242, 242)
        If Me.TabControl.Alignment = TabAlignment.Top Then
            light = Color.FromArgb(207, 207, 207)
        End If

        If Me.TabControl.SelectedIndex = index Then
            light = SystemColors.Window
        ElseIf Not Me.TabControl.TabPages(index).Enabled Then
            light = Color.FromArgb(207, 207, 207)
        ElseIf Me.HotTrack AndAlso index = Me.TabControl.ActiveIndex Then
            '	Enable hot tracking
            light = Color.FromArgb(234, 246, 253)
        End If

        Return New SolidBrush(light)
    End Function

#End Region

#Region "Tab border and rectangle"

    ''' <summary> Gets tab border. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="index"> Zero-based index of the. </param>
    ''' <returns> The tab border. </returns>
    Public Function GetTabBorder(index As Integer) As GraphicsPath

        Dim path As New GraphicsPath()
        Dim tabBounds As Rectangle = Me.GetTabRect(index)

        Me.AddTabBorder(path, tabBounds)

        path.CloseFigure()
        Return path
    End Function

#End Region

End Class

''' <summary> Values that represent tab styles. </summary>
''' <remarks> David, 2020-09-24. </remarks>
Public Enum TabStyle

    ''' <summary> An enum constant representing the none option. </summary>
    None

    ''' <summary> An enum constant representing the default] option. </summary>
    [Default]

    ''' <summary> An enum constant representing the visual studio option. </summary>
    VisualStudio

    ''' <summary> An enum constant representing the rounded option. </summary>
    Rounded

    ''' <summary> An enum constant representing the angled option. </summary>
    Angled

    ''' <summary> An enum constant representing the chrome option. </summary>
    Chrome

    ''' <summary> An enum constant representing the IE 8 option. </summary>
    IE8

    ''' <summary> An enum constant representing the vs 2010 option. </summary>
    VS2010
End Enum
