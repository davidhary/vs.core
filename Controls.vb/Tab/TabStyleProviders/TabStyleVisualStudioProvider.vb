''' <summary> A tab style visual studio provider. </summary>
''' <remarks>
''' (c) 2010 The Man from U.N.C.L.E. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 09/26/2015, Created.</para><para>
''' http://www.codeproject.com/Articles/91387/Painting-Your-Own-Tabs-Second-Edition </para>
''' </remarks>
<System.ComponentModel.ToolboxItem(False)>
Public Class TabStyleVisualStudioProvider
    Inherits TabStyleProvider

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="tabControl"> The tab control. </param>
    Public Sub New(tabControl As CustomTabControl)
        MyBase.New(tabControl, ContentAlignment.MiddleRight, 1, 7, New Point(14, 1))
    End Sub

    ''' <summary> Adds a tab border to 'tabBounds'. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="path">      Full pathname of the file. </param>
    ''' <param name="tabBounds"> The tab bounds. </param>
    Public Overrides Sub AddTabBorder(path As System.Drawing.Drawing2D.GraphicsPath, tabBounds As System.Drawing.Rectangle)
        If path Is Nothing Then Throw New ArgumentNullException(NameOf(path))
        Select Case Me.TabControl.Alignment
            Case TabAlignment.Top
                path.AddLine(tabBounds.X, tabBounds.Bottom, tabBounds.X + tabBounds.Height - 4, tabBounds.Y + 2)
                path.AddLine(tabBounds.X + tabBounds.Height, tabBounds.Y, tabBounds.Right - 3, tabBounds.Y)
                path.AddArc(tabBounds.Right - 6, tabBounds.Y, 6, 6, 270, 90)
                path.AddLine(tabBounds.Right, tabBounds.Y + 3, tabBounds.Right, tabBounds.Bottom)
                Exit Select
            Case TabAlignment.Bottom
                path.AddLine(tabBounds.Right, tabBounds.Y, tabBounds.Right, tabBounds.Bottom - 3)
                path.AddArc(tabBounds.Right - 6, tabBounds.Bottom - 6, 6, 6, 0, 90)
                path.AddLine(tabBounds.Right - 3, tabBounds.Bottom, tabBounds.X + tabBounds.Height, tabBounds.Bottom)
                path.AddLine(tabBounds.X + tabBounds.Height - 4, tabBounds.Bottom - 2, tabBounds.X, tabBounds.Y)
                Exit Select
            Case TabAlignment.Left
                path.AddLine(tabBounds.Right, tabBounds.Bottom, tabBounds.X + 3, tabBounds.Bottom)
                path.AddArc(tabBounds.X, tabBounds.Bottom - 6, 6, 6, 90, 90)
                path.AddLine(tabBounds.X, tabBounds.Bottom - 3, tabBounds.X, tabBounds.Y + tabBounds.Width)
                path.AddLine(tabBounds.X + 2, tabBounds.Y + tabBounds.Width - 4, tabBounds.Right, tabBounds.Y)
                Exit Select
            Case TabAlignment.Right
                path.AddLine(tabBounds.X, tabBounds.Y, tabBounds.Right - 2, tabBounds.Y + tabBounds.Width - 4)
                path.AddLine(tabBounds.Right, tabBounds.Y + tabBounds.Width, tabBounds.Right, tabBounds.Bottom - 3)
                path.AddArc(tabBounds.Right - 6, tabBounds.Bottom - 6, 6, 6, 0, 90)
                path.AddLine(tabBounds.Right - 3, tabBounds.Bottom, tabBounds.X, tabBounds.Bottom)
                Exit Select
        End Select
    End Sub

End Class
