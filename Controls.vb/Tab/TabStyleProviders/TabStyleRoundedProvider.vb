''' <summary> A tab style rounded provider. </summary>
''' <remarks>
''' (c) 2010 The Man from U.N.C.L.E. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 09/26/2015, Created.</para><para>
''' http://www.codeproject.com/Articles/91387/Painting-Your-Own-Tabs-Second-Edition </para>
''' </remarks>
<System.ComponentModel.ToolboxItem(False)>
Public Class TabStyleRoundedProvider
    Inherits TabStyleProvider

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="tabControl"> The tab control. </param>
    Public Sub New(tabControl As CustomTabControl)
        MyBase.New(tabControl, 10)
    End Sub

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="tabControl">        The tab control. </param>
    ''' <param name="borderColor">       The border color. </param>
    ''' <param name="borderColorHot">    The border color hot. </param>
    ''' <param name="textColor">         The text color. </param>
    ''' <param name="textColorDisabled"> The text color disabled. </param>
    ''' <param name="padding">           The padding. </param>
    ''' <param name="radius">            The radius. </param>
    ''' <param name="showTabCloser">     True to show, false to hide the tab closer. </param>
    ''' <param name="closerColorActive"> The closer color active. </param>
    ''' <param name="closerColor">       The closer color. </param>
    Public Sub New(tabControl As CustomTabControl,
                      ByVal borderColor As Color, ByVal borderColorHot As Color,
                      ByVal textColor As Color, ByVal textColorDisabled As Color,
                      ByVal padding As Point, ByVal radius As Integer,
                      ByVal showTabCloser As Boolean, ByVal closerColorActive As Color, ByVal closerColor As Color)
        MyBase.New(tabControl,
                   borderColor, borderColorHot,
                   textColor, textColorDisabled,
                   padding, radius,
               showTabCloser, closerColorActive, closerColor)
    End Sub

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="tabControl">        The tab control. </param>
    ''' <param name="radius">            The radius. </param>
    ''' <param name="showTabCloser">     True to show, false to hide the tab closer. </param>
    ''' <param name="closerColorActive"> The closer color active. </param>
    ''' <param name="padding">           The padding. </param>
    Public Sub New(tabControl As CustomTabControl, ByVal radius As Integer,
                      ByVal showTabCloser As Boolean, ByVal closerColorActive As Color,
                      ByVal padding As Point)
        MyBase.New(tabControl, radius, showTabCloser, closerColorActive, padding)
    End Sub

    ''' <summary> Adds a tab border to 'tabBounds'. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="path">      Full pathname of the file. </param>
    ''' <param name="tabBounds"> The tab bounds. </param>
    Public Overrides Sub AddTabBorder(path As System.Drawing.Drawing2D.GraphicsPath, tabBounds As System.Drawing.Rectangle)
        If path Is Nothing Then Throw New ArgumentNullException(NameOf(path))
        Select Case Me.TabControl.Alignment
            Case TabAlignment.Top
                path.AddLine(tabBounds.X, tabBounds.Bottom, tabBounds.X, tabBounds.Y + Me.Radius)
                path.AddArc(tabBounds.X, tabBounds.Y, Me.Radius * 2, Me.Radius * 2, 180, 90)
                path.AddLine(tabBounds.X + Me.Radius, tabBounds.Y, tabBounds.Right - Me.Radius, tabBounds.Y)
                path.AddArc(tabBounds.Right - Me.Radius * 2, tabBounds.Y, Me.Radius * 2, Me.Radius * 2, 270, 90)
                path.AddLine(tabBounds.Right, tabBounds.Y + Me.Radius, tabBounds.Right, tabBounds.Bottom)
                Exit Select
            Case TabAlignment.Bottom
                path.AddLine(tabBounds.Right, tabBounds.Y, tabBounds.Right, tabBounds.Bottom - Me.Radius)
                path.AddArc(tabBounds.Right - Me.Radius * 2, tabBounds.Bottom - Me.Radius * 2, Me.Radius * 2, Me.Radius * 2, 0, 90)
                path.AddLine(tabBounds.Right - Me.Radius, tabBounds.Bottom, tabBounds.X + Me.Radius, tabBounds.Bottom)
                path.AddArc(tabBounds.X, tabBounds.Bottom - Me.Radius * 2, Me.Radius * 2, Me.Radius * 2, 90, 90)
                path.AddLine(tabBounds.X, tabBounds.Bottom - Me.Radius, tabBounds.X, tabBounds.Y)
                Exit Select
            Case TabAlignment.Left
                path.AddLine(tabBounds.Right, tabBounds.Bottom, tabBounds.X + Me.Radius, tabBounds.Bottom)
                path.AddArc(tabBounds.X, tabBounds.Bottom - Me.Radius * 2, Me.Radius * 2, Me.Radius * 2, 90, 90)
                path.AddLine(tabBounds.X, tabBounds.Bottom - Me.Radius, tabBounds.X, tabBounds.Y + Me.Radius)
                path.AddArc(tabBounds.X, tabBounds.Y, Me.Radius * 2, Me.Radius * 2, 180, 90)
                path.AddLine(tabBounds.X + Me.Radius, tabBounds.Y, tabBounds.Right, tabBounds.Y)
                Exit Select
            Case TabAlignment.Right
                path.AddLine(tabBounds.X, tabBounds.Y, tabBounds.Right - Me.Radius, tabBounds.Y)
                path.AddArc(tabBounds.Right - Me.Radius * 2, tabBounds.Y, Me.Radius * 2, Me.Radius * 2, 270, 90)
                path.AddLine(tabBounds.Right, tabBounds.Y + Me.Radius, tabBounds.Right, tabBounds.Bottom - Me.Radius)
                path.AddArc(tabBounds.Right - Me.Radius * 2, tabBounds.Bottom - Me.Radius * 2, Me.Radius * 2, Me.Radius * 2, 0, 90)
                path.AddLine(tabBounds.Right - Me.Radius, tabBounds.Bottom, tabBounds.X, tabBounds.Bottom)
                Exit Select
        End Select
    End Sub
End Class
