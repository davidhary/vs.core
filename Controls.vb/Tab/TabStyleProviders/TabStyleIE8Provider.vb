Imports System.Drawing.Drawing2D

''' <summary> A tab style IE 8 provider. </summary>
''' <remarks>
''' (c) 2010 The Man from U.N.C.L.E. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 09/26/2015, Created.</para><para>
''' http://www.codeproject.com/Articles/91387/Painting-Your-Own-Tabs-Second-Edition </para>
''' </remarks>
<System.ComponentModel.ToolboxItem(False)>
Public Class TabStyleIE8Provider
    Inherits TabStyleRoundedProvider

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="tabControl"> The tab control. </param>
    Public Sub New(tabControl As CustomTabControl)
        MyBase.New(tabControl, 3, True, Color.Red, New Point(6, 5))
    End Sub

    ''' <summary> Gets tab rectangle. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="index"> Zero-based index of the. </param>
    ''' <returns> The tab rectangle. </returns>
    Public Overrides Function GetTabRect(index As Integer) As Rectangle
        If index < 0 Then
            Return New Rectangle()
        End If
        Dim tabBounds As Rectangle = MyBase.GetTabRect(index)
        Dim firstTabinRow As Boolean = Me.TabControl.IsFirstTabInRow(index)

        '	Make non-SelectedTabs smaller and selected tab bigger
        If index <> Me.TabControl.SelectedIndex Then
            Select Case Me.TabControl.Alignment
                Case TabAlignment.Top
                    tabBounds.Y += 1
                    tabBounds.Height -= 1
                    Exit Select
                Case TabAlignment.Bottom
                    tabBounds.Height -= 1
                    Exit Select
                Case TabAlignment.Left
                    tabBounds.X += 1
                    tabBounds.Width -= 1
                    Exit Select
                Case TabAlignment.Right
                    tabBounds.Width -= 1
                    Exit Select
            End Select
        Else
            Select Case Me.TabControl.Alignment
                Case TabAlignment.Top
                    tabBounds.Y -= 1
                    tabBounds.Height += 1

                    If firstTabinRow Then
                        tabBounds.Width += 1
                    Else
                        tabBounds.X -= 1
                        tabBounds.Width += 2
                    End If
                    Exit Select
                Case TabAlignment.Bottom
                    tabBounds.Height += 1

                    If firstTabinRow Then
                        tabBounds.Width += 1
                    Else
                        tabBounds.X -= 1
                        tabBounds.Width += 2
                    End If
                    Exit Select
                Case TabAlignment.Left
                    tabBounds.X -= 1
                    tabBounds.Width += 1

                    If firstTabinRow Then
                        tabBounds.Height += 1
                    Else
                        tabBounds.Y -= 1
                        tabBounds.Height += 2
                    End If
                    Exit Select
                Case TabAlignment.Right
                    tabBounds.Width += 1
                    If firstTabinRow Then
                        tabBounds.Height += 1
                    Else
                        tabBounds.Y -= 1
                        tabBounds.Height += 2
                    End If
                    Exit Select
            End Select
        End If

        '	Adjust first tab in the row to align with tab page
        Me.EnsureFirstTabIsInView(tabBounds, index)
        Return tabBounds
    End Function

    ''' <summary> Gets tab background brush. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="index"> Zero-based index of the. </param>
    ''' <returns> The tab background brush. </returns>
    Protected Overrides Function GetTabBackgroundBrush(index As Integer) As Brush
        Dim fillBrush As LinearGradientBrush = Nothing

        '	Capture the colors dependent on selection state of the tab
        Dim dark As Color = Color.FromArgb(227, 238, 251)
        Dim light As Color = Color.FromArgb(227, 238, 251)

        If Me.TabControl.SelectedIndex = index Then
            dark = Color.FromArgb(196, 222, 251)
            light = SystemColors.Window
        ElseIf Not Me.TabControl.TabPages(index).Enabled Then
            light = dark
        ElseIf Me.HotTrack AndAlso index = Me.TabControl.ActiveIndex Then
            '	Enable hot tracking
            light = SystemColors.Window
            dark = Color.FromArgb(166, 203, 248)
        End If

        '	Get the correctly aligned gradient
        Dim tabBounds As Rectangle = Me.GetTabRect(index)
        tabBounds.Inflate(3, 3)
        tabBounds.X -= 1
        tabBounds.Y -= 1
        Select Case Me.TabControl.Alignment
            Case TabAlignment.Top
                fillBrush = New LinearGradientBrush(tabBounds, dark, light, LinearGradientMode.Vertical)
                Exit Select
            Case TabAlignment.Bottom
                fillBrush = New LinearGradientBrush(tabBounds, light, dark, LinearGradientMode.Vertical)
                Exit Select
            Case TabAlignment.Left
                fillBrush = New LinearGradientBrush(tabBounds, dark, light, LinearGradientMode.Horizontal)
                Exit Select
            Case TabAlignment.Right
                fillBrush = New LinearGradientBrush(tabBounds, light, dark, LinearGradientMode.Horizontal)
                Exit Select
        End Select

        '	Add the blend
        fillBrush.Blend = Me.GetBackgroundBlend(index)

        Return fillBrush
    End Function

    ''' <summary> Gets background blend. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="index"> Zero-based index of the. </param>
    ''' <returns> The background blend. </returns>
    Private Overloads Function GetBackgroundBlend(index As Integer) As Blend
        Dim relativeIntensities As Single() = New Single() {0F, 0.7F, 1.0F}
        Dim relativePositions As Single() = New Single() {0F, 0.8F, 1.0F}
        If Me.TabControl.SelectedIndex <> index Then
            relativeIntensities = New Single() {0F, 0.3F, 1.0F}
            relativePositions = New Single() {0F, 0.2F, 1.0F}
        End If
        Dim blend As New Blend() With {.Factors = relativeIntensities, .Positions = relativePositions}
        Return blend
    End Function

    ''' <summary> Draw tab closer. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="index">    Zero-based index of the. </param>
    ''' <param name="graphics"> The graphics. </param>
    Protected Overrides Sub DrawTabCloser(index As Integer, graphics As Graphics)
        If graphics Is Nothing Then Throw New ArgumentNullException(NameOf(graphics))
        If Me.ShowTabCloser Then
            Dim closerRect As Rectangle = Me.TabControl.GetTabCloserRect(index)
            graphics.SmoothingMode = SmoothingMode.AntiAlias
            If closerRect.Contains(Me.TabControl.MousePosition) Then
                Using closerPath As GraphicsPath = GetCloserButtonPath(closerRect)
                    graphics.FillPath(Brushes.White, closerPath)
                    Using closerPen As New Pen(Me.BorderColor)
                        graphics.DrawPath(closerPen, closerPath)
                    End Using
                End Using
                Using closerPath As GraphicsPath = GetCloserPath(closerRect)
                    Using closerPen As New Pen(Me.CloserColorActive)
                        closerPen.Width = 2
                        graphics.DrawPath(closerPen, closerPath)
                    End Using
                End Using
            Else
                Using closerPath As GraphicsPath = GetCloserPath(closerRect)
                    Using closerPen As New Pen(Me.CloserColor)
                        closerPen.Width = 2

                        graphics.DrawPath(closerPen, closerPath)
                    End Using
                End Using

            End If
        End If
    End Sub

    ''' <summary> Gets closer button path. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="closerRect"> The closer rectangle. </param>
    ''' <returns> The closer button path. </returns>
    Private Shared Function GetCloserButtonPath(closerRect As Rectangle) As GraphicsPath
        Dim closerPath As New GraphicsPath()
        closerPath.AddLine(closerRect.X - 1, closerRect.Y - 2, closerRect.Right + 1, closerRect.Y - 2)
        closerPath.AddLine(closerRect.Right + 2, closerRect.Y - 1, closerRect.Right + 2, closerRect.Bottom + 1)
        closerPath.AddLine(closerRect.Right + 1, closerRect.Bottom + 2, closerRect.X - 1, closerRect.Bottom + 2)
        closerPath.AddLine(closerRect.X - 2, closerRect.Bottom + 1, closerRect.X - 2, closerRect.Y - 1)
        closerPath.CloseFigure()
        Return closerPath
    End Function

    ''' <summary> Gets page background brush. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="index"> Zero-based index of the. </param>
    ''' <returns> The page background brush. </returns>
    Public Overrides Function GetPageBackgroundBrush(index As Integer) As Brush

        '	Capture the colors dependent on selection state of the tab
        Dim light As Color = Color.FromArgb(227, 238, 251)

        If Me.TabControl.SelectedIndex = index Then
            light = SystemColors.Window
        ElseIf Not Me.TabControl.TabPages(index).Enabled Then
            light = Color.FromArgb(207, 207, 207)
        ElseIf Me.HotTrack AndAlso index = Me.TabControl.ActiveIndex Then
            '	Enable hot tracking
            light = Color.FromArgb(234, 246, 253)
        End If

        Return New SolidBrush(light)
    End Function

End Class
