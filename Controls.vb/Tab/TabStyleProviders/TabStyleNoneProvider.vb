''' <summary> A tab style none provider. </summary>
''' <remarks>
''' (c) 2010 The Man from U.N.C.L.E. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 09/26/2015, Created.</para><para>
''' http://www.codeproject.com/Articles/91387/Painting-Your-Own-Tabs-Second-Edition </para>
''' </remarks>
<System.ComponentModel.ToolboxItem(False)>
Public Class TabStyleNoneProvider
    Inherits TabStyleProvider

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="tabControl"> The tab control. </param>
    Public Sub New(tabControl As CustomTabControl)
        MyBase.New(tabControl)
    End Sub

    ''' <summary> Adds a tab border to 'tabBounds'. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="path">      Full pathname of the file. </param>
    ''' <param name="tabBounds"> The tab bounds. </param>
    Public Overrides Sub AddTabBorder(path As System.Drawing.Drawing2D.GraphicsPath, tabBounds As System.Drawing.Rectangle)
    End Sub
End Class
