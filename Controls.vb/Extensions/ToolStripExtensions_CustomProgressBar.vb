﻿Imports System.Runtime.CompilerServices
Namespace ToolStripExtensions

    ''' <summary> Includes extensions for <see cref="ToolStrip">Tool Strip</see>. </summary>
    ''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 11/19/2010, 1.2.3975.x. </para></remarks>
    Public Module Methods

#Region " STATUS STRIP CUSTOM PROGRESS BAR "

        ''' <summary>
        ''' Updates the <see cref="isr.Core.Controls.StatusStripCustomProgressBar">control</see> value to
        ''' the
        ''' <paramref name="value">value</paramref> limited by the control range.
        ''' This setter is thread safe.
        ''' </summary>
        ''' <remarks> David, 2020-09-24. </remarks>
        ''' <param name="control"> The tool strip progress bar control. </param>
        ''' <param name="value">   The candidate value. </param>
        ''' <returns> The value limited within the range of the progress bar. </returns>
        <Extension()>
        Public Function ValueUpdater(ByVal control As isr.Core.Controls.StatusStripCustomProgressBar, ByVal value As Integer) As Integer
            If control IsNot Nothing Then
                If control.Value <> value Then
                    Return ValueSetter(control, value)
                End If
            End If
            Return value
        End Function

        ''' <summary>
        ''' Sets the <see cref="ProgressBar">control</see> value to the
        ''' <paramref name="value">value</paramref> limited by the control range.
        ''' </summary>
        ''' <remarks> David, 2020-09-24. </remarks>
        ''' <param name="control"> The tool strip progress bar control. </param>
        ''' <param name="value">   The candidate value. </param>
        ''' <returns> The value limited within the range of the progress bar. </returns>
        <Extension()>
        Public Function ValueSetter(ByVal control As isr.Core.Controls.StatusStripCustomProgressBar, ByVal value As Integer) As Integer
            If control IsNot Nothing Then
                value = If(value < control.Minimum, control.Minimum, If(value > control.Maximum, control.Maximum, value))
                If control.Value <> value Then control.Value = value
                Return CInt(control.Value)
            End If
            Return value
        End Function

        ''' <summary>
        ''' Sets the <see cref="ProgressBar">
        ''' control</see>
        ''' value to the.
        ''' </summary>
        ''' <remarks> David, 2020-09-24. </remarks>
        ''' <param name="control"> The tool strip progress bar control. </param>
        ''' <param name="value">   The candidate value. </param>
        ''' <returns> The value limited within the range of the progress bar. </returns>
        <Extension()>
        Public Function ValueSetter(ByVal control As isr.Core.Controls.StatusStripCustomProgressBar, ByVal value As Double) As Integer
            Return ValueSetter(control, CInt(value))
        End Function

#End Region

    End Module
End Namespace
