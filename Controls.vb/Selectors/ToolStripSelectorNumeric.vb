Imports System.Windows.Forms.Design
Imports System.ComponentModel

''' <summary> Tool strip selector numeric up down. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 3/2/2015 </para>
''' </remarks>
<ToolStripItemDesignerAvailability(ToolStripItemDesignerAvailability.ToolStrip)>
Public Class ToolStripSelectorNumeric
    Inherits ToolStripSelectorBase

    ''' <summary> Default constructor. </summary>
    ''' <remarks> Call the base constructor passing in a SelectorNumeric instance. </remarks>
    Public Sub New()
        MyBase.New(New SelectorNumeric())
    End Sub

#Region " UNDERLYING CONTROL PROPERTIES "

    ''' <summary> Gets the numeric up down control. </summary>
    ''' <value> The numeric up down control. </value>
    Public ReadOnly Property SelectorNumericControl() As SelectorNumeric
        Get
            Return TryCast(Me.Control, SelectorNumeric)
        End Get
    End Property

    ''' <summary> Gets or sets the value. </summary>
    ''' <value> The value. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property SelectedValue() As Decimal?
        Get
            Return Me.SelectorNumericControl.SelectedValue
        End Get
        Set(ByVal value As Decimal?)
            Me.SelectorNumericControl.SelectedValue = value
        End Set
    End Property

    ''' <summary> Gets or sets the selected Value. </summary>
    ''' <value> The selected Value. </value>
    <DefaultValue("")>
    <Description("Value"), Category("Appearance")>
    Public Property Value() As Decimal
        Get
            Return Me.SelectorNumericControl.Value
        End Get
        Set(ByVal value As Decimal)
            Me.SelectorNumericControl.Value = value
        End Set
    End Property

    ''' <summary> Gets or sets the maximum value. </summary>
    ''' <value> The maximum value. </value>
    <DefaultValue(100)>
    <Description("Maximum"), Category("Appearance")>
    Public Property Maximum() As Decimal
        Get
            Return Me.SelectorNumericControl.Maximum
        End Get
        Set(ByVal value As Decimal)
            Me.SelectorNumericControl.Maximum = value
        End Set
    End Property

    ''' <summary> Gets or sets the minimum value. </summary>
    ''' <value> The minimum value. </value>
    <DefaultValue(0)>
    <Description("Minimum"), Category("Appearance")>
    Public Property Minimum() As Decimal
        Get
            Return Me.SelectorNumericControl.Minimum
        End Get
        Set(ByVal value As Decimal)
            Me.SelectorNumericControl.Minimum = value
        End Set
    End Property

    ''' <summary> Decimal Places. </summary>
    ''' <value> The decimal places. </value>
    <DefaultValue(0)>
    <Description("Decimal Places"), Category("Appearance")>
    Public Property DecimalPlaces As Integer
        Get
            Return Me.SelectorNumericControl.DecimalPlaces
        End Get
        Set(ByVal value As Integer)
            Me.SelectorNumericControl.DecimalPlaces = value
        End Set
    End Property

    ''' <summary> Increment. </summary>
    ''' <value> The amount to increment by. </value>
    <DefaultValue(1)>
    <Description("Increment"), Category("Appearance")>
    Public Property Increment() As Decimal
        Get
            Return Me.SelectorNumericControl.NumericUpDown.Increment
        End Get
        Set(ByVal value As Decimal)
            Me.SelectorNumericControl.NumericUpDown.Increment = value
        End Set
    End Property

    ''' <summary> Gets the numeric up down. </summary>
    ''' <value> The numeric up down. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property NumericUpDown As isr.Core.Controls.NumericUpDown
        Get
            Return Me.SelectorNumericControl.NumericUpDown
        End Get
    End Property

    ''' <summary> Gets the button. </summary>
    ''' <value> The button. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Button As System.Windows.Forms.Button
        Get
            Return Me.SelectorNumericControl.Button
        End Get
    End Property

#End Region

#Region " UNDERLYING CONTROL METHODS "

    ''' <summary> Select value. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The selected Value. </param>
    Public Overloads Sub SelectValue(ByVal value As Decimal)
        Me.SelectorNumericControl.SelectValue(value)
    End Sub

#End Region

End Class

