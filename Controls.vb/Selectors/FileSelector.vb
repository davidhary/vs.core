Imports System.ComponentModel

Imports isr.Core.Controls.ExceptionExtensions
Imports isr.Core.Forma.CompactExtensions

''' <summary> A simple text box and browse button file selector. </summary>
''' <remarks>
''' (c) 2007 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 03/14/2007, 1.2.3405. Add default event and description and move bitmap setting
''' from the designer file. </para><para>
''' David, 03/14/2007, 1.00.2619. </para>
''' </remarks>
<Description("File Selector"), DefaultEvent("SelectedChanged")>
Public Class FileSelector
    Inherits Forma.ModelViewBase

#Region " CONSTRUCTION "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        MyBase.New()
        'This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me._TextBoxContents = String.Empty
        Me._FilePath = String.Empty
        Me.DefaultExtension = ".TXT"
        Me.DialogFilter = "Text files (*.txt; *.lst)|*.txt;*.lst|Batch files (*.bat)|*.bat|All Files (*.*)|*.*"
        Me.DialogTitle = "SELECT A TEXT FILE"
        Me._FileExtension = String.Empty
        Me._FileName = String.Empty
        Me._FileTitle = String.Empty
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the isr.Core.Forma.ModelViewBase and optionally
    ''' releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.RemoveSelectedChangedEventHandler(Me.SelectedChangedEvent)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " FILE NAME PROPERTIES "

    ''' <summary> The default extension. </summary>
    Private _DefaultExtension As String

    ''' <summary> Gets or sets the default extension. </summary>
    ''' <value> The default extension. </value>
    <Category("Appearance"), Description("Default extension"), Browsable(True),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
        DefaultValue(".TXT")>
    Public Property DefaultExtension() As String
        Get
            Return Me._DefaultExtension
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            If Not String.Equals(value, Me.DefaultExtension, StringComparison.OrdinalIgnoreCase) Then
                Me._DefaultExtension = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> A filter specifying the dialog. </summary>
    Private _DialogFilter As String

    ''' <summary>
    ''' Gets or sets the filename filter string, which determines the choices of files that appear in
    ''' the file type selection drop down list.
    ''' </summary>
    ''' <value> The dialog filter. </value>
    <Category("Appearance"), Description("Filename filter string determining the choices of files that appear in the file type selection drop down list."),
        Browsable(True),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
        DefaultValue("Text files (*.txt; *.lst)|*.txt;*.lst|Batch files (*.bat)|*.bat|All Files (*.*)|*.*")>
    Public Property DialogFilter() As String
        Get
            Return Me._DialogFilter
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            If Not String.Equals(value, Me.DialogFilter, StringComparison.OrdinalIgnoreCase) Then
                Me._DialogFilter = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The dialog title. </summary>
    Private _DialogTitle As String

    ''' <summary> Gets or sets the dialog title. </summary>
    ''' <value> The dialog title. </value>
    <Category("Appearance"), Description("Dialog caption"),
        Browsable(True),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
        DefaultValue("SELECT A TEXT FILE")>
    Public Property DialogTitle() As String
        Get
            Return Me._DialogTitle
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            If Not String.Equals(value, Me.DialogTitle, StringComparison.OrdinalIgnoreCase) Then
                Me._DialogTitle = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The file extension. </summary>
    Private _FileExtension As String

    ''' <summary> Gets or sets the file extension. </summary>
    ''' <value> The name of the extension. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property FileExtension() As String
        Get
            Return Me._FileExtension
        End Get
        Protected Set(value As String)
            Me._FileExtension = value
            Me._FileTitle = FileSelector.ParseFileTitle(Me.FileName, Me.FileExtension)
        End Set
    End Property

    ''' <summary> Filename of the file. </summary>
    Private _FileName As String

    ''' <summary> Gets or sets the file name. </summary>
    ''' <value> The name of the file. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property FileName() As String
        Get
            Return Me._FileName
        End Get
        Set(value As String)
            Me._FileName = value
            Me._FileTitle = FileSelector.ParseFileTitle(Me.FileName, Me.FileExtension)
        End Set
    End Property

    ''' <summary> Parses file title. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="name">      The name. </param>
    ''' <param name="extension"> The extension. </param>
    ''' <returns> A String. </returns>
    Public Shared Function ParseFileTitle(ByVal name As String, ByVal extension As String) As String
        Return If(String.IsNullOrWhiteSpace(name),
            String.Empty,
            If(String.IsNullOrWhiteSpace(extension),
                name,
                name.Substring(0, name.LastIndexOf(extension, StringComparison.OrdinalIgnoreCase))))
    End Function

    ''' <summary> The text box contents. </summary>

    Private _TextBoxContents As String

    ''' <summary> Full pathname of the file. </summary>
    Private _FilePath As String

    ''' <summary> Gets or sets a new file path. </summary>
    ''' <value> The full pathname of the file. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property FilePath() As String
        Get
            Return Me._FilePath
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            value = value.Trim
            If Me._FilePath Is Nothing Then Me._FilePath = String.Empty
            If Not (Me._FilePath.Equals(value, comparisonType:=StringComparison.OrdinalIgnoreCase) AndAlso
                    Me._TextBoxContents.Equals(value.Compact(Me._FilePathTextBox), comparisonType:=StringComparison.OrdinalIgnoreCase)) Then
                If String.IsNullOrWhiteSpace(value) Then
                    Me._TextBoxContents = String.Empty
                    Me._FilePathTextBox.Text = Me._TextBoxContents
                    Me._FileName = Me._TextBoxContents
                    Me._FileExtension = String.Empty
                    Me._toolTip.SetToolTip(Me._FilePathTextBox, "Enter or browse for a file name")
                Else
                    If System.IO.File.Exists(value) Then
                        Dim fileInfo As System.IO.FileInfo = My.Computer.FileSystem.GetFileInfo(value)
                        value = fileInfo.FullName
                        Me._TextBoxContents = value.Compact(Me._FilePathTextBox)
                        Me._FilePathTextBox.Text = Me._TextBoxContents
                        Me._toolTip.SetToolTip(Me._FilePathTextBox, value)
                        Me._FileName = fileInfo.Name
                        Me._FileExtension = fileInfo.Extension
                    Else
                        Me._TextBoxContents = "<file not found: " & value & ">"
                        Me._FilePathTextBox.Text = Me._TextBoxContents
                        Me._toolTip.SetToolTip(Me._FilePathTextBox, "Enter or browse for a file name")
                        Me._FileName = String.Empty
                        Me._FileExtension = String.Empty
                    End If
                End If
                Me._FileTitle = FileSelector.ParseFileTitle(Me.FileName, Me.FileExtension)
                Me._FilePath = value
                Me.OnSelectedChanged()
            End If
            Me._FilePathTextBox.Refresh()
            Me.NotifyPropertyChanged()
        End Set
    End Property

    ''' <summary> The file title. </summary>
    Private _FileTitle As String

    ''' <summary> Gets the file title. </summary>
    ''' <value> The file title. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property FileTitle() As String
        Get
            Return Me._FileTitle
        End Get
    End Property

#End Region

#Region " REFRESH ; SELECT FILE "

    ''' <summary> Refreshes the control. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Overrides Sub Refresh()
        MyBase.Refresh()
    End Sub

    ''' <summary> Selects a file. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
    Public Function SelectFile() As Boolean

        Using dialog As New OpenFileDialog()

            ' Use the common dialog class
            dialog.RestoreDirectory = True
            dialog.Multiselect = False
            dialog.CheckFileExists = True
            dialog.CheckPathExists = True
            dialog.ReadOnlyChecked = False
            dialog.DefaultExt = Me._DefaultExtension
            dialog.Title = Me._DialogTitle
            dialog.FilterIndex = 0
            dialog.FileName = If(String.IsNullOrWhiteSpace(Me._FilePath), My.Computer.FileSystem.CurrentDirectory, Me.FilePath)
            dialog.Filter = Me._DialogFilter

            ' Open the Open dialog
            If dialog.ShowDialog(Me) = DialogResult.OK Then

                ' if file selected,
                Me.FilePath = dialog.FileName

                ' return true for pass
                Return True

            Else

                Return False

            End If

        End Using

    End Function

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary>
    ''' Event handler. Called by browseButton for click events. Browses for a file name.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="eventSender"> The event sender. </param>
    ''' <param name="eventArgs">   Event information. </param>
    Private Sub BrowseButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _BrowseButton.Click

        ' Select a file
        Me.SelectFile()

    End Sub

    ''' <summary>
    ''' Event handler. Called by filePathTextBox for validating events. Enters a file name.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Cancel event information. </param>
    Private Sub FilePathTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _FilePathTextBox.Validating

        Me.FilePath = Me._FilePathTextBox.Text

    End Sub

    ''' <summary>
    ''' Event handler. Called by UserControl for resize events. Resized the frame and sets the width
    ''' of the text boxes.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="eventSender"> The event sender. </param>
    ''' <param name="eventArgs">   Event information. </param>
    Private Sub UserControl_Resize(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Resize

        Me.Height = Math.Max(Me._BrowseButton.Height, Me._FilePathTextBox.Height) + 2

        Me._BrowseButton.Top = (Me.Height - Me._BrowseButton.Height) \ 2
        Me._BrowseButton.Left = Me.Width - Me._BrowseButton.Width - 1
        Me._BrowseButton.Refresh()

        Me._FilePathTextBox.Top = (Me.Height - Me._FilePathTextBox.Height) \ 2
        Me._FilePathTextBox.Width = Me._BrowseButton.Left - 1
        Me._FilePathTextBox.Refresh()

        Me.Refresh()

    End Sub

#End Region

#Region " EVENTS "

    ''' <summary> Occurs when a new file was selected. </summary>
    ''' <param name="e">Specifies the <see cref="EventArgs">event arguments</see>. </param>
    Public Event SelectedChanged As EventHandler(Of EventArgs)

    ''' <summary> Removes event handler. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The handler. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub RemoveSelectedChangedEventHandler(ByVal value As EventHandler(Of EventArgs))
#Disable Warning CA1825 ' Avoid zero-length array allocations.
        For Each d As [Delegate] In If(value Is Nothing, New [Delegate]() {}, value.GetInvocationList)
#Enable Warning CA1825 ' Avoid zero-length array allocations.
            Try
                RemoveHandler Me.SelectedChanged, CType(d, EventHandler(Of EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

    ''' <summary> Raises the selected changed event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub OnSelectedChanged()
        Dim evt As EventHandler(Of EventArgs) = Me.SelectedChangedEvent
        evt?.Invoke(Me, EventArgs.Empty)
        Windows.Forms.Application.DoEvents()
    End Sub

#End Region

End Class
