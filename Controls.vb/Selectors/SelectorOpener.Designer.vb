Imports System.Drawing
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class SelectorOpener

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me._ErrorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        Me._ToolStrip = New System.Windows.Forms.ToolStrip()
        Me._ClearButton = New isr.Core.Controls.ToolStripButton()
        Me._ResourceNamesComboBox = New isr.Core.Controls.ToolStripSpringComboBox()
        Me._ToggleOpenButton = New isr.Core.Controls.ToolStripButton()
        Me._EnumerateButton = New isr.Core.Controls.ToolStripButton()
        Me._OverflowLabel = New System.Windows.Forms.ToolStripLabel()
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._ToolStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_ErrorProvider
        '
        Me._ErrorProvider.ContainerControl = Me
        '
        '_ToolStrip
        '
        Me._ToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me._ToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ClearButton, Me._ResourceNamesComboBox, Me._ToggleOpenButton, Me._EnumerateButton, Me._OverflowLabel})
        Me._ToolStrip.Location = New System.Drawing.Point(0, 0)
        Me._ToolStrip.Name = "_ToolStrip"
        Me._ToolStrip.Size = New System.Drawing.Size(474, 29)
        Me._ToolStrip.TabIndex = 0
        Me._ToolStrip.Text = "Selector Opener"
        '
        '_ClearButton
        '
        Me._ClearButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._ClearButton.Image = Global.isr.Core.Controls.My.Resources.Resources.Clear_22x22
        Me._ClearButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me._ClearButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ClearButton.Name = "_ClearButton"
        Me._ClearButton.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me._ClearButton.Size = New System.Drawing.Size(26, 26)
        Me._ClearButton.Text = "Clear known state"
        '
        '_ResourceNamesComboBox
        '
        Me._ResourceNamesComboBox.Name = "_ResourceNamesComboBox"
        Me._ResourceNamesComboBox.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me._ResourceNamesComboBox.Size = New System.Drawing.Size(362, 29)
        Me._ResourceNamesComboBox.ToolTipText = "Available resource names"
        '
        '_ToggleOpenButton
        '
        Me._ToggleOpenButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._ToggleOpenButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._ToggleOpenButton.Image = Global.isr.Core.Controls.My.Resources.Resources.Disconnected_22x22
        Me._ToggleOpenButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me._ToggleOpenButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ToggleOpenButton.Name = "_ToggleOpenButton"
        Me._ToggleOpenButton.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me._ToggleOpenButton.Size = New System.Drawing.Size(26, 26)
        Me._ToggleOpenButton.ToolTipText = "Click to open"
        '
        '_EnumerateButton
        '
        Me._EnumerateButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._EnumerateButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._EnumerateButton.Image = Global.isr.Core.Controls.My.Resources.Resources.Find_22x22
        Me._EnumerateButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me._EnumerateButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._EnumerateButton.Name = "_EnumerateButton"
        Me._EnumerateButton.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me._EnumerateButton.Size = New System.Drawing.Size(26, 26)
        Me._EnumerateButton.ToolTipText = "Enumerate resources"
        '
        '_OverflowLabel
        '
        Me._OverflowLabel.Name = "_OverflowLabel"
        Me._OverflowLabel.Size = New System.Drawing.Size(0, 26)
        '
        'SelectorOpener
        '
        Me.BackColor = System.Drawing.Color.Transparent
        Me.Controls.Add(Me._ToolStrip)
        Me.Margin = New System.Windows.Forms.Padding(0)
        Me.Name = "SelectorOpener"
        Me.Size = New System.Drawing.Size(474, 29)
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).EndInit()
        Me._ToolStrip.ResumeLayout(False)
        Me._ToolStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _ErrorProvider As System.Windows.Forms.ErrorProvider
    Private WithEvents _ClearButton As isr.Core.Controls.ToolStripButton
    Private WithEvents _ResourceNamesComboBox As isr.Core.Controls.ToolStripSpringComboBox
    Private WithEvents _EnumerateButton As isr.Core.Controls.ToolStripButton
    Private WithEvents _ToggleOpenButton As isr.Core.Controls.ToolStripButton
    Private WithEvents _ToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _OverflowLabel As Windows.Forms.ToolStripLabel
End Class
