Imports System.ComponentModel

Imports isr.Core.WinForms.NumericUpDownExtensions

''' <summary> Selector numeric. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 3/6/2015 </para>
''' </remarks>
<Description("Selector Numeric"), System.Drawing.ToolboxBitmap(GetType(SelectorNumeric))>
<System.Runtime.InteropServices.ComVisible(False)>
Public Class SelectorNumeric

#Region " CONSTRUCTION "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()

        ' This call is required by the designer.
        Me.InitializeComponent()

        Me.SelectorButton = Me.Button
        Me.SelectorTextBox = Me.NumericUpDown

    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " EXPOSED PROPERTIES "

    ''' <summary> The watermark text. </summary>
    ''' <value> The water mark text with this control. </value>
    <DefaultValue(GetType(System.String), "Watermark"), Description("Watermark Text"), Category("Appearance")>
    Public Overrides Property Watermark As String
        Get
            Return MyBase.Watermark
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.Watermark) Then
                If System.Text.RegularExpressions.Regex.IsMatch(value, "^[0-9 ]+$") Then
                    MyBase.Watermark = value
                    MyBase.OnDirtyChanged()
                End If
            End If
        End Set
    End Property

    ''' <summary> Hexadecimal. </summary>
    ''' <value> <c>true</c> if hexadecimal; otherwise <c>false</c> </value>
    <DefaultValue(False)>
    <Description("Hexadecimal"), Category("Appearance")>
    Public Property Hexadecimal As Boolean
        Get
            Return Me.NumericUpDown.Hexadecimal
        End Get
        Set(value As Boolean)
            Me.NumericUpDown.Hexadecimal = value
        End Set
    End Property

    ''' <summary> Numeric Value. </summary>
    ''' <value> The Value associated with this control. </value>
    <DefaultValue(GetType(System.Decimal), "1")>
    <Description("Numeric Value"), Category("Appearance")>
    Public Property Value As Decimal
        Get
            Return Me.NumericUpDown.Value
        End Get
        Set(value As Decimal)
            Me.NumericUpDown.Value = value
        End Set
    End Property

    ''' <summary> Decimal Places. </summary>
    ''' <value> The number of decimal places. </value>
    <DefaultValue(GetType(System.Int32), "0")>
    <Description("Decimal Places"), Category("Appearance")>
    Public Property DecimalPlaces As Integer
        Get
            Return Me.NumericUpDown.DecimalPlaces
        End Get
        Set(value As Integer)
            Me.NumericUpDown.DecimalPlaces = value
        End Set
    End Property

    ''' <summary> Maximum numeric Value. </summary>
    ''' <value> The maximum value allowed with this control. </value>
    <DefaultValue(GetType(System.Decimal), "100")>
    <Description("Maximum Value"), Category("Appearance")>
    Public Property Maximum As Decimal
        Get
            Return Me.NumericUpDown.Maximum
        End Get
        Set(value As Decimal)
            Me.NumericUpDown.Maximum = value
        End Set
    End Property

    ''' <summary> Minimum numeric Value. </summary>
    ''' <value> The minimum value allowed with this control. </value>
    <DefaultValue(GetType(System.Decimal), "100")>
    <Description("Minimum Value"), Category("Appearance")>
    Public Property Minimum As Decimal
        Get
            Return Me.NumericUpDown.Minimum
        End Get
        Set(value As Decimal)
            Me.NumericUpDown.Minimum = value
        End Set
    End Property

    ''' <summary> Gets a value indicating whether this object has value. </summary>
    ''' <value> <c>true</c> if this object has value; otherwise <c>false</c> </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Overrides ReadOnly Property HasValue As Boolean
        Get
            Return Me.NumericUpDown.HasValue
        End Get
    End Property

    ''' <summary> Gets or sets the read only property. </summary>
    ''' <value> The read only. </value>
    <DefaultValue(False)>
    <Category("Behavior")>
    <Description("Indicates whether the check box is read only.")>
    Public Overrides Property [ReadOnly]() As Boolean
        Get
            Return Me.NumericUpDown.ReadOnly
        End Get
        Set(value As Boolean)
            Me.NumericUpDown.ReadOnly = value
            Me.Button.Visible = Not value
        End Set
    End Property

#End Region

#Region " CAPTION HANDLERS "

    ''' <summary> Gets the selected value. </summary>
    ''' <value> The selected value. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property SelectedValue As Decimal?

    ''' <summary> Gets a value indicating whether this object has a selected value. </summary>
    ''' <value> <c>true</c> if this object is new; otherwise <c>false</c> </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property HasSelectedValue As Boolean
        Get
            Return Me.SelectedValue.HasValue
        End Get
    End Property

    ''' <summary> Select value. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Overrides Sub SelectValue()
        Me.SelectedValue = Me.NumericUpDown.NullValue
        MyBase.SelectValue()
    End Sub

#End Region

#Region " SELECT VALUE "

    ''' <summary> Select text. </summary>
    ''' <remarks> David, 11/27/2015: Make the current selection current. </remarks>
    Public Overrides Sub SelectText()
        Me.SelectedValue = Me.NumericUpDown.NullValue
        MyBase.SelectText()
    End Sub

    ''' <summary> Select value. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The Value associated with this control. </param>
    Public Overloads Sub SelectValue(ByVal value As Decimal)
        Me.Value = value
        Me.SelectValue()
    End Sub

#End Region

#Region " EVENT HANDLERS "

    ''' <summary> Value changed. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub NumericUpDown_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles NumericUpDown.ValueChanged
        Me.OnDirtyChanged()
    End Sub

    ''' <summary> Text changed. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub NumericUpDown_NumericTextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles NumericUpDown.NumericTextChanged
        Me.OnDirtyChanged()
    End Sub

#End Region

#Region " VALUE SETTER "

    ''' <summary> value setter. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The Value associated with this control. </param>
    Public Sub ValueSetter(ByVal value As Decimal)
        Me.NumericUpDown.ValueSetter(value)
    End Sub

    ''' <summary> value setter. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The Value associated with this control. </param>
    Public Sub ValueSetter(ByVal value As Decimal?)
        Me.NumericUpDown.ValueSetter(value)
    End Sub

#End Region

End Class

