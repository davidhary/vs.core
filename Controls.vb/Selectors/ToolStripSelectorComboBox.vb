Imports System.Windows.Forms.Design
Imports System.ComponentModel

''' <summary> Tool strip selector combo box. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 3/2/2015 </para>
''' </remarks>
<ToolStripItemDesignerAvailability(ToolStripItemDesignerAvailability.ToolStrip)>
Public Class ToolStripSelectorComboBox
    Inherits ToolStripSelectorBase

    ''' <summary> Default constructor. </summary>
    ''' <remarks> Call the base constructor passing in a SelectorComboBox instance. </remarks>
    Public Sub New()
        MyBase.New(New SelectorComboBox())
    End Sub

#Region " UNDERLYING CONTROL PROPERTIES "

    ''' <summary> Gets the numeric up down control. </summary>
    ''' <value> The numeric up down control. </value>
    Public ReadOnly Property SelectorComboBoxControl() As SelectorComboBox
        Get
            Return TryCast(Me.Control, SelectorComboBox)
        End Get
    End Property

    ''' <summary> Gets the combo box. </summary>
    ''' <value> The combo box. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property ComboBox As isr.Core.Controls.ComboBox
        Get
            Return Me.SelectorComboBoxControl.ComboBox
        End Get
    End Property

    ''' <summary> Gets the button. </summary>
    ''' <value> The button. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Button As System.Windows.Forms.Button
        Get
            Return Me.SelectorComboBoxControl.Button
        End Get
    End Property

#End Region

#Region " UNDERLYING CONTROL METHODS "

    ''' <summary> Select value. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The value. </param>
    Public Overloads Sub SelectValue(ByVal value As String)
        Me.SelectorComboBoxControl.SelectValue(value)
    End Sub

#End Region

End Class

