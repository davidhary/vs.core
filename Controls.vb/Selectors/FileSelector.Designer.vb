<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FileSelector

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Friend _toolTip As System.Windows.Forms.ToolTip
    Private WithEvents _FilePathTextBox As System.Windows.Forms.TextBox
    Private WithEvents _BrowseButton As System.Windows.Forms.Button
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me._toolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._BrowseButton = New System.Windows.Forms.Button()
        Me._FilePathTextBox = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        '_BrowseButton
        '
        Me._BrowseButton.BackColor = System.Drawing.SystemColors.Control
        Me._BrowseButton.Cursor = System.Windows.Forms.Cursors.Default
        Me._BrowseButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._BrowseButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._BrowseButton.Location = New System.Drawing.Point(516, 1)
        Me._BrowseButton.Name = "_BrowseButton"
        Me._BrowseButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._BrowseButton.Size = New System.Drawing.Size(29, 22)
        Me._BrowseButton.TabIndex = 0
        Me._BrowseButton.Text = "..."
        Me._BrowseButton.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me._toolTip.SetToolTip(Me._BrowseButton, "Browses for a file")
        Me._BrowseButton.UseMnemonic = False
        Me._BrowseButton.UseVisualStyleBackColor = True
        '
        '_FilePathTextBox
        '
        Me._FilePathTextBox.AcceptsReturn = True
        Me._FilePathTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._FilePathTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._FilePathTextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._FilePathTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._FilePathTextBox.Location = New System.Drawing.Point(0, 0)
        Me._FilePathTextBox.MaxLength = 0
        Me._FilePathTextBox.Name = "_FilePathTextBox"
        Me._FilePathTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._FilePathTextBox.Size = New System.Drawing.Size(510, 25)
        Me._FilePathTextBox.TabIndex = 1
        '
        'FileSelector
        '
        Me.BackColor = System.Drawing.Color.Transparent
        Me.Controls.Add(Me._FilePathTextBox)
        Me.Controls.Add(Me._BrowseButton)
        Me.Name = "FileSelector"
        Me.Size = New System.Drawing.Size(547, 25)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

End Class
