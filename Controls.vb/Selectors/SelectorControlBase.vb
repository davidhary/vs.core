Imports System.ComponentModel

Imports isr.Core.Controls.ExceptionExtensions

''' <summary> Selector control base. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 3/6/2015 </para>
''' </remarks>
<DefaultEvent("ValueSelected")>
Public Class SelectorControlBase
    Inherits Forma.ModelViewBase

#Region " CONSTRUCTION "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()

        ' This call is required by the designer.
        Me.InitializeComponent()

        Me._SelectorButton = New System.Windows.Forms.Button
        Me._SelectorTextBox = New System.Windows.Forms.Control
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
                Me._SelectorButton?.Dispose() : Me._SelectorButton = Nothing
                Me._SelectorTextBox?.Dispose() : Me._SelectorTextBox = Nothing
                Me.RemoveDirtyChangedEventHandler(Me.DirtyChangedEvent)
                Me.RemoveValueSelectedEventHandler(Me.ValueSelectedEvent)
                Me.RemoveValueSelectingEventHandler(Me.ValueSelectingEvent)
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " BASE CONTROLS "

    ''' <summary> The with events control. </summary>
#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _SelectorButton As System.Windows.Forms.Button
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets or sets the selector button. </summary>
    ''' <value> The selector button. </value>
    Protected Property SelectorButton As System.Windows.Forms.Button
        Get
            Return Me._SelectorButton
        End Get
        Set(value As System.Windows.Forms.Button)
            Me._SelectorButton = value
        End Set
    End Property

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _SelectorTextBox As System.Windows.Forms.Control
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets or sets the selector text box. </summary>
    ''' <value> The selector text box. </value>
    Protected Property SelectorTextBox As System.Windows.Forms.Control
        Get
            Return Me._SelectorTextBox
        End Get
        Set(value As System.Windows.Forms.Control)
            Me._SelectorTextBox = value
            Me._IsDirty = Not Me.IsDirty
        End Set
    End Property


#End Region

#Region " EXPOSED PROPERTIES "

    ''' <summary> The watermark. </summary>
    Private _Watermark As String

    ''' <summary> The watermark text. </summary>
    ''' <value> The water mark text with this control. </value>
    <DefaultValue(GetType(System.String), "Watermark")>
    <Description("Watermark Text"), Category("Appearance")>
    Public Overridable Property Watermark As String
        Get
            Return Me._Watermark
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.Watermark) Then
                Me._Watermark = value
                Me.OnDirtyChangedThis()
            End If
        End Set
    End Property

    ''' <summary> Combo box text. </summary>
    ''' <value> The text associated with this control. </value>
    <DefaultValue(GetType(System.String), "")>
    <Description("Combo box text"), Category("Appearance")>
    Public Overrides Property Text As String
        Get
            Return Me.SelectorTextBox.Text
        End Get
        Set(value As String)
            Me.SelectorTextBox.Text = value
        End Set
    End Property

    ''' <summary> Selector icon image. </summary>
    ''' <value> The selector icon. </value>
    <Description("Selector icon image"), Category("Appearance")>
    Public Property SelectorIcon As Drawing.Image
        Get
            Return Me.SelectorButton.Image
        End Get
        Set(value As Drawing.Image)
            Me.SelectorButton.Image = value
        End Set
    End Property

    ''' <summary> Gets or sets the read only property. </summary>
    ''' <value> The read only. </value>
    <DefaultValue(False)>
    <Category("Behavior")>
    <Description("Indicates whether the selector is read only.")>
    Public Overridable Property [ReadOnly] As Boolean

    ''' <summary> Indicates whether an empty value can be selected. </summary>
    ''' <value> The can select empty value. </value>
    <DefaultValue(False)>
    <Category("Behavior")>
    <Description("Indicates whether an empty value can be selected.")>
    Public Property CanSelectEmptyValue As Boolean

#End Region

#Region " CAPTION HANDLERS "

    ''' <summary> Gets the selected text. </summary>
    ''' <value> The selected text. </value>
    <DefaultValue(""),
        Description("Selected text"), Category("Appearance"),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property SelectedText As String

    ''' <summary> The previous selected text. </summary>
    Private _PreviousSelectedText As String

    ''' <summary> Gets the previous selected text. </summary>
    ''' <value> The previous selected text. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property PreviousSelectedText As String
        Get
            Return Me._PreviousSelectedText
        End Get
    End Property

    ''' <summary> Gets a value indicating whether a new value was selected. </summary>
    ''' <value> <c>true</c> if a new value was selected; otherwise <c>false</c> </value>
    Public ReadOnly Property IsSelectedNew As Boolean
        Get
            Return Not String.Equals(Me.PreviousSelectedText, Me.SelectedText, StringComparison.OrdinalIgnoreCase)
        End Get
    End Property

    ''' <summary> Gets a value indicating whether the selector text is empty. </summary>
    ''' <value> <c>true</c> if the selector test is empty; otherwise <c>false</c> </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Overridable ReadOnly Property IsEmpty As Boolean
        Get
            Return String.IsNullOrWhiteSpace(Me.Text)
        End Get
    End Property

    ''' <summary> Gets a value indicating whether this object has value. </summary>
    ''' <value> <c>true</c> if this object has value; otherwise <c>false</c> </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Overridable ReadOnly Property HasValue As Boolean
        Get
            Return Not String.IsNullOrWhiteSpace(Me.Text)
        End Get
    End Property

    ''' <summary> True if is dirty, false if not. </summary>
    Private _IsDirty As Boolean

    ''' <summary> Gets a value indicating whether this object is dirty. </summary>
    ''' <value> <c>true</c> if this object is dirty; otherwise <c>false</c> </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property IsDirty As Boolean
        Get
            Return Not String.Equals(Me.SelectedText, Me.Text)
        End Get
    End Property

    ''' <summary> Executes the dirty changed action. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Sub OnDirtyChangedThis()
        If String.IsNullOrEmpty(Me.Text) AndAlso Not String.IsNullOrWhiteSpace(Me.Watermark) Then
            Me.Text = Me.Watermark
        End If
        If Not String.IsNullOrEmpty(Me.Text) AndAlso Not String.IsNullOrWhiteSpace(Me.Watermark) AndAlso
            String.Equals(Me.Text, Me.Watermark) Then
            Me.SelectorTextBox.ForeColor = SystemColors.InactiveCaption
        ElseIf Me.IsDirty <> Me._IsDirty Then
            Me._IsDirty = Me.IsDirty
            Dim evt As EventHandler(Of System.EventArgs) = Me.DirtyChangedEvent
            evt?.Invoke(Me, System.EventArgs.Empty)
        End If
        If Not String.IsNullOrEmpty(Me.Text) AndAlso Not String.IsNullOrWhiteSpace(Me.Watermark) AndAlso
            String.Equals(Me.Text, Me.Watermark) Then
            Me.SelectorTextBox.ForeColor = SystemColors.InactiveCaption
        ElseIf Not Me.Enabled Then
            Me.SelectorButton.BackColor = Me.BackColor
            Me.SelectorTextBox.ForeColor = SystemColors.InactiveCaption
        ElseIf Not Me.IsDirty AndAlso Me.SelectorTextBox.ForeColor <> Me.ForeColor Then
            Me.SelectorButton.BackColor = Me.BackColor
            Me.SelectorTextBox.ForeColor = Me.ForeColor
        ElseIf Me.IsDirty AndAlso Me.SelectorTextBox.ForeColor <> Me._DirtyForeColor Then
            Me.SelectorButton.BackColor = Me._DirtyBackColor
            Me.SelectorTextBox.ForeColor = Me._DirtyForeColor
        End If
        Me.OnContentChanged()
    End Sub

    ''' <summary> Executes the content changed action. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Protected Overridable Sub OnContentChanged()
        If Me.SelectorButton.Enabled <> Me.CanSelectItem Then
            Me.SelectorButton.Enabled = Me.CanSelectItem
            If Not Me.SelectorButton.Enabled Then
                Me.SelectorButton.BackColor = Me.BackColor
                Me.SelectorTextBox.ForeColor = SystemColors.InactiveCaption
            End If
        End If
    End Sub

    ''' <summary> Determine if we can select item. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> <c>true</c> if we can select item; otherwise <c>false</c> </returns>
    Protected Overridable Function CanSelectItem() As Boolean
        Return Me.CanSelectEmptyValue OrElse Me.HasValue
    End Function

    ''' <summary> Executes the dirty changed action. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Overridable Sub OnDirtyChanged()
        Me.OnDirtyChangedThis()
    End Sub

#End Region

#Region " COLORS "

    ''' <summary> The dirty back color. </summary>
    Private _DirtyBackColor As Color

    ''' <summary> Gets or sets the color of the read only back. </summary>
    ''' <value> The color of the read only back. </value>
    <DefaultValue(GetType(Drawing.Color), "SystemColors.orange")>
    <Description("Back color when dirty"), Category("Appearance")>
    Public Property DirtyBackColor() As Color
        Get
            If Me._DirtyBackColor.IsEmpty OrElse Me._DirtyBackColor = Me.BackColor Then
                Me._DirtyBackColor = Color.Orange
            End If
            Return Me._DirtyBackColor
        End Get
        Set(value As Color)
            Me._DirtyBackColor = value
        End Set
    End Property

    ''' <summary> The dirty foreground color. </summary>
    Private _DirtyForeColor As Color

    ''' <summary> Gets or sets the color of the read only foreground. </summary>
    ''' <value> The color of the read only foreground. </value>
    <DefaultValue(GetType(Drawing.Color), "SystemColors.ActiveCaption")>
    <Description("Fore color when dirty"), Category("Appearance")>
    Public Property DirtyForeColor() As Color
        Get
            If Me._DirtyForeColor.IsEmpty OrElse Me._DirtyForeColor = Me.ForeColor Then
                Me._DirtyForeColor = SystemColors.ActiveCaption
            End If
            Return Me._DirtyForeColor
        End Get
        Set(value As Color)
            Me._DirtyForeColor = value
        End Set
    End Property

#End Region

#Region " SELECT FUNCTION "

    ''' <summary> Select text. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Sub SelectTextThis()
        Me._PreviousSelectedText = Me.SelectedText
        Me.SelectedText = Me.SelectorTextBox.Text
        Me.OnDirtyChanged()
    End Sub

    ''' <summary> Select text. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Overridable Sub SelectText()
        Me.SelectTextThis()
    End Sub

    ''' <summary> Select value. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Sub SelectValueThis()
        Dim e As New CancelEventArgs
        Me.OnValueSelecting(Me, e)
        If e IsNot Nothing AndAlso Not e.Cancel Then
            Me.SelectTextThis()
            Me.OnValueSelected(Me, System.EventArgs.Empty)
        End If
    End Sub

    ''' <summary> Select value. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Overridable Sub SelectValue()
        Me.SelectValueThis()
    End Sub

#End Region

#Region " EVENT HANDLERS "

    ''' <summary> Combo box text changed. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SelectorTextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _SelectorTextBox.TextChanged
        Me.OnDirtyChanged()
    End Sub

    ''' <summary> Select value. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    Private Sub SelectValue(ByVal sender As System.Windows.Forms.Control)
        Dim cursor As System.Windows.Forms.Cursor = sender.Cursor
        Try
            sender.Cursor = System.Windows.Forms.Cursors.WaitCursor
            Me.SelectValue()
        Finally
            sender.Cursor = cursor
        End Try
    End Sub

    ''' <summary> Select button click. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SelectorButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _SelectorButton.Click
        Dim ctrl As System.Windows.Forms.Control = TryCast(sender, System.Windows.Forms.Control)
        If ctrl Is Nothing Then
            Me.SelectValue()
        Else
            Me.SelectValue(ctrl)
        End If
    End Sub

    ''' <summary> Event queue for all listeners interested in Value-Selected events. </summary>
    Public Event DirtyChanged As EventHandler(Of System.EventArgs)

    ''' <summary> Removes event handler. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The handler. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub RemoveDirtyChangedEventHandler(ByVal value As EventHandler(Of EventArgs))
#Disable Warning CA1825 ' Avoid zero-length array allocations.
        For Each d As [Delegate] In If(value Is Nothing, New [Delegate]() {}, value.GetInvocationList)
#Enable Warning CA1825 ' Avoid zero-length array allocations.
            Try
                RemoveHandler Me.DirtyChanged, CType(d, EventHandler(Of EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

    ''' <summary> Event queue for all listeners interested in Value-Selecting events. </summary>
    Public Event ValueSelecting As EventHandler(Of System.ComponentModel.CancelEventArgs)

    ''' <summary> Removes event handler. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The handler. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub RemoveValueSelectingEventHandler(ByVal value As EventHandler(Of CancelEventArgs))
#Disable Warning CA1825 ' Avoid zero-length array allocations.
        For Each d As [Delegate] In If(value Is Nothing, New [Delegate]() {}, value.GetInvocationList)
#Enable Warning CA1825 ' Avoid zero-length array allocations.
            Try
                RemoveHandler Me.ValueSelecting, CType(d, EventHandler(Of CancelEventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

    ''' <summary> Raises the Value-Selecting event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information to send to registered event handlers. </param>
    Public Overridable Sub OnValueSelecting(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
        Dim evt As EventHandler(Of System.ComponentModel.CancelEventArgs) = Me.ValueSelectingEvent
        evt?.Invoke(Me, e)
    End Sub

    ''' <summary> Event queue for all listeners interested in Value-Selected events. </summary>
    Public Event ValueSelected As EventHandler(Of System.EventArgs)

    ''' <summary> Removes event handler. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The handler. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub RemoveValueSelectedEventHandler(ByVal value As EventHandler(Of EventArgs))
#Disable Warning CA1825 ' Avoid zero-length array allocations.
        For Each d As [Delegate] In If(value Is Nothing, New [Delegate]() {}, value.GetInvocationList)
#Enable Warning CA1825 ' Avoid zero-length array allocations.
            Try
                RemoveHandler Me.ValueSelected, CType(d, EventHandler(Of EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

    ''' <summary> Raises the Value-Selected event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information to send to registered event handlers. </param>
    Public Overridable Sub OnValueSelected(ByVal sender As Object, ByVal e As EventArgs)
        Dim evt As EventHandler(Of System.EventArgs) = Me.ValueSelectedEvent
        evt?.Invoke(Me, e)
    End Sub

#End Region

End Class
