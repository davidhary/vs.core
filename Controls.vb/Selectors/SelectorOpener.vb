Imports System.ComponentModel
Imports isr.Core.Models
Imports isr.Core.Forma
Imports isr.Core.Controls.ExceptionExtensions
Imports isr.Core.WinForms.ErrorProviderExtensions

''' <summary> A control for selecting and opening a resource defined by a resource name. </summary>
''' <remarks>
''' David, 02/20/2006, 1.0.2242.x <para>
''' David, 12/11/2018, 3.4.6819.x. from VI resource selector connector. </para><para>
''' (c) 2006 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License. </para>
''' </remarks>
<Description("Resource Selector and Opener Base Control")>
<System.Drawing.ToolboxBitmap(GetType(SelectorOpener), "SelectorOpener"), ToolboxItem(True)>
Public Class SelectorOpener
    Inherits ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False
        Me._ClearButton.Visible = True
        Me._ToggleOpenButton.Visible = True
        Me._EnumerateButton.Visible = True
        Me._ToggleOpenButton.Enabled = False
        Me._ClearButton.Enabled = False
        Me._EnumerateButton.Enabled = True
        AddHandler Me._ResourceNamesComboBox.ComboBox.SelectedValueChanged, AddressOf Me.ResourceNamesComboBoxValueChanged
        AddHandler Me._ResourceNamesComboBox.ComboBox.Validated, AddressOf Me.ResourceNamesComboBoxValueChanged
    End Sub

    ''' <summary> Creates a new <see cref="SelectorOpener"/> </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> A <see cref="SelectorOpener"/>. </returns>
    Public Shared Function Create() As SelectorOpener
        Dim selectorOpener As SelectorOpener = Nothing
        Try
            selectorOpener = New SelectorOpener
            Return selectorOpener
        Catch
            selectorOpener.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.InitializingComponents = True
                ' make sure the views are unbound in case the form is closed without closing the views.
                Me.AssignSelectorViewModel(Nothing)
                Me.AssignOpenerViewModel(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " BROWSABLE PROPERTIES "

    ''' <summary>
    ''' Gets or sets the value indicating if the clear button is visible and can be enabled. An item
    ''' can be cleared only if it is open.
    ''' </summary>
    ''' <value> The clearable. </value>
    <Category("Appearance"), Description("Shows or hides the Clear button"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue(True)>
    Public Property Clearable() As Boolean
        Get
            Return Me._ClearButton.Visible
        End Get
        Set(ByVal value As Boolean)
            If Not Me.Clearable.Equals(value) Then
                Me._ClearButton.Visible = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the value indicating if the open button is visible and can be enabled. An item
    ''' can be opened only if it is validated.
    ''' </summary>
    ''' <value> The openable state. </value>
    <Category("Appearance"), Description("Shows or hides the open button"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
    RefreshProperties(RefreshProperties.All), DefaultValue(True)>
    Public Property Openable() As Boolean
        Get
            Return Me._ToggleOpenButton.Visible
        End Get
        Set(ByVal value As Boolean)
            If Not Me.Openable.Equals(value) Then
                Me._ToggleOpenButton.Visible = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the condition determining if the control can be searchable. The elements can be
    ''' searched only if not open.
    ''' </summary>
    ''' <value> The searchable. </value>
    <Category("Appearance"), Description("Shows or hides the Search (Find) button"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue(True)>
    Public Property Searchable() As Boolean
        Get
            Return Me._EnumerateButton.Visible
        End Get
        Set(ByVal value As Boolean)
            If Not Me.Searchable.Equals(value) Then
                Me._EnumerateButton.Visible = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " SELECTOR VIEW MODEL "

    ''' <summary> Gets the selector view model. </summary>
    ''' <value> The selector view model. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property SelectorViewModel As SelectorViewModel

    ''' <summary> Assign the selector view model. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="viewModel"> The view model. </param>
    Public Sub AssignSelectorViewModel(ByVal viewModel As SelectorViewModel)
        If Me.SelectorViewModel IsNot Nothing Then
            Me.BindEnumerateButton(False, Me.SelectorViewModel)
            Me.BindResourceNamesCombo(False, Me.SelectorViewModel)
            Me._SelectorViewModel = Nothing
        End If
        If viewModel IsNot Nothing Then
            Me._SelectorViewModel = viewModel
            Me.SelectorViewModel.SearchImage = My.Resources.Find_22x22
            Me.BindEnumerateButton(True, Me.SelectorViewModel)
            Me.BindResourceNamesCombo(True, Me.SelectorViewModel)
        End If
    End Sub

    ''' <summary> Bind enumerate button. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="add">       True to add; otherwise, remove. </param>
    ''' <param name="viewModel"> The view model. </param>
    Private Sub BindEnumerateButton(ByVal add As Boolean, ByVal viewModel As SelectorViewModel)
        Dim binding As Binding = Me.AddRemoveBinding(Me._EnumerateButton, add, NameOf(ToolStripButton.Visible), viewModel, NameOf(Models.SelectorViewModel.Searchable))
        binding.DataSourceUpdateMode = DataSourceUpdateMode.Never

        binding = Me.AddRemoveBinding(Me._EnumerateButton, add, NameOf(ToolStripButton.Enabled), viewModel, NameOf(Models.SelectorViewModel.SearchEnabled))
        binding.DataSourceUpdateMode = DataSourceUpdateMode.Never

        binding = Me.AddRemoveBinding(Me._EnumerateButton, add, NameOf(ToolStripButton.ToolTipText), viewModel, NameOf(Models.SelectorViewModel.SearchToolTip))
        binding.DataSourceUpdateMode = DataSourceUpdateMode.Never

        binding = Me.AddRemoveBinding(Me._EnumerateButton, add, NameOf(ToolStripButton.Image), viewModel, NameOf(Models.SelectorViewModel.SearchImage))
        binding.DataSourceUpdateMode = DataSourceUpdateMode.Never
    End Sub

    ''' <summary> Bind resource names combo. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="add">       True to add; otherwise, remove. </param>
    ''' <param name="viewModel"> The view model. </param>
    Private Sub BindResourceNamesCombo(ByVal add As Boolean, ByVal viewModel As SelectorViewModel)
        Dim binding As Binding = Me.AddRemoveBinding(Me._ResourceNamesComboBox.ComboBox, add, NameOf(ComboBox.Enabled), viewModel, NameOf(Models.SelectorViewModel.SearchEnabled))
        binding.DataSourceUpdateMode = DataSourceUpdateMode.Never

        Me._ResourceNamesComboBox.ComboBox.DataSource = If(add, viewModel.ResourceNames, Nothing)
        ' this binding has caused cross thread exceptions. occasionally. even though the binding is set from the control using the thread safe property change manager.
        Me.AddRemoveBinding(Me._ResourceNamesComboBox.ComboBox, add, NameOf(ComboBox.Text), viewModel, NameOf(Models.SelectorViewModel.CandidateResourceName))
    End Sub

    ''' <summary> Attempts to validate the specified resource name. </summary>
    ''' <remarks> David, 2020-07-20. </remarks>
    ''' <param name="resourceName"> The value. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Function TryValidateResourceName(ByVal resourceName As String) As (Success As Boolean, Details As String)
        If String.IsNullOrWhiteSpace(resourceName) Then resourceName = String.Empty
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        If String.Equals(Me.OpenerViewModel.ValidatedResourceName, resourceName, StringComparison.OrdinalIgnoreCase) Then Return result
        Dim activity As String = String.Empty
        Dim sender As ToolStripSpringComboBox = Me._ResourceNamesComboBox
        Try
            Me._ErrorProvider.Clear()
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            activity = $"validating {resourceName}" : Me.PublishInfo($"{activity};. ")
            result = Me.SelectorViewModel.TryValidateResource(resourceName)
            If result.Success Then
                If Not String.IsNullOrEmpty(result.Details) Then activity = Me.PublishInfo($"{activity};. reported {result.Details}")
                ' this enables opening 
                If Me.OpenerViewModel IsNot Nothing Then Me.OpenerViewModel.ValidatedResourceName = Me.SelectorViewModel.ValidatedResourceName
            Else
                Me._ErrorProvider.Annunciate(sender, result.Details)
                activity = Me.PublishWarning($"Failed {activity};. {result.Details}") : Me.PublishWarning($"{activity};. ")
                ' this disables opening 
                If Me.OpenerViewModel IsNot Nothing Then Me.OpenerViewModel.ValidatedResourceName = String.Empty
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.Message)
            result = (False, Me.PublishException(activity, ex))
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
        Return result
    End Function

    ''' <summary> Gets the number of selected value changes. </summary>
    ''' <value> The number of selected value changes. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property SelectedValueChangeCount As Integer

    ''' <summary> Event handler called by the validated and selected index changed events. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ResourceNamesComboBoxValueChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim control As ToolStripSpringComboBox = Me._ResourceNamesComboBox ' TryCast(sender, ToolStripSpringComboBox)
        If Not (control Is Nothing OrElse String.IsNullOrWhiteSpace(control.Text)) Then
            Me.SelectedValueChangeCount += 1
            Me.TryValidateResourceName(control.Text)
        End If
    End Sub

    ''' <summary> Event handler. Called by _EnumerateButton for click events. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub EnumerateButtonClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _EnumerateButton.Click
        Dim activity As String = String.Empty
        Try
            Me._ErrorProvider.Clear()
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            activity = "Enumerating resources" : Me.PublishInfo($"{activity};. ")
            ' enumerate resource with filtering when enumerating from the GUI.
            Me.SelectorViewModel.EnumerateResources(True)
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.Message)
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

#End Region

#Region " OPENER VIEW MODEL "

    ''' <summary> Gets the opener view model. </summary>
    ''' <value> The opener view model. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property OpenerViewModel As OpenerViewModel

    ''' <summary> Assigns the opener view model. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="viewModel"> The view model. </param>
    Public Sub AssignOpenerViewModel(ByVal viewModel As OpenerViewModel)
        If Me.OpenerViewModel IsNot Nothing Then
            Me.BindClearButton(False, Me.OpenerViewModel)
            Me.BindOpenButton(False, Me.OpenerViewModel)
            Me._OpenerViewModel = Nothing
        End If
        If viewModel IsNot Nothing Then
            Me._OpenerViewModel = viewModel
            Me.OpenerViewModel.OpenedImage = My.Resources.Connected_22x22
            Me.OpenerViewModel.ClosedImage = My.Resources.Disconnected_22x22
            Me.OpenerViewModel.ClearImage = My.Resources.Clear_22x22
            Me.BindClearButton(True, Me.OpenerViewModel)
            Me.BindOpenButton(True, Me.OpenerViewModel)
        End If
    End Sub

    ''' <summary> Binds the Clear button. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="add">       True to add. </param>
    ''' <param name="viewModel"> The view model. </param>
    Private Sub BindClearButton(ByVal add As Boolean, ByVal viewModel As OpenerViewModel)
        Dim binding As Binding = Me.AddRemoveBinding(Me._ClearButton, add, NameOf(ToolStripButton.Enabled), viewModel, NameOf(Models.OpenerViewModel.IsOpen))
        binding.DataSourceUpdateMode = DataSourceUpdateMode.Never

        binding = Me.AddRemoveBinding(Me._ClearButton, add, NameOf(ToolStripButton.Visible), viewModel, NameOf(Models.OpenerViewModel.Clearable))
        binding.DataSourceUpdateMode = DataSourceUpdateMode.Never

        binding = Me.AddRemoveBinding(Me._ClearButton, add, NameOf(ToolStripButton.ToolTipText), viewModel, NameOf(Models.OpenerViewModel.ClearToolTip))
        binding.DataSourceUpdateMode = DataSourceUpdateMode.Never
    End Sub

    ''' <summary> Bind open button. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="add">       True to add; otherwise, remove. </param>
    ''' <param name="viewModel"> The view model. </param>
    Private Sub BindOpenButton(ByVal add As Boolean, ByVal viewModel As OpenerViewModel)
        Dim binding As Binding = Me.AddRemoveBinding(Me._ToggleOpenButton, add, NameOf(ToolStripButton.Visible), viewModel, NameOf(Models.OpenerViewModel.Openable))
        binding.DataSourceUpdateMode = DataSourceUpdateMode.Never

        binding = Me.AddRemoveBinding(Me._ToggleOpenButton, add, NameOf(ToolStripButton.Enabled), viewModel, NameOf(Models.OpenerViewModel.OpenEnabled))
        binding.DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged
        binding.ControlUpdateMode = ControlUpdateMode.OnPropertyChanged

        binding = Me.AddRemoveBinding(Me._ToggleOpenButton, add, NameOf(ToolStripButton.ToolTipText), viewModel, NameOf(Models.OpenerViewModel.OpenToolTip))
        binding.DataSourceUpdateMode = DataSourceUpdateMode.Never

        binding = Me.AddRemoveBinding(Me._ToggleOpenButton, add, NameOf(ToolStripButton.Image), viewModel, NameOf(Models.OpenerViewModel.OpenImage))
        binding.DataSourceUpdateMode = DataSourceUpdateMode.Never

        viewModel.NotifyOpenChanged()
    End Sub

    ''' <summary> Executes the toggle open  action. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ToggleOpen(ByVal sender As ToolStripItem)
        If sender Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me._ErrorProvider.Clear()
            Dim e As New ActionEventArgs
            If Me.OpenerViewModel.IsOpen Then
                activity = $"closing {Me.OpenerViewModel.ResourceNameCaption}" : Me.PublishInfo($"{activity};. ")
                Me.OpenerViewModel.TryClose(e)
            Else
                activity = $"opening {Me.OpenerViewModel.ResourceTitleCaption}" : Me.PublishInfo($"{activity};. ")
                Me.OpenerViewModel.TryOpen(e)
            End If
            If e.Failed Then
                Me._ErrorProvider.Annunciate(sender, Me.Publish(e))
            Else
                ' this ensures that the control refreshes.
                Me._OverflowLabel.Text = "."
                Windows.Forms.Application.DoEvents()
                Me._OverflowLabel.Text = String.Empty
                Windows.Forms.Application.DoEvents()
                activity = $"{Me.OpenerViewModel.ResourceNameCaption} is {If(Me.OpenerViewModel.IsOpen, "open", "close")}" : Me.PublishInfo($"{activity};. ")
            End If
            ' this disables search when the resource is open
            If Me.SelectorViewModel IsNot Nothing Then Me.SelectorViewModel.IsOpen = Me.OpenerViewModel.IsOpen
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.Message)
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Toggle open button click. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> <see cref="Object"/> instance of this
    '''                                             <see cref="Control"/> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ToggleOpenButtonClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ToggleOpenButton.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.ToggleOpen(TryCast(sender, ToolStripItem))
    End Sub

    ''' <summary> Requests a clear. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ClearButtonClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ClearButton.Click
        Dim activity As String = String.Empty
        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            Me._ErrorProvider.Clear()
            activity = $"checking {Me.OpenerViewModel.ResourceNameCaption} open status"
            If Me.OpenerViewModel.IsOpen Then
                Dim args As New ActionEventArgs
                activity = $"clearing {Me.OpenerViewModel.ResourceNameCaption}"
                If Not Me.OpenerViewModel.TryClearActiveState(args) Then
                    Me._ErrorProvider.Annunciate(sender, Me.Publish(args))
                End If
            Else
                Me._ErrorProvider.Annunciate(sender, Me.PublishWarning($"failed {activity}; not open"))
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.Message)
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    ''' <summary> Event handler. Called by _ToolStrip for double click events. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ToolStripDoubleClick(sender As Object, e As EventArgs) Handles _ToolStrip.DoubleClick, _ResourceNamesComboBox.DoubleClick
        ' this ensures that the control refreshes.
        Me._OverflowLabel.Text = "."
        Windows.Forms.Application.DoEvents()
        Me._OverflowLabel.Text = String.Empty
        Windows.Forms.Application.DoEvents()
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Protected Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

#Region " UNIT TESTS INTERNALS "

    ''' <summary> Gets the number of internal resource names. </summary>
    ''' <value> The number of internal resource names. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Friend ReadOnly Property InternalResourceNamesCount As Integer
        Get
            Return If(Me._ResourceNamesComboBox.ComboBox Is Nothing, 0, Me._ResourceNamesComboBox.Items.Count)
        End Get
    End Property

    ''' <summary> Gets the name of the internal selected resource. </summary>
    ''' <value> The name of the internal selected resource. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Friend ReadOnly Property InternalSelectedResourceName As String
        Get
            Return If(Me._ResourceNamesComboBox.ComboBox Is Nothing, String.Empty, Me._ResourceNamesComboBox.Text)
        End Get
    End Property

#End Region

End Class
