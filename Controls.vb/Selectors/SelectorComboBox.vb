﻿Imports System.ComponentModel

''' <summary> Selector combo box. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 3/6/2015 </para>
''' </remarks>
<Description("Selector Combo Box"), System.Drawing.ToolboxBitmap(GetType(SelectorComboBox))>
<System.Runtime.InteropServices.ComVisible(False)>
Public Class SelectorComboBox
    Inherits SelectorControlBase

#Region " CONSTRUCTION "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()

        ' This call is required by the designer.
        Me.InitializeComponent()

        Me.SelectorButton = Me.Button
        Me.SelectorTextBox = Me.ComboBox
        MyBase.OnDirtyChanged()
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the isr.Core.Controls.SelectorComboBox and
    ''' optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " EXPOSED PROPERTIES "

    ''' <summary> Gets or sets the read only property. </summary>
    ''' <value> The read only. </value>
    <DefaultValue(False)>
    <Category("Behavior")>
    <Description("Indicates whether the check box is read only.")>
    Public Overrides Property [ReadOnly]() As Boolean
        Get
            Return Me.ComboBox.ReadOnly
        End Get
        Set(value As Boolean)
            MyBase.ReadOnly = value
            Me.ComboBox.ReadOnly = value
            Me.Button.Visible = Not value
        End Set
    End Property

    ''' <summary> Gets or sets the enabled. </summary>
    ''' <remarks>
    ''' Had some issues. Setting enabled on the control toggles the combo box. But if the combo
    ''' enabled is set, the link breaks.
    ''' </remarks>
    ''' <value> The enabled. </value>
    Public Overloads Property Enabled As Boolean
        Get
            Return MyBase.Enabled
        End Get
        Set(value As Boolean)
            If MyBase.Enabled <> value Then
                MyBase.Enabled = value
            End If
            If Me.ComboBox.Enabled <> value Then
                Me.ComboBox.Enabled = value
            End If
        End Set
    End Property

#End Region

#Region " CAPTION HANDLERS "

    ''' <summary> Gets the selected item. </summary>
    ''' <value> The selected item. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property SelectedItem As Object

    ''' <summary> Gets the selected value. </summary>
    ''' <value> The selected value. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property SelectedValue As Object

    ''' <summary> Gets a value indicating whether a value was selected. </summary>
    ''' <value> <c>true</c> if the selector has a selected value; otherwise <c>false</c> </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property HasSelectedValue As Boolean
        Get
            Return Me.SelectedValue IsNot Nothing
        End Get
    End Property

#End Region

#Region " SELECT VALUE "

    ''' <summary> Select value. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The value. </param>
    Public Overloads Sub SelectValue(ByVal value As String)
        Me.Text = value
        Me.SelectValue()
    End Sub

    ''' <summary> Searches for the first match for the given string. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> An Integer. </returns>
    Public Function FindIndex(ByVal value As String) As Integer
        Return Me.ComboBox.FindStringExact(value)
    End Function

    ''' <summary> Displays an existing value described by value. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub DisplayExistingValue(ByVal value As String)
        Me.Text = If(Me.ComboBox.FindString(value) >= 0, value, Me.Watermark)
    End Sub

#End Region

#Region " EVENT HANDLERS "

    ''' <summary> Combo box data source changed. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ComboBox_DataSourceChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox.DataSourceChanged
        Me.OnContentChanged()
    End Sub

    ''' <summary> Combo box selected value changed. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ComboBox_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox.SelectedValueChanged
        Me.OnDirtyChanged()
    End Sub

#End Region

End Class
