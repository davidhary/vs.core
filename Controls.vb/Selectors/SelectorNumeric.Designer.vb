﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SelectorNumeric
    Inherits SelectorControlBase

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button = New System.Windows.Forms.Button()
        Me.NumericUpDown = New isr.Core.Controls.NumericUpDown()
        CType(Me.NumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button
        '
        Me.Button.Dock = System.Windows.Forms.DockStyle.Right
        Me.Button.Image = Global.isr.Core.Controls.My.Resources.Resources.dialog_ok_4
        Me.Button.Location = New System.Drawing.Point(49, 0)
        Me.Button.MaximumSize = New System.Drawing.Size(25, 25)
        Me.Button.Name = "Button"
        Me.Button.Size = New System.Drawing.Size(25, 25)
        Me.Button.TabIndex = 1
        Me.Button.UseVisualStyleBackColor = True
        '
        'NumericUpDown
        '
        Me.NumericUpDown.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.NumericUpDown.Location = New System.Drawing.Point(0, 0)
        Me.NumericUpDown.Name = "NumericUpDown"
        Me.NumericUpDown.NullValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.NumericUpDown.ReadOnlyBackColor = System.Drawing.SystemColors.Control
        Me.NumericUpDown.ReadOnlyForeColor = System.Drawing.SystemColors.WindowText
        Me.NumericUpDown.ReadWriteBackColor = System.Drawing.SystemColors.Window
        Me.NumericUpDown.ReadWriteForeColor = System.Drawing.SystemColors.ControlText
        Me.NumericUpDown.Size = New System.Drawing.Size(49, 25)
        Me.NumericUpDown.TabIndex = 0
        Me.NumericUpDown.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'SelectorNumeric
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.NumericUpDown)
        Me.Controls.Add(Me.Button)
        Me.Name = "SelectorNumeric"
        Me.Size = New System.Drawing.Size(74, 25)
        CType(Me.NumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Public WithEvents Button As System.Windows.Forms.Button
    Public WithEvents NumericUpDown As isr.Core.Controls.NumericUpDown

End Class
