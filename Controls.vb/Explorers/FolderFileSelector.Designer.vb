<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FolderFileSelector
    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FolderFileSelector))
        Me._FileListBoxLabel = New System.Windows.Forms.Label()
        Me._FileNamePatternCheckBoxLabel = New System.Windows.Forms.Label()
        Me._FolderTextBoxLabel = New System.Windows.Forms.Label()
        Me._FilePathNameTextBoxLabel = New System.Windows.Forms.Label()
        Me._FilePathNameTextBox = New System.Windows.Forms.TextBox()
        Me._SelectFolderButton = New System.Windows.Forms.Button()
        Me._FolderTextBox = New System.Windows.Forms.TextBox()
        Me._FilePatternComboBox = New System.Windows.Forms.ComboBox()
        Me._ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._DataFileListBox = New isr.Core.Controls.FileListBox()
        Me._FileInfoTextBox = New isr.Core.Controls.RichTextBox()
        Me._FileInfoTextBoxLabel = New System.Windows.Forms.Label()
        Me._RefreshButton = New System.Windows.Forms.Button()
        Me._Annunciator = New System.Windows.Forms.ErrorProvider(Me.components)
        CType(Me._Annunciator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_FileListBoxLabel
        '
        Me._FileListBoxLabel.BackColor = System.Drawing.Color.Transparent
        Me._FileListBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._FileListBoxLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._FileListBoxLabel.ForeColor = System.Drawing.SystemColors.WindowText
        Me._FileListBoxLabel.Location = New System.Drawing.Point(2, 94)
        Me._FileListBoxLabel.Name = "_FileListBoxLabel"
        Me._FileListBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._FileListBoxLabel.Size = New System.Drawing.Size(72, 18)
        Me._FileListBoxLabel.TabIndex = 5
        Me._FileListBoxLabel.Text = "&Files:"
        Me._FileListBoxLabel.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        '_FileNamePatternCheckBoxLabel
        '
        Me._FileNamePatternCheckBoxLabel.BackColor = System.Drawing.Color.Transparent
        Me._FileNamePatternCheckBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._FileNamePatternCheckBoxLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._FileNamePatternCheckBoxLabel.ForeColor = System.Drawing.SystemColors.WindowText
        Me._FileNamePatternCheckBoxLabel.Location = New System.Drawing.Point(3, 342)
        Me._FileNamePatternCheckBoxLabel.Name = "_FileNamePatternCheckBoxLabel"
        Me._FileNamePatternCheckBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._FileNamePatternCheckBoxLabel.Size = New System.Drawing.Size(72, 18)
        Me._FileNamePatternCheckBoxLabel.TabIndex = 7
        Me._FileNamePatternCheckBoxLabel.Text = "File T&ype: "
        Me._FileNamePatternCheckBoxLabel.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        '_FolderTextBoxLabel
        '
        Me._FolderTextBoxLabel.AutoSize = True
        Me._FolderTextBoxLabel.BackColor = System.Drawing.Color.Transparent
        Me._FolderTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._FolderTextBoxLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._FolderTextBoxLabel.ForeColor = System.Drawing.SystemColors.WindowText
        Me._FolderTextBoxLabel.Location = New System.Drawing.Point(3, 3)
        Me._FolderTextBoxLabel.Name = "_FolderTextBoxLabel"
        Me._FolderTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._FolderTextBoxLabel.Size = New System.Drawing.Size(48, 17)
        Me._FolderTextBoxLabel.TabIndex = 0
        Me._FolderTextBoxLabel.Text = "F&older:"
        Me._FolderTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        '_FilePathNameTextBoxLabel
        '
        Me._FilePathNameTextBoxLabel.AutoSize = True
        Me._FilePathNameTextBoxLabel.BackColor = System.Drawing.Color.Transparent
        Me._FilePathNameTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._FilePathNameTextBoxLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._FilePathNameTextBoxLabel.ForeColor = System.Drawing.SystemColors.WindowText
        Me._FilePathNameTextBoxLabel.Location = New System.Drawing.Point(3, 51)
        Me._FilePathNameTextBoxLabel.Name = "_FilePathNameTextBoxLabel"
        Me._FilePathNameTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._FilePathNameTextBoxLabel.Size = New System.Drawing.Size(30, 17)
        Me._FilePathNameTextBoxLabel.TabIndex = 3
        Me._FilePathNameTextBoxLabel.Text = "F&ile:"
        Me._FilePathNameTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        '_FilePathNameTextBox
        '
        Me._FilePathNameTextBox.AcceptsReturn = True
        Me._FilePathNameTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._FilePathNameTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._FilePathNameTextBox.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._FilePathNameTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._FilePathNameTextBox.Location = New System.Drawing.Point(3, 70)
        Me._FilePathNameTextBox.MaxLength = 0
        Me._FilePathNameTextBox.Name = "_FilePathNameTextBox"
        Me._FilePathNameTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._FilePathNameTextBox.Size = New System.Drawing.Size(246, 22)
        Me._FilePathNameTextBox.TabIndex = 4
        Me._ToolTip.SetToolTip(Me._FilePathNameTextBox, "The name of the file that was selected or entered")
        Me._FilePathNameTextBox.WordWrap = False
        '
        '_SelectFolderButton
        '
        Me._SelectFolderButton.Font = New System.Drawing.Font("Segoe UI Black", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SelectFolderButton.Location = New System.Drawing.Point(609, 22)
        Me._SelectFolderButton.Name = "_SelectFolderButton"
        Me._SelectFolderButton.Size = New System.Drawing.Size(36, 26)
        Me._SelectFolderButton.TabIndex = 2
        Me._SelectFolderButton.Text = "..."
        Me._ToolTip.SetToolTip(Me._SelectFolderButton, "Click to select a folder")
        '
        '_FolderTextBox
        '
        Me._FolderTextBox.AcceptsReturn = True
        Me._FolderTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._FolderTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._FolderTextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._FolderTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._FolderTextBox.Location = New System.Drawing.Point(3, 22)
        Me._FolderTextBox.MaxLength = 0
        Me._FolderTextBox.Name = "_FolderTextBox"
        Me._FolderTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._FolderTextBox.Size = New System.Drawing.Size(604, 25)
        Me._FolderTextBox.TabIndex = 1
        Me._ToolTip.SetToolTip(Me._FolderTextBox, "The name of the file you have selected or entered")
        '
        '_FilePatternComboBox
        '
        Me._FilePatternComboBox.BackColor = System.Drawing.SystemColors.Window
        Me._FilePatternComboBox.Cursor = System.Windows.Forms.Cursors.Default
        Me._FilePatternComboBox.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._FilePatternComboBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._FilePatternComboBox.Items.AddRange(New Object() {"Data Files (*.dat)", "All Files (*.*)"})
        Me._FilePatternComboBox.Location = New System.Drawing.Point(3, 362)
        Me._FilePatternComboBox.Name = "_FilePatternComboBox"
        Me._FilePatternComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._FilePatternComboBox.Size = New System.Drawing.Size(184, 24)
        Me._FilePatternComboBox.TabIndex = 8
        Me._FilePatternComboBox.Text = "*.dat"
        Me._ToolTip.SetToolTip(Me._FilePatternComboBox, "Determines the type listed in the file list")
        '
        '_DataFileListBox
        '
        Me._DataFileListBox.BackColor = System.Drawing.SystemColors.Window
        Me._DataFileListBox.Cursor = System.Windows.Forms.Cursors.Default
        Me._DataFileListBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._DataFileListBox.ForeColor = System.Drawing.Color.Black
        Me._DataFileListBox.FormattingEnabled = True
        Me._DataFileListBox.ItemHeight = 17
        Me._DataFileListBox.Location = New System.Drawing.Point(3, 113)
        Me._DataFileListBox.Name = "_DataFileListBox"
        Me._DataFileListBox.DirectoryPath = String.Empty
        Me._DataFileListBox.SearchPattern = "*.dat"
        Me._DataFileListBox.ReadOnlyBackColor = System.Drawing.SystemColors.Control
        Me._DataFileListBox.ReadOnlyForeColor = System.Drawing.SystemColors.WindowText
        Me._DataFileListBox.ReadWriteBackColor = System.Drawing.SystemColors.Window
        Me._DataFileListBox.ReadWriteForeColor = System.Drawing.SystemColors.ControlText
        Me._DataFileListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me._DataFileListBox.Size = New System.Drawing.Size(248, 225)
        Me._DataFileListBox.TabIndex = 6
        Me._ToolTip.SetToolTip(Me._DataFileListBox, "Select file(s)")
        '
        '_FileInfoTextBox
        '
        Me._FileInfoTextBox.Location = New System.Drawing.Point(273, 70)
        Me._FileInfoTextBox.Name = "_FileInfoTextBox"
        Me._FileInfoTextBox.Size = New System.Drawing.Size(371, 316)
        Me._FileInfoTextBox.TabIndex = 10
        Me._FileInfoTextBox.Text = String.Empty
        Me._ToolTip.SetToolTip(Me._FileInfoTextBox, "Displays file information if available")
        '
        '_FileInfoTextBoxLabel
        '
        Me._FileInfoTextBoxLabel.AutoSize = True
        Me._FileInfoTextBoxLabel.Location = New System.Drawing.Point(272, 51)
        Me._FileInfoTextBoxLabel.Name = "_FileInfoTextBoxLabel"
        Me._FileInfoTextBoxLabel.Size = New System.Drawing.Size(56, 17)
        Me._FileInfoTextBoxLabel.TabIndex = 11
        Me._FileInfoTextBoxLabel.Text = "File info:"
        '
        '_RefreshButton
        '
        Me._RefreshButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._RefreshButton.Image = CType(resources.GetObject("_RefreshButton.Image"), System.Drawing.Image)
        Me._RefreshButton.Location = New System.Drawing.Point(214, 352)
        Me._RefreshButton.Name = "_RefreshButton"
        Me._RefreshButton.Size = New System.Drawing.Size(35, 34)
        Me._RefreshButton.TabIndex = 9
        Me._ToolTip.SetToolTip(Me._RefreshButton, "Click to select a folder")
        Me._RefreshButton.UseCompatibleTextRendering = True
        '
        '_Annunciator
        '
        Me._Annunciator.ContainerControl = Me
        '
        'FolderFileSelector
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit
        Me.Controls.Add(Me._FileInfoTextBoxLabel)
        Me.Controls.Add(Me._FileInfoTextBox)
        Me.Controls.Add(Me._FileListBoxLabel)
        Me.Controls.Add(Me._FileNamePatternCheckBoxLabel)
        Me.Controls.Add(Me._FolderTextBoxLabel)
        Me.Controls.Add(Me._FilePathNameTextBoxLabel)
        Me.Controls.Add(Me._FilePathNameTextBox)
        Me.Controls.Add(Me._FilePatternComboBox)
        Me.Controls.Add(Me._RefreshButton)
        Me.Controls.Add(Me._SelectFolderButton)
        Me.Controls.Add(Me._FolderTextBox)
        Me.Controls.Add(Me._DataFileListBox)
        Me.Name = "FolderFileSelector"
        Me.Size = New System.Drawing.Size(650, 390)
        CType(Me._Annunciator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _FileListBoxLabel As System.Windows.Forms.Label
    Private WithEvents _FileNamePatternCheckBoxLabel As System.Windows.Forms.Label
    Private WithEvents _FolderTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _ToolTip As System.Windows.Forms.ToolTip
    Private WithEvents _FilePathNameTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _FilePathNameTextBox As System.Windows.Forms.TextBox
    Private WithEvents _RefreshButton As System.Windows.Forms.Button
    Private WithEvents _SelectFolderButton As System.Windows.Forms.Button
    Private WithEvents _FolderTextBox As System.Windows.Forms.TextBox
    Private WithEvents _FilePatternComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _DataFileListBox As FileListBox
    Private WithEvents _Annunciator As System.Windows.Forms.ErrorProvider
    Private WithEvents _FileInfoTextBoxLabel As Label
    Private WithEvents _FileInfoTextBox As RichTextBox
End Class
