Imports System.ComponentModel

''' <summary> A file list view control. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 7/6/2019 </para>
''' </remarks>
<Description("File list view control"), System.Drawing.ToolboxBitmap(GetType(FileListView), "FileListViewControl.gif")>
Public Class FileListViewControl
    Inherits System.Windows.Forms.UserControl

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Gets the is initializing components. </summary>
    ''' <value> The is initializing components. </value>
    Private Property IsInitializingComponents As Boolean

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.Windows.Forms.UserControl" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        MyBase.New
        Me.IsInitializingComponents = True
        Me.InitializeComponent()
        Me.IsInitializingComponents = False
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="disposing"> <see langword="true" /> to release both managed and unmanaged
    '''                          resources; <see langword="false" /> to release only unmanaged
    '''                          resources. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso Me.components IsNot Nothing Then
                Me.components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " EVENT SOURCE "

    ''' <summary> Gets the file list view. </summary>
    ''' <value> The file list view. </value>
    Public ReadOnly Property FileListView As FileListView
        Get
            Return Me._FileListView
        End Get
    End Property

#End Region

#Region " APPEARANCE "

    ''' <summary> Gets or sets information describing the hide file. </summary>
    ''' <value> Information describing the hide file. </value>
    <Category("Appearance"), Description("Hide file info"), DefaultValue(True)>
    Public Property HideFileInfo As Boolean
        Get
            Return Me._Split.Panel2Collapsed
        End Get
        Set(value As Boolean)
            If value <> Me.HideFileInfo Then
                Me._Split.Panel2Collapsed = value
            End If
        End Set
    End Property

#End Region

#Region " BEHAVIOR "

    ''' <summary> Full pathname of the directory file. </summary>
    Private _DirectoryPath As String

    ''' <summary> The current directory path. </summary>
    ''' <value> The full pathname of the folder tree view. </value>
    <Category("Behavior"), Description("Current directory path")>
    Public Property DirectoryPath() As String
        Get
            Return Me._DirectoryPath
        End Get
        Set(ByVal value As String)
            If Not String.Equals(value, Me.DirectoryPath, StringComparison.OrdinalIgnoreCase) Then
                Me._DirectoryPath = value
            End If
        End Set
    End Property

    ''' <summary> Search pattern. </summary>
    ''' <value> The search pattern. </value>
    <Category("Behavior"), Description("Search pattern"), DefaultValue("*.*")>
    Public Property SearchPattern As String
        Get
            Return Me._SearchPatternComboBox.Text
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.SearchPattern) Then
                Me._SearchPatternComboBox.Text = Me.SearchPattern
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the search pattern delimiter. </summary>
    ''' <value> The search pattern delimiter. </value>
    Public Property SearchPatternDelimiter As Char = ";"c

    ''' <summary> Gets or sets the search patterns. </summary>
    ''' <value> The search patterns. </value>
    <Category("Behavior"), Description("Search patterns"), DefaultValue("*.*;*.bmp;*.png;*.csv;*.txt")>
    Public Property SearchPatterns As String
        Get
            Dim values As New System.Text.StringBuilder
            For Each item As Object In Me._SearchPatternComboBox.Items
                values.Append($"{item}{Me.SearchPatternDelimiter}")
            Next
            Return values.ToString.TrimEnd(Me.SearchPatternDelimiter)
        End Get
        Set(value As String)
            If String.IsNullOrWhiteSpace(value) Then
                Me._SearchPatternComboBox.Items.Clear()
                Me._SearchPatternComboBox.Items.Add("*.*")
            Else
                Me._SearchPatternComboBox.Items.Clear()
                For Each pattern As String In value.Split(Me.SearchPatternDelimiter)
                    Me._SearchPatternComboBox.Items.Add(pattern)
                Next
            End If
            Me._SearchPatternComboBox.SelectedIndex = 0
        End Set
    End Property

    ''' <summary>
    ''' Forces the control to invalidate its client area and immediately redraw itself and any child
    ''' controls.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Overrides Sub Refresh()
        MyBase.Refresh()
        If Me.IsInitializingComponents Then Return
        Me._FileListView.PopulateFiles(Me.DirectoryPath, Me._SearchPatternComboBox.Text)
    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary> Refresh button click. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub RefreshButtonClick(sender As Object, e As EventArgs) Handles _RefreshButton.Click
        Me.Refresh()
    End Sub

#End Region

End Class
