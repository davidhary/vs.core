Imports System.ComponentModel
Imports System.IO
Imports System.Management

Imports isr.Core.Controls.ExceptionExtensions

''' <summary> A folder tree view. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 7/4/2019 </para>
''' </remarks>
<Description("Folder Tree View"), System.Drawing.ToolboxBitmap(GetType(FolderTreeViewPassive), "FolderTreeViewPassive.gif")>
Public Class FolderTreeViewPassive
    Inherits Windows.Forms.TreeView

#Region " CONSTRUCTION "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        MyBase.New()
        Dim resources As New System.Resources.ResourceManager(GetType(FolderTreeViewPassive))
        Me.ImageList = New System.Windows.Forms.ImageList With {
            .ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit,
            .ImageSize = New System.Drawing.Size(16, 16),
            .ImageStream = CType(resources.GetObject("ImageStream"), System.Windows.Forms.ImageListStreamer),
            .TransparentColor = System.Drawing.Color.Transparent
        }

        ' Populate TreeView with Drive list
        Me.PopulateDriveList()
    End Sub

#End Region

#Region " BEHAVIOR "

    ''' <summary> Populate drive list. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub PopulateDriveList()
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.PopulateDriveListThis()
        Catch
            Throw
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Populate drive list. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Sub PopulateDriveListThis()

        Dim nodeTreeNode As TreeNode

        Const Removable As Integer = 2
        Const LocalDisk As Integer = 3
        Const Network As Integer = 4
        Const CD As Integer = 5

        'clear TreeView
        Me.Nodes.Clear()
        nodeTreeNode = New TreeNode("My Computer", 0, 0) With {.Name = String.Empty}
        Me.Nodes.Add(nodeTreeNode)
        Me.CurrentNode = nodeTreeNode

        'set node collection
        Dim nodeCollection As TreeNodeCollection = nodeTreeNode.Nodes

        'Get Drive list
        Dim queryCollection As ManagementObjectCollection = SelectDrives()
        For Each mo As ManagementObject In queryCollection

            Dim imageIndex As Integer
            Dim selectIndex As Integer

            Select Case Integer.Parse(mo("DriveType").ToString())
                Case Removable ' removable drives
                    imageIndex = 5
                    selectIndex = 5
                Case LocalDisk 'Local drives
                    imageIndex = 6
                    selectIndex = 6
                Case CD ' CD-Rom drives
                    imageIndex = 7
                    selectIndex = 7
                Case Network 'Network drives
                    imageIndex = 8
                    selectIndex = 8
                Case Else ' defaults to folder image
                    imageIndex = 2
                    selectIndex = 3
            End Select
            'create new drive node
            nodeTreeNode = New TreeNode(mo("Name").ToString() & "\", imageIndex, selectIndex)

            'add new node
            nodeCollection.Add(nodeTreeNode)
        Next mo
    End Sub

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.TreeView.AfterSelect" /> event.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.TreeViewEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnAfterSelect(e As TreeViewEventArgs)
        If e Is Nothing Then Return
        ' get current selected drive or folder
        Dim selectedNode As TreeNode = e.Node

        ' clear all sub-folders
        selectedNode.Nodes.Clear()

        If selectedNode.SelectedImageIndex = 0 Then
            ' Selected My Computer - repopulate drive list
            Me.PopulateDriveList()
        Else
            ' populate sub-folders and folder files
            FolderTreeViewPassive.PopulateDirectory(selectedNode, selectedNode.Nodes)
        End If
        Me.CurrentNode = selectedNode
        MyBase.OnAfterSelect(e)

    End Sub

    ''' <summary> Populate directory. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="currentNode">           The current node. </param>
    ''' <param name="currentNodeCollection"> current Collection of nodes. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Shared Sub PopulateDirectory(ByVal currentNode As TreeNode, ByVal currentNodeCollection As TreeNodeCollection)
        If currentNode Is Nothing OrElse currentNodeCollection Is Nothing Then Return
        Dim nodeDir As TreeNode
        Dim imageIndex As Integer = 2 ' unselected image index
        Dim selectIndex As Integer = 3 'selected image index

        If currentNode.SelectedImageIndex <> 0 Then
            ' populate tree view with folders
            Try
                ' check path
                If Directory.Exists(ParseFullPath(currentNode.FullPath)) = False Then
                    MessageBox.Show($"Directory or path {currentNode.Name} does not exist.")
                Else
                    Dim stringDirectories() As String = Directory.GetDirectories(ParseFullPath(currentNode.FullPath))
                    Dim fullPath As String = String.Empty
                    Dim pathName As String = String.Empty

                    ' loop through all directories
                    For Each directoryName As String In stringDirectories
                        fullPath = directoryName
                        pathName = FolderTreeViewPassive.ParsePathName(fullPath)

                        ' create node for directories
                        nodeDir = New TreeNode(pathName, imageIndex, selectIndex) With {.Name = fullPath}
                        currentNodeCollection.Add(nodeDir)
                    Next directoryName
                End If
            Catch ex As IOException
                MessageBox.Show("Error: Drive not ready or directory does not exist.")
            Catch ex As UnauthorizedAccessException
                MessageBox.Show("Error: Drive or directory access denied.")
            Catch ex As Exception
                MessageBox.Show($"Error: {ex.ToFullBlownString}")
            End Try
        End If
    End Sub

    ''' <summary> Parse path name. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="filePath"> Full pathname of the file. </param>
    ''' <returns> A String. </returns>
    Private Shared Function ParsePathName(ByVal filePath As String) As String
        Return If(String.IsNullOrWhiteSpace(filePath), filePath, filePath.Split("\"c).Last)
    End Function

    ''' <summary> Parse full path removing 'My Computer\'. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="compositeFolderPath"> The composite folder path including 'My Computer'. </param>
    ''' <returns> A String. </returns>
    Public Shared Function ParseFullPath(ByVal compositeFolderPath As String) As String
        Return If(String.IsNullOrWhiteSpace(compositeFolderPath), compositeFolderPath, compositeFolderPath.Replace("My Computer\", ""))
    End Function

    ''' <summary> Select drives. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> A ManagementObjectCollection. </returns>
    Private Shared Function SelectDrives() As ManagementObjectCollection
        Dim query As New ManagementObjectSearcher("SELECT * From Win32_LogicalDisk ")
        Dim queryCollection As ManagementObjectCollection = query.Get()
        Return queryCollection
    End Function

#End Region

#Region " PROPERTIES "

    ''' <summary> Searches for the first node. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="path"> The full pathname of the file. </param>
    Public Sub FindNode(ByVal path As String)
        If Directory.Exists(path) Then
            Dim nodes As IEnumerable(Of TreeNode) = Me.Nodes.Find(path, True)
            Me.CurrentNode = If(nodes.Any, nodes.First, Me.Nodes(0))
        End If
    End Sub

    ''' <summary> The node. </summary>
    Private _Node As TreeNode

    ''' <summary> Gets or sets the current node. </summary>
    ''' <value> The current node. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property CurrentNode As TreeNode
        Get
            Return Me._Node
        End Get
        Set(value As TreeNode)
            If value IsNot Nothing AndAlso
                (Me.CurrentNode Is Nothing OrElse Not String.Equals(value.Name, Me.CurrentNode.Name)) Then
                Me._Node = value
                Me.Path = value.Name
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the full pathname of the file. </summary>
    ''' <value> The full pathname of the file. </value>
    <DefaultValue(False)>
    <Description("Current Path"), Category("Appearance")>
    Public Property Path As String
        Get
            Return Me.CurrentNode.Name
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.Path) Then
                Me.FindNode(value)
            End If
        End Set
    End Property

#End Region


End Class
