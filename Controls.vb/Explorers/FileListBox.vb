Imports System.ComponentModel

''' <summary> File list box. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/27/2013 </para>
''' </remarks>
<DesignerCategory("code"), System.ComponentModel.Description("File List Box")>
Public Class FileListBox
    Inherits System.Windows.Forms.ListBox

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        MyBase.New()
        '_ReadOnlyBackColor = SystemColors.Control
        '_ReadOnlyForeColor = SystemColors.WindowText
    End Sub

    ''' <summary> Gets the selection enabled. </summary>
    ''' <value> The selection enabled. </value>
    Public ReadOnly Property SelectionEnabled As Boolean
        Get
            Return MyBase.AllowSelection
        End Get
    End Property

    ''' <summary> A pattern specifying the search. </summary>
    Private _SearchPattern As String

    ''' <summary>
    ''' Gets the file name search pattern for files displayed in the file list box.
    ''' </summary>
    ''' <value> The pattern. </value>
    <Category("Pattern")> <Description("Specifies the file name search pattern.")>
    Public Property SearchPattern As String
        Get
            Return Me._SearchPattern
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.SearchPattern, StringComparison.OrdinalIgnoreCase) Then
                Me._SearchPattern = value
            End If
        End Set
    End Property

    ''' <summary> Updates the display. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub UpdateDisplay()
        If Not String.IsNullOrWhiteSpace(Me.DirectoryPath) Then
            Me.Items.Clear()
            Dim di As New System.IO.DirectoryInfo(Me._DirectoryPath)
            If di.Exists Then
                For Each fi As System.IO.FileInfo In di.GetFiles(Me.SearchPattern)
                    Me.Items.Add(fi.Name)
                Next
            End If
        End If
    End Sub

    ''' <summary> Gets or sets the filename of the selected file. </summary>
    ''' <value> The filename of the selected file. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property SelectedFileName As String
        Get
            Return If(Me.SelectedItem Is Nothing, String.Empty, CStr(Me.SelectedItem))
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.SelectedFileName, StringComparison.OrdinalIgnoreCase) Then
                Dim selectableIndex As Integer = Me.FindString(value)
                If selectableIndex >= 0 Then Me.SelectedIndex = selectableIndex
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the full file name including path). </summary>
    ''' <value> The full filename of the selected file. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property SelectedFilePath As String
        Get
            Return If(Me.SelectedItem Is Nothing, String.Empty, System.IO.Path.Combine(Me.DirectoryPath, CStr(Me.SelectedItem)))
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.SelectedFilePath, StringComparison.OrdinalIgnoreCase) Then
                Dim fi As New System.IO.FileInfo(value)
                Me.DirectoryPath = fi.DirectoryName
                Me.SelectedFileName = fi.Name
            End If
        End Set
    End Property

    ''' <summary> Gets a list of names of the files. </summary>
    ''' <value> A list of names of the files. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property SelectedFileNames As IList(Of String)
        Get
            Dim l As New List(Of String)
            For Each v As Object In Me.SelectedItems
                Dim name As String = TryCast(v, String)
                If Not String.IsNullOrWhiteSpace(name) Then
                    l.Add(name)
                End If
            Next
            Return l
        End Get
    End Property

    ''' <summary> Gets the selected file paths. </summary>
    ''' <value> The selected file paths. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property SelectedFilePaths As IList(Of String)
        Get
            Dim l As New List(Of String)
            For Each v As Object In Me.SelectedItems
                Dim name As String = TryCast(v, String)
                If Not String.IsNullOrWhiteSpace(name) Then
                    l.Add(System.IO.Path.Combine(Me.DirectoryPath, name))
                End If
            Next
            Return l
        End Get
    End Property

    ''' <summary> Full pathname of the directory. </summary>
    Private _DirectoryPath As String

    ''' <summary> Gets or sets the current directory path. </summary>
    ''' <value> The directory path. </value>
    <Category("Behavior")> <Description("The current directory path")>
    Public Property DirectoryPath As String
        Get
            Return Me._DirectoryPath
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            If Not String.Equals(value, Me.DirectoryPath, StringComparison.OrdinalIgnoreCase) Then
                Me._DirectoryPath = value
                Me.UpdateDisplay()
            End If
        End Set
    End Property

    ''' <summary> The read write context menu. </summary>
    Private _ReadWriteContextMenu As ContextMenu

    ''' <summary> true to read only. </summary>
    Private _ReadOnly As Boolean

    ''' <summary> Gets or sets the read only. </summary>
    ''' <value> The read only. </value>
    <DefaultValue(False)>
    <Category("Behavior")> <Description("Indicates whether the List Box is read only.")>
    Public Property [ReadOnly]() As Boolean
        Get
            Return Me._ReadOnly
        End Get
        Set(ByVal value As Boolean)
            If Me._ReadOnly <> value Then
                Me._ReadOnly = value
                If value Then
                    Me._ReadWriteContextMenu = MyBase.ContextMenu
                    Me.ContextMenu = New ContextMenu()
                    Dim h As Integer = MyBase.Height
                    Me.Height = h + 3
                    Me.BackColor = Me.ReadOnlyBackColor
                    Me.ForeColor = Me.ReadOnlyForeColor
                Else
                    Me.ContextMenu = Me._ReadWriteContextMenu
                    Me.BackColor = Me.ReadWriteBackColor
                    Me.ForeColor = Me.ReadWriteForeColor
                End If
            End If
        End Set
    End Property

    ''' <summary> The read only back color. </summary>
    Private _ReadOnlyBackColor As Color

    ''' <summary> Gets or sets the color of the read only back. </summary>
    ''' <value> The color of the read only back. </value>
    <DefaultValue(GetType(Drawing.Color), "SystemColors.Control")>
    <Description("Back color when read only"), Category("Appearance")>
    Public Property ReadOnlyBackColor() As Color
        Get
            If Me._ReadOnlyBackColor.IsEmpty Then
                Me._ReadOnlyBackColor = SystemColors.Control
            End If
            Return Me._ReadOnlyBackColor
        End Get
        Set(value As Color)
            Me._ReadOnlyBackColor = value
        End Set
    End Property

    ''' <summary> The read only foreground color. </summary>
    Private _ReadOnlyForeColor As Color

    ''' <summary> Gets or sets the color of the read only foreground. </summary>
    ''' <value> The color of the read only foreground. </value>
    <DefaultValue(GetType(Drawing.Color), "SystemColors.WindowText")>
    <Description("Fore color when read only"), Category("Appearance")>
    Public Property ReadOnlyForeColor() As Color
        Get
            If Me._ReadOnlyForeColor.IsEmpty Then
                Me._ReadOnlyForeColor = SystemColors.WindowText
            End If
            Return Me._ReadOnlyForeColor
        End Get
        Set(value As Color)
            Me._ReadOnlyForeColor = value
        End Set
    End Property

    ''' <summary> The read write back color. </summary>
    Private _ReadWriteBackColor As System.Drawing.Color

    ''' <summary> Gets or sets the color of the read write back. </summary>
    ''' <value> The color of the read write back. </value>
    <DefaultValue(GetType(System.Drawing.Color), "SystemColors.Window")>
    <Description("Back color when control is read/write"), Category("Appearance")>
    Public Property ReadWriteBackColor() As System.Drawing.Color
        Get
            If Me._ReadWriteBackColor.IsEmpty Then
                Me._ReadWriteBackColor = SystemColors.Window
            End If
            Return Me._ReadWriteBackColor
        End Get
        Set(value As Color)
            Me._ReadWriteBackColor = value
        End Set
    End Property

    ''' <summary> The read write foreground color. </summary>
    Private _ReadWriteForeColor As System.Drawing.Color

    ''' <summary> Gets or sets the color of the read write foreground. </summary>
    ''' <value> The color of the read write foreground. </value>
    <DefaultValue(GetType(System.Drawing.Color), "System.Drawing.SystemColors.ControlText")>
    <Description("Fore color when control is read/write"), Category("Appearance")>
    Public Property ReadWriteForeColor() As System.Drawing.Color
        Get
            If Me._ReadWriteForeColor.IsEmpty Then
                Me._ReadWriteForeColor = SystemColors.ControlText
            End If
            Return Me._ReadWriteForeColor
        End Get
        Set(value As Color)
            Me._ReadWriteForeColor = value
        End Set
    End Property

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.KeyDown" /> event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.KeyEventArgs" /> that contains the event
    '''                  data. </param>
    Protected Overrides Sub OnKeyDown(ByVal e As KeyEventArgs)
        If e Is Nothing Then Return
        If Me.ReadOnly AndAlso (e.KeyCode = Keys.Up OrElse e.KeyCode = Keys.Down OrElse e.KeyCode = Keys.Delete OrElse
                                e.KeyCode = Keys.F4 OrElse e.KeyCode = Keys.PageDown OrElse e.KeyCode = Keys.PageUp) Then
            e.Handled = True
        Else
            MyBase.OnKeyDown(e)
        End If
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.KeyPress" /> event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.KeyPressEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnKeyPress(ByVal e As KeyPressEventArgs)
        If e Is Nothing Then Return
        If Me.ReadOnly Then
            e.Handled = True
        Else
            MyBase.OnKeyPress(e)
        End If
    End Sub

End Class
