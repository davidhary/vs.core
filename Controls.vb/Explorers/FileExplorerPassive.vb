Imports System.ComponentModel

''' <summary> A file explorer. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 7/4/2019 </para>
''' </remarks>
<Description("File explorer"), System.Drawing.ToolboxBitmap(GetType(FileExplorerPassive), "FileExplorerPassive.gif")>
Public Class FileExplorerPassive
    Inherits Forma.ModelViewBase

#Region " CONSTRUCTION "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        'This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the isr.Core.Forma.ModelViewBase and optionally
    ''' releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me._Components IsNot Nothing Then Me._Components.Dispose() : Me._Components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " DESIGNER "

    ''' <summary> The splitter. </summary>
    Private _Splitter As System.Windows.Forms.Splitter

        Private WithEvents FolderTreeView As FolderTreeViewPassive

    ''' <summary> The file list view. </summary>
    Private _FileListView As FileListView

    ''' <summary> The components. </summary>
    Private _Components As System.ComponentModel.IContainer

    ''' <summary>
    ''' Required method for Designer support - do not modify the contents of this method with the
    ''' code editor.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Sub InitializeComponent()
        Me._Components = New System.ComponentModel.Container()
        Dim TreeNode1 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("C:\", 6, 6)
        Dim TreeNode2 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("D:\", 7, 7)
        Dim TreeNode3 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("My Computer", 0, 0, New System.Windows.Forms.TreeNode() {TreeNode1, TreeNode2})
        Me.FolderTreeView = New isr.Core.Controls.FolderTreeViewPassive()
        Me._Splitter = New System.Windows.Forms.Splitter()
        Me._FileListView = New isr.Core.Controls.FileListView()
        Me.SuspendLayout()
        '
        '_FolderTreeView
        '
        Me.FolderTreeView.Cursor = System.Windows.Forms.Cursors.Default
        Me.FolderTreeView.Dock = System.Windows.Forms.DockStyle.Left
        Me.FolderTreeView.ImageIndex = 0
        Me.FolderTreeView.Location = New System.Drawing.Point(0, 0)
        Me.FolderTreeView.Name = "_FolderTreeView"
        TreeNode1.ImageIndex = 6
        TreeNode1.Name = String.Empty
        TreeNode1.SelectedImageIndex = 6
        TreeNode1.Text = "C:\"
        TreeNode2.ImageIndex = 7
        TreeNode2.Name = String.Empty
        TreeNode2.SelectedImageIndex = 7
        TreeNode2.Text = "D:\"
        TreeNode3.ImageIndex = 0
        TreeNode3.Name = String.Empty
        TreeNode3.SelectedImageIndex = 0
        TreeNode3.Text = "My Computer"
        Me.FolderTreeView.Nodes.AddRange(New System.Windows.Forms.TreeNode() {TreeNode3})
        Me.FolderTreeView.Path = String.Empty
        Me.FolderTreeView.SelectedImageIndex = 0
        Me.FolderTreeView.Size = New System.Drawing.Size(168, 357)
        Me.FolderTreeView.TabIndex = 0
        '
        '_Splitter
        '
        Me._Splitter.Location = New System.Drawing.Point(168, 0)
        Me._Splitter.Name = "_Splitter"
        Me._Splitter.Size = New System.Drawing.Size(3, 357)
        Me._Splitter.TabIndex = 3
        Me._Splitter.TabStop = False
        '
        '_FileListView
        '
        Me._FileListView.Dock = System.Windows.Forms.DockStyle.Fill
        Me._FileListView.HideSelection = False
        Me._FileListView.Location = New System.Drawing.Point(171, 0)
        Me._FileListView.Name = "_FileListView"
        Me._FileListView.Size = New System.Drawing.Size(429, 357)
        Me._FileListView.TabIndex = 1
        Me._FileListView.UseCompatibleStateImageBehavior = False
        Me._FileListView.View = System.Windows.Forms.View.Details
        '
        'FileExplorerPassive
        '
        Me.Controls.Add(Me._FileListView)
        Me.Controls.Add(Me._Splitter)
        Me.Controls.Add(Me.FolderTreeView)
        Me.Name = "FileExplorerPassive"
        Me.Size = New System.Drawing.Size(600, 357)
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region " CONTROL EVENTS "

    ''' <summary> Handles the after select. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Tree view event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub FolderTreeViewAfterSelect(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles FolderTreeView.AfterSelect
        'Populate folders and files when a folder is selected
        Me.Cursor = Cursors.WaitCursor
        Dim fullPath As String = String.Empty
        Try
            Dim folderTreeNode As TreeNode = e.Node
            fullPath = FolderTreeViewPassive.ParseFullPath(e.Node.FullPath)
            If folderTreeNode Is Nothing OrElse folderTreeNode.SelectedImageIndex = 0 Then
                ' nothing to do: leave clear file list
            ElseIf System.IO.Directory.Exists(fullPath) Then
                Me._FileListView.PopulateFiles(fullPath, "*.*")
            Else
            End If
        Catch ex As UnauthorizedAccessException
            MessageBox.Show($"Access denied to folder {fullPath}", "Access denied", MessageBoxButtons.OK,
                            MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        Catch ex As Exception
            MessageBox.Show($"Error: {ex}")
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

End Class

