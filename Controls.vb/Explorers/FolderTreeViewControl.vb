Imports System.ComponentModel
Imports System.IO

''' <summary> Summary description for ExplorerTree. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 7/6/2019 </para>
''' </remarks>
<ToolboxBitmapAttribute(GetType(FolderTreeViewControl), "FolderTreeViewControl.gif")>
Partial Public Class FolderTreeViewControl
    Inherits System.Windows.Forms.UserControl

#Region " CONSTRUCTION And CLEANUP "

    ''' <summary> Gets the is initializing components. </summary>
    ''' <value> The is initializing components. </value>
    Private Property IsInitializingComponents As Boolean

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.Windows.Forms.UserControl" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        Me.IsInitializingComponents = True
        ' This call is required by the Windows.Forms Form Designer.
        Me.InitializeComponent()
        Me.IsInitializingComponents = False
        Me._HomeToolStripButton.Image = Me._ImageList.Images.Item(23)
        Me._AddFolderButton.Image = Me._ImageList.Images.Item(27)
        Me._BackToolStripButton.Image = Me._ImageList.Images.Item(20)
        Me._ForwardToolStripButton.Image = Me._ImageList.Images.Item(19)
        Me._FolderUpButton.Image = Me._ImageList.Images.Item(24)
        Me._RefreshToolStripButton.Image = Me._ImageList.Images.Item(25)
    End Sub

    ''' <summary> Clean up any resources being used. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="disposing"> <see langword="true" /> to release both managed and unmanaged
    '''                          resources; <see langword="false" /> to release only unmanaged
    '''                          resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Me.components IsNot Nothing Then
                Me.components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

#End Region

#Region " APPEARANCE "

    ''' <summary> Gets the folder tree view. </summary>
    ''' <value> The folder tree view. </value>
    Public ReadOnly Property FolderTreeView As FolderTreeView
        Get
            Return Me._FolderTreeView
        End Get
    End Property

    ''' <summary> Gets or sets the show current path text box. </summary>
    ''' <value> The show current path text box. </value>
    Public Property ShowCurrentPathTextBox() As Boolean
        Get
            Return Me._CurrentPathLayoutPanel.Visible
        End Get
        Set(ByVal value As Boolean)
            Me._CurrentPathLayoutPanel.Visible = value
        End Set
    End Property

    ''' <summary> Gets or sets the show tool bar. </summary>
    ''' <value> The show tool bar. </value>
    Public Property ShowToolBar() As Boolean
        Get
            Return Me._ToolStrip.Visible
        End Get
        Set(ByVal value As Boolean)
            Me._ToolStrip.Visible = value
        End Set
    End Property

    ''' <summary> Gets or sets the show my documents. </summary>
    ''' <value> The show my documents. </value>
    Public Property ShowMyDocuments() As Boolean
        Get
            Return Me.FolderTreeView.ShowMyDocuments
        End Get
        Set(ByVal value As Boolean)
            Me.FolderTreeView.ShowMyDocuments = value
        End Set
    End Property

    ''' <summary> Gets or sets the show my favorites. </summary>
    ''' <value> The show my favorites. </value>
    Public Property ShowMyFavorites() As Boolean
        Get
            Return Me.FolderTreeView.ShowMyFavorites
        End Get
        Set(ByVal value As Boolean)
            Me.FolderTreeView.ShowMyFavorites = value
        End Set
    End Property

    ''' <summary> Gets or sets the show my network. </summary>
    ''' <value> The show my network. </value>
    Public Property ShowMyNetwork() As Boolean
        Get
            Return Me.FolderTreeView.ShowMyNetwork
        End Get
        Set(ByVal value As Boolean)
            Me.FolderTreeView.ShowMyNetwork = value
        End Set
    End Property

#End Region

#Region " BEHAVIOR "

    ''' <summary> The selected path. </summary>
    ''' <value> The full pathname of the folder tree view. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property SelectedPath() As String
        Get
            Return Me._FolderTreeView.SelectedPath
        End Get
        Set(ByVal value As String)
            If Not String.Equals(value, Me.SelectedPath, StringComparison.OrdinalIgnoreCase) Then
                Me.SelectCurrentPath(value)
            End If
        End Set
    End Property

    ''' <summary> Select current path. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub SelectCurrentPath(ByVal value As String)
        Dim outcome As (success As Boolean, details As String) = Me.FolderTreeView.TrySelectPath(value)
        If outcome.success Then
            Me._PathTextBox.Text = value
        End If
    End Sub

    ''' <summary> Refresh view. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub RefreshView()
        Me.FolderTreeView.Refresh()
    End Sub

#End Region

#Region " TOOL BAR "

    ''' <summary> Event handler. Called by _RefreshToolStripButton for click events. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub RefreshButtonClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles _RefreshToolStripButton.Click
        Cursor.Current = Cursors.WaitCursor
        Try
            Me.RefreshView()
        Catch e1 As Exception
            MessageBox.Show("Error: " & e1.Message)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub

    ''' <summary> Event handler. Called by _GoToDirectoryButton for click events. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub GoToDirectoryButtonClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles _GoToDirectoryButton.Click
        Cursor.Current = Cursors.WaitCursor
        Try
            Me.SelectCurrentPath(Me._PathTextBox.Text.Trim)
        Catch e1 As Exception
            MessageBox.Show("Error: " & e1.Message)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub

    ''' <summary> Home tool strip button click. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub HomeToolStripButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _HomeToolStripButton.Click
        Me.FolderTreeView.TrySelectHome()
    End Sub

    ''' <summary> Forward tool strip button click. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ForwardToolStripButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ForwardToolStripButton.Click
        Me.FolderTreeView.TryGoingForward()
    End Sub

    ''' <summary> Back tool strip button click. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub BackToolStripButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _BackToolStripButton.Click
        Me.FolderTreeView.TryGoingBack()
    End Sub

    ''' <summary> Folder up button click. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub FolderUpButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _FolderUpButton.Click
        Me.FolderTreeView.TryGoingUp()
    End Sub

    ''' <summary> Adds a folder button click to 'e'. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub AddFolderButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _AddFolderButton.Click
        Me.FolderTreeView.TryAddFolder()
    End Sub

#End Region

#Region " PATH TEXT BOX "

    ''' <summary> Folder tree view path changed. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub FolderTreeView_PathChanged(sender As Object, e As EventArgs) Handles _FolderTreeView.PathChanged
        If Me.IsInitializingComponents Then Return
        Me._PathTextBox.Text = Me._FolderTreeView.SelectedPath
    End Sub

    ''' <summary> Path text box key up. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Key event information. </param>
    Private Sub PathTextBox_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles _PathTextBox.KeyUp
        If e.KeyValue = 13 Then
            Dim value As String = Me._PathTextBox.Text.Trim
            If Directory.Exists(value) AndAlso Not String.Equals(Me.FolderTreeView.SelectedPath, value, StringComparison.OrdinalIgnoreCase) Then
                Me.FolderTreeView.SelectPath(value)
            End If
            Me._PathTextBox.Focus()
        End If
    End Sub


#End Region

End Class

