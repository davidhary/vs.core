
Partial Public Class FolderTreeView

    ''' <summary> Network computers. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> A list of. </returns>
    <CodeAnalysis.SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private Shared Function NetworkComputers() As IList(Of String)
        Dim servers As New List(Of String)
        'TO_DO: Use Win32_MyComputerSystem node instead of Win NT
        Using root As New DirectoryServices.DirectoryEntry("WinNT:")
            For Each entries As DirectoryServices.DirectoryEntries In root.Children
                For Each entry As DirectoryServices.DirectoryEntry In entries
                    If entry.SchemaClassName = "Computer" Then
                        servers.Add(entry.Name)
                    End If
                Next
            Next
        End Using
        Return servers
    End Function

    ''' <summary> Expand entire network node. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> Tree view event information. </param>
    <CodeAnalysis.SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private Shared Sub ExpandEntireNetworkNode(ByVal e As System.Windows.Forms.TreeViewEventArgs)

        Dim n As TreeNode = e.Node
        Dim nodeNN As TreeNode
        Dim nodemN As TreeNode

        If String.Equals(n.Text, "Entire Network", StringComparison.OrdinalIgnoreCase) Then
            If n.FirstNode.Text = "Network Node" Then
                n.FirstNode.Remove()

                'NETRESOURCE netRoot = new NETRESOURCE();
                Using shares As Management.ManagementClass = New Management.ManagementClass()

                End Using

                Dim servers As New ArrayList
                Using root As New DirectoryServices.DirectoryEntry("WinNT:")
                    For Each entries As DirectoryServices.DirectoryEntries In root.Children
                        For Each entry As DirectoryServices.DirectoryEntry In entries
                            servers.Add(entry.Name)
                        Next
                    Next
                End Using
                For Each s1 As String In servers
                    Dim s2 As String = String.Empty
                    s2 = s1.Substring(0, s1.IndexOf("|", 1, StringComparison.OrdinalIgnoreCase))

                    If s1.IndexOf("NETWORK", 1, StringComparison.OrdinalIgnoreCase) > 0 Then
                        nodeNN = New TreeNode With {
                                    .Tag = s2,
                                    .Text = s2, 'dir.Substring(dir.LastIndexOf(@"\") + 1);
                                    .ImageIndex = 15,
                                    .SelectedImageIndex = 15
                                }
                        n.Nodes.Add(nodeNN)
                    Else
                        ' need to handle the VMWare situation where 
                        nodemN = New TreeNode With {
                                    .Tag = s2, '"my Node";
                                    .Text = s2, '"my Node";//dir.Substring(dir.LastIndexOf(@"\") + 1);
                                    .ImageIndex = 16,
                                    .SelectedImageIndex = 16
                                }
                        ' DH: this does not work. there are initially no nodes here
                        ' so last node is null.
                        If n.LastNode Is Nothing Then
                            n.Nodes.Add(nodemN)
                        Else
                            n.LastNode.Nodes.Add(nodemN)
                        End If

                        Dim nodemNc As TreeNode
                        nodemNc = New TreeNode With {
                                    .Tag = FolderTreeView._MyNetworkNodeName,
                                    .Text = FolderTreeView._MyNetworkNodeName, 'dir.Substring(dir.LastIndexOf(@"\") + 1);
                                    .ImageIndex = 12,
                                    .SelectedImageIndex = 12
                                }
                        nodemN.Nodes.Add(nodemNc)
                    End If
                Next s1
            End If
        End If

    End Sub

    ''' <summary> Name of my network node. </summary>
    Private Const _MyNetworkNodeName As String = "My Node"

    ''' <summary> Expand Microsoft windows network node. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> Tree view event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <CodeAnalysis.SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private Shared Sub ExpandMicrosoftWindowsNetworkNode(ByVal e As System.Windows.Forms.TreeViewEventArgs)

        Dim n As TreeNode = e.Node
        Dim nodeNN As TreeNode
        Dim nodeNNode As TreeNode

        If (n.Parent IsNot Nothing) AndAlso String.Equals(n.Parent.Text, "Microsoft Windows Network", StringComparison.OrdinalIgnoreCase) Then

            If String.Equals(n.FirstNode.Text, FolderTreeView._MyNetworkNodeName, StringComparison.OrdinalIgnoreCase) Then
                n.FirstNode.Remove()

                ' NETRESOURCE netRoot = new NETRESOURCE();
                ' Dim servers As New ServerEnum(ResourceScope.RESOURCE_GLOBALNET, ResourceType.RESOURCETYPE_DISK, 
                ' ResourceUsage.RESOURCEUSAGE_ALL, ResourceDisplayType.RESOURCEDISPLAYTYPE_SERVER, n.Text)
#Disable Warning CA1825 ' Avoid zero-length array allocations.
                Dim servers As String() = New String() {} ' ServerEnum(n.Text)
#Enable Warning CA1825 ' Avoid zero-length array allocations.

                For Each s1 As String In servers

                    If (s1.Length < 6) OrElse Not String.Equals(s1.Substring(s1.Length - 6, 6), "-share", StringComparison.OrdinalIgnoreCase) Then
                        Dim s2 As String = s1
                        nodeNN = New TreeNode With {
                            .Tag = s2,
                            .Text = s2.Substring(2),
                            .ImageIndex = 12,
                            .SelectedImageIndex = 12
                        }
                        n.Nodes.Add(nodeNN)
                        For Each s1node As String In servers
                            If s1node.Length > 6 Then
                                If String.Equals(s1node.Substring(s1node.Length - 6, 6), "-share", StringComparison.OrdinalIgnoreCase) Then
                                    If s2.Length <= s1node.Length Then
                                        Try
                                            If String.Equals(s1node.Substring(0, s2.Length + 1), s2 & "\", StringComparison.OrdinalIgnoreCase) Then
                                                nodeNNode = New TreeNode With {
                                                    .Tag = s1node.Substring(0, s1node.Length - 6),
                                                    .Text = s1node.Substring(s2.Length + 1, s1node.Length - s2.Length - 7),
                                                    .ImageIndex = 28,
                                                    .SelectedImageIndex = 28
                                                }
                                                nodeNN.Nodes.Add(nodeNNode)
                                            End If
                                        Catch e2 As Exception
                                        End Try
                                    End If
                                End If
                            End If

                        Next s1node
                    End If

                Next s1
            End If
        End If
    End Sub

End Class
