Imports System.ComponentModel

''' <summary> Folder and file explorer view. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 7/4/2019 </para>
''' </remarks>
<Description("Folder and file explorer"), System.Drawing.ToolboxBitmap(GetType(FileExplorerView), "FileExplorerView.gif")>
Public Class FileExplorerView
    Inherits Forma.ModelViewBase

#Region " CONSTRUCTION "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        'This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False
        AddHandler Me.FolderExplorer.FolderTreeView.PathChanged, AddressOf Me.FolderTreeViewControlPathChanged
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the isr.Core.Forma.ModelViewBase and optionally
    ''' releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me._Components IsNot Nothing Then Me._Components.Dispose() : Me._Components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " DESIGNER "

    ''' <summary> The splitter. </summary>
    Private _Splitter As System.Windows.Forms.Splitter

        Private WithEvents FolderExplorer As FolderTreeViewControl

        Friend WithEvents FileListViewControl As FileListViewControl

    ''' <summary> The components. </summary>
    Private _Components As System.ComponentModel.IContainer

    ''' <summary>
    ''' Required method for Designer support - do not modify the contents of this method with the
    ''' code editor.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Sub InitializeComponent()
        Me.FolderExplorer = New isr.Core.Controls.FolderTreeViewControl()
        Me._Splitter = New System.Windows.Forms.Splitter()
        Me.FileListViewControl = New isr.Core.Controls.FileListViewControl()
        Me.SuspendLayout()
        '
        '_FolderExplorer
        '
        Me.FolderExplorer.BackColor = System.Drawing.SystemColors.Control
        Me.FolderExplorer.Dock = System.Windows.Forms.DockStyle.Left
        Me.FolderExplorer.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FolderExplorer.Location = New System.Drawing.Point(0, 0)
        Me.FolderExplorer.Name = "_FolderExplorer"
        Me.FolderExplorer.ShowCurrentPathTextBox = True
        Me.FolderExplorer.ShowMyDocuments = True
        Me.FolderExplorer.ShowMyFavorites = True
        Me.FolderExplorer.ShowMyNetwork = True
        Me.FolderExplorer.ShowToolBar = True
        Me.FolderExplorer.Size = New System.Drawing.Size(168, 357)
        Me.FolderExplorer.TabIndex = 0
        '
        '_Splitter
        '
        Me._Splitter.Location = New System.Drawing.Point(168, 0)
        Me._Splitter.Name = "_Splitter"
        Me._Splitter.Size = New System.Drawing.Size(3, 357)
        Me._Splitter.TabIndex = 3
        Me._Splitter.TabStop = False
        '
        '_FileListViewControl
        '
        Me.FileListViewControl.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FileListViewControl.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FileListViewControl.HideFileInfo = False
        Me.FileListViewControl.Location = New System.Drawing.Point(171, 0)
        Me.FileListViewControl.Name = "_FileListViewControl"
        Me.FileListViewControl.DirectoryPath = Nothing
        Me.FileListViewControl.SearchPatternDelimiter = Global.Microsoft.VisualBasic.ChrW(59)
        Me.FileListViewControl.SearchPatterns = "*.*;*.bmp, *.jpg, *.png;*.csv, *.txt"
        Me.FileListViewControl.Size = New System.Drawing.Size(429, 357)
        Me.FileListViewControl.TabIndex = 1
        '
        'FileExplorerView
        '
        Me.Controls.Add(Me.FileListViewControl)
        Me.Controls.Add(Me._Splitter)
        Me.Controls.Add(Me.FolderExplorer)
        Me.Name = "FileExplorerView"
        Me.Size = New System.Drawing.Size(600, 357)
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region " CONTROL EVENTS "

    ''' <summary> Select home. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub SelectHome()
        Me.FolderExplorer.FolderTreeView.TrySelectHome()
    End Sub

    ''' <summary>
    ''' Forces the control to invalidate its client area and immediately redraw itself and any child
    ''' controls.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Overrides Sub Refresh()
        MyBase.Refresh()
        Me.FolderExplorer.Refresh()
    End Sub

    ''' <summary> Handles the path change. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Tree view event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub FolderTreeViewControlPathChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'Populate folders and files when a folder is selected
        Me.Cursor = Cursors.WaitCursor
        Try
            Me.FileListViewControl.DirectoryPath = Me.FolderExplorer.SelectedPath
            Me.FileListViewControl.Refresh()
            ' Me._FileListViewControl.FileListView.PopulateFiles(Me._FolderExplorer.SelectedPath)
        Catch ex As UnauthorizedAccessException
            MessageBox.Show($"Access denied to folder {Me.FolderExplorer.SelectedPath}", "Access denied", MessageBoxButtons.OK,
                            MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        Catch ex As Exception
            MessageBox.Show($"Error: {ex}")
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

End Class

