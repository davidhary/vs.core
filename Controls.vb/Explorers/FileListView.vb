Imports System.IO
Imports System.ComponentModel

''' <summary> A file list view. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 7/4/2019 </para>
''' </remarks>
<Description("File list view"), System.Drawing.ToolboxBitmap(GetType(FileListView), "FileListView.gif")>
Public Class FileListView
    Inherits Windows.Forms.ListView

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        MyBase.New()
        Me.SmallImageList = New ImageList()
        Me.SmallImageList.Images.Add(My.Resources.file)
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the isr.Core.Forma.ModelViewBase and optionally
    ''' releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " CONSTRUCTION "

    ''' <summary> Initializes the list view. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub InitListView()
        ' initialize the file list view 
        Me.Clear() 'clear control
        'create column header for ListView
        Me.Columns.Add("Name", 150, System.Windows.Forms.HorizontalAlignment.Left)
        Me.Columns.Add("Size", 75, System.Windows.Forms.HorizontalAlignment.Right)
        Me.Columns.Add("Created", 140, System.Windows.Forms.HorizontalAlignment.Left)
        Me.Columns.Add("Modified", 140, System.Windows.Forms.HorizontalAlignment.Left)
    End Sub

    ''' <summary> Parse path name. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="pathName"> Full pathname. </param>
    ''' <returns> A String. </returns>
    Private Shared Function ParsePathName(ByVal pathName As String) As String
        If String.IsNullOrWhiteSpace(pathName) Then
            Return pathName
        Else
            Dim stringSplit() As String = pathName.Split("\"c)
            Dim _maxIndex As Integer = stringSplit.Length
            Return stringSplit(_maxIndex - 1)
        End If
    End Function

    ''' <summary> Populate files. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="path"> Full pathname of the file. </param>
    Public Sub PopulateFiles(ByVal path As String)
        Me.PopulateFiles(path, "*.*")
    End Sub

    ''' <summary> Enumerates select files in this collection. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="path">           Full pathname of the file. </param>
    ''' <param name="searchPatterns"> The search string to match against the names of files in path.
    '''                               This parameter can contain a combination of valid literal
    '''                               path and wildcard (* and ?) characters (see Remarks), but
    '''                               doesn't support regular expressions. </param>
    ''' <param name="delimiter">      The delimiter. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process select files in this collection.
    ''' </returns>
    Private Shared Function SelectFiles(ByVal path As String, ByVal searchPatterns As String, ByVal delimiter As Char) As IList(Of String)
        Dim fileNames As New List(Of String)
        If String.IsNullOrWhiteSpace(searchPatterns) OrElse searchPatterns.Contains("*.*") Then
            fileNames = New List(Of String)(Directory.EnumerateFiles(path))
        Else
            For Each searchPattern As String In searchPatterns.Split(delimiter)
                fileNames.AddRange(Directory.EnumerateFiles(path, searchPattern))
            Next
            fileNames.Sort()
        End If
        Return fileNames
    End Function

    ''' <summary>
    ''' Converts a UTC timestamp to an explorer time, which is incorrect when daylight saving time
    ''' (DST) was in a different state to now.
    ''' </summary>
    ''' <remarks>
    ''' The file times are different between .NET and Explorer if daylight saving time (DST) was in a
    ''' different state to now.
    ''' .NET is processing this difference differently to Explorer and Scripting.FileSystemObject.
    ''' You can see a similar issue when extracting files from a zip file when DST Is different to
    ''' when the files were zipped. Via Reflector, the reason .NET differs from the non .NET code
    ''' paths Is that all three local timestamps in <see cref="IO.File"/> and
    ''' <see cref="IO.FileInfo"/>
    ''' are implemented As getting the UTC timestamp and applying .ToLocalTime, which determines the
    ''' offset to add based upon whether DST was happening in the local timezone For the UTC time. So,
    ''' in summary, .NET Is more correct. But if you want the incorrect, but same as Explorer, date
    ''' use This has been discussed On Raymond Chen's blog
    ''' http://blogs.msdn.com/b/oldnewthing/archive/2003/10/24/55413.aspx. Raymond's summary is:
    ''' Win32 does not attempt to guess which time zone rules were in effect at that other time. So
    ''' Win32 says, “Thursday, October 17, 2002 8:45:38 AM PST”. Note: Pacific Standard Time. Even
    ''' though October 17 was during Pacific Daylight Time, Win32 displays the time as standard time
    ''' because that’s what time it is now.
    ''' .NET says, “Well, if the rules in effect now were also in effect on October 17, 2003, then
    ''' that would be daylight time” so it displays “Thursday, October 17, 2003, 9:45 AM PDT” –
    ''' daylight time. So .NET gives a value which Is more intuitively correct, but Is also
    ''' potentially incorrect, And which Is Not invertible. Win32 gives a value which Is intuitively
    ''' incorrect, but Is strictly correct. In short: .NET is intuitively correct but not always
    ''' invertible, and Win32 is strictly correct and invertible).
    ''' https://stackoverflow.com/questions/4605983/io-file-getlastaccesstime-is-off-by-one-hour.
    ''' </remarks>
    ''' <param name="fileInfoUtcTime"> The file information UTC time. </param>
    ''' <returns> Corrected local time. </returns>
    Public Shared Function ToExplorerLocalTime(ByVal fileInfoUtcTime As DateTime) As DateTime
        Return fileInfoUtcTime.Add(TimeZone.CurrentTimeZone.GetUtcOffset(Now))
    End Function

    ''' <summary> Legacy to-explorer local time. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="fileInfoLocalTime"> The file information local time. </param>
    ''' <returns> A DateTime. </returns>
    Public Shared Function LegacyToExplorerLocalTime(ByVal fileInfoLocalTime As DateTime) As DateTime
        If TimeZone.CurrentTimeZone.IsDaylightSavingTime(fileInfoLocalTime) = False Then
            ' adjust time for day light saving time 
            Return fileInfoLocalTime.AddHours(1)
        Else
            ' not in day light saving time
            Return fileInfoLocalTime
        End If
    End Function

    ''' <summary> Populate files. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="path">           Full pathname of the file. </param>
    ''' <param name="searchPatterns"> The search string to match against the names of files in path.
    '''                               This parameter can contain a combination of valid literal
    '''                               path and wildcard (* and ?) characters (see Remarks), but
    '''                               doesn't support regular expressions. </param>
    Public Sub PopulateFiles(ByVal path As String, ByVal searchPatterns As String)

        ' Populate list view with files
        Dim columnValues(3) As String

        ' clear list
        Me.InitListView()

        'check path
        If Directory.Exists(path) Then
            Dim fileNames As IList(Of String) = FileListView.SelectFiles(path, searchPatterns, ","c)

            Dim fileName As String
            ' loop through all files
            For Each fileName In fileNames
                Dim fileInfo As New FileInfo(fileName)
                Dim fileSize As Long = fileInfo.Length

                ' create list view data
                columnValues(0) = FileListView.ParsePathName(fileName)
                columnValues(1) = If(Me.ShowSizeKilobytes,
                                     If(fileSize > 1024, $"{fileSize / 1024:N0} K", $"{fileSize:N0} B"),
                                                         fileSize.ToString("N0", Globalization.CultureInfo.CurrentCulture))

                ' file creation time
                columnValues(2) = FileListView.FormatFileDate(FileListView.ToExplorerLocalTime(fileInfo.CreationTimeUtc))

                ' file modify time
                columnValues(3) = FileListView.FormatFileDate(FileListView.ToExplorerLocalTime(fileInfo.LastWriteTimeUtc))

                ' Create actual list item
                Dim listViewItem As New ListViewItem(columnValues, 0) With {.Tag = fileName}
                Me.Items.Add(listViewItem)

            Next
        End If
    End Sub

    ''' <summary> Gets or sets the show size kilo bytes. </summary>
    ''' <value> The show size kilo bytes. </value>
    <Category("Options"), Description("Show size in Kilo Bytes."), DefaultValue(False), Browsable(True)>
    Public Property ShowSizeKilobytes As Boolean

    ''' <summary> Formats the date using short date and time values. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> The formatted date. </returns>
    Private Shared Function FormatFileDate(ByVal value As DateTime) As String
        Return $"{value.ToShortDateString()} {value.ToShortTimeString()}"
    End Function

#End Region

#Region " SELECTIONS "

    ''' <summary> Enumerates selected file names in this collection. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process selected file names in this
    ''' collection.
    ''' </returns>
    Public Function EnumerateSelectedFileNames() As IList(Of String)
        Dim result As New List(Of String)
        For Each item As ListViewItem In Me.SelectedItems
            result.Add(CStr(item.Tag))
        Next
        Return result
    End Function

#End Region

End Class

