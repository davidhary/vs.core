<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Public Class FolderTreeViewControl

    Private components As System.ComponentModel.IContainer

    ''' <summary> 
    ''' Required method for Designer support - do not modify 
    ''' the contents of this method with the code editor.
    ''' </summary>
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FolderTreeViewControl))
        Me._PathTextBox = New System.Windows.Forms.TextBox()
        Me._GoToDirectoryButton = New System.Windows.Forms.Button()
        Me._ImageList = New System.Windows.Forms.ImageList(Me.components)
        Me._ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._FolderTreeView = New isr.Core.Controls.FolderTreeView()
        Me._CurrentPathLayoutPanel = New System.Windows.Forms.TableLayoutPanel()
        Me._ToolStrip = New System.Windows.Forms.ToolStrip()
        Me._HomeToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me._FolderUpButton = New System.Windows.Forms.ToolStripButton()
        Me._BackToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me._ForwardToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me._AddFolderButton = New System.Windows.Forms.ToolStripButton()
        Me._RefreshToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me._CurrentPathLayoutPanel.SuspendLayout()
        Me._ToolStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_PathTextBox
        '
        Me._PathTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._PathTextBox.Location = New System.Drawing.Point(3, 3)
        Me._PathTextBox.Name = "_PathTextBox"
        Me._PathTextBox.Size = New System.Drawing.Size(276, 23)
        Me._PathTextBox.TabIndex = 0
        Me._ToolTip.SetToolTip(Me._PathTextBox, "Current directory")
        '
        '_GoToDirectoryButton
        '
        Me._GoToDirectoryButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._GoToDirectoryButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me._GoToDirectoryButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me._GoToDirectoryButton.ForeColor = System.Drawing.Color.White
        Me._GoToDirectoryButton.ImageIndex = 21
        Me._GoToDirectoryButton.ImageList = Me._ImageList
        Me._GoToDirectoryButton.Location = New System.Drawing.Point(285, 3)
        Me._GoToDirectoryButton.Name = "_GoToDirectoryButton"
        Me._GoToDirectoryButton.Size = New System.Drawing.Size(20, 22)
        Me._GoToDirectoryButton.TabIndex = 1
        Me._ToolTip.SetToolTip(Me._GoToDirectoryButton, "Go to the directory")
        '
        '_ImageList
        '
        Me._ImageList.ImageStream = CType(resources.GetObject("_ImageList.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me._ImageList.TransparentColor = System.Drawing.Color.Transparent
        Me._ImageList.Images.SetKeyName(0, "")
        Me._ImageList.Images.SetKeyName(1, "")
        Me._ImageList.Images.SetKeyName(2, "")
        Me._ImageList.Images.SetKeyName(3, "")
        Me._ImageList.Images.SetKeyName(4, "")
        Me._ImageList.Images.SetKeyName(5, "")
        Me._ImageList.Images.SetKeyName(6, "")
        Me._ImageList.Images.SetKeyName(7, "")
        Me._ImageList.Images.SetKeyName(8, "")
        Me._ImageList.Images.SetKeyName(9, "")
        Me._ImageList.Images.SetKeyName(10, "")
        Me._ImageList.Images.SetKeyName(11, "")
        Me._ImageList.Images.SetKeyName(12, "")
        Me._ImageList.Images.SetKeyName(13, "")
        Me._ImageList.Images.SetKeyName(14, "")
        Me._ImageList.Images.SetKeyName(15, "")
        Me._ImageList.Images.SetKeyName(16, "")
        Me._ImageList.Images.SetKeyName(17, "")
        Me._ImageList.Images.SetKeyName(18, "")
        Me._ImageList.Images.SetKeyName(19, "")
        Me._ImageList.Images.SetKeyName(20, "")
        Me._ImageList.Images.SetKeyName(21, "")
        Me._ImageList.Images.SetKeyName(22, "")
        Me._ImageList.Images.SetKeyName(23, "")
        Me._ImageList.Images.SetKeyName(24, "")
        Me._ImageList.Images.SetKeyName(25, "")
        Me._ImageList.Images.SetKeyName(26, "")
        Me._ImageList.Images.SetKeyName(27, "")
        Me._ImageList.Images.SetKeyName(28, "")
        '
        '_FolderTreeView
        '
        Me._FolderTreeView.Dock = System.Windows.Forms.DockStyle.Fill
        Me._FolderTreeView.ImageIndex = 0
        Me._FolderTreeView.Location = New System.Drawing.Point(0, 57)
        Me._FolderTreeView.Name = "_FolderTreeView"
        Me._FolderTreeView.SelectedImageIndex = 2
        Me._FolderTreeView.ShowLines = False
        Me._FolderTreeView.ShowRootLines = False
        Me._FolderTreeView.Size = New System.Drawing.Size(308, 382)
        Me._FolderTreeView.TabIndex = 2
        '
        '_CurrentPathLayoutPanel
        '
        Me._CurrentPathLayoutPanel.ColumnCount = 2
        Me._CurrentPathLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._CurrentPathLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._CurrentPathLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me._CurrentPathLayoutPanel.Controls.Add(Me._GoToDirectoryButton, 1, 0)
        Me._CurrentPathLayoutPanel.Controls.Add(Me._PathTextBox, 0, 0)
        Me._CurrentPathLayoutPanel.Dock = System.Windows.Forms.DockStyle.Top
        Me._CurrentPathLayoutPanel.Location = New System.Drawing.Point(0, 25)
        Me._CurrentPathLayoutPanel.Name = "_CurrentPathLayoutPanel"
        Me._CurrentPathLayoutPanel.RowCount = 2
        Me._CurrentPathLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._CurrentPathLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._CurrentPathLayoutPanel.Size = New System.Drawing.Size(308, 32)
        Me._CurrentPathLayoutPanel.TabIndex = 1
        '
        '_ToolStrip
        '
        Me._ToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._ToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me._ToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._HomeToolStripButton, Me._FolderUpButton, Me._BackToolStripButton, Me._ForwardToolStripButton, Me._AddFolderButton, Me._RefreshToolStripButton})
        Me._ToolStrip.Location = New System.Drawing.Point(0, 0)
        Me._ToolStrip.Name = "_ToolStrip"
        Me._ToolStrip.Size = New System.Drawing.Size(308, 25)
        Me._ToolStrip.TabIndex = 0
        Me._ToolStrip.Text = "ToolStrip1"
        '
        '_HomeToolStripButton
        '
        Me._HomeToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._HomeToolStripButton.Image = Global.isr.Core.Controls.My.Resources.Resources.go_up_8
        Me._HomeToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._HomeToolStripButton.Name = "_HomeToolStripButton"
        Me._HomeToolStripButton.Size = New System.Drawing.Size(23, 22)
        Me._HomeToolStripButton.Text = "Home"
        '
        '_FolderUpButton
        '
        Me._FolderUpButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._FolderUpButton.Image = Global.isr.Core.Controls.My.Resources.Resources.go_up_8
        Me._FolderUpButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._FolderUpButton.Name = "_FolderUpButton"
        Me._FolderUpButton.Size = New System.Drawing.Size(23, 22)
        Me._FolderUpButton.Text = "Up"
        '
        '_BackToolStripButton
        '
        Me._BackToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._BackToolStripButton.Image = Global.isr.Core.Controls.My.Resources.Resources.go_up_8
        Me._BackToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._BackToolStripButton.Name = "_BackToolStripButton"
        Me._BackToolStripButton.Size = New System.Drawing.Size(23, 22)
        Me._BackToolStripButton.Text = "Back"
        '
        '_ForwardToolStripButton
        '
        Me._ForwardToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._ForwardToolStripButton.Image = Global.isr.Core.Controls.My.Resources.Resources.go_up_8
        Me._ForwardToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ForwardToolStripButton.Name = "_ForwardToolStripButton"
        Me._ForwardToolStripButton.Size = New System.Drawing.Size(23, 22)
        Me._ForwardToolStripButton.Text = "Forward"
        '
        '_AddFolderButton
        '
        Me._AddFolderButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._AddFolderButton.Image = Global.isr.Core.Controls.My.Resources.Resources.go_up_8
        Me._AddFolderButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._AddFolderButton.Name = "_AddFolderButton"
        Me._AddFolderButton.Size = New System.Drawing.Size(23, 22)
        Me._AddFolderButton.Text = "Add"
        '
        '_RefreshToolStripButton
        '
        Me._RefreshToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._RefreshToolStripButton.Image = Global.isr.Core.Controls.My.Resources.Resources.go_up_8
        Me._RefreshToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._RefreshToolStripButton.Name = "_RefreshToolStripButton"
        Me._RefreshToolStripButton.Size = New System.Drawing.Size(23, 22)
        Me._RefreshToolStripButton.Text = "Refresh"
        '
        'FolderTreeViewControl
        '
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.Controls.Add(Me._FolderTreeView)
        Me.Controls.Add(Me._CurrentPathLayoutPanel)
        Me.Controls.Add(Me._ToolStrip)
        Me.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "FolderTreeViewControl"
        Me.Size = New System.Drawing.Size(308, 439)
        Me._CurrentPathLayoutPanel.ResumeLayout(False)
        Me._CurrentPathLayoutPanel.PerformLayout()
        Me._ToolStrip.ResumeLayout(False)
        Me._ToolStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub


    Private WithEvents _GoToDirectoryButton As System.Windows.Forms.Button
    Private _ToolTip As System.Windows.Forms.ToolTip
    Private _ImageList As System.Windows.Forms.ImageList
    Private WithEvents _PathTextBox As System.Windows.Forms.TextBox
    Private WithEvents _CurrentPathLayoutPanel As TableLayoutPanel
    Private WithEvents _FolderUpButton As Windows.Forms.ToolStripButton
    Private WithEvents _FolderTreeView As FolderTreeView
    Private WithEvents _ToolStrip As ToolStrip
    Private WithEvents _HomeToolStripButton As Windows.Forms.ToolStripButton
    Private WithEvents _BackToolStripButton As Windows.Forms.ToolStripButton
    Private WithEvents _ForwardToolStripButton As Windows.Forms.ToolStripButton
    Private WithEvents _AddFolderButton As Windows.Forms.ToolStripButton
    Private WithEvents _RefreshToolStripButton As Windows.Forms.ToolStripButton
End Class
