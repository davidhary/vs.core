''' <summary> A folder file selector. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 7/3/2019 </para>
''' </remarks>
Public Class FolderFileSelector
    Inherits Forma.ModelViewBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="FolderFileSelector" /> class from being created.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Sub New()

        MyBase.New()

        ' This method is required by the Windows Form Designer.
        Me.InitializeComponent()

    End Sub

#End Region

#Region " METHODS  and  PROPERTIES "

    ''' <summary> Adds a file name pattern. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="pattern"> Specify the file type and pattern such as "Data Files (*.dat)" or
    '''                        "Pictures (*.bmp; *.jpg)". </param>
    Public Sub AddFilePattern(ByVal pattern As String)

        If String.IsNullOrWhiteSpace(pattern) Then
            Me._FilePatternComboBox.Items.Clear()
        Else
            ' Add an item to the combo box.
            Me._FilePatternComboBox.Items.Add(pattern)
        End If

    End Sub

    ''' <summary> Gets or sets the caption of the form. </summary>
    ''' <value> The caption. </value>
    Public Property Caption() As String
        Get
            Return Me.Text
        End Get
        Set(ByVal value As String)
            Me.Text = value
        End Set
    End Property

    ''' <summary> Gets or sets the data file list selection mode. </summary>
    ''' <value> The selection mode. </value>
    Public Property SelectionMode() As Windows.Forms.SelectionMode
        Get
            Return Me._DataFileListBox.SelectionMode
        End Get
        Set(ByVal value As Windows.Forms.SelectionMode)
            Me._DataFileListBox.SelectionMode = value
        End Set
    End Property

    ''' <summary> Returns the number of selected files. </summary>
    ''' <value> The selected count. </value>
    Public ReadOnly Property SelectedCount() As Int32
        Get
            Return Me._DataFileListBox.SelectedItems.Count
        End Get
    End Property

    ''' <summary> Gets the array of selected files. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> System.String[][]. </returns>
    Public Function SelectedFiles() As String()

        Dim selected(Me._DataFileListBox.SelectedItems.Count - 1) As String
        Me._DataFileListBox.SelectedItems.CopyTo(selected, 0)
        Return selected

    End Function

    ''' <summary> Returns the selected file names. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> System.String[][]. </returns>
    Public Function SelectedFilePathNames() As String()
        If Me.SelectedFiles Is Nothing OrElse Me.SelectedFiles.Length = 0 Then
#Disable Warning CA1825 ' Avoid zero-length array allocations.
            Return New String() {}
#Enable Warning CA1825 ' Avoid zero-length array allocations.
        Else
            Dim filePathNames As New Generic.List(Of String)
            For Each fileName As String In Me.SelectedFiles
                filePathNames.Add(System.IO.Path.Combine(Me.SelectedPathName, fileName))
            Next
            Return filePathNames.ToArray
        End If
    End Function

    ''' <summary> Returns the path of the selected file. </summary>
    ''' <value> The name of the selected path. </value>
    Public Property SelectedPathName() As String
        Get
            ' Set file path.
            Return Me._DataFileListBox.DirectoryPath
        End Get
        Set(ByVal value As String)
            Me._FolderTextBox.Text = value
            If System.IO.Directory.Exists(value) Then
                ' clear the previous path so that we get a refresh
                Me._DataFileListBox.DirectoryPath = System.Environment.CurrentDirectory
                Me._DataFileListBox.Invalidate()
                For i As Int32 = 1 To 1000
                    isr.Core.My.MyLibrary.DoEvents()
                Next
                Me._DataFileListBox.DirectoryPath = value
                Me._DataFileListBox.Invalidate()
            End If
        End Set
    End Property

    ''' <summary> Returns the name of the select file. </summary>
    ''' <value> The selected filename. </value>
    Public Property SelectedFileName() As String
        Get
            ' return the file title.
            Return Me._FilePathNameTextBox.Text
        End Get
        Set(ByVal value As String)
            Me._FilePathNameTextBox.Text = value
        End Set
    End Property

    ''' <summary> Returns the title of the select file and path. </summary>
    ''' <value> The name of the selected file path. </value>
    Public ReadOnly Property SelectedFilePathName() As String
        Get
            ' return the file title.
            Return System.IO.Path.Combine(Me.SelectedPathName, Me.SelectedFileName)
            ' Return String.Format(Globalization.CultureInfo.CurrentCulture, "{0}\{1}", Me.SelectedPathName, Me.SelectedFilename)
        End Get
    End Property

    ''' <summary> Returns the title of the select file. </summary>
    ''' <value> The selected file title. </value>
    Public ReadOnly Property SelectedFileTitle() As String
        Get
            ' return the file title.
            Return If(String.IsNullOrWhiteSpace(Me.SelectedFileName),
                String.Empty,
                Me.SelectedFileName.Substring(0, Me.SelectedFileName.LastIndexOf(".", StringComparison.OrdinalIgnoreCase)))
        End Get
    End Property

    ''' <summary> Returns the file type pattern. </summary>
    ''' <value> The pattern. </value>
    Public Property Pattern() As String
        Get
            Return Me._FilePatternComboBox.Text
        End Get
        Set(ByVal value As String)
            Me._FilePatternComboBox.Text = value
            If Not String.IsNullOrWhiteSpace(value) Then
                Dim firstLocation As Integer = value.IndexOf("(", StringComparison.OrdinalIgnoreCase) + 1
                Dim patternLength As Integer = value.IndexOf(")", firstLocation, StringComparison.OrdinalIgnoreCase) - firstLocation
                Me._DataFileListBox.SearchPattern = value.Substring(firstLocation, patternLength)
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the status message. </summary>
    ''' <remarks> Use this property to get the status message generated by the object. </remarks>
    ''' <value> A <see cref="System.String">String</see>. </value>
    Public Property StatusMessage() As String

#End Region

#Region " PRIVATE  and  PROTECTED "

    ''' <summary> Verifies the file name and provides alerts as necessary. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> <c>true</c> if okay, <c>false</c> otherwise. </returns>
    Public Function VerifyFileName() As Boolean

        ' Set file name for testing (faster than using the Text property over again.
        Dim fileTitle As String = Me._FilePathNameTextBox.Text.Trim

        ' clear the Annunciator
        Me._Annunciator.SetError(Me._FilePathNameTextBox, "")

        ' Check if file name was given.
        If fileTitle Is Nothing OrElse fileTitle.Length < 2 Then

            ' If file is not given, alert and exit:
            Me._StatusMessage = "Please select or enter a file name or use Cancel to exit this dialog box."
            Me._Annunciator.SetIconAlignment(Me._FilePathNameTextBox, ErrorIconAlignment.TopRight)
            Me._Annunciator.SetError(Me._FilePathNameTextBox, "")
            Me._Annunciator.SetError(Me._FilePathNameTextBox, Me.StatusMessage)
            Return False

        Else

            Try

                ' This will create an error if the file name is bad.
                Return System.IO.File.Exists(Me.SelectedFilePathName)

            Catch ex As System.IO.IOException

                ' If error in file name, alert:
                Me._StatusMessage = "Please enter a correct file name!"
                Me._Annunciator.SetIconAlignment(Me._FilePathNameTextBox, ErrorIconAlignment.TopLeft)
                Me._Annunciator.SetError(Me._FilePathNameTextBox, "")
                Me._Annunciator.SetError(Me._FilePathNameTextBox, Me.StatusMessage)
                Return False

            End Try

            ' Check for wild cards in the file name.
            If fileTitle.IndexOf("*"c) + fileTitle.IndexOf("?"c) > 0 Then

                ' If wild cards in file name, alert and exit:
                Me._StatusMessage = "Please enter a complete file name!"
                Me._Annunciator.SetIconAlignment(Me._FilePathNameTextBox, ErrorIconAlignment.TopLeft)
                Me._Annunciator.SetError(Me._FilePathNameTextBox, "")
                Me._Annunciator.SetError(Me._FilePathNameTextBox, Me.StatusMessage)
                Return False
            End If

        End If

    End Function

    ''' <summary> Event queue for all listeners interested in FileSelected events. </summary>
    Public Event FileSelected As EventHandler(Of System.IO.FileSystemEventArgs)

    ''' <summary> Update data for the specified file name. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="filePathName"> Name of the file path. </param>
    Private Sub UpdateData(ByVal filePathName As String)

        ' clear the Annunciator
        Me._Annunciator.SetError(Me._FilePathNameTextBox, "")

        If Not System.IO.File.Exists(filePathName) Then
            Me._Annunciator.SetIconAlignment(Me._FilePathNameTextBox, ErrorIconAlignment.TopLeft)
            Me._Annunciator.SetError(Me._FilePathNameTextBox, "")
            Me.StatusMessage = "File not found"
            Me._Annunciator.SetError(Me._FilePathNameTextBox, Me.StatusMessage)
            Return
        End If

        Try
            Dim evt As EventHandler(Of System.IO.FileSystemEventArgs) = Me.FileSelectedEvent
            evt.Invoke(Me, New IO.FileSystemEventArgs(IO.WatcherChangeTypes.Changed, Me.SelectedPathName, Me.SelectedFileName))
        Catch
            Throw
        Finally

            Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    ''' <summary> Displays a file information described by value. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub DisplayFileInfo(ByVal value As String)
        Me._FileInfoTextBox.Text = value
    End Sub

    ''' <summary> Displays a file information rich text described by value. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub DisplayFileInfoRichText(ByVal value As String)
        Me._FileInfoTextBox.Rtf = value
    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary> Handles the SelectedIndexChanged event of the Me._dataFileListBox control. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Private Sub DataFileListBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _DataFileListBox.SelectedIndexChanged

        ' Set the file name text box.
        Me._FilePathNameTextBox.Text = Me._DataFileListBox.SelectedFilePath
        Me.UpdateData(Me.SelectedFilePathName)

    End Sub

    ''' <summary> Updates and exits. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Private Sub DataFileListBox_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _DataFileListBox.DoubleClick

        ' Just update the file and try to exit.
        Me.DataFileListBox_SelectedIndexChanged(Me._DataFileListBox, New System.EventArgs)

        ' allow other threads to execute.
        isr.Core.My.MyLibrary.DoEvents()

    End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Me._filePatternComboBox control.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Private Sub FilePatternComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _FilePatternComboBox.SelectedIndexChanged

        ' clear the Annunciator
        Me._Annunciator.SetError(Me._FilePatternComboBox, "")
        Me._Annunciator.SetError(Me._FilePathNameTextBox, "")

        ' Reset the file pattern upon a click on the Pattern combo box.
        Me.Pattern = Me._FilePatternComboBox.Text

    End Sub

    ''' <summary> Handles the TextChanged event of the fileTitleTextBox control. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="EventArgs" /> instance containing the event data. </param>
    Private Sub FileTitleTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _FilePathNameTextBox.TextChanged

        ' clear the Annunciator
        Me._Annunciator.SetError(Me._FilePathNameTextBox, "")

        Dim newFileName As String = Me._FilePathNameTextBox.Text

        ' Check if wild cards in file name.
        If newFileName.IndexOf("*", StringComparison.OrdinalIgnoreCase) +
            newFileName.IndexOf("?", StringComparison.OrdinalIgnoreCase) > 0 Then

            ' If so, reset the file pattern.
            Me._DataFileListBox.SearchPattern = newFileName

        End If

    End Sub

    ''' <summary>
    ''' Handles the Enter event of the edit text box controls. Selects the control text.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Private Sub EditTextBox_Enter(ByVal sender As System.Object,
                                  ByVal e As System.EventArgs) Handles _FilePathNameTextBox.Enter, _FolderTextBox.Enter
        Dim editBox As TextBox = CType(sender, TextBox)
        editBox.SelectionStart = 0
        editBox.SelectionLength = editBox.Text.Length

    End Sub

    ''' <summary> Handles the TextChanged event of the Me._folderTextBox control. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Private Sub FolderTextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _FolderTextBox.TextChanged
        If System.IO.Directory.Exists(Me._FolderTextBox.Text) Then
            Me.SelectedPathName = Me._FolderTextBox.Text
        End If
    End Sub

    ''' <summary> Handles the Click event of the Me._refreshButton control. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Private Sub RefreshButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _RefreshButton.Click

        ' clear the Annunciator
        Me._Annunciator.SetError(Me._FilePathNameTextBox, "")

        Me.SelectedPathName = Me.SelectedPathName

    End Sub

    ''' <summary> Gets or sets the full pathname of the parent folder file. </summary>
    ''' <value> The full pathname of the parent folder file. </value>
    Public Property ParentFolderPathName As String

    ''' <summary> Handles the Click event of the Me._selectFolderButton control. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Private Sub SelectFolderButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _SelectFolderButton.Click

        ' clear the Annunciator
        Me._Annunciator.SetError(Me._FilePathNameTextBox, "")

        Using browser As FolderBrowserDialog = New FolderBrowserDialog
            ' browser.Description = "Select folder for files to retrieve"
            browser.RootFolder = Environment.SpecialFolder.MyComputer
            ' set the default path
            browser.SelectedPath = If(System.IO.Directory.Exists(Me.ParentFolderPathName), Me.ParentFolderPathName, Me._DataFileListBox.DirectoryPath)
            browser.ShowNewFolderButton = True
            If browser.ShowDialog = DialogResult.OK Then
                ' Set the path label
                Me.SelectedPathName = browser.SelectedPath
            End If
        End Using

    End Sub

#End Region

End Class
