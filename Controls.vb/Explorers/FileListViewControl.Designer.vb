﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FileListViewControl

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me._ToolStrip = New System.Windows.Forms.ToolStrip()
        Me._SearchPatternComboBox = New System.Windows.Forms.ToolStripComboBox()
        Me._RefreshButton = New System.Windows.Forms.ToolStripButton()
        Me._FileListView = New isr.Core.Controls.FileListView()
        Me._Split = New System.Windows.Forms.SplitContainer()
        Me._FileInfoRichTextBox = New System.Windows.Forms.RichTextBox()
        Me._ToolStrip.SuspendLayout()
        CType(Me._Split, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._Split.Panel1.SuspendLayout()
        Me._Split.Panel2.SuspendLayout()
        Me._Split.SuspendLayout()
        Me.SuspendLayout()
        '
        '_ToolStrip
        '
        Me._ToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._ToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me._ToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._SearchPatternComboBox, Me._RefreshButton})
        Me._ToolStrip.Location = New System.Drawing.Point(0, 0)
        Me._ToolStrip.Name = "_ToolStrip"
        Me._ToolStrip.Size = New System.Drawing.Size(301, 25)
        Me._ToolStrip.Stretch = True
        Me._ToolStrip.TabIndex = 0
        Me._ToolStrip.Text = "Tool Strip"
        '
        '_SearchPatternComboBox
        '
        Me._SearchPatternComboBox.Items.AddRange(New Object() {"*.*", "*.bmp, *.jpg, *.png", "*.csv, *.txt"})
        Me._SearchPatternComboBox.Name = "_SearchPatternComboBox"
        Me._SearchPatternComboBox.Size = New System.Drawing.Size(140, 25)
        Me._SearchPatternComboBox.Text = "*.*"
        Me._SearchPatternComboBox.ToolTipText = "Search patterns"
        '
        '_RefreshButton
        '
        Me._RefreshButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._RefreshButton.Image = Global.isr.Core.Controls.My.Resources.Resources.arrow_refresh_small
        Me._RefreshButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me._RefreshButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._RefreshButton.Name = "_RefreshButton"
        Me._RefreshButton.Size = New System.Drawing.Size(23, 22)
        Me._RefreshButton.Text = "Refresh"
        '
        '_FileListView
        '
        Me._FileListView.Dock = System.Windows.Forms.DockStyle.Fill
        Me._FileListView.HideSelection = False
        Me._FileListView.Location = New System.Drawing.Point(0, 0)
        Me._FileListView.Name = "_FileListView"
        Me._FileListView.Size = New System.Drawing.Size(301, 238)
        Me._FileListView.TabIndex = 0
        Me._FileListView.UseCompatibleStateImageBehavior = False
        Me._FileListView.View = System.Windows.Forms.View.Details
        '
        '_Split
        '
        Me._Split.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Split.Location = New System.Drawing.Point(0, 25)
        Me._Split.Name = "_Split"
        Me._Split.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        '_Split.Panel1
        '
        Me._Split.Panel1.Controls.Add(Me._FileListView)
        '
        '_Split.Panel2
        '
        Me._Split.Panel2.Controls.Add(Me._FileInfoRichTextBox)
        Me._Split.Panel2MinSize = 5
        Me._Split.Size = New System.Drawing.Size(301, 472)
        Me._Split.SplitterDistance = 238
        Me._Split.SplitterWidth = 5
        Me._Split.TabIndex = 1
        '
        '_FileInfoRichTextBox
        '
        Me._FileInfoRichTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._FileInfoRichTextBox.Location = New System.Drawing.Point(0, 0)
        Me._FileInfoRichTextBox.Name = "_FileInfoRichTextBox"
        Me._FileInfoRichTextBox.Size = New System.Drawing.Size(301, 229)
        Me._FileInfoRichTextBox.TabIndex = 0
        Me._FileInfoRichTextBox.Text = "file info"
        '
        'FileListViewControl
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me._Split)
        Me.Controls.Add(Me._ToolStrip)
        Me.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "FileListViewControl"
        Me.Size = New System.Drawing.Size(301, 497)
        Me._ToolStrip.ResumeLayout(False)
        Me._ToolStrip.PerformLayout()
        Me._Split.Panel1.ResumeLayout(False)
        Me._Split.Panel2.ResumeLayout(False)
        CType(Me._Split, System.ComponentModel.ISupportInitialize).EndInit()
        Me._Split.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Private WithEvents _ToolStrip As ToolStrip
    Private WithEvents _SearchPatternComboBox As Windows.Forms.ToolStripComboBox
    Private WithEvents _RefreshButton As Windows.Forms.ToolStripButton
    Private WithEvents _Split As SplitContainer
    Private WithEvents _FileInfoRichTextBox As Windows.Forms.RichTextBox
    Private WithEvents _FileListView As FileListView
End Class
