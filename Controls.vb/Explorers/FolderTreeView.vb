Imports System.ComponentModel
Imports System.IO

''' <summary> Implements a tree view for exploring folders. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 7/6/2019 </para><para>
''' David, 7/3/2019, Uses tree view from
''' https://www.codeproject.com/Articles/14570/A-Windows-Explorer-in-a-user-control
''' </para>
''' </remarks>
<ToolboxBitmapAttribute(GetType(FolderTreeView), "FolderTreeView.gif"), DefaultEvent("PathChanged")>
Partial Public Class FolderTreeView
    Inherits Windows.Forms.TreeView

#Region " CONSTRUCTION And CLEANUP "

    ''' <summary> Gets or sets the initlizing components. </summary>
    ''' <value> The initlizing components. </value>
    Private Property InitlizingComponents As Boolean

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        MyBase.New
        Me.InitlizingComponents = True
        Me.ShowLines = False
        Me.ShowRootLines = False
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FolderTreeView))
        Me.ImageList = New System.Windows.Forms.ImageList() With {.ImageStream = CType(resources.GetObject("_ImageList.ImageStream"), System.Windows.Forms.ImageListStreamer),
            .TransparentColor = System.Drawing.Color.Transparent}
        For i As Integer = 0 To 28
            Me.ImageList.Images.SetKeyName(i, String.Empty)
        Next

        ' List view managers the contiguous list of paths.
        Me._PathColumnHeader = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me._StatusColumnHeader = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me._ListView = New System.Windows.Forms.ListView With {
            .Name = "_ListView",
            .HideSelection = False,
            .UseCompatibleStateImageBehavior = False,
            .View = System.Windows.Forms.View.Details,
            .Visible = False
        }
        Me._ListView.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me._PathColumnHeader, Me._StatusColumnHeader})

        ' Context menu
        MyBase.ContextMenuStrip = Me.CreateContextMenuStrip()

        Me.ImageIndex = 0
        Me.SelectedImageIndex = 2
        Me.InitlizingComponents = False

    End Sub

    ''' <summary> Clean up any resources being used. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="disposing"> <see langword="true" /> to release both managed and unmanaged
    '''                          resources; <see langword="false" /> to release only unmanaged
    '''                          resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            Me.ContextMenuStrip = Nothing
            If Me._MyContextMenuStrip IsNot Nothing Then Me._MyContextMenuStrip.Dispose()
            If Me._ListView IsNot Nothing Then Me._ListView.Dispose()
            Me._ListView = Nothing
        End If
        MyBase.Dispose(disposing)
    End Sub

    ''' <summary> The list view control. </summary>
    Private _ListView As System.Windows.Forms.ListView

    ''' <summary> The path column header. </summary>
    Private ReadOnly _PathColumnHeader As System.Windows.Forms.ColumnHeader

    ''' <summary> The status column header. </summary>
    Private ReadOnly _StatusColumnHeader As System.Windows.Forms.ColumnHeader

#End Region

#Region " APPEARANCE "

    ''' <summary> True to show, false to hide my documents. </summary>
    Private _ShowMyDocuments As Boolean = True

    ''' <summary> Show my documents. </summary>
    ''' <value> The show my documents. </value>
    <Category("Appearance"), Description("Show my documents"), DefaultValue(True)>
    Public Property ShowMyDocuments() As Boolean
        Get
            Return Me._ShowMyDocuments
        End Get
        Set(ByVal value As Boolean)
            If value <> Me.ShowMyDocuments Then
                Me._ShowMyDocuments = value
                If Me.IsInitialized Then Me.Refresh()
            End If
        End Set
    End Property

    ''' <summary> True to show, false to hide my favorites. </summary>
    Private _ShowMyFavorites As Boolean = True

    ''' <summary> True to show, false to hide my favorites. </summary>
    ''' <value> The show my favorites. </value>
    <Category("Appearance"), Description("Show my favorites"), DefaultValue(True)>
    Public Property ShowMyFavorites() As Boolean
        Get
            Return Me._ShowMyFavorites
        End Get
        Set(ByVal value As Boolean)
            If value <> Me.ShowMyFavorites Then
                Me._ShowMyFavorites = value
                If Me.IsInitialized Then Me.Refresh()
            End If
        End Set
    End Property

    ''' <summary> True to show, false to hide my network. </summary>
    Private _ShowMyNetwork As Boolean = True

    ''' <summary> True to show, false to hide my network. </summary>
    ''' <value> The show my network. </value>
    <Category("Appearance"), Description("Show my network"), DefaultValue(True)>
    Public Property ShowMyNetwork() As Boolean
        Get
            Return Me._ShowMyNetwork
        End Get
        Set(ByVal value As Boolean)
            If value <> Me.ShowMyNetwork Then
                Me._ShowMyNetwork = value
                If Me.IsInitialized Then Me.Refresh()
            End If
        End Set
    End Property

#End Region

#Region " EVENTS "

    ''' <summary> Event queue for all listeners interested in PathChanged events. </summary>
    Public Event PathChanged As EventHandler(Of System.EventArgs)

    ''' <summary> Notifies the path changed. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Protected Sub NotifyPathChanged()
        If Not Me.InitlizingComponents Then
            Dim evt As EventHandler(Of System.EventArgs) = Me.PathChangedEvent
            evt?.Invoke(Me, System.EventArgs.Empty)
        End If
    End Sub

#End Region

#Region " BEHAVIOR "

    ''' <summary> Name of the 'Home' path </summary>
    Public Const HomePathName As String = "home"

    ''' <summary> Full pathname of the selected file. </summary>
    Private _SelectedPath As String

    ''' <summary> Gets or sets the full pathname of the selected directory. </summary>
    ''' <value> The full pathname of the selected directory. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property SelectedPath As String
        Get
            Return Me._SelectedPath
        End Get
        Protected Set(value As String)
            If Not String.Equals(value, Me.SelectedPath, StringComparison.OrdinalIgnoreCase) Then
                Me._CandidatePath = value
                Me._SelectedPath = value
                Me.NotifyPathChanged()
            End If
        End Set
    End Property

    ''' <summary> Select path. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="pathName"> Full pathname of the file. </param>
    Public Sub SelectPath(ByVal pathName As String)
        Me.SetCandidatePath(pathName)
        Me.SelectMyComputerTreeNode(Me.CandidatePath)
    End Sub

    ''' <summary> Full pathname of the candidate file. </summary>
    Private _CandidatePath As String

    ''' <summary> Gets or sets the full pathname of the Candidate path. </summary>
    ''' <value> The full pathname of the Candidate path. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property CandidatePath As String
        Get
            Return Me._CandidatePath
        End Get
        Protected Set(value As String)
            If Not String.Equals(value, Me.CandidatePath, StringComparison.OrdinalIgnoreCase) Then
                Me._CandidatePath = value
            End If
        End Set
    End Property

    ''' <summary> Sets candidate path. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="pathName"> Full pathname of the file. </param>
    Public Sub SetCandidatePath(ByVal pathName As String)
        Me.CandidatePath = If(String.Equals(pathName, FolderTreeView.HomePathName, StringComparison.OrdinalIgnoreCase),
                              Application.StartupPath, If(System.IO.Directory.Exists(pathName), pathName, Application.StartupPath))
    End Sub

    ''' <summary> my computer tree node. </summary>
    Private _MyComputerTreeNode As TreeNode

    ''' <summary> The root tree node. </summary>
    Private _RootTreeNode As TreeNode

    ''' <summary> Gets the is initialized. </summary>
    ''' <value> The is initialized. </value>
    Public ReadOnly Property IsInitialized As Boolean
        Get
            Return Me._RootTreeNode IsNot Nothing
        End Get
    End Property

    ''' <summary> Initializes the nodes and explores the desktop folder. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub InitializeKnownState()
        Me.Nodes.Clear()

        ' Dim drives() As String = Environment.GetLogicalDrives()

        'Environment.UserDomainName .GetFolderPath( 
        'Environment.GetFolderPath (Environment.SystemDirectory);

        Dim desktopNode As New TreeNode With {
            .Tag = Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
            .Text = "Desktop",
            .ImageIndex = 10,
            .SelectedImageIndex = 10
        }

        Me.Nodes.Add(desktopNode)
        Me._RootTreeNode = desktopNode

        If Me.ShowMyDocuments Then
            'Add My Documents and Desktop folder outside
            Dim myDocumentsNode As New TreeNode With {
                .Tag = Environment.GetFolderPath(Environment.SpecialFolder.Personal),
                .Text = "My Documents",
                .ImageIndex = 9,
                .SelectedImageIndex = 9
            }
            desktopNode.Nodes.Add(myDocumentsNode)
            FolderTreeView.TryFillFilesAndDirectories(myDocumentsNode)
        End If

        Dim myComputerNode As New TreeNode With {
            .Tag = "My Computer",
            .Text = "My Computer",
            .ImageIndex = 12,
            .SelectedImageIndex = 12
        }
        desktopNode.Nodes.Add(myComputerNode)
        myComputerNode.EnsureVisible()

        Me._MyComputerTreeNode = myComputerNode
        Dim myNode As New TreeNode With {
            .Tag = "my Node",
            .Text = "my Node",
            .ImageIndex = 12,
            .SelectedImageIndex = 12
        }
        myComputerNode.Nodes.Add(myNode)

        If Me.ShowMyNetwork Then

            Dim myNetworkPlacesNode As New TreeNode With {
                .Tag = "My Network Places",
                .Text = "My Network Places",
                .ImageIndex = 13,
                .SelectedImageIndex = 13
            }
            desktopNode.Nodes.Add(myNetworkPlacesNode)
            myNetworkPlacesNode.EnsureVisible()

            Dim entireNetworkNode As New TreeNode With {
                .Tag = "Entire Network",
                .Text = "Entire Network",
                .ImageIndex = 14,
                .SelectedImageIndex = 14
            }
            myNetworkPlacesNode.Nodes.Add(entireNetworkNode)

            Dim networkNode As New TreeNode With {
                .Tag = "Network Node",
                .Text = "Network Node",
                .ImageIndex = 15,
                .SelectedImageIndex = 15
            }
            entireNetworkNode.Nodes.Add(networkNode)

            entireNetworkNode.EnsureVisible()
        End If

        If Me.ShowMyFavorites Then
            Dim myFevoritesNode As New TreeNode With {
                .Tag = Environment.GetFolderPath(Environment.SpecialFolder.Favorites),
                .Text = "My Favorites",
                .ImageIndex = 26,
                .SelectedImageIndex = 26
            }
            desktopNode.Nodes.Add(myFevoritesNode)
            FolderTreeView.TryFillFilesAndDirectories(myFevoritesNode)
        End If
        FolderTreeView.ExploreTreeNode(desktopNode)

    End Sub

    ''' <summary> Explore tree node. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="parentNode"> The node. </param>
    Private Shared Sub ExploreTreeNode(ByVal parentNode As TreeNode)
        Cursor.Current = Cursors.WaitCursor
        Try
            'get directories
            FolderTreeView.TryFillFilesAndDirectories(parentNode)
            'get directories one more level deep in current directory so user can see there is
            'more directories underneath current directory 
            For Each subNode As TreeNode In parentNode.Nodes
                If String.Equals(parentNode.Text, "Desktop", StringComparison.OrdinalIgnoreCase) Then
                    If String.Equals(subNode.Text, "My Documents", StringComparison.OrdinalIgnoreCase) OrElse
                        String.Equals(subNode.Text, "My Computer", StringComparison.OrdinalIgnoreCase) OrElse
                        String.Equals(subNode.Text, "Microsoft Windows Network", StringComparison.OrdinalIgnoreCase) OrElse
                        String.Equals(subNode.Text, "My Network Places", StringComparison.OrdinalIgnoreCase) Then
                    Else
                        FolderTreeView.TryFillFilesAndDirectories(subNode)
                    End If
                Else
                    FolderTreeView.TryFillFilesAndDirectories(subNode)
                End If
            Next subNode
        Catch
            Throw
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub

    ''' <summary> Gets the directories. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="parentNode"> The parent node. </param>
    Private Shared Sub GetDirectories(ByVal parentNode As TreeNode)

        Dim dirList() As String = Directory.GetDirectories(parentNode.Tag.ToString())
        Array.Sort(dirList)

        ' check if directory already exists in case click same directory twice
        If dirList.Length <> parentNode.Nodes.Count Then
            ' add each directory in selected director
            For Each path As String In dirList
                parentNode.Nodes.Add(New TreeNode With {.Tag = path, 'store path in tag
                                 .Text = path.Substring(path.LastIndexOf("\", StringComparison.OrdinalIgnoreCase) + 1),
                                 .ImageIndex = 1})
            Next
        End If
    End Sub

    ''' <summary> Fill files and directories. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="communalNode"> The communal node. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Shared Function TryFillFilesAndDirectories(ByVal communalNode As TreeNode) As (Success As Boolean, Details As String)
        Try
            FolderTreeView.GetDirectories(communalNode)
            Return (True, String.Empty)
        Catch ex As Exception
            Return (False, ex.ToString)
        End Try
    End Function

    ''' <summary> Refresh folders. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub RefreshFolders()
        Me._ListView.Items.Clear()
        Me.Nodes.Clear()
        ' Me.SetCurrentPath(Environment.GetFolderPath(Environment.SpecialFolder.Personal))
        Me.InitializeKnownState()
    End Sub

    ''' <summary> Select my computer tree node. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="searchPath"> Full pathname of the search file. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SelectMyComputerTreeNode(ByVal searchPath As String)
        If Not Me.IsInitialized Then Me.InitializeKnownState()
        Dim h As Integer = 1
        Dim tn As TreeNode = Me._MyComputerTreeNode
        Dim selectedNode As TreeNode = Nothing
        Me.ExploreMyComputer()

StartAgain:
        Do
            For Each t As TreeNode In tn.Nodes
                Dim mypath As String = t.Tag.ToString()
                Dim mypathf As String = mypath
                If Not mypath.EndsWith("\", StringComparison.OrdinalIgnoreCase) Then
                    If searchPath.Length > mypathf.Length Then
                        mypathf = $"{mypath}\"
                    End If
                End If

                If searchPath.StartsWith(mypathf, StringComparison.OrdinalIgnoreCase) Then
                    t.TreeView.Focus()
                    ' t.TreeView.SelectedNode = t
                    t.EnsureVisible()
                    t.Expand()
                    If t.Nodes.Count >= 1 Then
                        t.Expand()
                        tn = t
                    Else
                        If String.Equals(searchPath, mypath, StringComparison.OrdinalIgnoreCase) Then
                            selectedNode = t
                            h = -1
                            Exit For
                        Else
                            Continue For
                        End If
                    End If

                    If mypathf.StartsWith(searchPath, StringComparison.OrdinalIgnoreCase) Then
                        selectedNode = t
                        h = -1
                        Exit For
                    Else
                        GoTo StartAgain
                        'return;
                    End If
                End If
            Next t

            Try
                tn = tn.NextNode
            Catch
            End Try

        Loop While h >= 0
        If selectedNode IsNot Nothing Then selectedNode.TreeView.SelectedNode = selectedNode
    End Sub

    ''' <summary> Adds a folder node to 'path'. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="name"> The name. </param>
    ''' <param name="path"> Full pathname of the file. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub AddFolderNode(ByVal name As String, ByVal path As String)

        Try
            Dim nodemyC As New TreeNode With {
                .Tag = path,
                .Text = name,
                .ImageIndex = 18,
                .SelectedImageIndex = 18
            }

            Me._RootTreeNode.Nodes.Add(nodemyC)

            Try
                'add directories under drive
                If Directory.Exists(path) Then
                    For Each dir As String In Directory.GetDirectories(path)
                        Dim newNode As New TreeNode With {
                            .Tag = dir,
                            .Text = dir.Substring(dir.LastIndexOf("\", StringComparison.OrdinalIgnoreCase) + 1),
                            .ImageIndex = 1
                        }
                        nodemyC.Nodes.Add(newNode)
                    Next dir
                End If
            Catch ex As Exception 'error just add blank directory
                MessageBox.Show("Error while Filling the Explorer:" & ex.Message)
            End Try
        Catch e As Exception
            MessageBox.Show(e.Message)
        End Try
    End Sub

    ''' <summary> Explore my computer. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ExploreMyComputer()

        If Not Me.IsInitialized Then Me.RefreshFolders()
        Dim drives() As String = Environment.GetLogicalDrives()
        Dim nodeDrive As TreeNode

        If Me._MyComputerTreeNode.GetNodeCount(True) < 2 Then
            Me._MyComputerTreeNode.FirstNode.Remove()

            For Each drive As String In drives
                nodeDrive = New TreeNode With {.Tag = drive}
                FolderTreeView.UpdateDriveImage(drive, nodeDrive)

                Me._MyComputerTreeNode.Nodes.Add(nodeDrive)
                Try
                    'add directories under drive
                    If Directory.Exists(drive) Then
                        For Each dir As String In Directory.GetDirectories(drive)
                            nodeDrive.Nodes.Add(New TreeNode With {.Tag = dir,
                                                .Text = dir.Substring(dir.LastIndexOf("\", StringComparison.OrdinalIgnoreCase) + 1),
                                                .ImageIndex = 1})
                        Next dir
                    End If
                Catch ex As Exception
                    MessageBox.Show("Error while Filling the Explorer:" & ex.Message)
                End Try
            Next drive
        End If
        Me._MyComputerTreeNode.Expand()
    End Sub

#End Region

#Region " FOLDER LIST "

    ''' <summary> Updates the list add current. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Sub UpdateListAddCurrent()
        Dim i As Integer = 0
        Dim j As Integer = 0
        For i = 0 To Me._ListView.Items.Count - 2
            If String.Equals(Me._ListView.Items(i).SubItems(1).Text, "Selected", StringComparison.OrdinalIgnoreCase) Then
                For j = Me._ListView.Items.Count - 1 To i + 2 Step -1
                    Me._ListView.Items(j).Remove()
                Next j
                Exit For
            End If
        Next i
    End Sub

    ''' <summary> Updates the list go back. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Sub UpdateListGoBack()
        If (Me._ListView.Items.Count > 0) AndAlso String.Equals(Me._ListView.Items(0).SubItems(1).Text, "Selected", StringComparison.OrdinalIgnoreCase) Then
            Return
        End If
        Dim i As Integer = 0
        For i = 0 To Me._ListView.Items.Count - 1
            If String.Equals(Me._ListView.Items(i).SubItems(1).Text, "Selected", StringComparison.OrdinalIgnoreCase) Then
                If i <> 0 Then
                    Me._ListView.Items(i - 1).SubItems(1).Text = "Selected"
                    Me.CandidatePath = Me._ListView.Items(i - 1).Text
                End If
            End If
            If i <> 0 Then
                Me._ListView.Items(i).SubItems(1).Text = " -/- "
            End If
        Next i
    End Sub

    ''' <summary> Updates the list go forward. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Sub UpdateListGoFwd()
        If (Me._ListView.Items.Count > 0) AndAlso String.Equals(Me._ListView.Items(Me._ListView.Items.Count - 1).SubItems(1).Text, "Selected", StringComparison.OrdinalIgnoreCase) Then
            Return
        End If
        Dim i As Integer = 0
        For i = Me._ListView.Items.Count - 1 To 0 Step -1
            If String.Equals(Me._ListView.Items(i).SubItems(1).Text, "Selected", StringComparison.OrdinalIgnoreCase) Then
                If i <> Me._ListView.Items.Count Then
                    Me._ListView.Items(i + 1).SubItems(1).Text = "Selected"
                    Me.CandidatePath = Me._ListView.Items(i + 1).Text
                End If
            End If

            If i <> Me._ListView.Items.Count - 1 Then
                Me._ListView.Items(i).SubItems(1).Text = " -/- "
            End If
        Next i
    End Sub

    ''' <summary> Updates the list described by f. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="f"> The format string. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub UpdateList(ByVal f As String)
        Dim listViewItem As ListViewItem ' Used for creating list view items.
        Me.UpdateListAddCurrent()
        Try
            If Me._ListView.Items.Count > 0 Then
                If String.Equals(Me._ListView.Items(Me._ListView.Items.Count - 1).Text, f, StringComparison.OrdinalIgnoreCase) Then
                    Return
                End If
            End If
            Dim i As Integer
            For i = 0 To Me._ListView.Items.Count - 1
                Me._ListView.Items(i).SubItems(1).Text = " -/- "
            Next i
            listViewItem = New ListViewItem(f)
            listViewItem.SubItems.Add("Selected")
            listViewItem.Tag = f
            Me._ListView.Items.Add(listViewItem)
        Catch e As Exception
            MessageBox.Show(e.Message)
        End Try
    End Sub

    ''' <summary> Updates the drive image. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="drive">     The drive. </param>
    ''' <param name="nodeDrive"> The node drive. </param>
    Private Shared Sub UpdateDriveImage(ByVal drive As String, ByVal nodeDrive As TreeNode)
        If nodeDrive IsNot Nothing Then
            nodeDrive.Text = drive
            Dim di As New System.IO.DriveInfo(drive)
            Select Case di.DriveType
                Case DriveType.Removable ' 2
                    nodeDrive.ImageIndex = 17
                    nodeDrive.SelectedImageIndex = 17
                Case DriveType.Fixed '3
                    nodeDrive.ImageIndex = 0
                    nodeDrive.SelectedImageIndex = 0
                Case DriveType.Network ' 4
                    nodeDrive.ImageIndex = 8
                    nodeDrive.SelectedImageIndex = 8
                Case DriveType.CDRom ' 5
                    nodeDrive.ImageIndex = 7
                    nodeDrive.SelectedImageIndex = 7
                Case Else
                    nodeDrive.ImageIndex = 0
                    nodeDrive.SelectedImageIndex = 0
            End Select
        End If
    End Sub

#End Region

#Region " TREE VIEW CONTROL EVENTS "

    ''' <summary> Expand my computer node. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> Tree view event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ExpandMyComputerNode(ByVal e As System.Windows.Forms.TreeViewEventArgs)

        Dim drives() As String = Environment.GetLogicalDrives()
        Dim n As TreeNode
        Dim nodemyC As TreeNode
        Dim nodeDrive As TreeNode
        nodemyC = e.Node
        n = e.Node

        If String.Equals(n.Text, "My Computer", StringComparison.OrdinalIgnoreCase) AndAlso (nodemyC.GetNodeCount(True) < 2) Then
            '/////////
            'add each drive and files and directories
            nodemyC.FirstNode.Remove()

            For Each drive As String In drives
                nodeDrive = New TreeNode With {.Tag = drive}
                FolderTreeView.UpdateDriveImage(drive, nodeDrive)
                nodemyC.Nodes.Add(nodeDrive)
                nodeDrive.EnsureVisible()
                MyBase.Refresh()
                Try
                    'add directories under drive
                    If Directory.Exists(drive) Then
                        For Each dir As String In Directory.GetDirectories(drive)
                            nodeDrive.Nodes.Add(New TreeNode With {.Tag = dir,
                                                .Text = dir.Substring(dir.LastIndexOf("\", StringComparison.OrdinalIgnoreCase) + 1),
                                                .ImageIndex = 1})
                        Next dir
                    End If
                Catch
                End Try
                nodemyC.Expand()
            Next drive
        End If

    End Sub

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.TreeView.AfterExpand" /> event.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.TreeViewEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnAfterExpand(e As TreeViewEventArgs)
        MyBase.OnAfterExpand(e)
        If e Is Nothing Then Return
        Cursor.Current = Cursors.WaitCursor
        Try
            Dim n As TreeNode = e.Node
            If n.Text.IndexOf(":", 1, StringComparison.OrdinalIgnoreCase) > 0 Then
                FolderTreeView.ExploreTreeNode(n)
            Else
                If String.Equals(n.Text, "Desktop", StringComparison.OrdinalIgnoreCase) OrElse
                    String.Equals(n.Text, "Microsoft Windows Network", StringComparison.OrdinalIgnoreCase) OrElse
                    String.Equals(n.Text, "My Computer", StringComparison.OrdinalIgnoreCase) OrElse
                    String.Equals(n.Text, "My Network Places", StringComparison.OrdinalIgnoreCase) OrElse
                    String.Equals(n.Text, "Entire Network", StringComparison.OrdinalIgnoreCase) OrElse
                    ((n.Parent IsNot Nothing) AndAlso String.Equals(n.Parent.Text, "Microsoft Windows Network", StringComparison.OrdinalIgnoreCase)) Then
                    Me.ExpandMyComputerNode(e)
                    ' Me.ExpandEntireNetworkNode(e) ' TO_DO: Fix
                    ' Me.ExpandMicrosoftWindowsNetworkNode(e)  TO_DO: Fix
                Else
                    FolderTreeView.ExploreTreeNode(n)
                End If
            End If
        Catch ex As UnauthorizedAccessException
            MessageBox.Show($"Access denied to folder {e.Node.Tag}", "Access denied", MessageBoxButtons.OK,
                            MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        Catch
            Throw
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.TreeView.AfterSelect" /> event.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.TreeViewEventArgs" /> that contains the
    '''                  event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub OnAfterSelect(e As TreeViewEventArgs)
        MyBase.OnAfterSelect(e)
        If e Is Nothing Then Return
        Dim n As TreeNode
        n = e.Node
        Try
            If String.Equals(n.Text, "My Computer", StringComparison.OrdinalIgnoreCase) OrElse
                String.Equals(n.Text, "My Network Places", StringComparison.OrdinalIgnoreCase) OrElse
                String.Equals(n.Text, "Entire Network", StringComparison.OrdinalIgnoreCase) Then
            Else
                Me.SelectedPath = n.Tag.ToString()
            End If
        Catch
        End Try
    End Sub

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.Control.DoubleClick" /> event.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnDoubleClick(e As EventArgs)
        MyBase.OnDoubleClick(e)
        If e Is Nothing Then Return
        Dim n As TreeNode
        n = Me.SelectedNode
        If Not Me.SelectedNode.IsExpanded Then
            Me.SelectedNode.Collapse()
        Else
            FolderTreeView.ExploreTreeNode(n)
        End If
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.MouseUp" /> event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnMouseUp(e As MouseEventArgs)
        Me.UpdateList(Me.SelectedPath)
        MyBase.OnMouseUp(e)
    End Sub

    ''' <summary>
    ''' Forces the control to invalidate its client area and immediately redraw itself and any child
    ''' controls.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Overrides Sub Refresh()
        MyBase.Refresh()
        Cursor.Current = Cursors.WaitCursor
        Try
            Me.RefreshFolders()
            If String.IsNullOrWhiteSpace(Me.CandidatePath) Then
                Me.SetCandidatePath(FolderTreeView.HomePathName)
            End If
            Me.SelectMyComputerTreeNode(Me.CandidatePath)
        Catch
            Throw
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub

#End Region

#Region " TOOL BAR "

    ''' <summary> Try select directory. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="path"> Full pathname of the file. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function TrySelectPath(ByVal path As String) As (Success As Boolean, Details As String)
        Cursor.Current = Cursors.WaitCursor
        Try
            Me.SelectPath(path)
            Return (False, String.Empty)
        Catch ex As Exception
            Return (False, ex.ToString)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Function

    ''' <summary> Try select home. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function TrySelectHome() As (Success As Boolean, Details As String)
        Return Me.TrySelectPath(FolderTreeView.HomePathName)
    End Function

    ''' <summary> Try go forward. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function TryGoingForward() As (Success As Boolean, Details As String)
        Cursor.Current = Cursors.WaitCursor
        Try
            Me.UpdateListGoFwd()
            If Not String.Equals(Me.SelectedPath, Me.CandidatePath, StringComparison.OrdinalIgnoreCase) Then
                Me.SelectMyComputerTreeNode(Me.CandidatePath)
            End If
            Return (False, String.Empty)
        Catch ex As Exception
            Return (False, ex.ToString)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Function

    ''' <summary> Try go back. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function TryGoingBack() As (Success As Boolean, Details As String)
        Cursor.Current = Cursors.WaitCursor
        Try
            Me.UpdateListGoBack()
            If Not String.Equals(Me.SelectedPath, Me.CandidatePath, StringComparison.OrdinalIgnoreCase) Then
                Me.SelectMyComputerTreeNode(Me.CandidatePath)
            End If
            Return (False, String.Empty)
        Catch ex As Exception
            Return (False, ex.ToString)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Function

    ''' <summary> Try go up. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function TryGoingUp() As (Success As Boolean, Details As String)
        Cursor.Current = Cursors.WaitCursor
        Try
            Dim di As New DirectoryInfo(Me.SelectedPath)
            If di.Parent.Exists Then
                Me.CandidatePath = di.Parent.FullName
                Me.UpdateList(Me.CandidatePath)
                Me.SelectMyComputerTreeNode(Me.CandidatePath)
            End If
            Return (False, String.Empty)
        Catch ex As Exception
            Return (False, ex.ToString)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Function

    ''' <summary> Try add folder. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function TryAddFolder() As (Success As Boolean, Details As String)
        Try
            Using dialog As New FolderBrowserDialog With {
                .Description = "Add Folder in Explorer Tree",
                .ShowNewFolderButton = True,
                .SelectedPath = Me.SelectedPath}
                If dialog.ShowDialog() = DialogResult.OK Then
                    Dim newPath As String = dialog.SelectedPath
                    Dim pathTitle As String = newPath.Substring(newPath.LastIndexOf("\", StringComparison.OrdinalIgnoreCase) + 1)
                    Me.AddFolderNode(pathTitle, newPath)
                End If
            End Using
            Return (False, String.Empty)
        Catch ex As Exception
            Return (False, ex.ToString)
        Finally
        End Try
    End Function

#End Region

#Region " CONTEXT MENU STRIP "

    ''' <summary> my context menu strip. </summary>
    Private _MyContextMenuStrip As ContextMenuStrip

    ''' <summary> Creates a context menu strip. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> The new context menu strip. </returns>
    Private Function CreateContextMenuStrip() As ContextMenuStrip

        ' Create a new ContextMenuStrip control.
        Me._MyContextMenuStrip = New ContextMenuStrip()

        ' Attach an event handler for the 
        ' ContextMenuStrip control's Opening event.
        AddHandler Me._MyContextMenuStrip.Opening, AddressOf Me.ContectMenuOpeningHandler

        Return Me._MyContextMenuStrip

    End Function

    ''' <summary> Adds menu items. </summary>
    ''' <remarks>
    ''' This event handler is invoked when the <see cref="ContextMenuStrip"/> control's Opening event
    ''' is raised.
    ''' </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Cancel event information. </param>
    Private Sub ContectMenuOpeningHandler(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)

        Me._MyContextMenuStrip = TryCast(sender, ContextMenuStrip)

        ' Clear the ContextMenuStrip control's Items collection.
        Me._MyContextMenuStrip.Items.Clear()

        ' Populate the ContextMenuStrip control with its default items.
        ' myContextMenuStrip.Items.Add("-")
        Me._MyContextMenuStrip.Items.Add(New Windows.Forms.ToolStripMenuItem("&Refresh", Nothing, AddressOf Me.RefreshHandler, "Refresh"))
        If Me.SelectedNode.ImageIndex = 18 Then
            Me._MyContextMenuStrip.Items.Add(New Windows.Forms.ToolStripMenuItem("Remove &Shortcut", Nothing, AddressOf Me.RemoveShortcutHandler, "RemoveShortcut"))
        End If

        ' Set Cancel to false. 
        ' It is optimized to true based on empty entry.
        e.Cancel = False

    End Sub

    ''' <summary> Removes the shortcut handler. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub RemoveShortcutHandler(ByVal sender As Object, ByVal e As System.EventArgs)
        If Me.SelectedNode.ImageIndex = 18 Then
            Me.SelectedNode.Remove()
        End If
    End Sub

    ''' <summary> Handler, called when the refresh. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub RefreshHandler(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.Refresh()
    End Sub

#End Region

End Class

