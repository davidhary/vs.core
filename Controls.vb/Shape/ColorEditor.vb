Imports System.Drawing.Design
Imports System.Windows.Forms.Design

''' <summary> A color editor control. </summary>
''' <remarks>
''' (c) 2016 Yang Kok Wah. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/14/2016, </para><para>
''' http://www.codeproject.com/Articles/10558/Shape-Control-for-NET. </para>
''' </remarks>
Friend Class ColorEditorControl
    Inherits System.Windows.Forms.UserControl

#Region " CONSTRUCTOR "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="initial_color"> The initial color. </param>
    Public Sub New(initial_color As Color)
        Me.NewColor = initial_color
        Me.OldColor = initial_color

        Me.InitializeComponent()
    End Sub

#End Region

#Region " DESIGNER "

    Private _TrackBarAlpha As System.Windows.Forms.TrackBar

    Private _TrackBarRed As System.Windows.Forms.TrackBar

    Private _TrackBarGreen As System.Windows.Forms.TrackBar

    Private _TrackBarBlue As System.Windows.Forms.TrackBar

    Private _Label1 As System.Windows.Forms.Label

    Private _Label2 As System.Windows.Forms.Label

    Private _Label3 As System.Windows.Forms.Label

    Private _Label4 As System.Windows.Forms.Label

    Public Property NewColor As Color

    Public Property OldColor As Color

    Private _LabelColor As System.Windows.Forms.Label

    Private _PanelColor As System.Windows.Forms.Panel

    ''' <summary> Initializes the component. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Sub InitializeComponent()
        Me._TrackBarAlpha = New System.Windows.Forms.TrackBar()
        Me._TrackBarRed = New System.Windows.Forms.TrackBar()
        Me._TrackBarGreen = New System.Windows.Forms.TrackBar()
        Me._TrackBarBlue = New System.Windows.Forms.TrackBar()
        Me._Label1 = New System.Windows.Forms.Label()
        Me._Label2 = New System.Windows.Forms.Label()
        Me._Label3 = New System.Windows.Forms.Label()
        Me._Label4 = New System.Windows.Forms.Label()
        Me._PanelColor = New System.Windows.Forms.Panel()
        Me._LabelColor = New System.Windows.Forms.Label()
        DirectCast(Me._TrackBarAlpha, System.ComponentModel.ISupportInitialize).BeginInit()
        DirectCast(Me._TrackBarRed, System.ComponentModel.ISupportInitialize).BeginInit()
        DirectCast(Me._TrackBarGreen, System.ComponentModel.ISupportInitialize).BeginInit()
        DirectCast(Me._TrackBarBlue, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._PanelColor.SuspendLayout()
        Me.SuspendLayout()
        ' 
        ' trackBarAlpha
        ' 
        Me._TrackBarAlpha.Location = New System.Drawing.Point(40, 3)
        Me._TrackBarAlpha.Maximum = 255
        Me._TrackBarAlpha.Name = "trackBarAlpha"
        Me._TrackBarAlpha.Size = New System.Drawing.Size(94, 45)
        Me._TrackBarAlpha.TabIndex = 0
        Me._TrackBarAlpha.TickFrequency = 20
        AddHandler Me._TrackBarAlpha.ValueChanged, New System.EventHandler(AddressOf Me.TrackBar_ValueChanged)
        ' 
        ' trackBarRed
        ' 
        Me._TrackBarRed.Location = New System.Drawing.Point(40, 33)
        Me._TrackBarRed.Maximum = 255
        Me._TrackBarRed.Name = "trackBarRed"
        Me._TrackBarRed.Size = New System.Drawing.Size(94, 45)
        Me._TrackBarRed.TabIndex = 1
        Me._TrackBarRed.TickFrequency = 20
        AddHandler Me._TrackBarRed.ValueChanged, New System.EventHandler(AddressOf Me.TrackBar_ValueChanged)
        ' 
        ' trackBarGreen
        ' 
        Me._TrackBarGreen.Location = New System.Drawing.Point(40, 65)
        Me._TrackBarGreen.Maximum = 255
        Me._TrackBarGreen.Name = "trackBarGreen"
        Me._TrackBarGreen.Size = New System.Drawing.Size(94, 45)
        Me._TrackBarGreen.TabIndex = 2
        Me._TrackBarGreen.TickFrequency = 20
        AddHandler Me._TrackBarGreen.ValueChanged, New System.EventHandler(AddressOf Me.TrackBar_ValueChanged)
        ' 
        ' trackBarBlue
        ' 
        Me._TrackBarBlue.Location = New System.Drawing.Point(40, 97)
        Me._TrackBarBlue.Maximum = 255
        Me._TrackBarBlue.Name = "trackBarBlue"
        Me._TrackBarBlue.Size = New System.Drawing.Size(94, 45)
        Me._TrackBarBlue.TabIndex = 3
        Me._TrackBarBlue.TickFrequency = 20
        AddHandler Me._TrackBarBlue.ValueChanged, New System.EventHandler(AddressOf Me.TrackBar_ValueChanged)
        ' 
        ' label1
        ' 
        Me._Label1.Location = New System.Drawing.Point(8, 6)
        Me._Label1.Name = "label1"
        Me._Label1.Size = New System.Drawing.Size(40, 24)
        Me._Label1.TabIndex = 5
        Me._Label1.Text = "Alpha"
        ' 
        ' label2
        ' 
        Me._Label2.Location = New System.Drawing.Point(8, 38)
        Me._Label2.Name = "label2"
        Me._Label2.Size = New System.Drawing.Size(48, 24)
        Me._Label2.TabIndex = 6
        Me._Label2.Text = "Red"
        ' 
        ' label3
        ' 
        Me._Label3.Location = New System.Drawing.Point(8, 70)
        Me._Label3.Name = "label3"
        Me._Label3.Size = New System.Drawing.Size(48, 24)
        Me._Label3.TabIndex = 7
        Me._Label3.Text = "Green"
        ' 
        ' label4
        ' 
        Me._Label4.Location = New System.Drawing.Point(8, 104)
        Me._Label4.Name = "label4"
        Me._Label4.Size = New System.Drawing.Size(48, 24)
        Me._Label4.TabIndex = 8
        Me._Label4.Text = "Blue"
        ' 
        ' panelColor
        ' 
        Me._PanelColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._PanelColor.Controls.Add(Me._LabelColor)
        Me._PanelColor.Location = New System.Drawing.Point(136, 11)
        Me._PanelColor.Name = "panelColor"
        Me._PanelColor.Size = New System.Drawing.Size(31, 106)
        Me._PanelColor.TabIndex = 9
        ' 
        ' labelColor
        ' 
        Me._LabelColor.Location = New System.Drawing.Point(-10, -3)
        Me._LabelColor.Name = "labelColor"
        Me._LabelColor.Size = New System.Drawing.Size(56, 160)
        Me._LabelColor.TabIndex = 10
        ' 
        ' ColorEditorControl
        ' 
        Me.BackColor = System.Drawing.Color.LightGray
        Me.Controls.Add(Me._TrackBarBlue)
        Me.Controls.Add(Me._TrackBarGreen)
        Me.Controls.Add(Me._TrackBarRed)
        Me.Controls.Add(Me._PanelColor)
        Me.Controls.Add(Me._TrackBarAlpha)
        Me.Controls.Add(Me._Label4)
        Me.Controls.Add(Me._Label3)
        Me.Controls.Add(Me._Label2)
        Me.Controls.Add(Me._Label1)
        Me.Name = "ColorEditorControl"
        Me.Size = New System.Drawing.Size(171, 135)
        AddHandler Me.KeyPress, New System.Windows.Forms.KeyPressEventHandler(AddressOf Me.ColorEditorControl_KeyPress)
        AddHandler Me.Load, New System.EventHandler(AddressOf Me.ColorEditorControl_Load)
        DirectCast(Me._TrackBarAlpha, System.ComponentModel.ISupportInitialize).EndInit()
        DirectCast(Me._TrackBarRed, System.ComponentModel.ISupportInitialize).EndInit()
        DirectCast(Me._TrackBarGreen, System.ComponentModel.ISupportInitialize).EndInit()
        DirectCast(Me._TrackBarBlue, System.ComponentModel.ISupportInitialize).EndInit()
        Me._PanelColor.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region " EVENT HANDLERS "

    ''' <summary> Event handler. Called by ColorEditorControl for load events. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ColorEditorControl_Load(sender As Object, e As System.EventArgs)
        Dim argb As Integer = Me.NewColor.ToArgb()
        ' the colors are store in the argb value as 4 byte : AARRGGBB
        Dim alpha As Byte = CByte(argb >> 24)
        Dim red As Byte = CByte(argb >> 16)
        Dim green As Byte = CByte(argb >> 8)
        Dim blue As Byte = CByte(argb)
        Me._TrackBarAlpha.Value = alpha
        Me._TrackBarRed.Value = red
        Me._TrackBarGreen.Value = green
        Me._TrackBarBlue.Value = blue

        'Foreground Label on the Color Panel
        Me._LabelColor.BackColor = Color.FromArgb(alpha, red, green, blue)

        'Create the Background Image for Color Panel 
        'The Color Panel is to allow the user to check on Alpha transparency
        Dim bm As New Bitmap(Me._PanelColor.Width, Me._PanelColor.Height)
        Me._PanelColor.BackgroundImage = bm
        Dim g As Graphics = Graphics.FromImage(Me._PanelColor.BackgroundImage)
        g.FillRectangle(Brushes.White, 0, 0, Me._PanelColor.Width, Me._PanelColor.Height)

        'For formating the string
        Using sf As New StringFormat()
            sf.Alignment = StringAlignment.Center
            sf.LineAlignment = StringAlignment.Center

            'For rotating the string 
            'If you want the text to be rotated uncomment the 5 lines below and comment off the last line

            Using font As New Font("Arial", 16, FontStyle.Bold)
                'Matrix m=new Matrix ();
                'm.Rotate(90);
                'm.Translate(this.panelColor.Width ,0 ,MatrixOrder.Append);
                'g.Transform=m;
                'g.DrawString("TEST",new Font("Arial",16,FontStyle.Bold),Brushes.Black,new Rectangle(0,0,panelColor.Height ,panelColor.Width  ),sf);
                g.DrawString("TEST", font, Brushes.Black, New Rectangle(0, 0, Me._PanelColor.Width, Me._PanelColor.Height), sf)
            End Using
        End Using
    End Sub

    ''' <summary> Updates the color. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="a"> An Integer to process. </param>
    ''' <param name="r"> An Integer to process. </param>
    ''' <param name="g"> An Integer to process. </param>
    ''' <param name="b"> An Integer to process. </param>
    Private Sub UpdateColor(a As Integer, r As Integer, g As Integer, b As Integer)
        Me.NewColor = Color.FromArgb(a, r, g, b)
        Me._LabelColor.BackColor = Me.NewColor
    End Sub

    ''' <summary> Event handler. Called by TrackBar for value changed events. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub TrackBar_ValueChanged(sender As Object, e As System.EventArgs)
        Me.UpdateColor(Me._TrackBarAlpha.Value, Me._TrackBarRed.Value, Me._TrackBarGreen.Value, Me._TrackBarBlue.Value)
    End Sub

    ''' <summary> Event handler. Called by ColorEditorControl for key press events. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Key press event information. </param>
    Private Sub ColorEditorControl_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs)
        'If the user hit the escape key we restore back the old color
        If e.KeyChar.Equals(Keys.Escape) Then
            Me.NewColor = Me.OldColor
        End If
    End Sub
#End Region

End Class

''' <summary> Editor for color. </summary>
''' <remarks>
''' (c) 2016 Yang Kok Wah. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/14/2016, </para><para>
''' http://www.codeproject.com/Articles/10558/Shape-Control-for-NET. </para>
''' </remarks>
Public Class ColorEditor
    Inherits System.Drawing.Design.UITypeEditor

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.Drawing.Design.UITypeEditor" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
    End Sub

    ''' <summary>
    ''' Gets the editor style used by the
    ''' <see cref="M:System.Drawing.Design.UITypeEditor.EditValue(System.IServiceProvider,System.Object)" />
    ''' method.
    ''' </summary>
    ''' <remarks>
    ''' David, 2020-09-24. Indicates whether the UITypeEditor provides a form-based (modal) dialog,
    ''' drop down dialog, or no UI outside of the properties window.
    ''' </remarks>
    ''' <param name="context"> An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that
    '''                        can be used to gain additional context information. </param>
    ''' <returns>
    ''' A <see cref="T:System.Drawing.Design.UITypeEditorEditStyle" /> value that indicates the style
    ''' of editor used by the
    ''' <see cref="M:System.Drawing.Design.UITypeEditor.EditValue(System.IServiceProvider,System.Object)" />
    ''' method. If the <see cref="T:System.Drawing.Design.UITypeEditor" /> does not support this
    ''' method, then <see cref="M:System.Drawing.Design.UITypeEditor.GetEditStyle" /> will return
    ''' <see cref="F:System.Drawing.Design.UITypeEditorEditStyle.None" />.
    ''' </returns>
    <System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name:="FullTrust")>
    Public Overrides Function GetEditStyle(context As System.ComponentModel.ITypeDescriptorContext) As System.Drawing.Design.UITypeEditorEditStyle
        Return UITypeEditorEditStyle.DropDown
    End Function

    ''' <summary>
    ''' Edits the specified object's value using the editor style indicated by the
    ''' <see cref="M:System.Drawing.Design.UITypeEditor.GetEditStyle" /> method.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. Displays the UI for value selection. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="context">  An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that
    '''                         can be used to gain additional context information. </param>
    ''' <param name="provider"> An <see cref="T:System.IServiceProvider" /> that this editor can use
    '''                         to obtain services. </param>
    ''' <param name="value">    The object to edit. </param>
    ''' <returns>
    ''' The new value of the object. If the value of the object has not changed, this should return
    ''' the same object it was passed.
    ''' </returns>
    <System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name:="FullTrust")>
    Public Overrides Function EditValue(context As System.ComponentModel.ITypeDescriptorContext, provider As System.IServiceProvider, value As Object) As Object
        If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
        If provider Is Nothing Then Throw New ArgumentNullException(NameOf(provider))
        If value.[GetType]() IsNot GetType(Color) Then
            Return value
        End If

        ' Uses the IWindowsFormsEditorService to display a 
        ' drop-down UI in the Properties window.
        Dim edSvc As IWindowsFormsEditorService = DirectCast(provider.GetService(GetType(IWindowsFormsEditorService)), IWindowsFormsEditorService)
        If edSvc IsNot Nothing Then
            Using editor As New ColorEditorControl(CType(value, Color))
                edSvc.DropDownControl(editor)
                ' Return the value in the appropriate data format.
                If value.[GetType]() Is GetType(Color) Then
                    Dim result As Color = editor.NewColor
                    Return result
                End If
            End Using
        End If
        Return value
    End Function

    ''' <summary>
    ''' Paints a representation of the value of an object using the specified
    ''' <see cref="T:System.Drawing.Design.PaintValueEventArgs" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. Draws a representation of the property's value. </remarks>
    ''' <param name="e"> A <see cref="T:System.Drawing.Design.PaintValueEventArgs" /> that indicates
    '''                  what to paint and where to paint it. </param>
    <System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name:="FullTrust")>
    Public Overrides Sub PaintValue(e As System.Drawing.Design.PaintValueEventArgs)
        If e Is Nothing Then Return
        Using br As New SolidBrush(CType(e.Value, Color))
            e.Graphics.FillRectangle(br, 1, 1, e.Bounds.Width, e.Bounds.Height)
        End Using
    End Sub

    ''' <summary>
    ''' Indicates whether the specified context supports painting a representation of an object's
    ''' value within the specified context.
    ''' </summary>
    ''' <remarks>
    ''' David, 2020-09-24. Indicates whether the UITypeEditor supports painting a representation of a
    ''' property's value.
    ''' </remarks>
    ''' <param name="context"> An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that
    '''                        can be used to gain additional context information. </param>
    ''' <returns>
    ''' <see langword="true" /> if
    ''' <see cref="M:System.Drawing.Design.UITypeEditor.PaintValue(System.Object,System.Drawing.Graphics,System.Drawing.Rectangle)" />
    ''' is implemented; otherwise, <see langword="false" />.
    ''' </returns>
    <System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name:="FullTrust")>
    Public Overrides Function GetPaintValueSupported(context As System.ComponentModel.ITypeDescriptorContext) As Boolean
        Return True
    End Function
End Class




