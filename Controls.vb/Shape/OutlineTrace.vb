Imports System.Drawing.Imaging

''' <summary> An outline trace. </summary>
''' <remarks>
''' (c) 2016 Yang Kok Wah. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/14/2016, </para><para>
''' http://www.codeproject.com/Articles/10558/Shape-Control-for-NET. </para>
''' </remarks>
Friend Class OutlineTrace

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Gets or sets the color threshold. </summary>
    ''' <value> The color threshold. </value>
    Public Property ColorThreshold As Integer = 200

    ''' <summary> Gets or sets the use red. </summary>
    ''' <value> The use red. </value>
    Public Property UseRed As Boolean = True

    ''' <summary> Gets or sets the use green. </summary>
    ''' <value> The use green. </value>
    Public Property UseGreen As Boolean = True

    ''' <summary> Gets or sets the use blue. </summary>
    ''' <value> The use blue. </value>
    Public Property UseBlue As Boolean = True

    ''' <summary> Coordinates to index for ARGB color pixel format. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="x">      The x coordinate. </param>
    ''' <param name="y">      The y coordinate. </param>
    ''' <param name="stride"> The stride. </param>
    ''' <returns> An Integer. </returns>
    Private Shared Function CoordsToIndex(x As Integer, y As Integer, stride As Integer) As Integer
        Return (stride * y) + (x * 4)
    End Function

    ''' <summary> Gets gray scale color. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="c"> A Color to process. </param>
    ''' <returns> The gray scale color. </returns>
    Public Function GetGrayScaleColor(c As Color) As Integer

        Dim numcolorplane As Integer = 3

        numcolorplane = If(Not Me.UseBlue, numcolorplane - 1, numcolorplane)
        numcolorplane = If(Not Me.UseGreen, numcolorplane - 1, numcolorplane)
        numcolorplane = If(Not Me.UseRed, numcolorplane - 1, numcolorplane)

        If numcolorplane = 0 Then
            Return (CInt(c.B) + CInt(c.G) + CInt(c.R)) \ 3
        End If

        Dim accvalue As Integer = 0
        accvalue = If(Me.UseBlue, accvalue + CInt(c.B), accvalue)
        accvalue = If(Me.UseGreen, accvalue + CInt(c.G), accvalue)
        accvalue = If(Me.UseRed, accvalue + CInt(c.R), accvalue)


        Return CInt(accvalue \ numcolorplane)

    End Function

    ''' <summary> Gets mono color. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="c"> A Color to process. </param>
    ''' <returns> The mono color. </returns>
    Private Function GetMonoColor(c As Color) As Integer
        Dim i As Integer = Me.GetGrayScaleColor(c)
        Return If(i < Me.ColorThreshold, 0, 1)
    End Function

    ''' <summary> String outline 2 polygon. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="outline"> The outline. </param>
    ''' <returns> A Point() </returns>
    Public Shared Function StringOutline2Polygon(outline As String) As Point()
        Dim s As String() = outline.Split(";"c)

        If s.Length < 5 Then
            Return Nothing
        End If

        Dim p As Point() = New Point(s.Length - 2) {}

        Dim s1 As String() '= s(0).Split(","c)
        For i As Integer = 0 To s.Length - 2
            s1 = s(i).Split(","c)
            p(i).X = Integer.Parse(s1(0))

            p(i).Y = Integer.Parse(s1(1))
        Next
        Return p
    End Function

    ''' <summary> Trace outline n. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="bm">              The bm. </param>
    ''' <param name="x0">              The x coordinate 0. </param>
    ''' <param name="y0">              The y coordinate 0. </param>
    ''' <param name="probe_width">     Width of the probe. </param>
    ''' <param name="fg">              The foreground. </param>
    ''' <param name="bg">              The background. </param>
    ''' <param name="bauto_threshold"> True to bauto threshold. </param>
    ''' <param name="n">               An Integer to process. </param>
    ''' <returns> A String. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function TraceOutlineN(bm As Bitmap, x0 As Integer, y0 As Integer, probe_width As Integer, fg As Color, bg As Color,
        bauto_threshold As Boolean, n As Integer) As String

        Dim s As String = String.Empty

        Dim y As Integer = y0
        Dim x1 As Integer = 0, y1 As Integer = 0
        Dim c1 As Color, c2 As Color
        Dim start_direction As Integer = 0, current_direction As Integer = 0

        Dim hitborder As Boolean = False
        Dim hitstart As Boolean = False
        Dim max_width As Integer = bm.Width, max_height As Integer = bm.Height

        'direct bit manipulation
        Dim rect As New Rectangle(0, 0, bm.Width, bm.Height)
        Dim bmpData As BitmapData = bm.LockBits(rect, ImageLockMode.[ReadOnly], bm.PixelFormat)
        Dim ptr As IntPtr = bmpData.Scan0
        Dim bytes As Integer = bm.Width * bm.Height * 4
        Dim rgbValues As Byte() = New Byte(bytes - 1) {}

        ' Copy the RGB values into the array.
        System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes)
        bm.UnlockBits(bmpData)


        Dim x As Integer

        Dim gc1 As Integer

        Dim gc2 As Integer
        If bauto_threshold Then
            'get max pix value difference
            Dim maxpixdiff As Integer = 0
            Dim maxpixvalue As Integer = 0

            For i As Integer = 0 To (probe_width * 2) - 1
                Try
                    x = If(i Mod 2 = 1, x0 + i \ 2, x0 - i \ 2)


                    If x < 0 Then
                        Continue For
                    End If
                    If x >= max_width Then
                        Continue For
                    End If

                    Dim index As Integer = CoordsToIndex(x, y0, bmpData.Stride)
                    If index < 0 OrElse index > (bytes - 1) Then
                        Exit Try
                    End If
                    c1 = Color.FromArgb(rgbValues(index + 2), rgbValues(index + 1), rgbValues(index))

                    gc1 = Me.GetGrayScaleColor(c1)



                    index = CoordsToIndex(x + 1, y0, bmpData.Stride)
                    If index < 0 OrElse index > (bytes - 1) Then
                        Exit Try
                    End If
                    c2 = Color.FromArgb(rgbValues(index + 2), rgbValues(index + 1), rgbValues(index))

                    gc2 = Me.GetGrayScaleColor(c2)

                    If maxpixdiff < Math.Abs(gc1 - gc2) Then
                        maxpixdiff = Math.Abs(gc1 - gc2)
                    End If

                    If gc1 > maxpixvalue Then
                        maxpixvalue = gc1
                    End If
                    If gc2 > maxpixvalue Then
                        maxpixvalue = gc2
                    End If
                Catch generatedExceptionName As Exception
                    Exit Try

                End Try
            Next

            If maxpixdiff > 0 Then
                Me.ColorThreshold = maxpixvalue - CInt(Math.Truncate(0.3 * maxpixdiff))
            End If

            If Me.ColorThreshold < 0 Then
                Me.ColorThreshold = 0

            End If
        End If

        Dim gfg As Integer = Me.GetMonoColor(fg)
        Dim gbg As Integer = Me.GetMonoColor(bg)

        For i As Integer = 0 To (probe_width * 2) - 1
            Try
                x = If(i Mod 2 = 1, x0 + i \ 2, x0 - i \ 2)

                If x < 0 Then
                    Continue For
                End If
                If x >= max_width Then
                    Continue For
                End If

                Dim index As Integer = CoordsToIndex(x, y0, bmpData.Stride)
                If index < 0 OrElse index > (bytes - 1) Then
                    Exit Try
                End If
                c1 = Color.FromArgb(rgbValues(index + 2), rgbValues(index + 1), rgbValues(index))


                gc1 = Me.GetMonoColor(c1)

                index = CoordsToIndex(x + 1, y0, bmpData.Stride)
                If index < 0 OrElse index > (bytes - 1) Then
                    Exit Try
                End If
                c2 = Color.FromArgb(rgbValues(index + 2), rgbValues(index + 1), rgbValues(index))

                gc2 = Me.GetMonoColor(c2)

                If (gc1 = gfg AndAlso gc2 = gbg) OrElse (gc1 = gbg AndAlso gc2 = gfg) Then
                    If gc1 = gfg AndAlso gc2 = gbg Then
                        start_direction = 4
                    End If
                    If gc1 = gbg AndAlso gc2 = gfg Then
                        start_direction = 0
                    End If
                    hitborder = True
                    x1 = x
                    y1 = y
                    Exit Try
                End If
            Catch generatedExceptionName As Exception

                Exit Try

            End Try
        Next

        If Not hitborder Then
            Return String.Empty
        End If

        Dim cn As Color() = New Color(8 * n - 1) {}
        Dim count As Integer = 0
        Dim countlimit As Integer = 10000
        x = x1
        y = y1

        While Not hitstart
            count += 1

            'fall back to prevent infinite loop
            If count > countlimit Then

                Return String.Empty
            End If




            Dim diffx As Integer


            Dim diffy As Integer
            'getting all the neighbors' pixel color
            Try


                Dim index1 As Integer

                'processing top neighbors left to right
                For i As Integer = 0 To 2 * n
                    diffx = i - n
                    index1 = CoordsToIndex(x + diffx, y - n, bmpData.Stride)

                    cn(i) = If((x + diffx) >= 0 AndAlso (x + diffx) < max_width AndAlso (y - n) >= 0 AndAlso (y - n) < max_height, Color.FromArgb(rgbValues(index1 + 2), rgbValues(index1 + 1), rgbValues(index1)), Color.Empty)
                Next

                'processing right neighbors top to bottom
                For i As Integer = 2 * n + 1 To 4 * n - 1
                    diffy = i - 3 * n
                    index1 = CoordsToIndex(x + n, y + diffy, bmpData.Stride)


                    cn(i) = If((x + n) >= 0 AndAlso (x + n) < max_width AndAlso (y + diffy) >= 0 AndAlso (y + diffy) < max_height, Color.FromArgb(rgbValues(index1 + 2), rgbValues(index1 + 1), rgbValues(index1)), Color.Empty)
                Next

                'processing bottom neighbors right to left
                For i As Integer = 4 * n To 6 * n
                    diffx = i - 5 * n
                    index1 = CoordsToIndex(x - diffx, y + n, bmpData.Stride)


                    cn(i) = If((x - diffx) >= 0 AndAlso (x - diffx) < max_width AndAlso (y + n) >= 0 AndAlso (y + n) < max_height, Color.FromArgb(rgbValues(index1 + 2), rgbValues(index1 + 1), rgbValues(index1)), Color.Empty)
                Next

                'processing left neighbors bottom to top
                For i As Integer = 6 * n + 1 To 8 * n - 1
                    diffy = i - 7 * n
                    index1 = CoordsToIndex(x - n, y - diffy, bmpData.Stride)

                    cn(i) = If((x - n) >= 0 AndAlso (x - n) < max_width AndAlso (y - diffy) >= 0 AndAlso (y - diffy) < max_height, Color.FromArgb(rgbValues(index1 + 2), rgbValues(index1 + 1), rgbValues(index1)), Color.Empty)


                Next
            Catch e As Exception
                MessageBox.Show(e.ToString())
                Return String.Empty
            End Try

            Dim index As Integer = 0
            Dim dir_found As Boolean = False

            'find the first valid foreground pixel				
            For i As Integer = start_direction To start_direction + ((8 * n) - 1)
                index = i Mod (8 * n)


                If Not cn(index).Equals(Color.Empty) Then
                    If Me.GetMonoColor(cn(index)) = gfg Then
                        current_direction = index
                        dir_found = True
                        Exit For
                    End If

                End If
            Next


            'if no foreground pixel found, just find the next valid pixel 

            If Not dir_found Then
                For i As Integer = start_direction To start_direction + ((8 * n) - 1)
                    index = i Mod (8 * n)


                    If Not cn(index).Equals(Color.Empty) Then
                        current_direction = index

                        Exit For

                    End If
                Next
            End If


            ' find the next direction to look for foreground pixels
            If (index >= 0) AndAlso (index <= 2 * n) Then
                diffx = index - n
                x += diffx

                y -= n
            End If
            If (index > 2 * n) AndAlso (index < 4 * n) Then
                diffy = index - 3 * n
                x += n
                y += diffy
            End If

            If (index >= 4 * n) AndAlso (index <= 6 * n) Then
                diffx = index - 5 * n
                x -= diffx
                y += n
            End If

            If (index > 6 * n) AndAlso (index < 8 * n) Then
                diffy = index - 7 * n
                x -= n
                y -= diffy
            End If



            'store the found outline
            Dim tests As String = x & "," & y & ";"

            s &= tests

            start_direction = (current_direction + 4 * n + 1) Mod (8 * n)

            'adaptive stop condition
            Dim bMinCountOK As Boolean = If(n > 1, count > (max_height \ 5), count > 10)

            If bMinCountOK AndAlso Math.Abs(x - x1) < (n + 1) AndAlso (Math.Abs(y - y1) < (n + 1)) Then
                hitstart = True


            End If
        End While

        Return s

    End Function

End Class
