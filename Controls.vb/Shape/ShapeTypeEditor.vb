Imports System.Drawing.Design
Imports System.Windows.Forms.Design
Imports System.Drawing.Drawing2D

''' <summary> A shape type editor control. </summary>
''' <remarks>
''' (c) 2016 Yang Kok Wah. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/14/2016, </para><para>
''' http://www.codeproject.com/Articles/10558/Shape-Control-for-NET. </para>
''' </remarks>
Friend Class ShapeTypeEditorControl
    Inherits System.Windows.Forms.UserControl

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="initial_shape"> The initial shape. </param>
    Public Sub New(initial_shape As ShapeType)
        MyBase.New()
        Me.InitializeComponent()
        Me.Shape = initial_shape

        'Find the number of shapes in the enumeration
        Dim numshape As Integer = [Enum].GetValues(GetType(ShapeType)).GetLength(0)
        Dim numrow As Integer = CInt(Math.Truncate(Math.Sqrt(numshape)))
        'Find the number of rows and columns to accommodate the shapes
        Dim numcol As Integer = numshape \ numrow
        If numshape Mod numcol > 0 Then
            numcol += 1
        End If

        ' Record the specifications
        Me._Numrow = numrow
        Me._Numcol = numcol

        Me._Valid_width = Me._Numcol * Me._Width + (Me._Numcol - 1) * 6 + 2 * 4
        Me._Valid_height = Me._Numrow * Me._Height + (Me._Numrow - 1) * 6 + 2 * 4
    End Sub

    ''' <summary> Initializes the component. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Sub InitializeComponent()
        ' 
        ' ShapeTypeEditorControl
        ' 
        Me.BackColor = System.Drawing.Color.LightGray
        Me.Name = "ShapeTypeEditorControl"

    End Sub

    ''' <summary> Gets or sets the shape type. </summary>
    ''' <value> The shape. </value>
    Public Property Shape As ShapeType

    ' Specification for the UI

    'number of shapes
    ' Private _Numshape As Integer

    'number of rows
    'number of columns
    'width of each shape
    'height of each shape
#Disable Warning IDE0044 ' Add read only modifier

    ''' <summary> The numrow. </summary>
    Private _Numrow As Integer, _Numcol As Integer, _Valid_width As Integer, _Valid_height As Integer, _Width As Integer = 20, _Height As Integer = 20
#Enable Warning IDE0044 ' Add read only modifier

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.Paint" /> event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnPaint(e As System.Windows.Forms.PaintEventArgs)
        If e Is Nothing Then Return
        Using bm As New Bitmap(Me.Width, Me.Height, e.Graphics)
            Dim g As Graphics = Graphics.FromImage(bm)
            g.FillRectangle(Brushes.LightGray, New Rectangle(0, 0, bm.Width, bm.Height))
            e.Graphics.FillRectangle(Brushes.LightGray, New Rectangle(0, 0, Me.Width, Me.Height))
            Dim x As Integer = 4, y As Integer = 4
            Dim n As Integer = 0
            For Each shape As ShapeType In [Enum].GetValues(GetType(ShapeType))
                Using path As New GraphicsPath()
                    ShapeControl.UpdateOutline(path, shape, Me._Width, Me._Height)
                    g.FillRectangle(Brushes.LightGray, 0, 0, bm.Width, bm.Height)
                    g.FillPath(Brushes.Yellow, path)
                    g.DrawPath(Pens.Red, path)
                    e.Graphics.DrawImage(bm, x, y, New Rectangle(New Point(0, 0), New Size(Me._Width + 1, Me._Height + 1)), GraphicsUnit.Pixel)
                    If Me.Shape.Equals(shape) Then
                        e.Graphics.DrawRectangle(Pens.Red, New Rectangle(New Point(x - 2, y - 2), New Size(Me._Width + 4, Me._Height + 4)))
                    End If
                    n += 1
                    x = (n Mod Me._Numcol) * Me._Width
                    x = x + (n Mod Me._Numcol) * 6 + 4
                    y = (n \ Me._Numcol) * Me._Height
                    y = y + (n \ Me._Numcol) * 6 + 4
                End Using
            Next
        End Using
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.MouseDown" /> event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnMouseDown(e As System.Windows.Forms.MouseEventArgs)
        If e Is Nothing Then Return
        If e.Button.Equals(MouseButtons.Left) Then
            If e.X > Me._Valid_width Then
                Return
            End If
            If e.Y > Me._Valid_height Then
                Return
            End If

            Dim x As Integer, y As Integer
            Dim n As Integer
            x = e.X
            y = e.Y
            n = (y \ (Me._Valid_height \ Me._Numrow)) * Me._Numcol + ((x \ (Me._Valid_width \ Me._Numcol)) Mod Me._Numcol)

            Dim count As Integer = 0

            For Each shape As ShapeType In [Enum].GetValues(GetType(ShapeType))
                If count = n Then
                    Me.Shape = shape
                    'close the editor immediately
                    SendKeys.Send("{ENTER}")
                End If
                count += 1
            Next
        End If

    End Sub

End Class

''' <summary> Editor for shape type. </summary>
''' <remarks> David, 2020-09-24. </remarks>
Public Class ShapeTypeEditor
    Inherits System.Drawing.Design.UITypeEditor

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.Drawing.Design.UITypeEditor" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
    End Sub

    ''' <summary>
    ''' Gets the editor style used by the
    ''' <see cref="M:System.Drawing.Design.UITypeEditor.EditValue(System.IServiceProvider,System.Object)" />
    ''' method.
    ''' </summary>
    ''' <remarks>
    ''' David, 2020-09-24. Indicates whether the UITypeEditor provides a form-based (modal) dialog,
    ''' drop down dialog, or no UI outside of the properties window.
    ''' </remarks>
    ''' <param name="context"> An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that
    '''                        can be used to gain additional context information. </param>
    ''' <returns>
    ''' A <see cref="T:System.Drawing.Design.UITypeEditorEditStyle" /> value that indicates the style
    ''' of editor used by the
    ''' <see cref="M:System.Drawing.Design.UITypeEditor.EditValue(System.IServiceProvider,System.Object)" />
    ''' method. If the <see cref="T:System.Drawing.Design.UITypeEditor" /> does not support this
    ''' method, then <see cref="M:System.Drawing.Design.UITypeEditor.GetEditStyle" /> will return
    ''' <see cref="F:System.Drawing.Design.UITypeEditorEditStyle.None" />.
    ''' </returns>
    <System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name:="FullTrust")>
    Public Overrides Function GetEditStyle(context As System.ComponentModel.ITypeDescriptorContext) As System.Drawing.Design.UITypeEditorEditStyle
        Return UITypeEditorEditStyle.DropDown
    End Function

    ''' <summary>
    ''' Edits the specified object's value using the editor style indicated by the
    ''' <see cref="M:System.Drawing.Design.UITypeEditor.GetEditStyle" /> method.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. Displays the UI for value selection. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="context">  An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that
    '''                         can be used to gain additional context information. </param>
    ''' <param name="provider"> An <see cref="T:System.IServiceProvider" /> that this editor can use
    '''                         to obtain services. </param>
    ''' <param name="value">    The object to edit. </param>
    ''' <returns>
    ''' The new value of the object. If the value of the object has not changed, this should return
    ''' the same object it was passed.
    ''' </returns>
    <System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name:="FullTrust")>
    Public Overrides Function EditValue(context As System.ComponentModel.ITypeDescriptorContext, provider As System.IServiceProvider, value As Object) As Object
        If provider Is Nothing Then Throw New ArgumentNullException(NameOf(provider))
        ' Return the value if the value is not of type ShapeType
        If value Is Nothing OrElse value.[GetType]() IsNot GetType(ShapeType) Then
            Return value
        End If

        ' Uses the IWindowsFormsEditorService to display a 
        ' drop-down UI in the Properties window.
        Dim edSvc As IWindowsFormsEditorService = DirectCast(provider.GetService(GetType(IWindowsFormsEditorService)), IWindowsFormsEditorService)
        If edSvc IsNot Nothing Then
            ' Display an Shape Type Editor Control and retrieve the value.
            Using editor As New ShapeTypeEditorControl(CType(value, ShapeType))
                edSvc.DropDownControl(editor)
                ' Return the value in the appropriate data format.
                If value.[GetType]() Is GetType(ShapeType) Then
                    Dim result As ShapeType = editor.Shape
                    Return result
                End If
            End Using
        End If
        Return value
    End Function

    ''' <summary>
    ''' Paints a representation of the value of an object using the specified
    ''' <see cref="T:System.Drawing.Design.PaintValueEventArgs" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. Draws a representation of the property's value. </remarks>
    ''' <param name="e"> A <see cref="T:System.Drawing.Design.PaintValueEventArgs" /> that indicates
    '''                  what to paint and where to paint it. </param>
    <System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name:="FullTrust")>
    Public Overrides Sub PaintValue(e As System.Drawing.Design.PaintValueEventArgs)
        If e Is Nothing Then Return
        Using bm As New Bitmap(e.Bounds.Width + 4, e.Bounds.Height + 4, e.Graphics)
            Using g As Graphics = Graphics.FromImage(bm)
                Dim shape As ShapeType = CType(e.Value, ShapeType)
                Using path As New GraphicsPath()
                    ShapeControl.UpdateOutline(path, shape, e.Bounds.Width - 5, e.Bounds.Height - 5)
                    g.FillPath(Brushes.Yellow, path)
                    g.DrawPath(Pens.Red, path)
                    e.Graphics.DrawImage(bm, 3, 3, New Rectangle(New Point(0, 0), New Size(e.Bounds.Width, e.Bounds.Height)), GraphicsUnit.Pixel)
                End Using
            End Using
        End Using

    End Sub

    ''' <summary>
    ''' Indicates whether the specified context supports painting a representation of an object's
    ''' value within the specified context.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="context"> An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that
    '''                        can be used to gain additional context information. </param>
    ''' <returns>
    ''' <see langword="true" /> if
    ''' <see cref="M:System.Drawing.Design.UITypeEditor.PaintValue(System.Object,System.Drawing.Graphics,System.Drawing.Rectangle)" />
    ''' is implemented; otherwise, <see langword="false" />.
    ''' </returns>
    <System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name:="FullTrust")>
    Public Overrides Function GetPaintValueSupported(context As System.ComponentModel.ITypeDescriptorContext) As Boolean
        Return True
    End Function
End Class



