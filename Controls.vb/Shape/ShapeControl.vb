Imports System.ComponentModel
Imports System.Drawing.Drawing2D

''' <summary> A shape control. </summary>
''' <remarks>
''' (c) 2016 Yang Kok Wah. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/14/2016, </para><para>
''' http://www.codeproject.com/Articles/10558/Shape-Control-for-NET. </para>
''' </remarks>
Public Class ShapeControl
    Inherits System.Windows.Forms.Control

#Region " CONSTRUCTOR "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.Windows.Forms.Control" /> class with
    ''' default settings.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        MyBase.New()
        ' This call is required by the Windows.Forms Form Designer.
        Me.InitializeComponent()
        Me.DoubleBuffered = True
        'Using Double Buffer allow for smooth rendering 
        'minimizing flickering
        Me.SetStyle(ControlStyles.SupportsTransparentBackColor Or ControlStyles.DoubleBuffer Or ControlStyles.AllPaintingInWmPaint Or ControlStyles.UserPaint, True)

        'set the default back color and font
        Me.BackColor = Color.FromArgb(0, 255, 255, 255)
        Me.Font = New Font("Arial", 12, FontStyle.Bold)
        Me.Font = New Font("Arial", 12, FontStyle.Bold)
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="disposing"> <see langword="true" /> to release both managed and unmanaged
    '''                          resources; <see langword="false" /> to release only unmanaged
    '''                          resources. </param>
    Protected Overrides Sub Dispose(disposing As Boolean)
        If disposing Then
            If Me._Outline IsNot Nothing Then
                Me._Outline.Dispose()
                Me._Outline = Nothing
            End If
            If Me._Components IsNot Nothing Then
                Me._Components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

#End Region


#Region "Component Designer generated code"

    ''' <summary> Initializes the component. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Sub InitializeComponent()
        '  Me.DoubleBuffered = True
        Me._Components = New System.ComponentModel.Container()
        Me._Timer1 = New System.Windows.Forms.Timer(Me._Components)
        Me._Timer2 = New System.Windows.Forms.Timer(Me._Components)
        Me.SuspendLayout()
        ' 
        ' timer1
        ' 
        Me._Timer1.Interval = 200
        AddHandler Me._Timer1.Tick, New System.EventHandler(AddressOf Me.Timer1_Tick)
        ' 
        ' timer2
        ' 
        Me._Timer2.Interval = 200
        AddHandler Me._Timer2.Tick, New System.EventHandler(AddressOf Me.Timer2_Tick)
        ' 
        ' CustomControl1
        ' 
        AddHandler Me.TextChanged, New System.EventHandler(AddressOf Me.ShapeControl_TextChanged)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private _Components As IContainer

    Private _Istextset As Boolean = False

    Private _Shape As ShapeType = ShapeType.Rectangle

    Private _Borderstyle As DashStyle = DashStyle.Solid

    Private _Bordercolor As Color = Color.FromArgb(255, 255, 0, 0)

    Private _Borderwidth As Integer = 3

    Private _Outline As New GraphicsPath()

    Private _Usegradient As Boolean = False

    Private _Vibrate As Boolean = False

    Private _Voffseted As Boolean = False

    Private _Centercolor As Color = Color.FromArgb(100, 255, 0, 0)

    Private _Surroundcolor As Color = Color.FromArgb(100, 0, 255, 255)

    Private _Timer1 As Timer

    Private _Timer2 As Timer

    Private _Bm As Bitmap

    ''' <summary> Additional user-defined data. </summary>
    ''' <value> The tag 2. </value>
    <Category("Shape"), Description("Additional user-defined data")>
    Public Property Tag2() As String

    ''' <summary> True to blink. </summary>
    Private _Blink As Boolean = False

    ''' <summary> Causes the control to blink. </summary>
    ''' <value> The blink. </value>
    <Category("Shape"), Description("Causes the control to blink")>
    Public Property Blink() As Boolean
        Get
            Return Me._Blink
        End Get
        Set
            Me._Blink = Value
            Me._Timer1.Enabled = Me._Blink
            If Not Me._Blink Then
                Me.Visible = True
            End If
        End Set
    End Property

    ''' <summary> Causes the control to vibrate. </summary>
    ''' <value> The vibrate. </value>
    <Category("Shape"), Description("Causes the control to vibrate")>
    Public Property Vibrate() As Boolean
        Get
            Return Me._Vibrate
        End Get
        Set

            Me._Vibrate = Value
            Me._Timer2.Enabled = Me._Vibrate
            If Not Me._vibrate Then
                If Me._voffseted Then
                    Me.Top += 5
                    Me._voffseted = False
                End If
            End If
        End Set
    End Property

    ''' <summary> Background Image to define outline. </summary>
    ''' <value> The shape image. </value>
    <Category("Shape"), Description("Background Image to define outline")>
    Public Property ShapeImage() As Image
        Get
            Return Me._bm
        End Get
        Set
            If Value IsNot Nothing Then
                Me._bm = DirectCast(Value.Clone(), Bitmap)
                Me.Width = 150
                Me.Height = 150
                Me.OnResize(Nothing)
            Else
                If Me._bm IsNot Nothing Then
                    Me._bm = Nothing
                End If

                Me.OnResize(Nothing)
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the text associated with this control. </summary>
    ''' <value> The text associated with this control. </value>
    <Category("Shape"), Description("Text to display")>
    Public Overrides Property Text() As String
        Get
            Return MyBase.Text
        End Get
        Set

            MyBase.Text = Value

            'When Visual Studio first create a new control, text=name
            'we do not want any default text, thus we override it with blank
            If (Not Me._istextset) AndAlso MyBase.Text.Equals(MyBase.Name) Then
                MyBase.Text = String.Empty
            End If
            Me._istextset = True
        End Set
    End Property

    ''' <summary> Gets or sets the background color for the control. </summary>
    ''' <value>
    ''' A <see cref="T:System.Drawing.Color" /> that represents the background color of the control.
    ''' The default is the value of the
    ''' <see cref="P:System.Windows.Forms.Control.DefaultBackColor" /> property.
    ''' </value>
    <Category("Shape"), Description("Back Color")>
    <BrowsableAttribute(True)>
    <EditorAttribute(GetType(ColorEditor), GetType(System.Drawing.Design.UITypeEditor))>
    Public Overrides Property BackColor() As Color
        Get
            Return MyBase.BackColor
        End Get
        Set
            MyBase.BackColor = Value
            Me.Refresh()
        End Set
    End Property

    ''' <summary> Using Gradient to fill Shape. </summary>
    ''' <value> The use gradient. </value>
    <Category("Shape"), Description("Using Gradient to fill Shape")>
    Public Property UseGradient() As Boolean
        Get
            Return Me._usegradient
        End Get
        Set
            Me._usegradient = Value
            Me.Refresh()
        End Set
    End Property

    ''' <summary> Color at center. </summary>
    ''' <value> The color of the center. </value>
    ''' <remarks> For Gradient Rendering, this is the color at the center of the shape </remarks>
    <Category("Shape"), Description("Color at center")>
    <BrowsableAttribute(True)>
    <EditorAttribute(GetType(ColorEditor), GetType(System.Drawing.Design.UITypeEditor))>
    Public Property CenterColor() As Color
        Get
            Return Me._centercolor
        End Get
        Set
            Me._centercolor = Value
            Me.Refresh()
        End Set
    End Property

    ''' <summary> Color at the edges of the Shape. </summary>
    ''' <value> The color of the surround. </value>
    ''' <remarks> For Gradient Rendering, this is the color at the edges of the shape</remarks>
    <Category("Shape"), Description("Color at the edges of the Shape")>
    <BrowsableAttribute(True)>
    <EditorAttribute(GetType(ColorEditor), GetType(System.Drawing.Design.UITypeEditor))>
    Public Property SurroundColor() As Color
        Get
            Return Me._Surroundcolor
        End Get
        Set
            Me._Surroundcolor = Value
            Me.Refresh()
        End Set
    End Property

    ''' <summary> Border Width. </summary>
    ''' <value> The width of the border. </value>
    <Category("Shape"), Description("Border Width")>
    Public Property BorderWidth() As Integer
        Get
            Return Me._borderwidth
        End Get
        Set
            Me._borderwidth = Value
            If Me._borderwidth < 0 Then
                Me._borderwidth = 0
            End If

            Me.Refresh()
        End Set
    End Property

    ''' <summary> Border Color. </summary>
    ''' <value> The color of the border. </value>
    <Category("Shape"), Description("Border Color")>
    <BrowsableAttribute(True)>
    <EditorAttribute(GetType(ColorEditor), GetType(System.Drawing.Design.UITypeEditor))>
    Public Property BorderColor() As Color
        Get
            Return Me._bordercolor
        End Get
        Set
            Me._bordercolor = Value
            Me.Refresh()
        End Set
    End Property

    ''' <summary> Border Style. </summary>
    ''' <value> The border style. </value>
    <Category("Shape"), Description("Border Style")>
    Public Property BorderStyle() As DashStyle
        Get
            Return Me._borderstyle
        End Get
        Set
            Me._borderstyle = Value
            Me.Refresh()
        End Set
    End Property

    ''' <summary> Select Shape. </summary>
    ''' <value> The shape. </value>
    <Category("Shape"), Description("Select Shape")>
    <BrowsableAttribute(True)>
    <EditorAttribute(GetType(ShapeTypeEditor), GetType(System.Drawing.Design.UITypeEditor))>
    Public Property Shape() As ShapeType
        Get
            Return Me._shape
        End Get
        Set
            Me._shape = Value
            Me.OnResize(Nothing)
        End Set
    End Property

    ''' <summary> Updates the outline. </summary>
    ''' <remarks> David, 2020-09-24. 
    ''' This function creates the path for each shape It is also being used by the
    ''' ShapeTypeEditor to create the various shapes for the Shape property editor UI. 
    ''' </remarks>
    ''' <param name="outline"> [in,out] The outline. </param>
    ''' <param name="shape">   The shape. </param>
    ''' <param name="width">   The width. </param>
    ''' <param name="height">  The height. </param>
    Friend Shared Sub UpdateOutline(ByRef outline As GraphicsPath, shape As ShapeType, width As Integer, height As Integer)
        Select Case shape
            Case ShapeType.CustomPie
                outline.AddPie(0, 0, width, height, 180, 270)
                Exit Select
            Case ShapeType.CustomPolygon
                outline.AddPolygon(New Point() {New Point(0, 0), New Point(width \ 2, height \ 4), New Point(width, 0), New Point((width * 3) \ 4, height \ 2), New Point(width, height), New Point(width \ 2, (height * 3) \ 4),
                    New Point(0, height), New Point(width \ 4, height \ 2)})
                Exit Select
            Case ShapeType.Diamond
                outline.AddPolygon(New Point() {New Point(0, height \ 2), New Point(width \ 2, 0), New Point(width, height \ 2), New Point(width \ 2, height)})
                Exit Select

            Case ShapeType.Rectangle
                outline.AddRectangle(New Rectangle(0, 0, width, height))
                Exit Select

            Case ShapeType.Ellipse
                outline.AddEllipse(0, 0, width, height)
                Exit Select

            Case ShapeType.TriangleUp
                outline.AddPolygon(New Point() {New Point(0, height), New Point(width, height), New Point(width \ 2, 0)})
                Exit Select

            Case ShapeType.TriangleDown
                outline.AddPolygon(New Point() {New Point(0, 0), New Point(width, 0), New Point(width \ 2, height)})
                Exit Select

            Case ShapeType.TriangleLeft
                outline.AddPolygon(New Point() {New Point(width, 0), New Point(0, height \ 2), New Point(width, height)})
                Exit Select

            Case ShapeType.TriangleRight
                outline.AddPolygon(New Point() {New Point(0, 0), New Point(width, height \ 2), New Point(0, height)})
                Exit Select

            Case ShapeType.RoundedRectangle
                outline.AddArc(0, 0, width \ 4, width \ 4, 180, 90)
                outline.AddLine(width \ 8, 0, width - width \ 8, 0)
                outline.AddArc(width - width \ 4, 0, width \ 4, width \ 4, 270, 90)
                outline.AddLine(width, width \ 8, width, height - width \ 8)
                outline.AddArc(width - width \ 4, height - width \ 4, width \ 4, width \ 4, 0, 90)
                outline.AddLine(width - width \ 8, height, width \ 8, height)
                outline.AddArc(0, height - width \ 4, width \ 4, width \ 4, 90, 90)
                outline.AddLine(0, height - width \ 8, 0, width \ 8)
                Exit Select

            Case ShapeType.BalloonSW
                outline.AddArc(0, 0, width \ 4, width \ 4, 180, 90)
                outline.AddLine(width \ 8, 0, width - width \ 8, 0)
                outline.AddArc(width - width \ 4, 0, width \ 4, width \ 4, 270, 90)
                outline.AddLine(width, width \ 8, width, (height * 0.75F) - width \ 8)
                outline.AddArc(width - width \ 4, (height * 0.75F) - width \ 4, width \ 4, width \ 4, 0, 90)
                outline.AddLine(width - width \ 8, height * 0.75F, width \ 8 + (width \ 4), height * 0.75F)
                outline.AddLine(width \ 8 + (width \ 4), height * 0.75F, width \ 8 + (width \ 8), height)
                outline.AddLine(width \ 8 + (width \ 8), height, width \ 8 + (width \ 8), height * 0.75F)
                outline.AddLine(width \ 8 + (width \ 8), height * 0.75F, width \ 8, height * 0.75F)
                outline.AddArc(0, (height * 0.75F) - width \ 4, width \ 4, width \ 4, 90, 90)
                outline.AddLine(0, (height * 0.75F) - width \ 8, 0, width \ 8)
                Exit Select

            Case ShapeType.BalloonSE
                outline.AddArc(0, 0, width \ 4, width \ 4, 180, 90)
                outline.AddLine(width \ 8, 0, width - width \ 8, 0)
                outline.AddArc(width - width \ 4, 0, width \ 4, width \ 4, 270, 90)
                outline.AddLine(width, width \ 8, width, (height * 0.75F) - width \ 8)
                outline.AddArc(width - width \ 4, (height * 0.75F) - width \ 4, width \ 4, width \ 4, 0, 90)
                outline.AddLine(width - width \ 8, height * 0.75F, width - (width \ 4), height * 0.75F)
                outline.AddLine(width - (width \ 4), height * 0.75F, width - (width \ 4), height)
                outline.AddLine(width - (width \ 4), height, width - (3 * width \ 8), height * 0.75F)
                outline.AddLine(width - (3 * width \ 8), height * 0.75F, width \ 8, height * 0.75F)
                outline.AddArc(0, (height * 0.75F) - width \ 4, width \ 4, width \ 4, 90, 90)
                outline.AddLine(0, (height * 0.75F) - width \ 8, 0, width \ 8)
                Exit Select

            Case ShapeType.BalloonNW
                outline.AddArc(width - width \ 4, height - width \ 4, width \ 4, width \ 4, 0, 90)
                outline.AddLine(width - width \ 8, height, width - (width \ 4), height)
                outline.AddArc(0, height - width \ 4, width \ 4, width \ 4, 90, 90)
                outline.AddLine(0, height - width \ 8, 0, height * 0.25F + width \ 8)
                outline.AddArc(0, height * 0.25F, width \ 4, width \ 4, 180, 90)
                outline.AddLine(width \ 8, height * 0.25F, width \ 4, height * 0.25F)
                outline.AddLine(width \ 4, height * 0.25F, width \ 4, 0)
                outline.AddLine(width \ 4, 0, 3 * width \ 8, height * 0.25F)
                outline.AddLine(3 * width \ 8, height * 0.25F, width - width \ 8, height * 0.25F)
                outline.AddArc(width - width \ 4, height * 0.25F, width \ 4, width \ 4, 270, 90)
                outline.AddLine(width, width \ 8 + height * 0.25F, width, height - width \ 8)
                Exit Select

            Case ShapeType.BalloonNE
                outline.AddArc(width - width \ 4, height - width \ 4, width \ 4, width \ 4, 0, 90)
                outline.AddLine(width - width \ 8, height, width - (width \ 4), height)
                outline.AddArc(0, height - width \ 4, width \ 4, width \ 4, 90, 90)
                outline.AddLine(0, height - width \ 8, 0, height * 0.25F + width \ 8)
                outline.AddArc(0, height * 0.25F, width \ 4, width \ 4, 180, 90)
                outline.AddLine(width \ 8, height * 0.25F, 5 * width \ 8, height * 0.25F)
                outline.AddLine(5 * width \ 8, height * 0.25F, 3 * width \ 4, 0)
                outline.AddLine(3 * width \ 4, 0, 3 * width \ 4, height * 0.25F)
                outline.AddLine(3 * width \ 4, height * 0.25F, width - width \ 8, height * 0.25F)
                outline.AddArc(width - width \ 4, height * 0.25F, width \ 4, width \ 4, 270, 90)
                outline.AddLine(width, width \ 8 + height * 0.25F, width, height - width \ 8)
                Exit Select
            Case Else

                Exit Select
        End Select
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.Resize" /> event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnResize(e As EventArgs)
        If (Me.Width < 0) OrElse (Me.Height <= 0) Then
            Return
        End If

        If Me._Bm Is Nothing Then

            Me._Outline = New GraphicsPath()

            UpdateOutline(Me._Outline, Me._Shape, Me.Width, Me.Height)
        Else
            Using bm As Bitmap = DirectCast(Me._Bm.Clone(), Bitmap)
                Using bm2 As New Bitmap(Me.Width, Me.Height)
                    System.Diagnostics.Debug.WriteLine(bm2.Width & "," & bm2.Height)
                    Graphics.FromImage(bm2).DrawImage(bm, New RectangleF(0, 0, bm2.Width, bm2.Height),
                                                      New RectangleF(0, 0, bm.Width, bm.Height), GraphicsUnit.Pixel)
                    Dim trace As New OutlineTrace()
                    Dim s As String = trace.TraceOutlineN(bm2, 0, bm2.Height \ 2, bm2.Width \ 2,
                                                          Color.Black, Color.White, True, 1)
                    Dim p As Point() = OutlineTrace.StringOutline2Polygon(s)
                    Me._Outline = New GraphicsPath()
                    Me._Outline.AddPolygon(p)
                End Using
            End Using
        End If
        If Me._Outline IsNot Nothing Then
            Me.Region = New Region(Me._Outline)
        End If

        Me.Refresh()
        MyBase.OnResize(e)
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.Paint" /> event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnPaint(e As PaintEventArgs)
        If e Is Nothing Then Return
        'Rendering with Gradient
        If Me._Usegradient Then
            Using br As New PathGradientBrush(Me._Outline)
                br.CenterColor = Me._Centercolor
                br.SurroundColors = New Color() {Me._Surroundcolor}
                e.Graphics.FillPath(br, Me._Outline)
            End Using
        End If

        'Rendering with Border
        If Me._Borderwidth > 0 Then
            Using p As New Pen(Me._Bordercolor, Me._Borderwidth * 2)
                p.DashStyle = Me._Borderstyle
                e.Graphics.SmoothingMode = SmoothingMode.HighQuality
                e.Graphics.DrawPath(p, Me._Outline)
            End Using
        End If

        'Rendering the text to be at the center of the shape
        Using sf As New StringFormat()
            sf.Alignment = StringAlignment.Center
            sf.LineAlignment = StringAlignment.Center
            Select Case Me._Shape
                Case ShapeType.BalloonNE, ShapeType.BalloonNW
                    Using br As New SolidBrush(Me.ForeColor)
                        e.Graphics.DrawString(Me.Text, Me.Font, br, New RectangleF(0, Me.Height * 0.25F, Me.Width, Me.Height * 0.75F), sf)
                    End Using
                Case ShapeType.BalloonSE, ShapeType.BalloonSW
                    Using br As New SolidBrush(Me.ForeColor)
                        e.Graphics.DrawString(Me.Text, Me.Font, br, New RectangleF(0, 0, Me.Width, Me.Height * 0.75F), sf)
                    End Using
                Case Else
                    Using br As New SolidBrush(Me.ForeColor)
                        e.Graphics.DrawString(Me.Text, Me.Font, br, New Rectangle(0, 0, Me.Width, Me.Height), sf)
                    End Using
            End Select
        End Using
        ' Calling the base class OnPaint
        MyBase.OnPaint(e)
    End Sub

    ''' <summary> Event handler. Called by ShapeControl for text changed events. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ShapeControl_TextChanged(sender As Object, e As System.EventArgs)
        Me.Refresh()
    End Sub

    ''' <summary> Event handler. Called by Timer1 for tick events. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Timer1_Tick(sender As Object, e As EventArgs)
        Me.Visible = Not Me.Visible
    End Sub

    ''' <summary> Event handler. Called by Timer2 for tick events. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Timer2_Tick(sender As Object, e As EventArgs)
        If Not Me._Vibrate Then
            Return
        End If
        Me._Voffseted = Not Me._Voffseted
        Me.Top = If(Me._Voffseted, Me.Top - 5, Me.Top + 5)


    End Sub
End Class

''' <summary> Values that represent shape types. </summary>
''' <remarks> David, 2020-09-24. All the defined shape type </remarks>
Public Enum ShapeType

    ''' <summary> An enum constant representing the rectangle option. </summary>
    Rectangle

    ''' <summary> An enum constant representing the rounded rectangle option. </summary>
    RoundedRectangle

    ''' <summary> An enum constant representing the diamond option. </summary>
    Diamond

    ''' <summary> An enum constant representing the ellipse option. </summary>
    Ellipse

    ''' <summary> An enum constant representing the triangle up option. </summary>
    TriangleUp

    ''' <summary> An enum constant representing the triangle down option. </summary>
    TriangleDown

    ''' <summary> An enum constant representing the triangle left option. </summary>
    TriangleLeft

    ''' <summary> An enum constant representing the triangle right option. </summary>
    TriangleRight

    ''' <summary> An enum constant representing the balloon NW option. </summary>
    BalloonNE

    ''' <summary> An enum constant representing the balloon nw option. </summary>
    BalloonNW

    ''' <summary> An enum constant representing the balloon Software option. </summary>
    BalloonSW

    ''' <summary> An enum constant representing the balloon se option. </summary>
    BalloonSE

    ''' <summary> An enum constant representing the custom polygon option. </summary>
    CustomPolygon

    ''' <summary> An enum constant representing the custom pie option. </summary>
    CustomPie
End Enum
