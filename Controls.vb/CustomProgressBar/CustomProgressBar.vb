Imports System.ComponentModel
Imports System.Globalization
Imports System.Runtime.InteropServices

''' <summary> A custom progress bar.  Represents a Windows progress bar control which displays
''' its <see cref="ProgressBar.Value" /> as text on a faded background.
''' </summary>
''' <remarks> David, 2020-09-24.
''' CustomProgressBar is a specialized type of <see cref="ProgressBar" />, which it extends 
''' to fade its background colors and to display its <see cref="CustomProgressBar.Text" />. 
''' 
''' <para>You can manipulate the background fading intensity by changing the value of 
''' property <see cref="CustomProgressBar.Fade" /> which accepts values between 0 and 255. 
''' Lower values make the background darker; higher values make the background lighter.</para>
''' 
''' <para>The current <see cref="ProgressBar.Text" /> is displayed using the values of properties 
''' <see cref="CustomProgressBar.Font" /> and <see cref="CustomProgressBar.ForeColor" />.</para>
''' 
''' <para><note type="inherit">When you derive from CustomProgressBar, adding new functionality to the 
''' derived class, if your derived class references objects that must be disposed of before an instance of 
''' your class is destroyed, you must override the <see cref="CustomProgressBar.Dispose(System.Boolean)" /> 
''' method, and call <see cref="System.ComponentModel.Component.Dispose()">Dispose()</see> on all objects 
''' that are referenced in your class, before calling <c>Dispose(disposing)</c> on the base class.</note></para> <para>
''' (c) 2016 Hiske Bekkering. All rights reserved.</para><para>
''' Licensed under The MIT License.</para><para>
''' David, 3/1/2016 by Hiske Bekkering </para>
''' </remarks>
<Description("Provides a ProgressBar which displays its Value as text on a faded background."),
    Designer(GetType(CustomProgressBarDesigner)), ToolboxBitmap(GetType(ProgressBar))>
Public Class CustomProgressBar
    Inherits ProgressBar


#Region " Construction & Destruction "

    ''' <summary>
    ''' Initializes a new instance of the
    ''' <see cref="CustomProgressBar" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        MyBase.New()
        MyBase.ForeColor = SystemColors.ControlText
        Me._FadeBrush = New SolidBrush(Color.FromArgb(Me.Fade, Color.White))
    End Sub

    ''' <summary> Creates a new CustomProgressBar. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> A CustomProgressBar. </returns>
    Public Shared Function Create() As CustomProgressBar
        Dim result As CustomProgressBar = Nothing
        Try
            result = New CustomProgressBar
        Catch
            result?.Dispose()
            Throw
        End Try
        Return result
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="CustomProgressBar" />
    ''' and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks>
    ''' This method is called by the public <see cref="Control.Dispose" /> method and the
    ''' <see cref="Object.Finalize" /> method. Dispose invokes Dispose with the
    ''' <i>disposing</i> parameter set to <b>true</b>. Finalize invokes Dispose with
    ''' <i>disposing</i> set to <b>false</b>.
    ''' 
    ''' <para><note type="inherit">Dispose might be called multiple times by other objects. When
    ''' overriding <i>Dispose(Boolean)</i>, be careful not to reference objects that have been
    ''' previously disposed of in an earlier call to Dispose.
    ''' 
    ''' <para>If your derived class references objects that must be disposed of before an instance of
    ''' your class is destroyed, you must call <see cref="Control.Dispose" /> on all objects that are
    ''' referenced in your class, before calling <c>Dispose(disposing)</c>
    ''' on the base class.</para></note></para>
    ''' </remarks>
    ''' <param name="disposing"> <b>True</b> to release both managed and unmanaged resources;
    '''                          <b>false</b> to release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Me._FadeBrush IsNot Nothing Then
                Me._FadeBrush.Dispose()
                Me._FadeBrush = Nothing
            End If
        End If

        MyBase.Dispose(disposing)
    End Sub

#End Region

#Region " Public & Protected Properties "

    ''' <summary>
    ''' Returns the parameters used to create the window for the <see cref="CustomProgressBar" />
    ''' control.
    ''' </summary>
    ''' <remarks>
    ''' The information returned by the CreateParams property is used to pass information about the
    ''' initial state and appearance of this control, at the time an instance of this class is being
    ''' created.
    ''' 
    ''' <para><note type="inherit">When overriding the CreateParams property in a derived class, use
    ''' the base class's CreateParams property to extend the base implementation. Otherwise, you must
    ''' provide all the implementation.</note></para>
    ''' </remarks>
    ''' <value>
    ''' A <see cref="System.Windows.Forms.CreateParams" /> object that contains the required creation
    ''' parameters for the <see cref="CustomProgressBar" /> control.
    ''' </value>
    Protected Overrides ReadOnly Property CreateParams() As CreateParams
        Get
            Dim myParams As CreateParams = MyBase.CreateParams

            ' Make the control use double buffering
            myParams.ExStyle = myParams.ExStyle Or NativeMethods.WS_EX_COMPOSITED

            Return myParams
        End Get
    End Property

    ''' <summary> The fade. </summary>
    Private _Fade As Integer = 150

    ''' <summary> The fade brush. </summary>
    Private _FadeBrush As SolidBrush

    ''' <summary>
    ''' Gets or sets the opacity of the white overlay brush which fades the background colors of the
    ''' <see cref="CustomProgressBar" />.
    ''' </summary>
    ''' <remarks>
    ''' You can use this property to manipulate the density of the background coloring of this
    ''' control, to allow for better readability of any text within the
    ''' <see cref="CustomProgressBar" />. You can use the <see cref="CustomProgressBar.Font" /> and
    ''' <see cref="CustomProgressBar.ForeColor" /> properties to further optimize the display of text.
    ''' 
    ''' <para>Acceptable values for this property are between 0 and 255 inclusive. The default is 150;
    ''' lower values make the background darker; higher values make the background lighter.</para>
    ''' </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <value>
    ''' An <see cref="System.Int32" /> representing the alpha value of the overlay color. The default
    ''' is <b>150</b>.
    ''' </value>
    <Category("Appearance"), DefaultValue(150),
        Description("Specifies the opacity of the white overlay brush which fades the background colors of the CustomProgressBar.")>
    Public Property Fade() As Integer
        Get
            Return Me._Fade
        End Get
        Set(ByVal value As Integer)
            If value < 0 OrElse value > 255 Then
                Dim str() As Object = {value}
                Throw New ArgumentOutOfRangeException(NameOf(value), String.Format(System.Globalization.CultureInfo.CurrentCulture,
                                                                             "A value of '{0}' is not valid for 'Fade'. 'Fade' must be between 0 and 255.", str))
            End If

            Me._Fade = value

            ' Clean up previous brush
            If Me._FadeBrush IsNot Nothing Then
                Me._FadeBrush.Dispose()
            End If

            Me._FadeBrush = New SolidBrush(Color.FromArgb(value, Color.White))

            Me.Invalidate()
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the <see cref="System.Drawing.Font" /> of the text displayed by the
    ''' <see cref="CustomProgressBar" />.
    ''' </summary>
    ''' <remarks>
    ''' You can use the Font property to change the <see cref="System.Drawing.Font" />
    ''' to use when drawing text. To change the text <see cref="Color" />, use the
    ''' <see cref="CustomProgressBar.ForeColor" /> property.
    ''' 
    ''' <para>The Font property is an ambient property. An ambient property is a control property
    ''' that, if not set, is retrieved from the parent control.</para>
    ''' 
    ''' <para>Because the <see cref="System.Drawing.Font" /> class is immutable (meaning that you
    ''' cannot adjust any of its properties), you can only assign the Font property a new Font.
    ''' However, you can base the new font on the existing font.</para>
    ''' 
    ''' <para><note type="inherit">When overriding the Font property in a derived class, use the base
    ''' class's Font property to extend the base implementation. Otherwise, you must provide all the
    ''' implementation.</note></para>
    ''' </remarks>
    ''' <value>
    ''' The <see cref="System.Drawing.Font" /> to apply to the text displayed by the control.
    ''' </value>
    <Browsable(True), EditorBrowsable(EditorBrowsableState.Always)>
    Public Overrides Property Font() As Font
        Get
            Return MyBase.Font
        End Get
        Set(ByVal value As Font)
            MyBase.Font = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the color of the text displayed by <see cref="CustomProgressBar" />.
    ''' </summary>
    ''' <remarks>
    ''' You can use the ForeColor property to change the color of the text within the
    ''' <see cref="CustomProgressBar" /> to match the text of other controls on your form.
    ''' To change the <see cref="System.Drawing.Font" /> to use when drawing text, use the
    ''' <see cref="CustomProgressBar.Font">CustomProgressBar.Font</see> property.
    ''' 
    ''' <para><note type="inherit">When overriding the ForeColor property in a derived class, use the
    ''' base class's ForeColor property to extend the base implementation. Otherwise, you must
    ''' provide all the implementation.</note></para>
    ''' </remarks>
    ''' <value>
    ''' A <see cref="Color" /> that represents the control's foreground color. The default is
    ''' <b>ControlText</b>.
    ''' </value>
    <DefaultValue(GetType(System.Drawing.Color), "ControlText")>
    Public Overrides Property ForeColor() As Color
        Get
            Return MyBase.ForeColor
        End Get

        Set(ByVal value As Color)
            MyBase.ForeColor = value
        End Set
    End Property

    ''' <summary> The caption format. </summary>
    Private _CaptionFormat As String

    ''' <summary> Specifies the format of the overlay. </summary>
    ''' <value> The caption format. </value>
    <Category("Appearance"), DefaultValue("{0} %"),
        Description("Specifies the format of the overlay.")>
    Public Property CaptionFormat As String
        Get
            Return If(String.IsNullOrEmpty(Me._CaptionFormat), "{0} %", Me._CaptionFormat)
        End Get
        Set(value As String)
            Me._CaptionFormat = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the text associated with this <see cref="CustomProgressBar" />.
    ''' </summary>
    ''' <remarks>
    ''' The <see cref="CustomProgressBar" /> control supports display of a single line of text,
    ''' consisting of the <see cref="ProgressBar.Value" /> followed by a percent sign.
    ''' 
    ''' <para>The text is displayed using the values of properties
    ''' <see cref="CustomProgressBar.Font" /> and <see cref="CustomProgressBar.ForeColor" />.</para>
    ''' </remarks>
    ''' <value> A <see cref="String" /> representing the text displayed in the control. </value>
    <Browsable(False), EditorBrowsable(EditorBrowsableState.Always), Bindable(False)>
    Public Overrides Property Text() As String
        Get
            Dim format As String = Me.CaptionFormat
            If String.IsNullOrEmpty(format) Then format = "{0} %"
            Return String.Format(CultureInfo.CurrentCulture, format, Me.Value)
        End Get
        Set(value As String)
            MyBase.Text = value
        End Set
    End Property

#End Region

#Region " Public & Protected Methods "

    ''' <summary> Processes Windows messages. </summary>
    ''' <remarks>
    ''' All messages are sent to the WndProc method after getting filtered through the
    ''' PreProcessMessage method. The WndProc method corresponds exactly to the Windows WindowProc
    ''' function.
    ''' 
    ''' <para><note type="inherit">Inheriting controls should call the base class's WndProc method to
    ''' process any messages that they do not handle.</note></para>
    ''' </remarks>
    ''' <param name="m"> [in,out] The Windows Message to process. </param>
    Protected Overrides Sub WndProc(ByRef m As Message)
        Dim message As Integer = m.Msg

        If message = NativeMethods.WM_PAINT Then
            Me.WmPaint(m)
            Return
        End If

        If message = NativeMethods.WM_PRINTCLIENT Then
            Me.WmPrintClient(m)
            Return
        End If

        MyBase.WndProc(m)
    End Sub

    ''' <summary> Returns a string representation for this <see cref="CustomProgressBar" />. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> A <see cref="String" /> that describes this control. </returns>
    Public Overrides Function ToString() As String
        Dim builder As New System.Text.StringBuilder()
        builder.Append(Me.GetType().FullName)
        builder.Append(", Minimum: ")
        builder.Append(Me.Minimum.ToString(CultureInfo.CurrentCulture))
        builder.Append(", Maximum: ")
        builder.Append(Me.Maximum.ToString(CultureInfo.CurrentCulture))
        builder.Append(", Value: ")
        builder.Append(Me.Value.ToString(CultureInfo.CurrentCulture))
        Return builder.ToString()
    End Function

#End Region

#Region " Private Members "

    ''' <summary> Paints the private described by device. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="device"> The device. </param>
    Private Sub PaintPrivate(ByVal device As IntPtr)
        ' Create a Graphics object for the device context
        Using graphics As Graphics = System.Drawing.Graphics.FromHdc(device)
            Dim rect As Rectangle = Me.ClientRectangle

            If Me._FadeBrush IsNot Nothing Then
                ' Paint a translucent white layer on top, to fade the colors a bit
                graphics.FillRectangle(Me._FadeBrush, rect)
            End If

            TextRenderer.DrawText(graphics, Me.Text, Me.Font, rect, Me.ForeColor)
        End Using
    End Sub

    ''' <summary> Windows message paint. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="m"> [in,out] The Windows Message to process. </param>
    Private Sub WmPaint(ByRef m As Message)
        ' Create a wrapper for the Handle
        Dim myHandle As New HandleRef(Me, Me.Handle)

        ' Prepare the window for painting and retrieve a device context
        Dim pAINTSTRUCT As New NativeMethods.PAINTSTRUCT()
        Dim hDC As IntPtr = NativeMethods.BeginPaint(myHandle, pAINTSTRUCT)

        Try
            ' Apply hDC to message
            m.WParam = hDC

            ' Let Windows paint
            MyBase.WndProc(m)

            ' Custom painting
            Me.PaintPrivate(hDC)
        Finally
            ' Release the device context that BeginPaint retrieved
            NativeMethods.EndPaint(myHandle, pAINTSTRUCT)
        End Try
    End Sub

    ''' <summary> Windows message print client. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="m"> [in,out] The Windows Message to process. </param>
    Private Sub WmPrintClient(ByRef m As Message)
        ' Retrieve the device context
        Dim hDC As IntPtr = m.WParam

        ' Let Windows paint
        MyBase.WndProc(m)

        ' Custom painting
        Me.PaintPrivate(hDC)
    End Sub

#End Region

End Class
