Imports System.Runtime.InteropServices
Imports System.Windows.Forms.Design.Behavior

Friend Class CustomProgressBarDesigner
    Inherits System.Windows.Forms.Design.ControlDesigner

    ''' <summary>
    ''' Initializes a new instance of the
    ''' <see cref="T:System.Windows.Forms.Design.ControlDesigner" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
    End Sub

    ''' <summary>
    ''' Gets a list of <see cref="T:System.Windows.Forms.Design.Behavior.SnapLine" /> objects
    ''' representing significant alignment points for this control.
    ''' </summary>
    ''' <remarks>
    ''' A list of <see cref="T:System.Windows.Forms.Design.Behavior.SnapLine" /> objects representing
    ''' significant alignment points for this control.
    ''' </remarks>
    Public Overrides ReadOnly Property SnapLines() As IList
        Get
            ' Get the SnapLines collection from the base class
            Dim snapList As ArrayList = TryCast(MyBase.SnapLines, ArrayList)

            ' Calculate the Baseline for the Font used by the Control and add it to the SnapLines
            Dim textBaseline As Integer = GetBaseline(MyBase.Control, ContentAlignment.MiddleCenter)
            If textBaseline > 0 Then
                snapList.Add(New SnapLine(SnapLineType.Baseline, textBaseline, SnapLinePriority.Medium))
            End If

            Return snapList
        End Get
    End Property

    ''' <summary> Gets a baseline. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="ctrl">      The control. </param>
    ''' <param name="alignment"> The alignment. </param>
    ''' <returns> The baseline. </returns>
    Private Shared Function GetBaseline(ByVal ctrl As Control, ByVal alignment As ContentAlignment) As Integer
        Dim textAscent As Integer = 0
        Dim textHeight As Integer = 0

        ' Retrieve the ClientRect of the Control
        Dim clientRect As Rectangle = ctrl.ClientRectangle

        ' Create a Graphics object for the Control
        Using graphics As Graphics = ctrl.CreateGraphics()
            ' Retrieve the device context Handle
            Dim hDC As IntPtr = graphics.GetHdc()

            ' Create a wrapper for the Font of the Control
            Dim controlFont As Font = ctrl.Font
            Dim tempFontHandle As New HandleRef(controlFont, controlFont.ToHfont())

            Try
                ' Create a wrapper for the device context
                Dim deviceContextHandle As New HandleRef(ctrl, hDC)

                ' Select the Font into the device context
                Dim originalFont As IntPtr = NativeMethods.SelectObject(deviceContextHandle, tempFontHandle)

                ' Create a TEXTMETRIC and calculate metrics for the selected Font
                Dim tEXTMETRIC As New NativeMethods.TEXTMETRIC()
                If NativeMethods.GetTextMetrics(deviceContextHandle, tEXTMETRIC) <> 0 Then
                    textAscent = tEXTMETRIC.tmAscent + 1
                    textHeight = tEXTMETRIC.tmHeight
                End If

                ' Restore original Font
                Dim originalFontHandle As New HandleRef(ctrl, originalFont)
                NativeMethods.SelectObject(deviceContextHandle, originalFontHandle)
            Finally
                ' Cleanup tempFont
                NativeMethods.DeleteObject(tempFontHandle)

                ' Release device context
                graphics.ReleaseHdc(hDC)
            End Try
        End Using

        ' Calculate return value based on the specified alignment; first check top alignment
        If (alignment And (ContentAlignment.TopLeft Or ContentAlignment.TopCenter Or ContentAlignment.TopRight)) <> 0 Then
            Return clientRect.Top + textAscent
        End If

        ' Check middle alignment
        If (alignment And (ContentAlignment.MiddleLeft Or ContentAlignment.MiddleCenter Or ContentAlignment.MiddleRight)) = 0 Then
            Return clientRect.Bottom - textHeight + textAscent
        End If

        ' Assume bottom alignment
        Return CInt(Fix(Math.Round(CDbl(clientRect.Top) + CDbl(clientRect.Height) / 2 - CDbl(textHeight) / 2 + CDbl(textAscent))))
    End Function
End Class
