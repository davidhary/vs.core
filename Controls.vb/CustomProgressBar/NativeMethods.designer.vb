
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Friend NotInheritable Class NativeMethods

#Region " PARAMETERS "

    ''' <summary> The windows message print client. </summary>
    ''' <remarks> Sent to a window to request that it draw its client area 
    ''' in the specified device context, most commonly in a printer device context. 
    ''' </remarks>
    Public Const WM_PRINTCLIENT As Integer = &H318

    ''' <summary> The WS EX composited. </summary>
    ''' <remarks> Paints all descendants of a window in bottom-to-top painting order using double-buffering. </remarks>
    Public Const WS_EX_COMPOSITED As Integer = &H2000000

#End Region

#Region " STRUCTURES "

    ''' <summary> Contains basic information about a physical font. This is the Unicode version of the structure. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    <System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential, CharSet:=System.Runtime.InteropServices.CharSet.Unicode)>
    Public Structure TEXTMETRIC

        ''' <summary> The height (ascent + descent) of characters. . </summary>
        Public tmHeight As Integer

        ''' <summary> The ascent (units above the base line) of characters. </summary>
        Public tmAscent As Integer

        ''' <summary> The descent (units below the base line) of characters. . </summary>
        Public tmDescent As Integer

        ''' <summary> The amount of leading (space) inside the bounds set by the tmHeight member. . </summary>
        Public tmInternalLeading As Integer

        ''' <summary> The amount of extra leading (space) that the application adds between rows. </summary>
        Public tmExternalLeading As Integer

        ''' <summary> The average width of characters in the font (generally defined as the width of the letter x). </summary>
        Public tmAveCharWidth As Integer

        ''' <summary> The width of the widest character in the font.  </summary>
        Public tmMaxCharWidth As Integer

        ''' <summary> The weight of the font.  </summary>
        Public tmWeight As Integer

        ''' <summary> The extra width per string that may be added to some synthesized fonts.  </summary>
        Public tmOverhang As Integer

        ''' <summary> The horizontal aspect of the device for which the font was designed. </summary>
        Public tmDigitizedAspectX As Integer

        ''' <summary> The vertical aspect of the device for which the font was designed. </summary>
        Public tmDigitizedAspectY As Integer

        ''' <summary> The value of the first character defined in the font. </summary>
        Public tmFirstChar As Char

        ''' <summary> The value of the last character defined in the font. </summary>
        Public tmLastChar As Char

        ''' <summary> The value of the character to be substituted for characters not in the font. </summary>
        Public tmDefaultChar As Char

        ''' <summary> The value of the character that will be used to define word breaks for text justification. </summary>
        Public tmBreakChar As Char

        ''' <summary> Specifies an italic font if it is nonzero. </summary>
        Public tmItalic As Byte

        ''' <summary> Specifies an underlined font if it is nonzero. </summary>
        Public tmUnderlined As Byte

        ''' <summary> A strikeout font if it is nonzero. </summary>
        Public tmStruckOut As Byte

        ''' <summary> Specifies information about the pitch, the technology, and the family of a physical font. </summary>
        Public tmPitchAndFamily As Byte

        ''' <summary> The character set of the font. </summary>
        Public tmCharSet As Byte
    End Structure

    ''' <summary> Contains basic information about a physical font. This is the ANSI version of the structure. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    <System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential, CharSet:=System.Runtime.InteropServices.CharSet.Ansi)>
    Public Structure TEXTMETRICA

        ''' <summary> The height (ascent + descent) of characters. </summary>
        Public tmHeight As Integer

        ''' <summary> The ascent (units above the base line) of characters. </summary>
        Public tmAscent As Integer
        ''' <summary> The descent (units below the base line) of characters. </summary>
        Public tmDescent As Integer

        ''' <summary> The amount of leading (space) inside the bounds set by the tmHeight member. </summary>
        Public tmInternalLeading As Integer

        ''' <summary> The amount of extra leading (space) that the application adds between rows. </summary>
        Public tmExternalLeading As Integer

        ''' <summary> The average width of characters in the font (generally defined as the width of the letter x). </summary>
        Public tmAveCharWidth As Integer

        ''' <summary> The width of the widest character in the font. </summary>
        Public tmMaxCharWidth As Integer

        ''' <summary> The weight of the font. </summary>
        Public tmWeight As Integer

        ''' <summary> The extra width per string that may be added to some synthesized fonts. </summary>
        Public tmOverhang As Integer

        ''' <summary> The horizontal aspect of the device for which the font was designed. </summary>
        Public tmDigitizedAspectX As Integer

        ''' <summary> The vertical aspect of the device for which the font was designed. </summary>
        Public tmDigitizedAspectY As Integer

        ''' <summary> The value of the first character defined in the font. </summary>
        Public tmFirstChar As Byte

        ''' <summary> The value of the last character defined in the font. </summary>
        Public tmLastChar As Byte

        ''' <summary> The value of the character to be substituted for characters not in the font. </summary>
        Public tmDefaultChar As Byte

        ''' <summary> The value of the character that will be used to define word breaks for text justification. </summary>
        Public tmBreakChar As Byte

        ''' <summary> Specifies an italic font if it is nonzero. </summary>
        Public tmItalic As Byte

        ''' <summary> Specifies an underlined font if it is nonzero. </summary>
        Public tmUnderlined As Byte

        ''' <summary> A strikeout font if it is nonzero. </summary>
        Public tmStruckOut As Byte

        ''' <summary> Specifies information about the pitch, the technology, and the family of a physical font. </summary>
        Public tmPitchAndFamily As Byte

        ''' <summary> The character set of the font. </summary>
        Public tmCharSet As Byte

    End Structure

#End Region

End Class
