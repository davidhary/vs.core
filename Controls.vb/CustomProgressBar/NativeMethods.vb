Imports System.Runtime.InteropServices
Imports System.Security

<SuppressUnmanagedCodeSecurity>
Partial Friend NotInheritable Class NativeMethods

    ''' <summary> Begins a paint. </summary>
    ''' <remarks>
    ''' Prepares the specified window for painting and fills a PAINTSTRUCT structure with information
    ''' about the painting.
    ''' </remarks>
    ''' <param name="hWnd">    The window. </param>
    ''' <param name="lpPaint"> [in,out] The paint structure. </param>
    ''' <returns> An IntPtr. </returns>
    <DllImport("user32.dll", CharSet:=CharSet.Auto, ExactSpelling:=False)>
    Friend Shared Function BeginPaint(ByVal hWnd As HandleRef, <[In](), Out()> ByRef lpPaint As NativeMethods.PAINTSTRUCT) As IntPtr
    End Function

    ''' <summary> Ends a paint. </summary>
    ''' <remarks>
    ''' Marks the end of painting in the specified window. This function is required for each call to
    ''' the BeginPaint function, but only after painting is complete.
    ''' </remarks>
    ''' <param name="hWnd">    The window. </param>
    ''' <param name="lpPaint"> [in,out] The paint structure. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    <DllImport("user32.dll", CharSet:=CharSet.Auto, ExactSpelling:=False)>
    Friend Shared Function EndPaint(ByVal hWnd As HandleRef, ByRef lpPaint As NativeMethods.PAINTSTRUCT) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function

End Class

