Imports System.ComponentModel
Imports System.Windows.Forms.Design

''' <summary> Tool strip custom progress bar. </summary>
''' <remarks>
''' (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 4/16/2014 </para>
''' </remarks>
<ToolStripItemDesignerAvailability(ToolStripItemDesignerAvailability.ToolStrip)>
Public Class ToolStripCustomProgressBar
    Inherits ToolStripControlHost

#Region " CONSTRUCTOR "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> Call the base constructor passing in a CustomProgressBar instance. </remarks>
    Public Sub New()
        MyBase.New(New CustomProgressBar())
    End Sub

#End Region

#Region " CONTROL "

    ''' <summary> Gets the numeric up down control. </summary>
    ''' <value> The numeric up down control. </value>
    Public ReadOnly Property ProgressBar() As CustomProgressBar
        Get
            Return TryCast(Me.Control, CustomProgressBar)
        End Get
    End Property

#End Region

#Region " CONTROL PROPERTIES "

    ''' <summary> Gets or sets the selected text. </summary>
    ''' <value> The selected text. </value>
    <DefaultValue("")>
    <Description("text"), Category("Appearance")>
    Public Overrides Property Text() As String
        Get
            Return Me.ProgressBar.Text
        End Get
        Set(ByVal value As String)
            Me.ProgressBar.Text = value
        End Set
    End Property

    ''' <summary> Gets or sets the value. </summary>
    ''' <value> The value. </value>
    <DefaultValue("")>
    <Description("Value"), Category("Appearance")>
    Public Property Value() As Integer
        Get
            Return Me.ProgressBar.Value
        End Get
        Set(ByVal value As Integer)
            Me.ProgressBar.Value = value
        End Set
    End Property

    ''' <summary> Minimum. </summary>
    ''' <value> The minimum value. </value>
    <DefaultValue(0)>
    <Description("Minimum"), Category("Appearance")>
    Public Property Minimum() As Integer
        Get
            Return Me.ProgressBar.Minimum
        End Get
        Set(ByVal value As Integer)
            Me.ProgressBar.Minimum = value
        End Set
    End Property

    ''' <summary> Maximum. </summary>
    ''' <value> The Maximum value. </value>
    <DefaultValue(0)>
    <Description("Maximum"), Category("Appearance")>
    Public Property Maximum() As Integer
        Get
            Return Me.ProgressBar.Maximum
        End Get
        Set(ByVal value As Integer)
            Me.ProgressBar.Maximum = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the opacity of the white overlay brush which fades the background colors of the
    ''' <see cref="CustomProgressBar" />.
    ''' </summary>
    ''' <remarks>
    ''' You can use this property to manipulate the density of the background coloring of this
    ''' control, to allow for better readability of any text within the
    ''' <see cref="CustomProgressBar" />. You can use the <see cref="CustomProgressBar.Font" /> and
    ''' <see cref="CustomProgressBar.ForeColor" /> properties to further optimize the display of text.
    ''' 
    ''' <para>Acceptable values for this property are between 0 and 255 inclusive. The default is 150;
    ''' lower values make the background darker; higher values make the background lighter.</para>
    ''' </remarks>
    ''' <value>
    ''' An <see cref="System.Int32" /> representing the alpha value of the overlay color. The default
    ''' is <b>150</b>.
    ''' </value>
    <Category("Appearance"), DefaultValue(150),
        Description("Specifies the opacity of the white overlay brush which fades the background colors of the CustomProgressBar.")>
    Public Property Fade() As Integer
        Get
            Return Me.ProgressBar.Fade
        End Get
        Set(ByVal value As Integer)
            Me.ProgressBar.Fade = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the <see cref="System.Drawing.Font" /> of the text displayed by the
    ''' <see cref="CustomProgressBar" />.
    ''' </summary>
    ''' <remarks>
    ''' You can use the Font property to change the <see cref="System.Drawing.Font" />
    ''' to use when drawing text. To change the text <see cref="Color" />, use the
    ''' <see cref="CustomProgressBar.ForeColor" /> property.
    ''' 
    ''' <para>The Font property is an ambient property. An ambient property is a control property
    ''' that, if not set, is retrieved from the parent control.</para>
    ''' 
    ''' <para>Because the <see cref="System.Drawing.Font" /> class is immutable (meaning that you
    ''' cannot adjust any of its properties), you can only assign the Font property a new Font.
    ''' However, you can base the new font on the existing font.</para>
    ''' 
    ''' <para><note type="inherit">When overriding the Font property in a derived class, use the base
    ''' class's Font property to extend the base implementation. Otherwise, you must provide all the
    ''' implementation.</note></para>
    ''' </remarks>
    ''' <value>
    ''' The <see cref="System.Drawing.Font" /> to apply to the text displayed by the control.
    ''' </value>
    <Browsable(True), EditorBrowsable(EditorBrowsableState.Always)>
    Public Overrides Property Font() As Font
        Get
            Return Me.ProgressBar.Font
        End Get
        Set(ByVal value As Font)
            Me.ProgressBar.Font = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the color of the text displayed by <see cref="CustomProgressBar" />.
    ''' </summary>
    ''' <remarks>
    ''' You can use the ForeColor property to change the color of the text within the
    ''' <see cref="CustomProgressBar" /> to match the text of other controls on your form.
    ''' To change the <see cref="System.Drawing.Font" /> to use when drawing text, use the
    ''' <see cref="CustomProgressBar.Font">CustomProgressBar.Font</see> property.
    ''' 
    ''' <para><note type="inherit">When overriding the ForeColor property in a derived class, use the
    ''' base class's ForeColor property to extend the base implementation. Otherwise, you must
    ''' provide all the implementation.</note></para>
    ''' </remarks>
    ''' <value>
    ''' A <see cref="Color" /> that represents the control's foreground color. The default is
    ''' <b>ControlText</b>.
    ''' </value>
    <DefaultValue(GetType(System.Drawing.Color), "ControlText")>
    Public Overrides Property ForeColor() As Color
        Get
            Return Me.ProgressBar.ForeColor
        End Get

        Set(ByVal value As Color)
            Me.ProgressBar.ForeColor = value
        End Set
    End Property

    ''' <summary> Specifies the format of the overlay. </summary>
    ''' <value> The caption format. </value>
    <Category("Appearance"), DefaultValue("{0} %"),
        Description("Specifies the format of the overlay.")>
    Public Property CaptionFormat As String
        Get
            Return Me.ProgressBar.CaptionFormat
        End Get
        Set(value As String)
            Me.ProgressBar.CaptionFormat = value
        End Set
    End Property

#End Region

End Class
