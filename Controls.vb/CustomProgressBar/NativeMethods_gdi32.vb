Imports System.Runtime.InteropServices
Imports System.Security

<SuppressUnmanagedCodeSecurity>
Friend NotInheritable Class NativeMethods

    ''' <summary> Deletes the object described by hObject. </summary>
    ''' <remarks>
    ''' Deletes a logical pen, brush, font, bitmap, region, or palette, freeing all system resources
    ''' associated with the object. After the object has been deleted, the specified handle is no
    ''' longer valid.
    ''' </remarks>
    ''' <param name="hObject"> The object. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    <DllImport("gdi32.dll", CharSet:=CharSet.Auto, ExactSpelling:=False)>
    Friend Shared Function DeleteObject(ByVal hObject As HandleRef) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function

    ''' <summary> Select object. </summary>
    ''' <remarks>
    ''' Selects an object into the specified device context (DC). The new object replaces the
    ''' previous object of the same type.
    ''' </remarks>
    ''' <param name="hDC">     The device-context. </param>
    ''' <param name="hObject"> The object. </param>
    ''' <returns> An IntPtr. </returns>
    <DllImport("gdi32.dll", CharSet:=CharSet.Auto, ExactSpelling:=False)>
    Friend Shared Function SelectObject(ByVal hDC As HandleRef, ByVal hObject As HandleRef) As IntPtr
    End Function

    ''' <summary> Gets text metrics. </summary>
    ''' <remarks>
    ''' Fills the specified buffer with the metrics for the currently selected font.
    ''' </remarks>
    ''' <param name="hDC">  The device-context. </param>
    ''' <param name="lptm"> [in,out] The metrics. </param>
    ''' <returns> The text metrics. </returns>
    Friend Shared Function GetTextMetrics(ByVal hDC As HandleRef, ByRef lptm As NativeMethods.TEXTMETRIC) As Integer
        If Marshal.SystemDefaultCharSize <> 1 Then
            ' Handle Unicode
            Return NativeMethods.GetTextMetricsW(hDC, lptm)
        End If

        ' Handle ANSI; call GetTextMetricsA and translate to Unicode structure
        Dim tEXTMETRICA As New NativeMethods.TEXTMETRICA()
        Dim result As Integer = NativeMethods.GetTextMetricsA(hDC, tEXTMETRICA)

        lptm.tmHeight = tEXTMETRICA.tmHeight
        lptm.tmAscent = tEXTMETRICA.tmAscent
        lptm.tmDescent = tEXTMETRICA.tmDescent
        lptm.tmInternalLeading = tEXTMETRICA.tmInternalLeading
        lptm.tmExternalLeading = tEXTMETRICA.tmExternalLeading
        lptm.tmAveCharWidth = tEXTMETRICA.tmAveCharWidth
        lptm.tmMaxCharWidth = tEXTMETRICA.tmMaxCharWidth
        lptm.tmWeight = tEXTMETRICA.tmWeight
        lptm.tmOverhang = tEXTMETRICA.tmOverhang
        lptm.tmDigitizedAspectX = tEXTMETRICA.tmDigitizedAspectX
        lptm.tmDigitizedAspectY = tEXTMETRICA.tmDigitizedAspectY
        lptm.tmFirstChar = Convert.ToChar(tEXTMETRICA.tmFirstChar)
        lptm.tmLastChar = Convert.ToChar(tEXTMETRICA.tmLastChar)
        lptm.tmDefaultChar = Convert.ToChar(tEXTMETRICA.tmDefaultChar)
        lptm.tmBreakChar = Convert.ToChar(tEXTMETRICA.tmBreakChar)
        lptm.tmItalic = tEXTMETRICA.tmItalic
        lptm.tmUnderlined = tEXTMETRICA.tmUnderlined
        lptm.tmStruckOut = tEXTMETRICA.tmStruckOut
        lptm.tmPitchAndFamily = tEXTMETRICA.tmPitchAndFamily
        lptm.tmCharSet = tEXTMETRICA.tmCharSet

        Return result
    End Function

    ''' <summary> Gets text metrics a. </summary>
    ''' <remarks>
    ''' Fills the specified buffer with the metrics for the currently selected font. This is the ANSI
    ''' version of the function.
    ''' </remarks>
    ''' <param name="hDC">  The device-context. </param>
    ''' <param name="lptm"> [in,out] The metrics. </param>
    ''' <returns> The text metrics a. </returns>
    <DllImport("gdi32.dll", CharSet:=CharSet.Ansi, ExactSpelling:=False)>
    Private Shared Function GetTextMetricsA(ByVal hDC As HandleRef, ByRef lptm As NativeMethods.TEXTMETRICA) As Integer
    End Function

    ''' <summary> Gets text metrics w. </summary>
    ''' <remarks>
    ''' Fills the specified buffer with the metrics for the currently selected font. This is the
    ''' Unicode version of the function.
    ''' </remarks>
    ''' <param name="hDC">  The device-context. </param>
    ''' <param name="lptm"> [in,out] The metrics. </param>
    ''' <returns> The text metrics w. </returns>
    <DllImport("gdi32.dll", CharSet:=CharSet.Unicode, ExactSpelling:=False)>
    Private Shared Function GetTextMetricsW(ByVal hDC As HandleRef, ByRef lptm As NativeMethods.TEXTMETRIC) As Integer
    End Function

End Class
