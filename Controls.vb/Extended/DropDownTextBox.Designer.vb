﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DropDownTextBox

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me._dropDownToggle = New System.Windows.Forms.CheckBox()
        Me._TextBox = New isr.Core.Controls.TextBox()
        Me.SuspendLayout()
        '
        '_dropDownToggle
        '
        Me._dropDownToggle.Appearance = System.Windows.Forms.Appearance.Button
        Me._dropDownToggle.Cursor = System.Windows.Forms.Cursors.Arrow
        Me._dropDownToggle.Dock = System.Windows.Forms.DockStyle.Right
        Me._dropDownToggle.Image = Global.isr.Core.Controls.My.Resources.Resources.go_down_8
        Me._dropDownToggle.Location = New System.Drawing.Point(514, 0)
        Me._dropDownToggle.Margin = New System.Windows.Forms.Padding(0)
        Me._dropDownToggle.MaximumSize = New System.Drawing.Size(24, 25)
        Me._dropDownToggle.Name = "_dropDownToggle"
        Me._dropDownToggle.Size = New System.Drawing.Size(24, 25)
        Me._dropDownToggle.TabIndex = 0
        Me._dropDownToggle.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me._dropDownToggle.UseVisualStyleBackColor = True
        '
        '_TextBox
        '
        Me._TextBox.BackColor = System.Drawing.SystemColors.Window
        Me._TextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._TextBox.ForeColor = System.Drawing.SystemColors.ControlText
        Me._TextBox.Location = New System.Drawing.Point(0, 0)
        Me._TextBox.Margin = New System.Windows.Forms.Padding(0)
        Me._TextBox.Name = "_TextBox"
        Me._TextBox.ReadOnlyBackColor = System.Drawing.SystemColors.Control
        Me._TextBox.ReadOnlyForeColor = System.Drawing.SystemColors.WindowText
        Me._TextBox.ReadWriteBackColor = System.Drawing.SystemColors.Window
        Me._TextBox.ReadWriteForeColor = System.Drawing.SystemColors.ControlText
        Me._TextBox.Size = New System.Drawing.Size(514, 25)
        Me._TextBox.TabIndex = 1
        '
        'DropDownTextBox
        '
        Me.Controls.Add(Me._TextBox)
        Me.Controls.Add(Me._dropDownToggle)
        Me.Margin = New System.Windows.Forms.Padding(0)
        Me.Name = "DropDownTextBox"
        Me.Size = New System.Drawing.Size(538, 25)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _DropDownToggle As System.Windows.Forms.CheckBox
    Friend _TextBox As isr.Core.Controls.TextBox

End Class
