﻿Imports System.ComponentModel

''' <summary> Text box with read only non-validation. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/27/2013 </para>
''' </remarks>
<DesignerCategory("code"), System.ComponentModel.Description("Label Text Box")>
Public Class LabelTextBox
    Inherits System.Windows.Forms.TextBox

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        MyBase.New()
        MyBase.ReadOnly = True
        Me.CausesValidation = False
    End Sub

    ''' <summary> Gets or sets a value indicating whether text in the text box is read-only. </summary>
    ''' <value> <c>True</c> if [read only]; otherwise, <c>False</c>. </value>
    Public Shadows Property [ReadOnly] As Boolean
        Get
            Return MyBase.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            MyBase.ReadOnly = value
            Me.CausesValidation = Not MyBase.ReadOnly
        End Set
    End Property


End Class
