''' <summary> Pop-up container. </summary>
''' <remarks>
''' (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 10/6/2014, 2.1.5392. </para>
''' </remarks>
Public Class PopupContainer
    Inherits ToolStripDropDown

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        MyBase.New()
        Me.BackColor = System.Drawing.Color.Transparent
        Me.Margin = Padding.Empty
        Me.Padding = Padding.Empty
    End Sub

    ''' <summary> Gets or sets the host. </summary>
    Private ReadOnly _Host As ToolStripControlHost

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="containedControl"> The contained control. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub New(ByVal containedControl As Control)
        Me.New()
        Me._Host = Nothing
        Try
            Me._Host = New ToolStripControlHost(containedControl)
        Catch
            Me._Host?.Dispose() : Me._Host = Nothing
            Throw
        End Try
        Try
            ' no way to check if the control supports transparency
            Me._Host.BackColor = System.Drawing.Color.Transparent
        Catch
        End Try
        Me._Host.Margin = Padding.Empty
        Me._Host.Padding = Padding.Empty
        Me.Items.Add(Me._Host)
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the
    ''' <see cref="T:System.Windows.Forms.ToolStripDropDown" /> and optionally releases the managed
    ''' resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                ' this causes stack overflow.
                ' If Me._host IsNot Nothing Me._host.Dispose(): Me._host = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    ''' <summary>
    ''' Positions the System.Windows.Forms.ToolStripDropDown relative to the specified control
    ''' location.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="referencePointControl"> The control (typically, a
    '''                                      System.Windows.Forms.ToolStripDropDownButton) that is the
    '''                                      reference point for the
    '''                                      System.Windows.Forms.ToolStripDropDown position. </param>
    ''' <param name="position">              The horizontal and vertical location of the reference
    '''                                      control's upper-left corner, in pixels. </param>
    ''' <param name="size">                  The maximum size of the drop-down. </param>
    Public Overloads Sub Show(ByVal referencePointControl As Control, ByVal position As Drawing.Point, ByVal size As Drawing.Size)
        MyBase.Show(referencePointControl, position)
        Me.Size = size
    End Sub

    ''' <summary> Shows the information. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="referencePointControl"> The control (typically, a
    '''                                      System.Windows.Forms.ToolStripDropDownButton) that is the
    '''                                      reference point for the
    '''                                      System.Windows.Forms.ToolStripDropDown position. </param>
    ''' <param name="info">                  The information. </param>
    ''' <param name="position">              The horizontal and vertical location of the reference
    '''                                      control's upper-left corner, in pixels. </param>
    ''' <param name="size">                  The maximum size of the drop-down. </param>
    Public Shared Sub PopupInfo(ByVal referencePointControl As Control, ByVal info As String, position As Drawing.Point, size As Drawing.Size)
        Dim containedControl As New Label With {.Size = size, .Text = info}
        Dim Popup As New Core.Controls.PopupContainer(containedControl)
        Popup.Show(referencePointControl, position)
        Popup.Size = size
    End Sub

End Class
