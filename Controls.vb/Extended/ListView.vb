﻿Imports System.ComponentModel

''' <summary>
''' This class provides a non-flickering ListView.  A heartfelt "thank you" goes to stormenet on
''' StackOverflow.
''' </summary>
''' <remarks>
''' (c) 2011 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 08/19/2011, 1.02.4248.x. </para>
''' </remarks>
<DesignerCategory("code"), System.ComponentModel.Description("Non-Flickering List View")>
Public Class ListView
    Inherits System.Windows.Forms.ListView

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()

        MyBase.New()

        ' Activate double buffering
        Me.SetStyle(ControlStyles.OptimizedDoubleBuffer Or ControlStyles.AllPaintingInWmPaint, True)

        ' Enable the OnNotifyMessage event so we get a chance to filter out 
        ' Windows messages before they get to the form's WndProc
        Me.SetStyle(ControlStyles.EnableNotifyMessage, True)
    End Sub

    ''' <summary> Notifies the control of Windows messages. </summary>
    ''' <remarks> Filters out the erase background message. </remarks>
    ''' <param name="m"> A <see cref="T:System.Windows.Forms.Message" /> that represents the Windows
    '''                  message. </param>
    Protected Overrides Sub OnNotifyMessage(ByVal m As Message)
        ' Filter out the WM_ERASEBKGND message
        If m.Msg <> &H14 Then
            MyBase.OnNotifyMessage(m)
        End If
    End Sub

End Class
