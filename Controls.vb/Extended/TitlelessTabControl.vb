Imports System.ComponentModel
Imports System.Security.Permissions

''' <summary> A tab control with hidden tab page titles in run time. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 06/28/2012, 1.02.4562. From Hans PASSANT </para><para>
''' http://StackOverflow.com/users/17034/hans-PASSANT </para><para>
''' http://StackOverflow.com/questions/1824036/TabControl-how-can-you-remove-the-TabPage-title.
''' </para>
''' </remarks>
<DesignerCategory("code"), System.ComponentModel.Description("Title-Less Tab Control")>
Public Class TitleLessTabControl
    Inherits System.Windows.Forms.TabControl

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        MyBase.New()
        Me._PreviousPageCount = 0
    End Sub

#Region " TAB PAGE TITLE HIDING ENGINE "

    ''' <summary> Number of previous pages. </summary>
    Private _PreviousPageCount As Integer

    ''' <summary> Checks if a single tab page. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Sub CheckSingleTabPage()
        If Me.IsHandleCreated Then
            Dim pages As Integer = Me._PreviousPageCount
            Me._PreviousPageCount = Me.TabCount
            If (pages = 1 AndAlso Me._PreviousPageCount > 1) OrElse (pages > 1 AndAlso Me._PreviousPageCount = 1) Then
                Me.RecreateHandle()
            End If
        End If
    End Sub

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.Control.ControlAdded" /> event.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.ControlEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnControlAdded(e As System.Windows.Forms.ControlEventArgs)
        MyBase.OnControlAdded(e)
        Me.CheckSingleTabPage()
    End Sub

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.Control.ControlRemoved" /> event.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.ControlEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnControlRemoved(e As System.Windows.Forms.ControlEventArgs)
        MyBase.OnControlRemoved(e)
        Me.CheckSingleTabPage()
    End Sub

    ''' <summary> Windows Procedure override to hide tab headers. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="m"> [in,out] The Windows <see cref="T:System.Windows.Forms.Message" /> to
    '''                  process. </param>
    <SecurityPermission(SecurityAction.Demand, Flags:=SecurityPermissionFlag.UnmanagedCode)>
    Protected Overrides Sub WndProc(ByRef m As System.Windows.Forms.Message)
        ' Hide tabs by trapping the message
        Const TCM_ADJUSTRECT As Integer = &H1328
        If m.Msg = TCM_ADJUSTRECT AndAlso Not Me.DesignMode AndAlso
            (Me.TabCount = 1 OrElse Not Me.HideSingleTabLabelOnly) Then
            m.Result = New IntPtr(1) ' CType(1, IntPtr)
        Else
            MyBase.WndProc(m)
        End If
    End Sub

#End Region

#Region " VISIBLE PROPERTIES "

    ''' <summary> true to hide, false to show the single tab page title only. </summary>
    Private _HideSingleTabPageTitleOnly As Boolean

    ''' <summary> Gets or sets the hide single tab label only. </summary>
    ''' <value> The hide single tab label only. </value>
    <Category("Behavior"), Description("Toggle hiding all or only one tab page title."),
        Browsable(True),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
        DefaultValue(False)>
    Public Property HideSingleTabLabelOnly As Boolean
        Get
            Return Me._HideSingleTabPageTitleOnly
        End Get
        Set(ByVal value As Boolean)
            Me._HideSingleTabPageTitleOnly = value
            Me.CheckSingleTabPage()
        End Set
    End Property

#End Region

End Class
