Imports System.Runtime.InteropServices
Imports System.Drawing.Printing
Imports System.ComponentModel

''' <summary>
''' An extension to <see cref="System.Windows.Forms.RichTextBox">windows rich text box</see>
''' suitable for printing.
''' </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 12/2/2015 </para><para>
''' David, 1/8/2015. </para>
''' </remarks>
<DesignerCategory("code"), System.ComponentModel.Description("Extended Rich Text Box")>
Public Class RichTextBox
    Inherits System.Windows.Forms.RichTextBox

#Region " STRUCTURES AND IMPORTS "

    ''' <summary> A structure rectangle. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    <StructLayout(LayoutKind.Sequential)>
    Private Structure STRUCT_RECT

        ''' <summary> Gets or sets the left. </summary>
        ''' <value> The left. </value>
        Public Property Left As Int32

        ''' <summary> Gets or sets the top. </summary>
        ''' <value> The top. </value>
        Public Property Top As Int32

        ''' <summary> Gets or sets the right. </summary>
        ''' <value> The right. </value>
        Public Property Right As Int32

        ''' <summary> Gets or sets the bottom. </summary>
        ''' <value> The bottom. </value>
        Public Property Bottom As Int32
    End Structure

    ''' <summary> A structure charrange. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    <StructLayout(LayoutKind.Sequential)>
    Private Structure STRUCT_CHARRANGE

        ''' <summary> Gets or sets the cp minimum. </summary>
        ''' <value> The cp minimum. </value>
        Public Property CpMin As Int32

        ''' <summary> Gets or sets the cp maximum. </summary>
        ''' <value> The cp maximum. </value>
        Public Property CpMax As Int32
    End Structure

    ''' <summary> A structure formatrange. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    <StructLayout(LayoutKind.Sequential)>
    Private Structure STRUCT_FORMATRANGE

        ''' <summary> Gets or sets the hdc. </summary>
        ''' <value> The hdc. </value>
        Public Property Hdc As IntPtr

        ''' <summary> Gets or sets the hdc target. </summary>
        ''' <value> The hdc target. </value>
        Public Property HdcTarget As IntPtr

        ''' <summary> Gets or sets the rectangle. </summary>
        ''' <value> The rectangle. </value>
        Public Property Rc As STRUCT_RECT

        ''' <summary> Gets or sets the rectangle page. </summary>
        ''' <value> The rectangle page. </value>
        Public Property RcPage As STRUCT_RECT

        ''' <summary> Gets or sets the chrg. </summary>
        ''' <value> The chrg. </value>
        Public Property Chrg As STRUCT_CHARRANGE
    End Structure

    ''' <summary> A structure charformat. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    <StructLayout(LayoutKind.Sequential)>
    Private Structure STRUCT_CHARFORMAT

        ''' <summary> Gets or sets the size. </summary>
        ''' <value> The cb size. </value>
        Public Property CbSize As Integer

        ''' <summary> Gets or sets the mask. </summary>
        ''' <value> The double-word mask. </value>
        Public Property DwMask As UInt32

        ''' <summary> Gets or sets the effects. </summary>
        ''' <value> The double-word effects. </value>
        Public Property DwEffects As UInt32

        ''' <summary> Gets or sets the height. </summary>
        ''' <value> The y coordinate height. </value>
        Public Property YHeight As Int32

        ''' <summary> Gets or sets the offset. </summary>
        ''' <value> The y coordinate offset. </value>
        Public Property YOffset As Int32

        ''' <summary> Gets or sets the color of the carriage return text. </summary>
        ''' <value> The color of the carriage return text. </value>
        Public Property CrTextColor As Int32

        ''' <summary> Gets or sets the set the character belongs to. </summary>
        ''' <value> The b character set. </value>
        Public Property BCharSet As Byte

        ''' <summary> Gets or sets the pitch and family. </summary>
        ''' <value> The b pitch and family. </value>
        Public Property BPitchAndFamily As Byte
        ''' <summary> Name of the face. </summary>
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=32)>
        <CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>")>
        Public SzFaceName() As Char
    End Structure

    ''' <summary> A safe native methods. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private NotInheritable Class SafeNativeMethods

        ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        ''' <remarks> David, 2020-09-24. </remarks>
        Private Sub New()
            MyBase.New()
        End Sub

        ''' <summary> Sends a message. </summary>
        ''' <remarks> David, 2020-09-24. </remarks>
        ''' <param name="hWnd">   The window. </param>
        ''' <param name="msg">    The message. </param>
        ''' <param name="wParam"> The parameter. </param>
        ''' <param name="lParam"> The parameter. </param>
        ''' <returns> An Int32. </returns>
        <DllImport("user32.dll")>
        Friend Shared Function SendMessage(ByVal hWnd As IntPtr,
                                        ByVal msg As UInt32,
                                        ByVal wParam As UIntPtr,
                                        ByVal lParam As IntPtr) As IntPtr
        End Function

    End Class

    Private Const _WM_USER As Int32 = &H400&

    Private Const _EM_FORMATRANGE As Int32 = _WM_USER + 57

    'Private Constant _EM_GETCHARFORMAT As Int32 = _WM_USER + 58

    Private Const _EM_SETCHARFORMAT As Int32 = _WM_USER + 68

    Private ReadOnly _SCF_SELECTION As UIntPtr = New UIntPtr(&H1&)

    ' not used Private SCF_WORD As Int32 = &H2&
    ' not used Private SCF_ALL As Int32 = &H4&

    ' Defines for STRUCT_CHARFORMAT member dwMask
    ' (Long because UInt32 is not an intrinsic type)
    Private Const _CFM_BOLD As Long = &H1&

    Private Const _CFM_ITALIC As Long = &H2&

    Private Const _CFM_UNDERLINE As Long = &H4&

    'Private Constant CFM_STRIKEOUT As Long = &H8&
    'Private Constant CFM_PROTECTED As Long = &H10&
    'Private Constant CFM_LINK As Long = &H20&
    Private Const _CFM_SIZE As Long = &H80000000&

    'Private Constant CFM_COLOR As Long = &H40000000&
    Private Const _CFM_FACE As Long = &H20000000&

    'Private Constant CFM_OFFSET As Long = &H10000000&
    'Private Constant CFM_CHARSET As Long = &H8000000&

    ' Defines for STRUCT_CHARFORMAT member dwEffects
    Private Const _CFE_BOLD As Long = &H1&

    ''' <summary> The cfe italic. </summary>
    Private Const _CFE_ITALIC As Long = &H2&

    Private Const _CFE_UNDERLINE As Long = &H4&
    'Private Constant CFE_STRIKEOUT As Long = &H8&
    'Private Constant CFE_PROTECTED As Long = &H10&
    'Private Constant CFE_LINK As Long = &H20&
    'Private Constant CFE_AUTOCOLOR As Long = &H40000000&

#End Region

#Region " EXTENDED FUNCTIONS "

    ''' <summary>
    ''' Calculate or render the contents of the <see cref="RichTextBox">rich text box</see> for
    ''' printing Parameter.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="measureOnly"> If true, only the calculation is performed;
    '''                            otherwise the text is rendered as well. </param>
    ''' <param name="e">           The PrintPageEventArgs object from the PrintPage event. </param>
    ''' <param name="charFrom">    Index of first character to be printed. </param>
    ''' <param name="charTo">      Index of last character to be printed. </param>
    ''' <returns> (Index of last character that fitted on the page) + 1. </returns>
    Public Function FormatRange(ByVal measureOnly As Boolean,
                                ByVal e As PrintPageEventArgs,
                                ByVal charFrom As Integer,
                                ByVal charTo As Integer) As Integer
        If e Is Nothing Then Return 0

        ' Specify which characters to print
        Dim cr As STRUCT_CHARRANGE
        cr.CpMin = charFrom
        cr.CpMax = charTo

        ' Specify the area inside page margins
        Dim rc As STRUCT_RECT
        rc.Top = HundredthInchToTwips(e.MarginBounds.Top)
        rc.Bottom = HundredthInchToTwips(e.MarginBounds.Bottom)
        rc.Left = HundredthInchToTwips(e.MarginBounds.Left)
        rc.Right = HundredthInchToTwips(e.MarginBounds.Right)

        ' Specify the page area
        Dim rcPage As STRUCT_RECT
        rcPage.Top = HundredthInchToTwips(e.PageBounds.Top)
        rcPage.Bottom = HundredthInchToTwips(e.PageBounds.Bottom)
        rcPage.Left = HundredthInchToTwips(e.PageBounds.Left)
        rcPage.Right = HundredthInchToTwips(e.PageBounds.Right)

        ' Get device context of output device
        Dim hdc As IntPtr
        hdc = e.Graphics.GetHdc()

        ' Fill in the FORMATRANGE structure
        Dim fr As STRUCT_FORMATRANGE
        fr.Chrg = cr
        fr.Hdc = hdc
        fr.HdcTarget = hdc
        fr.Rc = rc
        fr.RcPage = rcPage

        ' Non-Zero wParam means render, Zero means measure
        Dim wParam As UIntPtr = If(measureOnly, UIntPtr.Zero, New UIntPtr(1))

        ' Allocate memory for the FORMATRANGE structure and
        ' copy the contents of our structure to this memory
        Dim lParam As IntPtr
        lParam = Marshal.AllocCoTaskMem(Marshal.SizeOf(fr))
        Marshal.StructureToPtr(fr, lParam, False)

        ' Send the actual Win32 message
        Dim res As IntPtr
        res = SafeNativeMethods.SendMessage(Me.Handle, _EM_FORMATRANGE, wParam, lParam)

        ' Free allocated memory
        Marshal.FreeCoTaskMem(lParam)

        ' and release the device context
        e.Graphics.ReleaseHdc(hdc)

        Return CInt(res)
    End Function

    ''' <summary>
    ''' Converts between 1/100 inch (unit used by the .NET framework)
    ''' and twips (1/1440 inch, used by Win32 API calls).
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="n"> Value in 1/100 inch Return value: Value in twips. </param>
    ''' <returns> An Int32. </returns>
    Private Shared Function HundredthInchToTwips(ByVal n As Integer) As Int32
        Return Convert.ToInt32(n * 14.4)
    End Function

    ''' <summary> Frees cached data from rich edit control after printing. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> The formatted range done. </returns>
    Public Function FormatRangeDone() As Integer
        Return CInt(SafeNativeMethods.SendMessage(Me.Handle, _EM_FORMATRANGE, UIntPtr.Zero, IntPtr.Zero))
    End Function

    ''' <summary>
    ''' Sets the font only for the selected part of the rich text box without modifying the other
    ''' properties like size or style.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="face"> Name of the font to use. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function SetSelectionFont(ByVal face As String) As Boolean
        If String.IsNullOrEmpty(face) Then Return False
        Dim cf As New STRUCT_CHARFORMAT()
        cf.CbSize = Marshal.SizeOf(cf)
        cf.DwMask = Convert.ToUInt32(_CFM_FACE)

        ' ReDim face name to relevant size
        ReDim cf.SzFaceName(32)
        face.CopyTo(0, cf.SzFaceName, 0, Math.Min(31, face.Length))

        Dim lParam As IntPtr
        lParam = Marshal.AllocCoTaskMem(Marshal.SizeOf(cf))
        Marshal.StructureToPtr(cf, lParam, False)

        Dim res As IntPtr
        res = SafeNativeMethods.SendMessage(Me.Handle, _EM_SETCHARFORMAT, Me._SCF_SELECTION, lParam)
        Return res = IntPtr.Zero

    End Function

    ''' <summary>
    ''' Sets the font size only for the selected part of the rich text box without modifying the
    ''' other properties like font or style.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="size"> new point size to use. </param>
    ''' <returns> true on success, false on failure. </returns>
    Public Function SetSelectionSize(ByVal size As Integer) As Boolean
        Dim cf As New STRUCT_CHARFORMAT()
        cf.CbSize = Marshal.SizeOf(cf)
        cf.DwMask = Convert.ToUInt32(_CFM_SIZE)
        ' yHeight is in 1/20 pt
        cf.YHeight = size * 20

        Dim lParam As IntPtr
        lParam = Marshal.AllocCoTaskMem(Marshal.SizeOf(cf))
        Marshal.StructureToPtr(cf, lParam, False)

        Dim res As IntPtr
        res = SafeNativeMethods.SendMessage(Me.Handle, _EM_SETCHARFORMAT, Me._SCF_SELECTION, lParam)
        Return res = IntPtr.Zero

    End Function

    ''' <summary>
    ''' Sets the bold style only for the selected part of the rich text box without modifying the
    ''' other properties like font or size.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="bold"> make selection bold (true) or regular (false) </param>
    ''' <returns> true on success, false on failure. </returns>
    Public Function SetSelectionBold(ByVal bold As Boolean) As Boolean
        Return If(bold, Me.SetSelectionStyle(_CFM_BOLD, _CFE_BOLD), Me.SetSelectionStyle(_CFM_BOLD, 0))
    End Function

    ''' <summary>
    ''' Sets the italic style only for the selected part of the rich text box without modifying the
    ''' other properties like font or size.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="italic"> make selection italic (true) or regular (false) </param>
    ''' <returns> true on success, false on failure. </returns>
    Public Function SetSelectionItalic(ByVal italic As Boolean) As Boolean
        Return If(italic, Me.SetSelectionStyle(_CFM_ITALIC, _CFE_ITALIC), Me.SetSelectionStyle(_CFM_ITALIC, 0))
    End Function

    ''' <summary>
    ''' Sets the underlined style only for the selected part of the rich text box ' without modifying
    ''' the other properties like font or size.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="underlined"> make selection underlined (true) or regular (false) </param>
    ''' <returns> true on success, false on failure. </returns>
    Public Function SetSelectionUnderlined(ByVal underlined As Boolean) As Boolean
        Return If(underlined, Me.SetSelectionStyle(_CFM_UNDERLINE, _CFE_UNDERLINE), Me.SetSelectionStyle(_CFM_UNDERLINE, 0))
    End Function

    ''' <summary>
    ''' Set the style only for the selected part of the rich text box with the possibility to mask
    ''' out some styles that are not to be modified.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="mask">   modify which styles? </param>
    ''' <param name="effect"> new values for the styles. </param>
    ''' <returns> true on success, false on failure. </returns>
    Private Function SetSelectionStyle(ByVal mask As Int32, ByVal effect As Int32) As Boolean
        Dim cf As New STRUCT_CHARFORMAT()
        cf.CbSize = Marshal.SizeOf(cf)
        cf.DwMask = Convert.ToUInt32(mask)
        cf.DwEffects = Convert.ToUInt32(effect)

        Dim lParam As IntPtr
        lParam = Marshal.AllocCoTaskMem(Marshal.SizeOf(cf))
        Marshal.StructureToPtr(cf, lParam, False)

        Dim res As IntPtr
        res = SafeNativeMethods.SendMessage(Me.Handle, _EM_SETCHARFORMAT, Me._SCF_SELECTION, lParam)
        Return res = IntPtr.Zero
    End Function
#End Region

End Class
