Imports System.ComponentModel

''' <summary> Information provider. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 4/4/2015 </para>
''' </remarks>
<DesignerCategory("code"), System.ComponentModel.Description("Information Provider")>
Public Class InfoProvider
    Inherits System.Windows.Forms.ErrorProvider

#Region " CONSTRUCTOR "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="container"> The container. </param>
    Public Sub New(ByVal container As System.ComponentModel.IContainer)
        MyBase.New(container)
    End Sub

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="parentControl"> The parent control. </param>
    Public Sub New(ByVal parentControl As System.Windows.Forms.ContainerControl)
        MyBase.New(parentControl)
    End Sub

#End Region

#Region " CLEAR "

    ''' <summary> Clears this object to its blank/initial state. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> The event sender. </param>
    Public Overloads Sub Clear(ByVal sender As Object)
        Dim control As Control = TryCast(sender, Control)
        If control IsNot Nothing Then
            Me.Clear(control)
        Else
            Dim toolStripItem As ToolStripItem = TryCast(sender, ToolStripItem)
            If toolStripItem IsNot Nothing Then
                Me.Clear(toolStripItem)
            End If
        End If
    End Sub

    ''' <summary> Clears this object to its blank/initial state. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> The event sender. </param>
    Public Overloads Sub Clear(ByVal sender As Control)
        If sender IsNot Nothing Then
            If TypeOf sender.Container Is ToolStripItem OrElse
                TypeOf sender.Container Is Windows.Forms.ToolStripMenuItem Then
                Me.Clear(sender.Container)
            Else
                Me.SetError(sender, "")
            End If
        End If
    End Sub

    ''' <summary> Clears this object to its blank/initial state. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> The event sender. </param>
    Public Overloads Sub Clear(ByVal sender As ToolStripItem)
        If sender IsNot Nothing Then
            Me.SetError(sender.Owner, "")
        End If
    End Sub

#End Region

#Region " ANNUNCIATE - OBJECT "

    ''' <summary> Annunciates error. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender">  The event sender. </param>
    ''' <param name="level">   The level. </param>
    ''' <param name="details"> The details. </param>
    ''' <returns> A String. </returns>
    Public Function Annunciate(ByVal sender As Object, ByVal level As InfoProviderLevel, ByVal details As String) As String
        Dim control As Control = TryCast(sender, Control)
        If control IsNot Nothing Then
            Me.Annunciate(control, level, details)
        Else
            Dim toolStripMenuItem As Windows.Forms.ToolStripMenuItem = TryCast(sender, Windows.Forms.ToolStripMenuItem)
            If toolStripMenuItem IsNot Nothing Then
                Me.Annunciate(toolStripMenuItem, level, details)
            Else
                Dim toolStripItem As ToolStripItem = TryCast(sender, ToolStripItem)
                If toolStripItem IsNot Nothing Then
                    Me.Annunciate(toolStripItem, level, details)
                End If
            End If
        End If
        Return details
    End Function

    ''' <summary> Annunciates error. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> The event sender. </param>
    ''' <param name="level">  The level. </param>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <param name="args">   A variable-length parameters list containing arguments. </param>
    ''' <returns> A String. </returns>
    Public Function Annunciate(ByVal sender As Object, ByVal level As InfoProviderLevel, ByVal format As String, ByVal ParamArray args() As Object) As String
        Return Me.Annunciate(sender, level, String.Format(format, args))
    End Function

#End Region

#Region " PAD / ALIGN - CONTROL "

    ''' <summary> Aligns the icon. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender">        The sender object; must be a control. </param>
    ''' <param name="iconAlignment"> The icon alignment. </param>
    Protected Sub AlignIcon(ByVal sender As Object, ByVal iconAlignment As ErrorIconAlignment)
        If sender IsNot Nothing Then
            Me.SetIconAlignment(TryCast(sender, Control), iconAlignment)
        End If
    End Sub

    ''' <summary> Set icon padding. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender">  The sender object; must be a control. </param>
    ''' <param name="padding"> The padding. </param>
    Protected Sub PadIcon(ByVal sender As Object, ByVal padding As Integer)
        If sender IsNot Nothing Then
            Me.SetIconPadding(TryCast(sender, Control), padding)
        End If
    End Sub

#End Region

#Region " ANNUNCIATE - CONTROL "

    ''' <summary> Annunciates error. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender">  The event sender. </param>
    ''' <param name="level">   The level. </param>
    ''' <param name="details"> The details. </param>
    ''' <returns> A String. </returns>
    Public Function Annunciate(ByVal sender As Control, ByVal level As InfoProviderLevel, ByVal details As String) As String
        If sender IsNot Nothing Then
            If TypeOf sender.Container Is ToolStripItem OrElse
                TypeOf sender.Container Is Windows.Forms.ToolStripMenuItem Then
                Me.Annunciate(sender.Container, level, details)
            Else
                Me.SelectIcon(level)
                Me.SetError(sender, details)
            End If
        End If
        Return details
    End Function

    ''' <summary> Annunciates error. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> The event sender. </param>
    ''' <param name="level">  The level. </param>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <param name="args">   A variable-length parameters list containing arguments. </param>
    ''' <returns> A String. </returns>
    Public Function Annunciate(ByVal sender As Control, ByVal level As InfoProviderLevel, ByVal format As String, ByVal ParamArray args() As Object) As String
        Return Me.Annunciate(sender, level, String.Format(format, args))
    End Function

#End Region

#Region " ANNUNCIATE -- TOOL STRIP "

    ''' <summary> Annunciates error. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender">  The sender. </param>
    ''' <param name="level">   The level. </param>
    ''' <param name="details"> The details. </param>
    ''' <returns> A String. </returns>
    Public Function Annunciate(ByVal sender As ToolStripItem, ByVal level As InfoProviderLevel, ByVal details As String) As String
        If sender IsNot Nothing AndAlso sender.Owner IsNot Nothing Then
            Me.SelectIcon(level)
            Me.SetIconAlignment(sender.Owner, ErrorIconAlignment.BottomLeft)
            Me.SetIconPadding(sender.Owner, -(10 + sender.Bounds.X))
            Me.SetError(sender.Owner, details)
        End If
        Return details
    End Function

    ''' <summary> Annunciates error. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="level">  The level. </param>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <param name="args">   A variable-length parameters list containing arguments. </param>
    ''' <returns> A String. </returns>
    Public Function Annunciate(ByVal sender As ToolStripItem, ByVal level As InfoProviderLevel, ByVal format As String, ByVal ParamArray args() As Object) As String
        Return Me.Annunciate(sender, level, String.Format(format, args))
    End Function

    ''' <summary> Annunciates error. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender">  The sender. </param>
    ''' <param name="level">   The level. </param>
    ''' <param name="details"> The details. </param>
    ''' <returns> A String. </returns>
    Public Function Annunciate(ByVal sender As Windows.Forms.ToolStripMenuItem, ByVal level As InfoProviderLevel, ByVal details As String) As String
        If sender IsNot Nothing AndAlso sender.Owner IsNot Nothing Then
            Dim item As ToolStripItem = TryCast(sender, ToolStripItem)
            Dim ownerItem As ToolStripItem = TryCast(sender.OwnerItem, ToolStripItem)
            Do While ownerItem IsNot Nothing
                item = item.OwnerItem
                ownerItem = TryCast(item.OwnerItem, ToolStripItem)
            Loop
            Me.Annunciate(TryCast(item, ToolStripItem), level, details)
        End If
        Return details
    End Function

    ''' <summary> Annunciates error. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="level">  The level. </param>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <param name="args">   A variable-length parameters list containing arguments. </param>
    ''' <returns> A String. </returns>
    Public Function Annunciate(ByVal sender As Windows.Forms.ToolStripMenuItem, ByVal level As InfoProviderLevel, ByVal format As String, ByVal ParamArray args() As Object) As String
        Return Me.Annunciate(sender, level, String.Format(format, args))
    End Function

    ''' <summary> Select icon. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="level"> The level. </param>
    Private Sub SelectIcon(ByVal level As InfoProviderLevel)
        Select Case level
            Case InfoProviderLevel.Alert
                Me.Icon = My.Resources.exclamation
            Case InfoProviderLevel.Error
                Me.Icon = My.Resources.dialog_error_2
            Case InfoProviderLevel.Info
                Me.Icon = My.Resources.dialog_information_3
            Case Else
                Me.Icon = My.Resources.dialog_information_3
        End Select
    End Sub

#End Region

End Class

''' <summary> Values that represent information provider levels. </summary>
''' <remarks> David, 2020-09-24. </remarks>
Public Enum InfoProviderLevel

    ''' <summary> An enum constant representing the Information option. </summary>
    <Description("Information")> Info

    ''' <summary> An enum constant representing the alert option. </summary>
    <Description("Alert")> Alert

    ''' <summary> An enum constant representing the error] option. </summary>
    <Description("Error")> [Error]
End Enum
