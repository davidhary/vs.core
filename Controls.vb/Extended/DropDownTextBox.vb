Imports System.ComponentModel

''' <summary> Drop down text box. </summary>
''' <remarks>
''' (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 12/6/2014 </para>
''' </remarks>
Public Class DropDownTextBox
    Inherits Forma.ModelViewBase

    ''' <summary>
    ''' A private constructor for this class making it not publicly creatable. This ensure using the
    ''' class as a singleton.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()

        ' This call is required by the designer.
        Me.InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me.InitialHeight = Me.Height

        If Me._DropDownHeight <= Me.InitialHeight Then
            Me._DropDownHeight = 300
        End If
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the isr.Core.Controls.DropDownTextBox and optionally
    ''' releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    ''' <summary> Gets the text box. </summary>
    ''' <value> The text box. </value>
    Public ReadOnly Property TextBox As isr.Core.Controls.TextBox
        Get
            Return Me._TextBox
        End Get
    End Property

    ''' <summary> Toggle multi line. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="turnOn"> true to enable, false to disable the turn. </param>
    Private Sub ToggleMultiLine(ByVal turnOn As Boolean)
        If Me._TextBox IsNot Nothing Then
            If turnOn AndAlso Not Me._TextBox.Multiline Then
                If Me.DropDownHeight > Me.InitialHeight Then
                    Me._TextBox.Multiline = True
                    Me.Height = Me.DropDownHeight
                End If
            ElseIf Not turnOn AndAlso Me._TextBox.Multiline Then
                Me._TextBox.Multiline = False
                Me.Height = Me.InitialHeight
            End If
        End If
    End Sub

    ''' <summary> Drop down toggle checked changed. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub DropDownToggle_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _DropDownToggle.CheckedChanged
        Dim checkBox As System.Windows.Forms.CheckBox = TryCast(sender, System.Windows.Forms.CheckBox)
        If checkBox IsNot Nothing Then
            Me.ToggleMultiLine(checkBox.Checked)
            If Me._TextBox IsNot Nothing Then
                checkBox.Image = If(Me._TextBox.Multiline, My.Resources.go_up_8, My.Resources.go_down_8)
            End If
        End If
    End Sub

    ''' <summary> Drop Down Height. </summary>
    ''' <value> The height of the drop down. </value>
    <DefaultValue(300)>
    <Description("Drop Down Height"), Category("Appearance")>
    Public Property DropDownHeight As Integer

    ''' <summary> Gets or sets the height of the initial. </summary>
    ''' <value> The height of the initial. </value>
    Private Property InitialHeight As Integer

End Class
