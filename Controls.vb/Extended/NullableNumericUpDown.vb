﻿Imports System.ComponentModel

''' <summary> A numeric up down with Nullable value. </summary>
''' <remarks>
''' (c) 2011 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 08/19/2011, 1.02.4248.x. </para>
''' </remarks>
<DesignerCategory("code"), System.ComponentModel.Description("Nullable Numeric Up Down")>
Public Class NullableNumericUpDown
    Inherits isr.Core.Controls.NumericUpDownBase

    ''' <summary>
    ''' Initializes a new instance of the <see cref="NullableNumericUpDown" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Gets or sets the Nullable value. </summary>
    ''' <value> The Nullable value. </value>
    <Category("Behavior"), Description("The Nullable value."), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property NullableValue As Decimal?
        Get
            Return If(String.IsNullOrWhiteSpace(Me.TextBox.Text), New Decimal?, Me.Value)
        End Get
        Set(ByVal value As Decimal?)
            If value.HasValue Then
                Me.Value = value.Value
            Else
                Me.TextBox.Text = String.Empty
            End If
        End Set
    End Property

    ''' <summary> Occurs when the underlying text changes. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="source"> The source of the event. </param>
    ''' <param name="e">      An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnChanged(source As Object, ByVal e As System.EventArgs)
        ' this is required so that the Nullable value is updated when spinning after it was set to null.
        Me.OnValueChanged(e)
        MyBase.OnChanged(source, e)
    End Sub

End Class
