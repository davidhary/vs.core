Imports System.ComponentModel

''' <summary> A drop down box with two fields. </summary>
''' <remarks>
''' (c) 2011 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 03/23/2011, 1.02.4099.x. </para>
''' </remarks>
<Description("Dual Field Combo Box"), DefaultBindingProperty("Text")>
Public Class DualFieldComboBox
    Inherits Forma.ModelViewBase

    ''' <summary>
    ''' Releases the unmanaged resources used by the isr.Core.Forma.ModelViewBase and optionally
    ''' releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    ''' <summary> Gets the combo box. </summary>
    ''' <value> The combo box. </value>
    Public ReadOnly Property ComboBox() As System.Windows.Forms.ComboBox
        Get
            Return Me._TextComboBox
        End Get
    End Property

    ''' <summary> Parse record. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub ParseRecord(ByVal value As String)
        If String.IsNullOrWhiteSpace(value) Then ' OrElse String.Is NullOrWhiteSpace(value.Trim) Then
            value = String.Empty
            Me._FirstFieldTextBox.Text = value
            Me._SecondFieldTextBox.Text = value
        Else
            value = value.Trim

            Dim names As String() = value.Split(" "c)
            If names.Length = 1 Then
                Me._SecondFieldTextBox.Text = value
                Me._FirstFieldTextBox.Text = String.Empty
            Else
                Me._SecondFieldTextBox.Text = names(names.Length - 1)
                Me._FirstFieldTextBox.Text = value.Substring(0, value.Length - Me.SecondField.Length).Trim
            End If
        End If
        Me._Text = value
        Me.NotifyPropertyChanged(NameOf(DualFieldComboBox.Text))
    End Sub

    ''' <summary> The text. </summary>
    Private _Text As String

    ''' <summary> Gets or sets the text, which includes the two fields. </summary>
    ''' <value> The full name. </value>
    Public Overrides Property Text() As String
        Get
            Return Me._Text
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then
                value = String.Empty
            End If
            If Not value.Equals(Me.Text) Then
                Me.ParseRecord(value)
            End If
        End Set
    End Property

    ''' <summary> Gets the first field. </summary>
    ''' <value> The first field. </value>
    Public ReadOnly Property FirstField() As String
        Get
            Return Me._FirstFieldTextBox.Text
        End Get
    End Property

    ''' <summary> Gets the second field. </summary>
    ''' <value> The second field. </value>
    Public ReadOnly Property SecondField() As String
        Get
            Return Me._SecondFieldTextBox.Text
        End Get
    End Property

    ''' <summary> Event handler. Called by _ValueComboBox for selected index changed events. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub TextComboBox_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _TextComboBox.SelectedIndexChanged
        Me.ParseRecord(Me._TextComboBox.Text)
    End Sub

    ''' <summary>
    ''' Builds a value based on the <see cref="FirstField">first</see> and
    ''' <see cref="SecondField">second </see> fields.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> The built value. </returns>
    Public Function BuildValue() As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, "{0} {1}", Me.FirstField, Me.SecondField)
    End Function

    ''' <summary> Event handler. Called by _FirstFieldTextBox for validated events. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub FirstFieldTextBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles _FirstFieldTextBox.Validated
        ' Me.parseRecord(Me.BuildValue)
    End Sub

    ''' <summary> Event handler. Called by _SecondFieldTextBox for validated events. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SecondFieldTextBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles _SecondFieldTextBox.Validated
        ' Me.parseRecord(Me.BuildValue)
    End Sub

    ''' <summary> Event handler. Called by DualFieldComboBox for resize events. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub DualFieldComboBox_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        Me.ResizeMe()
    End Sub

    ''' <summary> Event handler. Called by  for  events. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Sub ResizeMe()
        Const buttonWidth As Integer = 17
        Me._Layout.Width = Me.Width - buttonWidth - Me.Padding.Left - Me.Padding.Right
        Me._TextComboBox.Width = Me._Layout.Width + 17 - Me._Layout.Padding.Left
        Me._TextComboBox.Left = Me._Layout.Left + Me._Layout.Padding.Left
        Me._TextComboBox.Top = Me.Height - Me.Padding.Top - Me.Padding.Bottom - Me._TextComboBox.Height
    End Sub

    ''' <summary> Event handler. Called by _TextComboBox for layout events. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Layout event information. </param>
    Private Sub TextComboBox_Layout(ByVal sender As Object, ByVal e As System.Windows.Forms.LayoutEventArgs) Handles _TextComboBox.Layout
        Me.ResizeMe()
    End Sub

    ''' <summary> Event handler. Called by DualFieldComboBox for validated events. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub DualFieldComboBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Validated
        Me.ParseRecord(Me.BuildValue)
    End Sub

End Class
