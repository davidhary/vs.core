﻿Imports System.ComponentModel

''' <summary> Check box with read only capability. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/27/2013 </para>
''' </remarks>
<DesignerCategory("code"), Description("Extended Check Box")>
Public Class CheckBox
    Inherits System.Windows.Forms.CheckBox

    ''' <summary> Gets or sets the read only property. </summary>
    ''' <value> The read only. </value>
    <DefaultValue(False)>
    <Category("Behavior")>
    <Description("Indicates whether the check box is read only.")>
    Public Property [ReadOnly]() As Boolean

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.Control.Click" /> event. Ignored if read only.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnClick(ByVal e As System.EventArgs)
        If Not Me.ReadOnly Then
            MyBase.OnClick(e)
        End If
    End Sub


End Class

