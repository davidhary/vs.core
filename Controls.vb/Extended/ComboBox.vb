Imports System.ComponentModel

Imports isr.Core.Forma.BindingExtensions

''' <summary> Combo box with read only capability. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/27/2013. </para><para>
''' David, 01/07/2018. Added implementation of binding update on item changed. </para><para>
''' Dan.A. http://www.CodeProject.com/KB/ComboBox/CSReadOnlyComboBox.aspx </para>
''' </remarks>
<DesignerCategory("code"), System.ComponentModel.Description("Extended Combo Box")>
Public Class ComboBox
    Inherits System.Windows.Forms.ComboBox

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        MyBase.New()
        Me._DropDownStyle = ComboBoxStyle.DropDown
        '_ReadOnlyBackColor = SystemColors.Control
        '_ReadOnlyForeColor = SystemColors.WindowText
    End Sub

#Region " READ ONLY IMPLEMENTATION "

    ''' <summary> The read write context menu. </summary>
    Private _ReadWriteContextMenu As ContextMenu

    ''' <summary> true to read only. </summary>
    Private _ReadOnly As Boolean

    ''' <summary> Gets or sets the read only. </summary>
    ''' <value> The read only. </value>
    <DefaultValue(False)>
    <Category("Behavior")>
    <Description("Indicates whether the combo box is read only.")>
    Public Property [ReadOnly]() As Boolean
        Get
            Return Me._ReadOnly
        End Get
        Set(ByVal value As Boolean)
            If Me._ReadOnly <> value Then
                Me._ReadOnly = value
                If value Then
                    Me._ReadWriteContextMenu = MyBase.ContextMenu
                    Me.ContextMenu = New ContextMenu()
                    Dim h As Integer = Me.Height
                    MyBase.DropDownStyle = ComboBoxStyle.Simple
                    Me.Height = h + 3
                    Me.BackColor = Me.ReadOnlyBackColor
                    Me.ForeColor = Me.ReadOnlyForeColor
                Else
                    Me.ContextMenu = Me._ReadWriteContextMenu
                    MyBase.DropDownStyle = Me._DropDownStyle
                    Me.BackColor = Me.ReadWriteBackColor
                    Me.ForeColor = Me.ReadWriteForeColor
                End If
            End If
        End Set
    End Property

    ''' <summary> The read only back color. </summary>
    Private _ReadOnlyBackColor As Color

    ''' <summary> Gets or sets the color of the read only back. </summary>
    ''' <value> The color of the read only back. </value>
    <DefaultValue(GetType(Drawing.Color), "SystemColors.Control")>
    <Description("Back color when read only"), Category("Appearance")>
    Public Property ReadOnlyBackColor() As Color
        Get
            If Me._ReadOnlyBackColor.IsEmpty Then
                Me._ReadOnlyBackColor = SystemColors.Control
            End If
            Return Me._ReadOnlyBackColor
        End Get
        Set(value As Color)
            Me._ReadOnlyBackColor = value
        End Set
    End Property

    ''' <summary> The read only foreground color. </summary>
    Private _ReadOnlyForeColor As Color

    ''' <summary> Gets or sets the color of the read only foreground. </summary>
    ''' <value> The color of the read only foreground. </value>
    <DefaultValue(GetType(Drawing.Color), "SystemColors.WindowText")>
    <Description("Fore color when read only"), Category("Appearance")>
    Public Property ReadOnlyForeColor() As Color
        Get
            If Me._ReadOnlyForeColor.IsEmpty Then
                Me._ReadOnlyForeColor = SystemColors.WindowText
            End If
            Return Me._ReadOnlyForeColor
        End Get
        Set(value As Color)
            Me._ReadOnlyForeColor = value
        End Set
    End Property

    ''' <summary> The read write back color. </summary>
    Private _ReadWriteBackColor As System.Drawing.Color

    ''' <summary> Gets or sets the color of the read write back. </summary>
    ''' <value> The color of the read write back. </value>
    <DefaultValue(GetType(System.Drawing.Color), "SystemColors.Window")>
    <Description("Back color when control is read/write"), Category("Appearance")>
    Public Property ReadWriteBackColor() As System.Drawing.Color
        Get
            If Me._ReadWriteBackColor.IsEmpty Then
                Me._ReadWriteBackColor = SystemColors.Window
            End If
            Return Me._ReadWriteBackColor
        End Get
        Set(value As Color)
            Me._ReadWriteBackColor = value
        End Set
    End Property

    ''' <summary> The read write foreground color. </summary>
    Private _ReadWriteForeColor As System.Drawing.Color

    ''' <summary> Gets or sets the color of the read write foreground. </summary>
    ''' <value> The color of the read write foreground. </value>
    <DefaultValue(GetType(System.Drawing.Color), "System.Drawing.SystemColors.ControlText")>
    <Description("Fore color when control is read/write"), Category("Appearance")>
    Public Property ReadWriteForeColor() As System.Drawing.Color
        Get
            If Me._ReadWriteForeColor.IsEmpty Then
                Me._ReadWriteForeColor = SystemColors.ControlText
            End If
            Return Me._ReadWriteForeColor
        End Get
        Set(value As Color)
            Me._ReadWriteForeColor = value
        End Set
    End Property

    ''' <summary> The drop down style. </summary>
    Private _DropDownStyle As ComboBoxStyle

    ''' <summary> Gets or sets the drop down style. </summary>
    ''' <value> The drop down style. </value>
    Public Shadows Property DropDownStyle() As ComboBoxStyle
        Get
            Return Me._DropDownStyle
        End Get
        Set(ByVal value As ComboBoxStyle)
            If Me._DropDownStyle <> value Then
                Me._DropDownStyle = value
                If Not Me.ReadOnly Then
                    MyBase.DropDownStyle = value
                End If
            End If
        End Set
    End Property

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.KeyDown" /> event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.KeyEventArgs" /> that contains the event
    '''                  data. </param>
    Protected Overrides Sub OnKeyDown(ByVal e As KeyEventArgs)
        If e Is Nothing Then Return
        If Me.ReadOnly AndAlso (e.KeyCode = Keys.Up OrElse e.KeyCode = Keys.Down OrElse e.KeyCode = Keys.Delete OrElse
                                e.KeyCode = Keys.F4 OrElse e.KeyCode = Keys.PageDown OrElse e.KeyCode = Keys.PageUp) Then
            e.Handled = True
        Else
            MyBase.OnKeyDown(e)
        End If
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.KeyPress" /> event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.KeyPressEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnKeyPress(ByVal e As KeyPressEventArgs)
        If e Is Nothing Then Return
        If Me.ReadOnly Then
            e.Handled = True
        Else
            MyBase.OnKeyPress(e)
        End If
    End Sub

#End Region

#Region " SELECTED ITEM CHANGE CHANGE BINDING "

    ''' <summary>
    ''' Overrides raising the <see cref="E:System.Windows.Forms.DomainUpDown.SelectedItemChanged" />
    ''' event and updates the data source if <see cref="DataSourceUpdateMode"/> =.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    ''' <seealso cref="DataSourceUpdateMode.OnPropertyChanged "/>
    Protected Overrides Sub OnSelectedItemChanged(e As EventArgs)
        MyBase.OnSelectedItemChanged(e)
        Dim binding As Binding = Me.SelectBinding(NameOf(ComboBox.SelectedItem))
        If binding IsNot Nothing AndAlso binding.DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged Then
            binding.WriteValue()
        End If
    End Sub

#End Region

End Class



