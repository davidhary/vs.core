﻿Imports System.ComponentModel
Imports System.Security.Permissions

''' <summary> Extended Tab control with hidden tab headers. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 4/25/2015, 2.1.5593. </para>
''' </remarks>
<DesignerCategory("code"), System.ComponentModel.Description("Extended Tab Control")>
Public Class ExtendedTabControl
    Inherits System.Windows.Forms.TabControl

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

#Region " Windows Procedure "

    ''' <summary> Windows Procedure override to hide tab headers. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="m"> [in,out] The Windows <see cref="T:System.Windows.Forms.Message" /> to
    '''                  process. </param>
    <SecurityPermission(SecurityAction.Demand, Flags:=SecurityPermissionFlag.UnmanagedCode)>
    Protected Overrides Sub WndProc(ByRef m As System.Windows.Forms.Message)
        ' Hide tabs by trapping the message
        Const TCM_ADJUSTRECT As Integer = &H1328
        If Me.HideTabHeaders AndAlso m.Msg = TCM_ADJUSTRECT AndAlso (Not Me.DesignMode) Then
            m.Result = New IntPtr(1)
        Else
            MyBase.WndProc(m)
        End If
    End Sub

#End Region

    ''' <summary> Gets or sets a value indicating whether the tab headers should be drawn. </summary>
    ''' <value> <c>true</c> to hide tab headers; otherwise <c>false</c> </value>
    <Description("Gets or sets a value indicating whether the tab headers should be drawn"), DefaultValue(False)>
    Public Property HideTabHeaders() As Boolean

End Class
