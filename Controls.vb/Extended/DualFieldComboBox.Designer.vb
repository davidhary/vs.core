﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> 
Partial Class DualFieldComboBox

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> 
    Private Sub InitializeComponent()
        Me._Layout = New System.Windows.Forms.TableLayoutPanel
        Me._SecondFieldTextBox = New System.Windows.Forms.TextBox
        Me._FirstFieldTextBox = New System.Windows.Forms.TextBox
        Me._SecondFieldTextBoxLabel = New System.Windows.Forms.Label
        Me._FirstFieldTextBoxLabel = New System.Windows.Forms.Label
        Me._TextComboBox = New System.Windows.Forms.ComboBox
        Me._Layout.SuspendLayout()
        Me.SuspendLayout()
        '
        '_Layout
        '
        Me._Layout.ColumnCount = 2
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me._Layout.Controls.Add(Me._SecondFieldTextBox, 1, 1)
        Me._Layout.Controls.Add(Me._FirstFieldTextBox, 0, 1)
        Me._Layout.Controls.Add(Me._SecondFieldTextBoxLabel, 1, 0)
        Me._Layout.Controls.Add(Me._FirstFieldTextBoxLabel, 0, 0)
        Me._Layout.Dock = System.Windows.Forms.DockStyle.Left
        Me._Layout.Location = New System.Drawing.Point(0, 0)
        Me._Layout.Name = "_Layout"
        Me._Layout.RowCount = 2
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._Layout.Size = New System.Drawing.Size(339, 41)
        Me._Layout.TabIndex = 0
        '
        '_SecondFieldTextBox
        '
        Me._SecondFieldTextBox.Dock = System.Windows.Forms.DockStyle.Top
        Me._SecondFieldTextBox.Location = New System.Drawing.Point(172, 16)
        Me._SecondFieldTextBox.Name = "_SecondFieldTextBox"
        Me._SecondFieldTextBox.Size = New System.Drawing.Size(164, 20)
        Me._SecondFieldTextBox.TabIndex = 3
        '
        '_FirstFieldTextBox
        '
        Me._FirstFieldTextBox.Dock = System.Windows.Forms.DockStyle.Top
        Me._FirstFieldTextBox.Location = New System.Drawing.Point(3, 16)
        Me._FirstFieldTextBox.Name = "_FirstFieldTextBox"
        Me._FirstFieldTextBox.Size = New System.Drawing.Size(163, 20)
        Me._FirstFieldTextBox.TabIndex = 1
        '
        '_SecondFieldTextBoxLabel
        '
        Me._SecondFieldTextBoxLabel.AutoSize = True
        Me._SecondFieldTextBoxLabel.Dock = System.Windows.Forms.DockStyle.Top
        Me._SecondFieldTextBoxLabel.Location = New System.Drawing.Point(172, 0)
        Me._SecondFieldTextBoxLabel.Name = "_SecondFieldTextBoxLabel"
        Me._SecondFieldTextBoxLabel.Size = New System.Drawing.Size(164, 13)
        Me._SecondFieldTextBoxLabel.TabIndex = 2
        Me._SecondFieldTextBoxLabel.Text = "LAST NAME"
        '
        '_FirstFieldTextBoxLabel
        '
        Me._FirstFieldTextBoxLabel.AutoSize = True
        Me._FirstFieldTextBoxLabel.Dock = System.Windows.Forms.DockStyle.Top
        Me._FirstFieldTextBoxLabel.Location = New System.Drawing.Point(3, 0)
        Me._FirstFieldTextBoxLabel.Name = "_FirstFieldTextBoxLabel"
        Me._FirstFieldTextBoxLabel.Size = New System.Drawing.Size(163, 13)
        Me._FirstFieldTextBoxLabel.TabIndex = 0
        Me._FirstFieldTextBoxLabel.Text = "FIRST NAME"
        '
        '_TextComboBox
        '
        Me._TextComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._TextComboBox.FormattingEnabled = True
        Me._TextComboBox.Location = New System.Drawing.Point(0, 20)
        Me._TextComboBox.Name = "_TextComboBox"
        Me._TextComboBox.Size = New System.Drawing.Size(360, 21)
        Me._TextComboBox.TabIndex = 0
        '
        'DualFieldComboBox
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me._Layout)
        Me.Controls.Add(Me._TextComboBox)
        Me.Name = "DualFieldComboBox"
        Me.Size = New System.Drawing.Size(360, 41)
        Me._Layout.ResumeLayout(False)
        Me._Layout.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _Layout As System.Windows.Forms.TableLayoutPanel
    Private WithEvents _TextComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _SecondFieldTextBox As System.Windows.Forms.TextBox
    Private WithEvents _FirstFieldTextBox As System.Windows.Forms.TextBox
    Private WithEvents _SecondFieldTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _FirstFieldTextBoxLabel As System.Windows.Forms.Label

End Class
