﻿Imports System.ComponentModel

Imports isr.Core.Controls.ExceptionExtensions

''' <summary> Data grid view. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/27/2013 </para>
''' </remarks>
<DesignerCategory("code"), System.ComponentModel.Description("Extended Data Grid View")>
Public Class DataGridView
    Inherits System.Windows.Forms.DataGridView

#Region " CELL FORMATTING "

    ''' <summary>
    ''' Work around to a problem with the data grid view failure to handle the format provider.
    ''' http://stackoverflow.com/questions/3627922/format-time-span-in-datagridview-column.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overrides Sub OnCellFormatting(ByVal e As DataGridViewCellFormattingEventArgs)
        If e Is Nothing Then
            MyBase.OnCellFormatting(e)
        Else
            Dim formatter As ICustomFormatter = TryCast(e.CellStyle.FormatProvider, ICustomFormatter)
            If formatter Is Nothing Then
                MyBase.OnCellFormatting(e)
            Else
                e.Value = formatter.Format(e.CellStyle.Format, e.Value, e.CellStyle.FormatProvider)
                e.FormattingApplied = True
            End If
        End If
    End Sub

#End Region

#Region " DATA ERROR "

    ''' <summary> Ignores the data error. </summary>
    ''' <remarks> Uses the collection to determine if the collection is in edit mode. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      The <see cref="System.Windows.Forms.DataGridViewDataErrorEventArgs"/>
    '''                       instance containing the event data. </param>
    ''' <returns> <c>True</c> if editing error can be ignored, <c>False</c> otherwise. </returns>
    Public Shared Function IgnoreDataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) As Boolean

        If e Is Nothing OrElse sender Is Nothing Then
            Return True
        Else
            Dim grid As DataGridView = TryCast(sender, DataGridView)
            If grid Is Nothing Then
                Return True
            ElseIf grid.CurrentCell Is Nothing OrElse grid.CurrentRow Is Nothing Then
                Return True
            ElseIf grid.CurrentCell.IsInEditMode OrElse grid.CurrentRow.IsNewRow OrElse grid.IsCurrentCellInEditMode OrElse grid.IsCurrentRowDirty Then
                Return True
            End If
        End If
        Return False

    End Function

    ''' <summary> Builds the data error. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="grid"> The grid. </param>
    ''' <param name="e">    The <see cref="System.Windows.Forms.DataGridViewDataErrorEventArgs"/>
    '''                     instance containing the event data. </param>
    ''' <returns> System.String. </returns>
    Public Shared Function BuildDataError(ByVal grid As System.Windows.Forms.DataGridView, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) As String
        ' prevent error reporting when adding a new row or editing a cell
        If grid Is Nothing OrElse e Is Nothing OrElse e.Exception Is Nothing OrElse
            grid.CurrentCell Is Nothing OrElse grid.CurrentRow Is Nothing Then
            Return String.Empty
        Else
            Dim cellValue As String = "nothing"
            If grid.CurrentCell.Value IsNot Nothing Then
                cellValue = grid.CurrentCell.Value.ToString
            End If
            Return $"Data error occurred at Grid {grid.Name}(R{e.RowIndex},C{e.ColumnIndex}):{grid.Columns(e.ColumnIndex).Name}. Cell value is '{cellValue}';. {e.Exception.ToFullBlownString}"
        End If
    End Function

    ''' <summary> Gets or sets a value indicating whether to suppress data error. </summary>
    ''' <value> <c>True</c> if suppressing data error; otherwise, <c>False</c>. </value>
    <Category("Behavior"), Description("Suppresses all data errors."),
        Browsable(True),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
        DefaultValue(False)>
    Public Property SuppressDataError As Boolean

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.DataGridView.DataError" /> event.
    ''' </summary>
    ''' <remarks>
    ''' Allows suppression of data error. This became necessary as we are unable to prevent data
    ''' error when changing the data source of a data grid with combo boxes.
    ''' </remarks>
    ''' <param name="displayErrorDialogIfNoHandler"> true to display an error dialog box if there is
    '''                                              no handler for the
    '''                                              <see cref="E:System.Windows.Forms.DataGridView.DataError" />
    '''                                              event. </param>
    ''' <param name="e">                             A <see cref="T:System.Windows.Forms.DataGridViewDat
    '''                                              aErrorEventArgs" /> that contains the event data. 
    ''' </param>
    Protected Overrides Sub OnDataError(ByVal displayErrorDialogIfNoHandler As Boolean, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs)
        If Not Me.SuppressDataError Then
            MyBase.OnDataError(displayErrorDialogIfNoHandler, e)
        End If
    End Sub


#End Region

End Class
