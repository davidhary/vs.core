Imports System.ComponentModel

''' <summary>
''' Extends the standard <see cref="System.Windows.Forms.NumericUpDown">numeric up
''' down</see>control.
''' </summary>
''' <remarks>
''' (c) 2013 Claudio NiCora HTTP://CoolSoft.AlterVista.org.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/27/2013. Created from http://www.CodeProject.com/KB/edit/NumericUpDownEx.aspx.
''' </para>
''' </remarks>
<DesignerCategory("code"), System.ComponentModel.Description("Numeric Up Down")>
Public Class NumericUpDown
    Inherits isr.Core.Controls.NumericUpDownBase

    ''' <summary> object creator. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Embedded resource names. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> The resource names. </returns>
    Public Shared Function EmbeddedResourceNames() As IEnumerable(Of String)
        Return System.Reflection.Assembly.GetExecutingAssembly.GetManifestResourceNames
    End Function

End Class

