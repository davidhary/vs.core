﻿Imports System.ComponentModel

''' <summary> Radio button. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/27/2013 </para>
''' </remarks>
<DesignerCategory("code"), System.ComponentModel.Description("Extended Radio Button")>
Public Class RadioButton
    Inherits System.Windows.Forms.RadioButton

    ''' <summary> True to readonly. </summary>
    Private _Readonly As Boolean

    ''' <summary> Gets or sets the read only property. </summary>
    ''' <value> The read only. </value>
    <DefaultValue(False)>
    <Category("Behavior")>
    <Description("Indicates whether the radio button is read only.")>
    Public Property [ReadOnly]() As Boolean
        Get
            Return Me._readonly
        End Get
        Set(ByVal value As Boolean)
            If Me._readonly <> value Then
                Me._readonly = value
            End If
        End Set
    End Property

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.Click" /> event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnClick(ByVal e As System.EventArgs)
        If Not Me._Readonly Then
            MyBase.OnClick(e)
        End If
    End Sub

End Class
