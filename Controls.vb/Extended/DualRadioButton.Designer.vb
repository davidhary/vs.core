﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> 
Partial Class DualRadioButton

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> 
    Private Sub InitializeComponent()
        Me._Layout = New System.Windows.Forms.TableLayoutPanel
        Me._PrimaryRadioButton = New isr.Core.Controls.RadioButton
        Me._SecondaryRadioButton = New isr.Core.Controls.RadioButton
        Me._Layout.SuspendLayout()
        Me.SuspendLayout()
        '
        '_Layout
        '
        Me._Layout.BackColor = System.Drawing.Color.Transparent
        Me._Layout.ColumnCount = 3
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me._Layout.Controls.Add(Me._SecondaryRadioButton, 2, 1)
        Me._Layout.Controls.Add(Me._PrimaryRadioButton, 0, 1)
        Me._Layout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Layout.Location = New System.Drawing.Point(0, 0)
        Me._Layout.Name = "_Layout"
        Me._Layout.RowCount = 3
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.Size = New System.Drawing.Size(107, 34)
        Me._Layout.TabIndex = 0
        '
        '_PrimaryRadioButton
        '
        Me._PrimaryRadioButton.AutoSize = True
        Me._PrimaryRadioButton.Checked = True
        Me._PrimaryRadioButton.Dock = System.Windows.Forms.DockStyle.Top
        Me._PrimaryRadioButton.Location = New System.Drawing.Point(3, 8)
        Me._PrimaryRadioButton.Name = "_PrimaryRadioButton"
        Me._PrimaryRadioButton.Size = New System.Drawing.Size(41, 17)
        Me._PrimaryRadioButton.TabIndex = 1
        Me._PrimaryRadioButton.TabStop = True
        Me._PrimaryRadioButton.Text = "ON"
        Me._PrimaryRadioButton.UseVisualStyleBackColor = True
        '
        '_SecondaryRadioButton
        '
        Me._SecondaryRadioButton.AutoSize = True
        Me._SecondaryRadioButton.Dock = System.Windows.Forms.DockStyle.Top
        Me._SecondaryRadioButton.Location = New System.Drawing.Point(59, 8)
        Me._SecondaryRadioButton.Name = "_SecondaryRadioButton"
        Me._SecondaryRadioButton.Size = New System.Drawing.Size(45, 17)
        Me._SecondaryRadioButton.TabIndex = 1
        Me._SecondaryRadioButton.Text = "OFF"
        Me._SecondaryRadioButton.UseVisualStyleBackColor = True
        '
        'DualRadioButton
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Transparent
        Me.Controls.Add(Me._Layout)
        Me.Name = "DualRadioButton"
        Me.Size = New System.Drawing.Size(107, 34)
        Me._Layout.ResumeLayout(False)
        Me._Layout.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _Layout As System.Windows.Forms.TableLayoutPanel
    Private WithEvents _SecondaryRadioButton As isr.Core.Controls.RadioButton
    Private WithEvents _PrimaryRadioButton As isr.Core.Controls.RadioButton

End Class
