Imports System.ComponentModel

''' <summary> Engineering up down. </summary>
''' <remarks>
''' Features: <para>
''' Disables up/down events when read only.</para><para>
''' Adds up/down cursor.</para><para>
''' Adds engineering scaling. </para> (c) 2014 Integrated Scientific Resources, Inc. All rights
''' reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 4/5/2014 </para>
''' </remarks>
<System.ComponentModel.Description("Engineering Up Down")>
Public Class EngineeringUpDown
    Inherits isr.Core.Controls.NumericUpDownBase

#Region " CONSTRUCTION "

    ''' <summary> object creator. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        MyBase.New()
        Me._Unit = String.Empty
        Me.UpdateScalingExponent(0)
    End Sub

#End Region

#Region " TEXT BOX "

    ''' <summary> Strips post fix. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> A String. </returns>
    Private Function StripedText() As String
        Return If(Me.TextBox IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(Me.Postfix) AndAlso
             Me.TextBox.Text.EndsWith(Me.Postfix, StringComparison.OrdinalIgnoreCase),
            Me.TextBox.Text.Substring(0, Me.TextBox.Text.IndexOf(Me.Postfix, StringComparison.OrdinalIgnoreCase)),
            Me.Value.ToString)
    End Function

    ''' <summary> Strips post fix. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Sub StripPostFix()
        If Me.TextBox IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(Me.Postfix) AndAlso
            Me.TextBox.Text.EndsWith(Me.Postfix, StringComparison.OrdinalIgnoreCase) Then
            Me.TextBox.Text = Me.TextBox.Text.Substring(0, Me.TextBox.Text.IndexOf(Me.Postfix, StringComparison.OrdinalIgnoreCase))
        End If
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.LostFocus" /> event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnLostFocus(e As System.EventArgs)
        Me.StripPostFix()
        MyBase.OnLostFocus(e)
        Me.UpdateEditText()
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.Validating" /> event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.ComponentModel.CancelEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnValidating(e As System.ComponentModel.CancelEventArgs)
        Me.StripPostFix()
        MyBase.OnValidating(e)
        Me.UpdateEditText()
    End Sub

    ''' <summary>
    ''' Validates and updates the text displayed in the spin box (also known as an up-down control).
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Protected Overrides Sub ValidateEditText()
        Me.StripPostFix()
        MyBase.ValidateEditText()
    End Sub

    ''' <summary>
    ''' Displays the current value of the spin box (also known as an up-down control) in the
    ''' appropriate format.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Protected Overrides Sub UpdateEditText()
        MyBase.UpdateEditText()
        If Me.TextBox IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(Me.Postfix) Then
            Me.TextBox.AppendText(Me.Postfix)
        End If
    End Sub

#End Region

#Region " ENGINEERING "

    ''' <summary> Builds the engineering scale to scale exponent hash. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> A Dictionary for translating trace events to trace levels. </returns>
    Private Shared Function BuildEngineringUnitHash() As IDictionary(Of Integer, EngineeringScale)
        Dim dix2 As New Dictionary(Of Integer, EngineeringScale)
        Dim dix3 As Dictionary(Of Integer, EngineeringScale) = dix2
        dix3.Add(-18, EngineeringScale.Atto)
        dix3.Add(18, EngineeringScale.Exa)
        dix3.Add(15, EngineeringScale.Femto)
        dix3.Add(-9, EngineeringScale.Giga)
        dix3.Add(-3, EngineeringScale.Kilo)
        dix3.Add(-2, EngineeringScale.Deci)
        dix3.Add(-6, EngineeringScale.Mega)
        dix3.Add(6, EngineeringScale.Micro)
        dix3.Add(2, EngineeringScale.Percent)
        dix3.Add(3, EngineeringScale.Milli)
        dix3.Add(9, EngineeringScale.Nano)
        dix3.Add(-14, EngineeringScale.Peta)
        dix3.Add(12, EngineeringScale.Pico)
        dix3.Add(-12, EngineeringScale.Tera)
        dix3.Add(0, EngineeringScale.Unity)
        Return dix2
    End Function

    ''' <summary> Builds the engineering scale to scale exponent hash. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> A Dictionary for translating trace events to trace levels. </returns>
    Private Shared Function BuildScaleExponentHash() As IDictionary(Of EngineeringScale, Integer)
        Dim dix2 As New Dictionary(Of EngineeringScale, Integer)
        Dim dix3 As Dictionary(Of EngineeringScale, Integer) = dix2
        dix3.Add(EngineeringScale.Atto, -18)
        dix3.Add(EngineeringScale.Exa, 18)
        dix3.Add(EngineeringScale.Femto, 15)
        dix3.Add(EngineeringScale.Giga, -9)
        dix3.Add(EngineeringScale.Deci, -2)
        dix3.Add(EngineeringScale.Kilo, -3)
        dix3.Add(EngineeringScale.Mega, -6)
        dix3.Add(EngineeringScale.Micro, 6)
        dix3.Add(EngineeringScale.Milli, 3)
        dix3.Add(EngineeringScale.Nano, 9)
        dix3.Add(EngineeringScale.Percent, 2)
        dix3.Add(EngineeringScale.Peta, -15)
        dix3.Add(EngineeringScale.Pico, 12)
        dix3.Add(EngineeringScale.Tera, -12)
        dix3.Add(EngineeringScale.Unity, 0)
        Return dix2
    End Function

    ''' <summary> The engineering scale. </summary>
    Private _EngineeringScale As EngineeringScale

    ''' <summary> Gets or sets the engineering scale. </summary>
    ''' <value> The engineering scale. </value>
    <DefaultValue(0)>
    <Category("Behavior")>
    <Description("The engineering scale.")>
    Public Property EngineeringScale As EngineeringScale
        Get
            Return Me._EngineeringScale
        End Get
        Set(value As EngineeringScale)
            Me.UpdateEngineeringScale(value)
        End Set
    End Property

    ''' <summary> The scale exponent hash. </summary>
    Private Shared _ScaleExponentHash As IDictionary(Of EngineeringScale, Integer)

    ''' <summary> Gets the scale exponent hash. </summary>
    ''' <value> The scale exponent hash. </value>
    Private Shared ReadOnly Property ScaleExponentHash As IDictionary(Of EngineeringScale, Integer)
        Get
            If EngineeringUpDown._ScaleExponentHash Is Nothing Then EngineeringUpDown._ScaleExponentHash = EngineeringUpDown.BuildScaleExponentHash()
            Return EngineeringUpDown._ScaleExponentHash
        End Get
    End Property

    ''' <summary> Updates the engineering scale described by value. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub UpdateEngineeringScale(ByVal value As EngineeringScale)
        Me.UpdateScalingExponent(EngineeringUpDown.ScaleExponentHash(value))
    End Sub

    ''' <summary> The scale value prefix hash. </summary>
    Private Shared _ScaleValuePrefixHash As IDictionary(Of Integer, String)

    ''' <summary> Gets the scale value prefix hash. </summary>
    ''' <value> The scale value prefix hash. </value>
    Private Shared ReadOnly Property ScaleValuePrefixHash As IDictionary(Of Integer, String)
        Get
            If EngineeringUpDown._ScaleValuePrefixHash Is Nothing Then EngineeringUpDown._ScaleValuePrefixHash = EngineeringUpDown.BuildScaleValuePrefixHash()
            Return EngineeringUpDown._ScaleValuePrefixHash
        End Get
    End Property

    ''' <summary> The engineering scales hash. </summary>
    Private Shared _EngineeringScalesHash As IDictionary(Of Integer, EngineeringScale)

    ''' <summary> Gets the engineering scales hash. </summary>
    ''' <value> The engineering scales hash. </value>
    Private Shared ReadOnly Property EngineeringScalesHash As IDictionary(Of Integer, EngineeringScale)
        Get
            If EngineeringUpDown._EngineeringScalesHash Is Nothing Then EngineeringUpDown._EngineeringScalesHash = EngineeringUpDown.BuildEngineringUnitHash
            Return EngineeringUpDown._EngineeringScalesHash
        End Get
    End Property

    ''' <summary> Initializes the scale properties. </summary>
    ''' <remarks> Percent units are treated as a special case. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub UpdateScalingExponent(ByVal value As Integer)
        ' treat percent units as a special case
        If value = 2 OrElse value = -2 Then
            Me._ScalingExponent = 2
        Else
            ' must be in powers of 3.
            Me._ScalingExponent = 3 * (value \ 3)
        End If
        Me._ScaleFactor = CDec(Math.Pow(10, -Me._ScalingExponent))
        Me._Postfix = If(value = 0 AndAlso String.IsNullOrWhiteSpace(Me.Unit), String.Empty, $" {EngineeringUpDown.ScaleValuePrefixHash(value)}{Me.Unit}")
        Me._EngineeringScale = EngineeringUpDown.EngineeringScalesHash(Me.ScalingExponent)
        ' update the display to show the change in units, if any.
        Me.UpdateEditText()
        Me.OnValueChanged(System.EventArgs.Empty)
    End Sub

    ''' <summary> Builds a scale exponent to unit prefix hash. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> A Dictionary for translating exponents to units. </returns>
    Private Shared Function BuildScaleValuePrefixHash() As IDictionary(Of Integer, String)
        Dim dix2 As New Dictionary(Of Integer, String)
        Dim dix3 As Dictionary(Of Integer, String) = dix2
        dix3.Add(-18, "E") ' Exa
        dix3.Add(-15, "P") ' Peta
        dix3.Add(-12, "T") ' Tera
        dix3.Add(-9, "G")  ' Giga
        dix3.Add(-6, "M")  ' Mega
        dix3.Add(-2, "D")  ' Deci
        dix3.Add(-3, "K")  ' Kilo
        dix3.Add(0, "")
        dix3.Add(2, "%")  ' Percent
        dix3.Add(3, "m")  ' Milli
        dix3.Add(6, "u")  ' Micro
        dix3.Add(9, "n")  ' Nano
        dix3.Add(12, "p") ' Pico
        dix3.Add(15, "f") ' Femto
        dix3.Add(18, "a") ' Atto
        Return dix2
    End Function

    ''' <summary> The unit. </summary>
    Private _Unit As String

    ''' <summary> The engineering scale to display, e.g., V, A. </summary>
    ''' <value> The unit. </value>
    <DefaultValue("")>
    <Category("Appearance")>
    <Description("The engineering scale to display, e.g., V, A.")>
    Public Property Unit As String
        Get
            Return Me._Unit
        End Get
        Set(value As String)
            Me._Unit = value
            ' update the post fix.
            Me.UpdateScalingExponent(Me.ScalingExponent)
        End Set
    End Property

    ''' <summary> Gets or sets the unscaled value. </summary>
    ''' <value> The unscaled value. </value>
    Public Property UnscaledValue As Decimal
        Get
            Dim value As Decimal = 0
            If Not Decimal.TryParse(Me.StripedText, value) Then
                value = Me.Value
            End If
            Return value
        End Get
        Set(value As Decimal)
            Me.Value = value
        End Set
    End Property

    ''' <summary> Gets the scaled sentinel. </summary>
    ''' <value> <c>True</c> if scaled. </value>
    Private ReadOnly Property IsScaled As Boolean
        Get
            Return Me.ScaleFactor <> 1 AndAlso Me.ScaleFactor <> 0
        End Get
    End Property

    ''' <summary> Gets or sets the scaled value. </summary>
    ''' <value> The scaled value. </value>
    <DefaultValue(0)>
    <Category("Behavior")>
    <Description("The scaled value.")>
    Public Property ScaledValue As Decimal
        Get
            Return If(Me.IsScaled, Me.ScaleFactor * Me.UnscaledValue, Me.UnscaledValue)
        End Get
        Set(value As Decimal)
            If Me.IsScaled Then
                value /= Me.ScaleFactor
            End If
            If Me.TextBox IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(Me.Postfix) Then
                Me.TextBox.Text = value.ToString
            End If
            Me.UnscaledValue = value
            Me.TextBox.AppendText(Me.Postfix)
        End Set
    End Property

    ''' <summary> The scaling exponent. </summary>
    Private _ScalingExponent As Integer

    ''' <summary> Gets or sets the scaling exponent. </summary>
    ''' <value> The scaling exponent. </value>
    Protected Property ScalingExponent As Integer
        Get
            Return Me._ScalingExponent
        End Get
        Set(value As Integer)
            Me.UpdateScalingExponent(value)
        End Set
    End Property

    ''' <summary> Gets or sets the scale factor. </summary>
    ''' <value> The scale factor. </value>
    Protected Property ScaleFactor As Decimal

    ''' <summary> Gets or sets the text to append to the display value. </summary>
    ''' <value> The post fix. </value>
    Protected Property Postfix As String

#End Region

End Class

''' <summary> Values that represent engineering scale. </summary>
''' <remarks> David, 2020-09-24. </remarks>
Public Enum EngineeringScale

    ''' <summary> . </summary>
    <Description("Unity (0)")> Unity = 0

    ''' <summary> An enum constant representing the atto option. </summary>
    <Description("Atto (a:-18)")> Atto

    ''' <summary> An enum constant representing the femto option. </summary>
    <Description("Femto (f:-15)")> Femto

    ''' <summary> An enum constant representing the pico option. </summary>
    <Description("Pico (p:-12)")> Pico

    ''' <summary> An enum constant representing the nano option. </summary>
    <Description("Nano (n:-9)")> Nano

    ''' <summary> An enum constant representing the micro option. </summary>
    <Description("Micro (u:-6)")> Micro

    ''' <summary> An enum constant representing the milli option. </summary>
    <Description("Milli (m:-3)")> Milli

    ''' <summary> An enum constant representing the deci option. </summary>
    <Description("Deci (D:2)")> Deci

    ''' <summary> An enum constant representing the percent option. </summary>
    <Description("Percent (D:-2)")> Percent

    ''' <summary> An enum constant representing the kilo option. </summary>
    <Description("Kilo (K:3)")> Kilo

    ''' <summary> An enum constant representing the mega option. </summary>
    <Description("Mega (M:6)")> Mega

    ''' <summary> An enum constant representing the giga option. </summary>
    <Description("Giga (G:9)")> Giga

    ''' <summary> An enum constant representing the tera option. </summary>
    <Description("Tera (T:12)")> Tera

    ''' <summary> An enum constant representing the peta option. </summary>
    <Description("Peta (P:15)")> Peta

    ''' <summary> An enum constant representing the exa option. </summary>
    <Description("Exa (E:18)")> Exa
End Enum
