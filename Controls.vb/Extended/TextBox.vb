Imports System.ComponentModel

''' <summary> Text box with read only non-validation. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/27/2013 </para>
''' </remarks>
<DesignerCategory("code"), System.ComponentModel.Description("Text Box")>
Public Class TextBox
    Inherits System.Windows.Forms.TextBox

#Region " CONSTRUCTION "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        MyBase.New()
        MyBase.ReadOnly = False
        Me.ApplyReadOnly()
    End Sub

#End Region

#Region " READ ONLY "

    ''' <summary> The read only back color. </summary>
    Private _ReadOnlyBackColor As Color

    ''' <summary> Gets or sets the color of the read only back. </summary>
    ''' <value> The color of the read only back. </value>
    <DefaultValue(GetType(Drawing.Color), "SystemColors.Control")>
    <Description("Back color when read only"), Category("Appearance")>
    Public Property ReadOnlyBackColor() As Color
        Get
            If Me._ReadOnlyBackColor.IsEmpty Then
                Me._ReadOnlyBackColor = SystemColors.Control
            End If
            Return Me._ReadOnlyBackColor
        End Get
        Set(value As Color)
            Me._ReadOnlyBackColor = value
        End Set
    End Property

    ''' <summary> The read only foreground color. </summary>
    Private _ReadOnlyForeColor As Color

    ''' <summary> Gets or sets the color of the read only foreground. </summary>
    ''' <value> The color of the read only foreground. </value>
    <DefaultValue(GetType(Drawing.Color), "SystemColors.WindowText")>
    <Description("Fore color when read only"), Category("Appearance")>
    Public Property ReadOnlyForeColor() As Color
        Get
            If Me._ReadOnlyForeColor.IsEmpty Then
                Me._ReadOnlyForeColor = SystemColors.WindowText
            End If
            Return Me._ReadOnlyForeColor
        End Get
        Set(value As Color)
            Me._ReadOnlyForeColor = value
        End Set
    End Property

    ''' <summary> The read write back color. </summary>
    Private _ReadWriteBackColor As System.Drawing.Color

    ''' <summary> Gets or sets the color of the read write back. </summary>
    ''' <value> The color of the read write back. </value>
    <DefaultValue(GetType(System.Drawing.Color), "SystemColors.Window")>
    <Description("Back color when control is read/write"), Category("Appearance")>
    Public Property ReadWriteBackColor() As System.Drawing.Color
        Get
            If Me._ReadWriteBackColor.IsEmpty Then
                Me._ReadWriteBackColor = SystemColors.Window
            End If
            Return Me._ReadWriteBackColor
        End Get
        Set(value As Color)
            Me._ReadWriteBackColor = value
        End Set
    End Property

    ''' <summary> The read write foreground color. </summary>
    Private _ReadWriteForeColor As System.Drawing.Color

    ''' <summary> Gets or sets the color of the read write foreground. </summary>
    ''' <value> The color of the read write foreground. </value>
    <DefaultValue(GetType(System.Drawing.Color), "System.Drawing.SystemColors.ControlText")>
    <Description("Fore color when control is read/write"), Category("Appearance")>
    Public Property ReadWriteForeColor() As System.Drawing.Color
        Get
            If Me._ReadWriteForeColor.IsEmpty Then
                Me._ReadWriteForeColor = SystemColors.ControlText
            End If
            Return Me._ReadWriteForeColor
        End Get
        Set(value As Color)
            Me._ReadWriteForeColor = value
        End Set
    End Property

    ''' <summary> The read write context menu. </summary>
    Private _ReadWriteContextMenu As ContextMenu

    ''' <summary> Applies the read only. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Sub ApplyReadOnly()
        If MyBase.ReadOnly Then
            Me._ReadWriteContextMenu = MyBase.ContextMenu
            Me.ContextMenu = New ContextMenu()
            Me.BackColor = Me.ReadOnlyBackColor
            Me.ForeColor = Me.ReadOnlyForeColor
        Else
            Me.ContextMenu = Me._ReadWriteContextMenu
            Me.BackColor = Me.ReadWriteBackColor
            Me.ForeColor = Me.ReadWriteForeColor
        End If
        Me.CausesValidation = Not MyBase.ReadOnly
    End Sub

    ''' <summary> Gets or sets the read only. </summary>
    ''' <value> The read only. </value>
    <DefaultValue(False)>
    <Category("Behavior")>
    <Description("Indicates whether the combo box is read only.")>
    Public Overloads Property [ReadOnly]() As Boolean
        Get
            Return MyBase.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            If Me.ReadOnly <> value Then
                MyBase.ReadOnly = value
                Me.ApplyReadOnly()
            End If
        End Set
    End Property

#End Region

End Class
