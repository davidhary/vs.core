Imports System.ComponentModel

Imports isr.Core.Controls.ExceptionExtensions

''' <summary>
''' A dual radio button control functioning as a check box control displaying both its states.
''' </summary>
''' <remarks>
''' (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 12/09/2010, 1.02.3995.x. </para>
''' </remarks>
<Description("Dual Radio Button"), DefaultEvent("CheckToggled"), DefaultBindingProperty("Checked")>
Public Class DualRadioButton
    Inherits Forma.ModelViewBase

#Region " CONSTRUCTION "

    ''' <summary> Clears internal properties. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        MyBase.New()
        Me.InitializeComponent()
        Me._PrimaryButtonHorizontalLocation = HorizontalLocation.Left
        Me._PrimaryButtonVerticalLocation = VerticalLocation.Center
        Me._SecondaryButtonHorizontalLocation = HorizontalLocation.Right
        Me._SecondaryButtonVerticalLocation = VerticalLocation.Center
        Me._PrimaryRadioButton.ReadOnly = False
        Me._SecondaryRadioButton.ReadOnly = False
        Me._Checked = False
        Me._PrimaryRadioButton.Checked = False
        Me._SecondaryRadioButton.Checked = False
        Me.ArrangeButtons()
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the isr.Core.Forma.ModelViewBase and optionally
    ''' releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
                Me.RemoveCheckToggledEventHandler(Me.CheckToggledEvent)
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " BEHAVIOR "

    ''' <summary> Gets or sets the read only property. </summary>
    ''' <value> The read only. </value>
    <DefaultValue(False)>
    <Category("Behavior")>
    <Description("Indicates whether the dual radio button is read only.")>
    Public Property [ReadOnly]() As Boolean
        Get
            Return Me._PrimaryRadioButton.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            If Me.ReadOnly <> value Then
                Me._PrimaryRadioButton.ReadOnly = value
                Me._SecondaryRadioButton.ReadOnly = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> <c>True</c> if checked. </summary>
    Private _Checked As Boolean

    ''' <summary> Gets or sets the checked property. </summary>
    ''' <value> The checked. </value>
    <DefaultValue(False)>
    <Category("Behavior")>
    <Description("Indicates whether the radio button is checked or not.")>
    Public Property Checked() As Boolean
        Get
            Return Me._Checked
        End Get
        Set(ByVal value As Boolean)
            If Me.Checked <> value Then
                Me._Checked = value
                Me._PrimaryRadioButton.Checked = value
                Me._SecondaryRadioButton.Checked = Not value
                Me.NotifyPropertyChanged()
                Me.OnCheckToggled()
            End If
        End Set
    End Property

#End Region

#Region " APPEARANCE "

    ''' <summary> The checked color. </summary>
    Private _CheckedColor As Drawing.Color

    ''' <summary> Checked background color. </summary>
    ''' <value> The color of the checked. </value>
    <Category("Appearance"), Description("Checked background color"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
    DefaultValue(GetType(Drawing.Color), "LightGreen")>
    Public Property CheckedColor As Drawing.Color
        Get
            Return Me._CheckedColor
        End Get
        Set(value As Drawing.Color)
            If Not Drawing.Color.Equals(Me.CheckedColor, value) Then
                Me._CheckedColor = value
                Me.NotifyPropertyChanged()
                Me.RadioButtonCheckedChanged(Me._PrimaryRadioButton)
                Me.RadioButtonCheckedChanged(Me._SecondaryRadioButton)
            End If
        End Set
    End Property

    ''' <summary> The unchecked color. </summary>
    Private _UncheckedColor As Drawing.Color

    ''' <summary> Unchecked background color. </summary>
    ''' <value> The color of the unchecked. </value>
    <Category("Appearance"), Description("Unchecked background color"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
    DefaultValue(GetType(Drawing.Color), "DarkSeaGreen")>
    Public Property UncheckedColor As Drawing.Color
        Get
            Return Me._UncheckedColor
        End Get
        Set(value As Drawing.Color)
            If Not Drawing.Color.Equals(Me.UncheckedColor, value) Then
                Me._UncheckedColor = value
                Me.NotifyPropertyChanged()
                Me.RadioButtonCheckedChanged(Me._PrimaryRadioButton)
                Me.RadioButtonCheckedChanged(Me._SecondaryRadioButton)
            End If
        End Set
    End Property

#End Region

#Region " PRIMARY BUTTON "

    ''' <summary> Primary Button appearance. </summary>
    ''' <value> The Primary Button appearance. </value>
    <Category("Appearance"), Description("Primary Button appearance"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
    DefaultValue(GetType(Windows.Forms.Appearance), "Button")>
    Public Property PrimaryButtonAppearance As Windows.Forms.Appearance
        Get
            Return Me._PrimaryRadioButton.Appearance
        End Get
        Set(value As Windows.Forms.Appearance)
            If Me.PrimaryButtonAppearance <> value Then
                Me._PrimaryRadioButton.Appearance = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Primary Button font. </summary>
    ''' <value> The Primary Button font. </value>
    <Category("Appearance"), Description("Primary Button font"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)>
    Public Property PrimaryButtonFont As System.Drawing.Font
        Get
            Return Me._PrimaryRadioButton.Font
        End Get
        Set(value As System.Drawing.Font)
            If Not Font.Equals(Me.PrimaryButtonFont, value) Then
                Me._PrimaryRadioButton.Font = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The primary button horizontal location. </summary>
    Private _PrimaryButtonHorizontalLocation As HorizontalLocation

    ''' <summary> Gets or sets the primary button horizontal location. </summary>
    ''' <value> The primary button horizontal location. </value>
    <DefaultValue(GetType(HorizontalLocation), "Left")>
    <Category("Behavior")>
    <Description("Horizontal location of the primary button.")>
    Public Property PrimaryButtonHorizontalLocation() As HorizontalLocation
        Get
            Return Me._PrimaryButtonHorizontalLocation
        End Get
        Set(ByVal value As HorizontalLocation)
            If value <> Me.PrimaryButtonHorizontalLocation Then
                Me._PrimaryButtonHorizontalLocation = value
                Me.NotifyPropertyChanged()
                Me.ArrangeButtons()
            End If
        End Set
    End Property

    ''' <summary> The primary button vertical location. </summary>
    Private _PrimaryButtonVerticalLocation As VerticalLocation

    ''' <summary> Gets or sets the primary button vertical location. </summary>
    ''' <value> The primary button vertical location. </value>
    <DefaultValue(GetType(VerticalLocation), "Center")>
    <Category("Behavior")>
    <Description("Vertical location of the primary button.")>
    Public Property PrimaryButtonVerticalLocation() As VerticalLocation
        Get
            Return Me._PrimaryButtonVerticalLocation
        End Get
        Set(ByVal value As VerticalLocation)
            If value <> Me.PrimaryButtonVerticalLocation Then
                Me._PrimaryButtonVerticalLocation = value
                Me.NotifyPropertyChanged()
                Me.ArrangeButtons()
            End If
        End Set
    End Property

    ''' <summary> Primary Button Text. </summary>
    ''' <value> The Primary Button text. </value>
    <Category("Appearance"), Description("Primary Button Text"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue("O&N")>
    Public Property PrimaryButtonText As String
        Get
            Return Me._PrimaryRadioButton.Text
        End Get
        Set(value As String)
            If Not String.Equals(Me.PrimaryButtonText, value) Then
                Me._PrimaryRadioButton.Text = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the Primary Button toolTip. </summary>
    ''' <value> The Primary Button toolTip. </value>
    <Category("Appearance"), Description("Primary Button text alignment"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
    DefaultValue(GetType(Drawing.ContentAlignment), "MiddleCenter")>
    Public Property PrimaryButtonTextAlign As Drawing.ContentAlignment
        Get
            Return Me._PrimaryRadioButton.TextAlign
        End Get
        Set(value As Drawing.ContentAlignment)
            If Me.PrimaryButtonTextAlign <> value Then
                Me._PrimaryRadioButton.TextAlign = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The primary button tool tip. </summary>
    Private _PrimaryButtonToolTip As String

    ''' <summary> Gets or sets the Primary Button toolTip. </summary>
    ''' <value> The Primary Button toolTip. </value>
    <Category("Appearance"), Description("Primary Button Tool Tip"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue("Press to turn on")>
    Public Property PrimaryButtonToolTip As String
        Get
            Return Me._PrimaryButtonToolTip
        End Get
        Set(value As String)
            If Not String.Equals(Me.PrimaryButtonToolTip, value) Then
                Me._PrimaryButtonToolTip = value
                Me.ToolTip.SetToolTip(Me._PrimaryRadioButton, value)
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " SECONDARY BUTTON "

    ''' <summary> Secondary Button appearance. </summary>
    ''' <value> The Secondary Button appearance. </value>
    <Category("Appearance"), Description("Secondary Button appearance"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
    DefaultValue(GetType(Windows.Forms.Appearance), "Button")>
    Public Property SecondaryButtonAppearance As Windows.Forms.Appearance
        Get
            Return Me._SecondaryRadioButton.Appearance
        End Get
        Set(value As Windows.Forms.Appearance)
            If Me.SecondaryButtonAppearance <> value Then
                Me._SecondaryRadioButton.Appearance = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Secondary Button font. </summary>
    ''' <value> The Secondary Button font. </value>
    <Category("Appearance"), Description("Secondary Button font"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)>
    Public Property SecondaryButtonFont As System.Drawing.Font
        Get
            Return Me._SecondaryRadioButton.Font
        End Get
        Set(value As System.Drawing.Font)
            If Not Font.Equals(Me.SecondaryButtonFont, value) Then
                Me._SecondaryRadioButton.Font = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The secondary button horizontal location. </summary>
    Private _SecondaryButtonHorizontalLocation As HorizontalLocation

    ''' <summary> Gets or sets the secondary button horizontal location. </summary>
    ''' <value> The secondary button horizontal location. </value>
    <DefaultValue(GetType(HorizontalLocation), "Right")>
    <Category("Behavior")>
    <Description("Horizontal location of the Secondary button.")>
    Public Property SecondaryButtonHorizontalLocation() As HorizontalLocation
        Get
            Return Me._SecondaryButtonHorizontalLocation
        End Get
        Set(ByVal value As HorizontalLocation)
            If value <> Me.SecondaryButtonHorizontalLocation Then
                Me._SecondaryButtonHorizontalLocation = value
                Me.NotifyPropertyChanged()
                Me.ArrangeButtons()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the text associated with the secondary button. </summary>
    ''' <value> The secondary button text. </value>
    <Category("Appearance"), Description("The text of the Secondary button"),
        Browsable(True),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
        DefaultValue("O&FF")>
    Public Property SecondaryButtonText() As String
        Get
            Return Me._SecondaryRadioButton.Text
        End Get
        Set(ByVal value As String)
            If Not String.Equals(Me.SecondaryButtonText, value) Then
                Me._SecondaryRadioButton.Text = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The secondary button vertical location. </summary>
    Private _SecondaryButtonVerticalLocation As VerticalLocation

    ''' <summary> Gets or sets the secondary button vertical location. </summary>
    ''' <value> The secondary button vertical location. </value>
    <DefaultValue(GetType(VerticalLocation), "Center")>
    <Category("Behavior")>
    <Description("Vertical location of the Secondary button.")>
    Public Property SecondaryButtonVerticalLocation() As VerticalLocation
        Get
            Return Me._SecondaryButtonVerticalLocation
        End Get
        Set(ByVal value As VerticalLocation)
            If value <> Me.SecondaryButtonVerticalLocation Then
                Me._SecondaryButtonVerticalLocation = value
                Me.NotifyPropertyChanged()
                Me.ArrangeButtons()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the Secondary Button toolTip. </summary>
    ''' <value> The Secondary Button toolTip. </value>
    <Category("Appearance"), Description("Secondary Button text alignment"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
    DefaultValue(GetType(Drawing.ContentAlignment), "MiddleCenter")>
    Public Property SecondaryButtonTextAlign As Drawing.ContentAlignment
        Get
            Return Me._SecondaryRadioButton.TextAlign
        End Get
        Set(value As Drawing.ContentAlignment)
            If Me.SecondaryButtonTextAlign <> value Then
                Me._SecondaryRadioButton.TextAlign = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The secondary button tool tip. </summary>
    Private _SecondaryButtonToolTip As String

    ''' <summary> Gets or sets the Secondary Button toolTip. </summary>
    ''' <value> The Secondary Button toolTip. </value>
    <Category("Appearance"), Description("Secondary Button Tool Tip"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue("Press to turn off")>
    Public Property SecondaryButtonToolTip As String
        Get
            Return Me._SecondaryButtonToolTip
        End Get
        Set(value As String)
            If Not String.Equals(Me.SecondaryButtonToolTip, value) Then
                Me._SecondaryButtonToolTip = value
                Me.ToolTip.SetToolTip(Me._PrimaryRadioButton, value)
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " EVENT HANDLERS "

    ''' <summary> Handles the control checked changed described by control. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="control"> The control. </param>
    Private Sub RadioButtonCheckedChanged(ByVal control As Windows.Forms.RadioButton)
        control.BackColor = If(control.Checked, Me.CheckedColor, Me.UncheckedColor)
    End Sub

    ''' <summary> Handles the primary radio button checked changed described by sender. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    Private Sub PrimaryRadioButtonCheckedChanged(sender As Object)
        Dim control As RadioButton = TryCast(sender, RadioButton)
        If control IsNot Nothing Then
            Me.Checked = control.Checked
            Me._SecondaryRadioButton.Checked = Not control.Checked
            Me.RadioButtonCheckedChanged(control)
        End If
    End Sub

    ''' <summary> Handles the secondary radio button checked changed described by sender. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    Private Sub SecondaryRadioButtonCheckedChanged(sender As Object)
        Dim control As RadioButton = TryCast(sender, RadioButton)
        If control IsNot Nothing Then
            Me.Checked = Not control.Checked
            control.BackColor = If(control.Checked, Me.CheckedColor, Me.UncheckedColor)
            Me.RadioButtonCheckedChanged(control)
        End If
    End Sub

    ''' <summary> Raises the radio button checked changed event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub PrimaryRadioButtonCheckedChanged(sender As Object, e As EventArgs) Handles _PrimaryRadioButton.CheckedChanged
        Me.PrimaryRadioButtonCheckedChanged(sender)
    End Sub

    ''' <summary> Off radio button checked changed. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SecondaryRadioButtonCheckedChanged(sender As Object, e As EventArgs) Handles _SecondaryRadioButton.CheckedChanged
        Me.SecondaryRadioButtonCheckedChanged(sender)
    End Sub

    ''' <summary> Gets the row and column for the button withing the control layout. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="verticalLocation">   The vertical location. </param>
    ''' <param name="horizontalLocation"> The horizontal location. </param>
    ''' <returns> A (Column As Integer, Row As Integer) </returns>
    Private Shared Function LocateButton(ByVal verticalLocation As VerticalLocation, ByVal horizontalLocation As HorizontalLocation) As (Column As Integer, Row As Integer)

        Dim column As Integer = 1
        Dim row As Integer = 1

        Select Case verticalLocation
            Case isr.Core.Controls.VerticalLocation.Bottom
                row = 2
            Case isr.Core.Controls.VerticalLocation.Center
                row = 1
            Case isr.Core.Controls.VerticalLocation.Top
                row = 0
        End Select

        Select Case horizontalLocation
            Case HorizontalLocation.Center
                column = 1
            Case HorizontalLocation.Left
                column = 0
            Case HorizontalLocation.Right
                column = 2
        End Select

        Return (column, row)
    End Function

    ''' <summary> Arrange buttons within the control layout. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Sub ArrangeButtons()

        Dim primaryLocation As (column As Integer, row As Integer) = DualRadioButton.LocateButton(Me.PrimaryButtonVerticalLocation, Me.PrimaryButtonHorizontalLocation)
        Dim secondaryLocation As (column As Integer, row As Integer) = DualRadioButton.LocateButton(Me.SecondaryButtonVerticalLocation, Me.SecondaryButtonHorizontalLocation)

        Dim hundredPercent As Single = 100.0!

        ' clear the styles
        For Each colStyle As ColumnStyle In Me._Layout.ColumnStyles
            colStyle.SizeType = SizeType.AutoSize
        Next
        For Each rowStyle As RowStyle In Me._Layout.RowStyles
            rowStyle.SizeType = SizeType.AutoSize
        Next
        Me._Layout.RowStyles(primaryLocation.row).SizeType = SizeType.Percent
        Me._Layout.RowStyles(secondaryLocation.row).SizeType = SizeType.Percent
        Me._Layout.ColumnStyles(primaryLocation.column).SizeType = SizeType.Percent
        Me._Layout.ColumnStyles(secondaryLocation.column).SizeType = SizeType.Percent

        Dim percentCount As Integer = 0
        For Each rowStyle As RowStyle In Me._Layout.RowStyles
            If rowStyle.SizeType = SizeType.Percent Then
                percentCount += 1
            End If
        Next
        For Each rowStyle As RowStyle In Me._Layout.RowStyles
            If rowStyle.SizeType = SizeType.Percent Then
                rowStyle.Height = hundredPercent / percentCount
            End If
        Next
        percentCount = 0
        For Each columnStyle As ColumnStyle In Me._Layout.ColumnStyles
            If columnStyle.SizeType = SizeType.Percent Then
                percentCount += 1
            End If
        Next
        For Each columnStyle As ColumnStyle In Me._Layout.ColumnStyles
            If columnStyle.SizeType = SizeType.Percent Then
                columnStyle.Width = hundredPercent / percentCount
            End If
        Next
        Me._Layout.Controls.Clear()
        Me._PrimaryRadioButton.Dock = DockStyle.None
        Me._SecondaryRadioButton.Dock = DockStyle.None
        Me._Layout.Controls.Add(Me._PrimaryRadioButton, primaryLocation.column, primaryLocation.row)
        Me._Layout.Controls.Add(Me._SecondaryRadioButton, secondaryLocation.column, secondaryLocation.row)
        Me._PrimaryRadioButton.Dock = DockStyle.Fill
        Me._SecondaryRadioButton.Dock = DockStyle.Fill

    End Sub

#End Region

#Region " EVENTS "

    ''' <summary>Occurs when the checked value changed. </summary>
    ''' <param name="e">Specifies the <see cref="EventArgs">event arguments</see>. </param>
    Public Event CheckToggled As EventHandler(Of EventArgs)

    ''' <summary> Removes event handler. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The handler. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub RemoveCheckToggledEventHandler(ByVal value As EventHandler(Of EventArgs))
#Disable Warning CA1825 ' Avoid zero-length array allocations.
        For Each d As [Delegate] In If(value Is Nothing, New [Delegate]() {}, value.GetInvocationList)
#Enable Warning CA1825 ' Avoid zero-length array allocations.
            Try
                RemoveHandler Me.CheckToggled, CType(d, EventHandler(Of EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

    ''' <summary> Raises the Check changed event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Protected Overridable Sub OnCheckToggled()
        Dim evt As EventHandler(Of EventArgs) = Me.CheckToggledEvent
        evt?.Invoke(Me, EventArgs.Empty)
    End Sub

#End Region

End Class

''' <summary> Values that represent HorizontalLocation. </summary>
''' <remarks> David, 2020-09-24. </remarks>
Public Enum HorizontalLocation
    ''' <summary>Button located on the left side of the control (default behavior)</summary>
    Left
    ''' <summary>Button located in the center of the control</summary>
    Center
    ''' <summary>Button located at the right side of the control</summary>
    Right
End Enum

''' <summary> Values that represent VerticalLocation. </summary>
''' <remarks> David, 2020-09-24. </remarks>
Public Enum VerticalLocation
    ''' <summary>Button located on the top side of the control (default behavior)</summary>
    Top
    ''' <summary>Button located in the center of the control</summary>
    Center
    ''' <summary>Button located at the bottom of the control</summary>
    Bottom
End Enum

