Imports System.Drawing.Imaging

''' <summary> An image helper. </summary>
''' <remarks> David, 2020-09-24. </remarks>
Public NotInheritable Class ImageHelper

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Sub New()
    End Sub

    ''' <summary> The color matrix elements. </summary>
    Private Shared ReadOnly ColorMatrixElements()() As Single = {New Single() {CSng(0.299), CSng(0.299), CSng(0.299), 0, 0}, New Single() {CSng(0.587), CSng(0.587), CSng(0.587), 0, 0}, New Single() {CSng(0.114), CSng(0.114), CSng(0.114), 0, 0}, New Single() {0, 0, 0, 1, 0}, New Single() {0, 0, 0, 0, 1}}

    ''' <summary> The grayscale color matrix. </summary>
    Private Shared ReadOnly GrayscaleColorMatrix As New ColorMatrix(ColorMatrixElements)

    ''' <summary> Gets grayscale attributes. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> The grayscale attributes. </returns>
    Public Shared Function GetGrayscaleAttributes() As ImageAttributes
        Dim attr As New ImageAttributes()
        attr.SetColorMatrix(GrayscaleColorMatrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap)
        Return attr
    End Function

    ''' <summary> Rescale image to fit. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="imageSize">  Size of the image. </param>
    ''' <param name="canvasSize"> Size of the canvas. </param>
    ''' <returns> A Size. </returns>
    Public Shared Function RescaleImageToFit(ByVal imageSize As Size, ByVal canvasSize As Size) As Size
        'Code "borrowed" from http://stackoverflow.com/questions/1940581/c-sharp-image-resizing-to-different-size-while-preserving-aspect-ratio
        ' and the Math.Min improvement from http://stackoverflow.com/questions/6501797/resize-image-proportionally-with-maxheight-and-maxwidth-constraints

        ' Figure out the ratio
        Dim ratioX As Double = CDbl(canvasSize.Width) / CDbl(imageSize.Width)
        Dim ratioY As Double = CDbl(canvasSize.Height) / CDbl(imageSize.Height)

        ' use whichever multiplier is smaller
        Dim ratio As Double = Math.Min(ratioX, ratioY)

        ' now we can get the new height and width
        Dim newHeight As Integer = Convert.ToInt32(imageSize.Height * ratio)
        Dim newWidth As Integer = Convert.ToInt32(imageSize.Width * ratio)

        Dim resizedSize As New Size(newWidth, newHeight)

        If resizedSize.Width > canvasSize.Width OrElse resizedSize.Height > canvasSize.Height Then
            Throw New InvalidOperationException("Rescale image to fit - Resize failed!")
        End If

        Return resizedSize
    End Function
End Class
