﻿Imports isr.Core.ColorExtensions

''' <summary> A toggle switch metro renderer. </summary>
''' <remarks>
''' ((c) 2015 Johnny J.. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 12/4/2015 </para>
''' </remarks>
Public Class ToggleSwitchMetroRenderer
    Inherits ToggleSwitchRendererBase

#Region "Constructor"

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        Me.BackColor = Color.White
        Me.LeftSideColor = Color.FromArgb(255, 23, 153, 0)
        Me.LeftSideColorHovered = Color.FromArgb(255, 36, 182, 9)
        Me.LeftSideColorPressed = Color.FromArgb(255, 121, 245, 100)
        Me.RightSideColor = Color.FromArgb(255, 166, 166, 166)
        Me.RightSideColorHovered = Color.FromArgb(255, 181, 181, 181)
        Me.RightSideColorPressed = Color.FromArgb(255, 189, 189, 189)
        Me.BorderColor = Color.FromArgb(255, 166, 166, 166)
        Me.ButtonColor = Color.Black
        Me.ButtonColorHovered = Color.Black
        Me.ButtonColorPressed = Color.Black
    End Sub

#End Region ' Constructor

#Region "Public Properties"

    ''' <summary> Gets or sets the color of the back. </summary>
    ''' <value> The color of the back. </value>
    Public Property BackColor() As Color

    ''' <summary> Gets or sets the color of the left side. </summary>
    ''' <value> The color of the left side. </value>
    Public Property LeftSideColor() As Color

    ''' <summary> Gets or sets the left side color hovered. </summary>
    ''' <value> The left side color hovered. </value>
    Public Property LeftSideColorHovered() As Color

    ''' <summary> Gets or sets the left side color pressed. </summary>
    ''' <value> The left side color pressed. </value>
    Public Property LeftSideColorPressed() As Color

    ''' <summary> Gets or sets the color of the right side. </summary>
    ''' <value> The color of the right side. </value>
    Public Property RightSideColor() As Color

    ''' <summary> Gets or sets the right side color hovered. </summary>
    ''' <value> The right side color hovered. </value>
    Public Property RightSideColorHovered() As Color

    ''' <summary> Gets or sets the right side color pressed. </summary>
    ''' <value> The right side color pressed. </value>
    Public Property RightSideColorPressed() As Color

    ''' <summary> Gets or sets the color of the border. </summary>
    ''' <value> The color of the border. </value>
    Public Property BorderColor() As Color

    ''' <summary> Gets or sets the color of the button. </summary>
    ''' <value> The color of the button. </value>
    Public Property ButtonColor() As Color

    ''' <summary> Gets or sets the button color hovered. </summary>
    ''' <value> The button color hovered. </value>
    Public Property ButtonColorHovered() As Color

    ''' <summary> Gets or sets the button color pressed. </summary>
    ''' <value> The button color pressed. </value>
    Public Property ButtonColorPressed() As Color

#End Region ' Public Properties

#Region "Render Method Implementations"

    ''' <summary> Renders the border. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="g">               The Graphics to process. </param>
    ''' <param name="borderRectangle"> The border rectangle. </param>
    Public Overrides Sub RenderBorder(ByVal g As Graphics, ByVal borderRectangle As Rectangle)
        If g Is Nothing Then Return
        Dim renderColor As Color = If((Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled, Me.BorderColor.ToGrayscale(), Me.BorderColor)

        g.SetClip(borderRectangle)

        Using borderPen As New Pen(renderColor)
            g.DrawRectangle(borderPen, borderRectangle.X, borderRectangle.Y, borderRectangle.Width - 1, borderRectangle.Height - 1)
        End Using

        g.ResetClip()
    End Sub

    ''' <summary> Renders the left toggle field. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="g">                     The Graphics to process. </param>
    ''' <param name="leftRectangle">         The left rectangle. </param>
    ''' <param name="totalToggleFieldWidth"> Width of the total toggle field. </param>
    Public Overrides Sub RenderLeftToggleField(ByVal g As Graphics, ByVal leftRectangle As Rectangle, ByVal totalToggleFieldWidth As Integer)
        If g Is Nothing Then Return
        Dim adjustedLeftRect As New Rectangle(leftRectangle.X + 2, 2, leftRectangle.Width - 2, leftRectangle.Height - 4)

        If adjustedLeftRect.Width > 0 Then
            Dim leftColor As Color = Me.LeftSideColor

            If Me.ToggleSwitch.IsLeftSidePressed Then
                leftColor = Me.LeftSideColorPressed
            ElseIf Me.ToggleSwitch.IsLeftSideHovered Then
                leftColor = Me.LeftSideColorHovered
            End If

            If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
                leftColor = leftColor.ToGrayscale()
            End If

            g.SetClip(adjustedLeftRect)

            Using leftBrush As Brush = New SolidBrush(leftColor)
                g.FillRectangle(leftBrush, adjustedLeftRect)
            End Using

            If Me.ToggleSwitch.OnSideImage IsNot Nothing OrElse (Not String.IsNullOrEmpty(Me.ToggleSwitch.OnText)) Then
                Dim fullRectangle As New RectangleF(leftRectangle.X + 2 - (totalToggleFieldWidth - leftRectangle.Width), 2, totalToggleFieldWidth - 2, Me.ToggleSwitch.Height - 4)

                g.IntersectClip(fullRectangle)

                If Me.ToggleSwitch.OnSideImage IsNot Nothing Then
                    Dim imageSize As Size = Me.ToggleSwitch.OnSideImage.Size
                    Dim imageRectangle As Rectangle

                    Dim imageXPos As Integer = CInt(Fix(fullRectangle.X))

                    If Me.ToggleSwitch.OnSideScaleImageToFit Then
                        Dim canvasSize As New Size(CInt(fullRectangle.Width), CInt(fullRectangle.Height))
                        Dim resizedImageSize As Size = ImageHelper.RescaleImageToFit(imageSize, canvasSize)

                        If Me.ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Center Then
                            imageXPos = CInt(Fix(CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(resizedImageSize.Width)) / 2)))
                        ElseIf Me.ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Near Then
                            imageXPos = CInt(Fix(CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(resizedImageSize.Width)))
                        End If

                        imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(resizedImageSize.Height)) / 2))), resizedImageSize.Width, resizedImageSize.Height)

                        If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
                            g.DrawImage(Me.ToggleSwitch.OnSideImage, imageRectangle, 0, 0, Me.ToggleSwitch.OnSideImage.Width, Me.ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                        Else
                            g.DrawImage(Me.ToggleSwitch.OnSideImage, imageRectangle)
                        End If
                    Else
                        If Me.ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Center Then
                            imageXPos = CInt(Fix(CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(imageSize.Width)) / 2)))
                        ElseIf Me.ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Near Then
                            imageXPos = CInt(Fix(CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(imageSize.Width)))
                        End If

                        imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(imageSize.Height)) / 2))), imageSize.Width, imageSize.Height)

                        If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
                            g.DrawImage(Me.ToggleSwitch.OnSideImage, imageRectangle, 0, 0, Me.ToggleSwitch.OnSideImage.Width, Me.ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                        Else
                            g.DrawImageUnscaled(Me.ToggleSwitch.OnSideImage, imageRectangle)
                        End If
                    End If
                ElseIf Not String.IsNullOrEmpty(Me.ToggleSwitch.OnText) Then
                    Dim textSize As SizeF = g.MeasureString(Me.ToggleSwitch.OnText, Me.ToggleSwitch.OnFont)

                    Dim textXPos As Single = fullRectangle.X

                    If Me.ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Center Then
                        textXPos = CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(textSize.Width)) / 2)
                    ElseIf Me.ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Near Then
                        textXPos = CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(textSize.Width)
                    End If

                    Dim textRectangle As New RectangleF(textXPos, CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(textSize.Height)) / 2), textSize.Width, textSize.Height)

                    Dim textForeColor As Color = Me.ToggleSwitch.OnForeColor

                    If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
                        textForeColor = textForeColor.ToGrayscale()
                    End If

                    Using textBrush As Brush = New SolidBrush(textForeColor)
                        g.DrawString(Me.ToggleSwitch.OnText, Me.ToggleSwitch.OnFont, textBrush, textRectangle)
                    End Using
                End If
            End If

            g.ResetClip()
        End If
    End Sub

    ''' <summary> Renders the right toggle field. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="g">                     The Graphics to process. </param>
    ''' <param name="rightRectangle">        The right rectangle. </param>
    ''' <param name="totalToggleFieldWidth"> Width of the total toggle field. </param>
    Public Overrides Sub RenderRightToggleField(ByVal g As Graphics, ByVal rightRectangle As Rectangle, ByVal totalToggleFieldWidth As Integer)
        If g Is Nothing Then Return
        Dim adjustedRightRect As New Rectangle(rightRectangle.X, 2, rightRectangle.Width - 2, rightRectangle.Height - 4)

        If adjustedRightRect.Width > 0 Then
            Dim rightColor As Color = Me.RightSideColor

            If Me.ToggleSwitch.IsRightSidePressed Then
                rightColor = Me.RightSideColorPressed
            ElseIf Me.ToggleSwitch.IsRightSideHovered Then
                rightColor = Me.RightSideColorHovered
            End If

            If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
                rightColor = rightColor.ToGrayscale()
            End If

            g.SetClip(adjustedRightRect)

            Using rightBrush As Brush = New SolidBrush(rightColor)
                g.FillRectangle(rightBrush, adjustedRightRect)
            End Using

            If Me.ToggleSwitch.OffSideImage IsNot Nothing OrElse (Not String.IsNullOrEmpty(Me.ToggleSwitch.OffText)) Then
                Dim fullRectangle As New RectangleF(rightRectangle.X, 2, totalToggleFieldWidth - 2, Me.ToggleSwitch.Height - 4)

                g.IntersectClip(fullRectangle)

                If Me.ToggleSwitch.OffSideImage IsNot Nothing Then
                    Dim imageSize As Size = Me.ToggleSwitch.OffSideImage.Size
                    Dim imageRectangle As Rectangle

                    Dim imageXPos As Integer = CInt(Fix(fullRectangle.X))

                    If Me.ToggleSwitch.OffSideScaleImageToFit Then
                        Dim canvasSize As New Size(CInt(fullRectangle.Width), CInt(fullRectangle.Height))
                        Dim resizedImageSize As Size = ImageHelper.RescaleImageToFit(imageSize, canvasSize)

                        If Me.ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Center Then
                            imageXPos = CInt(Fix(CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(resizedImageSize.Width)) / 2)))
                        ElseIf Me.ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Far Then
                            imageXPos = CInt(Fix(CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(resizedImageSize.Width)))
                        End If

                        imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(resizedImageSize.Height)) / 2))), resizedImageSize.Width, resizedImageSize.Height)

                        If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
                            g.DrawImage(Me.ToggleSwitch.OnSideImage, imageRectangle, 0, 0, Me.ToggleSwitch.OnSideImage.Width, Me.ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                        Else
                            g.DrawImage(Me.ToggleSwitch.OnSideImage, imageRectangle)
                        End If
                    Else
                        If Me.ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Center Then
                            imageXPos = CInt(Fix(CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(imageSize.Width)) / 2)))
                        ElseIf Me.ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Far Then
                            imageXPos = CInt(Fix(CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(imageSize.Width)))
                        End If

                        imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(imageSize.Height)) / 2))), imageSize.Width, imageSize.Height)

                        If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
                            g.DrawImage(Me.ToggleSwitch.OnSideImage, imageRectangle, 0, 0, Me.ToggleSwitch.OnSideImage.Width, Me.ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                        Else
                            g.DrawImageUnscaled(Me.ToggleSwitch.OffSideImage, imageRectangle)
                        End If
                    End If
                ElseIf Not String.IsNullOrEmpty(Me.ToggleSwitch.OffText) Then
                    Dim textSize As SizeF = g.MeasureString(Me.ToggleSwitch.OffText, Me.ToggleSwitch.OffFont)

                    Dim textXPos As Single = fullRectangle.X

                    If Me.ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Center Then
                        textXPos = CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(textSize.Width)) / 2)
                    ElseIf Me.ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Far Then
                        textXPos = CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(textSize.Width)
                    End If

                    Dim textRectangle As New RectangleF(textXPos, CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(textSize.Height)) / 2), textSize.Width, textSize.Height)

                    Dim textForeColor As Color = Me.ToggleSwitch.OffForeColor

                    If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
                        textForeColor = textForeColor.ToGrayscale()
                    End If

                    Using textBrush As Brush = New SolidBrush(textForeColor)
                        g.DrawString(Me.ToggleSwitch.OffText, Me.ToggleSwitch.OffFont, textBrush, textRectangle)
                    End Using
                End If
            End If
        End If
    End Sub

    ''' <summary> Renders the button. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="g">               The Graphics to process. </param>
    ''' <param name="buttonRectangle"> The button rectangle. </param>
    Public Overrides Sub RenderButton(ByVal g As Graphics, ByVal buttonRectangle As Rectangle)
        If g Is Nothing Then Return
        Dim renderColor As Color = Me.ButtonColor

        If Me.ToggleSwitch.IsButtonPressed Then
            renderColor = Me.ButtonColorPressed
        ElseIf Me.ToggleSwitch.IsButtonHovered Then
            renderColor = Me.ButtonColorHovered
        End If

        If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
            renderColor = renderColor.ToGrayscale()
        End If

        g.SetClip(buttonRectangle)

        Using backBrush As Brush = New SolidBrush(renderColor)
            g.FillRectangle(backBrush, buttonRectangle)
        End Using

        g.ResetClip()
    End Sub

#End Region ' Render Method Implementations

#Region "Helper Method Implementations"

    ''' <summary> Gets button width. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> The button width. </returns>
    Public Overrides Function GetButtonWidth() As Integer
        Return CInt(CDbl(Me.ToggleSwitch.Height) / 3 * 2)
    End Function

    ''' <summary> Gets button rectangle. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> The button rectangle. </returns>
    Public Overrides Function GetButtonRectangle() As Rectangle
        Dim buttonWidth As Integer = Me.GetButtonWidth()
        Return Me.GetButtonRectangle(buttonWidth)
    End Function

    ''' <summary> Gets button rectangle. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="buttonWidth"> Width of the button. </param>
    ''' <returns> The button rectangle. </returns>
    Public Overrides Function GetButtonRectangle(ByVal buttonWidth As Integer) As Rectangle
        Dim buttonRect As New Rectangle(Me.ToggleSwitch.ButtonValue, 0, buttonWidth, Me.ToggleSwitch.Height)
        Return buttonRect
    End Function

#End Region ' Helper Method Implementations
End Class

