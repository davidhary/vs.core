Imports System.Drawing.Drawing2D

Imports isr.Core.ColorExtensions

''' <summary> A toggle switch ios 5 renderer. </summary>
''' <remarks>
''' (c) 2015 Johnny J.. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 12/4/2015 </para>
''' </remarks>
Public Class ToggleSwitchIos5Renderer
    Inherits ToggleSwitchRendererBase

#Region "Constructor"

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        Me.BorderColor = Color.FromArgb(255, 202, 202, 202)
        Me.LeftSideUpperColor1 = Color.FromArgb(255, 48, 115, 189)
        Me.LeftSideUpperColor2 = Color.FromArgb(255, 17, 123, 220)
        Me.LeftSideLowerColor1 = Color.FromArgb(255, 65, 143, 218)
        Me.LeftSideLowerColor2 = Color.FromArgb(255, 130, 190, 243)
        Me.LeftSideUpperBorderColor = Color.FromArgb(150, 123, 157, 196)
        Me.LeftSideLowerBorderColor = Color.FromArgb(150, 174, 208, 241)
        Me.RightSideUpperColor1 = Color.FromArgb(255, 191, 191, 191)
        Me.RightSideUpperColor2 = Color.FromArgb(255, 229, 229, 229)
        Me.RightSideLowerColor1 = Color.FromArgb(255, 232, 232, 232)
        Me.RightSideLowerColor2 = Color.FromArgb(255, 251, 251, 251)
        Me.RightSideUpperBorderColor = Color.FromArgb(150, 175, 175, 175)
        Me.RightSideLowerBorderColor = Color.FromArgb(150, 229, 230, 233)
        Me.ButtonShadowColor = Color.Transparent
        Me.ButtonNormalOuterBorderColor = Color.FromArgb(255, 149, 172, 194)
        Me.ButtonNormalInnerBorderColor = Color.FromArgb(255, 235, 235, 235)
        Me.ButtonNormalSurfaceColor1 = Color.FromArgb(255, 216, 215, 216)
        Me.ButtonNormalSurfaceColor2 = Color.FromArgb(255, 251, 250, 251)
        Me.ButtonHoverOuterBorderColor = Color.FromArgb(255, 141, 163, 184)
        Me.ButtonHoverInnerBorderColor = Color.FromArgb(255, 223, 223, 223)
        Me.ButtonHoverSurfaceColor1 = Color.FromArgb(255, 205, 204, 205)
        Me.ButtonHoverSurfaceColor2 = Color.FromArgb(255, 239, 238, 239)
        Me.ButtonPressedOuterBorderColor = Color.FromArgb(255, 111, 128, 145)
        Me.ButtonPressedInnerBorderColor = Color.FromArgb(255, 176, 176, 176)
        Me.ButtonPressedSurfaceColor1 = Color.FromArgb(255, 162, 161, 162)
        Me.ButtonPressedSurfaceColor2 = Color.FromArgb(255, 187, 187, 187)
    End Sub

#End Region ' Constructor

#Region "Public Properties"

    ''' <summary> Gets or sets the color of the border. </summary>
    ''' <value> The color of the border. </value>
    Public Property BorderColor() As Color

    ''' <summary> Gets or sets the left side upper color 1. </summary>
    ''' <value> The left side upper color 1. </value>
    Public Property LeftSideUpperColor1() As Color

    ''' <summary> Gets or sets the left side upper color 2. </summary>
    ''' <value> The left side upper color 2. </value>
    Public Property LeftSideUpperColor2() As Color

    ''' <summary> Gets or sets the left side lower color 1. </summary>
    ''' <value> The left side lower color 1. </value>
    Public Property LeftSideLowerColor1() As Color

    ''' <summary> Gets or sets the left side lower color 2. </summary>
    ''' <value> The left side lower color 2. </value>
    Public Property LeftSideLowerColor2() As Color

    ''' <summary> Gets or sets the color of the left side upper border. </summary>
    ''' <value> The color of the left side upper border. </value>
    Public Property LeftSideUpperBorderColor() As Color

    ''' <summary> Gets or sets the color of the left side lower border. </summary>
    ''' <value> The color of the left side lower border. </value>
    Public Property LeftSideLowerBorderColor() As Color

    ''' <summary> Gets or sets the right side upper color 1. </summary>
    ''' <value> The right side upper color 1. </value>
    Public Property RightSideUpperColor1() As Color

    ''' <summary> Gets or sets the right side upper color 2. </summary>
    ''' <value> The right side upper color 2. </value>
    Public Property RightSideUpperColor2() As Color

    ''' <summary> Gets or sets the right side lower color 1. </summary>
    ''' <value> The right side lower color 1. </value>
    Public Property RightSideLowerColor1() As Color

    ''' <summary> Gets or sets the right side lower color 2. </summary>
    ''' <value> The right side lower color 2. </value>
    Public Property RightSideLowerColor2() As Color

    ''' <summary> Gets or sets the color of the right side upper border. </summary>
    ''' <value> The color of the right side upper border. </value>
    Public Property RightSideUpperBorderColor() As Color

    ''' <summary> Gets or sets the color of the right side lower border. </summary>
    ''' <value> The color of the right side lower border. </value>
    Public Property RightSideLowerBorderColor() As Color

    ''' <summary> Gets or sets the color of the button shadow. </summary>
    ''' <value> The color of the button shadow. </value>
    Public Property ButtonShadowColor() As Color

    ''' <summary> Gets or sets the color of the button normal outer border. </summary>
    ''' <value> The color of the button normal outer border. </value>
    Public Property ButtonNormalOuterBorderColor() As Color

    ''' <summary> Gets or sets the color of the button normal inner border. </summary>
    ''' <value> The color of the button normal inner border. </value>
    Public Property ButtonNormalInnerBorderColor() As Color

    ''' <summary> Gets or sets the button normal surface color 1. </summary>
    ''' <value> The button normal surface color 1. </value>
    Public Property ButtonNormalSurfaceColor1() As Color

    ''' <summary> Gets or sets the button normal surface color 2. </summary>
    ''' <value> The button normal surface color 2. </value>
    Public Property ButtonNormalSurfaceColor2() As Color

    ''' <summary> Gets or sets the color of the button hover outer border. </summary>
    ''' <value> The color of the button hover outer border. </value>
    Public Property ButtonHoverOuterBorderColor() As Color

    ''' <summary> Gets or sets the color of the button hover inner border. </summary>
    ''' <value> The color of the button hover inner border. </value>
    Public Property ButtonHoverInnerBorderColor() As Color

    ''' <summary> Gets or sets the button hover surface color 1. </summary>
    ''' <value> The button hover surface color 1. </value>
    Public Property ButtonHoverSurfaceColor1() As Color

    ''' <summary> Gets or sets the button hover surface color 2. </summary>
    ''' <value> The button hover surface color 2. </value>
    Public Property ButtonHoverSurfaceColor2() As Color

    ''' <summary> Gets or sets the color of the button pressed outer border. </summary>
    ''' <value> The color of the button pressed outer border. </value>
    Public Property ButtonPressedOuterBorderColor() As Color

    ''' <summary> Gets or sets the color of the button pressed inner border. </summary>
    ''' <value> The color of the button pressed inner border. </value>
    Public Property ButtonPressedInnerBorderColor() As Color

    ''' <summary> Gets or sets the button pressed surface color 1. </summary>
    ''' <value> The button pressed surface color 1. </value>
    Public Property ButtonPressedSurfaceColor1() As Color

    ''' <summary> Gets or sets the button pressed surface color 2. </summary>
    ''' <value> The button pressed surface color 2. </value>
    Public Property ButtonPressedSurfaceColor2() As Color

#End Region ' Public Properties

#Region "Render Method Implementations"

    ''' <summary> Renders the border. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="g">               The Graphics to process. </param>
    ''' <param name="borderRectangle"> The border rectangle. </param>
    Public Overrides Sub RenderBorder(ByVal g As Graphics, ByVal borderRectangle As Rectangle)
        'Draw this one AFTER the button is drawn in the RenderButton method
    End Sub

    ''' <summary> Renders the left toggle field. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="g">                     The Graphics to process. </param>
    ''' <param name="leftRectangle">         The left rectangle. </param>
    ''' <param name="totalToggleFieldWidth"> Width of the total toggle field. </param>
    Public Overrides Sub RenderLeftToggleField(ByVal g As Graphics, ByVal leftRectangle As Rectangle, ByVal totalToggleFieldWidth As Integer)
        If g Is Nothing Then Return
        g.SmoothingMode = SmoothingMode.HighQuality
        g.PixelOffsetMode = PixelOffsetMode.HighQuality

        Dim buttonWidth As Integer = Me.GetButtonWidth()

        'Draw upper gradient field
        Dim gradientRectWidth As Integer = leftRectangle.Width + buttonWidth \ 2
        Dim upperGradientRectHeight As Integer = CInt(Fix(CDbl(0.8) * CDbl(leftRectangle.Height - 2)))

        Dim controlRectangle As New Rectangle(0, 0, Me.ToggleSwitch.Width, Me.ToggleSwitch.Height)
        Dim controlClipPath As GraphicsPath = GetControlClipPath(controlRectangle)

        Dim upperGradientRectangle As New Rectangle(leftRectangle.X, leftRectangle.Y + 1, gradientRectWidth, upperGradientRectHeight - 1)

        g.SetClip(controlClipPath)
        g.IntersectClip(upperGradientRectangle)

        Using upperGradientPath As New GraphicsPath()
            upperGradientPath.AddArc(upperGradientRectangle.X, upperGradientRectangle.Y, Me.ToggleSwitch.Height, Me.ToggleSwitch.Height, 135, 135)
            upperGradientPath.AddLine(upperGradientRectangle.X, upperGradientRectangle.Y, upperGradientRectangle.X + upperGradientRectangle.Width, upperGradientRectangle.Y)
            upperGradientPath.AddLine(upperGradientRectangle.X + upperGradientRectangle.Width, upperGradientRectangle.Y, upperGradientRectangle.X + upperGradientRectangle.Width, upperGradientRectangle.Y + upperGradientRectangle.Height)
            upperGradientPath.AddLine(upperGradientRectangle.X, upperGradientRectangle.Y + upperGradientRectangle.Height, upperGradientRectangle.X + upperGradientRectangle.Width, upperGradientRectangle.Y + upperGradientRectangle.Height)

            Dim upperColor1 As Color = If((Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled, Me.LeftSideUpperColor1.ToGrayscale(), Me.LeftSideUpperColor1)
            Dim upperColor2 As Color = If((Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled, Me.LeftSideUpperColor2.ToGrayscale(), Me.LeftSideUpperColor2)

            Using upperGradientBrush As Brush = New LinearGradientBrush(upperGradientRectangle, upperColor1, upperColor2, LinearGradientMode.Vertical)
                g.FillPath(upperGradientBrush, upperGradientPath)
            End Using
        End Using

        g.ResetClip()

        'Draw lower gradient field
        Dim lowerGradientRectHeight As Integer = CInt(Fix(Math.Ceiling(CDbl(0.5) * CDbl(leftRectangle.Height - 2))))

        Dim lowerGradientRectangle As New Rectangle(leftRectangle.X, leftRectangle.Y + (leftRectangle.Height \ 2), gradientRectWidth, lowerGradientRectHeight)

        g.SetClip(controlClipPath)
        g.IntersectClip(lowerGradientRectangle)

        Using lowerGradientPath As New GraphicsPath()
            lowerGradientPath.AddArc(1, lowerGradientRectangle.Y, CInt(Fix(0.75 * (Me.ToggleSwitch.Height - 1))), Me.ToggleSwitch.Height - 1, 215, 45) 'Arc from side to top
            lowerGradientPath.AddLine(lowerGradientRectangle.X + buttonWidth \ 2, lowerGradientRectangle.Y, lowerGradientRectangle.X + lowerGradientRectangle.Width, lowerGradientRectangle.Y)
            lowerGradientPath.AddLine(lowerGradientRectangle.X + lowerGradientRectangle.Width, lowerGradientRectangle.Y, lowerGradientRectangle.X + lowerGradientRectangle.Width, lowerGradientRectangle.Y + lowerGradientRectangle.Height)
            lowerGradientPath.AddLine(lowerGradientRectangle.X + buttonWidth \ 4, lowerGradientRectangle.Y + lowerGradientRectangle.Height, lowerGradientRectangle.X + lowerGradientRectangle.Width, lowerGradientRectangle.Y + lowerGradientRectangle.Height)
            lowerGradientPath.AddArc(1, 1, Me.ToggleSwitch.Height - 1, Me.ToggleSwitch.Height - 1, 90, 70) 'Arc from side to bottom

            Dim lowerColor1 As Color = If((Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled, Me.LeftSideLowerColor1.ToGrayscale(), Me.LeftSideLowerColor1)
            Dim lowerColor2 As Color = If((Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled, Me.LeftSideLowerColor2.ToGrayscale(), Me.LeftSideLowerColor2)

            Using lowerGradientBrush As Brush = New LinearGradientBrush(lowerGradientRectangle, lowerColor1, lowerColor2, LinearGradientMode.Vertical)
                g.FillPath(lowerGradientBrush, lowerGradientPath)
            End Using
        End Using

        g.ResetClip()

        controlRectangle = New Rectangle(0, 0, Me.ToggleSwitch.Width, Me.ToggleSwitch.Height)
        controlClipPath = GetControlClipPath(controlRectangle)

        g.SetClip(controlClipPath)

        'Draw upper inside border
        Dim upperBordercolor As Color = Me.LeftSideUpperBorderColor

        If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
            upperBordercolor = upperBordercolor.ToGrayscale()
        End If

        Using upperBorderPen As New Pen(upperBordercolor)
            g.DrawLine(upperBorderPen, leftRectangle.X, leftRectangle.Y + 1, leftRectangle.X + leftRectangle.Width + (buttonWidth \ 2), leftRectangle.Y + 1)
        End Using

        'Draw lower inside border
        Dim lowerBordercolor As Color = Me.LeftSideLowerBorderColor

        If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
            lowerBordercolor = lowerBordercolor.ToGrayscale()
        End If

        Using lowerBorderPen As New Pen(lowerBordercolor)
            g.DrawLine(lowerBorderPen, leftRectangle.X, leftRectangle.Y + leftRectangle.Height - 1, leftRectangle.X + leftRectangle.Width + (buttonWidth \ 2), leftRectangle.Y + leftRectangle.Height - 1)
        End Using

        'Draw image or text
        If Me.ToggleSwitch.OnSideImage IsNot Nothing OrElse (Not String.IsNullOrEmpty(Me.ToggleSwitch.OnText)) Then
            Dim fullRectangle As New RectangleF(leftRectangle.X + 2 - (totalToggleFieldWidth - leftRectangle.Width), 2, totalToggleFieldWidth - 2, Me.ToggleSwitch.Height - 4)

            g.IntersectClip(fullRectangle)

            If Me.ToggleSwitch.OnSideImage IsNot Nothing Then
                Dim imageSize As Size = Me.ToggleSwitch.OnSideImage.Size
                Dim imageRectangle As Rectangle

                Dim imageXPos As Integer = CInt(Fix(fullRectangle.X))

                If Me.ToggleSwitch.OnSideScaleImageToFit Then
                    Dim canvasSize As New Size(CInt(fullRectangle.Width), CInt(fullRectangle.Height))
                    Dim resizedImageSize As Size = ImageHelper.RescaleImageToFit(imageSize, canvasSize)

                    If Me.ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Center Then
                        imageXPos = CInt(Fix(CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(resizedImageSize.Width)) / 2)))
                    ElseIf Me.ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Near Then
                        imageXPos = CInt(Fix(CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(resizedImageSize.Width)))
                    End If

                    imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(resizedImageSize.Height)) / 2))), resizedImageSize.Width, resizedImageSize.Height)

                    If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
                        g.DrawImage(Me.ToggleSwitch.OnSideImage, imageRectangle, 0, 0, Me.ToggleSwitch.OnSideImage.Width, Me.ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                    Else
                        g.DrawImage(Me.ToggleSwitch.OnSideImage, imageRectangle)
                    End If
                Else
                    If Me.ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Center Then
                        imageXPos = CInt(Fix(CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(imageSize.Width)) / 2)))
                    ElseIf Me.ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Near Then
                        imageXPos = CInt(Fix(CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(imageSize.Width)))
                    End If

                    imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(imageSize.Height)) / 2))), imageSize.Width, imageSize.Height)

                    If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
                        g.DrawImage(Me.ToggleSwitch.OnSideImage, imageRectangle, 0, 0, Me.ToggleSwitch.OnSideImage.Width, Me.ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                    Else
                        g.DrawImageUnscaled(Me.ToggleSwitch.OnSideImage, imageRectangle)
                    End If
                End If
            ElseIf Not String.IsNullOrEmpty(Me.ToggleSwitch.OnText) Then
                Dim textSize As SizeF = g.MeasureString(Me.ToggleSwitch.OnText, Me.ToggleSwitch.OnFont)

                Dim textXPos As Single = fullRectangle.X

                If Me.ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Center Then
                    textXPos = CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(textSize.Width)) / 2)
                ElseIf Me.ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Near Then
                    textXPos = CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(textSize.Width)
                End If

                Dim textRectangle As New RectangleF(textXPos, CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(textSize.Height)) / 2), textSize.Width, textSize.Height)

                Dim textForeColor As Color = Me.ToggleSwitch.OnForeColor

                If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
                    textForeColor = textForeColor.ToGrayscale()
                End If

                Using textBrush As Brush = New SolidBrush(textForeColor)
                    g.DrawString(Me.ToggleSwitch.OnText, Me.ToggleSwitch.OnFont, textBrush, textRectangle)
                End Using
            End If
        End If

        g.ResetClip()
    End Sub

    ''' <summary> Renders the right toggle field. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="g">                     The Graphics to process. </param>
    ''' <param name="rightRectangle">        The right rectangle. </param>
    ''' <param name="totalToggleFieldWidth"> Width of the total toggle field. </param>
    Public Overrides Sub RenderRightToggleField(ByVal g As Graphics, ByVal rightRectangle As Rectangle, ByVal totalToggleFieldWidth As Integer)
        If g Is Nothing Then Return
        g.SmoothingMode = SmoothingMode.HighQuality
        g.PixelOffsetMode = PixelOffsetMode.HighQuality

        Dim buttonRectangle As Rectangle = Me.GetButtonRectangle()

        Dim controlRectangle As New Rectangle(0, 0, Me.ToggleSwitch.Width, Me.ToggleSwitch.Height)
        Dim controlClipPath As GraphicsPath = GetControlClipPath(controlRectangle)

        'Draw upper gradient field
        Dim gradientRectWidth As Integer = rightRectangle.Width + buttonRectangle.Width \ 2
        Dim upperGradientRectHeight As Integer = CInt(Fix(CDbl(0.8) * CDbl(rightRectangle.Height - 2)))

        Dim upperGradientRectangle As New Rectangle(rightRectangle.X - buttonRectangle.Width \ 2, rightRectangle.Y + 1, gradientRectWidth - 1, upperGradientRectHeight - 1)

        g.SetClip(controlClipPath)
        g.IntersectClip(upperGradientRectangle)

        Using upperGradientPath As New GraphicsPath()
            upperGradientPath.AddLine(upperGradientRectangle.X, upperGradientRectangle.Y, upperGradientRectangle.X + upperGradientRectangle.Width, upperGradientRectangle.Y)
            upperGradientPath.AddArc(upperGradientRectangle.X + upperGradientRectangle.Width - Me.ToggleSwitch.Height + 1, upperGradientRectangle.Y - 1, Me.ToggleSwitch.Height, Me.ToggleSwitch.Height, 270, 115)
            upperGradientPath.AddLine(upperGradientRectangle.X + upperGradientRectangle.Width, upperGradientRectangle.Y + upperGradientRectangle.Height, upperGradientRectangle.X, upperGradientRectangle.Y + upperGradientRectangle.Height)
            upperGradientPath.AddLine(upperGradientRectangle.X, upperGradientRectangle.Y + upperGradientRectangle.Height, upperGradientRectangle.X, upperGradientRectangle.Y)

            Dim upperColor1 As Color = If((Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled, Me.RightSideUpperColor1.ToGrayscale(), Me.RightSideUpperColor1)
            Dim upperColor2 As Color = If((Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled, Me.RightSideUpperColor2.ToGrayscale(), Me.RightSideUpperColor2)

            Using upperGradientBrush As Brush = New LinearGradientBrush(upperGradientRectangle, upperColor1, upperColor2, LinearGradientMode.Vertical)
                g.FillPath(upperGradientBrush, upperGradientPath)
            End Using
        End Using

        g.ResetClip()

        'Draw lower gradient field
        Dim lowerGradientRectHeight As Integer = CInt(Fix(Math.Ceiling(CDbl(0.5) * CDbl(rightRectangle.Height - 2))))

        Dim lowerGradientRectangle As New Rectangle(rightRectangle.X - buttonRectangle.Width \ 2, rightRectangle.Y + (rightRectangle.Height \ 2), gradientRectWidth - 1, lowerGradientRectHeight)

        g.SetClip(controlClipPath)
        g.IntersectClip(lowerGradientRectangle)

        Using lowerGradientPath As New GraphicsPath()
            lowerGradientPath.AddLine(lowerGradientRectangle.X, lowerGradientRectangle.Y, lowerGradientRectangle.X + lowerGradientRectangle.Width, lowerGradientRectangle.Y)
            lowerGradientPath.AddArc(lowerGradientRectangle.X + lowerGradientRectangle.Width - CInt(Fix(0.75 * (Me.ToggleSwitch.Height - 1))), lowerGradientRectangle.Y, CInt(Fix(0.75 * (Me.ToggleSwitch.Height - 1))), Me.ToggleSwitch.Height - 1, 270, 45) 'Arc from top to side
            lowerGradientPath.AddArc(Me.ToggleSwitch.Width - Me.ToggleSwitch.Height, 0, Me.ToggleSwitch.Height, Me.ToggleSwitch.Height, 20, 70) 'Arc from side to bottom
            lowerGradientPath.AddLine(lowerGradientRectangle.X + lowerGradientRectangle.Width, lowerGradientRectangle.Y + lowerGradientRectangle.Height, lowerGradientRectangle.X, lowerGradientRectangle.Y + lowerGradientRectangle.Height)
            lowerGradientPath.AddLine(lowerGradientRectangle.X, lowerGradientRectangle.Y + lowerGradientRectangle.Height, lowerGradientRectangle.X, lowerGradientRectangle.Y)

            Dim lowerColor1 As Color = If((Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled, Me.RightSideLowerColor1.ToGrayscale(), Me.RightSideLowerColor1)
            Dim lowerColor2 As Color = If((Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled, Me.RightSideLowerColor2.ToGrayscale(), Me.RightSideLowerColor2)

            Using lowerGradientBrush As Brush = New LinearGradientBrush(lowerGradientRectangle, lowerColor1, lowerColor2, LinearGradientMode.Vertical)
                g.FillPath(lowerGradientBrush, lowerGradientPath)
            End Using
        End Using

        g.ResetClip()

        controlRectangle = New Rectangle(0, 0, Me.ToggleSwitch.Width, Me.ToggleSwitch.Height)
        controlClipPath = GetControlClipPath(controlRectangle)

        g.SetClip(controlClipPath)

        'Draw upper inside border
        Dim upperBordercolor As Color = Me.RightSideUpperBorderColor

        If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
            upperBordercolor = upperBordercolor.ToGrayscale()
        End If

        Using upperBorderPen As New Pen(upperBordercolor)
            g.DrawLine(upperBorderPen, rightRectangle.X - (buttonRectangle.Width \ 2), rightRectangle.Y + 1, rightRectangle.X + rightRectangle.Width, rightRectangle.Y + 1)
        End Using

        'Draw lower inside border
        Dim lowerBordercolor As Color = Me.RightSideLowerBorderColor

        If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
            lowerBordercolor = lowerBordercolor.ToGrayscale()
        End If

        Using lowerBorderPen As New Pen(lowerBordercolor)
            g.DrawLine(lowerBorderPen, rightRectangle.X - (buttonRectangle.Width \ 2), rightRectangle.Y + rightRectangle.Height - 1, rightRectangle.X + rightRectangle.Width, rightRectangle.Y + rightRectangle.Height - 1)
        End Using

        'Draw image or text
        If Me.ToggleSwitch.OffSideImage IsNot Nothing OrElse (Not String.IsNullOrEmpty(Me.ToggleSwitch.OffText)) Then
            Dim fullRectangle As New RectangleF(rightRectangle.X, 2, totalToggleFieldWidth - 2, Me.ToggleSwitch.Height - 4)

            g.IntersectClip(fullRectangle)

            If Me.ToggleSwitch.OffSideImage IsNot Nothing Then
                Dim imageSize As Size = Me.ToggleSwitch.OffSideImage.Size
                Dim imageRectangle As Rectangle

                Dim imageXPos As Integer = CInt(Fix(fullRectangle.X))

                If Me.ToggleSwitch.OffSideScaleImageToFit Then
                    Dim canvasSize As New Size(CInt(fullRectangle.Width), CInt(fullRectangle.Height))
                    Dim resizedImageSize As Size = ImageHelper.RescaleImageToFit(imageSize, canvasSize)

                    If Me.ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Center Then
                        imageXPos = CInt(Fix(CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(resizedImageSize.Width)) / 2)))
                    ElseIf Me.ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Far Then
                        imageXPos = CInt(Fix(CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(resizedImageSize.Width)))
                    End If

                    imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(resizedImageSize.Height)) / 2))), resizedImageSize.Width, resizedImageSize.Height)

                    If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
                        g.DrawImage(Me.ToggleSwitch.OnSideImage, imageRectangle, 0, 0, Me.ToggleSwitch.OnSideImage.Width, Me.ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                    Else
                        g.DrawImage(Me.ToggleSwitch.OnSideImage, imageRectangle)
                    End If
                Else
                    If Me.ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Center Then
                        imageXPos = CInt(Fix(CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(imageSize.Width)) / 2)))
                    ElseIf Me.ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Far Then
                        imageXPos = CInt(Fix(CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(imageSize.Width)))
                    End If

                    imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(imageSize.Height)) / 2))), imageSize.Width, imageSize.Height)

                    If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
                        g.DrawImage(Me.ToggleSwitch.OnSideImage, imageRectangle, 0, 0, Me.ToggleSwitch.OnSideImage.Width, Me.ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                    Else
                        g.DrawImageUnscaled(Me.ToggleSwitch.OffSideImage, imageRectangle)
                    End If
                End If
            ElseIf Not String.IsNullOrEmpty(Me.ToggleSwitch.OffText) Then
                Dim textSize As SizeF = g.MeasureString(Me.ToggleSwitch.OffText, Me.ToggleSwitch.OffFont)

                Dim textXPos As Single = fullRectangle.X

                If Me.ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Center Then
                    textXPos = CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(textSize.Width)) / 2)
                ElseIf Me.ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Far Then
                    textXPos = CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(textSize.Width)
                End If

                Dim textRectangle As New RectangleF(textXPos, CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(textSize.Height)) / 2), textSize.Width, textSize.Height)

                Dim textForeColor As Color = Me.ToggleSwitch.OffForeColor

                If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
                    textForeColor = textForeColor.ToGrayscale()
                End If

                Using textBrush As Brush = New SolidBrush(textForeColor)
                    g.DrawString(Me.ToggleSwitch.OffText, Me.ToggleSwitch.OffFont, textBrush, textRectangle)
                End Using
            End If
        End If

        g.ResetClip()
    End Sub

    ''' <summary> Renders the button. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="g">               The Graphics to process. </param>
    ''' <param name="buttonRectangle"> The button rectangle. </param>
    Public Overrides Sub RenderButton(ByVal g As Graphics, ByVal buttonRectangle As Rectangle)
        If g Is Nothing Then Return
        If Me.ToggleSwitch.IsButtonOnLeftSide Then
            buttonRectangle.X += 1
        ElseIf Me.ToggleSwitch.IsButtonOnRightSide Then
            buttonRectangle.X -= 1
        End If

        g.SmoothingMode = SmoothingMode.HighQuality
        g.PixelOffsetMode = PixelOffsetMode.HighQuality

        'Draw button shadow
        buttonRectangle.Inflate(1, 1)

        Dim shadowClipRectangle As New Rectangle(buttonRectangle.Location, buttonRectangle.Size)
        shadowClipRectangle.Inflate(0, -1)

        If Me.ToggleSwitch.IsButtonOnLeftSide Then
            shadowClipRectangle.X += shadowClipRectangle.Width \ 2
            shadowClipRectangle.Width \= 2
        ElseIf Me.ToggleSwitch.IsButtonOnRightSide Then
            shadowClipRectangle.Width \= 2
        End If

        g.SetClip(shadowClipRectangle)

        'INSTANT VB NOTE: The variable buttonShadowColor was renamed since Visual Basic does not handle local variables named the same as class members well:
        Dim buttonShadowColor_Renamed As Color = If((Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled, Me.ButtonShadowColor.ToGrayscale(), Me.ButtonShadowColor)

        Using buttonShadowPen As New Pen(buttonShadowColor_Renamed)
            g.DrawEllipse(buttonShadowPen, buttonRectangle)
        End Using

        g.ResetClip()

        buttonRectangle.Inflate(-1, -1)

        'Draw outer button border
        Dim buttonOuterBorderColor As Color = Me.ButtonNormalOuterBorderColor

        If Me.ToggleSwitch.IsButtonPressed Then
            buttonOuterBorderColor = Me.ButtonPressedOuterBorderColor
        ElseIf Me.ToggleSwitch.IsButtonHovered Then
            buttonOuterBorderColor = Me.ButtonHoverOuterBorderColor
        End If

        If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
            buttonOuterBorderColor = buttonOuterBorderColor.ToGrayscale()
        End If

        Using outerBorderBrush As Brush = New SolidBrush(buttonOuterBorderColor)
            g.FillEllipse(outerBorderBrush, buttonRectangle)
        End Using

        'Draw inner button border
        buttonRectangle.Inflate(-1, -1)

        Dim buttonInnerBorderColor As Color = Me.ButtonNormalInnerBorderColor

        If Me.ToggleSwitch.IsButtonPressed Then
            buttonInnerBorderColor = Me.ButtonPressedInnerBorderColor
        ElseIf Me.ToggleSwitch.IsButtonHovered Then
            buttonInnerBorderColor = Me.ButtonHoverInnerBorderColor
        End If

        If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
            buttonInnerBorderColor = buttonInnerBorderColor.ToGrayscale()
        End If

        Using innerBorderBrush As Brush = New SolidBrush(buttonInnerBorderColor)
            g.FillEllipse(innerBorderBrush, buttonRectangle)
        End Using

        'Draw button surface
        buttonRectangle.Inflate(-1, -1)

        Dim buttonUpperSurfaceColor As Color = Me.ButtonNormalSurfaceColor1

        If Me.ToggleSwitch.IsButtonPressed Then
            buttonUpperSurfaceColor = Me.ButtonPressedSurfaceColor1
        ElseIf Me.ToggleSwitch.IsButtonHovered Then
            buttonUpperSurfaceColor = Me.ButtonHoverSurfaceColor1
        End If

        If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
            buttonUpperSurfaceColor = buttonUpperSurfaceColor.ToGrayscale()
        End If

        Dim buttonLowerSurfaceColor As Color = Me.ButtonNormalSurfaceColor2

        If Me.ToggleSwitch.IsButtonPressed Then
            buttonLowerSurfaceColor = Me.ButtonPressedSurfaceColor2
        ElseIf Me.ToggleSwitch.IsButtonHovered Then
            buttonLowerSurfaceColor = Me.ButtonHoverSurfaceColor2
        End If

        If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
            buttonLowerSurfaceColor = buttonLowerSurfaceColor.ToGrayscale()
        End If

        Using buttonSurfaceBrush As Brush = New LinearGradientBrush(buttonRectangle, buttonUpperSurfaceColor, buttonLowerSurfaceColor, LinearGradientMode.Vertical)
            g.FillEllipse(buttonSurfaceBrush, buttonRectangle)
        End Using

        g.CompositingMode = CompositingMode.SourceOver
        g.CompositingQuality = CompositingQuality.HighQuality

        'Draw outer control border
        Dim controlRectangle As New Rectangle(0, 0, Me.ToggleSwitch.Width, Me.ToggleSwitch.Height)

        Using borderPath As GraphicsPath = GetControlClipPath(controlRectangle)
            Dim controlBorderColor As Color = If((Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled, Me.BorderColor.ToGrayscale(), Me.BorderColor)

            Using borderPen As New Pen(controlBorderColor)
                g.DrawPath(borderPen, borderPath)
            End Using
        End Using

        g.ResetClip()

        'Draw button image
        Dim buttonImage As Image = If(Me.ToggleSwitch.ButtonImage, If(Me.ToggleSwitch.Checked, Me.ToggleSwitch.OnButtonImage, Me.ToggleSwitch.OffButtonImage))

        If buttonImage IsNot Nothing Then
            g.SetClip(Me.GetButtonClipPath())

            Dim alignment As ToggleSwitchButtonAlignment = If(Me.ToggleSwitch.ButtonImage IsNot Nothing, Me.ToggleSwitch.ButtonAlignment, If(Me.ToggleSwitch.Checked, Me.ToggleSwitch.OnButtonAlignment, Me.ToggleSwitch.OffButtonAlignment))

            Dim imageSize As Size = buttonImage.Size

            Dim imageRectangle As Rectangle

            Dim imageXPos As Integer = buttonRectangle.X

            Dim scaleImage As Boolean = If(Me.ToggleSwitch.ButtonImage IsNot Nothing, Me.ToggleSwitch.ButtonScaleImageToFit, If(Me.ToggleSwitch.Checked, Me.ToggleSwitch.OnButtonScaleImageToFit, Me.ToggleSwitch.OffButtonScaleImageToFit))

            If scaleImage Then
                Dim canvasSize As Size = buttonRectangle.Size
                Dim resizedImageSize As Size = ImageHelper.RescaleImageToFit(imageSize, canvasSize)

                If alignment = ToggleSwitchButtonAlignment.Center Then
                    imageXPos = CInt(Fix(CSng(buttonRectangle.X) + ((CSng(buttonRectangle.Width) - CSng(resizedImageSize.Width)) / 2)))
                ElseIf alignment = ToggleSwitchButtonAlignment.Right Then
                    imageXPos = CInt(Fix(CSng(buttonRectangle.X) + CSng(buttonRectangle.Width) - CSng(resizedImageSize.Width)))
                End If

                imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(buttonRectangle.Y) + ((CSng(buttonRectangle.Height) - CSng(resizedImageSize.Height)) / 2))), resizedImageSize.Width, resizedImageSize.Height)

                If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
                    g.DrawImage(buttonImage, imageRectangle, 0, 0, buttonImage.Width, buttonImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                Else
                    g.DrawImage(buttonImage, imageRectangle)
                End If
            Else
                If alignment = ToggleSwitchButtonAlignment.Center Then
                    imageXPos = CInt(Fix(CSng(buttonRectangle.X) + ((CSng(buttonRectangle.Width) - CSng(imageSize.Width)) / 2)))
                ElseIf alignment = ToggleSwitchButtonAlignment.Right Then
                    imageXPos = CInt(Fix(CSng(buttonRectangle.X) + CSng(buttonRectangle.Width) - CSng(imageSize.Width)))
                End If

                imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(buttonRectangle.Y) + ((CSng(buttonRectangle.Height) - CSng(imageSize.Height)) / 2))), imageSize.Width, imageSize.Height)

                If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
                    g.DrawImage(buttonImage, imageRectangle, 0, 0, buttonImage.Width, buttonImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                Else
                    g.DrawImageUnscaled(buttonImage, imageRectangle)
                End If
            End If

            g.ResetClip()
        End If
    End Sub

#End Region ' Render Method Implementations

#Region "Helper Method Implementations"

    ''' <summary> Gets control clip path. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="controlRectangle"> The control rectangle. </param>
    ''' <returns> The control clip path. </returns>
    Public Shared Function GetControlClipPath(ByVal controlRectangle As Rectangle) As GraphicsPath
        Dim borderPath As New GraphicsPath()
        borderPath.AddArc(controlRectangle.X, controlRectangle.Y, controlRectangle.Height, controlRectangle.Height, 90, 180)
        borderPath.AddArc(controlRectangle.Width - controlRectangle.Height, controlRectangle.Y, controlRectangle.Height, controlRectangle.Height, 270, 180)
        borderPath.CloseFigure()
        Return borderPath
    End Function

    ''' <summary> Gets button clip path. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> The button clip path. </returns>
    Public Function GetButtonClipPath() As GraphicsPath
        Dim buttonRectangle As Rectangle = Me.GetButtonRectangle()

        Dim buttonPath As New GraphicsPath()

        buttonPath.AddArc(buttonRectangle.X, buttonRectangle.Y, buttonRectangle.Height, buttonRectangle.Height, 0, 360)

        Return buttonPath
    End Function

    ''' <summary> Gets button width. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> The button width. </returns>
    Public Overrides Function GetButtonWidth() As Integer
        Return Me.ToggleSwitch.Height - 2
    End Function

    ''' <summary> Gets button rectangle. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> The button rectangle. </returns>
    Public Overrides Function GetButtonRectangle() As Rectangle
        Dim buttonWidth As Integer = Me.GetButtonWidth()
        Return Me.GetButtonRectangle(buttonWidth)
    End Function

    ''' <summary> Gets button rectangle. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="buttonWidth"> Width of the button. </param>
    ''' <returns> The button rectangle. </returns>
    Public Overrides Function GetButtonRectangle(ByVal buttonWidth As Integer) As Rectangle
        Dim buttonRect As New Rectangle(Me.ToggleSwitch.ButtonValue, 1, buttonWidth, buttonWidth)
        Return buttonRect
    End Function

#End Region ' Helper Method Implementations
End Class
