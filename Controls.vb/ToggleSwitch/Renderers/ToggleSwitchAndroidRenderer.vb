Imports isr.Core.ColorExtensions

''' <summary> A toggle switch android renderer. </summary>
''' <remarks>
''' (c) 2015 Johnny J.. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 12/4/2015 </para>
''' </remarks>
Public Class ToggleSwitchAndroidRenderer
    Inherits ToggleSwitchRendererBase

#Region "Constructor"

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        Me.BorderColor = Color.FromArgb(255, 166, 166, 166)
        Me.BackColor = Color.FromArgb(255, 32, 32, 32)
        Me.LeftSideColor = Color.FromArgb(255, 32, 32, 32)
        Me.RightSideColor = Color.FromArgb(255, 32, 32, 32)
        Me.OffButtonColor = Color.FromArgb(255, 70, 70, 70)
        Me.OnButtonColor = Color.FromArgb(255, 27, 161, 226)
        Me.OffButtonBorderColor = Color.FromArgb(255, 70, 70, 70)
        Me.OnButtonBorderColor = Color.FromArgb(255, 27, 161, 226)
        Me.SlantAngle = 8
    End Sub

#End Region ' Constructor

#Region "Public Properties"

    ''' <summary> Gets or sets the color of the border. </summary>
    ''' <value> The color of the border. </value>
    Public Property BorderColor() As Color

    ''' <summary> Gets or sets the color of the back. </summary>
    ''' <value> The color of the back. </value>
    Public Property BackColor() As Color

    ''' <summary> Gets or sets the color of the left side. </summary>
    ''' <value> The color of the left side. </value>
    Public Property LeftSideColor() As Color

    ''' <summary> Gets or sets the color of the right side. </summary>
    ''' <value> The color of the right side. </value>
    Public Property RightSideColor() As Color

    ''' <summary> Gets or sets the color of the off button. </summary>
    ''' <value> The color of the off button. </value>
    Public Property OffButtonColor() As Color

    ''' <summary> Gets or sets the color of the on button. </summary>
    ''' <value> The color of the on button. </value>
    Public Property OnButtonColor() As Color

    ''' <summary> Gets or sets the color of the off button border. </summary>
    ''' <value> The color of the off button border. </value>
    Public Property OffButtonBorderColor() As Color

    ''' <summary> Gets or sets the color of the on button border. </summary>
    ''' <value> The color of the on button border. </value>
    Public Property OnButtonBorderColor() As Color

    ''' <summary> Gets or sets the slant angle. </summary>
    ''' <value> The slant angle. </value>
    Public Property SlantAngle() As Integer

#End Region ' Public Properties

#Region "Render Method Implementations"

    ''' <summary> Renders the border. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="g">               The Graphics to process. </param>
    ''' <param name="borderRectangle"> The border rectangle. </param>
    Public Overrides Sub RenderBorder(ByVal g As Graphics, ByVal borderRectangle As Rectangle)
        If g Is Nothing Then Return
        Dim renderColor As Color = If((Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled, Me.BorderColor.ToGrayscale(), Me.BorderColor)
        g.SetClip(borderRectangle)
        Using borderPen As New Pen(renderColor)
            g.DrawRectangle(borderPen, borderRectangle.X, borderRectangle.Y, borderRectangle.Width - 1, borderRectangle.Height - 1)
        End Using
    End Sub

    ''' <summary> Renders the left toggle field. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="g">                     The Graphics to process. </param>
    ''' <param name="leftRectangle">         The left rectangle. </param>
    ''' <param name="totalToggleFieldWidth"> Width of the total toggle field. </param>
    Public Overrides Sub RenderLeftToggleField(ByVal g As Graphics, ByVal leftRectangle As Rectangle, ByVal totalToggleFieldWidth As Integer)
        If g Is Nothing Then Return
        Dim leftColor As Color = Me.LeftSideColor

        If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
            leftColor = leftColor.ToGrayscale()
        End If

        Dim controlRectangle As Rectangle = Me.GetInnerControlRectangle()

        g.SetClip(controlRectangle)

        Dim halfCathetusLength As Integer = Me.GetHalfCathetusLengthBasedOnAngle()

        Dim adjustedLeftRect As New Rectangle(leftRectangle.X, leftRectangle.Y, leftRectangle.Width + halfCathetusLength, leftRectangle.Height)

        g.IntersectClip(adjustedLeftRect)

        Using leftBrush As Brush = New SolidBrush(leftColor)
            g.FillRectangle(leftBrush, adjustedLeftRect)
        End Using

        g.ResetClip()
    End Sub

    ''' <summary> Renders the right toggle field. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="g">                     The Graphics to process. </param>
    ''' <param name="rightRectangle">        The right rectangle. </param>
    ''' <param name="totalToggleFieldWidth"> Width of the total toggle field. </param>
    Public Overrides Sub RenderRightToggleField(ByVal g As Graphics, ByVal rightRectangle As Rectangle, ByVal totalToggleFieldWidth As Integer)
        If g Is Nothing Then Return
        Dim rightColor As Color = Me.RightSideColor

        If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
            rightColor = rightColor.ToGrayscale()
        End If

        Dim controlRectangle As Rectangle = Me.GetInnerControlRectangle()

        g.SetClip(controlRectangle)

        Dim halfCathetusLength As Integer = Me.GetHalfCathetusLengthBasedOnAngle()

        Dim adjustedRightRect As New Rectangle(rightRectangle.X - halfCathetusLength, rightRectangle.Y, rightRectangle.Width + halfCathetusLength, rightRectangle.Height)

        g.IntersectClip(adjustedRightRect)

        Using rightBrush As Brush = New SolidBrush(rightColor)
            g.FillRectangle(rightBrush, adjustedRightRect)
        End Using

        g.ResetClip()
    End Sub

    ''' <summary> Renders the button. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="g">               The Graphics to process. </param>
    ''' <param name="buttonRectangle"> The button rectangle. </param>
    Public Overrides Sub RenderButton(ByVal g As Graphics, ByVal buttonRectangle As Rectangle)
        If g Is Nothing Then Return
        Dim controlRectangle As Rectangle = Me.GetInnerControlRectangle()

        g.SetClip(controlRectangle)

        Dim fullCathetusLength As Integer = Me.GetCathetusLengthBasedOnAngle()
        Dim dblFullCathetusLength As Integer = 2 * fullCathetusLength

        Dim polygonPoints(3) As Point

        Dim adjustedButtonRect As New Rectangle(buttonRectangle.X - fullCathetusLength, controlRectangle.Y, buttonRectangle.Width + dblFullCathetusLength, controlRectangle.Height)

        If Me.SlantAngle > 0 Then
            polygonPoints(0) = New Point(adjustedButtonRect.X + fullCathetusLength, adjustedButtonRect.Y)
            polygonPoints(1) = New Point(adjustedButtonRect.X + adjustedButtonRect.Width - 1, adjustedButtonRect.Y)
            polygonPoints(2) = New Point(adjustedButtonRect.X + adjustedButtonRect.Width - fullCathetusLength - 1, adjustedButtonRect.Y + adjustedButtonRect.Height - 1)
            polygonPoints(3) = New Point(adjustedButtonRect.X, adjustedButtonRect.Y + adjustedButtonRect.Height - 1)
        Else
            polygonPoints(0) = New Point(adjustedButtonRect.X, adjustedButtonRect.Y)
            polygonPoints(1) = New Point(adjustedButtonRect.X + adjustedButtonRect.Width - fullCathetusLength - 1, adjustedButtonRect.Y)
            polygonPoints(2) = New Point(adjustedButtonRect.X + adjustedButtonRect.Width - 1, adjustedButtonRect.Y + adjustedButtonRect.Height - 1)
            polygonPoints(3) = New Point(adjustedButtonRect.X + fullCathetusLength, adjustedButtonRect.Y + adjustedButtonRect.Height - 1)
        End If

        g.IntersectClip(adjustedButtonRect)

        Dim buttonColor As Color = If(Me.ToggleSwitch.Checked, Me.OnButtonColor, Me.OffButtonColor)
        Dim buttonBorderColor As Color = If(Me.ToggleSwitch.Checked, Me.OnButtonBorderColor, Me.OffButtonBorderColor)

        If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
            buttonColor = buttonColor.ToGrayscale()
            buttonBorderColor = buttonBorderColor.ToGrayscale()
        End If

        Using buttonPen As New Pen(buttonBorderColor)
            g.DrawPolygon(buttonPen, polygonPoints)
        End Using

        Using buttonBrush As Brush = New SolidBrush(buttonColor)
            g.FillPolygon(buttonBrush, polygonPoints)
        End Using

        Dim buttonImage As Image = If(Me.ToggleSwitch.ButtonImage, If(Me.ToggleSwitch.Checked, Me.ToggleSwitch.OnButtonImage, Me.ToggleSwitch.OffButtonImage))
        Dim buttonText As String = If(Me.ToggleSwitch.Checked, Me.ToggleSwitch.OnText, Me.ToggleSwitch.OffText)

        If buttonImage IsNot Nothing OrElse (Not String.IsNullOrEmpty(buttonText)) Then
            Dim alignment As ToggleSwitchButtonAlignment = If(Me.ToggleSwitch.ButtonImage IsNot Nothing, Me.ToggleSwitch.ButtonAlignment, If(Me.ToggleSwitch.Checked, Me.ToggleSwitch.OnButtonAlignment, Me.ToggleSwitch.OffButtonAlignment))

            If buttonImage IsNot Nothing Then
                Dim imageSize As Size = buttonImage.Size
                Dim imageRectangle As Rectangle

                Dim imageXPos As Integer = CInt(adjustedButtonRect.X)

                Dim scaleImage As Boolean = If(Me.ToggleSwitch.ButtonImage IsNot Nothing, Me.ToggleSwitch.ButtonScaleImageToFit, If(Me.ToggleSwitch.Checked, Me.ToggleSwitch.OnButtonScaleImageToFit, Me.ToggleSwitch.OffButtonScaleImageToFit))

                If scaleImage Then
                    Dim canvasSize As Size = adjustedButtonRect.Size
                    Dim resizedImageSize As Size = ImageHelper.RescaleImageToFit(imageSize, canvasSize)

                    If alignment = ToggleSwitchButtonAlignment.Center Then
                        imageXPos = CInt(Fix(CSng(adjustedButtonRect.X) + ((CSng(adjustedButtonRect.Width) - CSng(resizedImageSize.Width)) / 2)))
                    ElseIf alignment = ToggleSwitchButtonAlignment.Right Then
                        imageXPos = CInt(Fix(CSng(adjustedButtonRect.X) + CSng(adjustedButtonRect.Width) - CSng(resizedImageSize.Width)))
                    End If

                    imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(adjustedButtonRect.Y) + ((CSng(adjustedButtonRect.Height) - CSng(resizedImageSize.Height)) / 2))), resizedImageSize.Width, resizedImageSize.Height)

                    If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
                        g.DrawImage(buttonImage, imageRectangle, 0, 0, buttonImage.Width, buttonImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                    Else
                        g.DrawImage(buttonImage, imageRectangle)
                    End If
                Else
                    If alignment = ToggleSwitchButtonAlignment.Center Then
                        imageXPos = CInt(Fix(CSng(adjustedButtonRect.X) + ((CSng(adjustedButtonRect.Width) - CSng(imageSize.Width)) / 2)))
                    ElseIf alignment = ToggleSwitchButtonAlignment.Right Then
                        imageXPos = CInt(Fix(CSng(adjustedButtonRect.X) + CSng(adjustedButtonRect.Width) - CSng(imageSize.Width)))
                    End If

                    imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(adjustedButtonRect.Y) + ((CSng(adjustedButtonRect.Height) - CSng(imageSize.Height)) / 2))), imageSize.Width, imageSize.Height)

                    If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
                        g.DrawImage(buttonImage, imageRectangle, 0, 0, buttonImage.Width, buttonImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                    Else
                        g.DrawImageUnscaled(buttonImage, imageRectangle)
                    End If
                End If
            ElseIf Not String.IsNullOrEmpty(buttonText) Then
                Dim buttonFont As Font = If(Me.ToggleSwitch.Checked, Me.ToggleSwitch.OnFont, Me.ToggleSwitch.OffFont)
                Dim buttonForeColor As Color = If(Me.ToggleSwitch.Checked, Me.ToggleSwitch.OnForeColor, Me.ToggleSwitch.OffForeColor)

                If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
                    buttonForeColor = buttonForeColor.ToGrayscale()
                End If

                Dim textSize As SizeF = g.MeasureString(buttonText, buttonFont)

                Dim textXPos As Single = adjustedButtonRect.X

                If alignment = ToggleSwitchButtonAlignment.Center Then
                    textXPos = CSng(adjustedButtonRect.X) + ((CSng(adjustedButtonRect.Width) - CSng(textSize.Width)) / 2)
                ElseIf alignment = ToggleSwitchButtonAlignment.Right Then
                    textXPos = CSng(adjustedButtonRect.X) + CSng(adjustedButtonRect.Width) - CSng(textSize.Width)
                End If

                Dim textRectangle As New RectangleF(textXPos, CSng(adjustedButtonRect.Y) + ((CSng(adjustedButtonRect.Height) - CSng(textSize.Height)) / 2), textSize.Width, textSize.Height)

                Using textBrush As Brush = New SolidBrush(buttonForeColor)
                    g.DrawString(buttonText, buttonFont, textBrush, textRectangle)
                End Using
            End If
        End If

        g.ResetClip()
    End Sub

#End Region ' Render Method Implementations

#Region "Helper Method Implementations"

    ''' <summary> Gets inner control rectangle. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> The inner control rectangle. </returns>
    Public Function GetInnerControlRectangle() As Rectangle
        Return New Rectangle(1, 1, Me.ToggleSwitch.Width - 2, Me.ToggleSwitch.Height - 2)
    End Function

    ''' <summary> Gets cathetus length based on angle. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> The cathetus length based on angle. </returns>
    Public Function GetCathetusLengthBasedOnAngle() As Integer
        If Me.SlantAngle = 0 Then
            Return 0
        End If

        Dim degrees As Double = Math.Abs(Me.SlantAngle)
        Dim radians As Double = degrees * (Math.PI / 180)
        Dim length As Double = Math.Tan(radians) * Me.ToggleSwitch.Height

        Return CInt(Fix(length))
    End Function

    ''' <summary> Gets half cathetus length based on angle. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> The half cathetus length based on angle. </returns>
    Public Function GetHalfCathetusLengthBasedOnAngle() As Integer
        If Me.SlantAngle = 0 Then
            Return 0
        End If

        Dim degrees As Double = Math.Abs(Me.SlantAngle)
        Dim radians As Double = degrees * (Math.PI / 180)
        Dim length As Double = Math.Tan(radians) * Me.ToggleSwitch.Height / 2

        Return CInt(Fix(length))
    End Function

    ''' <summary> Gets button width. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> The button width. </returns>
    Public Overrides Function GetButtonWidth() As Integer
        Dim buttonWidth As Double = CDbl(Me.ToggleSwitch.Width) / 2
        Return CInt(Fix(buttonWidth))
    End Function

    ''' <summary> Gets button rectangle. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> The button rectangle. </returns>
    Public Overrides Function GetButtonRectangle() As Rectangle
        Dim buttonWidth As Integer = Me.GetButtonWidth()
        Return Me.GetButtonRectangle(buttonWidth)
    End Function

    ''' <summary> Gets button rectangle. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="buttonWidth"> Width of the button. </param>
    ''' <returns> The button rectangle. </returns>
    Public Overrides Function GetButtonRectangle(ByVal buttonWidth As Integer) As Rectangle
        Dim buttonRect As New Rectangle(Me.ToggleSwitch.ButtonValue, 0, buttonWidth, Me.ToggleSwitch.Height)
        Return buttonRect
    End Function

#End Region ' Helper Method Implementations
End Class
