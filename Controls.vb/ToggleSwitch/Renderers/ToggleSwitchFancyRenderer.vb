Imports System.Drawing.Drawing2D

Imports isr.Core.ColorExtensions

''' <summary> A toggle switch fancy renderer. </summary>
''' <remarks>
''' (c) 2015 Johnny J.. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 12/4/2015 </para>
''' </remarks>
Public Class ToggleSwitchFancyRenderer
    Inherits ToggleSwitchRendererBase
    Implements IDisposable

#Region "Constructor"

    ''' <summary> Full pathname of the inner control file. </summary>
    Private _InnerControlPath As GraphicsPath = Nothing

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        Me.OuterBorderColor1 = Color.FromArgb(255, 197, 199, 201)
        Me.OuterBorderColor2 = Color.FromArgb(255, 207, 209, 212)
        Me.InnerBorderColor1 = Color.FromArgb(200, 205, 208, 207)
        Me.InnerBorderColor2 = Color.FromArgb(255, 207, 209, 212)
        Me.LeftSideBackColor1 = Color.FromArgb(255, 61, 110, 6)
        Me.LeftSideBackColor2 = Color.FromArgb(255, 93, 170, 9)
        Me.RightSideBackColor1 = Color.FromArgb(255, 149, 0, 0)
        Me.RightSideBackColor2 = Color.FromArgb(255, 198, 0, 0)
        Me.ButtonNormalBorderColor1 = Color.FromArgb(255, 212, 209, 211)
        Me.ButtonNormalBorderColor2 = Color.FromArgb(255, 197, 199, 201)
        Me.ButtonNormalUpperSurfaceColor1 = Color.FromArgb(255, 252, 251, 252)
        Me.ButtonNormalUpperSurfaceColor2 = Color.FromArgb(255, 247, 247, 247)
        Me.ButtonNormalLowerSurfaceColor1 = Color.FromArgb(255, 233, 233, 233)
        Me.ButtonNormalLowerSurfaceColor2 = Color.FromArgb(255, 225, 225, 225)
        Me.ButtonHoverBorderColor1 = Color.FromArgb(255, 212, 207, 209)
        Me.ButtonHoverBorderColor2 = Color.FromArgb(255, 223, 223, 223)
        Me.ButtonHoverUpperSurfaceColor1 = Color.FromArgb(255, 240, 239, 240)
        Me.ButtonHoverUpperSurfaceColor2 = Color.FromArgb(255, 235, 235, 235)
        Me.ButtonHoverLowerSurfaceColor1 = Color.FromArgb(255, 221, 221, 221)
        Me.ButtonHoverLowerSurfaceColor2 = Color.FromArgb(255, 214, 214, 214)
        Me.ButtonPressedBorderColor1 = Color.FromArgb(255, 176, 176, 176)
        Me.ButtonPressedBorderColor2 = Color.FromArgb(255, 176, 176, 176)
        Me.ButtonPressedUpperSurfaceColor1 = Color.FromArgb(255, 189, 188, 189)
        Me.ButtonPressedUpperSurfaceColor2 = Color.FromArgb(255, 185, 185, 185)
        Me.ButtonPressedLowerSurfaceColor1 = Color.FromArgb(255, 175, 175, 175)
        Me.ButtonPressedLowerSurfaceColor2 = Color.FromArgb(255, 169, 169, 169)
        Me.ButtonShadowColor1 = Color.FromArgb(50, 0, 0, 0)
        Me.ButtonShadowColor2 = Color.FromArgb(0, 0, 0, 0)

        Me.ButtonShadowWidth = 7
        Me.CornerRadius = 6
    End Sub

#Region " Disposable Support "

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Me.Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived
    ''' class provided proper implementation.
    ''' </summary>
    ''' <value> <c>True</c> if disposed; otherwise, <c>False</c>. </value>
    Protected Property IsDisposed() As Boolean

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me._InnerControlPath?.Dispose() : Me._InnerControlPath = Nothing
            End If
        Finally
            Me.IsDisposed = True
        End Try
    End Sub

#End Region


#End Region ' Constructor

#Region "Public Properties"

    ''' <summary> Gets or sets the outer border color 1. </summary>
    ''' <value> The outer border color 1. </value>
    Public Property OuterBorderColor1() As Color

    ''' <summary> Gets or sets the outer border color 2. </summary>
    ''' <value> The outer border color 2. </value>
    Public Property OuterBorderColor2() As Color

    ''' <summary> Gets or sets the inner border color 1. </summary>
    ''' <value> The inner border color 1. </value>
    Public Property InnerBorderColor1() As Color

    ''' <summary> Gets or sets the inner border color 2. </summary>
    ''' <value> The inner border color 2. </value>
    Public Property InnerBorderColor2() As Color

    ''' <summary> Gets or sets the left side back color 1. </summary>
    ''' <value> The left side back color 1. </value>
    Public Property LeftSideBackColor1() As Color

    ''' <summary> Gets or sets the left side back color 2. </summary>
    ''' <value> The left side back color 2. </value>
    Public Property LeftSideBackColor2() As Color

    ''' <summary> Gets or sets the right side back color 1. </summary>
    ''' <value> The right side back color 1. </value>
    Public Property RightSideBackColor1() As Color

    ''' <summary> Gets or sets the right side back color 2. </summary>
    ''' <value> The right side back color 2. </value>
    Public Property RightSideBackColor2() As Color

    ''' <summary> Gets or sets the button normal border color 1. </summary>
    ''' <value> The button normal border color 1. </value>
    Public Property ButtonNormalBorderColor1() As Color

    ''' <summary> Gets or sets the button normal border color 2. </summary>
    ''' <value> The button normal border color 2. </value>
    Public Property ButtonNormalBorderColor2() As Color

    ''' <summary> Gets or sets the button normal upper surface color 1. </summary>
    ''' <value> The button normal upper surface color 1. </value>
    Public Property ButtonNormalUpperSurfaceColor1() As Color

    ''' <summary> Gets or sets the button normal upper surface color 2. </summary>
    ''' <value> The button normal upper surface color 2. </value>
    Public Property ButtonNormalUpperSurfaceColor2() As Color

    ''' <summary> Gets or sets the button normal lower surface color 1. </summary>
    ''' <value> The button normal lower surface color 1. </value>
    Public Property ButtonNormalLowerSurfaceColor1() As Color

    ''' <summary> Gets or sets the button normal lower surface color 2. </summary>
    ''' <value> The button normal lower surface color 2. </value>
    Public Property ButtonNormalLowerSurfaceColor2() As Color

    ''' <summary> Gets or sets the button hover border color 1. </summary>
    ''' <value> The button hover border color 1. </value>
    Public Property ButtonHoverBorderColor1() As Color

    ''' <summary> Gets or sets the button hover border color 2. </summary>
    ''' <value> The button hover border color 2. </value>
    Public Property ButtonHoverBorderColor2() As Color

    ''' <summary> Gets or sets the button hover upper surface color 1. </summary>
    ''' <value> The button hover upper surface color 1. </value>
    Public Property ButtonHoverUpperSurfaceColor1() As Color

    ''' <summary> Gets or sets the button hover upper surface color 2. </summary>
    ''' <value> The button hover upper surface color 2. </value>
    Public Property ButtonHoverUpperSurfaceColor2() As Color

    ''' <summary> Gets or sets the button hover lower surface color 1. </summary>
    ''' <value> The button hover lower surface color 1. </value>
    Public Property ButtonHoverLowerSurfaceColor1() As Color

    ''' <summary> Gets or sets the button hover lower surface color 2. </summary>
    ''' <value> The button hover lower surface color 2. </value>
    Public Property ButtonHoverLowerSurfaceColor2() As Color

    ''' <summary> Gets or sets the button pressed border color 1. </summary>
    ''' <value> The button pressed border color 1. </value>
    Public Property ButtonPressedBorderColor1() As Color

    ''' <summary> Gets or sets the button pressed border color 2. </summary>
    ''' <value> The button pressed border color 2. </value>
    Public Property ButtonPressedBorderColor2() As Color

    ''' <summary> Gets or sets the button pressed upper surface color 1. </summary>
    ''' <value> The button pressed upper surface color 1. </value>
    Public Property ButtonPressedUpperSurfaceColor1() As Color

    ''' <summary> Gets or sets the button pressed upper surface color 2. </summary>
    ''' <value> The button pressed upper surface color 2. </value>
    Public Property ButtonPressedUpperSurfaceColor2() As Color

    ''' <summary> Gets or sets the button pressed lower surface color 1. </summary>
    ''' <value> The button pressed lower surface color 1. </value>
    Public Property ButtonPressedLowerSurfaceColor1() As Color

    ''' <summary> Gets or sets the button pressed lower surface color 2. </summary>
    ''' <value> The button pressed lower surface color 2. </value>
    Public Property ButtonPressedLowerSurfaceColor2() As Color

    ''' <summary> Gets or sets the button shadow color 1. </summary>
    ''' <value> The button shadow color 1. </value>
    Public Property ButtonShadowColor1() As Color

    ''' <summary> Gets or sets the button shadow color 2. </summary>
    ''' <value> The button shadow color 2. </value>
    Public Property ButtonShadowColor2() As Color

    ''' <summary> Gets or sets the width of the button shadow. </summary>
    ''' <value> The width of the button shadow. </value>
    Public Property ButtonShadowWidth() As Integer

    ''' <summary> Gets or sets the corner radius. </summary>
    ''' <value> The corner radius. </value>
    Public Property CornerRadius() As Integer

#End Region ' Public Properties

#Region "Render Method Implementations"

    ''' <summary> Renders the border. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="g">               The Graphics to process. </param>
    ''' <param name="borderRectangle"> The border rectangle. </param>
    Public Overrides Sub RenderBorder(ByVal g As Graphics, ByVal borderRectangle As Rectangle)
        If g Is Nothing Then Return
        g.SmoothingMode = SmoothingMode.HighQuality
        g.PixelOffsetMode = PixelOffsetMode.HighQuality
        g.InterpolationMode = InterpolationMode.HighQualityBilinear

        'Draw outer border
        Using outerBorderPath As GraphicsPath = Me.GetRoundedRectanglePath(borderRectangle, Me.CornerRadius)
            g.SetClip(outerBorderPath)

            'INSTANT VB NOTE: The variable outerBorderColor1 was renamed since Visual Basic does not handle local variables named the same as class members well:
            Dim outerBorderColor1_Renamed As Color = If((Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled, Me.OuterBorderColor1.ToGrayscale(), Me.OuterBorderColor1)
            'INSTANT VB NOTE: The variable outerBorderColor2 was renamed since Visual Basic does not handle local variables named the same as class members well:
            Dim outerBorderColor2_Renamed As Color = If((Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled, Me.OuterBorderColor2.ToGrayscale(), Me.OuterBorderColor2)

            Using outerBorderBrush As Brush = New LinearGradientBrush(borderRectangle, outerBorderColor1_Renamed, outerBorderColor2_Renamed, LinearGradientMode.Vertical)
                g.FillPath(outerBorderBrush, outerBorderPath)
            End Using

            g.ResetClip()
        End Using

        'Draw inner border
        Dim innerborderRectangle As New Rectangle(borderRectangle.X + 1, borderRectangle.Y + 1, borderRectangle.Width - 2, borderRectangle.Height - 2)

        Using innerBorderPath As GraphicsPath = Me.GetRoundedRectanglePath(innerborderRectangle, Me.CornerRadius)
            g.SetClip(innerBorderPath)

            'INSTANT VB NOTE: The variable innerBorderColor1 was renamed since Visual Basic does not handle local variables named the same as class members well:
            Dim innerBorderColor1_Renamed As Color = If((Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled, Me.InnerBorderColor1.ToGrayscale(), Me.InnerBorderColor1)
            'INSTANT VB NOTE: The variable innerBorderColor2 was renamed since Visual Basic does not handle local variables named the same as class members well:
            Dim innerBorderColor2_Renamed As Color = If((Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled, Me.InnerBorderColor2.ToGrayscale(), Me.InnerBorderColor2)

            Using borderBrush As Brush = New LinearGradientBrush(borderRectangle, innerBorderColor1_Renamed, innerBorderColor2_Renamed, LinearGradientMode.Vertical)
                g.FillPath(borderBrush, innerBorderPath)
            End Using

            g.ResetClip()
        End Using

        Dim backgroundRectangle As New Rectangle(borderRectangle.X + 2, borderRectangle.Y + 2, borderRectangle.Width - 4, borderRectangle.Height - 4)
        Me._InnerControlPath = Me.GetRoundedRectanglePath(backgroundRectangle, Me.CornerRadius)
    End Sub

    ''' <summary> Renders the left toggle field. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="g">                     The Graphics to process. </param>
    ''' <param name="leftRectangle">         The left rectangle. </param>
    ''' <param name="totalToggleFieldWidth"> Width of the total toggle field. </param>
    Public Overrides Sub RenderLeftToggleField(ByVal g As Graphics, ByVal leftRectangle As Rectangle, ByVal totalToggleFieldWidth As Integer)
        If g Is Nothing Then Return
        g.SmoothingMode = SmoothingMode.HighQuality
        g.PixelOffsetMode = PixelOffsetMode.HighQuality
        g.InterpolationMode = InterpolationMode.HighQualityBilinear

        Dim buttonWidth As Integer = Me.GetButtonWidth()

        'Draw inner background
        Dim gradientRectWidth As Integer = leftRectangle.Width + buttonWidth \ 2
        Dim gradientRectangle As New Rectangle(leftRectangle.X, leftRectangle.Y, gradientRectWidth, leftRectangle.Height)

        'INSTANT VB NOTE: The variable leftSideBackColor1 was renamed since Visual Basic does not handle local variables named the same as class members well:
        Dim leftSideBackColor1_Renamed As Color = If((Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled, Me.LeftSideBackColor1.ToGrayscale(), Me.LeftSideBackColor1)
        'INSTANT VB NOTE: The variable leftSideBackColor2 was renamed since Visual Basic does not handle local variables named the same as class members well:
        Dim leftSideBackColor2_Renamed As Color = If((Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled, Me.LeftSideBackColor2.ToGrayscale(), Me.LeftSideBackColor2)

        If Me._InnerControlPath IsNot Nothing Then
            g.SetClip(Me._InnerControlPath)
            g.IntersectClip(gradientRectangle)
        Else
            g.SetClip(gradientRectangle)
        End If

        Using backgroundBrush As Brush = New LinearGradientBrush(gradientRectangle, leftSideBackColor1_Renamed, leftSideBackColor2_Renamed, LinearGradientMode.Vertical)
            g.FillRectangle(backgroundBrush, gradientRectangle)
        End Using

        g.ResetClip()

        Dim leftShadowRectangle As New Rectangle() With {
            .X = leftRectangle.X + leftRectangle.Width - Me.ButtonShadowWidth,
            .Y = leftRectangle.Y,
            .Width = Me.ButtonShadowWidth + Me.CornerRadius,
            .Height = leftRectangle.Height}

        If Me._InnerControlPath IsNot Nothing Then
            g.SetClip(Me._InnerControlPath)
            g.IntersectClip(leftShadowRectangle)
        Else
            g.SetClip(leftShadowRectangle)
        End If

        Using buttonShadowBrush As Brush = New LinearGradientBrush(leftShadowRectangle, Me.ButtonShadowColor2, Me.ButtonShadowColor1, LinearGradientMode.Horizontal)
            g.FillRectangle(buttonShadowBrush, leftShadowRectangle)
        End Using

        g.ResetClip()

        'Draw image or text
        If Me.ToggleSwitch.OnSideImage IsNot Nothing OrElse (Not String.IsNullOrEmpty(Me.ToggleSwitch.OnText)) Then
            Dim fullRectangle As New RectangleF(leftRectangle.X + 1 - (totalToggleFieldWidth - leftRectangle.Width), 1, totalToggleFieldWidth - 1, Me.ToggleSwitch.Height - 2)

            If Me._InnerControlPath IsNot Nothing Then
                g.SetClip(Me._InnerControlPath)
                g.IntersectClip(fullRectangle)
            Else
                g.SetClip(fullRectangle)
            End If

            If Me.ToggleSwitch.OnSideImage IsNot Nothing Then
                Dim imageSize As Size = Me.ToggleSwitch.OnSideImage.Size
                Dim imageRectangle As Rectangle

                Dim imageXPos As Integer = CInt(Fix(fullRectangle.X))

                If Me.ToggleSwitch.OnSideScaleImageToFit Then
                    Dim canvasSize As New Size(CInt(fullRectangle.Width), CInt(fullRectangle.Height))
                    Dim resizedImageSize As Size = ImageHelper.RescaleImageToFit(imageSize, canvasSize)

                    If Me.ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Center Then
                        imageXPos = CInt(Fix(CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(resizedImageSize.Width)) / 2)))
                    ElseIf Me.ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Near Then
                        imageXPos = CInt(Fix(CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(resizedImageSize.Width)))
                    End If

                    imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(resizedImageSize.Height)) / 2))), resizedImageSize.Width, resizedImageSize.Height)

                    If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
                        g.DrawImage(Me.ToggleSwitch.OnSideImage, imageRectangle, 0, 0, Me.ToggleSwitch.OnSideImage.Width, Me.ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                    Else
                        g.DrawImage(Me.ToggleSwitch.OnSideImage, imageRectangle)
                    End If
                Else
                    If Me.ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Center Then
                        imageXPos = CInt(Fix(CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(imageSize.Width)) / 2)))
                    ElseIf Me.ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Near Then
                        imageXPos = CInt(Fix(CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(imageSize.Width)))
                    End If

                    imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(imageSize.Height)) / 2))), imageSize.Width, imageSize.Height)

                    If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
                        g.DrawImage(Me.ToggleSwitch.OnSideImage, imageRectangle, 0, 0, Me.ToggleSwitch.OnSideImage.Width, Me.ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                    Else
                        g.DrawImageUnscaled(Me.ToggleSwitch.OnSideImage, imageRectangle)
                    End If
                End If
            ElseIf Not String.IsNullOrEmpty(Me.ToggleSwitch.OnText) Then
                Dim textSize As SizeF = g.MeasureString(Me.ToggleSwitch.OnText, Me.ToggleSwitch.OnFont)

                Dim textXPos As Single = fullRectangle.X

                If Me.ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Center Then
                    textXPos = CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(textSize.Width)) / 2)
                ElseIf Me.ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Near Then
                    textXPos = CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(textSize.Width)
                End If

                Dim textRectangle As New RectangleF(textXPos, CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(textSize.Height)) / 2), textSize.Width, textSize.Height)

                Dim textForeColor As Color = Me.ToggleSwitch.OnForeColor

                If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
                    textForeColor = textForeColor.ToGrayscale()
                End If

                Using textBrush As Brush = New SolidBrush(textForeColor)
                    g.DrawString(Me.ToggleSwitch.OnText, Me.ToggleSwitch.OnFont, textBrush, textRectangle)
                End Using
            End If

            g.ResetClip()
        End If
    End Sub

    ''' <summary> Renders the right toggle field. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="g">                     The Graphics to process. </param>
    ''' <param name="rightRectangle">        The right rectangle. </param>
    ''' <param name="totalToggleFieldWidth"> Width of the total toggle field. </param>
    Public Overrides Sub RenderRightToggleField(ByVal g As Graphics, ByVal rightRectangle As Rectangle, ByVal totalToggleFieldWidth As Integer)
        If g Is Nothing Then Return
        g.SmoothingMode = SmoothingMode.HighQuality
        g.PixelOffsetMode = PixelOffsetMode.HighQuality
        g.InterpolationMode = InterpolationMode.HighQualityBilinear

        Dim buttonWidth As Integer = Me.GetButtonWidth()

        'Draw inner background
        Dim gradientRectWidth As Integer = rightRectangle.Width + buttonWidth \ 2
        Dim gradientRectangle As New Rectangle(rightRectangle.X - buttonWidth \ 2, rightRectangle.Y, gradientRectWidth, rightRectangle.Height)

        'INSTANT VB NOTE: The variable rightSideBackColor1 was renamed since Visual Basic does not handle local variables named the same as class members well:
        Dim rightSideBackColor1_Renamed As Color = If((Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled, Me.RightSideBackColor1.ToGrayscale(), Me.RightSideBackColor1)
        'INSTANT VB NOTE: The variable rightSideBackColor2 was renamed since Visual Basic does not handle local variables named the same as class members well:
        Dim rightSideBackColor2_Renamed As Color = If((Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled, Me.RightSideBackColor2.ToGrayscale(), Me.RightSideBackColor2)

        If Me._InnerControlPath IsNot Nothing Then
            g.SetClip(Me._InnerControlPath)
            g.IntersectClip(gradientRectangle)
        Else
            g.SetClip(gradientRectangle)
        End If

        Using backgroundBrush As Brush = New LinearGradientBrush(gradientRectangle, rightSideBackColor1_Renamed, rightSideBackColor2_Renamed, LinearGradientMode.Vertical)
            g.FillRectangle(backgroundBrush, gradientRectangle)
        End Using

        g.ResetClip()

        Dim rightShadowRectangle As New Rectangle() With {
            .X = rightRectangle.X - Me.CornerRadius,
            .Y = rightRectangle.Y,
            .Width = Me.ButtonShadowWidth + Me.CornerRadius,
            .Height = rightRectangle.Height}

        If Me._InnerControlPath IsNot Nothing Then
            g.SetClip(Me._InnerControlPath)
            g.IntersectClip(rightShadowRectangle)
        Else
            g.SetClip(rightShadowRectangle)
        End If

        Using buttonShadowBrush As Brush = New LinearGradientBrush(rightShadowRectangle, Me.ButtonShadowColor1, Me.ButtonShadowColor2, LinearGradientMode.Horizontal)
            g.FillRectangle(buttonShadowBrush, rightShadowRectangle)
        End Using

        g.ResetClip()

        'Draw image or text
        If Me.ToggleSwitch.OffSideImage IsNot Nothing OrElse (Not String.IsNullOrEmpty(Me.ToggleSwitch.OffText)) Then
            Dim fullRectangle As New RectangleF(rightRectangle.X, 1, totalToggleFieldWidth - 1, Me.ToggleSwitch.Height - 2)

            If Me._InnerControlPath IsNot Nothing Then
                g.SetClip(Me._InnerControlPath)
                g.IntersectClip(fullRectangle)
            Else
                g.SetClip(fullRectangle)
            End If

            If Me.ToggleSwitch.OffSideImage IsNot Nothing Then
                Dim imageSize As Size = Me.ToggleSwitch.OffSideImage.Size
                Dim imageRectangle As Rectangle

                Dim imageXPos As Integer = CInt(Fix(fullRectangle.X))

                If Me.ToggleSwitch.OffSideScaleImageToFit Then
                    Dim canvasSize As New Size(CInt(fullRectangle.Width), CInt(fullRectangle.Height))
                    Dim resizedImageSize As Size = ImageHelper.RescaleImageToFit(imageSize, canvasSize)

                    If Me.ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Center Then
                        imageXPos = CInt(Fix(CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(resizedImageSize.Width)) / 2)))
                    ElseIf Me.ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Far Then
                        imageXPos = CInt(Fix(CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(resizedImageSize.Width)))
                    End If

                    imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(resizedImageSize.Height)) / 2))), resizedImageSize.Width, resizedImageSize.Height)

                    If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
                        g.DrawImage(Me.ToggleSwitch.OnSideImage, imageRectangle, 0, 0, Me.ToggleSwitch.OnSideImage.Width, Me.ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                    Else
                        g.DrawImage(Me.ToggleSwitch.OnSideImage, imageRectangle)
                    End If
                Else
                    If Me.ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Center Then
                        imageXPos = CInt(Fix(CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(imageSize.Width)) / 2)))
                    ElseIf Me.ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Far Then
                        imageXPos = CInt(Fix(CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(imageSize.Width)))
                    End If

                    imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(imageSize.Height)) / 2))), imageSize.Width, imageSize.Height)

                    If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
                        g.DrawImage(Me.ToggleSwitch.OnSideImage, imageRectangle, 0, 0, Me.ToggleSwitch.OnSideImage.Width, Me.ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                    Else
                        g.DrawImageUnscaled(Me.ToggleSwitch.OffSideImage, imageRectangle)
                    End If
                End If
            ElseIf Not String.IsNullOrEmpty(Me.ToggleSwitch.OffText) Then
                Dim textSize As SizeF = g.MeasureString(Me.ToggleSwitch.OffText, Me.ToggleSwitch.OffFont)

                Dim textXPos As Single = fullRectangle.X

                If Me.ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Center Then
                    textXPos = CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(textSize.Width)) / 2)
                ElseIf Me.ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Far Then
                    textXPos = CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(textSize.Width)
                End If

                Dim textRectangle As New RectangleF(textXPos, CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(textSize.Height)) / 2), textSize.Width, textSize.Height)

                Dim textForeColor As Color = Me.ToggleSwitch.OffForeColor

                If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
                    textForeColor = textForeColor.ToGrayscale()
                End If

                Using textBrush As Brush = New SolidBrush(textForeColor)
                    g.DrawString(Me.ToggleSwitch.OffText, Me.ToggleSwitch.OffFont, textBrush, textRectangle)
                End Using
            End If

            g.ResetClip()
        End If
    End Sub

    ''' <summary> Renders the button. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="g">               The Graphics to process. </param>
    ''' <param name="buttonRectangle"> The button rectangle. </param>
    Public Overrides Sub RenderButton(ByVal g As Graphics, ByVal buttonRectangle As Rectangle)
        If g Is Nothing Then Return
        g.SmoothingMode = SmoothingMode.HighQuality
        g.PixelOffsetMode = PixelOffsetMode.HighQuality
        g.InterpolationMode = InterpolationMode.HighQualityBilinear

        'Draw button surface
        Dim buttonUpperSurfaceColor1 As Color = Me.ButtonNormalUpperSurfaceColor1
        Dim buttonUpperSurfaceColor2 As Color = Me.ButtonNormalUpperSurfaceColor2
        Dim buttonLowerSurfaceColor1 As Color = Me.ButtonNormalLowerSurfaceColor1
        Dim buttonLowerSurfaceColor2 As Color = Me.ButtonNormalLowerSurfaceColor2

        If Me.ToggleSwitch.IsButtonPressed Then
            buttonUpperSurfaceColor1 = Me.ButtonPressedUpperSurfaceColor1
            buttonUpperSurfaceColor2 = Me.ButtonPressedUpperSurfaceColor2
            buttonLowerSurfaceColor1 = Me.ButtonPressedLowerSurfaceColor1
            buttonLowerSurfaceColor2 = Me.ButtonPressedLowerSurfaceColor2
        ElseIf Me.ToggleSwitch.IsButtonHovered Then
            buttonUpperSurfaceColor1 = Me.ButtonHoverUpperSurfaceColor1
            buttonUpperSurfaceColor2 = Me.ButtonHoverUpperSurfaceColor2
            buttonLowerSurfaceColor1 = Me.ButtonHoverLowerSurfaceColor1
            buttonLowerSurfaceColor2 = Me.ButtonHoverLowerSurfaceColor2
        End If

        If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
            buttonUpperSurfaceColor1 = buttonUpperSurfaceColor1.ToGrayscale()
            buttonUpperSurfaceColor2 = buttonUpperSurfaceColor2.ToGrayscale()
            buttonLowerSurfaceColor1 = buttonLowerSurfaceColor1.ToGrayscale()
            buttonLowerSurfaceColor2 = buttonLowerSurfaceColor2.ToGrayscale()
        End If

        buttonRectangle.Inflate(-1, -1)

        Dim upperHeight As Integer = buttonRectangle.Height \ 2

        Dim upperGradientRect As New Rectangle(buttonRectangle.X, buttonRectangle.Y, buttonRectangle.Width, upperHeight)
        Dim lowerGradientRect As New Rectangle(buttonRectangle.X, buttonRectangle.Y + upperHeight, buttonRectangle.Width, buttonRectangle.Height - upperHeight)

        Using buttonPath As GraphicsPath = Me.GetRoundedRectanglePath(buttonRectangle, Me.CornerRadius)
            g.SetClip(buttonPath)
            g.IntersectClip(upperGradientRect)

            'Draw upper button surface gradient
            Using buttonUpperSurfaceBrush As Brush = New LinearGradientBrush(buttonRectangle, buttonUpperSurfaceColor1, buttonUpperSurfaceColor2, LinearGradientMode.Vertical)
                g.FillPath(buttonUpperSurfaceBrush, buttonPath)
            End Using

            g.ResetClip()

            g.SetClip(buttonPath)
            g.IntersectClip(lowerGradientRect)

            'Draw lower button surface gradient
            Using buttonLowerSurfaceBrush As Brush = New LinearGradientBrush(buttonRectangle, buttonLowerSurfaceColor1, buttonLowerSurfaceColor2, LinearGradientMode.Vertical)
                g.FillPath(buttonLowerSurfaceBrush, buttonPath)
            End Using

            g.ResetClip()

            g.SetClip(buttonPath)

            'Draw button border
            Dim buttonBorderColor1 As Color = Me.ButtonNormalBorderColor1
            Dim buttonBorderColor2 As Color = Me.ButtonNormalBorderColor2

            If Me.ToggleSwitch.IsButtonPressed Then
                buttonBorderColor1 = Me.ButtonPressedBorderColor1
                buttonBorderColor2 = Me.ButtonPressedBorderColor2
            ElseIf Me.ToggleSwitch.IsButtonHovered Then
                buttonBorderColor1 = Me.ButtonHoverBorderColor1
                buttonBorderColor2 = Me.ButtonHoverBorderColor2
            End If

            If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
                buttonBorderColor1 = buttonBorderColor1.ToGrayscale()
                buttonBorderColor2 = buttonBorderColor2.ToGrayscale()
            End If

            Using buttonBorderBrush As Brush = New LinearGradientBrush(buttonRectangle, buttonBorderColor1, buttonBorderColor2, LinearGradientMode.Vertical)
                Using buttonBorderPen As New Pen(buttonBorderBrush)
                    g.DrawPath(buttonBorderPen, buttonPath)
                End Using
            End Using

            g.ResetClip()

            'Draw button image
            Dim buttonImage As Image = If(Me.ToggleSwitch.ButtonImage, If(Me.ToggleSwitch.Checked, Me.ToggleSwitch.OnButtonImage, Me.ToggleSwitch.OffButtonImage))

            If buttonImage IsNot Nothing Then
                g.SetClip(buttonPath)

                Dim alignment As ToggleSwitchButtonAlignment = If(Me.ToggleSwitch.ButtonImage IsNot Nothing, Me.ToggleSwitch.ButtonAlignment, If(Me.ToggleSwitch.Checked, Me.ToggleSwitch.OnButtonAlignment, Me.ToggleSwitch.OffButtonAlignment))

                Dim imageSize As Size = buttonImage.Size

                Dim imageRectangle As Rectangle

                Dim imageXPos As Integer = buttonRectangle.X

                Dim scaleImage As Boolean = If(Me.ToggleSwitch.ButtonImage IsNot Nothing, Me.ToggleSwitch.ButtonScaleImageToFit, If(Me.ToggleSwitch.Checked, Me.ToggleSwitch.OnButtonScaleImageToFit, Me.ToggleSwitch.OffButtonScaleImageToFit))

                If scaleImage Then
                    Dim canvasSize As Size = buttonRectangle.Size
                    Dim resizedImageSize As Size = ImageHelper.RescaleImageToFit(imageSize, canvasSize)

                    If alignment = ToggleSwitchButtonAlignment.Center Then
                        imageXPos = CInt(Fix(CSng(buttonRectangle.X) + ((CSng(buttonRectangle.Width) - CSng(resizedImageSize.Width)) / 2)))
                    ElseIf alignment = ToggleSwitchButtonAlignment.Right Then
                        imageXPos = CInt(Fix(CSng(buttonRectangle.X) + CSng(buttonRectangle.Width) - CSng(resizedImageSize.Width)))
                    End If

                    imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(buttonRectangle.Y) + ((CSng(buttonRectangle.Height) - CSng(resizedImageSize.Height)) / 2))), resizedImageSize.Width, resizedImageSize.Height)

                    If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
                        g.DrawImage(buttonImage, imageRectangle, 0, 0, buttonImage.Width, buttonImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                    Else
                        g.DrawImage(buttonImage, imageRectangle)
                    End If
                Else
                    If alignment = ToggleSwitchButtonAlignment.Center Then
                        imageXPos = CInt(Fix(CSng(buttonRectangle.X) + ((CSng(buttonRectangle.Width) - CSng(imageSize.Width)) / 2)))
                    ElseIf alignment = ToggleSwitchButtonAlignment.Right Then
                        imageXPos = CInt(Fix(CSng(buttonRectangle.X) + CSng(buttonRectangle.Width) - CSng(imageSize.Width)))
                    End If

                    imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(buttonRectangle.Y) + ((CSng(buttonRectangle.Height) - CSng(imageSize.Height)) / 2))), imageSize.Width, imageSize.Height)

                    If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
                        g.DrawImage(buttonImage, imageRectangle, 0, 0, buttonImage.Width, buttonImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                    Else
                        g.DrawImageUnscaled(buttonImage, imageRectangle)
                    End If
                End If

                g.ResetClip()
            End If
        End Using
    End Sub

#End Region ' Render Method Implementations

#Region "Helper Method Implementations"

    ''' <summary> Gets rounded rectangle path. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="rectangle"> The rectangle. </param>
    ''' <param name="radius">    The radius. </param>
    ''' <returns> The rounded rectangle path. </returns>
    Public Function GetRoundedRectanglePath(ByVal rectangle As Rectangle, ByVal radius As Integer) As GraphicsPath
        Dim gp As New GraphicsPath()
        Dim diameter As Integer = 2 * radius

        If diameter > Me.ToggleSwitch.Height Then
            diameter = Me.ToggleSwitch.Height
        End If

        If diameter > Me.ToggleSwitch.Width Then
            diameter = Me.ToggleSwitch.Width
        End If

        gp.AddArc(rectangle.X, rectangle.Y, diameter, diameter, 180, 90)
        gp.AddArc(rectangle.X + rectangle.Width - diameter, rectangle.Y, diameter, diameter, 270, 90)
        gp.AddArc(rectangle.X + rectangle.Width - diameter, rectangle.Y + rectangle.Height - diameter, diameter, diameter, 0, 90)
        gp.AddArc(rectangle.X, rectangle.Y + rectangle.Height - diameter, diameter, diameter, 90, 90)
        gp.CloseFigure()

        Return gp
    End Function

    ''' <summary> Gets button width. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> The button width. </returns>
    Public Overrides Function GetButtonWidth() As Integer
        Dim buttonWidth As Single = 1.61F * Me.ToggleSwitch.Height
        Return CInt(Fix(buttonWidth))
    End Function

    ''' <summary> Gets button rectangle. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> The button rectangle. </returns>
    Public Overrides Function GetButtonRectangle() As Rectangle
        Dim buttonWidth As Integer = Me.GetButtonWidth()
        Return Me.GetButtonRectangle(buttonWidth)
    End Function

    ''' <summary> Gets button rectangle. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="buttonWidth"> Width of the button. </param>
    ''' <returns> The button rectangle. </returns>
    Public Overrides Function GetButtonRectangle(ByVal buttonWidth As Integer) As Rectangle
        Dim buttonRect As New Rectangle(Me.ToggleSwitch.ButtonValue, 0, buttonWidth, Me.ToggleSwitch.Height)
        Return buttonRect
    End Function

#End Region ' Helper Method Implementations
End Class
