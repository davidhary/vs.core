Imports System.Drawing.Drawing2D

Imports isr.Core.ColorExtensions

''' <summary> A toggle switch brushed metal renderer. </summary>
''' <remarks>
''' (c) 2015 Johnny J.. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 12/4/2015 </para>
''' </remarks>
Public Class ToggleSwitchBrushedMetalRenderer
    Inherits ToggleSwitchRendererBase

#Region "Constructor"

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        Me.BorderColor1 = Color.FromArgb(255, 145, 146, 149)
        Me.BorderColor2 = Color.FromArgb(255, 227, 229, 232)
        Me.BackColor1 = Color.FromArgb(255, 125, 126, 128)
        Me.BackColor2 = Color.FromArgb(255, 224, 226, 228)
        Me.UpperShadowColor1 = Color.FromArgb(150, 0, 0, 0)
        Me.UpperShadowColor2 = Color.FromArgb(5, 0, 0, 0)
        Me.ButtonNormalBorderColor = Color.FromArgb(255, 144, 144, 145)
        Me.ButtonNormalSurfaceColor = Color.FromArgb(255, 251, 251, 251)
        Me.ButtonHoverBorderColor = Color.FromArgb(255, 166, 167, 168)
        Me.ButtonHoverSurfaceColor = Color.FromArgb(255, 238, 238, 238)
        Me.ButtonPressedBorderColor = Color.FromArgb(255, 123, 123, 123)
        Me.ButtonPressedSurfaceColor = Color.FromArgb(255, 184, 184, 184)

        Me.UpperShadowHeight = 8
    End Sub

#End Region ' Constructor

#Region "Public Properties"

    ''' <summary> Gets or sets the border color 1. </summary>
    ''' <value> The border color 1. </value>
    Public Property BorderColor1() As Color

    ''' <summary> Gets or sets the border color 2. </summary>
    ''' <value> The border color 2. </value>
    Public Property BorderColor2() As Color

    ''' <summary> Gets or sets the back color 1. </summary>
    ''' <value> The back color 1. </value>
    Public Property BackColor1() As Color

    ''' <summary> Gets or sets the back color 2. </summary>
    ''' <value> The back color 2. </value>
    Public Property BackColor2() As Color

    ''' <summary> Gets or sets the upper shadow color 1. </summary>
    ''' <value> The upper shadow color 1. </value>
    Public Property UpperShadowColor1() As Color

    ''' <summary> Gets or sets the upper shadow color 2. </summary>
    ''' <value> The upper shadow color 2. </value>
    Public Property UpperShadowColor2() As Color

    ''' <summary> Gets or sets the color of the button normal border. </summary>
    ''' <value> The color of the button normal border. </value>
    Public Property ButtonNormalBorderColor() As Color

    ''' <summary> Gets or sets the color of the button normal surface. </summary>
    ''' <value> The color of the button normal surface. </value>
    Public Property ButtonNormalSurfaceColor() As Color

    ''' <summary> Gets or sets the color of the button hover border. </summary>
    ''' <value> The color of the button hover border. </value>
    Public Property ButtonHoverBorderColor() As Color

    ''' <summary> Gets or sets the color of the button hover surface. </summary>
    ''' <value> The color of the button hover surface. </value>
    Public Property ButtonHoverSurfaceColor() As Color

    ''' <summary> Gets or sets the color of the button pressed border. </summary>
    ''' <value> The color of the button pressed border. </value>
    Public Property ButtonPressedBorderColor() As Color

    ''' <summary> Gets or sets the color of the button pressed surface. </summary>
    ''' <value> The color of the button pressed surface. </value>
    Public Property ButtonPressedSurfaceColor() As Color

    ''' <summary> Gets or sets the height of the upper shadow. </summary>
    ''' <value> The height of the upper shadow. </value>
    Public Property UpperShadowHeight() As Integer

#End Region ' Public Properties

#Region "Render Method Implementations"

    ''' <summary> Renders the border. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="g">               The Graphics to process. </param>
    ''' <param name="borderRectangle"> The border rectangle. </param>
    Public Overrides Sub RenderBorder(ByVal g As Graphics, ByVal borderRectangle As Rectangle)
        If g Is Nothing Then Return
        g.SmoothingMode = SmoothingMode.HighQuality
        g.PixelOffsetMode = PixelOffsetMode.HighQuality
        g.InterpolationMode = InterpolationMode.HighQualityBilinear

        'Draw outer border
        Using outerControlPath As GraphicsPath = GetRectangleClipPath(borderRectangle)
            g.SetClip(outerControlPath)
            Dim color1 As Color = If((Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled, Me.BorderColor1.ToGrayscale(), Me.BorderColor1)
            Dim color2 As Color = If((Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled, Me.BorderColor2.ToGrayscale(), Me.BorderColor2)
            Using borderBrush As Brush = New LinearGradientBrush(borderRectangle, color1, color2, LinearGradientMode.Vertical)
                g.FillPath(borderBrush, outerControlPath)
            End Using

            g.ResetClip()
        End Using

        'Draw inner background
        Dim innercontrolRectangle As New Rectangle(borderRectangle.X + 1, borderRectangle.Y + 1, borderRectangle.Width - 1, borderRectangle.Height - 2)

        Using innerControlPath As GraphicsPath = GetRectangleClipPath(innercontrolRectangle)
            g.SetClip(innerControlPath)
            Dim backColor1_Renamed As Color = If((Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled,
                                                                                    Me.BackColor1.ToGrayscale(), Me.BackColor1)
            Dim backColor2_Renamed As Color = If((Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled,
                                                                                    Me.BackColor2.ToGrayscale(), Me.BackColor2)

            Using backgroundBrush As Brush = New LinearGradientBrush(borderRectangle, backColor1_Renamed, backColor2_Renamed, LinearGradientMode.Horizontal)
                g.FillPath(backgroundBrush, innerControlPath)
            End Using

            'Draw inner top shadow
            Dim upperShadowRectangle As New Rectangle(innercontrolRectangle.X, innercontrolRectangle.Y, innercontrolRectangle.Width, Me.UpperShadowHeight)

            g.IntersectClip(upperShadowRectangle)

            Using shadowBrush As Brush = New LinearGradientBrush(upperShadowRectangle, Me.UpperShadowColor1, Me.UpperShadowColor2, LinearGradientMode.Vertical)
                g.FillRectangle(shadowBrush, upperShadowRectangle)
            End Using

            g.ResetClip()
        End Using
    End Sub

    ''' <summary> Renders the left toggle field. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="g">                     The Graphics to process. </param>
    ''' <param name="leftRectangle">         The left rectangle. </param>
    ''' <param name="totalToggleFieldWidth"> Width of the total toggle field. </param>
    Public Overrides Sub RenderLeftToggleField(ByVal g As Graphics, ByVal leftRectangle As Rectangle, ByVal totalToggleFieldWidth As Integer)
        If g Is Nothing Then Return
        g.SmoothingMode = SmoothingMode.HighQuality
        g.PixelOffsetMode = PixelOffsetMode.HighQuality
        g.InterpolationMode = InterpolationMode.HighQualityBilinear

        Dim innercontrolRectangle As New Rectangle(1, 1, Me.ToggleSwitch.Width - 1, Me.ToggleSwitch.Height - 2)

        Using innerControlPath As GraphicsPath = GetRectangleClipPath(innercontrolRectangle)
            g.SetClip(innerControlPath)

            'Draw image or text
            If Me.ToggleSwitch.OnSideImage IsNot Nothing OrElse (Not String.IsNullOrEmpty(Me.ToggleSwitch.OnText)) Then
                Dim fullRectangle As New RectangleF(leftRectangle.X + 2 - (totalToggleFieldWidth - leftRectangle.Width), 2, totalToggleFieldWidth - 2, Me.ToggleSwitch.Height - 4)

                g.IntersectClip(fullRectangle)

                If Me.ToggleSwitch.OnSideImage IsNot Nothing Then
                    Dim imageSize As Size = Me.ToggleSwitch.OnSideImage.Size
                    Dim imageRectangle As Rectangle

                    Dim imageXPos As Integer = CInt(Fix(fullRectangle.X))

                    If Me.ToggleSwitch.OnSideScaleImageToFit Then
                        Dim canvasSize As New Size(CInt(fullRectangle.Width), CInt(fullRectangle.Height))
                        Dim resizedImageSize As Size = ImageHelper.RescaleImageToFit(imageSize, canvasSize)

                        If Me.ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Center Then
                            imageXPos = CInt(Fix(CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(resizedImageSize.Width)) / 2)))
                        ElseIf Me.ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Near Then
                            imageXPos = CInt(Fix(CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(resizedImageSize.Width)))
                        End If

                        imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(resizedImageSize.Height)) / 2))), resizedImageSize.Width, resizedImageSize.Height)

                        If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
                            g.DrawImage(Me.ToggleSwitch.OnSideImage, imageRectangle, 0, 0, Me.ToggleSwitch.OnSideImage.Width, Me.ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                        Else
                            g.DrawImage(Me.ToggleSwitch.OnSideImage, imageRectangle)
                        End If
                    Else
                        If Me.ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Center Then
                            imageXPos = CInt(Fix(CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(imageSize.Width)) / 2)))
                        ElseIf Me.ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Near Then
                            imageXPos = CInt(Fix(CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(imageSize.Width)))
                        End If

                        imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(imageSize.Height)) / 2))), imageSize.Width, imageSize.Height)

                        If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
                            g.DrawImage(Me.ToggleSwitch.OnSideImage, imageRectangle, 0, 0, Me.ToggleSwitch.OnSideImage.Width, Me.ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                        Else
                            g.DrawImageUnscaled(Me.ToggleSwitch.OnSideImage, imageRectangle)
                        End If
                    End If
                ElseIf Not String.IsNullOrEmpty(Me.ToggleSwitch.OnText) Then
                    Dim textSize As SizeF = g.MeasureString(Me.ToggleSwitch.OnText, Me.ToggleSwitch.OnFont)

                    Dim textXPos As Single = fullRectangle.X

                    If Me.ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Center Then
                        textXPos = CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(textSize.Width)) / 2)
                    ElseIf Me.ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Near Then
                        textXPos = CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(textSize.Width)
                    End If

                    Dim textRectangle As New RectangleF(textXPos, CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(textSize.Height)) / 2), textSize.Width, textSize.Height)

                    Dim textForeColor As Color = Me.ToggleSwitch.OnForeColor

                    If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
                        textForeColor = textForeColor.ToGrayscale()
                    End If

                    Using textBrush As Brush = New SolidBrush(textForeColor)
                        g.DrawString(Me.ToggleSwitch.OnText, Me.ToggleSwitch.OnFont, textBrush, textRectangle)
                    End Using
                End If
            End If

            g.ResetClip()
        End Using
    End Sub

    ''' <summary> Renders the right toggle field. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="g">                     The Graphics to process. </param>
    ''' <param name="rightRectangle">        The right rectangle. </param>
    ''' <param name="totalToggleFieldWidth"> Width of the total toggle field. </param>
    Public Overrides Sub RenderRightToggleField(ByVal g As Graphics, ByVal rightRectangle As Rectangle, ByVal totalToggleFieldWidth As Integer)
        If g Is Nothing Then Return
        g.SmoothingMode = SmoothingMode.HighQuality
        g.PixelOffsetMode = PixelOffsetMode.HighQuality
        g.InterpolationMode = InterpolationMode.HighQualityBilinear

        Dim innercontrolRectangle As New Rectangle(1, 1, Me.ToggleSwitch.Width - 1, Me.ToggleSwitch.Height - 2)

        Using innerControlPath As GraphicsPath = GetRectangleClipPath(innercontrolRectangle)
            g.SetClip(innerControlPath)

            'Draw image or text
            If Me.ToggleSwitch.OffSideImage IsNot Nothing OrElse (Not String.IsNullOrEmpty(Me.ToggleSwitch.OffText)) Then
                Dim fullRectangle As New RectangleF(rightRectangle.X, 2, totalToggleFieldWidth - 2, Me.ToggleSwitch.Height - 4)

                g.IntersectClip(fullRectangle)

                If Me.ToggleSwitch.OffSideImage IsNot Nothing Then
                    Dim imageSize As Size = Me.ToggleSwitch.OffSideImage.Size
                    Dim imageRectangle As Rectangle

                    Dim imageXPos As Integer = CInt(Fix(fullRectangle.X))

                    If Me.ToggleSwitch.OffSideScaleImageToFit Then
                        Dim canvasSize As New Size(CInt(fullRectangle.Width), CInt(fullRectangle.Height))
                        Dim resizedImageSize As Size = ImageHelper.RescaleImageToFit(imageSize, canvasSize)

                        If Me.ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Center Then
                            imageXPos = CInt(Fix(CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(resizedImageSize.Width)) / 2)))
                        ElseIf Me.ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Far Then
                            imageXPos = CInt(Fix(CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(resizedImageSize.Width)))
                        End If

                        imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(resizedImageSize.Height)) / 2))), resizedImageSize.Width, resizedImageSize.Height)

                        If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
                            g.DrawImage(Me.ToggleSwitch.OnSideImage, imageRectangle, 0, 0, Me.ToggleSwitch.OnSideImage.Width, Me.ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                        Else
                            g.DrawImage(Me.ToggleSwitch.OnSideImage, imageRectangle)
                        End If
                    Else
                        If Me.ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Center Then
                            imageXPos = CInt(Fix(CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(imageSize.Width)) / 2)))
                        ElseIf Me.ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Far Then
                            imageXPos = CInt(Fix(CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(imageSize.Width)))
                        End If

                        imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(imageSize.Height)) / 2))), imageSize.Width, imageSize.Height)

                        If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
                            g.DrawImage(Me.ToggleSwitch.OnSideImage, imageRectangle, 0, 0, Me.ToggleSwitch.OnSideImage.Width, Me.ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                        Else
                            g.DrawImageUnscaled(Me.ToggleSwitch.OffSideImage, imageRectangle)
                        End If
                    End If
                ElseIf Not String.IsNullOrEmpty(Me.ToggleSwitch.OffText) Then
                    Dim textSize As SizeF = g.MeasureString(Me.ToggleSwitch.OffText, Me.ToggleSwitch.OffFont)

                    Dim textXPos As Single = fullRectangle.X

                    If Me.ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Center Then
                        textXPos = CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(textSize.Width)) / 2)
                    ElseIf Me.ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Far Then
                        textXPos = CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(textSize.Width)
                    End If

                    Dim textRectangle As New RectangleF(textXPos, CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(textSize.Height)) / 2), textSize.Width, textSize.Height)

                    Dim textForeColor As Color = Me.ToggleSwitch.OffForeColor

                    If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
                        textForeColor = textForeColor.ToGrayscale()
                    End If

                    Using textBrush As Brush = New SolidBrush(textForeColor)
                        g.DrawString(Me.ToggleSwitch.OffText, Me.ToggleSwitch.OffFont, textBrush, textRectangle)
                    End Using
                End If
            End If

            g.ResetClip()
        End Using
    End Sub

    ''' <summary> Renders the button. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="g">               The Graphics to process. </param>
    ''' <param name="buttonRectangle"> The button rectangle. </param>
    Public Overrides Sub RenderButton(ByVal g As Graphics, ByVal buttonRectangle As Rectangle)
        If g Is Nothing Then Return
        g.SmoothingMode = SmoothingMode.HighQuality
        g.PixelOffsetMode = PixelOffsetMode.HighQuality
        g.InterpolationMode = InterpolationMode.HighQualityBilinear

        g.SetClip(buttonRectangle)

        'Draw button surface
        Dim buttonSurfaceColor As Color = Me.ButtonNormalSurfaceColor

        If Me.ToggleSwitch.IsButtonPressed Then
            buttonSurfaceColor = Me.ButtonPressedSurfaceColor
        ElseIf Me.ToggleSwitch.IsButtonHovered Then
            buttonSurfaceColor = Me.ButtonHoverSurfaceColor
        End If

        If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
            buttonSurfaceColor = buttonSurfaceColor.ToGrayscale()
        End If

        Using buttonSurfaceBrush As Brush = New SolidBrush(buttonSurfaceColor)
            g.FillEllipse(buttonSurfaceBrush, buttonRectangle)
        End Using

        'Draw "metal" surface
        Dim centerPoint1 As New PointF(buttonRectangle.X + (buttonRectangle.Width / 2.0F), buttonRectangle.Y + 1.2F * (buttonRectangle.Height / 2.0F))

        Using firstMetalBrush As PathGradientBrush = GetBrush(New Color() {Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.FromArgb(255, 110, 110, 110), Color.Transparent, Color.Transparent, Color.Transparent}, buttonRectangle, centerPoint1)
            g.FillEllipse(firstMetalBrush, buttonRectangle)
        End Using

        Dim centerPoint2 As New PointF(buttonRectangle.X + 0.8F * (buttonRectangle.Width / 2.0F), buttonRectangle.Y + (buttonRectangle.Height / 2.0F))

        Using secondMetalBrush As PathGradientBrush = GetBrush(New Color() {Color.FromArgb(255, 110, 110, 110), Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.FromArgb(255, 110, 110, 110)}, buttonRectangle, centerPoint2)
            g.FillEllipse(secondMetalBrush, buttonRectangle)
        End Using

        Dim centerPoint3 As New PointF(buttonRectangle.X + 1.2F * (buttonRectangle.Width / 2.0F), buttonRectangle.Y + (buttonRectangle.Height / 2.0F))

        Using thirdMetalBrush As PathGradientBrush = GetBrush(New Color() {Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.FromArgb(255, 98, 98, 98), Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent}, buttonRectangle, centerPoint3)
            g.FillEllipse(thirdMetalBrush, buttonRectangle)
        End Using

        Dim centerPoint4 As New PointF(buttonRectangle.X + 0.9F * (buttonRectangle.Width / 2.0F), buttonRectangle.Y + 0.9F * (buttonRectangle.Height / 2.0F))

        Using fourthMetalBrush As PathGradientBrush = GetBrush(New Color() {Color.Transparent, Color.FromArgb(255, 188, 188, 188), Color.FromArgb(255, 110, 110, 110), Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent}, buttonRectangle, centerPoint4)
            g.FillEllipse(fourthMetalBrush, buttonRectangle)
        End Using

        'Draw button border
        Dim buttonBorderColor As Color = Me.ButtonNormalBorderColor

        If Me.ToggleSwitch.IsButtonPressed Then
            buttonBorderColor = Me.ButtonPressedBorderColor
        ElseIf Me.ToggleSwitch.IsButtonHovered Then
            buttonBorderColor = Me.ButtonHoverBorderColor
        End If

        If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
            buttonBorderColor = buttonBorderColor.ToGrayscale()
        End If

        Using buttonBorderPen As New Pen(buttonBorderColor)
            g.DrawEllipse(buttonBorderPen, buttonRectangle)
        End Using

        g.ResetClip()
    End Sub

#End Region ' Render Method Implementations

#Region "Helper Method Implementations"

    ''' <summary> Gets rectangle clip path. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="rect"> The rectangle. </param>
    ''' <returns> The rectangle clip path. </returns>
    Public Shared Function GetRectangleClipPath(ByVal rect As Rectangle) As GraphicsPath
        Dim borderPath As New GraphicsPath()
        borderPath.AddArc(rect.X, rect.Y, rect.Height, rect.Height, 90, 180)
        borderPath.AddArc(rect.Width - rect.Height, rect.Y, rect.Height, rect.Height, 270, 180)
        borderPath.CloseFigure()
        Return borderPath
    End Function

    ''' <summary> Gets button clip path. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> The button clip path. </returns>
    Public Function GetButtonClipPath() As GraphicsPath
        Dim buttonRectangle As Rectangle = Me.GetButtonRectangle()

        Dim buttonPath As New GraphicsPath()

        buttonPath.AddArc(buttonRectangle.X, buttonRectangle.Y, buttonRectangle.Height, buttonRectangle.Height, 0, 360)

        Return buttonPath
    End Function

    ''' <summary> Gets button width. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> The button width. </returns>
    Public Overrides Function GetButtonWidth() As Integer
        Return Me.ToggleSwitch.Height - 2
    End Function

    ''' <summary> Gets button rectangle. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> The button rectangle. </returns>
    Public Overrides Function GetButtonRectangle() As Rectangle
        Dim buttonWidth As Integer = Me.GetButtonWidth()
        Return Me.GetButtonRectangle(buttonWidth)
    End Function

    ''' <summary> Gets button rectangle. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="buttonWidth"> Width of the button. </param>
    ''' <returns> The button rectangle. </returns>
    Public Overrides Function GetButtonRectangle(ByVal buttonWidth As Integer) As Rectangle
        Dim buttonRect As New Rectangle(Me.ToggleSwitch.ButtonValue, 1, buttonWidth, buttonWidth)
        Return buttonRect
    End Function

    ''' <summary> Gets a brush. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="colors">      The colors. </param>
    ''' <param name="r">           A RectangleF to process. </param>
    ''' <param name="centerPoint"> The center point. </param>
    ''' <returns> The brush. </returns>
    Private Shared Function GetBrush(ByVal colors() As Color, ByVal r As RectangleF, ByVal centerPoint As PointF) As PathGradientBrush
        Dim i As Integer = colors.Length - 1
        Dim points(i) As PointF

        Dim a As Single = 0F
        Dim n As Integer = 0
        Dim cx As Single = r.Width / 2.0F
        Dim cy As Single = r.Height / 2.0F

        Dim w As Integer = CInt(Fix(Math.Floor(180.0 * (i - 2.0) / i / 2.0)))
        Dim wi As Double = w * Math.PI / 180.0
        Dim faktor As Double = 1.0 / Math.Sin(wi)

        Dim radx As Single = CSng(cx * faktor)
        Dim rady As Single = CSng(cy * faktor)

        Do While a <= Math.PI * 2
            points(n) = New PointF(CSng(cx + (Math.Cos(a) * radx)) + r.Left, CSng(cy + (Math.Sin(a) * rady)) + r.Top)
            n += 1
            a += CSng(Math.PI * 2 / i)
            ' a += CSng(Math.PI * 2 \ i)
        Loop

        points(i) = points(0)
        Dim graphicsPath As New GraphicsPath()
        graphicsPath.AddLines(points)

        Dim fBrush As New PathGradientBrush(graphicsPath) With {
            .CenterPoint = centerPoint,
            .CenterColor = Color.Transparent,
            .SurroundColors = New Color() {Color.White}}

        Try
            fBrush.SurroundColors = colors
        Catch ex As Exception
            Throw New InvalidOperationException("Too may colors!", ex)
        End Try

        Return fBrush
    End Function

#End Region ' Helper Method Implementations
End Class
