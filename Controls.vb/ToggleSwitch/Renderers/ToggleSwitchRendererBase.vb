Imports isr.Core.ColorExtensions

''' <summary> A toggle switch renderer base. </summary>
''' <remarks>
''' (c) 2015 Johnny J.. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 12/4/2015 </para>
''' </remarks>
Public MustInherit Class ToggleSwitchRendererBase

#Region "Private Members"

    ''' <summary> The toggle switch. </summary>
    Private _ToggleSwitch As ToggleSwitch

#End Region ' Private Members

#Region "Constructor"

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Protected Sub New()
    End Sub

    ''' <summary> Sets toggle switch. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="toggleSwitch"> The toggle switch. </param>
    Friend Sub SetToggleSwitch(ByVal toggleSwitch As ToggleSwitch)
        Me._ToggleSwitch = toggleSwitch
    End Sub

    ''' <summary> Gets the toggle switch. </summary>
    ''' <value> The toggle switch. </value>
    Friend ReadOnly Property ToggleSwitch() As ToggleSwitch
        Get
            Return Me._ToggleSwitch
        End Get
    End Property

#End Region ' Constructor

#Region "Render Methods"

    ''' <summary> Renders the background described by e. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> Paint event information. </param>
    Public Sub RenderBackground(ByVal e As System.Windows.Forms.PaintEventArgs)
        If e Is Nothing Then Return
        If Me._ToggleSwitch Is Nothing Then Return

        e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias

        Dim controlRectangle As New Rectangle(0, 0, Me._ToggleSwitch.Width, Me._ToggleSwitch.Height)

        Me.FillBackground(e.Graphics, controlRectangle)
        Me.RenderBorder(e.Graphics, controlRectangle)
    End Sub

    ''' <summary> Renders the control described by e. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> Paint event information. </param>
    Public Sub RenderControl(ByVal e As System.Windows.Forms.PaintEventArgs)
        If e Is Nothing Then Return
        If Me._ToggleSwitch Is Nothing Then Return

        e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias

        Dim buttonRectangle As Rectangle = Me.GetButtonRectangle()
        Dim totalToggleFieldWidth As Integer = Me.ToggleSwitch.Width - buttonRectangle.Width

        If buttonRectangle.X > 0 Then
            Dim leftRectangle As New Rectangle(0, 0, buttonRectangle.X, Me.ToggleSwitch.Height)

            If leftRectangle.Width > 0 Then
                Me.RenderLeftToggleField(e.Graphics, leftRectangle, totalToggleFieldWidth)
            End If
        End If

        If buttonRectangle.X + buttonRectangle.Width < e.ClipRectangle.Width Then
            Dim rightRectangle As New Rectangle(buttonRectangle.X + buttonRectangle.Width, 0, Me.ToggleSwitch.Width - buttonRectangle.X - buttonRectangle.Width, Me.ToggleSwitch.Height)

            If rightRectangle.Width > 0 Then
                Me.RenderRightToggleField(e.Graphics, rightRectangle, totalToggleFieldWidth)
            End If
        End If

        Me.RenderButton(e.Graphics, buttonRectangle)
    End Sub

    ''' <summary> Fill background. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="g">                The Graphics to process. </param>
    ''' <param name="controlRectangle"> The control rectangle. </param>
    Public Sub FillBackground(ByVal g As Graphics, ByVal controlRectangle As Rectangle)
        If g Is Nothing Then Return
        Dim backColor As Color = If((Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled,
                                     Me.ToggleSwitch.BackColor.ToGrayscale(),
                                     Me.ToggleSwitch.BackColor)

        Using backBrush As Brush = New SolidBrush(backColor)
            g.FillRectangle(backBrush, controlRectangle)
        End Using
    End Sub

    ''' <summary> Renders the border. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="g">               The Graphics to process. </param>
    ''' <param name="borderRectangle"> The border rectangle. </param>
    Public MustOverride Sub RenderBorder(ByVal g As Graphics, ByVal borderRectangle As Rectangle)

    ''' <summary> Renders the left toggle field. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="g">                     The Graphics to process. </param>
    ''' <param name="leftRectangle">         The left rectangle. </param>
    ''' <param name="totalToggleFieldWidth"> Width of the total toggle field. </param>
    Public MustOverride Sub RenderLeftToggleField(ByVal g As Graphics, ByVal leftRectangle As Rectangle, ByVal totalToggleFieldWidth As Integer)

    ''' <summary> Renders the right toggle field. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="g">                     The Graphics to process. </param>
    ''' <param name="rightRectangle">        The right rectangle. </param>
    ''' <param name="totalToggleFieldWidth"> Width of the total toggle field. </param>
    Public MustOverride Sub RenderRightToggleField(ByVal g As Graphics, ByVal rightRectangle As Rectangle, ByVal totalToggleFieldWidth As Integer)

    ''' <summary> Renders the button. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="g">               The Graphics to process. </param>
    ''' <param name="buttonRectangle"> The button rectangle. </param>
    Public MustOverride Sub RenderButton(ByVal g As Graphics, ByVal buttonRectangle As Rectangle)

#End Region ' Render Methods

#Region "Helper Methods"

    ''' <summary> Gets button width. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> The button width. </returns>
    Public MustOverride Function GetButtonWidth() As Integer

    ''' <summary> Gets button rectangle. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> The button rectangle. </returns>
    Public MustOverride Function GetButtonRectangle() As Rectangle

    ''' <summary> Gets button rectangle. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="buttonWidth"> Width of the button. </param>
    ''' <returns> The button rectangle. </returns>
    Public MustOverride Function GetButtonRectangle(ByVal buttonWidth As Integer) As Rectangle

#End Region ' Helper Methods
End Class

