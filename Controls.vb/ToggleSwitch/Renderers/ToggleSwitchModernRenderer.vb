Imports System.Drawing.Drawing2D

Imports isr.Core.ColorExtensions

''' <summary> A toggle switch modern renderer. </summary>
''' <remarks>
''' (c) 2015 Johnny J.. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 12/4/2015 </para>
''' </remarks>
Public Class ToggleSwitchModernRenderer
    Inherits ToggleSwitchRendererBase
    Implements IDisposable

#Region "Constructor"

    ''' <summary> Full pathname of the inner control file. </summary>
    Private _InnerControlPath As GraphicsPath = Nothing

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        Me.OuterBorderColor = Color.FromArgb(255, 31, 31, 31)
        Me.InnerBorderColor1 = Color.FromArgb(255, 80, 80, 82)
        Me.InnerBorderColor2 = Color.FromArgb(255, 109, 110, 112)
        Me.LeftSideBackColor1 = Color.FromArgb(255, 57, 166, 222)
        Me.LeftSideBackColor2 = Color.FromArgb(255, 53, 155, 229)
        Me.RightSideBackColor1 = Color.FromArgb(255, 48, 49, 45)
        Me.RightSideBackColor2 = Color.FromArgb(255, 51, 52, 48)
        Me.ButtonNormalBorderColor1 = Color.FromArgb(255, 31, 31, 31)
        Me.ButtonNormalBorderColor2 = Color.FromArgb(255, 31, 31, 31)
        Me.ButtonNormalSurfaceColor1 = Color.FromArgb(255, 51, 52, 48)
        Me.ButtonNormalSurfaceColor2 = Color.FromArgb(255, 51, 52, 48)
        Me.ArrowNormalColor = Color.FromArgb(255, 53, 156, 230)
        Me.ButtonHoverBorderColor1 = Color.FromArgb(255, 29, 29, 29)
        Me.ButtonHoverBorderColor2 = Color.FromArgb(255, 29, 29, 29)
        Me.ButtonHoverSurfaceColor1 = Color.FromArgb(255, 48, 49, 45)
        Me.ButtonHoverSurfaceColor2 = Color.FromArgb(255, 48, 49, 45)
        Me.ArrowHoverColor = Color.FromArgb(255, 50, 148, 219)
        Me.ButtonPressedBorderColor1 = Color.FromArgb(255, 23, 23, 23)
        Me.ButtonPressedBorderColor2 = Color.FromArgb(255, 23, 23, 23)
        Me.ButtonPressedSurfaceColor1 = Color.FromArgb(255, 38, 39, 36)
        Me.ButtonPressedSurfaceColor2 = Color.FromArgb(255, 38, 39, 36)
        Me.ArrowPressedColor = Color.FromArgb(255, 39, 117, 172)
        Me.ButtonShadowColor1 = Color.FromArgb(50, 0, 0, 0)
        Me.ButtonShadowColor2 = Color.FromArgb(0, 0, 0, 0)

        Me.ButtonShadowWidth = 7
        Me.CornerRadius = 6
        Me.ButtonCornerRadius = 6
    End Sub

#Region " Disposable Support "

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Me.Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived
    ''' class provided proper implementation.
    ''' </summary>
    ''' <value> <c>True</c> if disposed; otherwise, <c>False</c>. </value>
    Protected Property IsDisposed() As Boolean

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me._InnerControlPath?.Dispose() : Me._InnerControlPath = Nothing
            End If
        Finally
            Me.IsDisposed = True
        End Try
    End Sub

#End Region

#End Region ' Constructor

#Region "Public Properties"

    ''' <summary> Gets or sets the color of the outer border. </summary>
    ''' <value> The color of the outer border. </value>
    Public Property OuterBorderColor() As Color

    ''' <summary> Gets or sets the inner border color 1. </summary>
    ''' <value> The inner border color 1. </value>
    Public Property InnerBorderColor1() As Color

    ''' <summary> Gets or sets the inner border color 2. </summary>
    ''' <value> The inner border color 2. </value>
    Public Property InnerBorderColor2() As Color

    ''' <summary> Gets or sets the left side back color 1. </summary>
    ''' <value> The left side back color 1. </value>
    Public Property LeftSideBackColor1() As Color

    ''' <summary> Gets or sets the left side back color 2. </summary>
    ''' <value> The left side back color 2. </value>
    Public Property LeftSideBackColor2() As Color

    ''' <summary> Gets or sets the right side back color 1. </summary>
    ''' <value> The right side back color 1. </value>
    Public Property RightSideBackColor1() As Color

    ''' <summary> Gets or sets the right side back color 2. </summary>
    ''' <value> The right side back color 2. </value>
    Public Property RightSideBackColor2() As Color

    ''' <summary> Gets or sets the button normal border color 1. </summary>
    ''' <value> The button normal border color 1. </value>
    Public Property ButtonNormalBorderColor1() As Color

    ''' <summary> Gets or sets the button normal border color 2. </summary>
    ''' <value> The button normal border color 2. </value>
    Public Property ButtonNormalBorderColor2() As Color

    ''' <summary> Gets or sets the button normal surface color 1. </summary>
    ''' <value> The button normal surface color 1. </value>
    Public Property ButtonNormalSurfaceColor1() As Color

    ''' <summary> Gets or sets the button normal surface color 2. </summary>
    ''' <value> The button normal surface color 2. </value>
    Public Property ButtonNormalSurfaceColor2() As Color

    ''' <summary> Gets or sets the color of the arrow normal. </summary>
    ''' <value> The color of the arrow normal. </value>
    Public Property ArrowNormalColor() As Color

    ''' <summary> Gets or sets the button hover border color 1. </summary>
    ''' <value> The button hover border color 1. </value>
    Public Property ButtonHoverBorderColor1() As Color

    ''' <summary> Gets or sets the button hover border color 2. </summary>
    ''' <value> The button hover border color 2. </value>
    Public Property ButtonHoverBorderColor2() As Color

    ''' <summary> Gets or sets the button hover surface color 1. </summary>
    ''' <value> The button hover surface color 1. </value>
    Public Property ButtonHoverSurfaceColor1() As Color

    ''' <summary> Gets or sets the button hover surface color 2. </summary>
    ''' <value> The button hover surface color 2. </value>
    Public Property ButtonHoverSurfaceColor2() As Color

    ''' <summary> Gets or sets the color of the arrow hover. </summary>
    ''' <value> The color of the arrow hover. </value>
    Public Property ArrowHoverColor() As Color

    ''' <summary> Gets or sets the button pressed border color 1. </summary>
    ''' <value> The button pressed border color 1. </value>
    Public Property ButtonPressedBorderColor1() As Color

    ''' <summary> Gets or sets the button pressed border color 2. </summary>
    ''' <value> The button pressed border color 2. </value>
    Public Property ButtonPressedBorderColor2() As Color

    ''' <summary> Gets or sets the button pressed surface color 1. </summary>
    ''' <value> The button pressed surface color 1. </value>
    Public Property ButtonPressedSurfaceColor1() As Color

    ''' <summary> Gets or sets the button pressed surface color 2. </summary>
    ''' <value> The button pressed surface color 2. </value>
    Public Property ButtonPressedSurfaceColor2() As Color

    ''' <summary> Gets or sets the color of the arrow pressed. </summary>
    ''' <value> The color of the arrow pressed. </value>
    Public Property ArrowPressedColor() As Color

    ''' <summary> Gets or sets the button shadow color 1. </summary>
    ''' <value> The button shadow color 1. </value>
    Public Property ButtonShadowColor1() As Color

    ''' <summary> Gets or sets the button shadow color 2. </summary>
    ''' <value> The button shadow color 2. </value>
    Public Property ButtonShadowColor2() As Color

    ''' <summary> Gets or sets the width of the button shadow. </summary>
    ''' <value> The width of the button shadow. </value>
    Public Property ButtonShadowWidth() As Integer

    ''' <summary> Gets or sets the corner radius. </summary>
    ''' <value> The corner radius. </value>
    Public Property CornerRadius() As Integer

    ''' <summary> Gets or sets the button corner radius. </summary>
    ''' <value> The button corner radius. </value>
    Public Property ButtonCornerRadius() As Integer

#End Region ' Public Properties

#Region "Render Method Implementations"

    ''' <summary> Renders the border. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="g">               The Graphics to process. </param>
    ''' <param name="borderRectangle"> The border rectangle. </param>
    Public Overrides Sub RenderBorder(ByVal g As Graphics, ByVal borderRectangle As Rectangle)
        If g Is Nothing Then Return
        g.SmoothingMode = SmoothingMode.HighQuality
        g.PixelOffsetMode = PixelOffsetMode.HighQuality
        g.InterpolationMode = InterpolationMode.HighQualityBilinear

        'Draw outer border
        Using outerBorderPath As GraphicsPath = Me.GetRoundedRectanglePath(borderRectangle, Me.CornerRadius)
            g.SetClip(outerBorderPath)

            'INSTANT VB NOTE: The variable outerBorderColor was renamed since Visual Basic does not handle local variables named the same as class members well:
            Dim outerBorderColor_Renamed As Color = If((Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled, Me.OuterBorderColor.ToGrayscale(), Me.OuterBorderColor)

            Using outerBorderBrush As Brush = New SolidBrush(outerBorderColor_Renamed)
                g.FillPath(outerBorderBrush, outerBorderPath)
            End Using

            g.ResetClip()
        End Using

        'Draw inner border
        Dim innerborderRectangle As New Rectangle(borderRectangle.X + 1, borderRectangle.Y + 1, borderRectangle.Width - 2, borderRectangle.Height - 2)

        Using innerBorderPath As GraphicsPath = Me.GetRoundedRectanglePath(innerborderRectangle, Me.CornerRadius)
            g.SetClip(innerBorderPath)

            Dim borderColor1 As Color = If((Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled, Me.InnerBorderColor1.ToGrayscale(), Me.InnerBorderColor1)
            Dim borderColor2 As Color = If((Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled, Me.InnerBorderColor2.ToGrayscale(), Me.InnerBorderColor2)

            Using borderBrush As Brush = New LinearGradientBrush(borderRectangle, borderColor1, borderColor2, LinearGradientMode.Vertical)
                g.FillPath(borderBrush, innerBorderPath)
            End Using

            g.ResetClip()
        End Using

        Dim backgroundRectangle As New Rectangle(borderRectangle.X + 2, borderRectangle.Y + 2, borderRectangle.Width - 4, borderRectangle.Height - 4)
        Me._InnerControlPath = Me.GetRoundedRectanglePath(backgroundRectangle, Me.CornerRadius)
    End Sub

    ''' <summary> Renders the left toggle field. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="g">                     The Graphics to process. </param>
    ''' <param name="leftRectangle">         The left rectangle. </param>
    ''' <param name="totalToggleFieldWidth"> Width of the total toggle field. </param>
    Public Overrides Sub RenderLeftToggleField(ByVal g As Graphics, ByVal leftRectangle As Rectangle, ByVal totalToggleFieldWidth As Integer)
        If g Is Nothing Then Return
        g.SmoothingMode = SmoothingMode.HighQuality
        g.PixelOffsetMode = PixelOffsetMode.HighQuality
        g.InterpolationMode = InterpolationMode.HighQualityBilinear

        Dim buttonWidth As Integer = Me.GetButtonWidth()

        'Draw inner background
        Dim gradientRectWidth As Integer = leftRectangle.Width + buttonWidth \ 2
        Dim gradientRectangle As New Rectangle(leftRectangle.X, leftRectangle.Y, gradientRectWidth, leftRectangle.Height)

        'INSTANT VB NOTE: The variable leftSideBackColor1 was renamed since Visual Basic does not handle local variables named the same as class members well:
        Dim leftSideBackColor1_Renamed As Color = If((Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled, Me.LeftSideBackColor1.ToGrayscale(), Me.LeftSideBackColor1)
        'INSTANT VB NOTE: The variable leftSideBackColor2 was renamed since Visual Basic does not handle local variables named the same as class members well:
        Dim leftSideBackColor2_Renamed As Color = If((Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled, Me.LeftSideBackColor2.ToGrayscale(), Me.LeftSideBackColor2)

        If Me._InnerControlPath IsNot Nothing Then
            g.SetClip(Me._InnerControlPath)
            g.IntersectClip(gradientRectangle)
        Else
            g.SetClip(gradientRectangle)
        End If

        Using backgroundBrush As Brush = New LinearGradientBrush(gradientRectangle, leftSideBackColor1_Renamed, leftSideBackColor2_Renamed, LinearGradientMode.Vertical)
            g.FillRectangle(backgroundBrush, gradientRectangle)
        End Using

        g.ResetClip()

        Dim leftShadowRectangle As New Rectangle() With {
            .X = leftRectangle.X + leftRectangle.Width - Me.ButtonShadowWidth,
            .Y = leftRectangle.Y,
            .Width = Me.ButtonShadowWidth + Me.CornerRadius,
            .Height = leftRectangle.Height}

        If Me._InnerControlPath IsNot Nothing Then
            g.SetClip(Me._InnerControlPath)
            g.IntersectClip(leftShadowRectangle)
        Else
            g.SetClip(leftShadowRectangle)
        End If

        Using buttonShadowBrush As Brush = New LinearGradientBrush(leftShadowRectangle, Me.ButtonShadowColor2, Me.ButtonShadowColor1, LinearGradientMode.Horizontal)
            g.FillRectangle(buttonShadowBrush, leftShadowRectangle)
        End Using

        g.ResetClip()

        'Draw image or text
        If Me.ToggleSwitch.OnSideImage IsNot Nothing OrElse (Not String.IsNullOrEmpty(Me.ToggleSwitch.OnText)) Then
            Dim fullRectangle As New RectangleF(leftRectangle.X + 1 - (totalToggleFieldWidth - leftRectangle.Width), 1, totalToggleFieldWidth - 1, Me.ToggleSwitch.Height - 2)

            If Me._InnerControlPath IsNot Nothing Then
                g.SetClip(Me._InnerControlPath)
                g.IntersectClip(fullRectangle)
            Else
                g.SetClip(fullRectangle)
            End If

            If Me.ToggleSwitch.OnSideImage IsNot Nothing Then
                Dim imageSize As Size = Me.ToggleSwitch.OnSideImage.Size
                Dim imageRectangle As Rectangle

                Dim imageXPos As Integer = CInt(Fix(fullRectangle.X))

                If Me.ToggleSwitch.OnSideScaleImageToFit Then
                    Dim canvasSize As New Size(CInt(fullRectangle.Width), CInt(fullRectangle.Height))
                    Dim resizedImageSize As Size = ImageHelper.RescaleImageToFit(imageSize, canvasSize)

                    If Me.ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Center Then
                        imageXPos = CInt(Fix(CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(resizedImageSize.Width)) / 2)))
                    ElseIf Me.ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Near Then
                        imageXPos = CInt(Fix(CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(resizedImageSize.Width)))
                    End If

                    imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(resizedImageSize.Height)) / 2))), resizedImageSize.Width, resizedImageSize.Height)

                    If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
                        g.DrawImage(Me.ToggleSwitch.OnSideImage, imageRectangle, 0, 0, Me.ToggleSwitch.OnSideImage.Width, Me.ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                    Else
                        g.DrawImage(Me.ToggleSwitch.OnSideImage, imageRectangle)
                    End If
                Else
                    If Me.ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Center Then
                        imageXPos = CInt(Fix(CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(imageSize.Width)) / 2)))
                    ElseIf Me.ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Near Then
                        imageXPos = CInt(Fix(CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(imageSize.Width)))
                    End If

                    imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(imageSize.Height)) / 2))), imageSize.Width, imageSize.Height)

                    If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
                        g.DrawImage(Me.ToggleSwitch.OnSideImage, imageRectangle, 0, 0, Me.ToggleSwitch.OnSideImage.Width, Me.ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                    Else
                        g.DrawImageUnscaled(Me.ToggleSwitch.OnSideImage, imageRectangle)
                    End If
                End If
            ElseIf Not String.IsNullOrEmpty(Me.ToggleSwitch.OnText) Then
                Dim textSize As SizeF = g.MeasureString(Me.ToggleSwitch.OnText, Me.ToggleSwitch.OnFont)

                Dim textXPos As Single = fullRectangle.X

                If Me.ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Center Then
                    textXPos = CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(textSize.Width)) / 2)
                ElseIf Me.ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Near Then
                    textXPos = CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(textSize.Width)
                End If

                Dim textRectangle As New RectangleF(textXPos, CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(textSize.Height)) / 2), textSize.Width, textSize.Height)

                Dim textForeColor As Color = Me.ToggleSwitch.OnForeColor

                If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
                    textForeColor = textForeColor.ToGrayscale()
                End If

                Using textBrush As Brush = New SolidBrush(textForeColor)
                    g.DrawString(Me.ToggleSwitch.OnText, Me.ToggleSwitch.OnFont, textBrush, textRectangle)
                End Using
            End If

            g.ResetClip()
        End If
    End Sub

    ''' <summary> Renders the right toggle field. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="g">                     The Graphics to process. </param>
    ''' <param name="rightRectangle">        The right rectangle. </param>
    ''' <param name="totalToggleFieldWidth"> Width of the total toggle field. </param>
    Public Overrides Sub RenderRightToggleField(ByVal g As Graphics, ByVal rightRectangle As Rectangle, ByVal totalToggleFieldWidth As Integer)
        If g Is Nothing Then Return
        g.SmoothingMode = SmoothingMode.HighQuality
        g.PixelOffsetMode = PixelOffsetMode.HighQuality
        g.InterpolationMode = InterpolationMode.HighQualityBilinear

        Dim buttonWidth As Integer = Me.GetButtonWidth()

        'Draw inner background
        Dim gradientRectWidth As Integer = rightRectangle.Width + buttonWidth \ 2
        Dim gradientRectangle As New Rectangle(rightRectangle.X - buttonWidth \ 2, rightRectangle.Y, gradientRectWidth, rightRectangle.Height)

        'INSTANT VB NOTE: The variable rightSideBackColor1 was renamed since Visual Basic does not handle local variables named the same as class members well:
        Dim rightSideBackColor1_Renamed As Color = If((Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled, Me.RightSideBackColor1.ToGrayscale(), Me.RightSideBackColor1)
        'INSTANT VB NOTE: The variable rightSideBackColor2 was renamed since Visual Basic does not handle local variables named the same as class members well:
        Dim rightSideBackColor2_Renamed As Color = If((Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled, Me.RightSideBackColor2.ToGrayscale(), Me.RightSideBackColor2)

        If Me._InnerControlPath IsNot Nothing Then
            g.SetClip(Me._InnerControlPath)
            g.IntersectClip(gradientRectangle)
        Else
            g.SetClip(gradientRectangle)
        End If

        Using backgroundBrush As Brush = New LinearGradientBrush(gradientRectangle, rightSideBackColor1_Renamed, rightSideBackColor2_Renamed, LinearGradientMode.Vertical)
            g.FillRectangle(backgroundBrush, gradientRectangle)
        End Using

        g.ResetClip()

        Dim rightShadowRectangle As New Rectangle() With {
            .X = rightRectangle.X - Me.CornerRadius,
            .Y = rightRectangle.Y,
            .Width = Me.ButtonShadowWidth + Me.CornerRadius,
            .Height = rightRectangle.Height}

        If Me._InnerControlPath IsNot Nothing Then
            g.SetClip(Me._InnerControlPath)
            g.IntersectClip(rightShadowRectangle)
        Else
            g.SetClip(rightShadowRectangle)
        End If

        Using buttonShadowBrush As Brush = New LinearGradientBrush(rightShadowRectangle, Me.ButtonShadowColor1, Me.ButtonShadowColor2, LinearGradientMode.Horizontal)
            g.FillRectangle(buttonShadowBrush, rightShadowRectangle)
        End Using

        g.ResetClip()

        'Draw image or text
        If Me.ToggleSwitch.OffSideImage IsNot Nothing OrElse (Not String.IsNullOrEmpty(Me.ToggleSwitch.OffText)) Then
            Dim fullRectangle As New RectangleF(rightRectangle.X, 1, totalToggleFieldWidth - 1, Me.ToggleSwitch.Height - 2)

            If Me._InnerControlPath IsNot Nothing Then
                g.SetClip(Me._InnerControlPath)
                g.IntersectClip(fullRectangle)
            Else
                g.SetClip(fullRectangle)
            End If

            If Me.ToggleSwitch.OffSideImage IsNot Nothing Then
                Dim imageSize As Size = Me.ToggleSwitch.OffSideImage.Size
                Dim imageRectangle As Rectangle

                Dim imageXPos As Integer = CInt(Fix(fullRectangle.X))

                If Me.ToggleSwitch.OffSideScaleImageToFit Then
                    Dim canvasSize As New Size(CInt(fullRectangle.Width), CInt(fullRectangle.Height))
                    Dim resizedImageSize As Size = ImageHelper.RescaleImageToFit(imageSize, canvasSize)

                    If Me.ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Center Then
                        imageXPos = CInt(Fix(CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(resizedImageSize.Width)) / 2)))
                    ElseIf Me.ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Far Then
                        imageXPos = CInt(Fix(CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(resizedImageSize.Width)))
                    End If

                    imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(resizedImageSize.Height)) / 2))), resizedImageSize.Width, resizedImageSize.Height)

                    If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
                        g.DrawImage(Me.ToggleSwitch.OnSideImage, imageRectangle, 0, 0, Me.ToggleSwitch.OnSideImage.Width, Me.ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                    Else
                        g.DrawImage(Me.ToggleSwitch.OnSideImage, imageRectangle)
                    End If
                Else
                    If Me.ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Center Then
                        imageXPos = CInt(Fix(CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(imageSize.Width)) / 2)))
                    ElseIf Me.ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Far Then
                        imageXPos = CInt(Fix(CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(imageSize.Width)))
                    End If

                    imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(imageSize.Height)) / 2))), imageSize.Width, imageSize.Height)

                    If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
                        g.DrawImage(Me.ToggleSwitch.OnSideImage, imageRectangle, 0, 0, Me.ToggleSwitch.OnSideImage.Width, Me.ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                    Else
                        g.DrawImageUnscaled(Me.ToggleSwitch.OffSideImage, imageRectangle)
                    End If
                End If
            ElseIf Not String.IsNullOrEmpty(Me.ToggleSwitch.OffText) Then
                Dim textSize As SizeF = g.MeasureString(Me.ToggleSwitch.OffText, Me.ToggleSwitch.OffFont)

                Dim textXPos As Single = fullRectangle.X

                If Me.ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Center Then
                    textXPos = CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(textSize.Width)) / 2)
                ElseIf Me.ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Far Then
                    textXPos = CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(textSize.Width)
                End If

                Dim textRectangle As New RectangleF(textXPos, CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(textSize.Height)) / 2), textSize.Width, textSize.Height)

                Dim textForeColor As Color = Me.ToggleSwitch.OffForeColor

                If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
                    textForeColor = textForeColor.ToGrayscale()
                End If

                Using textBrush As Brush = New SolidBrush(textForeColor)
                    g.DrawString(Me.ToggleSwitch.OffText, Me.ToggleSwitch.OffFont, textBrush, textRectangle)
                End Using
            End If

            g.ResetClip()
        End If
    End Sub

    ''' <summary> Renders the button. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="g">               The Graphics to process. </param>
    ''' <param name="buttonRectangle"> The button rectangle. </param>
    Public Overrides Sub RenderButton(ByVal g As Graphics, ByVal buttonRectangle As Rectangle)
        If g Is Nothing Then Return
        g.SmoothingMode = SmoothingMode.HighQuality
        g.PixelOffsetMode = PixelOffsetMode.HighQuality
        g.InterpolationMode = InterpolationMode.HighQualityBilinear

        If Me._InnerControlPath IsNot Nothing Then
            g.SetClip(Me._InnerControlPath)
            g.IntersectClip(buttonRectangle)
        Else
            g.SetClip(buttonRectangle)
        End If

        Using buttonPath As GraphicsPath = Me.GetRoundedRectanglePath(buttonRectangle, Me.ButtonCornerRadius)
            'Draw button surface
            Dim buttonSurfaceColor1 As Color = Me.ButtonNormalSurfaceColor1
            Dim buttonSurfaceColor2 As Color = Me.ButtonNormalSurfaceColor2

            If Me.ToggleSwitch.IsButtonPressed Then
                buttonSurfaceColor1 = Me.ButtonPressedSurfaceColor1
                buttonSurfaceColor2 = Me.ButtonPressedSurfaceColor2
            ElseIf Me.ToggleSwitch.IsButtonHovered Then
                buttonSurfaceColor1 = Me.ButtonHoverSurfaceColor1
                buttonSurfaceColor2 = Me.ButtonHoverSurfaceColor2
            End If

            If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
                buttonSurfaceColor1 = buttonSurfaceColor1.ToGrayscale()
                buttonSurfaceColor2 = buttonSurfaceColor2.ToGrayscale()
            End If

            Using buttonSurfaceBrush As Brush = New LinearGradientBrush(buttonRectangle, buttonSurfaceColor1, buttonSurfaceColor2, LinearGradientMode.Vertical)
                g.FillPath(buttonSurfaceBrush, buttonPath)
            End Using

            'Draw button border
            Dim buttonBorderColor1 As Color = Me.ButtonNormalBorderColor1
            Dim buttonBorderColor2 As Color = Me.ButtonNormalBorderColor2

            If Me.ToggleSwitch.IsButtonPressed Then
                buttonBorderColor1 = Me.ButtonPressedBorderColor1
                buttonBorderColor2 = Me.ButtonPressedBorderColor2
            ElseIf Me.ToggleSwitch.IsButtonHovered Then
                buttonBorderColor1 = Me.ButtonHoverBorderColor1
                buttonBorderColor2 = Me.ButtonHoverBorderColor2
            End If

            If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
                buttonBorderColor1 = buttonBorderColor1.ToGrayscale()
                buttonBorderColor2 = buttonBorderColor2.ToGrayscale()
            End If

            Using buttonBorderBrush As Brush = New LinearGradientBrush(buttonRectangle, buttonBorderColor1, buttonBorderColor2, LinearGradientMode.Vertical)
                Using buttonBorderPen As New Pen(buttonBorderBrush)
                    g.DrawPath(buttonBorderPen, buttonPath)
                End Using
            End Using
        End Using

        g.ResetClip()

        'Draw button arrows
        Dim arrowColor As Color = Me.ArrowNormalColor

        If Me.ToggleSwitch.IsButtonPressed Then
            arrowColor = Me.ArrowPressedColor
        ElseIf Me.ToggleSwitch.IsButtonHovered Then
            arrowColor = Me.ArrowHoverColor
        End If

        If (Not Me.ToggleSwitch.Enabled) AndAlso Me.ToggleSwitch.GrayWhenDisabled Then
            arrowColor = arrowColor.ToGrayscale()
        End If

        Dim arrowRectangle As New Rectangle() With {
            .Height = 9,
            .Width = 22,
            .X = buttonRectangle.X + CInt((CDbl(buttonRectangle.Width) - CDbl(arrowRectangle.Width)) / 2),
            .Y = buttonRectangle.Y + CInt((CDbl(buttonRectangle.Height) - CDbl(arrowRectangle.Height)) / 2)}

        Using arrowBrush As Brush = New SolidBrush(arrowColor)
            Using arrowLeftPath As GraphicsPath = GetArrowLeftPath(arrowRectangle)
                g.FillPath(arrowBrush, arrowLeftPath)
            End Using

            Using arrowRightPath As GraphicsPath = GetArrowRightPath(arrowRectangle)
                g.FillPath(arrowBrush, arrowRightPath)
            End Using
        End Using
    End Sub

#End Region ' Render Method Implementations

#Region "Helper Method Implementations"

    ''' <summary> Gets rounded rectangle path. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="rectangle"> The rectangle. </param>
    ''' <param name="radius">    The radius. </param>
    ''' <returns> The rounded rectangle path. </returns>
    Public Function GetRoundedRectanglePath(ByVal rectangle As Rectangle, ByVal radius As Integer) As GraphicsPath
        Dim gp As New GraphicsPath()
        Dim diameter As Integer = 2 * radius

        If diameter > Me.ToggleSwitch.Height Then
            diameter = Me.ToggleSwitch.Height
        End If

        If diameter > Me.ToggleSwitch.Width Then
            diameter = Me.ToggleSwitch.Width
        End If

        gp.AddArc(rectangle.X, rectangle.Y, diameter, diameter, 180, 90)
        gp.AddArc(rectangle.X + rectangle.Width - diameter, rectangle.Y, diameter, diameter, 270, 90)
        gp.AddArc(rectangle.X + rectangle.Width - diameter, rectangle.Y + rectangle.Height - diameter, diameter, diameter, 0, 90)
        gp.AddArc(rectangle.X, rectangle.Y + rectangle.Height - diameter, diameter, diameter, 90, 90)
        gp.CloseFigure()

        Return gp
    End Function

    ''' <summary> Gets arrow left path. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="arrowRectangle"> The arrow rectangle. </param>
    ''' <returns> The arrow left path. </returns>
    Public Shared Function GetArrowLeftPath(ByVal arrowRectangle As Rectangle) As GraphicsPath
        Dim gp As New GraphicsPath()

        Dim top As New Point(arrowRectangle.X + 8, arrowRectangle.Y)
        Dim bottom As New Point(arrowRectangle.X + 8, arrowRectangle.Y + arrowRectangle.Height)
        Dim tip As New Point(arrowRectangle.X, arrowRectangle.Y + (arrowRectangle.Height \ 2))

        gp.AddLine(top, bottom)
        gp.AddLine(bottom, tip)
        gp.AddLine(tip, top)

        Return gp
    End Function

    ''' <summary> Gets arrow right path. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="arrowRectangle"> The arrow rectangle. </param>
    ''' <returns> The arrow right path. </returns>
    Public Shared Function GetArrowRightPath(ByVal arrowRectangle As Rectangle) As GraphicsPath
        Dim gp As New GraphicsPath()

        Dim top As New Point(arrowRectangle.X + 14, arrowRectangle.Y)
        Dim bottom As New Point(arrowRectangle.X + 14, arrowRectangle.Y + arrowRectangle.Height)
        Dim tip As New Point(arrowRectangle.X + arrowRectangle.Width, arrowRectangle.Y + (arrowRectangle.Height \ 2))

        gp.AddLine(top, bottom)
        gp.AddLine(bottom, tip)
        gp.AddLine(tip, top)

        Return gp
    End Function

    ''' <summary> Gets button width. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> The button width. </returns>
    Public Overrides Function GetButtonWidth() As Integer
        Dim buttonWidth As Single = 1.41F * Me.ToggleSwitch.Height
        Return CInt(Fix(buttonWidth))
    End Function

    ''' <summary> Gets button rectangle. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> The button rectangle. </returns>
    Public Overrides Function GetButtonRectangle() As Rectangle
        Dim buttonWidth As Integer = Me.GetButtonWidth()
        Return Me.GetButtonRectangle(buttonWidth)
    End Function

    ''' <summary> Gets button rectangle. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="buttonWidth"> Width of the button. </param>
    ''' <returns> The button rectangle. </returns>
    Public Overrides Function GetButtonRectangle(ByVal buttonWidth As Integer) As Rectangle
        Dim buttonRect As New Rectangle(Me.ToggleSwitch.ButtonValue, 0, buttonWidth, Me.ToggleSwitch.Height)
        Return buttonRect
    End Function

#End Region ' Helper Method Implementations
End Class
