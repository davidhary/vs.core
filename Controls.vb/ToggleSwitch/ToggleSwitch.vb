Imports System.ComponentModel

Imports isr.Core.Controls.ExceptionExtensions

''' <summary> A toggle switch. </summary>
''' <remarks>
''' (c) 2015 Johnny J.. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/11/2015 from Johnny J.</para><para>
'''   http://www.codeproject.com/Articles/1029499/ToggleSwitch-Winforms-Control. </para>
''' </remarks>
<DefaultValue("Checked"), DefaultEvent("CheckedChanged"), ToolboxBitmap(GetType(CheckBox))>
Public Class ToggleSwitch
    Inherits Control

#Region "Constructor Etc."

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.Windows.Forms.Control" /> class with
    ''' default settings.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()

        MyBase.New

        Me.SetStyle(ControlStyles.ResizeRedraw Or ControlStyles.SupportsTransparentBackColor Or
                    ControlStyles.AllPaintingInWmPaint Or ControlStyles.UserPaint Or
                    ControlStyles.OptimizedDoubleBuffer Or ControlStyles.DoubleBuffer, True)

        Me._OnFont = MyBase.Font
        Me._OffFont = MyBase.Font

        Me.SetRendererThis(New ToggleSwitchMetroRenderer())

        Me._AnimationTimer.Enabled = False
        Me._AnimationTimer.Interval = Me._AnimationInterval
        AddHandler Me._AnimationTimer.Tick, AddressOf Me.AnimationTimer_Tick
    End Sub

    ''' <summary> Sets renderer this. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="renderer"> The renderer. </param>
    Private Sub SetRendererThis(ByVal renderer As ToggleSwitchRendererBase)
        renderer?.SetToggleSwitch(Me)
        Me._Renderer = renderer
        If Me._Renderer IsNot Nothing Then
            Me.Refresh()
        End If
    End Sub

    ''' <summary> Sets a renderer. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="renderer"> The renderer. </param>
    Public Sub SetRenderer(ByVal renderer As ToggleSwitchRendererBase)
        Me.SetRendererThis(renderer)
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.RemoveCheckedChangedEventHandler(Me.CheckedChangedEvent)
                If Me._OffFont IsNot Nothing Then Me._OffFont.Dispose() : Me._OffFont = Nothing
                If Me._OnFont IsNot Nothing Then Me._OnFont.Dispose() : Me._OffFont = Nothing
                If Me._AnimationTimer IsNot Nothing Then Me._AnimationTimer.Dispose()
                If Me._ButtonImage IsNot Nothing Then Me._ButtonImage.Dispose() : Me._ButtonImage = Nothing
                If Me._OffButtonImage IsNot Nothing Then Me._OffButtonImage.Dispose() : Me._OffButtonImage = Nothing
                If Me._OffSideImage IsNot Nothing Then Me._OffSideImage.Dispose() : Me._OffSideImage = Nothing
                If Me._OnButtonImage IsNot Nothing Then Me._OnButtonImage.Dispose() : Me._OnButtonImage = Nothing
                If Me._OnSideImage IsNot Nothing Then Me._OnSideImage.Dispose() : Me._OnSideImage = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region ' Constructor Etc.

#Region " Public Properties "

    ''' <summary> Gets or sets the style of the ToggleSwitch. </summary>
    ''' <value> The style. </value>
    <Bindable(False), DefaultValue(GetType(ToggleSwitchStyle), "Metro"), Category("Appearance"), Description("Gets or sets the style of the ToggleSwitch")>
    Public Property Style() As ToggleSwitchStyle
        Get
            Return Me._Style
        End Get
        Set(ByVal value As ToggleSwitchStyle)
            If value <> Me._Style Then
                Me._Style = value

                Select Case Me._Style
                    Case ToggleSwitchStyle.Metro
                        Me.SetRenderer(New ToggleSwitchMetroRenderer())
                    Case ToggleSwitchStyle.Android
                        Me.SetRenderer(New ToggleSwitchAndroidRenderer())
                    Case ToggleSwitchStyle.Ios5
                        Me.SetRenderer(New ToggleSwitchIos5Renderer())
                    Case ToggleSwitchStyle.BrushedMetal
                        Me.SetRenderer(New ToggleSwitchBrushedMetalRenderer())
                    Case ToggleSwitchStyle.OS10
                        Me.SetRenderer(New ToggleSwitchOS10Renderer())
                    Case ToggleSwitchStyle.Carbon
                        Me.SetRenderer(New ToggleSwitchCarbonRenderer())
                    Case ToggleSwitchStyle.IPhone
                        Me.SetRenderer(New ToggleSwitchIPhoneRenderer())
                    Case ToggleSwitchStyle.Fancy
                        Me.SetRenderer(New ToggleSwitchFancyRenderer())
                    Case ToggleSwitchStyle.Modern
                        Me.SetRenderer(New ToggleSwitchModernRenderer())
                End Select
            End If

            Me.Refresh()
        End Set
    End Property

    ''' <summary> Gets or sets the Checked value of the ToggleSwitch. </summary>
    ''' <value> The checked. </value>
    <Bindable(True), DefaultValue(False), Category("Data"), Description("Gets or sets the Checked value of the ToggleSwitch")>
    Public Property Checked() As Boolean
        Get
            Return Me._Checked
        End Get
        Set(ByVal value As Boolean)
            If value <> Me._Checked Then
                Do While Me._Animating
                    Windows.Forms.Application.DoEvents()
                Loop

                If value = True Then
                    Dim buttonWidth As Integer = Me._Renderer.GetButtonWidth()
                    Me._AnimationTarget = Me.Width - buttonWidth
                    Me.BeginAnimation(True)
                Else
                    Me._AnimationTarget = 0
                    Me.BeginAnimation(False)
                End If
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets whether the user can change the value of the button or not.
    ''' </summary>
    ''' <value> The allow user change. </value>
    <Bindable(True), DefaultValue(True), Category("Behavior"), Description("Gets or sets whether the user can change the value of the button or not")>
    Public Property AllowUserChange() As Boolean = True

    ''' <summary> Gets the checked string. </summary>
    ''' <value> The checked string. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public ReadOnly Property CheckedString() As String
        Get
            Return If(Me.Checked, If(String.IsNullOrEmpty(Me.OnText), "ON", Me.OnText), If(String.IsNullOrEmpty(Me.OffText), "OFF", Me.OffText))
        End Get
    End Property

    ''' <summary> Gets the button rectangle. </summary>
    ''' <value> The button rectangle. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public ReadOnly Property ButtonRectangle() As Rectangle
        Get
            Return Me._Renderer.GetButtonRectangle()
        End Get
    End Property

    ''' <summary> Gets or sets if the ToggleSwitch should be grayed out when disabled. </summary>
    ''' <value> The gray when disabled. </value>
    <Bindable(False), DefaultValue(True), Category("Appearance"), Description("Gets or sets if the ToggleSwitch should be grayed out when disabled")>
    Public Property GrayWhenDisabled() As Boolean
        Get
            Return Me._GrayWhenDisabled
        End Get
        Set(ByVal value As Boolean)
            If value <> Me._GrayWhenDisabled Then
                Me._GrayWhenDisabled = value

                If Not Me.Enabled Then
                    Me.Refresh()
                End If
            End If
        End Set
    End Property

    ''' <summary> Gets or sets if the ToggleSwitch should toggle when the button is clicked. </summary>
    ''' <value> The toggle on button click. </value>
    <Bindable(False), DefaultValue(True), Category("Behavior"), Description("Gets or sets if the ToggleSwitch should toggle when the button is clicked")>
    Public Property ToggleOnButtonClick() As Boolean = True

    ''' <summary>
    ''' Gets or sets if the ToggleSwitch should toggle when the track besides the button is clicked.
    ''' </summary>
    ''' <value> The toggle on side click. </value>
    <Bindable(False), DefaultValue(True), Category("Behavior"), Description("Gets or sets if the ToggleSwitch should toggle when the track besides the button is clicked")>
    Public Property ToggleOnSideClick() As Boolean = True

    ''' <summary>
    ''' Gets or sets how much the button need to be on the other side (in percent) before it snaps.
    ''' </summary>
    ''' <value> The threshold percentage. </value>
    <Bindable(False), DefaultValue(50), Category("Behavior"), Description("Gets or sets how much the button need to be on the other side (in percent) before it snaps")>
    Public Property ThresholdPercentage() As Integer = 50

    ''' <summary> Gets or sets the fore-color of the text when Checked=false. </summary>
    ''' <value> The color of the off foreground. </value>
    <Bindable(False), DefaultValue(GetType(Color), "Black"), Category("Appearance"), Description("Gets or sets the fore-color of the text when Checked=false")>
    Public Property OffForeColor() As Color
        Get
            Return Me._OffForeColor
        End Get
        Set(ByVal value As Color)
            If value <> Me._OffForeColor Then
                Me._OffForeColor = value
                Me.Refresh()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the font of the text when Checked=false. </summary>
    ''' <value> The off font. </value>
    <Bindable(False), DefaultValue(GetType(Font), "Microsoft Sans Serif, 8.25pt"), Category("Appearance"), Description("Gets or sets the font of the text when Checked=false")>
    Public Property OffFont() As Font
        Get
            Return Me._OffFont
        End Get
        Set(ByVal value As Font)
            If Not Font.Equals(value, Me.OffFont) Then
                Me._OffFont = value
                Me.Refresh()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the text when Checked=true. </summary>
    ''' <value> The off text. </value>
    <Bindable(False), DefaultValue(""), Category("Appearance"), Description("Gets or sets the text when Checked=true")>
    Public Property OffText() As String
        Get
            Return Me._OffText
        End Get
        Set(ByVal value As String)
            If Not String.Equals(value, Me._OffText) Then
                Me._OffText = value
                Me.Refresh()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the side image when Checked=false - Note: Settings the OffSideImage overrules
    ''' the OffText property. Only the image will be shown.
    ''' </summary>
    ''' <value> The off side image. </value>
    <Bindable(False), System.ComponentModel.DefaultValue(CType(Nothing, Object)), Category("Appearance"), Description("Gets or sets the side image when Checked=false - Note: Settings the OffSideImage overrules the OffText property. Only the image will be shown")>
    Public Property OffSideImage() As Image
        Get
            Return Me._OffSideImage
        End Get
        Set(ByVal value As Image)
            If Not Image.Equals(value, Me.OffSideImage) Then
                Me._OffSideImage = value
                Me.Refresh()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets whether the side image visible when Checked=false should be scaled to fit.
    ''' </summary>
    ''' <value> The off side scale image to fit. </value>
    <Bindable(False), DefaultValue(False), Category("Behavior"), Description("Gets or sets whether the side image visible when Checked=false should be scaled to fit")>
    Public Property OffSideScaleImageToFit() As Boolean
        Get
            Return Me._OffSideScaleImage
        End Get
        Set(ByVal value As Boolean)
            If value <> Me._OffSideScaleImage Then
                Me._OffSideScaleImage = value
                Me.Refresh()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets how the text or side image visible when Checked=false should be aligned.
    ''' </summary>
    ''' <value> The off side alignment. </value>
    <Bindable(False), DefaultValue(GetType(ToggleSwitchAlignment), "Center"), Category("Appearance"), Description("Gets or sets how the text or side image visible when Checked=false should be aligned")>
    Public Property OffSideAlignment() As ToggleSwitchAlignment
        Get
            Return Me._OffSideAlignment
        End Get
        Set(ByVal value As ToggleSwitchAlignment)
            If value <> Me._OffSideAlignment Then
                Me._OffSideAlignment = value
                Me.Refresh()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the button image when Checked=false and ButtonImage is not set.
    ''' </summary>
    ''' <value> The off button image. </value>
    <Bindable(False), System.ComponentModel.DefaultValue(CType(Nothing, Object)), Category("Appearance"), Description("Gets or sets the button image when Checked=false and ButtonImage is not set")>
    Public Property OffButtonImage() As Image
        Get
            Return Me._OffButtonImage
        End Get
        Set(ByVal value As Image)
            If Not Image.Equals(value, Me.OffButtonImage) Then
                Me._OffButtonImage = value
                Me.Refresh()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets whether the button image visible when Checked=false should be scaled to fit.
    ''' </summary>
    ''' <value> The off button scale image to fit. </value>
    <Bindable(False), DefaultValue(False), Category("Behavior"), Description("Gets or sets whether the button image visible when Checked=false should be scaled to fit")>
    Public Property OffButtonScaleImageToFit() As Boolean
        Get
            Return Me._OffButtonScaleImage
        End Get
        Set(ByVal value As Boolean)
            If value <> Me._OffButtonScaleImage Then
                Me._OffButtonScaleImage = value
                Me.Refresh()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets how the button image visible when Checked=false should be aligned.
    ''' </summary>
    ''' <value> The off button alignment. </value>
    <Bindable(False), DefaultValue(GetType(ToggleSwitchButtonAlignment), "Center"), Category("Appearance"), Description("Gets or sets how the button image visible when Checked=false should be aligned")>
    Public Property OffButtonAlignment() As ToggleSwitchButtonAlignment
        Get
            Return Me._OffButtonAlignment
        End Get
        Set(ByVal value As ToggleSwitchButtonAlignment)
            If value <> Me._OffButtonAlignment Then
                Me._OffButtonAlignment = value
                Me.Refresh()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the fore-color of the text when Checked=true. </summary>
    ''' <value> The color of the on foreground. </value>
    <Bindable(False), DefaultValue(GetType(Color), "Black"), Category("Appearance"), Description("Gets or sets the fore-color of the text when Checked=true")>
    Public Property OnForeColor() As Color
        Get
            Return Me._OnForeColor
        End Get
        Set(ByVal value As Color)
            If value <> Me._OnForeColor Then
                Me._OnForeColor = value
                Me.Refresh()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the font of the text when Checked=true. </summary>
    ''' <value> The on font. </value>
    <Bindable(False), DefaultValue(GetType(Font), "Microsoft Sans Serif, 8,25pt"), Category("Appearance"), Description("Gets or sets the font of the text when Checked=true")>
    Public Property OnFont() As Font
        Get
            Return Me._OnFont
        End Get
        Set(ByVal value As Font)
            If Not Font.Equals(value, Me.OnFont) Then
                Me._OnFont = value
                Me.Refresh()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the text when Checked=true. </summary>
    ''' <value> The on text. </value>
    <Bindable(False), DefaultValue(""), Category("Appearance"), Description("Gets or sets the text when Checked=true")>
    Public Property OnText() As String
        Get
            Return Me._OnText
        End Get
        Set(ByVal value As String)
            If Not String.Equals(value, Me.OnText) Then
                Me._OnText = value
                Me.Refresh()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the side image visible when Checked=true - Note: Settings the OnSideImage
    ''' overrules the OnText property. Only the image will be shown.
    ''' </summary>
    ''' <value> The on side image. </value>
    <Bindable(False), System.ComponentModel.DefaultValue(CType(Nothing, Object)), Category("Appearance"), Description("Gets or sets the side image visible when Checked=true - Note: Settings the OnSideImage overrules the OnText property. Only the image will be shown.")>
    Public Property OnSideImage() As Image
        Get
            Return Me._OnSideImage
        End Get
        Set(ByVal value As Image)
            If value IsNot Me._OnSideImage Then
                Me._OnSideImage = value
                Me.Refresh()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets whether the side image visible when Checked=true should be scaled to fit.
    ''' </summary>
    ''' <value> The on side scale image to fit. </value>
    <Bindable(False), DefaultValue(False), Category("Behavior"), Description("Gets or sets whether the side image visible when Checked=true should be scaled to fit")>
    Public Property OnSideScaleImageToFit() As Boolean
        Get
            Return Me._OnSideScaleImage
        End Get
        Set(ByVal value As Boolean)
            If value <> Me._OnSideScaleImage Then
                Me._OnSideScaleImage = value
                Me.Refresh()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the button image. </summary>
    ''' <value> The button image. </value>
    <Bindable(False), System.ComponentModel.DefaultValue(CType(Nothing, Object)), Category("Appearance"), Description("Gets or sets the button image")>
    Public Property ButtonImage() As Image
        Get
            Return Me._ButtonImage
        End Get
        Set(ByVal value As Image)
            If value IsNot Me._ButtonImage Then
                Me._ButtonImage = value
                Me.Refresh()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets whether the button image should be scaled to fit. </summary>
    ''' <value> The button scale image to fit. </value>
    <Bindable(False), DefaultValue(False), Category("Behavior"), Description("Gets or sets whether the button image should be scaled to fit")>
    Public Property ButtonScaleImageToFit() As Boolean
        Get
            Return Me._ButtonScaleImage
        End Get
        Set(ByVal value As Boolean)
            If value <> Me._ButtonScaleImage Then
                Me._ButtonScaleImage = value
                Me.Refresh()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets how the button image should be aligned. </summary>
    ''' <value> The button alignment. </value>
    <Bindable(False), DefaultValue(GetType(ToggleSwitchButtonAlignment), "Center"), Category("Appearance"), Description("Gets or sets how the button image should be aligned")>
    Public Property ButtonAlignment() As ToggleSwitchButtonAlignment
        Get
            Return Me._ButtonAlignment
        End Get
        Set(ByVal value As ToggleSwitchButtonAlignment)
            If value <> Me._ButtonAlignment Then
                Me._ButtonAlignment = value
                Me.Refresh()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets how the text or side image visible when Checked=true should be aligned.
    ''' </summary>
    ''' <value> The on side alignment. </value>
    <Bindable(False), DefaultValue(GetType(ToggleSwitchAlignment), "Center"), Category("Appearance"), Description("Gets or sets how the text or side image visible when Checked=true should be aligned")>
    Public Property OnSideAlignment() As ToggleSwitchAlignment
        Get
            Return Me._OnSideAlignment
        End Get
        Set(ByVal value As ToggleSwitchAlignment)
            If value <> Me._OnSideAlignment Then
                Me._OnSideAlignment = value
                Me.Refresh()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the button image visible when Checked=true and ButtonImage is not set.
    ''' </summary>
    ''' <value> The on button image. </value>
    <Bindable(False), System.ComponentModel.DefaultValue(CType(Nothing, Object)), Category("Appearance"), Description("Gets or sets the button image visible when Checked=true and ButtonImage is not set")>
    Public Property OnButtonImage() As Image
        Get
            Return Me._OnButtonImage
        End Get
        Set(ByVal value As Image)
            If value IsNot Me._OnButtonImage Then
                Me._OnButtonImage = value
                Me.Refresh()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets whether the button image visible when Checked=true should be scaled to fit.
    ''' </summary>
    ''' <value> The on button scale image to fit. </value>
    <Bindable(False), DefaultValue(False), Category("Behavior"), Description("Gets or sets whether the button image visible when Checked=true should be scaled to fit")>
    Public Property OnButtonScaleImageToFit() As Boolean
        Get
            Return Me._OnButtonScaleImage
        End Get
        Set(ByVal value As Boolean)
            If value <> Me._OnButtonScaleImage Then
                Me._OnButtonScaleImage = value
                Me.Refresh()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets how the button image visible when Checked=true should be aligned.
    ''' </summary>
    ''' <value> The on button alignment. </value>
    <Bindable(False), DefaultValue(GetType(ToggleSwitchButtonAlignment), "Center"), Category("Appearance"), Description("Gets or sets how the button image visible when Checked=true should be aligned")>
    Public Property OnButtonAlignment() As ToggleSwitchButtonAlignment
        Get
            Return Me._OnButtonAlignment
        End Get
        Set(ByVal value As ToggleSwitchButtonAlignment)
            If value <> Me._OnButtonAlignment Then
                Me._OnButtonAlignment = value
                Me.Refresh()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets whether the toggle change should be animated or not. </summary>
    ''' <value> The use animation. </value>
    <Bindable(False), DefaultValue(True), Category("Behavior"), Description("Gets or sets whether the toggle change should be animated or not")>
    Public Property UseAnimation() As Boolean = True

    ''' <summary> Gets or sets the interval in ms between animation frames. </summary>
    ''' <value> The animation interval. </value>
    <Bindable(False), DefaultValue(1), Category("Behavior"), Description("Gets or sets the interval in ms between animation frames")>
    Public Property AnimationInterval() As Integer
        Get
            Return Me._AnimationInterval
        End Get
        Set(ByVal value As Integer)
            If value <> Me.AnimationInterval AndAlso value > 0 Then
                Me._AnimationInterval = value
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the step in pixes the button should be moved between each animation interval.
    ''' </summary>
    ''' <value> The animation step. </value>
    <Bindable(False), DefaultValue(10), Category("Behavior"), Description("Gets or sets the step in pixes the button should be moved between each animation interval")>
    Public Property AnimationStep() As Integer
        Get
            Return Me._AnimationStep
        End Get
        Set(ByVal value As Integer)
            If value <> Me.AnimationStep AndAlso value > 0 Then
                Me._AnimationStep = value
            End If
        End Set
    End Property

#Region "Hidden Base Properties"

    ''' <summary> Gets or sets the text. </summary>
    ''' <value> The text. </value>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Shadows Property Text() As String
        Get
            Return String.Empty
        End Get
        Set(ByVal value As String)
            MyBase.Text = String.Empty
        End Set
    End Property

    ''' <summary> Gets or sets the color of the foreground. </summary>
    ''' <value> The color of the foreground. </value>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Shadows Property ForeColor() As Color
        Get
            Return Color.Black
        End Get
        Set(ByVal value As Color)
            MyBase.ForeColor = Color.Black
        End Set
    End Property

    ''' <summary> Gets or sets the font. </summary>
    ''' <value> The font. </value>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Shadows Property Font() As Font
        Get
            Return MyBase.Font
        End Get
        Set(ByVal value As Font)
            MyBase.Font = New Font(MyBase.Font, FontStyle.Regular)
        End Set
    End Property

#End Region ' Hidden Base Properties

#End Region ' Public Properties

#Region "Internal Properties"

    ''' <summary> Gets the is button hovered. </summary>
    ''' <value> The is button hovered. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Friend ReadOnly Property IsButtonHovered() As Boolean
        Get
            Return Me._IsButtonHovered AndAlso Not Me._IsButtonPressed
        End Get
    End Property

    ''' <summary> Gets the is button pressed. </summary>
    ''' <value> The is button pressed. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Friend ReadOnly Property IsButtonPressed() As Boolean
        Get
            Return Me._IsButtonPressed
        End Get
    End Property

    ''' <summary> Gets the is left side hovered. </summary>
    ''' <value> The is left side hovered. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Friend ReadOnly Property IsLeftSideHovered() As Boolean
        Get
            Return Me._IsLeftFieldHovered AndAlso Not Me._IsLeftFieldPressed
        End Get
    End Property

    ''' <summary> Gets the is left side pressed. </summary>
    ''' <value> The is left side pressed. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Friend ReadOnly Property IsLeftSidePressed() As Boolean
        Get
            Return Me._IsLeftFieldPressed
        End Get
    End Property

    ''' <summary> Gets the is right side hovered. </summary>
    ''' <value> The is right side hovered. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Friend ReadOnly Property IsRightSideHovered() As Boolean
        Get
            Return Me._IsRightFieldHovered AndAlso Not Me._IsRightFieldPressed
        End Get
    End Property

    ''' <summary> Gets the is right side pressed. </summary>
    ''' <value> The is right side pressed. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Friend ReadOnly Property IsRightSidePressed() As Boolean
        Get
            Return Me._IsRightFieldPressed
        End Get
    End Property

    ''' <summary> Gets the button value. </summary>
    ''' <value> The button value. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Friend Property ButtonValue() As Integer
        Get
            If Me._Animating OrElse Me._Moving Then
                Return Me._ButtonValue
            ElseIf Me._Checked Then
                Return Me.Width - Me._Renderer.GetButtonWidth()
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            If value <> Me._ButtonValue Then
                Me._ButtonValue = value
                Me.Refresh()
            End If
        End Set
    End Property

    ''' <summary> Gets the is button left side. </summary>
    ''' <value> The is button on left side. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Friend ReadOnly Property IsButtonOnLeftSide() As Boolean
        Get
            Return Me.ButtonValue <= 0
        End Get
    End Property

    ''' <summary> Gets the is button right side. </summary>
    ''' <value> The is button on right side. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Friend ReadOnly Property IsButtonOnRightSide() As Boolean
        Get
            Return Me.ButtonValue >= (Me.Width - Me._Renderer.GetButtonWidth())
        End Get
    End Property

    ''' <summary> Gets the is button moving left. </summary>
    ''' <value> The is button moving left. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Friend ReadOnly Property IsButtonMovingLeft() As Boolean
        Get
            Return Me._Animating AndAlso (Not Me.IsButtonOnLeftSide) AndAlso Me._AnimationResult = False
        End Get
    End Property

    ''' <summary> Gets the is button moving right. </summary>
    ''' <value> The is button moving right. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Friend ReadOnly Property IsButtonMovingRight() As Boolean
        Get
            Return Me._Animating AndAlso (Not Me.IsButtonOnRightSide) AndAlso Me._AnimationResult = True
        End Get
    End Property

    ''' <summary> Gets the animation result. </summary>
    ''' <value> The animation result. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Friend ReadOnly Property AnimationResult() As Boolean
        Get
            Return Me._AnimationResult
        End Get
    End Property

#End Region ' Private Properties

#Region "Overridden Control Methods"

    ''' <summary> Gets the default size of the control. </summary>
    ''' <value> The default <see cref="T:System.Drawing.Size" /> of the control. </value>
    Protected Overrides ReadOnly Property DefaultSize() As Size
        Get
            Return New Size(50, 19)
        End Get
    End Property

    ''' <summary> Paints the background of the control. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="pevent"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains
    '''                       information about the control to paint. </param>
    Protected Overrides Sub OnPaintBackground(ByVal pevent As PaintEventArgs)
        If pevent Is Nothing Then Return
        pevent.Graphics.ResetClip()
        MyBase.OnPaintBackground(pevent)
        If Me._Renderer IsNot Nothing Then
            Me._Renderer.RenderBackground(pevent)
        End If
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.Paint" /> event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnPaint(ByVal e As PaintEventArgs)
        If e Is Nothing Then Return
        e.Graphics.ResetClip()

        MyBase.OnPaint(e)

        If Me._Renderer IsNot Nothing Then
            Me._Renderer.RenderControl(e)
        End If
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.MouseMove" /> event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnMouseMove(ByVal e As MouseEventArgs)
        If e Is Nothing Then Return
        Me._LastMouseEventArgs = e

        Dim buttonWidth As Integer = Me._Renderer.GetButtonWidth()
        Dim rectangle As Rectangle = Me._Renderer.GetButtonRectangle(buttonWidth)

        If Me._Moving Then
            Dim val As Integer = Me._XValue + (e.Location.X - Me._XOffset)

            If val < 0 Then
                val = 0
            End If

            If val > Me.Width - buttonWidth Then
                val = Me.Width - buttonWidth
            End If

            Me.ButtonValue = val
            Me.Refresh()
            Return
        End If

        If rectangle.Contains(e.Location) Then
            Me._IsButtonHovered = True
            Me._IsLeftFieldHovered = False
            Me._IsRightFieldHovered = False
        Else
            If e.Location.X > rectangle.X + rectangle.Width Then
                Me._IsButtonHovered = False
                Me._IsLeftFieldHovered = False
                Me._IsRightFieldHovered = True
            ElseIf e.Location.X < rectangle.X Then
                Me._IsButtonHovered = False
                Me._IsLeftFieldHovered = True
                Me._IsRightFieldHovered = False
            End If
        End If

        Me.Refresh()
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.MouseDown" /> event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnMouseDown(ByVal e As MouseEventArgs)
        If e Is Nothing Then Return
        If Me._Animating OrElse (Not Me.AllowUserChange) Then
            Return
        End If

        Dim buttonWidth As Integer = Me._Renderer.GetButtonWidth()
        'INSTANT VB NOTE: The variable buttonRectangle was renamed since Visual Basic does not handle local variables named the same as class members well:
        Dim buttonRectangle_Renamed As Rectangle = Me._Renderer.GetButtonRectangle(buttonWidth)

        Me._SavedButtonValue = Me.ButtonValue

        If buttonRectangle_Renamed.Contains(e.Location) Then
            Me._IsButtonPressed = True
            Me._IsLeftFieldPressed = False
            Me._IsRightFieldPressed = False

            Me._Moving = True
            Me._XOffset = e.Location.X
            Me._ButtonValue = buttonRectangle_Renamed.X
            Me._XValue = Me.ButtonValue
        Else
            If e.Location.X > buttonRectangle_Renamed.X + buttonRectangle_Renamed.Width Then
                Me._IsButtonPressed = False
                Me._IsLeftFieldPressed = False
                Me._IsRightFieldPressed = True
            ElseIf e.Location.X < buttonRectangle_Renamed.X Then
                Me._IsButtonPressed = False
                Me._IsLeftFieldPressed = True
                Me._IsRightFieldPressed = False
            End If
        End If

        Me.Refresh()
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.MouseUp" /> event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnMouseUp(ByVal e As MouseEventArgs)
        If Me._Animating OrElse (Not Me.AllowUserChange) Then
            Return
        End If

        Dim buttonWidth As Integer = Me._Renderer.GetButtonWidth()

        Dim wasLeftSidePressed As Boolean = Me.IsLeftSidePressed
        Dim wasRightSidePressed As Boolean = Me.IsRightSidePressed

        Me._IsButtonPressed = False
        Me._IsLeftFieldPressed = False
        Me._IsRightFieldPressed = False

        If Me._Moving Then
            Dim percentage As Integer = CInt(Fix(100 * CDbl(Me.ButtonValue) / (CDbl(Me.Width) - CDbl(buttonWidth))))

            If Me._Checked Then
                If percentage <= (100 - Me.ThresholdPercentage) Then
                    Me._AnimationTarget = 0
                    Me.BeginAnimation(False)
                ElseIf Me.ToggleOnButtonClick AndAlso Me._SavedButtonValue = Me.ButtonValue Then
                    Me._AnimationTarget = 0
                    Me.BeginAnimation(False)
                Else
                    Me._AnimationTarget = Me.Width - buttonWidth
                    Me.BeginAnimation(True)
                End If
            Else
                If percentage >= Me.ThresholdPercentage Then
                    Me._AnimationTarget = Me.Width - buttonWidth
                    Me.BeginAnimation(True)
                ElseIf Me.ToggleOnButtonClick AndAlso Me._SavedButtonValue = Me.ButtonValue Then
                    Me._AnimationTarget = Me.Width - buttonWidth
                    Me.BeginAnimation(True)
                Else
                    Me._AnimationTarget = 0
                    Me.BeginAnimation(False)
                End If
            End If

            Me._Moving = False
            Return
        End If

        If Me.IsButtonOnRightSide Then
            Me._ButtonValue = Me.Width - buttonWidth
            Me._AnimationTarget = 0
        Else
            Me._ButtonValue = 0
            Me._AnimationTarget = Me.Width - buttonWidth
        End If

        If wasLeftSidePressed AndAlso Me.ToggleOnSideClick Then
            Me.SetValueInternal(False)
        ElseIf wasRightSidePressed AndAlso Me.ToggleOnSideClick Then
            Me.SetValueInternal(True)
        End If

        Me.Refresh()
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.MouseLeave" /> event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnMouseLeave(ByVal e As EventArgs)
        Me._IsButtonHovered = False
        Me._IsLeftFieldHovered = False
        Me._IsRightFieldHovered = False
        Me._IsButtonPressed = False
        Me._IsLeftFieldPressed = False
        Me._IsRightFieldPressed = False

        Me.Refresh()
    End Sub

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.Control.EnabledChanged" /> event.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnEnabledChanged(ByVal e As EventArgs)
        MyBase.OnEnabledChanged(e)
        Me.Refresh()
    End Sub

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.Control.RegionChanged" /> event.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnRegionChanged(ByVal e As EventArgs)
        MyBase.OnRegionChanged(e)
        Me.Refresh()
    End Sub

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.Control.SizeChanged" /> event.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnSizeChanged(ByVal e As EventArgs)
        If Me._AnimationTarget > 0 Then
            Dim buttonWidth As Integer = Me._Renderer.GetButtonWidth()
            Me._AnimationTarget = Me.Width - buttonWidth
        End If

        MyBase.OnSizeChanged(e)
    End Sub

#End Region ' Overridden Control Methods

#Region "Private Methods"

    ''' <summary> Sets value internal. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="checkedValue"> True to checked value. </param>
    Private Sub SetValueInternal(ByVal checkedValue As Boolean)
        If checkedValue = Me._Checked Then
            Return
        End If

        Do While Me._Animating
            Windows.Forms.Application.DoEvents()
        Loop

        Me.BeginAnimation(checkedValue)
    End Sub

    ''' <summary> Begins an animation. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="checkedValue"> True to checked value. </param>
    Private Sub BeginAnimation(ByVal checkedValue As Boolean)
        Me._Animating = True
        Me._AnimationResult = checkedValue

        If Me._AnimationTimer IsNot Nothing AndAlso Me.UseAnimation Then
            Me._AnimationTimer.Interval = Me._AnimationInterval
            Me._AnimationTimer.Enabled = True
        Else
            Me.AnimationComplete()
        End If
    End Sub

    ''' <summary> Event handler. Called by AnimationTimer for tick events. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub AnimationTimer_Tick(ByVal sender As Object, ByVal e As EventArgs)
        Me._AnimationTimer.Enabled = False

        Dim newButtonValue As Integer


        Dim animationDone As Boolean
        If Me.IsButtonMovingRight Then
            newButtonValue = Me.ButtonValue + Me._AnimationStep

            If newButtonValue > Me._AnimationTarget Then
                newButtonValue = Me._AnimationTarget
            End If

            Me.ButtonValue = newButtonValue

            animationDone = Me.ButtonValue >= Me._AnimationTarget
        Else
            newButtonValue = Me.ButtonValue - Me._AnimationStep

            If newButtonValue < Me._AnimationTarget Then
                newButtonValue = Me._AnimationTarget
            End If

            Me.ButtonValue = newButtonValue

            animationDone = Me.ButtonValue <= Me._AnimationTarget
        End If

        If animationDone Then
            Me.AnimationComplete()
        Else
            Me._AnimationTimer.Enabled = True
        End If
    End Sub

    ''' <summary> Animation complete. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Sub AnimationComplete()
        Me._Animating = False
        Me._Moving = False
        Me._Checked = Me._AnimationResult

        Me._IsButtonHovered = False
        Me._IsButtonPressed = False
        Me._IsLeftFieldHovered = False
        Me._IsLeftFieldPressed = False
        Me._IsRightFieldHovered = False
        Me._IsRightFieldPressed = False

        Me.Refresh()

        RaiseEvent CheckedChanged(Me, New EventArgs())

        If Me._LastMouseEventArgs IsNot Nothing Then
            Me.OnMouseMove(Me._LastMouseEventArgs)
        End If

        Me._LastMouseEventArgs = Nothing
    End Sub

#End Region ' Private Methods

#Region " EVENTS "

    ''' <summary> Event queue for all listeners interested in CheckedChanged events. </summary>
    <Description("Raised when the ToggleSwitch has changed state")>
    Public Event CheckedChanged As EventHandler(Of EventArgs)

    ''' <summary> Removes event handler. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The handler. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub RemoveCheckedChangedEventHandler(ByVal value As EventHandler(Of EventArgs))
#Disable Warning CA1825 ' Avoid zero-length array allocations.
        For Each d As [Delegate] In If(value Is Nothing, New [Delegate]() {}, value.GetInvocationList)
#Enable Warning CA1825 ' Avoid zero-length array allocations.
            Try
                RemoveHandler Me.CheckedChanged, CType(d, EventHandler(Of EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

#End Region

#Region "Private Members"

    ''' <summary> The animation timer. </summary>
    Private ReadOnly _AnimationTimer As New Timer()

    ''' <summary> The renderer. </summary>
    Private _Renderer As ToggleSwitchRendererBase

    ''' <summary> The style. </summary>
    Private _Style As ToggleSwitchStyle = ToggleSwitchStyle.Metro

    ''' <summary> True if checked. </summary>
    Private _Checked As Boolean = False

    ''' <summary> True to moving. </summary>
    Private _Moving As Boolean = False

    ''' <summary> True to animating. </summary>
    Private _Animating As Boolean = False

    ''' <summary> True to animation result. </summary>
    Private _AnimationResult As Boolean = False

    ''' <summary> The animation target. </summary>
    Private _AnimationTarget As Integer = 0

    ''' <summary> The animation interval. </summary>
    Private _AnimationInterval As Integer = 1

    ''' <summary> The animation step. </summary>
    Private _AnimationStep As Integer = 10

    ''' <summary> True if is left field hovered, false if not. </summary>
    Private _IsLeftFieldHovered As Boolean = False

    ''' <summary> True if is button hovered, false if not. </summary>
    Private _IsButtonHovered As Boolean = False

    ''' <summary> True if is right field hovered, false if not. </summary>
    Private _IsRightFieldHovered As Boolean = False

    ''' <summary> True if is left field pressed, false if not. </summary>
    Private _IsLeftFieldPressed As Boolean = False

    ''' <summary> True if is button pressed, false if not. </summary>
    Private _IsButtonPressed As Boolean = False

    ''' <summary> True if is right field pressed, false if not. </summary>
    Private _IsRightFieldPressed As Boolean = False

    ''' <summary> The button value. </summary>
    Private _ButtonValue As Integer = 0

    ''' <summary> The saved button value. </summary>
    Private _SavedButtonValue As Integer = 0

    ''' <summary> The offset. </summary>
    Private _XOffset As Integer = 0

    ''' <summary> The value. </summary>
    Private _XValue As Integer = 0

    ''' <summary> True to disable, false to enable the gray when. </summary>
    Private _GrayWhenDisabled As Boolean = True

    ''' <summary> Mouse event information. </summary>
    Private _LastMouseEventArgs As MouseEventArgs = Nothing

    ''' <summary> True to button scale image. </summary>
    Private _ButtonScaleImage As Boolean

    ''' <summary> The button alignment. </summary>
    Private _ButtonAlignment As ToggleSwitchButtonAlignment = ToggleSwitchButtonAlignment.Center

    ''' <summary> The button image. </summary>
    Private _ButtonImage As Image = Nothing

    ''' <summary> The off text. </summary>
    Private _OffText As String = String.Empty

    ''' <summary> The off foreground color. </summary>
    Private _OffForeColor As Color = Color.Black

    ''' <summary> The off font. </summary>
    Private _OffFont As Font

    ''' <summary> The off side image. </summary>
    Private _OffSideImage As Image = Nothing

    ''' <summary> True to off side scale image. </summary>
    Private _OffSideScaleImage As Boolean

    ''' <summary> The off side alignment. </summary>
    Private _OffSideAlignment As ToggleSwitchAlignment = ToggleSwitchAlignment.Center

    ''' <summary> The off button image. </summary>
    Private _OffButtonImage As Image = Nothing

    ''' <summary> True to off button scale image. </summary>
    Private _OffButtonScaleImage As Boolean

    ''' <summary> The off button alignment. </summary>
    Private _OffButtonAlignment As ToggleSwitchButtonAlignment = ToggleSwitchButtonAlignment.Center

    ''' <summary> The on text. </summary>
    Private _OnText As String = String.Empty

    ''' <summary> The on foreground color. </summary>
    Private _OnForeColor As Color = Color.Black

    ''' <summary> The on font. </summary>
    Private _OnFont As Font

    ''' <summary> The on side image. </summary>
    Private _OnSideImage As Image = Nothing

    ''' <summary> True to on side scale image. </summary>
    Private _OnSideScaleImage As Boolean

    ''' <summary> The on side alignment. </summary>
    Private _OnSideAlignment As ToggleSwitchAlignment = ToggleSwitchAlignment.Center

    ''' <summary> The on button image. </summary>
    Private _OnButtonImage As Image = Nothing

    ''' <summary> True to on button scale image. </summary>
    Private _OnButtonScaleImage As Boolean

    ''' <summary> The on button alignment. </summary>
    Private _OnButtonAlignment As ToggleSwitchButtonAlignment = ToggleSwitchButtonAlignment.Center

#End Region ' Private Members

End Class

#Region "Enums"

''' <summary> Values that represent toggle switch styles. </summary>
''' <remarks> David, 2020-09-24. </remarks>
Public Enum ToggleSwitchStyle

    ''' <summary> An enum constant representing the metro option. </summary>
    Metro

    ''' <summary> An enum constant representing the android option. </summary>
    Android

    ''' <summary> An enum constant representing the ios 5 option. </summary>
    Ios5

    ''' <summary> An enum constant representing the brushed metal option. </summary>
    BrushedMetal

    ''' <summary> An enum constant representing the Operating system 10 option. </summary>
    OS10

    ''' <summary> An enum constant representing the carbon option. </summary>
    Carbon

    ''' <summary> An enum constant representing the iPhone option. </summary>
    IPhone

    ''' <summary> An enum constant representing the fancy option. </summary>
    Fancy

    ''' <summary> An enum constant representing the modern option. </summary>
    Modern
End Enum

''' <summary> Values that represent toggle switch alignments. </summary>
''' <remarks> David, 2020-09-24. </remarks>
Public Enum ToggleSwitchAlignment

    ''' <summary> An enum constant representing the near option. </summary>
    Near

    ''' <summary> An enum constant representing the center option. </summary>
    Center

    ''' <summary> An enum constant representing the far option. </summary>
    Far
End Enum

''' <summary> Values that represent toggle switch button alignments. </summary>
''' <remarks> David, 2020-09-24. </remarks>
Public Enum ToggleSwitchButtonAlignment

    ''' <summary> An enum constant representing the left option. </summary>
    Left

    ''' <summary> An enum constant representing the center option. </summary>
    Center

    ''' <summary> An enum constant representing the right option. </summary>
    Right
End Enum

#End Region ' Enums

