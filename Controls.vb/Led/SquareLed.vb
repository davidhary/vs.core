Imports isr.Core.Controls.ExceptionExtensions

''' <summary> Defines a simple LED display with a caption. </summary>
''' <remarks>
''' (c) 2006 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 09/05/2006, 2.0.2439.x. </para>
''' </remarks>
<System.ComponentModel.Description("Squared Led Control")>
Public Class SquareLed
    Inherits Forma.ModelViewBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Releases the unmanaged resources used by the isr.Core.Forma.ModelViewBase and optionally
    ''' releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.RemoveClickEvent(Me.ClickEvent)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " APPEARANCE "

    ''' <summary> The background color. </summary>
    Private _BackgroundColor As Integer

    ''' <summary> Gets or sets the color of the background. </summary>
    ''' <value> The color of the background. </value>
    <System.ComponentModel.Browsable(True),
      System.ComponentModel.Category("Appearance"),
      System.ComponentModel.Description("Background Color.")>
    Public Property BackgroundColor() As Integer
        Get
            Return Me._BackgroundColor
        End Get
        Set(ByVal value As Integer)
            If value <> Me.BackgroundColor Then
                Me._BackgroundColor = value
                Me.BackColor = System.Drawing.ColorTranslator.FromOle(value)
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The caption. </summary>
    Private _Caption As String

    ''' <summary> Gets or sets the caption. </summary>
    ''' <value> The caption. </value>
    <System.ComponentModel.Browsable(True),
      System.ComponentModel.Category("Appearance"),
      System.ComponentModel.Description("Caption.")>
    Public Property Caption() As String
        Get
            Return Me._Caption
        End Get
        Set(ByVal value As String)
            If Not String.Equals(value, Me.Caption) Then
                Me._Caption = value
                Me.CaptionLabel.Text = Me.Caption
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The indicator color. </summary>
    Private _IndicatorColor As Integer

    ''' <summary> Gets or sets the color of the indicator. </summary>
    ''' <value> The color of the indicator. </value>
    <System.ComponentModel.Browsable(True),
      System.ComponentModel.Category("Appearance"),
      System.ComponentModel.Description("Indicator color.")>
    Public Property IndicatorColor() As Integer
        Get
            Return Me._IndicatorColor
        End Get
        Set(ByVal value As Integer)
            If value <> Me.IndicatorColor Then
                Me._IndicatorColor = value
                Me.IndicatorLabel.BackColor = System.Drawing.ColorTranslator.FromOle(Me._IndicatorColor)
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The state. </summary>
    Private _State As IndicatorState

    ''' <summary> Gets or sets the is on. </summary>
    ''' <value> The is on. </value>
    <System.ComponentModel.Browsable(True),
      System.ComponentModel.Category("Appearance"),
      System.ComponentModel.Description("Is On.")>
    Public Property IsOn() As Boolean
        Get
            Return Me._State = IndicatorState.On
        End Get
        Set(ByVal value As Boolean)
            If value <> Me.IsOn Then
                Me.State = If(value, IndicatorState.On, IndicatorState.Off)
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The no color. </summary>
    Private _NoColor As Integer

    ''' <summary> Gets or sets the color of the no. </summary>
    ''' <value> The color of the no. </value>
    <System.ComponentModel.Browsable(True),
      System.ComponentModel.Category("Appearance"),
      System.ComponentModel.Description("No Color.")>
    Public Property NoColor() As Integer
        Get
            Return Me._NoColor
        End Get
        Set(ByVal value As Integer)
            If value <> Me.NoColor Then
                Me._NoColor = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The off color. </summary>
    Private _OffColor As Integer

    ''' <summary> Gets or sets the color of the off. </summary>
    ''' <value> The color of the off. </value>
    <System.ComponentModel.Browsable(True),
      System.ComponentModel.Category("Appearance"),
      System.ComponentModel.Description("Off Color.")>
    Public Property OffColor() As Integer
        Get
            Return Me._OffColor
        End Get
        Set(ByVal value As Integer)
            If value <> Me.OffColor Then
                Me._OffColor = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The on color. </summary>
    Private _OnColor As Integer

    ''' <summary> Gets or sets the color of the on. </summary>
    ''' <value> The color of the on. </value>
    <System.ComponentModel.Browsable(True),
      System.ComponentModel.Category("Appearance"),
      System.ComponentModel.Description("On Color.")>
    Public Property OnColor() As Integer
        Get
            Return Me._OnColor
        End Get
        Set(ByVal value As Integer)
            If value <> Me.OnColor Then
                Me._OnColor = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the indicator color based on the state. </summary>
    ''' <value> The state. </value>
    <System.ComponentModel.Browsable(True),
      System.ComponentModel.Category("Appearance"),
      System.ComponentModel.Description("Current State.")>
    Public Property State() As IndicatorState
        Get
            Return Me._State
        End Get
        Set(ByVal value As IndicatorState)
            If value <> Me.State Then
                Me._State = value
                If Me.Visible Then
                    Select Case True
                        Case value = IndicatorState.None
                            Me.IndicatorColor = Me.NoColor
                        Case value = IndicatorState.On
                            Me.IndicatorColor = Me.OnColor
                        Case value = IndicatorState.Off
                            Me.IndicatorColor = Me.OffColor
                    End Select
                End If
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " EVENTS HANDLERS "

    ''' <summary>Occurs when the user clicks the indicator.
    ''' </summary>
    Public Shadows Event Click As EventHandler(Of EventArgs)

    ''' <summary> Removes click event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The value. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub RemoveClickEvent(ByVal value As EventHandler(Of System.EventArgs))
#Disable Warning CA1825 ' Avoid zero-length array allocations.
        For Each d As [Delegate] In If(value Is Nothing, New [Delegate]() {}, value.GetInvocationList)
#Enable Warning CA1825 ' Avoid zero-length array allocations.
            Try
                RemoveHandler Me.Click, CType(d, EventHandler(Of System.EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

    ''' <summary> Raises the click event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="eventSender"> The source of the event. </param>
    ''' <param name="e">           The <see cref="System.EventArgs" /> instance containing the event
    '''                            data. </param>
    Private Sub IndicatorLabel_Click(ByVal eventSender As System.Object, ByVal e As System.EventArgs) Handles IndicatorLabel.Click

        Dim evt As EventHandler(Of EventArgs) = Me.ClickEvent
        evt?.Invoke(Me, e)

    End Sub

    ''' <summary> Position the controls. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="eventSender"> The source of the event. </param>
    ''' <param name="eventArgs">   Event information. </param>
    Private Sub SquareLed_Resize(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Resize

        Me.CaptionLabel.SetBounds((Me.Width - Me.CaptionLabel.Width) \ 2, 0, Me.Width, Me.CaptionLabel.Height, System.Windows.Forms.BoundsSpecified.All)
        Me.IndicatorLabel.SetBounds((Me.Width - Me.IndicatorLabel.Width) \ 2, Me.Height - Me.IndicatorLabel.Height, 0, 0, System.Windows.Forms.BoundsSpecified.X Or System.Windows.Forms.BoundsSpecified.Y)

    End Sub

#End Region

End Class

''' <summary> Enumerates the indicator states. </summary>
''' <remarks> David, 2020-09-24. </remarks>
Public Enum IndicatorState

    ''' <summary> . </summary>
    <System.ComponentModel.Description("None")> None

    ''' <summary> . </summary>
    <System.ComponentModel.Description("On")> [On]

    ''' <summary> . </summary>
    <System.ComponentModel.Description("Off")> [Off]
End Enum

