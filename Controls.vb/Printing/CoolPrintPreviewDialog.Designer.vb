﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CoolPrintPreviewDialog

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CoolPrintPreviewDialog))
        Me._toolStrip = New System.Windows.Forms.ToolStrip
        Me._PrintButton = New System.Windows.Forms.ToolStripButton
        Me._PageSetupButton = New System.Windows.Forms.ToolStripButton
        Me._Separator2 = New System.Windows.Forms.ToolStripSeparator
        Me._ZoomButton = New System.Windows.Forms.ToolStripSplitButton
        Me._ActualSizeMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._FullPageMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._PageWidthMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._TwoPagesMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._ZoomSeparatorMenuItem = New System.Windows.Forms.ToolStripSeparator
        Me._Zoom500MenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._Zomm200MenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._Zoom150MenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._Zoom100MenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._Zoom75MenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._Zoom50MenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._Zoom25MenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._Zoom10MenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._FirstButton = New System.Windows.Forms.ToolStripButton
        Me._PreviousButton = New System.Windows.Forms.ToolStripButton
        Me._StartPageTextBox = New System.Windows.Forms.ToolStripTextBox
        Me._PageCountLabel = New System.Windows.Forms.ToolStripLabel
        Me._NextButton = New System.Windows.Forms.ToolStripButton
        Me._LastButton = New System.Windows.Forms.ToolStripButton
        Me._Separator1 = New System.Windows.Forms.ToolStripSeparator
        Me._CancelButton = New System.Windows.Forms.ToolStripButton
        Me._Preview = New CoolPrintPreviewControl
        Me._toolStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_toolStrip
        '
        Me._toolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me._toolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._PrintButton, Me._PageSetupButton, Me._Separator2, Me._ZoomButton, Me._FirstButton, Me._PreviousButton, Me._StartPageTextBox, Me._PageCountLabel, Me._NextButton, Me._LastButton, Me._Separator1, Me._CancelButton})
        Me._toolStrip.Location = New System.Drawing.Point(0, 0)
        Me._toolStrip.Name = "_toolStrip"
        Me._toolStrip.Size = New System.Drawing.Size(532, 25)
        Me._toolStrip.TabIndex = 1
        Me._toolStrip.Text = "toolStrip1"
        '
        '_PrintButton
        '
        Me._PrintButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._PrintButton.Image = CType(resources.GetObject("PrinterImage"), System.Drawing.Image)
        Me._PrintButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._PrintButton.Name = "_PrintButton"
        Me._PrintButton.Size = New System.Drawing.Size(23, 22)
        Me._PrintButton.Text = "Print Document"
        '
        '_PageSetupButton
        '
        Me._PageSetupButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._PageSetupButton.Image = CType(resources.GetObject("PageSetupImage"), System.Drawing.Image)
        Me._PageSetupButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._PageSetupButton.Name = "_PageSetupButton"
        Me._PageSetupButton.Size = New System.Drawing.Size(23, 22)
        Me._PageSetupButton.Text = "Page Setup"
        '
        '_Separator2
        '
        Me._Separator2.Name = "_Separator2"
        Me._Separator2.Size = New System.Drawing.Size(6, 25)
        '
        '_ZoomButton
        '
        Me._ZoomButton.AutoToolTip = False
        Me._ZoomButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ActualSizeMenuItem, Me._FullPageMenuItem, Me._PageWidthMenuItem, Me._TwoPagesMenuItem, Me._ZoomSeparatorMenuItem, Me._Zoom500MenuItem, Me._Zomm200MenuItem, Me._Zoom150MenuItem, Me._Zoom100MenuItem, Me._Zoom75MenuItem, Me._Zoom50MenuItem, Me._Zoom25MenuItem, Me._Zoom10MenuItem})
        Me._ZoomButton.Image = CType(resources.GetObject("ZoomImage"), System.Drawing.Image)
        Me._ZoomButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ZoomButton.Name = "_ZoomButton"
        Me._ZoomButton.Size = New System.Drawing.Size(77, 22)
        Me._ZoomButton.Text = "&Zoom"
        '
        '_ActualSizeMenuItem
        '
        Me._ActualSizeMenuItem.Image = CType(resources.GetObject("ActualSizeImage"), System.Drawing.Image)
        Me._ActualSizeMenuItem.Name = "_ActualSizeMenuItem"
        Me._ActualSizeMenuItem.Size = New System.Drawing.Size(150, 22)
        Me._ActualSizeMenuItem.Text = "Actual Size"
        '
        '_FullPageMenuItem
        '
        Me._FullPageMenuItem.Image = CType(resources.GetObject("FullPageImage"), System.Drawing.Image)
        Me._FullPageMenuItem.Name = "_FullPageMenuItem"
        Me._FullPageMenuItem.Size = New System.Drawing.Size(150, 22)
        Me._FullPageMenuItem.Text = "Full Page"
        '
        '_PageWidthMenuItem
        '
        Me._PageWidthMenuItem.Image = CType(resources.GetObject("PageWidthImage"), System.Drawing.Image)
        Me._PageWidthMenuItem.Name = "_PageWidthMenuItem"
        Me._PageWidthMenuItem.Size = New System.Drawing.Size(150, 22)
        Me._PageWidthMenuItem.Text = "Page Width"
        '
        '_TwoPagesMenuItem
        '
        Me._TwoPagesMenuItem.Image = CType(resources.GetObject("TwoPagesImage"), System.Drawing.Image)
        Me._TwoPagesMenuItem.Name = "_TwoPagesMenuItem"
        Me._TwoPagesMenuItem.Size = New System.Drawing.Size(150, 22)
        Me._TwoPagesMenuItem.Text = "Two Pages"
        '
        '_ZoomSeparatorMenuItem
        '
        Me._ZoomSeparatorMenuItem.Name = "_ZoomSeparatorMenuItem"
        Me._ZoomSeparatorMenuItem.Size = New System.Drawing.Size(147, 6)
        '
        '_Zoom500MenuItem
        '
        Me._Zoom500MenuItem.Name = "_Zoom500MenuItem"
        Me._Zoom500MenuItem.Size = New System.Drawing.Size(150, 22)
        Me._Zoom500MenuItem.Text = "500%"
        '
        '_Zomm200MenuItem
        '
        Me._Zomm200MenuItem.Name = "_Zomm200MenuItem"
        Me._Zomm200MenuItem.Size = New System.Drawing.Size(150, 22)
        Me._Zomm200MenuItem.Text = "200%"
        '
        '_Zoom150MenuItem
        '
        Me._Zoom150MenuItem.Name = "_Zoom150MenuItem"
        Me._Zoom150MenuItem.Size = New System.Drawing.Size(150, 22)
        Me._Zoom150MenuItem.Text = "150%"
        '
        '_Zoom100MenuItem
        '
        Me._Zoom100MenuItem.Name = "_Zoom100MenuItem"
        Me._Zoom100MenuItem.Size = New System.Drawing.Size(150, 22)
        Me._Zoom100MenuItem.Text = "100%"
        '
        '_Zoom75MenuItem
        '
        Me._Zoom75MenuItem.Name = "_Zoom75MenuItem"
        Me._Zoom75MenuItem.Size = New System.Drawing.Size(150, 22)
        Me._Zoom75MenuItem.Text = "75%"
        '
        '_Zoom50MenuItem
        '
        Me._Zoom50MenuItem.Name = "_Zoom50MenuItem"
        Me._Zoom50MenuItem.Size = New System.Drawing.Size(150, 22)
        Me._Zoom50MenuItem.Text = "50%"
        '
        '_Zoom25MenuItem
        '
        Me._Zoom25MenuItem.Name = "_Zoom25MenuItem"
        Me._Zoom25MenuItem.Size = New System.Drawing.Size(150, 22)
        Me._Zoom25MenuItem.Text = "25%"
        '
        '_Zoom10MenuItem
        '
        Me._Zoom10MenuItem.Name = "_Zoom10MenuItem"
        Me._Zoom10MenuItem.Size = New System.Drawing.Size(150, 22)
        Me._Zoom10MenuItem.Text = "10%"
        '
        '_FirstButton
        '
        Me._FirstButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._FirstButton.Image = CType(resources.GetObject("FirstImage"), System.Drawing.Image)
        Me._FirstButton.ImageTransparentColor = System.Drawing.Color.Red
        Me._FirstButton.Name = "_FirstButton"
        Me._FirstButton.Size = New System.Drawing.Size(23, 22)
        Me._FirstButton.Text = "First Page"
        '
        '_PreviousButton
        '
        Me._PreviousButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._PreviousButton.Image = CType(resources.GetObject("PreviousImage"), System.Drawing.Image)
        Me._PreviousButton.ImageTransparentColor = System.Drawing.Color.Red
        Me._PreviousButton.Name = "_PreviousButton"
        Me._PreviousButton.Size = New System.Drawing.Size(23, 22)
        Me._PreviousButton.Text = "Previous Page"
        '
        '_StartPageTextBox
        '
        Me._StartPageTextBox.AutoSize = False
        Me._StartPageTextBox.Name = "_StartPageTextBox"
        Me._StartPageTextBox.Size = New System.Drawing.Size(39, 24)
        Me._StartPageTextBox.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        '_PageCountLabel
        '
        Me._PageCountLabel.Name = "_PageCountLabel"
        Me._PageCountLabel.Size = New System.Drawing.Size(13, 22)
        Me._PageCountLabel.Text = " "
        '
        '_NextButton
        '
        Me._NextButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._NextButton.Image = CType(resources.GetObject("NextImage"), System.Drawing.Image)
        Me._NextButton.ImageTransparentColor = System.Drawing.Color.Red
        Me._NextButton.Name = "_NextButton"
        Me._NextButton.Size = New System.Drawing.Size(23, 22)
        Me._NextButton.Text = "Next Page"
        '
        '_LastButton
        '
        Me._LastButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._LastButton.Image = CType(resources.GetObject("LastImage"), System.Drawing.Image)
        Me._LastButton.ImageTransparentColor = System.Drawing.Color.Red
        Me._LastButton.Name = "_LastButton"
        Me._LastButton.Size = New System.Drawing.Size(23, 22)
        Me._LastButton.Text = "Last Page"
        '
        '_Separator1
        '
        Me._Separator1.Name = "_Separator1"
        Me._Separator1.Size = New System.Drawing.Size(6, 25)
        Me._Separator1.Visible = False
        '
        '_CancelButton
        '
        Me._CancelButton.AutoToolTip = False
        Me._CancelButton.Image = CType(resources.GetObject("CancelImage"), System.Drawing.Image)
        Me._CancelButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._CancelButton.Name = "_CancelButton"
        Me._CancelButton.Size = New System.Drawing.Size(70, 22)
        Me._CancelButton.Text = "Cancel"
        '
        '_Preview
        '
        Me._Preview.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Preview.Document = Nothing
        Me._Preview.Location = New System.Drawing.Point(0, 25)
        Me._Preview.Name = "_Preview"
        Me._Preview.Size = New System.Drawing.Size(532, 410)
        Me._Preview.TabIndex = 2
        Me._Preview.ZoomMode = ZoomMode.FullPage
        '
        'CoolPrintPreviewDialog
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(120.0!, 120.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.ClientSize = New System.Drawing.Size(532, 435)
        Me.Controls.Add(Me._Preview)
        Me.Controls.Add(Me._toolStrip)
        Me.Name = "CoolPrintPreviewDialog"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Print Preview"
        Me._toolStrip.ResumeLayout(False)
        Me._toolStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _ToolStrip As System.Windows.Forms.ToolStrip
    Private WithEvents _PrintButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _PageSetupButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _Separator2 As System.Windows.Forms.ToolStripSeparator
    Private WithEvents _ZoomButton As System.Windows.Forms.ToolStripSplitButton
    Private WithEvents _ActualSizeMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _FullPageMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _PageWidthMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _TwoPagesMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _ZoomSeparatorMenuItem As System.Windows.Forms.ToolStripSeparator
    Private WithEvents _Zoom500MenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _Zomm200MenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _Zoom150MenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _Zoom100MenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _Zoom75MenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _Zoom50MenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _Zoom25MenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _Zoom10MenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _FirstButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _PreviousButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _StartPageTextBox As System.Windows.Forms.ToolStripTextBox
    Private WithEvents _PageCountLabel As System.Windows.Forms.ToolStripLabel
    Private WithEvents _NextButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _LastButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _Separator1 As System.Windows.Forms.ToolStripSeparator
    Private WithEvents _CancelButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _Preview As CoolPrintPreviewControl
End Class
