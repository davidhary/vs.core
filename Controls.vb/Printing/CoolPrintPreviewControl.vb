Imports System.ComponentModel
Imports System.Drawing.Imaging
Imports System.Drawing.Printing

Imports isr.Core.Controls.ExceptionExtensions

''' <summary> Cool print preview control. </summary>
''' <remarks>
''' (c) 2009 Bernardo Castillo. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 8/5/2009 from Bernardo Castilho </para><para>
''' http://www.codeproject.com/Articles/38758/An-Enhanced-PrintPreviewDialog. </para>
''' </remarks>
<System.ComponentModel.Description("Print Preview Control")>
Public Class CoolPrintPreviewControl
    Inherits UserControl

#Region " CONSTRUCTOR "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        MyBase.New()
        Me._Himm2pix = New PointF(-1.0!, -1.0!)
        Me._PageImages = New List(Of Image)
        Me.BackColor = SystemColors.AppWorkspace
        Me.ZoomMode = ZoomMode.FullPage
        Me.StartPage = 0
        Me.SetStyle(ControlStyles.OptimizedDoubleBuffer, True)
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the CoolPrintPreviewVB.CoolPrintPreviewControl and
    ''' optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me._BackBrush IsNot Nothing Then Me._BackBrush.Dispose() : Me._BackBrush = Nothing
                Me.RemovePageCountChangedEventHandler(Me.PageCountChangedEvent)
                Me.RemoveStartPageChangedEventHandler(Me.StartPageChangedEvent)
                Me.RemoveZoomModeChangedEventHandler(Me.ZoomModeChangedEvent)
                Me._PageImages?.Clear() : Me._PageImages = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " EVENT HANDLERS "

    ''' <summary> Print document end print. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Print event information. </param>
    Private Sub PrintDocumentEndPrint(ByVal sender As Object, ByVal e As PrintEventArgs)
        Me.SyncPageImages(True)
    End Sub

    ''' <summary> Print document print page. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Print page event information. </param>
    Private Sub PrintDocumentPrintPage(ByVal sender As Object, ByVal e As PrintPageEventArgs)
        If e Is Nothing Then Return
        Me.SyncPageImages(False)
        If Me._Cancel Then
            e.Cancel = True
        End If
    End Sub

#End Region

#Region " FUNCTIONS "

    ''' <summary> True to cancel. </summary>
    Private _Cancel As Boolean

    ''' <summary> Cancels printing. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub Cancel()
        Me._Cancel = True
    End Sub

    ''' <summary> Gets an image. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="page"> The page. </param>
    ''' <returns> The image. </returns>
    Private Function GetImage(ByVal page As Integer) As Image
        Return If((page > -1) AndAlso (page < Me.PageCount), Me._PageImages.Item(page), Nothing)
    End Function

    ''' <summary> Gets image rectangle. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="img"> The image. </param>
    ''' <returns> The image rectangle. </returns>
    Private Function GetImageRectangle(ByVal img As Image) As Rectangle

        Dim sz As Size = Me.GetImageSizeInPixels(img)
        Dim rc As New Rectangle(0, 0, sz.Width, sz.Height)

        Dim rcCli As Rectangle = Me.ClientRectangle
        Select Case Me._ZoomMode
            Case ZoomMode.ActualSize
                Me._Zoom = 1
            Case ZoomMode.FullPage
                Me._Zoom = Math.Min(CDbl(IIf(rc.Width > 0, CDbl(rcCli.Width) / CDbl(rc.Width), 0)),
                                    CDbl(IIf(rc.Height > 0, CDbl(rcCli.Height) / CDbl(rc.Height), 0)))
            Case ZoomMode.PageWidth
                Me._Zoom = CDbl(IIf(rc.Width > 0, CDbl(rcCli.Width) / CDbl(rc.Width), 0))
            Case ZoomMode.TwoPages
                rc.Width *= 2
                Me._Zoom = Math.Min(CDbl(IIf(rc.Width > 0, CDbl(rcCli.Width) / CDbl(rc.Width), 0)),
                                    CDbl(IIf(rc.Height > 0, CDbl(rcCli.Height) / CDbl(rc.Height), 0)))
            Case Else
        End Select
        ' not used Dim zoomX As Double = CDbl(IIf((rc.Width > 0), (CDbl(rcCli.Width) / CDbl(rc.Width)), 0))
        ' not used Dim zoomY As Double = CDbl(IIf((rc.Height > 0), (CDbl(rcCli.Height) / CDbl(rc.Height)), 0))
        rc.Width = CInt(rc.Width * Me._Zoom)
        rc.Height = CInt(rc.Height * Me._Zoom)
        Dim dx As Integer = CInt((rcCli.Width - rc.Width) / 2)
        If dx > 0 Then
            rc.X += dx
        End If
        Dim dy As Integer = CInt((rcCli.Height - rc.Height) / 2)
        If dy > 0 Then
            rc.Y += dy
        End If
        rc.Inflate(-4, -4)
        If Me._ZoomMode = ZoomMode.TwoPages Then
            rc.Inflate(-2, 0)
        End If
        Return rc
    End Function

    ''' <summary> The himm 2pix. </summary>
    Private _Himm2pix As PointF

    ''' <summary> Gets image size in pixels. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="img"> The image. </param>
    ''' <returns> The image size in pixels. </returns>
    Private Function GetImageSizeInPixels(ByVal img As Image) As Size
        Dim szf As SizeF = img.PhysicalDimension
        If TypeOf img Is Metafile Then
            If Me._Himm2pix.X < 0.0! Then
                Using g As Graphics = Me.CreateGraphics
                    Me._Himm2pix.X = g.DpiX / 2540.0!
                    Me._Himm2pix.Y = g.DpiY / 2540.0!
                End Using
            End If
            szf.Width *= Me._Himm2pix.X
            szf.Height *= Me._Himm2pix.Y
        End If
        Return Size.Truncate(szf)
    End Function

    ''' <summary>
    ''' Determines whether the specified key is a regular input key or a special key that requires
    ''' preprocessing.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="keyData"> One of the <see cref="T:System.Windows.Forms.Keys" /> values. </param>
    ''' <returns> true if the specified key is a regular input key; otherwise, false. </returns>
    Protected Overrides Function IsInputKey(ByVal keyData As Keys) As Boolean
        Select Case keyData
            Case Keys.Prior, Keys.Next, Keys.End, Keys.Home, Keys.Left, Keys.Up, Keys.Right, Keys.Down
                Return True
        End Select
        Return MyBase.IsInputKey(keyData)
    End Function

#End Region

#Region " OVERRIDES "

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.KeyDown" /> event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.KeyEventArgs" /> that contains the event
    '''                  data. </param>
    Protected Overrides Sub OnKeyDown(ByVal e As KeyEventArgs)
        If e Is Nothing Then Return
        MyBase.OnKeyDown(e)
        If e.Handled Then Return

        Select Case e.KeyCode

            ' arrow keys scroll or browse, depending on ZoomMode
            Case Keys.Left, Keys.Up, Keys.Right, Keys.Down

                ' browse
                If Me.ZoomMode = ZoomMode.FullPage OrElse Me.ZoomMode = ZoomMode.TwoPages Then
                    Select Case e.KeyCode
                        Case Keys.Left, Keys.Up
                            Me.StartPage -= 1

                        Case Keys.Right, Keys.Down
                            Me.StartPage += 1
                    End Select
                End If

                ' scroll
                Dim pt As Point = Me.AutoScrollPosition
                Select Case e.KeyCode
                    Case Keys.Left
                        pt.X += 20
                    Case Keys.Right
                        pt.X -= 20
                    Case Keys.Up
                        pt.Y += 20
                    Case Keys.Down
                        pt.Y -= 20
                End Select
                Me.AutoScrollPosition = New Point(-pt.X, -pt.Y)

                ' page up/down browse pages
            Case Keys.PageUp
                Me.StartPage -= 1
            Case Keys.PageDown
                Me.StartPage += 1

                ' home/end 
            Case Keys.Home
                Me.AutoScrollPosition = Point.Empty
                Me.StartPage = 0
            Case Keys.End
                Me.AutoScrollPosition = Point.Empty
                Me.StartPage = Me.PageCount - 1

            Case Else
                Return
        End Select

        ' if we got here, the event was handled
        e.Handled = True
    End Sub

    ''' <summary> The point last. </summary>
    Private _PtLast As Point

    ''' <summary> Raises the mouse event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnMouseDown(ByVal e As MouseEventArgs)
        If e Is Nothing Then Return
        MyBase.OnMouseDown(e)
        If (e.Button = MouseButtons.Left) AndAlso (Me.AutoScrollMinSize <> Size.Empty) Then
            Me.Cursor = Cursors.NoMove2D
            Me._PtLast = New Point(e.X, e.Y)
        End If
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.MouseMove" /> event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnMouseMove(ByVal e As MouseEventArgs)
        If e Is Nothing Then Return
        MyBase.OnMouseMove(e)
        If Me.Cursor Is Cursors.NoMove2D Then
            Dim dx As Integer = e.X - Me._PtLast.X
            Dim dy As Integer = e.Y - Me._PtLast.Y
            If (dx <> 0) OrElse (dy <> 0) Then
                Dim pt As Point = Me.AutoScrollPosition
                Me.AutoScrollPosition = New Point(-(pt.X + dx), -(pt.Y + dy))
                Me._PtLast = New Point(e.X, e.Y)
            End If
        End If
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.MouseUp" /> event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnMouseUp(ByVal e As MouseEventArgs)
        If e Is Nothing Then Return
        MyBase.OnMouseUp(e)
        If (e.Button = MouseButtons.Left) AndAlso (Me.Cursor Is Cursors.NoMove2D) Then
            Me.Cursor = Cursors.Default
        End If
    End Sub

    ''' <summary> Event queue for all listeners interested in PageCountChanged events. </summary>
    Public Event PageCountChanged As EventHandler(Of System.EventArgs)

    ''' <summary> Removes event handler. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The handler. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub RemovePageCountChangedEventHandler(ByVal value As EventHandler(Of EventArgs))
#Disable Warning CA1825 ' Avoid zero-length array allocations.
        For Each d As [Delegate] In If(value Is Nothing, New [Delegate]() {}, value.GetInvocationList)
#Enable Warning CA1825 ' Avoid zero-length array allocations.
            Try
                RemoveHandler Me.PageCountChanged, CType(d, EventHandler(Of EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

    ''' <summary> Raises the page count changed event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Sub OnPageCountChanged(ByVal e As EventArgs)
        RaiseEvent PageCountChanged(Me, e)
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.Paint" /> event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnPaint(ByVal e As PaintEventArgs)
        If e Is Nothing Then Return
        Dim img As Image = Me.GetImage(Me.StartPage)
        If Not img Is Nothing Then
            Dim rc As Rectangle = Me.GetImageRectangle(img)
            If (rc.Width > 2) AndAlso (rc.Height > 2) Then
                rc.Offset(Me.AutoScrollPosition)
                If Me._ZoomMode <> ZoomMode.TwoPages Then
                    CoolPrintPreviewControl.RenderPage(e.Graphics, img, rc)
                Else
                    rc.Width = CInt((rc.Width - 4) / 2)
                    CoolPrintPreviewControl.RenderPage(e.Graphics, img, rc)
                    img = Me.GetImage(Me.StartPage + 1)
                    If Not img Is Nothing Then
                        rc = Me.GetImageRectangle(img)
                        rc.Width = CInt((rc.Width - 4) / 2)
                        rc.Offset(rc.Width + 4, 0)
                        CoolPrintPreviewControl.RenderPage(e.Graphics, img, rc)
                    End If
                End If
            End If
        End If
        e.Graphics.FillRectangle(Me._BackBrush, Me.ClientRectangle)
    End Sub

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.Control.SizeChanged" /> event.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnSizeChanged(ByVal e As EventArgs)
        Me.UpdateScrollBars()
        MyBase.OnSizeChanged(e)
    End Sub

    ''' <summary> Event queue for all listeners interested in StartPageChanged events. </summary>
    Public Event StartPageChanged As EventHandler(Of System.EventArgs)

    ''' <summary> Removes event handler. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The handler. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub RemoveStartPageChangedEventHandler(ByVal value As EventHandler(Of EventArgs))
#Disable Warning CA1825 ' Avoid zero-length array allocations.
        For Each d As [Delegate] In If(value Is Nothing, New [Delegate]() {}, value.GetInvocationList)
#Enable Warning CA1825 ' Avoid zero-length array allocations.
            Try
                RemoveHandler Me.StartPageChanged, CType(d, EventHandler(Of EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

    ''' <summary> Raises the start page changed event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Sub OnStartPageChanged(ByVal e As EventArgs)
        RaiseEvent StartPageChanged(Me, e)
    End Sub

    ''' <summary> Event queue for all listeners interested in ZoomModeChanged events. </summary>
    Public Event ZoomModeChanged As EventHandler(Of System.EventArgs)

    ''' <summary> Removes event handler. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The handler. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub RemoveZoomModeChangedEventHandler(ByVal value As EventHandler(Of EventArgs))
#Disable Warning CA1825 ' Avoid zero-length array allocations.
        For Each d As [Delegate] In If(value Is Nothing, New [Delegate]() {}, value.GetInvocationList)
#Enable Warning CA1825 ' Avoid zero-length array allocations.
            Try
                RemoveHandler Me.ZoomModeChanged, CType(d, EventHandler(Of EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

    ''' <summary> Raises the zoom mode changed event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Sub OnZoomModeChanged(ByVal e As EventArgs)
        RaiseEvent ZoomModeChanged(Me, e)
    End Sub

#End Region

#Region " PRINT FUNCTIONS "

    ''' <summary> Prints this object. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub Print()
        Dim ps As PrinterSettings = Me._PrintDocument.PrinterSettings
        Dim first As Integer = ps.MinimumPage - 1
        Dim last As Integer = ps.MaximumPage - 1
        Select Case ps.PrintRange
            Case PrintRange.AllPages
                Me.Document.Print()
                Return
            Case PrintRange.Selection
                first = Me.StartPage
                last = Me.StartPage
                If Me.ZoomMode = ZoomMode.TwoPages Then
                    last = Math.Min(CInt(first + 1), CInt(Me.PageCount - 1))
                End If
                Exit Select
            Case PrintRange.SomePages
                first = ps.FromPage - 1
                last = ps.ToPage - 1
                Exit Select
            Case PrintRange.CurrentPage
                first = Me.StartPage
                last = Me.StartPage
                Exit Select
        End Select
        Using dp As DocumentPrinter = New DocumentPrinter(Me, first, last)
            dp.Print()
        End Using
    End Sub

    ''' <summary> Refresh preview. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub RefreshPreview()
        If Not Me._PrintDocument Is Nothing Then
            Me._PageImages.Clear()
            Dim savePC As PrintController = Me._PrintDocument.PrintController
            Try
                Me._Cancel = False
                Me._IsRendering = True
                Me._PrintDocument.PrintController = New PreviewPrintController
                AddHandler Me._PrintDocument.PrintPage, New PrintPageEventHandler(AddressOf Me.PrintDocumentPrintPage)
                AddHandler Me._PrintDocument.EndPrint, New PrintEventHandler(AddressOf Me.PrintDocumentEndPrint)
                Me._PrintDocument.Print()
            Finally
                Me._Cancel = False
                Me._IsRendering = False
                RemoveHandler Me._PrintDocument.PrintPage, New PrintPageEventHandler(AddressOf Me.PrintDocumentPrintPage)
                RemoveHandler Me._PrintDocument.EndPrint, New PrintEventHandler(AddressOf Me.PrintDocumentEndPrint)
                Me._PrintDocument.PrintController = savePC
            End Try
        End If
        Me.OnPageCountChanged(EventArgs.Empty)
        Me.UpdatePreview()
        Me.UpdateScrollBars()
    End Sub

    ''' <summary> Renders the page. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="g">   The Graphics to process. </param>
    ''' <param name="img"> The image. </param>
    ''' <param name="rc">  The rectangle. </param>
    Private Shared Sub RenderPage(ByVal g As Graphics, ByVal img As Image, ByVal rc As Rectangle)
        If g Is Nothing Then Return
        rc.Offset(1, 1)
        g.DrawRectangle(Pens.Black, rc)
        rc.Offset(-1, -1)
        g.FillRectangle(Brushes.White, rc)
        g.DrawImage(img, rc)
        g.DrawRectangle(Pens.Black, rc)
        rc.Width += 1
        rc.Height += 1
        g.ExcludeClip(rc)
        rc.Offset(1, 1)
        g.ExcludeClip(rc)
    End Sub

    ''' <summary> Synchronization page images. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="lastPageReady"> true to last page ready. </param>
    Private Sub SyncPageImages(ByVal lastPageReady As Boolean)
        Dim pv As PreviewPrintController = DirectCast(Me._PrintDocument.PrintController, PreviewPrintController)
        If Not pv Is Nothing Then
            Dim pageInfo As PreviewPageInfo() = pv.GetPreviewPageInfo
            Dim count As Integer = CInt(IIf(lastPageReady, pageInfo.Length, pageInfo.Length - 1))
            Dim i As Integer
            For i = Me._PageImages.Count To count - 1
                Dim img As Image = pageInfo(i).Image
                Me._PageImages.Add(img)
                Me.OnPageCountChanged(EventArgs.Empty)
                If Me.StartPage < 0 Then
                    Me.StartPage = 0
                End If
                If (i = Me.StartPage) OrElse (i = (Me.StartPage + 1)) Then
                    Me.Refresh()
                End If
                Windows.Forms.Application.DoEvents()
            Next i
        End If
    End Sub

    ''' <summary> Updates the preview. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Sub UpdatePreview()
        If Me._StartPage < 0 Then
            Me._StartPage = 0
        End If
        If Me._StartPage > (Me.PageCount - 1) Then
            Me._StartPage = Me.PageCount - 1
        End If
        Me.Invalidate()
    End Sub

    ''' <summary> Updates the scroll bars. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Sub UpdateScrollBars()
        Dim rc As Rectangle = Rectangle.Empty
        Dim img As Image = Me.GetImage(Me.StartPage)
        If Not img Is Nothing Then
            rc = Me.GetImageRectangle(img)
        End If
        Dim scrollSize As New Size(0, 0)
        Select Case Me._ZoomMode
            Case ZoomMode.ActualSize, ZoomMode.Custom
                scrollSize = New Size(rc.Width + 8, rc.Height + 8)
                Exit Select
            Case ZoomMode.PageWidth
                scrollSize = New Size(0, rc.Height + 8)
                Exit Select
        End Select
        If scrollSize <> Me.AutoScrollMinSize Then
            Me.AutoScrollMinSize = scrollSize
        End If
        Me.UpdatePreview()
    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary> The back brush. </summary>
    Private _BackBrush As Brush

    ''' <summary> Gets or sets the background color for the control. </summary>
    ''' <value>
    ''' A <see cref="T:System.Drawing.Color" /> that represents the background color of the control.
    ''' The default is the value of the
    ''' <see cref="P:System.Windows.Forms.Control.DefaultBackColor" /> property.
    ''' </value>
    <DefaultValue(GetType(Color), "AppWorkspace")>
    Public Overrides Property BackColor() As Color
        Get
            Return MyBase.BackColor
        End Get
        Set(ByVal value As Color)
            MyBase.BackColor = value
            Me._BackBrush = New SolidBrush(value)
        End Set
    End Property

    ''' <summary> The print document. </summary>
    Private _PrintDocument As PrintDocument

    ''' <summary> Gets or sets the document. </summary>
    ''' <value> The document. </value>
    Public Property Document() As PrintDocument
        Get
            Return Me._PrintDocument
        End Get
        Set(ByVal value As PrintDocument)
            If Not value Is Me._PrintDocument Then
                Me._PrintDocument = value
                Me.RefreshPreview()
            End If
        End Set
    End Property

    ''' <summary> True if is rendering, false if not. </summary>
    Private _IsRendering As Boolean

    ''' <summary> Gets a value indicating whether this object is rendering. </summary>
    ''' <value> <c>true</c> if this object is rendering; otherwise <c>false</c> </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property IsRendering() As Boolean
        Get
            Return Me._IsRendering
        End Get
    End Property

    ''' <summary> Gets the number of pages. </summary>
    ''' <value> The number of pages. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public ReadOnly Property PageCount() As Integer
        Get
            Return Me._PageImages.Count
        End Get
    End Property

    ''' <summary> The page images. </summary>
    Private _PageImages As List(Of Image)

    ''' <summary> Gets the page images. </summary>
    ''' <value> The page images. </value>
    <Browsable(False)>
    Public ReadOnly Property PageImages() As PageImageCollection
        Get
            Return New PageImageCollection(Me._PageImages.ToArray)
        End Get
    End Property

    ''' <summary> The start page. </summary>
    Private _StartPage As Integer

    ''' <summary> Gets or sets the start page number. </summary>
    ''' <value> The Zero-Based start page number. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property StartPage() As Integer
        Get
            Return Me._StartPage
        End Get
        Set(ByVal value As Integer)
            If value > (Me.PageCount - 1) Then
                value = Me.PageCount - 1
            End If
            If value < 0 Then
                value = 0
            End If
            If value <> Me._StartPage Then
                Me._StartPage = value
                Me.UpdateScrollBars()
                Me.OnStartPageChanged(EventArgs.Empty)
            End If
        End Set
    End Property

    ''' <summary> The zoom. </summary>
    Private _Zoom As Double

    ''' <summary> Gets or sets the zoom. </summary>
    ''' <value> The zoom. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property Zoom() As Double
        Get
            Return Me._Zoom
        End Get
        Set(ByVal value As Double)
            If (value <> Me._Zoom) OrElse (Me.ZoomMode <> ZoomMode.Custom) Then
                Me.ZoomMode = ZoomMode.Custom
                Me._Zoom = value
                Me.UpdateScrollBars()
                Me.OnZoomModeChanged(EventArgs.Empty)
            End If
        End Set
    End Property

    ''' <summary> The zoom mode. </summary>
    Private _ZoomMode As ZoomMode

    ''' <summary> Gets or sets the zoom mode. </summary>
    ''' <value> The zoom mode. </value>
    <DefaultValue(1)>
    Public Property ZoomMode() As ZoomMode
        Get
            Return Me._ZoomMode
        End Get
        Set(ByVal value As ZoomMode)
            If value <> Me._ZoomMode Then
                Me._ZoomMode = value
                Me.UpdateScrollBars()
                Me.OnZoomModeChanged(EventArgs.Empty)
            End If
        End Set
    End Property

#End Region

#Region " DOCUMENT PRINTER CLASS "

    ''' <summary> Document printer. </summary>
    ''' <remarks>
    ''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 5/2/2015 </para>
    ''' </remarks>
    Friend Class DocumentPrinter
        Inherits PrintDocument

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 2020-09-24. </remarks>
        ''' <param name="preview"> The preview. </param>
        ''' <param name="first">   The first. </param>
        ''' <param name="last">    The last. </param>
        Public Sub New(ByVal preview As CoolPrintPreviewControl, ByVal first As Integer, ByVal last As Integer)
            Me._First = first
            Me._Last = last
            Me._PageImages = preview.PageImages
            Me.DefaultPageSettings = preview.Document.DefaultPageSettings
            Me.PrinterSettings = preview.Document.PrinterSettings
        End Sub

        ''' <summary> Gets or sets the first. </summary>
        ''' <value> The first. </value>
        Public ReadOnly Property First As Integer

        ''' <summary> Gets or sets the zero-based index of this object. </summary>
        ''' <value> The index. </value>
        Public ReadOnly Property Index As Integer

        ''' <summary>
        ''' Raises the <see cref="E:System.Drawing.Printing.PrintDocument.BeginPrint" /> event. It is
        ''' called after the <see cref="M:System.Drawing.Printing.PrintDocument.Print" /> method is
        ''' called and before the first page of the document prints.
        ''' </summary>
        ''' <remarks> David, 2020-09-24. </remarks>
        ''' <param name="e"> A <see cref="T:System.Drawing.Printing.PrintEventArgs" /> that contains the
        '''                  event data. </param>
        Protected Overrides Sub OnBeginPrint(ByVal e As PrintEventArgs)
            Me._Index = Me._First
        End Sub

        ''' <summary> Gets or sets the page images. </summary>
        ''' <value> The page images. </value>
        Public ReadOnly Property PageImages As PageImageCollection

        ''' <summary> Gets or sets the last. </summary>
        ''' <value> The last. </value>
        Public ReadOnly Property Last As Integer

        ''' <summary>
        ''' Raises the <see cref="E:System.Drawing.Printing.PrintDocument.PrintPage" /> event. It is
        ''' called before a page prints.
        ''' </summary>
        ''' <remarks> David, 2020-09-24. </remarks>
        ''' <param name="e"> A <see cref="T:System.Drawing.Printing.PrintPageEventArgs" /> that contains
        '''                  the event data. </param>
        Protected Overrides Sub OnPrintPage(ByVal e As PrintPageEventArgs)
            If e Is Nothing Then Return
            e.Graphics.PageUnit = GraphicsUnit.Display
            e.Graphics.DrawImage(Me._PageImages.Item(Me._Index), e.PageBounds)
            Me._Index += 1
            e.HasMorePages = Me._Index <= Me._Last
        End Sub

    End Class
#End Region

End Class

''' <summary> Collection of page images. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/2/2015 </para>
''' </remarks>
Public Class PageImageCollection
    Inherits Collections.ObjectModel.ReadOnlyCollection(Of Image)

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="images"> The images. </param>
    Public Sub New(ByVal images As Image())
        MyBase.New(images)
    End Sub
End Class

''' <summary> Values that represent zoom modes. </summary>
''' <remarks> David, 2020-09-24. </remarks>
Public Enum ZoomMode

    ''' <summary> An enum constant representing the actual size option. </summary>
    ActualSize = 0

    ''' <summary> An enum constant representing the full page option. </summary>
    FullPage = 1

    ''' <summary> An enum constant representing the page width option. </summary>
    PageWidth = 2

    ''' <summary> An enum constant representing the two pages option. </summary>
    TwoPages = 3

    ''' <summary> An enum constant representing the custom option. </summary>
    Custom = 4
End Enum
