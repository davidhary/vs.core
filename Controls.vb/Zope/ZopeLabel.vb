''' <summary> A zope label. </summary>
''' <remarks>
''' (c) 2017 Pritam Zope, All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 3/8/2017, 3.1.6276. </para><para>
''' https://www.codeproject.com/Articles/1068043/Creating-Custom-Windows-Forms-in-Csharp-using-Pane.
''' </para>
''' </remarks>
Public Class ZopeLabel
    Inherits System.Windows.Forms.Label

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.Windows.Forms.Label" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        MyBase.New
        Me.ForeColor = Color.Transparent
        Me.Font = New Font(SystemFonts.MessageBoxFont.FontFamily, 9.75!, FontStyle.Regular, GraphicsUnit.Point, CType(0, Byte))
    End Sub

    ''' <summary> The display text. </summary>
    Private _DisplayText As String = "ZopeLabel"

    ''' <summary> Gets or sets the display text. </summary>
    ''' <value> The display text. </value>
    Public Property DisplayText() As String
        Get
            Return Me._DisplayText
        End Get
        Set(ByVal value As String)
            Me._DisplayText = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The start color. </summary>
    Private _StartColor As Color = Color.LightGreen

    ''' <summary> Gets or sets the color of the start. </summary>
    ''' <value> The color of the start. </value>
    Public Property StartColor() As Color
        Get
            Return Me._StartColor
        End Get
        Set(ByVal value As Color)
            Me._StartColor = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The end color. </summary>
    Private _EndColor As Color = Color.DarkBlue

    ''' <summary> Gets or sets the color of the end. </summary>
    ''' <value> The color of the end. </value>
    Public Property EndColor() As Color
        Get
            Return Me._EndColor
        End Get
        Set(ByVal value As Color)
            Me._EndColor = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The start opacity. </summary>
    Private _StartOpacity As Integer = 150

    ''' <summary> Gets or sets the start opacity. </summary>
    ''' <value> The start opacity. </value>
    Public Property StartOpacity() As Integer
        Get
            Return Me._StartOpacity
        End Get
        Set(ByVal value As Integer)
            Me._StartOpacity = value
            If Me._StartOpacity > 255 Then
                Me._StartOpacity = 255
                Me.Invalidate()
            Else
                Me.Invalidate()
            End If
        End Set
    End Property

    ''' <summary> The end opacity. </summary>
    Private _EndOpacity As Integer = 150

    ''' <summary> Gets or sets the end opacity. </summary>
    ''' <value> The end opacity. </value>
    Public Property EndOpacity() As Integer
        Get
            Return Me._EndOpacity
        End Get
        Set(ByVal value As Integer)
            Me._EndOpacity = value
            If Me._EndOpacity > 255 Then
                Me._EndOpacity = 255
                Me.Invalidate()
            Else
                Me.Invalidate()
            End If
        End Set
    End Property

    ''' <summary> The gradient angle. </summary>
    Private _GradientAngle As Integer = 90

    ''' <summary> Gets or sets the gradient angle. </summary>
    ''' <value> The gradient angle. </value>
    Public Property GradientAngle() As Integer
        Get
            Return Me._GradientAngle
        End Get
        Set(ByVal value As Integer)
            Me._GradientAngle = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.Paint" /> event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnPaint(ByVal e As PaintEventArgs)
        If e Is Nothing Then Return
        MyBase.OnPaint(e)
        Me._DisplayText = Me.DisplayText
        Dim c1 As Color = Color.FromArgb(Me.StartOpacity, Me.StartColor)
        Dim c2 As Color = Color.FromArgb(Me.EndOpacity, Me.EndColor)
        Using b As Brush = New System.Drawing.Drawing2D.LinearGradientBrush(New Rectangle(0, 0, 50, 50), c1, c2, Me.GradientAngle)
            e.Graphics.DrawString(Me.DisplayText, Me.Font, b, New Point(0, 0))
        End Using
    End Sub

End Class
