Imports System.Drawing.Drawing2D

''' <summary> A Zope tab control. </summary>
''' <remarks>
''' (c) 2017 Pritam Zope, All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 3/8/2017, 3.1.6276. </para><para>
''' https://www.codeproject.com/Articles/1068043/Creating-Custom-Windows-Forms-in-Csharp-using-Pane.
''' </para>
''' </remarks>
Public Class ZopeTabControl
    Inherits System.Windows.Forms.TabControl

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.Windows.Forms.TabControl" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        MyBase.New
        Me.DrawMode = TabDrawMode.OwnerDrawFixed
        Me.Padding = New System.Drawing.Point(22, 4)
        Me.Font = New Font(SystemFonts.MessageBoxFont.FontFamily, 9.75!, FontStyle.Regular, GraphicsUnit.Point, CType(0, Byte))
    End Sub

    ''' <summary> The active tab start color. </summary>

    Private _ActiveTabStartColor As Color = Color.Yellow

    ''' <summary> Gets or sets the active tab start color. </summary>
    ''' <value> The color of the active tab start. </value>
    Public Property ActiveTabStartColor() As Color
        Get
            Return Me._ActiveTabStartColor
        End Get
        Set(ByVal value As Color)
            Me._ActiveTabStartColor = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The active tab end color. </summary>

    Private _ActiveTabEndColor As Color = Color.DarkOrange

    ''' <summary> Gets or sets the active tab end color. </summary>
    ''' <value> The color of the active tab end. </value>
    Public Property ActiveTabEndColor() As Color
        Get
            Return Me._ActiveTabEndColor
        End Get
        Set(ByVal value As Color)
            Me._ActiveTabEndColor = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The inactive tab start color. </summary>

    Private _InactiveTabStartColor As Color = Color.LightGreen

    ''' <summary> Gets or sets the color of the inactive tab start. </summary>
    ''' <value> The color of the inactive tab start. </value>
    Public Property InactiveTabStartColor() As Color
        Get
            Return Me._InactiveTabStartColor
        End Get
        Set(ByVal value As Color)
            Me._InactiveTabStartColor = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The inactive tab end color. </summary>
    Private _InactiveTabEndColor As Color = Color.DarkBlue

    ''' <summary> Gets or sets the color of the inactive tab end. </summary>
    ''' <value> The color of the inactive tab end. </value>
    Public Property InactiveTabEndColor() As Color
        Get
            Return Me._InactiveTabEndColor
        End Get
        Set(ByVal value As Color)
            Me._InactiveTabEndColor = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The start opacity. </summary>
    Private _StartOpacity As Integer = 150

    ''' <summary> Gets or sets the start opacity. </summary>
    ''' <value> The start opacity. </value>
    Public Property StartOpacity() As Integer
        Get
            Return Me._StartOpacity
        End Get
        Set(ByVal value As Integer)
            Me._StartOpacity = value
            If Me._StartOpacity > 255 Then
                Me._StartOpacity = 255
                Me.Invalidate()
            Else
                Me.Invalidate()
            End If
        End Set
    End Property

    ''' <summary> The end opacity. </summary>

    Private _EndOpacity As Integer = 150

    ''' <summary> Gets or sets the end opacity. </summary>
    ''' <value> The end opacity. </value>
    Public Property EndOpacity() As Integer
        Get
            Return Me._EndOpacity
        End Get
        Set(ByVal value As Integer)
            Me._EndOpacity = value
            If Me._EndOpacity > 255 Then
                Me._EndOpacity = 255
                Me.Invalidate()
            Else
                Me.Invalidate()
            End If
        End Set
    End Property

    ''' <summary> The gradient angle. </summary>
    Private _GradientAngle As Integer = 90

    ''' <summary> Gets or sets the gradient angle. </summary>
    ''' <value> The gradient angle. </value>
    Public Property GradientAngle() As Integer
        Get
            Return Me._GradientAngle
        End Get
        Set(ByVal value As Integer)
            Me._GradientAngle = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The text color. </summary>
    Private _TextColor As Color = Color.Navy

    ''' <summary> Gets or sets the color of the text. </summary>
    ''' <value> The color of the text. </value>
    Public Property TextColor() As Color
        Get
            Return Me._TextColor
        End Get
        Set(ByVal value As Color)
            Me._TextColor = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The close button color. </summary>
    Private _CloseButtonColor As Color = Color.Red

    ''' <summary> Gets or sets the color of the close button. </summary>
    ''' <value> The color of the close button. </value>
    Public Property CloseButtonColor() As Color
        Get
            Return Me._CloseButtonColor
        End Get
        Set(ByVal value As Color)
            Me._CloseButtonColor = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.Paint" /> event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnPaint(ByVal e As PaintEventArgs)
        MyBase.OnPaint(e)
    End Sub

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.TabControl.DrawItem" /> event.
    ''' Draws tab items.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.DrawItemEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnDrawItem(ByVal e As DrawItemEventArgs)
        If e Is Nothing Then Return
        MyBase.OnDrawItem(e)
        Dim rc As Rectangle = Me.GetTabRect(e.Index)

        'if tab is selected
        If String.Equals(Me.SelectedTab.Text, Me.TabPages(e.Index).Text) Then
            Dim c1 As Color = Color.FromArgb(Me._StartOpacity, Me._ActiveTabStartColor)
            Dim c2 As Color = Color.FromArgb(Me._EndOpacity, Me._ActiveTabEndColor)
            Using br As New LinearGradientBrush(rc, c1, c2, Me._GradientAngle)
                e.Graphics.FillRectangle(br, rc)
            End Using
        Else
            Dim c1 As Color = Color.FromArgb(Me._StartOpacity, Me._InactiveTabStartColor)
            Dim c2 As Color = Color.FromArgb(Me._EndOpacity, Me._InactiveTabEndColor)
            Using br As New LinearGradientBrush(rc, c1, c2, Me._GradientAngle)
                e.Graphics.FillRectangle(br, rc)
            End Using
        End If

        Me.TabPages(e.Index).BorderStyle = BorderStyle.FixedSingle
        Me.TabPages(e.Index).ForeColor = SystemColors.ControlText

        'draw close button on tabs

        Dim paddedBounds As Rectangle = e.Bounds
        paddedBounds.Inflate(-5, -4)
        Using b As New SolidBrush(Me._TextColor)
            e.Graphics.DrawString(Me.TabPages(e.Index).Text, Me.Font, b, paddedBounds)
        End Using

        Dim pad As Point = Me.Padding

        'drawing close button to tab items
        Using b As New SolidBrush(Me._CloseButtonColor)
            Using f As New Font(Me.Font.FontFamily, 10, FontStyle.Bold)
                e.Graphics.DrawString("X", f, b, e.Bounds.Right + 1 - 18, e.Bounds.Top + pad.Y - 2)
            End Using
        End Using
        e.DrawFocusRectangle()
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.MouseDown" /> event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnMouseDown(ByVal e As MouseEventArgs)
        If e Is Nothing Then Return
        MyBase.OnMouseDown(e)
        Dim i As Integer = 0
        Do While i < Me.TabPages.Count
            Dim r As Rectangle = Me.GetTabRect(i)
            Dim closeButton As New Rectangle(r.Right + 1 - 15, r.Top + 4, 12, 12)
            If closeButton.Contains(e.Location) Then
                If Me.IsTabClosing() Then Me.TabPages.RemoveAt(i)
                Exit Do
            End If
            i += 1
        Loop
    End Sub

    ''' <summary> Event queue for all listeners interested in TabClosing events. </summary>
    Public Event TabClosing As EventHandler(Of System.ComponentModel.CancelEventArgs)

    ''' <summary> Raises the system. component model. cancel event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Sub OnTabClosing(ByVal e As System.ComponentModel.CancelEventArgs)
        Dim evt As EventHandler(Of System.ComponentModel.CancelEventArgs) = Me.TabClosingEvent
        evt?.Invoke(Me, e)
    End Sub

    ''' <summary> Query if this object is tab closing. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> True if tab closing, false if not. </returns>
    Private Function IsTabClosing() As Boolean
        Dim cancelArgs As New System.ComponentModel.CancelEventArgs
        Me.OnTabClosing(cancelArgs)
        Return Not cancelArgs.Cancel
    End Function

End Class
