﻿''' <summary> A zope menu strip. </summary>
''' <remarks>
''' (c) 2017 Pritam Zope, All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 3/8/2017, 3.1.6276. </para><para>
''' https://www.codeproject.com/Articles/1068043/Creating-Custom-Windows-Forms-in-Csharp-using-Pane.
''' </para>
''' </remarks>
Public Class ZopeMenuStrip
    Inherits MenuStrip


    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.Windows.Forms.MenuStrip" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        Me.Renderer = New ZopeDarkMenuRenderer()
    End Sub
End Class

''' <summary> Zope Dark Menu renderer. </summary>
''' <remarks>
''' (c) 2017 Pritam Zope, All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 3/8/2017, 3.1.6276.  </para><para>
''' https://www.codeproject.com/Articles/1068043/Creating-Custom-Windows-Forms-in-Csharp-using-Pane.
''' </para>
''' </remarks>
Public Class ZopeDarkMenuRenderer

    Inherits ToolStripRenderer

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.ToolStripRenderer.RenderMenuItemBackground" />
    ''' event.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.ToolStripItemRenderEventArgs" /> that
    '''                  contains the event data. </param>
    Protected Overrides Sub OnRenderMenuItemBackground(ByVal e As ToolStripItemRenderEventArgs)
        If e Is Nothing Then Return
        MyBase.OnRenderMenuItemBackground(e)

        If e.Item.Enabled Then
            If e.Item.IsOnDropDown = False AndAlso e.Item.Selected Then
                Dim rect As New Rectangle(0, 0, e.Item.Width - 1, e.Item.Height - 1)
                Dim rect2 As New Rectangle(0, 0, e.Item.Width - 1, e.Item.Height - 1)
                Using b As New SolidBrush(Color.FromArgb(60, 60, 60))
                    e.Graphics.FillRectangle(b, rect)
                End Using
                Using b As New SolidBrush(Color.Black)
                    Using p As New Pen(b)
                        e.Graphics.DrawRectangle(p, rect2)
                    End Using
                End Using
                e.Item.ForeColor = Color.White
            Else
                e.Item.ForeColor = Color.White
            End If

            If e.Item.IsOnDropDown AndAlso e.Item.Selected Then
                Dim rect As New Rectangle(0, 0, e.Item.Width - 1, e.Item.Height - 1)
                Using b As New SolidBrush(Color.FromArgb(60, 60, 60))
                    e.Graphics.FillRectangle(b, rect)
                End Using
                Using b As New SolidBrush(Color.Black)
                    Using p As New Pen(b)
                        e.Graphics.DrawRectangle(p, rect)
                    End Using
                End Using
                e.Item.ForeColor = Color.White
            End If
            If TryCast(e.Item, Windows.Forms.ToolStripMenuItem).DropDown.Visible AndAlso e.Item.IsOnDropDown = False Then
                Dim rect As New Rectangle(0, 0, e.Item.Width - 1, e.Item.Height - 1)
                Dim rect2 As New Rectangle(0, 0, e.Item.Width - 1, e.Item.Height - 1)
                Using b As New SolidBrush(Color.FromArgb(20, 20, 20))
                    e.Graphics.FillRectangle(b, rect)
                End Using
                Using b As New SolidBrush(Color.Black)
                    Using p As New Pen(b)
                        e.Graphics.DrawRectangle(p, rect2)
                    End Using
                End Using
                e.Item.ForeColor = Color.White
            End If
        End If
    End Sub

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.ToolStripRenderer.RenderSeparator" /> event.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.ToolStripSeparatorRenderEventArgs" />
    '''                  that contains the event data. </param>
    Protected Overrides Sub OnRenderSeparator(ByVal e As ToolStripSeparatorRenderEventArgs)
        If e Is Nothing Then Return
        MyBase.OnRenderSeparator(e)
        Dim rect As New Rectangle(30, 3, e.Item.Width - 30, 1)
        Using darkLine As New SolidBrush(Color.FromArgb(30, 30, 30))
            e.Graphics.FillRectangle(darkLine, rect)
        End Using
    End Sub

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.ToolStripRenderer.RenderItemCheck" /> event.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.ToolStripItemImageRenderEventArgs" />
    '''                  that contains the event data. </param>
    Protected Overrides Sub OnRenderItemCheck(ByVal e As ToolStripItemImageRenderEventArgs)
        If e Is Nothing Then Return
        MyBase.OnRenderItemCheck(e)

        If e.Item.Selected Then
            Dim rect As New Rectangle(4, 2, 18, 18)
            Dim rect2 As New Rectangle(5, 3, 16, 16)
            Using b As New SolidBrush(Color.Black)
                e.Graphics.FillRectangle(b, rect)
            End Using
            Using b2 As New SolidBrush(Color.FromArgb(220, 220, 220))
                e.Graphics.FillRectangle(b2, rect2)
            End Using
            e.Graphics.DrawImage(e.Image, New Point(5, 3))
        Else
            Dim rect As New Rectangle(4, 2, 18, 18)
            Dim rect2 As New Rectangle(5, 3, 16, 16)
            Using b As New SolidBrush(Color.White)
                e.Graphics.FillRectangle(b, rect)
            End Using
            Using b2 As New SolidBrush(Color.FromArgb(255, 80, 90, 90))
                e.Graphics.FillRectangle(b2, rect2)
            End Using
            e.Graphics.DrawImage(e.Image, New Point(5, 3))
        End If
    End Sub

    ''' <summary> Draws the item background. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.ToolStripRenderEventArgs" /> that
    '''                  contains the event data. </param>
    Protected Overrides Sub OnRenderImageMargin(ByVal e As ToolStripRenderEventArgs)
        If e Is Nothing Then Return
        MyBase.OnRenderImageMargin(e)

        Dim rect As New Rectangle(0, 0, e.ToolStrip.Width, e.ToolStrip.Height)
        Dim rect3 As New Rectangle(0, 0, 26, e.AffectedBounds.Height)
        Using darkLine As New SolidBrush(Color.FromArgb(20, 20, 20))
            e.Graphics.FillRectangle(darkLine, rect)
            e.Graphics.FillRectangle(darkLine, rect3)
        End Using
        Using b As New SolidBrush(Color.FromArgb(20, 20, 20))
            Using p As New Pen(b)
                e.Graphics.DrawLine(p, 28, 0, 28, e.AffectedBounds.Height)
            End Using
        End Using
        Dim rect2 As New Rectangle(0, 0, e.ToolStrip.Width - 1, e.ToolStrip.Height - 1)
        Using b As New SolidBrush(Color.Black)
            Using p As New Pen(b)
                e.Graphics.DrawRectangle(p, rect2)
            End Using
        End Using
    End Sub

End Class
