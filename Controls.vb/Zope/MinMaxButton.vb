''' <summary> A minimum maximum button. </summary>
''' <remarks>
''' (c) 2017 Pritam Zope, All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 3/8/2017, 3.1.6276. </para><para>
''' https://www.codeproject.com/Articles/1068043/Creating-Custom-Windows-Forms-in-Csharp-using-Pane.
''' </para>
''' </remarks>
Public Class MinMaxButton
    Inherits System.Windows.Forms.Button

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.Windows.Forms.Button" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        MyBase.New
        Me.Size = New System.Drawing.Size(31, 24)
        Me.Font = New Font(SystemFonts.MessageBoxFont.FontFamily, 9.75!, FontStyle.Regular, GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.Color.White
        Me.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Text = "_"
        Me._DisplayText = Me.Text
    End Sub

    ''' <summary> State of the custom form. </summary>
    Private _CustomFormState As CustomFormState

    ''' <summary> Gets or sets the state of the custom form. </summary>
    ''' <value> The custom form state. </value>
    Public Property CustomFormState() As CustomFormState
        Get
            Return Me._CustomFormState
        End Get
        Set(ByVal value As CustomFormState)
            Me._CustomFormState = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The display text. </summary>
    Private _DisplayText As String = "_"

    ''' <summary> Gets or sets the display text. </summary>
    ''' <value> The display text. </value>
    Public Property DisplayText() As String
        Get
            Return Me._DisplayText
        End Get
        Set(ByVal value As String)
            Me._DisplayText = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The busy back color. </summary>
    Private _BusyBackColor As Color = System.Drawing.Color.Gray

    ''' <summary> Gets or sets the color of the busy back. </summary>
    ''' <value> The color of the busy back. </value>
    Public Property BusyBackColor() As Color
        Get
            Return Me._BusyBackColor
        End Get
        Set(ByVal value As Color)
            Me._BusyBackColor = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The mouse hover color. </summary>
    Private _MouseHoverColor As Color = System.Drawing.Color.FromArgb(180, 200, 240)

    ''' <summary> Gets or sets the color of the mouse hover. </summary>
    ''' <value> The color of the mouse hover. </value>
    Public Property MouseHoverColor() As Color
        Get
            Return Me._MouseHoverColor
        End Get
        Set(ByVal value As Color)
            Me._MouseHoverColor = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The mouse click color. </summary>
    Private _MouseClickColor As Color = System.Drawing.Color.FromArgb(160, 180, 200)

    ''' <summary> Gets or sets the color of the mouse click. </summary>
    ''' <value> The color of the mouse click. </value>
    Public Property MouseClickColor() As Color
        Get
            Return Me._MouseClickColor
        End Get
        Set(ByVal value As Color)
            Me._MouseClickColor = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The text location. </summary>
    Private _TextLocation As Point = New Point(6, -20)

    ''' <summary> Gets or sets the text location. </summary>
    ''' <value> The text location. </value>
    Public Property TextLocation As Point
        Get
            Return Me._TextLocation
        End Get
        Set(value As Point)
            Me._TextLocation = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The cached back color. </summary>
    Private _CachedBackColor As Color

    ''' <summary>
    ''' Raises the <see cref="M:System.Windows.Forms.Control.OnMouseEnter(System.EventArgs)" /> event.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnMouseEnter(ByVal e As EventArgs)
        MyBase.OnMouseEnter(e)
        Me._CachedBackColor = Me._BusyBackColor
        Me._BusyBackColor = Me._MouseHoverColor
    End Sub

    ''' <summary>
    ''' Raises the <see cref="M:System.Windows.Forms.Control.OnMouseLeave(System.EventArgs)" /> event.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnMouseLeave(ByVal e As EventArgs)
        MyBase.OnMouseLeave(e)
        Me._BusyBackColor = Me._CachedBackColor
    End Sub

    ''' <summary>
    ''' Raises the
    ''' <see cref="M:System.Windows.Forms.Control.OnMouseDown(System.Windows.Forms.MouseEventArgs)" />
    ''' event.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="mevent"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
    '''                       event data. </param>
    Protected Overrides Sub OnMouseDown(ByVal mevent As MouseEventArgs)
        MyBase.OnMouseDown(mevent)
        Me._BusyBackColor = Me._MouseClickColor
    End Sub

    ''' <summary>
    ''' Raises the
    ''' <see cref="M:System.Windows.Forms.ButtonBase.OnMouseUp(System.Windows.Forms.MouseEventArgs)" />
    ''' event.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="mevent"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
    '''                       event data. </param>
    Protected Overrides Sub OnMouseUp(ByVal mevent As MouseEventArgs)
        MyBase.OnMouseUp(mevent)
        Me._BusyBackColor = Me._CachedBackColor
    End Sub

    ''' <summary>
    ''' Raises the
    ''' <see cref="M:System.Windows.Forms.ButtonBase.OnPaint(System.Windows.Forms.PaintEventArgs)" />
    ''' event.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="pevent"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
    '''                       event data. </param>
    Protected Overrides Sub OnPaint(ByVal pevent As PaintEventArgs)
        If pevent Is Nothing Then Return
        MyBase.OnPaint(pevent)

        Select Case Me._CustomFormState
            Case CustomFormState.Normal
                Using b As New SolidBrush(Me.BusyBackColor)
                    pevent.Graphics.FillRectangle(b, Me.ClientRectangle)
                End Using

                'draw and fill the rectangles of maximized window     
                Using p As New Pen(Me.ForeColor)
                    Using b As New SolidBrush(Me.ForeColor)
                        For i As Integer = 0 To 1
                            pevent.Graphics.DrawRectangle(p, Me.TextLocation.X + i + 1, Me.TextLocation.Y, 10, 10)
                            pevent.Graphics.FillRectangle(b, Me.TextLocation.X + 1, Me.TextLocation.Y - 1, 12, 4)
                        Next i
                    End Using
                End Using

            Case CustomFormState.Maximize
                Using b As New SolidBrush(Me.BusyBackColor)
                    pevent.Graphics.FillRectangle(b, Me.ClientRectangle)
                End Using
                Using p As New Pen(Me.ForeColor)
                    Using b As New SolidBrush(Me.ForeColor)
                        'draw and fill the rectangles of maximized window     
                        For i As Integer = 0 To 1
                            pevent.Graphics.DrawRectangle(p, Me.TextLocation.X + 5, Me.TextLocation.Y, 8, 8)
                            pevent.Graphics.FillRectangle(b, Me.TextLocation.X + 5, Me.TextLocation.Y - 1, 9, 4)
                            pevent.Graphics.DrawRectangle(p, Me.TextLocation.X + 2, Me.TextLocation.Y + 5, 8, 8)
                            pevent.Graphics.FillRectangle(b, Me.TextLocation.X + 2, Me.TextLocation.Y + 4, 9, 4)
                        Next i
                    End Using
                End Using

        End Select

    End Sub


End Class

''' <summary> Values that represent custom form states. </summary>
''' <remarks> David, 2020-09-24. </remarks>
Public Enum CustomFormState

    ''' <summary> An enum constant representing the normal option. </summary>
    Normal

    ''' <summary> An enum constant representing the maximize option. </summary>
    Maximize
End Enum
