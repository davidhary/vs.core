''' <summary> A Zope button. </summary>
''' <remarks>
''' (c) 2017 Pritam Zope, All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 3/8/2017, 3.1.6276. </para><para>
''' https://www.codeproject.com/Articles/1068043/Creating-Custom-Windows-Forms-in-Csharp-using-Pane.
''' </para>
''' </remarks>
Public Class ZopeButton
    Inherits System.Windows.Forms.Button

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.Windows.Forms.Button" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        MyBase.New()
        Me.Size = New System.Drawing.Size(31, 24)
        Me.ForeColor = System.Drawing.Color.White
        Me.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Font = New Font(SystemFonts.MessageBoxFont.FontFamily, 20.25!, FontStyle.Bold, GraphicsUnit.Point, CType(0, Byte))
        Me.Text = "_"
        Me._DisplayText = Me.Text
    End Sub

    ''' <summary> The display text. </summary>
    Private _DisplayText As String = "_"

    ''' <summary> Gets or sets the display text. </summary>
    ''' <value> The display text. </value>
    Public Property DisplayText() As String
        Get
            Return Me._DisplayText
        End Get
        Set(ByVal value As String)
            Me._DisplayText = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The busy back color. </summary>
    Private _BusyBackColor As Color = System.Drawing.Color.Teal

    ''' <summary> Gets or sets the color of the busy back. </summary>
    ''' <value> The color of the busy back. </value>
    Public Property BusyBackColor() As Color
        Get
            Return Me._BusyBackColor
        End Get
        Set(ByVal value As Color)
            Me._BusyBackColor = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The mouse hover color. </summary>
    Private _MouseHoverColor As Color = System.Drawing.Color.FromArgb(0, 0, 140)

    ''' <summary> Gets or sets the color of the mouse hover. </summary>
    ''' <value> The color of the mouse hover. </value>
    Public Property MouseHoverColor() As Color
        Get
            Return Me._MouseHoverColor
        End Get
        Set(ByVal value As Color)
            Me._MouseHoverColor = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The mouse click color. </summary>
    Private _MouseClickColor As Color = System.Drawing.Color.FromArgb(160, 180, 200)

    ''' <summary> Gets or sets the color of the mouse click. </summary>
    ''' <value> The color of the mouse click. </value>
    Public Property MouseClickColor() As Color
        Get
            Return Me._MouseClickColor
        End Get
        Set(ByVal value As Color)
            Me._MouseClickColor = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> True to enable, false to disable the mouse colors. </summary>
    Private _MouseColorsEnabled As Boolean = True

    ''' <summary> Gets or sets the mouse colors enabled. </summary>
    ''' <value> The mouse colors enabled. </value>
    Public Property MouseColorsEnabled() As Boolean
        Get
            Return Me._MouseColorsEnabled
        End Get
        Set(ByVal value As Boolean)
            Me._MouseColorsEnabled = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The text location left. </summary>
    Private _TextLocationLeft As Integer = 6

    ''' <summary> Gets or sets the text location left. </summary>
    ''' <value> The text location left. </value>
    Public Property TextLocationLeft() As Integer
        Get
            Return Me._TextLocationLeft
        End Get
        Set(ByVal value As Integer)
            Me._TextLocationLeft = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The text location top. </summary>
    Private _TextLocationTop As Integer = -20

    ''' <summary> Gets or sets the text location top. </summary>
    ''' <value> The text location top. </value>
    Public Property TextLocationTop() As Integer
        Get
            Return Me._TextLocationTop
        End Get
        Set(ByVal value As Integer)
            Me._TextLocationTop = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The cached color. </summary>
    Private _CachedColor As Color

    ''' <summary>
    ''' Raises the <see cref="M:System.Windows.Forms.Control.OnMouseEnter(System.EventArgs)" /> event.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnMouseEnter(ByVal e As EventArgs)
        MyBase.OnMouseEnter(e)
        Me._CachedColor = Me.BusyBackColor
        Me._BusyBackColor = Me.MouseHoverColor
    End Sub

    ''' <summary>
    ''' Raises the <see cref="M:System.Windows.Forms.Control.OnMouseLeave(System.EventArgs)" /> event.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnMouseLeave(ByVal e As EventArgs)
        MyBase.OnMouseLeave(e)
        If Me._MouseColorsEnabled Then Me._BusyBackColor = Me._CachedColor
    End Sub

    ''' <summary>
    ''' Raises the
    ''' <see cref="M:System.Windows.Forms.Control.OnMouseDown(System.Windows.Forms.MouseEventArgs)" />
    ''' event.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="mevent"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
    '''                       event data. </param>
    Protected Overrides Sub OnMouseDown(ByVal mevent As MouseEventArgs)
        MyBase.OnMouseDown(mevent)
        If Me.MouseColorsEnabled Then Me._BusyBackColor = Me.MouseClickColor
    End Sub

    ''' <summary>
    ''' Raises the
    ''' <see cref="M:System.Windows.Forms.ButtonBase.OnMouseUp(System.Windows.Forms.MouseEventArgs)" />
    ''' event.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="mevent"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
    '''                       event data. </param>
    Protected Overrides Sub OnMouseUp(ByVal mevent As MouseEventArgs)
        MyBase.OnMouseUp(mevent)
        If Me.MouseColorsEnabled Then Me._BusyBackColor = Me._CachedColor
    End Sub

    ''' <summary>
    ''' Raises the
    ''' <see cref="M:System.Windows.Forms.ButtonBase.OnPaint(System.Windows.Forms.PaintEventArgs)" />
    ''' event.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="pevent"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
    '''                       event data. </param>
    Protected Overrides Sub OnPaint(ByVal pevent As PaintEventArgs)
        If pevent Is Nothing Then Return
        MyBase.OnPaint(pevent)
        Me._DisplayText = Me.Text
        If Me._TextLocationLeft = 100 AndAlso Me._TextLocationTop = 25 Then
            Me._TextLocationLeft = (Me.Width \ 3) + 10
            Me._TextLocationTop = (Me.Height \ 2) - 1
        End If

        Dim p As New Point(Me.TextLocationLeft, Me.TextLocationTop)
        Using b As New SolidBrush(Me._BusyBackColor)
            pevent.Graphics.FillRectangle(b, Me.ClientRectangle)
        End Using
        Using b As New SolidBrush(Me.ForeColor)
            pevent.Graphics.DrawString(Me.DisplayText, Me.Font, b, p)
        End Using
    End Sub

End Class
