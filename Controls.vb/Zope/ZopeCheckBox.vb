''' <summary> A zope check box. </summary>
''' <remarks>
''' (c) 2017 Pritam Zope, All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 3/8/2017, 3.1.6276.  </para><para>
''' https://www.codeproject.com/Articles/1068043/Creating-Custom-Windows-Forms-in-Csharp-using-Pane.
''' </para>
''' </remarks>
Public Class ZopeCheckBox
    Inherits System.Windows.Forms.CheckBox

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.Windows.Forms.CheckBox" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        MyBase.New
        Me.ForeColor = Color.White
        Me.AutoSize = False
        Me.Font = New Font(SystemFonts.MessageBoxFont.FontFamily, 9.75!, FontStyle.Regular, GraphicsUnit.Point, CType(0, Byte))
    End Sub

    ''' <summary> The display text. </summary>
    Private _DisplayText As String = String.Empty

    ''' <summary> Gets or sets the display text. </summary>
    ''' <value> The display text. </value>
    Public Property DisplayText() As String
        Get
            Return Me._DisplayText
        End Get
        Set(ByVal value As String)
            Me._DisplayText = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The start color. </summary>
    Private _StartColor As Color = Color.SteelBlue

    ''' <summary> Gets or sets the color of the start. </summary>
    ''' <value> The color of the start. </value>
    Public Property StartColor() As Color
        Get
            Return Me._StartColor
        End Get
        Set(ByVal value As Color)
            Me._StartColor = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The end color. </summary>
    Private _EndColor As Color = Color.DarkBlue

    ''' <summary> Gets or sets the color of the end. </summary>
    ''' <value> The color of the end. </value>
    Public Property EndColor() As Color
        Get
            Return Me._EndColor
        End Get
        Set(ByVal value As Color)
            Me._EndColor = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The mouse hover start color. </summary>
    Private _MouseHoverStartColor As Color = Color.Yellow

    ''' <summary> Gets or sets the color of the mouse hover start. </summary>
    ''' <value> The color of the mouse hover start. </value>
    Public Property MouseHoverStartColor() As Color
        Get
            Return Me._MouseHoverStartColor
        End Get
        Set(ByVal value As Color)
            Me._MouseHoverStartColor = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The mouse hover end color. </summary>
    Private _MouseHoverEndColor As Color = Color.DarkOrange

    ''' <summary> Gets or sets the color of the mouse hover end. </summary>
    ''' <value> The color of the mouse hover end. </value>
    Public Property MouseHoverEndColor() As Color
        Get
            Return Me._MouseHoverEndColor
        End Get
        Set(ByVal value As Color)
            Me._MouseHoverEndColor = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The start opacity. </summary>
    Private _StartOpacity As Integer = 150

    ''' <summary> Gets or sets the start opacity. </summary>
    ''' <value> The start opacity. </value>
    Public Property StartOpacity() As Integer
        Get
            Return Me._StartOpacity
        End Get
        Set(ByVal value As Integer)
            Me._StartOpacity = value
            If Me._StartOpacity > 255 Then
                Me._StartOpacity = 255
                Me.Invalidate()
            Else
                Me.Invalidate()
            End If
        End Set
    End Property

    ''' <summary> The end opacity. </summary>
    Private _EndOpacity As Integer = 150

    ''' <summary> Gets or sets the end opacity. </summary>
    ''' <value> The end opacity. </value>
    Public Property EndOpacity() As Integer
        Get
            Return Me._EndOpacity
        End Get
        Set(ByVal value As Integer)
            Me._EndOpacity = value
            If Me._EndOpacity > 255 Then
                Me._EndOpacity = 255
                Me.Invalidate()
            Else
                Me.Invalidate()
            End If
        End Set
    End Property

    ''' <summary> The gradient angle. </summary>
    Private _GradientAngle As Integer = 90

    ''' <summary> Gets or sets the gradient angle. </summary>
    ''' <value> The gradient angle. </value>
    Public Property GradientAngle() As Integer
        Get
            Return Me._GradientAngle
        End Get
        Set(ByVal value As Integer)
            Me._GradientAngle = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The text location. </summary>
    Private _TextLocation As Point = New Point(14, 4)

    ''' <summary> Gets or sets the text location. </summary>
    ''' <value> The text location. </value>
    Public Property TextLocation As Point
        Get
            Return Me._TextLocation
        End Get
        Set(value As Point)
            Me._TextLocation = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> Size of the box. </summary>
    Private _BoxSize As Size = New Size(18, 18)

    ''' <summary> Gets or sets the box size. </summary>
    ''' <value> The size of the box. </value>
    Public Property BoxSize() As Size
        Get
            Return Me._BoxSize
        End Get
        Set(ByVal value As Size)
            Me._BoxSize = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The box location. </summary>
    Private _BoxLocation As Point = New Point(0, 0)

    ''' <summary> Gets or sets the box location. </summary>
    ''' <value> The box location. </value>
    Public Property BoxLocation As Point
        Get
            Return Me._BoxLocation
        End Get
        Set(value As Point)
            Me._BoxLocation = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The cached start color. </summary>
    Private _CachedStartColor, _CachedEndColor As Color

    ''' <summary>
    ''' Raises the <see cref="M:System.Windows.Forms.Control.OnMouseEnter(System.EventArgs)" /> event.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnMouseEnter(ByVal e As EventArgs)
        MyBase.OnMouseEnter(e)
        Me._CachedStartColor = Me.StartColor
        Me._CachedEndColor = Me.EndColor
        Me._StartColor = Me._MouseHoverStartColor
        Me._EndColor = Me._MouseHoverEndColor
    End Sub

    ''' <summary>
    ''' Raises the <see cref="M:System.Windows.Forms.Control.OnMouseLeave(System.EventArgs)" /> event.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnMouseLeave(ByVal e As EventArgs)
        MyBase.OnMouseLeave(e)
        Me._StartColor = Me._CachedStartColor
        Me._EndColor = Me._CachedEndColor
    End Sub

    ''' <summary>
    ''' Raises the
    ''' <see cref="M:System.Windows.Forms.ButtonBase.OnPaint(System.Windows.Forms.PaintEventArgs)" />
    ''' event.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnPaint(ByVal e As PaintEventArgs)
        If e Is Nothing Then Return
        MyBase.OnPaint(e)
        Me.AutoSize = False
        Me._DisplayText = Me._DisplayText
        If Me.TextLocation.X = 100 AndAlso Me.TextLocation.Y = 25 Then
            Me.TextLocation = New Point((Me.Width \ 3) + 10, (Me.Height \ 2) - 1)
        End If
        'drawing string & filling gradient rectangle
        Dim c1 As Color = Color.FromArgb(Me._StartOpacity, Me._StartColor)
        Dim c2 As Color = Color.FromArgb(Me._EndOpacity, Me._EndColor)
        Using b As Brush = New System.Drawing.Drawing2D.LinearGradientBrush(Me.ClientRectangle, c1, c2, Me._GradientAngle)
            e.Graphics.FillRectangle(b, Me.ClientRectangle)
        End Using
        Dim p As New Point(Me.TextLocation.X, Me.TextLocation.Y)
        Using frcolor As New SolidBrush(Me.ForeColor)
            e.Graphics.DrawString(Me._DisplayText, Me.Font, frcolor, p)
        End Using
        Dim rc As New Rectangle(Me.BoxLocation, Me.BoxSize)
        'drawing check box
        ControlPaint.DrawCheckBox(e.Graphics, rc, If(Me.Checked, ButtonState.Checked, ButtonState.Normal))
    End Sub
End Class


