Imports System.Drawing.Drawing2D

''' <summary> A shaped button. </summary>
''' <remarks>
''' (c) 2017 Pritam Zope, All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 3/8/2017, 3.1.6276. </para><para>
''' https://www.codeproject.com/Articles/1068043/Creating-Custom-Windows-Forms-in-Csharp-using-Pane.
''' </para>
''' </remarks>
Public Class ShapedButton
    Inherits System.Windows.Forms.Button

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.Windows.Forms.Button" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        Me.Size = New Size(100, 40)
        Me.BackColor = Color.Transparent
        Me.FlatStyle = FlatStyle.Flat
        Me.FlatAppearance.BorderSize = 0
        Me.FlatAppearance.MouseOverBackColor = Color.Transparent
        Me.FlatAppearance.MouseDownBackColor = Color.Transparent
        Me._ButtonText = Me.Text
        Me.Font = New Font(SystemFonts.MessageBoxFont.FontFamily, 20.25!, FontStyle.Bold, GraphicsUnit.Point, CType(0, Byte))
    End Sub

#End Region

#Region " SHAPE "

    ''' <summary> The radius percent. </summary>
    Private _RadiusPercent As Integer = 25

    ''' <summary> Gets or sets the radius percent. </summary>
    ''' <value> The radius percent. </value>
    Public Property RadiusPercent As Integer
        Get
            Return Me._RadiusPercent
        End Get
        Set(value As Integer)
            Me._RadiusPercent = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The button shape. </summary>
    Private _ButtonShape As ButtonShape

    ''' <summary> Gets or sets the button shape. </summary>
    ''' <value> The button shape. </value>
    Public Property ButtonShape() As ButtonShape
        Get
            Return Me._ButtonShape
        End Get
        Set(ByVal value As ButtonShape)
            Me._ButtonShape = value
            Me.Invalidate()
        End Set
    End Property

#End Region

#Region " TEXT "

    ''' <summary> The button text. </summary>
    Private _ButtonText As String = String.Empty

    ''' <summary> Gets or sets the button text. </summary>
    ''' <value> The button text. </value>
    Public Property ButtonText() As String
        Get
            Return Me._ButtonText
        End Get
        Set(ByVal value As String)
            Me._ButtonText = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The text location. </summary>
    Private _TextLocation As Point = New Point(100, 25)

    ''' <summary> Gets or sets the text location. </summary>
    ''' <value> The text location. </value>
    Public Property TextLocation As Point
        Get
            Return Me._TextLocation
        End Get
        Set(value As Point)
            Me._TextLocation = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> True to show, false to hide the button text. </summary>
    Private _ShowButtonText As Boolean = True

    ''' <summary> Gets or sets the show button text. </summary>
    ''' <value> The show button text. </value>
    Public Property ShowButtonText() As Boolean
        Get
            Return Me._ShowButtonText
        End Get
        Set(ByVal value As Boolean)
            Me._ShowButtonText = value
            Me.Invalidate()
        End Set
    End Property

#End Region

#Region " BORDER "

    ''' <summary> Width of the border. </summary>
    Private _BorderWidth As Integer = 2

    ''' <summary> Gets or sets the width of the border. </summary>
    ''' <value> The width of the border. </value>
    Public Property BorderWidth() As Integer
        Get
            Return Me._BorderWidth
        End Get
        Set(ByVal value As Integer)
            Me._BorderWidth = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> Sets border color. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="bdrColor"> The bdr color. </param>
    Private Sub SetBorderColor(ByVal bdrColor As Color)
        Dim red As Integer = bdrColor.R - 40
        Dim green As Integer = bdrColor.G - 40
        Dim blue As Integer = bdrColor.B - 40
        If red < 0 Then
            red = 0
        End If
        If green < 0 Then
            green = 0
        End If
        If blue < 0 Then
            blue = 0
        End If

        Me._ButtonBorderColor = Color.FromArgb(red, green, blue)
    End Sub

    ''' <summary> The button border color. </summary>

    Private _ButtonBorderColor As Color = Color.FromArgb(220, 220, 220)

    ''' <summary> The border color. </summary>
    Private _BorderColor As Color = Color.Transparent

    ''' <summary> Gets or sets the color of the border. </summary>
    ''' <value> The color of the border. </value>
    Public Property BorderColor() As Color
        Get
            Return Me._BorderColor
        End Get
        Set(ByVal value As Color)
            Me._BorderColor = value
            If Me._BorderColor = Color.Transparent Then
                Me._ButtonBorderColor = Color.FromArgb(220, 220, 220)
            Else
                Me.SetBorderColor(Me._BorderColor)
            End If

        End Set
    End Property

#End Region

#Region " BUTTON COLORS "

    ''' <summary> The start color. </summary>
    Private _StartColor As Color = Color.DodgerBlue

    ''' <summary> Gets or sets the color of the start. </summary>
    ''' <value> The color of the start. </value>
    Public Property StartColor() As Color
        Get
            Return Me._StartColor
        End Get
        Set(ByVal value As Color)
            Me._StartColor = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The end color. </summary>
    Private _EndColor As Color = Color.MidnightBlue

    ''' <summary> Gets or sets the color of the end. </summary>
    ''' <value> The color of the end. </value>
    Public Property EndColor() As Color
        Get
            Return Me._EndColor
        End Get
        Set(ByVal value As Color)
            Me._EndColor = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The mouse hover start color. </summary>
    Private _MouseHoverStartColor As Color = Color.Turquoise

    ''' <summary> Gets or sets the color of the mouse hover start. </summary>
    ''' <value> The color of the mouse hover start. </value>
    Public Property MouseHoverStartColor() As Color
        Get
            Return Me._MouseHoverStartColor
        End Get
        Set(ByVal value As Color)
            Me._MouseHoverStartColor = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The mouse hover end color. </summary>
    Private _MouseHoverEndColor As Color = Color.DarkSlateGray

    ''' <summary> Gets or sets the color of the mouse hover end. </summary>
    ''' <value> The color of the mouse hover end. </value>
    Public Property MouseHoverEndColor() As Color
        Get
            Return Me._MouseHoverEndColor
        End Get
        Set(ByVal value As Color)
            Me._MouseHoverEndColor = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The mouse click start color. </summary>
    Private _MouseClickStartColor As Color = Color.Yellow

    ''' <summary> Gets or sets the color of the mouse click start. </summary>
    ''' <value> The color of the mouse click start. </value>
    Public Property MouseClickStartColor() As Color
        Get
            Return Me._MouseClickStartColor
        End Get
        Set(ByVal value As Color)
            Me._MouseClickStartColor = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The mouse click end color. </summary>
    Private _MouseClickEndColor As Color = Color.Red

    ''' <summary> Gets or sets the color of the mouse click end. </summary>
    ''' <value> The color of the mouse click end. </value>
    Public Property MouseClickEndColor() As Color
        Get
            Return Me._MouseClickEndColor
        End Get
        Set(ByVal value As Color)
            Me._MouseClickEndColor = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The start opacity. </summary>
    Private _StartOpacity As Integer = 250

    ''' <summary> Gets or sets the start opacity. </summary>
    ''' <value> The start opacity. </value>
    Public Property StartOpacity() As Integer
        Get
            Return Me._StartOpacity
        End Get
        Set(ByVal value As Integer)
            Me._StartOpacity = value
            If Me._StartOpacity > 255 Then
                Me._StartOpacity = 255
                Me.Invalidate()
            Else
                Me.Invalidate()
            End If
        End Set
    End Property

    ''' <summary> The end opacity. </summary>
    Private _EndOpacity As Integer = 250

    ''' <summary> Gets or sets the end opacity. </summary>
    ''' <value> The end opacity. </value>
    Public Property EndOpacity() As Integer
        Get
            Return Me._EndOpacity
        End Get
        Set(ByVal value As Integer)
            Me._EndOpacity = value
            If Me._EndOpacity > 255 Then
                Me._EndOpacity = 255
                Me.Invalidate()
            Else
                Me.Invalidate()
            End If
        End Set
    End Property

    ''' <summary> The gradient angle. </summary>
    Private _GradientAngle As Integer = 90

    ''' <summary> Gets or sets the gradient angle. </summary>
    ''' <value> The gradient angle. </value>
    Public Property GradientAngle() As Integer
        Get
            Return Me._GradientAngle
        End Get
        Set(ByVal value As Integer)
            Me._GradientAngle = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The cached start color. </summary>

    Private _CachedStartColor, _CachedEndColor As Color

    ''' <summary>
    ''' Raises the <see cref="M:System.Windows.Forms.Control.OnMouseEnter(System.EventArgs)" /> event.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnMouseEnter(ByVal e As EventArgs)
        MyBase.OnMouseEnter(e)
        Me._CachedStartColor = Me._StartColor
        Me._CachedEndColor = Me._EndColor
        Me._StartColor = Me._MouseHoverStartColor
        Me._EndColor = Me._MouseHoverEndColor
    End Sub

    ''' <summary>
    ''' Raises the <see cref="M:System.Windows.Forms.Control.OnMouseLeave(System.EventArgs)" /> event.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnMouseLeave(ByVal e As EventArgs)
        MyBase.OnMouseLeave(e)
        Me._StartColor = Me._CachedStartColor
        Me._EndColor = Me._CachedEndColor
        Me.SetBorderColor(Me._BorderColor)
    End Sub

    ''' <summary>
    ''' Raises the
    ''' <see cref="M:System.Windows.Forms.Control.OnMouseDown(System.Windows.Forms.MouseEventArgs)" />
    ''' event.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="mevent"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
    '''                       event data. </param>
    Protected Overrides Sub OnMouseDown(ByVal mevent As MouseEventArgs)
        MyBase.OnMouseDown(mevent)
        Me._StartColor = Me._MouseClickStartColor
        Me._EndColor = Me._MouseClickEndColor

        Me._ButtonBorderColor = Me._BorderColor
        Me.SetBorderColor(Me._BorderColor)
        Me.Invalidate()
    End Sub

    ''' <summary>
    ''' Raises the
    ''' <see cref="M:System.Windows.Forms.ButtonBase.OnMouseUp(System.Windows.Forms.MouseEventArgs)" />
    ''' event.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="mevent"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
    '''                       event data. </param>
    Protected Overrides Sub OnMouseUp(ByVal mevent As MouseEventArgs)
        MyBase.OnMouseUp(mevent)
        Me.OnMouseLeave(mevent)
        Me._StartColor = Me._CachedStartColor
        Me._EndColor = Me._CachedEndColor
        Me.SetBorderColor(Me._BorderColor)
        Me.Invalidate()
    End Sub

    ''' <summary>
    ''' Raises the <see cref="M:System.Windows.Forms.ButtonBase.OnLostFocus(System.EventArgs)" />
    ''' event.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnLostFocus(ByVal e As EventArgs)
        MyBase.OnLostFocus(e)
        Me._StartColor = Me._CachedStartColor
        Me._EndColor = Me._CachedEndColor
        Me.Invalidate()
    End Sub

#End Region

#Region " RENDER "

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.Resize" /> event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnResize(ByVal e As EventArgs)
        MyBase.OnResize(e)
        Me.TextLocation = New Point(CInt((Me.Width \ 3) - 1), CInt((Me.Height \ 3) + 5))
    End Sub

    ''' <summary> Draw circular button. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="g"> The Graphics to process. </param>
    Private Sub DrawCircularButton(ByVal g As Graphics)
        Dim c1 As Color = Color.FromArgb(Me._StartOpacity, Me._StartColor)
        Dim c2 As Color = Color.FromArgb(Me._EndOpacity, Me._EndColor)


        Using b As Brush = New System.Drawing.Drawing2D.LinearGradientBrush(Me.ClientRectangle, c1, c2, Me._GradientAngle)
            g.FillEllipse(b, 5, 5, Me.Width - 10, Me.Height - 10)
        End Using

        Using b As New SolidBrush(Me._ButtonBorderColor)
            Using p As New Pen(b)
                For i As Integer = 0 To Me._BorderWidth - 1
                    g.DrawEllipse(p, 5 + i, 5, Me.Width - 10, Me.Height - 10)
                Next i
            End Using
        End Using


        If Me._ShowButtonText Then
            Dim p As New Point(Me.TextLocation.X, Me.TextLocation.Y)
            Using sb As New SolidBrush(Me.ForeColor)
                g.DrawString(Me._ButtonText, Me.Font, sb, p)
            End Using
        End If
    End Sub

    ''' <summary> Draw round rectangular button. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="g"> The Graphics to process. </param>
    Private Sub DrawRoundRectangularButton(ByVal g As Graphics)

        Dim c1 As Color = Color.FromArgb(Me._StartOpacity, Me._StartColor)
        Dim c2 As Color = Color.FromArgb(Me._EndOpacity, Me._EndColor)

        Dim rec As New Rectangle(5, 5, Me.Width, Me.Height)

        ' create the radius variable and set it equal to 20% the height of the rectangle
        ' this will determine the amount of bend at the corners
        Dim radius As Integer = CInt(Fix(0.01 * Math.Min(rec.Height, rec.Height) * Me.RadiusPercent))

        ' make sure that we have a valid radius, too small and we have a problem
        If radius < 1 Then radius = 2
        Dim diameter As Integer = radius * 2

        ' create an x and y variable so that we can reduce the length of our code lines
        Dim x As Integer = rec.Left
        Dim y As Integer = rec.Top
        Dim h As Integer = rec.Height
        Dim w As Integer = rec.Width

        Using _region As Region = New System.Drawing.Region(rec)
            Using path As New GraphicsPath()
                path.AddArc(x, y, diameter, diameter, 180, 90)
                path.AddArc(w - diameter - x, y, diameter, diameter, 270, 90)
                path.AddArc(w - diameter - x, h - diameter - y, diameter, diameter, 0, 90)
                path.AddArc(x, h - diameter - y, diameter, diameter, 90, 90)
                path.CloseFigure()
                _region.Intersect(path)
            End Using

            Using b As Brush = New System.Drawing.Drawing2D.LinearGradientBrush(Me.ClientRectangle, c1, c2, Me._GradientAngle)
                g.FillRegion(b, _region)
            End Using
        End Using


        Using p As New Pen(Me._ButtonBorderColor)
            For i As Integer = 0 To Me._BorderWidth - 1
                g.DrawArc(p, x + i, y + i, diameter, diameter, 180, 90)
                g.DrawLine(p, x + radius, y + i, Me.Width - x - radius, y + i)
                g.DrawArc(p, Me.Width - diameter - x - i, y + i, diameter, diameter, 270, 90)
                g.DrawLine(p, x + i, y + radius, x + i, Me.Height - y - radius)

                g.DrawLine(p, Me.Width - x - i, y + radius, Me.Width - x - i, Me.Height - y - radius)
                g.DrawArc(p, Me.Width - diameter - x - i, Me.Height - diameter - y - i, diameter, diameter, 0, 90)
                g.DrawLine(p, x + radius, Me.Height - y - i, Me.Width - x - radius, Me.Height - y - i)
                g.DrawArc(p, x + i, Me.Height - diameter - y - i, diameter, diameter, 90, 90)
            Next i
        End Using

        If Me._ShowButtonText Then
            Using sb As New SolidBrush(Me.ForeColor)
                g.DrawString(Me.ButtonText, Me.Font, sb, Me.TextLocation)
            End Using
        End If

    End Sub

    ''' <summary>
    ''' Raises the
    ''' <see cref="M:System.Windows.Forms.ButtonBase.OnPaint(System.Windows.Forms.PaintEventArgs)" />
    ''' event.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnPaint(ByVal e As PaintEventArgs)
        If e Is Nothing Then Return
        e.Graphics.SmoothingMode = SmoothingMode.AntiAlias
        MyBase.OnPaint(e)

        Select Case Me._ButtonShape
            Case ButtonShape.Circle
                Me.DrawCircularButton(e.Graphics)

            Case ButtonShape.RoundRect
                Me.DrawRoundRectangularButton(e.Graphics)
        End Select
    End Sub

#End Region

End Class

''' <summary> Values that represent button shapes. </summary>
''' <remarks> David, 2020-09-24. </remarks>
Public Enum ButtonShape

    ''' <summary> An enum constant representing the round Rectangle option. </summary>
    RoundRect

    ''' <summary> An enum constant representing the circle option. </summary>
    Circle
End Enum
