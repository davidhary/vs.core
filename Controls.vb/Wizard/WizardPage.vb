﻿Imports System.Windows.Forms.Design
Imports System.ComponentModel

''' <summary> Represents a wizard page control with basic layout functionality. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc., All Rights Reserved <para>
''' (c) 2005 Cristi Potlog - All Rights Reserved </para><para>
''' Licensed under the MIT License. </para><para>
''' David, 09/19/2012, 1.05.4645.</para><para>
''' Based on http://www.CodeProject.com/Articles/10808/Cristi-Potlog-s-Wizard-Control-for-NET
''' </para>
''' </remarks>
Public Class WizardPage
    Inherits Panel



#Region " CONSTANTS "

    ''' <summary> Height of the header area. </summary>
    Private Const HeaderAreaHeight As Integer = 64

    ''' <summary> Size of the header glyph. </summary>
    Private Const HeaderGlyphSize As Integer = 48

    ''' <summary> The header text padding. </summary>
    Private Const HeaderTextPadding As Integer = 8

    ''' <summary> Width of the welcome glyph. </summary>
    Private Const WelcomeGlyphWidth As Integer = 164
#End Region

#Region " CONSTRUCTOR "

    ''' <summary> Creates a new instance of the <see cref="WizardPage"/> class. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        MyBase.New()

        Me._Description = String.Empty
        Me._Title = String.Empty
        Me._WizardPageStyle = WizardPageStyle.Standard

        ' reset control style to improve rendering (reduce flicker)
        MyBase.SetStyle(ControlStyles.AllPaintingInWmPaint, True)
        MyBase.SetStyle(ControlStyles.DoubleBuffer, True)
        MyBase.SetStyle(ControlStyles.ResizeRedraw, True)
        MyBase.SetStyle(ControlStyles.UserPaint, True)

    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary> The wizard page style. </summary>
    Private _WizardPageStyle As WizardPageStyle

    ''' <summary> Gets or sets the style of the wizard page. </summary>
    ''' <value> The wizard page style. </value>
    <DefaultValue(WizardPageStyle.Standard), Category("Wizard"), Description("Gets or sets the style of the wizard page.")>
    Public Property WizardPageStyle() As WizardPageStyle
        Get
            Return Me._WizardPageStyle
        End Get
        Set(ByVal value As WizardPageStyle)
            If Me._WizardPageStyle <> value Then
                Me._WizardPageStyle = value
                ' get the parent wizard control
                If Me.Parent IsNot Nothing AndAlso TypeOf Me.Parent Is Wizard Then
                    Dim parentWizard As Wizard = CType(Me.Parent, Wizard)
                    ' check if page is selected
                    If parentWizard.SelectedPage Is Me Then
                        ' reactivate the selected page (performs redraw too)
                        parentWizard.SelectedPage = Me
                    End If
                Else
                    ' just redraw the page
                    Me.Invalidate()
                End If
            End If
        End Set
    End Property

    ''' <summary> The title. </summary>
    Private _Title As String

    ''' <summary> Gets or sets the title of the wizard page. </summary>
    ''' <value> The title. </value>
    <DefaultValue(""), Category("Wizard"), Description("Gets or sets the title of the wizard page.")>
    Public Property Title() As String
        Get
            Return Me._Title
        End Get
        Set(ByVal value As String)
            If value Is Nothing Then
                value = String.Empty
            End If
            If Me._Title <> value Then
                Me._Title = value
                Me.Invalidate()
            End If
        End Set
    End Property

    ''' <summary> The description. </summary>
    Private _Description As String

    ''' <summary> Gets or sets the description of the wizard page. </summary>
    ''' <value> The description. </value>
    <DefaultValue(""), Category("Wizard"), Description("Gets or sets the description of the wizard page.")>
    Public Property Description() As String
        Get
            Return Me._Description
        End Get
        Set(ByVal value As String)
            If value Is Nothing Then
                value = String.Empty
            End If
            If Me._Description <> value Then
                Me._Description = value
                Me.Invalidate()
            End If
        End Set
    End Property
#End Region

#Region " METHODS "

    ''' <summary> Provides custom drawing to the wizard page. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnPaint(ByVal e As PaintEventArgs)
        ' raise paint event
        MyBase.OnPaint(e)

        If e Is Nothing Then
            Return
        End If

        ' check if custom style
        If Me._WizardPageStyle = WizardPageStyle.Custom Then
            ' filter out
            Return
        End If

        ' initialize graphic resources
        Dim headerRect As Rectangle = Me.ClientRectangle
        Dim glyphRect As Rectangle = Rectangle.Empty
        Dim titleRect As Rectangle = Rectangle.Empty
        Dim descriptionRect As Rectangle = Rectangle.Empty

        ' determine text format
        Dim textFormat As StringFormat = StringFormat.GenericDefault
        textFormat.LineAlignment = StringAlignment.Near
        textFormat.Alignment = StringAlignment.Near
        textFormat.Trimming = StringTrimming.EllipsisCharacter

        Select Case Me._WizardPageStyle
            Case WizardPageStyle.Standard
                ' adjust height for header
                headerRect.Height = WizardPage.HeaderAreaHeight
                ' draw header border
                ControlPaint.DrawBorder3D(e.Graphics, headerRect, Border3DStyle.Etched, Border3DSide.Bottom)
                ' adjust header rect not to overwrite the border
                headerRect.Height -= SystemInformation.Border3DSize.Height
                ' fill header with window color
                e.Graphics.FillRectangle(SystemBrushes.Window, headerRect)

                ' determine header image rectangle
                Dim headerPadding As Integer = CInt(Math.Floor((WizardPage.HeaderAreaHeight - WizardPage.HeaderGlyphSize) / 2.0))
                glyphRect.Location = New Point(Me.Width - WizardPage.HeaderGlyphSize - headerPadding, headerPadding)
                glyphRect.Size = New Size(WizardPage.HeaderGlyphSize, WizardPage.HeaderGlyphSize)

                ' determine the header content
                Dim headerImage As Image = Nothing
                Dim headerFont As Font = Me.Font
                Dim headerTitleFont As Font = Me.Font
                If Me.Parent IsNot Nothing AndAlso TypeOf Me.Parent Is Wizard Then
                    ' get content from parent wizard, if exists
                    Dim parentWizard As Wizard = CType(Me.Parent, Wizard)
                    headerImage = parentWizard.HeaderImage
                    headerFont = parentWizard.HeaderFont
                    headerTitleFont = parentWizard.HeaderTitleFont
                End If

                ' check if we have an image
                If headerImage Is Nothing Then
                    ' display a focus rect as a place holder
                    ControlPaint.DrawFocusRectangle(e.Graphics, glyphRect)
                Else
                    ' draw header image
                    e.Graphics.DrawImage(headerImage, glyphRect)
                End If

                ' determine title height
                Dim headerTitleHeight As Integer = CInt(Math.Ceiling(e.Graphics.MeasureString(Me._Title, headerTitleFont, 0, textFormat).Height))

                ' calculate text sizes
                titleRect.Location = New Point(WizardPage.HeaderTextPadding, WizardPage.HeaderTextPadding)
                titleRect.Size = New Size(glyphRect.Left - WizardPage.HeaderTextPadding, headerTitleHeight)
                descriptionRect.Location = titleRect.Location
                descriptionRect.Y += headerTitleHeight + WizardPage.HeaderTextPadding \ 2
                descriptionRect.Size = New Size(titleRect.Width, WizardPage.HeaderAreaHeight - descriptionRect.Y)

                ' draw title text (single line, truncated with ellipsis)
                e.Graphics.DrawString(Me._Title, headerTitleFont, SystemBrushes.WindowText, titleRect, textFormat)
                ' draw description text (multiple lines if needed)
                e.Graphics.DrawString(Me._Description, headerFont, SystemBrushes.WindowText, descriptionRect, textFormat)
            Case WizardPageStyle.Welcome, WizardPageStyle.Finish
                ' fill whole page with window color
                e.Graphics.FillRectangle(SystemBrushes.Window, headerRect)

                ' determine welcome image rectangle
                glyphRect.Location = Point.Empty
                glyphRect.Size = New Size(WizardPage.WelcomeGlyphWidth, Me.Height)

                ' determine the icon that should appear on the welcome page
                Dim welcomeImage As Image = Nothing
                Dim welcomeFont As Font = Me.Font
                Dim welcomeTitleFont As Font = Me.Font
                If Me.Parent IsNot Nothing AndAlso TypeOf Me.Parent Is Wizard Then
                    ' get content from parent wizard, if exists
                    Dim parentWizard As Wizard = CType(Me.Parent, Wizard)
                    welcomeImage = parentWizard.WelcomeImage
                    welcomeFont = parentWizard.WelcomeFont
                    welcomeTitleFont = parentWizard.WelcomeTitleFont
                End If

                ' check if we have an image
                If welcomeImage Is Nothing Then
                    ' display a focus rect as a place holder
                    ControlPaint.DrawFocusRectangle(e.Graphics, glyphRect)
                Else
                    ' draw welcome page image
                    e.Graphics.DrawImage(welcomeImage, glyphRect)
                End If

                ' calculate text sizes
                titleRect.Location = New Point(WizardPage.WelcomeGlyphWidth + WizardPage.HeaderTextPadding, WizardPage.HeaderTextPadding)
                titleRect.Width = Me.Width - titleRect.Left - WizardPage.HeaderTextPadding
                ' determine title height
                Dim welcomeTitleHeight As Integer = CInt(Math.Ceiling(e.Graphics.MeasureString(Me._Title, welcomeTitleFont, titleRect.Width, textFormat).Height))
                descriptionRect.Location = titleRect.Location
                descriptionRect.Y += welcomeTitleHeight + WizardPage.HeaderTextPadding
                descriptionRect.Size = New Size(Me.Width - descriptionRect.Left - WizardPage.HeaderTextPadding, Me.Height - descriptionRect.Y)

                ' draw title text (multiple lines if needed)
                e.Graphics.DrawString(Me._Title, welcomeTitleFont, SystemBrushes.WindowText, titleRect, textFormat)
                ' draw description text (multiple lines if needed)
                e.Graphics.DrawString(Me._Description, welcomeFont, SystemBrushes.WindowText, descriptionRect, textFormat)
        End Select
    End Sub
#End Region

#Region " INNER CLASSES "

    ''' <summary>
    ''' This is a designer for the Banner. This designer locks the control vertical sizing.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Friend Class WizardPageDesigner
        Inherits ParentControlDesigner

        ''' <summary>
        ''' Gets the selection rules that indicate the movement capabilities of a component.
        ''' </summary>
        ''' <value>
        ''' A bitwise combination of <see cref="T:System.Windows.Forms.Design.SelectionRules" /> values.
        ''' </value>
        Public Overrides ReadOnly Property SelectionRules() As SelectionRules
            Get
                ' lock the control
                Return SelectionRules.Visible Or SelectionRules.Locked
            End Get
        End Property
    End Class

#End Region

End Class
