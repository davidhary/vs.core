﻿Imports System.ComponentModel

''' <summary> Provides data for the Page Changed Event of the Wizard control. </summary>
''' <remarks> David, 2020-09-24. </remarks>
Public Class PageChangedEventArgs
    Inherits EventArgs

#Region " CONSTRUCTOR "

    ''' <summary> Creates a new instance of the <see cref="PageChangedEventArgs"/> class. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="oldIndex"> An integer value representing the index of the old page. </param>
    ''' <param name="newIndex"> An integer value representing the index of the new page. </param>
    Friend Sub New(ByVal oldIndex As Integer, ByVal newIndex As Integer)
        MyBase.New()
        Me._OldIndex = oldIndex
        Me._NewIndex = newIndex
    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary> Gets or sets the index of the old page. </summary>
    ''' <value> The old index. </value>
    Public ReadOnly Property OldIndex() As Integer

    ''' <summary> The new index. </summary>
    Private _NewIndex As Integer

    ''' <summary> Gets or sets the index of the new page. </summary>
    ''' <value> The new index. </value>
    Public Property NewIndex() As Integer
        Get
            Return Me._NewIndex
        End Get
        Protected Set(value As Integer)
            Me._NewIndex = value
        End Set
    End Property

    ''' <summary> Gets the direction. </summary>
    ''' <value> The direction. </value>
    Public ReadOnly Property Direction As Direction
        Get
            Return If(Me.NewIndex = Me.OldIndex, Direction.None, If(Me.NewIndex > Me.OldIndex, Direction.Next, Direction.Previous))
        End Get
    End Property

#End Region

End Class

''' <summary> Provides data for the Page Changing Event of the Wizard control. </summary>
''' <remarks> David, 2020-09-24. </remarks>
Public Class PageChangingEventArgs
    Inherits PageChangedEventArgs

#Region " CONSTRUCTOR "

    ''' <summary> Creates a new instance of the <see cref="PageChangingEventArgs"/> class. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="oldIndex"> An integer value representing the index of the old page. </param>
    ''' <param name="newIndex"> An integer value representing the index of the new page. </param>
    Friend Sub New(ByVal oldIndex As Integer, ByVal newIndex As Integer)
        MyBase.New(oldIndex, newIndex)
    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary> Indicates whether the page change should be canceled. </summary>
    ''' <value> The cancel. </value>
    Public Property Cancel() As Boolean

    ''' <summary> Gets or sets the index of the new page. </summary>
    ''' <value> The new index. </value>
    Public Shadows Property NewIndex() As Integer
        Get
            Return MyBase.NewIndex
        End Get
        Set(ByVal value As Integer)
            MyBase.NewIndex = value
        End Set
    End Property

#End Region

End Class

''' <summary> Values that represent directions. </summary>
''' <remarks> David, 2020-09-24. </remarks>
Public Enum Direction

    ''' <summary> An enum constant representing the none option. </summary>
    <Description("None")> None

    ''' <summary> An enum constant representing the next] option. </summary>
    <Description("Next")> [Next]

    ''' <summary> An enum constant representing the previous] option. </summary>
    <Description("Previous")> [Previous]
End Enum