Imports System.ComponentModel
Imports System.Windows.Forms.Design
Imports System.ComponentModel.Design

''' <summary>
''' Represents an extensible wizard control with basic page navigation functionality.
''' </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc., All Rights Reserved <para>
''' (c) 2005 Cristi Potlog - All Rights Reserved </para><para>
''' Licensed under the MIT License. </para><para>
''' David, 09/19/2012, 1.05.4645.</para><para>
''' Based on http://www.CodeProject.com/Articles/10808/Cristi-Potlog-s-Wizard-Control-for-NET
''' </para>
''' </remarks>
<Designer(GetType(Wizard.WizardDesigner))>
Public Class Wizard
    Inherits UserControl

#Region " CONSTANTS "

    ''' <summary> Height of the footer area. </summary>
    Private Const _FooterAreaHeight As Integer = 48

    ''' <summary> The offset cancel. </summary>
    Private ReadOnly _OffsetCancel As New Point(84, 36)

    ''' <summary> The offset next. </summary>
    Private ReadOnly _OffsetNext As New Point(168, 36)

    ''' <summary> The offset back. </summary>
    Private ReadOnly _OffsetBack As New Point(244, 36)

#End Region

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Creates a new instance of the <see cref="Wizard"/> class. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        ' call required by designer
        Me.InitializeComponent()

        ' reset control style to improve rendering (reduce flicker)
        MyBase.SetStyle(ControlStyles.AllPaintingInWmPaint, True)
        MyBase.SetStyle(ControlStyles.DoubleBuffer, True)
        MyBase.SetStyle(ControlStyles.ResizeRedraw, True)
        MyBase.SetStyle(ControlStyles.UserPaint, True)

        ' reset dock style
        MyBase.Dock = DockStyle.Fill

        ' initialize pages collection
        Me.Pages = New WizardPagesCollection(Me)

        Me.CancelText = "&Cancel"
    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary> Gets or sets which edge of the parent container a control is docked to. </summary>
    ''' <value> The dock. </value>
    <DefaultValue(DockStyle.Fill), Category("Layout"), Description("Gets or sets which edge of the parent container a control is docked to.")>
    Public Shadows Property Dock() As DockStyle
        Get
            Return MyBase.Dock
        End Get
        Set(ByVal value As DockStyle)
            MyBase.Dock = value
        End Set
    End Property

    ''' <summary> Gets the collection of wizard pages in this tab control. </summary>
    ''' <value> The pages. </value>
    <Category("Wizard"), DesignerSerializationVisibility(DesignerSerializationVisibility.Content), Description("Gets the collection of wizard pages in this tab control.")>
    Public ReadOnly Property Pages() As WizardPagesCollection

    ''' <summary> The new page. </summary>
    Private _NewPage As WizardPage

    ''' <summary>
    ''' Gets the New page -- the page to be displayed. Selected when the <see cref="PageChanged">page
    ''' changed event</see> occurs but before it is invoked.
    ''' </summary>
    ''' <value> The new page. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public ReadOnly Property NewPage() As WizardPage
        Get
            Return Me._NewPage
        End Get
    End Property

    ''' <summary> The old page. </summary>
    Private _OldPage As WizardPage

    ''' <summary>
    ''' Gets the old page -- the page already displayed. Selected when the
    ''' <see cref="PageChanged">page changed event</see> occurs but before it is invoked.
    ''' </summary>
    ''' <value> The old page. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public ReadOnly Property OldPage() As WizardPage
        Get
            Return Me._OldPage
        End Get
    End Property

    ''' <summary> The selected page. </summary>
    Private _SelectedPage As WizardPage

    ''' <summary> Gets or sets the currently-selected wizard page. </summary>
    ''' <value> The selected page. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property SelectedPage() As WizardPage
        Get
            Return Me._SelectedPage
        End Get
        Set(ByVal value As WizardPage)
            ' select new page
            Me.ActivatePage(value)
        End Set
    End Property

    ''' <summary> Gets the currently-selected wizard page by index. </summary>
    ''' <value> The selected index. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Friend Property SelectedIndex() As Integer
        Get
            Return Me.Pages.IndexOf(Me._SelectedPage)
        End Get
        Set(ByVal value As Integer)
            ' check if there are any pages or index out of range.
            If Me.Pages.Count = 0 OrElse value < -1 OrElse value >= Me.Pages.Count Then
                ' reset invalid index
                Me.ActivatePage(-1)
            Else
                ' select new page
                Me.ActivatePage(value)
            End If
        End Set
    End Property

    ''' <summary> The header image. </summary>
    Private _HeaderImage As Image

    ''' <summary> Gets or sets the image displayed on the header of the standard pages. </summary>
    ''' <value> The header image. </value>
    <System.ComponentModel.DefaultValue(CType(Nothing, Object)), Category("Wizard"), Description("Gets or sets the image displayed on the header of the standard pages.")>
    Public Property HeaderImage() As Image
        Get
            Return Me._HeaderImage
        End Get
        Set(ByVal value As Image)
            If Me._HeaderImage IsNot value Then
                Me._HeaderImage = value
                Me.Invalidate()
            End If
        End Set
    End Property

    ''' <summary> The welcome image. </summary>
    Private _WelcomeImage As Image

    ''' <summary> Gets or sets the image displayed on the welcome and finish pages. </summary>
    ''' <value> The welcome image. </value>
    <System.ComponentModel.DefaultValue(CType(Nothing, Object)), Category("Wizard"), Description("Gets or sets the image displayed on the welcome and finish pages.")>
    Public Property WelcomeImage() As Image
        Get
            Return Me._WelcomeImage
        End Get
        Set(ByVal value As Image)
            If Me._WelcomeImage IsNot value Then
                Me._WelcomeImage = value
                Me.Invalidate()
            End If
        End Set
    End Property

    ''' <summary> The header font. </summary>

    Private _HeaderFont As Font

    ''' <summary> Gets or sets the font used to display the description of a standard page. </summary>
    ''' <value> The header font. </value>
    <Category("Appearance"), Description("Gets or sets the font used to display the description of a standard page.")>
    Public Property HeaderFont() As Font
        Get
            Return If(Me._HeaderFont, Me.Font)
        End Get
        Set(ByVal value As Font)
            If Me._HeaderFont IsNot value Then
                Me._HeaderFont = value
                Me.Invalidate()
            End If
        End Set
    End Property

    ''' <summary> Determine if we should serialize header font. </summary>
    ''' <remarks> David, 2020-03-07. </remarks>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Protected Function ShouldSerializeHeaderFont() As Boolean
        Return Me._HeaderFont IsNot Nothing
    End Function

    ''' <summary> The header title font. </summary>
    Private _HeaderTitleFont As Font

    ''' <summary> Gets or sets the font used to display the title of a standard page. </summary>
    ''' <value> The header title font. </value>
    <Category("Appearance"), Description("Gets or sets the font used to display the title of a standard page.")>
    Public Property HeaderTitleFont() As Font
        Get
            Return If(Me._HeaderTitleFont, New Font(Me.Font.FontFamily, Me.Font.Size + 2, FontStyle.Bold))
        End Get
        Set(ByVal value As Font)
            If Me._HeaderTitleFont IsNot value Then
                Me._HeaderTitleFont = value
                Me.Invalidate()
            End If
        End Set
    End Property

    ''' <summary> Returns the sentinel indicating if header title font should be serialized. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Protected Function ShouldSerializeHeaderTitleFont() As Boolean
        Return Me._HeaderTitleFont IsNot Nothing
    End Function

    ''' <summary> The welcome font. </summary>
    Private _WelcomeFont As Font

    ''' <summary>
    ''' Gets or sets the font used to display the description of a welcome of finish page.
    ''' </summary>
    ''' <value> The welcome font. </value>
    <Category("Appearance"), Description("Gets or sets the font used to display the description of a welcome of finish page.")>
    Public Property WelcomeFont() As Font
        Get
            Return If(Me._WelcomeFont, Me.Font)
        End Get
        Set(ByVal value As Font)
            If Me._WelcomeFont IsNot value Then
                Me._WelcomeFont = value
                Me.Invalidate()
            End If
        End Set
    End Property

    ''' <summary> Returns the sentinel indicating if welcome font should be serialized. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Protected Function ShouldSerializeWelcomeFont() As Boolean
        Return Me._WelcomeFont IsNot Nothing
    End Function

    ''' <summary> The welcome title font. </summary>
    Private _WelcomeTitleFont As Font

    ''' <summary>
    ''' Gets or sets the font used to display the title of a welcome of finish page.
    ''' </summary>
    ''' <value> The welcome title font. </value>
    <Category("Appearance"), Description("Gets or sets the font used to display the title of a welcome of finish page.")>
    Public Property WelcomeTitleFont() As Font
        Get
            Return If(Me._WelcomeTitleFont, New Font(Me.Font.FontFamily, Me.Font.Size + 10, FontStyle.Bold))
        End Get
        Set(ByVal value As Font)
            If Me._WelcomeTitleFont IsNot value Then
                Me._WelcomeTitleFont = value
                Me.Invalidate()
            End If
        End Set
    End Property

    ''' <summary> Determine if we should serialize welcome title font. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Protected Function ShouldSerializeWelcomeTitleFont() As Boolean
        Return Me._WelcomeTitleFont IsNot Nothing
    End Function

    ''' <summary> Gets or sets the enabled state of the Next button. </summary>
    ''' <value> The next enabled. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property NextEnabled() As Boolean
        Get
            Return Me._NextButton.Enabled
        End Get
        Set(ByVal value As Boolean)
            Me._NextButton.Enabled = value
        End Set
    End Property

    ''' <summary> Gets or sets the enabled state of the back button. </summary>
    ''' <value> The back enabled. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property BackEnabled() As Boolean
        Get
            Return Me._BackButton.Enabled
        End Get
        Set(ByVal value As Boolean)
            Me._BackButton.Enabled = value
        End Set
    End Property

    ''' <summary> Gets or sets the enabled state of the cancel button. </summary>
    ''' <value> The cancel enabled. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property CancelEnabled() As Boolean
        Get
            Return Me._CancelButton.Enabled
        End Get
        Set(ByVal value As Boolean)
            Me._CancelButton.Enabled = value
        End Set
    End Property

    ''' <summary> Gets or sets the visible state of the help button. </summary>
    ''' <value> The help visible. </value>
    <Category("Behavior"), DefaultValue(False), Description("Gets or sets the visible state of the help button.")>
    Public Property HelpVisible() As Boolean
        Get
            Return Me._HelpButton.Visible
        End Get
        Set(ByVal value As Boolean)
            Me._HelpButton.Visible = value
        End Set
    End Property

    ''' <summary> Gets or sets the text displayed by the Next button. </summary>
    ''' <value> The next text. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property NextText() As String
        Get
            Return Me._NextButton.Text
        End Get
        Set(ByVal value As String)
            Me._NextButton.Text = value
        End Set
    End Property

    ''' <summary> Gets or sets the text displayed by the back button. </summary>
    ''' <value> The back text. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property BackText() As String
        Get
            Return Me._BackButton.Text
        End Get
        Set(ByVal value As String)
            Me._BackButton.Text = value
        End Set
    End Property

    ''' <summary> Gets or sets the text displayed by the cancel button. </summary>
    ''' <value> The cancel text. </value>
    <Browsable(True), Category("Appearance"), DefaultValue("&Cancel"),
    Description("Gets or sets the test to display on the title of the Cancel button.")>
    Public Property CancelText() As String

    ''' <summary> Gets or sets the text displayed by the cancel button. </summary>
    ''' <value> The help text. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property HelpText() As String
        Get
            Return Me._HelpButton.Text
        End Get
        Set(ByVal value As String)
            Me._HelpButton.Text = value
        End Set
    End Property

    ''' <summary> Gets or sets the text displayed by the Finish button. </summary>
    ''' <value> The finish text. </value>
    <Browsable(True), Category("Appearance"), DefaultValue("&Finish"),
    Description("Gets or sets the test to display on the title of Finish button on the finish page.")>
    Public Property FinishText() As String

#End Region

#Region " METHODS "

    ''' <summary>
    ''' Advances to next wizard page.
    ''' </summary>
    Public Sub [Next]()
        ' check if we're on the last page (finish)
        If Me.SelectedIndex = Me.Pages.Count - 1 Then
            Me._NextButton.Enabled = False
        Else
            ' handle page change
            Me.OnPageChanging(New PageChangingEventArgs(Me.SelectedIndex, Me.SelectedIndex + 1))
        End If
    End Sub

    ''' <summary> Retreats to the previous wizard page. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub Back()
        If Me.SelectedIndex = 0 Then
            Me._BackButton.Enabled = False
        Else
            ' handle page change
            Me.OnPageChanging(New PageChangingEventArgs(Me.SelectedIndex, Me.SelectedIndex - 1))
        End If
    End Sub

    ''' <summary> Activates the specified wizard page. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="index"> An Integer value representing the zero-based index of the page to be
    '''                      activated. </param>
    Private Sub ActivatePage(ByVal index As Integer)
        ' check if new page is invalid
        If index < 0 OrElse index >= Me.Pages.Count Then
            ' filter out
            Return
        End If

        ' get new page
        Dim page As WizardPage = CType(Me.Pages(index), WizardPage)

        ' activate page
        Me.ActivatePage(page)

    End Sub

    ''' <summary> Activates the specified wizard page. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="page"> A WizardPage object representing the page to be activated. </param>
    Private Sub ActivatePage(ByVal page As WizardPage)

        ' validate given page
        If Me.Pages.Contains(page) = False Then
            ' filter out
            Return
        End If

        ' deactivate current page
        If Me._SelectedPage IsNot Nothing Then
            Me._SelectedPage.Visible = False
        End If

        ' activate new page
        Me._SelectedPage = page

        If Me._SelectedPage IsNot Nothing Then
            'Ensure that this panel displays inside the wizard
            Me._SelectedPage.Parent = Me
            If Me.Contains(Me._SelectedPage) = False Then
                Me.Container.Add(Me._SelectedPage)
            End If
            If Me._SelectedPage.WizardPageStyle = WizardPageStyle.Finish Then
                Me._CancelButton.Text = Me.FinishText
                Me._CancelButton.DialogResult = DialogResult.OK
            Else
                Me._CancelButton.Text = Me.CancelText ' "Cancel"
                Me._CancelButton.DialogResult = DialogResult.Cancel
            End If

            'Make it fill the space
            Me._SelectedPage.SetBounds(0, 0, Me.Width, Me.Height - _FooterAreaHeight)
            Me._SelectedPage.Visible = True
            Me._SelectedPage.BringToFront()
            Wizard.FocusFirstTabIndex(Me._SelectedPage)
        End If

        'What should the back button say
        Me._BackButton.Enabled = Me.SelectedIndex > 0

        'What should the Next button say
        If Me.SelectedIndex < Me.Pages.Count - 1 Then
            Me._NextButton.Enabled = True
        Else
            If Me.DesignMode = False Then
                ' at runtime disable back button (we finished; there's no point going back)
                Me._BackButton.Enabled = False
            End If
            Me._NextButton.Enabled = False
        End If

        ' refresh
        If Me._SelectedPage IsNot Nothing Then
            Me._SelectedPage.Invalidate()
        Else
            Me.Invalidate()
        End If
    End Sub

    ''' <summary> Focus the control with a lowest tab index in the given container. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="container"> A Control object to process. </param>
    Private Shared Sub FocusFirstTabIndex(ByVal container As Control)
        ' initialize search result variable
        Dim searchResult As Control = Nothing

        ' find the control with the lowest tab index
        For Each control As Control In container.Controls
            If control.CanFocus AndAlso (searchResult Is Nothing OrElse control.TabIndex < searchResult.TabIndex) Then
                searchResult = control
            End If
        Next control

        ' check if anything searchResult
        If searchResult IsNot Nothing Then
            ' focus found control
            searchResult.Focus()
        Else
            ' focus the container
            container.Focus()
        End If
    End Sub

    ''' <summary> Raises the <see cref="PageChanging">page changing event</see>. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A WizardPageEventArgs object that holds event data. </param>
    Protected Overridable Sub OnPageChanging(ByVal e As PageChangingEventArgs)

        If e Is Nothing Then Return

        ' get wizard page already displayed
        Me._OldPage = Me.Pages(e.OldIndex)

        ' get wizard page to be displayed
        Me._NewPage = Me.Pages(e.NewIndex)

        ' check if there are subscribers and raise changing event
        If Me.PageChangingEvent IsNot Nothing Then Me.PageChangingEvent.Invoke(Me, e)

        ' check if user canceled
        If e.Cancel Then
            ' filter
            Return
        End If

        ' activate new page
        Me.ActivatePage(e.NewIndex)

        ' raise the changed
        Me.OnPageChanged(TryCast(e, PageChangedEventArgs))
    End Sub

    ''' <summary> Raises the <see cref="PageChanged">page changed event</see>. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A WizardPageEventArgs object that holds event data. </param>
    Protected Overridable Sub OnPageChanged(ByVal e As PageChangedEventArgs)

        If e Is Nothing Then Return

        ' get wizard page already displayed
        Me._OldPage = Me.Pages(e.OldIndex)

        ' get wizard page to be displayed
        Me._NewPage = Me.Pages(e.NewIndex)

        ' check if there are subscribers and raise the changed event
        If Me.PageChangedEvent IsNot Nothing Then Me.PageChangedEvent.Invoke(Me, e)
    End Sub

    ''' <summary> Raises the Cancel event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A CancelEventArgs object that holds event data. </param>
    Protected Overridable Sub OnCancel(ByVal e As CancelEventArgs)
        If e Is Nothing Then Return
        ' check if there are subscribers and raise Cancel event
        If Me.CancelEvent IsNot Nothing Then Me.CancelEvent.Invoke(Me, e)
    End Sub

    ''' <summary> Raises the Finish event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A EventArgs object that holds event data. </param>
    Protected Overridable Sub OnFinish(ByVal e As EventArgs)
        If e Is Nothing Then Return
        ' check if there are subscribers and raise Finish event
        If Me.FinishEvent IsNot Nothing Then Me.FinishEvent.Invoke(Me, e)
    End Sub

    ''' <summary> Raises the Help event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A EventArgs object that holds event data. </param>
    Protected Overridable Sub OnHelp(ByVal e As EventArgs)
        If e Is Nothing Then Return
        ' check if there are subscribers and raise the Help event
        If Me.HelpEvent IsNot Nothing Then Me.HelpEvent.Invoke(Me, e)
    End Sub

    ''' <summary> Raises the Load event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnLoad(ByVal e As EventArgs)

        ' raise the Load event
        MyBase.OnLoad(e)

        ' activate first page, if exists
        If Me.Pages.Count > 0 Then
            Me.ActivatePage(0)
        End If
    End Sub

    ''' <summary> Raises the Resize event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnResize(ByVal e As EventArgs)
        ' raise the Resize event
        MyBase.OnResize(e)

        ' resize the selected page to fit the wizard
        If Me._SelectedPage IsNot Nothing Then
            Me._SelectedPage.SetBounds(0, 0, Me.Width, Me.Height - _FooterAreaHeight)
        End If

        ' position navigation buttons
        Me._CancelButton.Location = New Point(Me.Width - Me._OffsetCancel.X, Me.Height - Me._OffsetCancel.Y)
        Me._NextButton.Location = New Point(Me.Width - Me._OffsetNext.X, Me.Height - Me._OffsetNext.Y)
        Me._BackButton.Location = New Point(Me.Width - Me._OffsetBack.X, Me.Height - Me._OffsetBack.Y)
        Me._HelpButton.Location = New Point(Me._HelpButton.Location.X, Me.Height - Me._OffsetBack.Y)
    End Sub

    ''' <summary> Raises the Paint event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnPaint(ByVal e As PaintEventArgs)
        ' raise the Paint event
        MyBase.OnPaint(e)
        If e Is Nothing Then Return
        Dim bottomRect As Rectangle = Me.ClientRectangle
        bottomRect.Y = Me.Height - _FooterAreaHeight
        bottomRect.Height = _FooterAreaHeight
        ControlPaint.DrawBorder3D(e.Graphics, bottomRect, Border3DStyle.Etched, Border3DSide.Top)
    End Sub

    ''' <summary> Raises the ControlAdded event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.ControlEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnControlAdded(ByVal e As ControlEventArgs)
        ' prevent other controls from being added directly to the wizard
        If e Is Nothing Then Return
        If TypeOf e.Control Is WizardPage = False AndAlso e.Control IsNot Me._CancelButton AndAlso
            e.Control IsNot Me._NextButton AndAlso e.Control IsNot Me._BackButton Then
            ' add the control to the selected page
            If Me._SelectedPage IsNot Nothing Then
                Me._SelectedPage.Controls.Add(e.Control)
            End If
        Else
            ' raise the ControlAdded event
            MyBase.OnControlAdded(e)
        End If
    End Sub

#End Region

#Region " EVENTS "

    ''' <summary> Event queue for all listeners interested in PageChanging events. </summary>
    ''' <remarks>
    ''' Occurs before the wizard page changes, giving the user a chance to validate.
    ''' </remarks>
    <Category("Wizard"), Description("Occurs before the wizard page changes, giving the user a chance to validate.")>
    Public Event PageChanging As EventHandler(Of PageChangingEventArgs)

    ''' <summary> Event queue for all listeners interested in PageChanged events. </summary>
    ''' <remarks>
    ''' Occurs after the wizard page changed, giving the user a chance to setup the new page.
    ''' </remarks>
    <Category("Wizard"), Description("Occurs after the wizard page changed, giving the user a chance to setup the new page.")>
    Public Event PageChanged As EventHandler(Of PageChangedEventArgs)

    ''' <summary> Event queue for all listeners interested in Cancel events. </summary>
    ''' <remarks>
    ''' Occurs when wizard is canceled, giving the user a chance to validate.
    ''' </remarks>
    <Category("Wizard"), Description("Occurs when wizard is canceled, giving the user a chance to validate.")>
    Public Event Cancel As CancelEventHandler

    ''' <summary> Event queue for all listeners interested in Finish events. </summary>
    ''' <remarks>
    ''' Occurs when wizard is finished, giving the user a chance to do extra stuff.
    ''' </remarks>
    <Category("Wizard"), Description("Occurs when wizard is finished, giving the user a chance to do extra stuff.")>
    Public Event Finish As EventHandler

    ''' <summary> Event queue for all listeners interested in Help events. </summary>
    ''' <remarks>
    ''' Occurs when the user clicks the help button.
    ''' </remarks>
    <Category("Wizard"), Description("Occurs when the user clicks the help button.")>
    Public Event Help As EventHandler

#End Region

#Region " EVENTS HANDLERS "

    ''' <summary> Handles the Click event of the Next button. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub NextButtonClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles _NextButton.Click
        Me.Next()
    End Sub

    ''' <summary> Handles the Click event of the Back button. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub BackButtonClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles _BackButton.Click
        Me.Back()
    End Sub

    ''' <summary> Handles the Click event of the Cancel button. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub CancelButtonClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles _CancelButton.Click
        ' check if button is cancel mode
        If Me._CancelButton.DialogResult = DialogResult.Cancel Then
            Me.OnCancel(New CancelEventArgs())
            ' check if button is finish mode
        ElseIf Me._CancelButton.DialogResult = DialogResult.OK Then
            Me.OnFinish(EventArgs.Empty)
        End If
    End Sub

    ''' <summary> Handles the Click event of the Help button. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub HelpButtonClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles _HelpButton.Click
        Me.OnHelp(EventArgs.Empty)
    End Sub

#End Region

#Region " INNER CLASSES "

    ''' <summary> Represents a designer for the wizard control. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Friend Class WizardDesigner
        Inherits ParentControlDesigner

#Region " METHODS "

        ''' <summary>
        ''' Overrides the handling of Mouse clicks to allow back-next to work in the designer.
        ''' </summary>
        ''' <remarks> David, 2020-09-24. </remarks>
        ''' <param name="msg"> [in,out] A Message value. </param>
        Protected Overrides Sub WndProc(ByRef msg As Message)
            ' declare PInvoke constants
            Const WM_LBUTTONDOWN As Integer = &H201
            Const WM_LBUTTONDBLCLK As Integer = &H203

            ' check message
            If msg.Msg = WM_LBUTTONDOWN OrElse msg.Msg = WM_LBUTTONDBLCLK Then
                ' get the control under the mouse
                Dim ss As ISelectionService = CType(Me.GetService(GetType(ISelectionService)), ISelectionService)

                If TypeOf ss.PrimarySelection Is Wizard Then
                    Dim wizard As Wizard = CType(ss.PrimarySelection, Wizard)

                    ' extract the mouse position
                    Dim xPos As Integer = CShort(CUInt(msg.LParam) And &HFFFF)
                    Dim yPos As Integer = CShort((CUInt(msg.LParam) And &HFFFF0000L) >> 16)
                    Dim mousePos As New Point(xPos, yPos)

                    If msg.HWnd = wizard._NextButton.Handle Then
                        If wizard._NextButton.Enabled AndAlso wizard._NextButton.ClientRectangle.Contains(mousePos) Then
                            'Press the button
                            wizard.Next()
                        End If
                    ElseIf msg.HWnd = wizard._BackButton.Handle Then
                        If wizard._BackButton.Enabled AndAlso wizard._BackButton.ClientRectangle.Contains(mousePos) Then
                            'Press the button
                            wizard.Back()
                        End If
                    End If

                    ' filter message
                    Return
                End If
            End If

            ' forward message
            MyBase.WndProc(msg)
        End Sub

        ''' <summary> Prevents the grid from being drawn on the Wizard. </summary>
        ''' <value>
        ''' <see langword="true" /> if a grid should be drawn on the control in the designer; otherwise,
        ''' <see langword="false" />.
        ''' </value>
        Protected Overrides Property DrawGrid() As Boolean
            Get
                Return False
            End Get
            Set(value As Boolean)
                MyBase.DrawGrid = value
            End Set
        End Property

#End Region

    End Class

#End Region

End Class

