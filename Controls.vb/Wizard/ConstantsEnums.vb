﻿#Region " ENUMS "

''' <summary> Represents possible styles of a wizard page. </summary>
''' <remarks> David, 2020-09-24. </remarks>
Public Enum WizardPageStyle
    ''' <summary>
    ''' Represents a standard interior wizard page with a white banner at the top.
    ''' </summary>
    Standard
    ''' <summary>
    ''' Represents a welcome wizard page with white background and large logo on the left.
    ''' </summary>
    Welcome
    ''' <summary>
    ''' Represents a finish wizard page with white background,
    ''' a large logo on the left and OK button.
    ''' </summary>
    Finish
    ''' <summary>
    ''' Represents a blank wizard page.
    ''' </summary>
    Custom

End Enum

#End Region
