﻿Imports System.Runtime.CompilerServices

Namespace CombinationsExtensions

    ''' <summary> Extension methods for combinations. </summary>
    ''' <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para> </remarks>
    Public Module Methods

        ''' <summary> Generates the combinations. </summary>
        ''' <param name="collectionOfSeries"> The collection of series. </param>
        ''' <returns> The combinations. </returns>
        ''' <remarks>https://stackoverflow.com/questions/545703/combination-of-listlistint</remarks>
                <Extension>
        Public Function GenerateCombinations(Of T)(ByVal collectionOfSeries As List(Of List(Of T))) As List(Of List(Of T))
            Dim generatedCombinations As List(Of List(Of T)) = collectionOfSeries.Take(1).FirstOrDefault().[Select](Function(i) (New T() {i}).ToList()).ToList()
            For Each series As List(Of T) In collectionOfSeries.Skip(1)
                generatedCombinations = generatedCombinations.Join(TryCast(series, List(Of T)), Function(combination) True, Function(i) True,
                                                                   Function(combination, i)
                                                                       Dim nextLevelCombination As List(Of T) = New List(Of T)(combination) From {i}
                                                                       ' nextLevelCombination.Add(i)
                                                                       Return nextLevelCombination
                                                                   End Function).ToList()
            Next
            Return generatedCombinations
        End Function

    End Module

End Namespace

