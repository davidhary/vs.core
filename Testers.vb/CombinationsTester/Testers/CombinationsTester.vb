﻿Imports isr.Core.Tester.CombinationsExtensions
Namespace ConsoleApplication1
    Friend Class Program
        <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
        Public Shared Sub Main(ByVal args() As String)
            GenerateCombinationsTest()
        End Sub

        Private Shared Sub GenerateCombinationsTest()
            Dim collectionOfSeries As New List(Of List(Of Integer)) From {New List(Of Integer)({1, 2, 3, 4, 5}),
                                                                      New List(Of Integer)({0, 1}),
                                                                      New List(Of Integer)({6, 3}),
                                                                      New List(Of Integer)({1, 3, 5})}
            Dim combinations As List(Of List(Of Integer)) = GenerateCombinations(Of Integer)(collectionOfSeries)
            Display(Of Integer)(combinations)
        End Sub

        Private Shared Sub Display(Of T)(ByVal generatedCombinations As List(Of List(Of T)))
            Dim index As Integer = 0
            For Each generatedCombination As List(Of T) In generatedCombinations
                Console.Write("{0}" & vbTab & ":", System.Threading.Interlocked.Increment(index))
                For Each i As T In generatedCombination
                    Console.Write("{0,3}", i)
                Next
                Console.WriteLine()
            Next
            Console.ReadKey()
        End Sub

    End Class

End Namespace
