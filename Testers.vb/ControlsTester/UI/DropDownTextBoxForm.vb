' file:		ControlsTester\UI\DropDownTextBoxForm.vb
'
' summary:	Drop down text box Windows Form




''' <summary> Form for viewing the drop down text box. </summary>
''' <remarks> David, 2020-10-25. </remarks>
Public Class DropDownTextBoxForm

    ''' <summary> The height. </summary>
    Private _Height As Integer

    ''' <summary> Win Forms Text Box  mouse click. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub WinFormsTextBox_MouseClick(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles WinFormsDropDownTextBox.MouseClick
        Dim textBox As TextBox = TryCast(sender, TextBox)
        If textBox IsNot Nothing Then
            If Me._Height = 0 Then
                Me._Height = textBox.Height
            End If
            textBox.Multiline = True
            ' textBox.Width = 300
            textBox.Height = 200
            textBox.Refresh()
        End If
    End Sub

    ''' <summary> Win Forms Text Box mouse double click. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub WinFormsTextBox_MouseDoubleClick(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles WinFormsDropDownTextBox.MouseDoubleClick
        Dim textBox As TextBox = TryCast(sender, TextBox)
        If textBox IsNot Nothing Then
            textBox.Multiline = False
            ' textBox.Width = 300
            textBox.Height = 20
            textBox.Refresh()
        End If
    End Sub

End Class
