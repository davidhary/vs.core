''' <summary> Form for viewing the engineering up down. </summary>
''' <remarks>
''' (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2014-04-05 </para>
''' </remarks>
Public Class EngineeringUpDownForm

#Region " FORM EVENTS "

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Form.Load" /> event. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnLoad(e As EventArgs)
        Try
            Me._EngineeringScaleComboBox.DataSource = Nothing
            Me._EngineeringScaleComboBox.Items.Clear()
            Me._EngineeringScaleComboBox.DataSource = Extensions.ValueDescriptionPairs(GetType(isr.Core.Controls.EngineeringScale))
            Me._EngineeringScaleComboBox.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
            Me._EngineeringScaleComboBox.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
        Catch
            Throw
        Finally
            MyBase.OnLoad(e)
        End Try
    End Sub

#End Region

#Region " CONTROL EVENTS "

    ''' <summary> Enter value numeric value changed. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub EnterValueNumeric_ValueChanged(sender As System.Object, e As System.EventArgs) Handles _EnterValueNumeric.ValueChanged, NumericUpDown1.ValueChanged
        Me._EditedEngineeringUpDown.ScaledValue = Me._EnterValueNumeric.Value
        Me.NumericUpDown1.Value = Me._EnterValueNumeric.Value
    End Sub

    ''' <summary> Engineering scale combo box selection change committed. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub EngineeringScaleComboBox_SelectionChangeCommitted(sender As Object, e As System.EventArgs) Handles _EngineeringScaleComboBox.SelectionChangeCommitted
        Me._EditedEngineeringUpDown.EngineeringScale = CType(CType(Me._EngineeringScaleComboBox.SelectedItem, KeyValuePair(Of [Enum], String)).Key,
                                                             isr.Core.Controls.EngineeringScale)
    End Sub

    ''' <summary> Edited engineering up down value changed. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub EditedEngineeringUpDown_ValueChanged(sender As Object, e As System.EventArgs) Handles _EditedEngineeringUpDown.ValueChanged
        Me._ScaledValueTextBox.Text = Me._EditedEngineeringUpDown.ScaledValue.ToString("0.#########E-00 " & Me._UnitTextBox.Text)
    End Sub

    ''' <summary> Decimal places numeric value changed. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub DecimalPlacesNumeric_ValueChanged(sender As Object, e As System.EventArgs) Handles _DecimalPlacesNumeric.ValueChanged
        Me._EditedEngineeringUpDown.DecimalPlaces = CInt(Me._DecimalPlacesNumeric.Value)
    End Sub

    ''' <summary> Unit text box text changed. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub UnitTextBox_TextChanged(sender As Object, e As System.EventArgs) Handles _UnitTextBox.TextChanged
        Me._EditedEngineeringUpDown.Unit = Me._UnitTextBox.Text
    End Sub

    ''' <summary> Reads only check box checked changed. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ReadOnlyCheckBox_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles _ReadOnlyCheckBox.CheckedChanged
        Me._EditedEngineeringUpDown.ReadOnly = Me._ReadOnlyCheckBox.Checked
    End Sub

#End Region

End Class
