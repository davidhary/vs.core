''' <summary> Form for viewing the selector. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2015-03-06 </para>
''' </remarks>
Public Class SelectorForm

#Region " FORM EVENTS "

#End Region

#Region " CONTROL EVENTS "

    ''' <summary> Reads only check box checked changed. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ReadOnlyCheckBox_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles _ReadOnlyCheckBox.CheckedChanged
        Me._SelectorComboBox.ReadOnly = Me._ReadOnlyCheckBox.Checked
        Me._SelectorNumeric.ReadOnly = Me._ReadOnlyCheckBox.Checked
        Me._ToolStripSelectorComboBox.ReadOnly = Me._ReadOnlyCheckBox.Checked
        Me._ToolStripSelectorNumeric.ReadOnly = Me._ReadOnlyCheckBox.Checked
    End Sub

    ''' <summary> Applies the button click. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ApplyButton_Click(sender As System.Object, e As System.EventArgs) Handles _ApplyButton.Click
        Me._SelectorComboBox.Text = Me._EnterNumeric.Text
        Me._SelectorNumeric.Value = Me._EnterNumeric.Value
        Me._ToolStripSelectorComboBox.Text = Me._EnterNumeric.Text
        Me._ToolStripSelectorNumeric.Value = Me._EnterNumeric.Value
    End Sub

    ''' <summary> Select button click. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SelectButton_Click(sender As System.Object, e As System.EventArgs) Handles _SelectButton.Click
        Me._SelectorComboBox.SelectValue()
        Me._SelectorNumeric.SelectValue()
        Me._ToolStripSelectorComboBox.SelectValue()
        Me._ToolStripSelectorNumeric.SelectValue()
    End Sub

    ''' <summary> Selector combo box value selected. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SelectorComboBox_ValueSelected(sender As Object, e As System.EventArgs) Handles _SelectorComboBox.ValueSelected
        Me._SelectedTextBox.Text = Me._SelectorComboBox.SelectedText
    End Sub

    ''' <summary> Selector numeric value selected. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SelectorNumeric_ValueSelected(sender As Object, e As System.EventArgs) Handles _SelectorNumeric.ValueSelected
        Me._SelectedTextBox.Text = Me._SelectorNumeric.SelectedText
    End Sub

    ''' <summary> Tool strip selector combo box value selected. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ToolStripSelectorComboBox_ValueSelected(sender As System.Object, e As System.EventArgs) Handles _ToolStripSelectorComboBox.ValueSelected
        Me._SelectedTextBox.Text = Me._ToolStripSelectorComboBox.SelectedText
    End Sub

    ''' <summary> Tool strip selector numeric value selected. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ToolStripSelectorNumeric_ValueSelected(sender As System.Object, e As System.EventArgs) Handles _ToolStripSelectorNumeric.ValueSelected
        Me._SelectedTextBox.Text = Me._ToolStripSelectorNumeric.SelectedText
    End Sub

#End Region

End Class
