''' <summary> Form for viewing the nullable numeric up down. </summary>
''' <remarks> David, 2020-10-25. </remarks>
Public Class NullableNumericUpDownForm

    Private Sub NullableNumericUpDownForm_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown
        Me._Timer.Interval = 1000
        Me._Timer.Enabled = True
    End Sub

    ''' <summary> Nullable numeric up down validated. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub NullableNumericUpDown_Validated(sender As Object, e As System.EventArgs) Handles _NullableNumericUpDown.Validated
        Me._ValidatedNullableValueTextBox.Text = Me._NullableNumericUpDown.NullableValue.ToString
        Me._ValidatedValueTextBox.Text = Me._NullableNumericUpDown.Value.ToString
        Me._NullableTextTextBox.Text = Me._NullableNumericUpDown.Text
    End Sub

    ''' <summary> Nullable numeric up down value changed. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub NullableNumericUpDown_ValueChanged(sender As Object, e As System.EventArgs) Handles _NullableNumericUpDown.ValueChanged
        Me._ChangedNullableValueTextBox.Text = Me._NullableNumericUpDown.NullableValue.ToString
        Me._ChangedValueTextBox.Text = Me._NullableNumericUpDown.Value.ToString
        Me._NullableTextTextBox.Text = Me._NullableNumericUpDown.Text
    End Sub

    ''' <summary> Nullable numeric up down value decrementing. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Cancel event information. </param>
    Private Sub NullableNumericUpDown_ValueDecrementing(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles _NullableNumericUpDown.ValueDecrementing
        Me._SpinningNullableValueTextBox.Text = Me._NullableNumericUpDown.NullableValue.ToString
        Me._SpinningValueTextBox.Text = Me._NullableNumericUpDown.Value.ToString
        Me._NullableTextTextBox.Text = Me._NullableNumericUpDown.Text
    End Sub

    ''' <summary> Nullable numeric up down value incrementing. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Cancel event information. </param>
    Private Sub NullableNumericUpDown_ValueIncrementing(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles _NullableNumericUpDown.ValueIncrementing
        Me._SpinningNullableValueTextBox.Text = Me._NullableNumericUpDown.NullableValue.ToString
        Me._SpinningValueTextBox.Text = Me._NullableNumericUpDown.Value.ToString
        Me._NullableTextTextBox.Text = Me._NullableNumericUpDown.Text
    End Sub

    ''' <summary> Nilify button click. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub NilifyButton_Click(sender As System.Object, e As System.EventArgs) Handles _NilifyButton.Click
        Me._NullableNumericUpDown.NullableValue = New Decimal?
        Me._NullableNumericUpDown.Validate()
        Windows.Forms.Application.DoEvents()
    End Sub

    ''' <summary> Timer tick. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Timer_Tick(sender As System.Object, e As System.EventArgs) Handles _Timer.Tick
        Me._NullableValueTextBox.Text = Me._NullableNumericUpDown.NullableValue.ToString
    End Sub

End Class
