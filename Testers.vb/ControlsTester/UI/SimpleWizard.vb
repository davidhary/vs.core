#Region "Copyright ©2005, Cristi Potlog - All Rights Reserved"
' ------------------------------------------------------------------- *
'*                            Cristi Potlog                             *
'*                  Copyright ©2005 - All Rights reserved               *
'*                                                                      *
'* THIS SOURCE CODE IS PROVIDED "AS IS" WITH NO WARRANTIES OF ANY KIND, *
'* EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE        *
'* WARRANTIES OF DESIGN, MERCHANTIBILITY AND FITNESS FOR A PARTICULAR   *
'* PURPOSE, NONINFRINGEMENT, OR ARISING FROM A COURSE OF DEALING,       *
'* USAGE OR TRADE PRACTICE.                                             *
'*                                                                      *
'* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.             *
'* ------------------------------------------------------------------- 
#End Region

#Region " REFERENCES "
Imports isr.Core.Controls
#End Region

''' <summary> An example for using the CP wizard control. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc., All Rights Reserved <para>
''' (c) 2005 Cristi Potlog - All Rights Reserved </para><para>
''' Licensed under the MIT License. </para><para>
''' David, 2012-09-19, 1.05.4645.  </para><para>
''' Based on http://www.CodeProject.com/Articles/10808/Cristi-Potlog-s-Wizard-Control-for-NET
''' </para>
''' </remarks>
Public Class SimpleWizard
    Inherits System.Windows.Forms.Form

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Creates a new instance of the <see cref="SimpleWizard"/> class. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    Public Sub New()
        ' required for designer support
        Me.InitializeComponent()
    End Sub

    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the
    ''' <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="disposing"> <see langword="true" /> to release both managed and unmanaged
    '''                          resources; <see langword="false" /> to release only unmanaged
    '''                          resources. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If Me.components IsNot Nothing Then
                    Me.components.Dispose()
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " METHODS "

    ''' <summary> Starts a sample task simulation. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    Private Sub StartTask()
        ' setup wizard page
        Me._SampleWizard.BackEnabled = False
        Me._SampleWizard.NextEnabled = False
        Me._LongTaskProgressBar.Value = Me._LongTaskProgressBar.Minimum

        ' start timer to simulate a long running task
        Me._LongTaskTimer.Enabled = True
    End Sub

#End Region

#Region " WIZARD EVENTS HANDLERS "

    ''' <summary> Handles the AfterSwitchPages event of the wizard form. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Page changed event information. </param>
    Private Sub SampleWizard_AfterSwitchPages(ByVal sender As Object, ByVal e As PageChangedEventArgs) Handles _SampleWizard.PageChanged

        ' check if license page
        If Me._SampleWizard.NewPage Is Me._LicenseWizardPage Then
            ' sync next button's state with check box
            Me._SampleWizard.NextEnabled = Me._AgreeCheckBox.Checked
            ' check if progress page
        ElseIf Me._SampleWizard.NewPage Is Me._ProgressWizardPage Then
            ' start the sample task
            Me.StartTask()
        End If
    End Sub

    ''' <summary> Handles the BeforeSwitchPages event of the wizard form. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Page changing event information. </param>
    Private Sub SampleWizard_BeforeSwitchPages(ByVal sender As Object, ByVal e As PageChangingEventArgs) Handles _SampleWizard.PageChanging

        ' check if we're going forward from options page
        If Me._SampleWizard.OldPage Is Me._OptionsWizardPage AndAlso e.NewIndex > e.OldIndex Then
            ' check if user selected one option
            If Me._CheckOptionRadioButton.Checked = False AndAlso Me._SkipOptionRadioButton.Checked = False Then
                ' display hint & cancel step
                MessageBox.Show("Please chose one of the options presented.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
                e.Cancel = True
                ' check if user chose to skip validation
            ElseIf Me._SkipOptionRadioButton.Checked Then
                ' skip the validation page
                e.NewIndex += 1
            End If
        End If
    End Sub

    ''' <summary> Handles the Cancel event of the wizard form. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Cancel event information. </param>
    Private Sub SampleWizard_Cancel(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _SampleWizard.Cancel
        ' check if task is running
        Dim isTaskRunning As Boolean = Me._LongTaskTimer.Enabled
        ' stop the task
        Me._LongTaskTimer.Enabled = False

        ' ask user to confirm
        If MessageBox.Show("Are you sure you wand to exit the Sample Wizard?", Me.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> System.Windows.Forms.DialogResult.Yes Then
            ' cancel closing
            e.Cancel = True
            ' restart the task
            Me._LongTaskTimer.Enabled = isTaskRunning
        Else
            ' ensure parent form is closed (even when ShowDialog is not used)
            Me.DialogResult = DialogResult.Cancel
            Me.Close()
        End If
    End Sub

    ''' <summary> Handles the Finish event of the wizard form. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SampleWizard_Finish(ByVal sender As Object, ByVal e As System.EventArgs) Handles _SampleWizard.Finish
        MessageBox.Show("The Sample Wizard finished successfully.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
        ' ensure parent form is closed (even when ShowDialog is not used)
        Me.Close()
    End Sub

    ''' <summary> Handles the Help event of the wizard form. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SampleWizard_Help(ByVal sender As Object, ByVal e As System.EventArgs) Handles _SampleWizard.Help
        MessageBox.Show("This is a really cool wizard control!" & Environment.NewLine & ":-)", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

#End Region

#Region " SIMULATION EVENT HANDLERS "

    ''' <summary> Handles the CheckedChanged event of checkIAgree. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub CheckIAgree_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _AgreeCheckBox.CheckedChanged
        ' sync next button's state with check box
        Me._SampleWizard.NextEnabled = Me._AgreeCheckBox.Checked
    End Sub

    ''' <summary> Handles the Tick event of timerTask. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub TimerTask_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles _LongTaskTimer.Tick
        ' check if task completed
        If Me._LongTaskProgressBar.Value = Me._LongTaskProgressBar.Maximum Then
            ' stop the timer & switch to next page
            Me._LongTaskTimer.Enabled = False
            Me._SampleWizard.Next()
        Else
            ' update progress bar
            Me._LongTaskProgressBar.PerformStep()
        End If
    End Sub

#End Region

End Class
