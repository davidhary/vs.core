<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EngineeringUpDownForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me._DecimalPlacesNumericLabel = New System.Windows.Forms.Label()
        Me._DecimalPlacesNumeric = New System.Windows.Forms.NumericUpDown()
        Me._EngineeringScaleComboBox = New System.Windows.Forms.ComboBox()
        Me._EngineeringScaleComboBoxLabel = New System.Windows.Forms.Label()
        Me._UnitTextBoxLabel = New System.Windows.Forms.Label()
        Me._UnitTextBox = New System.Windows.Forms.TextBox()
        Me._ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._ScaledValueTextBox = New System.Windows.Forms.TextBox()
        Me._EnterValueNumeric = New isr.Core.Controls.NumericUpDown()
        Me._ReadOnlyCheckBox = New System.Windows.Forms.CheckBox()
        Me._EditedEngineeringUpDown = New isr.Core.Controls.EngineeringUpDown()
        Me._EnterValueNumericLabel = New System.Windows.Forms.Label()
        Me._EditedEngineeringUpDownLabel = New System.Windows.Forms.Label()
        Me._ScaledValueTextBoxLabel = New System.Windows.Forms.Label()
        Me.NumericUpDown1 = New isr.Core.Controls.NumericUpDown()
        CType(Me._DecimalPlacesNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._EnterValueNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._EditedEngineeringUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_DecimalPlacesNumericLabel
        '
        Me._DecimalPlacesNumericLabel.AutoSize = True
        Me._DecimalPlacesNumericLabel.Location = New System.Drawing.Point(48, 191)
        Me._DecimalPlacesNumericLabel.Name = "_DecimalPlacesNumericLabel"
        Me._DecimalPlacesNumericLabel.Size = New System.Drawing.Size(97, 17)
        Me._DecimalPlacesNumericLabel.TabIndex = 1
        Me._DecimalPlacesNumericLabel.Text = "Decimal Places:"
        Me._DecimalPlacesNumericLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        '_DecimalPlacesNumeric
        '
        Me._DecimalPlacesNumeric.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._DecimalPlacesNumeric.Location = New System.Drawing.Point(146, 188)
        Me._DecimalPlacesNumeric.Maximum = New Decimal(New Integer() {6, 0, 0, 0})
        Me._DecimalPlacesNumeric.Name = "_DecimalPlacesNumeric"
        Me._DecimalPlacesNumeric.Size = New System.Drawing.Size(58, 25)
        Me._DecimalPlacesNumeric.TabIndex = 2
        Me._ToolTip.SetToolTip(Me._DecimalPlacesNumeric, "Enter decimal places")
        '
        '_EngineeringScaleComboBox
        '
        Me._EngineeringScaleComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._EngineeringScaleComboBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._EngineeringScaleComboBox.FormattingEnabled = True
        Me._EngineeringScaleComboBox.Location = New System.Drawing.Point(147, 150)
        Me._EngineeringScaleComboBox.Name = "_EngineeringScaleComboBox"
        Me._EngineeringScaleComboBox.Size = New System.Drawing.Size(121, 25)
        Me._EngineeringScaleComboBox.TabIndex = 3
        Me._ToolTip.SetToolTip(Me._EngineeringScaleComboBox, "Select engineering scale")
        '
        '_EngineeringScaleComboBoxLabel
        '
        Me._EngineeringScaleComboBoxLabel.AutoSize = True
        Me._EngineeringScaleComboBoxLabel.Location = New System.Drawing.Point(31, 153)
        Me._EngineeringScaleComboBoxLabel.Name = "_EngineeringScaleComboBoxLabel"
        Me._EngineeringScaleComboBoxLabel.Size = New System.Drawing.Size(114, 17)
        Me._EngineeringScaleComboBoxLabel.TabIndex = 1
        Me._EngineeringScaleComboBoxLabel.Text = "Engineering Scale:"
        Me._EngineeringScaleComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        '_UnitTextBoxLabel
        '
        Me._UnitTextBoxLabel.AutoSize = True
        Me._UnitTextBoxLabel.Location = New System.Drawing.Point(111, 118)
        Me._UnitTextBoxLabel.Name = "_UnitTextBoxLabel"
        Me._UnitTextBoxLabel.Size = New System.Drawing.Size(34, 17)
        Me._UnitTextBoxLabel.TabIndex = 1
        Me._UnitTextBoxLabel.Text = "Unit:"
        Me._UnitTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        '_UnitTextBox
        '
        Me._UnitTextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._UnitTextBox.Location = New System.Drawing.Point(147, 114)
        Me._UnitTextBox.Name = "_UnitTextBox"
        Me._UnitTextBox.Size = New System.Drawing.Size(58, 25)
        Me._UnitTextBox.TabIndex = 5
        Me._ToolTip.SetToolTip(Me._UnitTextBox, "Enter the unit, e.g., A, V, Ohm.")
        '
        '_ScaledValueTextBox
        '
        Me._ScaledValueTextBox.Location = New System.Drawing.Point(147, 73)
        Me._ScaledValueTextBox.Name = "_ScaledValueTextBox"
        Me._ScaledValueTextBox.Size = New System.Drawing.Size(121, 25)
        Me._ScaledValueTextBox.TabIndex = 4
        Me._ToolTip.SetToolTip(Me._ScaledValueTextBox, "Scaled value")
        '
        '_EnterValueNumeric
        '
        Me._EnterValueNumeric.Location = New System.Drawing.Point(148, 12)
        Me._EnterValueNumeric.Maximum = New Decimal(New Integer() {2000000000, 0, 0, 0})
        Me._EnterValueNumeric.Minimum = New Decimal(New Integer() {2000000000, 0, 0, -2147483648})
        Me._EnterValueNumeric.Name = "_EnterValueNumeric"
        Me._EnterValueNumeric.ReadOnlyBackColor = System.Drawing.Color.Empty
        Me._EnterValueNumeric.ReadOnlyForeColor = System.Drawing.Color.Empty
        Me._EnterValueNumeric.ReadWriteBackColor = System.Drawing.Color.Empty
        Me._EnterValueNumeric.ReadWriteForeColor = System.Drawing.Color.Empty
        Me._EnterValueNumeric.Size = New System.Drawing.Size(120, 25)
        Me._EnterValueNumeric.TabIndex = 9
        Me._ToolTip.SetToolTip(Me._EnterValueNumeric, "Set this value")
        '
        '_ReadOnlyCheckBox
        '
        Me._ReadOnlyCheckBox.AutoSize = True
        Me._ReadOnlyCheckBox.Location = New System.Drawing.Point(146, 228)
        Me._ReadOnlyCheckBox.Name = "_ReadOnlyCheckBox"
        Me._ReadOnlyCheckBox.Size = New System.Drawing.Size(87, 21)
        Me._ReadOnlyCheckBox.TabIndex = 8
        Me._ReadOnlyCheckBox.Text = "Read Only"
        Me._ReadOnlyCheckBox.UseVisualStyleBackColor = True
        '
        '_EditedEngineeringUpDown
        '
        Me._EditedEngineeringUpDown.ForeColor = System.Drawing.Color.Black
        Me._EditedEngineeringUpDown.Location = New System.Drawing.Point(16, 73)
        Me._EditedEngineeringUpDown.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me._EditedEngineeringUpDown.Minimum = New Decimal(New Integer() {1000, 0, 0, -2147483648})
        Me._EditedEngineeringUpDown.Name = "_EditedEngineeringUpDown"
        Me._EditedEngineeringUpDown.ReadOnlyBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me._EditedEngineeringUpDown.ReadOnlyForeColor = System.Drawing.Color.Black
        Me._EditedEngineeringUpDown.ReadWriteBackColor = System.Drawing.Color.Empty
        Me._EditedEngineeringUpDown.ReadWriteForeColor = System.Drawing.Color.Black
        Me._EditedEngineeringUpDown.ScaledValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me._EditedEngineeringUpDown.Size = New System.Drawing.Size(120, 25)
        Me._EditedEngineeringUpDown.TabIndex = 6
        Me._EditedEngineeringUpDown.UnscaledValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me._EditedEngineeringUpDown.UpDownCursor = System.Windows.Forms.Cursors.Hand
        Me._EditedEngineeringUpDown.UpDownDisplayMode = isr.Core.Controls.UpDownButtonsDisplayMode.WhenMouseOver
        '
        '_EnterValueNumericLabel
        '
        Me._EnterValueNumericLabel.AutoSize = True
        Me._EnterValueNumericLabel.Location = New System.Drawing.Point(10, 16)
        Me._EnterValueNumericLabel.Name = "_EnterValueNumericLabel"
        Me._EnterValueNumericLabel.Size = New System.Drawing.Size(112, 17)
        Me._EnterValueNumericLabel.TabIndex = 1
        Me._EnterValueNumericLabel.Text = "Enter value to set:"
        Me._EnterValueNumericLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        '_EditedEngineeringUpDownLabel
        '
        Me._EditedEngineeringUpDownLabel.AutoSize = True
        Me._EditedEngineeringUpDownLabel.Location = New System.Drawing.Point(13, 53)
        Me._EditedEngineeringUpDownLabel.Name = "_EditedEngineeringUpDownLabel"
        Me._EditedEngineeringUpDownLabel.Size = New System.Drawing.Size(69, 17)
        Me._EditedEngineeringUpDownLabel.TabIndex = 1
        Me._EditedEngineeringUpDownLabel.Text = "Edit Value:"
        Me._EditedEngineeringUpDownLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        '_ScaledValueTextBoxLabel
        '
        Me._ScaledValueTextBoxLabel.AutoSize = True
        Me._ScaledValueTextBoxLabel.Location = New System.Drawing.Point(145, 53)
        Me._ScaledValueTextBoxLabel.Name = "_ScaledValueTextBoxLabel"
        Me._ScaledValueTextBoxLabel.Size = New System.Drawing.Size(104, 17)
        Me._ScaledValueTextBoxLabel.TabIndex = 1
        Me._ScaledValueTextBoxLabel.Text = "Observed Value:"
        Me._ScaledValueTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'NumericUpDown1
        '
        Me.NumericUpDown1.Location = New System.Drawing.Point(12, 116)
        Me.NumericUpDown1.Maximum = New Decimal(New Integer() {2000000000, 0, 0, 0})
        Me.NumericUpDown1.Minimum = New Decimal(New Integer() {2000000000, 0, 0, -2147483648})
        Me.NumericUpDown1.Name = "NumericUpDown1"
        Me.NumericUpDown1.ReadOnlyBackColor = System.Drawing.Color.Empty
        Me.NumericUpDown1.ReadOnlyForeColor = System.Drawing.Color.Empty
        Me.NumericUpDown1.ReadWriteBackColor = System.Drawing.Color.Empty
        Me.NumericUpDown1.ReadWriteForeColor = System.Drawing.Color.Empty
        Me.NumericUpDown1.Size = New System.Drawing.Size(80, 25)
        Me.NumericUpDown1.TabIndex = 9
        '
        'EngineeringUpDownForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(291, 257)
        Me.Controls.Add(Me.NumericUpDown1)
        Me.Controls.Add(Me._EnterValueNumeric)
        Me.Controls.Add(Me._ReadOnlyCheckBox)
        Me.Controls.Add(Me._EditedEngineeringUpDown)
        Me.Controls.Add(Me._ScaledValueTextBox)
        Me.Controls.Add(Me._ScaledValueTextBoxLabel)
        Me.Controls.Add(Me._EditedEngineeringUpDownLabel)
        Me.Controls.Add(Me._EnterValueNumericLabel)
        Me.Controls.Add(Me._EngineeringScaleComboBoxLabel)
        Me.Controls.Add(Me._EngineeringScaleComboBox)
        Me.Controls.Add(Me._DecimalPlacesNumericLabel)
        Me.Controls.Add(Me._DecimalPlacesNumeric)
        Me.Controls.Add(Me._UnitTextBoxLabel)
        Me.Controls.Add(Me._UnitTextBox)
        Me.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "EngineeringUpDownForm"
        Me.Text = "Engineering Up Down Form"
        CType(Me._DecimalPlacesNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._EnterValueNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._EditedEngineeringUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _DecimalPlacesNumericLabel As System.Windows.Forms.Label
    Private WithEvents _DecimalPlacesNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _EngineeringScaleComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _EngineeringScaleComboBoxLabel As System.Windows.Forms.Label
    Private WithEvents _ToolTip As System.Windows.Forms.ToolTip
    Private WithEvents _UnitTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _UnitTextBox As System.Windows.Forms.TextBox
    Private WithEvents _EditedEngineeringUpDown As isr.Core.Controls.EngineeringUpDown
    Private WithEvents _ScaledValueTextBox As System.Windows.Forms.TextBox
    Private WithEvents _ReadOnlyCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _EnterValueNumeric As isr.Core.Controls.NumericUpDown
    Private WithEvents _EnterValueNumericLabel As System.Windows.Forms.Label
    Private WithEvents _EditedEngineeringUpDownLabel As System.Windows.Forms.Label
    Private WithEvents _ScaledValueTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents NumericUpDown1 As isr.Core.Controls.NumericUpDown
End Class
