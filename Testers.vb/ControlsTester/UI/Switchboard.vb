Imports System.Runtime.CompilerServices
Imports System.ComponentModel
Imports ControlsTester.ExceptionExtensions

''' <summary> Switches between test panels. </summary>
''' <remarks>
''' (c) 2003 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2014-04-05, . based on legacy code. </para>
''' </remarks>
Public Class Switchboard

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of this class. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        Me.InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary> Event handler. Called by form for load events. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try


            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            Me._MessagesTextBox.AddMessage("Loading...")

            ' instantiate form objects
            Me.PopulateTestPanelSelector()

            ' set the form caption
            Me.Text = Extensions.BuildDefaultCaption("TESTER")

            ' center the form
            Me.CenterToScreen()

        Catch

            Me._MessagesTextBox.AddMessage("Exception...")

            ' Use throw without an argument in order to preserve the stack location 
            ' where the exception was initially raised.
            Throw

        Finally

            Me._MessagesTextBox.AddMessage("Loaded.")

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary> Closes the form and exits the application. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ExitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ExitButton.Click

        Try
            Me.Close()
        Catch ex As Exception
            ex.Data.Add("@isr", "Unhandled Exception.")
            MessageBox.Show(ex.ToFullBlownString, "Unhandled Exception",
                            MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        End Try

    End Sub

    ''' <summary> Cancel button click. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub CancelButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _CancelButton.Click
        Me.Close()
    End Sub

    ''' <summary> Tests button click. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub TestButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _TestButton.Click

        If True Then
            Dim fi As New System.IO.FileInfo(System.Windows.Forms.Application.ExecutablePath)
            Debug.Print(fi.FullName)
        End If

    End Sub

#End Region

#Region " LOGON "

        Private WithEvents MachineLogOn As isr.Core.Controls.MachineLogOn

    ''' <summary> Machine log on property changed. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property changed event information. </param>
    Private Sub MachineLogOn_PropertyChanged(sender As Object, e As System.ComponentModel.PropertyChangedEventArgs) Handles MachineLogOn.PropertyChanged
        If Me.InvokeRequired Then
            Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.MachineLogOn_PropertyChanged), New Object() {sender, e})
        End If
        Dim logon As isr.Core.Controls.MachineLogOn = CType(sender, isr.Core.Controls.MachineLogOn)
        If logon IsNot Nothing Then
            Select Case e.PropertyName
                Case "UserName"
                    Me._MessagesTextBox.AddMessage($"User name: {logon.UserName}")
                Case "UserRoles"
                    Dim role As String = String.Empty
                    For Each s As String In logon.UserRoles
                        role = role & "," & s
                    Next
                    Me._MessagesTextBox.AddMessage($"User role: {role}")
                Case "IsAuthenticated"
                    If logon.IsAuthenticated Then
                        Me._MessagesTextBox.AddMessage($"User {logon.UserName} authenticated")
                    Else
                        Me._MessagesTextBox.AddMessage("User not authenticated")
                    End If
                    If Me._Popup IsNot Nothing Then
                        ' _popup.Hide()
                    End If
            End Select
        End If
    End Sub

    ''' <summary> The popup. </summary>
    Private _Popup As ToolStripDropDown

    ''' <summary> Logs on button click. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub LogOnButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _LogOnButton.Click

        Dim roles As New List(Of String) From {"Administrators"}
        Me.MachineLogOn = New isr.Core.Controls.MachineLogOn
        If True Then
            Dim content As New isr.Core.Controls.LogOnControl With {.UserLogOn = Me.MachineLogOn}
            content.ShowPopup(Me, System.Drawing.Point.Add(Me._LogOnButton.Parent.Location, New System.Drawing.Size(Me._LogOnButton.Location.X, Me._LogOnButton.Location.Y)))
        Else
            ' logon.Validate("admin", New System.Collections.ObjectModel.ReadOnlyCollection(Of String)(roles))
            Dim content As New isr.Core.Controls.LogOnControl With {.UserLogOn = Me.MachineLogOn, .Enabled = True}
            Dim host As New ToolStripControlHost(content) With {.Enabled = True, .Margin = Padding.Empty, .Padding = Padding.Empty}
            Me._Popup = New ToolStripDropDown() With {.Margin = Padding.Empty, .Padding = Padding.Empty}
            Me._Popup.Items.Add(host)
            Me._Popup.Show(Me, System.Drawing.Point.Add(Me._LogOnButton.Parent.Location,
                                                        New System.Drawing.Size(Me._LogOnButton.Location.X, Me._LogOnButton.Location.Y)))
        End If

    End Sub

#End Region

#Region " SWITCH BOARD "

    ''' <summary> Descriptive Enumeration for test forms. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    Private Enum TestPanel

        ''' <summary> An enum constant representing the console control option. </summary>
        <System.ComponentModel.Description("Console Control")> ConsoleControl

        ''' <summary> An enum constant representing the drop down text box form option. </summary>
        <System.ComponentModel.Description("Drop Down text Box Control Form")> DropDownTextBoxForm

        ''' <summary> An enum constant representing the engineering up down control form option. </summary>
        <System.ComponentModel.Description("Engineering Up Down Control Form")> EngineeringUpDownControlForm

        ''' <summary> An enum constant representing the nullable up down control form option. </summary>
        <System.ComponentModel.Description("Nullable Up Down Control Form")> NullableUpDownControlForm

        ''' <summary> An enum constant representing the rich text box form option. </summary>
        <System.ComponentModel.Description("Rich Text Box Form")> RichTextBoxForm

        ''' <summary> An enum constant representing the rounded corners controls option. </summary>
        <System.ComponentModel.Description("Rounded Corners Controls Form")> RoundedCornersControls

        ''' <summary> An enum constant representing the selector form option. </summary>
        <System.ComponentModel.Description("Selector Form")> SelectorForm

        ''' <summary> An enum constant representing the wizard option. </summary>
        <System.ComponentModel.Description("Wizard Form")> Wizard

        ''' <summary> An enum constant representing the gnu plot option. </summary>
        <System.ComponentModel.Description("Gnu Plot Form")> GnuPlot

        ''' <summary> An enum constant representing the folder file explorer option. </summary>
        <System.ComponentModel.Description("Folder and File Explorer")> FolderFileExplorer

        ''' <summary> An enum constant representing the shell drag drop option. </summary>
        <System.ComponentModel.Description("Shell Drag and Drop")> ShellDragDrop

        ''' <summary> An enum constant representing the shell explorer option. </summary>
        <System.ComponentModel.Description("Shell Explorer")> ShellExplorer

        ''' <summary> An enum constant representing the visual styles option. </summary>
        <System.ComponentModel.Description("Visual Styles")> VisualStyles
    End Enum

    ''' <summary> Open selected items. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub OpenButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _OpenButton.Click

        Try

            Select Case CType(CType(Me._ActionsComboBox.SelectedItem, KeyValuePair(Of [Enum], String)).Key, TestPanel)

                Case TestPanel.ConsoleControl

                    Using myForm As New ConsoleControlForm
                        myForm.ShowDialog()
                    End Using

                Case TestPanel.EngineeringUpDownControlForm

                    Using myForm As New EngineeringUpDownForm
                        myForm.ShowDialog()
                    End Using

                Case TestPanel.NullableUpDownControlForm

                    Using myForm As New NullableNumericUpDownForm
                        myForm.ShowDialog()
                    End Using

                Case TestPanel.DropDownTextBoxForm

                    Using myForm As New DropDownTextBoxForm
                        myForm.ShowDialog()
                    End Using

                Case TestPanel.RichTextBoxForm

                    Using control As New isr.Core.Controls.RichTextEditControl
                        Using Font As New Drawing.Font("Lucida Console", 9)
                            control.Font = Font
                            Using form As isr.Core.Forma.ConsoleForm = isr.Core.Controls.RichTextEditControl.CreateForm("Editor", "Edit", control,
                                                                                                                isr.Core.My.MyLibrary.Logger)
                                form.ShowDialog(Nothing)
                            End Using
                        End Using
                    End Using

                Case TestPanel.RoundedCornersControls

                    Using myform As New RoundedControlsTester
                        myform.ShowDialog(Nothing)
                    End Using

                Case TestPanel.SelectorForm

                    Using myform As New SelectorForm
                        myform.ShowDialog(Nothing)
                    End Using

                Case TestPanel.Wizard

                    Using myform As New SimpleWizard
                        myform.ShowDialog(Nothing)
                    End Using

                Case TestPanel.FolderFileExplorer

                    Using myform As New isr.Core.Forma.ConsoleForm
                        Using ctrl As New isr.Core.Controls.FileExplorerView
                            ' ctrl.Refresh()
                            ctrl.SelectHome()
                            myform.AddPropertyNotifyControl(ctrl)
                            myform.ShowDialog(Nothing)
                        End Using
                    End Using

                Case TestPanel.ShellDragDrop

                    Using myform As New isr.Core.Forma.ConsoleForm
                        Using ctrl As New isr.Core.Shell.Forms.DragDropControl
                            myform.AddUserControl(ctrl)
                            myform.ShowDialog(Nothing)
                        End Using

                    End Using

                Case TestPanel.ShellExplorer

                    Using myform As New isr.Core.Forma.ConsoleForm
                        Using ctrl As New isr.Core.Shell.Forms.ExplorerControl
                            myform.AddUserControl(ctrl)
                            myform.ShowDialog(Nothing)

                        End Using
                    End Using

                Case TestPanel.VisualStyles

                    Using myform As New VisualStyleElementViewer
                        myform.ShowDialog(Nothing)
                    End Using

            End Select

        Catch ex As Exception

            ex.Data.Add("@isr", "Unhandled Exception.")
            MessageBox.Show(ex.ToFullBlownString, "Unhandled Exception",
                            MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)

        Finally
        End Try

    End Sub

    ''' <summary> Populates the list of test panels. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    Private Sub PopulateTestPanelSelector()

        Me._ActionsComboBox.DataSource = Nothing
        Me._ActionsComboBox.Items.Clear()
        Me._ActionsComboBox.DataSource = Extensions.ValueDescriptionPairs(GetType(TestPanel))
        Me._ActionsComboBox.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
        Me._ActionsComboBox.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)

    End Sub

#End Region

End Class

Public Module Extensions

    ''' <summary> Adds a message to 'message'. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="box">     The box control. </param>
    ''' <param name="message"> The message. </param>
    <Extension()>
    Public Sub AddMessage(ByVal box As TextBox, ByVal message As String)
        If box IsNot Nothing Then
            box.SelectionStart = box.Text.Length
            box.SelectionLength = 0
            box.SelectedText = message & Environment.NewLine
        End If
    End Sub

    ''' <summary>
    ''' Gets the <see cref="DescriptionAttribute"/> of an <see cref="System.Enum"/> type value.
    ''' </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="value"> The <see cref="System.Enum"/> type value. </param>
    ''' <returns> A string containing the text of the <see cref="DescriptionAttribute"/>. </returns>
    <Extension()>
    Public Function Description(ByVal value As System.Enum) As String

        If value Is Nothing Then Return String.Empty

        Dim candidate As String = value.ToString()
        Dim fieldInfo As Reflection.FieldInfo = value.GetType().GetField(candidate)
        Dim attributes As DescriptionAttribute() = CType(fieldInfo.GetCustomAttributes(GetType(DescriptionAttribute), False), DescriptionAttribute())

        If attributes IsNot Nothing AndAlso attributes.Length > 0 Then
            candidate = attributes(0).Description
        End If
        Return candidate

    End Function

    ''' <summary> Gets a Key Value Pair description item. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="value"> The <see cref="System.Enum"/> type value. </param>
    ''' <returns> A list of. </returns>
    <Extension()>
    Public Function ValueDescriptionPair(ByVal value As System.Enum) As System.Collections.Generic.KeyValuePair(Of System.Enum, String)
        Return New System.Collections.Generic.KeyValuePair(Of System.Enum, String)(value, value.Description)
    End Function

    ''' <summary> Value description pairs. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="type"> The type. </param>
    ''' <returns> An IList. </returns>
    <Extension()>
    Public Function ValueDescriptionPairs(ByVal type As Type) As IList

        Dim keyValuePairs As ArrayList = New ArrayList()
        For Each value As System.Enum In System.Enum.GetValues(type)
            keyValuePairs.Add(value.ValueDescriptionPair())
        Next value
        Return keyValuePairs

    End Function

    ''' <summary> Builds the default caption. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="subtitle"> The subtitle. </param>
    ''' <returns> System.String. </returns>
    Public Function BuildDefaultCaption(ByVal subtitle As String) As String

        Dim builder As New System.Text.StringBuilder
        builder.Append(My.Application.Info.Title)
        builder.Append(" ")
        builder.Append(My.Application.Info.Version.ToString)
        If My.Application.Info.Version.Major < 1 Then
            builder.Append(".")
            Select Case My.Application.Info.Version.Minor
                Case 0
                    builder.Append("Alpha")
                Case 1
                    builder.Append("Beta")
                Case 2 To 8
                    builder.Append($"RC{My.Application.Info.Version.Minor - 1}")
                Case Else
                    builder.Append("Gold")
            End Select
        End If
        If Not String.IsNullOrWhiteSpace(subtitle) Then
            builder.Append(": ")
            builder.Append(subtitle)
        End If
        Return builder.ToString

    End Function

End Module
