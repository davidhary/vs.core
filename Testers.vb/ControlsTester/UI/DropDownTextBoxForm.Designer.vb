﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DropDownTextBoxForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.WinFormsDropDownTextBox = New System.Windows.Forms.TextBox()
        Me.WinFormsTextBox = New System.Windows.Forms.TextBox()
        Me.DropDownTextBox = New isr.Core.Controls.DropDownTextBox()
        Me.WinFormDropDownTextBoxLabel = New System.Windows.Forms.Label()
        Me.DropDownTextBoxLabel = New System.Windows.Forms.Label()
        Me.WinFormsTextBoxLabel = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'WinFormsDropDownTextBox
        '
        Me.WinFormsDropDownTextBox.Location = New System.Drawing.Point(12, 105)
        Me.WinFormsDropDownTextBox.Name = "WinFormsDropDownTextBox"
        Me.WinFormsDropDownTextBox.Size = New System.Drawing.Size(243, 20)
        Me.WinFormsDropDownTextBox.TabIndex = 0
        '
        'WinFormsTextBox
        '
        Me.WinFormsTextBox.Location = New System.Drawing.Point(12, 33)
        Me.WinFormsTextBox.Name = "WinFormsTextBox"
        Me.WinFormsTextBox.Size = New System.Drawing.Size(100, 20)
        Me.WinFormsTextBox.TabIndex = 1
        '
        'DropDownTextBox
        '
        Me.DropDownTextBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.DropDownTextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DropDownTextBox.Location = New System.Drawing.Point(283, 105)
        Me.DropDownTextBox.Margin = New System.Windows.Forms.Padding(0)
        Me.DropDownTextBox.Name = "DropDownTextBox"
        Me.DropDownTextBox.Size = New System.Drawing.Size(305, 26)
        Me.DropDownTextBox.TabIndex = 2
        '
        'WinFormDropDownTextBoxLabel
        '
        Me.WinFormDropDownTextBoxLabel.Location = New System.Drawing.Point(9, 70)
        Me.WinFormDropDownTextBoxLabel.Name = "WinFormDropDownTextBoxLabel"
        Me.WinFormDropDownTextBoxLabel.Size = New System.Drawing.Size(256, 32)
        Me.WinFormDropDownTextBoxLabel.TabIndex = 3
        Me.WinFormDropDownTextBoxLabel.Text = "Windows Forms Text Box with Mouse Drop Effect. Click to drop; double click to res" &
    "tore"
        '
        'DropDownTextBoxLabel
        '
        Me.DropDownTextBoxLabel.AutoSize = True
        Me.DropDownTextBoxLabel.Location = New System.Drawing.Point(286, 88)
        Me.DropDownTextBoxLabel.Name = "DropDownTextBoxLabel"
        Me.DropDownTextBoxLabel.Size = New System.Drawing.Size(299, 13)
        Me.DropDownTextBoxLabel.TabIndex = 4
        Me.DropDownTextBoxLabel.Text = "Custom Drop Down; Click the down arrow to drop and restore."
        '
        'WinFormsTextBoxLabel
        '
        Me.WinFormsTextBoxLabel.AutoSize = True
        Me.WinFormsTextBoxLabel.Location = New System.Drawing.Point(14, 17)
        Me.WinFormsTextBoxLabel.Name = "WinFormsTextBoxLabel"
        Me.WinFormsTextBoxLabel.Size = New System.Drawing.Size(102, 13)
        Me.WinFormsTextBoxLabel.TabIndex = 5
        Me.WinFormsTextBoxLabel.Text = "This text box is fixed"
        '
        'DropDownTextBoxForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(595, 363)
        Me.Controls.Add(Me.WinFormsTextBoxLabel)
        Me.Controls.Add(Me.DropDownTextBoxLabel)
        Me.Controls.Add(Me.WinFormDropDownTextBoxLabel)
        Me.Controls.Add(Me.DropDownTextBox)
        Me.Controls.Add(Me.WinFormsDropDownTextBox)
        Me.Controls.Add(Me.WinFormsTextBox)
        Me.Name = "DropDownTextBoxForm"
        Me.Text = "Drop Down Text Box Form"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents WinFormsDropDownTextBox As System.Windows.Forms.TextBox
    Private WithEvents WinFormsTextBox As System.Windows.Forms.TextBox
    Private WithEvents DropDownTextBox As isr.Core.Controls.DropDownTextBox
    Private WithEvents WinFormDropDownTextBoxLabel As Label
    Friend WithEvents WinFormsTextBoxLabel As Label
    Private WithEvents DropDownTextBoxLabel As Label
End Class
