Imports System.Text
Imports System.Reflection
Imports System.Windows.Forms.VisualStyles

''' <summary> A visual style element viewer. </summary>
''' <remarks> David, 2020-10-25. </remarks>
Public Class VisualStyleElementViewer

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.Windows.Forms.Form" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    Public Sub New()
        Me.InitializeComponent()
        Dim elementViewer1 As New ElementViewer()
        Me.Controls.Add(elementViewer1)
        Me.Text = elementViewer1.Text
        Me.Size = New Size(700, 550)
    End Sub
End Class

''' <summary> An element viewer. </summary>
''' <remarks> David, 2020-10-25. </remarks>
Public Class ElementViewer
    Inherits UserControl

    ''' <summary> The element. </summary>
    Private _Element As VisualStyleElement

    ''' <summary> The renderer. </summary>
    Private _Renderer As VisualStyleRenderer

    ''' <summary> Dictionary of elements. </summary>
    Private ReadOnly _ElementDictionary As New Dictionary(Of String,
            VisualStyleElement)

    ''' <summary> The description rectangle. </summary>
    Private _DescriptionRect As Rectangle

    ''' <summary> The display rectangle. </summary>
    Private _DisplayRect As Rectangle

    ''' <summary> The display rectangle full. </summary>
    Private _DisplayRectFull As Rectangle

    ''' <summary> Size of the current true. </summary>
    Private _CurrentTrueSize As New Size()

    ''' <summary> Information describing the element. </summary>
    Private ReadOnly _ElementDescription As New StringBuilder()

    ''' <summary> The first label. </summary>
    Private ReadOnly _Label1 As New Label()

        Private WithEvents TreeView1 As New TreeView()

        Private WithEvents DomainUpDown1 As New DomainUpDown()

    ''' <summary> True to draw element. </summary>
    Private _DrawElement As Boolean = False

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.Windows.Forms.UserControl" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    Public Sub New()
        Me.Location = New Point(10, 10)
        Me.Size = New Size(650, 500)
        Me.Text = "VisualStyleElement Viewer"
        Me.Font = SystemFonts.IconTitleFont
        Me.BackColor = Color.White
        Me.BorderStyle = BorderStyle.Fixed3D
        Me.AutoSize = True
    End Sub

    ''' <summary> Element viewer load. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ElementViewer_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

        ' Make sure the visual styles are enabled before 
        ' going any further.
        If Not Application.RenderWithVisualStyles Then
            Return
        End If

        Me._Label1.Location = New Point(320, 10)
        Me._Label1.Size = New Size(300, 60)
        Me._Label1.Text = "Expand the element class nodes in the " +
                          "tree view to access visual style elements. " +
                          "Click an element name to draw the element " +
                          "below. To change the size of a resizable " +
                          "element, use the spin control."

        Me.DomainUpDown1.Location = New Point(320, 80)
        Me.DomainUpDown1.Size = New Size(70, 30)
        Me.DomainUpDown1.ReadOnly = True
        Me.DomainUpDown1.Items.Add(ElementSizes.Large)
        Me.DomainUpDown1.Items.Add(ElementSizes.Medium)
        Me.DomainUpDown1.Items.Add(ElementSizes.TrueSize)
        Me.DomainUpDown1.SelectedIndex = 2
        Me.DomainUpDown1.DownButton()

        Me._DescriptionRect = New Rectangle(320, 120, 250, 50)
        Me._DisplayRect = New Rectangle(320, 160, 0, 0)
        Me._DisplayRectFull = New Rectangle(320, 160, 300, 200)

        ' Initialize the element and renderer to known good values.
        Me._Element = VisualStyleElement.Button.PushButton.Normal
        Me._Renderer = New VisualStyleRenderer(Me._Element)

        Me.SetupElementCollection()
        Me.SetupTreeView()

        Me.Controls.AddRange(New Control() {Me.TreeView1,
                Me.DomainUpDown1, Me._Label1})
    End Sub

    ''' <summary>
    ''' Sets up the element collection. Use reflection to build a Dictionary of all
    ''' VisualStyleElement objects exposed in the System.Windows.Forms.VisualStyles namespace.
    ''' </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    Private Sub SetupElementCollection()
        Dim elementName As New StringBuilder()
        Dim currentElement As VisualStyleElement
        Dim tempObject As Object

        ' Get array of first-level nested types within 
        ' VisualStyleElement; these are the element classes.
        Dim elementClasses As Type() =
                GetType(VisualStyleElement).GetNestedTypes()

        Dim elementClass As Type
        For Each elementClass In elementClasses

            ' Get an array of second-level nested types within
            ' VisualStyleElement; these are the element parts.
            Dim elementParts As Type() =
                    elementClass.GetNestedTypes()

            ' Get the index of the first '+' character in 
            ' the full element class name.
            Dim plusSignIndex As Integer = elementClass.FullName.IndexOf("+")

            Dim elementPart As Type
            For Each elementPart In elementParts

                ' Get an array of Shared property details 
                ' for  the current type. Each of these types have 
                ' properties that return VisualStyleElement objects.
                Dim elementProperties As PropertyInfo() = elementPart.GetProperties(BindingFlags.Static Or BindingFlags.Public)

                ' For each property, insert the unique full element   
                ' name and the element into the collection.
                Dim elementProperty As PropertyInfo
                For Each elementProperty In elementProperties

                    ' Get the element.
                    tempObject = elementProperty.GetValue(Nothing, BindingFlags.Static, Nothing, Nothing, Nothing)
                    currentElement = CType(tempObject, VisualStyleElement)

                    ' Append the full element name.
                    elementName.Append(elementClass.FullName, plusSignIndex + 1, elementClass.FullName.Length - plusSignIndex - 1)
                    elementName.Append(("." + elementPart.Name.ToString() + "." + elementProperty.Name))

                    ' Add the element and element name to 
                    ' the Dictionary.
                    Me._ElementDictionary.Add(elementName.ToString(), currentElement)

                    ' Clear the element name for the next iteration.
                    elementName.Remove(0, elementName.Length)
                Next elementProperty
            Next elementPart
        Next elementClass
    End Sub

    ''' <summary> Sets up the tree view. Initialize the tree view with the element names. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    Private Sub SetupTreeView()

        Me.TreeView1.Location = New Point(10, 10)
        Me.TreeView1.Size = New Size(300, 450)
        Me.TreeView1.BorderStyle = BorderStyle.FixedSingle
        Me.TreeView1.BackColor = Color.WhiteSmoke
        Me.TreeView1.SelectedNode = Nothing
        Me.TreeView1.BeginUpdate()

        ' An index into the top-level tree nodes.
        Dim nodeIndex As Integer = 0

        ' An index into the first '.' character in an element name.
        Dim firstDotIndex As Integer

        ' Initialize the element class name to compare 
        ' with the class name of the first element 
        ' in the Dictionary, and set this name to the first 
        ' top-level node.
        Dim compareClassName As New StringBuilder("Button")
        Me.TreeView1.Nodes.Add(
                New TreeNode(compareClassName.ToString()))

        ' The current element class name.
        Dim currentClassName As New StringBuilder()

        ' The text for each second-level node.
        Dim nodeText As New StringBuilder()

        Dim entry As KeyValuePair(Of String, VisualStyleElement)
        For Each entry In Me._ElementDictionary

            ' Isolate the class name of the current element.
            firstDotIndex = entry.Key.IndexOf(".")
            currentClassName.Append(entry.Key, 0, firstDotIndex)

            ' Determine whether we need to increment to the next 
            ' element class.
            If currentClassName.ToString() <>
                    compareClassName.ToString() Then

                ' Increment the index to the next top-level node 
                ' in the tree view.
                nodeIndex += 1

                ' Update the class name to compare with.
                compareClassName.Remove(0, compareClassName.Length)
                compareClassName.Append(entry.Key)
                compareClassName.Remove(firstDotIndex,
                        compareClassName.Length - firstDotIndex)

                ' Add a new top-level node to the tree view.
                Dim node As New TreeNode(compareClassName.ToString())
                Me.TreeView1.Nodes.Add(node)
            End If

            ' Get the text for the new second-level node.
            nodeText.Append(entry.Key, firstDotIndex + 1,
                    entry.Key.Length - firstDotIndex - 1)

            ' Create and insert the new second-level node.
            Dim newNode As New TreeNode(nodeText.ToString()) With {.Name = entry.Key}
            Me.TreeView1.Nodes(nodeIndex).Nodes.Add(newNode)

            currentClassName.Remove(0, currentClassName.Length)
            nodeText.Remove(0, nodeText.Length)
        Next entry

        Me.TreeView1.EndUpdate()
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.Paint" /> event. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnPaint(ByVal e As PaintEventArgs)
        MyBase.OnPaint(e)

        ' Do nothing further if visual styles are disabled.
        If Not Application.RenderWithVisualStyles Then
            Me.Text = "Visual styles are disabled."
            TextRenderer.DrawText(e.Graphics, Me.Text, Me.Font,
                    Me.Location, Me.ForeColor)
            Return
        End If

        ' Draw the element description.
        TextRenderer.DrawText(e.Graphics,
                Me._ElementDescription.ToString(), Me.Font,
                Me._DescriptionRect, Me.ForeColor,
                TextFormatFlags.WordBreak)

        ' Draw the element, if an element is selected.
        If Me._DrawElement Then
            Me._Renderer.DrawBackground(e.Graphics, Me._DisplayRect)
        End If
    End Sub

    ''' <summary> Set the element to draw. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Tree view event information. </param>
    Private Sub TreeView1_AfterSelect(ByVal sender As Object,
            ByVal e As TreeViewEventArgs) Handles TreeView1.AfterSelect

        ' Clear the element description.
        Me._ElementDescription.Remove(0, Me._ElementDescription.Length)

        ' If the user clicked a first-level node, disable drawing.
        If e.Node.Nodes.Count > 0 Then
            Me._DrawElement = False
            Me._ElementDescription.Append("No element is selected")
            Me.DomainUpDown1.Enabled = False

            ' The user clicked an element node.
        Else
            ' Add the element name to the description.
            Me._ElementDescription.Append(e.Node.Text)

            ' Get the element that corresponds to the selected  
            ' node's name.
            Dim key As String = e.Node.Name
            Me._Element = Me._ElementDictionary(key)

            ' Disable resizing if the element is not defined.
            If Not VisualStyleRenderer.IsElementDefined(Me._Element) Then
                Me._DrawElement = False
                Me._ElementDescription.Append(" is not defined.")
                Me.DomainUpDown1.Enabled = False
            Else
                ' Set the element to the renderer.
                Me._DrawElement = True
                Me._Renderer.SetParameters(Me._Element)
                Me._ElementDescription.Append(" is defined.")

                ' Get the system-defined size of the element.
                Dim g As Graphics = Me.CreateGraphics()
                Me._CurrentTrueSize = Me._Renderer.GetPartSize(g,
                        ThemeSizeType.True)
                g.Dispose()
                Me._DisplayRect.Size = Me._CurrentTrueSize

                Me.DomainUpDown1.Enabled = True
                Me.DomainUpDown1.SelectedIndex = 2
            End If
        End If
        Me.Invalidate()
    End Sub

    ''' <summary> Resize the element display area. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub DomainUpDown1_SelectedItemChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DomainUpDown1.SelectedItemChanged

        Select Case CInt(Me.DomainUpDown1.SelectedItem)
            Case CInt(ElementSizes.TrueSize)
                Me._DisplayRect.Size = Me._CurrentTrueSize
            Case CInt(ElementSizes.Medium)
                Me._DisplayRect.Size =
                    New Size(Me._DisplayRectFull.Width \ 2, Me._DisplayRectFull.Height \ 2)
            Case CInt(ElementSizes.Large)
                Me._DisplayRect.Size = Me._DisplayRectFull.Size
        End Select

        Me.Invalidate()
    End Sub

    ''' <summary> Values that represent element sizes. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    Private Enum ElementSizes

        ''' <summary> An enum constant representing the true size option. </summary>
        TrueSize

        ''' <summary> An enum constant representing the medium option. </summary>
        Medium

        ''' <summary> An enum constant representing the large option. </summary>
        Large
    End Enum

End Class
