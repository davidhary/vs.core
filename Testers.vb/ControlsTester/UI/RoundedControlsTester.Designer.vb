<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RoundedControlsTester
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me._Panel = New System.Windows.Forms.Panel()
        Me.RoundedPanel1 = New isr.Core.Controls.RoundedPanel()
        Me._RoundButton = New isr.Core.Controls.RoundedButton()
        Me._RoundedPanel = New isr.Core.Controls.RoundedPanel()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me._RoundedLabel = New isr.Core.Controls.RoundedLabel()
        Me._RoundedPanel.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        '_Panel
        '
        Me._Panel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._Panel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me._Panel.Location = New System.Drawing.Point(45, 145)
        Me._Panel.Name = "_Panel"
        Me._Panel.Size = New System.Drawing.Size(251, 51)
        Me._Panel.TabIndex = 2
        '
        'RoundedPanel1
        '
        Me.RoundedPanel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RoundedPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.RoundedPanel1.BorderSize = 3
        Me.RoundedPanel1.BottomLeftRadius = 15
        Me.RoundedPanel1.BottomRightRadius = 15
        Me.RoundedPanel1.Location = New System.Drawing.Point(46, 205)
        Me.RoundedPanel1.Name = "RoundedPanel1"
        Me.RoundedPanel1.ShadowAngle = 0
        Me.RoundedPanel1.Size = New System.Drawing.Size(251, 49)
        Me.RoundedPanel1.TabIndex = 5
        Me.RoundedPanel1.TopLeftRadius = 30
        Me.RoundedPanel1.TopRightRadius = 50
        '
        '_RoundButton
        '
        Me._RoundButton.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._RoundButton.BorderSize = 3
        Me._RoundButton.BottomLeftRadius = 13
        Me._RoundButton.BottomRightRadius = 15
        Me._RoundButton.Cursor = System.Windows.Forms.Cursors.Default
        Me._RoundButton.Location = New System.Drawing.Point(177, 85)
        Me._RoundButton.Name = "_RoundButton"
        Me._RoundButton.ShadowAngle = 0
        Me._RoundButton.Size = New System.Drawing.Size(126, 45)
        Me._RoundButton.TabIndex = 4
        Me._RoundButton.Text = "Button"
        Me._RoundButton.TopLeftRadius = 15
        Me._RoundButton.TopRightRadius = 15
        Me._RoundButton.UseVisualStyleBackColor = True
        '
        '_RoundedPanel
        '
        Me._RoundedPanel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._RoundedPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me._RoundedPanel.BorderSize = 3
        Me._RoundedPanel.BottomLeftRadius = 50
        Me._RoundedPanel.BottomRightRadius = 50
        Me._RoundedPanel.Controls.Add(Me.TableLayoutPanel1)
        Me._RoundedPanel.Location = New System.Drawing.Point(62, 12)
        Me._RoundedPanel.Name = "_RoundedPanel"
        Me._RoundedPanel.Padding = New System.Windows.Forms.Padding(6)
        Me._RoundedPanel.ShadowAngle = 0
        Me._RoundedPanel.Size = New System.Drawing.Size(251, 64)
        Me._RoundedPanel.TabIndex = 1
        Me._RoundedPanel.TopLeftRadius = 30
        Me._RoundedPanel.TopRightRadius = 50
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 3.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 3.0!))
        Me.TableLayoutPanel1.Controls.Add(Me._RoundedLabel, 1, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(6, 6)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 3.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 3.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(239, 52)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        '_RoundedLabel
        '
        Me._RoundedLabel.BackColor = System.Drawing.SystemColors.Control
        Me._RoundedLabel.BorderSize = 3
        Me._RoundedLabel.BottomLeftRadius = 23
        Me._RoundedLabel.BottomRightRadius = 23
        Me._RoundedLabel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._RoundedLabel.Location = New System.Drawing.Point(6, 6)
        Me._RoundedLabel.Margin = New System.Windows.Forms.Padding(3)
        Me._RoundedLabel.Name = "_RoundedLabel"
        Me._RoundedLabel.ShadowAngle = 0
        Me._RoundedLabel.Size = New System.Drawing.Size(227, 40)
        Me._RoundedLabel.TabIndex = 0
        Me._RoundedLabel.Text = "RoundedLabel1"
        Me._RoundedLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me._RoundedLabel.TopLeftRadius = 23
        Me._RoundedLabel.TopRightRadius = 23
        '
        'RoundedControlsTester
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(348, 309)
        Me.Controls.Add(Me.RoundedPanel1)
        Me.Controls.Add(Me._RoundButton)
        Me.Controls.Add(Me._Panel)
        Me.Controls.Add(Me._RoundedPanel)
        Me.Name = "RoundedControlsTester"
        Me.Text = "Rounded Controls Tester"
        Me._RoundedPanel.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Private WithEvents _Panel As Panel
    Private WithEvents _RoundedPanel As isr.Core.Controls.RoundedPanel
    Private WithEvents _RoundedLabel As isr.Core.Controls.RoundedLabel
    Private WithEvents _RoundButton As isr.Core.Controls.RoundedButton
    Private WithEvents RoundedPanel1 As isr.Core.Controls.RoundedPanel
    Private WithEvents TableLayoutPanel1 As TableLayoutPanel
End Class
