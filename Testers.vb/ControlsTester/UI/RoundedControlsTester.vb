﻿''' <summary> A rounded controls tester. </summary>
''' <remarks> David, 2020-10-25. </remarks>
Public Class RoundedControlsTester

    Public Sub New()

        ' This call is required by the designer.
        Me.InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        ' Display the OK close button. 
        Me.MinimizeBox = False
        Me.Text = "Controls tester"

    End Sub

End Class