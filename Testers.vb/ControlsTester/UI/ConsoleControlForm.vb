Imports System.Runtime.InteropServices

''' <summary> The application's main form. </summary>
''' <remarks>
''' (c) 2012 icemanind. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2016-07-09 </para><para>
''' http://www.codeproject.com/Articles/1053951/Console-Control </para>
''' </remarks>
Partial Public Class ConsoleControlForm
    Inherits Form

#Region " CONSTRUCTOR "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.Windows.Forms.Form" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2234:PassSystemUriObjectsInsteadOfStrings")>
    Public Sub New()
        Me.InitializeComponent()

        Dim codeBase As String = System.Reflection.Assembly.GetExecutingAssembly().CodeBase
        Dim uri As New UriBuilder(codeBase)
        Me._CurrentDirectory = System.IO.Path.GetDirectoryName(System.Uri.UnescapeDataString(uri.Path))
    End Sub

    ''' <summary> Clean up any resources being used. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="disposing"> true if managed resources should be disposed; otherwise, false. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso (Me.components IsNot Nothing) Then
            Me.components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    ''' <summary> A safe native methods. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    Private NotInheritable Class SafeNativeMethods

        ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        ''' <remarks> David, 2020-10-25. </remarks>
        Private Sub New()
            MyBase.New()
        End Sub

        ''' <summary> Gets the focus. </summary>
        ''' <remarks> David, 2020-10-25. </remarks>
        ''' <returns> The focus. </returns>
        <DllImport("user32.dll", CharSet:=CharSet.Auto, CallingConvention:=CallingConvention.Winapi)>
        Friend Shared Function GetFocus() As IntPtr
        End Function
    End Class

#End Region

    ''' <summary> Pathname of the current directory. </summary>
    Private _CurrentDirectory As String

    ''' <summary> Form 1 load. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Form1_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        Me._LeftConsoleControl.SetCursorPosition(0, 0)
        Me._LeftConsoleControl.Write("Hello World!")
        Me._LeftConsoleControl.SetCursorPosition(2, 15)

        Me._LeftConsoleControl.Write("Look at this message!", Color.Yellow, Color.Blue)

        Me._CursorColumnTextBox.Text = Me._LeftConsoleControl.GetCursorPosition().Column.ToString()
        Me._CursorRowTextBox.Text = Me._LeftConsoleControl.GetCursorPosition().Row.ToString()

        Me._MessageTextBox.Text = "Hello World!"

        Me._BackgroundColorPanel.BackColor = Me._LeftConsoleControl.CurrentBackgroundColor
        Me._ForegroundColorPanel.BackColor = Me._LeftConsoleControl.CurrentForegroundColor

        Me._LeftConsoleControl.AllowInput = False

        Me._RightConsoleControl.SetCursorPosition(0, 0)
        Me._RightConsoleControl.Write("Welcome to Iceman Shell!")
        Me._RightConsoleControl.SetCursorPosition(1, 0)
        Me._RightConsoleControl.Write($"The current date and time is {DateTimeOffset.Now:F}")
        Me._RightConsoleControl.Write()
        Me._RightConsoleControl.Write("Use the command 'help' if you need help!")
        Me._RightConsoleControl.SetCursorPosition(Me._RightConsoleControl.GetCursorPosition().Row + 2, 0)
        Me.ShowPrompt()


        AddHandler Me._RightConsoleControl.LineEntered, AddressOf Me.ConsoleControl2LineEntered

        Me.ActiveControl = Me._RightConsoleControl
    End Sub

    ''' <summary> Shows the prompt. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    Private Sub ShowPrompt()
        Me._RightConsoleControl.Write()
        Me._RightConsoleControl.Write("# ")
    End Sub

    ''' <summary> Console control 2 line entered. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Line entered event information. </param>
    Private Sub ConsoleControl2LineEntered(ByVal sender As Object, ByVal e As isr.Core.Controls.LineEnteredEventArgs)
        If e Is Nothing Then Return
        Dim line As String = e.Line
        line = line.Trim()
        Dim ndx As Integer = line.IndexOf(" "c)
        Dim cmd As String = If(ndx < 0, line, line.Substring(0, ndx))
        Dim parameters As String = If(ndx < 0, "", line.Remove(0, ndx + 1))

        Select Case cmd.ToLower()
            Case ""
                Me.ShowPrompt()
            Case "help"
                Me.ShowHelp()
                Me.ShowPrompt()
            Case "exit"
                Application.Exit()
            Case "ls"
                Me.DoLsCommand(parameters)
            Case "echo"
                Me.DoEchoCommand(parameters)
                Me.ShowPrompt()
            Case "date"
                Me.DoDateCommand()
                Me.ShowPrompt()
            Case "pwd"
                Me.DoPwdCommand()
                Me.ShowPrompt()
            Case "cd"
                Me.DoCdCommand(parameters)
                Me.ShowPrompt()
            Case Else
                Me._RightConsoleControl.Write("> Unknown Command!")
                Me._RightConsoleControl.Write()
                Me.ShowPrompt()
        End Select
    End Sub

    ''' <summary> Button clear console click. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub BtnClearConsole_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _ClearConsoleButton.Click
        Me._LeftConsoleControl.Clear()
        Me._CursorColumnTextBox.Text = Me._LeftConsoleControl.GetCursorPosition().Column.ToString()
        Me._CursorRowTextBox.Text = Me._LeftConsoleControl.GetCursorPosition().Row.ToString()
    End Sub

    ''' <summary> Button move cursor click. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub BtnMoveCursor_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _MoveCursorButton.Click
        Dim row As Integer = Integer.Parse(Me._CursorRowTextBox.Text)
        Dim column As Integer = Integer.Parse(Me._CursorColumnTextBox.Text)

        If row < 0 OrElse row > 24 Then
            MessageBox.Show("Cursor Row Must be between 0 and 24!")
            Return
        End If

        If column < 0 OrElse column > 79 Then
            MessageBox.Show("Cursor Column Must be between 0 and 79!")
            Return
        End If

        Me._LeftConsoleControl.SetCursorPosition(row, column)
    End Sub

    ''' <summary> Rb underline checked changed. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub RbUnderline_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles _UnderlineRadioButton.CheckedChanged
        Me.ChangeCursorType()
    End Sub

    ''' <summary> Rb block checked changed. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub RbBlock_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles _BlockRadioButton.CheckedChanged
        Me.ChangeCursorType()
    End Sub

    ''' <summary> Rb none checked changed. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub RbNone_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles _NoneRadioButton.CheckedChanged
        Me.ChangeCursorType()
    End Sub

    ''' <summary> Change cursor type. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    Private Sub ChangeCursorType()
        If Me._UnderlineRadioButton.Checked Then
            Me._LeftConsoleControl.CursorType = isr.Core.Controls.CursorType.Underline
        End If
        If Me._BlockRadioButton.Checked Then
            Me._LeftConsoleControl.CursorType = isr.Core.Controls.CursorType.Block
        End If
        If Me._NoneRadioButton.Checked Then
            Me._LeftConsoleControl.CursorType = isr.Core.Controls.CursorType.Invisible
        End If
    End Sub

    ''' <summary> Button write message click. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub BtnWriteMessage_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _WriteMessageButton.Click
        Me._LeftConsoleControl.Write(Me._MessageTextBox.Text)
    End Sub

    ''' <summary> Pnl background color double click. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub PnlBackgroundColor_DoubleClick(ByVal sender As Object, ByVal e As EventArgs) Handles _BackgroundColorPanel.DoubleClick
        Me._ColorDialog.Color = Me._LeftConsoleControl.CurrentBackgroundColor
        Dim dr As DialogResult = Me._ColorDialog.ShowDialog()

        If dr = System.Windows.Forms.DialogResult.OK Then
            Me._BackgroundColorPanel.BackColor = Me._ColorDialog.Color
            Me._LeftConsoleControl.CurrentBackgroundColor = Me._ColorDialog.Color
        End If
    End Sub

    ''' <summary> Pnl foreground color double click. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub PnlForegroundColor_DoubleClick(ByVal sender As Object, ByVal e As EventArgs) Handles _ForegroundColorPanel.DoubleClick
        Me._ColorDialog.Color = Me._LeftConsoleControl.CurrentForegroundColor
        Dim dr As DialogResult = Me._ColorDialog.ShowDialog()

        If dr = System.Windows.Forms.DialogResult.OK Then
            Me._ForegroundColorPanel.BackColor = Me._ColorDialog.Color
            Me._LeftConsoleControl.CurrentForegroundColor = Me._ColorDialog.Color
        End If
    End Sub

    ''' <summary> Searches for the first focused control. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <returns> The found focused control. </returns>
    Private Function FindFocusedControl() As Control
        Dim focusedControl As Control = Nothing
        Dim focusedHandle As IntPtr = SafeNativeMethods.GetFocus()
        If focusedHandle <> IntPtr.Zero Then
            focusedControl = FromHandle(focusedHandle)
        End If
        Return focusedControl
    End Function

    ''' <summary> Form 1 paint. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Paint event information. </param>
    Private Sub Form1_Paint(ByVal sender As Object, ByVal e As PaintEventArgs) Handles MyBase.Paint
        Const offsetX As Integer = 2

        Dim b As New Bitmap(Me.ClientSize.Width, Me.ClientSize.Height)
        Dim g As Graphics = Graphics.FromImage(b)

        Dim c1 As Control = Me.FindFocusedControl()

        If c1 IsNot Nothing AndAlso c1.Name.StartsWith("consoleControl") Then
            For i As Integer = 1 To 5
                Dim p As New Pen(Color.FromArgb(Math.Min(25 * (i * 2), 255), 50, 80, 255))
                g.DrawRectangle(p, c1.Location.X - i, c1.Location.Y - i, c1.Size.Width + i + i, c1.Size.Height + i + i)
            Next i
        End If

        g.DrawLine(New Pen(Color.FromArgb(200, 200, 200)), 660 + offsetX, 3, 660 + offsetX, Me.ClientSize.Height - 5)
        g.DrawLine(New Pen(Color.FromArgb(180, 180, 180)), 661 + offsetX, 3, 661 + offsetX, Me.ClientSize.Height - 5)
        g.DrawLine(New Pen(Color.FromArgb(128, 128, 128)), 662 + offsetX, 3, 662 + offsetX, Me.ClientSize.Height - 5)
        g.DrawLine(New Pen(Color.FromArgb(108, 108, 108)), 663 + offsetX, 3, 663 + offsetX, Me.ClientSize.Height - 5)

        e.Graphics.DrawImage(b, New Point(0, 0))
        g.Dispose()
        b.Dispose()
    End Sub

    ''' <summary> Console control 1 enter. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ConsoleControl1_Enter(ByVal sender As Object, ByVal e As EventArgs) Handles _LeftConsoleControl.Enter
        Me.Refresh()
    End Sub

    ''' <summary> Console control 1 leave. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ConsoleControl1_Leave(ByVal sender As Object, ByVal e As EventArgs) Handles _LeftConsoleControl.Leave
        Me.Refresh()
    End Sub

    ''' <summary> Console control 2 enter. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ConsoleControl2_Enter(ByVal sender As Object, ByVal e As EventArgs) Handles _RightConsoleControl.Enter
        Me.Refresh()
    End Sub

    ''' <summary> Console control 2 leave. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ConsoleControl2_Leave(ByVal sender As Object, ByVal e As EventArgs) Handles _RightConsoleControl.Leave
        Me.Refresh()
    End Sub

    ''' <summary> Shows the help. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    Private Sub ShowHelp()
        Me._RightConsoleControl.Write("IcemanShell is just a 'toy' shell to show off Icemanind's Console Control.")
        Me._RightConsoleControl.Write()
        Me._RightConsoleControl.Write("Here is a list of commands:")
        Me._RightConsoleControl.Write()
        Me._RightConsoleControl.Write("(Note that the commands are NOT case sensitive)")
        Me._RightConsoleControl.Write()
        Me._RightConsoleControl.Write()
        Me._RightConsoleControl.Write("----------------------------------------------------------------------")
        Me._RightConsoleControl.Write()
        Me._RightConsoleControl.Write("HELP                Shows this help message")
        Me._RightConsoleControl.Write()
        Me._RightConsoleControl.Write("EXIT                Quits the application")
        Me._RightConsoleControl.Write()
        Me._RightConsoleControl.Write("LS <path>           Shows the contents of the specified directory.")
        Me._RightConsoleControl.Write()
        Me._RightConsoleControl.Write("ECHO [string]       Echo's the [string] to the console.")
        Me._RightConsoleControl.Write()
        Me._RightConsoleControl.Write("DATE                Shows the current date.")
        Me._RightConsoleControl.Write()
        Me._RightConsoleControl.Write("PWD                 Shows the current working directory.")
        Me._RightConsoleControl.Write()
        Me._RightConsoleControl.Write("CD <path>           Change the current working directory to <path>.")
        Me._RightConsoleControl.Write()
    End Sub

    ''' <summary> Executes the ls command operation. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="parameters"> Options for controlling the operation. </param>
    Private Sub DoLsCommand(ByVal parameters As String)
        Dim path As String
        Dim bgColor As Color = Me._RightConsoleControl.CurrentBackgroundColor
        Dim fgColor As Color = Me._RightConsoleControl.CurrentForegroundColor

        path = If(String.IsNullOrEmpty(parameters), Me._CurrentDirectory, parameters.Trim(""""c))

        Dim files() As String = System.IO.Directory.GetFiles(path)
        Dim directories() As String = System.IO.Directory.GetDirectories(path)

        Me._RightConsoleControl.CurrentForegroundColor = Color.CadetBlue
        For Each directory As String In directories.OrderBy(Function(z) z)
            Dim dirName As String = "[" & System.IO.Path.GetFileName(directory) & "]"
            Me._RightConsoleControl.Write($"{If(dirName.Length > 40, Environment.NewLine & dirName & Environment.NewLine, dirName),-40}")
        Next directory

        For Each file As String In files.OrderBy(Function(z) z)
            Dim fileName As String = If(System.IO.Path.GetFileName(file), "")
            Dim extension As String = If(System.IO.Path.GetExtension(file), "")
            Select Case extension.ToLower()
                Case ".exe"
                    Me._RightConsoleControl.CurrentForegroundColor = Color.FromArgb(100, 255, 100)
                Case ".dll"
                    Me._RightConsoleControl.CurrentForegroundColor = Color.FromArgb(10, 165, 10)
                Case Else
                    Me._RightConsoleControl.CurrentForegroundColor = Color.LightGray
            End Select
            Me._RightConsoleControl.Write($"{If(fileName.Length > 40, Environment.NewLine & fileName & Environment.NewLine, fileName),-40}")
        Next file

        Me._RightConsoleControl.CurrentBackgroundColor = bgColor
        Me._RightConsoleControl.CurrentForegroundColor = fgColor

        Me.ShowPrompt()
    End Sub

    ''' <summary> Executes the echo command operation. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="parameters"> Options for controlling the operation. </param>
    Private Sub DoEchoCommand(ByVal parameters As String)
        Me._RightConsoleControl.Write(parameters)
        Me._RightConsoleControl.Write()
    End Sub

    ''' <summary> Executes the date command operation. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    Private Sub DoDateCommand()
        Me._RightConsoleControl.Write($"{DateTimeOffset.Now:ddd MMM d HH:mm:ss K yyyy}")
        Me._RightConsoleControl.Write()
    End Sub

    ''' <summary> Executes the password command operation. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    Private Sub DoPwdCommand()

        Me._RightConsoleControl.Write(Me._CurrentDirectory)
        Me._RightConsoleControl.Write()
    End Sub

    ''' <summary> Executes the CD command operation. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="parameters"> Options for controlling the operation. </param>
    Private Sub DoCdCommand(ByVal parameters As String)
        parameters = parameters.Trim(""""c)

        If parameters.Length >= 2 AndAlso parameters.Chars(1) = ":"c Then
            If System.IO.Directory.Exists(parameters) Then
                Me._currentDirectory = parameters
            Else
                Me._RightConsoleControl.Write("Invalid Directory ==> " & parameters)
                Me._RightConsoleControl.Write()
                Return
            End If
        ElseIf parameters.Length > 0 AndAlso parameters.Chars(0) = "\"c Then
            If System.IO.Directory.Exists(System.IO.Path.GetPathRoot(Me._currentDirectory) & parameters.Remove(0, 1)) Then
                Me._currentDirectory = System.IO.Path.GetPathRoot(Me._currentDirectory) & parameters.Remove(0, 1)
            Else
                Me._RightConsoleControl.Write("Invalid Directory ==> " & System.IO.Path.GetPathRoot(Me._currentDirectory) & parameters.Remove(0, 1))
                Me._RightConsoleControl.Write()
                Return
            End If
        ElseIf parameters.Length > 0 Then
            If System.IO.Directory.Exists(Me._currentDirectory & "\" & parameters) Then
                Me._currentDirectory = Me._currentDirectory & "\" & parameters
            Else
                Me._RightConsoleControl.Write("Invalid Directory ==> " & Me._currentDirectory & parameters)
                Me._RightConsoleControl.Write()
                Return
            End If
        End If

        Me._currentDirectory = Me._currentDirectory.Replace("\\", "\")
        Me._RightConsoleControl.Write($"Current Directory is now: {Me._CurrentDirectory}")
        Me._RightConsoleControl.Write()
    End Sub
End Class
