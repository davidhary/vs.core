<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class NullableNumericUpDownForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me._ValueTextBoxLabel = New System.Windows.Forms.Label()
        Me._SpinningValueTextBox = New System.Windows.Forms.TextBox()
        Me._ChangedValueTextBoxLabel = New System.Windows.Forms.Label()
        Me._ChangedValueTextBox = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me._ValidatedValueTextBox = New System.Windows.Forms.TextBox()
        Me._SpinningNullableValueTextBox = New System.Windows.Forms.TextBox()
        Me._ValidatedNullableValueTextBox = New System.Windows.Forms.TextBox()
        Me._ChangedNullableValueTextBox = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me._NullableNumericUpDown = New isr.Core.Controls.NullableNumericUpDown()
        Me._NullableTextTextBoxLabel = New System.Windows.Forms.Label()
        Me._NullableTextTextBox = New System.Windows.Forms.TextBox()
        Me._NilifyButton = New System.Windows.Forms.Button()
        Me._NullableValueTextBox = New System.Windows.Forms.TextBox()
        Me._Timer = New System.Windows.Forms.Timer(Me.components)
        CType(Me._NullableNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_ValueTextBoxLabel
        '
        Me._ValueTextBoxLabel.AutoSize = True
        Me._ValueTextBoxLabel.Location = New System.Drawing.Point(40, 75)
        Me._ValueTextBoxLabel.Name = "_ValueTextBoxLabel"
        Me._ValueTextBoxLabel.Size = New System.Drawing.Size(51, 13)
        Me._ValueTextBoxLabel.TabIndex = 4
        Me._ValueTextBoxLabel.Text = "Spinning:"
        Me._ValueTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        '_SpinningValueTextBox
        '
        Me._SpinningValueTextBox.Location = New System.Drawing.Point(93, 72)
        Me._SpinningValueTextBox.Name = "_SpinningValueTextBox"
        Me._SpinningValueTextBox.Size = New System.Drawing.Size(100, 20)
        Me._SpinningValueTextBox.TabIndex = 5
        '
        '_ChangedValueTextBoxLabel
        '
        Me._ChangedValueTextBoxLabel.AutoSize = True
        Me._ChangedValueTextBoxLabel.Location = New System.Drawing.Point(38, 113)
        Me._ChangedValueTextBoxLabel.Name = "_ChangedValueTextBoxLabel"
        Me._ChangedValueTextBoxLabel.Size = New System.Drawing.Size(53, 13)
        Me._ChangedValueTextBoxLabel.TabIndex = 7
        Me._ChangedValueTextBoxLabel.Text = "Changed:"
        Me._ChangedValueTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        '_ChangedValueTextBox
        '
        Me._ChangedValueTextBox.Location = New System.Drawing.Point(93, 110)
        Me._ChangedValueTextBox.Name = "_ChangedValueTextBox"
        Me._ChangedValueTextBox.Size = New System.Drawing.Size(100, 20)
        Me._ChangedValueTextBox.TabIndex = 8
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(37, 152)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(54, 13)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Validated:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        '_ValidatedValueTextBox
        '
        Me._ValidatedValueTextBox.Location = New System.Drawing.Point(93, 149)
        Me._ValidatedValueTextBox.Name = "_ValidatedValueTextBox"
        Me._ValidatedValueTextBox.Size = New System.Drawing.Size(100, 20)
        Me._ValidatedValueTextBox.TabIndex = 11
        '
        '_SpinningNullableValueTextBox
        '
        Me._SpinningNullableValueTextBox.Location = New System.Drawing.Point(199, 72)
        Me._SpinningNullableValueTextBox.Name = "_SpinningNullableValueTextBox"
        Me._SpinningNullableValueTextBox.Size = New System.Drawing.Size(100, 20)
        Me._SpinningNullableValueTextBox.TabIndex = 6
        '
        '_ValidatedNullableValueTextBox
        '
        Me._ValidatedNullableValueTextBox.Location = New System.Drawing.Point(199, 149)
        Me._ValidatedNullableValueTextBox.Name = "_ValidatedNullableValueTextBox"
        Me._ValidatedNullableValueTextBox.Size = New System.Drawing.Size(100, 20)
        Me._ValidatedNullableValueTextBox.TabIndex = 12
        '
        '_ChangedNullableValueTextBox
        '
        Me._ChangedNullableValueTextBox.Location = New System.Drawing.Point(199, 110)
        Me._ChangedNullableValueTextBox.Name = "_ChangedNullableValueTextBox"
        Me._ChangedNullableValueTextBox.Size = New System.Drawing.Size(100, 20)
        Me._ChangedNullableValueTextBox.TabIndex = 9
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(97, 56)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(37, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Value:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(196, 56)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(78, 13)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Nullable Value:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        '_NullableNumericUpDown
        '
        Me._NullableNumericUpDown.Location = New System.Drawing.Point(49, 16)
        Me._NullableNumericUpDown.Maximum = New Decimal(New Integer() {100000, 0, 0, 0})
        Me._NullableNumericUpDown.Name = "_NullableNumericUpDown"
        Me._NullableNumericUpDown.ReadOnlyBackColor = System.Drawing.Color.Empty
        Me._NullableNumericUpDown.ReadOnlyForeColor = System.Drawing.Color.Empty
        Me._NullableNumericUpDown.ReadWriteBackColor = System.Drawing.Color.Empty
        Me._NullableNumericUpDown.ReadWriteForeColor = System.Drawing.Color.Empty
        Me._NullableNumericUpDown.Size = New System.Drawing.Size(145, 20)
        Me._NullableNumericUpDown.TabIndex = 0
        '
        '_NullableTextTextBoxLabel
        '
        Me._NullableTextTextBoxLabel.AutoSize = True
        Me._NullableTextTextBoxLabel.Location = New System.Drawing.Point(164, 186)
        Me._NullableTextTextBoxLabel.Name = "_NullableTextTextBoxLabel"
        Me._NullableTextTextBoxLabel.Size = New System.Drawing.Size(31, 13)
        Me._NullableTextTextBoxLabel.TabIndex = 13
        Me._NullableTextTextBoxLabel.Text = "Text:"
        Me._NullableTextTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        '_NullableTextTextBox
        '
        Me._NullableTextTextBox.Location = New System.Drawing.Point(198, 183)
        Me._NullableTextTextBox.Name = "_NullableTextTextBox"
        Me._NullableTextTextBox.Size = New System.Drawing.Size(100, 20)
        Me._NullableTextTextBox.TabIndex = 14
        '
        '_NilifyButton
        '
        Me._NilifyButton.Location = New System.Drawing.Point(223, 16)
        Me._NilifyButton.Name = "_NilifyButton"
        Me._NilifyButton.Size = New System.Drawing.Size(75, 23)
        Me._NilifyButton.TabIndex = 1
        Me._NilifyButton.Text = "Nilify"
        Me._NilifyButton.UseVisualStyleBackColor = True
        '
        '_NullableValueTextBox
        '
        Me._NullableValueTextBox.Location = New System.Drawing.Point(302, 18)
        Me._NullableValueTextBox.Name = "_NullableValueTextBox"
        Me._NullableValueTextBox.Size = New System.Drawing.Size(57, 20)
        Me._NullableValueTextBox.TabIndex = 14
        '
        '_Timer
        '
        '
        'NullableNumericUpDownForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(371, 261)
        Me.Controls.Add(Me._NilifyButton)
        Me.Controls.Add(Me._NullableValueTextBox)
        Me.Controls.Add(Me._NullableTextTextBox)
        Me.Controls.Add(Me._ValidatedValueTextBox)
        Me.Controls.Add(Me._NullableTextTextBoxLabel)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me._ChangedNullableValueTextBox)
        Me.Controls.Add(Me._ChangedValueTextBox)
        Me.Controls.Add(Me._ChangedValueTextBoxLabel)
        Me.Controls.Add(Me._ValidatedNullableValueTextBox)
        Me.Controls.Add(Me._SpinningNullableValueTextBox)
        Me.Controls.Add(Me._SpinningValueTextBox)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me._ValueTextBoxLabel)
        Me.Controls.Add(Me._NullableNumericUpDown)
        Me.Name = "NullableNumericUpDownForm"
        Me.Text = "Nullable Numeric Up Down Form"
        CType(Me._NullableNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _NullableNumericUpDown As isr.Core.Controls.NullableNumericUpDown
    Private WithEvents _ValueTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _SpinningValueTextBox As System.Windows.Forms.TextBox
    Private WithEvents _ChangedValueTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _ChangedValueTextBox As System.Windows.Forms.TextBox
    Private WithEvents Label2 As System.Windows.Forms.Label
    Private WithEvents _ValidatedValueTextBox As System.Windows.Forms.TextBox
    Private WithEvents _SpinningNullableValueTextBox As System.Windows.Forms.TextBox
    Private WithEvents _ValidatedNullableValueTextBox As System.Windows.Forms.TextBox
    Private WithEvents _ChangedNullableValueTextBox As System.Windows.Forms.TextBox
    Private WithEvents Label1 As System.Windows.Forms.Label
    Private WithEvents Label3 As System.Windows.Forms.Label
    Private WithEvents _NullableTextTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _NullableTextTextBox As System.Windows.Forms.TextBox
    Private WithEvents _NilifyButton As System.Windows.Forms.Button
    Private WithEvents _NullableValueTextBox As System.Windows.Forms.TextBox
    Private WithEvents _Timer As System.Windows.Forms.Timer
End Class
