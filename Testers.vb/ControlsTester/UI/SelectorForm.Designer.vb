<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SelectorForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SelectorForm))
        Me._ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._SelectedTextBox = New System.Windows.Forms.TextBox()
        Me._ReadOnlyCheckBox = New System.Windows.Forms.CheckBox()
        Me._EnterValueNumericLabel = New System.Windows.Forms.Label()
        Me._EditedEngineeringUpDownLabel = New System.Windows.Forms.Label()
        Me._ScaledValueTextBoxLabel = New System.Windows.Forms.Label()
        Me._ApplyButton = New System.Windows.Forms.Button()
        Me._EnterValueLabel = New System.Windows.Forms.ToolStripTextBox()
        Me._SelectedValueLabel = New System.Windows.Forms.ToolStripTextBox()
        Me._SelectorComboBox = New isr.Core.Controls.SelectorComboBox()
        Me._SelectButton = New System.Windows.Forms.Button()
        Me._EnterNumeric = New isr.Core.Controls.EngineeringUpDown()
        Me._SelectorNumeric = New isr.Core.Controls.SelectorNumeric()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me._ToolStripSelectorComboBox = New isr.Core.Controls.ToolStripSelectorComboBox()
        Me._ToolStripSelectorNumeric = New isr.Core.Controls.ToolStripSelectorNumeric()
        CType(Me._EnterNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        '_SelectedTextBox
        '
        Me._SelectedTextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SelectedTextBox.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me._SelectedTextBox.Location = New System.Drawing.Point(147, 122)
        Me._SelectedTextBox.Name = "_SelectedTextBox"
        Me._SelectedTextBox.Size = New System.Drawing.Size(121, 25)
        Me._SelectedTextBox.TabIndex = 4
        Me._SelectedTextBox.Text = "ABC"
        Me._ToolTip.SetToolTip(Me._SelectedTextBox, "Scaled value")
        '
        '_ReadOnlyCheckBox
        '
        Me._ReadOnlyCheckBox.AutoSize = True
        Me._ReadOnlyCheckBox.Location = New System.Drawing.Point(16, 171)
        Me._ReadOnlyCheckBox.Name = "_ReadOnlyCheckBox"
        Me._ReadOnlyCheckBox.Size = New System.Drawing.Size(87, 21)
        Me._ReadOnlyCheckBox.TabIndex = 8
        Me._ReadOnlyCheckBox.Text = "Read Only"
        Me._ReadOnlyCheckBox.UseVisualStyleBackColor = True
        '
        '_EnterValueNumericLabel
        '
        Me._EnterValueNumericLabel.AutoSize = True
        Me._EnterValueNumericLabel.Location = New System.Drawing.Point(10, 65)
        Me._EnterValueNumericLabel.Name = "_EnterValueNumericLabel"
        Me._EnterValueNumericLabel.Size = New System.Drawing.Size(112, 17)
        Me._EnterValueNumericLabel.TabIndex = 1
        Me._EnterValueNumericLabel.Text = "Enter value to set:"
        Me._EnterValueNumericLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        '_EditedEngineeringUpDownLabel
        '
        Me._EditedEngineeringUpDownLabel.AutoSize = True
        Me._EditedEngineeringUpDownLabel.Location = New System.Drawing.Point(13, 102)
        Me._EditedEngineeringUpDownLabel.Name = "_EditedEngineeringUpDownLabel"
        Me._EditedEngineeringUpDownLabel.Size = New System.Drawing.Size(69, 17)
        Me._EditedEngineeringUpDownLabel.TabIndex = 1
        Me._EditedEngineeringUpDownLabel.Text = "Edit Value:"
        Me._EditedEngineeringUpDownLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        '_ScaledValueTextBoxLabel
        '
        Me._ScaledValueTextBoxLabel.AutoSize = True
        Me._ScaledValueTextBoxLabel.Location = New System.Drawing.Point(145, 102)
        Me._ScaledValueTextBoxLabel.Name = "_ScaledValueTextBoxLabel"
        Me._ScaledValueTextBoxLabel.Size = New System.Drawing.Size(104, 17)
        Me._ScaledValueTextBoxLabel.TabIndex = 1
        Me._ScaledValueTextBoxLabel.Text = "Observed Value:"
        Me._ScaledValueTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        '_ApplyButton
        '
        Me._ApplyButton.Location = New System.Drawing.Point(146, 163)
        Me._ApplyButton.Name = "_ApplyButton"
        Me._ApplyButton.Size = New System.Drawing.Size(122, 35)
        Me._ApplyButton.TabIndex = 10
        Me._ApplyButton.Text = "Apply Edit Value"
        Me._ApplyButton.UseVisualStyleBackColor = True
        '
        '_EnterValueLabel
        '
        Me._EnterValueLabel.Name = "_EnterValueLabel"
        Me._EnterValueLabel.Size = New System.Drawing.Size(47, 28)
        Me._EnterValueLabel.Text = "Entered"
        '
        '_SelectedValueLabel
        '
        Me._SelectedValueLabel.Name = "_SelectedValueLabel"
        Me._SelectedValueLabel.Size = New System.Drawing.Size(51, 28)
        Me._SelectedValueLabel.Text = "Selected"
        '
        '_SelectorComboBox
        '
        Me._SelectorComboBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._SelectorComboBox.DirtyBackColor = System.Drawing.Color.Orange
        Me._SelectorComboBox.DirtyForeColor = System.Drawing.SystemColors.ActiveCaption
        Me._SelectorComboBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SelectorComboBox.Location = New System.Drawing.Point(311, 122)
        Me._SelectorComboBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._SelectorComboBox.Name = "_SelectorComboBox"
        Me._SelectorComboBox.SelectedText = Nothing
        Me._SelectorComboBox.SelectorIcon = CType(resources.GetObject("_SelectorComboBox.SelectorIcon"), System.Drawing.Image)
        Me._SelectorComboBox.Size = New System.Drawing.Size(99, 25)
        Me._SelectorComboBox.TabIndex = 11
        '
        '_SelectButton
        '
        Me._SelectButton.Location = New System.Drawing.Point(311, 162)
        Me._SelectButton.Name = "_SelectButton"
        Me._SelectButton.Size = New System.Drawing.Size(75, 36)
        Me._SelectButton.TabIndex = 12
        Me._SelectButton.Text = "Select"
        Me._SelectButton.UseVisualStyleBackColor = True
        '
        '_EnterNumeric
        '
        Me._EnterNumeric.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._EnterNumeric.ForeColor = System.Drawing.SystemColors.WindowText
        Me._EnterNumeric.Location = New System.Drawing.Point(16, 122)
        Me._EnterNumeric.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me._EnterNumeric.Minimum = New Decimal(New Integer() {1000, 0, 0, -2147483648})
        Me._EnterNumeric.Name = "_EnterNumeric"
        Me._EnterNumeric.NullValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me._EnterNumeric.ReadOnlyBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me._EnterNumeric.ReadOnlyForeColor = System.Drawing.Color.Black
        Me._EnterNumeric.ReadWriteBackColor = System.Drawing.SystemColors.Window
        Me._EnterNumeric.ReadWriteForeColor = System.Drawing.Color.Black
        Me._EnterNumeric.ScaledValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me._EnterNumeric.Size = New System.Drawing.Size(120, 25)
        Me._EnterNumeric.TabIndex = 6
        Me._EnterNumeric.UnscaledValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me._EnterNumeric.UpDownDisplayMode = isr.Core.Controls.UpDownButtonsDisplayMode.WhenMouseOver
        Me._EnterNumeric.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        '_SelectorNumeric
        '
        Me._SelectorNumeric.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._SelectorNumeric.DirtyBackColor = System.Drawing.Color.Orange
        Me._SelectorNumeric.DirtyForeColor = System.Drawing.SystemColors.ActiveCaption
        Me._SelectorNumeric.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SelectorNumeric.Location = New System.Drawing.Point(442, 122)
        Me._SelectorNumeric.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._SelectorNumeric.Name = "_SelectorNumeric"
        Me._SelectorNumeric.SelectedText = Nothing
        Me._SelectorNumeric.SelectedValue = Nothing
        Me._SelectorNumeric.SelectorIcon = CType(resources.GetObject("_SelectorNumeric.SelectorIcon"), System.Drawing.Image)
        Me._SelectorNumeric.Size = New System.Drawing.Size(76, 25)
        Me._SelectorNumeric.TabIndex = 13
        Me._SelectorNumeric.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ToolStripSelectorComboBox, Me._ToolStripSelectorNumeric})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(622, 28)
        Me.ToolStrip1.TabIndex = 14
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        '_ToolStripSelectorComboBox
        '
        Me._ToolStripSelectorComboBox.AutoSize = False
        Me._ToolStripSelectorComboBox.DirtyBackColor = System.Drawing.Color.Orange
        Me._ToolStripSelectorComboBox.DirtyForeColor = System.Drawing.SystemColors.ActiveCaption
        Me._ToolStripSelectorComboBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ToolStripSelectorComboBox.Name = "_ToolStripSelectorComboBox"
        Me._ToolStripSelectorComboBox.SelectedText = Nothing
        Me._ToolStripSelectorComboBox.SelectorIcon = CType(resources.GetObject("_ToolStripSelectorComboBox.SelectorIcon"), System.Drawing.Image)
        Me._ToolStripSelectorComboBox.Size = New System.Drawing.Size(91, 25)
        '
        '_ToolStripSelectorNumeric
        '
        Me._ToolStripSelectorNumeric.AutoSize = False
        Me._ToolStripSelectorNumeric.DirtyBackColor = System.Drawing.Color.Orange
        Me._ToolStripSelectorNumeric.DirtyForeColor = System.Drawing.SystemColors.ActiveCaption
        Me._ToolStripSelectorNumeric.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ToolStripSelectorNumeric.Name = "_ToolStripSelectorNumeric"
        Me._ToolStripSelectorNumeric.SelectedText = Nothing
        Me._ToolStripSelectorNumeric.SelectorIcon = CType(resources.GetObject("_ToolStripSelectorNumeric.SelectorIcon"), System.Drawing.Image)
        Me._ToolStripSelectorNumeric.Size = New System.Drawing.Size(74, 25)
        Me._ToolStripSelectorNumeric.Text = "0"
        Me._ToolStripSelectorNumeric.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'SelectorForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(622, 263)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me._SelectorNumeric)
        Me.Controls.Add(Me._SelectButton)
        Me.Controls.Add(Me._SelectorComboBox)
        Me.Controls.Add(Me._ApplyButton)
        Me.Controls.Add(Me._ReadOnlyCheckBox)
        Me.Controls.Add(Me._EnterNumeric)
        Me.Controls.Add(Me._SelectedTextBox)
        Me.Controls.Add(Me._ScaledValueTextBoxLabel)
        Me.Controls.Add(Me._EditedEngineeringUpDownLabel)
        Me.Controls.Add(Me._EnterValueNumericLabel)
        Me.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "SelectorForm"
        Me.Text = "Engineering Up Down Form"
        CType(Me._EnterNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _ToolTip As System.Windows.Forms.ToolTip
    Private WithEvents _EnterNumeric As isr.Core.Controls.EngineeringUpDown
    Private WithEvents _SelectedTextBox As System.Windows.Forms.TextBox
    Private WithEvents _ReadOnlyCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _EnterValueNumericLabel As System.Windows.Forms.Label
    Private WithEvents _EditedEngineeringUpDownLabel As System.Windows.Forms.Label
    Private WithEvents _ScaledValueTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _ApplyButton As System.Windows.Forms.Button
    Private WithEvents _EnterValueLabel As System.Windows.Forms.ToolStripTextBox
    Private WithEvents _SelectedValueLabel As System.Windows.Forms.ToolStripTextBox
    Private WithEvents _SelectorComboBox As isr.Core.Controls.SelectorComboBox
    Private WithEvents _SelectButton As System.Windows.Forms.Button
    Private WithEvents _SelectorNumeric As isr.Core.Controls.SelectorNumeric
    Private WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Private WithEvents _ToolStripSelectorComboBox As isr.Core.Controls.ToolStripSelectorComboBox
    Private WithEvents _ToolStripSelectorNumeric As isr.Core.Controls.ToolStripSelectorNumeric
End Class
