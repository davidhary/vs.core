﻿Partial Public Class ConsoleControlForm

    ''' <summary>
    ''' Required designer variable.
    ''' </summary>
    Private components As System.ComponentModel.IContainer = Nothing


#Region "Windows Form Designer generated code"

    ''' <summary>
    ''' Required method for Designer support - do not modify
    ''' the contents of this method with the code editor.
    ''' </summary>
    Private Sub InitializeComponent()
        Me._ClearConsoleButton = New System.Windows.Forms.Button()
        Me._CursorGroupBox = New System.Windows.Forms.GroupBox()
        Me._CursorTypegroupBox = New System.Windows.Forms.GroupBox()
        Me._NoneRadioButton = New System.Windows.Forms.RadioButton()
        Me._BlockRadioButton = New System.Windows.Forms.RadioButton()
        Me._UnderlineRadioButton = New System.Windows.Forms.RadioButton()
        Me._MoveCursorButton = New System.Windows.Forms.Button()
        Me._CursorColumnTextBox = New System.Windows.Forms.TextBox()
        Me._CursorRowTextBox = New System.Windows.Forms.TextBox()
        Me._CursorColumnTextBoxLabel = New System.Windows.Forms.Label()
        Me._CursorRowTextBoxLabel = New System.Windows.Forms.Label()
        Me._MessageTextBoxLabel = New System.Windows.Forms.Label()
        Me._MessageTextBox = New System.Windows.Forms.TextBox()
        Me._WriteMessageButton = New System.Windows.Forms.Button()
        Me._BackgroundColorPanelLabel = New System.Windows.Forms.Label()
        Me._BackgroundColorPanel = New System.Windows.Forms.Panel()
        Me._ForegroundColorPanel = New System.Windows.Forms.Panel()
        Me._ForegroundColorPanelLabel = New System.Windows.Forms.Label()
        Me._ColorDialog = New System.Windows.Forms.ColorDialog()
        Me._RightConsoleControl = New isr.Core.Controls.ConsoleControl()
        Me._LeftconsoleControl = New isr.Core.Controls.ConsoleControl()
        Me._CursorGroupBox.SuspendLayout()
        Me._CursorTypegroupBox.SuspendLayout()
        Me.SuspendLayout()
        ' 
        ' _ClearConsoleButton
        ' 
        Me._ClearConsoleButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me._ClearConsoleButton.Location = New System.Drawing.Point(11, 388)
        Me._ClearConsoleButton.Name = "_ClearConsoleButton"
        Me._ClearConsoleButton.Size = New System.Drawing.Size(625, 31)
        Me._ClearConsoleButton.TabIndex = 1
        Me._ClearConsoleButton.Text = "Clear Console Screen"
        Me._ClearConsoleButton.UseVisualStyleBackColor = True
        ' 
        ' groupBox1
        ' 
        Me._CursorGroupBox.Controls.Add(Me._CursorTypegroupBox)
        Me._CursorGroupBox.Controls.Add(Me._MoveCursorButton)
        Me._CursorGroupBox.Controls.Add(Me._CursorColumnTextBox)
        Me._CursorGroupBox.Controls.Add(Me._CursorRowTextBox)
        Me._CursorGroupBox.Controls.Add(Me._CursorColumnTextBoxLabel)
        Me._CursorGroupBox.Controls.Add(Me._CursorRowTextBoxLabel)
        Me._CursorGroupBox.Location = New System.Drawing.Point(12, 425)
        Me._CursorGroupBox.Name = "_CursorGroupBox"
        Me._CursorGroupBox.Size = New System.Drawing.Size(226, 157)
        Me._CursorGroupBox.TabIndex = 2
        Me._CursorGroupBox.TabStop = False
        Me._CursorGroupBox.Text = "Cursor"
        ' 
        ' groupBox2
        ' 
        Me._CursorTypegroupBox.Controls.Add(Me._NoneRadioButton)
        Me._CursorTypegroupBox.Controls.Add(Me._BlockRadioButton)
        Me._CursorTypegroupBox.Controls.Add(Me._UnderlineRadioButton)
        Me._CursorTypegroupBox.Location = New System.Drawing.Point(13, 95)
        Me._CursorTypegroupBox.Name = "_CursorTypegroupBox"
        Me._CursorTypegroupBox.Size = New System.Drawing.Size(197, 50)
        Me._CursorTypegroupBox.TabIndex = 5
        Me._CursorTypegroupBox.TabStop = False
        Me._CursorTypegroupBox.Text = "Cursor Type"
        ' 
        ' _NoneRadioButton
        ' 
        Me._NoneRadioButton.AutoSize = True
        Me._NoneRadioButton.Location = New System.Drawing.Point(138, 21)
        Me._NoneRadioButton.Name = "_NoneRadioButton"
        Me._NoneRadioButton.Size = New System.Drawing.Size(51, 17)
        Me._NoneRadioButton.TabIndex = 2
        Me._NoneRadioButton.Text = "None"
        Me._NoneRadioButton.UseVisualStyleBackColor = True
        ' 
        ' rbBlock
        ' 
        Me._BlockRadioButton.AutoSize = True
        Me._BlockRadioButton.Location = New System.Drawing.Point(84, 21)
        Me._BlockRadioButton.Name = "_BlockRadioButton"
        Me._BlockRadioButton.Size = New System.Drawing.Size(52, 17)
        Me._BlockRadioButton.TabIndex = 1
        Me._BlockRadioButton.Text = "Block"
        Me._BlockRadioButton.UseVisualStyleBackColor = True
        ' 
        ' _UnderlineRadioButton
        ' 
        Me._UnderlineRadioButton.AutoSize = True
        Me._UnderlineRadioButton.Checked = True
        Me._UnderlineRadioButton.Location = New System.Drawing.Point(12, 20)
        Me._UnderlineRadioButton.Name = "_UnderlineRadioButton"
        Me._UnderlineRadioButton.Size = New System.Drawing.Size(70, 17)
        Me._UnderlineRadioButton.TabIndex = 0
        Me._UnderlineRadioButton.TabStop = True
        Me._UnderlineRadioButton.Text = "Underline"
        Me._UnderlineRadioButton.UseVisualStyleBackColor = True
        ' 
        ' _MoveCursorButton
        ' 
        Me._MoveCursorButton.Location = New System.Drawing.Point(13, 66)
        Me._MoveCursorButton.Name = "_MoveCursorButton"
        Me._MoveCursorButton.Size = New System.Drawing.Size(103, 23)
        Me._MoveCursorButton.TabIndex = 4
        Me._MoveCursorButton.Text = "Move Cursor"
        Me._MoveCursorButton.UseVisualStyleBackColor = True
        ' 
        ' _CursorColumnTextBox
        ' 
        Me._CursorColumnTextBox.Location = New System.Drawing.Point(85, 43)
        Me._CursorColumnTextBox.Name = "_CursorColumnTextBox"
        Me._CursorColumnTextBox.Size = New System.Drawing.Size(31, 20)
        Me._CursorColumnTextBox.TabIndex = 3
        ' 
        ' _CursorRowTextBox
        ' 
        Me._CursorRowTextBox.Location = New System.Drawing.Point(85, 21)
        Me._CursorRowTextBox.Name = "_CursorRowTextBox"
        Me._CursorRowTextBox.Size = New System.Drawing.Size(31, 20)
        Me._CursorRowTextBox.TabIndex = 2
        ' 
        ' _CursorColumnTextBoxLabel
        ' 
        Me._CursorColumnTextBoxLabel.AutoSize = True
        Me._CursorColumnTextBoxLabel.Location = New System.Drawing.Point(10, 46)
        Me._CursorColumnTextBoxLabel.Name = "_CursorColumnTextBoxLabel"
        Me._CursorColumnTextBoxLabel.Size = New System.Drawing.Size(75, 13)
        Me._CursorColumnTextBoxLabel.TabIndex = 1
        Me._CursorColumnTextBoxLabel.Text = "Column (0-79):"
        ' 
        ' _CursorRowTextBoxLabel
        ' 
        Me._CursorRowTextBoxLabel.AutoSize = True
        Me._CursorRowTextBoxLabel.Location = New System.Drawing.Point(22, 24)
        Me._CursorRowTextBoxLabel.Name = "_CursorRowTextBoxLabel"
        Me._CursorRowTextBoxLabel.Size = New System.Drawing.Size(62, 13)
        Me._CursorRowTextBoxLabel.TabIndex = 0
        Me._CursorRowTextBoxLabel.Text = "Row (0-24):"
        ' 
        ' _MessageTextBoxLabel
        ' 
        Me._MessageTextBoxLabel.AutoSize = True
        Me._MessageTextBoxLabel.Location = New System.Drawing.Point(293, 480)
        Me._MessageTextBoxLabel.Name = "_MessageTextBoxLabel"
        Me._MessageTextBoxLabel.Size = New System.Drawing.Size(31, 13)
        Me._MessageTextBoxLabel.TabIndex = 3
        Me._MessageTextBoxLabel.Text = "Text:"
        ' 
        ' _MessageTextBox
        ' 
        Me._MessageTextBox.Location = New System.Drawing.Point(330, 478)
        Me._MessageTextBox.Name = "_MessageTextBox"
        Me._MessageTextBox.Size = New System.Drawing.Size(306, 20)
        Me._MessageTextBox.TabIndex = 4
        ' 
        ' _WriteMessageButton
        ' 
        Me._WriteMessageButton.Location = New System.Drawing.Point(329, 501)
        Me._WriteMessageButton.Name = "_WriteMessageButton"
        Me._WriteMessageButton.Size = New System.Drawing.Size(306, 43)
        Me._WriteMessageButton.TabIndex = 5
        Me._WriteMessageButton.Text = "Write Text to Console Screen at Current Cursor Position Using The Default Colors"
        Me._WriteMessageButton.UseVisualStyleBackColor = True
        ' 
        ' _BackgroundColorPanelLabel
        ' 
        Me._BackgroundColorPanelLabel.AutoSize = True
        Me._BackgroundColorPanelLabel.Location = New System.Drawing.Point(240, 431)
        Me._BackgroundColorPanelLabel.Name = "_BackgroundColorPanelLabel"
        Me._BackgroundColorPanelLabel.Size = New System.Drawing.Size(95, 13)
        Me._BackgroundColorPanelLabel.TabIndex = 6
        Me._BackgroundColorPanelLabel.Text = "Background Color:"
        ' 
        ' _BackgroundColorPanel
        ' 
        Me._BackgroundColorPanel.Location = New System.Drawing.Point(335, 430)
        Me._BackgroundColorPanel.Name = "_BackgroundColorPanel"
        Me._BackgroundColorPanel.Size = New System.Drawing.Size(40, 20)
        Me._BackgroundColorPanel.TabIndex = 7
        ' 
        ' _ForegroundColorPanel
        ' 
        Me._ForegroundColorPanel.Location = New System.Drawing.Point(335, 453)
        Me._ForegroundColorPanel.Name = "_ForegroundColorPanel"
        Me._ForegroundColorPanel.Size = New System.Drawing.Size(40, 20)
        Me._ForegroundColorPanel.TabIndex = 9
        ' 
        ' _ForegroundColorPanelLabel
        ' 
        Me._ForegroundColorPanelLabel.AutoSize = True
        Me._ForegroundColorPanelLabel.Location = New System.Drawing.Point(245, 454)
        Me._ForegroundColorPanelLabel.Name = "_ForegroundColorPanelLabel"
        Me._ForegroundColorPanelLabel.Size = New System.Drawing.Size(91, 13)
        Me._ForegroundColorPanelLabel.TabIndex = 8
        Me._ForegroundColorPanelLabel.Text = "Foreground Color:"
        ' 
        ' _ColorDialog
        ' 
        Me._ColorDialog.AnyColor = True
        Me._ColorDialog.FullOpen = True
        ' 
        ' _RightConsoleControl
        ' 
        Me._RightConsoleControl.AllowInput = True
        Me._RightConsoleControl.BackColor = System.Drawing.Color.Black
        Me._RightConsoleControl.ConsoleBackgroundColor = System.Drawing.Color.Black
        Me._RightConsoleControl.ConsoleForegroundColor = System.Drawing.Color.LightGray
        Me._RightConsoleControl.CurrentBackgroundColor = System.Drawing.Color.Black
        Me._RightConsoleControl.CurrentForegroundColor = System.Drawing.Color.LightGray
        Me._RightConsoleControl.CursorType = isr.Core.Controls.CursorType.Underline
        Me._RightConsoleControl.EchoInput = True
        Me._RightConsoleControl.ForeColor = System.Drawing.Color.LightGray
        Me._RightConsoleControl.Location = New System.Drawing.Point(678, 6)
        Me._RightConsoleControl.Name = "_RightConsoleControl"
        Me._RightConsoleControl.ShowCursor = True
        Me._RightConsoleControl.Size = New System.Drawing.Size(646, 377)
        Me._RightConsoleControl.TabIndex = 10
        ' 
        ' _LeftconsoleControl
        ' 
        Me._LeftconsoleControl.AllowInput = True
        Me._LeftconsoleControl.BackColor = System.Drawing.Color.Black
        Me._LeftconsoleControl.ConsoleBackgroundColor = System.Drawing.Color.Black
        Me._LeftconsoleControl.ConsoleForegroundColor = System.Drawing.Color.LightGray
        Me._LeftconsoleControl.CurrentBackgroundColor = System.Drawing.Color.Black
        Me._LeftconsoleControl.CurrentForegroundColor = System.Drawing.Color.LightGray
        Me._LeftconsoleControl.CursorType = isr.Core.Controls.CursorType.Underline
        Me._LeftconsoleControl.EchoInput = True
        Me._LeftconsoleControl.ForeColor = System.Drawing.Color.LightGray
        Me._LeftconsoleControl.Location = New System.Drawing.Point(6, 6)
        Me._LeftconsoleControl.Name = "_LeftconsoleControl"
        Me._LeftconsoleControl.ShowCursor = True
        Me._LeftconsoleControl.Size = New System.Drawing.Size(646, 377)
        Me._LeftconsoleControl.TabIndex = 0
        ' 
        ' Form1
        ' 
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0F, 13.0F)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1334, 587)
        Me.Controls.Add(Me._RightConsoleControl)
        Me.Controls.Add(Me._ForegroundColorPanel)
        Me.Controls.Add(Me._ForegroundColorPanelLabel)
        Me.Controls.Add(Me._BackgroundColorPanel)
        Me.Controls.Add(Me._BackgroundColorPanelLabel)
        Me.Controls.Add(Me._WriteMessageButton)
        Me.Controls.Add(Me._MessageTextBox)
        Me.Controls.Add(Me._MessageTextBoxLabel)
        Me.Controls.Add(Me._CursorGroupBox)
        Me.Controls.Add(Me._ClearConsoleButton)
        Me.Controls.Add(Me._LeftconsoleControl)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "Form1"
        Me.Text = "Console Control Demo"
        Me._CursorGroupBox.ResumeLayout(False)
        Me._CursorGroupBox.PerformLayout()
        Me._CursorTypegroupBox.ResumeLayout(False)
        Me._CursorTypegroupBox.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private WithEvents _LeftConsoleControl As isr.Core.Controls.ConsoleControl
    Private WithEvents _ClearConsoleButton As System.Windows.Forms.Button
    Private _CursorGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _MoveCursorButton As System.Windows.Forms.Button
    Private _CursorColumnTextBox As System.Windows.Forms.TextBox
    Private _CursorRowTextBox As System.Windows.Forms.TextBox
    Private _CursorColumnTextBoxLabel As System.Windows.Forms.Label
    Private _CursorRowTextBoxLabel As System.Windows.Forms.Label
    Private _CursorTypegroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _NoneRadioButton As System.Windows.Forms.RadioButton
    Private WithEvents _BlockRadioButton As System.Windows.Forms.RadioButton
    Private WithEvents _UnderlineRadioButton As System.Windows.Forms.RadioButton
    Private _MessageTextBoxLabel As System.Windows.Forms.Label
    Private _MessageTextBox As System.Windows.Forms.TextBox
    Private WithEvents _WriteMessageButton As System.Windows.Forms.Button
    Private _BackgroundColorPanelLabel As System.Windows.Forms.Label
    Private WithEvents _BackgroundColorPanel As System.Windows.Forms.Panel
    Private WithEvents _ForegroundColorPanel As System.Windows.Forms.Panel
    Private _ForegroundColorPanelLabel As System.Windows.Forms.Label
    Private _ColorDialog As System.Windows.Forms.ColorDialog
    Private WithEvents _RightConsoleControl As isr.Core.Controls.ConsoleControl
End Class
