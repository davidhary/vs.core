Namespace ExceptionExtensions

    ''' <summary> Adds exception data for building the exception full blown report. </summary>
    ''' <remarks> (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para> </remarks>
    Public Module Methods

        ''' <summary> Adds exception data from the specified exception. </summary>
        ''' <remarks> David, 2020-10-25. </remarks>
        ''' <param name="exception"> The exception. </param>
        ''' <returns> <c>true</c> if exception was added; otherwise <c>false</c> </returns>
        <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
        Public Function AddExceptionData(ByVal exception As System.Exception) As Boolean
            ' presently this project add no project-specific extensions.
            Return False
        End Function

        ''' <summary> Adds an exception data. </summary>
        ''' <remarks> David, 2020-10-25. </remarks>
        ''' <param name="exception"> The exception. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        Private Function AddExceptionDataThis(ByVal exception As System.Exception) As Boolean
            Return Methods.AddExceptionData(exception) OrElse
                   isr.Core.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData(exception)
        End Function

        ''' <summary> Converts a value to a full blown string. </summary>
        ''' <remarks> David, 2020-10-25. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <returns> Value as a String. </returns>
        <System.Runtime.CompilerServices.Extension>
        Friend Function ToFullBlownString(ByVal value As System.Exception) As String
            Return Methods.ToFullBlownString(value, Integer.MaxValue)
        End Function

        ''' <summary> Converts this object to a full blown string. </summary>
        ''' <remarks> David, 2020-10-25. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <param name="level"> The level. </param>
        ''' <returns> The given data converted to a String. </returns>
        <System.Runtime.CompilerServices.Extension>
        Friend Function ToFullBlownString(ByVal value As System.Exception, ByVal level As Integer) As String
            Return isr.Core.ExceptionExtensions.ExceptionExtensionMethods.ToFullBlownString(value, level, AddressOf AddExceptionDataThis)
        End Function

    End Module

End Namespace

