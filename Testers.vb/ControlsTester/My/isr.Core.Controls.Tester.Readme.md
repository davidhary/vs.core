## ISR Core Controls Tester<sub>&trade;</sub>
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Revision History [](#){name=Revision-History}

*6.0.7604 2020-10-25*  
Add VIsual Styles viewer. Document for C# conversion.

*3.0.6033 2016-07-08*  
Adds the CP Wizard controls.

*2.2.5207 2014-04-05*  
Created

\(c\) 2009 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:


Core Controls Library:\
Safe events: ([Code Project](http://www.CodeProject.com/KB/cs/EventSafeTrigger.aspx)
[dream in code](http://www.DreamInCode.net/forums/user/334492-aeonhack/)
[dream in code](http://www.DreamInCode.net/code/snippet5016.htm))  
[Flashing Led](https://www.codeproject.com/Articles/8393/Flash-LED-Control)  
[Extended Numeric Up Down Control](http://www.CodeProject.com/KB/edit/NumericUpDownEx.aspx)  
[Cristi Potlog Controls Wizard](http://www.codeproject.com/Articles/10808/Cristi-Potlog-s-Wizard-Control-for-NET)  
[Cool Print Preview Dialog](http://www.codeproject.com/Articles/38758/An-Enhanced-PrintPreviewDialog)  
[Image Button](http://www.codeproject.com/Articles/29010/WinForm-ImageButton)  
[Metro Style Progress Bar](http://www.codeproject.com/Articles/804316/Office-Style-Splash-Screen)  
[Toggle Switch Control](http://www.codeproject.com/Articles/1029499/ToggleSwitch-Winforms-Control)

Core Agnostic Library:\
Safe events: ([Code Project](http://www.CodeProject.com/KB/cs/EventSafeTrigger.aspx)
[dream in code](http://www.DreamInCode.net/forums/user/334492-aeonhack/)
[dream in code](http://www.DreamInCode.net/code/snippet5016.htm))  
[Drop Shadow and Fade Form](http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx)  
[Engineering Format](http://WallaceKelly.BlogSpot.com/)  
[Efficient Strong Enumerations](http://www.CodeProject.com/KB/cs/efficient_strong_enum.aspx)  
[Enumeration Extensions](http://www.CodeProject.com/Articles/37921/Enums-Flags-and-CSharp-Oh-my-bad-pun.aspx)  
[String Enumerator](http://www.codeproject.com/Articles/17472/StringEnumerator)
