<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ResizeTester
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me._LabelInsidePanel = New System.Windows.Forms.Panel()
        Me._InsidePaneLabel = New System.Windows.Forms.Label()
        Me._LabelInsidePanelLabel = New System.Windows.Forms.Label()
        Me._PanelLayoutPanel = New System.Windows.Forms.TableLayoutPanel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.LabelPanelUserControl2 = New LabelPanelUserControl()
        Me.LabelPanelUserControl1 = New LabelPanelUserControl()
        Me.ControlInsideUserControl1 = New ControlInsideUserControl()
        Me._LabelInsidePanel.SuspendLayout()
        Me._PanelLayoutPanel.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        '_LabelInsidePanel
        '
        Me._LabelInsidePanel.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._LabelInsidePanel.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me._LabelInsidePanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._LabelInsidePanel.Controls.Add(Me._InsidePaneLabel)
        Me._LabelInsidePanel.Location = New System.Drawing.Point(11, 33)
        Me._LabelInsidePanel.Name = "_LabelInsidePanel"
        Me._LabelInsidePanel.Padding = New System.Windows.Forms.Padding(3)
        Me._LabelInsidePanel.Size = New System.Drawing.Size(202, 38)
        Me._LabelInsidePanel.TabIndex = 0
        '
        '_InsidePaneLabel
        '
        Me._InsidePaneLabel.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me._InsidePaneLabel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._InsidePaneLabel.Location = New System.Drawing.Point(3, 3)
        Me._InsidePaneLabel.Margin = New System.Windows.Forms.Padding(0)
        Me._InsidePaneLabel.Name = "_InsidePaneLabel"
        Me._InsidePaneLabel.Size = New System.Drawing.Size(194, 30)
        Me._InsidePaneLabel.TabIndex = 0
        Me._InsidePaneLabel.Text = "This label resizes inside the panel"
        Me._InsidePaneLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        '_LabelInsidePanelLabel
        '
        Me._LabelInsidePanelLabel.AutoSize = True
        Me._LabelInsidePanelLabel.Location = New System.Drawing.Point(4, 17)
        Me._LabelInsidePanelLabel.Name = "_LabelInsidePanelLabel"
        Me._LabelInsidePanelLabel.Size = New System.Drawing.Size(101, 13)
        Me._LabelInsidePanelLabel.TabIndex = 1
        Me._LabelInsidePanelLabel.Text = "Label inside a panel"
        '
        '_PanelLayoutPanel
        '
        Me._PanelLayoutPanel.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._PanelLayoutPanel.ColumnCount = 3
        Me._PanelLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me._PanelLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._PanelLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me._PanelLayoutPanel.Controls.Add(Me.Panel1, 1, 1)
        Me._PanelLayoutPanel.Location = New System.Drawing.Point(13, 135)
        Me._PanelLayoutPanel.Name = "_PanelLayoutPanel"
        Me._PanelLayoutPanel.RowCount = 3
        Me._PanelLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me._PanelLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._PanelLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me._PanelLayoutPanel.Size = New System.Drawing.Size(232, 82)
        Me._PanelLayoutPanel.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(5, 119)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(171, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Label inside a panel inside a layout"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(23, 23)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Padding = New System.Windows.Forms.Padding(3)
        Me.Panel1.Size = New System.Drawing.Size(186, 36)
        Me.Panel1.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label2.Location = New System.Drawing.Point(3, 3)
        Me.Label2.Margin = New System.Windows.Forms.Padding(0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(178, 28)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "label inside a panel inside a layout"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 3.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 3.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.LabelPanelUserControl2, 1, 1)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(13, 369)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 3.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 3.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(200, 59)
        Me.TableLayoutPanel1.TabIndex = 4
        '
        'LabelPanelUserControl2
        '
        Me.LabelPanelUserControl2.BackColor = System.Drawing.SystemColors.Info
        Me.LabelPanelUserControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LabelPanelUserControl2.Location = New System.Drawing.Point(6, 6)
        Me.LabelPanelUserControl2.Name = "LabelPanelUserControl2"
        Me.LabelPanelUserControl2.Padding = New System.Windows.Forms.Padding(4)
        Me.LabelPanelUserControl2.Size = New System.Drawing.Size(188, 47)
        Me.LabelPanelUserControl2.TabIndex = 3
        '
        'LabelPanelUserControl1
        '
        Me.LabelPanelUserControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelPanelUserControl1.BackColor = System.Drawing.SystemColors.Info
        Me.LabelPanelUserControl1.Location = New System.Drawing.Point(17, 255)
        Me.LabelPanelUserControl1.Name = "LabelPanelUserControl1"
        Me.LabelPanelUserControl1.Padding = New System.Windows.Forms.Padding(4)
        Me.LabelPanelUserControl1.Size = New System.Drawing.Size(227, 53)
        Me.LabelPanelUserControl1.TabIndex = 3
        '
        'ControlInsideUserControl1
        '
        Me.ControlInsideUserControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ControlInsideUserControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ControlInsideUserControl1.Location = New System.Drawing.Point(14, 472)
        Me.ControlInsideUserControl1.Name = "ControlInsideUserControl1"
        Me.ControlInsideUserControl1.Padding = New System.Windows.Forms.Padding(5)
        Me.ControlInsideUserControl1.Size = New System.Drawing.Size(199, 88)
        Me.ControlInsideUserControl1.TabIndex = 5
        '
        'ResizeTester
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(280, 574)
        Me.Controls.Add(Me.ControlInsideUserControl1)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.LabelPanelUserControl1)
        Me.Controls.Add(Me._PanelLayoutPanel)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me._LabelInsidePanelLabel)
        Me.Controls.Add(Me._LabelInsidePanel)
        Me.Name = "ResizeTester"
        Me.Text = "ResizeTester"
        Me._LabelInsidePanel.ResumeLayout(False)
        Me._PanelLayoutPanel.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Private WithEvents _LabelInsidePanel As Panel
    Private WithEvents _InsidePaneLabel As Label
    Private WithEvents _LabelInsidePanelLabel As Label
    Private WithEvents _PanelLayoutPanel As TableLayoutPanel
    Private WithEvents Panel1 As Panel
    Private WithEvents Label2 As Label
    Private WithEvents Label1 As Label
    Private WithEvents LabelPanelUserControl1 As LabelPanelUserControl
    Private WithEvents TableLayoutPanel1 As TableLayoutPanel
    Private WithEvents LabelPanelUserControl2 As LabelPanelUserControl
    Private WithEvents ControlInsideUserControl1 As ControlInsideUserControl
End Class
