﻿''' <summary> A control inside user control. </summary>
''' <remarks> David, 2020-10-25. </remarks>
Public Class ControlInsideUserControl


    Public Sub Render()
        Me.BackColor = Color.Red
        Me.LabelPanelUserControl2.Render()
    End Sub
End Class
