﻿''' <summary> A resize tester. </summary>
''' <remarks> David, 2020-10-25. </remarks>
Public Class ResizeTester

    ''' <summary> Resize tester double click. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ResizeTester_DoubleClick(sender As Object, e As EventArgs) Handles Me.DoubleClick
        Me.ControlInsideUserControl1.Render()
    End Sub
End Class