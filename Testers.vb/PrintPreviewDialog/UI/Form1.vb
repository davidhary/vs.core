''' <summary> Form 1. </summary>
''' <remarks> (c) 2009 Bernardo Castilho. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2009-08-05" by="Bernardo Castilho" revision=""> 
''' http://www.codeproject.com/Articles/38758/An-Enhanced-PrintPreviewDialog. </para></remarks>
Public Class Form1

    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId:="ShowDialog")> <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId:="PrintDocument")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId:="CoolPrintPreviewDialog")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId:="PrintPreviewDialog")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId:="ShowDialog")>
    Public Sub New()

        ' This call is required by the designer.
        Me.InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me._CoolCodeLabel.Text =
"Using dialog As CoolPrintPreviewDialog = New CoolPrintPreviewDialog
    dialog.Document = Me._PrintDocument
    dialog.ShowDialog(Me)
End Using"
        Me._StandardCodeLabel.Text =
"Using dialog As PrintPreviewDialog = New PrintPreviewDialog
    dialog.Document = Me._PrintDocument
    dialog.ShowDialog(Me)
End Using"

    End Sub


    ' fields
    Private ReadOnly _Font As Font = New Font("Segoe UI", 14.0!)
    Private _Page As Integer = 0
    Private _Start As DateTimeOffset

    ''' <summary>
    ''' Event handler. Called by _OpenStandardPreviewDialogButton for click events.
    ''' Show standard print preview.
    ''' </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub OpenStandardPreviewDialogButtonClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _OpenStandardPreviewDialogButton.Click
        Using dialog As PrintPreviewDialog = New PrintPreviewDialog
            dialog.Document = Me._PrintDocument
            dialog.ShowDialog(Me)
        End Using
    End Sub

    ''' <summary>
    ''' Event handler. Called by _OpenCoolPreviewDialogButton for click events.
    ''' Show cool print preview.
    ''' </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub OpenCoolPreviewDialogButtonClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _OpenCoolPreviewDialogButton.Click
        Using dialog As isr.Core.Controls.CoolPrintPreviewDialog = New isr.Core.Controls.CoolPrintPreviewDialog
            dialog.Document = Me._PrintDocument
            dialog.ShowDialog(Me)
        End Using
    End Sub

    ''' <summary> Event handler. Called by _PrintDocument for begin print events. Render document. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Print event information. </param>
    Private Sub PrintDocumentBeginPrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles _PrintDocument.BeginPrint
        Me._Start = DateTimeOffset.Now
        Me._Page = 0
    End Sub

    ''' <summary> Event handler. Called by _PrintDocument for end print events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Print event information. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId:="ms")>
    Private Sub PrintDocumentEndPrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles _PrintDocument.EndPrint
        Console.WriteLine($"Document rendered in {DateTimeOffset.Now.Subtract(Me._Start).TotalMilliseconds} ms")
    End Sub

    ''' <summary> Event handler. Called by _PrintDocument for print page events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Print page event information. </param>
    Private Sub PrintDocumentPrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles _PrintDocument.PrintPage
        Dim rc As Rectangle = e.MarginBounds
        rc.Height = (Me._Font.Height + 10)
        Dim i As Integer = 0
        Do While True
            Dim text As String = $"line {i + 1} on page {Me._Page + 1}"
            e.Graphics.DrawString([text], Me._Font, Brushes.Black, rc)
            rc.Y += rc.Height
            If (rc.Bottom > e.MarginBounds.Bottom) Then
                Me._Page += 1
                e.HasMorePages = If(Me._LongDocumentCheckBox.Checked, Me._Page < 3000, Me._Page < 30)
                Return
            End If
            i += 1
        Loop
    End Sub
End Class
