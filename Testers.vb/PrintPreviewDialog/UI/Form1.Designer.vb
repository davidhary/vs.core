<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                If Me._Font IsNot Nothing Then Me._Font.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId:="PrintPreviewDialog")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId:="CoolPrintPreviewDialog")>
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me._LongDocumentCheckBox = New System.Windows.Forms.CheckBox
        Me._CoolCodeLabel = New System.Windows.Forms.Label
        Me._StandardCodeLabel = New System.Windows.Forms.Label
        Me._OpenCoolPreviewDialogButton = New System.Windows.Forms.Button
        Me._OpenStandardPreviewDialogButton = New System.Windows.Forms.Button
        Me._PrintDocument = New System.Drawing.Printing.PrintDocument
        Me.SuspendLayout()
        '
        '_LongDocumentCheckBox
        '
        Me._LongDocumentCheckBox.AutoSize = True
        Me._LongDocumentCheckBox.Location = New System.Drawing.Point(232, 238)
        Me._LongDocumentCheckBox.Name = "_LongDocumentCheckBox"
        Me._LongDocumentCheckBox.Size = New System.Drawing.Size(162, 21)
        Me._LongDocumentCheckBox.TabIndex = 15
        Me._LongDocumentCheckBox.Text = "Create Long Document"
        Me._LongDocumentCheckBox.UseVisualStyleBackColor = True
        '
        '_CoolCodeLabel
        '
        Me._CoolCodeLabel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._CoolCodeLabel.Font = New System.Drawing.Font("Courier New", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._CoolCodeLabel.Location = New System.Drawing.Point(229, 124)
        Me._CoolCodeLabel.Name = "_CoolCodeLabel"
        Me._CoolCodeLabel.Size = New System.Drawing.Size(388, 102)
        Me._CoolCodeLabel.TabIndex = 13
        '
        '_StandardCodeLabel
        '
        Me._StandardCodeLabel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._StandardCodeLabel.Font = New System.Drawing.Font("Courier New", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._StandardCodeLabel.Location = New System.Drawing.Point(229, 13)
        Me._StandardCodeLabel.Name = "_StandardCodeLabel"
        Me._StandardCodeLabel.Size = New System.Drawing.Size(388, 102)
        Me._StandardCodeLabel.TabIndex = 14
        '
        '_OpenCoolPreviewDialogButton
        '
        Me._OpenCoolPreviewDialogButton.Location = New System.Drawing.Point(13, 124)
        Me._OpenCoolPreviewDialogButton.Margin = New System.Windows.Forms.Padding(4)
        Me._OpenCoolPreviewDialogButton.Name = "_OpenCoolPreviewDialogButton"
        Me._OpenCoolPreviewDialogButton.Size = New System.Drawing.Size(209, 102)
        Me._OpenCoolPreviewDialogButton.TabIndex = 12
        Me._OpenCoolPreviewDialogButton.Text = "Enhanced" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "CoolPrintPreviewDialog"
        Me._OpenCoolPreviewDialogButton.UseVisualStyleBackColor = True
        '
        '_OpenStandardPreviewDialogButton
        '
        Me._OpenStandardPreviewDialogButton.Location = New System.Drawing.Point(13, 13)
        Me._OpenStandardPreviewDialogButton.Margin = New System.Windows.Forms.Padding(4)
        Me._OpenStandardPreviewDialogButton.Name = "_OpenStandardPreviewDialogButton"
        Me._OpenStandardPreviewDialogButton.Size = New System.Drawing.Size(209, 102)
        Me._OpenStandardPreviewDialogButton.TabIndex = 11
        Me._OpenStandardPreviewDialogButton.Text = "Standard" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "PrintPreviewDialog"
        Me._OpenStandardPreviewDialogButton.UseVisualStyleBackColor = True
        '
        'PrintDocument1
        '
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(120.0!, 120.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.ClientSize = New System.Drawing.Size(650, 275)
        Me.Controls.Add(Me._LongDocumentCheckBox)
        Me.Controls.Add(Me._CoolCodeLabel)
        Me.Controls.Add(Me._StandardCodeLabel)
        Me.Controls.Add(Me._OpenCoolPreviewDialogButton)
        Me.Controls.Add(Me._OpenStandardPreviewDialogButton)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Compare Print Preview Dialogs"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _LongDocumentCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _CoolCodeLabel As System.Windows.Forms.Label
    Private WithEvents _StandardCodeLabel As System.Windows.Forms.Label
    Private WithEvents _OpenCoolPreviewDialogButton As System.Windows.Forms.Button
    Private WithEvents _OpenStandardPreviewDialogButton As System.Windows.Forms.Button
    Private WithEvents _PrintDocument As System.Drawing.Printing.PrintDocument

End Class
