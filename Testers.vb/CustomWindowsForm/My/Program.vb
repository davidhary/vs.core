﻿''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
''' <remarks> David, 2021-03-12. </remarks>
Friend NotInheritable Class Program

    Private Sub New()
    End Sub

    ''' <summary> The main entry point for the application. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    <System.STAThread>
    Public Shared Sub Main()
        Application.EnableVisualStyles()
        Application.SetCompatibleTextRenderingDefault(False)
        Application.Run(New MainForm())
    End Sub
End Class

