''' <summary> The application's main window. </summary>
''' <remarks> David, 2021-03-12. </remarks>
Partial Public Class MainForm
    Inherits Form

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.Windows.Forms.Form" /> class.
    ''' </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    Public Sub New()
        Me.InitializeComponent()
    End Sub
    ''' <summary> The offset. </summary>

    Private _Offset As Point
    ''' <summary> True if is top panel dragged, false if not. </summary>
    Private _IsTopPanelDragged As Boolean = False

    ''' <summary> Top panel mouse down. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub TopPanel_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _TopPanel.MouseDown
        If e.Button = MouseButtons.Left Then
            Me._IsTopPanelDragged = True
            Dim pointStartPosition As Point = Me.PointToScreen(New Point(e.X, e.Y))
            Me._Offset = New Point() With {
                .X = Me.Location.X - pointStartPosition.X,
                .Y = Me.Location.Y - pointStartPosition.Y}
        Else
            Me._IsTopPanelDragged = False
        End If
    End Sub

    ''' <summary> Top panel mouse move. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub TopPanel_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _TopPanel.MouseMove
        If Me._IsTopPanelDragged Then
            Dim newPoint As Point = Me._TopPanel.PointToScreen(New Point(e.X, e.Y))
            newPoint.Offset(Me._Offset)
            Me.Location = newPoint
        End If
    End Sub

    ''' <summary> Top panel mouse up. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub TopPanel_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _TopPanel.MouseUp
        Me._IsTopPanelDragged = False
    End Sub

    ''' <summary> Window text label mouse down. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub WindowTextLabel_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _WindowTextLabel.MouseDown
        Me.TopPanel_MouseDown(sender, e)
    End Sub

    ''' <summary> Window text label mouse move. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub WindowTextLabel_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _WindowTextLabel.MouseMove
        Me.TopPanel_MouseMove(sender, e)
    End Sub

    ''' <summary> Window text label mouse up. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub WindowTextLabel_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _WindowTextLabel.MouseUp
        Me.TopPanel_MouseUp(sender, e)
    End Sub

    ''' <summary> Closes button click. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub CloseButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _CloseButton.Click
        Me.Close()
    End Sub

    ''' <summary> Minimum button click. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub MinButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _MinButton.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub

    ''' <summary> Opens blue form shaped button click. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub OpenBlueFormShapedButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _OpenBlueFormShapedButton.Click
        TryCast(New BlueForm(), BlueForm)?.Show()
    End Sub

    ''' <summary> Opens dark form shaped button click. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub OpenDarkFormShapedButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _OpenDarkFormShapedButton.Click
        TryCast(New BlackForm(), BlackForm)?.Show()
    End Sub

    ''' <summary> Opens dashboard shaped button click. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub OpenDashboardShapedButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _OpenDashboardShapedButton.Click
        TryCast(New DashboardForm(), DashboardForm)?.Show()
    End Sub

    ''' <summary> Exit button click. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ExitButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ExitButton.Click
        Me.Close()
    End Sub


End Class

