''' <summary> Default constructor. </summary>
''' <remarks> David, 2021-03-12. </remarks>
Imports isr.Core.Controls

Partial Public Class BlackForm
    Inherits Form

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.Windows.Forms.Form" /> class.
    ''' </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    Public Sub New()
        Me.InitializeComponent()
    End Sub
    ''' <summary> True if is top panel dragged, false if not. </summary>

    Private _IsTopPanelDragged As Boolean = False
    ''' <summary> True if is left panel dragged, false if not. </summary>
    Private _IsLeftPanelDragged As Boolean = False
    ''' <summary> True if is right panel dragged, false if not. </summary>
    Private _IsRightPanelDragged As Boolean = False
    ''' <summary> True if is bottom panel dragged, false if not. </summary>
    Private _IsBottomPanelDragged As Boolean = False
    ''' <summary> True if is top border panel dragged, false if not. </summary>
    Private _IsTopBorderPanelDragged As Boolean = False
    ''' <summary> True if is right bottom panel dragged, false if not. </summary>

    Private _IsRightBottomPanelDragged As Boolean = False
    ''' <summary> True if is left bottom panel dragged, false if not. </summary>
    Private _IsLeftBottomPanelDragged As Boolean = False
    ''' <summary> True if is right top panel dragged, false if not. </summary>
    Private _IsRightTopPanelDragged As Boolean = False
    ''' <summary> True if is left top panel dragged, false if not. </summary>
    Private _IsLeftTopPanelDragged As Boolean = False
    ''' <summary> True if is window maximized, false if not. </summary>

    Private _IsWindowMaximized As Boolean = False
    ''' <summary> The offset. </summary>
    Private _Offset As Point
    ''' <summary> Size of the normal window. </summary>
    Private _NormalWindowSize As Size
    ''' <summary> The normal window location. </summary>
    Private _NormalWindowLocation As Point = Point.Empty

    ''' <summary> Top border panel mouse down. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub TopBorderPanel_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _TopBorderPanel.MouseDown
        Me._IsTopBorderPanelDragged = e.Button = MouseButtons.Left
    End Sub

    ''' <summary> Top border panel mouse move. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub TopBorderPanel_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _TopBorderPanel.MouseMove
        If e.Y < Me.Location.Y Then
            If Me._IsTopBorderPanelDragged Then
                If Me.Height < 50 Then
                    Me.Height = 50
                    Me._IsTopBorderPanelDragged = False
                Else
                    Me.Location = New Point(Me.Location.X, Me.Location.Y + e.Y)
                    Me.Height -= e.Y
                End If
            End If
        End If
    End Sub

    ''' <summary> Top border panel mouse up. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub TopBorderPanel_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _TopBorderPanel.MouseUp
        Me._IsTopBorderPanelDragged = False
    End Sub

    ''' <summary> Top panel mouse down. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub TopPanel_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _TopPanel.MouseDown
        If e.Button = MouseButtons.Left Then
            Me._IsTopPanelDragged = True
            Dim pointStartPosition As Point = Me.PointToScreen(New Point(e.X, e.Y))
            Me._Offset = New Point() With {
                .X = Me.Location.X - pointStartPosition.X,
                .Y = Me.Location.Y - pointStartPosition.Y}
        Else
            Me._IsTopPanelDragged = False
        End If
        If e.Clicks = 2 Then
            Me._IsTopPanelDragged = False
            Me.MaxButton_Click(sender, e)
        End If
    End Sub

    ''' <summary> Top panel mouse move. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub TopPanel_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _TopPanel.MouseMove
        If Me._IsTopPanelDragged Then
            Dim newPoint As Point = Me._TopPanel.PointToScreen(New Point(e.X, e.Y))
            newPoint.Offset(Me._Offset)
            Me.Location = newPoint

            If Me.Location.X > 2 OrElse Me.Location.Y > 2 Then
                If Me.WindowState = FormWindowState.Maximized Then
                    Me.Location = Me._NormalWindowLocation
                    Me.Size = Me._NormalWindowSize
                    Me._ToolTip.SetToolTip(Me._MaxButton, "Maximize")
                    Me._MaxButton.CustomFormState = CustomFormState.Normal
                    Me._IsWindowMaximized = False
                End If
            End If
        End If
    End Sub

    ''' <summary> Top panel mouse up. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub TopPanel_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _TopPanel.MouseUp
        Me._IsTopPanelDragged = False
        If Me.Location.Y <= 5 Then
            If Not Me._IsWindowMaximized Then
                Me._NormalWindowSize = Me.Size
                Me._NormalWindowLocation = Me.Location

                Dim rect As Rectangle = Screen.PrimaryScreen.WorkingArea
                Me.Location = New Point(0, 0)
                Me.Size = New System.Drawing.Size(rect.Width, rect.Height)
                Me._ToolTip.SetToolTip(Me._MaxButton, "Restore Down")
                Me._MaxButton.CustomFormState = CustomFormState.Maximize
                Me._IsWindowMaximized = True
            End If
        End If
    End Sub

    ''' <summary> Left panel mouse down. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub LeftPanel_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _LeftBorderPanel.MouseDown
        If Me.Location.X <= 0 OrElse e.X < 0 Then
            Me._IsLeftPanelDragged = False
            Me.Location = New Point(10, Me.Location.Y)
        Else
            Me._IsLeftPanelDragged = e.Button = MouseButtons.Left
        End If
    End Sub

    ''' <summary> Left panel mouse move. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub LeftPanel_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _LeftBorderPanel.MouseMove
        If e.X < Me.Location.X Then
            If Me._IsLeftPanelDragged Then
                If Me.Width < 100 Then
                    Me.Width = 100
                    Me._IsLeftPanelDragged = False
                Else
                    Me.Location = New Point(Me.Location.X + e.X, Me.Location.Y)
                    Me.Width -= e.X
                End If
            End If
        End If
    End Sub

    ''' <summary> Left panel mouse up. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub LeftPanel_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _LeftBorderPanel.MouseUp
        Me._IsLeftPanelDragged = False
    End Sub

    ''' <summary> Right panel mouse down. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub RightPanel_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _RightBorderPanel.MouseDown
        Me._IsRightPanelDragged = e.Button = MouseButtons.Left
    End Sub

    ''' <summary> Right panel mouse move. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub RightPanel_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _RightBorderPanel.MouseMove
        If Me._IsRightPanelDragged Then
            If Me.Width < 100 Then
                Me.Width = 100
                Me._IsRightPanelDragged = False
            Else
                Me.Width += e.X
            End If
        End If
    End Sub

    ''' <summary> Right panel mouse up. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub RightPanel_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _RightBorderPanel.MouseUp
        Me._IsRightPanelDragged = False
    End Sub

    ''' <summary> Bottom panel mouse down. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub BottomPanel_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _BottomBorderPanel.MouseDown
        Me._IsBottomPanelDragged = e.Button = MouseButtons.Left
    End Sub

    ''' <summary> Bottom panel mouse move. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub BottomPanel_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _BottomBorderPanel.MouseMove
        If Me._IsBottomPanelDragged Then
            If Me.Height < 50 Then
                Me.Height = 50
                Me._IsBottomPanelDragged = False
            Else
                Me.Height += e.Y
            End If
        End If
    End Sub

    ''' <summary> Bottom panel mouse up. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub BottomPanel_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _BottomBorderPanel.MouseUp
        Me._IsBottomPanelDragged = False
    End Sub

    ''' <summary> Minimum button click. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub MinButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _MinButton.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub

    ''' <summary> Maximum button click. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub MaxButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _MaxButton.Click
        If Me._IsWindowMaximized Then
            Me.Location = Me._NormalWindowLocation
            Me.Size = Me._NormalWindowSize
            Me._ToolTip.SetToolTip(Me._MaxButton, "Maximize")
            Me._MaxButton.CustomFormState = CustomFormState.Normal
            Me._IsWindowMaximized = False
        Else
            Me._NormalWindowSize = Me.Size
            Me._NormalWindowLocation = Me.Location

            Dim rect As Rectangle = Screen.PrimaryScreen.WorkingArea
            Me.Location = New Point(0, 0)
            Me.Size = New System.Drawing.Size(rect.Width, rect.Height)
            Me._ToolTip.SetToolTip(Me._MaxButton, "Restore Down")
            Me._MaxButton.CustomFormState = CustomFormState.Maximize
            Me._IsWindowMaximized = True
        End If
    End Sub

    ''' <summary> Closes button click. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub CloseButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _CloseButton.Click
        Me.Close()
    End Sub

    ''' <summary> Right bottom panel 1 mouse down. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub RightBottomPanel_1_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _BottomBorderRightPanel.MouseDown
        Me._IsRightBottomPanelDragged = True
    End Sub

    ''' <summary> Right bottom panel 1 mouse move. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub RightBottomPanel_1_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _BottomBorderRightPanel.MouseMove
        If Me._IsRightBottomPanelDragged Then
            If Me.Width < 100 OrElse Me.Height < 50 Then
                Me.Width = 100
                Me.Height = 50
                Me._IsRightBottomPanelDragged = False
            Else
                Me.Width += e.X
                Me.Height += e.Y
            End If
        End If
    End Sub

    ''' <summary> Right bottom panel 1 mouse up. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub RightBottomPanel_1_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _BottomBorderRightPanel.MouseUp
        Me._IsRightBottomPanelDragged = False
    End Sub

    ''' <summary> Right bottom panel 2 mouse down. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub RightBottomPanel_2_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _RightBorderBottomPanel.MouseDown
        Me.RightBottomPanel_1_MouseDown(sender, e)
    End Sub

    ''' <summary> Right bottom panel 2 mouse move. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub RightBottomPanel_2_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _RightBorderBottomPanel.MouseMove
        Me.RightBottomPanel_1_MouseMove(sender, e)
    End Sub

    ''' <summary> Right bottom panel 2 mouse up. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub RightBottomPanel_2_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _RightBorderBottomPanel.MouseUp
        Me.RightBottomPanel_1_MouseUp(sender, e)
    End Sub

    ''' <summary> Left bottom panel 1 mouse down. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub LeftBottomPanel_1_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _BottomBorderLeftPanel.MouseDown
        Me._IsLeftBottomPanelDragged = True
    End Sub

    ''' <summary> Left bottom panel 1 mouse move. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub LeftBottomPanel_1_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _BottomBorderLeftPanel.MouseMove
        If e.X < Me.Location.X Then
            If Me._IsLeftBottomPanelDragged OrElse Me.Height < 50 Then
                If Me.Width < 100 Then
                    Me.Width = 100
                    Me.Height = 50
                    Me._IsLeftBottomPanelDragged = False
                Else
                    Me.Location = New Point(Me.Location.X + e.X, Me.Location.Y)
                    Me.Width -= e.X
                    Me.Height += e.Y
                End If
            End If
        End If
    End Sub

    ''' <summary> Left bottom panel 1 mouse up. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub LeftBottomPanel_1_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _BottomBorderLeftPanel.MouseUp
        Me._IsLeftBottomPanelDragged = False
    End Sub

    ''' <summary> Left bottom panel 2 mouse down. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub LeftBottomPanel_2_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _LeftBorderBottomPanel.MouseDown
        Me.LeftBottomPanel_1_MouseDown(sender, e)
    End Sub

    ''' <summary> Left bottom panel 2 mouse move. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub LeftBottomPanel_2_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _LeftBorderBottomPanel.MouseMove
        Me.LeftBottomPanel_1_MouseMove(sender, e)
    End Sub

    ''' <summary> Left bottom panel 2 mouse up. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub LeftBottomPanel_2_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _LeftBorderBottomPanel.MouseUp
        Me.LeftBottomPanel_1_MouseUp(sender, e)
    End Sub

    ''' <summary> Right top panel 1 mouse down. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub RightTopPanel_1_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _RightBorderTopPanel.MouseDown
        Me._IsRightTopPanelDragged = True
    End Sub

    ''' <summary> Right top panel 1 mouse move. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub RightTopPanel_1_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _RightBorderTopPanel.MouseMove
        If e.Y < Me.Location.Y OrElse e.X < Me.Location.X Then
            If Me._IsRightTopPanelDragged Then
                If Me.Height < 50 OrElse Me.Width < 100 Then
                    Me.Height = 50
                    Me.Width = 100
                    Me._IsRightTopPanelDragged = False
                Else
                    Me.Location = New Point(Me.Location.X, Me.Location.Y + e.Y)
                    Me.Height -= e.Y
                    Me.Width += e.X
                End If
            End If
        End If
    End Sub

    ''' <summary> Right top panel 1 mouse up. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub RightTopPanel_1_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _RightBorderTopPanel.MouseUp
        Me._IsRightTopPanelDragged = False
    End Sub

    ''' <summary> Right top panel 2 mouse down. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub RightTopPanel_2_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _TopBorderRightPanel.MouseDown
        Me.RightTopPanel_1_MouseDown(sender, e)
    End Sub

    ''' <summary> Right top panel 2 mouse move. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub RightTopPanel_2_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _TopBorderRightPanel.MouseMove
        Me.RightTopPanel_1_MouseMove(sender, e)
    End Sub

    ''' <summary> Right top panel 2 mouse up. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub RightTopPanel_2_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _TopBorderRightPanel.MouseUp
        Me.RightTopPanel_1_MouseUp(sender, e)
    End Sub

    ''' <summary> Left top panel 1 mouse down. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub LeftTopPanel_1_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _TopBorderLeftPanel.MouseDown
        Me._IsLeftTopPanelDragged = True
    End Sub

    ''' <summary> Left top panel 1 mouse move. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub LeftTopPanel_1_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _TopBorderLeftPanel.MouseMove
        If e.X < Me.Location.X OrElse e.Y < Me.Location.Y Then
            If Me._IsLeftTopPanelDragged Then
                If Me.Width < 100 OrElse Me.Height < 50 Then
                    Me.Width = 100
                    Me.Height = 100
                    Me._IsLeftTopPanelDragged = False
                Else
                    Me.Location = New Point(Me.Location.X + e.X, Me.Location.Y)
                    Me.Width -= e.X
                    Me.Location = New Point(Me.Location.X, Me.Location.Y + e.Y)
                    Me.Height -= e.Y
                End If
            End If
        End If

    End Sub

    ''' <summary> Left top panel 1 mouse up. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub LeftTopPanel_1_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _TopBorderLeftPanel.MouseUp
        Me._IsLeftTopPanelDragged = False
    End Sub

    ''' <summary> Left top panel 2 mouse down. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub LeftTopPanel_2_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _LeftBorderTopPanel.MouseDown
        Me.LeftTopPanel_1_MouseDown(sender, e)
    End Sub

    ''' <summary> Left top panel 2 mouse move. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub LeftTopPanel_2_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _LeftBorderTopPanel.MouseMove
        Me.LeftTopPanel_1_MouseMove(sender, e)
    End Sub

    ''' <summary> Left top panel 2 mouse up. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub LeftTopPanel_2_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _LeftBorderTopPanel.MouseUp
        Me.LeftTopPanel_1_MouseUp(sender, e)
    End Sub

    ''' <summary> File button click. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub File_button_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _FileButton.Click
        Me._FileButton.BusyBackColor = Color.Black
        Me._FileButton.MouseColorsEnabled = False
        Me._EditButton.BusyBackColor = Color.FromArgb(60, 60, 60)
        Me._ViewButton.BusyBackColor = Color.FromArgb(60, 60, 60)
        Me._RunButton.BusyBackColor = Color.FromArgb(60, 60, 60)
        Me._HelpButton.BusyBackColor = Color.FromArgb(60, 60, 60)
        Me._EditButton.MouseColorsEnabled = True
        Me._ViewButton.MouseColorsEnabled = True
        Me._RunButton.MouseColorsEnabled = True
        Me._HelpButton.MouseColorsEnabled = True
    End Sub

    ''' <summary> Edit button click. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Edit_button_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _EditButton.Click
        Me._EditButton.BusyBackColor = Color.Black
        Me._EditButton.MouseColorsEnabled = False
        Me._FileButton.BusyBackColor = Color.FromArgb(60, 60, 60)
        Me._ViewButton.BusyBackColor = Color.FromArgb(60, 60, 60)
        Me._RunButton.BusyBackColor = Color.FromArgb(60, 60, 60)
        Me._HelpButton.BusyBackColor = Color.FromArgb(60, 60, 60)
        Me._FileButton.MouseColorsEnabled = True
        Me._ViewButton.MouseColorsEnabled = True
        Me._RunButton.MouseColorsEnabled = True
        Me._HelpButton.MouseColorsEnabled = True
    End Sub

    ''' <summary> View button click. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub View_button_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ViewButton.Click
        Me._ViewButton.BusyBackColor = Color.Black
        Me._ViewButton.MouseColorsEnabled = False
        Me._FileButton.BusyBackColor = Color.FromArgb(60, 60, 60)
        Me._EditButton.BusyBackColor = Color.FromArgb(60, 60, 60)
        Me._RunButton.BusyBackColor = Color.FromArgb(60, 60, 60)
        Me._HelpButton.BusyBackColor = Color.FromArgb(60, 60, 60)
        Me._FileButton.MouseColorsEnabled = True
        Me._EditButton.MouseColorsEnabled = True
        Me._RunButton.MouseColorsEnabled = True
        Me._HelpButton.MouseColorsEnabled = True
    End Sub

    ''' <summary> Executes the 'button click' operation. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Run_button_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _RunButton.Click
        Me._RunButton.BusyBackColor = Color.Black
        Me._RunButton.MouseColorsEnabled = False
        Me._FileButton.BusyBackColor = Color.FromArgb(60, 60, 60)
        Me._EditButton.BusyBackColor = Color.FromArgb(60, 60, 60)
        Me._ViewButton.BusyBackColor = Color.FromArgb(60, 60, 60)
        Me._HelpButton.BusyBackColor = Color.FromArgb(60, 60, 60)
        Me._FileButton.MouseColorsEnabled = True
        Me._EditButton.MouseColorsEnabled = True
        Me._ViewButton.MouseColorsEnabled = True
        Me._HelpButton.MouseColorsEnabled = True
    End Sub

    ''' <summary> Help button click. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Help_button_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _HelpButton.Click
        Me._HelpButton.BusyBackColor = Color.Black
        Me._HelpButton.MouseColorsEnabled = False
        Me._FileButton.BusyBackColor = Color.FromArgb(60, 60, 60)
        Me._EditButton.BusyBackColor = Color.FromArgb(60, 60, 60)
        Me._ViewButton.BusyBackColor = Color.FromArgb(60, 60, 60)
        Me._RunButton.BusyBackColor = Color.FromArgb(60, 60, 60)
        Me._FileButton.MouseColorsEnabled = True
        Me._EditButton.MouseColorsEnabled = True
        Me._ViewButton.MouseColorsEnabled = True
        Me._RunButton.MouseColorsEnabled = True
    End Sub

    ''' <summary> Window text label mouse down. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub WindowTextLabel_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _WindowTextLabel.MouseDown
        Me.TopPanel_MouseDown(sender, e)
    End Sub

    ''' <summary> Window text label mouse move. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub WindowTextLabel_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _WindowTextLabel.MouseMove
        Me.TopPanel_MouseMove(sender, e)
    End Sub

    ''' <summary> Window text label mouse up. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub WindowTextLabel_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _WindowTextLabel.MouseUp
        Me.TopPanel_MouseUp(sender, e)
    End Sub




End Class

