﻿Imports isr.Core.Controls
Partial Public Class BlueForm
    ''' <summary>
    ''' Required designer variable.
    ''' </summary>
    Private components As System.ComponentModel.IContainer = Nothing

    ''' <summary>
    ''' Clean up any resources being used.
    ''' </summary>
    ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso (components IsNot Nothing) Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

#Region "Windows Form Designer generated code"

    ''' <summary>
    ''' Required method for Designer support - do not modify
    ''' the contents of this method with the code editor.
    ''' </summary>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me._TopBorderPanel = New System.Windows.Forms.Panel()
        Me._RightBorderPanel = New System.Windows.Forms.Panel()
        Me._LeftBorderPanel = New System.Windows.Forms.Panel()
        Me._BottomBorderPanel = New System.Windows.Forms.Panel()
        Me._TopPanel = New System.Windows.Forms.Panel()
        Me._IconPanel = New System.Windows.Forms.Panel()
        Me._WindowTextLabel = New System.Windows.Forms.Label()
        Me._MaxButton = New MinMaxButton()
        Me._MinButton = New ZopeButton()
        Me._CloseButton = New ZopeButton()
        Me._ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._TopPanel.SuspendLayout()
        Me.SuspendLayout()
        '
        '_TopBorderPanel
        '
        Me._TopBorderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(20, Byte), Integer), CType(CType(50, Byte), Integer))
        Me._TopBorderPanel.Cursor = System.Windows.Forms.Cursors.SizeNS
        Me._TopBorderPanel.Dock = System.Windows.Forms.DockStyle.Top
        Me._TopBorderPanel.Location = New System.Drawing.Point(0, 0)
        Me._TopBorderPanel.Name = "_TopBorderPanel"
        Me._TopBorderPanel.Size = New System.Drawing.Size(684, 2)
        Me._TopBorderPanel.TabIndex = 0
        '
        '_RightBorderPanel
        '
        Me._RightBorderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(20, Byte), Integer), CType(CType(50, Byte), Integer))
        Me._RightBorderPanel.Cursor = System.Windows.Forms.Cursors.SizeWE
        Me._RightBorderPanel.Dock = System.Windows.Forms.DockStyle.Right
        Me._RightBorderPanel.Location = New System.Drawing.Point(682, 2)
        Me._RightBorderPanel.Name = "_RightBorderPanel"
        Me._RightBorderPanel.Size = New System.Drawing.Size(2, 459)
        Me._RightBorderPanel.TabIndex = 1
        '
        '_LeftBorderPanel
        '
        Me._LeftBorderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(20, Byte), Integer), CType(CType(50, Byte), Integer))
        Me._LeftBorderPanel.Cursor = System.Windows.Forms.Cursors.SizeWE
        Me._LeftBorderPanel.Dock = System.Windows.Forms.DockStyle.Left
        Me._LeftBorderPanel.Location = New System.Drawing.Point(0, 2)
        Me._LeftBorderPanel.Name = "_LeftBorderPanel"
        Me._LeftBorderPanel.Size = New System.Drawing.Size(2, 459)
        Me._LeftBorderPanel.TabIndex = 2
        '
        '_BottomBorderPanel
        '
        Me._BottomBorderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(20, Byte), Integer), CType(CType(50, Byte), Integer))
        Me._BottomBorderPanel.Cursor = System.Windows.Forms.Cursors.SizeNS
        Me._BottomBorderPanel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._BottomBorderPanel.Location = New System.Drawing.Point(2, 459)
        Me._BottomBorderPanel.Name = "_BottomBorderPanel"
        Me._BottomBorderPanel.Size = New System.Drawing.Size(680, 2)
        Me._BottomBorderPanel.TabIndex = 3
        '
        '_TopPanel
        '
        Me._TopPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(20, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(80, Byte), Integer))
        Me._TopPanel.Controls.Add(Me._IconPanel)
        Me._TopPanel.Controls.Add(Me._WindowTextLabel)
        Me._TopPanel.Controls.Add(Me._MaxButton)
        Me._TopPanel.Controls.Add(Me._MinButton)
        Me._TopPanel.Controls.Add(Me._CloseButton)
        Me._TopPanel.Dock = System.Windows.Forms.DockStyle.Top
        Me._TopPanel.Location = New System.Drawing.Point(2, 2)
        Me._TopPanel.Name = "_TopPanel"
        Me._TopPanel.Size = New System.Drawing.Size(680, 35)
        Me._TopPanel.TabIndex = 4
        '
        '_IconPanel
        '
        Me._IconPanel.BackgroundImage = Global.isr.Core.Tester.My.Resources.Resources.Crazy
        Me._IconPanel.Location = New System.Drawing.Point(10, 3)
        Me._IconPanel.Name = "_IconPanel"
        Me._IconPanel.Size = New System.Drawing.Size(26, 26)
        Me._IconPanel.TabIndex = 5
        '
        '_WindowTextLabel
        '
        Me._WindowTextLabel.AutoSize = True
        Me._WindowTextLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._WindowTextLabel.ForeColor = System.Drawing.Color.White
        Me._WindowTextLabel.Location = New System.Drawing.Point(57, 7)
        Me._WindowTextLabel.Name = "_WindowTextLabel"
        Me._WindowTextLabel.Size = New System.Drawing.Size(77, 18)
        Me._WindowTextLabel.TabIndex = 6
        Me._WindowTextLabel.Text = "Blue Form"
        '
        '_MaxButton
        '
        Me._MaxButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._MaxButton.BusyBackColor = System.Drawing.Color.FromArgb(CType(CType(20, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(80, Byte), Integer))
        Me._MaxButton.CustomFormState = CustomFormState.Normal
        Me._MaxButton.DisplayText = "_"
        Me._MaxButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me._MaxButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._MaxButton.ForeColor = System.Drawing.Color.White
        Me._MaxButton.Location = New System.Drawing.Point(604, 3)
        Me._MaxButton.MouseClickColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(20, Byte), Integer), CType(CType(60, Byte), Integer))
        Me._MaxButton.MouseHoverColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(180, Byte), Integer))
        Me._MaxButton.Name = "_MaxButton"
        Me._MaxButton.Size = New System.Drawing.Size(35, 25)
        Me._MaxButton.TabIndex = 5
        Me._MaxButton.Text = "minMaxButton1"
        Me._MaxButton.TextLocation = New Point(11, 8)
        Me._ToolTip.SetToolTip(Me._MaxButton, "Maximize")
        Me._MaxButton.UseVisualStyleBackColor = True
        '
        '_MinButton
        '
        Me._MinButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._MinButton.BusyBackColor = System.Drawing.Color.FromArgb(CType(CType(20, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(80, Byte), Integer))
        Me._MinButton.DisplayText = "_"
        Me._MinButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me._MinButton.Font = New System.Drawing.Font("Microsoft YaHei UI", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._MinButton.ForeColor = System.Drawing.Color.White
        Me._MinButton.Location = New System.Drawing.Point(569, 3)
        Me._MinButton.MouseClickColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(20, Byte), Integer), CType(CType(60, Byte), Integer))
        Me._MinButton.MouseColorsEnabled = True
        Me._MinButton.MouseHoverColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(180, Byte), Integer))
        Me._MinButton.Name = "_MinButton"
        Me._MinButton.Size = New System.Drawing.Size(35, 25)
        Me._MinButton.TabIndex = 1
        Me._MinButton.Text = "_"
        Me._MinButton.TextLocationLeft = 8
        Me._MinButton.TextLocationTop = -14
        Me._ToolTip.SetToolTip(Me._MinButton, "Minimize")
        Me._MinButton.UseVisualStyleBackColor = True
        '
        '_CloseButton
        '
        Me._CloseButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._CloseButton.BusyBackColor = System.Drawing.Color.FromArgb(CType(CType(20, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(80, Byte), Integer))
        Me._CloseButton.DisplayText = "X"
        Me._CloseButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me._CloseButton.Font = New System.Drawing.Font("Microsoft YaHei UI", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._CloseButton.ForeColor = System.Drawing.Color.White
        Me._CloseButton.Location = New System.Drawing.Point(639, 3)
        Me._CloseButton.MouseClickColor = System.Drawing.Color.FromArgb(CType(CType(150, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me._CloseButton.MouseColorsEnabled = True
        Me._CloseButton.MouseHoverColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(180, Byte), Integer))
        Me._CloseButton.Name = "_CloseButton"
        Me._CloseButton.Size = New System.Drawing.Size(35, 25)
        Me._CloseButton.TabIndex = 0
        Me._CloseButton.Text = "X"
        Me._CloseButton.TextLocationLeft = 10
        Me._CloseButton.TextLocationTop = 4
        Me._ToolTip.SetToolTip(Me._CloseButton, "Close")
        Me._CloseButton.UseVisualStyleBackColor = True
        '
        'BlueForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(150, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(684, 461)
        Me.Controls.Add(Me._TopPanel)
        Me.Controls.Add(Me._BottomBorderPanel)
        Me.Controls.Add(Me._LeftBorderPanel)
        Me.Controls.Add(Me._RightBorderPanel)
        Me.Controls.Add(Me._TopBorderPanel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "BlueForm"
        Me.Text = "Blue Form"
        Me._TopPanel.ResumeLayout(False)
        Me._TopPanel.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private WithEvents _TopBorderPanel As System.Windows.Forms.Panel
    Private WithEvents _RightBorderPanel As System.Windows.Forms.Panel
    Private WithEvents _LeftBorderPanel As System.Windows.Forms.Panel
    Private WithEvents _BottomBorderPanel As System.Windows.Forms.Panel
    Private WithEvents _TopPanel As System.Windows.Forms.Panel
    Private WithEvents _CloseButton As ZopeButton
    Private WithEvents _MaxButton As MinMaxButton
    Private WithEvents _MinButton As ZopeButton
    Private _ToolTip As System.Windows.Forms.ToolTip
    Private _WindowTextLabel As System.Windows.Forms.Label
    Private _IconPanel As System.Windows.Forms.Panel
End Class
