''' <summary> Form for viewing the dashboard. </summary>
''' <remarks> David, 2021-03-12. </remarks>
Partial Public Class DashboardForm
    Inherits Form

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.Windows.Forms.Form" /> class.
    ''' </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    Public Sub New()
        Me.InitializeComponent()
    End Sub
    ''' <summary> The offset. </summary>

    Private _Offset As Point
    ''' <summary> True if is top panel dragged, false if not. </summary>
    Private _IsTopPanelDragged As Boolean = False

    ''' <summary> Dashboard form load. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Dashboard_Form_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Dashboard_button_Click(sender, e)
    End Sub

    ''' <summary> Top panel mouse down. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub TopPanel_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _TopPanel.MouseDown
        If e.Button = MouseButtons.Left Then
            Me._IsTopPanelDragged = True
            Dim pointStartPosition As Point = Me.PointToScreen(New Point(e.X, e.Y))
            Me._Offset = New Point() With {
                .X = Me.Location.X - (pointStartPosition.X + Me._LeftTopPanel.Size.Width),
                .Y = Me.Location.Y - pointStartPosition.Y}
        Else
            Me._IsTopPanelDragged = False
        End If
    End Sub

    ''' <summary> Top panel mouse move. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub TopPanel_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _TopPanel.MouseMove
        If Me._IsTopPanelDragged Then
            Dim newPoint As Point = Me._TopPanel.PointToScreen(New Point(e.X, e.Y))
            newPoint.Offset(Me._Offset)
            Me.Location = newPoint
        End If
    End Sub

    ''' <summary> Top panel mouse up. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub TopPanel_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _TopPanel.MouseUp
        Me._IsTopPanelDragged = False
    End Sub

    ''' <summary> Closes button click. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub CloseButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _CloseButton.Click
        Me.Close()
    End Sub

    ''' <summary> Minimum button click. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub MinButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _MinButton.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub

    ''' <summary> Dashboard label mouse down. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub DashboardLabel_mouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _DashboardLabel.MouseDown
        Me.TopPanel_MouseDown(sender, e)
    End Sub

    ''' <summary> Dashboard label mouse move. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub DashboardLabel_mouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _DashboardLabel.MouseMove
        Me.TopPanel_MouseMove(sender, e)
    End Sub

    ''' <summary> Dashboard label mouse up. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub DashboardLabel_mouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _DashboardLabel.MouseUp
        Me.TopPanel_MouseUp(sender, e)
    End Sub

    ''' <summary> Dashboard button click. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Dashboard_button_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _DashboardButton.Click
        Me._DashboardButton.BusyBackColor = Color.FromArgb(20, 20, 20)
        Me._DashboardButton.MouseColorsEnabled = False
        Me._StatsButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        Me._PagesButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        Me._LayoutButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        Me._ThemeButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        Me._SettingsButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        Me._StatsButton.MouseColorsEnabled = True
        Me._PagesButton.MouseColorsEnabled = True
        Me._LayoutButton.MouseColorsEnabled = True
        Me._ThemeButton.MouseColorsEnabled = True
        Me._SettingsButton.MouseColorsEnabled = True
    End Sub

    ''' <summary> Statistics button click. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Stats_button_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _StatsButton.Click
        Me._StatsButton.BusyBackColor = Color.FromArgb(20, 20, 20)
        Me._StatsButton.MouseColorsEnabled = False
        Me._DashboardButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        Me._PagesButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        Me._LayoutButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        Me._ThemeButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        Me._SettingsButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        Me._DashboardButton.MouseColorsEnabled = True
        Me._PagesButton.MouseColorsEnabled = True
        Me._LayoutButton.MouseColorsEnabled = True
        Me._ThemeButton.MouseColorsEnabled = True
        Me._SettingsButton.MouseColorsEnabled = True
    End Sub

    ''' <summary> Pages button click. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Pages_button_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _PagesButton.Click
        Me._PagesButton.BusyBackColor = Color.FromArgb(20, 20, 20)
        Me._PagesButton.MouseColorsEnabled = False
        Me._DashboardButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        Me._StatsButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        Me._LayoutButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        Me._ThemeButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        Me._SettingsButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        Me._DashboardButton.MouseColorsEnabled = True
        Me._StatsButton.MouseColorsEnabled = True
        Me._LayoutButton.MouseColorsEnabled = True
        Me._ThemeButton.MouseColorsEnabled = True
        Me._SettingsButton.MouseColorsEnabled = True
    End Sub

    ''' <summary> Layout button click. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Layout_button_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _LayoutButton.Click
        Me._LayoutButton.BusyBackColor = Color.FromArgb(20, 20, 20)
        Me._LayoutButton.MouseColorsEnabled = False
        Me._DashboardButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        Me._StatsButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        Me._PagesButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        Me._ThemeButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        Me._SettingsButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        Me._DashboardButton.MouseColorsEnabled = True
        Me._StatsButton.MouseColorsEnabled = True
        Me._PagesButton.MouseColorsEnabled = True
        Me._ThemeButton.MouseColorsEnabled = True
        Me._SettingsButton.MouseColorsEnabled = True
    End Sub

    ''' <summary> Theme button click. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Theme_button_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ThemeButton.Click
        Me._ThemeButton.BusyBackColor = Color.FromArgb(20, 20, 20)
        Me._ThemeButton.MouseColorsEnabled = False
        Me._DashboardButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        Me._StatsButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        Me._PagesButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        Me._LayoutButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        Me._SettingsButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        Me._DashboardButton.MouseColorsEnabled = True
        Me._StatsButton.MouseColorsEnabled = True
        Me._PagesButton.MouseColorsEnabled = True
        Me._LayoutButton.MouseColorsEnabled = True
        Me._SettingsButton.MouseColorsEnabled = True
    End Sub

    ''' <summary> Settings button click. </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Settings_button_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _SettingsButton.Click
        Me._SettingsButton.BusyBackColor = Color.FromArgb(20, 20, 20)
        Me._SettingsButton.MouseColorsEnabled = False
        Me._DashboardButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        Me._StatsButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        Me._PagesButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        Me._LayoutButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        Me._ThemeButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        Me._DashboardButton.MouseColorsEnabled = True
        Me._StatsButton.MouseColorsEnabled = True
        Me._PagesButton.MouseColorsEnabled = True
        Me._LayoutButton.MouseColorsEnabled = True
        Me._ThemeButton.MouseColorsEnabled = True
    End Sub



End Class
