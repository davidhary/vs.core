﻿Imports isr.Core.Controls
Partial Public Class DashboardForm
    ''' <summary>
    ''' Required designer variable.
    ''' </summary>
    Private components As System.ComponentModel.IContainer = Nothing

    ''' <summary>
    ''' Clean up any resources being used.
    ''' </summary>
    ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso (components IsNot Nothing) Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

#Region "Windows Form Designer generated code"

    ''' <summary>
    ''' Required method for Designer support - do not modify
    ''' the contents of this method with the code editor.
    ''' </summary>
    Private Sub InitializeComponent()
        Me._LeftTopPanel = New System.Windows.Forms.Panel()
        Me._LeftTopPanelLabel = New System.Windows.Forms.Label()
        Me._TopPanel = New System.Windows.Forms.Panel()
        Me._DashboardLabel = New System.Windows.Forms.Label()
        Me._IconPanel = New System.Windows.Forms.Panel()
        Me._MinButton = New ZopeButton()
        Me._CloseButton = New ZopeButton()
        Me._LeftPanel = New System.Windows.Forms.Panel()
        Me._LeftBottomPanel = New System.Windows.Forms.Panel()
        Me._LeftBottomPanelLabel = New System.Windows.Forms.Label()
        Me._SettingsButton = New ZopeButton()
        Me._ThemeButton = New ZopeButton()
        Me._LayoutButton = New ZopeButton()
        Me._PagesButton = New ZopeButton()
        Me._StatsButton = New ZopeButton()
        Me._DashboardButton = New ZopeButton()
        Me._VisitorsPanel = New System.Windows.Forms.Panel()
        Me._MonthVisitorsCountLabel = New System.Windows.Forms.Label()
        Me._MonthVisitorsCountLabelLabel = New System.Windows.Forms.Label()
        Me._TodayVisitorsCountLabel = New System.Windows.Forms.Label()
        Me._TodayVisitorsCountLabelLabel = New System.Windows.Forms.Label()
        Me._VisitorsCountPanel = New System.Windows.Forms.Panel()
        Me._VisitorsCountLabel = New System.Windows.Forms.Label()
        Me._VisitorsCountLabelLabel = New System.Windows.Forms.Label()
        Me._AdminPanel = New System.Windows.Forms.Panel()
        Me._AdminNameLabel = New System.Windows.Forms.Label()
        Me._AdminLabelLabel = New System.Windows.Forms.Label()
        Me._AdminIconPanel = New System.Windows.Forms.Panel()
        Me._StoragePanel = New System.Windows.Forms.Panel()
        Me._TotalStorageLabel = New System.Windows.Forms.Label()
        Me._TotalStorageLabelLabel = New System.Windows.Forms.Label()
        Me._FreeStorageLabel = New System.Windows.Forms.Label()
        Me._FreeStorageLabelLabel = New System.Windows.Forms.Label()
        Me._UsedStorageLabel = New System.Windows.Forms.Label()
        Me._UsedStorageLabelLabel = New System.Windows.Forms.Label()
        Me._StorageTopPanel = New System.Windows.Forms.Panel()
        Me._StoratgeTitleLabel = New System.Windows.Forms.Label()
        Me._ControlPanel = New System.Windows.Forms.Panel()
        Me._ContrlPanelLabel = New System.Windows.Forms.Label()
        Me._DeleteBlogButton = New ZopeButton()
        Me._ViewBlogButton = New ZopeButton()
        Me._CampainsPanel = New System.Windows.Forms.Panel()
        Me._CampaignsLabel = New System.Windows.Forms.Label()
        Me._LeftTopPanel.SuspendLayout()
        Me._TopPanel.SuspendLayout()
        Me._LeftPanel.SuspendLayout()
        Me._LeftBottomPanel.SuspendLayout()
        Me._VisitorsPanel.SuspendLayout()
        Me._VisitorsCountPanel.SuspendLayout()
        Me._AdminPanel.SuspendLayout()
        Me._StoragePanel.SuspendLayout()
        Me._StorageTopPanel.SuspendLayout()
        Me._ControlPanel.SuspendLayout()
        Me._CampainsPanel.SuspendLayout()
        Me.SuspendLayout()
        '
        '_LeftTopPanel
        '
        Me._LeftTopPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(150, Byte), Integer), CType(CType(80, Byte), Integer))
        Me._LeftTopPanel.Controls.Add(Me._LeftTopPanelLabel)
        Me._LeftTopPanel.Location = New System.Drawing.Point(0, 0)
        Me._LeftTopPanel.Name = "_LeftTopPanel"
        Me._LeftTopPanel.Size = New System.Drawing.Size(245, 60)
        Me._LeftTopPanel.TabIndex = 0
        '
        '_LeftTopPanelLabel
        '
        Me._LeftTopPanelLabel.AutoSize = True
        Me._LeftTopPanelLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._LeftTopPanelLabel.ForeColor = System.Drawing.Color.White
        Me._LeftTopPanelLabel.Location = New System.Drawing.Point(45, 13)
        Me._LeftTopPanelLabel.Name = "_LeftTopPanelLabel"
        Me._LeftTopPanelLabel.Size = New System.Drawing.Size(131, 29)
        Me._LeftTopPanelLabel.TabIndex = 0
        Me._LeftTopPanelLabel.Text = "Extreme UI"
        '
        '_TopPanel
        '
        Me._TopPanel.BackColor = System.Drawing.Color.White
        Me._TopPanel.Controls.Add(Me._DashboardLabel)
        Me._TopPanel.Controls.Add(Me._IconPanel)
        Me._TopPanel.Controls.Add(Me._MinButton)
        Me._TopPanel.Controls.Add(Me._CloseButton)
        Me._TopPanel.Location = New System.Drawing.Point(245, 0)
        Me._TopPanel.Name = "_TopPanel"
        Me._TopPanel.Size = New System.Drawing.Size(605, 60)
        Me._TopPanel.TabIndex = 1
        '
        '_DashboardLabel
        '
        Me._DashboardLabel.AutoSize = True
        Me._DashboardLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._DashboardLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
        Me._DashboardLabel.Location = New System.Drawing.Point(60, 13)
        Me._DashboardLabel.Name = "_DashboardLabel"
        Me._DashboardLabel.Size = New System.Drawing.Size(131, 29)
        Me._DashboardLabel.TabIndex = 3
        Me._DashboardLabel.Text = "Dashboard"
        '
        '_IconPanel
        '
        Me._IconPanel.BackgroundImage = Global.isr.Core.Tester.My.Resources.Resources.home
        Me._IconPanel.Location = New System.Drawing.Point(6, 13)
        Me._IconPanel.Name = "_IconPanel"
        Me._IconPanel.Size = New System.Drawing.Size(30, 30)
        Me._IconPanel.TabIndex = 2
        '
        '_MinButton
        '
        Me._MinButton.BusyBackColor = System.Drawing.Color.White
        Me._MinButton.DisplayText = "_"
        Me._MinButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me._MinButton.Font = New System.Drawing.Font("Microsoft YaHei UI", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._MinButton.ForeColor = System.Drawing.Color.Black
        Me._MinButton.Location = New System.Drawing.Point(540, 4)
        Me._MinButton.MouseClickColor = System.Drawing.Color.FromArgb(CType(CType(160, Byte), Integer), CType(CType(180, Byte), Integer), CType(CType(200, Byte), Integer))
        Me._MinButton.MouseColorsEnabled = True
        Me._MinButton.MouseHoverColor = System.Drawing.Color.FromArgb(CType(CType(100, Byte), Integer), CType(CType(150, Byte), Integer), CType(CType(220, Byte), Integer))
        Me._MinButton.Name = "_MinButton"
        Me._MinButton.Size = New System.Drawing.Size(31, 24)
        Me._MinButton.TabIndex = 1
        Me._MinButton.Text = "_"
        Me._MinButton.TextLocationLeft = 6
        Me._MinButton.TextLocationTop = -20
        Me._MinButton.UseVisualStyleBackColor = True
        '
        '_CloseButton
        '
        Me._CloseButton.BusyBackColor = System.Drawing.Color.White
        Me._CloseButton.DisplayText = "X"
        Me._CloseButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me._CloseButton.Font = New System.Drawing.Font("Microsoft YaHei UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._CloseButton.ForeColor = System.Drawing.Color.Black
        Me._CloseButton.Location = New System.Drawing.Point(571, 4)
        Me._CloseButton.MouseClickColor = System.Drawing.Color.FromArgb(CType(CType(100, Byte), Integer), CType(CType(150, Byte), Integer), CType(CType(220, Byte), Integer))
        Me._CloseButton.MouseColorsEnabled = True
        Me._CloseButton.MouseHoverColor = System.Drawing.Color.FromArgb(CType(CType(180, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(50, Byte), Integer))
        Me._CloseButton.Name = "_CloseButton"
        Me._CloseButton.Size = New System.Drawing.Size(31, 24)
        Me._CloseButton.TabIndex = 0
        Me._CloseButton.Text = "X"
        Me._CloseButton.TextLocationLeft = 6
        Me._CloseButton.TextLocationTop = -1
        Me._CloseButton.UseVisualStyleBackColor = True
        '
        '_LeftPanel
        '
        Me._LeftPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer), CType(CType(40, Byte), Integer))
        Me._LeftPanel.Controls.Add(Me._LeftBottomPanel)
        Me._LeftPanel.Controls.Add(Me._SettingsButton)
        Me._LeftPanel.Controls.Add(Me._ThemeButton)
        Me._LeftPanel.Controls.Add(Me._LayoutButton)
        Me._LeftPanel.Controls.Add(Me._PagesButton)
        Me._LeftPanel.Controls.Add(Me._StatsButton)
        Me._LeftPanel.Controls.Add(Me._DashboardButton)
        Me._LeftPanel.Location = New System.Drawing.Point(0, 60)
        Me._LeftPanel.Name = "_LeftPanel"
        Me._LeftPanel.Size = New System.Drawing.Size(245, 440)
        Me._LeftPanel.TabIndex = 2
        '
        '_LeftBottomPanel
        '
        Me._LeftBottomPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(20, Byte), Integer), CType(CType(20, Byte), Integer), CType(CType(30, Byte), Integer))
        Me._LeftBottomPanel.Controls.Add(Me._LeftBottomPanelLabel)
        Me._LeftBottomPanel.Location = New System.Drawing.Point(0, 380)
        Me._LeftBottomPanel.Name = "_LeftBottomPanel"
        Me._LeftBottomPanel.Size = New System.Drawing.Size(245, 60)
        Me._LeftBottomPanel.TabIndex = 6
        '
        '_LeftBottomPanelLabel
        '
        Me._LeftBottomPanelLabel.AutoSize = True
        Me._LeftBottomPanelLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._LeftBottomPanelLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(200, Byte), Integer), CType(CType(200, Byte), Integer), CType(CType(200, Byte), Integer))
        Me._LeftBottomPanelLabel.Location = New System.Drawing.Point(12, 14)
        Me._LeftBottomPanelLabel.Name = "_LeftBottomPanelLabel"
        Me._LeftBottomPanelLabel.Size = New System.Drawing.Size(102, 16)
        Me._LeftBottomPanelLabel.TabIndex = 0
        Me._LeftBottomPanelLabel.Text = "Copyright   2016"
        '
        '_SettingsButton
        '
        Me._SettingsButton.BusyBackColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer), CType(CType(40, Byte), Integer))
        Me._SettingsButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me._SettingsButton.DisplayText = "Settings"
        Me._SettingsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me._SettingsButton.Font = New System.Drawing.Font("Microsoft YaHei UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SettingsButton.ForeColor = System.Drawing.Color.White
        Me._SettingsButton.Location = New System.Drawing.Point(0, 250)
        Me._SettingsButton.MouseClickColor = System.Drawing.Color.FromArgb(CType(CType(20, Byte), Integer), CType(CType(20, Byte), Integer), CType(CType(20, Byte), Integer))
        Me._SettingsButton.MouseColorsEnabled = True
        Me._SettingsButton.MouseHoverColor = System.Drawing.Color.FromArgb(CType(CType(20, Byte), Integer), CType(CType(20, Byte), Integer), CType(CType(20, Byte), Integer))
        Me._SettingsButton.Name = "_SettingsButton"
        Me._SettingsButton.Size = New System.Drawing.Size(245, 50)
        Me._SettingsButton.TabIndex = 5
        Me._SettingsButton.Text = "Settings"
        Me._SettingsButton.TextLocationLeft = 80
        Me._SettingsButton.TextLocationTop = 10
        Me._SettingsButton.UseVisualStyleBackColor = True
        '
        '_ThemeButton
        '
        Me._ThemeButton.BusyBackColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer), CType(CType(40, Byte), Integer))
        Me._ThemeButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me._ThemeButton.DisplayText = "Theme"
        Me._ThemeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me._ThemeButton.Font = New System.Drawing.Font("Microsoft YaHei UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ThemeButton.ForeColor = System.Drawing.Color.White
        Me._ThemeButton.Location = New System.Drawing.Point(0, 200)
        Me._ThemeButton.MouseClickColor = System.Drawing.Color.FromArgb(CType(CType(20, Byte), Integer), CType(CType(20, Byte), Integer), CType(CType(20, Byte), Integer))
        Me._ThemeButton.MouseColorsEnabled = True
        Me._ThemeButton.MouseHoverColor = System.Drawing.Color.FromArgb(CType(CType(20, Byte), Integer), CType(CType(20, Byte), Integer), CType(CType(20, Byte), Integer))
        Me._ThemeButton.Name = "_ThemeButton"
        Me._ThemeButton.Size = New System.Drawing.Size(245, 50)
        Me._ThemeButton.TabIndex = 4
        Me._ThemeButton.Text = "Theme"
        Me._ThemeButton.TextLocationLeft = 82
        Me._ThemeButton.TextLocationTop = 10
        Me._ThemeButton.UseVisualStyleBackColor = True
        '
        '_LayoutButton
        '
        Me._LayoutButton.BusyBackColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer), CType(CType(40, Byte), Integer))
        Me._LayoutButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me._LayoutButton.DisplayText = "Layout"
        Me._LayoutButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me._LayoutButton.Font = New System.Drawing.Font("Microsoft YaHei UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._LayoutButton.ForeColor = System.Drawing.Color.White
        Me._LayoutButton.Location = New System.Drawing.Point(0, 150)
        Me._LayoutButton.MouseClickColor = System.Drawing.Color.FromArgb(CType(CType(20, Byte), Integer), CType(CType(20, Byte), Integer), CType(CType(20, Byte), Integer))
        Me._LayoutButton.MouseColorsEnabled = True
        Me._LayoutButton.MouseHoverColor = System.Drawing.Color.FromArgb(CType(CType(20, Byte), Integer), CType(CType(20, Byte), Integer), CType(CType(20, Byte), Integer))
        Me._LayoutButton.Name = "_LayoutButton"
        Me._LayoutButton.Size = New System.Drawing.Size(245, 50)
        Me._LayoutButton.TabIndex = 3
        Me._LayoutButton.Text = "Layout"
        Me._LayoutButton.TextLocationLeft = 80
        Me._LayoutButton.TextLocationTop = 10
        Me._LayoutButton.UseVisualStyleBackColor = True
        '
        '_PagesButton
        '
        Me._PagesButton.BusyBackColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer), CType(CType(40, Byte), Integer))
        Me._PagesButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me._PagesButton.DisplayText = "Pages"
        Me._PagesButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me._PagesButton.Font = New System.Drawing.Font("Microsoft YaHei UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._PagesButton.ForeColor = System.Drawing.Color.White
        Me._PagesButton.Location = New System.Drawing.Point(0, 100)
        Me._PagesButton.MouseClickColor = System.Drawing.Color.FromArgb(CType(CType(20, Byte), Integer), CType(CType(20, Byte), Integer), CType(CType(20, Byte), Integer))
        Me._PagesButton.MouseColorsEnabled = True
        Me._PagesButton.MouseHoverColor = System.Drawing.Color.FromArgb(CType(CType(20, Byte), Integer), CType(CType(20, Byte), Integer), CType(CType(20, Byte), Integer))
        Me._PagesButton.Name = "_PagesButton"
        Me._PagesButton.Size = New System.Drawing.Size(245, 50)
        Me._PagesButton.TabIndex = 2
        Me._PagesButton.Text = "Pages"
        Me._PagesButton.TextLocationLeft = 82
        Me._PagesButton.TextLocationTop = 10
        Me._PagesButton.UseVisualStyleBackColor = True
        '
        '_StatsButton
        '
        Me._StatsButton.BusyBackColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer), CType(CType(40, Byte), Integer))
        Me._StatsButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me._StatsButton.DisplayText = "Stats"
        Me._StatsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me._StatsButton.Font = New System.Drawing.Font("Microsoft YaHei UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._StatsButton.ForeColor = System.Drawing.Color.White
        Me._StatsButton.Location = New System.Drawing.Point(0, 50)
        Me._StatsButton.MouseClickColor = System.Drawing.Color.FromArgb(CType(CType(20, Byte), Integer), CType(CType(20, Byte), Integer), CType(CType(20, Byte), Integer))
        Me._StatsButton.MouseColorsEnabled = True
        Me._StatsButton.MouseHoverColor = System.Drawing.Color.FromArgb(CType(CType(20, Byte), Integer), CType(CType(20, Byte), Integer), CType(CType(20, Byte), Integer))
        Me._StatsButton.Name = "_StatsButton"
        Me._StatsButton.Size = New System.Drawing.Size(245, 50)
        Me._StatsButton.TabIndex = 1
        Me._StatsButton.Text = "Stats"
        Me._StatsButton.TextLocationLeft = 84
        Me._StatsButton.TextLocationTop = 10
        Me._StatsButton.UseVisualStyleBackColor = True
        '
        '_DashboardButton
        '
        Me._DashboardButton.BusyBackColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer), CType(CType(40, Byte), Integer))
        Me._DashboardButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me._DashboardButton.DisplayText = "Dashboard"
        Me._DashboardButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me._DashboardButton.Font = New System.Drawing.Font("Microsoft YaHei UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._DashboardButton.ForeColor = System.Drawing.Color.White
        Me._DashboardButton.Location = New System.Drawing.Point(0, 0)
        Me._DashboardButton.MouseClickColor = System.Drawing.Color.FromArgb(CType(CType(20, Byte), Integer), CType(CType(20, Byte), Integer), CType(CType(20, Byte), Integer))
        Me._DashboardButton.MouseColorsEnabled = True
        Me._DashboardButton.MouseHoverColor = System.Drawing.Color.FromArgb(CType(CType(20, Byte), Integer), CType(CType(20, Byte), Integer), CType(CType(20, Byte), Integer))
        Me._DashboardButton.Name = "_DashboardButton"
        Me._DashboardButton.Size = New System.Drawing.Size(245, 50)
        Me._DashboardButton.TabIndex = 0
        Me._DashboardButton.Text = "Dashboard"
        Me._DashboardButton.TextLocationLeft = 60
        Me._DashboardButton.TextLocationTop = 10
        Me._DashboardButton.UseVisualStyleBackColor = True
        '
        '_VisitorsPanel
        '
        Me._VisitorsPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(160, Byte), Integer))
        Me._VisitorsPanel.Controls.Add(Me._MonthVisitorsCountLabel)
        Me._VisitorsPanel.Controls.Add(Me._MonthVisitorsCountLabelLabel)
        Me._VisitorsPanel.Controls.Add(Me._TodayVisitorsCountLabel)
        Me._VisitorsPanel.Controls.Add(Me._TodayVisitorsCountLabelLabel)
        Me._VisitorsPanel.Controls.Add(Me._VisitorsCountPanel)
        Me._VisitorsPanel.Location = New System.Drawing.Point(251, 66)
        Me._VisitorsPanel.Name = "_VisitorsPanel"
        Me._VisitorsPanel.Size = New System.Drawing.Size(590, 109)
        Me._VisitorsPanel.TabIndex = 3
        '
        '_MonthVisitorsCountLabel
        '
        Me._MonthVisitorsCountLabel.AutoSize = True
        Me._MonthVisitorsCountLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._MonthVisitorsCountLabel.ForeColor = System.Drawing.Color.Azure
        Me._MonthVisitorsCountLabel.Location = New System.Drawing.Point(457, 61)
        Me._MonthVisitorsCountLabel.Name = "_MonthVisitorsCountLabel"
        Me._MonthVisitorsCountLabel.Size = New System.Drawing.Size(36, 16)
        Me._MonthVisitorsCountLabel.TabIndex = 4
        Me._MonthVisitorsCountLabel.Text = "1035"
        '
        '_MonthVisitorsCountLabelLabel
        '
        Me._MonthVisitorsCountLabelLabel.AutoSize = True
        Me._MonthVisitorsCountLabelLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._MonthVisitorsCountLabelLabel.ForeColor = System.Drawing.Color.White
        Me._MonthVisitorsCountLabelLabel.Location = New System.Drawing.Point(446, 23)
        Me._MonthVisitorsCountLabelLabel.Name = "_MonthVisitorsCountLabelLabel"
        Me._MonthVisitorsCountLabelLabel.Size = New System.Drawing.Size(64, 20)
        Me._MonthVisitorsCountLabelLabel.TabIndex = 3
        Me._MonthVisitorsCountLabelLabel.Text = "Monthly"
        '
        '_TodayVisitorsCountLabel
        '
        Me._TodayVisitorsCountLabel.AutoSize = True
        Me._TodayVisitorsCountLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._TodayVisitorsCountLabel.ForeColor = System.Drawing.Color.Azure
        Me._TodayVisitorsCountLabel.Location = New System.Drawing.Point(285, 61)
        Me._TodayVisitorsCountLabel.Name = "_TodayVisitorsCountLabel"
        Me._TodayVisitorsCountLabel.Size = New System.Drawing.Size(29, 16)
        Me._TodayVisitorsCountLabel.TabIndex = 2
        Me._TodayVisitorsCountLabel.Text = "568"
        '
        '_TodayVisitorsCountLabelLabel
        '
        Me._TodayVisitorsCountLabelLabel.AutoSize = True
        Me._TodayVisitorsCountLabelLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._TodayVisitorsCountLabelLabel.ForeColor = System.Drawing.Color.White
        Me._TodayVisitorsCountLabelLabel.Location = New System.Drawing.Point(274, 23)
        Me._TodayVisitorsCountLabelLabel.Name = "_TodayVisitorsCountLabelLabel"
        Me._TodayVisitorsCountLabelLabel.Size = New System.Drawing.Size(52, 20)
        Me._TodayVisitorsCountLabelLabel.TabIndex = 1
        Me._TodayVisitorsCountLabelLabel.Text = "Today"
        '
        '_VisitorsCountPanel
        '
        Me._VisitorsCountPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(30, Byte), Integer), CType(CType(60, Byte), Integer))
        Me._VisitorsCountPanel.Controls.Add(Me._VisitorsCountLabel)
        Me._VisitorsCountPanel.Controls.Add(Me._VisitorsCountLabelLabel)
        Me._VisitorsCountPanel.Location = New System.Drawing.Point(0, 0)
        Me._VisitorsCountPanel.Name = "_VisitorsCountPanel"
        Me._VisitorsCountPanel.Size = New System.Drawing.Size(200, 109)
        Me._VisitorsCountPanel.TabIndex = 0
        '
        '_VisitorsCountLabel
        '
        Me._VisitorsCountLabel.AutoSize = True
        Me._VisitorsCountLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._VisitorsCountLabel.ForeColor = System.Drawing.Color.Cyan
        Me._VisitorsCountLabel.Location = New System.Drawing.Point(67, 61)
        Me._VisitorsCountLabel.Name = "_VisitorsCountLabel"
        Me._VisitorsCountLabel.Size = New System.Drawing.Size(49, 20)
        Me._VisitorsCountLabel.TabIndex = 1
        Me._VisitorsCountLabel.Text = "5,274"
        '
        '_VisitorsCountLabelLabel
        '
        Me._VisitorsCountLabelLabel.AutoSize = True
        Me._VisitorsCountLabelLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._VisitorsCountLabelLabel.ForeColor = System.Drawing.Color.White
        Me._VisitorsCountLabelLabel.Location = New System.Drawing.Point(55, 20)
        Me._VisitorsCountLabelLabel.Name = "_VisitorsCountLabelLabel"
        Me._VisitorsCountLabelLabel.Size = New System.Drawing.Size(70, 24)
        Me._VisitorsCountLabelLabel.TabIndex = 0
        Me._VisitorsCountLabelLabel.Text = "Visitors"
        '
        '_AdminPanel
        '
        Me._AdminPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(130, Byte), Integer), CType(CType(40, Byte), Integer))
        Me._AdminPanel.Controls.Add(Me._AdminNameLabel)
        Me._AdminPanel.Controls.Add(Me._AdminLabelLabel)
        Me._AdminPanel.Controls.Add(Me._AdminIconPanel)
        Me._AdminPanel.Location = New System.Drawing.Point(251, 190)
        Me._AdminPanel.Name = "_AdminPanel"
        Me._AdminPanel.Size = New System.Drawing.Size(341, 130)
        Me._AdminPanel.TabIndex = 4
        '
        '_AdminNameLabel
        '
        Me._AdminNameLabel.AutoSize = True
        Me._AdminNameLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._AdminNameLabel.ForeColor = System.Drawing.Color.White
        Me._AdminNameLabel.Location = New System.Drawing.Point(195, 69)
        Me._AdminNameLabel.Name = "_AdminNameLabel"
        Me._AdminNameLabel.Size = New System.Drawing.Size(81, 16)
        Me._AdminNameLabel.TabIndex = 2
        Me._AdminNameLabel.Text = "Pritam Zope"
        '
        '_AdminLabelLabel
        '
        Me._AdminLabelLabel.AutoSize = True
        Me._AdminLabelLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._AdminLabelLabel.ForeColor = System.Drawing.Color.White
        Me._AdminLabelLabel.Location = New System.Drawing.Point(202, 19)
        Me._AdminLabelLabel.Name = "_AdminLabelLabel"
        Me._AdminLabelLabel.Size = New System.Drawing.Size(65, 24)
        Me._AdminLabelLabel.TabIndex = 1
        Me._AdminLabelLabel.Text = "Admin"
        '
        '_AdminIconPanel
        '
        Me._AdminIconPanel.BackgroundImage = Global.isr.Core.Tester.My.Resources.Resources.pritam12345
        Me._AdminIconPanel.Location = New System.Drawing.Point(0, 0)
        Me._AdminIconPanel.Name = "_AdminIconPanel"
        Me._AdminIconPanel.Size = New System.Drawing.Size(152, 130)
        Me._AdminIconPanel.TabIndex = 0
        '
        '_StoragePanel
        '
        Me._StoragePanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(200, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(30, Byte), Integer))
        Me._StoragePanel.Controls.Add(Me._TotalStorageLabel)
        Me._StoragePanel.Controls.Add(Me._TotalStorageLabelLabel)
        Me._StoragePanel.Controls.Add(Me._FreeStorageLabel)
        Me._StoragePanel.Controls.Add(Me._FreeStorageLabelLabel)
        Me._StoragePanel.Controls.Add(Me._UsedStorageLabel)
        Me._StoragePanel.Controls.Add(Me._UsedStorageLabelLabel)
        Me._StoragePanel.Controls.Add(Me._StorageTopPanel)
        Me._StoragePanel.Location = New System.Drawing.Point(251, 337)
        Me._StoragePanel.Name = "_StoragePanel"
        Me._StoragePanel.Size = New System.Drawing.Size(341, 151)
        Me._StoragePanel.TabIndex = 7
        '
        '_TotalStorageLabel
        '
        Me._TotalStorageLabel.AutoSize = True
        Me._TotalStorageLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._TotalStorageLabel.ForeColor = System.Drawing.Color.White
        Me._TotalStorageLabel.Location = New System.Drawing.Point(268, 103)
        Me._TotalStorageLabel.Name = "_TotalStorageLabel"
        Me._TotalStorageLabel.Size = New System.Drawing.Size(55, 20)
        Me._TotalStorageLabel.TabIndex = 6
        Me._TotalStorageLabel.Text = "50 GB"
        '
        '_TotalStorageLabelLabel
        '
        Me._TotalStorageLabelLabel.AutoSize = True
        Me._TotalStorageLabelLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._TotalStorageLabelLabel.ForeColor = System.Drawing.Color.White
        Me._TotalStorageLabelLabel.Location = New System.Drawing.Point(268, 64)
        Me._TotalStorageLabelLabel.Name = "_TotalStorageLabelLabel"
        Me._TotalStorageLabelLabel.Size = New System.Drawing.Size(46, 20)
        Me._TotalStorageLabelLabel.TabIndex = 5
        Me._TotalStorageLabelLabel.Text = "Total"
        '
        '_FreeStorageLabel
        '
        Me._FreeStorageLabel.AutoSize = True
        Me._FreeStorageLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._FreeStorageLabel.ForeColor = System.Drawing.Color.White
        Me._FreeStorageLabel.Location = New System.Drawing.Point(146, 103)
        Me._FreeStorageLabel.Name = "_FreeStorageLabel"
        Me._FreeStorageLabel.Size = New System.Drawing.Size(68, 20)
        Me._FreeStorageLabel.TabIndex = 4
        Me._FreeStorageLabel.Text = "26.5 GB"
        '
        '_FreeStorageLabelLabel
        '
        Me._FreeStorageLabelLabel.AutoSize = True
        Me._FreeStorageLabelLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._FreeStorageLabelLabel.ForeColor = System.Drawing.Color.White
        Me._FreeStorageLabelLabel.Location = New System.Drawing.Point(157, 64)
        Me._FreeStorageLabelLabel.Name = "_FreeStorageLabelLabel"
        Me._FreeStorageLabelLabel.Size = New System.Drawing.Size(43, 20)
        Me._FreeStorageLabelLabel.TabIndex = 3
        Me._FreeStorageLabelLabel.Text = "Free"
        '
        '_UsedStorageLabel
        '
        Me._UsedStorageLabel.AutoSize = True
        Me._UsedStorageLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._UsedStorageLabel.ForeColor = System.Drawing.Color.White
        Me._UsedStorageLabel.Location = New System.Drawing.Point(27, 103)
        Me._UsedStorageLabel.Name = "_UsedStorageLabel"
        Me._UsedStorageLabel.Size = New System.Drawing.Size(68, 20)
        Me._UsedStorageLabel.TabIndex = 2
        Me._UsedStorageLabel.Text = "23.5 GB"
        '
        '_UsedStorageLabelLabel
        '
        Me._UsedStorageLabelLabel.AutoSize = True
        Me._UsedStorageLabelLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._UsedStorageLabelLabel.ForeColor = System.Drawing.Color.White
        Me._UsedStorageLabelLabel.Location = New System.Drawing.Point(35, 64)
        Me._UsedStorageLabelLabel.Name = "_UsedStorageLabelLabel"
        Me._UsedStorageLabelLabel.Size = New System.Drawing.Size(48, 20)
        Me._UsedStorageLabelLabel.TabIndex = 1
        Me._UsedStorageLabelLabel.Text = "Used"
        '
        '_StorageTopPanel
        '
        Me._StorageTopPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me._StorageTopPanel.Controls.Add(Me._StoratgeTitleLabel)
        Me._StorageTopPanel.Location = New System.Drawing.Point(0, 0)
        Me._StorageTopPanel.Name = "_StorageTopPanel"
        Me._StorageTopPanel.Size = New System.Drawing.Size(341, 49)
        Me._StorageTopPanel.TabIndex = 0
        '
        '_StoratgeTitleLabel
        '
        Me._StoratgeTitleLabel.AutoSize = True
        Me._StoratgeTitleLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._StoratgeTitleLabel.ForeColor = System.Drawing.Color.White
        Me._StoratgeTitleLabel.Location = New System.Drawing.Point(119, 11)
        Me._StoratgeTitleLabel.Name = "_StoratgeTitleLabel"
        Me._StoratgeTitleLabel.Size = New System.Drawing.Size(81, 25)
        Me._StoratgeTitleLabel.TabIndex = 0
        Me._StoratgeTitleLabel.Text = "Storage"
        '
        '_ControlPanel
        '
        Me._ControlPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(100, Byte), Integer), CType(CType(30, Byte), Integer), CType(CType(120, Byte), Integer))
        Me._ControlPanel.Controls.Add(Me._ContrlPanelLabel)
        Me._ControlPanel.Location = New System.Drawing.Point(598, 385)
        Me._ControlPanel.Name = "_ControlPanel"
        Me._ControlPanel.Size = New System.Drawing.Size(240, 103)
        Me._ControlPanel.TabIndex = 8
        '
        '_ContrlPanelLabel
        '
        Me._ContrlPanelLabel.AutoSize = True
        Me._ContrlPanelLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ContrlPanelLabel.ForeColor = System.Drawing.Color.White
        Me._ContrlPanelLabel.Location = New System.Drawing.Point(48, 40)
        Me._ContrlPanelLabel.Name = "_ContrlPanelLabel"
        Me._ContrlPanelLabel.Size = New System.Drawing.Size(142, 25)
        Me._ContrlPanelLabel.TabIndex = 0
        Me._ContrlPanelLabel.Text = "Control Panel"
        '
        '_DeleteBlogButton
        '
        Me._DeleteBlogButton.BusyBackColor = System.Drawing.Color.FromArgb(CType(CType(180, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(60, Byte), Integer))
        Me._DeleteBlogButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me._DeleteBlogButton.DisplayText = "Delete Blog"
        Me._DeleteBlogButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me._DeleteBlogButton.Font = New System.Drawing.Font("Microsoft YaHei UI", 12.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._DeleteBlogButton.ForeColor = System.Drawing.Color.White
        Me._DeleteBlogButton.Location = New System.Drawing.Point(598, 260)
        Me._DeleteBlogButton.MouseClickColor = System.Drawing.Color.FromArgb(CType(CType(100, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(10, Byte), Integer))
        Me._DeleteBlogButton.MouseColorsEnabled = True
        Me._DeleteBlogButton.MouseHoverColor = System.Drawing.Color.FromArgb(CType(CType(150, Byte), Integer), CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer))
        Me._DeleteBlogButton.Name = "_DeleteBlogButton"
        Me._DeleteBlogButton.Size = New System.Drawing.Size(240, 60)
        Me._DeleteBlogButton.TabIndex = 6
        Me._DeleteBlogButton.Text = "Delete Blog"
        Me._DeleteBlogButton.TextLocationLeft = 65
        Me._DeleteBlogButton.TextLocationTop = 16
        Me._DeleteBlogButton.UseVisualStyleBackColor = True
        '
        '_ViewBlogButton
        '
        Me._ViewBlogButton.BusyBackColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(130, Byte), Integer), CType(CType(110, Byte), Integer))
        Me._ViewBlogButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me._ViewBlogButton.DisplayText = "View Blog"
        Me._ViewBlogButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me._ViewBlogButton.Font = New System.Drawing.Font("Microsoft YaHei UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ViewBlogButton.ForeColor = System.Drawing.Color.White
        Me._ViewBlogButton.Location = New System.Drawing.Point(599, 191)
        Me._ViewBlogButton.MouseClickColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(50, Byte), Integer))
        Me._ViewBlogButton.MouseColorsEnabled = True
        Me._ViewBlogButton.MouseHoverColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(90, Byte), Integer))
        Me._ViewBlogButton.Name = "_ViewBlogButton"
        Me._ViewBlogButton.Size = New System.Drawing.Size(240, 60)
        Me._ViewBlogButton.TabIndex = 5
        Me._ViewBlogButton.Text = "View Blog"
        Me._ViewBlogButton.TextLocationLeft = 65
        Me._ViewBlogButton.TextLocationTop = 16
        Me._ViewBlogButton.UseVisualStyleBackColor = True
        '
        '_CampainsPanel
        '
        Me._CampainsPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(80, Byte), Integer))
        Me._CampainsPanel.Controls.Add(Me._CampaignsLabel)
        Me._CampainsPanel.Location = New System.Drawing.Point(599, 337)
        Me._CampainsPanel.Name = "_CampainsPanel"
        Me._CampainsPanel.Size = New System.Drawing.Size(239, 42)
        Me._CampainsPanel.TabIndex = 9
        '
        '_CampaignsLabel
        '
        Me._CampaignsLabel.AutoSize = True
        Me._CampaignsLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._CampaignsLabel.ForeColor = System.Drawing.Color.White
        Me._CampaignsLabel.Location = New System.Drawing.Point(73, 11)
        Me._CampaignsLabel.Name = "_CampaignsLabel"
        Me._CampaignsLabel.Size = New System.Drawing.Size(89, 20)
        Me._CampaignsLabel.TabIndex = 0
        Me._CampaignsLabel.Text = "Campaigns"
        '
        'DashboardForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Gainsboro
        Me.ClientSize = New System.Drawing.Size(873, 515)
        Me.Controls.Add(Me._CampainsPanel)
        Me.Controls.Add(Me._ControlPanel)
        Me.Controls.Add(Me._StoragePanel)
        Me.Controls.Add(Me._DeleteBlogButton)
        Me.Controls.Add(Me._ViewBlogButton)
        Me.Controls.Add(Me._AdminPanel)
        Me.Controls.Add(Me._VisitorsPanel)
        Me.Controls.Add(Me._LeftPanel)
        Me.Controls.Add(Me._TopPanel)
        Me.Controls.Add(Me._LeftTopPanel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "DashboardForm"
        Me.Text = "Dashboard"
        Me._LeftTopPanel.ResumeLayout(False)
        Me._LeftTopPanel.PerformLayout()
        Me._TopPanel.ResumeLayout(False)
        Me._TopPanel.PerformLayout()
        Me._LeftPanel.ResumeLayout(False)
        Me._LeftBottomPanel.ResumeLayout(False)
        Me._LeftBottomPanel.PerformLayout()
        Me._VisitorsPanel.ResumeLayout(False)
        Me._VisitorsPanel.PerformLayout()
        Me._VisitorsCountPanel.ResumeLayout(False)
        Me._VisitorsCountPanel.PerformLayout()
        Me._AdminPanel.ResumeLayout(False)
        Me._AdminPanel.PerformLayout()
        Me._StoragePanel.ResumeLayout(False)
        Me._StoragePanel.PerformLayout()
        Me._StorageTopPanel.ResumeLayout(False)
        Me._StorageTopPanel.PerformLayout()
        Me._ControlPanel.ResumeLayout(False)
        Me._ControlPanel.PerformLayout()
        Me._CampainsPanel.ResumeLayout(False)
        Me._CampainsPanel.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private _LeftTopPanel As System.Windows.Forms.Panel
    Private WithEvents _TopPanel As System.Windows.Forms.Panel
    Private WithEvents _CloseButton As ZopeButton
    Private WithEvents _MinButton As ZopeButton
    Private _LeftTopPanelLabel As System.Windows.Forms.Label
    Private _IconPanel As System.Windows.Forms.Panel
    Private WithEvents _DashboardLabel As System.Windows.Forms.Label
    Private _LeftPanel As System.Windows.Forms.Panel
    Private WithEvents _DashboardButton As ZopeButton
    Private WithEvents _StatsButton As ZopeButton
    Private WithEvents _LayoutButton As ZopeButton
    Private WithEvents _PagesButton As ZopeButton
    Private WithEvents _SettingsButton As ZopeButton
    Private WithEvents _ThemeButton As ZopeButton
    Private _LeftBottomPanel As System.Windows.Forms.Panel
    Private _LeftBottomPanelLabel As System.Windows.Forms.Label
    Private _VisitorsPanel As System.Windows.Forms.Panel
    Private _VisitorsCountPanel As System.Windows.Forms.Panel
    Private _VisitorsCountLabel As System.Windows.Forms.Label
    Private _VisitorsCountLabelLabel As System.Windows.Forms.Label
    Private _MonthVisitorsCountLabel As System.Windows.Forms.Label
    Private _MonthVisitorsCountLabelLabel As System.Windows.Forms.Label
    Private _TodayVisitorsCountLabel As System.Windows.Forms.Label
    Private _TodayVisitorsCountLabelLabel As System.Windows.Forms.Label
    Private _AdminPanel As System.Windows.Forms.Panel
    Private _AdminNameLabel As System.Windows.Forms.Label
    Private _AdminLabelLabel As System.Windows.Forms.Label
    Private _AdminIconPanel As System.Windows.Forms.Panel
    Private _ViewBlogButton As ZopeButton
    Private _DeleteBlogButton As ZopeButton
    Private _StoragePanel As System.Windows.Forms.Panel
    Private _TotalStorageLabel As System.Windows.Forms.Label
    Private _TotalStorageLabelLabel As System.Windows.Forms.Label
    Private _FreeStorageLabel As System.Windows.Forms.Label
    Private _FreeStorageLabelLabel As System.Windows.Forms.Label
    Private _UsedStorageLabel As System.Windows.Forms.Label
    Private _UsedStorageLabelLabel As System.Windows.Forms.Label
    Private _StorageTopPanel As System.Windows.Forms.Panel
    Private _StoratgeTitleLabel As System.Windows.Forms.Label
    Private _ControlPanel As System.Windows.Forms.Panel
    Private _ContrlPanelLabel As System.Windows.Forms.Label
    Private _CampainsPanel As System.Windows.Forms.Panel
    Private _CampaignsLabel As System.Windows.Forms.Label


End Class
