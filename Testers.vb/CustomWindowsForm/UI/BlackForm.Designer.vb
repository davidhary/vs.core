﻿Imports isr.Core.Controls
Partial Public Class BlackForm
    ''' <summary>
    ''' Required designer variable.
    ''' </summary>
    Private components As System.ComponentModel.IContainer = Nothing

    ''' <summary>
    ''' Clean up any resources being used.
    ''' </summary>
    ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso (components IsNot Nothing) Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

#Region "Windows Form Designer generated code"

    ''' <summary>
    ''' Required method for Designer support - do not modify
    ''' the contents of this method with the code editor.
    ''' </summary>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me._TopBorderPanel = New System.Windows.Forms.Panel()
        Me._RightBorderPanel = New System.Windows.Forms.Panel()
        Me._LeftBorderPanel = New System.Windows.Forms.Panel()
        Me._BottomBorderPanel = New System.Windows.Forms.Panel()
        Me._TopPanel = New System.Windows.Forms.Panel()
        Me._MinButton = New ZopeButton()
        Me._MaxButton = New MinMaxButton()
        Me._WindowTextLabel = New System.Windows.Forms.Label()
        Me._CloseButton = New ZopeButton()
        Me._MenuStrip = New ZopeMenuStrip()
        Me._FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._NewToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._OpenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me._SaveToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SaveAsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me._CloseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._CloseAllToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me._PrintToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._PrintPreviewToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me._CloseToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me._EditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._CutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._CopyToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._PasteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator()
        Me._UnduToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._RedoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator()
        Me._FindToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ReplaceToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SelectAllToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._HelpContentsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._OnlineHelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._BuyNowShapedButton = New ShapedButton()
        Me._BottomBorderRightPanel = New System.Windows.Forms.Panel()
        Me._BottomPanel = New System.Windows.Forms.Panel()
        Me._TimesShapedButton = New ShapedButton()
        Me._TestShapedButton = New ShapedButton()
        Me._AddShapedButton = New ShapedButton()
        Me._ToolsShapedButton = New ShapedButton()
        Me._ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._RightBorderBottomPanel = New System.Windows.Forms.Panel()
        Me._BottomBorderLeftPanel = New System.Windows.Forms.Panel()
        Me._LeftBorderBottomPanel = New System.Windows.Forms.Panel()
        Me._RightBorderTopPanel = New System.Windows.Forms.Panel()
        Me._TopBorderRightPanel = New System.Windows.Forms.Panel()
        Me._TopBorderLeftPanel = New System.Windows.Forms.Panel()
        Me._LeftBorderTopPanel = New System.Windows.Forms.Panel()
        Me._LeftPanel = New System.Windows.Forms.Panel()
        Me._HelpButton = New ZopeButton()
        Me._RunButton = New ZopeButton()
        Me._ViewButton = New ZopeButton()
        Me._EditButton = New ZopeButton()
        Me._FileButton = New ZopeButton()
        Me._SeparatorPanel = New System.Windows.Forms.Panel()
        Me._InformationLabel = New System.Windows.Forms.Label()
        Me._TopPanel.SuspendLayout()
        Me._MenuStrip.SuspendLayout()
        Me._BottomPanel.SuspendLayout()
        Me._LeftPanel.SuspendLayout()
        Me.SuspendLayout()
        '
        '_TopBorderPanel
        '
        Me._TopBorderPanel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._TopBorderPanel.BackColor = System.Drawing.Color.Black
        Me._TopBorderPanel.Cursor = System.Windows.Forms.Cursors.SizeNS
        Me._TopBorderPanel.Location = New System.Drawing.Point(20, 0)
        Me._TopBorderPanel.Name = "_TopBorderPanel"
        Me._TopBorderPanel.Size = New System.Drawing.Size(690, 2)
        Me._TopBorderPanel.TabIndex = 0
        '
        '_RightBorderPanel
        '
        Me._RightBorderPanel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._RightBorderPanel.BackColor = System.Drawing.Color.Black
        Me._RightBorderPanel.Cursor = System.Windows.Forms.Cursors.SizeWE
        Me._RightBorderPanel.Location = New System.Drawing.Point(728, 22)
        Me._RightBorderPanel.Name = "_RightBorderPanel"
        Me._RightBorderPanel.Size = New System.Drawing.Size(2, 430)
        Me._RightBorderPanel.TabIndex = 1
        '
        '_LeftBorderPanel
        '
        Me._LeftBorderPanel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
        Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._LeftBorderPanel.BackColor = System.Drawing.Color.Black
        Me._LeftBorderPanel.Cursor = System.Windows.Forms.Cursors.SizeWE
        Me._LeftBorderPanel.Location = New System.Drawing.Point(0, 20)
        Me._LeftBorderPanel.Name = "_LeftBorderPanel"
        Me._LeftBorderPanel.Size = New System.Drawing.Size(2, 430)
        Me._LeftBorderPanel.TabIndex = 2
        '
        '_BottomBorderPanel
        '
        Me._BottomBorderPanel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._BottomBorderPanel.BackColor = System.Drawing.Color.Black
        Me._BottomBorderPanel.Cursor = System.Windows.Forms.Cursors.SizeNS
        Me._BottomBorderPanel.Location = New System.Drawing.Point(15, 471)
        Me._BottomBorderPanel.Name = "_BottomBorderPanel"
        Me._BottomBorderPanel.Size = New System.Drawing.Size(692, 2)
        Me._BottomBorderPanel.TabIndex = 3
        '
        '_TopPanel
        '
        Me._TopPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer))
        Me._TopPanel.Controls.Add(Me._MinButton)
        Me._TopPanel.Controls.Add(Me._MaxButton)
        Me._TopPanel.Controls.Add(Me._WindowTextLabel)
        Me._TopPanel.Controls.Add(Me._CloseButton)
        Me._TopPanel.Controls.Add(Me._MenuStrip)
        Me._TopPanel.Dock = System.Windows.Forms.DockStyle.Top
        Me._TopPanel.Location = New System.Drawing.Point(0, 0)
        Me._TopPanel.Name = "_TopPanel"
        Me._TopPanel.Size = New System.Drawing.Size(730, 76)
        Me._TopPanel.TabIndex = 4
        '
        '_MinButton
        '
        Me._MinButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._MinButton.BusyBackColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer))
        Me._MinButton.DisplayText = "_"
        Me._MinButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me._MinButton.Font = New System.Drawing.Font("Microsoft YaHei UI", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._MinButton.ForeColor = System.Drawing.Color.White
        Me._MinButton.Location = New System.Drawing.Point(632, 6)
        Me._MinButton.MouseClickColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(160, Byte), Integer))
        Me._MinButton.MouseColorsEnabled = True
        Me._MinButton.MouseHoverColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
        Me._MinButton.Name = "_MinButton"
        Me._MinButton.Size = New System.Drawing.Size(31, 24)
        Me._MinButton.TabIndex = 4
        Me._MinButton.Text = "_"
        Me._MinButton.TextLocationLeft = 6
        Me._MinButton.TextLocationTop = -20
        Me._ToolTip.SetToolTip(Me._MinButton, "Minimize")
        Me._MinButton.UseVisualStyleBackColor = True
        '
        '_MaxButton
        '
        Me._MaxButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._MaxButton.BusyBackColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer))
        Me._MaxButton.CustomFormState = CustomFormState.Normal
        Me._MaxButton.DisplayText = "_"
        Me._MaxButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me._MaxButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._MaxButton.ForeColor = System.Drawing.Color.White
        Me._MaxButton.Location = New System.Drawing.Point(663, 6)
        Me._MaxButton.MouseClickColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(160, Byte), Integer))
        Me._MaxButton.MouseHoverColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
        Me._MaxButton.Name = "_MaxButton"
        Me._MaxButton.Size = New System.Drawing.Size(31, 24)
        Me._MaxButton.TabIndex = 2
        Me._MaxButton.Text = "minMaxButton1"
        Me._MaxButton.TextLocation = New Point(8, 6)
        Me._ToolTip.SetToolTip(Me._MaxButton, "Maximize")
        Me._MaxButton.UseVisualStyleBackColor = True
        '
        '_WindowTextLabel
        '
        Me._WindowTextLabel.AutoSize = True
        Me._WindowTextLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 26.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._WindowTextLabel.ForeColor = System.Drawing.Color.White
        Me._WindowTextLabel.Location = New System.Drawing.Point(28, 21)
        Me._WindowTextLabel.Name = "_WindowTextLabel"
        Me._WindowTextLabel.Size = New System.Drawing.Size(134, 39)
        Me._WindowTextLabel.TabIndex = 1
        Me._WindowTextLabel.Text = "My App"
        '
        '_CloseButton
        '
        Me._CloseButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._CloseButton.BusyBackColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer))
        Me._CloseButton.DisplayText = "X"
        Me._CloseButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me._CloseButton.Font = New System.Drawing.Font("Microsoft YaHei UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._CloseButton.ForeColor = System.Drawing.Color.White
        Me._CloseButton.Location = New System.Drawing.Point(694, 6)
        Me._CloseButton.MouseClickColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(160, Byte), Integer))
        Me._CloseButton.MouseColorsEnabled = True
        Me._CloseButton.MouseHoverColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
        Me._CloseButton.Name = "_CloseButton"
        Me._CloseButton.Size = New System.Drawing.Size(31, 24)
        Me._CloseButton.TabIndex = 0
        Me._CloseButton.Text = "X"
        Me._CloseButton.TextLocationLeft = 6
        Me._CloseButton.TextLocationTop = 1
        Me._ToolTip.SetToolTip(Me._CloseButton, "Close")
        Me._CloseButton.UseVisualStyleBackColor = True
        '
        '_MenuStrip
        '
        Me._MenuStrip.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._MenuStrip.Dock = System.Windows.Forms.DockStyle.None
        Me._MenuStrip.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._MenuStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._FileToolStripMenuItem, Me._EditToolStripMenuItem, Me._HelpToolStripMenuItem})
        Me._MenuStrip.Location = New System.Drawing.Point(421, 32)
        Me._MenuStrip.Name = "_MenuStrip"
        Me._MenuStrip.Size = New System.Drawing.Size(200, 28)
        Me._MenuStrip.TabIndex = 19
        Me._MenuStrip.Text = "menuStripZ1"
        '
        '_FileToolStripMenuItem
        '
        Me._FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._NewToolStripMenuItem, Me._OpenToolStripMenuItem, Me._ToolStripSeparator1, Me._SaveToolStripMenuItem, Me._SaveAsToolStripMenuItem, Me._ToolStripSeparator2, Me._CloseToolStripMenuItem, Me._CloseAllToolStripMenuItem, Me._ToolStripSeparator3, Me._PrintToolStripMenuItem, Me._PrintPreviewToolStripMenuItem, Me._ToolStripSeparator4, Me._CloseToolStripMenuItem1})
        Me._FileToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me._FileToolStripMenuItem.Name = "_FileToolStripMenuItem"
        Me._FileToolStripMenuItem.Size = New System.Drawing.Size(60, 24)
        Me._FileToolStripMenuItem.Text = "  File  "
        '
        '_NewToolStripMenuItem
        '
        Me._NewToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me._NewToolStripMenuItem.Name = "_NewToolStripMenuItem"
        Me._NewToolStripMenuItem.Size = New System.Drawing.Size(240, 24)
        Me._NewToolStripMenuItem.Text = "New                                 "
        '
        '_OpenToolStripMenuItem
        '
        Me._OpenToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me._OpenToolStripMenuItem.Name = "_OpenToolStripMenuItem"
        Me._OpenToolStripMenuItem.Size = New System.Drawing.Size(240, 24)
        Me._OpenToolStripMenuItem.Text = "Open"
        '
        '_ToolStripSeparator1
        '
        Me._ToolStripSeparator1.Name = "_ToolStripSeparator1"
        Me._ToolStripSeparator1.Size = New System.Drawing.Size(237, 6)
        '
        '_SaveToolStripMenuItem
        '
        Me._SaveToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me._SaveToolStripMenuItem.Name = "_SaveToolStripMenuItem"
        Me._SaveToolStripMenuItem.Size = New System.Drawing.Size(240, 24)
        Me._SaveToolStripMenuItem.Text = "Save"
        '
        '_SaveAsToolStripMenuItem
        '
        Me._SaveAsToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me._SaveAsToolStripMenuItem.Name = "_SaveAsToolStripMenuItem"
        Me._SaveAsToolStripMenuItem.Size = New System.Drawing.Size(240, 24)
        Me._SaveAsToolStripMenuItem.Text = "Save As"
        '
        '_ToolStripSeparator2
        '
        Me._ToolStripSeparator2.Name = "_ToolStripSeparator2"
        Me._ToolStripSeparator2.Size = New System.Drawing.Size(237, 6)
        '
        '_CloseToolStripMenuItem
        '
        Me._CloseToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me._CloseToolStripMenuItem.Name = "_CloseToolStripMenuItem"
        Me._CloseToolStripMenuItem.Size = New System.Drawing.Size(240, 24)
        Me._CloseToolStripMenuItem.Text = "Close"
        '
        '_CloseAllToolStripMenuItem
        '
        Me._CloseAllToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me._CloseAllToolStripMenuItem.Name = "_CloseAllToolStripMenuItem"
        Me._CloseAllToolStripMenuItem.Size = New System.Drawing.Size(240, 24)
        Me._CloseAllToolStripMenuItem.Text = "Close All"
        '
        '_ToolStripSeparator3
        '
        Me._ToolStripSeparator3.Name = "_ToolStripSeparator3"
        Me._ToolStripSeparator3.Size = New System.Drawing.Size(237, 6)
        '
        '_PrintToolStripMenuItem
        '
        Me._PrintToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me._PrintToolStripMenuItem.Name = "_PrintToolStripMenuItem"
        Me._PrintToolStripMenuItem.Size = New System.Drawing.Size(240, 24)
        Me._PrintToolStripMenuItem.Text = "Print"
        '
        '_PrintPreviewToolStripMenuItem
        '
        Me._PrintPreviewToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me._PrintPreviewToolStripMenuItem.Name = "_PrintPreviewToolStripMenuItem"
        Me._PrintPreviewToolStripMenuItem.Size = New System.Drawing.Size(240, 24)
        Me._PrintPreviewToolStripMenuItem.Text = "Print Preview"
        '
        '_ToolStripSeparator4
        '
        Me._ToolStripSeparator4.Name = "_ToolStripSeparator4"
        Me._ToolStripSeparator4.Size = New System.Drawing.Size(237, 6)
        '
        '_CloseToolStripMenuItem1
        '
        Me._CloseToolStripMenuItem1.ForeColor = System.Drawing.Color.White
        Me._CloseToolStripMenuItem1.Name = "_CloseToolStripMenuItem1"
        Me._CloseToolStripMenuItem1.Size = New System.Drawing.Size(240, 24)
        Me._CloseToolStripMenuItem1.Text = "Close"
        '
        '_EditToolStripMenuItem
        '
        Me._EditToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._CutToolStripMenuItem, Me._CopyToolStripMenuItem, Me._PasteToolStripMenuItem, Me._ToolStripSeparator5, Me._UnduToolStripMenuItem, Me._RedoToolStripMenuItem, Me._ToolStripSeparator6, Me._FindToolStripMenuItem, Me._ReplaceToolStripMenuItem, Me._SelectAllToolStripMenuItem})
        Me._EditToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me._EditToolStripMenuItem.Name = "_EditToolStripMenuItem"
        Me._EditToolStripMenuItem.Size = New System.Drawing.Size(63, 24)
        Me._EditToolStripMenuItem.Text = "  Edit  "
        '
        '_CutToolStripMenuItem
        '
        Me._CutToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me._CutToolStripMenuItem.Name = "_CutToolStripMenuItem"
        Me._CutToolStripMenuItem.Size = New System.Drawing.Size(216, 24)
        Me._CutToolStripMenuItem.Text = "Cut                             "
        '
        '_CopyToolStripMenuItem
        '
        Me._CopyToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me._CopyToolStripMenuItem.Name = "_CopyToolStripMenuItem"
        Me._CopyToolStripMenuItem.Size = New System.Drawing.Size(216, 24)
        Me._CopyToolStripMenuItem.Text = "Copy"
        '
        '_PasteToolStripMenuItem
        '
        Me._PasteToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me._PasteToolStripMenuItem.Name = "_PasteToolStripMenuItem"
        Me._PasteToolStripMenuItem.Size = New System.Drawing.Size(216, 24)
        Me._PasteToolStripMenuItem.Text = "Paste"
        '
        '_ToolStripSeparator5
        '
        Me._ToolStripSeparator5.Name = "_ToolStripSeparator5"
        Me._ToolStripSeparator5.Size = New System.Drawing.Size(213, 6)
        '
        '_UnduToolStripMenuItem
        '
        Me._UnduToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me._UnduToolStripMenuItem.Name = "_UnduToolStripMenuItem"
        Me._UnduToolStripMenuItem.Size = New System.Drawing.Size(216, 24)
        Me._UnduToolStripMenuItem.Text = "Undo"
        '
        '_RedoToolStripMenuItem
        '
        Me._RedoToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me._RedoToolStripMenuItem.Name = "_RedoToolStripMenuItem"
        Me._RedoToolStripMenuItem.Size = New System.Drawing.Size(216, 24)
        Me._RedoToolStripMenuItem.Text = "Redo"
        '
        '_ToolStripSeparator6
        '
        Me._ToolStripSeparator6.Name = "_ToolStripSeparator6"
        Me._ToolStripSeparator6.Size = New System.Drawing.Size(213, 6)
        '
        '_FindToolStripMenuItem
        '
        Me._FindToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me._FindToolStripMenuItem.Name = "_FindToolStripMenuItem"
        Me._FindToolStripMenuItem.Size = New System.Drawing.Size(216, 24)
        Me._FindToolStripMenuItem.Text = "Find"
        '
        '_ReplaceToolStripMenuItem
        '
        Me._ReplaceToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me._ReplaceToolStripMenuItem.Name = "_ReplaceToolStripMenuItem"
        Me._ReplaceToolStripMenuItem.Size = New System.Drawing.Size(216, 24)
        Me._ReplaceToolStripMenuItem.Text = "Replace"
        '
        '_SelectAllToolStripMenuItem
        '
        Me._SelectAllToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me._SelectAllToolStripMenuItem.Name = "_SelectAllToolStripMenuItem"
        Me._SelectAllToolStripMenuItem.Size = New System.Drawing.Size(216, 24)
        Me._SelectAllToolStripMenuItem.Text = "Select All"
        '
        '_HelpToolStripMenuItem
        '
        Me._HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._HelpContentsToolStripMenuItem, Me._OnlineHelpToolStripMenuItem, Me._AboutToolStripMenuItem})
        Me._HelpToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me._HelpToolStripMenuItem.Name = "_HelpToolStripMenuItem"
        Me._HelpToolStripMenuItem.Size = New System.Drawing.Size(69, 24)
        Me._HelpToolStripMenuItem.Text = "  Help  "
        '
        '_HelpContentsToolStripMenuItem
        '
        Me._HelpContentsToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me._HelpContentsToolStripMenuItem.Name = "_HelpContentsToolStripMenuItem"
        Me._HelpContentsToolStripMenuItem.Size = New System.Drawing.Size(172, 24)
        Me._HelpContentsToolStripMenuItem.Text = "Help Contents"
        '
        '_OnlineHelpToolStripMenuItem
        '
        Me._OnlineHelpToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me._OnlineHelpToolStripMenuItem.Name = "_OnlineHelpToolStripMenuItem"
        Me._OnlineHelpToolStripMenuItem.Size = New System.Drawing.Size(172, 24)
        Me._OnlineHelpToolStripMenuItem.Text = "Online Help"
        '
        '_AboutToolStripMenuItem
        '
        Me._AboutToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me._AboutToolStripMenuItem.Name = "_AboutToolStripMenuItem"
        Me._AboutToolStripMenuItem.Size = New System.Drawing.Size(172, 24)
        Me._AboutToolStripMenuItem.Text = "About"
        '
        '_BuyNowShapedButton
        '
        Me._BuyNowShapedButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._BuyNowShapedButton.BackColor = System.Drawing.Color.Transparent
        Me._BuyNowShapedButton.BorderColor = System.Drawing.Color.Transparent
        Me._BuyNowShapedButton.BorderWidth = 2
        Me._BuyNowShapedButton.ButtonShape = ButtonShape.RoundRect
        Me._BuyNowShapedButton.ButtonText = "Buy Now"
        Me._BuyNowShapedButton.EndColor = System.Drawing.Color.FromArgb(CType(CType(20, Byte), Integer), CType(CType(20, Byte), Integer), CType(CType(40, Byte), Integer))
        Me._BuyNowShapedButton.EndOpacity = 250
        Me._BuyNowShapedButton.FlatAppearance.BorderSize = 0
        Me._BuyNowShapedButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me._BuyNowShapedButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me._BuyNowShapedButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me._BuyNowShapedButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._BuyNowShapedButton.ForeColor = System.Drawing.Color.White
        Me._BuyNowShapedButton.GradientAngle = 90
        Me._BuyNowShapedButton.Location = New System.Drawing.Point(427, 10)
        Me._BuyNowShapedButton.MouseClickStartColor = System.Drawing.Color.Black
        Me._BuyNowShapedButton.MouseClickEndColor = System.Drawing.Color.Black
        Me._BuyNowShapedButton.MouseHoverStartColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(30, Byte), Integer))
        Me._BuyNowShapedButton.MouseHoverEndColor = System.Drawing.Color.FromArgb(CType(CType(20, Byte), Integer), CType(CType(20, Byte), Integer), CType(CType(80, Byte), Integer))
        Me._BuyNowShapedButton.Name = "_BuyNowShapedButton"
        Me._BuyNowShapedButton.ShowButtontext = True
        Me._BuyNowShapedButton.Size = New System.Drawing.Size(145, 54)
        Me._BuyNowShapedButton.StartColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(190, Byte), Integer))
        Me._BuyNowShapedButton.StartOpacity = 250
        Me._BuyNowShapedButton.TabIndex = 3
        Me._BuyNowShapedButton.Text = "shapedButton2"
        Me._BuyNowShapedButton.TextLocation = New Point(40, 18)
        Me._BuyNowShapedButton.UseVisualStyleBackColor = False
        '
        '_BottomBorderRightPanel
        '
        Me._BottomBorderRightPanel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._BottomBorderRightPanel.BackColor = System.Drawing.Color.Black
        Me._BottomBorderRightPanel.Cursor = System.Windows.Forms.Cursors.SizeNWSE
        Me._BottomBorderRightPanel.Location = New System.Drawing.Point(710, 471)
        Me._BottomBorderRightPanel.Name = "_BottomBorderRightPanel"
        Me._BottomBorderRightPanel.Size = New System.Drawing.Size(19, 2)
        Me._BottomBorderRightPanel.TabIndex = 5
        '
        '_BottomPanel
        '
        Me._BottomPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer))
        Me._BottomPanel.Controls.Add(Me._TimesShapedButton)
        Me._BottomPanel.Controls.Add(Me._BuyNowShapedButton)
        Me._BottomPanel.Controls.Add(Me._TestShapedButton)
        Me._BottomPanel.Controls.Add(Me._AddShapedButton)
        Me._BottomPanel.Controls.Add(Me._ToolsShapedButton)
        Me._BottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._BottomPanel.Location = New System.Drawing.Point(0, 402)
        Me._BottomPanel.Name = "_BottomPanel"
        Me._BottomPanel.Size = New System.Drawing.Size(730, 71)
        Me._BottomPanel.TabIndex = 8
        '
        '_TimesShapedButton
        '
        Me._TimesShapedButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._TimesShapedButton.BackColor = System.Drawing.Color.Transparent
        Me._TimesShapedButton.BorderColor = System.Drawing.Color.Transparent
        Me._TimesShapedButton.BorderWidth = 2
        Me._TimesShapedButton.ButtonShape = ButtonShape.Circle
        Me._TimesShapedButton.ButtonText = "X"
        Me._TimesShapedButton.EndColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer))
        Me._TimesShapedButton.EndOpacity = 250
        Me._TimesShapedButton.FlatAppearance.BorderSize = 0
        Me._TimesShapedButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me._TimesShapedButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me._TimesShapedButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me._TimesShapedButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._TimesShapedButton.ForeColor = System.Drawing.Color.White
        Me._TimesShapedButton.GradientAngle = 90
        Me._TimesShapedButton.Location = New System.Drawing.Point(643, 8)
        Me._TimesShapedButton.MouseClickStartColor = System.Drawing.Color.Black
        Me._TimesShapedButton.MouseClickEndColor = System.Drawing.Color.Black
        Me._TimesShapedButton.MouseHoverStartColor = System.Drawing.Color.FromArgb(CType(CType(150, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me._TimesShapedButton.MouseHoverEndColor = System.Drawing.Color.FromArgb(CType(CType(150, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me._TimesShapedButton.Name = "_TimesShapedButton"
        Me._TimesShapedButton.ShowButtontext = True
        Me._TimesShapedButton.Size = New System.Drawing.Size(75, 55)
        Me._TimesShapedButton.StartColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer))
        Me._TimesShapedButton.StartOpacity = 250
        Me._TimesShapedButton.TabIndex = 9
        Me._TimesShapedButton.TextLocation = New Point(28, 16)
        Me._TimesShapedButton.UseVisualStyleBackColor = False
        '
        '_TestShapedButton
        '
        Me._TestShapedButton.BackColor = System.Drawing.Color.Transparent
        Me._TestShapedButton.BorderColor = System.Drawing.Color.Transparent
        Me._TestShapedButton.BorderWidth = 2
        Me._TestShapedButton.ButtonShape = ButtonShape.RoundRect
        Me._TestShapedButton.ButtonText = "Test"
        Me._TestShapedButton.EndColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer))
        Me._TestShapedButton.EndOpacity = 250
        Me._TestShapedButton.FlatAppearance.BorderSize = 0
        Me._TestShapedButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me._TestShapedButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me._TestShapedButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me._TestShapedButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._TestShapedButton.ForeColor = System.Drawing.Color.White
        Me._TestShapedButton.GradientAngle = 90
        Me._TestShapedButton.Location = New System.Drawing.Point(257, 10)
        Me._TestShapedButton.MouseClickStartColor = System.Drawing.Color.Black
        Me._TestShapedButton.MouseClickEndColor = System.Drawing.Color.Black
        Me._TestShapedButton.MouseHoverStartColor = System.Drawing.Color.FromArgb(CType(CType(80, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(80, Byte), Integer))
        Me._TestShapedButton.MouseHoverEndColor = System.Drawing.Color.FromArgb(CType(CType(80, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(80, Byte), Integer))
        Me._TestShapedButton.Name = "_TestShapedButton"
        Me._TestShapedButton.ShowButtontext = True
        Me._TestShapedButton.Size = New System.Drawing.Size(136, 55)
        Me._TestShapedButton.StartColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer))
        Me._TestShapedButton.StartOpacity = 250
        Me._TestShapedButton.TabIndex = 8
        Me._TestShapedButton.TextLocation = New Point(46, 18)
        Me._TestShapedButton.UseVisualStyleBackColor = False
        '
        '_AddShapedButton
        '
        Me._AddShapedButton.BackColor = System.Drawing.Color.Transparent
        Me._AddShapedButton.BorderColor = System.Drawing.Color.Transparent
        Me._AddShapedButton.BorderWidth = 2
        Me._AddShapedButton.ButtonShape = ButtonShape.Circle
        Me._AddShapedButton.ButtonText = "+"
        Me._AddShapedButton.EndColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer))
        Me._AddShapedButton.EndOpacity = 250
        Me._AddShapedButton.FlatAppearance.BorderSize = 0
        Me._AddShapedButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me._AddShapedButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me._AddShapedButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me._AddShapedButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._AddShapedButton.ForeColor = System.Drawing.Color.White
        Me._AddShapedButton.GradientAngle = 90
        Me._AddShapedButton.Location = New System.Drawing.Point(10, 10)
        Me._AddShapedButton.MouseClickStartColor = System.Drawing.Color.Black
        Me._AddShapedButton.MouseClickEndColor = System.Drawing.Color.Black
        Me._AddShapedButton.MouseHoverStartColor = System.Drawing.Color.FromArgb(CType(CType(80, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(80, Byte), Integer))
        Me._AddShapedButton.MouseHoverEndColor = System.Drawing.Color.FromArgb(CType(CType(80, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(80, Byte), Integer))
        Me._AddShapedButton.Name = "_AddShapedButton"
        Me._AddShapedButton.ShowButtontext = True
        Me._AddShapedButton.Size = New System.Drawing.Size(59, 55)
        Me._AddShapedButton.StartColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer))
        Me._AddShapedButton.StartOpacity = 250
        Me._AddShapedButton.TabIndex = 6
        Me._AddShapedButton.TextLocation = New Point(13, 6)
        Me._AddShapedButton.UseVisualStyleBackColor = False
        '
        '_ToolsShapedButton
        '
        Me._ToolsShapedButton.BackColor = System.Drawing.Color.Transparent
        Me._ToolsShapedButton.BorderColor = System.Drawing.Color.Transparent
        Me._ToolsShapedButton.BorderWidth = 2
        Me._ToolsShapedButton.ButtonShape = ButtonShape.RoundRect
        Me._ToolsShapedButton.ButtonText = "Tools"
        Me._ToolsShapedButton.EndColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer))
        Me._ToolsShapedButton.EndOpacity = 250
        Me._ToolsShapedButton.FlatAppearance.BorderSize = 0
        Me._ToolsShapedButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me._ToolsShapedButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me._ToolsShapedButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me._ToolsShapedButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ToolsShapedButton.ForeColor = System.Drawing.Color.White
        Me._ToolsShapedButton.GradientAngle = 90
        Me._ToolsShapedButton.Location = New System.Drawing.Point(96, 10)
        Me._ToolsShapedButton.MouseClickStartColor = System.Drawing.Color.Black
        Me._ToolsShapedButton.MouseClickEndColor = System.Drawing.Color.Black
        Me._ToolsShapedButton.MouseHoverStartColor = System.Drawing.Color.FromArgb(CType(CType(80, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(80, Byte), Integer))
        Me._ToolsShapedButton.MouseHoverEndColor = System.Drawing.Color.FromArgb(CType(CType(80, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(80, Byte), Integer))
        Me._ToolsShapedButton.Name = "_ToolsShapedButton"
        Me._ToolsShapedButton.ShowButtontext = True
        Me._ToolsShapedButton.Size = New System.Drawing.Size(136, 55)
        Me._ToolsShapedButton.StartColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer))
        Me._ToolsShapedButton.StartOpacity = 250
        Me._ToolsShapedButton.TabIndex = 7
        Me._ToolsShapedButton.TextLocation = New Point(45, 18)
        Me._ToolsShapedButton.UseVisualStyleBackColor = False
        '
        '_RightBorderBottomPanel
        '
        Me._RightBorderBottomPanel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._RightBorderBottomPanel.BackColor = System.Drawing.Color.Black
        Me._RightBorderBottomPanel.Cursor = System.Windows.Forms.Cursors.SizeNWSE
        Me._RightBorderBottomPanel.Location = New System.Drawing.Point(728, 452)
        Me._RightBorderBottomPanel.Name = "_RightBorderBottomPanel"
        Me._RightBorderBottomPanel.Size = New System.Drawing.Size(2, 19)
        Me._RightBorderBottomPanel.TabIndex = 9
        '
        '_BottomBorderLeftPanel
        '
        Me._BottomBorderLeftPanel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._BottomBorderLeftPanel.BackColor = System.Drawing.Color.Black
        Me._BottomBorderLeftPanel.Cursor = System.Windows.Forms.Cursors.SizeNESW
        Me._BottomBorderLeftPanel.Location = New System.Drawing.Point(0, 471)
        Me._BottomBorderLeftPanel.Name = "_BottomBorderLeftPanel"
        Me._BottomBorderLeftPanel.Size = New System.Drawing.Size(15, 2)
        Me._BottomBorderLeftPanel.TabIndex = 10
        '
        '_LeftBorderBottomPanel
        '
        Me._LeftBorderBottomPanel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._LeftBorderBottomPanel.BackColor = System.Drawing.Color.Black
        Me._LeftBorderBottomPanel.Cursor = System.Windows.Forms.Cursors.SizeNESW
        Me._LeftBorderBottomPanel.Location = New System.Drawing.Point(0, 453)
        Me._LeftBorderBottomPanel.Name = "_LeftBorderBottomPanel"
        Me._LeftBorderBottomPanel.Size = New System.Drawing.Size(2, 19)
        Me._LeftBorderBottomPanel.TabIndex = 11
        '
        '_RightBorderTopPanel
        '
        Me._RightBorderTopPanel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._RightBorderTopPanel.BackColor = System.Drawing.Color.Black
        Me._RightBorderTopPanel.Cursor = System.Windows.Forms.Cursors.SizeNESW
        Me._RightBorderTopPanel.Location = New System.Drawing.Point(728, 2)
        Me._RightBorderTopPanel.Name = "_RightBorderTopPanel"
        Me._RightBorderTopPanel.Size = New System.Drawing.Size(2, 20)
        Me._RightBorderTopPanel.TabIndex = 12
        '
        '_TopBorderRightPanel
        '
        Me._TopBorderRightPanel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._TopBorderRightPanel.BackColor = System.Drawing.Color.Black
        Me._TopBorderRightPanel.Cursor = System.Windows.Forms.Cursors.SizeNESW
        Me._TopBorderRightPanel.Location = New System.Drawing.Point(710, 0)
        Me._TopBorderRightPanel.Name = "_TopBorderRightPanel"
        Me._TopBorderRightPanel.Size = New System.Drawing.Size(20, 2)
        Me._TopBorderRightPanel.TabIndex = 13
        '
        '_TopBorderLeftPanel
        '
        Me._TopBorderLeftPanel.BackColor = System.Drawing.Color.Black
        Me._TopBorderLeftPanel.Cursor = System.Windows.Forms.Cursors.SizeNWSE
        Me._TopBorderLeftPanel.Location = New System.Drawing.Point(0, 0)
        Me._TopBorderLeftPanel.Name = "_TopBorderLeftPanel"
        Me._TopBorderLeftPanel.Size = New System.Drawing.Size(20, 2)
        Me._TopBorderLeftPanel.TabIndex = 14
        '
        '_LeftBorderTopPanel
        '
        Me._LeftBorderTopPanel.BackColor = System.Drawing.Color.Black
        Me._LeftBorderTopPanel.Cursor = System.Windows.Forms.Cursors.SizeNWSE
        Me._LeftBorderTopPanel.Location = New System.Drawing.Point(0, 0)
        Me._LeftBorderTopPanel.Name = "_LeftBorderTopPanel"
        Me._LeftBorderTopPanel.Size = New System.Drawing.Size(2, 20)
        Me._LeftBorderTopPanel.TabIndex = 15
        '
        '_LeftPanel
        '
        Me._LeftPanel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
        Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._LeftPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me._LeftPanel.Controls.Add(Me._HelpButton)
        Me._LeftPanel.Controls.Add(Me._RunButton)
        Me._LeftPanel.Controls.Add(Me._ViewButton)
        Me._LeftPanel.Controls.Add(Me._EditButton)
        Me._LeftPanel.Controls.Add(Me._FileButton)
        Me._LeftPanel.Location = New System.Drawing.Point(2, 77)
        Me._LeftPanel.Name = "_LeftPanel"
        Me._LeftPanel.Size = New System.Drawing.Size(280, 325)
        Me._LeftPanel.TabIndex = 16
        '
        '_HelpButton
        '
        Me._HelpButton.BusyBackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me._HelpButton.DisplayText = "Help"
        Me._HelpButton.Dock = System.Windows.Forms.DockStyle.Top
        Me._HelpButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me._HelpButton.Font = New System.Drawing.Font("Microsoft YaHei UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._HelpButton.ForeColor = System.Drawing.Color.White
        Me._HelpButton.Location = New System.Drawing.Point(0, 140)
        Me._HelpButton.MouseClickColor = System.Drawing.Color.Black
        Me._HelpButton.MouseColorsEnabled = True
        Me._HelpButton.MouseHoverColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer))
        Me._HelpButton.Name = "_HelpButton"
        Me._HelpButton.Size = New System.Drawing.Size(280, 35)
        Me._HelpButton.TabIndex = 4
        Me._HelpButton.Text = "Help"
        Me._HelpButton.TextLocationLeft = 110
        Me._HelpButton.TextLocationTop = 6
        Me._HelpButton.UseVisualStyleBackColor = True
        '
        '_RunButton
        '
        Me._RunButton.BusyBackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me._RunButton.DisplayText = "Run"
        Me._RunButton.Dock = System.Windows.Forms.DockStyle.Top
        Me._RunButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me._RunButton.Font = New System.Drawing.Font("Microsoft YaHei UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._RunButton.ForeColor = System.Drawing.Color.White
        Me._RunButton.Location = New System.Drawing.Point(0, 105)
        Me._RunButton.MouseClickColor = System.Drawing.Color.Black
        Me._RunButton.MouseColorsEnabled = True
        Me._RunButton.MouseHoverColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer))
        Me._RunButton.Name = "_RunButton"
        Me._RunButton.Size = New System.Drawing.Size(280, 35)
        Me._RunButton.TabIndex = 3
        Me._RunButton.Text = "Run"
        Me._RunButton.TextLocationLeft = 110
        Me._RunButton.TextLocationTop = 6
        Me._RunButton.UseVisualStyleBackColor = True
        '
        '_ViewButton
        '
        Me._ViewButton.BusyBackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me._ViewButton.DisplayText = "View"
        Me._ViewButton.Dock = System.Windows.Forms.DockStyle.Top
        Me._ViewButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me._ViewButton.Font = New System.Drawing.Font("Microsoft YaHei UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ViewButton.ForeColor = System.Drawing.Color.White
        Me._ViewButton.Location = New System.Drawing.Point(0, 70)
        Me._ViewButton.MouseClickColor = System.Drawing.Color.Black
        Me._ViewButton.MouseColorsEnabled = True
        Me._ViewButton.MouseHoverColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer))
        Me._ViewButton.Name = "_ViewButton"
        Me._ViewButton.Size = New System.Drawing.Size(280, 35)
        Me._ViewButton.TabIndex = 2
        Me._ViewButton.Text = "View"
        Me._ViewButton.TextLocationLeft = 110
        Me._ViewButton.TextLocationTop = 6
        Me._ViewButton.UseVisualStyleBackColor = True
        '
        '_EditButton
        '
        Me._EditButton.BusyBackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me._EditButton.DisplayText = "Edit"
        Me._EditButton.Dock = System.Windows.Forms.DockStyle.Top
        Me._EditButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me._EditButton.Font = New System.Drawing.Font("Microsoft YaHei UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._EditButton.ForeColor = System.Drawing.Color.White
        Me._EditButton.Location = New System.Drawing.Point(0, 35)
        Me._EditButton.MouseClickColor = System.Drawing.Color.Black
        Me._EditButton.MouseColorsEnabled = True
        Me._EditButton.MouseHoverColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer))
        Me._EditButton.Name = "_EditButton"
        Me._EditButton.Size = New System.Drawing.Size(280, 35)
        Me._EditButton.TabIndex = 1
        Me._EditButton.Text = "Edit"
        Me._EditButton.TextLocationLeft = 110
        Me._EditButton.TextLocationTop = 6
        Me._EditButton.UseVisualStyleBackColor = True
        '
        '_FileButton
        '
        Me._FileButton.BusyBackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me._FileButton.DisplayText = "File"
        Me._FileButton.Dock = System.Windows.Forms.DockStyle.Top
        Me._FileButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me._FileButton.Font = New System.Drawing.Font("Microsoft YaHei UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._FileButton.ForeColor = System.Drawing.Color.White
        Me._FileButton.Location = New System.Drawing.Point(0, 0)
        Me._FileButton.MouseClickColor = System.Drawing.Color.Black
        Me._FileButton.MouseColorsEnabled = True
        Me._FileButton.MouseHoverColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer))
        Me._FileButton.Name = "_FileButton"
        Me._FileButton.Size = New System.Drawing.Size(280, 35)
        Me._FileButton.TabIndex = 0
        Me._FileButton.Text = "File"
        Me._FileButton.TextLocationLeft = 110
        Me._FileButton.TextLocationTop = 6
        Me._FileButton.UseVisualStyleBackColor = True
        '
        '_SeparatorPanel
        '
        Me._SeparatorPanel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
        Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._SeparatorPanel.BackColor = System.Drawing.Color.Black
        Me._SeparatorPanel.Location = New System.Drawing.Point(282, 78)
        Me._SeparatorPanel.Name = "_SeparatorPanel"
        Me._SeparatorPanel.Size = New System.Drawing.Size(2, 324)
        Me._SeparatorPanel.TabIndex = 17
        '
        '_InformationLabel
        '
        Me._InformationLabel.AutoSize = True
        Me._InformationLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._InformationLabel.ForeColor = System.Drawing.Color.White
        Me._InformationLabel.Location = New System.Drawing.Point(309, 204)
        Me._InformationLabel.Name = "_InformationLabel"
        Me._InformationLabel.Size = New System.Drawing.Size(378, 24)
        Me._InformationLabel.TabIndex = 18
        Me._InformationLabel.Text = "Resizable Black colored Custom Form in C#"
        '
        'BlackForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(730, 473)
        Me.Controls.Add(Me._InformationLabel)
        Me.Controls.Add(Me._SeparatorPanel)
        Me.Controls.Add(Me._LeftPanel)
        Me.Controls.Add(Me._LeftBorderTopPanel)
        Me.Controls.Add(Me._TopBorderLeftPanel)
        Me.Controls.Add(Me._TopBorderRightPanel)
        Me.Controls.Add(Me._RightBorderTopPanel)
        Me.Controls.Add(Me._LeftBorderBottomPanel)
        Me.Controls.Add(Me._BottomBorderLeftPanel)
        Me.Controls.Add(Me._RightBorderBottomPanel)
        Me.Controls.Add(Me._BottomBorderRightPanel)
        Me.Controls.Add(Me._BottomBorderPanel)
        Me.Controls.Add(Me._LeftBorderPanel)
        Me.Controls.Add(Me._RightBorderPanel)
        Me.Controls.Add(Me._TopBorderPanel)
        Me.Controls.Add(Me._TopPanel)
        Me.Controls.Add(Me._BottomPanel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MainMenuStrip = Me._MenuStrip
        Me.Name = "BlackForm"
        Me.Text = "My App"
        Me._TopPanel.ResumeLayout(False)
        Me._TopPanel.PerformLayout()
        Me._MenuStrip.ResumeLayout(False)
        Me._MenuStrip.PerformLayout()
        Me._BottomPanel.ResumeLayout(False)
        Me._LeftPanel.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private WithEvents _TopBorderPanel As System.Windows.Forms.Panel
    Private WithEvents _RightBorderPanel As System.Windows.Forms.Panel
    Private WithEvents _LeftBorderPanel As System.Windows.Forms.Panel
    Private WithEvents _BottomBorderPanel As System.Windows.Forms.Panel
    Private WithEvents _TopPanel As System.Windows.Forms.Panel
    Private WithEvents _CloseButton As ZopeButton
    Private WithEvents _BottomBorderRightPanel As System.Windows.Forms.Panel
    Private _AddShapedButton As ShapedButton
    Private WithEvents _WindowTextLabel As System.Windows.Forms.Label
    Private WithEvents _MaxButton As MinMaxButton
    Private _BuyNowShapedButton As ShapedButton
    Private _ToolsShapedButton As ShapedButton
    Private _BottomPanel As System.Windows.Forms.Panel
    Private WithEvents _MinButton As ZopeButton
    Private _ToolTip As System.Windows.Forms.ToolTip
    Private WithEvents _RightBorderBottomPanel As System.Windows.Forms.Panel
    Private WithEvents _BottomBorderLeftPanel As System.Windows.Forms.Panel
    Private WithEvents _LeftBorderBottomPanel As System.Windows.Forms.Panel
    Private WithEvents _RightBorderTopPanel As System.Windows.Forms.Panel
    Private WithEvents _TopBorderRightPanel As System.Windows.Forms.Panel
    Private WithEvents _TopBorderLeftPanel As System.Windows.Forms.Panel
    Private WithEvents _LeftBorderTopPanel As System.Windows.Forms.Panel
    Private _LeftPanel As System.Windows.Forms.Panel
    Private WithEvents _FileButton As ZopeButton
    Private _SeparatorPanel As System.Windows.Forms.Panel
    Private WithEvents _EditButton As ZopeButton
    Private WithEvents _ViewButton As ZopeButton
    Private WithEvents _HelpButton As ZopeButton
    Private WithEvents _RunButton As ZopeButton
    Private _InformationLabel As System.Windows.Forms.Label
    Private _TestShapedButton As ShapedButton
    Private _TimesShapedButton As ShapedButton
    Private _MenuStrip As ZopeMenuStrip
    Private _FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private _NewToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private _OpenToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private _ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Private _SaveToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private _SaveAsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private _ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Private _CloseToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private _CloseAllToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private _ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Private _PrintToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private _PrintPreviewToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private _ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Private _CloseToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Private _EditToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private _CutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private _CopyToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private _PasteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private _ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Private _UnduToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private _RedoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private _ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
    Private _FindToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private _ReplaceToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private _SelectAllToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private _HelpToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private _HelpContentsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private _OnlineHelpToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private _AboutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem


End Class
