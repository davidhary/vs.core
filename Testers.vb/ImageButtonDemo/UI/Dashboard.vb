Imports isr.Core.Forma

Imports System.Drawing.Drawing2D

Partial Public Class Dashboard
    Inherits FormBase

    Private _HideAlerts As Boolean = False
    Public Sub New()
        Me.InitializeComponent()
        Me._TraceMessages = New TraceMessagesStack
        Me.TraceMessageToolStrip1.TraceMessagesStack = Me._TraceMessages
    End Sub

    Private Const _TraceEventId As Integer = &H11

    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private Property TraceMessages As TraceMessagesStack
    Private Sub ImageButton1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ImageButton1.Click
        If Not Me._HideAlerts Then
            ' MessageBox.Show("Clicked default button.")
            Me._TraceMessages.Push(New TraceMessage(TraceEventType.Information, _TraceEventId, "Clicked default button."))
        End If
    End Sub

    Private Sub ImageButton2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ImageButton2.Click
        If Not Me._HideAlerts Then
            'MessageBox.Show("Clicked button B.")
            Me._TraceMessages.Push(New TraceMessage(TraceEventType.Verbose, _TraceEventId, "Clicked button B"))
        End If
    End Sub

    Private Sub ImageButton3_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ImageButton3.Click
        Me._HideAlerts = Not Me._HideAlerts
        If Me._HideAlerts Then
            Me.ImageButton3.NormalImage = My.Resources.CCheckedNormal
            Me.ImageButton3.HoverImage = My.Resources.CCheckedHover
            Me.ImageButton3.DownImage = My.Resources.CCheckedDown
        Else
            Me.ImageButton3.NormalImage = My.Resources.CUncheckedNormal
            Me.ImageButton3.HoverImage = My.Resources.CUncheckedHover
            Me.ImageButton3.DownImage = My.Resources.CUncheckedDown
        End If
    End Sub

    Private Sub ImageButton4_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ImageButton4.Click
        If Not Me._HideAlerts Then
            ' MessageBox.Show("Clicked button C.")
            Me._TraceMessages.Push(New TraceMessage(TraceEventType.Warning, _TraceEventId, "Clicked button C"))
        End If
    End Sub

    Private Sub ImageButton5_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ImageButton5.Click
        If Not Me._HideAlerts Then
            ' MessageBox.Show("Clicked button D.")
            Me._TraceMessages.Push(New TraceMessage(TraceEventType.Error, _TraceEventId, "Clicked button D"))
        End If
    End Sub

    Private Sub ImageButton6_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ImageButton6.Click
        If Not Me._HideAlerts Then
            'MessageBox.Show("Clicked button E.")
            Me._TraceMessages.Push(New TraceMessage(TraceEventType.Information, _TraceEventId, "Clicked button E"))
        End If
    End Sub

    Private Sub ImageButton7_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ImageButton7.Click
        If Not Me._HideAlerts Then
            'MessageBox.Show("Clicked button F.")
            Me._TraceMessages.Push(New TraceMessage(TraceEventType.Information, _TraceEventId, "Clicked button F"))
        End If
    End Sub

#Region " DROP SHADOW "

    Public Property ShadowDistance As Integer = 4

    ''' <summary>
    ''' Draws and Fills a Rounded Rectangle and it's accompanying shadow
    ''' </summary>
    ''' <param name="e">PaintEventArgs object passed in from the Picture box</param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private Sub DrawRndRect(ByVal picCanvas As Control, ByRef e As PaintEventArgs)
        ' I like clean lines so set the smoothing mode to Anti-Alias
        e.Graphics.SmoothingMode = SmoothingMode.AntiAlias

        ' lets create a rectangle that will be centered in the picture box and
        ' just under half the size
        Dim _Rectangle As New Rectangle(CInt(Fix(picCanvas.Width * 0.3)), CInt(Fix(picCanvas.Height * 0.3)), CInt(Fix(picCanvas.Width * 0.4)), CInt(Fix(picCanvas.Height * 0.4)))

        ' create the radius variable and set it equal to 20% the height of the rectangle
        ' this will determine the amount of bend at the corners
        Dim _Radius As Single = CInt(Fix(_Rectangle.Height * 0.2))

        ' create an x and y variable so that we can reduce the length of our code lines
        Dim x As Single = _Rectangle.Left
        Dim y As Single = _Rectangle.Top

        ' make sure that we have a valid radius, too small and we have a problem
        If _Radius < 1 Then
            _Radius = 1
        End If

        Try
            ' Create a graphics path object with the using operator so the framework
            ' can clean up the resources for us
            Using _Path As New GraphicsPath()
                ' build the rounded rectangle starting at the top line and going around
                ' until the line meets itself again
                _Path.AddLine(x + _Radius, y, x + _Rectangle.Width - (_Radius * 2), y)
                _Path.AddArc(x + _Rectangle.Width - (_Radius * 2), y, _Radius * 2, _Radius * 2, 270, 90)
                _Path.AddLine(x + _Rectangle.Width, y + _Radius, x + _Rectangle.Width, y + _Rectangle.Height - (_Radius * 2))
                _Path.AddArc(x + _Rectangle.Width - (_Radius * 2), y + _Rectangle.Height - (_Radius * 2), _Radius * 2, _Radius * 2, 0, 90)
                _Path.AddLine(x + _Rectangle.Width - (_Radius * 2), y + _Rectangle.Height, x + _Radius, y + _Rectangle.Height)
                _Path.AddArc(x, y + _Rectangle.Height - (_Radius * 2), _Radius * 2, _Radius * 2, 90, 90)
                _Path.AddLine(x, y + _Rectangle.Height - (_Radius * 2), x, y + _Radius)
                _Path.AddArc(x, y, _Radius * 2, _Radius * 2, 180, 90)

                ' this is where we create the shadow effect, so we will use a 
                ' path gradient brush
                Using _Brush As New PathGradientBrush(_Path)
                    ' set the wrap mode so that the colors will layer themselves
                    ' from the outer edge in
                    _Brush.WrapMode = WrapMode.Clamp

                    ' Create a color blend to manage our colors and positions and
                    ' since we need 3 colors set the default length to 3

                    ' here is the important part of the shadow making process, remember
                    ' the clamp mode on the color blend object layers the colors from
                    ' the outside to the center so we want our transparent color first
                    ' followed by the actual shadow color. Set the shadow color to a 
                    ' slightly transparent DimGray, I find that it works best.

                    ' our color blend will control the distance of each color layer
                    ' we want to set our transparent color to 0 indicating that the 
                    ' transparent color should be the outer most color drawn, then
                    ' our gray color at about 10% of the distance from the edge

                    Dim _ColorBlend As New ColorBlend(3) With {
                        .Colors = New Color() {Color.Transparent, Color.FromArgb(180, Color.DimGray), Color.FromArgb(180, Color.DimGray)},
                        .Positions = New Single() {0F, 0.1F, 1.0F}}

                    ' assign the color blend to the path gradient brush
                    _Brush.InterpolationColors = _ColorBlend

                    ' fill the shadow with our path gradient brush
                    e.Graphics.FillPath(_Brush, _Path)
                End Using

                ' since the shadow was drawn first we need to move the actual path
                ' up and back a little so that we can show the shadow underneath
                ' the object. To accomplish this we will create a Matrix Object
                Dim _Matrix As New Matrix()

                ' tell the matrix to move the path up and back the designated distance
                _Matrix.Translate(Me._ShadowDistance, Me._ShadowDistance)

                ' assign the matrix to the graphics path of the rounded rectangle
                _Path.Transform(_Matrix)

                ' fill the graphics path first
                Using _Brush As New LinearGradientBrush(picCanvas.ClientRectangle, Color.Tomato, Color.MistyRose, LinearGradientMode.Vertical)
                    e.Graphics.FillPath(_Brush, _Path)
                End Using

                ' Draw the Graphics path last so that we have cleaner borders
                Using _Pen As New Pen(Color.DimGray, 1.0F)
                    e.Graphics.DrawPath(_Pen, _Path)
                End Using
            End Using
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(Me.GetType().Name & ".DrawRndRect() Error: " & ex.Message)
        End Try
    End Sub

#End Region

End Class
