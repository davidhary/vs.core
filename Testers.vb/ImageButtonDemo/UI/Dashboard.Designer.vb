Imports System.Windows.Forms
Partial Public Class Dashboard
    ''' <summary>
    ''' Required designer variable.
    ''' </summary>
    Private components As System.ComponentModel.IContainer = Nothing

    ''' <summary>
    ''' Clean up any resources being used.
    ''' </summary>
    ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso (components IsNot Nothing) Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

#Region "Windows Form Designer generated code"

    ''' <summary>
    ''' Required method for Designer support - do not modify
    ''' the contents of this method with the code editor.
    ''' </summary>
    Private Sub InitializeComponent()
        Me.ImageButton1 = New isr.Core.Controls.ImageButton()
        Me.ImageButton3 = New isr.Core.Controls.ImageButton()
        Me.ImageButton2 = New isr.Core.Controls.ImageButton()
        Me.label1 = New System.Windows.Forms.Label()
        Me.ImageButton4 = New isr.Core.Controls.ImageButton()
        Me.ImageButton5 = New isr.Core.Controls.ImageButton()
        Me.ImageButton6 = New isr.Core.Controls.ImageButton()
        Me.ImageButton7 = New isr.Core.Controls.ImageButton()
        Me.label2 = New System.Windows.Forms.Label()
        Me.TraceMessageToolStrip1 = New isr.Core.Forma.TraceMessageToolStrip()
        CType(Me.ImageButton1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ImageButton3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ImageButton2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ImageButton4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ImageButton5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ImageButton6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ImageButton7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ImageButton1
        '
        Me.ImageButton1.DialogResult = System.Windows.Forms.DialogResult.None
        Me.ImageButton1.DownImage = Global.isr.Core.Tester.My.Resources.Resources.ExampleButtonDown
        Me.ImageButton1.HoverImage = Global.isr.Core.Tester.My.Resources.Resources.ExampleButtonHover
        Me.ImageButton1.Location = New System.Drawing.Point(61, 61)
        Me.ImageButton1.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.ImageButton1.Name = "ImageButton1"
        Me.ImageButton1.NormalImage = Global.isr.Core.Tester.My.Resources.Resources.ExampleButton
        Me.ImageButton1.Size = New System.Drawing.Size(100, 50)
        Me.ImageButton1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.ImageButton1.TabIndex = 0
        Me.ImageButton1.TabStop = False
        '
        'ImageButton3
        '
        Me.ImageButton3.DialogResult = System.Windows.Forms.DialogResult.None
        Me.ImageButton3.DownImage = Global.isr.Core.Tester.My.Resources.Resources.CUncheckedDown
        Me.ImageButton3.HoverImage = Global.isr.Core.Tester.My.Resources.Resources.CUncheckedHover
        Me.ImageButton3.Location = New System.Drawing.Point(105, 154)
        Me.ImageButton3.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.ImageButton3.Name = "ImageButton3"
        Me.ImageButton3.NormalImage = Global.isr.Core.Tester.My.Resources.Resources.CUncheckedNormal
        Me.ImageButton3.Size = New System.Drawing.Size(20, 20)
        Me.ImageButton3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.ImageButton3.TabIndex = 2
        Me.ImageButton3.TabStop = False
        '
        'ImageButton2
        '
        Me.ImageButton2.DialogResult = System.Windows.Forms.DialogResult.None
        Me.ImageButton2.DownImage = Global.isr.Core.Tester.My.Resources.Resources.ExampleButtonDownA
        Me.ImageButton2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ImageButton2.HoverImage = Global.isr.Core.Tester.My.Resources.Resources.ExampleButtonHoverA
        Me.ImageButton2.Location = New System.Drawing.Point(184, 61)
        Me.ImageButton2.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.ImageButton2.Name = "ImageButton2"
        Me.ImageButton2.NormalImage = Global.isr.Core.Tester.My.Resources.Resources.ExampleButtonA
        Me.ImageButton2.Size = New System.Drawing.Size(100, 50)
        Me.ImageButton2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.ImageButton2.TabIndex = 1
        Me.ImageButton2.TabStop = False
        Me.ImageButton2.Text = "Example B"
        '
        'label1
        '
        Me.label1.AutoSize = True
        Me.label1.BackColor = System.Drawing.Color.Transparent
        Me.label1.Location = New System.Drawing.Point(126, 160)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(115, 17)
        Me.label1.TabIndex = 3
        Me.label1.Text = "Disable click alerts"
        '
        'ImageButton4
        '
        Me.ImageButton4.DialogResult = System.Windows.Forms.DialogResult.None
        Me.ImageButton4.DownImage = Global.isr.Core.Tester.My.Resources.Resources.ExampleButtonDownA
        Me.ImageButton4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ImageButton4.HoverImage = Nothing
        Me.ImageButton4.Location = New System.Drawing.Point(308, 61)
        Me.ImageButton4.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.ImageButton4.Name = "ImageButton4"
        Me.ImageButton4.NormalImage = Global.isr.Core.Tester.My.Resources.Resources.ExampleButtonA
        Me.ImageButton4.Size = New System.Drawing.Size(100, 50)
        Me.ImageButton4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.ImageButton4.TabIndex = 4
        Me.ImageButton4.TabStop = False
        Me.ImageButton4.Text = "Example C"
        '
        'ImageButton5
        '
        Me.ImageButton5.DialogResult = System.Windows.Forms.DialogResult.None
        Me.ImageButton5.DownImage = Nothing
        Me.ImageButton5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ImageButton5.HoverImage = Global.isr.Core.Tester.My.Resources.Resources.ExampleButtonHoverA
        Me.ImageButton5.Location = New System.Drawing.Point(432, 61)
        Me.ImageButton5.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.ImageButton5.Name = "ImageButton5"
        Me.ImageButton5.NormalImage = Global.isr.Core.Tester.My.Resources.Resources.ExampleButtonA
        Me.ImageButton5.Size = New System.Drawing.Size(100, 50)
        Me.ImageButton5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.ImageButton5.TabIndex = 5
        Me.ImageButton5.TabStop = False
        Me.ImageButton5.Text = "Example D"
        '
        'ImageButton6
        '
        Me.ImageButton6.DialogResult = System.Windows.Forms.DialogResult.None
        Me.ImageButton6.DownImage = Nothing
        Me.ImageButton6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ImageButton6.HoverImage = Nothing
        Me.ImageButton6.Location = New System.Drawing.Point(308, 135)
        Me.ImageButton6.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.ImageButton6.Name = "ImageButton6"
        Me.ImageButton6.NormalImage = Global.isr.Core.Tester.My.Resources.Resources.ExampleButtonA
        Me.ImageButton6.Size = New System.Drawing.Size(100, 50)
        Me.ImageButton6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.ImageButton6.TabIndex = 6
        Me.ImageButton6.TabStop = False
        Me.ImageButton6.Text = "Example E"
        '
        'ImageButton7
        '
        Me.ImageButton7.DialogResult = System.Windows.Forms.DialogResult.None
        Me.ImageButton7.DownImage = Global.isr.Core.Tester.My.Resources.Resources.ExampleButtonDownA
        Me.ImageButton7.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ImageButton7.HoverImage = Global.isr.Core.Tester.My.Resources.Resources.ExampleButtonHoverA
        Me.ImageButton7.Location = New System.Drawing.Point(432, 135)
        Me.ImageButton7.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.ImageButton7.Name = "ImageButton7"
        Me.ImageButton7.NormalImage = Nothing
        Me.ImageButton7.Size = New System.Drawing.Size(100, 50)
        Me.ImageButton7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.ImageButton7.TabIndex = 7
        Me.ImageButton7.TabStop = False
        Me.ImageButton7.Text = "Example F"
        '
        'label2
        '
        Me.label2.AutoSize = True
        Me.label2.Location = New System.Drawing.Point(66, 126)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(104, 17)
        Me.label2.TabIndex = 8
        Me.label2.Text = "Default button ^"
        '
        'TraceMessageToolStrip1
        '
        Me.TraceMessageToolStrip1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TraceMessageToolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.TraceMessageToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.TraceMessageToolStrip1.Name = "TraceMessageToolStrip1"
        Me.TraceMessageToolStrip1.Size = New System.Drawing.Size(649, 29)
        Me.TraceMessageToolStrip1.Stretch = True
        Me.TraceMessageToolStrip1.TabIndex = 9
        Me.TraceMessageToolStrip1.Text = "TraceMessageToolStrip1"
        '
        'Dashboard
        '
        Me.AcceptButton = Me.ImageButton1
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(649, 251)
        Me.Controls.Add(Me.TraceMessageToolStrip1)
        Me.Controls.Add(Me.ImageButton7)
        Me.Controls.Add(Me.ImageButton6)
        Me.Controls.Add(Me.ImageButton5)
        Me.Controls.Add(Me.ImageButton4)
        Me.Controls.Add(Me.ImageButton3)
        Me.Controls.Add(Me.label1)
        Me.Controls.Add(Me.ImageButton2)
        Me.Controls.Add(Me.ImageButton1)
        Me.Controls.Add(Me.label2)
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.Name = "Dashboard"
        Me.ShowIcon = False
        Me.Text = "Image Button Demo"
        CType(Me.ImageButton1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ImageButton3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ImageButton2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ImageButton4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ImageButton5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ImageButton6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ImageButton7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private WithEvents ImageButton1 As isr.Core.Controls.ImageButton
    Private WithEvents ImageButton2 As isr.Core.Controls.ImageButton
    Private WithEvents ImageButton3 As isr.Core.Controls.ImageButton
    Private label1 As System.Windows.Forms.Label
    Private WithEvents ImageButton4 As isr.Core.Controls.ImageButton
    Private WithEvents ImageButton5 As isr.Core.Controls.ImageButton
    Private WithEvents ImageButton6 As isr.Core.Controls.ImageButton
    Private WithEvents ImageButton7 As isr.Core.Controls.ImageButton
    Private label2 As System.Windows.Forms.Label
    Private WithEvents TraceMessageToolStrip1 As isr.Core.Forma.TraceMessageToolStrip
End Class

