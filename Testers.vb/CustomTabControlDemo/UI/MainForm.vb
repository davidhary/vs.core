''' <summary> The application's main window. </summary>
''' <remarks> David, 2021-03-12. </remarks>
Partial Public Class MainForm
    Inherits System.Windows.Forms.Form

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.Windows.Forms.Form" /> class.
    ''' </summary>
    ''' <remarks> David, 2021-03-12. </remarks>
    Public Sub New()

        Me.InitializeComponent()
    End Sub

End Class
