'
' * Created by SharpDevelop.
' * User: mjackson
' * Date: 2010-28-06
' * Time: 13:38
' * 
' * To change this template use Tools | Options | Coding | Edit Standard Headers.
' 

Imports System.Windows.Forms

''' <summary> Class with program entry point. </summary>
''' <remarks> David, 2021-03-12. </remarks>
Friend NotInheritable Class Program

	''' <summary> Program entry point. </summary>
	''' <remarks> David, 2021-03-12. </remarks>
	<STAThread>
	Friend Shared Sub Main()
		Application.EnableVisualStyles()
		Application.SetCompatibleTextRenderingDefault(False)
		Application.Run(New MainForm())
	End Sub



End Class
