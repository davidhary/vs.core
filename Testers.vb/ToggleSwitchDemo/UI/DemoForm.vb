Imports System.Threading
Imports isr.Core.Controls

Partial Public Class DemoForm
    Inherits Form

    Public Sub New()
        Me.InitializeComponent()
        Me.SetPropertiesForStylesTabSwitches()
        Me.SetPropertiesForPropertiesTabSwitches()
        Me.SetPropertiesForCustomizationsTabSwitches()
    End Sub

    Public Sub SetPropertiesForStylesTabSwitches()
        'Set the properties for the ToggleSwitches on the "Styles" tab

        Me.MetroStyleToggleSwitch.Style = ToggleSwitchStyle.Metro 'Default
        Me.MetroStyleToggleSwitch.Size = New Size(75, 23)

        Me.IOS5StyleToggleSwitch.Style = ToggleSwitchStyle.Ios5
        Me.IOS5StyleToggleSwitch.Size = New Size(98, 42)
        Me.IOS5StyleToggleSwitch.OnText = "ON"
        Me.IOS5StyleToggleSwitch.OnFont = New Font(Me.DemoTabControl.Font.FontFamily, 12, FontStyle.Bold)
        Me.IOS5StyleToggleSwitch.OnForeColor = Color.White
        Me.IOS5StyleToggleSwitch.OffText = "OFF"
        Me.IOS5StyleToggleSwitch.OffFont = New Font(Me.DemoTabControl.Font.FontFamily, 12, FontStyle.Bold)
        Me.IOS5StyleToggleSwitch.OffForeColor = Color.FromArgb(141, 123, 141)

        Me.AndroidStyleToggleSwitch.Style = ToggleSwitchStyle.Android
        Me.AndroidStyleToggleSwitch.Size = New Size(78, 23)
        Me.AndroidStyleToggleSwitch.OnText = "ON"
        Me.AndroidStyleToggleSwitch.OnFont = New Font(Me.DemoTabControl.Font.FontFamily, 8, FontStyle.Bold)
        Me.AndroidStyleToggleSwitch.OnForeColor = Color.White
        Me.AndroidStyleToggleSwitch.OffText = "OFF"
        Me.AndroidStyleToggleSwitch.OffFont = New Font(Me.DemoTabControl.Font.FontFamily, 8, FontStyle.Bold)
        Me.AndroidStyleToggleSwitch.OffForeColor = Color.FromArgb(141, 123, 141)

        Me.BrushedMetalStyleToggleSwitch.Style = ToggleSwitchStyle.BrushedMetal
        Me.BrushedMetalStyleToggleSwitch.Size = New Size(93, 30)
        Me.BrushedMetalStyleToggleSwitch.OnText = "ON"
        Me.BrushedMetalStyleToggleSwitch.OnFont = New Font(Me.DemoTabControl.Font.FontFamily, 10, FontStyle.Bold)
        Me.BrushedMetalStyleToggleSwitch.OnForeColor = Color.White
        Me.BrushedMetalStyleToggleSwitch.OffText = "OFF"
        Me.BrushedMetalStyleToggleSwitch.OffFont = New Font(Me.DemoTabControl.Font.FontFamily, 10, FontStyle.Bold)
        Me.BrushedMetalStyleToggleSwitch.OffForeColor = Color.White

        Me.IphoneStyleToggleSwitch.Style = ToggleSwitchStyle.IPhone
        Me.IphoneStyleToggleSwitch.Size = New Size(93, 30)
        Me.IphoneStyleToggleSwitch.OnText = "ON"
        Me.IphoneStyleToggleSwitch.OnFont = New Font(Me.DemoTabControl.Font.FontFamily, 10, FontStyle.Bold)
        Me.IphoneStyleToggleSwitch.OnForeColor = Color.White
        Me.IphoneStyleToggleSwitch.OffText = "OFF"
        Me.IphoneStyleToggleSwitch.OffFont = New Font(Me.DemoTabControl.Font.FontFamily, 10, FontStyle.Bold)
        Me.IphoneStyleToggleSwitch.OffForeColor = Color.FromArgb(92, 92, 92)

        Me.ModernStyleToggleSwitch.Style = ToggleSwitchStyle.Modern
        Me.ModernStyleToggleSwitch.Size = New Size(85, 32)
        Me.ModernStyleToggleSwitch.OnText = "ON"
        Me.ModernStyleToggleSwitch.OnFont = New Font(Me.DemoTabControl.Font.FontFamily, 10, FontStyle.Bold)
        Me.ModernStyleToggleSwitch.OnForeColor = Color.White
        Me.ModernStyleToggleSwitch.OffText = "OFF"
        Me.ModernStyleToggleSwitch.OffFont = New Font(Me.DemoTabControl.Font.FontFamily, 10, FontStyle.Bold)
        Me.ModernStyleToggleSwitch.OffForeColor = Color.FromArgb(153, 153, 153)

        Me.CarbonStyleToggleSwitch.Style = ToggleSwitchStyle.Carbon
        Me.CarbonStyleToggleSwitch.Size = New Size(93, 30)
        Me.CarbonStyleToggleSwitch.OnText = "On"
        Me.CarbonStyleToggleSwitch.OnFont = New Font(Me.DemoTabControl.Font.FontFamily, 10, FontStyle.Bold)
        Me.CarbonStyleToggleSwitch.OnForeColor = Color.White
        Me.CarbonStyleToggleSwitch.OffText = "Off"
        Me.CarbonStyleToggleSwitch.OffFont = New Font(Me.DemoTabControl.Font.FontFamily, 10, FontStyle.Bold)
        Me.CarbonStyleToggleSwitch.OffForeColor = Color.White

        Me.OSXStyleToggleSwitch.Style = ToggleSwitchStyle.OS10
        Me.OSXStyleToggleSwitch.Size = New Size(93, 25)

        Me.FancyStyleToggleSwitch.Style = ToggleSwitchStyle.Fancy
        Me.FancyStyleToggleSwitch.Size = New Size(100, 30)
        Me.FancyStyleToggleSwitch.OnText = "ON"
        Me.FancyStyleToggleSwitch.OnFont = New Font(Me.DemoTabControl.Font.FontFamily, 10, FontStyle.Bold)
        Me.FancyStyleToggleSwitch.OnForeColor = Color.White
        Me.FancyStyleToggleSwitch.OffText = "OFF"
        Me.FancyStyleToggleSwitch.OffFont = New Font(Me.DemoTabControl.Font.FontFamily, 10, FontStyle.Bold)
        Me.FancyStyleToggleSwitch.OffForeColor = Color.White
        Me.FancyStyleToggleSwitch.ButtonImage = My.Resources.handle
    End Sub

    Public Sub SetPropertiesForPropertiesTabSwitches()
        'Set the properties for the ToggleSwitches on the "(Semi)-Important Properties" tab

        'AllowUserChange example:

        Me.AllowUserChangeToggleSwitch1.AllowUserChange = False
        Me.AllowUserChangeToggleSwitch2.AllowUserChange = True

        'Animation example:

        Me.NoAnimationToggleSwitch.Style = ToggleSwitchStyle.Carbon 'Only to provide an interesting look
        Me.NoAnimationToggleSwitch.Size = New Size(93, 30) 'Only to provide an interesting look
        Me.NoAnimationToggleSwitch.OnText = "On" 'Only to provide an interesting look
        Me.NoAnimationToggleSwitch.OnFont = New Font(Me.DemoTabControl.Font.FontFamily, 10, FontStyle.Bold) 'Only to provide an interesting look
        Me.NoAnimationToggleSwitch.OnForeColor = Color.White 'Only to provide an interesting look
        Me.NoAnimationToggleSwitch.OffText = "Off" 'Only to provide an interesting look
        Me.NoAnimationToggleSwitch.OffFont = New Font(Me.DemoTabControl.Font.FontFamily, 10, FontStyle.Bold) 'Only to provide an interesting look
        Me.NoAnimationToggleSwitch.OffForeColor = Color.White 'Only to provide an interesting look
        Me.NoAnimationToggleSwitch.UseAnimation = False

        Me.FastAnimationToggleSwitch.Style = ToggleSwitchStyle.Carbon 'Only to provide an interesting look
        Me.FastAnimationToggleSwitch.Size = New Size(93, 30) 'Only to provide an interesting look
        Me.FastAnimationToggleSwitch.OnText = "On" 'Only to provide an interesting look
        Me.FastAnimationToggleSwitch.OnFont = New Font(Me.DemoTabControl.Font.FontFamily, 10, FontStyle.Bold) 'Only to provide an interesting look
        Me.FastAnimationToggleSwitch.OnForeColor = Color.White 'Only to provide an interesting look
        Me.FastAnimationToggleSwitch.OffText = "Off" 'Only to provide an interesting look
        Me.FastAnimationToggleSwitch.OffFont = New Font(Me.DemoTabControl.Font.FontFamily, 10, FontStyle.Bold) 'Only to provide an interesting look
        Me.FastAnimationToggleSwitch.OffForeColor = Color.White 'Only to provide an interesting look
        Me.FastAnimationToggleSwitch.UseAnimation = True 'Default
        Me.FastAnimationToggleSwitch.AnimationInterval = 1 'Default
        Me.FastAnimationToggleSwitch.AnimationStep = 10 'Default

        Me.SlowAnimationToggleSwitch.Style = ToggleSwitchStyle.Carbon 'Only to provide an interesting look
        Me.SlowAnimationToggleSwitch.Size = New Size(93, 30) 'Only to provide an interesting look
        Me.SlowAnimationToggleSwitch.OnText = "On" 'Only to provide an interesting look
        Me.SlowAnimationToggleSwitch.OnFont = New Font(Me.DemoTabControl.Font.FontFamily, 10, FontStyle.Bold) 'Only to provide an interesting look
        Me.SlowAnimationToggleSwitch.OnForeColor = Color.White 'Only to provide an interesting look
        Me.SlowAnimationToggleSwitch.OffText = "Off" 'Only to provide an interesting look
        Me.SlowAnimationToggleSwitch.OffFont = New Font(Me.DemoTabControl.Font.FontFamily, 10, FontStyle.Bold) 'Only to provide an interesting look
        Me.SlowAnimationToggleSwitch.OffForeColor = Color.White 'Only to provide an interesting look
        Me.SlowAnimationToggleSwitch.UseAnimation = True 'Default
        Me.SlowAnimationToggleSwitch.AnimationInterval = 10
        Me.SlowAnimationToggleSwitch.AnimationStep = 1

        'GrayWhenDisabled example:

        Me.GrayWhenDisabledToggleSwitch1.Style = ToggleSwitchStyle.Fancy 'Only to provide an interesting look
        Me.GrayWhenDisabledToggleSwitch1.Size = New Size(100, 30) 'Only to provide an interesting look
        Me.GrayWhenDisabledToggleSwitch1.OnText = "ON" 'Only to provide an interesting look
        Me.GrayWhenDisabledToggleSwitch1.OnFont = New Font(Me.DemoTabControl.Font.FontFamily, 10, FontStyle.Bold) 'Only to provide an interesting look
        Me.GrayWhenDisabledToggleSwitch1.OnForeColor = Color.White 'Only to provide an interesting look
        Me.GrayWhenDisabledToggleSwitch1.OffText = "OFF" 'Only to provide an interesting look
        Me.GrayWhenDisabledToggleSwitch1.OffFont = New Font(Me.DemoTabControl.Font.FontFamily, 10, FontStyle.Bold) 'Only to provide an interesting look
        Me.GrayWhenDisabledToggleSwitch1.OffForeColor = Color.White 'Only to provide an interesting look
        Me.GrayWhenDisabledToggleSwitch1.ButtonImage = My.Resources.arrowright 'Only to provide an interesting look
        Me.GrayWhenDisabledToggleSwitch1.GrayWhenDisabled = False

        Me.GrayWhenDisabledToggleSwitch2.Style = ToggleSwitchStyle.Fancy 'Only to provide an interesting look
        Me.GrayWhenDisabledToggleSwitch2.Size = New Size(100, 30) 'Only to provide an interesting look
        Me.GrayWhenDisabledToggleSwitch2.OnText = "ON" 'Only to provide an interesting look
        Me.GrayWhenDisabledToggleSwitch2.OnFont = New Font(Me.DemoTabControl.Font.FontFamily, 10, FontStyle.Bold) 'Only to provide an interesting look
        Me.GrayWhenDisabledToggleSwitch2.OnForeColor = Color.White 'Only to provide an interesting look
        Me.GrayWhenDisabledToggleSwitch2.OffText = "OFF" 'Only to provide an interesting look
        Me.GrayWhenDisabledToggleSwitch2.OffFont = New Font(Me.DemoTabControl.Font.FontFamily, 10, FontStyle.Bold) 'Only to provide an interesting look
        Me.GrayWhenDisabledToggleSwitch2.OffForeColor = Color.White 'Only to provide an interesting look
        Me.GrayWhenDisabledToggleSwitch2.ButtonImage = My.Resources.arrowright 'Only to provide an interesting look
        Me.GrayWhenDisabledToggleSwitch2.GrayWhenDisabled = True 'Default

        'ThresholdPercentage example:

        Me.ThresholdPercentageToggleSwitch.Style = ToggleSwitchStyle.Ios5 'Only to provide an interesting look
        Me.ThresholdPercentageToggleSwitch.Size = New Size(98, 42) 'Only to provide an interesting look
        Me.ThresholdPercentageToggleSwitch.OnText = "ON" 'Only to provide an interesting look
        Me.ThresholdPercentageToggleSwitch.OnFont = New Font(Me.DemoTabControl.Font.FontFamily, 12, FontStyle.Bold) 'Only to provide an interesting look
        Me.ThresholdPercentageToggleSwitch.OnForeColor = Color.White 'Only to provide an interesting look
        Me.ThresholdPercentageToggleSwitch.OffText = "OFF" 'Only to provide an interesting look
        Me.ThresholdPercentageToggleSwitch.OffFont = New Font(Me.DemoTabControl.Font.FontFamily, 12, FontStyle.Bold) 'Only to provide an interesting look
        Me.ThresholdPercentageToggleSwitch.OffForeColor = Color.FromArgb(141, 123, 141) 'Only to provide an interesting look
        Me.ThresholdPercentageToggleSwitch.ThresholdPercentage = 50 'Default

        'ToggleOnButtonClick & ToggleOnSideClick example:

        Me.ToggleOnClickToggleSwitch.Style = ToggleSwitchStyle.BrushedMetal 'Only to provide an interesting look
        Me.ToggleOnClickToggleSwitch.Size = New Size(93, 30) 'Only to provide an interesting look
        Me.ToggleOnClickToggleSwitch.OnText = "ON" 'Only to provide an interesting look
        Me.ToggleOnClickToggleSwitch.OnFont = New Font(Me.DemoTabControl.Font.FontFamily, 10, FontStyle.Bold) 'Only to provide an interesting look
        Me.ToggleOnClickToggleSwitch.OnForeColor = Color.White 'Only to provide an interesting look
        Me.ToggleOnClickToggleSwitch.OffText = "OFF" 'Only to provide an interesting look
        Me.ToggleOnClickToggleSwitch.OffFont = New Font(Me.DemoTabControl.Font.FontFamily, 10, FontStyle.Bold) 'Only to provide an interesting look
        Me.ToggleOnClickToggleSwitch.OffForeColor = Color.White 'Only to provide an interesting look
        Me.ToggleOnClickToggleSwitch.ToggleOnButtonClick = True 'Default
        Me.ToggleOnClickToggleSwitch.ToggleOnSideClick = True 'Default
    End Sub

    Public Sub SetPropertiesForCustomizationsTabSwitches()
        'Set the properties for the ToggleSwitches on the "Special Customizations" tab

        'Color customization example, Metro Style ToggleSwitch:

        Me.NormalMetroToggleSwitch.Style = ToggleSwitchStyle.Metro 'Default
        Me.NormalMetroToggleSwitch.Size = New Size(75, 23)

        Dim customizedMetroRenderer As New ToggleSwitchMetroRenderer() With {
            .LeftSideColor = Color.Red,
            .LeftSideColorHovered = Color.FromArgb(210, 0, 0),
            .LeftSideColorPressed = Color.FromArgb(190, 0, 0),
            .RightSideColor = Color.Yellow,
            .RightSideColorHovered = Color.FromArgb(245, 245, 0),
            .RightSideColorPressed = Color.FromArgb(235, 235, 0)}

        Me.CustomizedMetroToggleSwitch.Style = ToggleSwitchStyle.Metro 'Default
        Me.CustomizedMetroToggleSwitch.Size = New Size(75, 23)
        Me.CustomizedMetroToggleSwitch.SetRenderer(customizedMetroRenderer)

        'Color customization example, IOS5 Style ToggleSwitch:

        Me.NormalIOS5ToggleSwitch.Style = ToggleSwitchStyle.Ios5
        Me.NormalIOS5ToggleSwitch.Size = New Size(98, 42)
        Me.NormalIOS5ToggleSwitch.OnText = "ON"
        Me.NormalIOS5ToggleSwitch.OnFont = New Font(Me.DemoTabControl.Font.FontFamily, 12, FontStyle.Bold)
        Me.NormalIOS5ToggleSwitch.OnForeColor = Color.White
        Me.NormalIOS5ToggleSwitch.OffText = "OFF"
        Me.NormalIOS5ToggleSwitch.OffFont = New Font(Me.DemoTabControl.Font.FontFamily, 12, FontStyle.Bold)
        Me.NormalIOS5ToggleSwitch.OffForeColor = Color.FromArgb(141, 123, 141)

        'Maybe not the prettiest color scheme in the world - It's just for demonstration :-)
        Dim customizedIos5Renderer As New ToggleSwitchIos5Renderer() With {
            .LeftSideUpperColor1 = Color.FromArgb(128, 0, 64),
            .LeftSideUpperColor2 = Color.FromArgb(180, 0, 90),
            .LeftSideLowerColor1 = Color.FromArgb(250, 0, 125),
            .LeftSideLowerColor2 = Color.FromArgb(255, 120, 190),
            .RightSideUpperColor1 = Color.FromArgb(0, 64, 128),
            .RightSideUpperColor2 = Color.FromArgb(0, 90, 180),
            .RightSideLowerColor1 = Color.FromArgb(0, 125, 250),
            .RightSideLowerColor2 = Color.FromArgb(120, 190, 255),
            .ButtonNormalOuterBorderColor = Color.Green,
            .ButtonNormalInnerBorderColor = Color.Green,
            .ButtonNormalSurfaceColor1 = Color.Red,
            .ButtonNormalSurfaceColor2 = Color.Red,
            .ButtonHoverOuterBorderColor = Color.Green,
            .ButtonHoverInnerBorderColor = Color.Green,
            .ButtonHoverSurfaceColor1 = Color.Red,
            .ButtonHoverSurfaceColor2 = Color.Red,
            .ButtonPressedOuterBorderColor = Color.Green,
            .ButtonPressedInnerBorderColor = Color.Green,
            .ButtonPressedSurfaceColor1 = Color.Red,
            .ButtonPressedSurfaceColor2 = Color.Red}

        Me.CustomizedIOS5ToggleSwitch.Style = ToggleSwitchStyle.Ios5
        Me.CustomizedIOS5ToggleSwitch.Size = New Size(98, 42)
        Me.CustomizedIOS5ToggleSwitch.OnText = "ON"
        Me.CustomizedIOS5ToggleSwitch.OnFont = New Font(Me.DemoTabControl.Font.FontFamily, 12, FontStyle.Bold)
        Me.CustomizedIOS5ToggleSwitch.OnForeColor = Color.White
        Me.CustomizedIOS5ToggleSwitch.OffText = "OFF"
        Me.CustomizedIOS5ToggleSwitch.OffFont = New Font(Me.DemoTabControl.Font.FontFamily, 12, FontStyle.Bold)
        Me.CustomizedIOS5ToggleSwitch.OffForeColor = Color.White 'OBS: Need to change this for text visibility
        Me.CustomizedIOS5ToggleSwitch.SetRenderer(customizedIos5Renderer)

        'Image customization example, Fancy Style ToggleSwitch:

        Me.NormalFancyToggleSwitch.Style = ToggleSwitchStyle.Fancy
        Me.NormalFancyToggleSwitch.Size = New Size(100, 30)
        Me.NormalFancyToggleSwitch.OnText = "ON"
        Me.NormalFancyToggleSwitch.OnFont = New Font(Me.DemoTabControl.Font.FontFamily, 10, FontStyle.Bold)
        Me.NormalFancyToggleSwitch.OnForeColor = Color.White
        Me.NormalFancyToggleSwitch.OffText = "OFF"
        Me.NormalFancyToggleSwitch.OffFont = New Font(Me.DemoTabControl.Font.FontFamily, 10, FontStyle.Bold)
        Me.NormalFancyToggleSwitch.OffForeColor = Color.White

        Me.CustomizedFancyToggleSwitch.Style = ToggleSwitchStyle.Fancy
        Me.CustomizedFancyToggleSwitch.Size = New Size(100, 30)
        Me.CustomizedFancyToggleSwitch.OffButtonImage = My.Resources.arrowright
        Me.CustomizedFancyToggleSwitch.OffSideImage = My.Resources.cross
        Me.CustomizedFancyToggleSwitch.OnButtonImage = My.Resources.arrowleft
        Me.CustomizedFancyToggleSwitch.OnSideImage = My.Resources.check

        'Advanced behavior example, Fancy Style ToggleSwitch:

        Dim tempColor As Color

        Dim customizedFancyRenderer As New ToggleSwitchFancyRenderer()
        tempColor = customizedFancyRenderer.LeftSideBackColor1
        customizedFancyRenderer.LeftSideBackColor1 = customizedFancyRenderer.RightSideBackColor1
        customizedFancyRenderer.RightSideBackColor1 = tempColor
        tempColor = customizedFancyRenderer.LeftSideBackColor2
        customizedFancyRenderer.LeftSideBackColor2 = customizedFancyRenderer.RightSideBackColor2
        customizedFancyRenderer.RightSideBackColor2 = tempColor

        Me.AdvancedBehaviorFancyToggleSwitch.Style = ToggleSwitchStyle.Fancy
        Me.AdvancedBehaviorFancyToggleSwitch.Size = New Size(150, 30)
        Me.AdvancedBehaviorFancyToggleSwitch.OnText = "Restart"
        Me.AdvancedBehaviorFancyToggleSwitch.OnFont = New Font(Me.DemoTabControl.Font.FontFamily, 10, FontStyle.Bold)
        Me.AdvancedBehaviorFancyToggleSwitch.OnForeColor = Color.White
        Me.AdvancedBehaviorFancyToggleSwitch.OffText = "Online"
        Me.AdvancedBehaviorFancyToggleSwitch.OffFont = New Font(Me.DemoTabControl.Font.FontFamily, 10, FontStyle.Bold)
        Me.AdvancedBehaviorFancyToggleSwitch.OffForeColor = Color.White
        Me.AdvancedBehaviorFancyToggleSwitch.OffButtonImage = My.Resources.arrowright
        Me.AdvancedBehaviorFancyToggleSwitch.UseAnimation = False
        Me.AdvancedBehaviorFancyToggleSwitch.SetRenderer(customizedFancyRenderer)
        AddHandler Me.AdvancedBehaviorFancyToggleSwitch.CheckedChanged, AddressOf Me.AdvancedBehaviorFancyToggleSwitch_CheckedChanged

        Me.AnimatedGifPictureBox.Parent = Me.AdvancedBehaviorFancyToggleSwitch 'Necessary to get the ToggleSwitch button to show through the picture box' transparent background
    End Sub

    Private Sub AllowUserChangeCheckBox_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles AllowUserChangeCheckBox.CheckedChanged
        Me.AllowUserChangeToggleSwitch1.Checked = Me.AllowUserChangeCheckBox.Checked
        Me.AllowUserChangeToggleSwitch2.Checked = Me.AllowUserChangeCheckBox.Checked
    End Sub

    Private Sub GrayWhenDisabledCheckBox_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GrayWhenDisabledCheckBox.CheckedChanged
        Me.GrayWhenDisabledToggleSwitch1.Enabled = Me.GrayWhenDisabledCheckBox.Checked
        Me.GrayWhenDisabledToggleSwitch2.Enabled = Me.GrayWhenDisabledCheckBox.Checked
    End Sub

    Private Sub ThresholdPercentageTrackBar_Scroll(ByVal sender As Object, ByVal e As System.EventArgs) Handles ThresholdPercentageTrackBar.Scroll
        Me.label15.Text = $"Value = {Me.ThresholdPercentageTrackBar.Value} (Default = 50)"
        Me.ThresholdPercentageToggleSwitch.ThresholdPercentage = Me.ThresholdPercentageTrackBar.Value
    End Sub

    Private Sub ToggleOnButtonClickCheckBox_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ToggleOnButtonClickCheckBox.CheckedChanged
        Me.ToggleOnClickToggleSwitch.ToggleOnButtonClick = Me.ToggleOnButtonClickCheckBox.Checked
    End Sub

    Private Sub ToggleOnSideClickCheckBox_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ToggleOnSideClickCheckBox.CheckedChanged
        Me.ToggleOnClickToggleSwitch.ToggleOnSideClick = Me.ToggleOnSideClickCheckBox.Checked
    End Sub

    Private Sub AdvancedBehaviorFancyToggleSwitch_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Me.AdvancedBehaviorFancyToggleSwitch.Checked Then
            Me.AdvancedBehaviorFancyToggleSwitch.AllowUserChange = False
            Me.AdvancedBehaviorFancyToggleSwitch.OnText = "Restarting..."

            Me.PositionAniGifPictureBox()
            Me.AnimatedGifPictureBox.Visible = True

            If Not Me.SimulateRestartBackgroundWorker.IsBusy Then
                Me.SimulateRestartBackgroundWorker.RunWorkerAsync()
            End If
        Else
            Me.AdvancedBehaviorFancyToggleSwitch.AllowUserChange = True
            Me.AdvancedBehaviorFancyToggleSwitch.OnText = "Restart"
        End If
    End Sub

    Private Sub PositionAniGifPictureBox()
        'Position anigif picturebox

        Dim buttonRectangle As Rectangle = Me.AdvancedBehaviorFancyToggleSwitch.ButtonRectangle

        Me.AnimatedGifPictureBox.Height = buttonRectangle.Height - 2
        Me.AnimatedGifPictureBox.Width = Me.AnimatedGifPictureBox.Height
        Me.AnimatedGifPictureBox.Left = buttonRectangle.X + ((buttonRectangle.Width - Me.AnimatedGifPictureBox.Width) \ 2)
        Me.AnimatedGifPictureBox.Top = buttonRectangle.Y + ((buttonRectangle.Height - Me.AnimatedGifPictureBox.Height) \ 2)
    End Sub

    Private Sub SimulateRestartBackgroundWorker_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles SimulateRestartBackgroundWorker.DoWork
        'Simulate restart delay
        Thread.Sleep(1500)
    End Sub

    Private Sub SimulateRestartBackgroundWorker_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles SimulateRestartBackgroundWorker.RunWorkerCompleted
        Me.AnimatedGifPictureBox.Visible = False
        Me.AdvancedBehaviorFancyToggleSwitch.Checked = False
    End Sub
End Class
