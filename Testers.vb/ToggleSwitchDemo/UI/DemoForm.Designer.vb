﻿
Partial Public Class DemoForm
    ''' <summary>
    ''' Required designer variable.
    ''' </summary>
    Private components As System.ComponentModel.IContainer = Nothing

    ''' <summary>
    ''' Clean up any resources being used.
    ''' </summary>
    ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso (components IsNot Nothing) Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

#Region "Windows Form Designer generated code"

    ''' <summary>
    ''' Required method for Designer support - do not modify
    ''' the contents of this method with the code editor.
    ''' </summary>
    Private Sub InitializeComponent()
        Dim resources As New System.ComponentModel.ComponentResourceManager(GetType(DemoForm))
        Me.TopPanel = New System.Windows.Forms.Panel()
        Me.InfoLabel = New System.Windows.Forms.Label()
        Me.DemoTabControl = New System.Windows.Forms.TabControl()
        Me.StylesTabPage = New System.Windows.Forms.TabPage()
        Me.OSXStyleOnLabel = New System.Windows.Forms.Label()
        Me.OSXStyleOffLabel = New System.Windows.Forms.Label()
        Me.FancyStyleToggleSwitch = New isr.Core.Controls.ToggleSwitch()
        Me.OSXStyleToggleSwitch = New isr.Core.Controls.ToggleSwitch()
        Me.CarbonStyleToggleSwitch = New isr.Core.Controls.ToggleSwitch()
        Me.ModernStyleToggleSwitch = New isr.Core.Controls.ToggleSwitch()
        Me.IphoneStyleToggleSwitch = New isr.Core.Controls.ToggleSwitch()
        Me.BrushedMetalStyleToggleSwitch = New isr.Core.Controls.ToggleSwitch()
        Me.AndroidStyleToggleSwitch = New isr.Core.Controls.ToggleSwitch()
        Me.IOS5StyleToggleSwitch = New isr.Core.Controls.ToggleSwitch()
        Me.MetroStyleToggleSwitch = New isr.Core.Controls.ToggleSwitch()
        Me.OSXStyleLabel = New System.Windows.Forms.Label()
        Me.ModernStyleLabel = New System.Windows.Forms.Label()
        Me.IphoneStyleLabel = New System.Windows.Forms.Label()
        Me.IOS5StyleLabel = New System.Windows.Forms.Label()
        Me.FancyStyleLabel = New System.Windows.Forms.Label()
        Me.CarbonStyleLabel = New System.Windows.Forms.Label()
        Me.BrushedMetalStyleLabel = New System.Windows.Forms.Label()
        Me.AndroidStyleLabel = New System.Windows.Forms.Label()
        Me.MetroStyleLabel = New System.Windows.Forms.Label()
        Me.SemiImportantPropertiesTabPage = New System.Windows.Forms.TabPage()
        Me.label19 = New System.Windows.Forms.Label()
        Me.ToggleOnSideClickCheckBox = New System.Windows.Forms.CheckBox()
        Me.ToggleOnButtonClickCheckBox = New System.Windows.Forms.CheckBox()
        Me.label18 = New System.Windows.Forms.Label()
        Me.ToggleOnClickToggleSwitch = New isr.Core.Controls.ToggleSwitch()
        Me.label17 = New System.Windows.Forms.Label()
        Me.label16 = New System.Windows.Forms.Label()
        Me.label15 = New System.Windows.Forms.Label()
        Me.ThresholdPercentageTrackBar = New System.Windows.Forms.TrackBar()
        Me.ThresholdPercentageToggleSwitch = New isr.Core.Controls.ToggleSwitch()
        Me.label11 = New System.Windows.Forms.Label()
        Me.label12 = New System.Windows.Forms.Label()
        Me.GrayWhenDisabledCheckBox = New System.Windows.Forms.CheckBox()
        Me.label13 = New System.Windows.Forms.Label()
        Me.label14 = New System.Windows.Forms.Label()
        Me.GrayWhenDisabledToggleSwitch2 = New isr.Core.Controls.ToggleSwitch()
        Me.GrayWhenDisabledToggleSwitch1 = New isr.Core.Controls.ToggleSwitch()
        Me.label10 = New System.Windows.Forms.Label()
        Me.label9 = New System.Windows.Forms.Label()
        Me.label8 = New System.Windows.Forms.Label()
        Me.SlowAnimationToggleSwitch = New isr.Core.Controls.ToggleSwitch()
        Me.label7 = New System.Windows.Forms.Label()
        Me.FastAnimationToggleSwitch = New isr.Core.Controls.ToggleSwitch()
        Me.label6 = New System.Windows.Forms.Label()
        Me.NoAnimationToggleSwitch = New isr.Core.Controls.ToggleSwitch()
        Me.label5 = New System.Windows.Forms.Label()
        Me.label4 = New System.Windows.Forms.Label()
        Me.label3 = New System.Windows.Forms.Label()
        Me.AllowUserChangeCheckBox = New System.Windows.Forms.CheckBox()
        Me.label2 = New System.Windows.Forms.Label()
        Me.label1 = New System.Windows.Forms.Label()
        Me.AllowUserChangeToggleSwitch2 = New isr.Core.Controls.ToggleSwitch()
        Me.AllowUserChangeToggleSwitch1 = New isr.Core.Controls.ToggleSwitch()
        Me.AllowUserChangeLabel = New System.Windows.Forms.Label()
        Me.SpecialCustomizationsTabPage = New System.Windows.Forms.TabPage()
        Me.AnimatedGifPictureBox = New System.Windows.Forms.PictureBox()
        Me.AdvancedBehaviorFancyToggleSwitch = New isr.Core.Controls.ToggleSwitch()
        Me.label28 = New System.Windows.Forms.Label()
        Me.label26 = New System.Windows.Forms.Label()
        Me.label27 = New System.Windows.Forms.Label()
        Me.CustomizedFancyToggleSwitch = New isr.Core.Controls.ToggleSwitch()
        Me.NormalFancyToggleSwitch = New isr.Core.Controls.ToggleSwitch()
        Me.label25 = New System.Windows.Forms.Label()
        Me.label23 = New System.Windows.Forms.Label()
        Me.label24 = New System.Windows.Forms.Label()
        Me.CustomizedIOS5ToggleSwitch = New isr.Core.Controls.ToggleSwitch()
        Me.NormalIOS5ToggleSwitch = New isr.Core.Controls.ToggleSwitch()
        Me.label22 = New System.Windows.Forms.Label()
        Me.label21 = New System.Windows.Forms.Label()
        Me.CustomizedMetroToggleSwitch = New isr.Core.Controls.ToggleSwitch()
        Me.NormalMetroToggleSwitch = New isr.Core.Controls.ToggleSwitch()
        Me.label20 = New System.Windows.Forms.Label()
        Me.PlaygroundTabPage = New System.Windows.Forms.TabPage()
        Me.PlaygroundToggleSwitch = New isr.Core.Controls.ToggleSwitch()
        Me.PlaygroundPropertyGrid = New System.Windows.Forms.PropertyGrid()
        Me.SimulateRestartBackgroundWorker = New System.ComponentModel.BackgroundWorker()
        Me.TopPanel.SuspendLayout()
        Me.DemoTabControl.SuspendLayout()
        Me.StylesTabPage.SuspendLayout()
        Me.SemiImportantPropertiesTabPage.SuspendLayout()
        CType(Me.ThresholdPercentageTrackBar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SpecialCustomizationsTabPage.SuspendLayout()
        CType(Me.AnimatedGifPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PlaygroundTabPage.SuspendLayout()
        Me.SuspendLayout()
        ' 
        ' TopPanel
        ' 
        Me.TopPanel.BackColor = System.Drawing.Color.White
        Me.TopPanel.Controls.Add(Me.InfoLabel)
        Me.TopPanel.Dock = System.Windows.Forms.DockStyle.Top
        Me.TopPanel.Location = New System.Drawing.Point(0, 0)
        Me.TopPanel.Name = "TopPanel"
        Me.TopPanel.Size = New System.Drawing.Size(878, 100)
        Me.TopPanel.TabIndex = 0
        ' 
        ' InfoLabel
        ' 
        Me.InfoLabel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.InfoLabel.Location = New System.Drawing.Point(0, 0)
        Me.InfoLabel.Name = "InfoLabel"
        Me.InfoLabel.Padding = New System.Windows.Forms.Padding(5)
        Me.InfoLabel.Size = New System.Drawing.Size(878, 100)
        Me.InfoLabel.TabIndex = 0
        Me.InfoLabel.Text = resources.GetString("InfoLabel.Text")
        ' 
        ' DemoTabControl
        ' 
        Me.DemoTabControl.Controls.Add(Me.StylesTabPage)
        Me.DemoTabControl.Controls.Add(Me.SemiImportantPropertiesTabPage)
        Me.DemoTabControl.Controls.Add(Me.SpecialCustomizationsTabPage)
        Me.DemoTabControl.Controls.Add(Me.PlaygroundTabPage)
        Me.DemoTabControl.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DemoTabControl.Location = New System.Drawing.Point(0, 100)
        Me.DemoTabControl.Name = "DemoTabControl"
        Me.DemoTabControl.SelectedIndex = 0
        Me.DemoTabControl.Size = New System.Drawing.Size(878, 505)
        Me.DemoTabControl.TabIndex = 1
        ' 
        ' StylesTabPage
        ' 
        Me.StylesTabPage.Controls.Add(Me.OSXStyleOnLabel)
        Me.StylesTabPage.Controls.Add(Me.OSXStyleOffLabel)
        Me.StylesTabPage.Controls.Add(Me.FancyStyleToggleSwitch)
        Me.StylesTabPage.Controls.Add(Me.OSXStyleToggleSwitch)
        Me.StylesTabPage.Controls.Add(Me.CarbonStyleToggleSwitch)
        Me.StylesTabPage.Controls.Add(Me.ModernStyleToggleSwitch)
        Me.StylesTabPage.Controls.Add(Me.IphoneStyleToggleSwitch)
        Me.StylesTabPage.Controls.Add(Me.BrushedMetalStyleToggleSwitch)
        Me.StylesTabPage.Controls.Add(Me.AndroidStyleToggleSwitch)
        Me.StylesTabPage.Controls.Add(Me.IOS5StyleToggleSwitch)
        Me.StylesTabPage.Controls.Add(Me.MetroStyleToggleSwitch)
        Me.StylesTabPage.Controls.Add(Me.OSXStyleLabel)
        Me.StylesTabPage.Controls.Add(Me.ModernStyleLabel)
        Me.StylesTabPage.Controls.Add(Me.IphoneStyleLabel)
        Me.StylesTabPage.Controls.Add(Me.IOS5StyleLabel)
        Me.StylesTabPage.Controls.Add(Me.FancyStyleLabel)
        Me.StylesTabPage.Controls.Add(Me.CarbonStyleLabel)
        Me.StylesTabPage.Controls.Add(Me.BrushedMetalStyleLabel)
        Me.StylesTabPage.Controls.Add(Me.AndroidStyleLabel)
        Me.StylesTabPage.Controls.Add(Me.MetroStyleLabel)
        Me.StylesTabPage.Location = New System.Drawing.Point(4, 22)
        Me.StylesTabPage.Name = "StylesTabPage"
        Me.StylesTabPage.Padding = New System.Windows.Forms.Padding(3)
        Me.StylesTabPage.Size = New System.Drawing.Size(870, 479)
        Me.StylesTabPage.TabIndex = 0
        Me.StylesTabPage.Text = "Styles"
        Me.StylesTabPage.UseVisualStyleBackColor = True
        ' 
        ' OSXStyleOnLabel
        ' 
        Me.OSXStyleOnLabel.AutoSize = True
        Me.OSXStyleOnLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.OSXStyleOnLabel.Location = New System.Drawing.Point(530, 312)
        Me.OSXStyleOnLabel.Name = "OSXStyleOnLabel"
        Me.OSXStyleOnLabel.Size = New System.Drawing.Size(31, 17)
        Me.OSXStyleOnLabel.TabIndex = 19
        Me.OSXStyleOnLabel.Text = "ON"
        ' 
        ' OSXStyleOffLabel
        ' 
        Me.OSXStyleOffLabel.AutoSize = True
        Me.OSXStyleOffLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.OSXStyleOffLabel.Location = New System.Drawing.Point(392, 312)
        Me.OSXStyleOffLabel.Name = "OSXStyleOffLabel"
        Me.OSXStyleOffLabel.Size = New System.Drawing.Size(38, 17)
        Me.OSXStyleOffLabel.TabIndex = 18
        Me.OSXStyleOffLabel.Text = "OFF"
        ' 
        ' FancyStyleToggleSwitch
        ' 
        Me.FancyStyleToggleSwitch.Location = New System.Drawing.Point(626, 309)
        Me.FancyStyleToggleSwitch.Name = "FancyStyleToggleSwitch"
        Me.FancyStyleToggleSwitch.OffFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.FancyStyleToggleSwitch.OnFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.FancyStyleToggleSwitch.Size = New System.Drawing.Size(50, 19)
        Me.FancyStyleToggleSwitch.TabIndex = 17
        ' 
        ' OSXStyleToggleSwitch
        ' 
        Me.OSXStyleToggleSwitch.Location = New System.Drawing.Point(434, 309)
        Me.OSXStyleToggleSwitch.Name = "OSXStyleToggleSwitch"
        Me.OSXStyleToggleSwitch.OffFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.OSXStyleToggleSwitch.OnFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.OSXStyleToggleSwitch.Size = New System.Drawing.Size(50, 19)
        Me.OSXStyleToggleSwitch.TabIndex = 16
        ' 
        ' CarbonStyleToggleSwitch
        ' 
        Me.CarbonStyleToggleSwitch.Location = New System.Drawing.Point(164, 309)
        Me.CarbonStyleToggleSwitch.Name = "CarbonStyleToggleSwitch"
        Me.CarbonStyleToggleSwitch.OffFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.CarbonStyleToggleSwitch.OnFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.CarbonStyleToggleSwitch.Size = New System.Drawing.Size(50, 19)
        Me.CarbonStyleToggleSwitch.TabIndex = 15
        ' 
        ' ModernStyleToggleSwitch
        ' 
        Me.ModernStyleToggleSwitch.Location = New System.Drawing.Point(626, 188)
        Me.ModernStyleToggleSwitch.Name = "ModernStyleToggleSwitch"
        Me.ModernStyleToggleSwitch.OffFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.ModernStyleToggleSwitch.OnFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.ModernStyleToggleSwitch.Size = New System.Drawing.Size(50, 19)
        Me.ModernStyleToggleSwitch.TabIndex = 14
        ' 
        ' IphoneStyleToggleSwitch
        ' 
        Me.IphoneStyleToggleSwitch.Location = New System.Drawing.Point(395, 188)
        Me.IphoneStyleToggleSwitch.Name = "IphoneStyleToggleSwitch"
        Me.IphoneStyleToggleSwitch.OffFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.IphoneStyleToggleSwitch.OnFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.IphoneStyleToggleSwitch.Size = New System.Drawing.Size(50, 19)
        Me.IphoneStyleToggleSwitch.TabIndex = 13
        ' 
        ' BrushedMetalStyleToggleSwitch
        ' 
        Me.BrushedMetalStyleToggleSwitch.Location = New System.Drawing.Point(164, 188)
        Me.BrushedMetalStyleToggleSwitch.Name = "BrushedMetalStyleToggleSwitch"
        Me.BrushedMetalStyleToggleSwitch.OffFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.BrushedMetalStyleToggleSwitch.OnFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.BrushedMetalStyleToggleSwitch.Size = New System.Drawing.Size(50, 19)
        Me.BrushedMetalStyleToggleSwitch.TabIndex = 12
        ' 
        ' AndroidStyleToggleSwitch
        ' 
        Me.AndroidStyleToggleSwitch.Location = New System.Drawing.Point(626, 79)
        Me.AndroidStyleToggleSwitch.Name = "AndroidStyleToggleSwitch"
        Me.AndroidStyleToggleSwitch.OffFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.AndroidStyleToggleSwitch.OnFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.AndroidStyleToggleSwitch.Size = New System.Drawing.Size(50, 19)
        Me.AndroidStyleToggleSwitch.TabIndex = 11
        ' 
        ' IOS5StyleToggleSwitch
        ' 
        Me.IOS5StyleToggleSwitch.Location = New System.Drawing.Point(395, 79)
        Me.IOS5StyleToggleSwitch.Name = "IOS5StyleToggleSwitch"
        Me.IOS5StyleToggleSwitch.OffFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.IOS5StyleToggleSwitch.OnFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.IOS5StyleToggleSwitch.Size = New System.Drawing.Size(50, 19)
        Me.IOS5StyleToggleSwitch.TabIndex = 10
        ' 
        ' MetroStyleToggleSwitch
        ' 
        Me.MetroStyleToggleSwitch.Location = New System.Drawing.Point(164, 79)
        Me.MetroStyleToggleSwitch.Name = "MetroStyleToggleSwitch"
        Me.MetroStyleToggleSwitch.OffFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.MetroStyleToggleSwitch.OnFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.MetroStyleToggleSwitch.Size = New System.Drawing.Size(50, 19)
        Me.MetroStyleToggleSwitch.TabIndex = 9
        ' 
        ' OSXStyleLabel
        ' 
        Me.OSXStyleLabel.AutoSize = True
        Me.OSXStyleLabel.Location = New System.Drawing.Point(392, 280)
        Me.OSXStyleLabel.Name = "OSXStyleLabel"
        Me.OSXStyleLabel.Size = New System.Drawing.Size(64, 13)
        Me.OSXStyleLabel.TabIndex = 8
        Me.OSXStyleLabel.Text = "Style = OSX"
        ' 
        ' ModernStyleLabel
        ' 
        Me.ModernStyleLabel.AutoSize = True
        Me.ModernStyleLabel.Location = New System.Drawing.Point(623, 161)
        Me.ModernStyleLabel.Name = "ModernStyleLabel"
        Me.ModernStyleLabel.Size = New System.Drawing.Size(78, 13)
        Me.ModernStyleLabel.TabIndex = 7
        Me.ModernStyleLabel.Text = "Style = Modern"
        ' 
        ' IphoneStyleLabel
        ' 
        Me.IphoneStyleLabel.AutoSize = True
        Me.IphoneStyleLabel.Location = New System.Drawing.Point(392, 161)
        Me.IphoneStyleLabel.Name = "IphoneStyleLabel"
        Me.IphoneStyleLabel.Size = New System.Drawing.Size(75, 13)
        Me.IphoneStyleLabel.TabIndex = 6
        Me.IphoneStyleLabel.Text = "Style = iPhone"
        ' 
        ' IOS5StyleLabel
        ' 
        Me.IOS5StyleLabel.AutoSize = True
        Me.IOS5StyleLabel.Location = New System.Drawing.Point(392, 52)
        Me.IOS5StyleLabel.Name = "IOS5StyleLabel"
        Me.IOS5StyleLabel.Size = New System.Drawing.Size(66, 13)
        Me.IOS5StyleLabel.TabIndex = 5
        Me.IOS5StyleLabel.Text = "Style = IOS5"
        ' 
        ' FancyStyleLabel
        ' 
        Me.FancyStyleLabel.AutoSize = True
        Me.FancyStyleLabel.Location = New System.Drawing.Point(623, 280)
        Me.FancyStyleLabel.Name = "FancyStyleLabel"
        Me.FancyStyleLabel.Size = New System.Drawing.Size(71, 13)
        Me.FancyStyleLabel.TabIndex = 4
        Me.FancyStyleLabel.Text = "Style = Fancy"
        ' 
        ' CarbonStyleLabel
        ' 
        Me.CarbonStyleLabel.AutoSize = True
        Me.CarbonStyleLabel.Location = New System.Drawing.Point(161, 280)
        Me.CarbonStyleLabel.Name = "CarbonStyleLabel"
        Me.CarbonStyleLabel.Size = New System.Drawing.Size(76, 13)
        Me.CarbonStyleLabel.TabIndex = 3
        Me.CarbonStyleLabel.Text = "Style = Carbon"
        ' 
        ' BrushedMetalStyleLabel
        ' 
        Me.BrushedMetalStyleLabel.AutoSize = True
        Me.BrushedMetalStyleLabel.Location = New System.Drawing.Point(161, 161)
        Me.BrushedMetalStyleLabel.Name = "BrushedMetalStyleLabel"
        Me.BrushedMetalStyleLabel.Size = New System.Drawing.Size(107, 13)
        Me.BrushedMetalStyleLabel.TabIndex = 2
        Me.BrushedMetalStyleLabel.Text = "Style = Brushed Metal"
        ' 
        ' AndroidStyleLabel
        ' 
        Me.AndroidStyleLabel.AutoSize = True
        Me.AndroidStyleLabel.Location = New System.Drawing.Point(623, 52)
        Me.AndroidStyleLabel.Name = "AndroidStyleLabel"
        Me.AndroidStyleLabel.Size = New System.Drawing.Size(78, 13)
        Me.AndroidStyleLabel.TabIndex = 1
        Me.AndroidStyleLabel.Text = "Style = Android"
        ' 
        ' MetroStyleLabel
        ' 
        Me.MetroStyleLabel.AutoSize = True
        Me.MetroStyleLabel.Location = New System.Drawing.Point(161, 52)
        Me.MetroStyleLabel.Name = "MetroStyleLabel"
        Me.MetroStyleLabel.Size = New System.Drawing.Size(69, 13)
        Me.MetroStyleLabel.TabIndex = 0
        Me.MetroStyleLabel.Text = "Style = Metro"
        ' 
        ' SemiImportantPropertiesTabPage
        ' 
        Me.SemiImportantPropertiesTabPage.Controls.Add(Me.label19)
        Me.SemiImportantPropertiesTabPage.Controls.Add(Me.ToggleOnSideClickCheckBox)
        Me.SemiImportantPropertiesTabPage.Controls.Add(Me.ToggleOnButtonClickCheckBox)
        Me.SemiImportantPropertiesTabPage.Controls.Add(Me.label18)
        Me.SemiImportantPropertiesTabPage.Controls.Add(Me.ToggleOnClickToggleSwitch)
        Me.SemiImportantPropertiesTabPage.Controls.Add(Me.label17)
        Me.SemiImportantPropertiesTabPage.Controls.Add(Me.label16)
        Me.SemiImportantPropertiesTabPage.Controls.Add(Me.label15)
        Me.SemiImportantPropertiesTabPage.Controls.Add(Me.ThresholdPercentageTrackBar)
        Me.SemiImportantPropertiesTabPage.Controls.Add(Me.ThresholdPercentageToggleSwitch)
        Me.SemiImportantPropertiesTabPage.Controls.Add(Me.label11)
        Me.SemiImportantPropertiesTabPage.Controls.Add(Me.label12)
        Me.SemiImportantPropertiesTabPage.Controls.Add(Me.GrayWhenDisabledCheckBox)
        Me.SemiImportantPropertiesTabPage.Controls.Add(Me.label13)
        Me.SemiImportantPropertiesTabPage.Controls.Add(Me.label14)
        Me.SemiImportantPropertiesTabPage.Controls.Add(Me.GrayWhenDisabledToggleSwitch2)
        Me.SemiImportantPropertiesTabPage.Controls.Add(Me.GrayWhenDisabledToggleSwitch1)
        Me.SemiImportantPropertiesTabPage.Controls.Add(Me.label10)
        Me.SemiImportantPropertiesTabPage.Controls.Add(Me.label9)
        Me.SemiImportantPropertiesTabPage.Controls.Add(Me.label8)
        Me.SemiImportantPropertiesTabPage.Controls.Add(Me.SlowAnimationToggleSwitch)
        Me.SemiImportantPropertiesTabPage.Controls.Add(Me.label7)
        Me.SemiImportantPropertiesTabPage.Controls.Add(Me.FastAnimationToggleSwitch)
        Me.SemiImportantPropertiesTabPage.Controls.Add(Me.label6)
        Me.SemiImportantPropertiesTabPage.Controls.Add(Me.NoAnimationToggleSwitch)
        Me.SemiImportantPropertiesTabPage.Controls.Add(Me.label5)
        Me.SemiImportantPropertiesTabPage.Controls.Add(Me.label4)
        Me.SemiImportantPropertiesTabPage.Controls.Add(Me.label3)
        Me.SemiImportantPropertiesTabPage.Controls.Add(Me.AllowUserChangeCheckBox)
        Me.SemiImportantPropertiesTabPage.Controls.Add(Me.label2)
        Me.SemiImportantPropertiesTabPage.Controls.Add(Me.label1)
        Me.SemiImportantPropertiesTabPage.Controls.Add(Me.AllowUserChangeToggleSwitch2)
        Me.SemiImportantPropertiesTabPage.Controls.Add(Me.AllowUserChangeToggleSwitch1)
        Me.SemiImportantPropertiesTabPage.Controls.Add(Me.AllowUserChangeLabel)
        Me.SemiImportantPropertiesTabPage.Location = New System.Drawing.Point(4, 22)
        Me.SemiImportantPropertiesTabPage.Name = "SemiImportantPropertiesTabPage"
        Me.SemiImportantPropertiesTabPage.Padding = New System.Windows.Forms.Padding(3)
        Me.SemiImportantPropertiesTabPage.Size = New System.Drawing.Size(870, 479)
        Me.SemiImportantPropertiesTabPage.TabIndex = 1
        Me.SemiImportantPropertiesTabPage.Text = "(Semi)-Important Properties"
        Me.SemiImportantPropertiesTabPage.UseVisualStyleBackColor = True
        ' 
        ' label19
        ' 
        Me.label19.AutoSize = True
        Me.label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.label19.Location = New System.Drawing.Point(427, 444)
        Me.label19.Name = "label19"
        Me.label19.Size = New System.Drawing.Size(67, 13)
        Me.label19.TabIndex = 42
        Me.label19.Text = "both or none"
        ' 
        ' ToggleOnSideClickCheckBox
        ' 
        Me.ToggleOnSideClickCheckBox.AutoSize = True
        Me.ToggleOnSideClickCheckBox.Checked = True
        Me.ToggleOnSideClickCheckBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ToggleOnSideClickCheckBox.Location = New System.Drawing.Point(205, 444)
        Me.ToggleOnSideClickCheckBox.Name = "ToggleOnSideClickCheckBox"
        Me.ToggleOnSideClickCheckBox.Size = New System.Drawing.Size(117, 17)
        Me.ToggleOnSideClickCheckBox.TabIndex = 41
        Me.ToggleOnSideClickCheckBox.Text = "Toggle On Side Click"
        Me.ToggleOnSideClickCheckBox.UseVisualStyleBackColor = True
        '			Me.ToggleOnSideClickCheckBox.CheckedChanged += New System.EventHandler(Me.ToggleOnSideClickCheckBox_CheckedChanged)
        ' 
        ' ToggleOnButtonClickCheckBox
        ' 
        Me.ToggleOnButtonClickCheckBox.AutoSize = True
        Me.ToggleOnButtonClickCheckBox.Checked = True
        Me.ToggleOnButtonClickCheckBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ToggleOnButtonClickCheckBox.Location = New System.Drawing.Point(205, 421)
        Me.ToggleOnButtonClickCheckBox.Name = "ToggleOnButtonClickCheckBox"
        Me.ToggleOnButtonClickCheckBox.Size = New System.Drawing.Size(127, 17)
        Me.ToggleOnButtonClickCheckBox.TabIndex = 40
        Me.ToggleOnButtonClickCheckBox.Text = "Toggle On Button Click"
        Me.ToggleOnButtonClickCheckBox.UseVisualStyleBackColor = True
        '			Me.ToggleOnButtonClickCheckBox.CheckedChanged += New System.EventHandler(Me.ToggleOnButtonClickCheckBox_CheckedChanged)
        ' 
        ' label18
        ' 
        Me.label18.AutoSize = True
        Me.label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.label18.Location = New System.Drawing.Point(427, 419)
        Me.label18.Name = "label18"
        Me.label18.Size = New System.Drawing.Size(436, 13)
        Me.label18.TabIndex = 39
        Me.label18.Text = "Determines where you have to click to toggle the switch, on the button, besides t" & "he button,"
        ' 
        ' ToggleOnClickToggleSwitch
        ' 
        Me.ToggleOnClickToggleSwitch.Location = New System.Drawing.Point(11, 419)
        Me.ToggleOnClickToggleSwitch.Name = "ToggleOnClickToggleSwitch"
        Me.ToggleOnClickToggleSwitch.OffFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.ToggleOnClickToggleSwitch.OnFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.ToggleOnClickToggleSwitch.Size = New System.Drawing.Size(50, 19)
        Me.ToggleOnClickToggleSwitch.TabIndex = 38
        ' 
        ' label17
        ' 
        Me.label17.AutoSize = True
        Me.label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.label17.ForeColor = System.Drawing.Color.Gray
        Me.label17.Location = New System.Drawing.Point(8, 384)
        Me.label17.Name = "label17"
        Me.label17.Size = New System.Drawing.Size(399, 17)
        Me.label17.TabIndex = 37
        Me.label17.Text = "Toggle On Button Click (boolean), Toggle On Side Click (boolean)"
        ' 
        ' label16
        ' 
        Me.label16.AutoSize = True
        Me.label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.label16.Location = New System.Drawing.Point(427, 336)
        Me.label16.Name = "label16"
        Me.label16.Size = New System.Drawing.Size(410, 13)
        Me.label16.TabIndex = 36
        Me.label16.Text = "Determines how far you have to drag the slider button before it snaps to the othe" & "r side"
        ' 
        ' label15
        ' 
        Me.label15.AutoSize = True
        Me.label15.Location = New System.Drawing.Point(266, 336)
        Me.label15.Name = "label15"
        Me.label15.Size = New System.Drawing.Size(125, 13)
        Me.label15.TabIndex = 35
        Me.label15.Text = "Value = 50 (Default = 50)"
        ' 
        ' ThresholdPercentageTrackBar
        ' 
        Me.ThresholdPercentageTrackBar.BackColor = System.Drawing.Color.White
        Me.ThresholdPercentageTrackBar.Location = New System.Drawing.Point(145, 336)
        Me.ThresholdPercentageTrackBar.Maximum = 100
        Me.ThresholdPercentageTrackBar.Name = "ThresholdPercentageTrackBar"
        Me.ThresholdPercentageTrackBar.Size = New System.Drawing.Size(104, 45)
        Me.ThresholdPercentageTrackBar.TabIndex = 34
        Me.ThresholdPercentageTrackBar.TickFrequency = 10
        Me.ThresholdPercentageTrackBar.Value = 50
        '			Me.ThresholdPercentageTrackBar.Scroll += New System.EventHandler(Me.ThresholdPercentageTrackBar_Scroll)
        ' 
        ' ThresholdPercentageToggleSwitch
        ' 
        Me.ThresholdPercentageToggleSwitch.Location = New System.Drawing.Point(11, 336)
        Me.ThresholdPercentageToggleSwitch.Name = "ThresholdPercentageToggleSwitch"
        Me.ThresholdPercentageToggleSwitch.OffFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.ThresholdPercentageToggleSwitch.OnFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.ThresholdPercentageToggleSwitch.Size = New System.Drawing.Size(50, 19)
        Me.ThresholdPercentageToggleSwitch.TabIndex = 33
        ' 
        ' label11
        ' 
        Me.label11.AutoSize = True
        Me.label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.label11.ForeColor = System.Drawing.Color.Gray
        Me.label11.Location = New System.Drawing.Point(8, 305)
        Me.label11.Name = "label11"
        Me.label11.Size = New System.Drawing.Size(203, 17)
        Me.label11.TabIndex = 32
        Me.label11.Text = "Threshold Percentage (integer)"
        ' 
        ' label12
        ' 
        Me.label12.AutoSize = True
        Me.label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.label12.Location = New System.Drawing.Point(427, 235)
        Me.label12.Name = "label12"
        Me.label12.Size = New System.Drawing.Size(252, 13)
        Me.label12.TabIndex = 31
        Me.label12.Text = "Affects the coloring of the control when it is disabled"
        ' 
        ' GrayWhenDisabledCheckBox
        ' 
        Me.GrayWhenDisabledCheckBox.AutoSize = True
        Me.GrayWhenDisabledCheckBox.Checked = True
        Me.GrayWhenDisabledCheckBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me.GrayWhenDisabledCheckBox.Location = New System.Drawing.Point(319, 234)
        Me.GrayWhenDisabledCheckBox.Name = "GrayWhenDisabledCheckBox"
        Me.GrayWhenDisabledCheckBox.Size = New System.Drawing.Size(65, 17)
        Me.GrayWhenDisabledCheckBox.TabIndex = 30
        Me.GrayWhenDisabledCheckBox.Text = "Enabled"
        Me.GrayWhenDisabledCheckBox.UseVisualStyleBackColor = True
        '			Me.GrayWhenDisabledCheckBox.CheckedChanged += New System.EventHandler(Me.GrayWhenDisabledCheckBox_CheckedChanged)
        ' 
        ' label13
        ' 
        Me.label13.AutoSize = True
        Me.label13.Location = New System.Drawing.Point(116, 268)
        Me.label13.Name = "label13"
        Me.label13.Size = New System.Drawing.Size(172, 13)
        Me.label13.TabIndex = 29
        Me.label13.Text = "Gray When Disabled = true (Default)"
        ' 
        ' label14
        ' 
        Me.label14.AutoSize = True
        Me.label14.Location = New System.Drawing.Point(116, 234)
        Me.label14.Name = "label14"
        Me.label14.Size = New System.Drawing.Size(133, 13)
        Me.label14.TabIndex = 28
        Me.label14.Text = "Gray When Disabled = false"
        ' 
        ' GrayWhenDisabledToggleSwitch2
        ' 
        Me.GrayWhenDisabledToggleSwitch2.Location = New System.Drawing.Point(11, 262)
        Me.GrayWhenDisabledToggleSwitch2.Name = "GrayWhenDisabledToggleSwitch2"
        Me.GrayWhenDisabledToggleSwitch2.OffFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.GrayWhenDisabledToggleSwitch2.OnFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.GrayWhenDisabledToggleSwitch2.Size = New System.Drawing.Size(50, 19)
        Me.GrayWhenDisabledToggleSwitch2.TabIndex = 27
        ' 
        ' GrayWhenDisabledToggleSwitch1
        ' 
        Me.GrayWhenDisabledToggleSwitch1.Location = New System.Drawing.Point(11, 228)
        Me.GrayWhenDisabledToggleSwitch1.Name = "GrayWhenDisabledToggleSwitch1"
        Me.GrayWhenDisabledToggleSwitch1.OffFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.GrayWhenDisabledToggleSwitch1.OnFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.GrayWhenDisabledToggleSwitch1.Size = New System.Drawing.Size(50, 19)
        Me.GrayWhenDisabledToggleSwitch1.TabIndex = 26
        ' 
        ' label10
        ' 
        Me.label10.AutoSize = True
        Me.label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.label10.ForeColor = System.Drawing.Color.Gray
        Me.label10.Location = New System.Drawing.Point(8, 194)
        Me.label10.Name = "label10"
        Me.label10.Size = New System.Drawing.Size(196, 17)
        Me.label10.TabIndex = 25
        Me.label10.Text = "Gray When Disabled (boolean)"
        ' 
        ' label9
        ' 
        Me.label9.AutoSize = True
        Me.label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.label9.Location = New System.Drawing.Point(8, 168)
        Me.label9.Name = "label9"
        Me.label9.Size = New System.Drawing.Size(681, 13)
        Me.label9.TabIndex = 24
        Me.label9.Text = "Different values for the two integer properties will affect the animation speed. " & "Animation is turned off completely using the Use Animation property"
        ' 
        ' label8
        ' 
        Me.label8.AutoSize = True
        Me.label8.Location = New System.Drawing.Point(621, 136)
        Me.label8.Name = "label8"
        Me.label8.Size = New System.Drawing.Size(78, 13)
        Me.label8.TabIndex = 23
        Me.label8.Text = "Slow animation"
        ' 
        ' SlowAnimationToggleSwitch
        ' 
        Me.SlowAnimationToggleSwitch.Location = New System.Drawing.Point(510, 130)
        Me.SlowAnimationToggleSwitch.Name = "SlowAnimationToggleSwitch"
        Me.SlowAnimationToggleSwitch.OffFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.SlowAnimationToggleSwitch.OnFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.SlowAnimationToggleSwitch.Size = New System.Drawing.Size(50, 19)
        Me.SlowAnimationToggleSwitch.TabIndex = 22
        ' 
        ' label7
        ' 
        Me.label7.AutoSize = True
        Me.label7.Location = New System.Drawing.Point(360, 136)
        Me.label7.Name = "label7"
        Me.label7.Size = New System.Drawing.Size(75, 13)
        Me.label7.TabIndex = 21
        Me.label7.Text = "Fast animation"
        ' 
        ' FastAnimationToggleSwitch
        ' 
        Me.FastAnimationToggleSwitch.Location = New System.Drawing.Point(252, 130)
        Me.FastAnimationToggleSwitch.Name = "FastAnimationToggleSwitch"
        Me.FastAnimationToggleSwitch.OffFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.FastAnimationToggleSwitch.OnFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.FastAnimationToggleSwitch.Size = New System.Drawing.Size(50, 19)
        Me.FastAnimationToggleSwitch.TabIndex = 20
        ' 
        ' label6
        ' 
        Me.label6.AutoSize = True
        Me.label6.Location = New System.Drawing.Point(123, 136)
        Me.label6.Name = "label6"
        Me.label6.Size = New System.Drawing.Size(69, 13)
        Me.label6.TabIndex = 19
        Me.label6.Text = "No animation"
        ' 
        ' NoAnimationToggleSwitch
        ' 
        Me.NoAnimationToggleSwitch.Location = New System.Drawing.Point(11, 130)
        Me.NoAnimationToggleSwitch.Name = "NoAnimationToggleSwitch"
        Me.NoAnimationToggleSwitch.OffFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.NoAnimationToggleSwitch.OnFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.NoAnimationToggleSwitch.Size = New System.Drawing.Size(50, 19)
        Me.NoAnimationToggleSwitch.TabIndex = 18
        ' 
        ' label5
        ' 
        Me.label5.AutoSize = True
        Me.label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.label5.ForeColor = System.Drawing.Color.Gray
        Me.label5.Location = New System.Drawing.Point(8, 96)
        Me.label5.Name = "label5"
        Me.label5.Size = New System.Drawing.Size(491, 17)
        Me.label5.TabIndex = 17
        Me.label5.Text = "Animation Interval (integer), Animation Step (integer), Use Animation (boolean)"
        ' 
        ' label4
        ' 
        Me.label4.AutoSize = True
        Me.label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.label4.Location = New System.Drawing.Point(427, 61)
        Me.label4.Name = "label4"
        Me.label4.Size = New System.Drawing.Size(158, 13)
        Me.label4.TabIndex = 16
        Me.label4.Text = "you cannot change it in the GUI"
        ' 
        ' label3
        ' 
        Me.label3.AutoSize = True
        Me.label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.label3.Location = New System.Drawing.Point(427, 37)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(397, 13)
        Me.label3.TabIndex = 15
        Me.label3.Text = "Notice: You can set the Checked value from code, but if Allow User Change = false,"
        ' 
        ' AllowUserChangeCheckBox
        ' 
        Me.AllowUserChangeCheckBox.AutoSize = True
        Me.AllowUserChangeCheckBox.Location = New System.Drawing.Point(288, 37)
        Me.AllowUserChangeCheckBox.Name = "AllowUserChangeCheckBox"
        Me.AllowUserChangeCheckBox.Size = New System.Drawing.Size(69, 17)
        Me.AllowUserChangeCheckBox.TabIndex = 14
        Me.AllowUserChangeCheckBox.Text = "Checked"
        Me.AllowUserChangeCheckBox.UseVisualStyleBackColor = True
        '			Me.AllowUserChangeCheckBox.CheckedChanged += New System.EventHandler(Me.AllowUserChangeCheckBox_CheckedChanged)
        ' 
        ' label2
        ' 
        Me.label2.AutoSize = True
        Me.label2.Location = New System.Drawing.Point(85, 62)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(164, 13)
        Me.label2.TabIndex = 13
        Me.label2.Text = "Allow User Change = true (Default)"
        ' 
        ' label1
        ' 
        Me.label1.AutoSize = True
        Me.label1.Location = New System.Drawing.Point(85, 37)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(125, 13)
        Me.label1.TabIndex = 12
        Me.label1.Text = "Allow User Change = false"
        ' 
        ' AllowUserChangeToggleSwitch2
        ' 
        Me.AllowUserChangeToggleSwitch2.Location = New System.Drawing.Point(11, 60)
        Me.AllowUserChangeToggleSwitch2.Name = "AllowUserChangeToggleSwitch2"
        Me.AllowUserChangeToggleSwitch2.OffFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.AllowUserChangeToggleSwitch2.OnFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.AllowUserChangeToggleSwitch2.Size = New System.Drawing.Size(50, 19)
        Me.AllowUserChangeToggleSwitch2.TabIndex = 11
        ' 
        ' AllowUserChangeToggleSwitch1
        ' 
        Me.AllowUserChangeToggleSwitch1.Location = New System.Drawing.Point(11, 35)
        Me.AllowUserChangeToggleSwitch1.Name = "AllowUserChangeToggleSwitch1"
        Me.AllowUserChangeToggleSwitch1.OffFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.AllowUserChangeToggleSwitch1.OnFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.AllowUserChangeToggleSwitch1.Size = New System.Drawing.Size(50, 19)
        Me.AllowUserChangeToggleSwitch1.TabIndex = 10
        ' 
        ' AllowUserChangeLabel
        ' 
        Me.AllowUserChangeLabel.AutoSize = True
        Me.AllowUserChangeLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.AllowUserChangeLabel.ForeColor = System.Drawing.Color.Gray
        Me.AllowUserChangeLabel.Location = New System.Drawing.Point(8, 3)
        Me.AllowUserChangeLabel.Name = "AllowUserChangeLabel"
        Me.AllowUserChangeLabel.Size = New System.Drawing.Size(184, 17)
        Me.AllowUserChangeLabel.TabIndex = 0
        Me.AllowUserChangeLabel.Text = "Allow User Change (boolean)"
        ' 
        ' SpecialCustomizationsTabPage
        ' 
        Me.SpecialCustomizationsTabPage.Controls.Add(Me.AnimatedGifPictureBox)
        Me.SpecialCustomizationsTabPage.Controls.Add(Me.AdvancedBehaviorFancyToggleSwitch)
        Me.SpecialCustomizationsTabPage.Controls.Add(Me.label28)
        Me.SpecialCustomizationsTabPage.Controls.Add(Me.label26)
        Me.SpecialCustomizationsTabPage.Controls.Add(Me.label27)
        Me.SpecialCustomizationsTabPage.Controls.Add(Me.CustomizedFancyToggleSwitch)
        Me.SpecialCustomizationsTabPage.Controls.Add(Me.NormalFancyToggleSwitch)
        Me.SpecialCustomizationsTabPage.Controls.Add(Me.label25)
        Me.SpecialCustomizationsTabPage.Controls.Add(Me.label23)
        Me.SpecialCustomizationsTabPage.Controls.Add(Me.label24)
        Me.SpecialCustomizationsTabPage.Controls.Add(Me.CustomizedIOS5ToggleSwitch)
        Me.SpecialCustomizationsTabPage.Controls.Add(Me.NormalIOS5ToggleSwitch)
        Me.SpecialCustomizationsTabPage.Controls.Add(Me.label22)
        Me.SpecialCustomizationsTabPage.Controls.Add(Me.label21)
        Me.SpecialCustomizationsTabPage.Controls.Add(Me.CustomizedMetroToggleSwitch)
        Me.SpecialCustomizationsTabPage.Controls.Add(Me.NormalMetroToggleSwitch)
        Me.SpecialCustomizationsTabPage.Controls.Add(Me.label20)
        Me.SpecialCustomizationsTabPage.Location = New System.Drawing.Point(4, 22)
        Me.SpecialCustomizationsTabPage.Name = "SpecialCustomizationsTabPage"
        Me.SpecialCustomizationsTabPage.Size = New System.Drawing.Size(870, 479)
        Me.SpecialCustomizationsTabPage.TabIndex = 2
        Me.SpecialCustomizationsTabPage.Text = "Special Customizations"
        Me.SpecialCustomizationsTabPage.UseVisualStyleBackColor = True
        ' 
        ' AnimatedGifPictureBox
        ' 
        Me.AnimatedGifPictureBox.Image = (CType(resources.GetObject("AnimatedGifPictureBox.Image"), System.Drawing.Image))
        Me.AnimatedGifPictureBox.Location = New System.Drawing.Point(171, 355)
        Me.AnimatedGifPictureBox.Name = "AnimatedGifPictureBox"
        Me.AnimatedGifPictureBox.Size = New System.Drawing.Size(36, 31)
        Me.AnimatedGifPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.AnimatedGifPictureBox.TabIndex = 2
        Me.AnimatedGifPictureBox.TabStop = False
        ' 
        ' AdvancedBehaviorFancyToggleSwitch
        ' 
        Me.AdvancedBehaviorFancyToggleSwitch.Location = New System.Drawing.Point(22, 280)
        Me.AdvancedBehaviorFancyToggleSwitch.Name = "AdvancedBehaviorFancyToggleSwitch"
        Me.AdvancedBehaviorFancyToggleSwitch.OffFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.AdvancedBehaviorFancyToggleSwitch.OnFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.AdvancedBehaviorFancyToggleSwitch.Size = New System.Drawing.Size(50, 19)
        Me.AdvancedBehaviorFancyToggleSwitch.TabIndex = 32
        ' 
        ' label28
        ' 
        Me.label28.AutoSize = True
        Me.label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.label28.ForeColor = System.Drawing.Color.Gray
        Me.label28.Location = New System.Drawing.Point(19, 237)
        Me.label28.Name = "label28"
        Me.label28.Size = New System.Drawing.Size(188, 17)
        Me.label28.TabIndex = 31
        Me.label28.Text = "Advanced Behavior Example"
        ' 
        ' label26
        ' 
        Me.label26.AutoSize = True
        Me.label26.Location = New System.Drawing.Point(349, 191)
        Me.label26.Name = "label26"
        Me.label26.Size = New System.Drawing.Size(61, 13)
        Me.label26.TabIndex = 30
        Me.label26.Text = "Customized"
        ' 
        ' label27
        ' 
        Me.label27.AutoSize = True
        Me.label27.Location = New System.Drawing.Point(126, 191)
        Me.label27.Name = "label27"
        Me.label27.Size = New System.Drawing.Size(40, 13)
        Me.label27.TabIndex = 29
        Me.label27.Text = "Normal"
        ' 
        ' CustomizedFancyToggleSwitch
        ' 
        Me.CustomizedFancyToggleSwitch.Location = New System.Drawing.Point(238, 187)
        Me.CustomizedFancyToggleSwitch.Name = "CustomizedFancyToggleSwitch"
        Me.CustomizedFancyToggleSwitch.OffFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.CustomizedFancyToggleSwitch.OnFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.CustomizedFancyToggleSwitch.Size = New System.Drawing.Size(50, 19)
        Me.CustomizedFancyToggleSwitch.TabIndex = 28
        ' 
        ' NormalFancyToggleSwitch
        ' 
        Me.NormalFancyToggleSwitch.Location = New System.Drawing.Point(22, 187)
        Me.NormalFancyToggleSwitch.Name = "NormalFancyToggleSwitch"
        Me.NormalFancyToggleSwitch.OffFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.NormalFancyToggleSwitch.OnFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.NormalFancyToggleSwitch.Size = New System.Drawing.Size(50, 19)
        Me.NormalFancyToggleSwitch.TabIndex = 27
        ' 
        ' label25
        ' 
        Me.label25.AutoSize = True
        Me.label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.label25.ForeColor = System.Drawing.Color.Gray
        Me.label25.Location = New System.Drawing.Point(19, 148)
        Me.label25.Name = "label25"
        Me.label25.Size = New System.Drawing.Size(255, 17)
        Me.label25.TabIndex = 26
        Me.label25.Text = "Images in the button and the side fields"
        ' 
        ' label23
        ' 
        Me.label23.AutoSize = True
        Me.label23.Location = New System.Drawing.Point(349, 110)
        Me.label23.Name = "label23"
        Me.label23.Size = New System.Drawing.Size(61, 13)
        Me.label23.TabIndex = 25
        Me.label23.Text = "Customized"
        ' 
        ' label24
        ' 
        Me.label24.AutoSize = True
        Me.label24.Location = New System.Drawing.Point(126, 110)
        Me.label24.Name = "label24"
        Me.label24.Size = New System.Drawing.Size(40, 13)
        Me.label24.TabIndex = 24
        Me.label24.Text = "Normal"
        ' 
        ' CustomizedIOS5ToggleSwitch
        ' 
        Me.CustomizedIOS5ToggleSwitch.Location = New System.Drawing.Point(238, 93)
        Me.CustomizedIOS5ToggleSwitch.Name = "CustomizedIOS5ToggleSwitch"
        Me.CustomizedIOS5ToggleSwitch.OffFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.CustomizedIOS5ToggleSwitch.OnFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.CustomizedIOS5ToggleSwitch.Size = New System.Drawing.Size(50, 19)
        Me.CustomizedIOS5ToggleSwitch.TabIndex = 23
        ' 
        ' NormalIOS5ToggleSwitch
        ' 
        Me.NormalIOS5ToggleSwitch.Location = New System.Drawing.Point(22, 93)
        Me.NormalIOS5ToggleSwitch.Name = "NormalIOS5ToggleSwitch"
        Me.NormalIOS5ToggleSwitch.OffFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.NormalIOS5ToggleSwitch.OnFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.NormalIOS5ToggleSwitch.Size = New System.Drawing.Size(50, 19)
        Me.NormalIOS5ToggleSwitch.TabIndex = 22
        ' 
        ' label22
        ' 
        Me.label22.AutoSize = True
        Me.label22.Location = New System.Drawing.Point(349, 52)
        Me.label22.Name = "label22"
        Me.label22.Size = New System.Drawing.Size(61, 13)
        Me.label22.TabIndex = 21
        Me.label22.Text = "Customized"
        ' 
        ' label21
        ' 
        Me.label21.AutoSize = True
        Me.label21.Location = New System.Drawing.Point(126, 52)
        Me.label21.Name = "label21"
        Me.label21.Size = New System.Drawing.Size(40, 13)
        Me.label21.TabIndex = 20
        Me.label21.Text = "Normal"
        ' 
        ' CustomizedMetroToggleSwitch
        ' 
        Me.CustomizedMetroToggleSwitch.Location = New System.Drawing.Point(238, 48)
        Me.CustomizedMetroToggleSwitch.Name = "CustomizedMetroToggleSwitch"
        Me.CustomizedMetroToggleSwitch.OffFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.CustomizedMetroToggleSwitch.OnFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.CustomizedMetroToggleSwitch.Size = New System.Drawing.Size(50, 19)
        Me.CustomizedMetroToggleSwitch.TabIndex = 11
        ' 
        ' NormalMetroToggleSwitch
        ' 
        Me.NormalMetroToggleSwitch.Location = New System.Drawing.Point(22, 48)
        Me.NormalMetroToggleSwitch.Name = "NormalMetroToggleSwitch"
        Me.NormalMetroToggleSwitch.OffFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.NormalMetroToggleSwitch.OnFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.NormalMetroToggleSwitch.Size = New System.Drawing.Size(50, 19)
        Me.NormalMetroToggleSwitch.TabIndex = 10
        ' 
        ' label20
        ' 
        Me.label20.AutoSize = True
        Me.label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.label20.ForeColor = System.Drawing.Color.Gray
        Me.label20.Location = New System.Drawing.Point(19, 14)
        Me.label20.Name = "label20"
        Me.label20.Size = New System.Drawing.Size(133, 17)
        Me.label20.TabIndex = 1
        Me.label20.Text = "Color Customization"
        ' 
        ' PlaygroundTabPage
        ' 
        Me.PlaygroundTabPage.Controls.Add(Me.PlaygroundToggleSwitch)
        Me.PlaygroundTabPage.Controls.Add(Me.PlaygroundPropertyGrid)
        Me.PlaygroundTabPage.Location = New System.Drawing.Point(4, 22)
        Me.PlaygroundTabPage.Name = "PlaygroundTabPage"
        Me.PlaygroundTabPage.Size = New System.Drawing.Size(870, 479)
        Me.PlaygroundTabPage.TabIndex = 3
        Me.PlaygroundTabPage.Text = "Playground"
        Me.PlaygroundTabPage.UseVisualStyleBackColor = True
        ' 
        ' PlaygroundToggleSwitch
        ' 
        Me.PlaygroundToggleSwitch.Location = New System.Drawing.Point(51, 42)
        Me.PlaygroundToggleSwitch.Name = "PlaygroundToggleSwitch"
        Me.PlaygroundToggleSwitch.OffFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.PlaygroundToggleSwitch.OnFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.PlaygroundToggleSwitch.Size = New System.Drawing.Size(92, 33)
        Me.PlaygroundToggleSwitch.TabIndex = 11
        ' 
        ' PlaygroundPropertyGrid
        ' 
        Me.PlaygroundPropertyGrid.Dock = System.Windows.Forms.DockStyle.Right
        Me.PlaygroundPropertyGrid.Location = New System.Drawing.Point(459, 0)
        Me.PlaygroundPropertyGrid.Name = "PlaygroundPropertyGrid"
        Me.PlaygroundPropertyGrid.SelectedObject = Me.PlaygroundToggleSwitch
        Me.PlaygroundPropertyGrid.Size = New System.Drawing.Size(411, 479)
        Me.PlaygroundPropertyGrid.TabIndex = 0
        ' 
        ' SimulateRestartBackgroundWorker
        ' 
        '			Me.SimulateRestartBackgroundWorker.DoWork += New System.ComponentModel.DoWorkEventHandler(Me.SimulateRestartBackgroundWorker_DoWork)
        '			Me.SimulateRestartBackgroundWorker.RunWorkerCompleted += New System.ComponentModel.RunWorkerCompletedEventHandler(Me.SimulateRestartBackgroundWorker_RunWorkerCompleted)
        ' 
        ' DemoForm
        ' 
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0F, 13.0F)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(878, 605)
        Me.Controls.Add(Me.DemoTabControl)
        Me.Controls.Add(Me.TopPanel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = (CType(resources.GetObject("$this.Icon"), System.Drawing.Icon))
        Me.MaximizeBox = False
        Me.Name = "DemoForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "JCS Toggle Switch Demo Form"
        Me.TopPanel.ResumeLayout(False)
        Me.DemoTabControl.ResumeLayout(False)
        Me.StylesTabPage.ResumeLayout(False)
        Me.StylesTabPage.PerformLayout()
        Me.SemiImportantPropertiesTabPage.ResumeLayout(False)
        Me.SemiImportantPropertiesTabPage.PerformLayout()
        CType(Me.ThresholdPercentageTrackBar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SpecialCustomizationsTabPage.ResumeLayout(False)
        Me.SpecialCustomizationsTabPage.PerformLayout()
        CType(Me.AnimatedGifPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PlaygroundTabPage.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private TopPanel As System.Windows.Forms.Panel
    Private InfoLabel As System.Windows.Forms.Label
    Private DemoTabControl As System.Windows.Forms.TabControl
    Private StylesTabPage As System.Windows.Forms.TabPage
    Private SemiImportantPropertiesTabPage As System.Windows.Forms.TabPage
    Private OSXStyleLabel As System.Windows.Forms.Label
    Private ModernStyleLabel As System.Windows.Forms.Label
    Private IphoneStyleLabel As System.Windows.Forms.Label
    Private IOS5StyleLabel As System.Windows.Forms.Label
    Private FancyStyleLabel As System.Windows.Forms.Label
    Private CarbonStyleLabel As System.Windows.Forms.Label
    Private BrushedMetalStyleLabel As System.Windows.Forms.Label
    Private AndroidStyleLabel As System.Windows.Forms.Label
    Private MetroStyleLabel As System.Windows.Forms.Label
    Private MetroStyleToggleSwitch As isr.Core.Controls.ToggleSwitch
    Private FancyStyleToggleSwitch As isr.Core.Controls.ToggleSwitch
    Private OSXStyleToggleSwitch As isr.Core.Controls.ToggleSwitch
    Private CarbonStyleToggleSwitch As isr.Core.Controls.ToggleSwitch
    Private ModernStyleToggleSwitch As isr.Core.Controls.ToggleSwitch
    Private IphoneStyleToggleSwitch As isr.Core.Controls.ToggleSwitch
    Private BrushedMetalStyleToggleSwitch As isr.Core.Controls.ToggleSwitch
    Private AndroidStyleToggleSwitch As isr.Core.Controls.ToggleSwitch
    Private IOS5StyleToggleSwitch As isr.Core.Controls.ToggleSwitch
    Private OSXStyleOffLabel As System.Windows.Forms.Label
    Private OSXStyleOnLabel As System.Windows.Forms.Label
    Private AllowUserChangeLabel As System.Windows.Forms.Label
    Private label2 As System.Windows.Forms.Label
    Private label1 As System.Windows.Forms.Label
    Private AllowUserChangeToggleSwitch2 As isr.Core.Controls.ToggleSwitch
    Private AllowUserChangeToggleSwitch1 As isr.Core.Controls.ToggleSwitch
    Private WithEvents AllowUserChangeCheckBox As System.Windows.Forms.CheckBox
    Private label4 As System.Windows.Forms.Label
    Private label3 As System.Windows.Forms.Label
    Private label5 As System.Windows.Forms.Label
    Private NoAnimationToggleSwitch As isr.Core.Controls.ToggleSwitch
    Private label6 As System.Windows.Forms.Label
    Private label9 As System.Windows.Forms.Label
    Private label8 As System.Windows.Forms.Label
    Private SlowAnimationToggleSwitch As isr.Core.Controls.ToggleSwitch
    Private label7 As System.Windows.Forms.Label
    Private FastAnimationToggleSwitch As isr.Core.Controls.ToggleSwitch
    Private label10 As System.Windows.Forms.Label
    Private label12 As System.Windows.Forms.Label
    Private WithEvents GrayWhenDisabledCheckBox As System.Windows.Forms.CheckBox
    Private label13 As System.Windows.Forms.Label
    Private label14 As System.Windows.Forms.Label
    Private GrayWhenDisabledToggleSwitch2 As isr.Core.Controls.ToggleSwitch
    Private GrayWhenDisabledToggleSwitch1 As isr.Core.Controls.ToggleSwitch
    Private label11 As System.Windows.Forms.Label
    Private ThresholdPercentageToggleSwitch As isr.Core.Controls.ToggleSwitch
    Private WithEvents ThresholdPercentageTrackBar As System.Windows.Forms.TrackBar
    Private label15 As System.Windows.Forms.Label
    Private label16 As System.Windows.Forms.Label
    Private label17 As System.Windows.Forms.Label
    Private ToggleOnClickToggleSwitch As isr.Core.Controls.ToggleSwitch
    Private label18 As System.Windows.Forms.Label
    Private WithEvents ToggleOnButtonClickCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents ToggleOnSideClickCheckBox As System.Windows.Forms.CheckBox
    Private label19 As System.Windows.Forms.Label
    Private SpecialCustomizationsTabPage As System.Windows.Forms.TabPage
    Private PlaygroundTabPage As System.Windows.Forms.TabPage
    Private PlaygroundToggleSwitch As isr.Core.Controls.ToggleSwitch
    Private PlaygroundPropertyGrid As System.Windows.Forms.PropertyGrid
    Private label22 As System.Windows.Forms.Label
    Private label21 As System.Windows.Forms.Label
    Private CustomizedMetroToggleSwitch As isr.Core.Controls.ToggleSwitch
    Private NormalMetroToggleSwitch As isr.Core.Controls.ToggleSwitch
    Private label20 As System.Windows.Forms.Label
    Private label23 As System.Windows.Forms.Label
    Private label24 As System.Windows.Forms.Label
    Private CustomizedIOS5ToggleSwitch As isr.Core.Controls.ToggleSwitch
    Private NormalIOS5ToggleSwitch As isr.Core.Controls.ToggleSwitch
    Private label26 As System.Windows.Forms.Label
    Private label27 As System.Windows.Forms.Label
    Private CustomizedFancyToggleSwitch As isr.Core.Controls.ToggleSwitch
    Private NormalFancyToggleSwitch As isr.Core.Controls.ToggleSwitch
    Private label25 As System.Windows.Forms.Label
    Private AdvancedBehaviorFancyToggleSwitch As isr.Core.Controls.ToggleSwitch
    Private label28 As System.Windows.Forms.Label
    Private WithEvents SimulateRestartBackgroundWorker As System.ComponentModel.BackgroundWorker
    Private AnimatedGifPictureBox As System.Windows.Forms.PictureBox
End Class
