﻿Imports Microsoft.VisualBasic
' ***********************************************************
' Copyright ©   2010 Craig GreeNock.
' Contact     - GreeNock@bcs.org.uk
' Version 0.0 - 30 November 2010 21:01
' Machine     - KeFallonia Microsoft Windows NT 6.1.7600.0
' ***********************************************************

#Region "References"
Imports System
Imports isr.Core
#End Region ' References

Namespace RegisterDemo

    ''' <summary> Program. </summary>
    ''' <remarks> (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 6/15/2013, 1.0.*. </para></remarks>
    Friend Class Program

        ' Set up an enumeration that we would normally mark with a
        ' [Flags] attribute, but we see that we don't need to mark it.
        Public Enum Greek
            Alpha = 1
            Beta = 2
            Gamma = 4
            Delta = 8
            Epsilon = 16
        End Enum

        ' This is just to demonstrate that Register<T> is type safe.
        Public Enum Island
            Kefallonia = 1
            Thassos = 2
            Ikaria = 4
            Thira = 8
            Kerkira = 16
            Kriti = 32
        End Enum

        Shared Sub Main(ByVal args As String())

            ' We'll start with a flag register all clear and demonstrate set, clear and query  
            Dim startClear As Register(Of Greek) = New Register(Of Greek)()

            ' Demonstrate that the setClear instance is all clear.
            Console.WriteLine("Show that individual flags are clear." & Constants.vbLf)
            Console.WriteLine("{0} set {1}", Greek.Alpha, startClear.IsSet(Greek.Alpha))
            Console.WriteLine("{0} set {1}", Greek.Beta, startClear.IsSet(Greek.Beta))
            Console.WriteLine("{0} set {1}", Greek.Gamma, startClear.IsSet(Greek.Gamma))
            Console.WriteLine("{0} set {1}", Greek.Delta, startClear.IsSet(Greek.Delta))
            Console.WriteLine("{0} set {1}", Greek.Epsilon, startClear.IsSet(Greek.Epsilon))
            Console.ReadLine()

            ' Demonstrate that we can set all flags 
            startClear.Set()
            Console.WriteLine(Constants.vbLf & "Show that all flags are now set." & Constants.vbLf)
            Console.WriteLine("{0} set {1}", Greek.Alpha, startClear.IsSet(Greek.Alpha))
            Console.WriteLine("{0} set {1}", Greek.Beta, startClear.IsSet(Greek.Beta))
            Console.WriteLine("{0} set {1}", Greek.Gamma, startClear.IsSet(Greek.Gamma))
            Console.WriteLine("{0} set {1}", Greek.Delta, startClear.IsSet(Greek.Delta))
            Console.WriteLine("{0} set {1}", Greek.Epsilon, startClear.IsSet(Greek.Epsilon))
            Console.ReadLine()


            ' Demonstrate that we can clear flags either singly or in combination.
            startClear.Clear(Greek.Alpha)
            startClear.Clear(Greek.Gamma Or Greek.Epsilon)
            Console.WriteLine(Constants.vbLf & "Show that we have cleared Alpha, Gamma & Epsilon flags." & Constants.vbLf)
            Console.WriteLine("{0} set {1}", Greek.Alpha, startClear.IsSet(Greek.Alpha))
            Console.WriteLine("{0} set {1}", Greek.Beta, startClear.IsSet(Greek.Beta))
            Console.WriteLine("{0} set {1}", Greek.Gamma, startClear.IsSet(Greek.Gamma))
            Console.WriteLine("{0} set {1}", Greek.Delta, startClear.IsSet(Greek.Delta))
            Console.WriteLine("{0} set {1}", Greek.Epsilon, startClear.IsSet(Greek.Epsilon))
            Console.ReadLine()


            ' Demonstrate that we can set flags either singly or in combination.
            startClear.Set(Greek.Alpha)
            startClear.Set(Greek.Gamma Or Greek.Epsilon)
            Console.WriteLine(Constants.vbLf & "Show that we have set Alpha, Gamma & Epsilon flags." & Constants.vbLf)
            Console.WriteLine("{0} set {1}", Greek.Alpha, startClear.IsSet(Greek.Alpha))
            Console.WriteLine("{0} set {1}", Greek.Beta, startClear.IsSet(Greek.Beta))
            Console.WriteLine("{0} set {1}", Greek.Gamma, startClear.IsSet(Greek.Gamma))
            Console.WriteLine("{0} set {1}", Greek.Delta, startClear.IsSet(Greek.Delta))
            Console.WriteLine("{0} set {1}", Greek.Epsilon, startClear.IsSet(Greek.Epsilon))
            Console.ReadLine()

            ' Now we'll show a flag register initialized at construction.
            Console.WriteLine(Constants.vbLf & "Show that we can initialize a register with a starting combination." & Constants.vbLf)
            Dim startSet As Register(Of Greek) = New Register(Of Greek)(Greek.Delta Or Greek.Gamma)
            Console.WriteLine("{0} set {1}", Greek.Alpha, startSet.IsSet(Greek.Alpha))
            Console.WriteLine("{0} set {1}", Greek.Beta, startSet.IsSet(Greek.Beta))
            Console.WriteLine("{0} set {1}", Greek.Gamma, startSet.IsSet(Greek.Gamma))
            Console.WriteLine("{0} set {1}", Greek.Delta, startClear.IsSet(Greek.Delta))
            Console.WriteLine("{0} set {1}", Greek.Epsilon, startSet.IsSet(Greek.Epsilon))
            Console.ReadLine()

            Console.WriteLine(Constants.vbLf & "Show that we can query multiple flags at once." & Constants.vbLf)
            Console.WriteLine("{0} set {1}", Greek.Alpha.ToString() & Greek.Delta.ToString(), startSet.IsSet(Greek.Alpha Or Greek.Delta))
            Console.WriteLine("{0} set {1}", Greek.Gamma.ToString() & Greek.Delta.ToString(), startSet.IsSet(Greek.Gamma Or Greek.Delta))

            Console.WriteLine("Overall state {0}", startClear.State.ToString())

            ' Show that we have type safety
            ' Remove the comments and try for yourself.
            Dim g As Greek = startClear.State
            ' Island I = startClear.State;   
            ' startSet.Set(Island.Rodes);

            ' Alas we can't constrain the type used to instantiate our Register<T>
            ' The following Me._will_ compile but will throw an exception at runtime.
            ' Register<string> r = new Register<string>();

            Console.ReadLine()
        End Sub

    End Class
End Namespace
