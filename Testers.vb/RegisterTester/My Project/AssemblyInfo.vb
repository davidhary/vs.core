﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Register Tester")> 
<Assembly: AssemblyDescription("Register Tester")> 
<Assembly: AssemblyCompany("Integrated Scientific Resources")> 
<Assembly: AssemblyProduct("Register.Tester")> 
<Assembly: AssemblyCopyright("(c) 2010 Integrated Scientific Resources, Inc. All rights reserved.")> 
<Assembly: AssemblyTrademark("Licensed under The MIT License.")>

<Assembly: ComVisible(False)>


' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
<Assembly: AssemblyVersion("1.0.*")> 
