﻿Imports isr.Core.LinqStatistics.EnumerableStats

Namespace ConsoleApplication1
	Friend Class Program
        <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
        Shared Sub Main(ByVal args() As String)
            Dim data As IEnumerable(Of Integer) = New Integer() {1, 2, 5, 6, 6, 8, 9, 9, 9}

            Console.WriteLine("Count = {0}", data.Count())
            Console.WriteLine("Average = {0}", data.Average())
            Console.WriteLine("Median = {0}", data.Median())
            Console.WriteLine("Mode = {0}", data.Mode())
            Console.WriteLine("Sample Variance = {0}", data.Variance())
            Console.WriteLine("Sample Standard Deviation = {0}", data.StandardDeviation())
            Console.WriteLine("Population Variance = {0}", data.PopulationVariance())
            Console.WriteLine("Population Standard Deviation = {0}", data.PopulationStandardDeviation())
            Console.WriteLine("Range = {0}", data.Range())
        End Sub
    End Class
End Namespace
