Friend Class MyEventArgs
  Inherits EventArgs

  Public Sub New(ByVal eventData As String)
    Me.EventData = eventData
  End Sub

  Private _EventData As String
  Public Property EventData() As String
    Get
      Return Me._eventData
    End Get
    Private Set(ByVal value As String)
      Me._eventData = value
    End Set
  End Property

  Private _ReturnData As String
  Public Property ReturnData() As String
    Get
      Return Me._returnData
    End Get
    Set(ByVal value As String)
      Me._returnData = value
    End Set
  End Property

End Class
