Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports isr.Core

Friend Class Program

  Private Shared Sub Ef(ByVal sender As Object, ByVal e As MyEventArgs)
    Console.WriteLine("Handling event")
    Console.WriteLine("    Event Data    : {0}", e.EventData)
    e.ReturnData = "Return Data."
  End Sub

  Shared Sub Main()

    ' Set up a test class with an event handler that writes
    'out the details, and returns some data.
    Dim testClass As [MyClass] = New [MyClass]()
    AddHandler testClass.MyEvent, AddressOf ef

    ' Test the traditional trigger pattern.
    Dim returnedDataTraditional As String = testClass.TestEventTraditionalPattern()
    Console.WriteLine("    Returned Data : {0}", returnedDataTraditional)

    Console.WriteLine()

    ' Test the safe trigger pattern.
    Dim returnedDataSafeTrigger As String = testClass.TestEventSafeTriggerPattern()
    Console.WriteLine("    Returned Data : {0}", returnedDataSafeTrigger)

    Console.WriteLine()
    Console.WriteLine("Press enter to quit.")
    Console.ReadLine()
  End Sub

End Class
