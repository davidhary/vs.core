Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports isr.Core.EventHandlerExtensions

Friend Class [MyClass]

    Public Event MyEvent As EventHandler(Of MyEventArgs)

    Public Function TestEventSafeTriggerPattern() As String
        Return OnMyEventSafeTrigger("SafeTrigger Pattern Data")
    End Function

    Public Function TestEventTraditionalPattern() As String
        Return OnMyEventTraditional("Traditional Pattern Data")
    End Function

#If False Then
  Private Function ExtractReturnData(ByVal e As MyEventArgs) As String
    Return e.ReturnData
  End Function
#End If

    Protected Overridable Function OnMyEventSafeTrigger(ByVal eventData As String) As String

        ' Here we use the safe trigger extension
        'method to trigger the event.

        'We can provide a function that can be
        'used to extract return data from the event.

        'It is important to still consider the use of
        'a protected "On..." method so that inheriting
        'classes can still override this behavior.
        '   return MyEvent.SafeTrigger(this, new MyEventArgs(eventData), e => e.ReturnData);
        Dim evt As EventHandler(Of MyEventArgs) = Me.MyEventEvent
        If evt IsNot Nothing Then
            Return evt.SafeInvoke(Me, New MyEventArgs(eventData), Function(e) e.ReturnData)
        Else
            Return String.Empty
        End If

    End Function

    Protected Overridable Function OnMyEventTraditional(ByVal eventData As String) As String
        ' Here we use the more traditional pattern to trigger the event.

        Dim localEventCopy As EventHandler(Of MyEventArgs) = MyEventEvent
        If Not localEventCopy Is Nothing Then
            Dim eventArgs As MyEventArgs = New MyEventArgs(eventData)
            localEventCopy(Me, eventArgs)
            Return eventArgs.ReturnData
        Else
            Return Nothing
        End If
    End Function
End Class
