﻿
Partial Public Class Form1
    ''' <summary>
    ''' Required designer variable.
    ''' </summary>
    Private components As System.ComponentModel.IContainer = Nothing

    ''' <summary>
    ''' Clean up any resources being used.
    ''' </summary>
    ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso (components IsNot Nothing) Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

#Region "Windows Form Designer generated code"

    ''' <summary>
    ''' Required method for Designer support - do not modify
    ''' the contents of this method with the code editor.
    ''' </summary>
    Private Sub InitializeComponent()
        Me.StandardProgressBar = New System.Windows.Forms.ProgressBar()
        Me.FadeTrackBar = New System.Windows.Forms.TrackBar()
        Me.StandardProgressBarLabel = New System.Windows.Forms.Label()
        Me.CustomProgressBarLabel = New System.Windows.Forms.Label()
        Me.FadeTrackBarlabel = New System.Windows.Forms.Label()
        Me.CustomProgressBar = New isr.Core.Controls.CustomProgressBar()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me._ToolStripProgressBar = New System.Windows.Forms.ToolStripProgressBar()
        Me._StatusStripCustomProgressBar = New isr.Core.Controls.StatusStripCustomProgressBar()
        CType(Me.FadeTrackBar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'StandardProgressBar
        '
        Me.StandardProgressBar.Location = New System.Drawing.Point(133, 24)
        Me.StandardProgressBar.Name = "StandardProgressBar"
        Me.StandardProgressBar.Size = New System.Drawing.Size(321, 23)
        Me.StandardProgressBar.TabIndex = 1
        '
        'FadeTrackBar
        '
        Me.FadeTrackBar.Location = New System.Drawing.Point(125, 124)
        Me.FadeTrackBar.Maximum = 200
        Me.FadeTrackBar.Minimum = 100
        Me.FadeTrackBar.Name = "FadeTrackBar"
        Me.FadeTrackBar.Size = New System.Drawing.Size(336, 45)
        Me.FadeTrackBar.TabIndex = 5
        Me.FadeTrackBar.Value = 150
        '
        'StandardProgressBarLabel
        '
        Me.StandardProgressBarLabel.AutoSize = True
        Me.StandardProgressBarLabel.Location = New System.Drawing.Point(19, 29)
        Me.StandardProgressBarLabel.Name = "StandardProgressBarLabel"
        Me.StandardProgressBarLabel.Size = New System.Drawing.Size(110, 13)
        Me.StandardProgressBarLabel.TabIndex = 0
        Me.StandardProgressBarLabel.Text = "Standard Progress Bar"
        '
        'CustomProgressBarLabel
        '
        Me.CustomProgressBarLabel.AutoSize = True
        Me.CustomProgressBarLabel.Location = New System.Drawing.Point(19, 76)
        Me.CustomProgressBarLabel.Name = "CustomProgressBarLabel"
        Me.CustomProgressBarLabel.Size = New System.Drawing.Size(102, 13)
        Me.CustomProgressBarLabel.TabIndex = 2
        Me.CustomProgressBarLabel.Text = "Custom Progress Bar"
        '
        'FadeTrackBarlabel
        '
        Me.FadeTrackBarlabel.AutoSize = True
        Me.FadeTrackBarlabel.Location = New System.Drawing.Point(19, 123)
        Me.FadeTrackBarlabel.Name = "FadeTrackBarlabel"
        Me.FadeTrackBarlabel.Size = New System.Drawing.Size(85, 13)
        Me.FadeTrackBarlabel.TabIndex = 4
        Me.FadeTrackBarlabel.Text = "Fade Alpha: 150"
        '
        'CustomProgressBar
        '
        Me.CustomProgressBar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CustomProgressBar.Location = New System.Drawing.Point(133, 70)
        Me.CustomProgressBar.Name = "CustomProgressBar"
        Me.CustomProgressBar.Size = New System.Drawing.Size(321, 23)
        Me.CustomProgressBar.TabIndex = 3
        Me.CustomProgressBar.Text = "0 %"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ToolStripProgressBar, Me._StatusStripCustomProgressBar})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 150)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(484, 29)
        Me.StatusStrip1.TabIndex = 6
        Me.StatusStrip1.Text = "Status Strip"
        '
        '_ToolStripProgressBar
        '
        Me._ToolStripProgressBar.Name = "_ToolStripProgressBar"
        Me._ToolStripProgressBar.Size = New System.Drawing.Size(100, 23)
        '
        '_StatusStripCustomProgressBar
        '
        Me._StatusStripCustomProgressBar.ForeColor = System.Drawing.SystemColors.ControlText
        Me._StatusStripCustomProgressBar.Margin = New System.Windows.Forms.Padding(1, 3, 1, 3)
        Me._StatusStripCustomProgressBar.Name = "_StatusStripCustomProgressBar"
        Me._StatusStripCustomProgressBar.Size = New System.Drawing.Size(100, 23)
        Me._StatusStripCustomProgressBar.Text = "0 %"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(484, 179)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.CustomProgressBar)
        Me.Controls.Add(Me.FadeTrackBarlabel)
        Me.Controls.Add(Me.CustomProgressBarLabel)
        Me.Controls.Add(Me.StandardProgressBarLabel)
        Me.Controls.Add(Me.FadeTrackBar)
        Me.Controls.Add(Me.StandardProgressBar)
        Me.Name = "Form1"
        Me.Text = "Custom Bar Demo"
        CType(Me.FadeTrackBar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private StandardProgressBarLabel As System.Windows.Forms.Label
    Private StandardProgressBar As System.Windows.Forms.ProgressBar
    Private CustomProgressBarLabel As System.Windows.Forms.Label
    Private CustomProgressBar As isr.Core.Controls.CustomProgressBar
    Private FadeTrackBarlabel As System.Windows.Forms.Label
    Private WithEvents FadeTrackBar As System.Windows.Forms.TrackBar
    Private WithEvents StatusStrip1 As StatusStrip
    Private WithEvents _StatusStripCustomProgressBar As isr.Core.Controls.StatusStripCustomProgressBar
    Private WithEvents _ToolStripProgressBar As ToolStripProgressBar
End Class

