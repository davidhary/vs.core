﻿Partial Public Class Form1
    Inherits Form

    Public Sub New()
        Me.InitializeComponent()
    End Sub

    Private Sub FadeTrackBar_Scroll(ByVal sender As Object, ByVal e As EventArgs) Handles FadeTrackBar.Scroll
        Dim fadeAlpha As Integer = Me.FadeTrackBar.Value
        Me.CustomProgressBar.Fade = fadeAlpha
        Me._StatusStripCustomProgressBar.Fade = fadeAlpha
        Me.FadeTrackBarlabel.Text = "Fade Alpha: " & fadeAlpha.ToString()
    End Sub

    Private Sub Form1_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
    End Sub

    Private Sub Form1_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Me.CustomProgressBar.Value = 75
        Me.StandardProgressBar.Value = 75
        Me._StatusStripCustomProgressBar.Value = 75
        Me._ToolStripProgressBar.Value = 75
    End Sub
End Class
