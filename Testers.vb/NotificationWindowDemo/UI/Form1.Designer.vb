﻿<System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses")>
Partial Public Class Form1
    ''' <summary>
    ''' Required designer variable.
    ''' </summary>
    Private components As System.ComponentModel.IContainer = Nothing

    ''' <summary>
    ''' Clean up any resources being used.
    ''' </summary>
    ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso (components IsNot Nothing) Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

#Region "Windows Form Designer generated code"

    ''' <summary>
    ''' Required method for Designer support - do not modify
    ''' the contents of this method with the code editor.
    ''' </summary>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me._ExitButton = New System.Windows.Forms.Button()
        Me._ShowButton = New System.Windows.Forms.Button()
        Me._NotificationTitleTextBoxLabel = New System.Windows.Forms.Label()
        Me._NotificationTextTextBoxLabel = New System.Windows.Forms.Label()
        Me._NotificationTitleTextBox = New System.Windows.Forms.TextBox()
        Me._NotificationTextTextBox = New System.Windows.Forms.TextBox()
        Me._ShowNotificationIconCheckBox = New System.Windows.Forms.CheckBox()
        Me._ShowNotificationCloseButtonCheckBox = New System.Windows.Forms.CheckBox()
        Me._ShowNotificationOptionMenuCheckBox = New System.Windows.Forms.CheckBox()
        Me._NotificationDelayTextBoxLabel = New System.Windows.Forms.Label()
        Me._NotificationAnimationIntervalTextBoxLabel = New System.Windows.Forms.Label()
        Me._NotificationDelayTextBox = New System.Windows.Forms.TextBox()
        Me._NotificationAnimationIntervalTextBox = New System.Windows.Forms.TextBox()
        Me._ShowNotificationGripCheckBox = New System.Windows.Forms.CheckBox()
        Me._NotificationTitlePaddingTextBoxLabel = New System.Windows.Forms.Label()
        Me._NotificationIconPaddingTextBox = New System.Windows.Forms.TextBox()
        Me._NotificationScrollCheckBox = New System.Windows.Forms.CheckBox()
        Me._NotificationContentPaddingTextBox = New System.Windows.Forms.TextBox()
        Me._NotificationIconPaddingTextBoxLabel = New System.Windows.Forms.Label()
        Me._NotificationTitlePaddingTextBox = New System.Windows.Forms.TextBox()
        Me._NotificationContentPaddingTextBoxLabel = New System.Windows.Forms.Label()
        Me._ContextMenuStrip = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me._AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SettingsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._AnimationDurationTextBoxLabel = New System.Windows.Forms.Label()
        Me._AnimationDurationTextBox = New System.Windows.Forms.TextBox()
        Me._PopupNotifier = New isr.Core.Forma.PopupNotifier()
        Me._ContextMenuStrip.SuspendLayout()
        Me.SuspendLayout()
        ' 
        ' _ExitButton
        ' 
        Me._ExitButton.Location = New System.Drawing.Point(425, 12)
        Me._ExitButton.Name = "_ExitButton"
        Me._ExitButton.Size = New System.Drawing.Size(75, 23)
        Me._ExitButton.TabIndex = 0
        Me._ExitButton.Text = "Exit"
        Me._ExitButton.UseVisualStyleBackColor = True
        '			Me._ExitButton.Click += New System.EventHandler(Me.ExitButton_Click)
        ' 
        ' _ShowButton
        ' 
        Me._ShowButton.Location = New System.Drawing.Point(12, 202)
        Me._ShowButton.Name = "_ShowButton"
        Me._ShowButton.Size = New System.Drawing.Size(75, 23)
        Me._ShowButton.TabIndex = 1
        Me._ShowButton.Text = "Show!"
        Me._ShowButton.UseVisualStyleBackColor = True
        '			Me._ShowButton.Click += New System.EventHandler(Me._ShowButton_Click)
        ' 
        ' _NotificationTitleTextBoxLabel
        ' 
        Me._NotificationTitleTextBoxLabel.AutoSize = True
        Me._NotificationTitleTextBoxLabel.Location = New System.Drawing.Point(12, 17)
        Me._NotificationTitleTextBoxLabel.Name = "_NotificationTitleTextBoxLabel"
        Me._NotificationTitleTextBoxLabel.Size = New System.Drawing.Size(30, 13)
        Me._NotificationTitleTextBoxLabel.TabIndex = 2
        Me._NotificationTitleTextBoxLabel.Text = "Title:"
        ' 
        ' _NotificationTextTextBoxLabel
        ' 
        Me._NotificationTextTextBoxLabel.AutoSize = True
        Me._NotificationTextTextBoxLabel.Location = New System.Drawing.Point(12, 43)
        Me._NotificationTextTextBoxLabel.Name = "_NotificationTextTextBoxLabel"
        Me._NotificationTextTextBoxLabel.Size = New System.Drawing.Size(31, 13)
        Me._NotificationTextTextBoxLabel.TabIndex = 3
        Me._NotificationTextTextBoxLabel.Text = "Text:"
        ' 
        ' _NotificationTitleTextBox
        ' 
        Me._NotificationTitleTextBox.Location = New System.Drawing.Point(48, 14)
        Me._NotificationTitleTextBox.Name = "_NotificationTitleTextBox"
        Me._NotificationTitleTextBox.Size = New System.Drawing.Size(372, 20)
        Me._NotificationTitleTextBox.TabIndex = 4
        Me._NotificationTitleTextBox.Text = "Notification Title"
        ' 
        ' _NotificationTextTextBox
        ' 
        Me._NotificationTextTextBox.Location = New System.Drawing.Point(49, 40)
        Me._NotificationTextTextBox.Name = "_NotificationTextTextBox"
        Me._NotificationTextTextBox.Size = New System.Drawing.Size(371, 20)
        Me._NotificationTextTextBox.TabIndex = 5
        Me._NotificationTextTextBox.Text = "This is the notification text!"
        ' 
        ' _ShowNotificationIconCheckBox
        ' 
        Me._ShowNotificationIconCheckBox.AutoSize = True
        Me._ShowNotificationIconCheckBox.Checked = True
        Me._ShowNotificationIconCheckBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me._ShowNotificationIconCheckBox.Location = New System.Drawing.Point(15, 75)
        Me._ShowNotificationIconCheckBox.Name = "_ShowNotificationIconCheckBox"
        Me._ShowNotificationIconCheckBox.Size = New System.Drawing.Size(76, 17)
        Me._ShowNotificationIconCheckBox.TabIndex = 6
        Me._ShowNotificationIconCheckBox.Text = "Show icon"
        Me._ShowNotificationIconCheckBox.UseVisualStyleBackColor = True
        ' 
        ' _ShowNotificationCloseButtonCheckBox
        ' 
        Me._ShowNotificationCloseButtonCheckBox.AutoSize = True
        Me._ShowNotificationCloseButtonCheckBox.Checked = True
        Me._ShowNotificationCloseButtonCheckBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me._ShowNotificationCloseButtonCheckBox.Location = New System.Drawing.Point(15, 98)
        Me._ShowNotificationCloseButtonCheckBox.Name = "_ShowNotificationCloseButtonCheckBox"
        Me._ShowNotificationCloseButtonCheckBox.Size = New System.Drawing.Size(114, 17)
        Me._ShowNotificationCloseButtonCheckBox.TabIndex = 7
        Me._ShowNotificationCloseButtonCheckBox.Text = "Show close button"
        Me._ShowNotificationCloseButtonCheckBox.UseVisualStyleBackColor = True
        ' 
        ' _ShowNotificationOptionMenuCheckBox
        ' 
        Me._ShowNotificationOptionMenuCheckBox.AutoSize = True
        Me._ShowNotificationOptionMenuCheckBox.Checked = True
        Me._ShowNotificationOptionMenuCheckBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me._ShowNotificationOptionMenuCheckBox.Location = New System.Drawing.Point(15, 121)
        Me._ShowNotificationOptionMenuCheckBox.Name = "_ShowNotificationOptionMenuCheckBox"
        Me._ShowNotificationOptionMenuCheckBox.Size = New System.Drawing.Size(114, 17)
        Me._ShowNotificationOptionMenuCheckBox.TabIndex = 8
        Me._ShowNotificationOptionMenuCheckBox.Text = "Show option menu"
        Me._ShowNotificationOptionMenuCheckBox.UseVisualStyleBackColor = True
        ' 
        ' _NotificationDelayTextBoxLabel
        ' 
        Me._NotificationDelayTextBoxLabel.AutoSize = True
        Me._NotificationDelayTextBoxLabel.Location = New System.Drawing.Point(199, 76)
        Me._NotificationDelayTextBoxLabel.Name = "_NotificationDelayTextBoxLabel"
        Me._NotificationDelayTextBoxLabel.Size = New System.Drawing.Size(59, 13)
        Me._NotificationDelayTextBoxLabel.TabIndex = 9
        Me._NotificationDelayTextBoxLabel.Text = "Delay [ms]:"
        ' 
        ' _NotificationAnimationIntervalTextBoxLabel
        ' 
        Me._NotificationAnimationIntervalTextBoxLabel.AutoSize = True
        Me._NotificationAnimationIntervalTextBoxLabel.Location = New System.Drawing.Point(199, 99)
        Me._NotificationAnimationIntervalTextBoxLabel.Name = "_NotificationAnimationIntervalTextBoxLabel"
        Me._NotificationAnimationIntervalTextBoxLabel.Size = New System.Drawing.Size(115, 13)
        Me._NotificationAnimationIntervalTextBoxLabel.TabIndex = 10
        Me._NotificationAnimationIntervalTextBoxLabel.Text = "Animation interval [ms]:"
        ' 
        ' _NotificationDelayTextBox
        ' 
        Me._NotificationDelayTextBox.Location = New System.Drawing.Point(320, 70)
        Me._NotificationDelayTextBox.Name = "_NotificationDelayTextBox"
        Me._NotificationDelayTextBox.Size = New System.Drawing.Size(100, 20)
        Me._NotificationDelayTextBox.TabIndex = 11
        Me._NotificationDelayTextBox.Text = "3000"
        ' 
        ' _NotificationAnimationIntervalTextBox
        ' 
        Me._NotificationAnimationIntervalTextBox.Location = New System.Drawing.Point(320, 96)
        Me._NotificationAnimationIntervalTextBox.Name = "_NotificationAnimationIntervalTextBox"
        Me._NotificationAnimationIntervalTextBox.Size = New System.Drawing.Size(100, 20)
        Me._NotificationAnimationIntervalTextBox.TabIndex = 12
        Me._NotificationAnimationIntervalTextBox.Text = "10"
        ' 
        ' _ShowNotificationGripCheckBox
        ' 
        Me._ShowNotificationGripCheckBox.AutoSize = True
        Me._ShowNotificationGripCheckBox.Checked = True
        Me._ShowNotificationGripCheckBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me._ShowNotificationGripCheckBox.Location = New System.Drawing.Point(15, 144)
        Me._ShowNotificationGripCheckBox.Name = "_ShowNotificationGripCheckBox"
        Me._ShowNotificationGripCheckBox.Size = New System.Drawing.Size(73, 17)
        Me._ShowNotificationGripCheckBox.TabIndex = 13
        Me._ShowNotificationGripCheckBox.Text = "Show grip"
        Me._ShowNotificationGripCheckBox.UseVisualStyleBackColor = True
        ' 
        ' _NotificationTitlePaddingTextBoxLabel
        ' 
        Me._NotificationTitlePaddingTextBoxLabel.AutoSize = True
        Me._NotificationTitlePaddingTextBoxLabel.Location = New System.Drawing.Point(199, 151)
        Me._NotificationTitlePaddingTextBoxLabel.Name = "_NotificationTitlePaddingTextBoxLabel"
        Me._NotificationTitlePaddingTextBoxLabel.Size = New System.Drawing.Size(91, 13)
        Me._NotificationTitlePaddingTextBoxLabel.TabIndex = 14
        Me._NotificationTitlePaddingTextBoxLabel.Text = "Title padding [pixels]:"
        ' 
        ' _NotificationIconPaddingTextBox
        ' 
        Me._NotificationIconPaddingTextBox.Location = New System.Drawing.Point(320, 200)
        Me._NotificationIconPaddingTextBox.Name = "_NotificationIconPaddingTextBox"
        Me._NotificationIconPaddingTextBox.Size = New System.Drawing.Size(100, 20)
        Me._NotificationIconPaddingTextBox.TabIndex = 15
        Me._NotificationIconPaddingTextBox.Text = "0"
        ' 
        ' _NotificationScrollCheckBox
        ' 
        Me._NotificationScrollCheckBox.AutoSize = True
        Me._NotificationScrollCheckBox.Checked = True
        Me._NotificationScrollCheckBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me._NotificationScrollCheckBox.Location = New System.Drawing.Point(15, 167)
        Me._NotificationScrollCheckBox.Name = "_NotificationScrollCheckBox"
        Me._NotificationScrollCheckBox.Size = New System.Drawing.Size(83, 17)
        Me._NotificationScrollCheckBox.TabIndex = 16
        Me._NotificationScrollCheckBox.Text = "Scroll in/out"
        Me._NotificationScrollCheckBox.UseVisualStyleBackColor = True
        ' 
        ' _NotificationContentPaddingTextBox
        ' 
        Me._NotificationContentPaddingTextBox.Location = New System.Drawing.Point(320, 174)
        Me._NotificationContentPaddingTextBox.Name = "_NotificationContentPaddingTextBox"
        Me._NotificationContentPaddingTextBox.Size = New System.Drawing.Size(100, 20)
        Me._NotificationContentPaddingTextBox.TabIndex = 17
        Me._NotificationContentPaddingTextBox.Text = "0"
        ' 
        ' _NotificationIconPaddingTextBoxLabel
        ' 
        Me._NotificationIconPaddingTextBoxLabel.AutoSize = True
        Me._NotificationIconPaddingTextBoxLabel.Location = New System.Drawing.Point(199, 203)
        Me._NotificationIconPaddingTextBoxLabel.Name = "_NotificationIconPaddingTextBoxLabel"
        Me._NotificationIconPaddingTextBoxLabel.Size = New System.Drawing.Size(92, 13)
        Me._NotificationIconPaddingTextBoxLabel.TabIndex = 18
        Me._NotificationIconPaddingTextBoxLabel.Text = "Icon padding [pixels]:"
        ' 
        ' _NotificationTitlePaddingTextBox
        ' 
        Me._NotificationTitlePaddingTextBox.Location = New System.Drawing.Point(320, 148)
        Me._NotificationTitlePaddingTextBox.Name = "_NotificationTitlePaddingTextBox"
        Me._NotificationTitlePaddingTextBox.Size = New System.Drawing.Size(100, 20)
        Me._NotificationTitlePaddingTextBox.TabIndex = 19
        Me._NotificationTitlePaddingTextBox.Text = "0"
        ' 
        ' _NotificationContentPaddingTextBoxLabel
        ' 
        Me._NotificationContentPaddingTextBoxLabel.AutoSize = True
        Me._NotificationContentPaddingTextBoxLabel.Location = New System.Drawing.Point(199, 177)
        Me._NotificationContentPaddingTextBoxLabel.Name = "_NotificationContentPaddingTextBoxLabel"
        Me._NotificationContentPaddingTextBoxLabel.Size = New System.Drawing.Size(108, 13)
        Me._NotificationContentPaddingTextBoxLabel.TabIndex = 20
        Me._NotificationContentPaddingTextBoxLabel.Text = "Content padding [pixels]:"
        ' 
        ' _ContextMenuStrip
        ' 
        Me._ContextMenuStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._AboutToolStripMenuItem, Me._SettingsToolStripMenuItem, Me._ExitToolStripMenuItem})
        Me._ContextMenuStrip.Name = "_ContextMenuStrip"
        Me._ContextMenuStrip.Size = New System.Drawing.Size(126, 70)
        ' 
        ' _AboutToolStripMenuItem
        ' 
        Me._AboutToolStripMenuItem.Name = "_AboutToolStripMenuItem"
        Me._AboutToolStripMenuItem.Size = New System.Drawing.Size(125, 22)
        Me._AboutToolStripMenuItem.Text = "About..."
        ' 
        ' _SettingsToolStripMenuItem
        ' 
        Me._SettingsToolStripMenuItem.Name = "_SettingsToolStripMenuItem"
        Me._SettingsToolStripMenuItem.Size = New System.Drawing.Size(125, 22)
        Me._SettingsToolStripMenuItem.Text = "Settings..."
        ' 
        ' _ExitToolStripMenuItem
        ' 
        Me._ExitToolStripMenuItem.Name = "_ExitToolStripMenuItem"
        Me._ExitToolStripMenuItem.Size = New System.Drawing.Size(125, 22)
        Me._ExitToolStripMenuItem.Text = "Exit"
        ' 
        ' _AnimationDurationTextBoxLabel
        ' 
        Me._AnimationDurationTextBoxLabel.AutoSize = True
        Me._AnimationDurationTextBoxLabel.Location = New System.Drawing.Point(199, 125)
        Me._AnimationDurationTextBoxLabel.Name = "_AnimationDurationTextBoxLabel"
        Me._AnimationDurationTextBoxLabel.Size = New System.Drawing.Size(118, 13)
        Me._AnimationDurationTextBoxLabel.TabIndex = 21
        Me._AnimationDurationTextBoxLabel.Text = "Animation Duration [ms]:"
        ' 
        ' _AnimationDurationTextBox
        ' 
        Me._AnimationDurationTextBox.Location = New System.Drawing.Point(320, 122)
        Me._AnimationDurationTextBox.Name = "_AnimationDurationTextBox"
        Me._AnimationDurationTextBox.Size = New System.Drawing.Size(100, 20)
        Me._AnimationDurationTextBox.TabIndex = 22
        Me._AnimationDurationTextBox.Text = "1000"
        ' 
        ' _PopupNotifier
        ' 
        Me._PopupNotifier.BodyColor = System.Drawing.Color.FromArgb((CInt((CByte(128)))), (CInt((CByte(128)))), (CInt((CByte(255)))))
        Me._PopupNotifier.ContentFont = New System.Drawing.Font("Tahoma", 8.0F)
        Me._PopupNotifier.ContentText = Nothing
        Me._PopupNotifier.GradientPower = 300
        Me._PopupNotifier.HeaderHeight = 20
        Me._PopupNotifier.Image = Nothing
        Me._PopupNotifier.OptionsMenu = Me._ContextMenuStrip
        Me._PopupNotifier.Size = New System.Drawing.Size(400, 100)
        Me._PopupNotifier.TitleFont = New System.Drawing.Font("Segoe UI", 9.0F)
        Me._PopupNotifier.TitleText = Nothing
        ' 
        ' Form1
        ' 
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0F, 13.0F)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(512, 237)
        Me.Controls.Add(Me._NotificationTitlePaddingTextBox)
        Me.Controls.Add(Me._NotificationContentPaddingTextBoxLabel)
        Me.Controls.Add(Me._AnimationDurationTextBox)
        Me.Controls.Add(Me._AnimationDurationTextBoxLabel)
        Me.Controls.Add(Me._NotificationScrollCheckBox)
        Me.Controls.Add(Me._NotificationContentPaddingTextBox)
        Me.Controls.Add(Me._NotificationIconPaddingTextBox)
        Me.Controls.Add(Me._NotificationIconPaddingTextBoxLabel)
        Me.Controls.Add(Me._ShowNotificationGripCheckBox)
        Me.Controls.Add(Me._NotificationTitlePaddingTextBoxLabel)
        Me.Controls.Add(Me._NotificationDelayTextBox)
        Me.Controls.Add(Me._NotificationAnimationIntervalTextBoxLabel)
        Me.Controls.Add(Me._NotificationAnimationIntervalTextBox)
        Me.Controls.Add(Me._NotificationDelayTextBoxLabel)
        Me.Controls.Add(Me._ShowNotificationOptionMenuCheckBox)
        Me.Controls.Add(Me._ShowNotificationCloseButtonCheckBox)
        Me.Controls.Add(Me._ShowNotificationIconCheckBox)
        Me.Controls.Add(Me._NotificationTextTextBox)
        Me.Controls.Add(Me._NotificationTitleTextBox)
        Me.Controls.Add(Me._NotificationTextTextBoxLabel)
        Me.Controls.Add(Me._NotificationTitleTextBoxLabel)
        Me.Controls.Add(Me._ShowButton)
        Me.Controls.Add(Me._ExitButton)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Form1"
        Me.Text = "Notification Window Demo"
        Me._ContextMenuStrip.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private WithEvents _ExitButton As System.Windows.Forms.Button
    Private WithEvents _ShowButton As System.Windows.Forms.Button
    Private _NotificationTitleTextBoxLabel As System.Windows.Forms.Label
    Private _NotificationTextTextBoxLabel As System.Windows.Forms.Label
    Private _NotificationTitleTextBox As System.Windows.Forms.TextBox
    Private _NotificationTextTextBox As System.Windows.Forms.TextBox
    Private _ShowNotificationIconCheckBox As System.Windows.Forms.CheckBox
    Private _ShowNotificationCloseButtonCheckBox As System.Windows.Forms.CheckBox
    Private _ShowNotificationOptionMenuCheckBox As System.Windows.Forms.CheckBox
    Private _NotificationDelayTextBoxLabel As System.Windows.Forms.Label
    Private _NotificationAnimationIntervalTextBoxLabel As System.Windows.Forms.Label
    Private _NotificationDelayTextBox As System.Windows.Forms.TextBox
    Private _NotificationAnimationIntervalTextBox As System.Windows.Forms.TextBox
    Private _ShowNotificationGripCheckBox As System.Windows.Forms.CheckBox
    Private _NotificationTitlePaddingTextBoxLabel As System.Windows.Forms.Label
    Private _NotificationIconPaddingTextBox As System.Windows.Forms.TextBox
    Private _NotificationScrollCheckBox As System.Windows.Forms.CheckBox
    Private _NotificationContentPaddingTextBox As System.Windows.Forms.TextBox
    Private _NotificationIconPaddingTextBoxLabel As System.Windows.Forms.Label
    Private _NotificationTitlePaddingTextBox As System.Windows.Forms.TextBox
    Private _NotificationContentPaddingTextBoxLabel As System.Windows.Forms.Label
    Private _ContextMenuStrip As System.Windows.Forms.ContextMenuStrip
    Private _AboutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private _SettingsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private _ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private _AnimationDurationTextBoxLabel As System.Windows.Forms.Label
    Private _AnimationDurationTextBox As System.Windows.Forms.TextBox
    Private _PopupNotifier As isr.Core.Forma.PopupNotifier
End Class
