'
' *	Created/modified in 2011 by Simon Baer
' *	
' *  Licensed under the Code Project Open License (CPOL).
' 
Partial Public Class Form1
    Inherits Form

    Public Sub New()
        Me.InitializeComponent()
    End Sub

    Private Sub ShowButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _ShowButton.Click
        Me._PopupNotifier.TitleText = Me._NotificationTitleTextBox.Text
        Me._PopupNotifier.ContentText = Me._NotificationTextTextBox.Text
        Me._PopupNotifier.ShowCloseButton = Me._ShowNotificationCloseButtonCheckBox.Checked
        Me._PopupNotifier.ShowOptionsButton = Me._ShowNotificationOptionMenuCheckBox.Checked
        Me._PopupNotifier.ShowGrip = Me._ShowNotificationGripCheckBox.Checked
        Me._PopupNotifier.Delay = Integer.Parse(Me._NotificationDelayTextBox.Text)
        Me._PopupNotifier.AnimationInterval = Integer.Parse(Me._NotificationAnimationIntervalTextBox.Text)
        Me._PopupNotifier.AnimationDuration = Integer.Parse(Me._AnimationDurationTextBox.Text)
        Me._PopupNotifier.TitlePadding = New Padding(Integer.Parse(Me._NotificationTitlePaddingTextBox.Text))
        Me._PopupNotifier.ContentPadding = New Padding(Integer.Parse(Me._NotificationContentPaddingTextBox.Text))
        Me._PopupNotifier.ImagePadding = New Padding(Integer.Parse(Me._NotificationIconPaddingTextBox.Text))
        Me._PopupNotifier.Scroll = Me._NotificationScrollCheckBox.Checked

        Me._PopupNotifier.Image = If(Me._ShowNotificationIconCheckBox.Checked, My.Resources._157_GetPermission_48x48_72, Nothing)

        Me._PopupNotifier.Popup()
    End Sub

    Private Sub ExitButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _ExitButton.Click
        Me.Close()
        Application.Exit()
    End Sub
End Class
