''' <summary>
''' A sealed class the parses the command line and provides the command line values.
''' </summary>
''' <remarks>
''' (c) 2011 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 02/02/2011, x.x.4050.x. </para>
''' </remarks>
Public NotInheritable Class CommandLineInfo

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Constructor that prevents a default instance of this class from being created.
    ''' </summary>
    ''' <remarks> David, 2020-09-30. </remarks>
    Private Sub New()
        MyBase.New()
    End Sub

#End Region

#Region " PARSER "

    ''' <summary> Gets the no-operation command line option. </summary>
    ''' <value> The no operation option. </value>
    Public Shared ReadOnly Property NoOperationOption As String
        Get
            Return "/nop"
        End Get
    End Property

    ''' <summary> Gets the Device-Enabled option. </summary>
    ''' <value> The Device enabled option. </value>
    Public Shared ReadOnly Property DevicesEnabledOption As String
        Get
            Return "/d:"
        End Get
    End Property

    ''' <summary> Validate the command line. </summary>
    ''' <remarks> David, 2020-09-30. </remarks>
    ''' <exception cref="ArgumentException"> This exception is raised if a command line argument is
    '''                                      not handled. </exception>
    ''' <param name="commandLineArgs"> The command line arguments. </param>
    Public Shared Sub ValidateCommandLine(ByVal commandLineArgs As System.Collections.ObjectModel.ReadOnlyCollection(Of String))

        If commandLineArgs Is Nothing OrElse Not commandLineArgs.Any Then
            CommandLineInfo._CommandLine = String.Empty
        Else
            CommandLineInfo._CommandLine = String.Join(",", commandLineArgs)
            For Each argument As String In commandLineArgs
                If False Then
                ElseIf argument.StartsWith(CommandLineInfo.DevicesEnabledOption, StringComparison.OrdinalIgnoreCase) Then
                ElseIf argument.StartsWith(CommandLineInfo.NoOperationOption, StringComparison.OrdinalIgnoreCase) Then
                    CommandLineInfo.Nop = True
                Else
                    Throw New ArgumentException($"Unknown command line argument '{argument}' was detected. Should be Ignored.", NameOf(commandLineArgs))
                End If
            Next
        End If

    End Sub

    ''' <summary> Parses the command line. </summary>
    ''' <remarks> David, 2020-09-30. </remarks>
    ''' <param name="commandLineArgs"> The command line arguments. </param>
    Public Shared Sub ParseCommandLine(ByVal commandLineArgs As System.Collections.ObjectModel.ReadOnlyCollection(Of String))

        If commandLineArgs Is Nothing OrElse Not commandLineArgs.Any Then
            CommandLineInfo._CommandLine = String.Empty
        Else
            CommandLineInfo._CommandLine = String.Join(",", commandLineArgs)
            For Each argument As String In commandLineArgs
                If False Then
                ElseIf argument.StartsWith(CommandLineInfo.DevicesEnabledOption, StringComparison.OrdinalIgnoreCase) Then
                    Dim value As String = argument.Substring(CommandLineInfo.DevicesEnabledOption.Length)
                    CommandLineInfo.DevicesEnabled = Not value.StartsWith("n", StringComparison.OrdinalIgnoreCase)
                ElseIf argument.StartsWith(CommandLineInfo.NoOperationOption, StringComparison.OrdinalIgnoreCase) Then
                    CommandLineInfo.Nop = True
                Else
                    ' do nothing
                End If
            Next
        End If

    End Sub

    ''' <summary> Parses the command line. </summary>
    ''' <remarks> David, 2020-09-30. </remarks>
    ''' <param name="commandLineArgs"> The command line arguments. </param>
    ''' <returns> True if success or false if Exception occurred. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryParseCommandLine(ByVal commandLineArgs As System.Collections.ObjectModel.ReadOnlyCollection(Of String)) As Boolean
        Dim result As Boolean = True
        Dim activity As String = String.Empty
        If commandLineArgs Is Nothing OrElse Not commandLineArgs.Any Then
            CommandLineInfo._CommandLine = String.Empty
        Else
            Try
                CommandLineInfo._CommandLine = String.Join(",", commandLineArgs)
                activity = $"Parsing the commandLine {CommandLineInfo.CommandLine}"
                ' Data.ParseCommandLine(commandLineArgs)
                CommandLineInfo.ParseCommandLine(commandLineArgs)
            Catch ex As ArgumentException
                My.Application.Logger.WriteExceptionDetails(ex, TraceEventType.Error, My.MyApplication.TraceEventId, $"Failed {activity};  Unknown argument ignored")
            Catch ex As System.Exception
                My.Application.Logger.WriteExceptionDetails(ex, TraceEventType.Error, My.MyApplication.TraceEventId, $"Failed {activity}")
                result = False
            End Try
        End If
        Return result
    End Function

#End Region

#Region " COMMAND LINE ELEMENTS "

    ''' <summary> Gets or sets the command line. </summary>
    ''' <value> The command line. </value>
    Public Shared ReadOnly Property CommandLine As String

    ''' <summary> Gets or sets a value indicating whether the system uses actual devices. </summary>
    ''' <value> <c>True</c> if using devices; otherwise, <c>False</c>. </value>
    Public Shared Property DevicesEnabled() As Boolean?

    ''' <summary> Gets or sets the no operation option. </summary>
    ''' <value> The nop. </value>
    Public Shared Property Nop As Boolean

#End Region

End Class
