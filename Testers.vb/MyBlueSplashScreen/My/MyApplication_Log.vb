Imports isr.Core.Tester.ExceptionExtensions
Namespace My

    Partial Friend Class MyApplication

        ''' <summary> Logs unpublished exception. </summary>
        ''' <remarks> David, 2020-09-30. </remarks>
        ''' <param name="activity">  The activity. </param>
        ''' <param name="exception"> The exception. </param>
        Public Sub LogUnpublishedException(ByVal activity As String, ByVal exception As Exception)
            Me.LogUnpublishedMessage(New TraceMessage(TraceEventType.Error, MyApplication.TraceEventId, $"Exception {activity};. {exception.ToFullBlownString}"))
        End Sub

        ''' <summary> Applies the Logger. </summary>
        ''' <remarks> David, 2020-09-30. </remarks>
        ''' <param name="value"> The value. </param>
        Public Sub Apply(ByVal value As Logger)
            Me._Logger = value
        End Sub

        ''' <summary> Applies the trace level described by value. </summary>
        ''' <remarks> David, 2020-09-30. </remarks>
        ''' <param name="value"> The value. </param>
        Public Sub ApplyTraceLogLevel(ByVal value As TraceEventType)
            Me.TraceLevel = value
            Me.Logger.ApplyTraceLevel(value)
        End Sub

        ''' <summary> Applies the trace level described by value. </summary>
        ''' <remarks> David, 2020-09-30. </remarks>
        Public Sub ApplyTraceLogLevel()
            Me.ApplyTraceLogLevel(My.MySettings.Default.TraceLevel)
        End Sub

    End Class

End Namespace
