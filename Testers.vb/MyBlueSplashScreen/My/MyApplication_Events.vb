Imports isr.Core.Tester.ExceptionExtensions
Namespace My

    Partial Friend Class MyApplication

        ''' <summary> Destroys objects for this project. </summary>
        ''' <remarks> David, 2020-09-30. </remarks>
        Friend Sub Destroy()
            MySplashScreen.Close()
            MySplashScreen.Dispose()
            Me.SplashScreen = Nothing
        End Sub

        ''' <summary> Instantiates the application to its known state. </summary>
        ''' <remarks> David, 2020-09-30. </remarks>
        ''' <returns> <c>True</c> if success or <c>False</c> if failed. </returns>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Private Function TryinitializeKnownState() As Boolean

            Try

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting

                ' show status
                If My.MyApplication.InDesignMode Then
                    Me.Logger.TraceSource.TraceEvent(TraceEventType.Verbose, MyApplication.TraceEventId, "Application is initializing. Design Mode.")
                Else
                    Me.Logger.TraceSource.TraceEvent(TraceEventType.Verbose, MyApplication.TraceEventId, "Application is initializing. Runtime Mode.")
                End If

                ' Apply command line results.
                If CommandLineInfo.DevicesEnabled.HasValue Then
                    Me.Logger.TraceSource.TraceEvent(TraceEventType.Information, MyApplication.TraceEventId, "{0} use of devices from command line",
                                                  IIf(CommandLineInfo.DevicesEnabled.Value, "Enabled", "Disabling"))
                End If

                Return True

            Catch ex As Exception

                ' Turn off the hourglass
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
                Me.Logger.TraceSource.TraceEvent(TraceEventType.Error, MyApplication.TraceEventId,
                                              "Exception occurred initializing application known state;. {0}", ex.ToFullBlownString)
                Try
                    Me.Destroy()
                Finally
                End Try
                Return False
            Finally

                ' Turn off the hourglass
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default

            End Try

        End Function

        ''' <summary> Processes the shut down. </summary>
        ''' <remarks> David, 2020-09-30. </remarks>
        Private Sub ProcessShutDown()

            My.Application.SaveMySettingsOnExit = True
            If My.Application.SaveMySettingsOnExit Then
                ' Save library settings here
            End If
        End Sub

        ''' <summary>
        ''' Processes the startup. Sets the event arguments
        ''' <see cref="Microsoft.VisualBasic.ApplicationServices.StartupEventArgs.Cancel">cancel</see>
        ''' value if failed.
        ''' </summary>
        ''' <remarks> David, 2020-09-30. </remarks>
        ''' <param name="e"> The <see cref="Microsoft.VisualBasic.ApplicationServices.StartupEventArgs" />
        '''                  instance containing the event data. </param>
        Private Sub ProcessStartup(ByVal e As Microsoft.VisualBasic.ApplicationServices.StartupEventArgs)

            If Not e.Cancel Then
                MySplashScreen.CreateInstance(My.Application.SplashScreen)
                Me.Logger.TraceSource.TraceEvent(TraceEventType.Verbose, MyApplication.TraceEventId, "Using splash panel.")
                Me.Logger.TraceSource.TraceEvent(TraceEventType.Verbose, My.MyApplication.TraceEventId, "Parsing command line")
                e.Cancel = Not CommandLineInfo.TryParseCommandLine(e.CommandLine)
            End If
        End Sub

    End Class


End Namespace

