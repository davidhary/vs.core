Namespace My

    Partial Friend Class MyApplication

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = ProjectTraceEventId.MyBlueSplashScreen

        ''' <summary> The assembly title. </summary>
        Public Const AssemblyTitle As String = "Blue Splash Screen Tester"

        ''' <summary> Information describing the assembly. </summary>
        Public Const AssemblyDescription As String = "Blue Splash Screen Tester"

        ''' <summary> The assembly product. </summary>
        Public Const AssemblyProduct As String = "Blue.Splash.Screen.Tester"

    End Class

End Namespace

