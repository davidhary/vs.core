Imports isr.Core
Namespace My

    Partial Friend Class MyApplication

#Region " LOG "

        ''' <summary> The logger. </summary>
        Private _Logger As Logger

        ''' <summary> Gets the Logger. </summary>
        ''' <value> The logger. </value>
        Public ReadOnly Property Logger As Logger
            Get
                If Me._Logger Is Nothing Then Me.CreateLogger()
                Return Me._Logger
            End Get
        End Property

        ''' <summary> Creates the Logger. </summary>
        ''' <remarks> David, 2020-09-30. </remarks>
        Private Sub CreateLogger()
            Me._Logger = Nothing
            Try
                Me._Logger = Logger.NewInstance(My.Application.Info.ProductName)
                Dim listener As Microsoft.VisualBasic.Logging.FileLogTraceListener
                listener = Me.Logger.ReplaceDefaultTraceListener(True)
                If Not Me.Logger.LogFileExists Then Me.Logger.TraceEventOverride(MyApplication.ProductTimeTraceMessage)

                ' set the log for the application
                If Not String.Equals(My.Application.Log.DefaultFileLogWriter.FullLogFileName, listener.FullLogFileName, StringComparison.OrdinalIgnoreCase) Then
                    My.Application.Log.TraceSource.Listeners.Remove(CustomFileLogTraceListener.DefaultFileLogWriterName)
                    My.Application.Log.TraceSource.Listeners.Add(listener)
                    My.Application.Log.TraceSource.Switch.Level = SourceLevels.Verbose
                End If

                ' set the trace level.
                Me.ApplyTraceLogLevel()

            Catch
                If Me._Logger IsNot Nothing Then Me._Logger.Dispose()
                Me._Logger = Nothing
                Throw
            End Try
        End Sub

        ''' <summary> Gets the trace level. </summary>
        ''' <value> The trace level. </value>
        Public Property TraceLevel As TraceEventType

#End Region

#Region " IDENTITY "

        ''' <summary> Identifies this application. </summary>
        ''' <remarks> David, 2020-09-30. </remarks>
        ''' <param name="talker"> The talker. </param>
        Public Sub Identify(ByVal talker As ITraceMessageTalker)
            talker.PublishDateMessage(MyApplication.ProductTimeTraceMessage)
            talker.PublishDateMessage(Me.LogTraceMessage)
            talker.IdentifyTalker(Me.IdentityTraceMessage())
        End Sub

#Disable Warning CA1822 ' Mark members as static

        ''' <summary> Gets the identity. </summary>
        ''' <value> The identity. </value>
        Public ReadOnly Property Identity() As String
#Enable Warning CA1822 ' Mark members as static
            Get
                Return $"{MyApplication.AssemblyProduct} ID = {MyApplication.TraceEventId:X}"
            End Get
        End Property

        ''' <summary> Gets a message describing the identity trace. </summary>
        ''' <remarks> David, 2020-09-30. </remarks>
        ''' <returns> A TraceMessage. </returns>
        Public Function IdentityTraceMessage() As TraceMessage
            Return New TraceMessage(TraceEventType.Information, MyApplication.TraceEventId, Me.Identity)
        End Function

        ''' <summary> Application log trace message. </summary>
        ''' <remarks> David, 2020-09-30. </remarks>
        ''' <returns> A TraceMessage. </returns>
        Public Function LogTraceMessage() As TraceMessage
            Return New TraceMessage(TraceEventType.Information, MyApplication.TraceEventId, $"Log at;. {Me.Logger.FullLogFileName}")
        End Function

        ''' <summary> Product time trace message. </summary>
        ''' <remarks> David, 2020-09-30. </remarks>
        ''' <returns> A TraceMessage. </returns>
        Public Shared Function ProductTimeTraceMessage() As TraceMessage
            Return New TraceMessage(TraceEventType.Information, MyApplication.TraceEventId, AssemblyExtensions.Methods.BuildProductTimeCaption(My.Application.Info))
        End Function

#End Region

#Region " UNPUBLISHED MESSAGES "

        ''' <summary> Gets or sets the unpublished identify date. </summary>
        ''' <value> The unpublished identify date. </value>
        Public Property UnpublishedIdentifyDate As DateTimeOffset

        ''' <summary> Gets or sets the unpublished trace messages. </summary>
        ''' <value> The unpublished trace messages. </value>
        Public ReadOnly Property UnpublishedTraceMessages As TraceMessagesQueue = New TraceMessagesQueue

        ''' <summary> Logs unpublished exception. </summary>
        ''' <remarks> David, 2020-09-30. </remarks>
        ''' <param name="message"> The message. </param>
        ''' <returns> A String. </returns>
        Public Function LogUnpublishedMessage(ByVal message As TraceMessage) As String
            If message Is Nothing Then
                Return String.Empty
            Else
                If DateTimeOffset.Now.Date > Me.UnpublishedIdentifyDate Then
                    Me.Logger.TraceEventOverride(MyApplication.ProductTimeTraceMessage)
                    Me.Logger.TraceEventOverride(Me.LogTraceMessage)
                    Me.Logger.TraceEventOverride(Me.IdentityTraceMessage())
                    Me.UnpublishedIdentifyDate = DateTimeOffset.Now.Date
                End If
                Me.UnpublishedTraceMessages.Enqueue(message)
                Me.Logger.TraceEvent(message)
                Return message.Details
            End If
        End Function

#End Region

    End Class

End Namespace
