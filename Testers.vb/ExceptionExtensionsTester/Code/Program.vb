Imports isr.Core.Tester.ExceptionExtensions
Friend Class Program
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    Public Shared Sub Main(ByVal args() As String)
        Try
            Throw New ArgumentNullException(NameOf(args))
        Catch e As Exception
            Console.WriteLine(e.ToFullBlownString())
            Console.WriteLine(vbLf & vbLf & vbLf & "and now with level = 1:")
            Console.WriteLine(e.ToFullBlownString(1))
        End Try
        Console.WriteLine("Press any key to exit")
        Console.ReadKey()
    End Sub

    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared Function GetInnerException() As Exception
        Try
            Throw New InvalidOperationException("This is the inner exception. If this was production code, you'd be presented with details here.")
        Catch e As Exception
            Return e
        End Try
    End Function
End Class
