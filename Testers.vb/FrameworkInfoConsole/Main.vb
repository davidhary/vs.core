﻿Imports System.Windows.Threading
Imports isr.Core.Services.DispatcherExtensions
Module Main

    Sub Main()

        Console.WriteLine($"{GetType(isr.Core.Services.My.MyProcess)}.{NameOf(isr.Core.Services.My.MyProcess.CurrentProcessName)} = {isr.Core.Services.My.MyProcess.CurrentProcessName}")
        Console.WriteLine($"{GetType(isr.Core.Services.My.MyProcess)}.{NameOf(isr.Core.Services.My.MyProcess.AllocatedMemory)} = {isr.Core.Services.My.MyProcess.AllocatedMemory:X}")
        Console.WriteLine($"{GetType(GC)}.{NameOf(GC.GetTotalMemory)} = {GC.GetTotalMemory(True):X}")
        Console.WriteLine("Loaded modules")
        Console.WriteLine(isr.Core.Services.My.MyProcess.BuildLoadedAssembliesInfo)
        ' Console.WriteLine(isr.Core.Services.My.MyProcess.BuildLoadedModulesInfo)
        Console.WriteLine("press any key to exist")
        Console.ReadKey()

    End Sub

End Module
