﻿Namespace MicroTimerWinFormsDemo
	Partial Public Class FormMain
		''' <summary>
		''' Required designer variable.
		''' </summary>
		Private components As System.ComponentModel.IContainer = Nothing

		''' <summary>
		''' Clean up any resources being used.
		''' </summary>
		''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		Protected Overrides Sub Dispose(ByVal disposing As Boolean)
			If disposing AndAlso (components IsNot Nothing) Then
				components.Dispose()
			End If
			MyBase.Dispose(disposing)
		End Sub

		#Region "Windows Form Designer generated code"

		''' <summary>
		''' Required method for Designer support - do not modify
		''' the contents of this method with the code editor.
		''' </summary>
		Private Sub InitializeComponent()
			Me.buttonStart = New System.Windows.Forms.Button()
			Me.buttonStop = New System.Windows.Forms.Button()
			Me.TextBoxInterval = New System.Windows.Forms.TextBox()
			Me.LabelIntervalText = New System.Windows.Forms.Label()
			Me.LabelElapsedTimeText = New System.Windows.Forms.Label()
			Me.TextBoxElapsedTime = New System.Windows.Forms.TextBox()
			Me.SuspendLayout()
			' 
			' buttonStart
			' 
			Me.buttonStart.Location = New System.Drawing.Point(27, 88)
			Me.buttonStart.Name = "buttonStart"
			Me.buttonStart.Size = New System.Drawing.Size(102, 23)
			Me.buttonStart.TabIndex = 0
			Me.buttonStart.Text = "Start"
			Me.buttonStart.UseVisualStyleBackColor = True
'			Me.buttonStart.Click += New System.EventHandler(Me.ButtonStartClick)
			' 
			' buttonStop
			' 
			Me.buttonStop.Location = New System.Drawing.Point(158, 87)
			Me.buttonStop.Name = "buttonStop"
			Me.buttonStop.Size = New System.Drawing.Size(102, 23)
			Me.buttonStop.TabIndex = 1
			Me.buttonStop.Text = "Stop"
			Me.buttonStop.UseVisualStyleBackColor = True
'			Me.buttonStop.Click += New System.EventHandler(Me.ButtonStopClick)
			' 
			' TextBoxInterval
			' 
			Me.TextBoxInterval.Location = New System.Drawing.Point(158, 20)
			Me.TextBoxInterval.Name = "TextBoxInterval"
			Me.TextBoxInterval.Size = New System.Drawing.Size(102, 20)
			Me.TextBoxInterval.TabIndex = 2
			Me.TextBoxInterval.Text = "1111"
			' 
			' LabelIntervalText
			' 
			Me.LabelIntervalText.AutoSize = True
			Me.LabelIntervalText.Location = New System.Drawing.Point(24, 23)
			Me.LabelIntervalText.Name = "LabelIntervalText"
			Me.LabelIntervalText.Size = New System.Drawing.Size(128, 13)
			Me.LabelIntervalText.TabIndex = 3
			Me.LabelIntervalText.Text = "Timer Interval (micro sec):"
			' 
			' LabelElapsedTimeText
			' 
			Me.LabelElapsedTimeText.AutoSize = True
			Me.LabelElapsedTimeText.Location = New System.Drawing.Point(24, 55)
			Me.LabelElapsedTimeText.Name = "LabelElapsedTimeText"
			Me.LabelElapsedTimeText.Size = New System.Drawing.Size(128, 13)
			Me.LabelElapsedTimeText.TabIndex = 5
			Me.LabelElapsedTimeText.Text = "Elapsed Time (micro sec):"
			' 
			' TextBoxElapsedTime
			' 
			Me.TextBoxElapsedTime.Location = New System.Drawing.Point(158, 52)
			Me.TextBoxElapsedTime.Name = "TextBoxElapsedTime"
			Me.TextBoxElapsedTime.ReadOnly = True
			Me.TextBoxElapsedTime.Size = New System.Drawing.Size(102, 20)
			Me.TextBoxElapsedTime.TabIndex = 6
			' 
			' FormMain
			' 
			Me.AutoScaleDimensions = New System.Drawing.SizeF(6F, 13F)
			Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
			Me.ClientSize = New System.Drawing.Size(284, 132)
			Me.Controls.Add(Me.TextBoxElapsedTime)
			Me.Controls.Add(Me.LabelElapsedTimeText)
			Me.Controls.Add(Me.LabelIntervalText)
			Me.Controls.Add(Me.TextBoxInterval)
			Me.Controls.Add(Me.buttonStop)
			Me.Controls.Add(Me.buttonStart)
			Me.Name = "FormMain"
			Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
            Me.Text = "Micro Timer Demo"
            '			Me.FormClosing += New System.Windows.Forms.FormClosingEventHandler(Me.FormMainFormClosing)
            Me.ResumeLayout(False)
			Me.PerformLayout()

		End Sub

		#End Region

		Private WithEvents buttonStart As System.Windows.Forms.Button
		Private WithEvents buttonStop As System.Windows.Forms.Button
		Private TextBoxInterval As System.Windows.Forms.TextBox
		Private LabelIntervalText As System.Windows.Forms.Label
		Private LabelElapsedTimeText As System.Windows.Forms.Label
		Private TextBoxElapsedTime As System.Windows.Forms.TextBox
	End Class
End Namespace

