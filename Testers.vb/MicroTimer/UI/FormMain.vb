Imports isr.Core.Constructs

Namespace MicroTimerWinFormsDemo
    Partial Public Class FormMain
        Inherits Form

        ' Declare MicroTimer
        Private ReadOnly _MicroTimer As MicroTimer

        Public Sub New()
            Me.InitializeComponent()

            ' Instantiate new MicroTimer and add event handler
            Me._MicroTimer = New MicroTimer()
            AddHandler Me._MicroTimer.MicroTimerElapsed, AddressOf Me.OnTimedEvent
        End Sub

        Private Sub OnTimedEvent(ByVal sender As Object, ByVal timerEventArgs As MicroTimerEventArgs)
            ' Do something small that takes significantly less time than Interval.
            ' BeginInvoke executes on the UI thread but this calling thread does not
            '  wait for completion before continuing (i.e. it executes asynchronously)
            If Me.InvokeRequired Then
                Me.BeginInvoke(CType(Sub() Me.TextBoxElapsedTime.Text = timerEventArgs.ElapsedMicroseconds.ToString("#,#"), MethodInvoker))
            End If
        End Sub

        Private Sub ButtonStartClick(ByVal sender As Object, ByVal e As EventArgs) Handles buttonStart.Click
            Dim interval As Long

            ' Read interval from form
            If Not Long.TryParse(Me.TextBoxInterval.Text, interval) Then
                Return
            End If

            ' Set timer interval
            Me._MicroTimer.Interval = interval

            ' Ignore event if late by half the interval
            Me._MicroTimer.IgnoreEventIfLateBy = interval \ 2

            ' Start timer
            Me._MicroTimer.Start()
        End Sub

        Private Sub ButtonStopClick(ByVal sender As Object, ByVal e As EventArgs) Handles buttonStop.Click
            ' Stop the timer
            Me._MicroTimer.Stop()
        End Sub

        Private Sub FormMainFormClosing(ByVal sender As Object, ByVal e As FormClosingEventArgs) Handles MyBase.FormClosing
            ' Stop the timer, wait for up to 1 sec for current event to finish,
            ' if it does not finish within this time abort the timer thread
            Me._MicroTimer.Stop(TimeSpan.FromMilliseconds(1000))
            ' If Not _microTimer.StopAndWait(TimeSpan.FromSeconds(1000)) Then                _microTimer.Abort()
        End Sub
    End Class
End Namespace
