﻿Imports System.Drawing
Imports System.Windows.Forms

Partial Public Class uclExtensions : Inherits UserControl
   ' this one uses the AsyncWorkerX - methods as Extension-methods. Note the identical signatur of
   '°Extension and original call.

   Private _EvaluateData As Action(Of DateTime, Point) = AddressOf EvaluateData
   Private _DisplayResult As Action(Of String, Point) = AddressOf DisplayResult

   Private Sub Ucl_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Me.MouseDown
      'EvaluateData(DateTime.Now, e.Location);
      Me._EvaluateData.RunAsync(DateTime.Now, e.Location)
   End Sub

   Private Sub EvaluateData(ByVal t As DateTime, ByVal p As Point)
      System.Threading.Thread.Sleep(1000)
      Dim s = String.Format("Position {0} / {1}" & vbLf & "clicked at {2:T}", p.X, p.Y, t)
      'DisplayResult(s, p);
      Me._DisplayResult.NotifyGui(s, p)
   End Sub

   Private Sub DisplayResult(ByVal s As String, ByVal p As Point)
      label1.Text = s
      label1.Location = p - label1.Size
   End Sub

End Class
