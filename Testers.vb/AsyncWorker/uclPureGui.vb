﻿Imports System.Drawing
Imports System.Windows.Forms

Partial Public Class uclPureGui : Inherits UserControl
   'the basic process, which blocks the Gui

   Private Sub Ucl_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Me.MouseDown
      'evaluate data
      System.Threading.Thread.Sleep(1000)
      Dim p = e.Location
      Dim s = String.Concat( 
         "Position ", p.X, "/", p.Y, Lf, "clicked at ", DateTime.Now.ToString("T"))
      'display result
      label1.Text = s
      label1.Location = p - label1.Size
   End Sub

End Class