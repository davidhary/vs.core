﻿Imports System.Windows.Forms

Partial Public Class uclCountDown : Inherits UserControl

   Private WithEvents _CountDown As New CountDown()

   Public Sub New()
      InitializeComponent()
      ckIsRunning.DataBindings.Add( 
         "Checked", Me._CountDown, "IsRunning", False, DataSourceUpdateMode.OnPropertyChanged)
   End Sub

   Private Sub CountDown_Tick(ByVal sender As Object, ByVal e As CountDown.TickEventargs) 
         Handles _CountDown.Tick
      label1.Text = e.Counter.ToString()
      If e.Counter = 0 Then
         MessageBox.Show("Go!")
      End If
   End Sub

   Private Sub CkIsRunning_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) 
         Handles ckIsRunning.CheckedChanged
      If ckIsRunning.Checked Then Me._CountDown.Start(CInt(udInitValue.Value))

   End Sub

   Private Sub UclCountDown_Disposed(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Disposed
      Me._CountDown.Dispose()
   End Sub

End Class
