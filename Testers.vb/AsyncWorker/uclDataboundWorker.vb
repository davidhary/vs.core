﻿Imports System.Drawing
Imports System.Windows.Forms
Imports System.ComponentModel

Partial Public Class uclDataBoundWorker : Inherits UserControl

   Private _Worker As New AsyncWorker()

   Public Sub New()
      InitializeComponent()
      progressBar1.DataBindings.Add("Visible", Me._Worker, "IsRunning")
      btCancel.DataBindings.Add("Visible", Me._Worker, "IsRunning")
   End Sub

   Protected Overrides Sub OnMouseDown(ByVal e As MouseEventArgs)
      If Not Me._Worker.IsRunning Then
         Me._Worker.RunAsync(AddressOf EvaluateData, DateTime.Now, e.Location)
      End If
      MyBase.OnMouseDown(e)
   End Sub

   Private Sub EvaluateData(ByVal t As DateTime, ByVal p As Point)
      For i = 0 To 299
         Threading.Thread.Sleep(10)
         'reports a progress only if the previous report was more than 0.3s ago
         'leave method, if cancellation was requested
         If Not Me._Worker.ReportProgress(AddressOf ReportProgress, i \ 3) Then Return
      Next
      Dim s = String.Format("Position {0} / {1}" & vbLf & "clicked at {2:T}", p.X, p.Y, t)
      Me._Worker.NotifyGui(AddressOf DisplayResult, s, p)
   End Sub

   Private Sub ReportProgress(ByVal value As Integer)
      progressBar1.Value = value
   End Sub

   Private Sub DisplayResult(ByVal s As String, ByVal p As Point)
      label1.Text = s
      label1.Location = p - label1.Size
   End Sub

   Private Sub BtCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btCancel.Click
      Me._Worker.Cancel()
      label1.Text = "Canceled"
   End Sub

End Class
