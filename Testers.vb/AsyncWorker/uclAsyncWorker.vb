﻿Imports System.Drawing
Imports System.Windows.Forms

Partial Public Class uclAsyncWorker : Inherits UserControl

    Private WithEvents _Worker As New AsyncWorker()

    Public Sub New()
      InitializeComponent()
      UpdateGui()
   End Sub

    Private Sub Worker_IsRunningChanged(ByVal sender As Object, ByVal e As EventArgs) Handles _Worker.IsRunningChanged
        UpdateGui()
    End Sub

    Private Sub UpdateGui()
      btCancel.Visible = Me._Worker.IsRunning
      progressBar1.Visible = Me._Worker.IsRunning
      If Me._Worker.IsRunning Then
         RemoveHandler Me.MouseDown, AddressOf ucl_MouseDown
         progressBar1.Value = 0
      Else

         AddHandler Me.MouseDown, AddressOf ucl_MouseDown
      End If
   End Sub

   Private Sub Ucl_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs)
      Me._Worker.RunAsync(AddressOf EvaluateData, DateTime.Now, e.Location)
   End Sub

   Private Sub EvaluateData(ByVal t As DateTime, ByVal p As Point)
      For i = 0 To 299
         Threading.Thread.Sleep(10)
         'reports a progress only if the previous report was more than 0.3s ago
         If Not Me._Worker.ReportProgress(AddressOf ReportProgress, i \ 3) Then Return
      Next
      Dim s = String.Format("Position {0} / {1}" & vbLf & "clicked at {2:T}", p.X, p.Y, t)
      Me._Worker.NotifyGui(AddressOf DisplayResult, s, p)
   End Sub

   Private Sub ReportProgress(ByVal value As Integer)
      progressBar1.Value = value
   End Sub

   Private Sub DisplayResult(ByVal s As String, ByVal p As Point)
      label1.Text = s
      label1.Location = p - label1.Size
   End Sub

   Private Sub BtCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btCancel.Click
      Me._Worker.Cancel()
      label1.Text = "Canceled"
   End Sub

End Class
