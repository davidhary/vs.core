﻿Imports System.Windows.Forms
Imports System.ComponentModel

Partial Public Class uclExceptions : Inherits UserControl

   Private _ThrowEx As Action(Of String) = AddressOf ThrowEx

   Private Sub ThrowEx(ByVal text As String)
      Throw New Exception(text)
   End Sub

   Private Sub Button_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btBackgroundworker.Click, 
         btAsyncworker.Click, btWithEndInvoke.Click, btWithoutEndInvoke.Click, btSynchron.Click
      Select Case True
         Case sender Is btSynchron
            ThrowEx("thrown in main-thread")
         Case sender Is btBackgroundworker
            BackgroundWorker1.RunWorkerAsync()
         Case sender Is btAsyncworker
            Me._ThrowEx.RunAsync("thrown by Asyncworker")
         Case sender Is btWithEndInvoke
            Me._ThrowEx.BeginInvoke("thrown with EndInvoke", AddressOf Me.ThrowEx.EndInvoke, Nothing)
         Case sender Is btWithoutEndInvoke
            Me._ThrowEx.BeginInvoke("thrown without EndInvoke", Nothing, Nothing)
      End Select
   End Sub

   Private Sub BackgroundWorker1_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs)
      ThrowEx("thrown by  backgroundWorker1")
   End Sub

End Class
