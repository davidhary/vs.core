Imports System.Windows.Forms
Imports isr.Core.Controls

Public Partial Class Form2
    Inherits Form
    Public Sub New()
        Me.InitializeComponent()
    End Sub

    Private Sub CheckBoxB_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles checkBox4.CheckedChanged, checkBox3.CheckedChanged, checkBox2.CheckedChanged, checkBox1.CheckedChanged
        Dim tag As String = DirectCast(sender, Control).Tag.ToString()
        Dim ctrls As Control() = Me.panel1.Controls.Find("customControl1" & tag, False)
        If ctrls.Length > 0 Then
            Dim cam As ShapeControl = DirectCast(ctrls(0), ShapeControl)
            cam.Blink = DirectCast(sender, System.Windows.Forms.CheckBox).Checked
        End If


    End Sub

    Private Sub CheckBoxV_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles checkBox8.CheckedChanged, checkBox7.CheckedChanged, checkBox6.CheckedChanged, checkBox5.CheckedChanged
        Dim tag As String = DirectCast(sender, Control).Tag.ToString()
        Dim ctrls As Control() = Me.panel1.Controls.Find("customControl1" & tag, False)
        If ctrls.Length > 0 Then
            Dim cam As ShapeControl = DirectCast(ctrls(0), ShapeControl)
            cam.Vibrate = DirectCast(sender, System.Windows.Forms.CheckBox).Checked
        End If
    End Sub
End Class
