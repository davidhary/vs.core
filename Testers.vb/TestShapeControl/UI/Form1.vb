Imports System.Drawing
Imports System.Windows.Forms
Imports System.Reflection
Imports isr.Core.Controls

''' <summary>
''' Summary description for Form1.
''' </summary>
Public Class Form1
    Inherits System.Windows.Forms.Form


    <Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Public Sub New()

        '
        ' Required for Windows Form Designer support
        '
        Me.InitializeComponent()

        '
        ' TODO: Add any constructor code after InitializeComponent call
        '
        Me.bm = New Bitmap(Me.panel1.Width, Me.panel1.Height)
        Dim g As Graphics = System.Drawing.Graphics.FromImage(Me.bm)
        g.FillRectangle(Brushes.LightBlue, New Rectangle(0, 0, Me.panel1.Width, Me.panel1.Height))
        Using sf As New StringFormat() With {.Alignment = StringAlignment.Center, .LineAlignment = StringAlignment.Center}
            Using arialFont As New Font("Arial", 18, FontStyle.Bold)
                g.DrawString("THIS IS THE TEST BACKGROUND", arialFont,
                     Brushes.Brown, New Rectangle(New Point(0, 0), New Size(Me.panel1.Width, Me.panel1.Height)), sf)

            End Using
        End Using
        Me.panel1.BackgroundImage = Me.bm
    End Sub

    ''' <summary>
    ''' Clean up any resources being used.
    ''' </summary>
    Protected Overrides Sub Dispose(disposing As Boolean)
        If disposing Then
            If Me.components IsNot Nothing Then
                Me.components.Dispose()
                If Me.bm IsNot Nothing Then Me.bm.Dispose() : Me.bm = Nothing
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    ''' <summary>
    ''' The main entry point for the application.
    ''' </summary>
    <STAThread>
    Private Shared Sub Main()
        Application.Run(New Form1())
    End Sub

    Private Sub ShapeControl1_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles shapeControl1.MouseDown
        If e.Button.Equals(MouseButtons.Left) Then
            Me.sx = e.X
            'temp region = ((ShapeControl)sender).Region.Clone();
            '((ShapeControl)sender).Region = null;
            Me.sy = e.Y
        End If
    End Sub

    Private Sub ShapeControl1_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles shapeControl1.MouseMove
        If e.Button.Equals(MouseButtons.Left) Then
            Dim ctrl As Control = TryCast(sender, Control)
            If ctrl IsNot Nothing Then
                ctrl.Left += (e.X - Me.sx)
                ctrl.Top += (e.Y - Me.sy)
            End If
        End If
    End Sub

    Private Sub Form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.DoubleBuffered = True
        GetType(Panel).InvokeMember(NameOf(Me.DoubleBuffered), BindingFlags.NonPublic Or BindingFlags.Instance Or BindingFlags.SetProperty,
                                    Nothing, Me.panel1, New Object() {True}, Globalization.CultureInfo.InvariantCulture)
    End Sub

    Private Sub ShapeControl4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles shapeControl4.Click
        Me.shapeControl5.Visible = True
        Me.Refresh()
        System.Threading.Thread.Sleep(1000)
        Me.shapeControl5.Visible = False
        Me.Refresh()
    End Sub

    Private Sub ShapeControl3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles shapeControl3.Click
        Me.Close()
    End Sub

End Class
