Imports isr.Core.Controls
Partial Public Class Form2
    ''' <summary>
    ''' Required designer variable.
    ''' </summary>
    Private components As System.ComponentModel.IContainer = Nothing

    ''' <summary>
    ''' Clean up any resources being used.
    ''' </summary>
    ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    Protected Overrides Sub Dispose(disposing As Boolean)
        If disposing AndAlso (components IsNot Nothing) Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

#Region "Windows Form Designer generated code"

    ''' <summary>
    ''' Required method for Designer support - do not modify
    ''' the contents of this method with the code editor.
    ''' </summary>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form2))
        Me.checkBox1 = New System.Windows.Forms.CheckBox
        Me.checkBox2 = New System.Windows.Forms.CheckBox
        Me.checkBox3 = New System.Windows.Forms.CheckBox
        Me.checkBox4 = New System.Windows.Forms.CheckBox
        Me.label1 = New System.Windows.Forms.Label
        Me.label2 = New System.Windows.Forms.Label
        Me.checkBox5 = New System.Windows.Forms.CheckBox
        Me.checkBox6 = New System.Windows.Forms.CheckBox
        Me.checkBox7 = New System.Windows.Forms.CheckBox
        Me.checkBox8 = New System.Windows.Forms.CheckBox
        Me.panel1 = New System.Windows.Forms.Panel
        Me.customControl14 = New ShapeControl
        Me.customControl13 = New ShapeControl
        Me.customControl12 = New ShapeControl
        Me.customControl11 = New ShapeControl
        Me.panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'checkBox1
        '
        Me.checkBox1.AutoSize = True
        Me.checkBox1.Location = New System.Drawing.Point(135, 521)
        Me.checkBox1.Name = "checkBox1"
        Me.checkBox1.Size = New System.Drawing.Size(52, 17)
        Me.checkBox1.TabIndex = 1
        Me.checkBox1.Tag = "1"
        Me.checkBox1.Text = "cam1"
        Me.checkBox1.UseVisualStyleBackColor = True
        '
        'checkBox2
        '
        Me.checkBox2.AutoSize = True
        Me.checkBox2.Location = New System.Drawing.Point(205, 521)
        Me.checkBox2.Name = "checkBox2"
        Me.checkBox2.Size = New System.Drawing.Size(52, 17)
        Me.checkBox2.TabIndex = 2
        Me.checkBox2.Tag = "2"
        Me.checkBox2.Text = "cam2"
        Me.checkBox2.UseVisualStyleBackColor = True
        '
        'checkBox3
        '
        Me.checkBox3.AutoSize = True
        Me.checkBox3.Location = New System.Drawing.Point(135, 544)
        Me.checkBox3.Name = "checkBox3"
        Me.checkBox3.Size = New System.Drawing.Size(52, 17)
        Me.checkBox3.TabIndex = 3
        Me.checkBox3.Tag = "3"
        Me.checkBox3.Text = "cam3"
        Me.checkBox3.UseVisualStyleBackColor = True
        '
        'checkBox4
        '
        Me.checkBox4.AutoSize = True
        Me.checkBox4.Location = New System.Drawing.Point(205, 544)
        Me.checkBox4.Name = "checkBox4"
        Me.checkBox4.Size = New System.Drawing.Size(52, 17)
        Me.checkBox4.TabIndex = 4
        Me.checkBox4.Tag = "4"
        Me.checkBox4.Text = "cam4"
        Me.checkBox4.UseVisualStyleBackColor = True
        '
        'label1
        '
        Me.label1.AutoSize = True
        Me.label1.Location = New System.Drawing.Point(18, 521)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(75, 13)
        Me.label1.TabIndex = 5
        Me.label1.Text = "Check to blink"
        '
        'label2
        '
        Me.label2.AutoSize = True
        Me.label2.Location = New System.Drawing.Point(421, 521)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(85, 13)
        Me.label2.TabIndex = 10
        Me.label2.Text = "Check to vibrate"
        '
        'checkBox5
        '
        Me.checkBox5.AutoSize = True
        Me.checkBox5.Location = New System.Drawing.Point(608, 544)
        Me.checkBox5.Name = "checkBox5"
        Me.checkBox5.Size = New System.Drawing.Size(52, 17)
        Me.checkBox5.TabIndex = 9
        Me.checkBox5.Tag = "4"
        Me.checkBox5.Text = "cam4"
        Me.checkBox5.UseVisualStyleBackColor = True
        '
        'checkBox6
        '
        Me.checkBox6.AutoSize = True
        Me.checkBox6.Location = New System.Drawing.Point(538, 544)
        Me.checkBox6.Name = "checkBox6"
        Me.checkBox6.Size = New System.Drawing.Size(52, 17)
        Me.checkBox6.TabIndex = 8
        Me.checkBox6.Tag = "3"
        Me.checkBox6.Text = "cam3"
        Me.checkBox6.UseVisualStyleBackColor = True
        '
        'checkBox7
        '
        Me.checkBox7.AutoSize = True
        Me.checkBox7.Location = New System.Drawing.Point(608, 521)
        Me.checkBox7.Name = "checkBox7"
        Me.checkBox7.Size = New System.Drawing.Size(52, 17)
        Me.checkBox7.TabIndex = 7
        Me.checkBox7.Tag = "2"
        Me.checkBox7.Text = "cam2"
        Me.checkBox7.UseVisualStyleBackColor = True
        '
        'checkBox8
        '
        Me.checkBox8.AutoSize = True
        Me.checkBox8.Location = New System.Drawing.Point(538, 521)
        Me.checkBox8.Name = "checkBox8"
        Me.checkBox8.Size = New System.Drawing.Size(52, 17)
        Me.checkBox8.TabIndex = 6
        Me.checkBox8.Tag = "1"
        Me.checkBox8.Text = "cam1"
        Me.checkBox8.UseVisualStyleBackColor = True
        '
        'panel1
        '
        Me.panel1.BackgroundImage = Global.isr.Core.Tester.My.Resources.Resources.SIT_Redhill_Close_3_room_63sqm
        Me.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.panel1.Controls.Add(Me.customControl14)
        Me.panel1.Controls.Add(Me.customControl13)
        Me.panel1.Controls.Add(Me.customControl12)
        Me.panel1.Controls.Add(Me.customControl11)
        Me.panel1.Location = New System.Drawing.Point(12, 12)
        Me.panel1.Name = "panel1"
        Me.panel1.Size = New System.Drawing.Size(707, 497)
        Me.panel1.TabIndex = 0
        '
        'customControl14
        '
        Me.customControl14.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.customControl14.Blink = False
        Me.customControl14.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.customControl14.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
        Me.customControl14.BorderWidth = 3
        Me.customControl14.CenterColor = System.Drawing.Color.FromArgb(CType(CType(100, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.customControl14.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.customControl14.ForeColor = System.Drawing.SystemColors.ControlText
        Me.customControl14.Location = New System.Drawing.Point(517, 404)
        Me.customControl14.Name = "customControl14"
        Me.customControl14.Shape = ShapeType.Ellipse
        Me.customControl14.ShapeImage = Nothing
        Me.customControl14.Size = New System.Drawing.Size(33, 32)
        Me.customControl14.SurroundColor = System.Drawing.Color.FromArgb(CType(CType(100, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.customControl14.TabIndex = 3
        Me.customControl14.Tag2 = String.Empty
        Me.customControl14.Text = "cam4"
        Me.customControl14.UseGradient = False
        Me.customControl14.Vibrate = False
        '
        'customControl13
        '
        Me.customControl13.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.customControl13.Blink = False
        Me.customControl13.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.customControl13.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
        Me.customControl13.BorderWidth = 3
        Me.customControl13.CenterColor = System.Drawing.Color.FromArgb(CType(CType(100, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.customControl13.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.customControl13.ForeColor = System.Drawing.Color.Yellow
        Me.customControl13.Location = New System.Drawing.Point(302, 386)
        Me.customControl13.Name = "customControl13"
        Me.customControl13.Shape = ShapeType.Ellipse
        Me.customControl13.ShapeImage = CType(resources.GetObject("customControl13.ShapeImage"), System.Drawing.Image)
        Me.customControl13.Size = New System.Drawing.Size(43, 50)
        Me.customControl13.SurroundColor = System.Drawing.Color.FromArgb(CType(CType(100, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.customControl13.TabIndex = 2
        Me.customControl13.Tag2 = String.Empty
        Me.customControl13.Text = "cam3"
        Me.customControl13.UseGradient = False
        Me.customControl13.Vibrate = False
        '
        'customControl12
        '
        Me.customControl12.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.customControl12.Blink = False
        Me.customControl12.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.customControl12.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
        Me.customControl12.BorderWidth = 3
        Me.customControl12.CenterColor = System.Drawing.Color.FromArgb(CType(CType(100, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.customControl12.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.customControl12.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.customControl12.Location = New System.Drawing.Point(517, 205)
        Me.customControl12.Name = "customControl12"
        Me.customControl12.Shape = ShapeType.Ellipse
        Me.customControl12.ShapeImage = CType(resources.GetObject("customControl12.ShapeImage"), System.Drawing.Image)
        Me.customControl12.Size = New System.Drawing.Size(42, 39)
        Me.customControl12.SurroundColor = System.Drawing.Color.FromArgb(CType(CType(100, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.customControl12.TabIndex = 2
        Me.customControl12.Tag2 = String.Empty
        Me.customControl12.Text = "cam2"
        Me.customControl12.UseGradient = False
        Me.customControl12.Vibrate = False
        '
        'customControl11
        '
        Me.customControl11.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.customControl11.Blink = False
        Me.customControl11.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.customControl11.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
        Me.customControl11.BorderWidth = 3
        Me.customControl11.CenterColor = System.Drawing.Color.FromArgb(CType(CType(100, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.customControl11.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.customControl11.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.customControl11.Location = New System.Drawing.Point(266, 192)
        Me.customControl11.Name = "customControl11"
        Me.customControl11.Shape = ShapeType.Ellipse
        Me.customControl11.ShapeImage = CType(resources.GetObject("customControl11.ShapeImage"), System.Drawing.Image)
        Me.customControl11.Size = New System.Drawing.Size(59, 52)
        Me.customControl11.SurroundColor = System.Drawing.Color.FromArgb(CType(CType(100, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.customControl11.TabIndex = 0
        Me.customControl11.Tag2 = String.Empty
        Me.customControl11.Text = " cam1"
        Me.customControl11.UseGradient = False
        Me.customControl11.Vibrate = False
        '
        'Form2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.ClientSize = New System.Drawing.Size(747, 578)
        Me.Controls.Add(Me.label2)
        Me.Controls.Add(Me.checkBox5)
        Me.Controls.Add(Me.checkBox6)
        Me.Controls.Add(Me.checkBox7)
        Me.Controls.Add(Me.checkBox8)
        Me.Controls.Add(Me.label1)
        Me.Controls.Add(Me.checkBox4)
        Me.Controls.Add(Me.checkBox3)
        Me.Controls.Add(Me.checkBox2)
        Me.Controls.Add(Me.checkBox1)
        Me.Controls.Add(Me.panel1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Form2"
        Me.Text = "Test Blinking and Vibrating"
        Me.panel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private panel1 As System.Windows.Forms.Panel
    Private customControl12 As ShapeControl
    Private customControl11 As ShapeControl
    Private customControl13 As ShapeControl
    Private customControl14 As ShapeControl
    Private WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Private WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Private WithEvents CheckBox3 As System.Windows.Forms.CheckBox
    Private WithEvents CheckBox4 As System.Windows.Forms.CheckBox
    Private label1 As System.Windows.Forms.Label
    Private label2 As System.Windows.Forms.Label
    Private WithEvents CheckBox5 As System.Windows.Forms.CheckBox
    Private WithEvents CheckBox6 As System.Windows.Forms.CheckBox
    Private WithEvents CheckBox7 As System.Windows.Forms.CheckBox
    Private WithEvents CheckBox8 As System.Windows.Forms.CheckBox
End Class
