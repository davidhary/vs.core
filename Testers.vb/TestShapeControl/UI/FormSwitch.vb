Imports System.Windows.Forms

Public Partial Class FormSwitch
	Inherits Form
	Public Sub New()
        Me.InitializeComponent()
	End Sub

    <Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Private Sub Button1_Click(sender As Object, e As EventArgs)
        Dim f1 As New Form1()
        f1.ShowDialog()
    End Sub

    <Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Private Sub Button2_Click(sender As Object, e As EventArgs)
        Dim f2 As New Form2()
        f2.ShowDialog()
    End Sub

    <Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Private Sub Button3_Click(sender As Object, e As EventArgs)
        Dim f3 As New Form3()
        f3.ShowDialog()
    End Sub
End Class
