Imports System.Collections.Generic
Imports System.Drawing
Imports System.Windows.Forms
Imports System.Reflection
Imports System.Linq
Imports System.IO
Imports isr.Core.Controls
Imports System.Diagnostics

Partial Public Class Form3
    Inherits Form

    Private _ControlList As New List(Of ShapeControl)()
    Private _Sx As Integer, _Sy As Integer

    Private _Static_i As Integer = 0
    Private _CtrlKey As Boolean = False
    Private _AltKey As Boolean = False
    Private _PlusKey As Boolean = False
    Private _MinusKey As Boolean = False
    Public Sub New()
        Me.InitializeComponent()
    End Sub

    Private Sub BtnAddCam_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnAddCam.Click
        Me.AddCam("")
    End Sub

    Private Function GetNextCamIndex() As Integer
        If Me._ControlList.Count = 0 Then
            Return 1
        End If
        Dim tempvar As Object
        tempvar = Me._ControlList.OrderBy(Function(x) x.Name).ToList()
        Me._ControlList = DirectCast(tempvar, List(Of ShapeControl))
        Me._ControlList = Me._ControlList.OrderBy(Function(x) x.Name.Length).ToList()
        Dim templist As List(Of ShapeControl) = Me._ControlList.ToList()
        Dim count As Integer = templist.Count
        Dim retval As Integer = count + 1

        'find missing index
        For i As Integer = 0 To count - 1
            Dim ctrlname As String = templist(i).Name
            Dim ctrlindex As String = ctrlname.Substring(3)
            If (i + 1) <> Integer.Parse(ctrlindex) Then
                retval = (i + 1)
                Exit For
            End If
        Next


        Return retval
    End Function



    <Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    <Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId:="y")>
    <Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId:="x")>
    <Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")>
    Private Sub AddCam(caminfo As String)

        Dim bNew As Boolean = String.IsNullOrEmpty(caminfo)
        Dim name As String = String.Empty
        Dim tag As String = String.Empty, tag2 As String = String.Empty
        Dim x As Integer = 0, y As Integer = 0, w As Integer = 0, h As Integer = 0, c As Integer = 0

        If Not String.IsNullOrEmpty(caminfo) Then
            Dim info As String() = caminfo.Split("|"c)
            For i As Integer = 0 To info.Length - 1
                Dim details As String() = info(i).Split("="c)
                Select Case details(0)
                    Case "name"
                        name = details(1)
                        Exit Select
                    Case "x"
                        x = Integer.Parse(details(1))
                        Exit Select
                    Case "y"
                        y = Integer.Parse(details(1))
                        Exit Select
                    Case "w"
                        w = Integer.Parse(details(1))
                        Exit Select
                    Case "h"
                        h = Integer.Parse(details(1))
                        Exit Select
                    Case "c"
                        c = Integer.Parse(details(1))
                        Exit Select
                    Case "tag"
                        tag = details(1)
                        Exit Select
                    Case "tag2"
                        tag2 = details(1)
                        Exit Select

                End Select
            Next
        End If
        ' ctrllist.Add(ctrl1);
        Dim ctrl1 As New ShapeControl() With {
            .BackColor = If(bNew, System.Drawing.Color.FromArgb(CInt(CByte(126)), Color.Red), Color.FromArgb(c)),
            .Blink = False,
            .BorderColor = System.Drawing.Color.FromArgb(CInt(CByte(0)), CInt(CByte(255)), CInt(CByte(255)), CInt(CByte(255))),
            .BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid,
            .BorderWidth = 3,
            .Font = New System.Drawing.Font("Arial", 8.0F, System.Drawing.FontStyle.Bold),
            .Name = If(bNew, "cam" & Me.GetNextCamIndex(), name),
            .Shape = ShapeType.Ellipse,
            .ShapeImage = Global.isr.Core.Tester.My.Resources.camshape,
            .Size = If(bNew, New System.Drawing.Size(40, 40), New System.Drawing.Size(w, h)),
            .TabIndex = 0,
            .UseGradient = False,
            .Vibrate = False,
            .Visible = True}
        '  ctrl1.CenterColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
        'ctrllist.Count;
        ' ctrl1.SurroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));

        AddHandler ctrl1.MouseDown, New MouseEventHandler(AddressOf Me.Ctrl1_MouseDown)
        AddHandler ctrl1.MouseMove, New MouseEventHandler(AddressOf Me.Ctrl1_MouseMove)

        AddHandler ctrl1.MouseDoubleClick, New MouseEventHandler(AddressOf Me.Ctrl1_MouseDoubleClick)
        AddHandler ctrl1.MouseHover, New EventHandler(AddressOf Me.Ctrl1_MouseHover)
        Me._ControlList.Add(ctrl1)
        Dim ypos As Integer = (50 * Me._ControlList.Count) Mod Me.Panel1.Height
        Dim xpos As Integer = ((50 * Me._ControlList.Count) \ Me.Panel1.Height) * 50
        ctrl1.Location = If(bNew, New System.Drawing.Point(50 + xpos, ypos - 40), New System.Drawing.Point(50, 50))
        Me.Panel1.Controls.Add(ctrl1)
        ctrl1.Text = "cam"
        ctrl1.Text = If(bNew, DirectCast(ctrl1.Name.ToString().Clone(), String), name)
        ctrl1.BringToFront()
        ctrl1.Tag2 = If(bNew, "127.0.0.1:New cam", tag2)
        'set the color
        If bNew Then
            Me.Ctrl1_MouseDoubleClick(ctrl1, New MouseEventArgs(MouseButtons.Left, 2, 0, 0, 0))
        End If


        Dim dy As Single = CSng(ctrl1.Top + ctrl1.Height \ 2) - CSng(Me.Panel1.Height) / 2
        Dim dx As Single = CSng(ctrl1.Left + ctrl1.Width \ 2) - CSng(Me.Panel1.Width) / 2

        ctrl1.Tag = If(bNew, (dx & "," & dy & "," & Me.GetNumPixelforImageDisplayed()), tag)

    End Sub


    Private Sub Ctrl1_MouseHover(sender As Object, e As EventArgs)
        Dim ctrl As ShapeControl = TryCast(sender, ShapeControl)
        If ctrl Is Nothing Then Return
        Me.toolTip1.Show(ctrl.Tag2 & ",(" & ctrl.Left & "," & ctrl.Top & ")", ctrl, 2000)
    End Sub

    <Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1300:SpecifyMessageBoxOptions")>
    Private Sub Ctrl1_MouseDoubleClick(sender As Object, e As MouseEventArgs)
        If e.Clicks < 2 Then
            Return
        End If
        Dim ctrl As ShapeControl = TryCast(sender, ShapeControl)
        If ctrl Is Nothing Then Return

        If e.Button.Equals(MouseButtons.Left) Then
            If Me._PlusKey AndAlso Not Me._MinusKey Then
                If ctrl.Width < 80 Then
                    ctrl.Size = New Size(ctrl.Width + 5, ctrl.Height + 5)
                End If
                Me._PlusKey = False
                Return
            End If

            If Me._MinusKey AndAlso Not Me._PlusKey Then
                If ctrl.Width > 20 Then

                    ctrl.Size = New Size(ctrl.Width - 5, ctrl.Height - 5)
                End If
                Me._MinusKey = False
                Return
            End If
            If Me._CtrlKey AndAlso Not Me._AltKey Then
                Dim dr As DialogResult = MessageBox.Show(Me, "Delete cam?", "Delete", MessageBoxButtons.OKCancel)
                If dr = DialogResult.OK Then
                    Me._ControlList.Remove(ctrl)
                    Me.Panel1.Controls.Remove(ctrl)
                End If
                Me._CtrlKey = False

                Return
            End If

            If Me._AltKey AndAlso Not Me._CtrlKey Then
                ctrl.Vibrate = Not ctrl.Vibrate
                Me._AltKey = False
                Return
            End If

            If Me._Static_i >= 6 Then
                Me._Static_i = 0
            End If
            Select Case Me._Static_i
                Case 0
                    ctrl.BackColor = System.Drawing.Color.FromArgb(126, Color.Red)
                    Exit Select
                Case 1
                    ctrl.BackColor = System.Drawing.Color.FromArgb(126, Color.Blue)

                    Exit Select

                Case 2
                    ctrl.BackColor = System.Drawing.Color.FromArgb(126, Color.Green)

                    Exit Select

                Case 3
                    ctrl.BackColor = System.Drawing.Color.FromArgb(126, Color.Wheat)
                    Exit Select
                Case 4
                    ctrl.BackColor = System.Drawing.Color.FromArgb(126, Color.GreenYellow)

                    Exit Select

                Case 5
                    ctrl.BackColor = System.Drawing.Color.FromArgb(126, Color.Cyan)

                    Exit Select

            End Select
            Me._Static_i += 1
        End If


    End Sub

    <Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Private Sub Ctrl1_MouseDown(sender As Object, e As System.Windows.Forms.MouseEventArgs)
        If e.Button.Equals(MouseButtons.Left) Then
            Me._Sx = e.X

            Me._Sy = e.Y
        End If
        If e.Button.Equals(MouseButtons.Right) Then
            Dim frm As New FormProperty() With {.Caller = DirectCast(sender, ShapeControl)}
            frm.ShowDialog()
        End If
    End Sub

    Private Sub Ctrl1_MouseMove(sender As Object, e As System.Windows.Forms.MouseEventArgs)
        Dim ctrl As Control = TryCast(sender, Control)
        If ctrl Is Nothing Then Return

        If e.Button.Equals(MouseButtons.Left) Then
            ctrl.Left += (e.X - Me._Sx)
            ctrl.Top += (e.Y - Me._Sy)
            Dim dy As Single = CSng(ctrl.Top) + ctrl.Height \ 2 - CSng(Me.Panel1.Height) / 2
            Dim dx As Single = CSng(ctrl.Left) + ctrl.Width \ 2 - CSng(Me.Panel1.Width) / 2
            ctrl.Tag = dx & "," & dy & "," & Me.GetNumPixelforImageDisplayed()
        End If
    End Sub

    Private Sub Form3_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles MyBase.KeyDown
        Me._CtrlKey = e.Control
        Me._AltKey = e.Alt
        If e.KeyCode = Keys.OemMinus Then
            Me._MinusKey = True
        End If
        If e.KeyCode = Keys.Oemplus Then
            Me._PlusKey = True
        End If
    End Sub

    Private Sub Form3_KeyUp(ByVal sender As Object, ByVal e As KeyEventArgs) Handles MyBase.KeyUp
        Me._CtrlKey = False
        Me._AltKey = False
        Me._MinusKey = False
        Me._PlusKey = False
    End Sub

    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Private Sub Panel1_MouseDoubleClick(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Panel1.MouseDoubleClick
        Me.openFileDialog1.Filter = "Image files (*.bmp;*.jpg;*.gif)|*.bmp;*.jpg;*.gif|All files (*.*)|*.*"
        Dim dr As DialogResult = Me.openFileDialog1.ShowDialog()
        If dr = DialogResult.OK Then
            Try
                Dim tempImage As New Bitmap(Me.openFileDialog1.FileName)
                Me.Panel1.BackgroundImage = New Bitmap(tempImage)


            Catch
            End Try
        End If

    End Sub

    Private Function GetNumPixelforImageDisplayed() As Integer
        If Me.Panel1.BackgroundImage Is Nothing Then Exit Function
        Dim panelratio As Single = CSng(Me.Panel1.Width) / Me.Panel1.Height
        Dim imgratio As Single = CSng(Me.Panel1.BackgroundImage.Width) / CSng(Me.Panel1.BackgroundImage.Height)
        Dim dispwidth As Single, dispheight As Single
        If panelratio > imgratio Then
            'height limiting
            dispheight = Me.Panel1.Height
            dispwidth = imgratio * dispheight
        Else
            dispwidth = Me.Panel1.Width
            dispheight = dispwidth / imgratio
        End If

        ' System.Diagnostics.Debug.Print(imgratio +"," + dispwidth + "," + dispheight);

        Return CInt(Math.Truncate(dispwidth * dispheight))
    End Function

    'new 
    <Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    <Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId:="NewMap")>
    Private Sub Button2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button2.Click
        Me.label1.Text = "NewMap_" & Guid.NewGuid().ToString() & ".map"
        Me.Panel1.BackgroundImage = New Bitmap(Me.Panel1.Width, Me.Panel1.Height)
        Graphics.FromImage(Me.Panel1.BackgroundImage).FillRectangle(Brushes.White, New Rectangle(0, 0, Me.Panel1.Width, Me.Panel1.Height))
        Graphics.FromImage(Me.Panel1.BackgroundImage).DrawString("Dbl Click here to insert floor plan..", New Font(FontFamily.GenericSansSerif, 12), Brushes.Black, 50, 50)
        Me._ControlList.Clear()
        Me.Panel1.Controls.Clear()

    End Sub

    'save
    <Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1300:SpecifyMessageBoxOptions")>
    Private Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
        Using writer As StreamWriter = File.CreateText(Me.label1.Text)
            Me._ControlList = Me._ControlList.OrderBy(Function(x) x.Name).ToList()
            Me._ControlList = Me._ControlList.OrderBy(Function(x) x.Name.Length).ToList()
            Dim templist As List(Of ShapeControl) = Me._ControlList.ToList()
            writer.WriteLine("CAM_COUNT=" & templist.Count)
            For i As Integer = 0 To Me._ControlList.Count - 1

                writer.WriteLine("name=" & templist(i).Name & "|" & "x=" & templist(i).Left & "|" & "y=" & templist(i).Top & "|" & "w=" & templist(i).Width & "|" & "h=" & templist(i).Height & "|" & "c=" & templist(i).BackColor.ToArgb() & "|" & "tag=" & templist(i).Tag.ToString() & "|" & "tag2=" & templist(i).Tag2.ToString())
            Next
        End Using
        If Me.Panel1.BackgroundImage IsNot Nothing Then
            Me.Panel1.BackgroundImage.Save(Me.label1.Text & ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg)
        End If

        MessageBox.Show(Me.label1.Text & " is saved")
    End Sub

    <Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1304:SpecifyCultureInfo", MessageId:="System.Type.InvokeMember(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object,System.Object[])")>
    <Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId:="DblClick")>
    <Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId:="Ctl")>
    Private Sub Form3_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

        Me.DoubleBuffered = True

        'invoke double buffer 
        GetType(Panel).InvokeMember("DoubleBuffered", BindingFlags.NonPublic Or BindingFlags.Instance Or BindingFlags.SetProperty, Nothing, Me.Panel1, New Object() {True})

        Me.label2.Text = "On Cam> Right Click:Set Properties, Dbl_Click:Change Color, Ctl+Dbl_Click:Del, Alt+Dbl_Click:Vibrate, Minus+Dbl_Click:Smaller, Plus+Dbl_Click:Larger"
        Me.Button2_Click(Nothing, Nothing)
    End Sub

    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub BtnImportMap_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnImportMap.Click
        Me.openFileDialog1.Filter = "Map files (*.map)|*.map"
        Dim dr As DialogResult = Me.openFileDialog1.ShowDialog()
        If dr = DialogResult.OK Then

            Me.Button2_Click(Nothing, Nothing)
            Me.label1.Text = Me.openFileDialog1.FileName

            Try
                Using reader As StreamReader = File.OpenText(Me.label1.Text)
                    Dim s As String = reader.ReadLine()
                    Dim info As String() = s.Split("="c)
                    For i As Integer = 0 To Integer.Parse(info(1)) - 1
                        s = reader.ReadLine()
                        Me.AddCam(s)



                    Next
                End Using


                If File.Exists(Me.openFileDialog1.FileName & ".jpg") Then
                    Using tempImage As New Bitmap(Me.openFileDialog1.FileName & ".jpg")
                        Me.Panel1.BackgroundImage = New Bitmap(tempImage)
                    End Using

                End If


                'resize


                Me.UpdateCamPosAfterResize()

            Catch
            End Try
        End If

    End Sub


    Private Sub UpdateCamPosAfterResize()
        Dim newarea As Integer = Me.GetNumPixelforImageDisplayed()


        For i As Integer = 0 To Me._ControlList.Count - 1
            Dim info As String() = Me._ControlList(i).Tag.ToString().Split(","c)
            Dim dx As Single = Single.Parse(info(0))
            Dim dy As Single = Single.Parse(info(1))
            Dim area As Integer = Integer.Parse(info(2))
            'square root of area ratio = linear ratio
            Dim ratio As Single = CSng(Math.Sqrt(CSng(newarea) / area))
            'get the new offset using the calculated linear ratio
            Dim newdx As Single = ratio * dx
            Dim newdy As Single = ratio * dy


            'update the new pos for the cam 
            Me._ControlList(i).Left = CInt(Math.Truncate(Me.Panel1.Width \ 2 + newdx - Me._ControlList(i).Width \ 2))

            Me._ControlList(i).Top = CInt(Math.Truncate(Me.Panel1.Height \ 2 + newdy - Me._ControlList(i).Height \ 2))
        Next



    End Sub

    Private Sub Form3_Resize(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Resize

        Me.Panel1.Visible = False

        Me.Panel1.Height = Me.ClientSize.Height - (3 * Me.Panel1.Top \ 2)
        Me.Panel1.Width = Me.ClientSize.Width - 2 * Me.Panel1.Left
        Me.label2.Top = Me.Panel1.Top + Me.Panel1.Height + 10
        Me.UpdateCamPosAfterResize()

        Me.Panel1.Visible = True

    End Sub



End Class
