Imports System.Windows.Forms
Imports isr.Core.Controls

Public Partial Class FormProperty
    Inherits Form

    Private _Caller As ShapeControl = Nothing

    Public Property Caller() As ShapeControl
        Get
            Return Me._caller
        End Get
        Set
            Me._Caller = Value
            If Value IsNot Nothing Then
                Dim s As String = Me._Caller.Tag2
                Dim info As String() = s.Split(":"c)
                Me.textBoxIP.Text = info(0)
                Me.textBoxNotes.Text = info(1)
                Me.Text = Me._Caller.Text & " properties"
            End If
        End Set
    End Property

    Public Sub New()
        Me.InitializeComponent()
    End Sub

    Private Sub FormProperty_FormClosing(sender As Object, e As FormClosingEventArgs)
        'should validate first
        Me._caller.Tag2 = Me.textBoxIP.Text & ":" & Me.textBoxNotes.Text
    End Sub

    Private Sub FormProperty_Activated(sender As Object, e As EventArgs)
        Me.Location = Me._caller.Location
    End Sub

    Private Sub TextBoxIP_Enter(sender As Object, e As EventArgs)
        Me.textBoxIP.SelectionStart = Me.textBoxIP.Text.Length
    End Sub
End Class
