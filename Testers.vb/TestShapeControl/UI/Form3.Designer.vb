Partial Class Form3
	''' <summary>
	''' Required designer variable.
	''' </summary>
	Private components As System.ComponentModel.IContainer = Nothing

	''' <summary>
	''' Clean up any resources being used.
	''' </summary>
	''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	Protected Overrides Sub Dispose(disposing As Boolean)
		If disposing AndAlso (components IsNot Nothing) Then
			components.Dispose()
		End If
		MyBase.Dispose(disposing)
	End Sub

	#Region "Windows Form Designer generated code"

	''' <summary>
	''' Required method for Designer support - do not modify
	''' the contents of this method with the code editor.
	''' </summary>
	Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.panel1 = New System.Windows.Forms.Panel
        Me.btnImportMap = New System.Windows.Forms.Button
        Me.btnAddCam = New System.Windows.Forms.Button
        Me.openFileDialog1 = New System.Windows.Forms.OpenFileDialog
        Me.button1 = New System.Windows.Forms.Button
        Me.button2 = New System.Windows.Forms.Button
        Me.label1 = New System.Windows.Forms.Label
        Me.label2 = New System.Windows.Forms.Label
        Me.label3 = New System.Windows.Forms.Label
        Me.toolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.SuspendLayout()
        '
        'panel1
        '
        Me.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.panel1.Location = New System.Drawing.Point(24, 98)
        Me.panel1.Name = "panel1"
        Me.panel1.Size = New System.Drawing.Size(652, 430)
        Me.panel1.TabIndex = 0
        '
        'btnImportMap
        '
        Me.btnImportMap.Location = New System.Drawing.Point(24, 12)
        Me.btnImportMap.Name = "btnImportMap"
        Me.btnImportMap.Size = New System.Drawing.Size(57, 27)
        Me.btnImportMap.TabIndex = 1
        Me.btnImportMap.Text = "Load"
        Me.btnImportMap.UseVisualStyleBackColor = True
        '
        'btnAddCam
        '
        Me.btnAddCam.Location = New System.Drawing.Point(107, 12)
        Me.btnAddCam.Name = "btnAddCam"
        Me.btnAddCam.Size = New System.Drawing.Size(61, 26)
        Me.btnAddCam.TabIndex = 2
        Me.btnAddCam.Text = "Add Cam"
        Me.btnAddCam.UseVisualStyleBackColor = True
        '
        'openFileDialog1
        '
        Me.openFileDialog1.FileName = "openFileDialog1"
        '
        'button1
        '
        Me.button1.Location = New System.Drawing.Point(513, 11)
        Me.button1.Name = "button1"
        Me.button1.Size = New System.Drawing.Size(56, 27)
        Me.button1.TabIndex = 3
        Me.button1.Text = "Save"
        Me.button1.UseVisualStyleBackColor = True
        '
        'button2
        '
        Me.button2.Location = New System.Drawing.Point(455, 11)
        Me.button2.Name = "button2"
        Me.button2.Size = New System.Drawing.Size(52, 27)
        Me.button2.TabIndex = 4
        Me.button2.Text = "New"
        Me.button2.UseVisualStyleBackColor = True
        '
        'label1
        '
        Me.label1.AutoSize = True
        Me.label1.Location = New System.Drawing.Point(87, 51)
        Me.label1.MaximumSize = New System.Drawing.Size(550, 30)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(35, 13)
        Me.label1.TabIndex = 5
        Me.label1.Text = "label1"
        '
        'label2
        '
        Me.label2.AutoSize = True
        Me.label2.Location = New System.Drawing.Point(24, 542)
        Me.label2.MaximumSize = New System.Drawing.Size(600, 30)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(35, 13)
        Me.label2.TabIndex = 6
        Me.label2.Text = "label2"
        '
        'label3
        '
        Me.label3.AutoSize = True
        Me.label3.Location = New System.Drawing.Point(24, 51)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(57, 13)
        Me.label3.TabIndex = 7
        Me.label3.Text = "File Name:"
        '
        'Form3
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(705, 580)
        Me.Controls.Add(Me.label3)
        Me.Controls.Add(Me.label2)
        Me.Controls.Add(Me.label1)
        Me.Controls.Add(Me.button2)
        Me.Controls.Add(Me.button1)
        Me.Controls.Add(Me.btnAddCam)
        Me.Controls.Add(Me.btnImportMap)
        Me.Controls.Add(Me.panel1)
        Me.KeyPreview = True
        Me.MinimumSize = New System.Drawing.Size(640, 480)
        Me.Name = "Form3"
        Me.Text = "Cam Map Designer"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

	#End Region

    Private WithEvents Panel1 As System.Windows.Forms.Panel
    Private WithEvents BtnImportMap As System.Windows.Forms.Button
    Private WithEvents BtnAddCam As System.Windows.Forms.Button
	Private openFileDialog1 As System.Windows.Forms.OpenFileDialog
    Private WithEvents Button1 As System.Windows.Forms.Button
    Private WithEvents Button2 As System.Windows.Forms.Button
	Private label1 As System.Windows.Forms.Label
	Private label2 As System.Windows.Forms.Label
	Private label3 As System.Windows.Forms.Label
	Private toolTip1 As System.Windows.Forms.ToolTip

End Class
