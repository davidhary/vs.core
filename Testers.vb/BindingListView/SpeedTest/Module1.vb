Imports isr.Core.WinFormsViews

Friend Module Module1
    Public Sub Main()

        Dim list As New List(Of Foo)
        For i As Integer = 1 To My.Settings.Default.ItemCount
            list.Add(New Foo())
        Next
        Console.WriteLine($"BLV Size: {My.Settings.Default.ItemCount}")
        Console.WriteLine("Times in milliseconds")

        Dim sw As New Stopwatch()

        sw.Start()
        Using view As New BindingListView(Of Foo)(list)
            sw.Stop()
            Console.WriteLine($"BLV Create: {ControlChars.Tab} {ControlChars.Tab} {ControlChars.Tab} {sw.ElapsedMilliseconds}; {ControlChars.Tab} {My.Settings.Default.i7k2600}={My.Settings.Default.i7k2600CreateTimespan.TotalMilliseconds}")

            sw.Reset()
            sw.Start()
            view.Sort = NameOf(Foo.A)
            sw.Stop()
            Console.WriteLine($"BLV.Sort: {ControlChars.Tab} {ControlChars.Tab} {ControlChars.Tab} {sw.ElapsedMilliseconds}; {ControlChars.Tab} {My.Settings.Default.i7k2600}={My.Settings.Default.i7k2600SortInternalTimespan.TotalMilliseconds}")

            sw.Reset()
            sw.Start()
            view.ApplySort(AddressOf AComp)
            sw.Stop()
            Console.WriteLine($"BLV.ApplySort(delegate): {ControlChars.Tab} {sw.ElapsedMilliseconds}; {ControlChars.Tab} {My.Settings.Default.i7k2600}={My.Settings.Default.i7k2600SortExtTimeSpan.TotalMilliseconds}")

        End Using
        Using dataTable As New DataTable
            dataTable.Locale = Globalization.CultureInfo.CurrentCulture
            dataTable.Columns.Add(NameOf(Foo.A), GetType(Integer))
            dataTable.Columns.Add(NameOf(Foo.B), GetType(String))
            For i As Integer = 1 To My.Settings.Default.ItemCount
                Dim row As DataRow = dataTable.NewRow()
                row(0) = list(i - 1).A
                row(1) = list(i - 1).B
                dataTable.Rows.Add(row)
            Next
            sw.Start()
            dataTable.DefaultView.Sort = NameOf(Foo.A)
            sw.Stop()
            Console.WriteLine($"{NameOf(Data.DataTable)}.{NameOf(Data.DataTable.DefaultView)}.Sort: {ControlChars.Tab} {sw.ElapsedMilliseconds}; {ControlChars.Tab} {My.Settings.Default.i7k2600}={My.Settings.Default.i7k2600DataTableSortTimespan.TotalMilliseconds}")
        End Using

        Console.ReadLine()
    End Sub

    Public Function AComp(ByVal x As Foo, ByVal y As Foo) As Integer
        Return x.A.CompareTo(y.A)
    End Function

    Public Class FooComparer
        Implements IComparer(Of Foo)
        Public Function Compare(ByVal x As Foo, ByVal y As Foo) As Integer Implements System.Collections.Generic.IComparer(Of Foo).Compare
            Return x.A.CompareTo(x.B)
        End Function
    End Class

    Public Class Foo

        Public Sub New()
            Dim r As New Random()
            Me._A = r.Next(0, 100)
            Me._B = r.Next(100, 1000).ToString()
        End Sub
        Public Property A() As Integer
        Public Property B() As String

    End Class

End Module
