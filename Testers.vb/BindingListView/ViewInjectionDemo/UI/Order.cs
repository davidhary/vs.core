namespace Equin.ApplicationFramework.Demos.ViewInjectionDemo
{
    internal class Order
    {
        public Order() => Details = string.Empty;

        public int ID { get; set; }

        public string Details { get; set; }

    }
}
