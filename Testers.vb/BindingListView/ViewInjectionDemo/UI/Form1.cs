namespace Equin.ApplicationFramework.Demos.ViewInjectionDemo
{
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;

    using isr.Core.WinFormsViews;

    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            // Create view of customers list
            var view = new BindingListView<Customer>( GetCustomers() );
            // Data bind to the view
            customerBindingSource.DataSource = view;

            // Change the orders binding source to use the auto provided view
            // instead of the normal list.
            ordersBindingSource.DataMember = "OrdersView";

        }

        private BindingList<Customer> GetCustomers()
        {
            var customers = new BindingList<Customer>
            {
                new Customer("David"),
                new Customer("Andrew"),
                new Customer("Bob"),
                new Customer("Chris")
            };

            return customers;
        }

        private void Button1_Click( object sender, EventArgs e ) => _ = MessageBox.Show( ordersBindingSource.Current.GetType().Name );

    }
}
