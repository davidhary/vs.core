namespace Equin.ApplicationFramework.Demos.ViewInjectionDemo
{
    using System.Collections.Generic;

    internal class Customer
    {
        public Customer()
            : this( string.Empty )
        {
        }

        public Customer( string name )
        {
            Name = name;
            Orders = new List<Order>();
        }

        public string Name { get; set; }

        public List<Order> Orders { get; }

    }
}
