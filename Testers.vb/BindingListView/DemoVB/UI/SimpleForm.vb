Imports isr.Core.WinFormsViews

Public Class SimpleForm

    Private _ItemsView As BindingListView(Of FeedItem)

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me.LoadFeed()
    End Sub

    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Me._ItemsView IsNot Nothing Then Me._ItemsView.Dispose() : Me._ItemsView = Nothing
        End If
        If disposing AndAlso Me.components IsNot Nothing Then
            Me.components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    Private _Feed As Feed

    Private Sub LoadFeed()

        ' Get the BBC news RSS feed
        Me._Feed = New Feed("http://newsrss.bbc.co.uk/rss/newsonline_uk_edition/front_page/rss.xml")
        Me._Feed.Update()

        ' Create a view of the items
        Me._ItemsView = New BindingListView(Of FeedItem)(Me._Feed.Items)

        ' Make the grid display this view
        Me.itemsGrid.DataSource = Me._ItemsView
    End Sub

    Private Sub FilterTextBox_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles filterTextBox.TextChanged
        Me._ItemsView.ApplyFilter(AddressOf Me.TitleFilter)
    End Sub

    Private Function TitleFilter(ByVal item As FeedItem) As Boolean
        Return item.Title.ToLower().Contains(Me.filterTextBox.Text.ToLower())
    End Function

    ''' <summary>
    ''' Handles the DataError event of the _dataGridView control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="DataGridViewDataErrorEventArgs"/> instance containing the event data.</param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ItemsGrid_DataError(sender As Object, e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles itemsGrid.DataError
        Try
            ' prevent error reporting when adding a new row or editing a cell
            Dim grid As DataGridView = TryCast(sender, DataGridView)
            If grid IsNot Nothing Then
                If grid.CurrentRow IsNot Nothing AndAlso grid.CurrentRow.IsNewRow Then Return
                If grid.IsCurrentCellInEditMode Then Return
                If grid.IsCurrentRowDirty Then Return
                Me.EnunciateErrorDataGrid(grid, "Exception occurred displaying data")
            End If
        Catch
        End Try

    End Sub

    ''' <summary>
    ''' Enunciates the data grid error.
    ''' </summary>
    ''' <param name="value">The value.</param>
    Private Sub EnunciateErrorDataGrid(sender As Control, ByVal value As String)
        If sender IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(value) Then
            Me._ErrorProvider.SetIconPadding(sender, -15)
            Me._ErrorProvider.SetError(sender, value)
        End If
    End Sub

End Class
