Imports isr.Core.WinFormsViews

Public Class AggregateForm
    Private _ItemsView As AggregateBindingListView(Of FeedItem)

    Public Sub New()

        Me.InitializeComponent()

        Me._ItemsView = New AggregateBindingListView(Of FeedItem)()

        Me.LoadFeeds()

        Me.itemsGrid.DataSource = Me._ItemsView
    End Sub

    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Me._ItemsView IsNot Nothing Then Me._ItemsView.Dispose() : Me._ItemsView = Nothing
        End If
        If disposing AndAlso Me.components IsNot Nothing Then
            Me.components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    Private Sub LoadFeeds()

        Dim urls As String() = {
            "http://newsrss.bbc.co.uk/rss/newsonline_uk_edition/front_page/rss.xml",
            "http://channel9.msdn.com/rss.aspx",
            "https://devblogs.microsoft.com/visualstudio/feed/"
        }
        For Each url As String In urls

            Dim feed As New Feed(url)
            feed.Update()

            ' Add to the list box so we can check/uncheck the feed
            Me.feedsListBox.Items.Add(feed)
        Next
    End Sub

    Private Sub FeedsListBox_ItemCheck(ByVal sender As Object, ByVal e As ItemCheckEventArgs) Handles feedsListBox.ItemCheck
        ' Get the checked/unchecked feed
        Dim feed As Feed = DirectCast(Me.feedsListBox.Items(e.Index), Feed)

        If e.NewValue = CheckState.Checked Then
            ' Add the checked feed's items to the aggregate list
            Me._ItemsView.SourceLists.Add(feed.Items)
        Else
            ' Remove the checked feed's items from the aggregate list
            Me._ItemsView.SourceLists.Remove(feed.Items)
        End If
    End Sub

    Private Sub FilterTextBox_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles filterTextBox.TextChanged
        Me._ItemsView.ApplyFilter(AddressOf Me.TitleFilter)
    End Sub

    Private Function TitleFilter(ByVal item As FeedItem) As Boolean
        Return item.Title.ToLower().Contains(Me.filterTextBox.Text.ToLower())
    End Function

End Class
