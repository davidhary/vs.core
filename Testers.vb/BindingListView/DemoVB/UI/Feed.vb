Imports System.ComponentModel
Imports System.Xml

Public Class Feed

    Private ReadOnly _Url As String
    Private _Title As String

    Public Sub New(ByVal url As String)
        Me._Url = url
        Me.Items = New BindingList(Of FeedItem)
    End Sub

    Public Sub Update()
        Dim doc As New XmlDocument()
        doc.Load(Me._Url)
        Me._Title = doc.SelectSingleNode("/rss/channel/title").InnerText
        For Each node As XmlNode In doc.SelectNodes("//item")
            Dim item As New FeedItem With {
                .Title = node("title").InnerText,
                .Description = node("description").InnerText,
                .PubDate = DateTime.Parse(node("pubDate").InnerText)
            }
            Me.Items.Add(item)
        Next
    End Sub

    Public ReadOnly Property Items() As BindingList(Of FeedItem)

    Public Overrides Function ToString() As String
        Return If(Me._Title, Me._Url)
    End Function
End Class
