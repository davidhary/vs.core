﻿Partial Public Class ViewModelBenchmark

    Private Shared _ModelNotifyNoCallbacks As PropertyChangeModel
    ''' <summary> The Model receiving the property change events. </summary>
    Private Shared ReadOnly Property ModelNotifyNoCallbacks As PropertyChangeModel
        Get
            If ViewModelBenchmark._ModelNotifyNoCallbacks Is Nothing Then
                ViewModelBenchmark._ModelNotifyNoCallbacks = New PropertyChangeModel
                ViewModelBenchmark._ModelNotifyNoCallbacks.Reset(New PropertyNotifyWrapper, 0)
            End If
            Return ViewModelBenchmark._ModelNotifyNoCallbacks
        End Get
    End Property

    ''' <summary> Asynchronously notifies (Posts) Property Change benchmark </summary>
    ''' <remarks> Benchmark times in micro seconds:<list type="bullet">
    ''' I7-2600K 3.4GHz<item>
    ''' 1 callback: </item><item>  
    ''' 1 callback: </item><item>
    ''' </item> </list> </remarks>
    <Benchmark>
    Public Shared Sub ModelNotifyNoCallbacksPost()
        ViewModelBenchmark.ModelNotifyNoCallbacks.Clear()
        Do Until ViewModelBenchmark.ModelNotifyNoCallbacks.PostChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ModelNotifyNoCallbacks.PostChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ModelNotifyNoCallbacks.PropertyChangeHandledCount
        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ModelNotifyNoCallbacks.PropertyChangeCount
    End Sub

    ''' <summary> Synchronously notifies (Sends) Property Change benchmark </summary>
    <Benchmark>
    Public Shared Sub ModelNotifyNoCallbacksSend()
        ViewModelBenchmark.ModelNotifyNoCallbacks.Clear()
        Do Until ViewModelBenchmark.ModelNotifyNoCallbacks.SendChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ModelNotifyNoCallbacks.SendChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ModelNotifyNoCallbacks.PropertyChangeHandledCount
        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ModelNotifyNoCallbacks.PropertyChangeCount
    End Sub

    ''' <summary> Raise (send) property change benchmark </summary>
    <Benchmark>
    Public Shared Sub ModelNotifyNoCallbacksRaise()
        ViewModelBenchmark.ModelNotifyNoCallbacks.Clear()

        Do Until ViewModelBenchmark.ModelNotifyNoCallbacks.RaiseChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ModelNotifyNoCallbacks.RaiseChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ModelNotifyNoCallbacks.PropertyChangeHandledCount
        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ModelNotifyNoCallbacks.PropertyChangeCount
    End Sub

    ''' <summary> Dynamically invokes property change benchmark </summary>
    <Benchmark>
    Public Shared Sub ModelNotifyNoCallbacksPostDynamicInvoke()
        ViewModelBenchmark.ModelNotifyNoCallbacks.Clear()
        Do Until ViewModelBenchmark.ModelNotifyNoCallbacks.PostDynamicInvokeChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ModelNotifyNoCallbacks.PostDynamicInvokeChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ModelNotifyNoCallbacks.PropertyChangeHandledCount
        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ModelNotifyNoCallbacks.PropertyChangeCount
    End Sub

End Class


