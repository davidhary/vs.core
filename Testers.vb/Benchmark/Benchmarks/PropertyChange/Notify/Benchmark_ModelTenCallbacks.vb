﻿Partial Public Class ViewModelBenchmark

    Private Shared _ModelNotifyTenCallbacks As PropertyChangeModel
    ''' <summary> The Model receiving the property change events. </summary>
    Private Shared ReadOnly Property ModelNotifyTenCallbacks As PropertyChangeModel
        Get
            If ViewModelBenchmark._ModelNotifyTenCallbacks Is Nothing Then
                ViewModelBenchmark._ModelNotifyTenCallbacks = New PropertyChangeModel
                ViewModelBenchmark._ModelNotifyTenCallbacks.Reset(New PropertyNotifyWrapper, 10)
            End If
            Return ViewModelBenchmark._ModelNotifyTenCallbacks
        End Get
    End Property

    ''' <summary> Asynchronously notifies (Posts) Property Change benchmark </summary>
    ''' <remarks> Benchmark times in micro seconds:<list type="bullet">
    ''' I7-2600K 3.4GHz<item>
    ''' 1 callback: </item><item>  
    ''' 1 callback: </item><item>
    ''' </item> </list> </remarks>
    <Benchmark>
    Public Shared Sub ModelNotifyTenCallbacksPost()
        ViewModelBenchmark.ModelNotifyTenCallbacks.Clear()
        Do Until ViewModelBenchmark.ModelNotifyTenCallbacks.PostChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ModelNotifyTenCallbacks.PostChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ModelNotifyTenCallbacks.PropertyChangeHandledCount
        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ModelNotifyTenCallbacks.PropertyChangeCount
    End Sub

    ''' <summary> Synchronously notifies (Sends) Property Change benchmark </summary>
    <Benchmark>
    Public Shared Sub ModelNotifyTenCallbacksSend()
        ViewModelBenchmark.ModelNotifyTenCallbacks.Clear()
        Do Until ViewModelBenchmark.ModelNotifyTenCallbacks.SendChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ModelNotifyTenCallbacks.SendChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ModelNotifyTenCallbacks.PropertyChangeHandledCount
        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ModelNotifyTenCallbacks.PropertyChangeCount
    End Sub

    ''' <summary> Raise (send) property change benchmark </summary>
    <Benchmark>
    Public Shared Sub ModelNotifyTenCallbacksRaise()
        ViewModelBenchmark.ModelNotifyTenCallbacks.Clear()

        Do Until ViewModelBenchmark.ModelNotifyTenCallbacks.RaiseChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ModelNotifyTenCallbacks.RaiseChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ModelNotifyTenCallbacks.PropertyChangeHandledCount
        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ModelNotifyTenCallbacks.PropertyChangeCount
    End Sub

    ''' <summary> Dynamically invokes property change benchmark </summary>
    <Benchmark>
    Public Shared Sub ModelNotifyTenCallbacksPostDynamicInvoke()
        ViewModelBenchmark.ModelNotifyTenCallbacks.Clear()
        Do Until ViewModelBenchmark.ModelNotifyTenCallbacks.PostDynamicInvokeChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ModelNotifyTenCallbacks.PostDynamicInvokeChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ModelNotifyTenCallbacks.PropertyChangeHandledCount
        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ModelNotifyTenCallbacks.PropertyChangeCount
    End Sub

End Class


