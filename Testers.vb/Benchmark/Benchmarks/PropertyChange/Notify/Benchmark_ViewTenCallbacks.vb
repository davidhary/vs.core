﻿Partial Public Class ViewModelBenchmark

    Private Shared _ViewNotifyTenCallbacks As PropertyChangeView
    ''' <summary> The view receiving the property change events. </summary>
    Private Shared ReadOnly Property ViewNotifyTenCallbacks As PropertyChangeView
        Get
            If ViewModelBenchmark._ViewNotifyTenCallbacks Is Nothing Then
                ViewModelBenchmark._ViewNotifyTenCallbacks = New PropertyChangeView
                ViewModelBenchmark._ViewNotifyTenCallbacks.Reset(New PropertyNotifyWrapper, 10)
            End If
            Return ViewModelBenchmark._ViewNotifyTenCallbacks
        End Get
    End Property

    ''' <summary> Asynchronously notifies (Posts) Property Change benchmark </summary>
    ''' <remarks> Benchmark times in micro seconds:<list type="bullet">
    ''' I7-2600K 3.4GHz<item>
    ''' 1 callback: 0.74, 0.78  </item><item>  
    ''' 1 callback: 0.81, 0.81, 0.75, 0.75 </item><item>
    ''' 10 1 us callbacks: 4.6, 6.6</item><item>
    ''' 10 1 us callbacks: 7.9, ?, 6.6, ?, 24, ?
    ''' </item> </list> </remarks>
    <Benchmark>
    Public Shared Sub ViewNotifyTenCallbacksPost()
        ViewModelBenchmark.ViewNotifyTenCallbacks.Clear()
        Do Until ViewModelBenchmark.ViewNotifyTenCallbacks.PostChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ViewNotifyTenCallbacks.PostChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ViewNotifyTenCallbacks.PropertyChangeHandledCount

        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ViewNotifyTenCallbacks.PropertyChangeCount
    End Sub

    ''' <summary> Synchronously notifies (Sends) Property Change benchmark </summary>
    <Benchmark>
    Public Shared Sub ViewNotifyTenCallbacksSend()
        ViewModelBenchmark.ViewNotifyTenCallbacks.Clear()
        Do Until ViewModelBenchmark.ViewNotifyTenCallbacks.SendChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ViewNotifyTenCallbacks.SendChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ViewNotifyTenCallbacks.PropertyChangeHandledCount
        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ViewNotifyTenCallbacks.PropertyChangeCount
    End Sub

    ''' <summary> Raise (send) property change benchmark </summary>
    <Benchmark>
    Public Shared Sub ViewNotifyTenCallbacksRaise()
        ViewModelBenchmark.ViewNotifyTenCallbacks.Clear()
        Do Until ViewModelBenchmark.ViewNotifyTenCallbacks.RaiseChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ViewNotifyTenCallbacks.RaiseChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ViewNotifyTenCallbacks.PropertyChangeHandledCount
        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ViewNotifyTenCallbacks.PropertyChangeCount
    End Sub

    ''' <summary> Dynamically invokes property change benchmark </summary>
    <Benchmark>
    Public Shared Sub ViewNotifyTenCallbacksPostDynamicInvoke()
        ViewModelBenchmark.ViewNotifyTenCallbacks.Clear()
        Do Until ViewModelBenchmark.ViewNotifyTenCallbacks.PostDynamicInvokeChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ViewNotifyTenCallbacks.PostDynamicInvokeChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ViewNotifyTenCallbacks.PropertyChangeHandledCount
        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ViewNotifyTenCallbacks.PropertyChangeCount
    End Sub

End Class
