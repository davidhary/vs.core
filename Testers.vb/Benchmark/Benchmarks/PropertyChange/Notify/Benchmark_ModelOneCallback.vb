﻿Partial Public Class ViewModelBenchmark

    Private Shared _ModelNotifyOneCallback As PropertyChangeModel
    ''' <summary> The Model receiving the property change events. </summary>
    Private Shared ReadOnly Property ModelNotifyOneCallback As PropertyChangeModel
        Get
            If ViewModelBenchmark._ModelNotifyOneCallback Is Nothing Then
                ViewModelBenchmark._ModelNotifyOneCallback = New PropertyChangeModel
                ViewModelBenchmark._ModelNotifyOneCallback.Reset(New PropertyNotifyWrapper, 1)
            End If
            Return ViewModelBenchmark._ModelNotifyOneCallback
        End Get
    End Property

    ''' <summary> Asynchronously notifies (Posts) Property Change benchmark </summary>
    ''' <remarks> Benchmark times in micro seconds:<list type="bullet">
    ''' I7-2600K 3.4GHz<item>
    ''' 1 callback: </item><item>  
    ''' 1 callback: </item><item>
    ''' </item> </list> </remarks>
    <Benchmark>
    Public Shared Sub ModelNotifyOneCallbackPost()
        ViewModelBenchmark.ModelNotifyOneCallback.Clear()
        Do Until ViewModelBenchmark.ModelNotifyOneCallback.PostChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ModelNotifyOneCallback.PostChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ModelNotifyOneCallback.PropertyChangeHandledCount
        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ModelNotifyOneCallback.PropertyChangeCount
    End Sub

    ''' <summary> Synchronously notifies (Sends) Property Change benchmark </summary>
    <Benchmark>
    Public Shared Sub ModelNotifyOneCallbackSend()
        ViewModelBenchmark.ModelNotifyOneCallback.Clear()
        Do Until ViewModelBenchmark.ModelNotifyOneCallback.SendChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ModelNotifyOneCallback.SendChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ModelNotifyOneCallback.PropertyChangeHandledCount
        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ModelNotifyOneCallback.PropertyChangeCount
    End Sub

    ''' <summary> Raise (send) property change benchmark </summary>
    <Benchmark>
    Public Shared Sub ModelNotifyOneCallbackRaise()
        ViewModelBenchmark.ModelNotifyOneCallback.Clear()

        Do Until ViewModelBenchmark.ModelNotifyOneCallback.RaiseChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ModelNotifyOneCallback.RaiseChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ModelNotifyOneCallback.PropertyChangeHandledCount
        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ModelNotifyOneCallback.PropertyChangeCount
    End Sub

    ''' <summary> Dynamically invokes property change benchmark </summary>
    <Benchmark>
    Public Shared Sub ModelNotifyOneCallbackPostDynamicInvoke()
        ViewModelBenchmark.ModelNotifyOneCallback.Clear()
        Do Until ViewModelBenchmark.ModelNotifyOneCallback.PostDynamicInvokeChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ModelNotifyOneCallback.PostDynamicInvokeChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ModelNotifyOneCallback.PropertyChangeHandledCount
        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ModelNotifyOneCallback.PropertyChangeCount
    End Sub

End Class



