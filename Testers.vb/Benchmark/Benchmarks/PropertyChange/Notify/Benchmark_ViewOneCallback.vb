﻿Partial Public Class ViewModelBenchmark

    Private Shared _ViewNotifyOneCallback As PropertyChangeView
    ''' <summary> The view receiving the property change events. </summary>
    Private Shared ReadOnly Property ViewNotifyOneCallback As PropertyChangeView
        Get
            If ViewModelBenchmark._ViewNotifyOneCallback Is Nothing Then
                ViewModelBenchmark._ViewNotifyOneCallback = New PropertyChangeView
                ViewModelBenchmark._ViewNotifyOneCallback.Reset(New PropertyNotifyWrapper, 1)
            End If
            Return ViewModelBenchmark._ViewNotifyOneCallback
        End Get
    End Property

    ''' <summary> Asynchronously notifies (Posts) Property Change benchmark </summary>
    ''' <remarks> Benchmark times in micro seconds:<list type="bullet">
    ''' I7-2600K 3.4GHz<item>
    ''' 1 callback: 0.74, 0.78  </item><item>  
    ''' 1 callback: 0.81, 0.81, 0.75, 0.75 </item><item>
    ''' 10 1 us callbacks: 4.6, 6.6</item><item>
    ''' 10 1 us callbacks: 7.9, ?, 6.6, ?, 24, ?
    ''' </item> </list> </remarks>
    <Benchmark>
    Public Shared Sub ViewNotifyOneCallbackPost()
        ViewModelBenchmark.ViewNotifyOneCallback.Clear()
        Do Until ViewModelBenchmark.ViewNotifyOneCallback.PostChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ViewNotifyOneCallback.PostChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ViewNotifyOneCallback.PropertyChangeHandledCount

        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ViewNotifyOneCallback.PropertyChangeCount
    End Sub

    ''' <summary> Synchronously notifies (Sends) Property Change benchmark </summary>
    <Benchmark>
    Public Shared Sub ViewNotifyOneCallbackSend()
        ViewModelBenchmark.ViewNotifyOneCallback.Clear()
        Do Until ViewModelBenchmark.ViewNotifyOneCallback.SendChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ViewNotifyOneCallback.SendChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ViewNotifyOneCallback.PropertyChangeHandledCount
        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ViewNotifyOneCallback.PropertyChangeCount
    End Sub

    ''' <summary> Raise (send) property change benchmark </summary>
    <Benchmark>
    Public Shared Sub ViewNotifyOneCallbackRaise()
        ViewModelBenchmark.ViewNotifyOneCallback.Clear()
        Do Until ViewModelBenchmark.ViewNotifyOneCallback.RaiseChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ViewNotifyOneCallback.RaiseChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ViewNotifyOneCallback.PropertyChangeHandledCount
        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ViewNotifyOneCallback.PropertyChangeCount
    End Sub

    ''' <summary> Dynamically invokes property change benchmark </summary>
    <Benchmark>
    Public Shared Sub ViewNotifyOneCallbackPostDynamicInvoke()
        ViewModelBenchmark.ViewNotifyOneCallback.Clear()
        Do Until ViewModelBenchmark.ViewNotifyOneCallback.PostDynamicInvokeChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ViewNotifyOneCallback.PostDynamicInvokeChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ViewNotifyOneCallback.PropertyChangeHandledCount
        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ViewNotifyOneCallback.PropertyChangeCount
    End Sub

End Class
