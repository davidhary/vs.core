﻿Partial Public Class ViewModelBenchmark

    Private Shared _ViewNotifyNoCallbacks As PropertyChangeView
    ''' <summary> The view receiving the property change events. </summary>
    Private Shared ReadOnly Property ViewNotifyNoCallbacks As PropertyChangeView
        Get
            If ViewModelBenchmark._ViewNotifyNoCallbacks Is Nothing Then
                ViewModelBenchmark._ViewNotifyNoCallbacks = New PropertyChangeView
                ViewModelBenchmark._ViewNotifyNoCallbacks.Reset(New PropertyNotifyWrapper, 0)
            End If
            Return ViewModelBenchmark._ViewNotifyNoCallbacks
        End Get
    End Property

    ''' <summary> Asynchronously notifies (Posts) Property Change benchmark </summary>
    ''' <remarks> Benchmark times in micro seconds:<list type="bullet">
    ''' I7-2600K 3.4GHz<item>
    ''' 1 callback: 0.74, 0.78  </item><item>  
    ''' 1 callback: 0.81, 0.81, 0.75, 0.75 </item><item>
    ''' 10 1 us callbacks: 4.6, 6.6</item><item>
    ''' 10 1 us callbacks: 7.9, ?, 6.6, ?, 24, ?
    ''' </item> </list> </remarks>
    <Benchmark>
    Public Shared Sub ViewNotifyNoCallbacksPost()
        ViewModelBenchmark.ViewNotifyNoCallbacks.Clear()
        Do Until ViewModelBenchmark.ViewNotifyNoCallbacks.PostChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ViewNotifyNoCallbacks.PostChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ViewNotifyNoCallbacks.PropertyChangeHandledCount

        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ViewNotifyNoCallbacks.PropertyChangeCount
    End Sub

    ''' <summary> Synchronously notifies (Sends) Property Change benchmark </summary>
    <Benchmark>
    Public Shared Sub ViewNotifyNoCallbacksSend()
        ViewModelBenchmark.ViewNotifyNoCallbacks.Clear()
        Do Until ViewModelBenchmark.ViewNotifyNoCallbacks.SendChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ViewNotifyNoCallbacks.SendChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ViewNotifyNoCallbacks.PropertyChangeHandledCount
        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ViewNotifyNoCallbacks.PropertyChangeCount
    End Sub

    ''' <summary> Raise (send) property change benchmark </summary>
    <Benchmark>
    Public Shared Sub ViewNotifyNoCallbacksRaise()
        ViewModelBenchmark.ViewNotifyNoCallbacks.Clear()
        Do Until ViewModelBenchmark.ViewNotifyNoCallbacks.RaiseChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ViewNotifyNoCallbacks.RaiseChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ViewNotifyNoCallbacks.PropertyChangeHandledCount
        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ViewNotifyNoCallbacks.PropertyChangeCount
    End Sub

    ''' <summary> Dynamically invokes property change benchmark </summary>
    <Benchmark>
    Public Shared Sub ViewNotifyNoCallbacksPostDynamicInvoke()
        ViewModelBenchmark.ViewNotifyNoCallbacks.Clear()
        Do Until ViewModelBenchmark.ViewNotifyNoCallbacks.PostDynamicInvokeChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ViewNotifyNoCallbacks.PostDynamicInvokeChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ViewNotifyNoCallbacks.PropertyChangeHandledCount
        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ViewNotifyNoCallbacks.PropertyChangeCount
    End Sub

End Class
