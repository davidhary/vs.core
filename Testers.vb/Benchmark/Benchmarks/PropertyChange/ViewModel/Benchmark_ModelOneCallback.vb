﻿Partial Public Class ViewModelBenchmark

    Private Shared _ModelOneCallback As PropertyChangeModel
    ''' <summary> The Model receiving the property change events. </summary>
    Private Shared ReadOnly Property ModelOneCallback As PropertyChangeModel
        Get
            If ViewModelBenchmark._ModelOneCallback Is Nothing Then
                ViewModelBenchmark._ModelOneCallback = New PropertyChangeModel
                ViewModelBenchmark._ModelOneCallback.Reset(New ViewModelWrapper, 1)
            End If
            Return ViewModelBenchmark._ModelOneCallback
        End Get
    End Property

    ''' <summary> Asynchronously notifies (Posts) Property Change benchmark </summary>
    ''' <remarks> Benchmark times in micro seconds:<list type="bullet">
    ''' I7-2600K 3.4GHz<item>
    ''' 1 callback: </item><item>  
    ''' 1 callback: </item><item>
    ''' </item> </list> </remarks>
    <Benchmark>
    Public Shared Sub ModelOneCallbackPost()
        ViewModelBenchmark.ModelOneCallback.Clear()
        Do Until ViewModelBenchmark.ModelOneCallback.PostChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ModelOneCallback.PostChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ModelOneCallback.PropertyChangeHandledCount
        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ModelOneCallback.PropertyChangeCount
    End Sub

    ''' <summary> Synchronously notifies (Sends) Property Change benchmark </summary>
    <Benchmark>
    Public Shared Sub ModelOneCallbackSend()
        ViewModelBenchmark.ModelOneCallback.Clear()
        Do Until ViewModelBenchmark.ModelOneCallback.SendChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ModelOneCallback.SendChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ModelOneCallback.PropertyChangeHandledCount
        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ModelOneCallback.PropertyChangeCount
    End Sub

    ''' <summary> Raise (send) property change benchmark </summary>
    <Benchmark>
    Public Shared Sub ModelOneCallbackRaise()
        ViewModelBenchmark.ModelOneCallback.Clear()

        Do Until ViewModelBenchmark.ModelOneCallback.RaiseChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ModelOneCallback.RaiseChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ModelOneCallback.PropertyChangeHandledCount
        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ModelOneCallback.PropertyChangeCount
    End Sub

    ''' <summary> Dynamically invokes property change benchmark </summary>
    <Benchmark>
    Public Shared Sub ModelOneCallbackPostDynamicInvoke()
        ViewModelBenchmark.ModelOneCallback.Clear()
        Do Until ViewModelBenchmark.ModelOneCallback.PostDynamicInvokeChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ModelOneCallback.PostDynamicInvokeChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ModelOneCallback.PropertyChangeHandledCount
        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ModelOneCallback.PropertyChangeCount
    End Sub

End Class



