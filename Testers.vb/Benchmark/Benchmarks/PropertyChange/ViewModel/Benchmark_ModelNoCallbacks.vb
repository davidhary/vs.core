﻿Partial Public Class ViewModelBenchmark

    Private Shared _ModelNoCallbacks As PropertyChangeModel
    ''' <summary> The Model receiving the property change events. </summary>
    Private Shared ReadOnly Property ModelNoCallbacks As PropertyChangeModel
        Get
            If ViewModelBenchmark._ModelNoCallbacks Is Nothing Then
                ViewModelBenchmark._ModelNoCallbacks = New PropertyChangeModel
                ViewModelBenchmark._ModelNoCallbacks.Reset(New ViewModelWrapper, 0)
            End If
            Return ViewModelBenchmark._ModelNoCallbacks
        End Get
    End Property

    ''' <summary> Asynchronously notifies (Posts) Property Change benchmark </summary>
    ''' <remarks> Benchmark times in micro seconds:<list type="bullet">
    ''' I7-2600K 3.4GHz<item>
    ''' 1 callback: </item><item>  
    ''' 1 callback: </item><item>
    ''' </item> </list> </remarks>
    <Benchmark>
    Public Shared Sub ModelNoCallbacksPost()
        ViewModelBenchmark.ModelNoCallbacks.Clear()
        Do Until ViewModelBenchmark.ModelNoCallbacks.PostChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ModelNoCallbacks.PostChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ModelNoCallbacks.PropertyChangeHandledCount
        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ModelNoCallbacks.PropertyChangeCount
    End Sub

    ''' <summary> Synchronously notifies (Sends) Property Change benchmark </summary>
    <Benchmark>
    Public Shared Sub ModelNoCallbacksSend()
        ViewModelBenchmark.ModelNoCallbacks.Clear()
        Do Until ViewModelBenchmark.ModelNoCallbacks.SendChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ModelNoCallbacks.SendChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ModelNoCallbacks.PropertyChangeHandledCount
        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ModelNoCallbacks.PropertyChangeCount
    End Sub

    ''' <summary> Raise (send) property change benchmark </summary>
    <Benchmark>
    Public Shared Sub ModelNoCallbacksRaise()
        ViewModelBenchmark.ModelNoCallbacks.Clear()

        Do Until ViewModelBenchmark.ModelNoCallbacks.RaiseChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ModelNoCallbacks.RaiseChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ModelNoCallbacks.PropertyChangeHandledCount
        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ModelNoCallbacks.PropertyChangeCount
    End Sub

    ''' <summary> Dynamically invokes property change benchmark </summary>
    <Benchmark>
    Public Shared Sub ModelNoCallbacksPostDynamicInvoke()
        ViewModelBenchmark.ModelNoCallbacks.Clear()
        Do Until ViewModelBenchmark.ModelNoCallbacks.PostDynamicInvokeChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ModelNoCallbacks.PostDynamicInvokeChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ModelNoCallbacks.PropertyChangeHandledCount
        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ModelNoCallbacks.PropertyChangeCount
    End Sub

End Class


