﻿Partial Public Class ViewModelBenchmark

    Private Shared _ViewNoCallbacks As PropertyChangeView
    ''' <summary> The view receiving the property change events. </summary>
    Private Shared ReadOnly Property ViewNoCallbacks As PropertyChangeView
        Get
            If ViewModelBenchmark._ViewNoCallbacks Is Nothing Then
                ViewModelBenchmark._ViewNoCallbacks = New PropertyChangeView
                ViewModelBenchmark._ViewNoCallbacks.Reset(New ViewModelWrapper, 0)
            End If
            Return ViewModelBenchmark._ViewNoCallbacks
        End Get
    End Property

    ''' <summary> Asynchronously notifies (Posts) Property Change benchmark </summary>
    ''' <remarks> Benchmark times in micro seconds:<list type="bullet">
    ''' I7-2600K 3.4GHz<item>
    ''' 1 callback: 0.74, 0.78  </item><item>  
    ''' 1 callback: 0.81, 0.81, 0.75, 0.75 </item><item>
    ''' 10 1 us callbacks: 4.6, 6.6</item><item>
    ''' 10 1 us callbacks: 7.9, ?, 6.6, ?, 24, ?
    ''' </item> </list> </remarks>
    <Benchmark>
    Public Shared Sub ViewNoCallbacksPost()
        ViewModelBenchmark.ViewNoCallbacks.Clear()
        Do Until ViewModelBenchmark.ViewNoCallbacks.PostChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ViewNoCallbacks.PostChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ViewNoCallbacks.PropertyChangeHandledCount

        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ViewNoCallbacks.PropertyChangeCount
    End Sub

    ''' <summary> Synchronously notifies (Sends) Property Change benchmark </summary>
    <Benchmark>
    Public Shared Sub ViewNoCallbacksSend()
        ViewModelBenchmark.ViewNoCallbacks.Clear()
        Do Until ViewModelBenchmark.ViewNoCallbacks.SendChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ViewNoCallbacks.SendChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ViewNoCallbacks.PropertyChangeHandledCount
        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ViewNoCallbacks.PropertyChangeCount
    End Sub

    ''' <summary> Raise (send) property change benchmark </summary>
    <Benchmark>
    Public Shared Sub ViewNoCallbacksRaise()
        ViewModelBenchmark.ViewNoCallbacks.Clear()
        Do Until ViewModelBenchmark.ViewNoCallbacks.RaiseChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ViewNoCallbacks.RaiseChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ViewNoCallbacks.PropertyChangeHandledCount
        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ViewNoCallbacks.PropertyChangeCount
    End Sub

    ''' <summary> Dynamically invokes property change benchmark </summary>
    <Benchmark>
    Public Shared Sub ViewNoCallbacksPostDynamicInvoke()
        ViewModelBenchmark.ViewNoCallbacks.Clear()
        Do Until ViewModelBenchmark.ViewNoCallbacks.PostDynamicInvokeChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ViewNoCallbacks.PostDynamicInvokeChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ViewNoCallbacks.PropertyChangeHandledCount
        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ViewNoCallbacks.PropertyChangeCount
    End Sub

End Class
