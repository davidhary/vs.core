﻿Partial Public Class ViewModelBenchmark

    Private Shared _ViewOneCallback As PropertyChangeView
    ''' <summary> The view receiving the property change events. </summary>
    Private Shared ReadOnly Property ViewOneCallback As PropertyChangeView
        Get
            If ViewModelBenchmark._ViewOneCallback Is Nothing Then
                ViewModelBenchmark._ViewOneCallback = New PropertyChangeView
                ViewModelBenchmark._ViewOneCallback.Reset(New ViewModelWrapper, 1)
            End If
            Return ViewModelBenchmark._ViewOneCallback
        End Get
    End Property

    ''' <summary> Asynchronously notifies (Posts) Property Change benchmark </summary>
    ''' <remarks> Benchmark times in micro seconds:<list type="bullet">
    ''' I7-2600K 3.4GHz<item>
    ''' </item> <item>
    ''' </item> </list> </remarks>
    <Benchmark>
    Public Shared Sub ViewOneCallbackPost()
        ViewModelBenchmark.ViewOneCallback.Clear()
        Do Until ViewModelBenchmark.ViewOneCallback.PostChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ViewOneCallback.PostChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ViewOneCallback.PropertyChangeHandledCount

        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ViewOneCallback.PropertyChangeCount
    End Sub

    ''' <summary> Synchronously notifies (Sends) Property Change benchmark </summary>
    <Benchmark>
    Public Shared Sub ViewOneCallbackSend()
        ViewModelBenchmark.ViewOneCallback.Clear()
        Do Until ViewModelBenchmark.ViewOneCallback.SendChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ViewOneCallback.SendChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ViewOneCallback.PropertyChangeHandledCount
        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ViewOneCallback.PropertyChangeCount
    End Sub

    ''' <summary> Raise (send) property change benchmark </summary>
    <Benchmark>
    Public Shared Sub ViewOneCallbackRaise()
        ViewModelBenchmark.ViewOneCallback.Clear()
        Do Until ViewModelBenchmark.ViewOneCallback.RaiseChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ViewOneCallback.RaiseChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ViewOneCallback.PropertyChangeHandledCount
        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ViewOneCallback.PropertyChangeCount
    End Sub

    ''' <summary> Dynamically invokes property change benchmark </summary>
    <Benchmark>
    Public Shared Sub ViewOneCallbackPostDynamicInvoke()
        ViewModelBenchmark.ViewOneCallback.Clear()
        Do Until ViewModelBenchmark.ViewOneCallback.PostDynamicInvokeChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ViewOneCallback.PostDynamicInvokeChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ViewOneCallback.PropertyChangeHandledCount
        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ViewOneCallback.PropertyChangeCount
    End Sub

End Class
