﻿Partial Public Class ViewModelBenchmark

    Private Shared _ViewTenCallbacks As PropertyChangeView
    ''' <summary> The view receiving the property change events. </summary>
    Private Shared ReadOnly Property ViewTenCallbacks As PropertyChangeView
        Get
            If ViewModelBenchmark._ViewTenCallbacks Is Nothing Then
                ViewModelBenchmark._ViewTenCallbacks = New PropertyChangeView
                ViewModelBenchmark._ViewTenCallbacks.Reset(New ViewModelWrapper, 10)
            End If
            Return ViewModelBenchmark._ViewTenCallbacks
        End Get
    End Property

    ''' <summary> Asynchronously notifies (Posts) Property Change benchmark </summary>
    ''' <remarks> Benchmark times in micro seconds:<list type="bullet">
    ''' I7-2600K 3.4GHz<item>
    ''' 1 callback: 0.74, 0.78  </item><item>  
    ''' 1 callback: 0.81, 0.81, 0.75, 0.75 </item><item>
    ''' 10 1 us callbacks: 4.6, 6.6</item><item>
    ''' 10 1 us callbacks: 7.9, ?, 6.6, ?, 24, ?
    ''' </item> </list> </remarks>
    <Benchmark>
    Public Shared Sub ViewTenCallbacksPost()
        ViewModelBenchmark.ViewTenCallbacks.Clear()
        Do Until ViewModelBenchmark.ViewTenCallbacks.PostChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ViewTenCallbacks.PostChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ViewTenCallbacks.PropertyChangeHandledCount

        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ViewTenCallbacks.PropertyChangeCount
    End Sub

    ''' <summary> Synchronously notifies (Sends) Property Change benchmark </summary>
    <Benchmark>
    Public Shared Sub ViewTenCallbacksSend()
        ViewModelBenchmark.ViewTenCallbacks.Clear()
        Do Until ViewModelBenchmark.ViewTenCallbacks.SendChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ViewTenCallbacks.SendChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ViewTenCallbacks.PropertyChangeHandledCount
        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ViewTenCallbacks.PropertyChangeCount
    End Sub

    ''' <summary> Raise (send) property change benchmark </summary>
    <Benchmark>
    Public Shared Sub ViewTenCallbacksRaise()
        ViewModelBenchmark.ViewTenCallbacks.Clear()
        Do Until ViewModelBenchmark.ViewTenCallbacks.RaiseChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ViewTenCallbacks.RaiseChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ViewTenCallbacks.PropertyChangeHandledCount
        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ViewTenCallbacks.PropertyChangeCount
    End Sub

    ''' <summary> Dynamically invokes property change benchmark </summary>
    <Benchmark>
    Public Shared Sub ViewTenCallbacksPostDynamicInvoke()
        ViewModelBenchmark.ViewTenCallbacks.Clear()
        Do Until ViewModelBenchmark.ViewTenCallbacks.PostDynamicInvokeChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ViewTenCallbacks.PostDynamicInvokeChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ViewTenCallbacks.PropertyChangeHandledCount
        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ViewTenCallbacks.PropertyChangeCount
    End Sub

End Class
