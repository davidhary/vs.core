﻿Partial Public Class ViewModelBenchmark

    Private Shared _ModelTenCallbacks As PropertyChangeModel
    ''' <summary> The Model receiving the property change events. </summary>
    Private Shared ReadOnly Property ModelTenCallbacks As PropertyChangeModel
        Get
            If ViewModelBenchmark._ModelTenCallbacks Is Nothing Then
                ViewModelBenchmark._ModelTenCallbacks = New PropertyChangeModel
                ViewModelBenchmark._ModelTenCallbacks.Reset(New ViewModelWrapper, 10)
            End If
            Return ViewModelBenchmark._ModelTenCallbacks
        End Get
    End Property

    ''' <summary> Asynchronously notifies (Posts) Property Change benchmark </summary>
    <Benchmark>
    Public Shared Sub ModelTenCallbacksPost()
        ViewModelBenchmark.ModelTenCallbacks.Clear()
        Do Until ViewModelBenchmark.ModelTenCallbacks.PostChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ModelTenCallbacks.PostChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ModelTenCallbacks.PropertyChangeHandledCount
        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ModelTenCallbacks.PropertyChangeCount
    End Sub

    ''' <summary> Synchronously notifies (Sends) Property Change benchmark </summary>
    <Benchmark>
    Public Shared Sub ModelTenCallbacksSend()
        ViewModelBenchmark.ModelTenCallbacks.Clear()
        Do Until ViewModelBenchmark.ModelTenCallbacks.SendChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ModelTenCallbacks.SendChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ModelTenCallbacks.PropertyChangeHandledCount
        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ModelTenCallbacks.PropertyChangeCount
    End Sub

    ''' <summary> Raise (send) property change benchmark </summary>
    <Benchmark>
    Public Shared Sub ModelTenCallbacksRaise()
        ViewModelBenchmark.ModelTenCallbacks.Clear()

        Do Until ViewModelBenchmark.ModelTenCallbacks.RaiseChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ModelTenCallbacks.RaiseChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ModelTenCallbacks.PropertyChangeHandledCount
        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ModelTenCallbacks.PropertyChangeCount
    End Sub

    ''' <summary> Dynamically invokes property change benchmark </summary>
    <Benchmark>
    Public Shared Sub ModelTenCallbacksPostDynamicInvoke()
        ViewModelBenchmark.ModelTenCallbacks.Clear()
        Do Until ViewModelBenchmark.ModelTenCallbacks.PostDynamicInvokeChange >= ViewModelBenchmark.Iterations
            ViewModelBenchmark.ModelTenCallbacks.PostDynamicInvokeChange += 1
        Loop
        ViewModelBenchmark.PropertyChangeHandledCount = ViewModelBenchmark.ModelTenCallbacks.PropertyChangeHandledCount
        ViewModelBenchmark.PropertyChangeCount = ViewModelBenchmark.ModelTenCallbacks.PropertyChangeCount
    End Sub

End Class


