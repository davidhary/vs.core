Imports System.ComponentModel
Imports isr.Core.Services.StopwatchExtensions
#If False Then

Operating System Microsoft Windows NT 6.2.9200.0 Runtime version 4.0.30319.42000
CPU Intel(R) Core(TM) i7-2600K CPU @ 3.40GHz; current clock speed 3410MHz  +++ USE ++++
Iterations 10000    --------- Context  or Invoke-----------------    Context only
                     List(T) For  List(T) 4 Each   Col(T) 4 each   List(T) w/ For
Run M/V   CB  How     N       �S    N       �S     N      �S      N      �S
  1 Model  0  Post      0   1.03      0   1.05      0   0.97       0   1.30
  2 Model  0  Post      0   0.13      0   0.15      0   0.20       0   0.79
  1 Model  0  Send      0   0.58      0   0.42      0   1.56       0   2.66
  2 Model  0  Send      0   0.12      0   0.26      0   0.16       0   0.58
  1 Model  0  Raise     0   0.27      0   0.46      0   0.77       0   1.06
  2 Model  0  Raise     0   0.14      0   0.19      0   0.18       0   0.51
  1 Model  0  D.I.      0   0.58      0   0.32      0   0.34       0   0.51
  2 Model  0  D.I.      0   0.26      0   0.23      0   0.15       0   0.31
  1 Model  1  Post   9822   3.22   8799   4.51   9932   4.68       0   3.67
  2 Model  1  Post   9806   0.84   9640   0.61   9835   0.96    9728   1.04
  1 Model  1  Send   9834   0.99   9727   0.99   9765   1.24   10000   0.70
  2 Model  1  Send   9679   0.51   9922   0.96   9909   1.00   10000   0.61
  1 Model  1  Raise  9884   1.07   9784   1.08   9939   1.26    9905   2.30
  2 Model  1  Raise  9906   0.84   9785   1.07   9614   0.69    9433   1.02
  1 Model  1  D.I.   9746   1.14   9677   1.15   9594   1.20    6660   3.86
  2 Model  1  D.I.   9609   0.92   8287   0.59   8869   0.91    7761   2.66
  1 Model 10  Post  44238   8.23  26815  10.01  53241   9.60   17083  17.64
  2 Model 10  Post  38164   8.07  35472   7.82  33106  10.09   14372  19.36
  1 Model 10  Send  21921   7.46    896  15.55  31579   8.45  106498  25.45
  2 Model 10  Send  19943   8.77  38820   8.16  29247   9.13  135149  36.13
  1 Model 10  Raise 18673  15.03  39757   8.95  26243  10.06   17674  20.06
  2 Model 10  Raise 26015   7.65  42654   8.08  33745   9.97   31029  10.36
  1 Model 10  D.I.  22543  14.6   24516  15.27  32461  13.63   21506  18.51
  2 Model 10  D.I.  23658  14.76  20617  14.41  25584  15.17   21999  18.69
  1 View   0  Post      0   0.75      0   0.51      0   0.45       0   1.69
  2 View   0  Post      0   0.15      0   0.20      0   0.30       0   0.25
  1 View   0  Send      0   0.19      0   0.22      0   0.42       0   0.51
  2 View   0  Send      0   0.14      0   0.15      0   0.36       0   0.35
  1 View   0  Raise     0   0.21      0   0.24      0   0.34       0   0.41
  2 View   0  Raise     0   0.21      0   0.23      0   0.19       0   0.15
  1 View   0  D.I.      0   0.32      0   0.22      0   0.27       0   0.24
  2 View   0  D.I.      0   0.14      0   0.29      0   0.33       0   0.29
  1 View  10  Post      0  16.38      0  12.37      0  15.37       0  19.38
  2 View  10  Post      0  16.87      0  16.10      0  14.37       0  15.43
  1 View  10  Send      0  15.00      0  15.83      0  19.35   10000   4.61
  2 View  10  Send      0  17.30      0  18.03      0  14.95   10000   6.73
  1 View  10  Raise     0  13.49      0  12.76      0  14.39       0  16.89
  2 View  10  Raise     0  16.79      0  12.24      0  15.24       0  14.45
  1 View  10  D.I.      0  17.13      0  17.91      0  17.63       0  21.32
  2 View  10  D.I.      0  15.43      0  16.10      0  12.35       0  18.44
  1 View  10  Post      0  90.07      0  96.83      0  93.28       0  28.76
  2 View  10  Post      0  79.0       0  85.32      0  82.20       0   2.92
  1 View  10  Send      0  78.46      0  81.09      0  97.79  100000  92.21
  2 View  10  Send      0  84.02      0  76.03      0  79.91  100000  95.34
  1 View  10  Raise     0  78.19      0  84.31      0  93.20       0   9.65
  2 View  10  Raise     0  87.76      0  81.07      0  83.73       0   6.08
  1 View  19  D.I.      0  92.23      0  89.17      0  96.17       0   8.47
  2 View  10  D.I.      0  90.1       0  91.54      0  87.58       0   7.23

Times are in �s per iteration (not per callback). (de) stands for Do Events. 
                                   Model                                   View  
                       Model.de     Model      Notify          Model.de     Model        Notify   
  N     CBs Command   cb   �s      cb   �s      cb   �s        cb    �s    cb   �s       cb    �s
10000    0   Post       0  29.9      0  1.30     0   7.9        0  12.7      0   1.69       0   0.69
                        0  10.0      0  0.79     0   0.17       0  11.1      0   0.25       0   0.19
10000    0   Send       0  12.8      0  2.66     0   0.26       0  12.7      0   0.51       0   0.35
                        0   9.2      0  0.58     0   0.14       0  10.1      0   0.35       0   0.20
10000    0   Raise      0  12.5      0  1.06     0   0.25       0  11.4      0   0.41       0   0.19
                        0  14.3      0  0.51     0   0.12       0  10.7      0   0.15       0   0.13
10000    0   D.I.       0  11.7      0  0.51     0   0.28       0  11.8      0   0.24       0   0.21
                        0  10.5      0  0.31     0   0.15       0  15.2      0   0.29       0   0.31
10000    1   Post   10000  21.2      0  3.67   9163  4.7    10000  55.1      0  19.38       0   1.2
                    10000  18.0   9728  1.04   9843  1.0    10000  60.2      0  15.43    9180   0.95
10000    1   Send    9995  18.4  10000  0.70  10000  0.29   10000  55.3  10000   4.61   10000   0.34
                    10000  19.1  10000  0.61  10000  0.23   10000  56.1  10000   6.73   10000   0.25
10000    1   Raise   9999  16.5   9905  2.30  10000  0.17   10000  56.9      0  16.89   10000   0.23
                     9999  17.9   9433  1.02  10000  0.13   10000  53.7      0  14.45   10000   0.17
10000    1   D.I.    9996  21.3   6660  3.86  10000  1.1    10000  57.0      0  21.32    9042   1.2
                     9998  20.9   7761  2.66   8344  0.58   10000  53.8      0  18.44    6224   0.96
10000   10   Post   98760  33.2  17083 17.64   8048  0.88  100000 328        0  28.76       0  11.6 
                    99019  31.6  14372 19.36   1367  0.88  100000 317        0   2.92    8039   0.94
10000   10   Send   97510  28.5 106498 25.45 100000 13.7   100000 374   100000  92.21  100000  15.5
                    98725  24.8 135149 36.13 100000 16.1   100000 314   100000  95.34  100000  13.6
10000   10   Raise  98926  26.9  17674 20.06 100000 14.0   100000 340        0   9.65  100000  13.6  
                    98747  28.9  31029 10.36 100000 20.7   100000 317        0   6.08  100000  12.2
10000   10   D.I.   84809  29.0  21506 18.51  47522 35.9   100000 347        0   8.47   17488  16.6
                    74779  38.1  21999 18.69  20740  9.1   100000 370        0   7.23   17755  11.5
#End If

''' <summary> Benchmarks View Model Base property change handling. </summary>
''' <remarks>
''' See http://www.pobox.com/~skeet/csharp/benchmark.html for how to run this code. Benchmark
''' times in micro seconds per iteration (not per callback) of 0 to 10 callbacks for 10000 iterations to a model
''' (class) and View (control) using property notifications or context notifications using the
''' view model class ([de] stands for using Do Events for each iteration):
''' <list type="bullet">I7-2600K benchmarks WIndows 10 6.2.9200.0 Runtime version 4.0.30319.42000<item>
'''                                    Model                                   View  </item><item>  
'''                        Model.de     Model      Notify          Model.de     Model        Notify   </item><item>
'''   N     CBs Command   cb   �s      cb   �s      cb   �s        cb    �s    cb   �s       cb    �s</item><item>  
''' 10000    0   Post       0  29.9      0  1.30     0   7.9        0  12.7      0   1.69       0   0.69</item><item>
'''                         0  10.0      0  0.79     0   0.17       0  11.1      0   0.25       0   0.19</item><item>
''' 10000    0   Send       0  12.8      0  2.66     0   0.26       0  12.7      0   0.51       0   0.35</item><item>
'''                         0   9.2      0  0.58     0   0.14       0  10.1      0   0.35       0   0.20</item><item>
''' 10000    0   Raise      0  12.5      0  1.06     0   0.25       0  11.4      0   0.41       0   0.19</item><item>
'''                         0  14.3      0  0.51     0   0.12       0  10.7      0   0.15       0   0.13</item><item>
''' 10000    0   D.I.       0  11.7      0  0.51     0   0.28       0  11.8      0   0.24       0   0.21</item><item>
'''                         0  10.5      0  0.31     0   0.15       0  15.2      0   0.29       0   0.31</item><item>
''' 10000    1   Post   10000  21.2      0  3.67   9163  4.7    10000  55.1      0  19.38       0   1.2</item><item>
'''                     10000  18.0   9728  1.04   9843  1.0    10000  60.2      0  15.43    9180   0.95</item><item>
''' 10000    1   Send    9995  18.4  10000  0.70  10000  0.29   10000  55.3  10000   4.61   10000   0.34</item><item>
'''                     10000  19.1  10000  0.61  10000  0.23   10000  56.1  10000   6.73   10000   0.25</item><item>
''' 10000    1   Raise   9999  16.5   9905  2.30  10000  0.17   10000  56.9      0  16.89   10000   0.23</item><item>
'''                      9999  17.9   9433  1.02  10000  0.13   10000  53.7      0  14.45   10000   0.17</item><item>
''' 10000    1   D.I.    9996  21.3   6660  3.86  10000  1.1    10000  57.0      0  21.32    9042   1.2</item><item>
'''                      9998  20.9   7761  2.66   8344  0.58   10000  53.8      0  18.44    6224   0.96</item><item>
''' 10000   10   Post   98760  33.2  17083 17.64   8048  0.88  100000 328        0  28.76       0  11.6 </item><item>
'''                     99019  31.6  14372 19.36   1367  0.88  100000 317        0   2.92    8039   0.94</item><item>
''' 10000   10   Send   97510  28.5 106498 25.45 100000 13.7   100000 374   100000  92.21  100000  15.5</item><item>
'''                     98725  24.8 135149 36.13 100000 16.1   100000 314   100000  95.34  100000  13.6</item><item>
''' 10000   10   Raise  98926  26.9  17674 20.06 100000 14.0   100000 340        0   9.65  100000  13.6  </item><item>
'''                     98747  28.9  31029 10.36 100000 20.7   100000 317        0   6.08  100000  12.2</item><item>
''' 10000   10   D.I.   84809  29.0  21506 18.51  47522 35.9   100000 347        0   8.47   17488  16.6</item><item>
'''                     74779  38.1  21999 18.69  20740  9.1   100000 370        0   7.23   17755  11.5</item> </list>
''' Each of the 10 call backs had an internal 1 microsecond delay. Adding a single call to
''' <see cref="Windows.Forms.Application.DoEvents"/> increases the call implementations by 10
''' fold.<para>
''' Conclusion:</para><para>
''' View model using custom event with context dedicated to each event registration provides
''' invocation times that might be 2 to 5 times slower than the legacy property notifications.
''' </para><para>
''' For example, invocation times for dynamic invokes are comparable (1 callback) to x2 (10
''' callbacks with delay). Invocation times for posts are comparable (1 callback) and x2 to x5
''' times (10 call backs with 1 us delay).
''' </para><para>
''' Notices, however, that simple not using the synchronization contacts (raise event) is  10
''' times faster with a single event but comparable to send when handling 10 callbacks with
''' delay. </para><para>
''' <list type="bullet">Property Notify implementation:<item>
''' Notify function calls Post Dynamic Invoke </item><item>  
''' Async Notify function calls Synchronization Context Post </item><item>  
''' Sync Notify function calls Synchronization Context Send </item> </list>
''' <list type="bullet">View Model implementation:<item>
''' Notify function calls Synchronization Context Post  </item><item>  
''' Async Notify function calls Synchronization Context Post </item><item>  
''' Sync Notify function calls Synchronization Context Send </item><item>  
''' Custom event Raise Event calls Synchronization Context Post </item> </list>
''' <list type="bullet">Library implementation<item>
''' Both legacy and new implementations of synchronous invocations will use the
''' synchronization context send </item><item>
''' View model implementations will use synchronization context send method. </item><item>
''' </item> </list>
''' </para><para>
''' </para>
''' </remarks>
''' <remarks>
''' Copyright (c) 2008 Jon Skeet. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 1/16/2019 </para><para>
''' David, 10/4/2018 </para></remarks>
Public NotInheritable Class ViewModelBenchmark

#Region " CONSTRUCTION and CLEAUP "

    ''' <summary>
    ''' Constructor that prevents a default instance of this class from being created.
    ''' </summary>
    Private Sub New()
        MyBase.New
    End Sub

#End Region

#Region " CLASS AND TEST CONSTRUCTION AND CLEAUP "

    ''' <summary> The iterations. </summary>
    Public Shared Property Iterations As Integer = 10000 ' Give it a reasonable default

    Private Shared PropertyChangeCount As Integer

    Private Shared PropertyChangeHandledCount As Integer

    Private Shared ExpectedPropertyChangeHandledCount As Integer


    ''' <summary> Initializes this object. </summary>
    ''' <remarks>
    ''' The init method is run before each test class. This method reads the number iterations as the
    ''' first command line argument is exists. Otherwise, the default <seealso cref="iterations"/> is
    ''' used. This method must be Public And Static, and take an array Of strings As its only
    ''' parameter. The value passed In will never be null.
    ''' </remarks>
    ''' <param name="args"> The command line arguments. </param>
    Public Shared Sub Init(ByVal args() As String)
        If args?.Length > 0 Then
            ViewModelBenchmark.Iterations = Int32.Parse(args(0))
        End If
    End Sub

    ''' <summary> Resets this object. </summary>
    ''' <remarks>
    ''' If present, the <code>Reset</code> method is called before each test. Like benchmark
    ''' methods, it must be public, static And parameterless to be noticed by the framework. This can
    ''' be used to clear any information which might be left from the previous test, For instance. In
    ''' this example, we make sure that the <code>list</code> variable Is null before the start Of
    ''' the test.
    ''' </remarks>
    Public Shared Sub Reset()
        ViewModelBenchmark.PropertyChangeCount = 0
        ViewModelBenchmark.PropertyChangeHandledCount = 0
    End Sub

    ''' <summary> Checks this object. </summary>
    ''' <remarks>
    ''' If present, the <code>Check</code> method is called after each test is run. Again, it must be
    ''' public, static And parameterless. In practice, you can often Get away With resetting state at
    ''' the end Of this method rather than having a separate <code>Reset</code> method, but you may
    ''' prefer To use <code> Reset</code> for clarity. The aim Of the <code>Check</code> method Is
    ''' to verify that the results of the test are correct, and throw an exception
    ''' if they're not. If the check fails, the message of the exception is printed
    ''' out instead Of the time taken. In this example, we check that the size Of
    ''' the resultant list Is correct. Note that this check Is carried out <i>after</i>
    ''' the time has been calculated, so you can afford To put time-consuming checks
    ''' in here without affecting the results.
    ''' </remarks>
    ''' <exception cref="InvalidProgramException"> Thrown when an Invalid Program error condition
    '''                                            occurs. </exception>
    Public Shared Sub Check()
        ' a delay is required to allow enough post events to register.
        isr.Core.Services.My.MyLibrary.DoEventsDelay(500)
        If ViewModelBenchmark.PropertyChangeCount <> ViewModelBenchmark.Iterations Then
            Throw New InvalidOperationException($"View Model {ViewModelBenchmark.PropertyChangeCount} <> {ViewModelBenchmark.Iterations} iterations count")
        ElseIf ViewModelBenchmark.PropertyChangeHandledCount < ViewModelBenchmark.ExpectedPropertyChangeHandledCount Then
            Throw New InvalidOperationException($"View Model handled {ViewModelBenchmark.PropertyChangeHandledCount} events out of {ViewModelBenchmark.ExpectedPropertyChangeHandledCount} iterations")
        End If
        Console.WriteLine($"calls={ViewModelBenchmark.PropertyChangeCount} callbacks={ViewModelBenchmark.PropertyChangeHandledCount}")
    End Sub

#End Region

End Class

Public Interface IModelView
    Inherits INotifyPropertyChanged
    Property PostChange As Long
    Property SendChange As Long
    Property PostDynamicInvoke As Long
    Property RaiseChange As Long
End Interface

Public Class ViewModelWrapper
    Inherits isr.Core.Models.ViewModelBase
    Implements IModelView

    Private _PostChange As Long

    ''' <summary> Gets or sets the A-Sync notification count. </summary>
    ''' <value> The A-Sync notification count. </value>
    Public Property PostChange As Long Implements IModelView.PostChange
        Get
            Return Me._PostChange
        End Get
        Set(value As Long)
            If value <> Me.PostChange Then
                Me._PostChange = value
                Me.AsyncNotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _SendChange As Long

    ''' <summary> Gets or sets the sync notification count. </summary>
    ''' <value> The sync notification count. </value>
    Public Property SendChange As Long Implements IModelView.SendChange
        Get
            Return Me._SendChange
        End Get
        Set(value As Long)
            If value <> Me.SendChange Then
                Me._SendChange = value
                Me.SyncNotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _RaiseChange As Long

    ''' <summary> Gets or sets the raise (send) change count. </summary>
    ''' <value> The raise (send) change count. </value>
    Public Property RaiseChange As Long Implements IModelView.RaiseChange
        Get
            Return Me._RaiseChange
        End Get
        Set(value As Long)
            If value <> Me.RaiseChange Then
                Me._RaiseChange = value
                Me.RaisePropertyChanged()
            End If
        End Set
    End Property

    Private _PostDynamicInvokeChange As Long

    ''' <summary> Gets or sets the post dynamic invoke change. </summary>
    ''' <value> The post dynamic invoke change. </value>
    Public Property PostDynamicInvokeChange As Long Implements IModelView.PostDynamicInvoke
        Get
            Return Me._PostDynamicInvokeChange
        End Get
        Set(value As Long)
            If value <> Me.PostDynamicInvokeChange Then
                Me._PostDynamicInvokeChange = value
                Me.PostDynamicInvokePropertyChanged()
            End If
        End Set
    End Property

End Class

Public Class PropertyNotifyWrapper
    Inherits isr.Core.Relic.PropertyNotifyBase
    Implements IModelView

    Private _PostChange As Long

    ''' <summary> Gets or sets the notifications count. </summary>
    ''' <value> The   notifications count. </value>
    Public Property PostChange As Long Implements IModelView.PostChange
        Get
            Return Me._PostChange
        End Get
        Set(value As Long)
            If value <> Me.PostChange Then
                Me._PostChange = value
                Me.AsyncNotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _PostDynamicInvoke As Long

    ''' <summary> Gets or sets the async notifications (Post Dynamic Invoke) count. </summary>
    ''' <value> The async notifications count. </value>
    Public Property PostDynamicInvoke As Long Implements IModelView.PostDynamicInvoke
        Get
            Return Me._PostDynamicInvoke
        End Get
        Set(value As Long)
            If value <> Me.PostDynamicInvoke Then
                Me._PostDynamicInvoke = value
                Me.AsyncDynamicInvokePropertyChanged()
            End If
        End Set
    End Property

    Private _SendChange As Long

    ''' <summary> Gets or sets the sync notifications count. </summary>
    ''' <value> The sync notifications count. </value>
    Public Property SendChange As Long Implements IModelView.SendChange
        Get
            Return Me._SendChange
        End Get
        Set(value As Long)
            If value <> Me.SendChange Then
                Me._SendChange = value
                Me.SyncNotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _RaiseChange As Long

    ''' <summary> Gets or sets the raise (framework implementation) change count. </summary>
    ''' <value> The raise (post) change count. </value>
    Public Property RaiseChange As Long Implements IModelView.RaiseChange
        Get
            Return Me._RaiseChange
        End Get
        Set(value As Long)
            If value <> Me.RaiseChange Then
                Me._RaiseChange = value
                Me.RaisePropertyChanged()
            End If
        End Set
    End Property

End Class


