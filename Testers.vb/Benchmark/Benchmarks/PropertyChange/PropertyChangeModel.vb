﻿''' <summary> A data Model for consuming property change events. </summary>
''' <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 1/15/2019 </para></remarks>
Public Class PropertyChangeModel

    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> The model view raising the events, which is to be benchmarked. </summary>
    Public Property ModelView As IModelView

    Public Property PropertyChangeCount As Integer

    Public Property PropertyChangeHandledCount As Integer

    Private Sub HandlePropertyChange0(ByVal sender As Object, ByVal e As ComponentModel.PropertyChangedEventArgs)
        Me.PropertyChangeHandledCount += 1
    End Sub

    Private Sub HandlePropertyChange1(ByVal sender As Object, ByVal e As ComponentModel.PropertyChangedEventArgs)
        Threading.Thread.Sleep(TimeSpan.FromMicroseconds(1))
        Me.PropertyChangeHandledCount += 1
    End Sub
    Private Sub HandlePropertyChange2(ByVal sender As Object, ByVal e As ComponentModel.PropertyChangedEventArgs)
        Threading.Thread.Sleep(TimeSpan.FromMicroseconds(1))
        Me.PropertyChangeHandledCount += 1
    End Sub
    Private Sub HandlePropertyChange3(ByVal sender As Object, ByVal e As ComponentModel.PropertyChangedEventArgs)
        Threading.Thread.Sleep(TimeSpan.FromMicroseconds(1))
        Me.PropertyChangeHandledCount += 1
    End Sub
    Private Sub HandlePropertyChange4(ByVal sender As Object, ByVal e As ComponentModel.PropertyChangedEventArgs)
        Threading.Thread.Sleep(TimeSpan.FromMicroseconds(1))
        Me.PropertyChangeHandledCount += 1
    End Sub
    Private Sub HandlePropertyChange5(ByVal sender As Object, ByVal e As ComponentModel.PropertyChangedEventArgs)
        Threading.Thread.Sleep(TimeSpan.FromMicroseconds(1))
        Me.PropertyChangeHandledCount += 1
    End Sub
    Private Sub HandlePropertyChange6(ByVal sender As Object, ByVal e As ComponentModel.PropertyChangedEventArgs)
        Threading.Thread.Sleep(TimeSpan.FromMicroseconds(1))
        Me.PropertyChangeHandledCount += 1
    End Sub
    Private Sub HandlePropertyChange7(ByVal sender As Object, ByVal e As ComponentModel.PropertyChangedEventArgs)
        Threading.Thread.Sleep(TimeSpan.FromMicroseconds(1))
        Me.PropertyChangeHandledCount += 1
    End Sub
    Private Sub HandlePropertyChange8(ByVal sender As Object, ByVal e As ComponentModel.PropertyChangedEventArgs)
        Threading.Thread.Sleep(TimeSpan.FromMicroseconds(1))
        Me.PropertyChangeHandledCount += 1
    End Sub
    Private Sub HandlePropertyChange9(ByVal sender As Object, ByVal e As ComponentModel.PropertyChangedEventArgs)
        Threading.Thread.Sleep(TimeSpan.FromMicroseconds(1))
        Me.PropertyChangeHandledCount += 1
    End Sub

    Public Sub Reset(ByVal modelView As IModelView, ByVal callbackCount As Integer)
        Me.ModelView = modelView
        If callbackCount > 0 Then AddHandler Me.ModelView.PropertyChanged, AddressOf Me.HandlePropertyChange0
        callbackCount -= 1
        If callbackCount > 0 Then AddHandler Me.ModelView.PropertyChanged, AddressOf Me.HandlePropertyChange1
        callbackCount -= 1
        If callbackCount > 0 Then AddHandler Me.ModelView.PropertyChanged, AddressOf Me.HandlePropertyChange2
        callbackCount -= 1
        If callbackCount > 0 Then AddHandler Me.ModelView.PropertyChanged, AddressOf Me.HandlePropertyChange3
        callbackCount -= 1
        If callbackCount > 0 Then AddHandler Me.ModelView.PropertyChanged, AddressOf Me.HandlePropertyChange4
        callbackCount -= 1
        If callbackCount > 0 Then AddHandler Me.ModelView.PropertyChanged, AddressOf Me.HandlePropertyChange5
        callbackCount -= 1
        If callbackCount > 0 Then AddHandler Me.ModelView.PropertyChanged, AddressOf Me.HandlePropertyChange6
        callbackCount -= 1
        If callbackCount > 0 Then AddHandler Me.ModelView.PropertyChanged, AddressOf Me.HandlePropertyChange7
        callbackCount -= 1
        If callbackCount > 0 Then AddHandler Me.ModelView.PropertyChanged, AddressOf Me.HandlePropertyChange8
        callbackCount -= 1
        If callbackCount > 0 Then AddHandler Me.ModelView.PropertyChanged, AddressOf Me.HandlePropertyChange9
        callbackCount -= 1
        Me.Clear()
    End Sub

    Public Sub Clear()
        Me.PropertyChangeCount = 0
        Me.PropertyChangeHandledCount = 0
        Me._PostChange = 0
        Me._SendChange = 0
        Me._RaiseChange = 0
        Me._PostDynamicInvokeChange = 0
    End Sub

    Private _PostChange As Long

    ''' <summary> Gets or sets the A-Sync notification count. </summary>
    ''' <value> The A-Sync notification count. </value>
    Public Property PostChange As Long
        Get
            Return Me._PostChange
        End Get
        Set(value As Long)
            If value <> Me.PostChange Then
                Me._PostChange = value
                Me.ModelView.PostChange = value
                Me.PropertyChangeCount += 1
            End If
        End Set
    End Property

    Private _SendChange As Long

    ''' <summary> Gets or sets the sync notification count. </summary>
    ''' <value> The sync notification count. </value>
    Public Property SendChange As Long
        Get
            Return Me._SendChange
        End Get
        Set(value As Long)
            If value <> Me.SendChange Then
                Me._SendChange = value
                Me.ModelView.SendChange = value
                Me.PropertyChangeCount += 1
            End If
        End Set
    End Property

    Private _RaiseChange As Long

    ''' <summary> Gets or sets the raise (send) change count. </summary>
    ''' <value> The raise (send) change count. </value>
    Public Property RaiseChange As Long
        Get
            Return Me._RaiseChange
        End Get
        Set(value As Long)
            If value <> Me.RaiseChange Then
                Me._RaiseChange = value
                Me.ModelView.RaiseChange = value
                Me.PropertyChangeCount += 1
            End If
        End Set
    End Property

    Private _PostDynamicInvokeChange As Long

    ''' <summary> Gets or sets the post dynamic invoke change. </summary>
    ''' <value> The post dynamic invoke change. </value>
    Public Property PostDynamicInvokeChange As Long
        Get
            Return Me._PostDynamicInvokeChange
        End Get
        Set(value As Long)
            If value <> Me.PostDynamicInvokeChange Then
                Me._PostDynamicInvokeChange = value
                Me.ModelView.PostDynamicInvoke = value
                Me.PropertyChangeCount += 1
            End If
        End Set
    End Property

End Class



