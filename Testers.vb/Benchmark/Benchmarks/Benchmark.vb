Imports System.Reflection

''' <summary>
''' The attribute to use to mark methods as being
''' the targets of benchmarking.
''' </summary>
<AttributeUsage(AttributeTargets.Method)>
Public NotInheritable Class BenchmarkAttribute
    Inherits Attribute
End Class

''' <summary>
''' Very simple benchmarking framework. Looks for all types in the current assembly which have
''' public static parameterless methods marked with the Benchmark attribute. In addition, if
''' there are public static Init, Reset and Check methods with appropriate parameters (a string
''' array for Init, nothing for Reset or Check) these are called at appropriate times.
''' </summary>
''' <remarks>
''' Copyright (c) 2008 Jon Skeet. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-10-04, 1.0.0.0"> http://jonskeet.uk/csharp/benchmark.html. </para></remarks>
Public NotInheritable Class Benchmark

    ''' <summary>
    ''' Constructor that prevents a default instance of this class from being created.
    ''' </summary>
    Private Sub New()
        MyBase.New
    End Sub

    ''' <summary>
    ''' Number of times to run the methods in each type.
    ''' </summary>
    Private Shared _RunIterations As Integer = 1

    <CodeAnalysis.SuppressMessage("Performance", "CA1825:Avoid zero-length array allocations.", Justification:="<Pending>")>
    Public Shared Sub Main(ByVal args() As String)
        Benchmark.ParseCommandLine(args)
        ' We're only ever interested in public static methods. This variable
        ' just makes it easier to read the code...
        Dim publicStatic As BindingFlags = BindingFlags.Public Or BindingFlags.Static
        For Each type As Type In System.Reflection.Assembly.GetCallingAssembly().GetTypes()
            ' Find an Init method taking string[], if any
            Dim initMethod As MethodInfo = type.GetMethod("Init", publicStatic, Nothing, New Type() {GetType(String())}, Nothing)

            ' Find a parameterless Reset method, if any
            Dim onRunMethod As MethodInfo = type.GetMethod("OnRun", publicStatic, Nothing, New Type() {}, Nothing)

            ' Find a parameterless Reset method, if any
            Dim resetMethod As MethodInfo = type.GetMethod("Reset", publicStatic, Nothing, New Type() {}, Nothing)

            ' Find a parameterless Check method, if any
            Dim checkMethod As MethodInfo = type.GetMethod("Check", publicStatic, Nothing, New Type() {}, Nothing)

            ' gets the iteration count
            Dim iterationsProperty As PropertyInfo = type.GetProperty("Iterations", publicStatic, Nothing, GetType(Integer), New Type() {}, Nothing)

            ' Find all parameterless methods with the [Benchmark] attribute
            Dim benchmarkMethods As New ArrayList()
            For Each method As MethodInfo In type.GetMethods(publicStatic)
                Dim parameters() As ParameterInfo = method.GetParameters()
                If parameters IsNot Nothing AndAlso parameters.Length <> 0 Then
                    Continue For
                End If
                ' this misses the custom attribute if the method has multiple attributes.
                If method.GetCustomAttribute(Of BenchmarkAttribute) IsNot Nothing Then
                    benchmarkMethods.Add(method)
                ElseIf method.GetCustomAttributes(GetType(BenchmarkAttribute), True).Length <> 0 Then
                    benchmarkMethods.Add(method)
                End If
            Next method

            ' Ignore types with no appropriate methods to benchmark
            If benchmarkMethods.Count = 0 Then
                Continue For
            End If

            ' If we've got an Init method, call it once
            Try
                If initMethod IsNot Nothing Then
                    initMethod.Invoke(Nothing, New Object() {args})
                End If
            Catch e As TargetInvocationException
                Dim inner As Exception = e.InnerException
                Dim message As String = inner?.Message
                If message Is Nothing Then
                    message = "(No message)"
                End If
                Console.WriteLine("Init failed ({0})", message)
                Continue For ' Next type
            End Try

            Console.WriteLine($"Benchmarking {type.Name} {iterationsProperty.GetValue(type)}")

            For i As Integer = 0 To Benchmark._RunIterations - 1
                If Benchmark._RunIterations <> 1 Then
                    Console.WriteLine($"Run #{i + 1}")
                End If
                If onRunMethod IsNot Nothing Then
                    onRunMethod.Invoke(Nothing, Nothing)
                End If
                For Each method As MethodInfo In benchmarkMethods
                    Try
                        ' Reset (if appropriate)
                        If resetMethod IsNot Nothing Then
                            resetMethod.Invoke(Nothing, Nothing)
                        End If

                        ' Give the test as good a chance as possible
                        ' of avoiding garbage collection
                        GC.Collect()
                        GC.WaitForPendingFinalizers()
                        GC.Collect()

                        ' Now run the test itself

                        ' DH: Using stop watch 
                        Dim stopWatch As Stopwatch = Stopwatch.StartNew
                        method.Invoke(Nothing, Nothing)
                        stopWatch.Stop()
                        Dim elapsed As TimeSpan = stopWatch.Elapsed

                        ' Check the results (if appropriate)
                        ' Note that this doesn't affect the timing
                        If checkMethod IsNot Nothing Then
                            checkMethod.Invoke(Nothing, Nothing)
                        End If

                        ' If everything's worked, report the time taken, 
                        ' nicely lined up (assuming no very long method names!)
                        Console.WriteLine("  {0,-40} {1}", method.Name, elapsed)
                    Catch e As TargetInvocationException
                        Dim inner As Exception = e.InnerException
                        Dim message As String = inner?.Message
                        If message Is Nothing Then
                            message = "(No message)"
                        End If
                        Console.WriteLine("  {0}: Failed ({1})", method.Name, message)
                    End Try
                Next method
            Next i
        Next type
        Console.WriteLine("Benchmarking completed; press any key to exist")
        Console.ReadKey()
    End Sub

    ''' <summary>
    ''' Parses the command line, returning an array of strings which are the arguments the tasks
    ''' should receive. This array will definitely be non-null, even if null is passed in originally.
    ''' </summary>
    ''' <remarks> dh: Modified to be not destructive. </remarks>
    ''' <param name="args"> The arguments. </param>
    Private Shared Sub ParseCommandLine(ByVal args() As String)
        For Each s As String In args
            Select Case s
                Case "-runtwice"
                    _RunIterations = 2
                Case "-environment"
                    Benchmark.PrintEnvironment()
                Case "-endoptions"
                    Exit For
            End Select
        Next
    End Sub

    ''' <summary>
    ''' Prints out information about the operating environment.
    ''' </summary>
    Private Shared Sub PrintEnvironment()
        Console.WriteLine($"Operating System {Environment.OSVersion} Runtime version {Environment.Version}")
        Using env As New System.Management.ManagementObjectSearcher("Select * from Win32_Processor")
            For Each item As System.Management.ManagementObject In env.Get()
                Console.WriteLine($"CPU {item("Name")}; current clock speed {item("CurrentClockSpeed")}MHz")
            Next
        End Using
    End Sub

End Class
