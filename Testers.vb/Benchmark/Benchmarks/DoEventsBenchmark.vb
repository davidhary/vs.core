﻿Imports System.Windows.threading
Imports isr.Core.Services.DispatcherExtensions
Imports isr.Core.Services.StopwatchExtensions

Public NotInheritable Class DoEventsBenchmark

#Region " CONSTRUCTION and CLEAUP "

    ''' <summary>
    ''' Constructor that prevents a default instance of this class from being created.
    ''' </summary>
    Private Sub New()
        MyBase.New
    End Sub

#End Region

#Region " CLASS AND TEST CONSTRUCTION AND CLEAUP "

    ''' <summary> The iterations. </summary>
    Public Shared Property Iterations As Integer = 1000 ' Give it a reasonable default

    ''' <summary> Initializes each test class. </summary>
    ''' <remarks>
    ''' The init method is run before each test class. This method reads the number iterations as the
    ''' first command line argument is exists. Otherwise, the default <seealso cref="iterations"/> is
    ''' used. This method must be Public And Static, and take an array Of strings As its only
    ''' parameter. The value passed In will never be null.
    ''' </remarks>
    ''' <param name="args"> The command line arguments. </param>
    Public Shared Sub Init(ByVal args() As String)
        If args?.Length > 0 Then
            DoEventsBenchmark.Iterations = Int32.Parse(args(0))
        End If
    End Sub

    ''' <summary> Resets before each test is run. </summary>
    ''' <remarks>
    ''' If present, the <code>Reset</code> method is called before each test. Like benchmark
    ''' methods, it must be public, static And parameterless to be noticed by the framework. This can
    ''' be used to clear any information which might be left from the previous test, For instance. In
    ''' this example, we make sure that the <code>list</code> variable Is null before the start Of
    ''' the test.
    ''' </remarks>
    Public Shared Sub Reset()
    End Sub

    ''' <summary> Runs after each test is run. </summary>
    ''' <remarks>
    ''' If present, the <code>Check</code> method is called after each test is run. Again, it must be
    ''' public, static And parameterless. In practice, you can often Get away With resetting state at
    ''' the end Of this method rather than having a separate <code>Reset</code> method, but you may
    ''' prefer To use <code> Reset</code> for clarity. The aim Of the <code>Check</code> method Is
    ''' to verify that the results of the test are correct, and throw an exception
    ''' if they're not. If the check fails, the message of the exception is printed
    ''' out instead Of the time taken. In this example, we check that the size Of
    ''' the resultant list Is correct. Note that this check Is carried out <i>after</i>
    ''' the time has been calculated, so you can afford To put time-consuming checks
    ''' in here without affecting the results.
    ''' </remarks>
    ''' <exception cref="InvalidProgramException"> Thrown when an Invalid Program error condition
    '''                                            occurs. </exception>
    Public Shared Sub Check()
    End Sub

#End Region

#Region " DO EVENT FUNCTIONS "

    ''' <summary> Windows forms do events. </summary>
    ''' <remarks>
    ''' <list type="bullet">Do Events<item>
    ''' 1000 iterations of Application Do Events:   7.8,  8.0, 11,  8.4 us </item><item>
    ''' 1000 iterations of Dispatcher Do Events:   57.6, 92.5, 93, 61.6 us </item></list>
    ''' 
    ''' <list type="bullet">Stop watch wait with dispatcher do events:<item>
    ''' 1000 iterations of 1ms:   1.11,  1.11,   1.067</item><item>
    ''' 100 iterations of 10ms:  10.15, 10.16,  10.09 </item><item>
    ''' 10 iterations of 100ms: 100.1, 100.08, 100.05 </item></list>
    ''' 
    ''' <list type="bullet">Task delay:<item>
    ''' 1000 iterations of 1ms:  15.6ms </item><item>
    ''' 100 iterations of 10ms:  15.6, 15.7,   19.9ms </item><item>
    ''' 10 iterations of 100ms: 109.6, 108.9, 109.9ms </item></list>
    ''' 
    ''' <list type="bullet">Dispatcher do events task delay:<item>
    ''' 1000 iterations of 1ms:  15.62ms </item><item>
    ''' 100 iterations of 10ms:  15.63, 15.68, 20.1ms </item><item>
    ''' 10 iterations of 100ms: 109.7, 108.9, 109.9ms </item></list>
    '''
    ''' <list type="bullet">Dispatcher wait end time:<item>
    ''' 1000 iterations of 1ms:   1.69, 1.72,  10.0ms </item><item>
    ''' 100 iterations of 10ms:  10.68, 10.70, 18.0ms </item><item>
    ''' 10 iterations of 100ms: 100.5, 100.8, 105.7ms </item></list>
    '''
    ''' <list type="bullet">Wait end time with application do events:<item>
    ''' 1000 iterations of 1ms:   1.70, 1.73, 9.96ms </item><item>
    ''' 100 iterations of 10ms:  10.80, 10.77, 17.8ms </item><item>
    ''' 10 iterations of 100ms: 100.5, 100.5, 105.6ms </item></list>
    '''
    ''' <list type="bullet">Wait end time with dispatcher do events:<item>
    ''' 1000 iterations of 1ms:   1.71, 1.74,   9.96ms </item><item>
    ''' 100 iterations of 10ms:  10.70, 10.67, 17.9ms </item><item>
    ''' 10 iterations of 100ms: 100.5, 100.5, 104.9ms </item></list>
    '''
    ''' <list type="bullet">Wait elapsed time with dispatcher do events:<item>
    ''' 1000 iterations of 1ms:   1.10, 1.06ms </item><item>
    ''' 100 iterations of 10ms:  10.1, 10.07ms </item><item>
    ''' 10 iterations of 100ms: 100.05, 100.15ms </item></list>
    '''
    ''' <list type="bullet">Thread Sleep w/o do events:<item>
    ''' 1000 iterations of 1ms:   1.96, 1.92,  10.1ms </item><item>
    ''' 100 iterations of 10ms:  10.85, 10.89, 18.9ms </item><item>
    ''' 10 iterations of 100ms: 100.5, 100.8, 108.8ms </item></list>
    ''' 
    ''' </remarks>
    <Benchmark>
    Public Shared Sub WindowsFormsDoEvents()
        For i As Integer = 1 To Iterations
            Windows.Forms.Application.DoEvents()
        Next
    End Sub

    <Benchmark>
    Public Shared Sub DispatcherDoEvents()
        For i As Integer = 1 To Iterations
            isr.Core.Services.My.MyLibrary.DoEvents()
        Next
    End Sub

    Private Shared Sub StopwatchWaitBenchmark(ByVal milliseconds As Integer)
        Dim sw As Stopwatch = Stopwatch.StartNew
        For i As Integer = 1 To CInt(DoEventsBenchmark.Iterations / milliseconds)
            sw.Wait(TimeSpan.FromMilliseconds(milliseconds))
        Next
    End Sub

    <Benchmark>
    Public Shared Sub OneMsStopwatchWait()
        DoEventsBenchmark.StopwatchWaitBenchmark(1)
    End Sub


    <Benchmark>
    Public Shared Sub TenMsStopwatchWait()
        DoEventsBenchmark.StopwatchWaitBenchmark(10)
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Hunderd")>
    <Benchmark>
    Public Shared Sub HunderdMsStopwatchWait()
        DoEventsBenchmark.StopwatchWaitBenchmark(100)
    End Sub

    Private Shared Sub TaskDelayBenchmark(ByVal milliseconds As Integer)
        For i As Integer = 1 To CInt(DoEventsBenchmark.Iterations / milliseconds)
            Task.Delay(milliseconds).Wait()
        Next
    End Sub


    ''' <summary> One milliseconds task delay; removed too long. </summary>
    Public Shared Sub OneMsTaskDelay()
        DoEventsBenchmark.TaskDelayBenchmark(1)
    End Sub

    <Benchmark>
    Public Shared Sub TenMsTaskDelay()
        DoEventsBenchmark.TaskDelayBenchmark(10)
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Hunderd")>
    <Benchmark>
    Public Shared Sub HunderdMsTaskDelay()
        DoEventsBenchmark.TaskDelayBenchmark(100)
    End Sub

    Private Shared Sub DoEventsDelay(ByVal dispatcher As Dispatcher, ByVal delay As TimeSpan)
        isr.Core.Services.My.MyLibrary.DoEvents()
        Threading.Tasks.Task.Delay(delay).Wait()
        isr.Core.Services.My.MyLibrary.DoEvents()
    End Sub

    Private Shared Sub DispatcherDoEventsTaskDelayBenchmark(ByVal milliseconds As Integer)
        For i As Integer = 1 To CInt(DoEventsBenchmark.Iterations / milliseconds)
            DoEventsBenchmark.DoEventsDelay(Dispatcher.CurrentDispatcher, TimeSpan.FromMilliseconds(milliseconds))
        Next
    End Sub

    ''' <summary> One milliseconds dispatcher do events task delay; removed: too long. </summary>
    Public Shared Sub OneMsDispatcherDoEventsTaskDelay()
        DoEventsBenchmark.DispatcherDoEventsTaskDelayBenchmark(1)
    End Sub

    <Benchmark>
    Public Shared Sub TenMsDispatcherDoEventsTaskDelay()
        DoEventsBenchmark.DispatcherDoEventsTaskDelayBenchmark(10)
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Hunderd")>
    <Benchmark>
    Public Shared Sub OneHunderdMsDispatcherDoEventsTaskDelay()
        DoEventsBenchmark.DispatcherDoEventsTaskDelayBenchmark(100)
    End Sub

    ''' <summary> Waits for the specified <see cref="TimeSpan"/>. </summary>
    ''' <param name="delay">      The delay. </param>
    Private Shared Sub WaitEndTime(ByVal delay As TimeSpan)
        Dim sw As Stopwatch = Stopwatch.StartNew
        Do
            isr.Core.Services.My.MyLibrary.DoEvents()
        Loop Until sw.Elapsed > delay
    End Sub

    Private Shared Sub DispatcherWaitEndTimeBenchmark(ByVal milliseconds As Integer)
        For i As Integer = 1 To CInt(DoEventsBenchmark.Iterations / milliseconds)
            DoEventsBenchmark.WaitEndTime(TimeSpan.FromMilliseconds(milliseconds))
        Next
    End Sub

    <Benchmark>
    Public Shared Sub OneMsDispatcherWaitEndTime()
        DoEventsBenchmark.DispatcherWaitEndTimeBenchmark(1)
    End Sub

    <Benchmark>
    Public Shared Sub TenMsDispatcherWaitEndTime()
        DoEventsBenchmark.DispatcherWaitEndTimeBenchmark(10)
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Hunderd")>
    <Benchmark>
    Public Shared Sub HunderdMsDispatcherWaitEndTime()
        DoEventsBenchmark.DispatcherWaitEndTimeBenchmark(100)
    End Sub

    Private Shared Sub WaitEndTimeDoEvents(ByVal milliseconds As Integer)
        For i As Integer = 1 To CInt(DoEventsBenchmark.Iterations / milliseconds)
            Dim timeout As TimeSpan = TimeSpan.FromMilliseconds(milliseconds)
            Dim sw As Stopwatch = Stopwatch.StartNew
            Do
                Windows.Forms.Application.DoEvents()
            Loop Until sw.Elapsed > timeout
        Next
    End Sub

    <Benchmark>
    Public Shared Sub OneMsWaitEndTimeDoEvents()
        DoEventsBenchmark.WaitEndTimeDoEvents(1)
    End Sub

    <Benchmark>
    Public Shared Sub TenMsWaitEndTimeDoEvents()
        DoEventsBenchmark.WaitEndTimeDoEvents(10)
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Hunderd")>
    <Benchmark>
    Public Shared Sub HunderdMsWaitEndTimeDoEvents()
        DoEventsBenchmark.WaitEndTimeDoEvents(100)
    End Sub

    Private Shared Sub WaitEndTimeDispatcherDoEvents(ByVal milliseconds As Integer)
        For i As Integer = 1 To CInt(DoEventsBenchmark.Iterations / milliseconds)
            Dim timeout As TimeSpan = TimeSpan.FromMilliseconds(milliseconds)
            Dim sw As Stopwatch = Stopwatch.StartNew
            Do
                isr.Core.Services.My.MyLibrary.DoEvents()
            Loop Until sw.Elapsed > timeout
        Next
    End Sub

    <Benchmark>
    Public Shared Sub OneMsWaitEndTimeDispatcherDoEvents()
        DoEventsBenchmark.WaitEndTimeDispatcherDoEvents(1)
    End Sub

    <Benchmark>
    Public Shared Sub TenMsWaitEndTimeDispatcherDoEvents()
        DoEventsBenchmark.WaitEndTimeDispatcherDoEvents(10)
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Hunderd")>
    <Benchmark>
    Public Shared Sub HunderdMsWaitEndTimeDispatcherDoEvents()
        DoEventsBenchmark.WaitEndTimeDispatcherDoEvents(100)
    End Sub

    Private Shared Sub WaitElapsedDispatcherDoEvents(ByVal milliseconds As Integer)
        Dim ts As TimeSpan = TimeSpan.FromMilliseconds(milliseconds)
        For i As Integer = 1 To CInt(DoEventsBenchmark.Iterations / milliseconds)
            Dim sw As Stopwatch = Stopwatch.StartNew
            Dispatcher.CurrentDispatcher.Wait(sw, ts)
#If False Then
            Do
                Dispatcher.CurrentDispatcher.DoEvents()
            Loop Until sw.Elapsed >= ts
#End If
        Next
    End Sub

    <Benchmark>
    Public Shared Sub OneMsWaitElapsedDispatcherDoEvents()
        DoEventsBenchmark.WaitElapsedDispatcherDoEvents(1)
    End Sub

    <Benchmark>
    Public Shared Sub TenMsWaitElapsedDispatcherDoEvents()
        DoEventsBenchmark.WaitElapsedDispatcherDoEvents(10)
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Hunderd")>
    <Benchmark>
    Public Shared Sub HunderdMsWaitElapsedDispatcherDoEvents()
        DoEventsBenchmark.WaitElapsedDispatcherDoEvents(100)
    End Sub

    Private Shared Sub Sleep(ByVal milliseconds As Integer)
        For i As Integer = 1 To CInt(DoEventsBenchmark.Iterations / milliseconds)
            Threading.Thread.Sleep(milliseconds)
        Next
    End Sub

    <Benchmark>
    Public Shared Sub OneMillisecondSleep()
        DoEventsBenchmark.Sleep(1)
    End Sub


    <Benchmark>
    Public Shared Sub TenMillisecondSleep()
        DoEventsBenchmark.Sleep(10)
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Hunderd")>
    <Benchmark>
    Public Shared Sub HunderdMillisecondSleep()
        DoEventsBenchmark.Sleep(100)
    End Sub

#End Region

End Class
