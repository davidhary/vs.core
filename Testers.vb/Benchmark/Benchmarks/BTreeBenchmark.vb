Imports System.Linq
Imports isr.Core.Composites

Public NotInheritable Class BTreeBenchmark

#Region " CONSTRUCTION and CLEAUP "

    ''' <summary>
    ''' Constructor that prevents a default instance of this class from being created.
    ''' </summary>
    Private Sub New()
        MyBase.New
    End Sub

#End Region

#Region " CLASS AND TEST CONSTRUCTION AND CLEAUP "

    ''' <summary> The iterations. </summary>
    Public Shared Property Iterations As Integer = 2 ' Give it a reasonable default

    ''' <summary> Initializes each test class. </summary>
    ''' <remarks>
    ''' The init method is run before each test class. This method reads the number iterations as the
    ''' first command line argument is exists. Otherwise, the default <seealso cref="iterations"/> is
    ''' used. This method must be Public And Static, and take an array Of strings As its only
    ''' parameter. The value passed In will never be null.
    ''' </remarks>
    ''' <param name="args"> The command line arguments. </param>
    Public Shared Sub Init(ByVal args() As String)
        If args?.Length > 0 Then
            BTreeBenchmark.Iterations = Int32.Parse(args(0))
            Dim itOption As String = "-it:"
            For Each s As String In args
                If s.StartsWith(itOption, StringComparison.OrdinalIgnoreCase) Then
                    s = s.Substring(itOption.Length)
                    _It = Int32.Parse(s)
                End If
            Next
        End If
        Console.WriteLine($"Iterations: Outer {Iterations}; Inner {_It}")
    End Sub

    ''' <summary> Resets before each run. </summary>
    ''' <remarks>
    ''' If present, the <code>OnRun</code> method is called before each run. Like benchmark
    ''' methods, it must be public, static And parameterless to be noticed by the framework. This can
    ''' be used to clear any information which might be left from the previous run, For instance. In
    ''' this example, we make sure that the dictionaries are clear.
    ''' </remarks>
    Public Shared Sub OnRun()
        D.Clear()
        Sd.Clear()
        Sbtd.Clear()
        Satd.Clear()
        Sstd.Clear()
        BTreeBenchmark._RandomValues = BTreeBenchmark.FillRandom(_It)
    End Sub

    ''' <summary> Resets before each test is run. </summary>
    ''' <remarks>
    ''' If present, the <code>Reset</code> method is called before each test. Like benchmark
    ''' methods, it must be public, static And parameterless to be noticed by the framework. This can
    ''' be used to clear any information which might be left from the previous test, For instance. In
    ''' this example, we make sure that the <code>list</code> variable Is null before the start Of
    ''' the test.
    ''' </remarks>
    Public Shared Sub Reset()
    End Sub

    ''' <summary> Runs after each test is run. </summary>
    ''' <remarks>
    ''' If present, the <code>Check</code> method is called after each test is run. Again, it must be
    ''' public, static And parameterless. In practice, you can often Get away With resetting state at
    ''' the end Of this method rather than having a separate <code>Reset</code> method, but you may
    ''' prefer To use <code> Reset</code> for clarity. The aim Of the <code>Check</code> method Is
    ''' to verify that the results of the test are correct, and throw an exception
    ''' if they're not. If the check fails, the message of the exception is printed
    ''' out instead Of the time taken. In this example, we check that the size Of
    ''' the resultant list Is correct. Note that this check Is carried out <i>after</i>
    ''' the time has been calculated, so you can afford To put time-consuming checks
    ''' in here without affecting the results.
    ''' </remarks>
    ''' <exception cref="InvalidProgramException"> Thrown when an Invalid Program error condition
    '''                                            occurs. </exception>
    Public Shared Sub Check()
    End Sub

#End Region

#Region " SHARED FUNCTIONS "

    Private Const _TestRemoves As Boolean = False
    Private Const _ReportInternalStopwatch As Boolean = False

    ''' <summary> Removes the items target described by d. </summary>
    ''' <param name="d"> An IDictionary(OfInteger,String) to process. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Shared Sub RemoveItemsTarget(ByVal d As IDictionary(Of Integer, String))
        If Not _TestRemoves Then
            d.Clear()
            Return
        End If
        Try
            Do While 0 <> d.Count
                Dim first As Integer = -1
                ' grab the first element
                For Each item As KeyValuePair(Of Integer, String) In d
                    first = item.Key
                    Exit For
                Next item
                d.Remove(first)
            Loop
        Catch ex As Exception
            Console.WriteLine($"    {GetObjectName(d)} threw: {ex}")
            Console.ReadKey()
        End Try
    End Sub

    ''' <summary> Removes the items target described by d. </summary>
    ''' <param name="d"> An IDictionary(OfInteger,String) to process. </param>
    ''' <param name="s"> A Stopwatch to process. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Shared Sub RemoveItemsTarget(ByVal d As IDictionary(Of Integer, String), ByVal s As Stopwatch)
        If Not _TestRemoves Then
            d.Clear()
            Return
        End If
        Try
            s.Reset()
            Do While 0 <> d.Count
                Dim first As Integer = -1

                ' grab the first element
                For Each item As KeyValuePair(Of Integer, String) In d
                    first = item.Key
                    Exit For
                Next item
                s.Start()
                d.Remove(first)
                s.Stop()
            Loop
            Console.WriteLine($"    {GetObjectName(d)} Remove Items Target:         {s.Elapsed.TotalMilliseconds} ms")
        Catch ex As Exception
            Console.WriteLine($"    {GetObjectName(d)} threw: {ex}")
            Console.ReadKey()
        End Try
    End Sub

    ''' <summary> Searches for the first target sequential. </summary>
    ''' <param name="d">          An IDictionary(OfInteger,String) to process. </param>
    ''' <param name="s">          A Stopwatch to process. </param>
    ''' <param name="iterations"> The iterations. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private Shared Sub SearchTargetSequential(ByVal d As IDictionary(Of Integer, String), ByVal s As Stopwatch, ByVal iterations As Integer)
        Try
            s.Reset()
            For i As Integer = 0 To iterations - 1
                Dim v As String = String.Empty
                s.Start()
                d.TryGetValue(i, v)
                s.Stop()
            Next i
            Console.WriteLine($"    {GetObjectName(d)} Search target sequential:    {s.Elapsed.TotalMilliseconds} ms")
        Catch ex As Exception
            Console.WriteLine($"    {GetObjectName(d)} threw: {ex}")
            Console.ReadKey()
        End Try
    End Sub

    ''' <summary> Searches for the first target sequential. </summary>
    ''' <param name="d">          An IDictionary(OfInteger,String) to process. </param>
    ''' <param name="iterations"> The iterations. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Shared Sub SearchTargetSequential(ByVal d As IDictionary(Of Integer, String), ByVal iterations As Integer)
        Try
            For i As Integer = 0 To iterations - 1
                Dim v As String = String.Empty
                d.TryGetValue(i, v)
            Next i
        Catch ex As Exception
            Console.WriteLine($"    {GetObjectName(d)} threw: {ex}")
            Console.ReadKey()
        End Try
    End Sub

    ''' <summary> Adds to the target sequential. </summary>
    ''' <param name="d">          An IDictionary(OfInteger,String) to process. </param>
    ''' <param name="s">          A Stopwatch to process. </param>
    ''' <param name="iterations"> The iterations. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private Shared Sub AddToTargetSequential(ByVal d As IDictionary(Of Integer, String), ByVal s As Stopwatch, ByVal iterations As Integer)
        Dim i As Integer
        Try
            s.Reset()
            For i = 0 To iterations - 1
                s.Start()
                d.Add(i, i.ToString())
                s.Stop()
            Next i
            Console.WriteLine($"    {GetObjectName(d)} Add to target sequential:    {s.Elapsed.TotalMilliseconds} ms")
        Catch ex As Exception
            Console.WriteLine($"    {GetObjectName(d)} threw: {ex}")
            Console.ReadKey()
        End Try
    End Sub

    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Shared Sub AddToTargetSequential(ByVal d As IDictionary(Of Integer, String), ByVal iterations As Integer)
        Dim i As Integer
        Try
            For i = 0 To iterations - 1
                d.Add(i, i.ToString())
            Next i
        Catch ex As Exception
            Console.WriteLine($"    {GetObjectName(d)} threw: {ex}")
            Console.ReadKey()
        End Try
    End Sub

    Private Shared Function GetObjectName(Of TKey, TValue)(ByVal d As IDictionary(Of TKey, TValue)) As String
        Dim s As String = CType(d, Object).GetType().Name
        Dim i As Integer = s.IndexOf("`"c)
        Return If(1 > i, s, s.Substring(0, i))
    End Function

    ''' <summary> The random values. </summary>
    Private Shared _RandomValues As Integer()

    Private Shared Function FillRandom(ByVal iterations As Integer) As Integer()
        Dim seen As New HashSet(Of Integer)(iterations)
        Dim sw As Stopwatch = Stopwatch.StartNew
        Do While seen.Count < iterations
            Dim rnd As Integer = (New Random(CInt(sw.ElapsedTicks Mod Integer.MaxValue) Xor seen.Count)).Next()
            seen.Add(rnd)
        Loop
        Return seen.ToArray()
    End Function

    ''' <summary> Searches for the first target random access. </summary>
    ''' <param name="d">      An IDictionary(OfInteger,String) to process. </param>
    ''' <param name="s">      A Stopwatch to process. </param>
    ''' <param name="values"> The random values. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private Shared Sub SearchTargetRandomAccess(ByVal d As IDictionary(Of Integer, String), ByVal s As Stopwatch, ByVal values() As Integer)
        Try
            s.Reset()
            For i As Integer = 0 To values.Length - 1
                Dim v As String = String.Empty
                Dim j As Integer = values(i)
                s.Start()
                d.TryGetValue(j, v)
                s.Stop()
            Next i
            Console.WriteLine($"    {GetObjectName(d)} Search Target Random Access: {s.Elapsed.TotalMilliseconds} ms")
        Catch ex As Exception
            Console.WriteLine($"    {GetObjectName(d)} threw: {ex}")
            Console.ReadKey()
        End Try
    End Sub

    ''' <summary> Searches for the first target random access. </summary>
    ''' <param name="d">      An IDictionary(OfInteger,String) to process. </param>
    ''' <param name="values"> The random values. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Shared Sub SearchTargetRandomAccess(ByVal d As IDictionary(Of Integer, String), ByVal values() As Integer)
        Try
            For i As Integer = 0 To values.Length - 1
                Dim v As String = String.Empty
                Dim j As Integer = values(i)
                d.TryGetValue(j, v)
            Next i
        Catch ex As Exception
            Console.WriteLine($"    {GetObjectName(d)} threw: {ex}")
            Console.ReadKey()
        End Try
    End Sub

    ''' <summary> Adds to the target random access. </summary>
    ''' <param name="d">      An IDictionary(OfInteger,String) to process. </param>
    ''' <param name="s">      A Stopwatch to process. </param>
    ''' <param name="values"> The random values. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private Shared Sub AddToTargetRandomAccess(ByVal d As IDictionary(Of Integer, String), ByVal s As Stopwatch, ByVal values() As Integer)
        Try
            s.Reset()
            For i As Integer = 0 To values.Length - 1
                Dim j As Integer = values(i)
                s.Start()
                d.Add(j, j.ToString())
                s.Stop()
            Next i
            Console.WriteLine($"    {GetObjectName(d)} Add To Target Random Access: {s.Elapsed.TotalMilliseconds} ms")
        Catch ex As Exception
            Console.WriteLine($"    {GetObjectName(d)} threw: {ex}")
            Console.ReadKey()
        End Try
    End Sub

    ''' <summary> Adds to the target random access. </summary>
    ''' <param name="d">      An IDictionary(OfInteger,String) to process. </param>
    ''' <param name="values"> The random values. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Shared Sub AddToTargetRandomAccess(ByVal d As IDictionary(Of Integer, String), ByVal values() As Integer)
        Try
            For i As Integer = 0 To values.Length - 1
                Dim j As Integer = values(i)
                d.Add(j, j.ToString())
            Next i
        Catch ex As Exception
            Console.WriteLine($"    {GetObjectName(d)} threw: {ex}")
            Console.ReadKey()
        End Try
    End Sub

#End Region

#Region " BENCHMARK FUNCTIONS "

    Private Shared _It As Integer = 1000
    Private Shared ReadOnly D As New Dictionary(Of Integer, String)()
    Private Shared ReadOnly Sd As New SortedDictionary(Of Integer, String)()
    Private Shared ReadOnly Sbtd As New SortedBTreeDictionary(Of Integer, String)(5)
    Private Shared ReadOnly Satd As New SortedAvlTreeDictionary(Of Integer, String)()
    Private Shared ReadOnly Sstd As New SortedSplayTreeDictionary(Of Integer, String)()
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0052:Remove unread private members", Justification:="<Pending>")>
    Private Shared ReadOnly Sw As New Stopwatch()

    ''' <summary> Simple Dictionary Sequential Access. </summary>
    ''' <remarks>
    ''' <list type="bullet">Sequential Access<item>
    ''' :   </item><item>
    ''' :   </item></list>
    ''' 
    ''' <list type="bullet">random access<item>
    ''' :   1.11,  1.11,   1.067</item><item>
    ''' :  10.15, 10.16,  10.09 </item><item>
    ''' : 100.1, 100.08, 100.05 </item></list>
    ''' 
    ''' </remarks>
    <Benchmark>
    Public Shared Sub DictionaryAddSequentialAccess()
        If BTreeBenchmark._ReportInternalStopwatch Then
            BTreeBenchmark.AddToTargetSequential(D, Sw, _It)
        Else
            BTreeBenchmark.AddToTargetSequential(D, _It)
        End If
    End Sub

    <Benchmark>
    Public Shared Sub SortedDictionaryAddSequentialAccess()
        If BTreeBenchmark._ReportInternalStopwatch Then
            BTreeBenchmark.AddToTargetSequential(Sd, Sw, _It)
        Else
            BTreeBenchmark.AddToTargetSequential(Sd, _It)
        End If
    End Sub

    <Benchmark>
    Public Shared Sub SortedBTreeAddSequentialAccess()
        If BTreeBenchmark._ReportInternalStopwatch Then
            BTreeBenchmark.AddToTargetSequential(Sbtd, Sw, _It)
        Else
            BTreeBenchmark.AddToTargetSequential(Sbtd, _It)
        End If
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Avl")>
    <Benchmark>
    Public Shared Sub SortedAvlTreeAddSequentialAccess()
        If BTreeBenchmark._ReportInternalStopwatch Then
            BTreeBenchmark.AddToTargetSequential(Satd, Sw, _It)
        Else
            BTreeBenchmark.AddToTargetSequential(Satd, _It)
        End If
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Spla")>
    <Benchmark>
    Public Shared Sub SortedSplaTreeAddSequentialAccess()
        If BTreeBenchmark._ReportInternalStopwatch Then
            BTreeBenchmark.AddToTargetSequential(Sstd, Sw, _It)
        Else
            BTreeBenchmark.AddToTargetSequential(Sstd, _It)
        End If
    End Sub


    <Benchmark>
    Public Shared Sub DictionarySearchSequentialAccess()
        If BTreeBenchmark._ReportInternalStopwatch Then
            BTreeBenchmark.SearchTargetSequential(D, Sw, _It)
        Else
            BTreeBenchmark.SearchTargetSequential(D, _It)
        End If
    End Sub

    <Benchmark>
    Public Shared Sub SortedDictionarySearchSequentialAccess()
        If BTreeBenchmark._ReportInternalStopwatch Then
            BTreeBenchmark.SearchTargetSequential(Sd, Sw, _It)
        Else
            BTreeBenchmark.SearchTargetSequential(Sd, _It)
        End If
    End Sub

    <Benchmark>
    Public Shared Sub SortedBTreeSearchSequentialAccess()
        If BTreeBenchmark._ReportInternalStopwatch Then
            BTreeBenchmark.SearchTargetSequential(Sbtd, Sw, _It)
        Else
            BTreeBenchmark.SearchTargetSequential(Sbtd, _It)
        End If
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Avl")>
    <Benchmark>
    Public Shared Sub SortedAvlTreeSearchSequentialAccess()
        If BTreeBenchmark._ReportInternalStopwatch Then
            BTreeBenchmark.SearchTargetSequential(Satd, Sw, _It)
        Else
            BTreeBenchmark.SearchTargetSequential(Satd, _It)
        End If
    End Sub

    <Benchmark>
    Public Shared Sub SortedSplayTreeSearchSequentialAccess()
        If BTreeBenchmark._ReportInternalStopwatch Then
            BTreeBenchmark.SearchTargetSequential(Sstd, Sw, _It)
        Else
            BTreeBenchmark.SearchTargetSequential(Sstd, _It)
        End If
    End Sub

    <Benchmark>
    Public Shared Sub DictionaryRemoveItems()
        If BTreeBenchmark._ReportInternalStopwatch Then
            BTreeBenchmark.RemoveItemsTarget(D, Sw)
        Else
            BTreeBenchmark.RemoveItemsTarget(D)
        End If
    End Sub

    <Benchmark>
    Public Shared Sub SortedDictionaryRemoveItems()
        If BTreeBenchmark._ReportInternalStopwatch Then
            BTreeBenchmark.RemoveItemsTarget(Sd, Sw)
        Else
            BTreeBenchmark.RemoveItemsTarget(Sd)
        End If
    End Sub

    <Benchmark>
    Public Shared Sub SortedBTreeRemoveItems()
        If BTreeBenchmark._ReportInternalStopwatch Then
            BTreeBenchmark.RemoveItemsTarget(Sbtd, Sw)
        Else
            BTreeBenchmark.RemoveItemsTarget(Sbtd)
        End If
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Avl")>
    <Benchmark>
    Public Shared Sub SortedAvlTreeRemoveItems()
        If BTreeBenchmark._ReportInternalStopwatch Then
            BTreeBenchmark.RemoveItemsTarget(Satd, Sw)
        Else
            BTreeBenchmark.RemoveItemsTarget(Satd)
        End If
    End Sub

    <Benchmark>
    Public Shared Sub SortedSplayTreeRemoveItems()
        If BTreeBenchmark._ReportInternalStopwatch Then
            BTreeBenchmark.RemoveItemsTarget(Sstd, Sw)
        Else
            BTreeBenchmark.RemoveItemsTarget(Sstd)
        End If
    End Sub

    <Benchmark>
    Public Shared Sub DictionaryAddRandomAccess()
        If BTreeBenchmark._ReportInternalStopwatch Then
            BTreeBenchmark.AddToTargetRandomAccess(D, Sw, _RandomValues)
        Else
            BTreeBenchmark.AddToTargetRandomAccess(D, _RandomValues)
        End If
    End Sub

    <Benchmark>
    Public Shared Sub SortedDictionaryAddRandomAccess()
        If BTreeBenchmark._ReportInternalStopwatch Then
            BTreeBenchmark.AddToTargetRandomAccess(Sd, Sw, _RandomValues)
        Else
            BTreeBenchmark.AddToTargetRandomAccess(Sd, _RandomValues)
        End If
    End Sub

    <Benchmark>
    Public Shared Sub SortedBTreeAddRandomAccess()
        If BTreeBenchmark._ReportInternalStopwatch Then
            BTreeBenchmark.AddToTargetRandomAccess(Sbtd, Sw, _RandomValues)
        Else
            BTreeBenchmark.AddToTargetRandomAccess(Sbtd, _RandomValues)
        End If
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Avl")>
    <Benchmark>
    Public Shared Sub SortedAvlTreeAddRandomAccess()
        If BTreeBenchmark._ReportInternalStopwatch Then
            BTreeBenchmark.AddToTargetRandomAccess(Satd, Sw, _RandomValues)
        Else
            BTreeBenchmark.AddToTargetRandomAccess(Satd, _RandomValues)
        End If
    End Sub

    <Benchmark>
    Public Shared Sub SortedSplayTreeAddRandomAccess()
        If BTreeBenchmark._ReportInternalStopwatch Then
            BTreeBenchmark.AddToTargetRandomAccess(Sstd, Sw, _RandomValues)
        Else
            BTreeBenchmark.AddToTargetRandomAccess(Sstd, _RandomValues)
        End If
    End Sub

    <Benchmark>
    Public Shared Sub DictionarySearchRandomAccess()
        If BTreeBenchmark._ReportInternalStopwatch Then
            BTreeBenchmark.SearchTargetRandomAccess(D, Sw, _RandomValues)
        Else
            BTreeBenchmark.SearchTargetRandomAccess(D, _RandomValues)
        End If
    End Sub

    <Benchmark>
    Public Shared Sub SortedDictionarySearchRandomAccess()
        If BTreeBenchmark._ReportInternalStopwatch Then
            BTreeBenchmark.SearchTargetRandomAccess(Sd, Sw, _RandomValues)
        Else
            BTreeBenchmark.SearchTargetRandomAccess(Sd, _RandomValues)
        End If
    End Sub

    <Benchmark>
    Public Shared Sub SortedBTreeSearchRandomAccess()
        If BTreeBenchmark._ReportInternalStopwatch Then
            BTreeBenchmark.SearchTargetRandomAccess(Sbtd, Sw, _RandomValues)
        Else
            BTreeBenchmark.SearchTargetRandomAccess(Sbtd, _RandomValues)
        End If
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Avl")>
    <Benchmark>
    Public Shared Sub SortedAvlTreeSearchRandomAccess()
        If BTreeBenchmark._ReportInternalStopwatch Then
            BTreeBenchmark.SearchTargetRandomAccess(Satd, Sw, _RandomValues)
        Else
            BTreeBenchmark.SearchTargetRandomAccess(Satd, _RandomValues)
        End If
    End Sub

    <Benchmark>
    Public Shared Sub SortedSplayTreeSearchRandomAccess()
        If BTreeBenchmark._ReportInternalStopwatch Then
            BTreeBenchmark.SearchTargetRandomAccess(Sstd, Sw, _RandomValues)
        Else
            BTreeBenchmark.SearchTargetRandomAccess(Sstd, _RandomValues)
        End If
    End Sub

#End Region

End Class
