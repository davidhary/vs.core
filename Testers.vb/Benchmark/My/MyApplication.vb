Namespace My

    Partial Friend Class MyApplication

        Public Const AssemblyTitle As String = "ISR Core Benchmark"
        Public Const AssemblyDescription As String = "ISR Core Benchmark"
        Public Const AssemblyProduct As String = "Core.Benchmark"

    End Class

    ''' <summary>   Provides access to the project <see cref="My.MySettings"/> configuration information. </summary>
    ''' <remarks>   David, 2020-12-05. </remarks>
    <Global.Microsoft.VisualBasic.HideModuleNameAttribute(),
     Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),
     Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute()>
    Friend Module Settings

        ''' <summary>   Gets the <see cref="My.MySettings"/> configuration information instance. </summary>
        ''' <value> The <see cref="My.MySettings"/> configuration information instance. </value>
        <Global.System.ComponentModel.Design.HelpKeywordAttribute("My.Settings.Default")>
        Friend ReadOnly Property [Default]() As Global.isr.Core.Tester.My.MySettings
            Get
                Return Global.isr.Core.Tester.My.MySettings.Default
            End Get
        End Property
    End Module

End Namespace


