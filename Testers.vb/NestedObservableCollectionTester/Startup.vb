﻿Imports isr.Core
Imports System.Collections
Imports System.Linq
Imports System.ComponentModel

Module Startup

    Sub Main()

        Dim success As Boolean
        Dim weakEvents As WeakEvents = Nothing

        Try

            Console.WriteLine("Initializing test class {0}", "WeakEvents")
            weakEvents = New WeakEvents("WeakEvents")
            AddHandler weakEvents.PropertyChanged, AddressOf weakEvents_PropertyChanged
            AddHandler weakEvents.NestedPropertyChanged, AddressOf weakEvents_NestedPropertyChanged
            AddHandler weakEvents.SelectorsCollectionChanged, AddressOf weakEvents_SelectorsCollectionChanged
            AddHandler weakEvents.WordsCollectionChanged, AddressOf weakEvents_WordsCollectionChanged
            Console.WriteLine("Hit CR to continue")
            Console.ReadKey()

            Dim tempLookupItems() As LookupItem = {New LookupItem() With {.Value = 1, .Caption = "0x00000001"},
                                                  New LookupItem() With {.Value = 2, .Caption = "0x00000010"},
                                                  New LookupItem() With {.Value = 4, .Caption = "0x00000100"},
                                                  New LookupItem() With {.Value = 8, .Caption = "0x00001000"}}

            ' creates the collection of lookup items.
            For Each item As LookupItem In TempLookupItems
                Console.WriteLine("Adding lookup item {0}", item.Caption)
                weakEvents.Selectors.Add(item)
                Console.WriteLine("Hit CR to continue")
                Console.ReadKey()
            Next

            ' selects item number 3. This changes the Is Selected property. This would be handled by
            ' the event hander to display the hex value for selected or not hex value for not selected.
            Dim itemNumber As Integer = 3
            Console.WriteLine("Selecting item  {0}", itemNumber)
            weakEvents.Selectors(itemNumber).IsSelected = Not weakEvents.Selectors(itemNumber).IsSelected
            Console.WriteLine("Hit CR to continue")
            Console.ReadKey()

            ' adds a word. This is handled by the collection chaged event.
            Console.WriteLine("Adding a word to {0}", "WeakEvents")
            weakEvents.Words.Add(New NamedLookupItem("word" & CStr(weakEvents.Words.Count)) With {.Index = weakEvents.Words.Count, .Caption = "Really?"})
            Console.WriteLine("Hit CR to continue")
            Console.ReadKey()

            ' changes caption for item 3.  This is handled by the event handler to show the property changes 
            Console.WriteLine("Changing word {0} caption", itemNumber)
            weakEvents.Words.Item(3).Caption = weakEvents.Words.Item(3).Caption & "<>"
            Console.WriteLine("Hit CR to continue")
            Console.ReadKey()

            success = True

        Catch ex As Exception

            ' log the exception
            Console.Error.WriteLine(ex.ToString)

        Finally

            If weakEvents IsNot Nothing Then
                RemoveHandler weakEvents.PropertyChanged, AddressOf weakEvents_PropertyChanged
                RemoveHandler weakEvents.SelectorsCollectionChanged, AddressOf weakEvents_SelectorsCollectionChanged
                RemoveHandler weakEvents.WordsCollectionChanged, AddressOf weakEvents_WordsCollectionChanged
            End If

            Console.Error.WriteLine("Hit CR to end")
            Console.ReadKey()

            ' flush the log.
            My.Application.Log.DefaultFileLogWriter.Flush()

            ' For some reason the event handling set in the Settings class dos not really work.
            My.Settings.Save()

            ' do some garbage collection
            System.GC.Collect()

            If success Then
                ' exit with success code
                Environment.Exit(0)
            Else
                ' exit with an error code
                Environment.Exit(-1)
            End If

        End Try

    End Sub

    ''' <summary>
    ''' Handles the NestedPropertyChanged event of the weakEvents control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="isr.Core.NestedPropertyChangedEventArgs" /> instance containing the event data.</param>
    Private Sub WeakEvents_NestedPropertyChanged(ByVal sender As Object, ByVal e As NestedPropertyChangedEventArgs)
        Dim weakEvents As WeakEvents = TryCast(sender, WeakEvents)
        Dim source As Object = e.Sources.Pop
        If source Is weakEvents Then
            source = e.Sources.Pop
            If TryCast(source, INestedPropertyChanged).SourceName = "Selectors" Then
                source = e.Sources.Pop
                OnSelectorsNestedPropertyChanged(TryCast(source, LookupItem), e.PropertyName)
            ElseIf TryCast(source, INestedPropertyChanged).SourceName = "Words" Then
                source = e.Sources.Pop
                OnWordsNestedPropertyChanged(TryCast(source, NamedLookupItem), e.PropertyName)
            Else
                Console.WriteLine("Unexpected source name = '{0}'", TryCast(source, INestedPropertyChanged).SourceName)
            End If
        Else
            Console.WriteLine("Unexpected source -- expected weak events")
        End If
    End Sub

    Private Sub WeakEvents_PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        Console.WriteLine("Property Changed: {0}", e.PropertyName)
    End Sub

    Private Sub WeakEvents_SelectorsCollectionChanged(ByVal sender As Object, ByVal e As Specialized.NotifyCollectionChangedEventArgs)
        Console.WriteLine("Selectors Collection Changed. Action: {0}", e.Action)
    End Sub

    Private Sub WeakEvents_WordsCollectionChanged(ByVal sender As Object, ByVal e As Specialized.NotifyCollectionChangedEventArgs)
        Console.WriteLine("Words Collection Changed. Action: {0}", e.Action)
    End Sub

#Region " PROPERTY CHANGES HANDLING "

    Private Sub OnSelectorsNestedPropertyChanged(ByVal item As LookupItem, ByVal propertyName As String)
        Console.WriteLine("Selectors Property Changed: {0}", propertyName)
        If propertyName = "IsSelected" Then
            If item.IsSelected Then
                Console.WriteLine("Value: {0}", CInt(Microsoft.VisualBasic.Fix(item.Value)))
            Else
                Console.WriteLine("Not Value: {0}", Not CInt(Microsoft.VisualBasic.Fix(item.Value)))
            End If
        End If
    End Sub

    Private Sub OnWordsNestedPropertyChanged(ByVal item As NamedLookupItem, ByVal propertyName As String)
        Console.WriteLine("Words Property Changed: {0}.{1}", item.SourceName, propertyName)
        If propertyName = "Caption" Then
            Console.WriteLine("Caption changed: {0}", item.Caption)
        End If
    End Sub

#End Region



End Module

#Region " TEST.NESTED.COLLECTIONS.STEP2 "

''' <summary>
''' Utility class useful for various lookup lists and selectors
''' </summary>
Public Class LookupItem
  Implements ComponentModel.INotifyPropertyChanged

  Private _Index As Integer
  Public Property Index() As Integer
    Get
      Return Me._index
    End Get
    Set(ByVal value As Integer)
      Me._index = value
      OnPropertyChanged("Index")
    End Set
  End Property

  Private _IsSelected As Boolean
  Public Property IsSelected() As Boolean
    Get
      Return Me._isSelected
    End Get
    Set(ByVal value As Boolean)
      Me._isSelected = value
      OnPropertyChanged("IsSelected")
    End Set
  End Property

  Private _Caption As String
  Public Property Caption() As String
    Get
      Return Me._caption
    End Get
    Set(ByVal value As String)
      Me._caption = value
      OnPropertyChanged("Caption")
    End Set
  End Property

  Private _Value As Object
  Public Property Value() As Object
    Get
      Return Me._value
    End Get
    Set(ByVal value As Object)
      Me._value = value
      OnPropertyChanged("Value")
    End Set
  End Property

  Private _Type As String
  Public Property Type() As String
    Get
      Return Me._type
    End Get
    Set(ByVal value As String)
      Me._type = value
      OnPropertyChanged("Type")
    End Set
  End Property

  ''' <summary>
  ''' Occurs when a property value changes.
  ''' </summary>
  Public Event PropertyChanged As ComponentModel.PropertyChangedEventHandler Implements ComponentModel.INotifyPropertyChanged.PropertyChanged

  ''' <summary>
  ''' Called when a property changes.
  ''' </summary>
  ''' <param name="propertyName">The property name.</param>
  Protected Overridable Sub OnPropertyChanged(ByVal propertyName As String)
        Dim evt As PropertyChangedEventHandler = Me.PropertyChangedEvent
        evt?.Invoke(Me, New PropertyChangedEventArgs(propertyName))
    End Sub

End Class

''' <summary>
''' Utility class useful for various lookup lists and selectors
''' </summary>
Public Class NamedLookupItem
    Inherits LookupItem
    Implements INestedPropertyChanged

    Public Sub New(ByVal name As String)
        MyBase.New()
        Me._SourceName = name
    End Sub

    Public Property SourceName() As String Implements isr.Core.INestedPropertyChanged.SourceName

    ''' <summary>
    ''' Occurs when nested property changed.
    ''' </summary>
    Public Event NestedPropertyChanged As System.EventHandler(Of NestedPropertyChangedEventArgs) Implements INestedPropertyChanged.NestedPropertyChanged

    ''' <summary>
    ''' Called when a property changes.
    ''' </summary>
    ''' <param name="propertyName">The property name.</param>
    Protected Overrides Sub OnPropertyChanged(ByVal propertyName As String)
        Dim evt As EventHandler(Of NestedPropertyChangedEventArgs) = Me.NestedPropertyChangedEvent
        evt?.Invoke(Me, New NestedPropertyChangedEventArgs(Me, propertyName))
    End Sub

End Class

''' <summary>
''' Interaction logic for WeakEvents.xaml
''' </summary>
Partial Public Class WeakEvents

    Implements ComponentModel.INotifyPropertyChanged, INestedPropertyChanged, System.Windows.IWeakEventListener

    Public Sub New(ByVal sourceName As String)
        MyBase.New()
        Me._sourceName = sourceName
        Me.populateSelectors(New NestedObservableCollection(Of LookupItem)("Selectors"))
        ' trick number 1: converting string into char[]

        ' trick number 2: generating sequential numbers using LINQ
        Words = New NestedObservableCollection(Of NamedLookupItem)("Words")
        For Each w As String In "Hello world from WPF".Split(" "c)
            Words.Add(New NamedLookupItem("word" & CStr(Words.Count)) With {.Caption = Word, .Index = Words.Count})
        Next

    End Sub

    ''' <summary>
    ''' Populates the selectors.
    ''' </summary>
    ''' <param name="value">The value.</param>
    Private Sub PopulateSelectors(ByVal value As NestedObservableCollection(Of LookupItem))
        If Not Me._selectors Is Nothing Then
            NestedPropertyChangedWeakEventManager.RemoveListener(Me._selectors, Me)
            Specialized.CollectionChangedEventManager.RemoveListener(Me._selectors, Me)
        End If
        Me._selectors = value
        If Not Me._selectors Is Nothing Then
            NestedPropertyChangedWeakEventManager.AddListener(Me._selectors, Me)
            Specialized.CollectionChangedEventManager.AddListener(Me._selectors, Me)
        End If
        Me.OnPropertyChanged("Selectors")
    End Sub

    Private _Selectors As NestedObservableCollection(Of LookupItem)
    Public Property Selectors() As NestedObservableCollection(Of LookupItem)
        Get
            Return Me._selectors
        End Get
        Private Set(ByVal value As NestedObservableCollection(Of LookupItem))
            Me.populateSelectors(value)
        End Set
    End Property

    Private _Hex As Integer
    Public Property Hex() As Integer
        Get
            Return Me._hex
        End Get
        Set(ByVal value As Integer)
            Me._hex = value
            Me.OnPropertyChanged("Hex")
        End Set
    End Property

    Private _Word As String
    Public Property Word() As String
        Get
            Return Me._word
        End Get
        Set(ByVal value As String)
            Me._word = value
            Me.OnPropertyChanged("Word")
        End Set
    End Property

    Private _Phrase As String
    Public Property Phrase() As String
        Get
            Return Me._phrase
        End Get
        Set(ByVal value As String)
            Me._phrase = value
            Me.OnPropertyChanged("Phrase")
        End Set
    End Property

    Private _Words As NestedObservableCollection(Of NamedLookupItem)
    Public Property Words() As NestedObservableCollection(Of NamedLookupItem)
        Get
            Return Me._words
        End Get
        Private Set(ByVal value As NestedObservableCollection(Of NamedLookupItem))
            If Not Me._words Is Nothing Then
                NestedPropertyChangedWeakEventManager.RemoveListener(Me._words, Me)
            End If
            Me._words = value
            If Not Me._words Is Nothing Then
                NestedPropertyChangedWeakEventManager.AddListener(Me._words, Me)
                ' Phrase = String.Join(" ", New String() {"", ""}) ' CType(From word In Me._words, String()) Orderby word.Index Select word.Caption)
                ' Phrase = String.Join(" ", CType(From word In Me._words, String()) Orderby word.Index Select word.Caption)
                Dim linqQuery = From word In Me._words Order By word.Index Select word.Caption
                Phrase = String.Join(" ", linqQuery.ToArray)
            End If
            Me.OnPropertyChanged("Words")
        End Set
    End Property

#Region " LIST CHANGES HANDLING "

    Public Event SelectorsCollectionChanged As EventHandler(Of Specialized.NotifyCollectionChangedEventArgs)

    Private Sub OnSelectorsCollectionChanged(ByVal sender As Object, ByVal e As Specialized.NotifyCollectionChangedEventArgs)
        SelectorsCollectionChangedEvent(Me, e)
    End Sub

    Public Event WordsCollectionChanged As EventHandler(Of Specialized.NotifyCollectionChangedEventArgs)

    Private Sub OnWordsCollectionChanged(ByVal sender As Object, ByVal e As Specialized.NotifyCollectionChangedEventArgs)
        WordsCollectionChangedEvent(Me, e)
    End Sub

#End Region

#Region " PROPERTY CHANGES HANDLING "

    Public Property SourceName() As String Implements isr.Core.INestedPropertyChanged.SourceName

    ''' <summary>
    ''' Occurs when nested property changed.
    ''' </summary>
    Public Event NestedPropertyChanged As EventHandler(Of NestedPropertyChangedEventArgs) Implements INestedPropertyChanged.NestedPropertyChanged

    ''' <summary>
    ''' Occurs when a property value changes.
    ''' </summary>
    Public Event PropertyChanged As ComponentModel.PropertyChangedEventHandler Implements ComponentModel.INotifyPropertyChanged.PropertyChanged

    ''' <summary>
    ''' Called when a property changes.
    ''' </summary>
    ''' <param name="propertyName">The property name.</param>
    Protected Sub OnPropertyChanged(ByVal propertyName As String)
        Dim evt As PropertyChangedEventHandler = Me.PropertyChangedEvent
        evt?.Invoke(Me, New PropertyChangedEventArgs(propertyName))

        Dim evt1 As EventHandler(Of NestedPropertyChangedEventArgs) = Me.NestedPropertyChangedEvent
        If evt1 IsNot Nothing Then evt.Invoke(Me, New NestedPropertyChangedEventArgs(Me, propertyName))
    End Sub

    ''' <summary>
    ''' Raises the <see cref="E:PropertyChanged" /> event.
    ''' </summary>
    ''' <param name="e">The <see cref="isr.Core.NestedPropertyChangedEventArgs" /> instance containing the event data.</param>
    Protected Sub OnPropertyChanged(ByVal e As NestedPropertyChangedEventArgs)
        Dim evt As PropertyChangedEventHandler = Me.PropertyChangedEvent
        evt?.Invoke(Me, New PropertyChangedEventArgs(e.PropertyName))

        Dim evt1 As EventHandler(Of NestedPropertyChangedEventArgs) = Me.NestedPropertyChangedEvent
        If evt1 IsNot Nothing Then evt.Invoke(Me, New NestedPropertyChangedEventArgs(Me, e.PropertyName))
    End Sub

#End Region

#Region " WEAK EVENT HANDLING "

    ''' <summary>
    ''' Receives events from the centralized event manager.
    ''' </summary>
    ''' <param name="managerType">The type of the <see cref="T:System.Windows.WeakEventManager" /> calling this method.</param>
    ''' <param name="sender">Object that originated the event.</param>
    ''' <param name="e">Event data.</param><returns>
    ''' true if the listener handled the event. It is considered an error by the <see cref="T:System.Windows.WeakEventManager" /> handling in WPF to register a listener for an event that the listener does not handle. Regardless, the method should return false if it receives an event that it does not recognize or handle.
    ''' </returns>
    Public Function ReceiveWeakEvent(ByVal managerType As Type, ByVal sender As Object, ByVal e As EventArgs) As Boolean Implements System.Windows.IWeakEventListener.ReceiveWeakEvent

        If managerType Is GetType(Specialized.CollectionChangedEventManager) Then
            ' Put all your CollectionChanged event handlers here
            If sender Is Selectors Then
                OnSelectorsCollectionChanged(sender, CType(e, Specialized.NotifyCollectionChangedEventArgs))
            ElseIf sender Is Words Then
                OnWordsCollectionChanged(sender, CType(e, Specialized.NotifyCollectionChangedEventArgs))
            End If

        ElseIf managerType Is GetType(NestedPropertyChangedWeakEventManager) Then

            ' propagate the nested changes up the chain
            Me.OnPropertyChanged(CType(e, NestedPropertyChangedEventArgs))

        ElseIf managerType Is GetType(PropertyChangedWeakEventManager) Then
            ' Put all your PropertyChanged event handlers here
            Me.OnPropertyChanged(CType(e, PropertyChangedEventArgs).PropertyName)
        End If

        Return True

    End Function

#End Region

End Class

#End Region
