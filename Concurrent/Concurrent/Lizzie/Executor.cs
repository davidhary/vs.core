﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace isr.Core.Concurrent
{

    /// <summary> Class allowing you to execute a list of actions. </summary>
    /// <remarks>
    /// Copyright (c) 2018 Thomas Hansen - thomas@gaiasoul.com <para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class Executor
    {

        /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        private Executor()
        {
        }

        /// <summary> Sequentially executes each action on calling thread. </summary>
        /// <remarks> David, 2020-09-05. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="actions"> Actions to execute. </param>
        public static void Sequentially( Action[] actions )
        {
            if ( actions is null )
            {
                throw new ArgumentNullException( nameof( actions ) );
            }
            // Sequentially execute each action on calling thread.
            foreach ( Action ix in actions )
            {
                ix();
            }
        }

        /// <summary> Sequentially executes each action on calling thread. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="actions"> Actions to execute. </param>
        public static void Sequentially( IEnumerable<Action> actions )
        {
            if ( actions is null )
            {
                throw new ArgumentNullException( nameof( actions ) );
            }
            // Sequentially execute each action on calling thread.
            foreach ( Action ix in actions )
            {
                ix();
            }
        }

        /// <summary> Sequentially executes each action on calling thread. </summary>
        /// <remarks> David, 2020-09-05. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="actions"> Actions to execute. </param>
        public static void Sequentially( IList<Action> actions )
        {
            if ( actions is null )
            {
                throw new ArgumentNullException( nameof( actions ) );
            }
            // Sequentially execute each action on calling thread.
            foreach ( Action ix in actions )
            {
                ix();
            }
        }

        /// <summary>
        /// Sequentially executes each action on a different thread without blocking the calling thread.
        /// </summary>
        /// <remarks> David, 2020-09-05. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="actions"> Actions to execute. </param>
        public static void SequentiallyUnblocked( Action[] actions )
        {
            if ( actions is null )
            {
                throw new ArgumentNullException( nameof( actions ) );
            }
            // Sanity checking argument.
            if ( !actions.Any() )
            {
                return; // No reasons to create our thread.
            }

            // Executes actions on another thread.
            var thread = new Thread( new ThreadStart( () => Sequentially( actions ) ) );
            thread.Start();
        }

        /// <summary>
        /// Sequentially executes each action on a different thread without blocking the calling thread.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="actions"> Actions to execute. </param>
        public static void SequentiallyUnblocked( IEnumerable<Action> actions )
        {
            if ( actions is null )
            {
                throw new ArgumentNullException( nameof( actions ) );
            }
            // Sanity checking argument.
            if ( !actions.Any() )
            {
                return; // No reasons to create our thread.
            }

            // Executes actions on another thread.
            var thread = new Thread( new ThreadStart( () => Sequentially( actions ) ) );
            thread.Start();
        }

        /// <summary>
        /// Sequentially executes each action on a different thread without blocking the calling thread.
        /// </summary>
        /// <remarks> David, 2020-09-05. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="actions"> Actions to execute. </param>
        public static void SequentiallyUnblocked( IList<Action> actions )
        {
            if ( actions is null )
            {
                throw new ArgumentNullException( nameof( actions ) );
            }
            // Sanity checking argument.
            if ( !actions.Any() )
            {
                return; // No reasons to create our thread.
            }

            // Executes actions on another thread.
            var thread = new Thread( new ThreadStart( () => Sequentially( actions ) ) );
            thread.Start();
        }

        /// <summary>
        /// Executes each action in parallel blocking the calling thread until all actions are finished
        /// executing.
        /// </summary>
        /// <remarks> David, 2020-09-05. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="actions"> Actions to execute. </param>
        public static void Parallel( Action[] actions )
        {
            if ( actions is null )
            {
                throw new ArgumentNullException( nameof( actions ) );
            }

            var threads = actions.Select( ix => new Thread( new ThreadStart( () => ix() ) ) ).ToList();
            threads.ForEach( ix => ix.Start() );
            threads.ForEach( ix => ix.Join() );
        }

        /// <summary>
        /// Executes each action in parallel blocking the calling thread until all actions are finished
        /// executing.
        /// </summary>
        /// <remarks> David, 2020-09-05. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="actions"> Actions to execute. </param>
        public static void Parallel( IEnumerable<Action> actions )
        {
            if ( actions is null )
            {
                throw new ArgumentNullException( nameof( actions ) );
            }

            var threads = actions.Select( ix => new Thread( new ThreadStart( () => ix() ) ) ).ToList();
            threads.ForEach( ix => ix.Start() );
            threads.ForEach( ix => ix.Join() );
        }

        /// <summary>
        /// Executes each action in parallel blocking the calling thread until all actions are finished
        /// executing.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="actions"> Actions to execute. </param>
        public static void Parallel( IList<Action> actions )
        {
            if ( actions is null )
            {
                throw new ArgumentNullException( nameof( actions ) );
            }

            var threads = actions.Select( ix => new Thread( new ThreadStart( () => ix() ) ) ).ToList();
            threads.ForEach( ix => ix.Start() );
            threads.ForEach( ix => ix.Join() );
        }

        /// <summary> Executes each action in parallel without blocking the calling thread. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="actions"> Actions to execute. </param>
        public static void ParallelUnblocked( Action[] actions )
        {
            if ( actions is null )
            {
                throw new ArgumentNullException( nameof( actions ) );
            }

            foreach ( Action ix in actions )
            {
                var thread = new Thread( new ThreadStart( () => ix() ) );
                thread.Start();
            }
        }

        /// <summary> Executes each action in parallel without blocking the calling thread. </summary>
        /// <remarks> David, 2020-09-05. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="actions"> Actions to execute. </param>
        public static void ParallelUnblocked( IEnumerable<Action> actions )
        {
            if ( actions is null )
            {
                throw new ArgumentNullException( nameof( actions ) );
            }

            foreach ( Action ix in actions )
            {
                var thread = new Thread( new ThreadStart( () => ix() ) );
                thread.Start();
            }
        }

        /// <summary> Executes each action in parallel without blocking the calling thread. </summary>
        /// <remarks> David, 2020-09-05. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="actions"> Actions to execute. </param>
        public static void ParallelUnblocked( IList<Action> actions )
        {
            if ( actions is null )
            {
                throw new ArgumentNullException( nameof( actions ) );
            }

            foreach ( Action ix in actions )
            {
                var thread = new Thread( new ThreadStart( () => ix() ) );
                thread.Start();
            }
        }
    }
}