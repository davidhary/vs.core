namespace isr.Core.Concurrent
{
    /// <summary>
    /// Class encapsulating an immutable type, allowing you to 'change it', by assigning the Value
    /// property to a new instance of your immutable.
    /// </summary>
    /// <remarks>
    /// Copyright (c) 2018 Thomas Hansen - thomas@gaiasoul.com <para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public class Mutable<TImmutable>
    {

        /// <summary> Initializes a new instance of the <see cref="T:Mutable`1"/> class. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        public Mutable()
        {
        }

        /// <summary> Initializes a new instance of the <see cref="T:Mutable`1"/> class. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="initial"> Initial <see cref="T:TImmutable"/> value. </param>
        public Mutable( TImmutable initial )
        {
            this.Value = initial;
        }

        /// <summary> Gets or sets the instance value. </summary>
        /// <value> The new value. </value>
        public TImmutable Value { get; set; }
    }
}
