﻿using System;
using System.Threading;

namespace isr.Core.Concurrent
{

    /// <summary>
    /// Allows you to encapsulate an instance of a type that needs to be shared between multiple
    /// threads, such that access to the instance is easily synchronized.
    /// </summary>
    /// <remarks>
    /// <para>
    /// The <see cref="ReaderWriterLockSlim"/> class allows you to specify if your code is writing to
    /// the object or simply reading from the object. This enables multiple readers entrance at the
    /// same time, but denies any write code access until all other read and write threads are done
    /// doing their stuff. </para>
    /// <para>
    /// Now the problem: The syntax when consuming the ReaderWriterLock class becomes tedious, with
    /// lots of repetitive code that reduces readability and complicates maintenance over time, and
    /// your code often becomes scattered with multiple try and finally blocks. A simple typo can
    /// also produce disastrous effects that are sometimes extremely difficult to spot later.</para>
    /// <para>
    /// This class solves the problem without repetitive code, while reducing the risk that a minor
    /// typo will spoil your day. The class is entirely based on lambda trickery. It’s arguably just
    /// syntactic sugar around some delegates, assuming the existence of a couple interfaces. Most
    /// important, it can help make your code much more DRY (as in, “Don’t Repeat Yourself”).</para>
    /// <para>Implementation</para><para>
    /// The class assumes you have a read interface and a write interface on your type. You can also
    /// use it by repeating the template class itself three times, if for some reason you can’t
    /// change the implementation of the underlying class to which you need to synchronize access.
    /// </para><para>
    /// David, 2019-02-03. https://msdn.microsoft.com/en-us/magazine/mt833270.aspx. </para><para>
    /// Copyright (c) 2018 Thomas Hansen - thomas@gaiasoul.com </para><para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public class Synchronizer<TScribe, TIRead, TIWrite> : IDisposable where TScribe : TIWrite, TIRead
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> The slim lock. </summary>
        /// <remarks>  Our actual locker, that will synchronize access to our _shared instance. </remarks>
        private ReaderWriterLockSlim _SlimLock = new();

        /// <summary> The scribe. </summary>
        /// <remarks> Our actual scribe (read and write) resource. </remarks>
        private TScribe _Scribe;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:threadsynchronization.Synchronizer`1"/> class.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="scribe"> Instance that needs synchronize access in multiple threads. </param>
        public Synchronizer( TScribe scribe )
        {
            this._Scribe = scribe;
        }

        /// <summary> Gets or sets the sentinel to detect redundant calls. </summary>
        /// <value> The sentinel to detect redundant calls. </value>
        protected bool IsDisposed { get; private set; }

        /// <summary>
        /// Releases the unmanaged resources used by the isr.Core.Models.ThreadSafeToken{T} and
        /// optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="disposing"> True to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing )
                    {
                    }

                    if ( this._SlimLock is object )
                    {
                        this._SlimLock.Dispose();
                        this._SlimLock = null;
                    }
                }
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        public void Dispose()
        {
            // Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
            this.Dispose( true );
            // uncomment the following line if Finalize() is overridden above.
            GC.SuppressFinalize( this );
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called. It gives the base
        /// class the opportunity to finalize. Do not provide destructors in types derived from this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        ~Synchronizer()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal for readability and maintainability.
            this.Dispose( false );
        }

        #endregion

        /// <summary>
        /// Enters a read lock giving the caller access to the shared instance in "read only" mode.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="functor"> Functor. </param>
        public void Read( Action<TIRead> functor )
        {
            if ( functor is null )
            {
                throw new ArgumentNullException( nameof( functor ) );
            }

            this._SlimLock.EnterReadLock();
            try
            {
                functor( this._Scribe );
            }
            finally
            {
                this._SlimLock.ExitReadLock();
            }
        }

        /// <summary>
        /// Enters a write lock giving the caller access to the shared resource in "read and write" mode.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="functor"> Functor. </param>
        public void Write( Action<TIWrite> functor )
        {
            if ( functor is null )
            {
                throw new ArgumentNullException( nameof( functor ) );
            }

            this._SlimLock.EnterWriteLock();
            try
            {
                functor( this._Scribe );
            }
            finally
            {
                this._SlimLock.ExitWriteLock();
            }
        }

        /// <summary>
        /// Enters a write lock giving the caller access to the shared resource in "read and write" mode,
        /// for then to reassign the shared object to the value returned from the Func.
        /// 
        /// Useful for changing immutable instances.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="functor"> Functor. </param>
        public void Assign( Func<TIWrite, TScribe> functor )
        {
            if ( functor is null )
            {
                throw new ArgumentNullException( nameof( functor ) );
            }

            this._SlimLock.EnterWriteLock();
            try
            {
                this._Scribe = functor( this._Scribe );
            }
            finally
            {
                this._SlimLock.ExitWriteLock();
            }
        }
    }

    /// <summary>
    /// Simplified syntax where you cannot modify the shared type and implement your own read and
    /// write interfaces.
    /// 
    /// Notice, when using this class you are on your own in regards to making sure you never
    /// actually modify the shared instance inside a "read only" delegate.
    /// </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    public class Synchronizer<TScribe> : Synchronizer<TScribe, TScribe, TScribe>
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:threadsynchronization.Synchronizer`1" /> class.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="scribe"> Instance that needs synchronize access in multiple threads. </param>
        public Synchronizer( TScribe scribe ) : base( scribe )
        {
        }
    }
}