using System.Collections;
using System.Collections.Generic;

namespace isr.Core.Concurrent
{
    /// <summary> Contains a sequence of TLambda instances. </summary>
    /// <remarks>
    /// Copyright (c) 2018 Thomas Hansen - thomas@gaiasoul.com <para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public abstract class Sequence<T> : IEnumerable<T>
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:poetic.lambda.lambdas.Sequence`1"/> class.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        protected Sequence()
        {
            this.List = new List<T>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:poetic.lambda.lambdas.Sequence`1"/> class.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="items"> Initial items. </param>
        protected Sequence( params T[] items )
        {
            this.List = new List<T>( items );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:poetic.lambda.lambdas.Sequence`1"/> class.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="items"> Initial items. </param>
        protected Sequence( IEnumerable<T> items )
        {
            this.List = new List<T>( items );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:poetic.lambda.lambdas.Sequence`1"/> class.
        /// </summary>
        /// <remarks> David, 2020-09-05. </remarks>
        /// <param name="items"> Initial items. </param>
        protected Sequence( IList<T> items )
        {
            this.List = new List<T>( items );
        }

        /// <summary> Gets or sets the list. </summary>
        /// <value> The list. </value>
        private List<T> List { get; set; }

        /// <summary> Appends the specified item to the sequence. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="item"> Item to append. </param>
        public void Add( T item )
        {
            this.List.Add( item );
        }

        /// <summary> Adds a range of items to the sequence. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="items"> Items to add. </param>
        public void AddRange( params T[] items )
        {
            this.List.AddRange( items );
        }

        /// <summary> Adds a range of items to the sequence. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="items"> Items to add. </param>
        public void AddRange( IEnumerable<T> items )
        {
            this.List.AddRange( items );
        }

        /// <summary> Adds a range of items to the sequence. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="items"> Items to add. </param>
        public void AddRange( IList<T> items )
        {
            this.List.AddRange( items );
        }

        #region "[ -- Interface implementations -- ]"

        /// <summary> Gets the enumerator for the items. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> The enumerator. </returns>
        public IEnumerator<T> GetEnumerator()
        {
            return this.List.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
        #endregion

    }
}
