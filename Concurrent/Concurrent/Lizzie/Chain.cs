using System;
using System.Collections.Generic;

namespace isr.Core.Concurrent
{
    /// <summary>
    /// Class encapsulating a list of Func delegates where the output from one is being passed in as
    /// input to the next.
    /// </summary>
    /// <remarks>
    /// Copyright (c) 2018 Thomas Hansen - thomas@gaiasoul.com <para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public class Chain<T> : Functions<T, T>
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:poetic.lambda.lambdas.Chain`1"/> class.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        public Chain() : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:poetic.lambda.lambdas.Chain`1"/> class.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="functions"> Initial functors. </param>
        public Chain( params Func<T, T>[] functions ) : base( functions )
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:poetic.lambda.lambdas.Chain`1"/> class.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="functions"> Initial functors. </param>
        public Chain( IEnumerable<Func<T, T>> functions ) : base( functions )
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:poetic.lambda.lambdas.Sequence`1" /> class.
        /// </summary>
        /// <remarks> David, 2020-09-05. </remarks>
        /// <param name="functions"> Initial items. </param>
        public Chain( IList<Func<T, T>> functions ) : base( functions )
        {
        }

        /// <summary> Evaluates the chain, and returns the result to caller. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="actionArgument"> The action argument. </param>
        /// <returns> A T. </returns>
        public T EvaluateChain( T actionArgument )
        {
            foreach ( Func<T, T> ix in this )
            {
                actionArgument = ix( actionArgument );
            }

            return actionArgument;
        }
    }
}
