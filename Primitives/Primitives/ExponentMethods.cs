using System;

namespace isr.Core.Primitives.NumericExtensions
{
    /// <summary> Includes exponent extension methods. </summary>
    /// <remarks> (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para> </remarks>
    internal static partial class NumericExtensionMethods
    {

        #region " DOUBLE "

        /// <summary> Computes e<sup>x</sup>-1. </summary>
        /// <remarks>
        /// If x is close to 0, then e<sup>x</sup> is close to 1, and computing e<sup>x</sup>-1 by
        /// subtracting one from e<sup>x</sup> as computed by the <see cref="Math.Exp" /> function will
        /// be subject to severe loss of significance due to cancellation. This method maintains full
        /// precision for all values of x by switching to a series expansion for values of x near zero.
        /// </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when operation failed to execute. </exception>
        /// <param name="value">             The argument. </param>
        /// <param name="maximumIterations"> The maximum number of iterations of a series for convergence. </param>
        /// <returns> The value of e<sup>x</sup>-1. </returns>
        public static double ExpMinusOne( this double value, int maximumIterations )
        {
            if ( Math.Abs( value ) < 0.125d )
            {
                double dr = value;
                double r = dr;
                for ( int k = 2, loopTo = maximumIterations - 1; k <= loopTo; k++ )
                {
                    double r_old = r;
                    dr *= value / k;
                    r += dr;
                    if ( r == r_old )
                    {
                        return r;
                    }
                }

                throw new InvalidOperationException();
            }
            else
            {
                return Math.Exp( value ) - 1.0d;
            }
        }

        /// <summary> Returns the exponent. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> A <see cref="T:System.Double">Double</see> value. </param>
        /// <returns> The most significant decade of the value. </returns>
        /// <example>
        /// NumericExtensions.Exponent(1.12) returns 0.<p>
        /// Unary.Decade(11.23) returns 1.</p><p>
        /// NumericExtensions.Exponent(-1.123) returns 0.</p><p>
        /// NumericExtensions.Exponent(0.1234) returns -1</p>
        /// </example>
        /// <seealso cref="Decimal"/>
        public static int Exponent( this double value )
        {

            // check if the value is zero
            if ( value == 0d )
            {

                // if so, return This value
                return 0;
            }
            else
            {

                // get the exponent based on the most significant digit.
                // Return Convert.ToInt32(System.Math.Floor(System.Math.Log(System.Math.Abs(value))) / System.Math.Log(10.0#))
                return Convert.ToInt32( Math.Floor( Math.Abs( value ).Log10() ) );
            }
        }

        /// <summary> Returns the exponent. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value">               A <see cref="T:System.Double">Double</see> value. </param>
        /// <param name="useEngineeringScale"> true to use engineering scale. </param>
        /// <returns> The most significant decade of the value. </returns>
        /// <example>
        /// NumericExtensions.Exponent(11.12) returns 0.<p>
        /// Unary.Decade(111.23) returns 1.</p><p>
        /// NumericExtensions.Exponent(1111.123) returns 3.</p><p>
        /// NumericExtensions.Exponent(-11.12) returns 0.</p><p>
        /// Unary.Decade(-111.23) returns 1.</p><p>
        /// NumericExtensions.Exponent(-1111.123) returns 3.</p><p>
        /// NumericExtensions.Exponent(0.1234) returns 0</p><p>
        /// NumericExtensions.Exponent(0.01234) returns 0</p><p>
        /// NumericExtensions.Exponent(0.0001234) returns -3</p><p>
        /// NumericExtensions.Exponent(-0.1234) returns 0</p><p>
        /// NumericExtensions.Exponent(-0.01234) returns 0</p><p>
        /// NumericExtensions.Exponent(-0.001234) returns -3</p><p>
        /// NumericExtensions.Exponent(-0.0001234) returns -3</p>
        /// </example>
        public static int Exponent( this double value, bool useEngineeringScale )
        {

            // check if the value is zero
            if ( value == 0d )
            {

                // if so, return This value
                return 0;
            }
            else if ( useEngineeringScale )
            {
                // Use a power of 10 that is a multiple of 3 (engineering scale)
                return 3 * (value.Exponent() / 3);
            }
            else
            {
                return value.Exponent();
            }
        }

        /// <summary> Calculate a base 10 logarithm in a safe manner to avoid math exceptions. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The value for which the logarithm is to be calculated. </param>
        /// <returns>
        /// The value of the logarithm, or 0 if the <paramref name="value"/>
        /// argument was negative or zero.
        /// </returns>
        public static double Log10( this double value )
        {
            return Math.Log10( value < double.Epsilon ? double.Epsilon : value );
        }

        /// <summary> Raises an argument to an integer power. </summary>
        /// <remarks>
        /// <para>Low integer powers can be computed by optimized algorithms much faster than the general
        /// algorithm for an arbitrary real power employed by
        /// <see cref="T:System.Math.Pow"/>.</para>
        /// </remarks>
        /// <param name="value"> The argument. </param>
        /// <param name="power"> The power. </param>
        /// <returns> The value of x<sup>n</sup>. </returns>
        public static double Pow( this double value, int power )
        {
            if ( power < 0 )
            {
                return 1.0d / value.Pow( -power );
            }

            switch ( power )
            {
                case 0:
                    {
                        // we follow convention that 0^0 = 1
                        return 1.0d;
                    }

                case 1:
                    {
                        return value;
                    }

                case 2:
                    {
                        // 1 multiply
                        return value * value;
                    }

                case 3:
                    {
                        // 2 multiplies
                        return value * value * value;
                    }

                case 4:
                    {
                        // 2 multiplies
                        double x2 = value * value;
                        return x2 * x2;
                    }

                case 5:
                    {
                        // 3 multiplies
                        double x2 = value * value;
                        return x2 * x2 * value;
                    }

                case 6:
                    {
                        // 3 multiplies
                        double x2 = value * value;
                        return x2 * x2 * x2;
                    }

                case 7:
                    {
                        // 4 multiplies
                        double x3 = value * value * value;
                        return x3 * x3 * value;
                    }

                case 8:
                    {
                        // 3 multiplies
                        double x2 = value * value;
                        double x4 = x2 * x2;
                        return x4 * x4;
                    }

                case 9:
                    {
                        // 4 multiplies
                        double x3 = value * value * value;
                        return x3 * x3 * x3;
                    }

                case 10:
                    {
                        // 4 multiplies
                        double x2 = value * value;
                        double x4 = x2 * x2;
                        return x4 * x4 * x2;
                    }

                default:
                    {
                        return Math.Pow( value, power );
                    }
            }
        }

        /// <summary> An method for squaring. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> System.Double. </returns>
        public static double Squared( this double value )
        {
            return value * value;
        }

        /// <summary> An method for cubing. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> System.Double. </returns>
        public static double Cubed( this double value )
        {
            return value * value * value;
        }

        #endregion

        #region " SINGLE "

        /// <summary> Returns the exponent. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> A <see cref="T:System.Single">Single</see> value. </param>
        /// <returns> The most significant decade of the value. </returns>
        /// <example>
        /// NumericExtensions.Exponent(1.12) returns 0.<p>
        /// Unary.Decade(11.23) returns 1.</p><p>
        /// NumericExtensions.Exponent(-1.123) returns 0.</p><p>
        /// NumericExtensions.Exponent(0.1234) returns -1</p>
        /// </example>
        /// <seealso cref="Decimal"/>
        public static int Exponent( this float value )
        {
            return (( double ) value).Exponent();
        }

        /// <summary> Returns the exponent. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value">               A <see cref="T:System.Single">Single</see> value. </param>
        /// <param name="useEngineeringScale"> true to use engineering scale. </param>
        /// <returns> The most significant decade of the value. </returns>
        /// <example>
        /// NumericExtensions.Exponent(11.12) returns 0.<p>
        /// Unary.Decade(111.23) returns 1.</p><p>
        /// NumericExtensions.Exponent(1111.123) returns 3.</p><p>
        /// NumericExtensions.Exponent(-11.12) returns 0.</p><p>
        /// Unary.Decade(-111.23) returns 1.</p><p>
        /// NumericExtensions.Exponent(-1111.123) returns 3.</p><p>
        /// NumericExtensions.Exponent(0.1234) returns 0</p><p>
        /// NumericExtensions.Exponent(0.01234) returns 0</p><p>
        /// NumericExtensions.Exponent(0.0001234) returns -3</p><p>
        /// NumericExtensions.Exponent(-0.1234) returns 0</p><p>
        /// NumericExtensions.Exponent(-0.01234) returns 0</p><p>
        /// NumericExtensions.Exponent(-0.001234) returns -3</p><p>
        /// NumericExtensions.Exponent(-0.0001234) returns -3</p>
        /// </example>
        public static int Exponent( this float value, bool useEngineeringScale )
        {
            return (( double ) value).Exponent( useEngineeringScale );
        }

        /// <summary> Calculate a base 10 logarithm in a safe manner to avoid math exceptions. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The value for which the logarithm is to be calculated. </param>
        /// <returns>
        /// The value of the logarithm, or 0 if the <paramref name="value"/>
        /// argument was negative or zero.
        /// </returns>
        public static double Log10( this float value )
        {
            return Convert.ToDouble( value ).Log10();
        }

        /// <summary> Raises an argument to an integer power. </summary>
        /// <remarks>
        /// <para>Low integer powers can be computed by optimized algorithms much faster than the general
        /// algorithm for an arbitrary real power employed by
        /// <see cref="T:System.Math.Pow"/>.</para>
        /// </remarks>
        /// <param name="value"> The argument. </param>
        /// <param name="power"> The power. </param>
        /// <returns> The value of x<sup>n</sup>. </returns>
        public static double Pow( this float value, int power )
        {
            return Convert.ToDouble( value ).Pow( power );
        }

        /// <summary> An method for squaring. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> System.Double. </returns>
        public static double Squared( this float value )
        {
            return value * value;
        }

        /// <summary> An method for cubing. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> System.Double. </returns>
        public static double Cubed( this float value )
        {
            return value * value * value;
        }


        #endregion

    }
}
