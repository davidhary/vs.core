using System;
using System.Collections.Generic;
using System.Linq;

namespace isr.Core.Primitives
{

    /// <summary> Defines a <see cref="T:System.Integer">integer</see> range class. </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2004-04-30, 1.0.1581 </para>
    /// </remarks>
    public class RangeI
    {

        #region " SHARED "

        /// <summary> Gets the empty range. </summary>
        /// <value>
        /// A <see cref="RangeI"/> value with <see cref="int.MaxValue"/>
        /// minimum value and <see cref="int.MaxValue"/> for the maximum value.
        /// </value>
        public static RangeI Empty => new( int.MaxValue, -int.MaxValue );

        /// <summary> Gets the Unity range. </summary>
        /// <value> A <see cref="RangeI"/> [0,1] value. </value>
        public static RangeI Unity => new( 0, 1 );

        /// <summary> Gets the full zero range. </summary>
        /// <value> The zero. </value>
        public static RangeI Zero => new( 0, 0 );

        /// <summary> Gets the full non-negative range. </summary>
        /// <value> The full nonnegative. </value>
        public static RangeI FullNonnegative => new( 0, int.MaxValue );

        /// <summary> Gets the full non-negative range. </summary>
        /// <value> The full. </value>
        public static RangeI Full => new( int.MinValue, int.MaxValue );

        /// <summary> Return the range of the specified data array. </summary>
        /// <remarks> David, 2020-09-05. </remarks>
        /// <param name="values"> The data array. </param>
        /// <returns> The calculated range. </returns>
        public static RangeI GetRange( int[] values )
        {
            // return the unit range if no data
            if ( values is null || !values.Any() )
            {
                return Unity;
            }

            int min = int.MaxValue;
            int max = int.MinValue;
            foreach ( int temp in values )
            {
                if ( temp < min )
                {
                    min = temp;
                }
                else if ( temp > max )
                {
                    max = temp;
                }
            }

            return new RangeI( min, max );
        }

        /// <summary> Return the range of the specified data array. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="values"> The data array. </param>
        /// <returns> The calculated range. </returns>
        public static RangeI GetRange( IEnumerable<int> values )
        {
            // return the unit range if no data
            if ( values is null || !values.Any() )
            {
                return Unity;
            }

            int min = int.MaxValue;
            int max = int.MinValue;
            foreach ( int temp in values )
            {
                if ( temp < min )
                {
                    min = temp;
                }
                else if ( temp > max )
                {
                    max = temp;
                }
            }

            return new RangeI( min, max );
        }

        /// <summary> Return the range of the specified data array. </summary>
        /// <remarks> David, 2020-09-05. </remarks>
        /// <param name="values"> The data array. </param>
        /// <returns> The calculated range. </returns>
        public static RangeI GetRange( IList<int> values )
        {
            // return the unit range if no data.
            if ( values is null || !values.Any() )
            {
                return Unity;
            }

            int min = int.MaxValue;
            int max = int.MinValue;
            foreach ( int temp in values )
            {
                if ( temp < min )
                {
                    min = temp;
                }
                else if ( temp > max )
                {
                    max = temp;
                }
            }

            return new RangeI( min, max );
        }

        /// <summary> Extended range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="range"> A <see cref="RangeI"/> value. </param>
        /// <returns> A RangeI. </returns>
        public RangeI ExtendedRange( RangeI range )
        {
            var result = new RangeI( this );
            _ = result.ExtendRange( range );
            return result;
        }

        /// <summary> Shifted range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> A RangeI. </returns>
        public RangeI ShiftedRange( int value )
        {
            var result = new RangeI( this );
            result.ShiftRange( value );
            return result;
        }

        /// <summary> Transposed range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="shift"> The shift. </param>
        /// <param name="scale"> The scale. </param>
        /// <returns> A RangeI. </returns>
        public RangeI TransposedRange( int shift, int scale )
        {
            var result = new RangeI( this );
            result.TransposeRange( shift, scale );
            return result;
        }

        #endregion

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs a <see cref="RangeR"/> instance by its span. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="span"> A <see cref="T:System.Double">Double</see> property. </param>
        public RangeI( double span ) : this( ( int ) (-0.5d * span), ( int ) (0.5d * span) )
        {
        }

        /// <summary> Constructs a <see cref="RangeI"/> instance by its limits. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="minValue"> A <see cref="T:System.Integer">integer</see> expression that
        /// specifics the minimum range. </param>
        /// <param name="maxValue"> A <see cref="T:System.Integer">integer</see> expression that
        /// specifics the maximum range. </param>
        public RangeI( int minValue, int maxValue ) : base()
        {
            this.SetRange( minValue, maxValue );
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="model"> The RangeI object from which to copy. </param>
        public RangeI( RangeI model ) : base()
        {
            if ( model is object )
            {
                this.SetRange( model.Min, model.Max );
            }
        }

        #endregion

        #region " EQUALS "

        /// <summary> Returns True if equal. </summary>
        /// <remarks>
        /// Ranges are the same if the have the same
        /// <see cref="Type">min</see> and <see cref="Type">max</see> values.
        /// </remarks>
        /// <param name="left">  The left hand side item to compare for equality. </param>
        /// <param name="right"> The left hand side item to compare for equality. </param>
        /// <returns> <c>True</c> if equals. </returns>
        public static bool Equals( RangeI left, RangeI right )
        {
            return left is null ? right is null : !(right is null) && left.Max.Equals( right.Max ) && left.Min.Equals( right.Min );
        }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
        /// <see cref="T:System.Object" />. </param>
        /// <returns>
        /// <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />; otherwise, <c>False</c>.
        /// </returns>
        public override bool Equals( object obj )
        {
            return this.Equals( obj as RangeI );
        }

        /// <summary>
        /// Returns True if the value of the <paramref name="other"/> equals to the instance value.
        /// </summary>
        /// <remarks>
        /// Ranges are the same if the have the same
        /// <see cref="Type">min</see> and <see cref="Type">max</see> values.
        /// </remarks>
        /// <param name="other"> The other <see cref="RangeI">Range</see> to compare for equality with
        /// this instance. </param>
        /// <returns> A Boolean data type. </returns>
        public bool Equals( RangeI other )
        {
            return other is object && Equals( this, other );
        }

        #endregion

        #region " OPERATORS "

        /// <summary> = casting operator. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  The left hand side item to compare for equality. </param>
        /// <param name="right"> The left hand side item to compare for equality. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( RangeI left, RangeI right )
        {
            return left is null ? right is null : !(right is null) && Equals( left, right );
        }

        /// <summary> &lt;&gt; casting operator. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  The left hand side item to compare for equality. </param>
        /// <param name="right"> The left hand side item to compare for equality. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( RangeI left, RangeI right )
        {
            return !Equals( left, right );
        }

        #endregion

        #region " METHODS "

        /// <summary> Clips the given value. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> A Date. </returns>
        public int Clip( int value )
        {
            return value < this.Min ? this.Min : value > this.Max ? this.Max : value;
        }

        /// <summary> Query if 'point' is inside the range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="point"> A <see cref="T:System.Integer">integer</see> point value&gt; </param>
        /// <returns> <c>true</c> if inside; otherwise <c>false</c> </returns>
        public bool Encloses( int point )
        {
            return point > this.Min && point < this.Max;
        }

        /// <summary> Returns true if the point value is within the range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="point"> A <see cref="T:System.Integer">integer</see> point value&gt; </param>
        /// <returns>
        /// <c>True</c> if value above or equal to minimum or below or equal to maximum.
        /// </returns>
        public bool Contains( int point )
        {
            return point >= this.Min && point <= this.Max;
        }

        /// <summary> Returns true if the point value is within the range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="point">     A <see cref="T:System.Integer">integer</see> point value&gt; </param>
        /// <param name="tolerance"> Tolerance for comparison. </param>
        /// <returns>
        /// <c>True</c> if value above or equal to minimum - tolerance or below or equal to maximum +
        /// tolerance.
        /// </returns>
        public bool Contains( int point, int tolerance )
        {
            return point >= this.Min - tolerance && point <= this.Max + tolerance;
        }

        /// <summary>
        /// Extend this RangeI to include both its present values and the specified range.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="range"> A <see cref="RangeI"/> value. </param>
        /// <returns> Extended range. </returns>
        public RangeI ExtendRange( RangeI range )
        {
            if ( range is null )
            {
                throw new ArgumentNullException( nameof( range ) );
            }

            if ( this.Min > range.Min )
            {
                this.Min = range.Min;
            }

            if ( this.Max < range.Max )
            {
                this.Max = range.Max;
            }

            return this;
        }

        /// <summary> Shift range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="value"> The value by which to shift the range. </param>
        public void ShiftRange( int value )
        {
            this.SetRange( value + this.Min, value + this.Max );
        }

        /// <summary> Transpose range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="shift"> The shift. </param>
        /// <param name="scale"> The scale. </param>
        public void TransposeRange( double shift, double scale )
        {
            this.SetRange( ( int ) (shift + scale * this.Min), ( int ) (shift + scale * this.Max) );
        }

        /// <summary> Sets the range based on the extrema. </summary>
        /// <remarks> Use this class to set the range. </remarks>
        /// <param name="minValue"> A <see cref="T:System.Integer">integer</see> expression that
        /// specifies the minimum value of the range. </param>
        /// <param name="maxValue"> A <see cref="T:System.Integer">integer</see> expression that
        /// specifies the maximum value of the range. </param>
        public void SetRange( int minValue, int maxValue )
        {
            this.Min = minValue;
            this.Max = maxValue;
        }

        /// <summary>
        /// Gets the exponent based on the range extremum values.  This is the
        /// <see cref="T:System.Integer">integer</see> value representing the exponent of
        /// the most significant digit of range limits.  For example, the 4 for 20,000 or -3 for 0.0012.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> The exponent. </returns>
        public int GetExponent()
        {
            return Convert.ToInt32( Math.Max( NumericExtensions.NumericExtensionMethods.Exponent( this.Min ), NumericExtensions.NumericExtensionMethods.Exponent( this.Max ) ) );
        }

        /// <summary>
        /// Gets the exponent based on the range extremum values.  This is the
        /// <see cref="T:System.Integer">integer</see> value representing the exponent of
        /// the most significant digit of range limits.  For example, the 4 for 20,000 or -3 for 0.0012.
        /// With engineering scales, the exponents are multiples of three, e.g., 20,000 yields +3 and
        /// 0.0001 -3.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="useEngineeringScale"> True to use scale exponent increments of 3. </param>
        /// <returns> The exponent. </returns>
        public int GetExponent( bool useEngineeringScale )
        {
            return Convert.ToInt32( Math.Max( NumericExtensions.NumericExtensionMethods.Exponent( this.Min, useEngineeringScale ), NumericExtensions.NumericExtensionMethods.Exponent( this.Max, useEngineeringScale ) ) );
        }

        /// <summary> Creates a unique hash code. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> An <see cref="T:System.Integer">integer</see> value. </returns>
        public override int GetHashCode()
        {
            return new Tuple<int, int>( this.Min.GetHashCode(), this.Max.GetHashCode() ).GetHashCode();
        }

        /// <summary> Returns the default string representation of the range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> A representation of the range, e.g., '(min,max)' . </returns>
        public override string ToString()
        {
            return $"({this.Min},{this.Max})";
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> Gets the maximum value of the range. </summary>
        /// <value> A <see cref="T:System.Integer">integer</see> property. </value>
        public int Max { get; set; }

        /// <summary> Returns the mid range point of the range. </summary>
        /// <value> A read only <see cref="T:System.Integer">integer</see> property. </value>
        public int Midrange => (this.Max + this.Min) / 2;

        /// <summary> Gets the minimum value of the range. </summary>
        /// <value> A <see cref="T:System.Integer">integer</see> property. </value>
        public int Min { get; set; }

        /// <summary> Gets the span of the range. </summary>
        /// <value> A <see cref="T:System.Integer">integer</see> </value>
        public int Span => this.Max - this.Min;

        #endregion

        #region " ATTRIBUTES "

        /// <summary> Returns true if the range is <see cref="RangeI.Empty"/>. </summary>
        /// <value> The is <see cref="RangeI.Empty"/>. </value>
        public bool IsEmpty => this.Equals( Empty );

        /// <summary> Returns true if the range is <see cref="RangeI.Unity"/>. </summary>
        /// <value> The is <see cref="RangeI.Unity"/>. </value>
        public bool IsUnity => this.Equals( Unity );

        #endregion


    }
}
