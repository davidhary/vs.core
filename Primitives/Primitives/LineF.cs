using System;

namespace isr.Core.Primitives
{

    /// <summary>
    /// Defines a <see cref="T:System.Single">Single</see> Line: y = slope * x + offset.
    /// </summary>
    /// <remarks>
    /// (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2005-09-19, 1.0.2088.x1. </para>
    /// </remarks>
    public class LineF
    {

        #region " SHARED "

        /// <summary> Gets a new instance of the unit Line. </summary>
        /// <value> A <see cref="LineF"/> value. </value>
        public static LineF Unity => new( 0f, 0f, 1f, 1f );

        /// <summary> Returns the calculates offset, i.e., the pressure at zero volts. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="zero">  Zero span values. </param>
        /// <param name="slope"> Full span values. </param>
        /// <returns> null if it fails, else the calculated offset. </returns>
        private static double ComputeOffset( PointF zero, double slope )
        {
            return zero is null ? throw new ArgumentNullException( nameof( zero ) ) : -slope * zero.X;
        }

        /// <summary> Returns the calculates slope as the ratio of pressure to voltage change. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="zero"> Zero span values. </param>
        /// <param name="full"> Full span values. </param>
        /// <returns> The calculated slope. </returns>
        private static double ComputeSlope( PointF zero, PointF full )
        {
            return zero is null
                ? throw new ArgumentNullException( nameof( zero ) )
                : full is null ? throw new ArgumentNullException( nameof( full ) ) : (full.Y - zero.Y) / (full.X - zero.X);
        }

        /// <summary> Transposes the (x,y) line to a (y,x) line. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="line"> Specifies the <see cref="LineF">Line</see>
        /// to transpose. </param>
        /// <returns> The transposed (y,x) line. </returns>
        public static LineF Transpose( LineF line )
        {
            return line is null
                ? throw new ArgumentNullException( nameof( line ) )
                : new LineF( line.Origin.Y, line.Origin.X, line.Insertion.Y, line.Insertion.X );
        }

        /// <summary> Computes the ordinate value for the given abscissa. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="abscissa"> The x coordinate. </param>
        /// <param name="offset">   A <see cref="T:System.Double">Double</see> expression that specifies
        /// the offset of the line. </param>
        /// <param name="slope">    Full span values. </param>
        /// <returns> A Single. </returns>
        public static float Ordinate( float abscissa, double offset, double slope )
        {
            return ( float ) (offset + abscissa * slope);
        }


        #endregion

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a unity size instance of the <see cref="LineF" /> class. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        public LineF() : this( PointF.Zero, PointF.Unity )
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LineF" /> class by its limits.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="x1"> A <see cref="T:System.Single">Single</see> expression that specifies the x
        /// element of the first (x,y) point defining the line. </param>
        /// <param name="y1"> A <see cref="T:System.Single">Single</see> expression that specifies the y
        /// element of the first (x,y) point defining the line. </param>
        /// <param name="x2"> A <see cref="T:System.Single">Single</see> expression that specifies the x
        /// element of the second (x,y) point defining the line. </param>
        /// <param name="y2"> A <see cref="T:System.Single">Single</see> expression that specifies the y
        /// element of the second (x,y) point defining the line. </param>
        public LineF( float x1, float y1, float x2, float y2 ) : this()
        {
            this.SetLine( x1, y1, x2, y2 );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LineF" /> class by its slope and offset.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="slope">  A <see cref="T:System.Double">Double</see> expression that specifies the
        /// slope of the line. </param>
        /// <param name="offset"> A <see cref="T:System.Double">Double</see> expression that specifies the
        /// offset of the line. </param>
        public LineF( double slope, double offset ) : this()
        {
            this.SetLine( slope, offset );
        }

        /// <summary>
        /// Constructs a <see cref="LineF"/> instance by its origin and insertion points.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="origin">    A <see cref="PointF">Point</see> expression that specifies the origin
        /// point of the line. </param>
        /// <param name="insertion"> A <see cref="PointF">Point</see> expression that specifies the
        /// insertion (end) point of the line. </param>
        public LineF( PointF origin, PointF insertion ) : base()
        {
            this.SetLine( origin, insertion );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LineF" /> class. The Copy Constructor.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="model"> The LineF object from which to copy. </param>
        public LineF( LineF model ) : this()
        {
            if ( model is object )
            {
                this.SetLine( model._Origin, model._Insertion );
            }
        }

        #endregion

        #region " EQUALS "

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
        /// <see cref="T:System.Object" />. </param>
        /// <returns>
        /// <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />; otherwise, <c>False</c>.
        /// </returns>
        public override bool Equals( object obj )
        {
            return this.Equals( obj as LineF );
        }

        /// <summary>
        /// Compares two lines. The lines are compared using the distance between their slope and offset.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="other"> Specifies the other <see cref="LineF">Line</see>
        /// to compare for equality with this instance. </param>
        /// <returns> A Boolean data type. </returns>
        public bool Equals( LineF other )
        {
            return other is object && (this.OffsetSlope.Equals( other.OffsetSlope ) || this.OffsetSlope.Hypotenuse( other.OffsetSlope ) < float.Epsilon);
        }

        /// <summary> Compares two lines. The lines are compared using their slopes and offsets. </summary>
        /// <remarks>
        /// The two lines are the same if the have the same minimum and maximum values.
        /// </remarks>
        /// <param name="other">     Specifies the <see cref="LineF">Line</see>
        /// to compare for equality with this instance. </param>
        /// <param name="tolerance"> Specifies the relative tolerance for comparing the two lines. The
        /// lines are compared based on their slope and offset.  The offset
        /// tolerance is based on it relative change from the Y range.  The
        /// tolerance if based on the reference line. </param>
        /// <returns> A Boolean data type. </returns>
        public bool Equals( LineF other, float tolerance )
        {
            return other is object && this.OffsetSlope.Hypotenuse( other.OffsetSlope ) < tolerance * (this.OffsetSlope.Hypotenuse() + other.OffsetSlope.Hypotenuse());
        }

        /// <summary> Compares two lines. The lines are compared using their slopes and offsets. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="other">     Specifies the <see cref="LineF">Line</see>
        /// to compare for equality with this instance. </param>
        /// <param name="tolerance"> Specifies the relative tolerance for comparing the two lines. The
        /// lines are compared based on the distance between their slope and
        /// offset relative to the slope and offset size. </param>
        /// <returns> A Boolean data type. </returns>
        public bool Equals( LineF other, double tolerance )
        {
            return other is object && this.OffsetSlope.Hypotenuse( other.OffsetSlope ) < tolerance * (this.OffsetSlope.Hypotenuse() + other.OffsetSlope.Hypotenuse());
        }

        /// <summary> Implements the operator =. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( LineF left, LineF right )
        {
            return ReferenceEquals( left, right ) || left is object && left.Equals( right );
        }

        /// <summary> Implements the operator &lt;&gt;. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( LineF left, LineF right )
        {
            return !ReferenceEquals( left, right ) && (left is null || !left.Equals( right ));
        }

        /// <summary> Creates a unique hash code. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> An <see cref="T:System.Int32">Int32</see> value. </returns>
        public override int GetHashCode()
        {
            return new Tuple<int, int>( this.Origin.GetHashCode(), this.Insertion.GetHashCode() ).GetHashCode();
        }

        #endregion

        #region " METHODS "

        /// <summary> Sets the Line based on the slope and offset values. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="slope">  A <see cref="T:System.Double">Double</see> expression that specifies the
        /// slope of the line. </param>
        /// <param name="offset"> A <see cref="T:System.Double">Double</see> expression that specifies the
        /// offset of the line. </param>
        public void SetLine( double slope, double offset )
        {
            this.SetLine( new PointF( 0f, Convert.ToSingle( offset ) ), new PointF( 1f, Convert.ToSingle( slope + offset ) ) );
        }

        /// <summary> Sets the Line based its origin and insertion points. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="origin">    A <see cref="PointF">Point</see> expression that specifies the origin
        /// point of the line. </param>
        /// <param name="insertion"> A <see cref="PointF">Point</see> expression that specifies the
        /// insertion (end) point of the line. </param>
        public void SetLine( PointF origin, PointF insertion )
        {
            if ( origin is null )
            {
                throw new ArgumentNullException( nameof( origin ) );
            }

            if ( insertion is null )
            {
                throw new ArgumentNullException( nameof( insertion ) );
            }

            this._Origin = new PointF( origin );
            this._Insertion = new PointF( insertion );
            double slope = ComputeSlope( origin, insertion );
            this.OffsetSlope = new PointF( ( float ) ComputeOffset( origin, slope ), ( float ) slope );
            this.Size = new PointF( insertion.X - origin.X, insertion.Y - origin.Y );
            this.Length = ( float ) this.Size.Hypotenuse();
        }

        /// <summary> Sets the Line based on the slope and offset values. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="x1">     A <see cref="T:System.Single">Single</see> expression that specifies the
        /// x element of the first (x,y) point defining the line. </param>
        /// <param name="x2">     A <see cref="T:System.Single">Single</see> expression that specifies the
        /// x element of the second (x,y) point defining the line. </param>
        /// <param name="slope">  A <see cref="T:System.Double">Double</see> expression that specifies the
        /// slope of the line. </param>
        /// <param name="offset"> A <see cref="T:System.Double">Double</see> expression that specifies the
        /// offset of the line. </param>
        public void SetLine( float x1, float x2, double slope, double offset )
        {
            this.SetLine( new PointF( x1, Convert.ToSingle( x1 * slope + offset ) ), new PointF( x2, Convert.ToSingle( x2 * slope + offset ) ) );
        }

        /// <summary> Sets the Line based on the slope and offset values. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="slope">  A <see cref="T:System.Double">Double</see> expression that specifies the
        /// slope of the line. </param>
        /// <param name="offset"> A <see cref="T:System.Double">Double</see> expression that specifies the
        /// offset of the line. </param>
        /// <param name="y1">     A <see cref="T:System.Single">Single</see> expression that specifies the
        /// x element of the first (x,y) point defining the line. </param>
        /// <param name="y2">     A <see cref="T:System.Single">Single</see> expression that specifies the
        /// x element of the second (x,y) point defining the line. </param>
        public void SetLine( double slope, double offset, float y1, float y2 )
        {
            if ( slope == 0d )
            {
                this.SetLine( new PointF( 0f, 0f ), new PointF( 1f, Convert.ToSingle( offset ) ) );
            }
            else
            {
                this.SetLine( new PointF( Convert.ToSingle( (y1 - offset) / slope ), y1 ), new PointF( Convert.ToSingle( (y2 - offset) / slope ), y2 ) );
            }
        }

        /// <summary> Sets the Line based on the values. </summary>
        /// <remarks> Use this class to set the line. </remarks>
        /// <param name="x1"> A <see cref="T:System.Single">Single</see> expression that specifies the x
        /// element of the first (x,y) point defining the line. </param>
        /// <param name="y1"> A <see cref="T:System.Single">Single</see> expression that specifies the y
        /// element of the first (x,y) point defining the line. </param>
        /// <param name="x2"> A <see cref="T:System.Single">Single</see> expression that specifies the x
        /// element of the second (x,y) point defining the line. </param>
        /// <param name="y2"> A <see cref="T:System.Single">Single</see> expression that specifies the y
        /// element of the second (x,y) point defining the line. </param>
        public void SetLine( float x1, float y1, float x2, float y2 )
        {
            this.SetLine( new PointF( x1, y1 ), new PointF( x2, y2 ) );
        }

        /// <summary> Returns the default string representation of the line. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> A representation of the line, e.g., '[(x1,y1)-(x2,y2)]' . </returns>
        public override string ToString()
        {
            return $"[({this.Origin.X},{this.Origin.Y})-({this.Insertion.X},{this.Insertion.Y})]";
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> Gets or sets the size. </summary>
        /// <value> The size. </value>
        public PointF Size { get; private set; }

        /// <summary> Gets or sets the length. </summary>
        /// <value> The length. </value>
        public double Length { get; private set; }

        /// <summary> The insertion. </summary>
        private PointF _Insertion;

        /// <summary> Gets or sets the origin point of the line. </summary>
        /// <value> A <see cref="PointF">Point</see> property. </value>
        public PointF Insertion
        {
            get => this._Insertion;

            set {
                if ( value is null )
                {
                    this._Insertion = null;
                }
                else if ( !value.Equals( this.Insertion ) )
                {
                    this.SetLine( this.Origin, value );
                }
            }
        }

        /// <summary> The origin. </summary>
        private PointF _Origin;

        /// <summary> Gets or sets the origin point of the line. </summary>
        /// <value> A <see cref="PointF">Point</see> property. </value>
        public PointF Origin
        {
            get => this._Origin;

            set {
                if ( value is null )
                {
                    this._Origin = null;
                }
                else if ( !value.Equals( this.Origin ) )
                {
                    this.SetLine( value, this.Insertion );
                }
            }
        }

        /// <summary> Gets the [offset,slope] as a <see cref="PointF"/> . </summary>
        /// <value> The slope offset. </value>
        public PointF OffsetSlope { get; private set; }

        /// <summary>
        /// Gets the Offset of the line y = slope * x + offset. This is the Y value at x
        /// = 0.
        /// </summary>
        /// <value> A <see cref="T:System.Int32">Int32</see> property. </value>
        public double Offset => this.OffsetSlope.X;

        /// <summary> Gets the Slope of the line y = slope * x + offset. </summary>
        /// <value> A <see cref="T:System.Int32">Int32</see> property. </value>
        public double Slope => this.OffsetSlope.Y;

        /// <summary> Returns the X line value for the give Y value. </summary>
        /// <value> The x coordinate. </value>
        public float X( float y )
        {
            return this.Slope != 0d ? Convert.ToSingle( (y - this.Offset) / this.Slope ) : Convert.ToSingle( this.Offset );
        }

        /// <summary> Returns the Y line value for the give X value. </summary>
        /// <value> The y coordinate. </value>
        public float Y( float x )
        {
            return Ordinate( x, this.Offset, this.Slope );
        }

        #endregion

    }
}
