using System;
using System.Collections.Generic;
using System.Linq;

namespace isr.Core.Primitives
{

    /// <summary>
    /// Defines a <see cref="T:System.DateTimeOffset">DateTimeOffset</see> range class.
    /// </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2004-04-30, 1.0.1581 </para>
    /// </remarks>
    public class RangeDateTimeOffset
    {

        #region " SHARED "

        /// <summary> Gets a new instance of the empty range. </summary>
        /// <value> A <see cref="T:System.DateTimeOffset.MinValue"/> value with
        /// <see cref="T:System.DateTimeOffset.MinValue"/>
        /// minimum value and <see cref="T:System.DateTimeOffset.MinValue"/> for the maximum value. </value>
        public static RangeDateTimeOffset Empty => new( DateTimeOffset.MinValue, DateTimeOffset.MinValue );

        /// <summary> Gets a new instance of the Unity range. </summary>
        /// <value> A <see cref="RangeDateTimeOffset"/> [0,1] value. </value>
        public static RangeDateTimeOffset Unity => new( DateTimeOffset.Now, DateTimeOffset.Now.Add( TimeSpan.FromSeconds( 1d ) ) );

        /// <summary> Gets a new instance of the zero range value. </summary>
        /// <value> A <see cref="RangeDateTimeOffset"/> value. </value>
        public static RangeDateTimeOffset Zero => new( DateTimeOffset.Now, DateTimeOffset.Now );

        /// <summary> Return the range of the specified data array. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="values"> The data array. </param>
        /// <returns> The calculated range. </returns>
        public static RangeDateTimeOffset GetRange( IEnumerable<DateTimeOffset> values )
        {
            // return the unit range if no data
            if ( values is null || !values.Any() )
            {
                return Unity;
            }

            var min = DateTimeOffset.MaxValue;
            var max = DateTimeOffset.MinValue;
            foreach ( DateTimeOffset temp in values )
            {
                if ( temp < min )
                {
                    min = temp;
                }
                else if ( temp > max )
                {
                    max = temp;
                }
            }

            return new RangeDateTimeOffset( min, max );
        }

        /// <summary> Extended range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="range"> A <see cref="RangeDateTimeOffset"/> value. </param>
        /// <returns> A RangeDateTimeOffset. </returns>
        public RangeDateTimeOffset ExtendedRange( RangeDateTimeOffset range )
        {
            var result = new RangeDateTimeOffset( this );
            _ = result.ExtendRange( range );
            return result;
        }

        /// <summary> Shifted range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> A RangeDateTimeOffset. </returns>
        public RangeDateTimeOffset ShiftedRange( TimeSpan value )
        {
            var result = new RangeDateTimeOffset( this );
            result.ShiftRange( value );
            return result;
        }

        /// <summary> Transposed range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="shift"> The shift. </param>
        /// <param name="scale"> The scale. </param>
        /// <returns> A RangeDateTimeOffset. </returns>
        public RangeDateTimeOffset TransposedRange( TimeSpan shift, double scale )
        {
            var result = new RangeDateTimeOffset( this );
            result.TransposeRange( shift, scale );
            return result;
        }

        #endregion

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs a <see cref="RangeDateTimeOffset"/> instance by its limits. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="minValue"> A <see cref="T:System.DateTimeOffset">DateTimeOffset</see> expression
        /// that specifics the minimum range. </param>
        /// <param name="maxValue"> A <see cref="T:System.DateTimeOffset">DateTimeOffset</see> expression
        /// that specifics the maximum range. </param>
        public RangeDateTimeOffset( DateTimeOffset minValue, DateTimeOffset maxValue ) : base()
        {
            this.SetRange( minValue, maxValue );
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="model"> The RangeDateTimeOffset object from which to copy. </param>
        public RangeDateTimeOffset( RangeDateTimeOffset model ) : base()
        {
            if ( model is object )
            {
                this.SetRange( model.Min, model.Max );
            }
        }

        #endregion

        #region " EQUALS "

        /// <summary> = casting operator. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  The left hand side item to compare for equality. </param>
        /// <param name="right"> The left hand side item to compare for equality. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( RangeDateTimeOffset left, RangeDateTimeOffset right )
        {
            return left is null ? right is null : !(right is null) && Equals( left, right );
        }

        /// <summary> &lt;&gt; casting operator. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  The left hand side item to compare for equality. </param>
        /// <param name="right"> The left hand side item to compare for equality. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( RangeDateTimeOffset left, RangeDateTimeOffset right )
        {
            return !Equals( left, right );
        }

        /// <summary> Tests if two RangeDateTimeOffset objects are considered equal. </summary>
        /// <remarks>
        /// Range Date Times are the same if the have the same
        /// <see cref="Type">min</see> and <see cref="Type">max</see> values.
        /// </remarks>
        /// <param name="left">  The left hand side item to compare for equality. </param>
        /// <param name="right"> The left hand side item to compare for equality. </param>
        /// <returns> <c>True</c> if the objects are considered equal, false if they are not. </returns>
        public static bool Equals( RangeDateTimeOffset left, RangeDateTimeOffset right )
        {
            return left is null ? right is null : !(right is null) && left.Max.Equals( right.Max ) && left.Min.Equals( right.Min );
        }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
        /// <see cref="T:System.Object" />. </param>
        /// <returns>
        /// <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />; otherwise, <c>False</c>.
        /// </returns>
        public override bool Equals( object obj )
        {
            return this.Equals( obj as RangeDateTimeOffset );
        }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="other"> The other range to compare to this object. </param>
        /// <returns>
        /// <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />; otherwise, <c>False</c>.
        /// </returns>
        public bool Equals( RangeDateTimeOffset other )
        {
            return other is object && Equals( this, other );
        }

        #endregion

        #region " METHODS "

        /// <summary> Clips the given value. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> A Date. </returns>
        public DateTimeOffset Clip( DateTimeOffset value )
        {
            return value < this.Min ? this.Min : value > this.Max ? this.Max : value;
        }

        /// <summary> Query if 'point' is inside the range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="point"> A <see cref="T:System.DateTimeOffset">Date</see> point value&gt; </param>
        /// <returns> <c>true</c> if inside; otherwise <c>false</c> </returns>
        public bool Encloses( DateTimeOffset point )
        {
            return point > this.Min && point < this.Max;
        }

        /// <summary> Returns true if the point value is within the range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="point"> A <see cref="DateTime">Date</see> point value&gt; </param>
        /// <returns>
        /// <c>True</c> if value above or equal to minimum or below or equal to maximum.
        /// </returns>
        public bool Contains( DateTimeOffset point )
        {
            return point >= this.Min && point <= this.Max;
        }

        /// <summary> Returns true if the point value is within the range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="point">     A <see cref="DateTime">Date</see> point value&gt; </param>
        /// <param name="tolerance"> Tolerance for comparison. </param>
        /// <returns>
        /// <c>True</c> if value above or equal to minimum - tolerance or below or equal to maximum +
        /// tolerance.
        /// </returns>
        public bool Contains( DateTimeOffset point, DateTimeOffset tolerance )
        {
            return point.Subtract( this.Min.Subtract( tolerance ) ).Ticks >= 0L && this.Max.Subtract( point.Subtract( tolerance ) ).Ticks >= 0L;
        }

        /// <summary>
        /// Extend this range to include both its present values and the specified range.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="range"> A <see cref="RangeDateTimeOffset"/> value. </param>
        /// <returns> Extended range. </returns>
        public RangeDateTimeOffset ExtendRange( RangeDateTimeOffset range )
        {
            if ( range is null )
            {
                throw new ArgumentNullException( nameof( range ) );
            }

            if ( this.Min > range.Min )
            {
                this.Min = range.Min;
            }

            if ( this.Max < range.Max )
            {
                this.Max = range.Max;
            }

            return this;
        }

        /// <summary> Shift range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="value"> The value by which to shift the range. </param>
        public void ShiftRange( TimeSpan value )
        {
            this.SetRange( this.Min.Add( value ), this.Max.Add( value ) );
        }

        /// <summary> Transpose range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="shift"> The shift. </param>
        /// <param name="scale"> The scale. </param>
        public void TransposeRange( TimeSpan shift, double scale )
        {
            var newSpan = TimeSpan.FromTicks( ( long ) (scale * this.Span.Ticks) );
            var newMin = this.Min.Add( shift );
            this.SetRange( newMin, newMin.Add( newSpan ) );
        }

        /// <summary> Sets the range based on the extrema. </summary>
        /// <remarks> Use this class to set the range. </remarks>
        /// <param name="minValue"> A <see cref="T:System.DateTimeOffset">DateTimeOffset</see> expression
        /// that specifies the minimum value of the range. </param>
        /// <param name="maxValue"> A <see cref="T:System.DateTimeOffset">DateTimeOffset</see> expression
        /// that specifies the maximum value of the range. </param>
        public void SetRange( DateTimeOffset minValue, DateTimeOffset maxValue )
        {
            this.Min = minValue;
            this.Max = maxValue;
        }

        /// <summary> Creates a unique hash code. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> An <see cref="T:System.Integer">integer</see> value. </returns>
        public override int GetHashCode()
        {
            return new Tuple<int, int>( this.Min.GetHashCode(), this.Max.GetHashCode() ).GetHashCode();
        }

        /// <summary> Returns the default string representation of the range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> A representation of the range, e.g., '(min,max)' . </returns>
        public override string ToString()
        {
            return $"({this.Min},{this.Max})";
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> Gets the maximum value of the range. </summary>
        /// <value> A <see cref="T:System.DateTimeOffset">DateTimeOffset</see> property. </value>
        public DateTimeOffset Max { get; set; }

        /// <summary> Gets the minimum value of the range. </summary>
        /// <value> A <see cref="T:System.DateTimeOffset">DateTimeOffset</see> property. </value>
        public DateTimeOffset Min { get; set; }

        /// <summary> Gets the span of the range. </summary>
        /// <value> A <see cref="T:System.TimeSpan"/> property. </value>
        public TimeSpan Span => this.Max.Subtract( this.Min );

        #endregion

        #region " ATTRIBUTES "

        /// <summary> Returns true if the range is <see cref="RangeDateTimeOffset.Empty"/>. </summary>
        /// <value> The is <see cref="RangeDateTimeOffset.Empty"/>. </value>
        public bool IsEmpty => this.Equals( Empty );

        /// <summary> Returns true if the range is <see cref="RangeDateTimeOffset.Unity"/>. </summary>
        /// <value> The is <see cref="RangeDateTimeOffset.Unity"/>. </value>
        public bool IsUnity => this.Equals( Unity );

        #endregion


    }
}
