using System;

namespace isr.Core.Primitives
{

    /// <summary> Defines a <see cref="T:System.Single">Single</see> point. </summary>
    /// <remarks>
    /// (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2005-09-07, 1.0.2077 </para>
    /// </remarks>
    public class PointF
    {

        #region " SHARED "

        /// <summary> Gets a new instance of the zero point value. </summary>
        /// <value> A <see cref="PointF"/> value. </value>
        public static PointF Zero => new( 0f, 0f );

        /// <summary> Gets a new instance of the Unity [1,1] point value. </summary>
        /// <value> A <see cref="PointF"/> value. </value>
        public static PointF Unity => new( 1f, 1f );

        #endregion

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs a <see cref="PointF"/> instance by its limits. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="x"> A <see cref="T:System.Single">Single</see> expression that specifics the x
        /// element. </param>
        /// <param name="y"> A <see cref="T:System.Single">Single</see> expression that specifics the y
        /// element. </param>
        public PointF( float x, float y ) : base()
        {
            this.SetPointThis( x, y );
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="model"> The PointF object from which to copy. </param>
        public PointF( PointF model ) : base()
        {
            if ( model is object )
            {
                this.SetPointThis( model.X, model.Y );
            }
        }

        #endregion

        #region " EQUALS "

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
        /// <see cref="T:System.Object" />. </param>
        /// <returns>
        /// <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />; otherwise, <c>False</c>.
        /// </returns>
        public override bool Equals( object obj )
        {
            return this.Equals( obj as PointF );
        }

        /// <summary> Compares two ranges. </summary>
        /// <remarks>
        /// The two ranges are the same if the have the same minimum and maximum values.
        /// </remarks>
        /// <param name="other"> The other point to compare to this object. </param>
        /// <returns> A Boolean data type. </returns>
        public bool Equals( PointF other )
        {
            return other is object && other.X.Equals( this.X ) & other.Y.Equals( this.Y );
        }

        /// <summary> Implements the operator =. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( PointF left, PointF right )
        {
            return ReferenceEquals( left, right ) || left is object && left.Equals( right );
        }

        /// <summary> Implements the operator &lt;&gt;. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( PointF left, PointF right )
        {
            return !ReferenceEquals( left, right ) && (left is null || !left.Equals( right ));
        }

        /// <summary> Creates a unique hash code. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> An <see cref="T:System.Single">Single</see> value. </returns>
        public override int GetHashCode()
        {
            return new Tuple<int, int>( this.X.GetHashCode(), this.Y.GetHashCode() ).GetHashCode();
        }

        #endregion

        #region " METHODS "

        /// <summary>
        /// Gets the exponent based on the point extremum values.  This is the
        /// <see cref="T:System.Single">Single</see> value representing the exponent of
        /// the most significant digit of point values.  For example, the 4 for 20,000 or -3 for 0.0012.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> The exponent. </returns>
        public float GetExponent()
        {
            return Convert.ToInt32( Math.Max( NumericExtensions.NumericExtensionMethods.Exponent( this.X ), NumericExtensions.NumericExtensionMethods.Exponent( this.Y ) ) );
        }

        /// <summary>
        /// Gets the exponent based on the point values.  This is the
        /// <see cref="T:System.Single">Single</see> value representing the exponent of
        /// the most significant digit of point values limits.  For example, the 4 for 20,000 or -3 for
        /// 0.0012.  With engineering scales, the exponents are multiples of three, e.g., 20,000 yields
        /// +3 and 0.0001 -3.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="useEngineeringScale"> True to use scale exponent increments of 3. </param>
        /// <returns> The exponent. </returns>
        public float GetExponent( bool useEngineeringScale )
        {
            return Convert.ToInt32( Math.Max( NumericExtensions.NumericExtensionMethods.Exponent( this.X, useEngineeringScale ), NumericExtensions.NumericExtensionMethods.Exponent( this.Y, useEngineeringScale ) ) );
        }

        /// <summary> Sets the point based on the values. </summary>
        /// <remarks> Use this class to set the point. </remarks>
        /// <param name="x"> A <see cref="T:System.Single">Single</see> expression that specifies the x
        /// value of the point. </param>
        /// <param name="y"> A <see cref="T:System.Single">Single</see> expression that specifies the y
        /// value of the point. </param>
        private void SetPointThis( float x, float y )
        {
            if ( !this.X.Equals( x ) )
            {
                this.X = x;
            }

            if ( !this.Y.Equals( y ) )
            {
                this.Y = y;
            }
        }

        /// <summary> Sets the point based on the values. </summary>
        /// <remarks> Use this class to set the point. </remarks>
        /// <param name="x"> A <see cref="T:System.Single">Single</see> expression that specifies the x
        /// value of the point. </param>
        /// <param name="y"> A <see cref="T:System.Single">Single</see> expression that specifies the y
        /// value of the point. </param>
        public void SetPoint( float x, float y )
        {
            this.X = x;
            this.Y = y;
        }

        /// <summary> Returns the default string representation of the point. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> A representation of the point, e.g., '(x,y)' . </returns>
        public override string ToString()
        {
            return $"({this.X},{this.Y})";
        }

        /// <summary>   Computes the distance between two points. </summary>
        /// <remarks>
        /// <para>The length is computed accurately, even in cases where x<sup>2</sup> or y<sup>2</sup>
        /// would overflow.</para>
        /// </remarks>
        /// <param name="right">    Specifies the right hand side argument of the binary operation. </param>
        /// <returns>   A double. </returns>
        public double Hypotenuse( PointF right )
        {
            return GeometricMethods.Hypotenuse( right.X - this.X, right.Y - this.Y );
        }

        /// <summary>   Computes the distance between this point and the origin. </summary>
        /// <remarks>
        /// <para>The length is computed accurately, even in cases where x<sup>2</sup> or y<sup>2</sup>
        /// would overflow.</para>
        /// </remarks>
        /// <returns>   A double. </returns>
        public double Hypotenuse()
        {
            return GeometricMethods.Hypotenuse( this.X, this.Y );
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> Gets the x value of the point. </summary>
        /// <value> A <see cref="T:System.Single">Single</see> property. </value>
        public float X { get; set; }

        /// <summary> Gets the y value of the point. </summary>
        /// <value> A <see cref="T:System.Single">Single</see> property. </value>
        public float Y { get; set; }

        #endregion

        #region " ATTRIBUTES "

        /// <summary> Returns true if the range is <see cref="PointF.Zero"/>. </summary>
        /// <value> The is <see cref="PointF.Zero"/>. </value>
        public bool IsZero => this.Equals( Zero );

        /// <summary> Returns true if the range is <see cref="PointF.Unity"/>. </summary>
        /// <value> The is <see cref="PointF.Unity"/>. </value>
        public bool IsUnity => this.Equals( Unity );

        #endregion

    }

    internal static partial class GeometricMethods
    {

        /// <summary> Hypotenuses. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="value">      The value. </param>
        /// <param name="orthogonal"> The orthogonal. </param>
        /// <returns> A Double. </returns>
        public static double Hypotenuse( float value, float orthogonal )
        {
            if ( value == 0.0d && orthogonal == 0.0d )
            {
                return 0.0d;
            }
            else
            {
                double ax = Math.Abs( value );
                double ay = Math.Abs( orthogonal );
                if ( ax > ay )
                {
                    double r = orthogonal / value;
                    return ax * Math.Sqrt( 1.0d + r * r );
                }
                else
                {
                    double r = value / orthogonal;
                    return ay * Math.Sqrt( 1.0d + r * r );
                }
            }
        }
    }

}
