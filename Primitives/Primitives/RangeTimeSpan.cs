using System;
using System.Collections.Generic;
using System.Linq;

namespace isr.Core.Primitives
{

    /// <summary> Defines a <see cref="T:System.TimeSpan">TimeSpan</see> range class. </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2004-04-30, 1.0.1581. Create. </para>
    /// </remarks>
    public class RangeTimeSpan
    {

        #region " SHARED "

        /// <summary> Gets the empty range. </summary>
        /// <value> A <see cref="T:System.TimeSpan.MinValue"/> value with
        /// <see cref="T:System.TimeSpan.MinValue"/>
        /// minimum value and <see cref="T:System.TimeSpan.MinValue"/> for the maximum value. </value>
        public static RangeTimeSpan Empty => new( TimeSpan.MinValue, TimeSpan.MinValue );

        /// <summary> Gets the Unity range. </summary>
        /// <value> A <see cref="RangeTimeSpan"/> [0,1] value. </value>
        public static RangeTimeSpan Unity => new( TimeSpan.Zero, TimeSpan.FromSeconds( 1d ) );

        /// <summary> Gets the zero range value. </summary>
        /// <value> A <see cref="RangeTimeSpan"/> value. </value>
        public static RangeTimeSpan Zero => new( TimeSpan.Zero, TimeSpan.Zero );

        /// <summary> Return the range of the specified data array. </summary>
        /// <remarks> David, 2020-09-05. </remarks>
        /// <param name="values"> The data array. </param>
        /// <returns> The calculated range. </returns>
        public static RangeTimeSpan GetRange( TimeSpan[] values )
        {
            // return the unit range if no data
            if ( values is null || !values.Any() )
            {
                return Unity;
            }

            var min = TimeSpan.MaxValue;
            var max = TimeSpan.MinValue;
            foreach ( TimeSpan temp in values )
            {
                if ( temp < min )
                {
                    min = temp;
                }
                else if ( temp > max )
                {
                    max = temp;
                }
            }

            return new RangeTimeSpan( min, max );
        }

        /// <summary> Return the range of the specified data array. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="values"> The data array. </param>
        /// <returns> The calculated range. </returns>
        public static RangeTimeSpan GetRange( IEnumerable<TimeSpan> values )
        {
            // return the unit range if no data
            if ( values is null || !values.Any() )
            {
                return Unity;
            }

            var min = TimeSpan.MaxValue;
            var max = TimeSpan.MinValue;
            foreach ( TimeSpan temp in values )
            {
                if ( temp < min )
                {
                    min = temp;
                }
                else if ( temp > max )
                {
                    max = temp;
                }
            }

            return new RangeTimeSpan( min, max );
        }

        /// <summary> Return the range of the specified data array. </summary>
        /// <remarks> David, 2020-09-05. </remarks>
        /// <param name="values"> The data array. </param>
        /// <returns> The calculated range. </returns>
        public static RangeTimeSpan GetRange( IList<TimeSpan> values )
        {
            // return the unit range if no data
            if ( values is null || !values.Any() )
            {
                return Unity;
            }

            var min = TimeSpan.MaxValue;
            var max = TimeSpan.MinValue;
            foreach ( TimeSpan temp in values )
            {
                if ( temp < min )
                {
                    min = temp;
                }
                else if ( temp > max )
                {
                    max = temp;
                }
            }

            return new RangeTimeSpan( min, max );
        }

        /// <summary> Extended range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="range"> A <see cref="RangeTimeSpan"/> value. </param>
        /// <returns> A RangeTimeSpan. </returns>
        public RangeTimeSpan ExtendedRange( RangeTimeSpan range )
        {
            var result = new RangeTimeSpan( this );
            _ = result.ExtendRange( range );
            return result;
        }

        /// <summary> Shifted range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> A RangeTimeSpan. </returns>
        public RangeTimeSpan ShiftedRange( TimeSpan value )
        {
            var result = new RangeTimeSpan( this );
            result.ShiftRange( value );
            return result;
        }

        /// <summary> Transposed range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="shift"> The shift. </param>
        /// <param name="scale"> The scale. </param>
        /// <returns> A RangeTimeSpan. </returns>
        public RangeTimeSpan TransposedRange( TimeSpan shift, double scale )
        {
            var result = new RangeTimeSpan( this );
            result.TransposeRange( shift, scale );
            return result;
        }

        #endregion

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs a <see cref="RangeTimeSpan"/> instance by its limits. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="minValue"> A <see cref="T:System.TimeSpan">TimeSpan</see> expression that
        /// specifics the minimum range. </param>
        /// <param name="maxValue"> A <see cref="T:System.TimeSpan">TimeSpan</see> expression that
        /// specifics the maximum range. </param>
        public RangeTimeSpan( TimeSpan minValue, TimeSpan maxValue ) : base()
        {
            this.SetRange( minValue, maxValue );
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="model"> The RangeTimeSpan object from which to copy. </param>
        public RangeTimeSpan( RangeTimeSpan model ) : base()
        {
            if ( model is object )
            {
                this.SetRange( model.Min, model.Max );
            }
        }

        #endregion

        #region " EQUALS "

        /// <summary> = casting operator. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  The left hand side item to compare for equality. </param>
        /// <param name="right"> The left hand side item to compare for equality. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( RangeTimeSpan left, RangeTimeSpan right )
        {
            return left is null ? right is null : !(right is null) && Equals( left, right );
        }

        /// <summary> &lt;&gt; casting operator. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  The left hand side item to compare for equality. </param>
        /// <param name="right"> The left hand side item to compare for equality. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( RangeTimeSpan left, RangeTimeSpan right )
        {
            return !Equals( left, right );
        }

        /// <summary> Returns True if equal. </summary>
        /// <remarks>
        /// Range Time Spans are the same if the have the same
        /// <see cref="Type">min</see> and <see cref="Type">max</see> values.
        /// </remarks>
        /// <param name="left">  The left hand side item to compare for equality. </param>
        /// <param name="right"> The left hand side item to compare for equality. </param>
        /// <returns> <c>True</c> if equals. </returns>
        public static bool Equals( RangeTimeSpan left, RangeTimeSpan right )
        {
            return left is null ? right is null : !(right is null) && left.Max.Equals( right.Max ) && left.Min.Equals( right.Min );
        }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
        /// <see cref="T:System.Object" />. </param>
        /// <returns>
        /// <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />; otherwise, <c>False</c>.
        /// </returns>
        public override bool Equals( object obj )
        {
            return this.Equals( obj as RangeTimeSpan );
        }

        /// <summary>
        /// Returns True if the value of the <paramref name="other"/> equals to the instance value.
        /// </summary>
        /// <remarks>
        /// Range Time Spans are the same if the have the same
        /// <see cref="Type">min</see> and <see cref="Type">max</see> values.
        /// </remarks>
        /// <param name="other"> The other <see cref="RangeTimeSpan">Range Time Span</see> to compare for
        /// equality with this instance. </param>
        /// <returns> A Boolean data type. </returns>
        public bool Equals( RangeTimeSpan other )
        {
            return other is object && Equals( this, other );
        }

        #endregion

        #region " METHODS "

        /// <summary> Clips the given value. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> A Date. </returns>
        public TimeSpan Clip( TimeSpan value )
        {
            return value < this.Min ? this.Min : value > this.Max ? this.Max : value;
        }

        /// <summary> Returns true if the point value is within the range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="point"> A <see cref="T:System.Date">Date</see> point value&gt; </param>
        /// <returns>
        /// <c>True</c> if value above or equal to minimum or below or equal to maximum.
        /// </returns>
        public bool Contains( TimeSpan point )
        {
            return point.CompareTo( this.Min ) >= 0 && point.CompareTo( this.Max ) <= 0;
        }

        /// <summary> Query if 'point' is inside the range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="point"> A <see cref="T:System.TimeSpan">Time Span</see> point value&gt; </param>
        /// <returns> <c>true</c> if inside; otherwise <c>false</c> </returns>
        public bool Encloses( TimeSpan point )
        {
            return point > this.Min && point < this.Max;
        }

        /// <summary> Returns true if the point value is within the range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="point">     A <see cref="T:System.Date">Date</see> point value&gt; </param>
        /// <param name="tolerance"> Tolerance for comparison. </param>
        /// <returns>
        /// <c>True</c> if value above or equal to minimum - tolerance or below or equal to maximum +
        /// tolerance.
        /// </returns>
        public bool Contains( TimeSpan point, TimeSpan tolerance )
        {
            return point.Subtract( this.Min.Subtract( tolerance ) ).Ticks >= 0L && this.Max.Subtract( point.Subtract( tolerance ) ).Ticks >= 0L;
        }

        /// <summary>
        /// Extend this range to include both its present values and the specified range.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="range"> A <see cref="RangeTimeSpan"/> value. </param>
        /// <returns> Extended range. </returns>
        public RangeTimeSpan ExtendRange( RangeTimeSpan range )
        {
            if ( range is null )
            {
                throw new ArgumentNullException( nameof( range ) );
            }

            if ( this.Min.CompareTo( range.Min ) > 0 )
            {
                this.Min = range.Min;
            }

            if ( this.Max.CompareTo( range.Max ) < 0 )
            {
                this.Max = range.Max;
            }

            return this;
        }

        /// <summary> Shift range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="value"> The value by which to shift the range. </param>
        public void ShiftRange( TimeSpan value )
        {
            this.SetRange( this.Min.Add( value ), this.Max.Add( value ) );
        }

        /// <summary> Transpose range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="shift"> The shift. </param>
        /// <param name="scale"> The scale. </param>
        public void TransposeRange( TimeSpan shift, double scale )
        {
            var newSpan = TimeSpan.FromTicks( ( long ) (scale * this.Span.Ticks) );
            var newMin = this.Min.Add( shift );
            this.SetRange( newMin, newMin.Add( newSpan ) );
        }

        /// <summary> Sets the range based on the extrema. </summary>
        /// <remarks> Use this class to set the range. </remarks>
        /// <param name="minValue"> A <see cref="T:System.TimeSpan">TimeSpan</see> expression that
        /// specifies the minimum value of the range. </param>
        /// <param name="maxValue"> A <see cref="T:System.TimeSpan">TimeSpan</see> expression that
        /// specifies the maximum value of the range. </param>
        public void SetRange( TimeSpan minValue, TimeSpan maxValue )
        {
            this.Min = minValue;
            this.Max = maxValue;
        }

        /// <summary> Creates a unique hash code. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> An <see cref="T:System.Integer">integer</see> value. </returns>
        public override int GetHashCode()
        {
            return new Tuple<int, int>( this.Min.GetHashCode(), this.Max.GetHashCode() ).GetHashCode();
        }

        /// <summary> Returns the default string representation of the range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> A representation of the range, e.g., '[x,y]' . </returns>
        public override string ToString()
        {
            return $"[{this.Min},{this.Max}]";
        }

        /// <summary> Returns the default string representation of the range. </summary>
        /// <remarks> David, 2020-04-13. </remarks>
        /// <param name="format"> Describes the format to use, e.g., '"d\.hh\:mm\:ss\.fff"'. </param>
        /// <returns> A representation of the range, e.g., '[x,y]' . </returns>
        public string ToString( string format )
        {
            return $"[{this.Min.ToString( format )},{this.Max.ToString( format )}]";
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> Gets the maximum value of the range. </summary>
        /// <value> A <see cref="T:System.TimeSpan">TimeSpan</see> property. </value>
        public TimeSpan Max { get; set; }

        /// <summary> Gets the minimum value of the range. </summary>
        /// <value> A <see cref="T:System.TimeSpan">TimeSpan</see> property. </value>
        public TimeSpan Min { get; set; }

        /// <summary> Gets the Span value of the range. </summary>
        /// <value> A <see cref="T:System.TimeSpan"/> property. </value>
        public TimeSpan Span => this.Max.Subtract( this.Min );

        #endregion

        #region " ATTRIBUTES "

        /// <summary> Returns true if the range is <see cref="RangeTimeSpan.Empty"/>. </summary>
        /// <value> The is <see cref="RangeTimeSpan.Empty"/>. </value>
        public bool IsEmpty => this.Equals( Empty );

        /// <summary> Returns true if the range is <see cref="RangeTimeSpan.Unity"/>. </summary>
        /// <value> The is <see cref="RangeTimeSpan.Unity"/>. </value>
        public bool IsUnity => this.Equals( Unity );

        #endregion


    }
}
