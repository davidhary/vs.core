using System;
using System.Collections.Generic;
using System.Linq;

namespace isr.Core.Primitives
{

    /// <summary> Defines a <see cref="T:System.Single">Single</see> range class. </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2004-04-30, 1.0.1581 </para>
    /// </remarks>
    public class RangeF
    {

        #region " SHARED "

        /// <summary> Gets a new instance of the empty range. </summary>
        /// <value> A <see cref="RangeF"/> value with [0,0] for the [Min,Max] values. </value>
        public static RangeF Empty => new( 0f, 0f );

        /// <summary> Gets a new instance of the full range. </summary>
        public static RangeF Full => new( ( float ) double.MinValue, ( float ) double.MaxValue );

        /// <summary> Gets a new instance of the full non-negative range. </summary>
        public static RangeF FullNonnegative => new( 0f, ( float ) double.MaxValue );

        /// <summary> Gets a new instance of the Unity range. </summary>
        /// <value> A <see cref="RangeF"/> [0,1] value. </value>
        public static RangeF Unity => new( 0f, 1f );

        /// <summary> Gets a new instance of the zero range. </summary>
        /// <value> The zero. </value>
        public static RangeF Zero => new( 0f, 0f );

        /// <summary> Return the range of the specified data array. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="values"> The data array. </param>
        /// <returns> The calculated range. </returns>
        public static RangeF GetRange( IEnumerable<float> values )
        {
            // return the unit range if no data
            if ( values is null || !values.Any() )
            {
                return Unity;
            }

            float min = float.MaxValue;
            float max = float.MinValue;
            foreach ( float temp in values )
            {
                if ( temp < min )
                {
                    min = temp;
                }
                else if ( temp > max )
                {
                    max = temp;
                }
            }

            return new RangeF( min, max );
        }

        /// <summary> Extended range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="range"> A <see cref="RangeF"/> value. </param>
        /// <returns> A RangeF. </returns>
        public RangeF ExtendedRange( RangeF range )
        {
            var result = new RangeF( this );
            result.ExtendRange( range );
            return result;
        }

        /// <summary> Shifted range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> A RangeF. </returns>
        public RangeF ShiftedRange( float value )
        {
            var result = new RangeF( this );
            result.ShiftRange( value );
            return result;
        }

        /// <summary> Transposed range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="shift"> The shift. </param>
        /// <param name="scale"> The scale. </param>
        /// <returns> A RangeF. </returns>
        public RangeF TransposedRange( float shift, float scale )
        {
            var result = new RangeF( this );
            result.TransposeRange( shift, scale );
            return result;
        }

        #endregion

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs a <see cref="RangeR"/> instance by its span. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="span"> A <see cref="T:System.Single">Single</see> property. </param>
        public RangeF( float span ) : this( ( float ) (-0.5d * span), ( float ) (0.5d * span) )
        {
        }

        /// <summary> Constructs a <see cref="RangeF"/> instance by its limits. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="minValue"> A <see cref="T:System.Single">Single</see> expression that specifics
        /// the minimum range. </param>
        /// <param name="maxValue"> A <see cref="T:System.Single">Single</see> expression that specifics
        /// the maximum range. </param>
        public RangeF( float minValue, float maxValue ) : base()
        {
            this.SetRange( minValue, maxValue );
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="model"> The RangeF object from which to copy. </param>
        public RangeF( RangeF model ) : base()
        {
            if ( model is object )
            {
                this.SetRange( model.Min, model.Max );
            }
        }

        #endregion

        #region " EQUALS "

        /// <summary> = casting operator. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  The left hand side item to compare for equality. </param>
        /// <param name="right"> The left hand side item to compare for equality. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( RangeF left, RangeF right )
        {
            return left is null ? right is null : !(right is null) && Equals( left, right );
        }

        /// <summary> &lt;&gt; casting operator. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  The left hand side item to compare for equality. </param>
        /// <param name="right"> The left hand side item to compare for equality. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( RangeF left, RangeF right )
        {
            return !Equals( left, right );
        }

        /// <summary> Returns True if equal. </summary>
        /// <remarks>
        /// Ranges are the same if the have the same
        /// <see cref="Type">min</see> and <see cref="Type">max</see> values.
        /// </remarks>
        /// <param name="left">  The left hand side item to compare for equality. </param>
        /// <param name="right"> The left hand side item to compare for equality. </param>
        /// <returns> <c>True</c> if equals. </returns>
        public static bool Equals( RangeF left, RangeF right )
        {
            return left is null ? right is null : !(right is null) && left.Max.Equals( right.Max ) && left.Min.Equals( right.Min );
        }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
        /// <see cref="T:System.Object" />. </param>
        /// <returns>
        /// <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />; otherwise, <c>False</c>.
        /// </returns>
        public override bool Equals( object obj )
        {
            return this.Equals( obj as RangeF );
        }

        /// <summary>
        /// Returns True if the value of the <paramref name="other"/> equals to the instance value.
        /// </summary>
        /// <remarks>
        /// Ranges are the same if the have the same
        /// <see cref="Type">min</see> and <see cref="Type">max</see> values.
        /// </remarks>
        /// <param name="other"> The other <see cref="RangeF">Range</see> to compare for equality with
        /// this instance. </param>
        /// <returns> A Boolean data type. </returns>
        public bool Equals( RangeF other )
        {
            return other is object && Equals( this, other );
        }

        #endregion

        #region " METHODS "

        /// <summary> Clips the given value. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> A Date. </returns>
        public float Clip( float value )
        {
            return value < this.Min ? this.Min : value > this.Max ? this.Max : value;
        }

        /// <summary> Query if 'point' is inside the range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="point"> A <see cref="T:System.Single">Single</see> point value&gt; </param>
        /// <returns> <c>true</c> if inside; otherwise <c>false</c> </returns>
        public bool Encloses( float point )
        {
            return point > this.Min && point < this.Max;
        }

        /// <summary> Returns true if the point value is within the range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="point"> A <see cref="T:System.Single">Single</see> point value&gt; </param>
        /// <returns>
        /// <c>True</c> if value above or equal to minimum or below or equal to maximum.
        /// </returns>
        public bool Contains( float point )
        {
            return point >= this.Min && point <= this.Max;
        }

        /// <summary> Returns true if the point value is within the range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="point">     A <see cref="T:System.Single">Single</see> point value&gt; </param>
        /// <param name="tolerance"> Tolerance for comparison. </param>
        /// <returns>
        /// <c>True</c> if value above or equal to minimum - tolerance or below or equal to maximum +
        /// tolerance.
        /// </returns>
        public bool Contains( float point, float tolerance )
        {
            return point >= this.Min - tolerance && point <= this.Max + tolerance;
        }

        /// <summary>
        /// Extends this range to include both its present values and the specified range.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="range"> A <see cref="RangeR"/> value. </param>
        public void ExtendRange( RangeF range )
        {
            if ( range is null )
            {
                throw new ArgumentNullException( nameof( range ) );
            }

            if ( this.Min > range.Min )
            {
                this.Min = range.Min;
            }

            if ( this.Max < range.Max )
            {
                this.Max = range.Max;
            }
        }

        /// <summary> Shift range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="value"> The value by which to shift the range. </param>
        public void ShiftRange( float value )
        {
            this.SetRange( value + this.Min, value + this.Max );
        }

        /// <summary> Transpose range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="shift"> The shift. </param>
        /// <param name="scale"> The scale. </param>
        public void TransposeRange( float shift, float scale )
        {
            this.SetRange( shift + scale * this.Min, shift + scale * this.Max );
        }

        /// <summary> Sets the range based on the extrema. </summary>
        /// <remarks> Use this class to set the range. </remarks>
        /// <param name="minValue"> A <see cref="T:System.Single">Single</see> expression that specifies
        /// the minimum value of the range. </param>
        /// <param name="maxValue"> A <see cref="T:System.Single">Single</see> expression that specifies
        /// the maximum value of the range. </param>
        public void SetRange( float minValue, float maxValue )
        {
            this.Min = minValue;
            this.Max = maxValue;
        }

        /// <summary>
        /// Gets the exponent based on the range extremum values.  This is the
        /// <see cref="T:System.Integer">integer</see> value representing the exponent of
        /// the most significant digit of range limits.  For example, the 4 for 20,000 or -3 for 0.0012.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> The exponent. </returns>
        public int GetExponent()
        {
            return Convert.ToInt32( Math.Max( NumericExtensions.NumericExtensionMethods.Exponent( this.Min ), NumericExtensions.NumericExtensionMethods.Exponent( this.Max ) ) );
        }

        /// <summary>
        /// Gets the exponent based on the range extremum values.  This is the
        /// <see cref="T:System.Integer">integer</see> value representing the exponent of
        /// the most significant digit of range limits.  For example, the 4 for 20,000 or -3 for 0.0012.
        /// With engineering scales, the exponents are multiples of three, e.g., 20,000 yields +3 and
        /// 0.0001 -3.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="useEngineeringScale"> True to use scale exponent increments of 3. </param>
        /// <returns> The exponent. </returns>
        public int GetExponent( bool useEngineeringScale )
        {
            return Convert.ToInt32( Math.Max( NumericExtensions.NumericExtensionMethods.Exponent( this.Min, useEngineeringScale ), NumericExtensions.NumericExtensionMethods.Exponent( this.Max, useEngineeringScale ) ) );
        }

        /// <summary> Creates a unique hash code. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> An <see cref="T:System.Integer">integer</see> value. </returns>
        public override int GetHashCode()
        {
            return new Tuple<int, int>( this.Min.GetHashCode(), this.Max.GetHashCode() ).GetHashCode();
        }

        /// <summary> Returns the default string representation of the range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> A representation of the range, e.g., '(min,max)' . </returns>
        public override string ToString()
        {
            return $"({this.Min},{this.Max})";
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> Gets the maximum value of the range. </summary>
        /// <value> A <see cref="T:System.Single">Single</see> property. </value>
        public float Max { get; set; }

        /// <summary> Returns the mid range point of the range. </summary>
        /// <value> A read only <see cref="T:System.Single">Single</see> property. </value>
        public float Midrange => 0.5f * (this.Max + this.Min);

        /// <summary> Gets the minimum value of the range. </summary>
        /// <value> A <see cref="T:System.Single">Single</see> property. </value>
        public float Min { get; set; }

        /// <summary> Gets the range value of the range. </summary>
        /// <value> A <see cref="T:System.Single">Single</see> </value>
        public float Span => this.Max - this.Min;

        #endregion

        #region " ATTRIBUTES "

        /// <summary> Returns true if the range is <see cref="RangeF.Empty"/>. </summary>
        /// <value> The is <see cref="RangeF.Empty"/>. </value>
        public bool IsEmpty => this.Equals( Empty );

        /// <summary> Returns true if the range is <see cref="RangeF.Unity"/>. </summary>
        /// <value> The is <see cref="RangeF.Unity"/>. </value>
        public bool IsUnity => this.Equals( Unity );

        #endregion

    }
}
