''' <summary> Sample quartile calculations. </summary>
''' <remarks>
''' (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/29/2014 </para>
''' </remarks>
Public Class SampleQuartiles

#Region " CONSTRUCTOR "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Public Sub New()
        MyBase.New()
        Me.ResetKnownStateThis()
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="values"> The values. </param>
    Public Sub New(ByVal values As Double())
        Me.New()
        If values IsNot Nothing Then
            Me._Sample.AddValues(values)
        End If
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="sample"> The sample. </param>
    Public Sub New(ByVal sample As SampleStatistics)
        Me.New()
        If sample IsNot Nothing Then
            Me._Sample = New SampleStatistics(sample)
        End If
    End Sub

#End Region

#Region " RESET AND CLEAR "

    ''' <summary> Clears quartile values to their known (initial) state. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Private Sub ClearQuartilesThis()
        Me._Quartiles = New Quartiles
        Me._Fence = Interval.Empty
        Me._FilteredSample = New SampleStatistics
    End Sub

    ''' <summary> Clears values to their known (initial) state. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Private Sub ClearKnownStateThis()
        Me._Sample.ClearKnownState()
        Me.ClearQuartilesThis()
    End Sub

    ''' <summary> Clears values to their known (initial) state. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Public Overridable Sub ClearKnownState()
        Me.ClearKnownStateThis()
    End Sub

    ''' <summary> Resets to known (default/instantiated) state. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Private Sub ResetKnownStateThis()
        Me._FenceFactor = 1.5
        Me._Sample = New SampleStatistics()
        Me.ClearKnownStateThis()
    End Sub

    ''' <summary> Resets to known (default/instantiated) state. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Public Overridable Sub ResetKnownState()
        Me.ResetKnownStateThis()
    End Sub

#End Region

#Region " SAMPLE STATISTICS "

    ''' <summary> Gets the sample. </summary>
    ''' <value> The sample. </value>
    Public ReadOnly Property Sample As SampleStatistics

    ''' <summary> Gets the filtered sample with outliers removed. </summary>
    ''' <value> The filtered sample. </value>
    Public Property FilteredSample As SampleStatistics

    ''' <summary> Gets the number of outliers. </summary>
    ''' <value> The number of outliers. </value>
    Public ReadOnly Property OutlierCount As Integer
        Get
            Return Me.Sample.Count - Me.FilteredSample.Count
        End Get
    End Property

#End Region

#Region " QUARTILES "

    ''' <summary> Gets the fence factor. </summary>
    ''' <value> The fence factor. </value>
    Public Property FenceFactor As Double

    ''' <summary> Gets the quartiles. </summary>
    ''' <value> The quartiles. </value>
    Public Property Quartiles As Quartiles

    ''' <summary> Gets the fence. </summary>
    ''' <value> The fence. </value>
    Public Property Fence As Interval

    ''' <summary>
    ''' Evaluates the <see cref="Quartiles">quartiles</see> and the
    ''' <see cref="FilteredSample">filtered sample</see>.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Public Sub Evaluate()
        Me.ClearQuartilesThis()
        Me.Quartiles.Evaluate(Me.Sample.ValuesArray)
        Me._Fence = isr.Core.Engineering.Interval.CreateInstance(Me.Quartiles.First - Me.FenceFactor * Me.Quartiles.Range,
                                                                 Me.Quartiles.Third + Me.FenceFactor * Me.Quartiles.Range,
                                                                 0.000001 * Me.Quartiles.Range)
        Me.FilterSample()
    End Sub

    ''' <summary> Filter sample. </summary>
    ''' <remarks>
    ''' This function is set outside the evaluation to allow recalculation is the fence is updated.
    ''' </remarks>
    Public Sub FilterSample()
        ' clear the filtered quartiles.
        Me._FilteredQuartiles = Nothing
        Me._FilteredSample = New SampleStatistics
        For Each value As Double In Me.Sample.ValuesArray
            If Me.Fence.Contains(value) Then
                Me.FilteredSample.AddValue(value)
            End If
        Next
    End Sub

#End Region

#Region " FILTERED QUARTILES "
    ''' <summary> The filtered quartiles. </summary>
    Private _FilteredQuartiles As SampleQuartiles

    ''' <summary> Gets the filtered sample quartiles after removal of outliers. </summary>
    ''' <value> The filtered quartiles. </value>
    Public ReadOnly Property FilteredQuartiles As SampleQuartiles
        Get
            If Me._FilteredQuartiles Is Nothing Then
                Me._FilteredQuartiles = New SampleQuartiles(Me.FilteredSample)
                Me._FilteredQuartiles.Evaluate()
            End If
            Return Me._FilteredQuartiles
        End Get
    End Property

#End Region

End Class
