Imports System.ComponentModel

''' <summary> A histogram binding list. </summary>
''' <remarks> David, 2020-06-27. </remarks>
Public Class HistogramBindingList
    Inherits BindingList(Of Windows.Point)

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-06-27. </remarks>
    ''' <param name="lowerBound"> The lower bound. </param>
    ''' <param name="upperBound"> The upper bound. </param>
    ''' <param name="binCount">   The number of bins. </param>
    Public Sub New(ByVal lowerBound As Double, ByVal upperBound As Double, ByVal binCount As Integer)
        MyBase.New
        Me.BinCount = binCount
        Me.LowerBound = lowerBound
        Me.UpperBound = upperBound
        Me.BinWidth = (upperBound - lowerBound) / binCount
        Me.InverseWidth = 1 / Me.BinWidth
        Me._Counts = CType(Array.CreateInstance(GetType(Integer), binCount + 2), Integer())
    End Sub
    ''' <summary> The counts. </summary>
    Private ReadOnly _Counts As Integer()

    ''' <summary> Gets the counts. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> An Integer() </returns>
    Public Function Counts() As Integer()
        Return Me._Counts
    End Function

    ''' <summary> Gets or sets the number of bins. </summary>
    ''' <value> The number of bins. </value>
    Public Property BinCount As Integer

    ''' <summary> Gets or sets the width of the bin. </summary>
    ''' <value> The width of the bin. </value>
    Public Property BinWidth As Double

    ''' <summary> Gets or sets the width of the inverse. </summary>
    ''' <value> The width of the inverse. </value>
    Private Property InverseWidth As Double

    ''' <summary> Gets or sets the lower bound. </summary>
    ''' <value> The lower bound. </value>
    Public Property LowerBound As Double

    ''' <summary> Gets or sets the upper bound. </summary>
    ''' <value> The upper bound. </value>
    Public Property UpperBound As Double

    ''' <summary> Converts a value to a bin number. </summary>
    ''' <remarks> David, 2020-06-27. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> Value as an Integer. </returns>
    Public Function ToBinNumber(ByVal value As Double) As Integer
        Dim binValue As Double = Me.InverseWidth * (value - Me.LowerBound)
        Return If(binValue < 0, 0, If(binValue >= Me.BinCount, Me.BinCount + 1, 1 + CInt(Fix(binValue))))
    End Function

    ''' <summary> Initializes this. </summary>
    ''' <remarks> David, 2020-06-27. </remarks>
    Public Sub Initialize()
        For i As Integer = 0 To Me.Counts.Count - 1
            Dim x As Double
            If i = 0 Then
                x = Me.LowerBound
            ElseIf i = Me.Counts.Count - 1 Then
                x = Me.UpperBound
            Else
                ' x = lowerBound + 0.5 * binWidth + binWidth * (i - 1)
                x = Me.LowerBound + Me.BinWidth * (i - 0.5)
            End If
            Me.Add(New System.Windows.Point(x, Me.Counts(i)))
        Next
    End Sub

    ''' <summary> Initializes this. </summary>
    ''' <remarks> David, 2020-09-05. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="source"> Source for the. </param>
    Public Sub Initialize(ByVal source As Double())
        If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
        For Each x As Double In source
            Me._Counts(Me.ToBinNumber(x)) += 1
        Next
        Me.Initialize()
    End Sub

    ''' <summary> Initializes this. </summary>
    ''' <remarks> David, 2020-09-05. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="source"> Source for the. </param>
    Public Sub Initialize(ByVal source As IEnumerable(Of Double))
        If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
        For Each x As Double In source
            Me._Counts(Me.ToBinNumber(x)) += 1
        Next
        Me.Initialize()
    End Sub

    ''' <summary> Initializes this. </summary>
    ''' <remarks> David, 2020-06-27. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="source"> Source for the. </param>
    Public Sub Initialize(ByVal source As IList(Of Double))
        If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
        For Each x As Double In source
            Me._Counts(Me.ToBinNumber(x)) += 1
        Next
        Me.Initialize()
    End Sub

    ''' <summary> Updates the given value. </summary>
    ''' <remarks> David, 2020-06-27. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub Update(ByVal value As Double)
        Dim binNumber As Integer = Me.ToBinNumber(value)
        Me._Counts(binNumber) += 1
        Dim currentPoint As System.Windows.Point = Me(binNumber)
        Me(binNumber) = New System.Windows.Point(currentPoint.X, currentPoint.Y + 1)
    End Sub

    ''' <summary> Updates the given value. </summary>
    ''' <remarks> David, 2020-06-27. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub Update(ByVal value As Double?)
        If value.HasValue Then Me.Update(value.Value)
    End Sub

End Class
