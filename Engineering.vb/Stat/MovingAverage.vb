''' <summary> Moving average. </summary>
''' <remarks>
''' (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 11/19/2014 </para>
''' </remarks>
Public Class MovingAverage
    Inherits SampleStatistics
    Implements ICloneable

#Region " CONSTRUCTOR "
    ''' <summary> The minimum length of the. </summary>
    Private Const _MinimumLength As Integer = 2

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="length"> The length. </param>
    Public Sub New(ByVal length As Integer)
        MyBase.New()
        Me.Length = If(length < MovingAverage._MinimumLength, MovingAverage._MinimumLength, length)
    End Sub

    ''' <summary> The cloning constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As MovingAverage)
        MyBase.New(value)
        Me.Length = If(value Is Nothing,
            MovingAverage._MinimumLength,
            If(value.Length < MovingAverage._MinimumLength, MovingAverage._MinimumLength, value.Length))
    End Sub

    ''' <summary> Creates a new object that is a copy of the current instance. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> A new object that is a copy of this instance. </returns>
    Public Overrides Function Clone() As Object Implements System.ICloneable.Clone
        Return New MovingAverage(Me)
    End Function

#End Region

#Region " MOVING AVERAGE "

    ''' <summary> Gets or sets the length of the moving average. </summary>
    ''' <value> The length. </value>
    Public Property Length As Integer

    ''' <summary> Adds a value. </summary>
    ''' <remarks>
    ''' This method was modified to ensure that the moving value is updated correctly. Test showed
    ''' that when the first value was also an extremum value, the previous extremum was retained
    ''' indicating that the update range method estimate used the previous values and then the new
    ''' value was added. To this end, a new test was added with a large number of test to ensure that
    ''' the fix works.
    ''' </remarks>
    ''' <param name="value"> The value. </param>
    Public Overrides Sub AddValue(ByVal value As Double)
        MyBase.AddValue(value)
        Do While Me.Count > Me.Length
            Me.RemoveValueAt(0)
        Loop
    End Sub

#End Region

End Class
