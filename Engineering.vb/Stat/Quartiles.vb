''' <summary> A quartiles. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/10/2017 </para>
''' </remarks>
Public Class Quartiles

#Region " CONSTRUCTOR "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Public Sub New()
        MyBase.New()
        Me.ClearKnownStateThis()
    End Sub

#End Region

#Region " QUARTILES "

    ''' <summary> Clears values to their known (initial) state. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Private Sub ClearKnownStateThis()
        Me._Values = {0, 0, 0}
    End Sub

    ''' <summary> Clears values to their known (initial) state. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Public Overridable Sub ClearKnownState()
        Me.ClearKnownStateThis()
    End Sub

    ''' <summary> Gets the Median (second quartile). </summary>
    ''' <value> The Median value of the  excluding outliers. </value>
    Public ReadOnly Property Median As Double
        Get
            Return Me.Values(1)
        End Get
    End Property

    ''' <summary> Gets the first quartile. </summary>
    ''' <value> The first quartile. </value>
    Public ReadOnly Property First As Double
        Get
            Return Me.Values(0)
        End Get
    End Property

    ''' <summary> Gets the Second quartile (median). </summary>
    ''' <value> The Second quartile (median). </value>
    Public ReadOnly Property Second As Double
        Get
            Return Me.Values(1)
        End Get
    End Property

    ''' <summary> Gets the Third quartile. </summary>
    ''' <value> The Third quartile. </value>
    Public ReadOnly Property Third As Double
        Get
            Return Me.Values(2)
        End Get
    End Property
    ''' <summary> The values. </summary>
    Private _Values As Double()

    ''' <summary> Gets the quartiles. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> A Double() array with 1st, 2nd, and 3rd quartile values. </returns>
    Public Function Values() As Double()
        Return Me._Values
    End Function

    ''' <summary> Gets the quartile range. </summary>
    ''' <value> The quartile range. </value>
    Public ReadOnly Property Range As Double
        Get
            Return Me.Values(2) - Me.Values(0)
        End Get
    End Property

    ''' <summary> Calculates the percentile. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="sortedValues"> The sorted values. </param>
    ''' <param name="value">        The percentile value between 0 and 1. </param>
    ''' <returns> A Double value representing the percentile. </returns>
    Public Shared Function Percentile(ByVal sortedValues As Double(), ByVal value As Double) As Double
        If sortedValues Is Nothing Then Throw New ArgumentNullException(NameOf(sortedValues))
        Dim useLibraOfficeStyle As Boolean = True
        Dim realIndex As Double = If(useLibraOfficeStyle, value * (sortedValues.Length - 1), value * sortedValues.Length - 1)
        If realIndex < 0 Then Throw New InvalidOperationException("Insufficient data for establishing the percentile")
        Dim index As Integer = CInt(Math.Floor(realIndex))

        Dim fraction As Double = realIndex - index
        Return If(index >= (sortedValues.Length - 1),
            sortedValues(sortedValues.Length - 1),
            sortedValues(index) + fraction * (sortedValues(index + 1) - sortedValues(index)))
    End Function

    ''' <summary> Calculates the quartiles. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="values"> The values. </param>
    ''' <returns> The calculated quartiles. </returns>
    Public Shared Function CalculateQuartiles(ByVal values() As Double) As Double()
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        If values.Count < 4 Then Throw New InvalidOperationException("Insufficient data for establishing quartiles")
        Array.Sort(values)
        Return New Double() {Quartiles.Percentile(values, 0.25), Quartiles.Percentile(values, 0.5), Quartiles.Percentile(values, 0.75)}
    End Function

    ''' <summary> Evaluates the quartiles. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="samples"> The samples. </param>
    Public Sub Evaluate(ByVal samples() As Double)
        Me.ClearKnownState()
        Me._Values = Quartiles.CalculateQuartiles(samples)
    End Sub

#End Region

End Class
