Imports System.ComponentModel

Imports isr.Core.Constructs

''' <summary> Moving Window filter. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 1/27/2016 </para>
''' </remarks>
Public Class MovingWindow
    Inherits MovingAverage
    Implements ICloneable

#Region " CONSTRUCTOR "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="length"> The length. </param>
    Public Sub New(ByVal length As Integer)
        MyBase.New(length)
        Me.ResetKnownStateThis()
    End Sub

    ''' <summary> The cloning constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As MovingWindow)
        MyBase.New(value)
        Me.ResetKnownStateThis()
        If value IsNot Nothing Then
            Me._ReadingTimespan = value.ReadingTimespan
            Me._TimeoutInterval = value.TimeoutInterval
            Me._ReadingTimeoutInterval = value.ReadingTimeoutInterval
            Me._UpdateRule = value.UpdateRule
            Me._RelativeWindow = New isr.Core.Constructs.RangeR(value.RelativeWindow)
            Me._Status = value.Status
            Me._ConformityRange = New isr.Core.Constructs.RangeR(value.ConformityRange)
            Me._OverflowRange = New isr.Core.Constructs.RangeR(value.OverflowRange)
            Me._Readings = New ReadingCollection(value.Readings)
            Me._MaximumFailureCount = value.MaximumFailureCount
            Me._MaximumConsecutiveFailureCount = value.MaximumConsecutiveFailureCount
        End If
    End Sub

    ''' <summary> Creates a new object that is a copy of the current instance. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> A new object that is a copy of this instance. </returns>
    Public Overrides Function Clone() As Object Implements System.ICloneable.Clone
        Return New MovingWindow(Me)
    End Function

#End Region

#Region " RESET AND CLEAR "

    ''' <summary> Clears values to their known (initial) state. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Private Sub ClearKnownStateThis()
        Me._Readings.Clear()
        Me._ReadingTimespan = TimeSpan.Zero
        Me._Status = MovingWindowStatus.None
    End Sub

    ''' <summary> Clears values to their known (initial) state. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Public Overrides Sub ClearKnownState()
        MyBase.ClearKnownState()
        Me.ClearKnownStateThis()
    End Sub

    ''' <summary> Resets to known (default/instantiated) state. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Private Sub ResetKnownStateThis()
        Me._OverflowRange = isr.Core.Constructs.RangeR.Full
        Me._ConformityRange = isr.Core.Constructs.RangeR.Full
        Me._MaximumFailureCount = 0
        Me._MaximumConsecutiveFailureCount = 0
        Me._Readings = New ReadingCollection
        Me._TimeoutInterval = TimeSpan.MaxValue
        Me._ReadingTimeoutInterval = MovingWindow.DefaultReadingTimeoutInterval
        Me._UpdateRule = MovingWindowUpdateRule.None
        Me._RelativeWindow = isr.Core.Constructs.RangeR.Zero
        Me.ClearKnownStateThis()
    End Sub

    ''' <summary> Resets to known (default/instantiated) state. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Public Overrides Sub ResetKnownState()
        MyBase.ResetKnownState()
        Me.ResetKnownStateThis()
    End Sub

#End Region

#Region " TIMEOUT MANAGER "

    ''' <summary> Gets the timeout interval. </summary>
    ''' <value> The timeout interval. </value>
    Public Property TimeoutInterval As TimeSpan

    ''' <summary> Query if 'timeoutInterval' is timeout. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> <c>true</c> if timeout; otherwise <c>false</c> </returns>
    Public Function IsTimeout() As Boolean
        Return Me.Readings.ElapsedTime > Me.TimeoutInterval
    End Function

    ''' <summary> The default reading timeout interval. </summary>
    ''' <value> The default reading timeout interval. </value>
    Public Shared Property DefaultReadingTimeoutInterval As TimeSpan = TimeSpan.FromSeconds(2)

    ''' <summary> Gets the Reading Timeout interval. </summary>
    ''' <value> The ReadingTimeout interval. </value>
    Public Property ReadingTimeoutInterval As TimeSpan

#End Region

#Region " RANGE MANAGEMENT "

    ''' <summary>
    ''' Gets the Conformity range. Values outside this range are
    ''' <see cref="ReadingStatus.NonConformal">non conformal</see>, such as RTD resistance that
    ''' fall outside the RTD resistance range.
    ''' </summary>
    ''' <value> The acceptance range. </value>
    '''
    ''' ### <exception cref="ArgumentNullException"> Thrown when one or more required arguments are
    '''                                              null. </exception>
    Public Property ConformityRange As isr.Core.Constructs.RangeR

    ''' <summary>
    ''' Gets the overflow range. Values not-inside this range are
    ''' <see cref="ReadingStatus.Overflow">Overflow</see>, such as an open circuit.
    ''' </summary>
    ''' <remarks>
    ''' This is designed to address overflow condition such as getting an infinity value (open) from
    ''' a SCPI instrument.
    ''' </remarks>
    ''' <value> The overflow range. </value>
    Public Property OverflowRange As isr.Core.Constructs.RangeR

#End Region

#Region " MOVING WINDOW "

    ''' <summary> Gets the update rule. </summary>
    ''' <value> The update rule. </value>
    Public Property UpdateRule As MovingWindowUpdateRule

    ''' <summary> Gets the current window around the mean. </summary>
    ''' <value> The current window. </value>
    Public ReadOnly Property CurrentWindow As isr.Core.Constructs.RangeR
        Get
            Dim average As Double = Me.Mean
            Return Me.RelativeWindow.TransposedRange(average, average)
        End Get
    End Property

    ''' <summary> Gets the relative window. </summary>
    ''' <value> The relative window. </value>
    Public Property RelativeWindow As isr.Core.Constructs.RangeR

    ''' <summary> Gets the total number of reading. </summary>
    ''' <value> The number of readings. </value>
    Public ReadOnly Property TotalReadingsCount As Integer
        Get
            Return Me.Readings.Count
        End Get
    End Property

    ''' <summary> Gets the number of readings amenable for averaging. </summary>
    ''' <value> The number of readings. </value>
    Public ReadOnly Property AmenableReadingsCount As Integer
        Get
            Return Me.Readings.StatusReadingDictionary(ReadingStatus.Amenable).Count
        End Get
    End Property

    ''' <summary> Gets the first reading. </summary>
    ''' <value> The first reading. </value>
    Public ReadOnly Property FirstReading As MovingWindowReading
        Get
            Return Me.Readings.FirstReading
        End Get
    End Property

    ''' <summary> Gets the last averaged reading. </summary>
    ''' <value> The last averaged reading. </value>
    Public ReadOnly Property LastAveragedReading As Double
        Get
            Return If(Me.Any, Me.ValuesArray.Last, 0)
        End Get
    End Property

    ''' <summary> Gets the last reading. </summary>
    ''' <value> The last reading. </value>
    Public ReadOnly Property LastReading As MovingWindowReading
        Get
            Return Me.Readings.LastReading
        End Get
    End Property

    ''' <summary> Gets the amenable readings. </summary>
    ''' <value> The amenable readings. </value>
    Public ReadOnly Property AmenableReadings As IList(Of MovingWindowReading)
        Get
            Return Me.Readings.StatusReadingDictionary(ReadingStatus.Amenable)
        End Get
    End Property

    ''' <summary> Gets the last amenable reading. </summary>
    ''' <value> The last amenable reading. </value>
    Public ReadOnly Property LastAmenableReading As MovingWindowReading
        Get
            Return Me.Readings.LastAmenableReading
        End Get
    End Property

    ''' <summary> Gets the elapsed time. </summary>
    ''' <value> The elapsed time. </value>
    Public ReadOnly Property ElapsedTime As TimeSpan
        Get
            Return Me.Readings.ElapsedTime
        End Get
    End Property

    ''' <summary> Gets the elapsed milliseconds. </summary>
    ''' <value> The elapsed milliseconds. </value>
    Public ReadOnly Property ElapsedMilliseconds As Double
        Get
            Return Me.Readings.ElapsedMilliseconds
        End Get
    End Property

    ''' <summary> Gets the reading timespan. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The reading timespan. </value>
    Public ReadOnly Property ReadingTimespan As TimeSpan

    ''' <summary> Reads a value. Measures the reading timespan. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="action"> The action. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function ReadValue(action As Func(Of Double?)) As Boolean
        If action Is Nothing Then Throw New ArgumentNullException(NameOf(action))
        Dim readingStopwatch As Stopwatch = Stopwatch.StartNew
        Dim value As Double? = action()
        Me._ReadingTimespan = readingStopwatch.Elapsed
        Me.AddValue(value, Me._ReadingTimespan)
        Return value.HasValue
    End Function

    ''' <summary> Adds a value. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value"> The value. </param>
    Public Overrides Sub AddValue(ByVal value As Double)
        Me.AddValue(New Double?(value), TimeSpan.Zero)
    End Sub

    ''' <summary> Adds a value. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value">           The value. </param>
    ''' <param name="readingTimeSpan"> The reading timespan. </param>
    Public Overloads Sub AddValue(ByVal value As Double?, ByVal readingTimeSpan As TimeSpan)
        Dim reading As New MovingWindowReading(value, Me.ParseReadingStatus(value, readingTimeSpan), Me.Count) With {.InputTimespan = readingTimeSpan}
        Me.Readings.Add(reading)
        Me.AddValue(reading)
    End Sub

    ''' <summary> Adds a value. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="reading"> The reading. </param>
    Private Overloads Sub AddValue(ByVal reading As MovingWindowReading)
        If reading.Status = ReadingStatus.Amenable Then
            Dim value As Double = reading.Value.Value
            If Me.Count < Me.Length OrElse Me.UpdateRule = MovingWindowUpdateRule.None Then
                Me._Status = MovingWindowStatus.Filling
                MyBase.AddValue(value)
            Else
                Me._Status = MovingWindow.ParseMovingWindowStatus(Me.CurrentWindow, value)
                ' if value is within window and the window is full we are done.
                If Me.Status = MovingWindowStatus.Below OrElse Me.Status = MovingWindowStatus.Above Then
                    ' if not inside the window, drop first value and add this value
                    MyBase.AddValue(value)
                End If
            End If
        Else
            ' if the reading is not amenable, make sure to resume the filling status.
            Me._Status = MovingWindowStatus.Filling
        End If
    End Sub

#End Region

#Region " MOVING WINDOW STATUS "

    ''' <summary> Gets the status. </summary>
    ''' <value> The status. </value>
    Public ReadOnly Property Status As MovingWindowStatus

    ''' <summary> Returns true if the total failures equals or exceeds the maximum allowed. </summary>
    ''' <value> The number of hit failures. </value>
    Public ReadOnly Property HitFailureCount As Boolean
        Get
            Return Me.MaximumFailureCount <= Me.FailureCount
        End Get
    End Property

    ''' <summary>
    ''' Returns true if the number of consecutive total failures equals or exceeds the maximum
    ''' allowed.
    ''' </summary>
    ''' <value> The number of hit consecutive failures. </value>
    Public ReadOnly Property HitConsecutiveFailureCount As Boolean
        Get
            Return Me.MaximumConsecutiveFailureCount <= Me.ConsecutiveFailureCount
        End Get
    End Property

    ''' <summary> Query if this moving windows had a failure count out. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> <c>true</c> if failure count out; otherwise <c>false</c> </returns>
    Public Function IsFailureCountOut() As Boolean
        Return Me.HitConsecutiveFailureCount OrElse Me.HitFailureCount
    End Function

    ''' <summary> Query if the status is a stop status. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> <c>true</c> if stop status; otherwise <c>false</c> </returns>
    Public Function IsStopStatus() As Boolean
        Return Me.Status = MovingWindowStatus.Within OrElse Me.IsTimeout OrElse Me.IsFailureCountOut
    End Function

    ''' <summary> Parses the moving window status. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="currentWindow"> The current window. </param>
    ''' <param name="value">         The value. </param>
    ''' <returns> The <see cref="MovingWindowStatus"/>. </returns>
    Private Shared Function ParseMovingWindowStatus(ByVal currentWindow As Constructs.RangeR, ByVal value As Double) As MovingWindowStatus
        Dim range As RangeR = currentWindow
        If value > range.Max Then
            Return MovingWindowStatus.Above
        ElseIf value < range.Min Then
            Return MovingWindowStatus.Below
        Else
            Return MovingWindowStatus.Within
        End If
    End Function

    ''' <summary> Status annunciation caption. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="movingWindowStatus"> The status. </param>
    ''' <returns> A String. </returns>
    Public Shared Function StatusAnnunciationCaption(ByVal movingWindowStatus As Core.Engineering.MovingWindowStatus) As String
        Dim result As String
        If movingWindowStatus = Core.Engineering.MovingWindowStatus.Above Then
            result = "high"
        ElseIf movingWindowStatus = Core.Engineering.MovingWindowStatus.Below Then
            result = "low"
        ElseIf movingWindowStatus = Core.Engineering.MovingWindowStatus.Filling Then
            result = "filling"
        ElseIf movingWindowStatus = Core.Engineering.MovingWindowStatus.None Then
            result = "n/a"
        Else
            result = "within"
        End If
        Return result
    End Function


#End Region

#Region " READINGS "

    ''' <summary>
    ''' Gets the maximum number of <see cref="ReadingStatus.Overflow"/> or
    ''' <see cref="ReadingStatus.NonConformal"/> or
    ''' <see cref="ReadingStatus.Timeout"/> or
    ''' <see cref="ReadingStatus.Indeterminable"/> failures allowed. Zero if unlimited.
    ''' </summary>
    ''' <value> The number of maximum range failures. </value>
    Public Property MaximumFailureCount As Integer

    ''' <summary>
    ''' Gets the maximum number of consecutive <see cref="ReadingStatus.Overflow"/> or
    ''' <see cref="ReadingStatus.NonConformal"/> or
    ''' <see cref="ReadingStatus.Timeout"/> or
    ''' <see cref="ReadingStatus.Indeterminable"/> failures allowed. Zero if unlimited.
    ''' </summary>
    ''' <value> The number of maximum range failures. </value>
    Public Property MaximumConsecutiveFailureCount As Integer

    ''' <summary> Gets the number of failures. </summary>
    ''' <value> The number of failures. </value>
    Public ReadOnly Property FailureCount As Integer
        Get
            Return Me.Readings.FailureCount
        End Get
    End Property

    ''' <summary> Builds failure report. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> A String. </returns>
    Public Function BuildFailureReport() As String
        Return Me.Readings.BuildFailureReport
    End Function

    ''' <summary> Builds detailed failure report. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> A String. </returns>
    Public Function BuildDetailedFailureReport() As String
        Return Me.Readings.BuildDetailedFailureReport
    End Function

    ''' <summary> Gets the number of consecutive failures. </summary>
    ''' <value> The number of consecutive failures. </value>
    Public ReadOnly Property ConsecutiveFailureCount As Integer
        Get
            Return Me.Readings.ConsecutiveFailureCount
        End Get
    End Property

    ''' <summary> Parse reading status. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value">         The value. </param>
    ''' <param name="inputInterval"> The input interval. </param>
    ''' <returns> The ReadingStatus. </returns>
    Private Function ParseReadingStatus(ByVal value As Double?, ByVal inputInterval As TimeSpan) As ReadingStatus
        If inputInterval > Me.ReadingTimespan Then
            Return ReadingStatus.Timeout
        ElseIf value.HasValue Then
            Return Me.ParseReadingStatus(value.Value)
        Else
            Return ReadingStatus.Indeterminable
        End If
    End Function

    ''' <summary> Parse reading status. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> The ReadingStatus. </returns>
    Private Function ParseReadingStatus(ByVal value As Double) As ReadingStatus
        Dim result As ReadingStatus

        If Not Me.OverflowRange.Encloses(value) Then
            result = ReadingStatus.Overflow
        ElseIf Not Me.ConformityRange.Encloses(value) Then
            result = ReadingStatus.NonConformal
        Else
            result = ReadingStatus.Amenable
        End If
        Return result
    End Function

    ''' <summary> Query if 'status' has reading. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="status"> The status. </param>
    ''' <returns> <c>true</c> if reading; otherwise <c>false</c> </returns>
    Public Shared Function HasReading(ByVal status As Core.Engineering.MovingWindowStatus) As Boolean
        Return status = Core.Engineering.MovingWindowStatus.Above OrElse
               status = Core.Engineering.MovingWindowStatus.Below OrElse
               status = Core.Engineering.MovingWindowStatus.Within
    End Function

    ''' <summary> Status annunciation caption. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> A String. </returns>
    Public Function StatusAnnunciationCaption() As String
        Return MovingWindow.StatusAnnunciationCaption(Me.LastReading, Me.Status)
    End Function

    ''' <summary> Status annunciation caption. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="lastReading"> The last reading. </param>
    ''' <returns> A String. </returns>
    Public Shared Function StatusAnnunciationCaption(ByVal lastReading As MovingWindowReading) As String
        If lastReading Is Nothing Then Throw New ArgumentNullException(NameOf(lastReading))
        Dim result As String = String.Empty
        If lastReading.Status = ReadingStatus.Amenable Then
            result = "amenable"
        ElseIf lastReading.Status = ReadingStatus.Indeterminable Then
            result = "Indeterminable"
        ElseIf lastReading.Status = ReadingStatus.NonConformal Then
            result = "Non-Conforming"
        ElseIf lastReading.Status = ReadingStatus.Overflow Then
            result = "Overflow"
        ElseIf lastReading.Status = ReadingStatus.Timeout Then
            result = "Device Timeout"
        End If
        Return result
    End Function

    ''' <summary> Status annunciation caption. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="lastReading">        The last reading. </param>
    ''' <param name="movingWindowStatus"> The status. </param>
    ''' <returns> A String. </returns>
    Public Shared Function StatusAnnunciationCaption(ByVal lastReading As MovingWindowReading, ByVal movingWindowStatus As Core.Engineering.MovingWindowStatus) As String
        If lastReading Is Nothing Then Throw New ArgumentNullException(NameOf(lastReading))
        Return If(lastReading.Status = ReadingStatus.Amenable,
            MovingWindow.StatusAnnunciationCaption(movingWindowStatus),
            MovingWindow.StatusAnnunciationCaption(lastReading))
    End Function

    ''' <summary> Gets the readings. </summary>
    ''' <value> The readings. </value>
    Private ReadOnly Property Readings As ReadingCollection

    ''' <summary> Collection of readings. </summary>
    ''' <remarks>
    ''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 4/26/2018 </para>
    ''' </remarks>
    Private Class ReadingCollection
        Inherits ObjectModel.Collection(Of MovingWindowReading)

        ''' <summary> Default constructor. </summary>
        ''' <remarks> David, 2020-09-23. </remarks>
        Public Sub New()
            MyBase.New
            Me.StatusReadingDictionary = New Dictionary(Of ReadingStatus, StatusReadingList)
            For Each status As ReadingStatus In [Enum].GetValues(GetType(ReadingStatus))
                Me.StatusReadingDictionary.Add(status, New StatusReadingList(status))
            Next
        End Sub

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 2020-09-23. </remarks>
        ''' <param name="readings"> The readings. </param>
        Public Sub New(ByVal readings As ReadingCollection)
            Me.New
            For Each reading As MovingWindowReading In readings
                Me.Add(New MovingWindowReading(reading))
            Next
        End Sub

        ''' <summary>
        ''' Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        ''' </summary>
        ''' <remarks> David, 2020-09-23. </remarks>
        '''
        ''' ### <exception cref="T:System.NotSupportedException"> The
        '''                                                       <see cref="T:System.Collections.Generic.ICollection`1" />
        '''                                                       is read-only. </exception>
        Public Overloads Sub Clear()
            MyBase.Clear()
            For Each status As ReadingStatus In [Enum].GetValues(GetType(ReadingStatus))
                Me.StatusReadingDictionary(status).Clear()
            Next
            Me._LastStatus = ReadingStatus.Amenable
            Me._ConsecutiveCount = 0
        End Sub

        ''' <summary> Gets the last reading status. </summary>
        ''' <value> The last status. </value>
        Public ReadOnly Property LastStatus As ReadingStatus

        ''' <summary> Gets the number of readings with consecutive status. </summary>
        ''' <value> The number of readings with consecutive status. </value>
        Private ReadOnly Property ConsecutiveCount As Integer

        ''' <summary> Gets the number of consecutive failures. </summary>
        ''' <value> The number of consecutive failures. </value>
        Public ReadOnly Property ConsecutiveFailureCount As Integer
            Get
                Return If(Me.LastStatus = ReadingStatus.Amenable, 0, Me.ConsecutiveCount)
            End Get
        End Property

        ''' <summary>
        ''' Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
        ''' </summary>
        ''' <remarks> David, 2020-09-23. </remarks>
        ''' <param name="value"> The object to add to the
        '''                      <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
        '''
        ''' ### <exception cref="T:System.NotSupportedException"> The
        '''                                                       <see cref="T:System.Collections.Generic.ICollection`1" />
        '''                                                       is read-only. </exception>
        Public Overloads Sub Add(ByVal value As MovingWindowReading)
            MyBase.Add(value)
            If Me.LastStatus = value.Status Then
                If Me._ConsecutiveCount = 0 Then Me._ConsecutiveCount += 1
                Me._ConsecutiveCount += 1
            Else
                Me._ConsecutiveCount = 0
            End If
            Me._LastStatus = value.Status
            ' must handle all these cases in order to correctly count consecutive failures.
            For Each status As ReadingStatus In [Enum].GetValues(GetType(ReadingStatus))
                Me.StatusReadingDictionary(status).Add(value, Me.ConsecutiveCount)
            Next
        End Sub

        ''' <summary> Gets the last reading. </summary>
        ''' <value> The last reading. </value>
        Public ReadOnly Property FirstReading As MovingWindowReading
            Get
                Return If(Me.Any, Me.Item(0), New MovingWindowReading(New Double?, ReadingStatus.Amenable, 0))
            End Get
        End Property

        ''' <summary> Gets the last reading. </summary>
        ''' <value> The last reading. </value>
        Public ReadOnly Property LastReading As MovingWindowReading
            Get
                Return If(Me.Any, Me.Item(Me.Count - 1), New MovingWindowReading(New Double?, ReadingStatus.Amenable, 0))
            End Get
        End Property

        ''' <summary> Gets the elapsed time. </summary>
        ''' <value> The elapsed time. </value>
        Public ReadOnly Property ElapsedTime As TimeSpan
            Get
                Return If(Me.Any, Me.LastReading.Timestamp.Subtract(Me.FirstReading.Timestamp), TimeSpan.Zero)
            End Get
        End Property

        ''' <summary> Gets the elapsed milliseconds. </summary>
        ''' <value> The elapsed milliseconds. </value>
        Public ReadOnly Property ElapsedMilliseconds As Double
            Get
                Return Me.ElapsedTime.Ticks / TimeSpan.TicksPerMillisecond
            End Get
        End Property

        ''' <summary> Gets the amenable readings. </summary>
        ''' <value> The amenable readings. </value>
        Public ReadOnly Property AmenableReadings As IList(Of MovingWindowReading)
            Get
                Return Me.StatusReadingDictionary(ReadingStatus.Amenable)
            End Get
        End Property

        ''' <summary> Gets the last amenable reading. </summary>
        ''' <value> The last amenable reading. </value>
        Public ReadOnly Property LastAmenableReading As MovingWindowReading
            Get
                Return Me.StatusReadingDictionary(ReadingStatus.Amenable).LastReading
            End Get
        End Property

        ''' <summary> Gets a dictionary of status readings. </summary>
        ''' <value> A dictionary of status readings. </value>
        Public ReadOnly Property StatusReadingDictionary As Dictionary(Of ReadingStatus, StatusReadingList)

        ''' <summary> Gets the number of failures. </summary>
        ''' <value> The number of failures. </value>
        Public ReadOnly Property FailureCount As Integer
            Get
                Dim count As Integer = 0
                For Each status As ReadingStatus In [Enum].GetValues(GetType(ReadingStatus))
                    If status <> ReadingStatus.Amenable Then count += Me.StatusReadingDictionary(status).Count
                Next
                Return count
            End Get
        End Property

        ''' <summary> Appends a line if has value. </summary>
        ''' <remarks> David, 2020-09-23. </remarks>
        ''' <param name="builder"> The builder. </param>
        ''' <param name="value">   The object to add to the
        '''                        <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
        Private Shared Sub AppendLineIfHasValue(ByVal builder As System.Text.StringBuilder, ByVal value As String)
            If Not String.IsNullOrWhiteSpace(value) Then builder.AppendLine(value)
        End Sub

        ''' <summary> Builds failure report. </summary>
        ''' <remarks> David, 2020-09-23. </remarks>
        ''' <returns> A String. </returns>
        Public Function BuildFailureReport() As String
            Dim builder As New System.Text.StringBuilder
            For Each status As ReadingStatus In [Enum].GetValues(GetType(ReadingStatus))
                If status <> ReadingStatus.Amenable Then
                    ReadingCollection.AppendLineIfHasValue(builder, Me.StatusReadingDictionary(status).BuildFailureReport)
                End If
            Next
            Return builder.ToString.TrimEnd(Environment.NewLine.ToCharArray)
        End Function

        ''' <summary> Builds detailed failure report. </summary>
        ''' <remarks> David, 2020-09-23. </remarks>
        ''' <returns> A String. </returns>
        Public Function BuildDetailedFailureReport() As String
            Dim builder As New System.Text.StringBuilder
            For Each status As ReadingStatus In [Enum].GetValues(GetType(ReadingStatus))
                If status <> ReadingStatus.Amenable Then
                    ReadingCollection.AppendLineIfHasValue(builder, Me.StatusReadingDictionary(status).BuildDetailedFailureReport)
                End If
            Next
            Return builder.ToString.TrimEnd(Environment.NewLine.ToCharArray)
        End Function

    End Class

    ''' <summary> List of readings identified by reading status. </summary>
    ''' <remarks>
    ''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 4/26/2018 </para>
    ''' </remarks>
    Private Class StatusReadingList
        Inherits List(Of MovingWindowReading)

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 2020-09-23. </remarks>
        ''' <param name="status"> The status. </param>
        Public Sub New(ByVal status As ReadingStatus)
            MyBase.New
            Me.Status = status
        End Sub

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 2020-09-23. </remarks>
        ''' <param name="readings"> The readings. </param>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Sub New(ByVal readings As StatusReadingList)
            Me.New(readings.Status)
            For Each reading As MovingWindowReading In readings
                Me.Add(New MovingWindowReading(reading))
            Next
            Me._ConsecutiveCount = readings.ConsecutiveCount
        End Sub

        ''' <summary>
        ''' Removes all elements from the <see cref="T:System.Collections.Generic.List`1" />.
        ''' </summary>
        ''' <remarks> David, 2020-09-23. </remarks>
        Public Overloads Sub Clear()
            MyBase.Clear()
            Me._ConsecutiveCount = 0
        End Sub

        ''' <summary> Gets the status. </summary>
        ''' <value> The status. </value>
        Public ReadOnly Property Status As ReadingStatus

        ''' <summary> Gets the number of consecutive readings of the <see cref="Status"/>. </summary>
        ''' <value> The number of consecutive readings of the <see cref="Status"/>. </value>
        Public ReadOnly Property ConsecutiveCount As Integer

        ''' <summary>
        ''' Adds an item to the <see cref="T:System.Collections.Generic.Collection" />.
        ''' </summary>
        ''' <remarks> David, 2020-09-23. </remarks>
        ''' <param name="reading">          The object to add to the
        '''                                 <see cref="T:System.Collections.Generic.Collection" />. 
        ''' </param>
        ''' <param name="consecutiveCount"> The number of consecutive readings of the
        '''                                 <see cref="Status"/>. </param>
        Public Overloads Sub Add(ByVal reading As MovingWindowReading, ByVal consecutiveCount As Integer)
            If reading.Status = Me.Status Then
                MyBase.Add(reading)
                Me._ConsecutiveCount = consecutiveCount
            Else
                Me._ConsecutiveCount = 0
            End If
        End Sub

        ''' <summary> Builds failure report. </summary>
        ''' <remarks> David, 2020-09-23. </remarks>
        ''' <returns> A String. </returns>
        Public Function BuildFailureReport() As String
            Dim builder As New System.Text.StringBuilder
            If Me.ConsecutiveCount > 0 Then builder.AppendLine($"{Me.Status} {Me.ConsecutiveCount } consecutive {If(Me.Status = ReadingStatus.Amenable, "readings", "failures")}")
            If Me.Count > 0 Then builder.AppendLine($"{Me.Status} {Me.Count} {If(Me.Status = ReadingStatus.Amenable, "readings", "failures")}")
            Return builder.ToString.TrimEnd(Environment.NewLine.ToCharArray)
        End Function

        ''' <summary> Builds detailed failure report. </summary>
        ''' <remarks> David, 2020-09-23. </remarks>
        ''' <returns> A String. </returns>
        Public Function BuildDetailedFailureReport() As String
            Dim builder As New System.Text.StringBuilder
            If Me.ConsecutiveCount > 0 Then builder.AppendLine($"{Me.Status} {Me.ConsecutiveCount} consecutive failures")
            If Me.Count > 0 Then builder.AppendLine($"{Me.Status} {Me.Count} failures")
            For Each reading As MovingWindowReading In Me
                If reading.Value.HasValue Then
                    builder.AppendLine($"{reading.Value.Value:G4} {Me.Status} {reading.Timestamp:HH:mm:ss.fff} {reading.OrdinalNumber}")
                Else
                    builder.AppendLine($"{Me.Status} {reading.Timestamp:HH:mm:ss.fff} {reading.OrdinalNumber}")
                End If
            Next
            Return builder.ToString.TrimEnd(Environment.NewLine.ToCharArray)
        End Function

        ''' <summary> Gets the last reading. </summary>
        ''' <value> The last reading. </value>
        Public ReadOnly Property LastReading As MovingWindowReading
            Get
                Return If(Me.Any, Me.Last, New MovingWindowReading(New Double?, ReadingStatus.Amenable, 0))
            End Get
        End Property

    End Class

#End Region

#Region " PERCENT PROGRESS "

    ''' <summary> Gets the percent progress. This will reset if readings exceed the length. </summary>
    ''' <value> The percent progress. </value>
    Public ReadOnly Property PercentProgress As Integer
        Get
            Dim minimumExpectedCount As Integer = Me.Length + 1
            ' this will get to 100% when hitting the minimum expected count.
            Dim count As Integer = Me.TotalReadingsCount Mod (minimumExpectedCount + 1)
            Return CInt(100 * count / minimumExpectedCount)
        End Get
    End Property

#End Region

End Class

''' <summary> Values that represent moving window update rules. </summary>
''' <remarks> David, 2020-09-23. </remarks>
Public Enum MovingWindowUpdateRule
    ''' <summary> An enum constant representing the none option. </summary>
    <Description("Not stopping")> None
    ''' <summary> An enum constant representing the stop on within window option. </summary>
    <Description("Stop On Within Window")> StopOnWithinWindow
End Enum

''' <summary> Values that represent moving window status. </summary>
''' <remarks> David, 2020-09-23. </remarks>
Public Enum MovingWindowStatus
    ''' <summary> An enum constant representing the none option. </summary>
    <Description("Not set")> None
    ''' <summary> An enum constant representing the filling option. </summary>
    <Description("Filling")> Filling
    ''' <summary> An enum constant representing the within option. </summary>
    <Description("Within Window")> Within
    ''' <summary> An enum constant representing the above option. </summary>
    <Description("Above Window")> Above
    ''' <summary> An enum constant representing the below option. </summary>
    <Description("Below window")> Below
End Enum

''' <summary> Values that represent reading status. </summary>
''' <remarks> David, 2020-09-23. </remarks>
Public Enum ReadingStatus
    ''' <summary> An enum constant representing the amenable option; the reading has a value that can be averaged. </summary>
    <Description("Amenable")> Amenable
    ''' <summary> An enum constant representing the non conformal option; the reading is outside the conformity range. </summary>
    <Description("Non Conformal")> NonConformal
    ''' <summary> An enum constant representing the overflow option; the reading is an overflow. </summary>
    <Description("Overflow")> Overflow
    ''' <summary> An enum constant representing the indeterminable option; for example, a value could not be read from the instrument. </summary>
    <Description("Indeterminable")> Indeterminable
    ''' <summary> An enum constant representing the Timeout option; for example, a value could not be read from the instrument withing a prescribed time. </summary>
    <Description("Timeout")> Timeout
End Enum

''' <summary> A moving window reading. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 4/26/2018 </para>
''' </remarks>
Public Class MovingWindowReading

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value">         The value. </param>
    ''' <param name="status">        The status. </param>
    ''' <param name="ordinalNumber"> The ordinal number. </param>
    Public Sub New(ByVal value As Double?, ByVal status As ReadingStatus, ByVal ordinalNumber As Integer)
        MyBase.New
        Me.Value = value
        Me.Status = status
        Me.OrdinalNumber = ordinalNumber
        Me.Timestamp = DateTimeOffset.Now
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="reading"> The reading. </param>
    Public Sub New(ByVal reading As MovingWindowReading)
        Me.New(MovingWindowReading.Validated(reading).Value, reading.Status, reading.OrdinalNumber)
        Me.InputTimespan = reading.InputTimespan
    End Sub

    ''' <summary> Validated the given reading. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="reading"> The reading. </param>
    ''' <returns> A reading. </returns>
    Public Shared Function Validated(ByVal reading As MovingWindowReading) As MovingWindowReading
        If reading Is Nothing Then Throw New ArgumentNullException(NameOf(reading))
        Return reading
    End Function

    ''' <summary> Gets or sets the value. </summary>
    ''' <value> The value. </value>
    Public ReadOnly Property Value As Double?

    ''' <summary> Gets or sets the status. </summary>
    ''' <value> The status. </value>
    Public ReadOnly Property Status As ReadingStatus

    ''' <summary> Gets or sets the timestamp. </summary>
    ''' <value> The timestamp. </value>
    Public ReadOnly Property Timestamp As DateTimeOffset

    ''' <summary> Gets or sets the ordinal number. </summary>
    ''' <value> The ordinal number. </value>
    Public ReadOnly Property OrdinalNumber As Integer

    ''' <summary> Gets or sets the input timespan. </summary>
    ''' <value> The input timespan. </value>
    Public Property InputTimespan As TimeSpan

End Class

