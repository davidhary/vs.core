Namespace EnumerableStats

    ''' <summary> A methods. </summary>
    ''' <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para> </remarks>
    Public Module Methods

        ''' <summary> Computes the histogram. </summary>
        ''' <remarks>
        ''' The first bin at X=<paramref name="lowerBound"/> counts all the values below the lower limit;
        ''' <para>
        ''' The second bin at X=<paramref name="lowerBound"/> + half the bin width counts the values
        ''' higher and equal to the lower limit but lower than the bin width;</para><para>
        ''' The next to last bin at X=<paramref name="upperBound"/> - half the bin width counts the
        ''' values at the last bin;</para><para>
        ''' The last bin at <paramref name="upperBound"/> counts the number of values equal or higher
        ''' than the high limit. </para>
        ''' </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source">     Source for the. </param>
        ''' <param name="lowerBound"> The lower bound. </param>
        ''' <param name="upperBound"> The upper bound. </param>
        ''' <param name="binCount">   Number of bins. </param>
        ''' <returns>
        ''' An enumerator that allows for each to be used to process histogram direct in this collection.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Histogram(ByVal source As IList(Of Double), ByVal lowerBound As Double, ByVal upperBound As Double,
                                  ByVal binCount As Integer) As IList(Of System.Windows.Point)
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Dim binWidth As Double = (upperBound - lowerBound) / binCount
            Dim inverseWidth As Double = 1 / binWidth
            Dim binCounts As Integer() = CType(Array.CreateInstance(GetType(Integer), binCount + 2), Integer())
            For Each x As Double In source
                Dim binValue As Double = inverseWidth * (x - lowerBound)
                Dim binNumber As Integer = If(binValue < 0, 0, If(binValue >= binCount, binCount + 1, 1 + CInt(Fix(binValue))))
                binCounts(binNumber) += 1
            Next
            Dim histF As New List(Of System.Windows.Point)
            Dim i As Integer = 0
            For Each binValue As Integer In binCounts
                Dim x As Double
                If i = 0 Then
                    x = lowerBound
                ElseIf i = binCounts.Count - 1 Then
                    x = upperBound
                Else
                    x = lowerBound + binWidth * (i - 0.5)
                End If
                histF.Add(New System.Windows.Point(x, binValue))
                i += 1
            Next
            Return histF
        End Function

        ''' <summary> Computes the histogram. </summary>
        ''' <remarks>
        ''' The first bin at X=<paramref name="lowerBound"/> counts all the values below the lower limit;
        ''' <para>
        ''' The second bin at X=<paramref name="lowerBound"/> + half the bin width counts the values
        ''' higher and equal to the lower limit but lower than the bin width;</para><para>
        ''' The next to last bin at X=<paramref name="upperBound"/> - half the bin width counts the
        ''' values at the last bin;</para><para>
        ''' The last bin at <paramref name="upperBound"/> counts the number of values equal or higher
        ''' than the high limit. </para>
        ''' </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source">     Source for the. </param>
        ''' <param name="lowerBound"> The lower bound. </param>
        ''' <param name="upperBound"> The upper bound. </param>
        ''' <param name="binCount">   Number of bins. </param>
        ''' <returns>
        ''' An enumerator that allows for each to be used to process histogram direct in this collection.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Histogram(ByVal source As Double(), ByVal lowerBound As Double, ByVal upperBound As Double,
                                  ByVal binCount As Integer) As IList(Of System.Windows.Point)
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Dim binWidth As Double = (upperBound - lowerBound) / binCount
            Dim inverseWidth As Double = 1 / binWidth
            Dim binCounts As Integer() = CType(Array.CreateInstance(GetType(Integer), binCount + 2), Integer())
            For Each x As Double In source
                Dim binValue As Double = inverseWidth * (x - lowerBound)
                Dim binNumber As Integer = If(binValue < 0, 0, If(binValue >= binCount, binCount + 1, 1 + CInt(Fix(binValue))))
                binCounts(binNumber) += 1
            Next
            Dim histF As New List(Of System.Windows.Point)
            Dim i As Integer = 0
            For Each binValue As Integer In binCounts
                Dim x As Double
                If i = 0 Then
                    x = lowerBound
                ElseIf i = binCounts.Count - 1 Then
                    x = upperBound
                Else
                    x = lowerBound + binWidth * (i - 0.5)
                End If
                histF.Add(New System.Windows.Point(x, binValue))
                i += 1
            Next
            Return histF
        End Function

    End Module

End Namespace
