''' <summary> Process control statistics. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 4/29/2015 </para>
''' </remarks>
Public Class ProcessControlStatistics

#Region " PROCESS CONTROL "

    ''' <summary> Gets the sentinel indicating if the process statistics were evaluated. </summary>
    ''' <value> <c>True</c> is statistics were evaluated; otherwise <c>False</c> . </value>
    Public ReadOnly Property HasValue As Boolean
        Get
            Return Me.Sigma > 0
        End Get
    End Property

    ''' <summary> Gets or sets capability ratio (Cp). </summary>
    ''' <value> The cp. </value>
    Public Property Cp As Double

    ''' <summary> Gets or sets the capability ratio minimal (Cpk). </summary>
    ''' <value> The capability ratio minimal (Cpk). </value>
    Public Property Cpk As Double

    ''' <summary> Gets or sets the capability ratio nominal (Cpm). </summary>
    ''' <value> The capability ratio nominal  (Cpm). </value>
    Public Property Cpm As Double

    ''' <summary> Gets or sets the sigma level. DSefaults to 6. </summary>
    ''' <value> The sigma level. </value>
    Public Property SigmaLevel As Double = 6

    ''' <summary> Gets or sets the sigma. </summary>
    ''' <value> The sigma. </value>
    '''
    ''' ### <exception cref="ArgumentNullException"> Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' ### <exception cref="ArgumentException">     Thrown when one or more arguments have
    '''                                              unsupported or illegal values. </exception>
    Public ReadOnly Property Sigma As Double

    ''' <summary> Gets or sets the mean. </summary>
    ''' <value> The mean value. </value>
    '''
    ''' ### <exception cref="ArgumentNullException"> Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' ### <exception cref="ArgumentException">     Thrown when one or more arguments have
    '''                                              unsupported or illegal values. </exception>
    Public ReadOnly Property Mean As Double

    ''' <summary> Gets or sets target value. </summary>
    ''' <value> The target value. </value>
    '''
    ''' ### <exception cref="ArgumentNullException"> Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' ### <exception cref="ArgumentException">     Thrown when one or more arguments have
    '''                                              unsupported or illegal values. </exception>
    Public ReadOnly Property TargetValue As Double

    ''' <summary> Gets or sets the lower specification limit. </summary>
    ''' <value> The lower specification limit. </value>
    '''
    ''' ### <exception cref="ArgumentNullException"> Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' ### <exception cref="ArgumentException">     Thrown when one or more arguments have
    '''                                              unsupported or illegal values. </exception>
    Public ReadOnly Property LowerSpecificationLimit As Double

    ''' <summary> Gets or sets the upper specification limit. </summary>
    ''' <value> The upper specification limit. </value>
    '''
    ''' ### <exception cref="ArgumentNullException"> Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' ### <exception cref="ArgumentException">     Thrown when one or more arguments have
    '''                                              unsupported or illegal values. </exception>
    Public ReadOnly Property UpperSpecificationLimit As Double

    ''' <summary> Calculates the process control capability ratios. </summary>
    ''' <remarks>
    ''' See http://www.mpcps.com/ for confidence intervals for the process control values. The ratio
    ''' of sigma to standard deviation is 1.25/0.8 at N=30 at a confidence level of 95% and 1.5,0.75
    ''' at 99.5%. N-30 is often used as the minimum sample size for getting decent values.
    ''' </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="targetValue">        Target value. </param>
    ''' <param name="specificationLimit"> The specification limit interval. </param>
    ''' <param name="mean">               The sample mean. </param>
    ''' <param name="sigma">              The sample sigma. </param>
    Public Sub Evaluate(ByVal targetValue As Double, ByVal specificationLimit As Interval, ByVal mean As Double, ByVal sigma As Double)
        If specificationLimit Is Nothing Then Throw New ArgumentNullException(NameOf(specificationLimit))
        Me.Evaluate(targetValue, specificationLimit.LowEndPoint, specificationLimit.HighEndPoint, mean, sigma)
    End Sub

    ''' <summary> Calculates the process control capability ratios. </summary>
    ''' <remarks> Values are set to <see cref="Double.PositiveInfinity"/> if sigma is zero. </remarks>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="targetValue">             Target value. </param>
    ''' <param name="lowerSpecificationLimit"> The lower specification limit. </param>
    ''' <param name="upperSpecificationLimit"> The upper specification limit. </param>
    ''' <param name="mean">                    The sample mean. </param>
    ''' <param name="sigma">                   The sample sigma. </param>
    Public Sub Evaluate(ByVal targetValue As Double,
                        ByVal lowerSpecificationLimit As Double, ByVal upperSpecificationLimit As Double,
                        ByVal mean As Double, ByVal sigma As Double)
        If upperSpecificationLimit <= lowerSpecificationLimit Then Throw New ArgumentException($"USL {upperSpecificationLimit} must be higher than LSL {lowerSpecificationLimit}", NameOf(upperSpecificationLimit))
        Me._Sigma = sigma
        Me._Mean = mean
        Me._TargetValue = targetValue
        Me._LowerSpecificationLimit = lowerSpecificationLimit
        Me._UpperSpecificationLimit = upperSpecificationLimit
        Me.Evaluate()
    End Sub

    ''' <summary> Calculates the process control capability ratios. </summary>
    ''' <remarks>
    ''' See http://www.mpcps.com/ for confidence intervals for the process control values. The ratio
    ''' of sigma to standard deviation is 1.25/0.8 at N=30 at a confidence level of 95% and 1.5,0.75
    ''' at 99.5%. N-30 is often used as the minimum sample size for getting decent values.
    ''' </remarks>
    Private Sub Evaluate()
        If Me.HasValue Then
            Dim sixSigma As Double = Me.SigmaLevel * Me.Sigma
            Me.Cp = (Me.UpperSpecificationLimit - Me.LowerSpecificationLimit) / sixSigma
            Me.Cpk = Math.Min(Me.UpperSpecificationLimit - Me.Mean, Me.Mean - Me.LowerSpecificationLimit) / (0.5 * sixSigma)
            Dim delta As Double = (Me.Mean - Me.TargetValue) / Me.Sigma
            Me.Cpm = Me.Cp / Math.Sqrt(1 + (delta * delta))
        End If
    End Sub

    ''' <summary> Evaluate cp. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="sigma">                   The sample sigma. </param>
    ''' <param name="upperSpecificationLimit"> The upper specification limit. </param>
    ''' <param name="lowerSpecificationLimit"> The lower specification limit. </param>
    ''' <returns> A Double? </returns>
    Public Shared Function EvaluateCp(ByVal sigma As Double, ByVal upperSpecificationLimit As Double, ByVal lowerSpecificationLimit As Double) As Double
        If sigma < 0 Then Throw New InvalidOperationException($"Sigma is {sigma}")
        Return ProcessControlStatistics.EvaluateCp(sigma, upperSpecificationLimit, lowerSpecificationLimit, 6)

    End Function

    ''' <summary> Evaluate cp. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="sigma">                   The sample sigma. </param>
    ''' <param name="upperSpecificationLimit"> The upper specification limit. </param>
    ''' <param name="lowerSpecificationLimit"> The lower specification limit. </param>
    ''' <param name="sigmaLevel">              The sigma level. </param>
    ''' <returns> A Double? </returns>
    Public Shared Function EvaluateCp(ByVal sigma As Double, ByVal upperSpecificationLimit As Double, ByVal lowerSpecificationLimit As Double,
                                      ByVal sigmaLevel As Double) As Double
        Return (upperSpecificationLimit - lowerSpecificationLimit) / (sigmaLevel * sigma)
    End Function

    ''' <summary> Evaluate cpk. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="mean">                    The sample mean. </param>
    ''' <param name="sigma">                   The sample sigma. </param>
    ''' <param name="upperSpecificationLimit"> The upper specification limit. </param>
    ''' <param name="lowerSpecificationLimit"> The lower specification limit. </param>
    ''' <param name="sigmaLevel">              The sigma level. </param>
    ''' <returns> A Double? </returns>
    Public Shared Function EvaluateCpk(ByVal mean As Double, ByVal sigma As Double,
                                      ByVal upperSpecificationLimit As Double, ByVal lowerSpecificationLimit As Double,
                                      ByVal sigmaLevel As Double) As Double
        If sigma < 0 Then Throw New InvalidOperationException($"Sigma is {sigma}")
        Return Math.Min(upperSpecificationLimit - mean, mean - lowerSpecificationLimit) / (0.5 * sigmaLevel * sigma)

    End Function

    ''' <summary> Evaluate cpk. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="mean">                    The sample mean. </param>
    ''' <param name="sigma">                   The sample sigma. </param>
    ''' <param name="upperSpecificationLimit"> The upper specification limit. </param>
    ''' <param name="lowerSpecificationLimit"> The lower specification limit. </param>
    ''' <returns> A Double? </returns>
    Public Shared Function EvaluateCpk(ByVal mean As Double, ByVal sigma As Double,
                                      ByVal upperSpecificationLimit As Double, ByVal lowerSpecificationLimit As Double) As Double
        Return ProcessControlStatistics.EvaluateCpk(mean, sigma, upperSpecificationLimit, lowerSpecificationLimit, 6)
    End Function

    ''' <summary> Evaluate cpm. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="mean">                    The sample mean. </param>
    ''' <param name="sigma">                   The sample sigma. </param>
    ''' <param name="upperSpecificationLimit"> The upper specification limit. </param>
    ''' <param name="lowerSpecificationLimit"> The lower specification limit. </param>
    ''' <param name="targetValue">             Target value. </param>
    ''' <param name="sigmaLevel">              The sigma level. </param>
    ''' <returns> A Double? </returns>
    Public Shared Function EvaluateCpm(ByVal mean As Double, ByVal sigma As Double,
                                      ByVal upperSpecificationLimit As Double, ByVal lowerSpecificationLimit As Double,
                                      ByVal targetValue As Double, ByVal sigmaLevel As Double) As Double
        If sigma < 0 Then Throw New InvalidOperationException($"Sigma is {sigma}")
        Dim delta As Double = (mean - targetValue) / sigma
        Return (upperSpecificationLimit - lowerSpecificationLimit) / (sigmaLevel * sigma * Math.Sqrt(1 + (delta * delta)))

    End Function

    ''' <summary> Evaluate cpm. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="mean">                    The sample mean. </param>
    ''' <param name="sigma">                   The sample sigma. </param>
    ''' <param name="upperSpecificationLimit"> The upper specification limit. </param>
    ''' <param name="lowerSpecificationLimit"> The lower specification limit. </param>
    ''' <param name="targetValue">             Target value. </param>
    ''' <returns> A Double? </returns>
    Public Shared Function EvaluateCpm(ByVal mean As Double, ByVal sigma As Double,
                                      ByVal upperSpecificationLimit As Double, ByVal lowerSpecificationLimit As Double,
                                      ByVal targetValue As Double) As Double
        Return ProcessControlStatistics.EvaluateCpm(mean, sigma, upperSpecificationLimit, lowerSpecificationLimit, targetValue, 6)
    End Function

#End Region

End Class

