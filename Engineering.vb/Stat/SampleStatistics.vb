''' <summary> Sample statistics. </summary>
''' <remarks>
''' (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 11/19/2014 </para>
''' </remarks>
Public Class SampleStatistics
    Implements ICloneable

#Region " CONSTRUCTOR "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Public Sub New()
        MyBase.New()
        Me.ValuesList = New List(Of Double)
        Me.ResetKnownStateThis()
    End Sub

    ''' <summary> The cloning constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As SampleStatistics)
        Me.New()
        If value IsNot Nothing Then
            Me.ClearKnownState()
            Me._Mean = value.Mean
            Me._Sigma = value.Sigma
            Me._Sum = value.Sum
            Me._SumSquareDeviations = value.SumSquareDeviations
            Me._Maximum = value.Maximum
            Me._Minimum = value.Minimum
            Me.ValuesArray = value.ValuesArray.ToArray
            Me.AddValues(value.ValuesList.ToArray)
        End If
    End Sub

    ''' <summary> Creates a new object that is a copy of the current instance. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> A new object that is a copy of this instance. </returns>
    Public Overridable Function Clone() As Object Implements System.ICloneable.Clone
        Return New SampleStatistics(Me)
    End Function

    ''' <summary> Clears values to their known (initial) state. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Private Sub ClearKnownStateThis()
        Me._Count = 0
        Me._Mean = 0
        Me._Sigma = 0
        Me._Sum = 0
        Me._SumSquareDeviations = 0
        Me._Maximum = Double.MinValue
        Me._Minimum = Double.MaxValue
        Me._ValuesArray = Array.Empty(Of Double)
        Me._ValuesArray = Array.Empty(Of Double)
        Me.ValuesList.Clear()
    End Sub

    ''' <summary> Clears values to their known (initial) state. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Public Overridable Sub ClearKnownState()
        Me.ClearKnownStateThis()
    End Sub

    ''' <summary> Resets to known (default/instantiated) state. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Private Sub ResetKnownStateThis()
        Me.ClearKnownStateThis()
    End Sub

    ''' <summary> Resets to known (default/instantiated) state. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Public Overridable Sub ResetKnownState()
        Me.ResetKnownStateThis()
    End Sub

#End Region

#Region " STATISTICS "
    ''' <summary> The maximum. </summary>
    Private _Maximum As Double

    ''' <summary> Gets the maximum. </summary>
    ''' <value> The maximum value. </value>
    Public ReadOnly Property Maximum As Double
        Get
            If Me.RangeRequired Then Me.EvaluateRange()
            Return Me._Maximum
        End Get
    End Property
    ''' <summary> The minimum. </summary>
    Private _Minimum As Double

    ''' <summary> Gets the minimum. </summary>
    ''' <value> The minimum value. </value>
    Public ReadOnly Property Minimum As Double
        Get
            If Me.RangeRequired Then Me.EvaluateRange()
            Return Me._Minimum
        End Get
    End Property
    ''' <summary> The mean. </summary>
    Private _Mean As Double

    ''' <summary> Gets the mean. </summary>
    ''' <value> The mean value. </value>
    Public ReadOnly Property Mean As Double
        Get
            If Me.MeanRequired Then Me.EvaluateMean()
            Return Me._Mean
        End Get
    End Property
    ''' <summary> The sigma. </summary>
    Private _Sigma As Double

    ''' <summary> Gets the sigma. </summary>
    ''' <value> The sigma. </value>
    Public ReadOnly Property Sigma As Double
        Get
            If Me.SigmaRequired Then Me.EvaluateSigma(Me.Mean)
            Return Me._Sigma
        End Get
    End Property
    ''' <summary> Number of. </summary>
    Private _Sum As Double

    ''' <summary> Gets the number of. </summary>
    ''' <value> The sum. </value>
    Public ReadOnly Property Sum As Double
        Get
            If Me.MeanRequired Then Me.EvaluateMean()
            Return Me._Sum
        End Get
    End Property
    ''' <summary> The sum square deviations. </summary>
    Private _SumSquareDeviations As Double

    ''' <summary> Gets the sum square deviations. </summary>
    ''' <value> The total number of square deviations. </value>
    Public ReadOnly Property SumSquareDeviations As Double
        Get
            If Me.SigmaRequired Then Me.EvaluateSigma(Me.Mean)
            Return Me._SumSquareDeviations
        End Get
    End Property

    ''' <summary> Returns true if the sample includes values. </summary>
    ''' <value> any. </value>
    Public ReadOnly Property Any As Boolean
        Get
            Return Me.Count > 0
        End Get
    End Property
    ''' <summary> Number of. </summary>
    Private _Count As Integer

    ''' <summary> Gets or sets the number of values. </summary>
    ''' <value> The count. </value>
    Public Property Count As Integer
        Get
            Return Me._Count
        End Get
        Protected Set(value As Integer)
            Me._Count = value
        End Set
    End Property

    ''' <summary> Gets the mean required. </summary>
    ''' <value> The mean required. </value>
    Protected Property MeanRequired As Boolean

    ''' <summary> Gets the statistics required. </summary>
    ''' <value> The statistics required. </value>
    Protected Property SigmaRequired As Boolean

    ''' <summary> Gets the range required. </summary>
    ''' <value> The range required. </value>
    Protected Property RangeRequired As Boolean

    ''' <summary> Gets the cast to array required. </summary>
    ''' <value> The cast to array required. </value>
    Protected Property CastToArrayRequired As Boolean

    ''' <summary>
    ''' Gets the values changed indicating the values were either added or removed.
    ''' </summary>
    ''' <value> The values changed. </value>
    Public ReadOnly Property ValuesChanged As Boolean
        Get
            Return Me.CastToArrayRequired
        End Get
    End Property

#End Region

#Region " VALUES "

    ''' <summary> Gets or sets an array of values. </summary>
    ''' <value> An array of values. </value>
    <CodeAnalysis.SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification:="<Pending>")>
    Public ReadOnly Property ValuesArray As Double()

    ''' <summary> Gets or sets the values. </summary>
    ''' <value> A list of values. </value>
    Public ReadOnly Property ValuesList As IList(Of Double)

    ''' <summary> Adds a value. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value"> The value. </param>
    Public Overridable Sub AddValue(ByVal value As Double)
        Me.MeanRequired = True
        Me.SigmaRequired = True
        Me.RangeRequired = True
        Me.CastToArrayRequired = True
        Me.ValuesList.Add(value)
        Me._Count += 1
    End Sub

    ''' <summary> Adds the values. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="values"> The values. </param>
    Public Sub AddValues(ByVal values As Double())
        If values IsNot Nothing Then
            Me.MeanRequired = True
            Me.SigmaRequired = True
            Me.RangeRequired = True
            Me.CastToArrayRequired = True
            For Each value As Double In values
                Me.ValuesList.Add(value)
                Me._Count += 1
            Next
        End If
    End Sub

    ''' <summary> Removes the value at the index. </summary>
    ''' <remarks> David, 9/7/2020. </remarks>
    ''' <param name="index"> Zero-based index of the values. </param>
    Public Overridable Sub RemoveValueAt(ByVal index As Integer)
        Me.MeanRequired = True
        Me.SigmaRequired = True
        Me.RangeRequired = True
        Me.CastToArrayRequired = True
        Me.ValuesList.RemoveAt(index)
        Me._Count -= 1
    End Sub

#End Region

#Region " EVALUATE "

    ''' <summary> Evaluate mean. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> A Double. </returns>
    Public Function EvaluateMean() As Double
        If Me.CastToArrayRequired Then Me._ValuesArray = Me.ValuesList.ToArray : Me.CastToArrayRequired = False
        If Me.MeanRequired Then
            Me._Sum = 0
            Me._Mean = 0
            If Me.RangeRequired Then
                If Me.Any Then
                    Me._Minimum = Me.ValuesArray(0)
                    Me._Maximum = Me._Minimum
                    For Each value As Double In Me.ValuesArray
                        If value > Me._Maximum Then
                            Me._Maximum = value
                        ElseIf value < Me._Minimum Then
                            Me._Minimum = value
                        End If
                        Me._Sum += value
                    Next
                Else
                    Me._Maximum = Double.MinValue
                    Me._Minimum = Double.MaxValue
                End If
                Me.RangeRequired = False
            Else
                If Me.Any Then
                    For Each value As Double In Me.ValuesArray
                        Me._Sum += value
                    Next
                End If
            End If
            Me._Mean = Me._Sum / Me.Count
            Me.MeanRequired = False
        End If
        Return Me._Mean
    End Function

    ''' <summary> Evaluate sigma. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="average"> The average. </param>
    ''' <returns> A Double. </returns>
    Public Function EvaluateSigma(ByVal average As Double) As Double
        If Me.CastToArrayRequired Then Me._ValuesArray = Me.ValuesList.ToArray : Me.CastToArrayRequired = False
        If Me.SigmaRequired Then
            Me._Sigma = 0
            Me._SumSquareDeviations = 0
            If Me.Any AndAlso Me.Count > 1 Then
                For Each v As Double In Me.ValuesArray
                    Me._SumSquareDeviations += (v - average) * (v - average)
                Next
                Me._Sigma = Math.Sqrt(Me._SumSquareDeviations / (Me.Count - 1))
            End If
            Me.SigmaRequired = False
        End If
        Return Me._Sigma
    End Function

    ''' <summary> Evaluates correlation coefficient. </summary>
    ''' <remarks> Assumes that the function values already exist. </remarks>
    ''' <param name="values"> The values. </param>
    ''' <returns> The correlation coefficient or coefficient of multiple determination. </returns>
    Public Function EvaluateCorrelationCoefficient(ByVal values As Double()) As Double
        Dim valuesSample As New SampleStatistics()
        valuesSample.AddValues(values)
        Dim favg As Double = valuesSample.Mean
        Dim fssq As Double = valuesSample.SumSquareDeviations

        Dim oavg As Double = Me.Mean
        Dim ossq As Double = Me.SumSquareDeviations
        Dim ofssq As Double = 0
        For i As Integer = 0 To Me.Count - 1
            ofssq += (Me.ValuesArray(i) - oavg) * (valuesSample.ValuesArray(i) - favg)
        Next
        Return ofssq / (Math.Sqrt(fssq) * Math.Sqrt(ossq))
    End Function

    ''' <summary> Evaluates this object. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Public Sub Evaluate()
        Me.EvaluateSigma(Me.Mean)
        Me.EvaluateRange()
    End Sub

    ''' <summary> Evaluate range. </summary>
    ''' <remarks> David, 9/7/2020. </remarks>
    Public Sub EvaluateRange()
        If Me.CastToArrayRequired Then Me._ValuesArray = Me.ValuesList.ToArray : Me.CastToArrayRequired = False
        If Me.RangeRequired Then
            If Me.Any Then
                Me._Minimum = Me.ValuesArray(0)
                Me._Maximum = Me._Minimum
                For Each value As Double In Me.ValuesArray
                    If value > Me._Maximum Then
                        Me._Maximum = value
                    ElseIf value < Me._Minimum Then
                        Me._Minimum = value
                    End If
                Next
            Else
                Me._Maximum = Double.MinValue
                Me._Minimum = Double.MaxValue
            End If
            Me.RangeRequired = False
        End If
    End Sub

#End Region

End Class

