Imports System.Runtime.CompilerServices

Namespace StatisticsExtensions

    Public Module Methods

        ''' <summary> Splits arrays values around the splitting value. </summary>
        ''' <remarks>
        ''' The function split implements the scanning action. The two scan pointers are used by the
        ''' calling program To discover where they crossed.
        ''' </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="a">              The array values to process. </param>
        ''' <param name="splittingValue"> The splitting value. </param>
        ''' <param name="leftScanIndex">  [in,out] the left scan index. </param>
        ''' <param name="rightScanIndex"> [in,out] the right scan index. </param>
        <Extension>
        Public Sub Split(ByVal a() As Double, ByVal splittingValue As Double, ByRef leftScanIndex As Integer, ByRef rightScanIndex As Integer)
            If a Is Nothing Then Throw New ArgumentNullException(NameOf(a))
            Do
                ' do the left scan
                Do While a(leftScanIndex) < splittingValue
                    leftScanIndex += 1
                Loop
                ' do the right scan
                Do While a(rightScanIndex) > splittingValue
                    rightScanIndex -= 1
                Loop
                ' Once the two scans stop, swap values if they are in the wrong part:
                Dim t As Double = a(leftScanIndex)
                a(leftScanIndex) = a(rightScanIndex)
                a(rightScanIndex) = t
                ' scan until the pointers cross:
            Loop While leftScanIndex < rightScanIndex
        End Sub

        ''' <summary>
        ''' Determines the median of the given values using a
        ''' <see cref="Split(Double(), Double, ByRef Integer, ByRef Integer)"/> function.
        ''' </summary>
        ''' <remarks>
        ''' https://www.i-programmer.info/babbages-bag/505-quick-median.html?start=1 This method will (on
        ''' average) find the median of n elements in a time proportional to 2n - which is much better
        ''' than performing a full sort. Notice that this will only give you median if n is odd and it
        ''' doesn't work at all if there are duplicate values in the array. If you are interested in
        ''' finding percentiles other than the median then all you have to do is change the initial value
        ''' of k. The value that you end up with in a[k] is always bigger than the number of element to
        ''' its left and smaller than the number of elements to its right.
        '''        For example, if you set k=n/3 then you will find a value that divides the array into
        '''        1/3rd, 2/3rds.
        ''' Don't bother to use this algorithm unless the number of values is large. For small arrays
        ''' almost any method will do and sorting is invariably simpler. Even when it isn't practical,
        ''' this quick median method is always beautiful.
        ''' </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="values"> The array values to process. </param>
        ''' <returns> The median value. </returns>
        <Extension>
        Public Function SplitMedian(ByVal values() As Double) As Integer
            If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
            Dim n As Integer = values.Length
            If n = 1 Then Return 0
            ' This initially sets the scan to the whole of the array and x to the middle value. 
            Dim leftScanIndex As Integer = 0
            Dim rightScanIndex As Integer = n - 1
            Dim k As Integer = n \ 2
            Dim i As Integer
            Dim j As Integer
            Do While leftScanIndex < rightScanIndex
                Dim x As Double = values(k)
                i = leftScanIndex
                j = rightScanIndex
                ' It then calls split repeatedly on appropriate portions of the array 
                ' until the two pointers meet in the middle of the array when the value in a[k] is the median.
                Methods.Split(values, x, i, j)
                If j <= k Then
                    leftScanIndex = i
                End If
                If i >= k Then
                    rightScanIndex = j
                End If
            Loop
            Return k
        End Function
    End Module

End Namespace
