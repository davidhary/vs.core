''' <summary> A sealed class defining the acceptance Interval. </summary>
''' <remarks>
''' Defines a class that encodes intervals based on offsets from a nominal value. (c) 2014
''' Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/20/2014, 2.1.5376. </para>
''' </remarks>
Public MustInherit Class AcceptanceInterval

    ''' <summary> Specialized default constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Protected Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Constructor for symmetric margin factors. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="code">         The code. </param>
    ''' <param name="marginFactor"> The margin factor. </param>
    ''' <param name="caption">      The caption. </param>
    Protected Sub New(ByVal code As String, ByVal marginFactor As Interval, ByVal caption As String)
        MyBase.New()
        Me.Code = code
        Me.RelativeInterval = If(marginFactor Is Nothing, isr.Core.Engineering.Interval.Empty, isr.Core.Engineering.Interval.CreateInstance(marginFactor))
        Me.Caption = caption
        Me.CompoundCaption = AcceptanceInterval.BuildCompoundCaptionFormat(code, caption)
        Me.Parsed = True
    End Sub

    ''' <summary> The clone Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value"> The value. </param>
    Protected Sub New(ByVal value As AcceptanceInterval)
        Me.New(value.Code, value.RelativeInterval, value.Caption)
    End Sub

    ''' <summary> Gets or sets the acceptance interval code. </summary>
    ''' <value> The code. </value>
    Public Property Code As String

    ''' <summary>
    ''' Gets or sets the sentinel indicating that the interval was parsed using the interval code.
    ''' </summary>
    ''' <value> The sentinel indicating that the interval was parsed. </value>
    Public Property Parsed As Boolean
    ''' <summary> The relative interval. </summary>
    Private _RelativeInterval As Interval

    ''' <summary>
    ''' Gets or sets the relative interval. This is the interval, which actual values depend on the
    ''' nominal value. The relative interval can be compared to the relative deviation from the
    ''' nominal value. For example, +/- 5%.
    ''' </summary>
    ''' <value> The margin factor. </value>
    Public Property RelativeInterval As Interval
        Get
            Return Me._RelativeInterval
        End Get
        Set(value As Interval)
            Me._RelativeInterval = value
            ' update the acceptance interval.
            Me.NominalValue = Me.NominalValue
        End Set
    End Property
    ''' <summary> The nominal value. </summary>
    Private _NominalValue As Double

    ''' <summary> Gets or sets the nominal value. </summary>
    ''' <remarks> This also updates the interval based on the relative interval. </remarks>
    ''' <value> The nominal value. </value>
    Public Property NominalValue As Double
        Get
            Return Me._NominalValue
        End Get
        Set(value As Double)
            Me._NominalValue = value
            Me.Interval = New NominalInterval(value * (1 + Me.RelativeInterval.LowEndPoint), value * (1 + Me.RelativeInterval.HighEndPoint))
        End Set
    End Property

    ''' <summary>
    ''' Gets the acceptance interval with its end points. The interval defines the actual lower and
    ''' upper levels.
    ''' </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The acceptance interval. </value>
    Public Property Interval As Interval

    ''' <summary> Query if this object is empty; that is the limits range is zero. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> <c>true</c> if empty; otherwise <c>false</c> </returns>
    Public Function IsEmpty() As Boolean
        Return Me.Interval.IsEmpty
    End Function

    ''' <summary> Gets the unknown code. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The unknown code. </value>
    Public Shared Property EmptyCode As String = String.Empty

    ''' <summary> Gets the sentinel indicating if the entered code is empty. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> <c>True</c> if empty code. </value>
    Public MustOverride ReadOnly Property IsEmptyCode As Boolean

    ''' <summary> Gets the unknown code. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The unknown code. </value>
    Public Shared Property UnknownCode As String = "?"

    ''' <summary> Gets the sentinel indicating if the entered code is unknown. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> <c>True</c> if unknown code. </value>
    Public MustOverride ReadOnly Property IsUnknownCode As Boolean

    ''' <summary> Gets the user code. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The user code. </value>
    Public Shared Property UserCode As String = "@"

    ''' <summary> Gets the sentinel indicating if the entered value is user defined. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> <c>True</c> if unknown code. </value>
    Public MustOverride ReadOnly Property IsUserCode As Boolean

    ''' <summary> Checks if the acceptance interval contains the value. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns>
    ''' <c>true</c> if the acceptance interval contains the value; false otherwise.
    ''' </returns>
    Public Function Contains(ByVal value As Double) As Boolean
        Return Me.Interval.Contains(value)
    End Function

    ''' <summary> Gets the caption. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The caption. </value>
    Public Property Caption As String

    ''' <summary> Gets the compound caption. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The compound caption. </value>
    Public Property CompoundCaption As String

    ''' <summary> Gets the compound caption format. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The compound caption format. </value>
    Public Shared Property CompoundCaptionFormat As String = "{0}: {1}"

    ''' <summary> Builds compound caption format. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="code">    The code. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <returns> A String. </returns>
    Public Shared Function BuildCompoundCaptionFormat(ByVal code As String, ByVal caption As String) As String
        Return If(String.Equals(code, AcceptanceInterval.EmptyCode),
            AcceptanceInterval.EmptyCode,
            String.Format(AcceptanceInterval.CompoundCaptionFormat, code, caption))
    End Function

End Class

''' <summary> Dictionary of acceptance intervals. </summary>
''' <remarks>
''' (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/22/2014 </para>
''' </remarks>
Public Class AcceptanceIntervalCollection
    Inherits Collections.ObjectModel.KeyedCollection(Of String, AcceptanceInterval)

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Gets key for item. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="item"> The item. </param>
    ''' <returns> The key for item. </returns>
    Protected Overrides Function GetKeyForItem(item As AcceptanceInterval) As String
        If item Is Nothing Then Throw New ArgumentNullException(NameOf(item))
        Return item.Code
    End Function

    ''' <summary> Populates the given values. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="values"> The values. </param>
    Public Overloads Sub Populate(ByVal values As AcceptanceIntervalCollection)
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        Me.Clear()
        For Each value As AcceptanceInterval In values
            Me.Add(value)
        Next
    End Sub
    ''' <summary> The compound captions. </summary>
    Private _CompoundCaptions As CompoundCaptionCollection

    ''' <summary> Gets the compound captions. </summary>
    ''' <value> The compound captions. </value>
    Public ReadOnly Property CompoundCaptions As CompoundCaptionCollection
        Get
            If Me._CompoundCaptions Is Nothing OrElse Me._CompoundCaptions.Count = 0 Then
                Me._CompoundCaptions = New CompoundCaptionCollection
                Me._CompoundCaptions.Populate(Me)
            End If
            Return Me._CompoundCaptions
        End Get
    End Property

End Class

''' <summary> Collection of compound captions. </summary>
''' <remarks>
''' (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 12/20/2014 </para>
''' </remarks>
Public Class CompoundCaptionCollection
    Inherits Collections.ObjectModel.Collection(Of KeyValuePair(Of String, String))

    ''' <summary> Adds code. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="code">            The code. </param>
    ''' <param name="compoundCaption"> The compound caption. </param>
    Public Overloads Sub Add(ByVal code As String, ByVal compoundCaption As String)
        Me.Add(New KeyValuePair(Of String, String)(code, compoundCaption))
    End Sub

    ''' <summary> Adds code. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="acceptanceInterval"> The acceptance interval to add. </param>
    Public Overloads Sub Add(ByVal acceptanceInterval As AcceptanceInterval)
        Me.Add(acceptanceInterval.Code, acceptanceInterval.CompoundCaption)
    End Sub

    ''' <summary> Populates the given values. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="values"> The values. </param>
    Public Overloads Sub Populate(ByVal values As AcceptanceIntervalCollection)
        Me.Clear()
        If values IsNot Nothing Then
            For Each value As AcceptanceInterval In values
                Me.Add(value)
            Next
        End If
    End Sub

    ''' <summary> Populates the given values. </summary>
    ''' <remarks> David, 2020-04-20. </remarks>
    ''' <param name="values">                The values. </param>
    ''' <param name="keyValueCaptionFormat"> The compound format. </param>
    Public Overloads Sub Populate(ByVal values As IDictionary(Of String, Double), ByVal keyValueCaptionFormat As String)
        Me.Clear()
        If values IsNot Nothing Then
            For Each value As KeyValuePair(Of String, Double) In values
                Me.Add(value.Key, String.Format(keyValueCaptionFormat, value.Key, value.Value))
            Next
        End If
    End Sub

End Class
