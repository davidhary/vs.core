''' <summary>
''' Defines the Temperature Coefficient of Resistance (TCR) acceptance interval.
''' </summary>
''' <remarks>
''' (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/22/2014 </para>
''' </remarks>
Public Class TcrAcceptanceInterval
    Inherits AcceptanceInterval

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="code"> The temperature coefficient code. </param>
    Public Sub New(ByVal code As String)
        MyBase.New()
        If String.IsNullOrWhiteSpace(code) Then
            Me.Parsed = False
        Else
            Dim info As TcrAcceptanceInterval = TcrAcceptanceInterval.Parse(code)
            Me.Parsed = info IsNot Nothing
            If Me.Parsed Then
                Me.Code = code
                Me.RelativeInterval = New TcrInterval(info.RelativeInterval)
                Me.Caption = info.Caption
                Me.CompoundCaption = AcceptanceInterval.BuildCompoundCaptionFormat(code, Me.Caption)
            End If
        End If
    End Sub

    ''' <summary> Constructor for symmetric interval. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="code">        The temperature coefficient code. </param>
    ''' <param name="coefficient"> The coefficient interval. </param>
    ''' <param name="caption">     The caption. </param>
    Public Sub New(ByVal code As String, ByVal coefficient As Interval, ByVal caption As String)
        MyBase.New(code, coefficient, caption)
    End Sub

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value"> The acceptance interval. </param>
    Protected Sub New(ByVal value As AcceptanceInterval)
        Me.New(value.Code, value.RelativeInterval.HighEndPoint, value.Caption)
    End Sub

    ''' <summary> Constructor for symmetric interval and standard caption. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="code">        The tolerance code. </param>
    ''' <param name="coefficient"> The coefficient  interval. </param>
    Public Sub New(ByVal code As String, ByVal coefficient As Interval)
        Me.New(code, coefficient, TcrAcceptanceInterval.BuildCaption(coefficient.HighEndPoint))
    End Sub

    ''' <summary> Constructor for symmetric interval and standard caption. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="code">        The tolerance code. </param>
    ''' <param name="coefficient"> The coefficient. </param>
    Public Sub New(ByVal code As String, ByVal coefficient As Double)
        Me.New(code, coefficient, TcrAcceptanceInterval.BuildCaption(coefficient))
    End Sub

    ''' <summary> Constructor for symmetric interval. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="code">        The temperature coefficient code. </param>
    ''' <param name="coefficient"> The coefficient. </param>
    ''' <param name="caption">     The caption. </param>
    Public Sub New(ByVal code As String, ByVal coefficient As Double, ByVal caption As String)
        Me.New(code, New TcrInterval(coefficient), caption)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="lowerCoefficient"> The lower Coefficient. </param>
    ''' <param name="upperCoefficient"> The upper Coefficient. </param>
    Public Sub New(ByVal lowerCoefficient As Double, ByVal upperCoefficient As Double)
        Me.New(TcrAcceptanceInterval.UserCode, New TcrInterval(lowerCoefficient, upperCoefficient), TcrAcceptanceInterval.BuildCaption(lowerCoefficient, upperCoefficient, TcrAcceptanceInterval.CaptionFormat))
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="code">             The temperature coefficient code. </param>
    ''' <param name="lowerCoefficient"> The lower Coefficient. </param>
    ''' <param name="upperCoefficient"> The upper Coefficient. </param>
    ''' <param name="caption">          The caption. </param>
    Public Sub New(ByVal code As String, ByVal lowerCoefficient As Double, ByVal upperCoefficient As Double, ByVal caption As String)
        Me.New(code, New TcrInterval(lowerCoefficient, upperCoefficient), caption)
    End Sub

    ''' <summary> The cloning constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As TcrAcceptanceInterval)
        Me.New(value.Code, value.RelativeInterval.LowEndPoint, value.RelativeInterval.HighEndPoint, value.Caption)
    End Sub

    ''' <summary> Gets the empty. </summary>
    ''' <value> The empty. </value>
    Public Shared ReadOnly Property Empty As TcrAcceptanceInterval
        Get
            Return New TcrAcceptanceInterval(TcrAcceptanceInterval.EmptyCode, 0)
        End Get
    End Property

    ''' <summary> Gets or sets the units caption. </summary>
    ''' <value> The units format. </value>
    Public Shared Property UnitsCaption As String = "ppm/°C"

    ''' <summary> Gets or sets the caption format. </summary>
    ''' <value> The caption format. </value>
    Public Shared Property CaptionFormat As String = "±{0} " & TcrAcceptanceInterval.UnitsCaption

    ''' <summary> Builds a caption. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns>
    ''' A String value using the
    ''' <see cref="TcrAcceptanceInterval.CaptionFormat">caption format</see>.
    ''' </returns>
    Public Shared Function BuildCaption(ByVal value As Double) As String
        Dim scaleFactor As Double = 1
        If TcrAcceptanceInterval.CaptionFormat.Contains("ppm") Then
            scaleFactor = 1000000.0
        End If
        Return TcrAcceptanceInterval.BuildCaption(value, scaleFactor, TcrAcceptanceInterval.CaptionFormat)
    End Function

    ''' <summary> Builds a caption. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value">       The value. </param>
    ''' <param name="scaleFactor"> The scale factor. </param>
    ''' <param name="format">      Describes the format to use. </param>
    ''' <returns>
    ''' A String value using the
    ''' <see cref="TcrAcceptanceInterval.CaptionFormat">caption format</see>.
    ''' </returns>
    Public Shared Function BuildCaption(ByVal value As Double, ByVal scaleFactor As Double, ByVal format As String) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, format, scaleFactor * value)
    End Function

    ''' <summary> The unknown Coefficient value. </summary>
    ''' <value> The unknown value. </value>
    Public Shared Property UnknownValue As TcrAcceptanceInterval = New TcrAcceptanceInterval("??", 0.00025)

    ''' <summary> Gets or sets the unknown code. </summary>
    ''' <value> The unknown code. </value>
    Public Overloads Shared Property UnknownCode As String
        Get
            Return UnknownValue.Code
        End Get
        Set(value As String)
            TcrAcceptanceInterval.UnknownValue = New TcrAcceptanceInterval(value, TcrAcceptanceInterval.UnknownValue.RelativeInterval)
        End Set
    End Property

    ''' <summary> The user coefficient value. </summary>
    ''' <value> The user value. </value>
    Public Shared Property UserValue As TcrAcceptanceInterval = New TcrAcceptanceInterval("@@", 0.001)

    ''' <summary> Gets or sets the user code. </summary>
    ''' <value> The user code. </value>
    Public Overloads Shared Property UserCode As String
        Get
            Return UserValue.Code
        End Get
        Set(value As String)
            TcrAcceptanceInterval.UserValue = New TcrAcceptanceInterval(value, TcrAcceptanceInterval.UserValue.RelativeInterval)
        End Set
    End Property
    ''' <summary> The dictionary. </summary>
    Private Shared _Dictionary As AcceptanceIntervalCollection

    ''' <summary> Gets the <see cref="AcceptanceIntervalCollection">dictionary</see>. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> A Dictionary(Of String, CodedIntervalBase). </returns>
    Public Shared Function Dictionary() As AcceptanceIntervalCollection
        If TcrAcceptanceInterval._Dictionary Is Nothing OrElse TcrAcceptanceInterval._Dictionary.Count = 0 Then
            TcrAcceptanceInterval.BuildDictionary()
        End If
        Return TcrAcceptanceInterval._Dictionary
    End Function

    ''' <summary> Builds coded interval base dictionary. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="values"> The values. </param>
    Public Shared Sub BuildDictionary(ByVal values As AcceptanceIntervalCollection)
        TcrAcceptanceInterval._Dictionary = New AcceptanceIntervalCollection
        TcrAcceptanceInterval._Dictionary.Populate(values)
    End Sub

    ''' <summary> Builds Coefficient information dictionary. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Private Shared Sub BuildDictionary()
        Dim dix As New AcceptanceIntervalCollection From {
            New TcrAcceptanceInterval("00", -0.000125, -0.000075, "-100±25 " & TcrAcceptanceInterval.UnitsCaption), ' Commercial
            New TcrAcceptanceInterval("01", 0.0001),  ' Commercial
            New TcrAcceptanceInterval("02", 0.00005), ' Commercial
            New TcrAcceptanceInterval("03", 0.000025), ' Commercial
            New TcrAcceptanceInterval("10", 0.00002), ' Commercial
            New TcrAcceptanceInterval("11", 0.000015), ' Commercial
            New TcrAcceptanceInterval("12", 0.00001), ' Commercial
            New TcrAcceptanceInterval("13", 0.000005), ' Screened
            New TcrAcceptanceInterval("04", 0.0003),  ' Screened
            New TcrAcceptanceInterval("05", 0.0001),  ' Screened
            New TcrAcceptanceInterval("06", 0.00005), ' Screened
            New TcrAcceptanceInterval("07", 0.00025), ' Screened
            New TcrAcceptanceInterval("14", 0.00002), ' Screened
            New TcrAcceptanceInterval("15", 0.000015), ' Screened
            New TcrAcceptanceInterval("16", 0.00001), ' Screened
            New TcrAcceptanceInterval("17", 0.000005), ' Screened
            New TcrAcceptanceInterval("99", 0.00025), ' Commercial
            New TcrAcceptanceInterval("08", 0.00025), ' Screened
            TcrAcceptanceInterval.UnknownValue
        }
        TcrAcceptanceInterval.BuildDictionary(dix)
    End Sub

    ''' <summary> Tries to parse a coded value. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="code">    The code. </param>
    ''' <param name="value">   [in,out] The Margin Factor. </param>
    ''' <param name="details"> [in,out] The details. </param>
    ''' <returns> <c>True</c> if Margin Factor code can be parsed. </returns>
    Public Shared Function TryParse(ByVal code As String, ByRef value As TcrAcceptanceInterval, ByRef details As String) As Boolean
        Dim result As (Success As Boolean, Value As TcrAcceptanceInterval, Details As String) = TcrAcceptanceInterval.TryParse(code)
        value = result.Value
        details = result.Details
        Return result.Success
    End Function

    ''' <summary> Tries to parse a coded value. </summary>
    ''' <remarks> David, 4/18/2020. </remarks>
    ''' <param name="code"> The code. </param>
    ''' <returns> <c>True</c> if Margin Factor code can be parsed. </returns>
    Public Shared Function TryParse(ByVal code As String) As (Success As Boolean, Value As TcrAcceptanceInterval, Details As String)
        Dim result As (Success As Boolean, Value As TcrAcceptanceInterval, Details As String)
        Dim acceptanceIntervals As AcceptanceIntervalCollection = TcrAcceptanceInterval.Dictionary
        result = If(acceptanceIntervals.Contains(code),
                (True, New TcrAcceptanceInterval(acceptanceIntervals.Item(code)), String.Empty),
                (False, TcrAcceptanceInterval.Empty(), $"{GetType(TcrAcceptanceInterval)}.{NameOf(TcrAcceptanceInterval.Code)} '{code}' is unknown"))
        Return result
    End Function

    ''' <summary> Parses. </summary>
    ''' <remarks> David, 2020-04-20. </remarks>
    ''' <param name="code"> The code. </param>
    ''' <returns> A TcrAcceptanceInterval. </returns>
    Public Shared Function Parse(ByVal code As String) As TcrAcceptanceInterval
        Dim acceptanceIntervals As AcceptanceIntervalCollection = TcrAcceptanceInterval.Dictionary
        Return If(acceptanceIntervals.Contains(code), New TcrAcceptanceInterval(acceptanceIntervals.Item(code)), Nothing)
    End Function

    ''' <summary> Gets the sentinel indicating if the entered value is Empty defined. </summary>
    ''' <value> <c>True</c> if unknown code. </value>
    Public Overrides ReadOnly Property IsEmptyCode As Boolean
        Get
            Return String.Equals(Me.Code, TcrAcceptanceInterval.EmptyCode, StringComparison.OrdinalIgnoreCase)
        End Get
    End Property

    ''' <summary> Gets the sentinel indicating if the entered code is unknown. </summary>
    ''' <value> <c>True</c> if unknown code. </value>
    Public Overrides ReadOnly Property IsUnknownCode As Boolean
        Get
            Return String.Equals(Me.Code, TcrAcceptanceInterval.UnknownCode, StringComparison.OrdinalIgnoreCase)
        End Get
    End Property

    ''' <summary> Gets the sentinel indicating if the entered value is user defined. </summary>
    ''' <value> <c>True</c> if unknown code. </value>
    Public Overrides ReadOnly Property IsUserCode As Boolean
        Get
            Return String.Equals(Me.Code, TcrAcceptanceInterval.UserCode, StringComparison.OrdinalIgnoreCase)
        End Get
    End Property

End Class

