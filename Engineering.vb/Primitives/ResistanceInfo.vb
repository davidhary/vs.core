''' <summary> Information about the resistance. </summary>
''' <remarks>
''' (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/19/2014 </para>
''' </remarks>
Public Class ResistanceInfo
    Inherits NominalInfo

#Region " CONSTRUCTION "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="resistanceCode"> The resistance code. </param>
    Public Sub New(ByVal resistanceCode As String)
        MyBase.New(resistanceCode)
    End Sub

#End Region

#Region " VALUES "

    ''' <summary> Gets or sets the nominal resistance. </summary>
    ''' <value> The nominal resistance. </value>
    Public Property NominalResistance As Double
        Get
            Return Me.NominalValue
        End Get
        Set(value As Double)
            Me.NominalValue = value
        End Set
    End Property

    ''' <summary> Gets or sets the resistance code. </summary>
    ''' <value> The resistance code. </value>
    Public Property ResistanceCode As String
        Get
            Return Me.ValueCode
        End Get
        Set(value As String)
            Me.ValueCode = value
        End Set
    End Property

    ''' <summary> Attempts to parse. </summary>
    ''' <remarks> David, 4/17/2020. </remarks>
    ''' <param name="valueCode"> The value code. </param>
    ''' <param name="value">     [in,out] The value. </param>
    ''' <param name="details">   [in,out] The details. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Overloads Shared Function TryParse(ByVal valueCode As String, ByRef value As Double, ByRef details As String) As Boolean
        Dim result As (Success As Boolean, Value As Double, Details As String) = NominalInfo.TryParse(valueCode)
        value = result.Value
        details = result.Details
        Return result.Success
    End Function

#End Region

End Class
