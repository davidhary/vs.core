''' <summary>
''' British Standard (BS EN 60062) Temperature coefficient acceptance intervals.
''' </summary>
''' <remarks>
''' This class is sealed to ensure that the hash value of its elements is not used
'''           by two instances with different hash value set. (c) 2014 Integrated Scientific
'''           Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/19/2014 </para>
''' </remarks>
Public NotInheritable Class BritishStadnardTcr
    ''' <summary> The ppm. </summary>
    Private Const _Ppm As String = "ppm"

    ''' <summary> Gets or sets the units caption. </summary>
    ''' <value> The units format. </value>
    Public Shared Property UnitsCaption As String = $"{BritishStadnardTcr._Ppm}/{Convert.ToChar(&H2070)}K"

    ''' <summary> Gets or sets the caption format. </summary>
    ''' <value> The caption format. </value>
    Public Shared Property CaptionFormat As String = $"±{{0}} {BritishStadnardTcr.UnitsCaption}"

    ''' <summary> Gets or sets the compound Key-Value caption format. </summary>
    ''' <value> The compound caption format. </value>
    Public Shared Property KeyValueCaptionFormat As String = $"{{0}}: ±{{1}} {BritishStadnardTcr.UnitsCaption}"

    ''' <summary> Builds a caption. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns>
    ''' A String value using the
    ''' <see cref="BritishStadnardTcr.CaptionFormat">caption format</see>.
    ''' </returns>
    Public Shared Function BuildCaption(ByVal value As Double) As String
        Dim scaleFactor As Double = 1
        If BritishStadnardTcr.CaptionFormat.Contains(BritishStadnardTcr._Ppm) Then
            scaleFactor = 1000000.0
        End If
        Return BritishStadnardTcr.BuildCaption(value, scaleFactor, BritishStadnardTcr.CaptionFormat)
    End Function

    ''' <summary> Builds a caption. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value">       The value. </param>
    ''' <param name="scaleFactor"> The scale factor. </param>
    ''' <param name="format">      Describes the format to use. </param>
    ''' <returns>
    ''' A String value using the
    ''' <see cref="BritishStadnardTcr.CaptionFormat">caption format</see>.
    ''' </returns>
    Public Shared Function BuildCaption(ByVal value As Double, ByVal scaleFactor As Double, ByVal format As String) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, format, scaleFactor * value)
    End Function

    ''' <summary> The unknown Coefficient value. </summary>
    ''' <value> The unknown value. </value>
    Public Shared Property UnknownValue As KeyValuePair(Of String, Double) = New KeyValuePair(Of String, Double)("?", 0.001)

    ''' <summary> Gets or sets the unknown code. </summary>
    ''' <value> The unknown code. </value>
    Public Shared Property UnknownCode As String
        Get
            Return UnknownValue.Key
        End Get
        Set(value As String)
            BritishStadnardTcr.UnknownValue = New KeyValuePair(Of String, Double)(value, BritishStadnardTcr.UnknownValue.Value)
        End Set
    End Property

    ''' <summary> The unknown Coefficient value. </summary>
    ''' <value> The user value. </value>
    Public Shared Property UserValue As KeyValuePair(Of String, Double) = New KeyValuePair(Of String, Double)("@", 0.001)

    ''' <summary> Gets or sets the user code. </summary>
    ''' <value> The user code. </value>
    Public Overloads Shared Property UserCode As String
        Get
            Return UserValue.Key
        End Get
        Set(value As String)
            BritishStadnardTcr.UserValue = New KeyValuePair(Of String, Double)(value, BritishStadnardTcr.UserValue.Value)
        End Set
    End Property
    ''' <summary> The dictionary. </summary>
    Private Shared _Dictionary As IDictionary(Of String, Double)

    ''' <summary> Gets the <see cref="AcceptanceIntervalCollection">dictionary</see>. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> A Dictionary(Of String, CodedIntervalBase). </returns>
    Public Shared Function Dictionary() As IDictionary(Of String, Double)
        If BritishStadnardTcr._Dictionary Is Nothing OrElse BritishStadnardTcr._Dictionary.Count = 0 Then
            BritishStadnardTcr.BuildDictionary()
        End If
        Return BritishStadnardTcr._Dictionary
    End Function

    ''' <summary> Builds coded interval base dictionary. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="values"> The values. </param>
    '''
    ''' ### <exception cref="ArgumentNullException"> Thrown when one or more required arguments are
    '''                                              null. </exception>
    Public Shared Sub BuildDictionary(ByVal values As IDictionary(Of String, Double))
        BritishStadnardTcr._Dictionary = New Dictionary(Of String, Double)
        For Each value As KeyValuePair(Of String, Double) In values
            BritishStadnardTcr._Dictionary.Add(value.Key, value.Value)
        Next
    End Sub

    ''' <summary> Builds Coefficient information dictionary. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Private Shared Sub BuildDictionary()
        Dim dix As New Dictionary(Of String, Double) From {
            {"K", 0.000001},
            {"M", 0.000005},
            {"N", 0.00001},
            {"Z", 0.00002},  'TO_DO _ not right.()
            {"Q", 0.000025},
            {"P", 0.000015},
            {"R", 0.00005},
            {"S", 0.0001},
            {"U", 0.00025},
            {"1", 0.0001},
            {"5", 0.0001},
            {"2", 0.00005},
            {"6", 0.00005},
            {"3", 0.000025},
            {"7", 0.000025},
            {"4", 0.00025},
            {BritishStadnardTcr.UnknownValue.Key, BritishStadnardTcr.UnknownValue.Value},
            {BritishStadnardTcr.UserValue.Key, BritishStadnardTcr.UserValue.Value}
        }
        BritishStadnardTcr.BuildDictionary(dix)
    End Sub
    ''' <summary> The compound captions. </summary>
    Private Shared _CompoundCaptions As CompoundCaptionCollection

    ''' <summary> Gets the compound captions. </summary>
    ''' <value> The compound captions. </value>
    Public Shared ReadOnly Property CompoundCaptions As CompoundCaptionCollection
        Get
            If BritishStadnardTcr._CompoundCaptions Is Nothing OrElse BritishStadnardTcr._CompoundCaptions.Count = 0 Then
                BritishStadnardTcr._CompoundCaptions = New CompoundCaptionCollection
                BritishStadnardTcr._CompoundCaptions.Populate(BritishStadnardTcr.Dictionary, BritishStadnardTcr.KeyValueCaptionFormat)
            End If
            Return BritishStadnardTcr._CompoundCaptions
        End Get
    End Property


End Class
