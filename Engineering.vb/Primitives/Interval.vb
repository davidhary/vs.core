''' <summary> Interval. </summary>
''' <remarks>
''' This class is set as Abstract to force setting the correct
''' <see cref="Interval.Epsilon">epsilon</see>. (c) 2014 Integrated Scientific Resources, Inc.
''' All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 11/25/2014 </para>
''' </remarks>
Public Class Interval
    Implements ICloneable

#Region " CONSTRUCTOR"

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Protected Sub New()
        MyBase.New()
        Me._LowEndPoint = 0
        Me._HighEndPoint = 0
        Me._Epsilon = 1.0E-18
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="lowEndPoint">  The low end point. </param>
    ''' <param name="highEndPoint"> The high end point. </param>
    ''' <param name="epsilon">      The epsilon. </param>
    Protected Sub New(ByVal lowEndPoint As Double, ByVal highEndPoint As Double, ByVal epsilon As Double)
        Me.New()
        Me._HighEndPoint = highEndPoint
        Me._LowEndPoint = lowEndPoint
        Me._Epsilon = epsilon
    End Sub

    ''' <summary> Creates the instance. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="lowEndPoint">  The low end point. </param>
    ''' <param name="highEndPoint"> The high end point. </param>
    ''' <param name="epsilon">      The epsilon. </param>
    ''' <returns> The new instance. </returns>
    Public Shared Function CreateInstance(ByVal lowEndPoint As Double, ByVal highEndPoint As Double, ByVal epsilon As Double) As Interval
        Return New Interval(lowEndPoint, highEndPoint, epsilon)
    End Function

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="lowEndPoint">  The low end point. </param>
    ''' <param name="highEndPoint"> The high end point. </param>
    ''' <param name="epsilon">      The epsilon. </param>
    Protected Sub New(ByVal lowEndPoint As Double?, ByVal highEndPoint As Double?, ByVal epsilon As Double)
        Me.New()
        Me._HighEndPoint = highEndPoint.GetValueOrDefault(0)
        Me._LowEndPoint = lowEndPoint.GetValueOrDefault(0)
        Me._Epsilon = epsilon
    End Sub

    ''' <summary> Creates the instance. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="lowEndPoint">  The low end point. </param>
    ''' <param name="highEndPoint"> The high end point. </param>
    ''' <param name="epsilon">      The epsilon. </param>
    ''' <returns> The new instance. </returns>
    Public Shared Function CreateInstance(ByVal lowEndPoint As Double?, ByVal highEndPoint As Double?, ByVal epsilon As Double) As Interval
        Return New Interval(lowEndPoint, highEndPoint, epsilon)
    End Function

    ''' <summary> Creates the instance. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> The new instance. </returns>
    Public Shared Function CreateInstance(ByVal value As Interval) As Interval
        Return New Interval(value)
    End Function

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value"> The value. </param>
    Protected Sub New(ByVal value As Interval)
        Me.New()
        If value IsNot Nothing Then
            Me._HighEndPoint = value.HighEndPoint
            Me._LowEndPoint = value.LowEndPoint
            Me._Epsilon = value.Epsilon
        End If
    End Sub

    ''' <summary> Gets the empty interval. </summary>
    ''' <value> The empty. </value>
    Public Shared ReadOnly Property Empty As Interval
        Get
            Return Interval.CreateInstance(0, 0, 0.000001)
        End Get
    End Property

    ''' <summary> Creates a new object that is a copy of the current instance. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> A new object that is a copy of this instance. </returns>
    Public Overridable Function Clone() As Object Implements System.ICloneable.Clone
        Return Interval.CreateInstance(Me)
    End Function

#End Region

#Region " MEMBERS "

    ''' <summary> Gets the epsilon. </summary>
    ''' <value> The epsilon [1E-18]. </value>
    Public Property Epsilon As Double

    ''' <summary> Gets the low end point. </summary>
    ''' <value> The low end point. </value>
    Public Property LowEndPoint As Double

    ''' <summary> Gets the high end point. </summary>
    ''' <value> The high end point. </value>
    Public Property HighEndPoint As Double

#End Region

#Region " FUNCTIONS "

    ''' <summary> Gets the size or length of the interval. </summary>
    ''' <value> The size or length of the interval. </value>
    Public ReadOnly Property Size As Double
        Get
            Return Me.HighEndPoint - Me.LowEndPoint
        End Get
    End Property

    ''' <summary> Gets the center. </summary>
    ''' <value> The center. </value>
    Public ReadOnly Property Center As Double
        Get
            Return 0.5 * (Me.HighEndPoint + Me.LowEndPoint)
        End Get
    End Property

    ''' <summary> Gets a value indicating whether this object is symmetrical. </summary>
    ''' <value> <c>true</c> if this object is symmetrical; otherwise <c>false</c>. </value>
    Public ReadOnly Property IsSymmetrical As Boolean
        Get
            Return Math.Abs(Me.Center) < Me.Epsilon
        End Get
    End Property

    ''' <summary> Gets a value indicating whether the interval is empty--having a zero size. </summary>
    ''' <value> <c>true</c> if the interval is empty; otherwise <c>false</c>. </value>
    Public ReadOnly Property IsEmpty As Boolean
        Get
            Return Me.Size < Me.Epsilon
        End Get
    End Property

    ''' <summary>
    ''' Checks if the specified point is contained in the interval including its end points.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns>
    ''' <c>true</c> if the point is contained in the interval including its endpoints; otherwise
    ''' <c>false</c>.
    ''' </returns>
    Public Function Contains(ByVal value As Double) As Boolean
        Return value >= Me.LowEndPoint AndAlso value <= Me.HighEndPoint
    End Function

    ''' <summary> Check if the value is inside the interval excluding its endpoints. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns>
    ''' <c>true</c> if the point is contained in the interval excluding its endpoints; otherwise
    ''' <c>false</c>.
    ''' </returns>
    Public Function Encloses(ByVal value As Double) As Boolean
        Return value > Me.LowEndPoint AndAlso value < Me.HighEndPoint
    End Function

    ''' <summary> Returns a string that represents the current object. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> A string that represents the current object. </returns>
    Public Overrides Function ToString() As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, "({0},{1})", Me.LowEndPoint, Me.HighEndPoint)
    End Function

    ''' <summary> Returns a string that represents the current object. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <returns> A string that represents the current object. </returns>
    Public Overloads Function ToString(ByVal format As String) As String
        Return $"({Me.LowEndPoint.ToString(format)},{Me.HighEndPoint.ToString(format)})"
    End Function

#End Region

#Region " EQUALS "

    ''' <summary>
    ''' Determines whether the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
    '''                    <see cref="T:System.Object" />. </param>
    ''' <returns>
    ''' <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />; otherwise, <c>False</c>.
    ''' </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return Me.Equals(TryCast(obj, Interval))
    End Function

    ''' <summary>
    ''' Compares two Intervals. The Intervals are compared using their LowerLimits and UpperLimits.
    ''' </summary>
    ''' <remarks>
    ''' The two Intervals are the same if the have the same minimum and maximum values.
    ''' </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Function Equals(ByVal value As Double) As Boolean
        Return Math.Abs(Me.HighEndPoint - value) < Me.Epsilon
    End Function

    ''' <summary>
    ''' Compares two Intervals. The Intervals are compared using their LowerLimits and UpperLimits.
    ''' </summary>
    ''' <remarks>
    ''' The two Intervals are the same if the have the same minimum and maximum values.
    ''' </remarks>
    ''' <param name="other"> Specifies the other <see cref="Interval">Interval</see>
    '''                      to compare for equality with this instance. </param>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Function Equals(ByVal other As Interval) As Boolean
        Return Me.Equals(other, Me.Epsilon)
    End Function

    ''' <summary>
    ''' Compares two Intervals. The Intervals are compared using their LowerLimits and UpperLimits.
    ''' </summary>
    ''' <remarks>
    ''' The two Intervals are the same if the have the same minimum and maximum values.
    ''' </remarks>
    ''' <param name="other">     Specifies the other <see cref="Interval">Interval</see>
    '''                          to compare for equality with this instance. </param>
    ''' <param name="tolerance"> Specifies the relative tolerance for comparing the two values. The
    '''                          values are compared based on their end points. </param>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Function Equals(ByVal other As Interval, ByVal tolerance As Double) As Boolean
        Return If(other Is Nothing,
            False,
            Math.Abs(other.HighEndPoint - Me.HighEndPoint) < tolerance AndAlso
                   Math.Abs(other.LowEndPoint - Me.LowEndPoint) < tolerance)
    End Function

    ''' <summary> Implements the operator =. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Overloads Shared Function Equals(ByVal left As Interval, ByVal right As Interval) As Boolean
        Return (CObj(left) Is CObj(right)) OrElse (left IsNot Nothing AndAlso left.Equals(right))
    End Function

    ''' <summary> Implements the operator =. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As Interval, ByVal right As Interval) As Boolean
        Return Interval.Equals(left, right)
    End Operator

    ''' <summary> Implements the operator &lt;&gt;. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(ByVal left As Interval, ByVal right As Interval) As Boolean
        Return Not Interval.Equals(left, right)
    End Operator

    ''' <summary> Creates a unique hash code. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> An <see cref="System.Int32">Int32</see> value. </returns>
    Public Overloads Overrides Function GetHashCode() As Int32
        Return Me.HighEndPoint.GetHashCode Xor Me.LowEndPoint.GetHashCode
    End Function

#End Region

End Class

''' <summary> Tcr interval. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 1/9/2015 </para>
''' </remarks>
Public Class TcrInterval
    Inherits Interval

#Region " CONSTRUCTOR"

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Public Sub New()
        MyBase.New()
        Me.Epsilon = TcrInterval.DefaultEpsilon
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="endPoint"> The end point of a symmetrical interval. </param>
    Public Sub New(ByVal endPoint As Double)
        MyBase.New(-endPoint, endPoint, TcrInterval.DefaultEpsilon)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="lowEndPoint">  The low end point. </param>
    ''' <param name="highEndPoint"> The high end point. </param>
    Public Sub New(ByVal lowEndPoint As Double, ByVal highEndPoint As Double)
        MyBase.New(lowEndPoint, highEndPoint, TcrInterval.DefaultEpsilon)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As TcrInterval)
        MyBase.New(value)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As Interval)
        MyBase.New(value)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="lowEndPoint">  The low end point. </param>
    ''' <param name="highEndPoint"> The high end point. </param>
    Public Sub New(ByVal lowEndPoint As Double?, ByVal highEndPoint As Double?)
        MyBase.New(lowEndPoint, highEndPoint, TcrInterval.DefaultEpsilon)
    End Sub

    ''' <summary> Creates a new object that is a copy of the current instance. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> A new object that is a copy of this instance. </returns>
    Public Overrides Function Clone() As Object
        Return New TcrInterval(Me)
    End Function

    ''' <summary> Gets the empty interval. </summary>
    ''' <value> The empty. </value>
    Public Overloads Shared ReadOnly Property Empty As Interval
        Get
            Return New TcrInterval(0, 0)
        End Get
    End Property


#End Region

    ''' <summary> Gets the default minimum detectable difference. </summary>
    ''' <value> The epsilon (1E-12). </value>
    Public Shared Property DefaultEpsilon As Double = 0.000000000001

End Class

''' <summary> Tolerance interval. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 1/9/2015 </para>
''' </remarks>
Public Class ToleranceInterval
    Inherits Interval

#Region " CONSTRUCTOR"

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Public Sub New()
        MyBase.New()
        Me.Epsilon = ToleranceInterval.DefaultEpsilon
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="endPoint"> The end point of a symmetrical interval. </param>
    Public Sub New(ByVal endPoint As Double)
        MyBase.New(-endPoint, endPoint, ToleranceInterval.DefaultEpsilon)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="lowEndPoint">  The low end point. </param>
    ''' <param name="highEndPoint"> The high end point. </param>
    Public Sub New(ByVal lowEndPoint As Double, ByVal highEndPoint As Double)
        MyBase.New(lowEndPoint, highEndPoint, ToleranceInterval.DefaultEpsilon)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As ToleranceInterval)
        MyBase.New(value)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As Interval)
        MyBase.New(value)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="lowEndPoint">  The low end point. </param>
    ''' <param name="highEndPoint"> The high end point. </param>
    Public Sub New(ByVal lowEndPoint As Double?, ByVal highEndPoint As Double?)
        MyBase.New(lowEndPoint, highEndPoint, ToleranceInterval.DefaultEpsilon)
    End Sub

    ''' <summary> Creates a new object that is a copy of the current instance. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> A new object that is a copy of this instance. </returns>
    Public Overrides Function Clone() As Object
        Return New ToleranceInterval(Me)
    End Function

    ''' <summary> Gets the empty interval. </summary>
    ''' <value> The empty. </value>
    Public Overloads Shared ReadOnly Property Empty As Interval
        Get
            Return New ToleranceInterval(0, 0)
        End Get
    End Property

#End Region

    ''' <summary> Gets the default minimum detectable difference. </summary>
    ''' <value> The epsilon (1E-9). </value>
    Public Shared Property DefaultEpsilon As Double = 0.000000001

End Class

''' <summary> Nominal interval. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 1/9/2015 </para>
''' </remarks>
Public Class NominalInterval
    Inherits Interval

#Region " CONSTRUCTOR"

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Public Sub New()
        MyBase.New()
        Me.Epsilon = NominalInterval.DefaultEpsilon
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="endPoint"> The end point of a symmetrical interval. </param>
    Public Sub New(ByVal endPoint As Double)
        MyBase.New(endPoint, -endPoint, NominalInterval.DefaultEpsilon)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="lowEndPoint">  The low end point. </param>
    ''' <param name="highEndPoint"> The high end point. </param>
    Public Sub New(ByVal lowEndPoint As Double, ByVal highEndPoint As Double)
        MyBase.New(lowEndPoint, highEndPoint, NominalInterval.DefaultEpsilon)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As NominalInterval)
        MyBase.New(value)
        Me.Epsilon = NominalInterval.DefaultEpsilon
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As Interval)
        MyBase.New(value)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="lowEndPoint">  The low end point. </param>
    ''' <param name="highEndPoint"> The high end point. </param>
    Public Sub New(ByVal lowEndPoint As Double?, ByVal highEndPoint As Double?)
        MyBase.New(lowEndPoint, highEndPoint, NominalInterval.DefaultEpsilon)
    End Sub

    ''' <summary> Creates a new object that is a copy of the current instance. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> A new object that is a copy of this instance. </returns>
    Public Overrides Function Clone() As Object
        Return New NominalInterval(Me)
    End Function

    ''' <summary> Gets the empty interval. </summary>
    ''' <value> The empty. </value>
    Public Overloads Shared ReadOnly Property Empty As Interval
        Get
            Return New NominalInterval(0, 0)
        End Get
    End Property

#End Region

    ''' <summary> Gets or sets the default minimum detectable difference. </summary>
    ''' <value> The epsilon (1E-6). </value>
    Public Shared Property DefaultEpsilon As Double = 0.000001

End Class

