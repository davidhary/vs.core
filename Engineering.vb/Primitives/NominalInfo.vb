''' <summary> A parser for electrical component <see cref="NominalInfo.ValueCode"/>. </summary>
''' <remarks>
''' (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/19/2014 </para>
''' </remarks>
Public Class NominalInfo

#Region " CONSTRUCTION "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Public Sub New()
        MyBase.New()
        Me._ValueCode = NominalInfo.EmptyCode
        Me._ParseDetails = String.Empty
        Me._ParseFailed = False
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="valueCode"> The component value code. </param>
    Public Sub New(ByVal valueCode As String)
        Me.New()
        Me._ValueCode = valueCode
        Me.ParseThis()
    End Sub

#End Region

#Region " VALUES (properties) "

    ''' <summary> Gets the parse details. </summary>
    ''' <value> The parse details. </value>
    Public Property ParseDetails As String

    ''' <summary> Gets a value indicating whether the parse failed. </summary>
    ''' <value> <c>true</c> if parse failed; otherwise <c>false</c>. </value>
    Public Property ParseFailed As Boolean

    ''' <summary> Gets the nominal Component. </summary>
    ''' <value> The nominal Component. </value>
    Public Property NominalValue As Double

    ''' <summary> Gets the nominal value code. </summary>
    ''' <value> The value code. </value>
    Public Property ValueCode As String

#End Region

#Region " EMPTY IMPLEMENTATION "

    ''' <summary> Gets the unknown code. </summary>
    ''' <value> The unknown code. </value>
    Public Shared Property EmptyCode As String = String.Empty

    ''' <summary> Gets the sentinel indicating if the entered code is empty. </summary>
    ''' <value> <c>True</c> if empty code. </value>
    Public ReadOnly Property IsEmptyCode As Boolean
        Get
            Return String.Equals(Me.ValueCode, NominalInfo.EmptyCode)
        End Get
    End Property

    ''' <summary> Gets the empty value. </summary>
    ''' <value> The empty. </value>
    Public Shared ReadOnly Property Empty As NominalInfo
        Get
            Return New NominalInfo()
        End Get
    End Property

#End Region

#Region " PARSE "

    ''' <summary> Query if 'valueCode' is valid. </summary>
    ''' <remarks> David, 4/17/2020. </remarks>
    ''' <param name="valueCode"> The component value code. </param>
    ''' <returns> True if valid, false if not. </returns>
    Public Shared Function IsValid(ByVal valueCode As String) As Boolean
        Return NominalInfo.TryParse(valueCode).Success
    End Function

    ''' <summary> Parses the <see cref="ValueCode">Component code</see>. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Private Sub ParseThis()
        Dim result As (Success As Boolean, Value As Double, Details As String) = NominalInfo.TryParse(Me.ValueCode)
        Me.ParseDetails = result.Details
        Me.ParseFailed = Not result.Success
        Me.NominalValue = result.Value
    End Sub

    ''' <summary> Parses the <see cref="ValueCode">Component code</see>. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Public Sub Parse()
        Me.ParseThis()
    End Sub

    ''' <summary> Parses the <see cref="ValueCode">component value code</see>. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="valueCode"> The component value code. </param>
    ''' <returns> A Double. </returns>
    Public Shared Function Parse(ByVal valueCode As String) As Double
        Return NominalInfo.TryParse(valueCode).Value
    End Function

    ''' <summary> Tries to parse the Component code. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="valueCode"> The Component value code. </param>
    ''' <returns> <c>true</c> if Component code can be parsed. </returns>
    Public Shared Function TryParse(ByVal valueCode As String) As (Success As Boolean, Value As Double, Details As String)
        Dim result As (Success As Boolean, Value As Double, Details As String) = (True, 0, String.Empty)
        If String.IsNullOrWhiteSpace(valueCode) Then
            result.Success = False
            result.Details = "Value code is empty."
        Else
            Dim scaleFactors As Double() = New Double() {1, 1, 1000, 1000000, 1000000000}
            Dim scaleCodes As String() = New String() {".", "R", "K", "M", "G"}
            Dim scaleCode As String = String.Empty
            Dim scaleFactor As Double = 0
            Dim unscaledValue As String = String.Empty

            ' lookup the scale code:
            For i As Integer = 0 To scaleCodes.Length - 1
                scaleCode = scaleCodes(i)
                If valueCode.IndexOf(scaleCode, StringComparison.OrdinalIgnoreCase) >= 0 Then
                    scaleFactor = scaleFactors(i)
                    Exit For
                End If
            Next

            If scaleFactor > 0 Then
                ' found scale code in Component code, get the unscaled value
                unscaledValue = valueCode.Replace(scaleCode, ".")
                ' remove trailing periods to use cases such as 100.0R
                If unscaledValue.EndsWith(".", StringComparison.OrdinalIgnoreCase) Then unscaledValue = unscaledValue.Substring(0, unscaledValue.Length - 1)
            ElseIf valueCode.Length >= 3 Then
                ' no scale code in Component code; check if the Component code has at least 4 Components.
                ' then the scale power is the last digit.
                Dim scaler As Integer = 0
                If Integer.TryParse(valueCode.Substring(valueCode.Length - 1), scaler) Then
                    scaleFactor = Math.Pow(10, scaler)
                    unscaledValue = valueCode.Substring(0, valueCode.Length - 1)
                End If
            End If

            If String.IsNullOrWhiteSpace(unscaledValue) Then
                result.Details = $"Unknown Component code '{valueCode}'"
            ElseIf Double.TryParse(unscaledValue, result.Value) Then
                result.Value *= scaleFactor
                result.Success = True
                result.Details = String.Empty
            Else
                result.Details = $"Failed converting '{unscaledValue}' created from '{valueCode}' to nominal Component"
            End If
        End If
        Return result
    End Function

#End Region

End Class
