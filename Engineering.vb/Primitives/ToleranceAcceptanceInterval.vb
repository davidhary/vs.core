''' <summary> A tolerance acceptance interval. </summary>
''' <remarks>
''' This class is sealed to ensure that the hash value of its elements is not used
'''           by two instances with different hash value set. (c) 2014 Integrated Scientific
'''           Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/19/2014 </para>
''' </remarks>
Public Class ToleranceAcceptanceInterval
    Inherits AcceptanceInterval

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="code"> The tolerance code. </param>
    Public Sub New(ByVal code As String)
        MyBase.New()
        If String.IsNullOrWhiteSpace(code) Then
            Me.Parsed = False
        Else
            Dim info As ToleranceAcceptanceInterval = ToleranceAcceptanceInterval.Parse(code)
            Me.Parsed = info IsNot Nothing
            If Me.Parsed Then
                Me.Code = code
                Me.RelativeInterval = New ToleranceInterval(info.RelativeInterval)
                Me.Caption = info.Caption
                Me.CompoundCaption = AcceptanceInterval.BuildCompoundCaptionFormat(code, Me.Caption)
            End If
        End If
    End Sub

    ''' <summary> Constructor for symmetric interval. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="code">      The tolerance code. </param>
    ''' <param name="tolerance"> The tolerance interval. </param>
    ''' <param name="caption">   The caption. </param>
    Public Sub New(ByVal code As String, ByVal tolerance As Interval, ByVal caption As String)
        MyBase.New(code, tolerance, caption)
    End Sub

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As AcceptanceInterval)
        Me.New(value.Code, value.RelativeInterval, value.Caption)
    End Sub

    ''' <summary> Constructor for symmetric interval and standard caption. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="code">      The tolerance code. </param>
    ''' <param name="tolerance"> The tolerance. </param>
    Public Sub New(ByVal code As String, ByVal tolerance As Interval)
        Me.New(code, tolerance, ToleranceAcceptanceInterval.BuildCaption(tolerance.HighEndPoint))
    End Sub

    ''' <summary> Constructor for symmetric interval and standard caption. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="code">      The tolerance code. </param>
    ''' <param name="tolerance"> The tolerance. </param>
    Public Sub New(ByVal code As String, ByVal tolerance As Double)
        Me.New(code, tolerance, ToleranceAcceptanceInterval.BuildCaption(tolerance))
    End Sub

    ''' <summary> Constructor for symmetric interval. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="code">      The tolerance code. </param>
    ''' <param name="tolerance"> The tolerance. </param>
    ''' <param name="caption">   The caption. </param>
    Public Sub New(ByVal code As String, ByVal tolerance As Double, ByVal caption As String)
        Me.New(code, New ToleranceInterval(tolerance), caption)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="lowerTolerance"> The lower tolerance. </param>
    ''' <param name="upperTolerance"> The upper tolerance. </param>
    ''' <param name="caption">        The caption. </param>
    Public Sub New(ByVal lowerTolerance As Double, ByVal upperTolerance As Double, ByVal caption As String)
        Me.New(ToleranceAcceptanceInterval.UserCode, New ToleranceInterval(lowerTolerance, upperTolerance), caption)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="code">           The tolerance code. </param>
    ''' <param name="lowerTolerance"> The lower tolerance. </param>
    ''' <param name="upperTolerance"> The upper tolerance. </param>
    ''' <param name="caption">        The caption. </param>
    Public Sub New(ByVal code As String, ByVal lowerTolerance As Double, ByVal upperTolerance As Double, ByVal caption As String)
        Me.New(code, New ToleranceInterval(lowerTolerance, upperTolerance), caption)
    End Sub

    ''' <summary> The clone Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As ToleranceAcceptanceInterval)
        Me.New(value.Code, value.RelativeInterval, value.Caption)
    End Sub

    ''' <summary> Gets the empty value. </summary>
    ''' <value> The empty. </value>
    Public Shared ReadOnly Property Empty As ToleranceAcceptanceInterval
        Get
            Return New ToleranceAcceptanceInterval(ToleranceAcceptanceInterval.EmptyCode, 0)
        End Get
    End Property

    ''' <summary> Gets or sets the caption format. </summary>
    ''' <value> The caption format. </value>
    Public Shared Property CaptionFormat As String = "±{0}%"

    ''' <summary> Builds a caption. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns>
    ''' A String value using the
    ''' <see cref="ToleranceAcceptanceInterval.CaptionFormat">caption format</see>.
    ''' </returns>
    Public Shared Function BuildCaption(ByVal value As Double) As String
        Dim scaleFactor As Double = 1
        If ToleranceAcceptanceInterval.CaptionFormat.Contains("%}") Then
            scaleFactor = 1
        ElseIf ToleranceAcceptanceInterval.CaptionFormat.Contains("%") Then
            scaleFactor = 100
        End If
        Return BritishStadnardTcr.BuildCaption(value, scaleFactor, ToleranceAcceptanceInterval.CaptionFormat)
    End Function

    ''' <summary> Builds a caption. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value">       The value. </param>
    ''' <param name="scaleFactor"> The scale factor. </param>
    ''' <param name="format">      Describes the format to use. </param>
    ''' <returns>
    ''' A String value using the
    ''' <see cref="ToleranceAcceptanceInterval.CaptionFormat">caption format</see>.
    ''' </returns>
    Public Shared Function BuildCaption(ByVal value As Double, ByVal scaleFactor As Double, ByVal format As String) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, format, scaleFactor * value)
    End Function

    ''' <summary> The user tolerance value. </summary>
    ''' <value> The user value. </value>
    Public Shared Property UserValue As ToleranceAcceptanceInterval = New ToleranceAcceptanceInterval("@", 0.99)

    ''' <summary> The unknown tolerance value. </summary>
    ''' <value> The unknown value. </value>
    Public Shared Property UnknownValue As ToleranceAcceptanceInterval = New ToleranceAcceptanceInterval("Z", 0.99)

    ''' <summary> Gets or sets the unknown code. </summary>
    ''' <value> The unknown code. </value>
    Public Overloads Shared Property UnknownCode As String
        Get
            Return UnknownValue.Code
        End Get
        Set(value As String)
            ToleranceAcceptanceInterval.UnknownValue = New ToleranceAcceptanceInterval(value, ToleranceAcceptanceInterval.UnknownValue.RelativeInterval)
        End Set
    End Property

    ''' <summary> Gets or sets the user code. </summary>
    ''' <value> The user code. </value>
    Public Overloads Shared Property UserCode As String
        Get
            Return UserValue.Code
        End Get
        Set(value As String)
            ToleranceAcceptanceInterval.UserValue = New ToleranceAcceptanceInterval(value, ToleranceAcceptanceInterval.UserValue.RelativeInterval)
        End Set
    End Property
    ''' <summary> The dictionary. </summary>
    Private Shared _Dictionary As AcceptanceIntervalCollection

    ''' <summary> Gets the <see cref="AcceptanceIntervalCollection">dictionary</see>. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> A Dictionary(Of String, CodedIntervalBase). </returns>
    Public Shared Function Dictionary() As AcceptanceIntervalCollection
        If ToleranceAcceptanceInterval._Dictionary Is Nothing OrElse ToleranceAcceptanceInterval._Dictionary.Count = 0 Then
            ToleranceAcceptanceInterval.BuildDictionary()
        End If
        Return ToleranceAcceptanceInterval._Dictionary
    End Function

    ''' <summary> Builds coded interval base dictionary. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="values"> The values. </param>
    Public Shared Sub BuildDictionary(ByVal values As AcceptanceIntervalCollection)
        ToleranceAcceptanceInterval._Dictionary = New AcceptanceIntervalCollection
        ToleranceAcceptanceInterval._Dictionary.Populate(values)
    End Sub

    ''' <summary> Builds tolerance information dictionary. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Private Shared Sub BuildDictionary()
        Dim dix As New AcceptanceIntervalCollection From {
            New ToleranceAcceptanceInterval("A", 0.0005),
            New ToleranceAcceptanceInterval("B", 0.001),
            New ToleranceAcceptanceInterval("C", 0.0025),
            New ToleranceAcceptanceInterval("D", 0.005),
            New ToleranceAcceptanceInterval("F", 0.01),
            New ToleranceAcceptanceInterval("G", 0.02),
            New ToleranceAcceptanceInterval("H", 0.03),
            New ToleranceAcceptanceInterval("J", 0.05),
            New ToleranceAcceptanceInterval("K", 0.1),
            New ToleranceAcceptanceInterval("M", 0.2),
            New ToleranceAcceptanceInterval("P", 0.0015),
            New ToleranceAcceptanceInterval("Q", 0.0002),
            New ToleranceAcceptanceInterval("R", 0.002),
            New ToleranceAcceptanceInterval("S", 0.00025),
            New ToleranceAcceptanceInterval("T", 0.0001),
            New ToleranceAcceptanceInterval("U", 0.0003),
            New ToleranceAcceptanceInterval("V", 0.00005),
            New ToleranceAcceptanceInterval("Y", 0.00015),
            ToleranceAcceptanceInterval.UnknownValue,
            ToleranceAcceptanceInterval.UserValue
        }
        ToleranceAcceptanceInterval.BuildDictionary(dix)
    End Sub

    ''' <summary> Parses. </summary>
    ''' <remarks> David, 2020-04-20. </remarks>
    ''' <param name="code"> The code. </param>
    ''' <returns> A ToleranceAcceptanceInterval. </returns>
    Public Shared Function Parse(ByVal code As String) As ToleranceAcceptanceInterval
        Dim acceptanceIntervals As AcceptanceIntervalCollection = ToleranceAcceptanceInterval.Dictionary
        Return If(acceptanceIntervals.Contains(code), New ToleranceAcceptanceInterval(acceptanceIntervals.Item(code)), Nothing)
    End Function

    ''' <summary> Tries to parse a coded value. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="code">    The code. </param>
    ''' <param name="value">   [in,out] The Margin Factor. </param>
    ''' <param name="details"> [in,out] The details. </param>
    ''' <returns> <c>True</c> if Margin Factor code can be parsed. </returns>
    Public Shared Function TryParse(ByVal code As String, ByRef value As ToleranceAcceptanceInterval, ByRef details As String) As Boolean
        Dim result As (Success As Boolean, Value As ToleranceAcceptanceInterval, Details As String) = TryParse(code)
        value = result.Value
        details = result.Details
        Return result.Success
    End Function

    ''' <summary> Tries to parse a coded value. </summary>
    ''' <remarks> David, 4/18/2020. </remarks>
    ''' <param name="code"> The code. </param>
    ''' <returns> <c>True</c> if Margin Factor code can be parsed. </returns>
    Public Shared Function TryParse(ByVal code As String) As (Success As Boolean, Value As ToleranceAcceptanceInterval, Details As String)
        Dim result As (Success As Boolean, Value As ToleranceAcceptanceInterval, Details As String)
        Dim acceptanceIntervals As AcceptanceIntervalCollection = ToleranceAcceptanceInterval.Dictionary
        result = If(acceptanceIntervals.Contains(code),
                (True, New ToleranceAcceptanceInterval(acceptanceIntervals.Item(code)), String.Empty),
                (False, ToleranceAcceptanceInterval.Empty(), $"{GetType(ToleranceAcceptanceInterval)}.{NameOf(ToleranceAcceptanceInterval.Code)} '{code}' is unknown"))
        Return result
    End Function

    ''' <summary>
    ''' Searches for the first tolerance information matching the specific tolerance.
    ''' </summary>
    ''' <remarks> David, 4/18/2020. </remarks>
    ''' <param name="tolerance"> The tolerance value to find. </param>
    ''' <param name="epsilon">   The epsilon. </param>
    ''' <returns> The found tolerance information. </returns>
    Public Shared Function Find(ByVal tolerance As Double, ByVal epsilon As Double) As (Success As Boolean, Value As ToleranceAcceptanceInterval)
        Dim result As (Success As Boolean, Value As ToleranceAcceptanceInterval) = (False, ToleranceAcceptanceInterval.Empty)
        For Each toleranceAcceptInterval As ToleranceAcceptanceInterval In ToleranceAcceptanceInterval.Dictionary
            If Math.Abs(tolerance - toleranceAcceptInterval.RelativeInterval.HighEndPoint) <= epsilon Then
                result = (True, toleranceAcceptInterval)
            End If
        Next
        Return result
    End Function

    ''' <summary> Gets the sentinel indicating if the entered value is Empty defined. </summary>
    ''' <value> <c>True</c> if unknown code. </value>
    Public Overrides ReadOnly Property IsEmptyCode As Boolean
        Get
            Return String.Equals(Me.Code, ToleranceAcceptanceInterval.EmptyCode, StringComparison.OrdinalIgnoreCase)
        End Get
    End Property

    ''' <summary> Gets the sentinel indicating if the entered code is unknown. </summary>
    ''' <value> <c>True</c> if unknown code. </value>
    Public Overrides ReadOnly Property IsUnknownCode As Boolean
        Get
            Return String.Equals(Me.Code, ToleranceAcceptanceInterval.UnknownCode, StringComparison.OrdinalIgnoreCase)
        End Get
    End Property

    ''' <summary> Gets the sentinel indicating if the entered value is user defined. </summary>
    ''' <value> <c>True</c> if unknown code. </value>
    Public Overrides ReadOnly Property IsUserCode As Boolean
        Get
            Return String.Equals(Me.Code, ToleranceAcceptanceInterval.UserCode, StringComparison.OrdinalIgnoreCase)
        End Get
    End Property

End Class
