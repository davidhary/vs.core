''' <summary>
''' British Standard (BS EN 60062) Temperature coefficient acceptance intervals.
''' </summary>
''' <remarks>
''' This class is sealed to ensure that the hash value of its elements is not used
'''           by two instances with different hash value set. (c) 2014 Integrated Scientific
'''           Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/19/2014 </para>
''' </remarks>
Public NotInheritable Class BritishStadnardTcrInterval
    Inherits AcceptanceInterval

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="code"> The temperature coefficient code. </param>
    Public Sub New(ByVal code As String)
        MyBase.New()
        If String.IsNullOrWhiteSpace(code) Then
            Me.Parsed = False
        Else
            Dim info As BritishStadnardTcrInterval = BritishStadnardTcrInterval.Parse(code)
            Me.Parsed = info IsNot Nothing
            If Me.Parsed Then
                Me.Code = code
                Me.RelativeInterval = New TcrInterval(info.RelativeInterval)
                Me.Caption = info.Caption
                Me.CompoundCaption = AcceptanceInterval.BuildCompoundCaptionFormat(code, Me.Caption)
            End If
        End If
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="code">             The temperature coefficient code. </param>
    ''' <param name="lowerCoefficient"> The lower Coefficient. </param>
    ''' <param name="upperCoefficient"> The upper Coefficient. </param>
    ''' <param name="caption">          The caption. </param>
    Public Sub New(ByVal code As String, ByVal lowerCoefficient As Double, ByVal upperCoefficient As Double, ByVal caption As String)
        MyBase.New(code, New TcrInterval(lowerCoefficient, upperCoefficient), caption)
    End Sub

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As AcceptanceInterval)
        Me.New(value.Code, value.RelativeInterval, value.Caption)
    End Sub

    ''' <summary> Constructor for symmetric range and standard caption. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="code">        The tolerance code. </param>
    ''' <param name="coefficient"> The coefficient. </param>
    Public Sub New(ByVal code As String, ByVal coefficient As Interval)
        MyBase.New(code, coefficient, BritishStadnardTcr.BuildCaption(coefficient.HighEndPoint))
    End Sub

    ''' <summary> Constructor for symmetric range. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="code">        The temperature coefficient code. </param>
    ''' <param name="coefficient"> The coefficient. </param>
    ''' <param name="caption">     The caption. </param>
    Public Sub New(ByVal code As String, ByVal coefficient As Interval, ByVal caption As String)
        MyBase.New(code, coefficient, caption)
    End Sub

    ''' <summary> Constructor for symmetric range and standard caption. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="code">        The tolerance code. </param>
    ''' <param name="coefficient"> The coefficient. </param>
    Public Sub New(ByVal code As String, ByVal coefficient As Double)
        Me.New(code, coefficient, BritishStadnardTcr.BuildCaption(coefficient))
    End Sub

    ''' <summary> Constructor for symmetric range. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="code">        The temperature coefficient code. </param>
    ''' <param name="coefficient"> The coefficient. </param>
    ''' <param name="caption">     The caption. </param>
    Public Sub New(ByVal code As String, ByVal coefficient As Double, ByVal caption As String)
        Me.New(code, New ToleranceInterval(coefficient), caption)
    End Sub

    ''' <summary> The clone Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As BritishStadnardTcrInterval)
        Me.New(value.Code, value.RelativeInterval, value.Caption)
    End Sub

    ''' <summary> Gets the empty value. </summary>
    ''' <value> The empty. </value>
    Public Shared ReadOnly Property Empty As BritishStadnardTcrInterval
        Get
            Return New BritishStadnardTcrInterval(BritishStadnardTcrInterval.EmptyCode, 0)
        End Get
    End Property
    ''' <summary> The dictionary. </summary>
    Private Shared _Dictionary As AcceptanceIntervalCollection

    ''' <summary> Gets the <see cref="AcceptanceIntervalCollection">dictionary</see>. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> A Dictionary(Of String, CodedIntervalBase). </returns>
    Public Shared Function Dictionary() As AcceptanceIntervalCollection
        If BritishStadnardTcrInterval._Dictionary Is Nothing OrElse BritishStadnardTcrInterval._Dictionary.Count = 0 Then
            BritishStadnardTcrInterval.BuildDictionary()
        End If
        Return BritishStadnardTcrInterval._Dictionary
    End Function

    ''' <summary> Builds coded interval base dictionary. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="values"> The values. </param>
    '''
    ''' ### <exception cref="ArgumentNullException"> Thrown when one or more required arguments are
    '''                                              null. </exception>
    Public Shared Sub BuildDictionary(ByVal values As AcceptanceIntervalCollection)
        BritishStadnardTcrInterval._Dictionary = New AcceptanceIntervalCollection
        BritishStadnardTcrInterval._Dictionary.Populate(values)
    End Sub

    ''' <summary> Builds Coefficient information dictionary. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Private Shared Sub BuildDictionary()
        Dim dix As New AcceptanceIntervalCollection
        For Each kvp As KeyValuePair(Of String, Double) In BritishStadnardTcr.Dictionary
            dix.Add(New BritishStadnardTcrInterval(kvp.Key, kvp.Value))
        Next
        BritishStadnardTcrInterval.BuildDictionary(dix)
    End Sub

    ''' <summary> Parses. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="code"> The code. </param>
    ''' <returns> A Temperature Coefficient Info. </returns>
    Public Shared Function Parse(ByVal code As String) As BritishStadnardTcrInterval
        Dim acceptanceIndervals As AcceptanceIntervalCollection = BritishStadnardTcrInterval.Dictionary
        Return If(acceptanceIndervals.Contains(code), New BritishStadnardTcrInterval(acceptanceIndervals.Item(code)), Nothing)
    End Function

    ''' <summary> Tries to parse a coded value. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="code">    The code. </param>
    ''' <param name="value">   [in,out] The Margin Factor. </param>
    ''' <param name="details"> [in,out] The details. </param>
    ''' <returns> <c>True</c> if Margin Factor code can be parsed. </returns>
    Public Shared Function TryParse(ByVal code As String, ByRef value As BritishStadnardTcrInterval, ByRef details As String) As Boolean
        Dim affirmative As Boolean = False
        Dim acceptanceIndervals As AcceptanceIntervalCollection = BritishStadnardTcrInterval.Dictionary
        If acceptanceIndervals.Contains(code) Then
            value = New BritishStadnardTcrInterval(acceptanceIndervals.Item(code))
            affirmative = True
        Else
            details = $"{GetType(BritishStadnardTcrInterval)}.{NameOf(BritishStadnardTcrInterval.Code)} '{code}' is unknown"
        End If
        Return affirmative
    End Function

    ''' <summary> Gets the sentinel indicating if the entered value is Empty defined. </summary>
    ''' <value> <c>True</c> if unknown code. </value>
    Public Overrides ReadOnly Property IsEmptyCode As Boolean
        Get
            Return String.Equals(Me.Code, BritishStadnardTcrInterval.EmptyCode, StringComparison.OrdinalIgnoreCase)
        End Get
    End Property

    ''' <summary> Gets the sentinel indicating if the entered code is unknown. </summary>
    ''' <value> <c>True</c> if unknown code. </value>
    Public Overrides ReadOnly Property IsUnknownCode As Boolean
        Get
            Return String.Equals(Me.Code, BritishStadnardTcrInterval.UnknownCode, StringComparison.OrdinalIgnoreCase)
        End Get
    End Property

    ''' <summary> Gets the sentinel indicating if the entered value is user defined. </summary>
    ''' <value> <c>True</c> if unknown code. </value>
    Public Overrides ReadOnly Property IsUserCode As Boolean
        Get
            Return String.Equals(Me.Code, BritishStadnardTcrInterval.UserCode, StringComparison.OrdinalIgnoreCase)
        End Get
    End Property

End Class
