''' <summary> A quadratic polynomial. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2/6/2016 </para>
''' </remarks>
Public Class QuadraticPolynomial

#Region " CONSTRUCTION "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Public Sub New()
        MyBase.New
        Me.GoodnessOfFit = Double.NaN
        Me.StandardError = Double.NaN
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="constantCoefficient">  The constant coefficient. </param>
    ''' <param name="linearCoefficient">    The linear coefficient. </param>
    ''' <param name="quadraticCoefficient"> The quadratic coefficient. </param>
    Public Sub New(ByVal constantCoefficient As Double, ByVal linearCoefficient As Double, ByVal quadraticCoefficient As Double)
        Me.New
        Me.ConstantCoefficient = constantCoefficient
        Me.LinearCoefficient = linearCoefficient
        Me.QuadraticCoefficient = quadraticCoefficient
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value"> Information describing the polynomial. </param>
    Public Sub New(ByVal value As QuadraticPolynomial)
        Me.New
        If value IsNot Nothing Then
            Me.ConstantCoefficient = value.ConstantCoefficient
            Me.LinearCoefficient = value.LinearCoefficient
            Me.QuadraticCoefficient = value.QuadraticCoefficient
        End If
    End Sub

    ''' <summary> Gets the constant coefficient. </summary>
    ''' <value> The constant coefficient. </value>
    Public Property ConstantCoefficient As Double

    ''' <summary> Gets the linear coefficient. </summary>
    ''' <value> The linear coefficient. </value>
    Public Property LinearCoefficient As Double

    ''' <summary> Gets the quadratic coefficient. </summary>
    ''' <value> The quadratic coefficient. </value>
    Public Property QuadraticCoefficient As Double

#End Region

#Region " EVALUATION "

    ''' <summary> Evaluates the polynomial at the specified value. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value"> The value for evaluating the polynomial. </param>
    ''' <returns> A Double. </returns>
    Public Function Evaluate(ByVal value As Double) As Double
        Return Me.ConstantCoefficient + value * (Me.LinearCoefficient + value * (Me.QuadraticCoefficient))
    End Function

    ''' <summary> Evaluates the slope at the specified value. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value"> The value for evaluating the derivative. </param>
    ''' <returns> A Double. </returns>
    Public Function Slope(ByVal value As Double) As Double
        Return Me.LinearCoefficient + 2 * value * Me.QuadraticCoefficient
    End Function

#End Region

#Region " ROOTS "

    ''' <summary> Square root. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value"> Information describing the polynomial. </param>
    ''' <returns> A Double. </returns>
    Private Function SquareRoot(ByVal value As Double) As Double
        Return Math.Sqrt(Me.LinearCoefficient * Me.LinearCoefficient -
                             4 * Me.QuadraticCoefficient * (Me.ConstantCoefficient - value))
    End Function

    ''' <summary> Calculates the positive quadratic root for the given value. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value"> Information describing the polynomial. </param>
    ''' <returns> A Double. </returns>
    Public Function PositiveQuadraticRoot(ByVal value As Double) As Double
        Return (-Me.LinearCoefficient + Me.SquareRoot(value)) / (2 * Me.QuadraticCoefficient)
    End Function

    ''' <summary> Calculates the positive quadratic root for the given value. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value"> Information describing the polynomial. </param>
    ''' <returns> A Double. </returns>
    Public Function NegativeQuadraticRoot(ByVal value As Double) As Double
        Return (-Me.LinearCoefficient - Me.SquareRoot(value)) / (2 * Me.QuadraticCoefficient)
    End Function

#End Region

#Region " GOODNESS OF FIT "

    ''' <summary> Gets the standard error. </summary>
    ''' <value> The standard error. </value>
    Public ReadOnly Property StandardError As Double

    ''' <summary> Gets the goodness of fit (R-Squared). </summary>
    ''' <value> The goodness of fit (R-Squared). </value>
    Public ReadOnly Property GoodnessOfFit As Double

    ''' <summary> Gets the correlation coefficient. </summary>
    ''' <value> The correlation coefficient. </value>
    Public ReadOnly Property CorrelationCoefficient As Double
        Get
            Return Math.Sqrt(Me.GoodnessOfFit)
        End Get
    End Property

    ''' <summary> Sum squared deviations. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="values"> The values. </param>
    ''' <returns> The total number of squared deviations. </returns>
    Public Function SumSquaredDeviations(ByVal values As IList(Of System.Windows.Point)) As Double
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        Dim result As Double = 0
        For Each p As System.Windows.Point In values
            Dim temp As Double = p.Y - Me.Evaluate(p.X)
            result += temp * temp
        Next
        Return result
    End Function

    ''' <summary> Sum squared mean deviations. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="values"> The values. </param>
    ''' <returns> The total number of squared mean deviations. </returns>
    Public Shared Function SumSquaredMeanDeviations(ByVal values As IList(Of System.Windows.Point)) As Double
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        Dim mean As Double = 0
        Dim result As Double = 0
        If values.Any Then
            For Each p As System.Windows.Point In values
                mean += p.Y
            Next
            mean /= values.Count
            For Each p As System.Windows.Point In values
                Dim temp As Double = p.Y - mean
                result += temp * temp
            Next
        End If
        Return result
    End Function

    ''' <summary> Sum squared deviations. </summary>
    ''' <remarks> David, 2020-09-05. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="x"> A Double() to process. </param>
    ''' <param name="y"> A Double() to process. </param>
    ''' <returns> The total number of squared deviations. </returns>
    Public Function SumSquaredDeviations(ByVal x As Double(), y As Double()) As Double
        If x Is Nothing Then Throw New ArgumentNullException(NameOf(x))
        If y Is Nothing Then Throw New ArgumentNullException(NameOf(y))
        If x.Length <> y.Length Then Throw New InvalidOperationException($"{NameOf(x)} {NameOf(x.Length)} {x.Length} must equal {NameOf(y)} {NameOf(y.Length)} {y.Length} ")
        Dim result As Double = 0
        If x.Length > 0 Then
            Dim temp As Double
            For i As Integer = 0 To x.Length - 1
                temp = y(i) - Me.Evaluate(x(i))
                result += temp * temp
            Next
        End If
        Return result
    End Function

    ''' <summary> Sum squared mean deviations. </summary>
    ''' <remarks> David, 2020-09-05. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="x"> A Double() to process. </param>
    ''' <param name="y"> A Double() to process. </param>
    ''' <returns> The total number of squared mean deviations. </returns>
    Public Shared Function SumSquaredMeanDeviations(ByVal x As Double(), y As Double()) As Double
        If x Is Nothing Then Throw New ArgumentNullException(NameOf(x))
        If y Is Nothing Then Throw New ArgumentNullException(NameOf(y))
        If x.Length <> y.Length Then Throw New InvalidOperationException($"{NameOf(x)} {NameOf(x.Length)} {x.Length} must equal {NameOf(y)} {NameOf(y.Length)} {y.Length} ")
        Dim mean As Double = 0
        Dim result As Double = 0
        If x.Length > 0 Then
            For i As Integer = 0 To x.Length - 1
                mean += y(i)
            Next
            mean /= x.Length
            Dim temp As Double
            For i As Integer = 0 To x.Length - 1
                temp = y(i) - mean
                result += temp * temp
            Next
        End If
        Return result
    End Function

#End Region

#Region " POLY FIT "

    ''' <summary> Fits a second order polynomial to the values. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="InvalidOperationException">   Thrown when the requested operation is
    '''                                                invalid. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="values"> The values. </param>
    ''' <returns> A Double. </returns>
    Private Function PolyFitThis(ByVal values As IList(Of System.Windows.Point)) As Double
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        If Not values.Any Then Throw New InvalidOperationException("Input is empty.")
        If values.Count < 3 Then Throw New ArgumentOutOfRangeException(NameOf(values), "Input is too short; requires at least 3 elements.")
        Me.ConstantCoefficient = 0
        Me.LinearCoefficient = 0
        Me.QuadraticCoefficient = 0

        ' setup the normal equations. 
        Dim dimension As Integer = 2
        Dim a(dimension, dimension + 1) As Double
        Dim coefficients(dimension, dimension) As Double
        Dim constants(dimension) As Double
        Dim dp1 As Integer = dimension + 1
        For i As Integer = 0 To dimension
            For j As Integer = 0 To dimension
                Dim k As Integer = i + j
                For l As Integer = 0 To values.Count - 1
                    a(i, j) += Math.Pow(values(l).X, k)
                Next
                coefficients(i, j) = a(i, j)
            Next
            For Each p As System.Windows.Point In values
                a(i, dp1) += p.Y * Math.Pow(p.X, i)
            Next
            constants(i) = a(i, dp1)
        Next
        Dim d As Double = QuadraticPolynomial.Determinant(coefficients)
        Me._GoodnessOfFit = Double.NaN
        Me._StandardError = Double.NaN
        If Math.Abs(d) > Single.Epsilon Then
            Dim p() As Double = QuadraticPolynomial.CramerRule(coefficients, constants)
            Me.ConstantCoefficient = p(0) / d
            Me.LinearCoefficient = p(1) / d
            Me.QuadraticCoefficient = p(2) / d
            Dim ssq As Double = Me.SumSquaredDeviations(values)
            Me._StandardError = Math.Sqrt(ssq / values.Count)
            Dim ssqdm As Double = QuadraticPolynomial.SumSquaredMeanDeviations(values)
            If (ssqdm - ssq) > Single.Epsilon Then
                ' otherwise, correlation coefficient could be negative
                Me._GoodnessOfFit = (ssqdm - ssq) / ssqdm
            Else
                Me._GoodnessOfFit = 0
            End If
        End If
        Return Me.GoodnessOfFit
    End Function

    ''' <summary> Polynomial fit. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="values"> The values. </param>
    ''' <returns> A Double. </returns>
    Public Function PolyFit(ByVal values As IList(Of System.Windows.Point)) As Double
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        If Not values.Any Then Throw New InvalidOperationException("Input is empty.")
        Me.ConstantCoefficient = 0
        Me.LinearCoefficient = 0
        Me.QuadraticCoefficient = 0
        If values.Count = 1 Then
            Me.ConstantCoefficient = values(0).Y
            Me._GoodnessOfFit = 1
            Me._StandardError = 0
        ElseIf values.Count = 2 Then
            Dim dx As Double = (values(1).X - values(0).X)
            Dim dy As Double = (values(1).Y - values(0).Y)
            If dx > Single.Epsilon Then
                Me.LinearCoefficient = dy / dx
                Me.ConstantCoefficient = values(0).Y - values(0).X * Me.LinearCoefficient
                Me._GoodnessOfFit = 1
                Me._StandardError = 0
            Else
                Me.ConstantCoefficient = 0.5 * (values(1).Y + values(0).Y)
                Me._GoodnessOfFit = 0
                Me._StandardError = Me.ConstantCoefficient
            End If
        Else
            Me.PolyFitThis(values)
        End If
        Return Me.GoodnessOfFit
    End Function

    ''' <summary> Fits a second order polynomial to the values. </summary>
    ''' <remarks> David, 2020-09-05. </remarks>
    ''' <param name="x"> A Double() to process. </param>
    ''' <param name="y"> A Double() to process. </param>
    ''' <returns> A Double. </returns>
    Private Function PolyFitThis(ByVal x As Double(), ByVal y As Double()) As Double
        Me.ConstantCoefficient = 0
        Me.LinearCoefficient = 0
        Me.QuadraticCoefficient = 0

        ' setup the normal equations. 
        Dim dimension As Integer = 2
        Dim a(dimension, dimension + 1) As Double
        Dim coefficients(dimension, dimension) As Double
        Dim constants(dimension) As Double
        Dim dp1 As Integer = dimension + 1
        For i As Integer = 0 To dimension
            For j As Integer = 0 To dimension
                Dim k As Integer = i + j
                For l As Integer = 0 To x.Length - 1
                    a(i, j) += Math.Pow(x(l), k)
                Next
                coefficients(i, j) = a(i, j)
            Next
            For j As Integer = 0 To x.Length - 1
                a(i, dp1) += y(j) * Math.Pow(x(j), i)
            Next
            constants(i) = a(i, dp1)
        Next
        Dim d As Double = QuadraticPolynomial.Determinant(coefficients)
        Me._GoodnessOfFit = Double.NaN
        Me._StandardError = Double.NaN
        If Math.Abs(d) > Single.Epsilon Then
            Dim p() As Double = QuadraticPolynomial.CramerRule(coefficients, constants)
            Me.ConstantCoefficient = p(0) / d
            Me.LinearCoefficient = p(1) / d
            Me.QuadraticCoefficient = p(2) / d
            Dim ssq As Double = Me.SumSquaredDeviations(x, y)
            Me._StandardError = Math.Sqrt(ssq / x.Length)
            Dim ssqdm As Double = QuadraticPolynomial.SumSquaredMeanDeviations(x, y)
            If (ssqdm - ssq) > Single.Epsilon Then
                ' otherwise, correlation coefficient could be negative
                Me._GoodnessOfFit = (ssqdm - ssq) / ssqdm
            Else
                Me._GoodnessOfFit = 0
            End If
        End If
        Return Me.GoodnessOfFit
    End Function

    ''' <summary> Polynomial fit. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="x"> A Double() to process. </param>
    ''' <param name="y"> A Double() to process. </param>
    ''' <returns> A Double. </returns>
    Public Function PolyFit(ByVal x As Double(), ByVal y As Double()) As Double
        If x Is Nothing Then Throw New ArgumentNullException(NameOf(x))
        If y Is Nothing Then Throw New ArgumentNullException(NameOf(y))
        If x.Length <> y.Length Then Throw New InvalidOperationException($"{NameOf(x)} {NameOf(x.Length)} {x.Length} must equal {NameOf(y)} {NameOf(y.Length)} {y.Length} ")
        Me.ConstantCoefficient = 0
        Me.LinearCoefficient = 0
        Me.QuadraticCoefficient = 0
        If x.Length = 1 Then
            Me.ConstantCoefficient = y(0)
            Me._GoodnessOfFit = 1
            Me._StandardError = 0
        ElseIf x.Length = 2 Then
            Dim dx As Double = (x(1) - x(0))
            Dim dy As Double = (y(1) - y(0))
            If dx > Single.Epsilon Then
                Me.LinearCoefficient = dy / dx
                Me.ConstantCoefficient = y(0) - x(0) * Me.LinearCoefficient
                Me._GoodnessOfFit = 1
                Me._StandardError = 0
            Else
                Me.ConstantCoefficient = 0.5 * (y(1) + y(0))
                Me._GoodnessOfFit = 0
                Me._StandardError = Me.ConstantCoefficient
            End If
        Else
            Me.PolyFitThis(x, y)
        End If
        Return Me.GoodnessOfFit
    End Function

    ''' <summary> Builds the matrix for calculating the numerator of the Cramer Rule. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="index">        Zero-based index of the solution variable. </param>
    ''' <param name="coefficients"> The coefficients. </param>
    ''' <param name="constants">    The constants. </param>
    ''' <returns> The matrix array of solution values times the determinant. </returns>
    Public Shared Function CramerSubstitution(ByVal index As Integer, ByVal coefficients(,) As Double, ByVal constants() As Double) As Double(,)
        If coefficients Is Nothing Then Throw New ArgumentNullException(NameOf(coefficients))
        If constants Is Nothing Then Throw New ArgumentNullException(NameOf(constants))
        If index < 0 Then Throw New ArgumentOutOfRangeException(NameOf(index), "Must be non-negative")
        If index >= constants.Count Then Throw New ArgumentOutOfRangeException(NameOf(index), $"Must be less than {constants.Count}")
        Dim d As Integer = constants.Count - 1
        Dim a(d, d) As Double
        For col As Integer = 0 To d
            For row As Integer = 0 To d
                a(col, row) = If(col = index, constants(row), coefficients(col, row))
            Next
        Next
        Return a
    End Function

    ''' <summary> Use Cramer rule to calculate the solution of the linear equations. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="coefficients"> The coefficients. </param>
    ''' <param name="constants">    The constants. </param>
    ''' <returns> The array of solution values times the determinant. </returns>
    Public Shared Function CramerRule(ByVal coefficients(,) As Double, ByVal constants() As Double) As Double()
        If coefficients Is Nothing Then Throw New ArgumentNullException(NameOf(coefficients))
        If constants Is Nothing Then Throw New ArgumentNullException(NameOf(constants))
        Dim l As New List(Of Double)
        For Xi As Integer = 0 To constants.Count - 1
            Dim a(,) As Double = CramerSubstitution(Xi, coefficients, constants)
            l.Add(QuadraticPolynomial.Determinant(a))
        Next
        Return l.ToArray
    End Function

    ''' <summary> Determinants. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="matrix"> The matrix values to process. </param>
    ''' <returns> A Double. </returns>
    Public Shared Function Determinant(ByVal matrix(,) As Double) As Double
        If matrix Is Nothing Then Throw New ArgumentNullException(NameOf(matrix))
        ' If Not matrix.Any Then Throw New InvalidOperationException("matrix is empty.")
        If matrix.GetLength(0) <> 3 Then Throw New ArgumentOutOfRangeException(NameOf(matrix), $"First dimension {matrix.GetLength(0)} must be 3.")
        If matrix.GetLength(1) <> 3 Then Throw New ArgumentOutOfRangeException(NameOf(matrix), $"Second dimension {matrix.GetLength(1)} must be 3.")
        Dim result As Double = 0
        result += matrix(0, 0) * matrix(1, 1) * matrix(2, 2)
        result -= matrix(0, 0) * matrix(1, 2) * matrix(2, 1)
        result -= matrix(0, 1) * matrix(1, 0) * matrix(2, 2)
        result += matrix(0, 1) * matrix(1, 2) * matrix(2, 0)
        result += matrix(0, 2) * matrix(1, 0) * matrix(2, 1)
        result -= matrix(0, 2) * matrix(1, 1) * matrix(2, 0)
        Return result
    End Function

#End Region

End Class
