Imports isr.Core.Constructs

''' <summary> A quadratic polynomial. </summary>
''' <remarks>
''' David, 2/6/2016. https://en.wikipedia.org/wiki/Simple_linear_regression <para>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
''' Licensed under The MIT License.</para> <para></para>
''' </remarks>
Public Class LinearFit

#Region " CONSTRUCTION "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Public Sub New()
        MyBase.New
        Me.GoodnessOfFit = Double.NaN
        Me.LinearCoefficientStandardError = Double.NaN
        Me.ConstantCoefficientStandardError = Double.NaN
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="constantCoefficient"> The constant coefficient. </param>
    ''' <param name="linearCoefficient">   The linear coefficient. </param>
    Public Sub New(ByVal constantCoefficient As Double, ByVal linearCoefficient As Double)
        Me.New
        Me.ConstantCoefficient = constantCoefficient
        Me.LinearCoefficient = linearCoefficient
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value"> Information describing the polynomial. </param>
    Public Sub New(ByVal value As LinearFit)
        Me.New
        If value IsNot Nothing Then
            Me.ConstantCoefficient = value.ConstantCoefficient
            Me.LinearCoefficient = value.LinearCoefficient
        End If
    End Sub

    ''' <summary> Gets the constant coefficient. </summary>
    ''' <value> The constant coefficient. </value>
    Public Property ConstantCoefficient As Double

    ''' <summary> Gets the linear coefficient. </summary>
    ''' <value> The linear coefficient. </value>
    Public Property LinearCoefficient As Double

#End Region

#Region " EVALUATION "

    ''' <summary> Evaluates the line at the specified value. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value"> The value for evaluating the list. </param>
    ''' <returns> A Double. </returns>
    Public Function Evaluate(ByVal value As Double) As Double
        Return Me.ConstantCoefficient + value * Me.LinearCoefficient
    End Function

#End Region

#Region " STATISTICS AND LINE VALUES "

    ''' <summary> Gets the number of. </summary>
    ''' <value> The count. </value>
    Public ReadOnly Property Count As Integer

    ''' <summary> Gets the ordinate mean. </summary>
    ''' <value> The ordinate mean. </value>
    Public ReadOnly Property OrdinateMean As Double

    ''' <summary> Gets the abscissa mean. </summary>
    ''' <value> The abscissa mean. </value>
    Public ReadOnly Property AbscissaMean As Double

    ''' <summary>
    ''' Gets the residual sum of squares, or SSR (sum of squares residual); equals the sum of squares
    ''' of the difference between the estimated and actual values<code>
    ''' Sum{Squared[a + b*X.i - Y.i]}</code>
    ''' </summary>
    ''' <value> The residual sum of squares. </value>
    Public ReadOnly Property ResidualSumOfSquares As Double

    ''' <summary>
    ''' Gets the regression sum of squares or the model deviation from the mean ordinate. It measures
    ''' how far the regression line is from the average ordinate value. <code>
    ''' Sum{Squared[a + b*X.i - Average(Y.i)]}</code>
    ''' </summary>
    ''' <value> The regression sum of squares. </value>
    Public ReadOnly Property RegressionSumOfSquares As Double

    ''' <summary> Gets the explained sum of squares. </summary>
    ''' <value> The explained sum of squares. </value>
    Public ReadOnly Property ExplainedSumOfSquares As Double
        Get
            Return Me.RegressionSumOfSquares
        End Get
    End Property

    ''' <summary>
    ''' Gets the total sum of squares, SST equals the sum of squares of the ordinate values from the
    ''' mean ordinate value<code>
    ''' Sum{Squared[Y.i - Average(Y.i)]}</code>
    ''' </summary>
    ''' <value> The total number of sum of squares. </value>
    Public ReadOnly Property TotalSumOfSquares As Double

    ''' <summary>
    ''' Gets the sum of squares of the abscissa deviation from the mean:<code>
    ''' Sum{Squared[X.i - Average(X.i)]}</code>
    ''' </summary>
    ''' <value> The abscissa sum of squares. </value>
    Public ReadOnly Property AbscissaSumOfSquareDeviations As Double

    ''' <summary> Gets the abscissa sum of squares. </summary>
    ''' <value> The abscissa sum of squares. </value>
    Public ReadOnly Property AbscissaSumOfSquares As Double
        Get
            Return Me.AbscissaSumOfSquareDeviations + Me.Count * Me.AbscissaMean * Me.AbscissaMean
        End Get
    End Property

    ''' <summary>
    ''' Gets the sum of the product abscissa and ordinate deviations for their respective means<code>
    ''' Sum{[X.i - Average(X.i)]*[Y.i - Average(Y.i)]}</code>
    ''' </summary>
    ''' <value> The total number of sum of squares. </value>
    Public ReadOnly Property CovarianceSum As Double

    ''' <summary> Gets the degrees of freedom. </summary>
    ''' <value> The degrees of freedom. </value>
    Public ReadOnly Property DegreesOfFreedom As Integer
        Get
            ' set degrees of freedom to 1 for 2 or 1 elements. 
            Return If(Me.Count > 2, Me.Count - 2, 1)
        End Get
    End Property

    ''' <summary> Gets the statistic. </summary>
    ''' <value> The f statistic. </value>
    Public ReadOnly Property FStatistic As Double
        Get
            Return If(Me.ResidualSumOfSquares > 0, Me.DegreesOfFreedom * (Me.TotalSumOfSquares / Me.ResidualSumOfSquares - 1), Double.NaN)
        End Get
    End Property

    ''' <summary> Gets the Student-T statistic. </summary>
    ''' <value> The Student-T  statistic. </value>
    Public ReadOnly Property TStatistic As Double
        Get
            Return If(Double.IsNaN(Me.FStatistic), Double.NaN, Math.Sqrt(Me.FStatistic))
        End Get
    End Property

    ''' <summary> Gets the standard error also called the standard error of the estimate. </summary>
    ''' <value> The standard error. </value>
    Public ReadOnly Property StandardError As Double
        Get
            Return Math.Sqrt(Me.ResidualSumOfSquares / Me.DegreesOfFreedom)
        End Get
    End Property

#End Region

#Region " GOODNESS OF FIT "

    ''' <summary> Gets the standard error of the constant coefficient. </summary>
    ''' <value> The constant coefficient standard error. </value>
    Public ReadOnly Property ConstantCoefficientStandardError As Double

    ''' <summary> Gets the constant coefficient confidence interval. </summary>
    ''' <remarks>
    ''' The 95% confident interval of the coefficient is given by <code>mean +/- (std error) *
    ''' t(0.975,n)</code>
    ''' where t(0.975,n) is the 0.975 quantile of Student's t-distribution with n degrees of freedom,
    ''' e.g., 2.1604 for 13 degrees of freedom. This property uses a ball park value of 2 for the
    ''' 0.974 quartile.
    ''' </remarks>
    ''' <value> The constant coefficient confidence interval. </value>
    Public ReadOnly Property ConstantCoefficientConfidenceInterval As RangeR
        Get
            Return New RangeR(Me.ConstantCoefficient - 2 * Me.ConstantCoefficientStandardError,
                              Me.ConstantCoefficient + 2 * Me.ConstantCoefficientStandardError)
        End Get
    End Property

    ''' <summary> Gets the standard error of the linear coefficient. </summary>
    ''' <remarks>
    ''' The 95% confident interval of the coefficient is given by <code>mean +/- (std error) *
    ''' t(0.975,n)</code>
    ''' where t(0.975,n) is the 0.975 quantile of Student's t-distribution with n degrees of freedom,
    ''' e.g., 2.1604 for 13 degrees of freedom, and thus.
    ''' </remarks>
    ''' <value> The linear coefficient standard error. </value>
    Public ReadOnly Property LinearCoefficientStandardError As Double

    ''' <summary> Gets the Linear coefficient confidence interval. </summary>
    ''' <remarks>
    ''' The 95% confident interval of the coefficient is given by <code>mean +/- (std error) *
    ''' t(0.975,n)</code>
    ''' where t(0.975,n) is the 0.975 quantile of Student's t-distribution with n degrees of freedom,
    ''' e.g., 2.1604 for 13 degrees of freedom. This property uses a ball park value of 2 for the
    ''' 0.974 quartile.
    ''' </remarks>
    ''' <value> The Linear coefficient confidence interval. </value>
    Public ReadOnly Property LinearCoefficientConfidenceInterval As RangeR
        Get
            Return New Constructs.RangeR(Me.LinearCoefficient - 2 * Me.LinearCoefficientStandardError,
                                         Me.LinearCoefficient + 2 * Me.LinearCoefficientStandardError)
        End Get
    End Property

    ''' <summary> Gets the goodness of fit, or R-Squared; = 1 - SSR/SST. </summary>
    ''' <value> The goodness of fit (R-Squared). </value>
    Public ReadOnly Property GoodnessOfFit As Double

    ''' <summary> Gets the correlation coefficient. </summary>
    ''' <value> The correlation coefficient. </value>
    Public ReadOnly Property CorrelationCoefficient As Double
        Get
            Return Math.Sqrt(Me.GoodnessOfFit)
        End Get
    End Property

    ''' <summary>
    ''' Calculates the sum of squared deviations of the model from the actual values.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="values"> The values. </param>
    ''' <returns> The total number of squared deviations. </returns>
    Public Function CalculateResidualSumOfSquares(ByVal values As IList(Of System.Windows.Point)) As Double
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        Dim result As Double = 0
        For Each p As System.Windows.Point In values
            Dim temp As Double = p.Y - Me.Evaluate(p.X)
            result += temp * temp
        Next
        Return result
    End Function

    ''' <summary>
    ''' Calculates the sum of squared deviations of the model from the actual values.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="abscissa"> The abscissa. </param>
    ''' <param name="ordinate"> The ordinate. </param>
    ''' <returns> The total number of squared deviations. </returns>
    Public Function CalculateResidualSumOfSquares(ByVal abscissa As IList(Of Double), ByVal ordinate As IList(Of Double)) As Double
        If abscissa Is Nothing Then Throw New ArgumentNullException(NameOf(abscissa))
        If ordinate Is Nothing Then Throw New ArgumentNullException(NameOf(ordinate))
        If abscissa.Count <> ordinate.Count Then Throw New InvalidOperationException($"Abscissa counts {abscissa.Count} must equal ordinate counts {ordinate.Count}")
        Dim result As Double = 0
        If abscissa.Any Then
            For i As Integer = 0 To abscissa.Count - 1
                Dim temp As Double = ordinate(i) - Me.Evaluate(abscissa(i))
                result += temp * temp
            Next
        End If
        Return result
    End Function

    ''' <summary>
    ''' Calculates the sum of squares of the deviations of the ordinate from the mean.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="values"> The values. </param>
    ''' <returns> The total number of squared mean deviations. </returns>
    Public Shared Function CalculateSumOfOrdinateSquaredMeanDeviations(ByVal values As IList(Of System.Windows.Point)) As Double
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        Dim mean As Double = 0
        Dim result As Double = 0
        If values.Any Then
            For Each p As System.Windows.Point In values
                mean += p.Y
            Next
            mean /= values.Count
            result = LinearFit.CalculateSumOfOrdinateSquaredMeanDeviations(values, mean)
        End If
        Return result
    End Function

    ''' <summary>
    ''' Calculates the sum of squares of the deviations of the ordinate from the mean.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="values"> The values. </param>
    ''' <param name="mean">   The mean. </param>
    ''' <returns> The total number of squared mean deviations. </returns>
    Public Shared Function CalculateSumOfOrdinateSquaredMeanDeviations(ByVal values As IList(Of System.Windows.Point), ByVal mean As Double) As Double
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        Dim result As Double = 0
        For Each p As System.Windows.Point In values
            Dim temp As Double = p.Y - mean
            result += temp * temp
        Next
        Return result
    End Function

    ''' <summary> Calculates the sum of squares of  deviations from the mean. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="values"> The ordinate. </param>
    ''' <returns> The total number of squared mean deviations. </returns>
    Public Shared Function CalculateSumOfSquaredMeanDeviations(ByVal values As IList(Of Double)) As Double
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        Dim result As Double = 0
        Dim mean As Double = 0
        If values.Any Then
            For Each y As Double In values
                mean += y
            Next
            mean /= values.Count
            result = LinearFit.CalculateSumOfSquaredMeanDeviations(values, mean)
        End If
        Return result
    End Function

    ''' <summary> Calculates the sum of squares of  deviations from the mean. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="values"> The ordinate. </param>
    ''' <param name="mean">   The mean. </param>
    ''' <returns> The total number of squared mean deviations. </returns>
    Public Shared Function CalculateSumOfSquaredMeanDeviations(ByVal values As IList(Of Double), ByVal mean As Double) As Double
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        Dim result As Double = 0
        For Each value As Double In values
            Dim temp As Double = value - mean
            result += temp * temp
        Next
        Return result
    End Function

    ''' <summary> Calculates the sum of squares of  deviations from the mean. </summary>
    ''' <remarks> David, 2020-09-05. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="values"> The ordinate. </param>
    ''' <returns> The total number of squared mean deviations. </returns>
    Public Shared Function CalculateSumOfSquaredMeanDeviations(ByVal values As Double()) As Double
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        Dim result As Double = 0
        Dim mean As Double = 0
        If values.Any Then
            For Each y As Double In values
                mean += y
            Next
            mean /= values.Count
            result = LinearFit.CalculateSumOfSquaredMeanDeviations(values, mean)
        End If
        Return result
    End Function

    ''' <summary> Calculates the sum of squares of  deviations from the mean. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="values"> The ordinate. </param>
    ''' <param name="mean">   The mean. </param>
    ''' <returns> The total number of squared mean deviations. </returns>
    Public Shared Function CalculateSumOfSquaredMeanDeviations(ByVal values As Double(), ByVal mean As Double) As Double
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        Dim result As Double = 0
        For Each value As Double In values
            Dim temp As Double = value - mean
            result += temp * temp
        Next
        Return result
    End Function


#End Region

#Region " LINEAR FIT "

    ''' <summary> Fits a line to the values. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    '''
    ''' ### <exception cref="ArgumentNullException">       Thrown when one or more required
    '''                                                    arguments are null. </exception>
    ''' ### <exception cref="InvalidOperationException">   Thrown when the requested operation is
    '''                                                    invalid. </exception>
    ''' ### <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are
    '''                                                    outside the required range. </exception>
    Private Sub DoFit()
        If Me.AbscissaSumOfSquareDeviations > 0 Then
            Me.LinearCoefficient = Me.CovarianceSum / Me.AbscissaSumOfSquareDeviations
            Me.ConstantCoefficient = Me.OrdinateMean - Me.LinearCoefficient * Me.AbscissaMean
        Else
            Me.LinearCoefficient = 0
            Me.ConstantCoefficient = Me.OrdinateMean - Me.LinearCoefficient * Me.AbscissaMean
        End If
    End Sub

    ''' <summary> Summarizes; Calculates residual and standard errors. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Private Sub Summarize()
        If Me.AbscissaSumOfSquareDeviations > 0 Then
            Me._LinearCoefficientStandardError = Math.Sqrt(Me.ResidualSumOfSquares / (Me.DegreesOfFreedom * Me.AbscissaSumOfSquareDeviations))
            Me._ConstantCoefficientStandardError = Me.LinearCoefficientStandardError * Math.Sqrt(Me.AbscissaSumOfSquares / Me.Count)
        Else
            Me._LinearCoefficientStandardError = 0
            Me._ConstantCoefficientStandardError = Me.ConstantCoefficient
        End If
        If (Me.TotalSumOfSquares - Me.ResidualSumOfSquares) > Single.Epsilon Then
            ' otherwise, correlation coefficient could be negative
            Me._GoodnessOfFit = If(Me.TotalSumOfSquares > 0, 1 - Me.ResidualSumOfSquares / Me.TotalSumOfSquares, 0)
        Else
            Me._GoodnessOfFit = 1
        End If
    End Sub

#End Region

#Region " LINEAR FIT: ARRAYS "

    ''' <summary> Initializes sum and sums of squares and initial values. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="abscissa"> The abscissa. </param>
    ''' <param name="ordinate"> The ordinate. </param>
    Private Sub Initialize(ByVal abscissa As Double(), ByVal ordinate As Double())
        If abscissa Is Nothing Then Throw New ArgumentNullException(NameOf(abscissa))
        If ordinate Is Nothing Then Throw New ArgumentNullException(NameOf(ordinate))
        If abscissa.Count <> ordinate.Count Then Throw New InvalidOperationException($"Abscissa counts {abscissa.Count} must equal ordinate counts {ordinate.Count}")
        Me._Count = abscissa.Count
        Me.ConstantCoefficient = 0
        Me.LinearCoefficient = 0
        Dim x As Double = 0
        Dim y As Double = 0
        For i As Integer = 0 To Me.Count - 1
            y += ordinate(i)
            x += abscissa(i)
        Next
        Me._OrdinateMean = y / Me.Count
        Me._AbscissaMean = x / Me.Count

        Dim cov As Double = 0
        Dim xdev As Double = 0

        For i As Integer = 0 To Me.Count - 1
            x = abscissa(i)
            Dim temp As Double = x - Me.AbscissaMean

            xdev += temp * temp
            cov += temp * (ordinate(i) - Me.OrdinateMean)
        Next
        Me._CovarianceSum = cov
        Me._AbscissaSumOfSquareDeviations = xdev
        Me._GoodnessOfFit = Double.NaN
        Me._LinearCoefficientStandardError = Double.NaN
        Me._ConstantCoefficientStandardError = Double.NaN
    End Sub

    ''' <summary> Executes the fit operation. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="abscissa"> The abscissa. </param>
    ''' <param name="ordinate"> The ordinate. </param>
    ''' <returns> A measure of the goodness of fit. </returns>
    Public Function DoFit(ByVal abscissa As Double(), ByVal ordinate As Double()) As Double
        If abscissa Is Nothing Then Throw New ArgumentNullException(NameOf(abscissa))
        If ordinate Is Nothing Then Throw New ArgumentNullException(NameOf(ordinate))
        If abscissa.Count <> ordinate.Count Then Throw New InvalidOperationException($"Abscissa counts {abscissa.Count} must equal ordinate counts {ordinate.Count}")
        Me.Initialize(abscissa, ordinate)
        Me.DoFit()
        Me._ResidualSumOfSquares = Me.CalculateResidualSumOfSquares(abscissa, ordinate)
        Me._TotalSumOfSquares = LinearFit.CalculateSumOfSquaredMeanDeviations(ordinate, Me.OrdinateMean)
        Me.Summarize()
        Return Me.GoodnessOfFit
    End Function

#End Region

#Region " LINEAR FIT: LIST "

    ''' <summary> Initializes sum and sums of squares and initial values. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="abscissa"> The abscissa. </param>
    ''' <param name="ordinate"> The ordinate. </param>
    Private Sub Initialize(ByVal abscissa As IList(Of Double), ByVal ordinate As IList(Of Double))
        If abscissa Is Nothing Then Throw New ArgumentNullException(NameOf(abscissa))
        If ordinate Is Nothing Then Throw New ArgumentNullException(NameOf(ordinate))
        If abscissa.Count <> ordinate.Count Then Throw New InvalidOperationException($"Abscissa counts {abscissa.Count} must equal ordinate counts {ordinate.Count}")
        Me._Count = abscissa.Count
        Me.ConstantCoefficient = 0
        Me.LinearCoefficient = 0
        Dim x As Double = 0
        Dim y As Double = 0
        For i As Integer = 0 To Me.Count - 1
            y += ordinate(i)
            x += abscissa(i)
        Next
        Me._OrdinateMean = y / Me.Count
        Me._AbscissaMean = x / Me.Count

        Dim cov As Double = 0
        Dim xdev As Double = 0

        For i As Integer = 0 To Me.Count - 1
            x = abscissa(i)
            Dim temp As Double = x - Me.AbscissaMean

            xdev += temp * temp
            cov += temp * (ordinate(i) - Me.OrdinateMean)
        Next
        Me._CovarianceSum = cov
        Me._AbscissaSumOfSquareDeviations = xdev
        Me._GoodnessOfFit = Double.NaN
        Me._LinearCoefficientStandardError = Double.NaN
        Me._ConstantCoefficientStandardError = Double.NaN
    End Sub

    ''' <summary> Executes the fit operation. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="abscissa"> The abscissa. </param>
    ''' <param name="ordinate"> The ordinate. </param>
    ''' <returns> A measure of the goodness of fit. </returns>
    Public Function DoFit(ByVal abscissa As IList(Of Double), ByVal ordinate As IList(Of Double)) As Double
        If abscissa Is Nothing Then Throw New ArgumentNullException(NameOf(abscissa))
        If ordinate Is Nothing Then Throw New ArgumentNullException(NameOf(ordinate))
        If abscissa.Count <> ordinate.Count Then Throw New InvalidOperationException($"Abscissa counts {abscissa.Count} must equal ordinate counts {ordinate.Count}")
        Me.Initialize(abscissa, ordinate)
        Me.DoFit()
        Me._ResidualSumOfSquares = Me.CalculateResidualSumOfSquares(abscissa, ordinate)
        Me._TotalSumOfSquares = LinearFit.CalculateSumOfSquaredMeanDeviations(ordinate, Me.OrdinateMean)
        Me.Summarize()
        Return Me.GoodnessOfFit
    End Function

#End Region

#Region " LINEAR FIT - POINTS "

    ''' <summary> Initializes sum and sums of squares and initial values. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="values"> The values. </param>
    Private Sub Initialize(ByVal values As IList(Of System.Windows.Point))
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        If Not values.Any Then Throw New InvalidOperationException("Input is empty.")
        Me._Count = values.Count
        Me.ConstantCoefficient = 0
        Me.LinearCoefficient = 0
        Dim x As Double = 0
        Dim y As Double = 0
        For Each p As System.Windows.Point In values
            y += p.Y
            x += p.X
        Next
        Me._OrdinateMean = y / Me.Count
        Me._AbscissaMean = x / Me.Count

        Dim cov As Double = 0
        Dim xdev As Double = 0

        For Each p As System.Windows.Point In values
            x = p.X
            Dim temp As Double = x - Me.AbscissaMean

            xdev += temp * temp
            cov += temp * (p.Y - Me.OrdinateMean)
        Next
        Me._CovarianceSum = cov
        Me._GoodnessOfFit = Double.NaN
        Me._AbscissaSumOfSquareDeviations = xdev
        Me._LinearCoefficientStandardError = Double.NaN
        Me._ConstantCoefficientStandardError = Double.NaN
    End Sub

    ''' <summary> Executes the fit operation. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="values"> The values. </param>
    ''' <returns> A measure of the goodness of fit. </returns>
    Public Function DoFit(ByVal values As IList(Of System.Windows.Point)) As Double
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        If Not values.Any Then Throw New InvalidOperationException("Input is empty.")
        Me.Initialize(values)
        Me.DoFit()
        Me._ResidualSumOfSquares = Me.CalculateResidualSumOfSquares(values)
        Me._TotalSumOfSquares = LinearFit.CalculateSumOfOrdinateSquaredMeanDeviations(values, Me.OrdinateMean)
        Me.Summarize()
        Return Me.GoodnessOfFit
    End Function

#End Region

End Class
