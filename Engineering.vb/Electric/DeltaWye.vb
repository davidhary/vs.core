''' <summary> Delta Wye Computation class. </summary>
''' <remarks>
''' <para>Delta: (a) -- R1 -- (b) -- R3 -- (c) -- R2 -- (a)</para>
''' <para>Wye: (a) -- Ra -- (x) -- Rb -- (b) (c) -- Rc -- (x) where x is the center. </para> (c)
''' 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2/18/2015 </para>
''' </remarks>
Public NotInheritable Class DeltaWye

#Region " CONSTRUCTOR "

    ''' <summary>
    ''' Constructor that prevents a default instance of this class from being created.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Private Sub New()
        MyBase.New()
    End Sub

#End Region

#Region " CONVERSION FUNCTIONS "

    ''' <summary> Delta to Wye. </summary>
    ''' <remarks> Ra = R1*R2/Sum(R); Rb = R1*R2/sum(R); Rc = R2*R3/sum(R). </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="values"> The values. </param>
    ''' <returns> A Double() </returns>
    Public Shared Function DeltaToWye(ByVal values As Double()) As Double()
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        If values.Count < 3 Then Throw New InvalidOperationException("Delta array must include at least three elements")
        Dim sum As Double = 0
        For i As Integer = 0 To 2
            sum += values(i)
        Next
        If sum = 0 Then Throw New InvalidOperationException("Delta array must include at least one non-zero value")
        Return {values(0) * values(1) / sum, values(0) * values(2) / sum, values(1) * values(2) / sum}
    End Function

    ''' <summary> Wye to delta. </summary>
    ''' <remarks> R1 = Sum(RxR)/Rc; R2 = sum(RxR)/Rb; Rc =sum(RxR)/Ra. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="values"> The values. </param>
    ''' <returns> A Double() </returns>
    Public Shared Function WyeToDelta(ByVal values As Double()) As Double()
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        If values.Count < 3 Then Throw New InvalidOperationException("Wye array must include at least three elements")
        Dim deltas As New List(Of Double)
        Dim sum As Double = values(0) * values(1)
        sum += values(0) * values(2)
        sum += values(1) * values(2)
        For i As Integer = 0 To 2
            If values(i) = 0 Then
                Throw New InvalidOperationException(String.Format("Wye array must not include zero values--zero at {0}", i))
            Else
                deltas.Add(sum / values(2 - i))
            End If
        Next
        Return deltas.ToArray
    End Function

    ''' <summary> Delta to measured. </summary>
    ''' <remarks> Converts delta circuit values to measured values. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="values"> The values. </param>
    ''' <returns> A Double() </returns>
    Public Shared Function DeltaToMeasured(ByVal values As Double()) As Double()
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        If values.Count < 3 Then Throw New InvalidOperationException("Delta array must include at least three elements")
        ' convert delta to Wye.
        Dim wyes As Double() = DeltaWye.DeltaToWye(values)
        ' add the Wye values
        Return {wyes(0) + wyes(1), wyes(0) + wyes(2), wyes(1) + wyes(2)}
    End Function

    ''' <summary> Calculates a delta value from measured values. </summary>
    ''' <remarks> Calculates the delta loop resistance from the delta measured values. </remarks>
    ''' <param name="x"> The x coordinate. </param>
    ''' <param name="y"> The y coordinate. </param>
    ''' <param name="z"> The z coordinate. </param>
    ''' <returns> The calculated delta Wye element. </returns>
    Private Shared Function MeasuredToDelta(ByVal x As Double, ByVal y As Double, ByVal z As Double) As Double
        If (y + z - x) = 0 Then
            Return Double.NaN
        Else
            ' Return x + (x * x - (y - z) * (y - z)) / (2 * (y + z - x))
            Return x + (2 * y * z + x * x - y * y - z * z) / (2 * (y + z - x))
        End If
    End Function

    ''' <summary> Calculates a delta value from measured values. </summary>
    ''' <remarks> Calculates the delta loop resistances from the delta measured values. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="values"> The values. </param>
    ''' <returns> The calculated delta Wye element. </returns>
    Public Shared Function MeasuredToDelta(ByVal values As Double()) As Double()
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        If values.Count < 3 Then Throw New InvalidOperationException("Measured delta loop array must include at least three elements")
        Dim deltas As New List(Of Double)
        For i As Integer = 0 To 2
            deltas.Add(DeltaWye.MeasuredToDelta(values(i), values((i + 1) Mod 3), values((i + 2) Mod 3)))
        Next
        Return deltas.ToArray
    End Function

    ''' <summary> Calculates a delta value from measured values. </summary>
    ''' <remarks>
    ''' When emulating, the values provided are assumed to be the desired delta values. Therefore,
    ''' the values are first converted to measured values. This allows to validate the conversion
    ''' equations.
    ''' </remarks>
    ''' <param name="values">  The values. </param>
    ''' <param name="emulate"> true to emulate. </param>
    ''' <returns> The calculated delta Wye element. </returns>
    Public Shared Function MeasuredToDelta(ByVal values As Double(), ByVal emulate As Boolean) As Double()
        If emulate Then
            values = DeltaWye.DeltaToMeasured(values)
        End If
        Return DeltaWye.MeasuredToDelta(values)
    End Function

#End Region

End Class
