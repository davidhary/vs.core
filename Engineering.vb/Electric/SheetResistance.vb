''' <summary> A sheet resistance. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 6/28/2018 </para>
''' </remarks>
Public Class SheetResistance
    Inherits Resistance

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="sheetResistance"> The sheet resistance or <see cref="Double.NaN"/> if no value. </param>
    Public Sub New(ByVal sheetResistance As SheetResistance)
        MyBase.New(sheetResistance)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="voltage"> The voltage. </param>
    ''' <param name="current"> The current. </param>
    Public Sub New(ByVal voltage As Double, ByVal current As Double)
        MyBase.New(voltage, current)
    End Sub

    ''' <summary> Gets the sheet resistance. </summary>
    ''' <value> The sheet resistance or <see cref="Double.NaN"/> if no value. </value>
    Public ReadOnly Property SheetResistance As Double
        Get
            Return If(Me.HasValue, Engineering.SheetResistance.ToSheetResistance(Me.Resistance), Double.NaN)
        End Get
    End Property

    ''' <summary> Gets the Pi over log two. </summary>
    ''' <value> The pi over log two. </value>
    Private Shared ReadOnly Property PiOverLogTwo As Double = Math.PI / Math.Log(2)

    ''' <summary> Converts a resistance to a sheet resistance. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="resistance"> The resistance. </param>
    ''' <returns> Resistance as a Double. </returns>
    Public Shared Function ToSheetResistance(ByVal resistance As Double) As Double
        Return resistance * PiOverLogTwo
    End Function

End Class

''' <summary> Collection of sheet resistances. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 4/21/2018 </para>
''' </remarks>
Public Class SheetResistanceCollection
    Inherits ObjectModel.Collection(Of Core.Engineering.SheetResistance)

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="sheetResistances"> The sheet resistances. </param>
    Public Sub New(sheetResistances As SheetResistanceCollection)
        Me.New
        If sheetResistances IsNot Nothing Then
            For Each sr As SheetResistance In sheetResistances
                Me.Add(New SheetResistance(sr))
            Next
        End If
    End Sub

    ''' <summary> Gets the number of sheet resistances. </summary>
    ''' <value> The number of sheet resistances. </value>
    Public ReadOnly Property SheetResistanceCount As Integer

    ''' <summary> Gets the sheet resistance. </summary>
    ''' <value>
    ''' The sheet resistance or <see cref="Double.NaN"/> if <see cref="SheetResistanceCount"/> is
    ''' zero.
    ''' </value>
    Public ReadOnly Property SheetResistance As Double?
        Get
            Dim sum As Double = 0
            Dim count As Integer = 0
            For Each sr As Core.Engineering.SheetResistance In Me
                If sr.HasValue Then
                    sum += sr.SheetResistance
                    count += 1
                End If
            Next
            Me._SheetResistanceCount = count
            Return If(count > 0, sum / Me.Count, New Double?)
        End Get
    End Property

    ''' <summary> Gets the average measured Voltage. </summary>
    ''' <value>
    ''' The average measured Voltage or <see cref="Double.NaN"/> if
    ''' <see cref="SheetResistanceCount"/> is zero.
    ''' </value>
    Public ReadOnly Property AverageAbsoluteVoltage As Double?
        Get
            Dim sum As Double = 0
            Dim count As Integer = 0
            For Each sr As Core.Engineering.SheetResistance In Me
                If sr.HasValue Then
                    sum += Math.Abs(sr.Voltage)
                    count += 1
                End If
            Next
            Return If(count > 0, sum / Me.Count, New Double?)
        End Get
    End Property

    ''' <summary> Gets the average source current. </summary>
    ''' <value>
    ''' The average source current or <see cref="Double.NaN"/> if <see cref="SheetResistanceCount"/>
    ''' is zero.
    ''' </value>
    Public ReadOnly Property AbsoluteCurrent As Double?
        Get
            Dim sum As Double = 0
            Dim count As Integer = 0
            For Each sr As Core.Engineering.SheetResistance In Me
                If sr.HasValue Then
                    sum += Math.Abs(sr.Current)
                    count += 1
                End If
            Next
            Return If(count > 0, sum / Me.Count, New Double?)
        End Get
    End Property

End Class


