''' <summary> A voltage source. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/15/2017 </para>
''' </remarks>
Public Class VoltageSource

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="voltage">    The voltage. </param>
    ''' <param name="resistance"> The resistance. </param>
    Public Sub New(ByVal voltage As Double, ByVal resistance As Double)
        MyBase.New()
        Me.InitializeThis(voltage, resistance)
    End Sub

    ''' <summary> Cloning Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="voltageSource"> The voltage source. </param>
    Public Sub New(ByVal voltageSource As VoltageSource)
        Me.New(VoltageSource.ValidatedVoltageSource(voltageSource).Voltage, voltageSource.Resistance)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="currentSource"> The current source. </param>
    Public Sub New(ByVal currentSource As CurrentSource)
        Me.New(CurrentSource.ValidatedCurrentSource(currentSource).ToVoltageSource)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="voltageSource"> The voltage source. </param>
    Public Sub New(ByVal voltageSource As AttenuatedVoltageSource)
        Me.New(AttenuatedVoltageSource.ValidatedVoltageSource(voltageSource).ToCurrentSource)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="currentSource"> The current source. </param>
    Public Sub New(ByVal currentSource As AttenuatedCurrentSource)
        Me.New(AttenuatedCurrentSource.ValidatedCurrentSource(currentSource).ToVoltageSource)
    End Sub

    ''' <summary> Validated voltage source. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="voltageSource"> The voltage source. </param>
    ''' <returns> A VoltageSource. </returns>
    Public Shared Function ValidatedVoltageSource(ByVal voltageSource As VoltageSource) As VoltageSource
        If voltageSource Is Nothing Then Throw New ArgumentNullException(NameOf(voltageSource))
        Return voltageSource
    End Function

    ''' <summary> Initializes this object. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="voltage">    The voltage. </param>
    ''' <param name="resistance"> The resistance. </param>
    Private Sub InitializeThis(ByVal voltage As Double, ByVal resistance As Double)
        Me._Voltage = voltage
        Me._Resistance = resistance
    End Sub

    ''' <summary> Initialized. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="voltage">    The voltage. </param>
    ''' <param name="resistance"> The resistance. </param>
    Public Sub Initialize(ByVal voltage As Double, ByVal resistance As Double)
        Me.InitializeThis(voltage, resistance)
    End Sub

    ''' <summary> Initialized. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="voltageSource"> The voltage source. </param>
    Public Sub Initialize(ByVal voltageSource As VoltageSource)
        If voltageSource Is Nothing Then Throw New ArgumentNullException(NameOf(voltageSource))
        Me.InitializeThis(voltageSource.Voltage, voltageSource.Resistance)
    End Sub

#End Region

#Region " EQUALS "

    ''' <summary>
    ''' Determines whether the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
    '''                    <see cref="T:System.Object" />. </param>
    ''' <returns>
    ''' <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />; otherwise, <c>False</c>.
    ''' </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return obj IsNot Nothing AndAlso Me.GetType() Is obj.GetType() AndAlso Me.Equals(CType(obj, VoltageSource))
    End Function

    ''' <summary>
    ''' Compares two Voltage Sources. The Voltage Sources are compared using their resistances and
    ''' voltages.
    ''' </summary>
    ''' <remarks>
    ''' The Voltage Sources are the same if the have the same Voltage and Resistance.
    ''' </remarks>
    ''' <param name="other"> Specifies the other <see cref="VoltageSource">Voltage Source</see>
    '''                      to compare for equality with this instance. </param>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Function Equals(ByVal other As VoltageSource) As Boolean
        Return If(other Is Nothing, False, Me.Voltage.Equals(other.Voltage) AndAlso Me.Resistance.Equals(other.Resistance))
    End Function

    ''' <summary>
    ''' Compares two Voltage Sources. The Voltage Sources are compared using their resistance and
    ''' voltage.
    ''' </summary>
    ''' <remarks>
    ''' The Voltage Sources are the same if the have the same Voltage and Resistance.
    ''' </remarks>
    ''' <param name="other">     Specifies the other <see cref="VoltageSource">Voltage Source</see>
    '''                          to compare for equality with this instance. </param>
    ''' <param name="tolerance"> Specifies the relative tolerance for comparing the two values. The
    '''                          values are compared based on their end points. </param>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Function Equals(ByVal other As VoltageSource, ByVal tolerance As Double) As Boolean
        If other Is Nothing Then
            Return False
        ElseIf Me.Equals(other) Then
            Return True
        Else
            Return Math.Abs(Me.Voltage - other.Voltage) <= other.Voltage * tolerance AndAlso
                   Math.Abs(Me.Resistance - other.Resistance) <= other.Resistance * tolerance
        End If
    End Function

    ''' <summary> Implements the operator =. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Overloads Shared Function Equals(ByVal left As VoltageSource, ByVal right As VoltageSource) As Boolean
        Return (CObj(left) Is CObj(right)) OrElse (left IsNot Nothing AndAlso left.Equals(right))
    End Function

    ''' <summary>
    ''' Compares two Voltage Sources. The Voltage Sources are compared using their resistance and
    ''' voltage.
    ''' </summary>
    ''' <remarks>
    ''' The Voltage Sources are the same if the have the same Voltage and Resistance.
    ''' </remarks>
    ''' <param name="left">      Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right">     Specifies the right hand side argument of the binary operation. </param>
    ''' <param name="tolerance"> Specifies the relative tolerance for comparing the two values. The
    '''                          values are compared based on their end points. </param>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Shared Function Equals(ByVal left As VoltageSource, ByVal right As VoltageSource, ByVal tolerance As Double) As Boolean
        Return ((left Is Nothing) AndAlso (right Is Nothing)) OrElse (left IsNot Nothing) AndAlso left.Equals(right, tolerance)
    End Function

    ''' <summary> Implements the operator =. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Overloads Shared Operator =(ByVal left As VoltageSource, ByVal right As VoltageSource) As Boolean
        Return VoltageSource.Equals(left, right)
    End Operator

    ''' <summary> Implements the operator &lt;&gt;. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Overloads Shared Operator <>(ByVal left As VoltageSource, ByVal right As VoltageSource) As Boolean
        Return Not VoltageSource.Equals(left, right)
    End Operator

    ''' <summary> Creates a unique hash code. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> An <see cref="System.Int32">Int32</see> value. </returns>
    Public Overloads Overrides Function GetHashCode() As Int32
        Return Me.Voltage.GetHashCode Xor Me.Resistance.GetHashCode
    End Function

#End Region

#Region " TO STRING "

    ''' <summary> Returns a string that represents the current object. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> A string that represents the current object. </returns>
    Public Overrides Function ToString() As String
        Return $"{Me.Voltage}:{Me.Resistance}"
    End Function

    ''' <summary> Returns a string that represents the current object. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="voltageFormat">    The voltage format. </param>
    ''' <param name="resistanceFormat"> The resistance format. </param>
    ''' <returns> A string that represents the current object. </returns>
    Public Overloads Function ToString(ByVal voltageFormat As String, ByVal resistanceFormat As String) As String
        Return $"{String.Format(voltageFormat, Me.Voltage)}:{String.Format(resistanceFormat, Me.Resistance)}"
    End Function

#End Region

#Region " COMPONENETS "

    ''' <summary> Gets or sets the voltage source voltage. This is the open load voltage. </summary>
    ''' <value> The voltage. </value>
    Public Property Voltage As Double

    ''' <summary> Gets or sets the voltage source equivalent resistance. </summary>
    ''' <value> The resistance. </value>
    Public Property Resistance As Double

#End Region

#Region " OUTPUT "

    ''' <summary> Returns the load voltage. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="loadResistance"> The load resistance. </param>
    ''' <returns> The voltage. </returns>
    Public Function LoadVoltage(ByVal loadResistance As Double) As Double
        If Double.IsInfinity(loadResistance) Then
            Return Me.Voltage
        ElseIf loadResistance = 0 Then
            Return 0
        Else
            Return loadResistance * Me.LoadCurrent(loadResistance)
        End If
    End Function

    ''' <summary> Returns the load current. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="loadResistance"> The load resistance. </param>
    ''' <returns> The current. </returns>
    Public Function LoadCurrent(ByVal loadResistance As Double) As Double
        Return If(Double.IsInfinity(loadResistance), 0, Me.Voltage / (loadResistance + Me.Resistance))
    End Function

#End Region

#Region " CONVERTERS "

    ''' <summary> Converts this object to a current source. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="voltage">    The voltage. </param>
    ''' <param name="resistance"> The resistance. </param>
    ''' <returns> This object as a CurrentSource. </returns>
    Public Shared Function ToCurrentSource(ByVal voltage As Double, ByVal resistance As Double) As CurrentSource
        Dim conductance As Double = 1 / resistance
        Return New CurrentSource(voltage * conductance, conductance)
    End Function

    ''' <summary> Converts this object to a current source. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="voltageSource"> The voltage source. </param>
    ''' <returns> This object as a CurrentSource. </returns>
    Public Shared Function ToCurrentSource(ByVal voltageSource As VoltageSource) As CurrentSource
        If voltageSource Is Nothing Then Throw New ArgumentNullException(NameOf(voltageSource))
        Return voltageSource.ToCurrentSource()
    End Function

    ''' <summary> Converts this object to a current source. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> This object as a CurrentSource. </returns>
    Public Function ToCurrentSource() As CurrentSource
        Dim conductance As Double = 1 / Me.Resistance
        Return New CurrentSource(Me.Voltage * conductance, conductance)
    End Function

    ''' <summary> Initializes this object from the given from current source. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="current">     The current. </param>
    ''' <param name="conductance"> The conductance. </param>
    ''' <returns> A VoltageSource. </returns>
    Public Shared Function FromCurrentSource(ByVal current As Double, ByVal conductance As Double) As VoltageSource
        Dim resistance As Double = 1 / conductance
        Return New VoltageSource(current * resistance, resistance)
    End Function

    ''' <summary> Initializes this object from the given from current source. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="currentSource"> The current source. </param>
    Public Sub FromCurrentSource(ByVal currentSource As CurrentSource)
        If currentSource Is Nothing Then Throw New ArgumentNullException(NameOf(currentSource))
        Me.Resistance = 1 / currentSource.Conductance
        Me.Voltage = currentSource.Current * Me.Resistance
    End Sub

#End Region

#Region " ATTENUATED VOLTAGE SOURCE CONVERTERS "

    ''' <summary>
    ''' Builds an attenuated voltage source using the voltage source and attenuation.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="voltageSource"> The voltage source. </param>
    ''' <param name="attenuation">   The attenuation. </param>
    ''' <returns> The given data converted to an AttenuatedVoltageSource. </returns>
    Public Shared Function ToAttenuatedVoltageSource(ByVal voltageSource As VoltageSource, ByVal attenuation As Double) As AttenuatedVoltageSource
        If voltageSource Is Nothing Then Throw New ArgumentNullException(NameOf(voltageSource))
        If attenuation < AttenuatedVoltageSource.MinimumAttenuation Then Throw New ArgumentOutOfRangeException(NameOf(attenuation),
                                                                                                            $"Value must be greater or equal to {AttenuatedVoltageSource.MinimumAttenuation}")
        Return voltageSource.ToAttenuatedVoltageSource(attenuation)
    End Function

    ''' <summary> Converts this object to an Attenuated voltage source. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="attenuation"> The attenuation. </param>
    ''' <returns> The given data converted to an AttenuatedVoltageSource. </returns>
    Public Function ToAttenuatedVoltageSource(ByVal attenuation As Double) As AttenuatedVoltageSource
        If attenuation < AttenuatedVoltageSource.MinimumAttenuation Then Throw New ArgumentOutOfRangeException(NameOf(attenuation),
                                                                                   $"Value must be greater or equal to {AttenuatedVoltageSource.MinimumAttenuation}")
        Dim resistance As Double = Me.Resistance
        Dim seriesResistance As Double = attenuation * resistance
        Dim conductance As Double = 0
        If attenuation > AttenuatedVoltageSource.MinimumAttenuation Then
            conductance = (attenuation - 1) / seriesResistance
        End If
        Return New AttenuatedVoltageSource(Me.Voltage, seriesResistance, conductance)
    End Function

    ''' <summary>
    ''' Converts this voltage source to an attenuated voltage source with the specified nominal
    ''' voltage.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="nominalVoltage"> The nominal voltage. </param>
    ''' <returns> NominalVoltage as an AttenuatedVoltageSource. </returns>
    Public Function ToAttenuatedVoltageSource(ByVal nominalVoltage As Decimal) As AttenuatedVoltageSource
        Return Me.ToAttenuatedVoltageSource(CDbl(nominalVoltage / Me.Voltage))
    End Function

    ''' <summary>
    ''' Builds an attenuated voltage source using the voltage source and nominal voltage.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="voltageSource">  The voltage source. </param>
    ''' <param name="nominalVoltage"> The nominal voltage. </param>
    ''' <returns> The given data converted to an AttenuatedVoltageSource. </returns>
    Public Shared Function ToAttenuatedVoltageSource(ByVal voltageSource As VoltageSource, ByVal nominalVoltage As Decimal) As AttenuatedVoltageSource
        If voltageSource Is Nothing Then Throw New ArgumentNullException(NameOf(voltageSource))
        Return voltageSource.ToAttenuatedVoltageSource(nominalVoltage)
    End Function

    ''' <summary>
    ''' Builds an attenuated voltage source using the nominal voltage, equivalent resistance and
    ''' attenuation.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="nominalVoltage"> The nominal voltage. </param>
    ''' <param name="resistance">     The resistance. </param>
    ''' <param name="attenuation">    The attenuation. The ratio of the nominal voltage to the source
    '''                               voltage over open load. </param>
    ''' <returns> The given data converted to a VoltageSource. </returns>
    Public Shared Function ToAttenuatedVoltageSource(ByVal nominalVoltage As Decimal, ByVal resistance As Double, ByVal attenuation As Double) As AttenuatedVoltageSource
        Return New VoltageSource(CDec(nominalVoltage / attenuation), resistance).ToAttenuatedVoltageSource(attenuation)
    End Function


#End Region

End Class
