''' <summary> An attenuated voltage source. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/15/2017 </para>
''' </remarks>
Public Class AttenuatedVoltageSource
    Inherits VoltageSource

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="openLoadVoltage">     The open load voltage. </param>
    ''' <param name="seriesResistance">    The series resistance. </param>
    ''' <param name="parallelConductance"> The parallel conductance. </param>
    Public Sub New(ByVal openLoadVoltage As Double, ByVal seriesResistance As Double, ByVal parallelConductance As Double)
        MyBase.New(AttenuatedVoltageSource.ToVoltageSource(openLoadVoltage, seriesResistance, parallelConductance))
        Me.InitializeThis(openLoadVoltage, seriesResistance, parallelConductance)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="nominalVoltage">      The nominal voltage. </param>
    ''' <param name="seriesResistance">    The series resistance. </param>
    ''' <param name="parallelConductance"> The parallel conductance. </param>
    Public Sub New(ByVal nominalVoltage As Decimal, ByVal seriesResistance As Double, ByVal parallelConductance As Double)
        MyBase.New(AttenuatedVoltageSource.ToVoltageSource(nominalVoltage, seriesResistance, parallelConductance))
        Me.InitializeThis(nominalVoltage, seriesResistance, parallelConductance)
    End Sub

    ''' <summary> The cloning Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="voltageSource"> The voltage source. </param>
    Public Sub New(ByVal voltageSource As AttenuatedVoltageSource)
        Me.New(ValidatedVoltageSource(voltageSource).Voltage, voltageSource.SeriesResistance, voltageSource.ParallelConductance)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="voltageSource"> The voltage Source. </param>
    ''' <param name="attenuation">   The attenuation. The ratio of the nominal voltage to the source
    '''                              voltage over open load. </param>
    Public Sub New(ByVal voltageSource As VoltageSource, ByVal attenuation As Double)
        Me.New(VoltageSource.ToAttenuatedVoltageSource(VoltageSource.ValidatedVoltageSource(voltageSource), attenuation))
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="nominalVoltage"> The nominal voltage. </param>
    ''' <param name="resistance">     The resistance. </param>
    ''' <param name="attenuation">    The attenuation. The ratio of the nominal voltage to the source
    '''                               voltage over open load. </param>
    Public Sub New(ByVal nominalVoltage As Decimal, ByVal resistance As Double, ByVal attenuation As Decimal)
        Me.New(VoltageSource.ToAttenuatedVoltageSource(nominalVoltage, resistance, attenuation))
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="voltageSource">  The voltage Source. </param>
    ''' <param name="nominalVoltage"> The nominal voltage. </param>
    Public Sub New(ByVal voltageSource As VoltageSource, ByVal nominalVoltage As Decimal)
        Me.New(VoltageSource.ToAttenuatedVoltageSource(VoltageSource.ValidatedVoltageSource(voltageSource), nominalVoltage))
    End Sub

    ''' <summary> Validated Attenuated voltage source. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="voltageSource"> The voltage source. </param>
    ''' <returns> An AttenuatedVoltageSource. </returns>
    Public Overloads Shared Function ValidatedVoltageSource(ByVal voltageSource As AttenuatedVoltageSource) As AttenuatedVoltageSource
        If voltageSource Is Nothing Then Throw New ArgumentNullException(NameOf(voltageSource))
        Return voltageSource
    End Function

    ''' <summary> Initializes the attenuated voltage source using nominal voltage. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="nominalVoltage">      The nominal voltage. </param>
    ''' <param name="seriesResistance">    The series resistance. </param>
    ''' <param name="parallelConductance"> The parallel conductance. </param>
    Private Sub InitializeThis(ByVal nominalVoltage As Decimal, ByVal seriesResistance As Double, ByVal parallelConductance As Double)
        Me._NominalVoltage = nominalVoltage
        Me._SeriesResistance = seriesResistance
        Me._ParallelConductance = parallelConductance
        Me._Attenuation = AttenuatedVoltageSource.ToAttenuation(seriesResistance, parallelConductance)
    End Sub

    ''' <summary> Updates the attenuated voltage source using nominal voltage. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="nominalVoltage">      The nominal voltage. </param>
    ''' <param name="seriesResistance">    The series resistance. </param>
    ''' <param name="parallelConductance"> The parallel conductance. </param>
    Public Overloads Sub Initialize(ByVal nominalVoltage As Decimal, ByVal seriesResistance As Double, ByVal parallelConductance As Double)
        Me.InitializeThis(nominalVoltage, seriesResistance, parallelConductance)
        MyBase.Initialize(nominalVoltage / Me._Attenuation, AttenuatedVoltageSource.ToEquivalentResistance(seriesResistance, parallelConductance))
    End Sub

    ''' <summary> Initializes the attenuated voltage source using open load voltage. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="openLoadVoltage">     The open load voltage. </param>
    ''' <param name="seriesResistance">    The series resistance. </param>
    ''' <param name="parallelConductance"> The parallel conductance. </param>
    Private Sub InitializeThis(ByVal openLoadVoltage As Double, ByVal seriesResistance As Double, ByVal parallelConductance As Double)
        Me._SeriesResistance = seriesResistance
        Me._ParallelConductance = parallelConductance
        Me._Attenuation = AttenuatedVoltageSource.ToAttenuation(seriesResistance, parallelConductance)
        Me._NominalVoltage = Me._Attenuation * openLoadVoltage
    End Sub

    ''' <summary> Updates the attenuated voltage source using open load voltage. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="openLoadVoltage">     The open load voltage. </param>
    ''' <param name="seriesResistance">    The series resistance. </param>
    ''' <param name="parallelConductance"> The parallel conductance. </param>
    Public Overloads Sub Initialize(ByVal openLoadVoltage As Double, ByVal seriesResistance As Double, ByVal parallelConductance As Double)
        Me.InitializeThis(openLoadVoltage, seriesResistance, parallelConductance)
        MyBase.Initialize(openLoadVoltage, AttenuatedVoltageSource.ToEquivalentResistance(seriesResistance, parallelConductance))
    End Sub

    ''' <summary> Initializes this object. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="voltageSource"> The voltage source. </param>
    ''' <param name="attenuation">   The attenuation. </param>
    Public Overloads Sub Initialize(ByVal voltageSource As VoltageSource, ByVal attenuation As Double)
        If voltageSource Is Nothing Then Throw New ArgumentNullException(NameOf(voltageSource))
        If attenuation < AttenuatedVoltageSource.MinimumAttenuation Then Throw New ArgumentOutOfRangeException(NameOf(attenuation),
                                                                          $"must be greater or equal to {AttenuatedVoltageSource.MinimumAttenuation}")
        Me._Attenuation = attenuation
        Me._NominalVoltage = voltageSource.Voltage / Me.Attenuation
        Me._SeriesResistance = Me.Attenuation * voltageSource.Resistance
        Me._ParallelConductance = (Me.Attenuation - 1) / Me.SeriesResistance
        MyBase.Initialize(voltageSource)
    End Sub

    ''' <summary>
    ''' Initializes the attenuated voltage source for a new attenuation based on the underly voltage
    ''' source open voltage and equivalent resistance.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="attenuation"> The attenuation. The ratio of the nominal to the equivalent
    '''                            voltage. Must be greater or equal to 1. </param>
    Public Overloads Sub Initialize(ByVal attenuation As Double)
        If attenuation < AttenuatedVoltageSource.MinimumAttenuation Then Throw New ArgumentOutOfRangeException(NameOf(attenuation),
                                                                          $"must be greater or equal to {AttenuatedVoltageSource.MinimumAttenuation}")
        ' keeps the open load voltage and equivalent resistance unchanged.
        Me._NominalVoltage = attenuation * Me.Voltage / Me.Attenuation
        Me._Attenuation = attenuation
        Me._SeriesResistance = attenuation * Me.Resistance
        Me._ParallelConductance = (attenuation - 1) / Me.SeriesResistance
    End Sub

#End Region

#Region " EQUALS "

    ''' <summary>
    ''' Determines whether the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
    '''                    <see cref="T:System.Object" />. </param>
    ''' <returns>
    ''' <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />; otherwise, <c>False</c>.
    ''' </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return Me.Equals(TryCast(obj, AttenuatedVoltageSource))
    End Function

    ''' <summary>
    ''' Compares two Attenuated Voltage Sources. The Attenuated Voltage Sources are compared using
    ''' their resistances and voltages.
    ''' </summary>
    ''' <remarks> The balances are the same if the have the same resistances and voltages. </remarks>
    ''' <param name="other"> Specifies the other <see cref="AttenuatedVoltageSource">Attenuated
    '''                      Voltage Source</see>
    '''                      to compare for equality with this instance. </param>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Function Equals(ByVal other As AttenuatedVoltageSource) As Boolean
        Return other IsNot Nothing AndAlso MyBase.Equals(other) AndAlso Me.NominalVoltage.Equals(other.NominalVoltage) AndAlso
                Me.SeriesResistance.Equals(other.SeriesResistance) AndAlso Me.ParallelConductance.Equals(other.ParallelConductance)
    End Function

    ''' <summary> Compares two Attenuated Voltage Sources. </summary>
    ''' <remarks> The balances are the same if the have the same resistances and voltages. </remarks>
    ''' <param name="other">     Specifies the other <see cref="AttenuatedVoltageSource">Attenuated
    '''                          Voltage Source</see>
    '''                          to compare for equality with this instance. </param>
    ''' <param name="tolerance"> Specifies the relative tolerance for comparing the two values. The
    '''                          values are compared based on their end points. </param>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Function Equals(ByVal other As AttenuatedVoltageSource, ByVal tolerance As Double) As Boolean
        If other Is Nothing Then
            Return False
        ElseIf Me.Equals(other) Then
            Return True
        Else
            Return MyBase.Equals(other, tolerance) AndAlso
                Math.Abs(Me.NominalVoltage - other.NominalVoltage) < other.NominalVoltage * tolerance AndAlso
                Math.Abs(Me.SeriesResistance - other.SeriesResistance) <= other.SeriesResistance * tolerance AndAlso
                Math.Abs(Me.ParallelConductance - other.ParallelConductance) <= other.ParallelConductance * tolerance
        End If
    End Function

    ''' <summary> Implements the operator =. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Overloads Shared Function Equals(ByVal left As AttenuatedVoltageSource, ByVal right As AttenuatedVoltageSource) As Boolean
        Return (CObj(left) Is CObj(right)) OrElse (left IsNot Nothing AndAlso left.Equals(right))
    End Function

    ''' <summary>
    ''' Compares two Attenuated Voltage Sources. The Attenuated Voltage Sources are compared using
    ''' their resistances and voltages.
    ''' </summary>
    ''' <remarks> The balances are the same if the have the same values and layout. </remarks>
    ''' <param name="left">      Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right">     Specifies the right hand side argument of the binary operation. </param>
    ''' <param name="tolerance"> Specifies the relative tolerance for comparing the two values. The
    '''                          values are compared based on their end points. </param>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Shared Function Equals(ByVal left As AttenuatedVoltageSource, ByVal right As AttenuatedVoltageSource, ByVal tolerance As Double) As Boolean
        Return ((left Is Nothing) AndAlso (right Is Nothing)) OrElse (left IsNot Nothing) AndAlso left.Equals(right, tolerance)
    End Function

    ''' <summary> Implements the operator =. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Overloads Shared Operator =(ByVal left As AttenuatedVoltageSource, ByVal right As AttenuatedVoltageSource) As Boolean
        Return AttenuatedVoltageSource.Equals(left, right)
    End Operator

    ''' <summary> Implements the operator &lt;&gt;. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Overloads Shared Operator <>(ByVal left As AttenuatedVoltageSource, ByVal right As AttenuatedVoltageSource) As Boolean
        Return Not AttenuatedVoltageSource.Equals(left, right)
    End Operator

    ''' <summary> Creates a unique hash code. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> An <see cref="System.Int32">Int32</see> value. </returns>
    Public Overloads Overrides Function GetHashCode() As Int32
        Return MyBase.GetHashCode Xor Me.NominalVoltage.GetHashCode Xor
            Me.SeriesResistance.GetHashCode Xor Me.ParallelConductance.GetHashCode
    End Function

#End Region

#Region " TO STRING "

    ''' <summary> Returns a string that represents the current object. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> A string that represents the current object. </returns>
    Public Overrides Function ToString() As String
        Return $"{Me.NominalVoltage}:{Me.SeriesResistance}:{Me.ParallelConductance}"
    End Function

    ''' <summary> Returns a string that represents the current object. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="voltageFormat">    The voltage format. </param>
    ''' <param name="resistanceFormat"> The resistance format. </param>
    ''' <returns> A string that represents the current object. </returns>
    Public Overloads Function ToString(ByVal voltageFormat As String, ByVal resistanceFormat As String) As String
        Return $"{String.Format(voltageFormat, Me.NominalVoltage)}:{String.Format(resistanceFormat, Me.SeriesResistance)}:{String.Format(resistanceFormat, Me.ParallelConductance)}"
    End Function

#End Region

#Region " COMPONENTS "
    ''' <summary> The nominal voltage. </summary>
    Private _NominalVoltage As Double

    ''' <summary> Gets or sets the nominal voltage of the attenuated source. </summary>
    ''' <value> The nominal voltage. </value>
    Public Property NominalVoltage As Double
        Get
            Return Me._NominalVoltage
        End Get
        Set(value As Double)
            If value <> Me.NominalVoltage Then
                Me._NominalVoltage = value
                Me.Initialize(CDec(Me.NominalVoltage), Me.SeriesResistance, Me.ParallelConductance)
            End If
        End Set
    End Property
    ''' <summary> The series resistance. </summary>
    Private _SeriesResistance As Double

    ''' <summary> Gets or sets the series resistance. </summary>
    ''' <value> The series resistance. </value>
    Public Property SeriesResistance As Double
        Get
            Return Me._SeriesResistance
        End Get
        Set(value As Double)
            If value <> Me.SeriesResistance Then
                Me._SeriesResistance = value
                Me.Initialize(CDec(Me.NominalVoltage), Me.SeriesResistance, Me.ParallelConductance)
            End If
        End Set
    End Property

    ''' <summary> Gets the parallel resistance. </summary>
    ''' <value> The parallel resistance. </value>
    Public ReadOnly Property ParallelResistance As Double
        Get
            Return Conductor.ToResistance(Me.ParallelConductance)
        End Get
    End Property
    ''' <summary> The parallel conductance. </summary>
    Private _ParallelConductance As Double

    ''' <summary> Gets or sets the parallel conductance. </summary>
    ''' <value> The parallel conductance. </value>
    Public Property ParallelConductance As Double
        Get
            Return Me._ParallelConductance
        End Get
        Set(value As Double)
            If value <> Me.ParallelConductance Then
                Me._ParallelConductance = value
                Me.Initialize(CDec(Me.NominalVoltage), Me.SeriesResistance, Me.ParallelConductance)
            End If
        End Set
    End Property

    ''' <summary> The minimum attenuation. This is the minimum ratio between the nominal voltage source voltage and the equivalent voltage source voltage. </summary>
    Public Const MinimumAttenuation As Double = 1
    ''' <summary> The attenuation. </summary>
    Private _Attenuation As Double

    ''' <summary>
    ''' Gets or sets the attenuation. This is the ratio of the nominal source voltage to the output
    ''' voltage over open load.
    ''' </summary>
    ''' <value> The attenuation. </value>
    Public Property Attenuation As Double
        Get
            Return Me._Attenuation
        End Get
        Set(value As Double)
            If value <> Me.Attenuation Then
                Me.Initialize(value)
            End If
        End Set
    End Property

#End Region

#Region " ATTENUATION AND EQUIVALENT RESISTANCE "

    ''' <summary> Evaluate attenuation. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="seriesResistance">    The series resistance. </param>
    ''' <param name="parallelConductance"> The parallel conductance. </param>
    ''' <returns> A Double. </returns>
    Public Shared Function ToAttenuation(ByVal seriesResistance As Double, ByVal parallelConductance As Double) As Double
        If seriesResistance < 0 Then Throw New ArgumentException($"Value {seriesResistance} must not be negative", NameOf(seriesResistance))
        If parallelConductance < 0 Then Throw New ArgumentException($"Value {parallelConductance} must not be negative", NameOf(parallelConductance))
        Return If(seriesResistance = 0 OrElse parallelConductance = 0, 1, 1 + seriesResistance * parallelConductance)
    End Function

    ''' <summary> Evaluates attenuation. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="seriesResistance">   The series resistance. </param>
    ''' <param name="parallelResistance"> The parallel resistance. </param>
    ''' <returns> A Double. </returns>
    Public Shared Function ToAttenuation(ByVal seriesResistance As Double, ByVal parallelResistance As Decimal) As Double
        If parallelResistance <= 0 Then Throw New ArgumentException($"Value {parallelResistance} must be positive", NameOf(parallelResistance))
        Return AttenuatedVoltageSource.ToAttenuation(seriesResistance, 1 / parallelResistance)
    End Function

    ''' <summary> Evaluates attenuation. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="nominalVoltage">  The nominal voltage. </param>
    ''' <param name="openLoadVoltage"> The open load voltage. </param>
    ''' <returns> A Double. </returns>
    Public Shared Function ToAttenuation(ByVal nominalVoltage As Decimal, ByVal openLoadVoltage As Double) As Double
        Return nominalVoltage / openLoadVoltage
    End Function

    ''' <summary> Evaluates equivalent resistance. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="seriesResistance">    The series resistance. </param>
    ''' <param name="parallelConductance"> The parallel conductance. </param>
    ''' <returns> A Double. </returns>
    Public Shared Function ToEquivalentResistance(ByVal seriesResistance As Double, ByVal parallelConductance As Double) As Double
        If seriesResistance < 0 Then Throw New ArgumentException($"Value {seriesResistance} must not be negative", NameOf(seriesResistance))
        If parallelConductance < 0 Then Throw New ArgumentException($"Value {parallelConductance} must not be negative", NameOf(parallelConductance))
        Return Core.Engineering.Resistor.ShuntConductor(seriesResistance, parallelConductance)
    End Function

    ''' <summary> Evaluates equivalent resistance. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="seriesResistance">   The series resistance. </param>
    ''' <param name="parallelResistance"> The parallel resistance. </param>
    ''' <returns> A Double. </returns>
    Public Shared Function ToEquivalentResistance(ByVal seriesResistance As Double, ByVal parallelResistance As Decimal) As Double
        If parallelResistance <= 0 Then Throw New ArgumentException($"Value {parallelResistance} must be positive", NameOf(parallelResistance))
        If seriesResistance < 0 Then Throw New ArgumentException($"Value {seriesResistance} must not be negative", NameOf(seriesResistance))
        Return Core.Engineering.Resistor.ToParallel(seriesResistance, CDbl(parallelResistance))
    End Function

#End Region

#Region " NOMINAL VOLTAGE CONVERTERS "

    ''' <summary>
    ''' Converts an open load voltage of an attenuated voltage source to the nominal voltage of a non-
    ''' attenuated voltage source.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="openLoadVoltage">     The open load voltage. </param>
    ''' <param name="seriesResistance">    The series resistance. </param>
    ''' <param name="parallelConductance"> The parallel conductance. </param>
    ''' <returns> The given data converted to a Double. </returns>
    Public Shared Function ToNominalVoltage(ByVal openLoadVoltage As Double, ByVal seriesResistance As Double, ByVal parallelConductance As Double) As Double
        Return If(parallelConductance = 0 OrElse seriesResistance = 0,
            openLoadVoltage,
            openLoadVoltage * AttenuatedVoltageSource.ToAttenuation(seriesResistance, parallelConductance))
    End Function

    ''' <summary>
    ''' Converts nominal voltage of a non-attenuated voltage source to an open load voltage or the
    ''' equivalent attenuated voltage source.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="nominalVoltage">      The nominal voltage. </param>
    ''' <param name="seriesResistance">    The series resistance. </param>
    ''' <param name="parallelConductance"> The parallel conductance. </param>
    ''' <returns> The given data converted to a Double. </returns>
    Public Shared Function ToOpenLoadVoltage(ByVal nominalVoltage As Decimal, ByVal seriesResistance As Double, ByVal parallelConductance As Double) As Double
        Return If(parallelConductance = 0 OrElse seriesResistance = 0,
            nominalVoltage,
            nominalVoltage / AttenuatedVoltageSource.ToAttenuation(seriesResistance, parallelConductance))
    End Function

    ''' <summary>
    ''' Converts nominal voltage of a non-attenuated voltage source to an attenuated voltage source
    ''' with the specified seres and parallel resistances.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="nominalVoltage">      The nominal voltage. </param>
    ''' <param name="seriesResistance">    The series resistance. </param>
    ''' <param name="parallelConductance"> The parallel conductance. </param>
    ''' <returns> The given data converted to an AttenuatedVoltageSource. </returns>
    Public Overloads Shared Function ToAttenuatedVoltageSource(ByVal nominalVoltage As Decimal, ByVal seriesResistance As Double, ByVal parallelConductance As Double) As AttenuatedVoltageSource
        Return New AttenuatedVoltageSource(ToOpenLoadVoltage(nominalVoltage, seriesResistance, parallelConductance), seriesResistance, parallelConductance)
    End Function

#End Region

#Region " VOLTAGE SOURCE CONVERTERS "

    ''' <summary>
    ''' Build a voltage source using the open load voltage and equivalent resistance for the given
    ''' resistance and conductance values.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="openLoadVoltage">     The open load voltage. </param>
    ''' <param name="seriesResistance">    The series resistance. </param>
    ''' <param name="parallelConductance"> The parallel conductance. </param>
    ''' <returns> The given data converted to a VoltageSource. </returns>
    Public Shared Function ToVoltageSource(ByVal openLoadVoltage As Double, ByVal seriesResistance As Double, ByVal parallelConductance As Double) As VoltageSource
        Return New VoltageSource(openLoadVoltage, AttenuatedVoltageSource.ToEquivalentResistance(seriesResistance, parallelConductance))
    End Function

    ''' <summary>
    ''' Build a voltage source using the nominal voltage and equivalent resistance for the given
    ''' resistance and conductance values.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="nominalVoltage">      The nominal voltage. </param>
    ''' <param name="seriesResistance">    The series resistance. </param>
    ''' <param name="parallelConductance"> The parallel conductance. </param>
    ''' <returns> The given data converted to a VoltageSource. </returns>
    Public Shared Function ToVoltageSource(ByVal nominalVoltage As Decimal, ByVal seriesResistance As Double, ByVal parallelConductance As Double) As VoltageSource
        Return New VoltageSource(AttenuatedVoltageSource.ToOpenLoadVoltage(nominalVoltage, seriesResistance, parallelConductance),
                                 AttenuatedVoltageSource.ToEquivalentResistance(seriesResistance, parallelConductance))
    End Function

    ''' <summary>
    ''' Converts the attenuated voltage source to a voltage source with equivalent resistance and
    ''' open load voltage.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> The given data converted to a VoltageSource. </returns>
    Public Function ToVoltageSource() As VoltageSource
        Return New VoltageSource(Me.Voltage, Me.Resistance)
    End Function

    ''' <summary> Update this object from a voltage source. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="voltageSource">  The voltage source. </param>
    ''' <param name="nominalVoltage"> The nominal voltage. </param>
    Public Overloads Sub FromVoltageSource(ByVal voltageSource As VoltageSource, ByVal nominalVoltage As Decimal)
        If voltageSource Is Nothing Then Throw New ArgumentNullException(NameOf(voltageSource))
        Dim attenuation As Double = AttenuatedVoltageSource.ToAttenuation(nominalVoltage, voltageSource.Voltage)
        Me.Initialize(New VoltageSource(nominalVoltage, voltageSource.Resistance), attenuation)
    End Sub

#End Region

#Region " CURRENT SOURCE CONVERTERS "

    ''' <summary> Updates this object from an Attenuated current source. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="currentSource">  The current source. </param>
    ''' <param name="nominalVoltage"> The nominal voltage. </param>
    Public Sub FromAttenuatedCurrentSource(ByVal currentSource As AttenuatedCurrentSource, ByVal nominalVoltage As Decimal)
        If currentSource Is Nothing Then Throw New ArgumentNullException(NameOf(currentSource))
        Dim voltage As Double = currentSource.Current / currentSource.Conductance
        Dim attenuation As Double = nominalVoltage / voltage
        Me.Initialize(New VoltageSource(voltage, 1 / currentSource.Conductance), attenuation)
    End Sub

    ''' <summary> Converts a nominalCurrent to an Attenuated current source. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="nominalCurrent"> The nominal current. </param>
    ''' <returns> NominalCurrent as an AttenuatedCurrentSource. </returns>
    Public Function ToAttenuatedCurrentSource(ByVal nominalCurrent As Double) As AttenuatedCurrentSource
        Me.ValidateAttenuatedCurrentSourceConversion(nominalCurrent)
        Return Me.ToCurrentSource().ToAttenuatedCurrentSource(CDec(nominalCurrent))
    End Function

    ''' <summary>
    ''' Validates the attenuated current source conversion described by nominalCurrent.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="nominalCurrent"> The nominal current. </param>
    Public Sub ValidateAttenuatedCurrentSourceConversion(ByVal nominalCurrent As Double)
        Dim shortLoadCurrent As Double = Me.LoadCurrent(Resistor.ShortResistance)
        If shortLoadCurrent > nominalCurrent Then
            Throw New InvalidOperationException($"Voltage source with an short load current of {shortLoadCurrent:G4} cannot be converted to a current source with a lower nominal current of {nominalCurrent:G4}")
        End If
    End Sub

#End Region

End Class
