''' <summary> an Attenuated current source. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/15/2017 </para>
''' </remarks>
Public Class AttenuatedCurrentSource
    Inherits CurrentSource

    #Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="shortLoadCurrent">    The short load current. </param>
    ''' <param name="seriesResistance">    The series resistance. </param>
    ''' <param name="parallelConductance"> The parallel conductance. </param>
    Public Sub New(ByVal shortLoadCurrent As Double, ByVal seriesResistance As Double, ByVal parallelConductance As Double)
        MyBase.New(AttenuatedCurrentSource.ToCurrentSource(shortLoadCurrent, seriesResistance, parallelConductance))
        Me.InitializeThis(shortLoadCurrent, seriesResistance, parallelConductance)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="nominalCurrent">      The nominal Current. </param>
    ''' <param name="seriesResistance">    The series resistance. </param>
    ''' <param name="parallelConductance"> The parallel conductance. </param>
    Public Sub New(ByVal nominalCurrent As Decimal, ByVal seriesResistance As Double, ByVal parallelConductance As Double)
        MyBase.New(AttenuatedCurrentSource.ToCurrentSource(nominalCurrent, seriesResistance, parallelConductance))
        Me.InitializeThis(nominalCurrent, seriesResistance, parallelConductance)
    End Sub

    ''' <summary> The cloning Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="currentSource"> The current source. </param>
    '''
    ''' ### <param name="CurrentSource"> The Current source. </param>
    Public Sub New(ByVal currentSource As AttenuatedCurrentSource)
        Me.New(ValidatedCurrentSource(currentSource).Current, currentSource.SeriesResistance, currentSource.ParallelConductance)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="nominalCurrent"> The nominal Current. </param>
    ''' <param name="resistance">     The resistance. </param>
    ''' <param name="attenuation">    The attenuation. </param>
    Public Sub New(ByVal nominalCurrent As Decimal, ByVal resistance As Double, ByVal attenuation As Decimal)
        Me.New(New CurrentSource(nominalCurrent, resistance), attenuation)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="source">      The current source. </param>
    ''' <param name="attenuation"> The attenuation. </param>
    Public Sub New(ByVal source As CurrentSource, ByVal attenuation As Double)
        Me.New(CurrentSource.ToAttenuatedCurrentSource(CurrentSource.ValidatedCurrentSource(source), attenuation))
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="source">         The current source. </param>
    ''' <param name="nominalCurrent"> The nominal Current. </param>
    Public Sub New(ByVal source As CurrentSource, ByVal nominalCurrent As Decimal)
        Me.New(CurrentSource.ToAttenuatedCurrentSource(CurrentSource.ValidatedCurrentSource(source), nominalCurrent))
    End Sub

    ''' <summary> Validated attenuated current source. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="currentSource"> The current source. </param>
    ''' <returns> An AttenuatedCurrentSource. </returns>
    Public Overloads Shared Function ValidatedCurrentSource(ByVal currentSource As AttenuatedCurrentSource) As AttenuatedCurrentSource
        If currentSource Is Nothing Then Throw New ArgumentNullException(NameOf(currentSource))
        Return currentSource
    End Function

    ''' <summary> Initializes this object. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="shortLoadCurrent">    The short load current. </param>
    ''' <param name="seriesResistance">    The series resistance. </param>
    ''' <param name="parallelConductance"> The parallel conductance. </param>
    Private Sub InitializeThis(ByVal shortLoadCurrent As Double, ByVal seriesResistance As Double, ByVal parallelConductance As Double)
        Me._SeriesResistance = seriesResistance
        Me._ParallelConductance = parallelConductance
        Me._Attenuation = AttenuatedCurrentSource.ToAttenuation(seriesResistance, parallelConductance)
        Me._NominalCurrent = Me._Attenuation * shortLoadCurrent
    End Sub

    ''' <summary> Initializes this object. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="nominalCurrent">      The nominal Current. </param>
    ''' <param name="seriesResistance">    The series resistance. </param>
    ''' <param name="parallelConductance"> The parallel conductance. </param>
    Private Sub InitializeThis(ByVal nominalCurrent As Decimal, ByVal seriesResistance As Double, ByVal parallelConductance As Double)
        Me._SeriesResistance = seriesResistance
        Me._ParallelConductance = parallelConductance
        Me._Attenuation = AttenuatedCurrentSource.ToAttenuation(seriesResistance, parallelConductance)
        Me._NominalCurrent = nominalCurrent
    End Sub

    ''' <summary> Initializes this object. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="shortLoadCurrent">    The short load current. </param>
    ''' <param name="seriesResistance">    The series resistance. </param>
    ''' <param name="parallelConductance"> The parallel conductance. </param>
    Public Overloads Sub Initialize(ByVal shortLoadCurrent As Double, ByVal seriesResistance As Double, ByVal parallelConductance As Double)
        Me.InitializeThis(shortLoadCurrent, seriesResistance, parallelConductance)
        MyBase.Initialize(shortLoadCurrent / Me._Attenuation, AttenuatedCurrentSource.ToEquivalentConductance(seriesResistance, parallelConductance))
    End Sub

    ''' <summary> Initializes this object. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="nominalCurrent">      The nominal Current. </param>
    ''' <param name="seriesResistance">    The series resistance. </param>
    ''' <param name="parallelConductance"> The parallel conductance. </param>
    Public Overloads Sub Initialize(ByVal nominalCurrent As Decimal, ByVal seriesResistance As Double, ByVal parallelConductance As Double)
        Me.InitializeThis(nominalCurrent, seriesResistance, parallelConductance)
        MyBase.Initialize(CDbl(nominalCurrent / Me._Attenuation), AttenuatedCurrentSource.ToEquivalentConductance(seriesResistance, parallelConductance))
    End Sub

    ''' <summary> Initializes this object. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="currentSource"> The current source. </param>
    ''' <param name="attenuation">   The attenuation. The ratio of nominal to equivalent current
    '''                              source current. </param>
    Public Overloads Sub Initialize(ByVal currentSource As CurrentSource, ByVal attenuation As Double)
        If currentSource Is Nothing Then Throw New ArgumentNullException(NameOf(currentSource))
        If attenuation < AttenuatedCurrentSource.MinimumAttenuation Then Throw New ArgumentOutOfRangeException(NameOf(attenuation),
                                                                          $"must be greater or equal to {AttenuatedCurrentSource.MinimumAttenuation}")
        Me._Attenuation = attenuation
        Me._NominalCurrent = currentSource.Current * Me.Attenuation
        Me._ParallelConductance = Me.Attenuation * currentSource.Conductance
        Me._SeriesResistance = (attenuation - 1) / Me._ParallelConductance
        MyBase.Initialize(currentSource)
    End Sub

    ''' <summary> Initializes this object. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="attenuation"> The attenuation. </param>
    Public Overloads Sub Initialize(ByVal attenuation As Double)
        If attenuation < AttenuatedCurrentSource.MinimumAttenuation Then Throw New ArgumentOutOfRangeException(NameOf(attenuation),
                                                                          $"must be greater or equal to {AttenuatedCurrentSource.MinimumAttenuation}")
        Me._Attenuation = attenuation
        Me._NominalCurrent = Me.Current * Me.Attenuation
        Me._ParallelConductance = Me.Attenuation * Me.Conductance
        Me._SeriesResistance = (attenuation - 1) / Me._ParallelConductance
    End Sub

    #End Region

    #Region " EQUALS "

    ''' <summary>
    ''' Determines whether the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
    '''                    <see cref="T:System.Object" />. </param>
    ''' <returns>
    ''' <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />; otherwise, <c>False</c>.
    ''' </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return Me.Equals(TryCast(obj, AttenuatedCurrentSource))
    End Function

    ''' <summary>
    ''' Compares two Attenuated Current Sources. The Attenuated Current Sources are compared using
    ''' their resistances and Currents.
    ''' </summary>
    ''' <remarks> The balances are the same if the have the same resistances and Currents. </remarks>
    ''' <param name="other"> Specifies the other <see cref="AttenuatedCurrentSource">Attenuated
    '''                      Current Source</see>
    '''                      to compare for equality with this instance. </param>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Function Equals(ByVal other As AttenuatedCurrentSource) As Boolean
        Return other IsNot Nothing AndAlso MyBase.Equals(other) AndAlso Me.NominalCurrent.Equals(other.NominalCurrent) AndAlso
               Me.SeriesResistance.Equals(other.SeriesResistance) AndAlso Me.ParallelResistance.Equals(other.ParallelResistance)
    End Function

    ''' <summary> Compares two Attenuated Current Sources. </summary>
    ''' <remarks> The balances are the same if the have the same resistances and Currents. </remarks>
    ''' <param name="other">     Specifies the other <see cref="AttenuatedCurrentSource">Attenuated
    '''                          Current Source</see>
    '''                          to compare for equality with this instance. </param>
    ''' <param name="tolerance"> Specifies the relative tolerance for comparing the two values. The
    '''                          values are compared based on their end points. </param>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Function Equals(ByVal other As AttenuatedCurrentSource, ByVal tolerance As Double) As Boolean
        If other Is Nothing Then
            Return False
        ElseIf Me.Equals(other) Then
            Return True
        Else
            Return MyBase.Equals(other, tolerance) AndAlso
                Math.Abs(Me.NominalCurrent - other.NominalCurrent) < other.NominalCurrent * tolerance AndAlso
                Math.Abs(Me.SeriesResistance - other.SeriesResistance) <= other.SeriesResistance * tolerance AndAlso
                Math.Abs(Me.ParallelResistance - other.ParallelResistance) <= other.ParallelResistance * tolerance
        End If
    End Function

    ''' <summary> Implements the operator =. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Overloads Shared Function Equals(ByVal left As AttenuatedCurrentSource, ByVal right As AttenuatedCurrentSource) As Boolean
        Return (CObj(left) Is CObj(right)) OrElse (left IsNot Nothing AndAlso left.Equals(right))
    End Function

    ''' <summary>
    ''' Compares two Attenuated Current Sources. The Attenuated Current Sources are compared using
    ''' their resistances and Currents.
    ''' </summary>
    ''' <remarks> The balances are the same if the have the same values and layout. </remarks>
    ''' <param name="left">      Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right">     Specifies the right hand side argument of the binary operation. </param>
    ''' <param name="tolerance"> Specifies the relative tolerance for comparing the two values. The
    '''                          values are compared based on their end points. </param>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Shared Function Equals(ByVal left As AttenuatedCurrentSource, ByVal right As AttenuatedCurrentSource, ByVal tolerance As Double) As Boolean
        Return ((left Is Nothing) AndAlso (right Is Nothing)) OrElse (left IsNot Nothing) AndAlso left.Equals(right, tolerance)
    End Function

    ''' <summary> Implements the operator =. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Overloads Shared Operator =(ByVal left As AttenuatedCurrentSource, ByVal right As AttenuatedCurrentSource) As Boolean
        Return AttenuatedCurrentSource.Equals(left, right)
    End Operator

    ''' <summary> Implements the operator &lt;&gt;. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Overloads Shared Operator <>(ByVal left As AttenuatedCurrentSource, ByVal right As AttenuatedCurrentSource) As Boolean
        Return Not AttenuatedCurrentSource.Equals(left, right)
    End Operator

    ''' <summary> Creates a unique hash code. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> An <see cref="System.Int32">Int32</see> value. </returns>
    Public Overloads Overrides Function GetHashCode() As Int32
        Return MyBase.GetHashCode Xor Me.NominalCurrent.GetHashCode Xor
            Me.SeriesResistance.GetHashCode Xor Me.ParallelResistance.GetHashCode
    End Function

    #End Region

    #Region " TO STRING "

    ''' <summary> Returns a string that represents the current object. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> A string that represents the current object. </returns>
    Public Overrides Function ToString() As String
        Return $"{Me.NominalCurrent}:{Me.SeriesResistance}:{Me.ParallelConductance}"
    End Function

    ''' <summary> Returns a string that represents the current object. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="currentFormat">    The Current format. </param>
    ''' <param name="resistanceFormat"> The resistance format. </param>
    ''' <returns> A string that represents the current object. </returns>
    Public Overloads Function ToString(ByVal currentFormat As String, ByVal resistanceFormat As String) As String
        Return $"{String.Format(currentFormat, Me.NominalCurrent)}:{String.Format(resistanceFormat, Me.SeriesResistance)}:{String.Format(resistanceFormat, Me.ParallelResistance)}"
    End Function

    #End Region

    #Region " COMPONENTS "
    ''' <summary> The nominal current. </summary>
    Private _NominalCurrent As Double

    ''' <summary> Gets or sets the Current of the attenuated source. </summary>
    ''' <value> The nominal Current. </value>
    Public Property NominalCurrent As Double
        Get
            Return Me._NominalCurrent
        End Get
        Set(value As Double)
            If value <> Me.NominalCurrent Then
                Me._NominalCurrent = value
                Me.Initialize(CDec(Me.NominalCurrent), Me.SeriesResistance, Me.ParallelConductance)
            End If
        End Set
    End Property
    ''' <summary> The series resistance. </summary>
    Private _SeriesResistance As Double

    ''' <summary> Gets or sets the series resistance. </summary>
    ''' <value> The series resistance. </value>
    Public Property SeriesResistance As Double
        Get
            Return Me._SeriesResistance
        End Get
        Set(value As Double)
            If value <> Me.SeriesResistance Then
                Me._SeriesResistance = value
                Me.Initialize(CDec(Me.NominalCurrent), Me.SeriesResistance, Me.ParallelConductance)
            End If
        End Set
    End Property

    ''' <summary> Gets the equivalent resistance. </summary>
    ''' <value> The equivalent resistance. </value>
    Public ReadOnly Property Resistance As Double
        Get
            Return Conductor.ToResistance(Me.Conductance)
        End Get
    End Property

    ''' <summary> Gets the parallel resistance. </summary>
    ''' <value> The parallel resistance. </value>
    Public ReadOnly Property ParallelResistance As Double
        Get
            Return Conductor.ToResistance(Me.ParallelConductance)
        End Get
    End Property
    ''' <summary> The parallel conductance. </summary>
    Private _ParallelConductance As Double

    ''' <summary> Gets or sets the parallel conductance. </summary>
    ''' <value> The parallel conductance. </value>
    Public Property ParallelConductance As Double
        Get
            Return Me._ParallelConductance
        End Get
        Set(value As Double)
            If value <> Me.ParallelConductance Then
                Me._ParallelConductance = value
                Me.Initialize(CDec(Me.NominalCurrent), Me.SeriesResistance, Me.ParallelConductance)
            End If
        End Set
    End Property

    ''' <summary> The minimum attenuation. This is the minimum ratio between the nominal source current to the equivalent source current over a short. </summary>
    Public Const MinimumAttenuation As Double = 1
    ''' <summary> The attenuation. </summary>
    Private _Attenuation As Double

    ''' <summary>
    ''' Gets or sets the attenuation. This is the ratio of the nominal source voltage to the
    ''' equivalent source voltage.
    ''' </summary>
    ''' <value> The attenuation. </value>
    Public Property Attenuation As Double
        Get
            Return Me._Attenuation
        End Get
        Set(value As Double)
            If value <> Me.Attenuation Then
                Me.Initialize(value)
            End If
        End Set
    End Property

    #End Region

    #Region " ATTENUATION AND EQUIVALENT CONDUCTANCE "

    ''' <summary> Evaluates attenuation. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="seriesResistance">    The series resistance. </param>
    ''' <param name="parallelConductance"> The parallel conductance. </param>
    ''' <returns> A Double. </returns>
    Public Shared Function ToAttenuation(ByVal seriesResistance As Double, ByVal parallelConductance As Double) As Double
        If seriesResistance < 0 Then Throw New ArgumentException($"Value {seriesResistance} must not be negative", NameOf(seriesResistance))
        If parallelConductance < 0 Then Throw New ArgumentException($"Value {parallelConductance} must not be negative", NameOf(parallelConductance))
        Return If(seriesResistance = 0 OrElse parallelConductance = 0, 1, 1 + seriesResistance * parallelConductance)
    End Function

    ''' <summary> Evaluates attenuation. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="seriesResistance">   The series resistance. </param>
    ''' <param name="parallelResistance"> The parallel resistance. </param>
    ''' <returns> A Double. </returns>
    Public Shared Function ToAttenuation(ByVal seriesResistance As Double, ByVal parallelResistance As Decimal) As Double
        If parallelResistance <= 0 Then Throw New ArgumentException($"Value {parallelResistance} must be positive", NameOf(parallelResistance))
        Return AttenuatedVoltageSource.ToAttenuation(seriesResistance, 1 / parallelResistance)
    End Function

    ''' <summary> Evaluates attenuation. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="nominalCurrent">   The nominal current. </param>
    ''' <param name="shortLoadCurrent"> The short load current. </param>
    ''' <returns> A Double. </returns>
    Public Shared Function ToAttenuation(ByVal nominalCurrent As Decimal, ByVal shortLoadCurrent As Double) As Double
        Return nominalCurrent / shortLoadCurrent
    End Function

    ''' <summary> Evaluates equivalent Conductance. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="seriesResistance">    The series resistance. </param>
    ''' <param name="parallelConductance"> The parallel conductance. </param>
    ''' <returns> A Double. </returns>
    Public Shared Function ToEquivalentConductance(ByVal seriesResistance As Double, ByVal parallelConductance As Double) As Double
        If seriesResistance < 0 Then Throw New ArgumentException($"Value {seriesResistance} must not be negative", NameOf(seriesResistance))
        If parallelConductance < 0 Then Throw New ArgumentException($"Value {parallelConductance} must not be negative", NameOf(parallelConductance))
        Return Core.Engineering.Conductor.SeriesResistor(parallelConductance, seriesResistance)
    End Function

    ''' <summary> Evaluates equivalent Conductance. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="seriesResistance">   The series resistance. </param>
    ''' <param name="parallelResistance"> The parallel resistance. </param>
    ''' <returns> A Double. </returns>
    Public Shared Function ToEquivalentConductance(ByVal seriesResistance As Double, ByVal parallelResistance As Decimal) As Double
        If parallelResistance <= 0 Then Throw New ArgumentException($"Value {parallelResistance} must be positive", NameOf(parallelResistance))
        If seriesResistance < 0 Then Throw New ArgumentException($"Value {seriesResistance} must not be negative", NameOf(seriesResistance))
        Return Core.Engineering.Resistor.ToConductance(Core.Engineering.Resistor.ToSeries(seriesResistance, CDbl(parallelResistance)))
    End Function

    #End Region

    #Region " NOMINAL CURRENT CONVERTERS "

    ''' <summary>
    ''' Converts an short load current of an attenuated current source to the nominal current of a
    ''' non-attenuated current source.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="shortLoadCurrent">    The short load current. </param>
    ''' <param name="seriesResistance">    The series resistance. </param>
    ''' <param name="parallelConductance"> The parallel conductance. </param>
    ''' <returns> The given data converted to a Double. </returns>
    Public Shared Function ToNominalCurrent(ByVal shortLoadCurrent As Double, ByVal seriesResistance As Double, ByVal parallelConductance As Double) As Double
        Return If(parallelConductance = 0 OrElse seriesResistance = 0,
            shortLoadCurrent,
            shortLoadCurrent * AttenuatedCurrentSource.ToAttenuation(seriesResistance, parallelConductance))
    End Function

    ''' <summary>
    ''' Converts nominal current of a non-attenuated current source to an short load current or the
    ''' equivalent attenuated current source.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="nominalCurrent">      The nominal current. </param>
    ''' <param name="seriesResistance">    The series resistance. </param>
    ''' <param name="parallelConductance"> The parallel conductance. </param>
    ''' <returns> The given data converted to a Double. </returns>
    Public Shared Function ToShortLoadCurrent(ByVal nominalCurrent As Decimal, ByVal seriesResistance As Double, ByVal parallelConductance As Double) As Double
        Return If(parallelConductance = 0 OrElse seriesResistance = 0,
            nominalCurrent,
            nominalCurrent / AttenuatedCurrentSource.ToAttenuation(seriesResistance, parallelConductance))
    End Function

    ''' <summary>
    ''' Converts nominal current of a non-attenuated current source to an attenuated current source
    ''' with the specified seres and parallel resistances.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="nominalCurrent">      The nominal current. </param>
    ''' <param name="seriesResistance">    The series resistance. </param>
    ''' <param name="parallelConductance"> The parallel conductance. </param>
    ''' <returns> The given data converted to an AttenuatedcurrentSource. </returns>
    Public Overloads Shared Function ToAttenuatedCurrentSource(ByVal nominalCurrent As Decimal, ByVal seriesResistance As Double, ByVal parallelConductance As Double) As AttenuatedCurrentSource
        Return New AttenuatedCurrentSource(ToShortLoadCurrent(nominalCurrent, seriesResistance, parallelConductance), seriesResistance, parallelConductance)
    End Function

    #End Region

    #Region " CONVERTERS "

    ''' <summary>
    ''' Build a current source using the short load current and equivalent conductance for the given
    ''' resistance and conductance values.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="shortLoadCurrent">    The short load current. </param>
    ''' <param name="seriesResistance">    The series resistance. </param>
    ''' <param name="parallelConductance"> The parallel conductance. </param>
    ''' <returns> The given data converted to a CurrentSource. </returns>
    '''
    ''' ### <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are
    '''                                                    outside the required range. </exception>
    Public Shared Function ToCurrentSource(ByVal shortLoadCurrent As Double, ByVal seriesResistance As Double, ByVal parallelConductance As Double) As CurrentSource
        Return New CurrentSource(shortLoadCurrent, AttenuatedCurrentSource.ToEquivalentConductance(seriesResistance, parallelConductance))
    End Function

    ''' <summary>
    ''' Build a current source using the nominal current and equivalent conductance for the given
    ''' resistance and conductance values.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="nominalCurrent">      The nominal Current. </param>
    ''' <param name="seriesResistance">    The series resistance. </param>
    ''' <param name="parallelConductance"> The parallel conductance. </param>
    ''' <returns> The given data converted to a CurrentSource. </returns>
    '''
    ''' ### <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are
    '''                                                    outside the required range. </exception>
    Public Shared Function ToCurrentSource(ByVal nominalCurrent As Decimal, ByVal seriesResistance As Double, ByVal parallelConductance As Double) As CurrentSource
        Return New CurrentSource(AttenuatedCurrentSource.ToShortLoadCurrent(nominalCurrent, seriesResistance, parallelConductance),
                                 AttenuatedCurrentSource.ToEquivalentConductance(seriesResistance, parallelConductance))
    End Function

    ''' <summary> Converts this object to a Current source. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> The given data converted to a CurrentSource. </returns>
    Public Function ToCurrentSource() As CurrentSource
        Return New CurrentSource(Me.Current, Me.Conductance)
    End Function

    ''' <summary> From voltage source. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="voltageSource">  The voltage source. </param>
    ''' <param name="nominalCurrent"> The nominal Current. </param>
    Public Overloads Sub FromVoltageSource(ByVal voltageSource As VoltageSource, ByVal nominalCurrent As Double)
        If voltageSource Is Nothing Then Throw New ArgumentNullException(NameOf(voltageSource))
        Dim attenuation As Double = nominalCurrent * voltageSource.Resistance / voltageSource.Voltage
        Me.Initialize(New CurrentSource(nominalCurrent, 1 / voltageSource.Resistance), attenuation)
    End Sub

    ''' <summary> From Attenuated voltage source. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="voltageSource">  The voltage source. </param>
    ''' <param name="nominalCurrent"> The nominal Current. </param>
    Public Sub FromAttenuatedVoltageSource(ByVal voltageSource As AttenuatedVoltageSource, ByVal nominalCurrent As Double)
        If voltageSource Is Nothing Then Throw New ArgumentNullException(NameOf(voltageSource))
        Dim attenuation As Double = nominalCurrent * voltageSource.Resistance / voltageSource.Voltage
        Me.Initialize(New CurrentSource(nominalCurrent, 1 / voltageSource.Resistance), attenuation)
    End Sub

    ''' <summary> Converts a nominalVoltage to an Attenuated voltage source. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="nominalVoltage"> The nominal voltage. </param>
    ''' <returns> NominalVoltage as an AttenuatedVoltageSource. </returns>
    Public Function ToAttenuatedVoltageSource(ByVal nominalVoltage As Double) As AttenuatedVoltageSource
        Me.ValidateAttenuatedVoltageSourceConversion(nominalVoltage)
        Return Me.ToVoltageSource.ToAttenuatedVoltageSource(nominalVoltage / Me.ToVoltageSource.Voltage)
    End Function

    ''' <summary>
    ''' Validates the attenuated voltage source conversion described by nominalVoltage.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="nominalVoltage"> The nominal voltage. </param>
    Public Sub ValidateAttenuatedVoltageSourceConversion(ByVal nominalVoltage As Double)
        Dim openLoadVoltage As Double = Me.LoadVoltage(Resistor.OpenResistance)
        If openLoadVoltage > nominalVoltage Then
            Throw New InvalidOperationException($"Current source with an open load voltage of {openLoadVoltage:G4} cannot be converted to a voltage source with a lower nominal voltage of {nominalVoltage:G4}")
        End If
    End Sub

    #End Region

End Class
