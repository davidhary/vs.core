﻿''' <summary> A Seebeck measure. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 6/28/2018 </para>
''' </remarks>
Public Class SeebeckMeasure

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="voltage">         The voltage. </param>
    ''' <param name="lowTemperature">  The low temperature. </param>
    ''' <param name="highTemperature"> The high temperature. </param>
    Public Sub New(ByVal voltage As Double, ByVal lowTemperature As Double, ByVal highTemperature As Double)
        Me.New
        Me.Voltage = voltage
        Me.LowTemperature = lowTemperature
        Me.HighTemperature = highTemperature
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="seebeckMeasure"> The seebeck measure. </param>
    Public Sub New(ByVal seebeckMeasure As SeebeckMeasure)
        Me.New(SeebeckMeasure.Validated(seebeckMeasure).Voltage, seebeckMeasure.LowTemperature, seebeckMeasure.HighTemperature)
    End Sub

    ''' <summary> Validated the given seebeck measure. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="seebeckMeasure"> The seebeck measure. </param>
    ''' <returns> A SeebeckMeasure. </returns>
    Public Shared Function Validated(ByVal seebeckMeasure As SeebeckMeasure) As SeebeckMeasure
        If seebeckMeasure Is Nothing Then Throw New ArgumentNullException(NameOf(seebeckMeasure))
        Return seebeckMeasure
    End Function

    ''' <summary> Gets the voltage. Zero if measurement was not made. </summary>
    ''' <value> The voltage. </value>
    Public Property Voltage As Double

    ''' <summary> Gets the high temperature. </summary>
    ''' <value> The high temperature. </value>
    Public Property HighTemperature As Double

    ''' <summary> Gets the low temperature. </summary>
    ''' <value> The low temperature. </value>
    Public Property LowTemperature As Double

    ''' <summary> Gets true if the seebeck measurement temperature difference is positive. </summary>
    ''' <value> The indicator of having valid temperatures. </value>
    Public ReadOnly Property HasTemperatureDifference As Boolean
        Get
            Return (Me.HighTemperature - Me.LowTemperature) <> 0
        End Get
    End Property

    ''' <summary> Gets the has measurement. </summary>
    ''' <value> The has measurement. </value>
    Public ReadOnly Property HasMeasurement As Boolean
        Get
            Return Me.Voltage <> 0 AndAlso Me.HasTemperatureDifference
        End Get
    End Property

    ''' <summary> Gets the Seebeck Coefficient. </summary>
    ''' <value>
    ''' The Seebeck Coefficient or <see cref="Double.NaN"/> if temperature difference is not positive.
    ''' </value>
    Public ReadOnly Property SeebeckCoefficient As Double
        Get
            Return If(Me.HasMeasurement, Me.Voltage / (Me.HighTemperature - Me.LowTemperature), Double.NaN)
        End Get
    End Property

End Class
