''' <summary> A current source. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/15/2017 </para>
''' </remarks>
Public Class CurrentSource

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="current">     The Current. </param>
    ''' <param name="conductance"> The conductance. </param>
    Public Sub New(ByVal current As Double, ByVal conductance As Double)
        MyBase.New()
        Me.Initialize(current, conductance)
    End Sub

    ''' <summary> Cloning constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="currentSource"> The current source. </param>
    Public Sub New(ByVal currentSource As CurrentSource)
        Me.New(CurrentSource.ValidatedCurrentSource(currentSource).Current, currentSource.Conductance)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="voltageSource"> The voltage source. </param>
    Public Sub New(ByVal voltageSource As VoltageSource)
        Me.New(VoltageSource.ValidatedVoltageSource(voltageSource).ToCurrentSource)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="currentSource"> The current source. </param>
    Public Sub New(ByVal currentSource As AttenuatedCurrentSource)
        Me.New(AttenuatedCurrentSource.ValidatedCurrentSource(currentSource).ToCurrentSource)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="voltageSource"> The voltage source. </param>
    Public Sub New(ByVal voltageSource As AttenuatedVoltageSource)
        Me.New(AttenuatedVoltageSource.ValidatedVoltageSource(voltageSource).ToCurrentSource)
    End Sub

    ''' <summary> Validated current source. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="currentSource"> The current source. </param>
    ''' <returns> A CurrentSource. </returns>
    Public Shared Function ValidatedCurrentSource(ByVal currentSource As CurrentSource) As CurrentSource
        If currentSource Is Nothing Then Throw New ArgumentNullException(NameOf(currentSource))
        Return currentSource
    End Function

    ''' <summary> Initializes this object. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="current">     The Current. </param>
    ''' <param name="conductance"> The conductance. </param>
    Private Sub InitializeThis(ByVal current As Double, ByVal conductance As Double)
        Me._Current = current
        Me._Conductance = conductance
    End Sub

    ''' <summary> Initializes this object. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="current">     The Current. </param>
    ''' <param name="conductance"> The conductance. </param>
    Public Sub Initialize(ByVal current As Double, ByVal conductance As Double)
        Me.InitializeThis(current, conductance)
    End Sub

    ''' <summary> Initializes this object. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="currentSource"> The current source. </param>
    Public Sub Initialize(ByVal currentSource As CurrentSource)
        If currentSource Is Nothing Then Throw New ArgumentNullException(NameOf(currentSource))
        Me.InitializeThis(currentSource.Current, currentSource.Conductance)
    End Sub

#End Region

#Region " EQUALS "

    ''' <summary>
    ''' Determines whether the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
    '''                    <see cref="T:System.Object" />. </param>
    ''' <returns>
    ''' <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />; otherwise, <c>False</c>.
    ''' </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return obj IsNot Nothing AndAlso Me.GetType() Is obj.GetType() AndAlso Me.Equals(CType(obj, CurrentSource))
    End Function

    ''' <summary>
    ''' Compares two Current Sources. The Current Sources are compared using their Conductances and
    ''' Currents.
    ''' </summary>
    ''' <remarks>
    ''' The Current Sources are the same if the have the same Current and Conductance.
    ''' </remarks>
    ''' <param name="other"> Specifies the other <see cref="CurrentSource">Current Source</see>
    '''                      to compare for equality with this instance. </param>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Function Equals(ByVal other As CurrentSource) As Boolean
        Return If(other Is Nothing, False, Me.Current.Equals(other.Current) AndAlso Me.Conductance.Equals(other.Conductance))
    End Function

    ''' <summary>
    ''' Compares two Current Sources. The Current Sources are compared using their Conductance and
    ''' Current.
    ''' </summary>
    ''' <remarks>
    ''' The Current Sources are the same if the have the same Current and Conductance.
    ''' </remarks>
    ''' <param name="other">     Specifies the other <see cref="CurrentSource">Current Source</see>
    '''                          to compare for equality with this instance. </param>
    ''' <param name="tolerance"> Specifies the relative tolerance for comparing the two values. The
    '''                          values are compared based on their end points. </param>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Function Equals(ByVal other As CurrentSource, ByVal tolerance As Double) As Boolean
        If other Is Nothing Then
            Return False
        ElseIf Me.Equals(other) Then
            Return True
        Else
            Return Math.Abs(Me.Current - other.Current) <= other.Current * tolerance AndAlso
                   Math.Abs(Me.Conductance - other.Conductance) <= other.Conductance * tolerance
        End If
    End Function

    ''' <summary> Implements the operator =. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Overloads Shared Function Equals(ByVal left As CurrentSource, ByVal right As CurrentSource) As Boolean
        Return (CObj(left) Is CObj(right)) OrElse (left IsNot Nothing AndAlso left.Equals(right))
    End Function

    ''' <summary>
    ''' Compares two Current Sources. The Current Sources are compared using their Conductance and
    ''' Current.
    ''' </summary>
    ''' <remarks>
    ''' The Current Sources are the same if the have the same Current and Conductance.
    ''' </remarks>
    ''' <param name="left">      Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right">     Specifies the right hand side argument of the binary operation. </param>
    ''' <param name="tolerance"> Specifies the relative tolerance for comparing the two values. The
    '''                          values are compared based on their end points. </param>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Shared Function Equals(ByVal left As CurrentSource, ByVal right As CurrentSource, ByVal tolerance As Double) As Boolean
        Return ((left Is Nothing) AndAlso (right Is Nothing)) OrElse (left IsNot Nothing) AndAlso left.Equals(right, tolerance)
    End Function

    ''' <summary> Implements the operator =. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Overloads Shared Operator =(ByVal left As CurrentSource, ByVal right As CurrentSource) As Boolean
        Return CurrentSource.Equals(left, right)
    End Operator

    ''' <summary> Implements the operator &lt;&gt;. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Overloads Shared Operator <>(ByVal left As CurrentSource, ByVal right As CurrentSource) As Boolean
        Return Not CurrentSource.Equals(left, right)
    End Operator

    ''' <summary> Creates a unique hash code. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> An <see cref="System.Int32">Int32</see> value. </returns>
    Public Overloads Overrides Function GetHashCode() As Int32
        Return Me.Current.GetHashCode Xor Me.Conductance.GetHashCode
    End Function

#End Region

#Region " TO STRING "

    ''' <summary> Returns a string that represents the current object. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> A string that represents the current object. </returns>
    Public Overrides Function ToString() As String
        Return $"{Me.Current}:{Me.Conductance}"
    End Function

    ''' <summary> Returns a string that represents the current object. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="currentFormat">     The Current format. </param>
    ''' <param name="conductanceFormat"> The conductance format. </param>
    ''' <returns> A string that represents the current object. </returns>
    Public Overloads Function ToString(ByVal currentFormat As String, ByVal conductanceFormat As String) As String
        Return $"{String.Format(currentFormat, Me.Current)}:{String.Format(conductanceFormat, Me.Conductance)}"
    End Function

#End Region

#Region " COMPONENETS "

    ''' <summary> Gets or sets the current source Current. This is the current into a short. </summary>
    ''' <value> The Current. </value>
    Public Property Current As Double

    ''' <summary> Gets or sets the current source equivalent conductance. </summary>
    ''' <value> The equivalent conductance. </value>
    Public Property Conductance As Double

#End Region

#Region " OUTPUT "

    ''' <summary> Load current. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="loadConductance"> The load conductance. </param>
    ''' <returns> A Double. </returns>
    Public Function LoadCurrent(ByVal loadConductance As Double) As Double
        If loadConductance = 0 Then
            Return 0
        ElseIf Double.IsInfinity(loadConductance) Then
            Return Me.Current
        End If
        Return Me.Current * loadConductance / (loadConductance + Me.Conductance)
    End Function

    ''' <summary> Returns the load voltage. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="loadResistance"> The load resistance. </param>
    ''' <returns> The voltage. </returns>
    Public Function LoadVoltage(ByVal loadResistance As Double) As Double
        If loadResistance = 0 Then
            Return 0
        ElseIf Double.IsInfinity(loadResistance) Then
            Return Me.Current / Me.Conductance
        Else
            Return loadResistance * Me.LoadCurrent(1 / loadResistance)
        End If
    End Function

#End Region

#Region " VOLTAGE SOURCE CONVERTERS "

    ''' <summary> Converts this object to a voltage source. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="current">     The Current. </param>
    ''' <param name="conductance"> The resistance. </param>
    ''' <returns> This object as a VoltageSource. </returns>
    Public Shared Function ToVoltageSource(ByVal current As Double, ByVal conductance As Double) As VoltageSource
        Dim resistance As Double = 1 / conductance
        Return New VoltageSource(current * resistance, resistance)
    End Function

    ''' <summary> Initializes this object from the given from voltage source. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="voltage">    The voltage. </param>
    ''' <param name="resistance"> The resistance. </param>
    ''' <returns> A CurrentSource. </returns>
    Public Shared Function FromVoltageSource(ByVal voltage As Double, ByVal resistance As Double) As CurrentSource
        Dim conductance As Double = 1 / resistance
        Return New CurrentSource(voltage * conductance, conductance)
    End Function

    ''' <summary> Converts this object to a voltage source. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> This object as a VoltageSource. </returns>
    Public Function ToVoltageSource() As VoltageSource
        Return CurrentSource.ToVoltageSource(Me.Current, Me.Conductance)
    End Function

    ''' <summary> Initializes this object from the given from voltage source. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="voltageSource"> The voltage source. </param>
    Public Sub FromVoltageSource(ByVal voltageSource As VoltageSource)
        If voltageSource Is Nothing Then Throw New ArgumentNullException(NameOf(voltageSource))
        Me.Conductance = 1 / voltageSource.Resistance
        Me.Current = voltageSource.Voltage * Me.Conductance
    End Sub

#End Region

#Region " ATTENUATED CURRENT SOURCE CONVERTERS "

    ''' <summary> Converts this object to an Attenuated current source. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="currentSource"> The current source. </param>
    ''' <param name="attenuation">   The attenuation. </param>
    ''' <returns> The given data converted to an AttenuatedCurrentSource. </returns>
    Public Shared Function ToAttenuatedCurrentSource(ByVal currentSource As CurrentSource, ByVal attenuation As Double) As AttenuatedCurrentSource
        If currentSource Is Nothing Then Throw New ArgumentNullException(NameOf(currentSource))
        If attenuation < AttenuatedVoltageSource.MinimumAttenuation Then Throw New ArgumentOutOfRangeException(NameOf(attenuation),
                                                                                   $"Value must be greater or equal to {AttenuatedCurrentSource.MinimumAttenuation}")
        Return currentSource.ToAttenuatedCurrentSource(attenuation)
    End Function

    ''' <summary> Converts this object to an Attenuated current source. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="attenuation"> The attenuation. </param>
    ''' <returns> The given data converted to an AttenuatedCurrentSource. </returns>
    Public Function ToAttenuatedCurrentSource(ByVal attenuation As Double) As AttenuatedCurrentSource
        If attenuation < AttenuatedCurrentSource.MinimumAttenuation Then Throw New ArgumentOutOfRangeException(NameOf(attenuation),
                                                                                                            $"Value must be greater or equal to {AttenuatedCurrentSource.MinimumAttenuation}")
        Dim conductance As Double = Me.Conductance * attenuation
        Dim resistance As Double = 0
        If attenuation > AttenuatedCurrentSource.MinimumAttenuation Then
            resistance = 1 / Me.Conductance - 1 / conductance
        End If
        Return New AttenuatedCurrentSource(Me.Current, resistance, conductance)
    End Function

    ''' <summary> Converts this object to an Attenuated current source. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="nominalCurrent"> The nominal current. </param>
    ''' <returns> The given data converted to an AttenuatedCurrentSource. </returns>
    Public Function ToAttenuatedCurrentSource(ByVal nominalCurrent As Decimal) As AttenuatedCurrentSource
        Return Me.ToAttenuatedCurrentSource(CDbl(nominalCurrent / Me.Current))
    End Function

    ''' <summary> Converts this object to an Attenuated current source. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="currentSource">  The current source. </param>
    ''' <param name="nominalCurrent"> The nominal current. </param>
    ''' <returns> The given data converted to an AttenuatedCurrentSource. </returns>
    Public Shared Function ToAttenuatedCurrentSource(ByVal currentSource As CurrentSource, ByVal nominalCurrent As Decimal) As AttenuatedCurrentSource
        If currentSource Is Nothing Then Throw New ArgumentNullException(NameOf(currentSource))
        Return currentSource.ToAttenuatedCurrentSource(nominalCurrent)
    End Function

#End Region

End Class
