﻿''' <summary> A conductor. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/16/2017 </para>
''' </remarks>
Public NotInheritable Class Conductor

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Constructor that prevents a default instance of this class from being created.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Private Sub New()
        MyBase.New
    End Sub

#End Region

#Region " CONDUCTOR PAIR "

    ''' <summary> Returns the equivalent conductance of two conductors in parallel. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="leftValue">  The left value. </param>
    ''' <param name="rightValue"> The right value. </param>
    ''' <returns> A Double. </returns>
    Public Shared Function Parallel(ByVal leftValue As Double, ByVal rightValue As Double) As Double
        Return leftValue + rightValue
    End Function

    ''' <summary> Returns the equivalent conductance of two conductors in series. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="leftValue">  The left value. </param>
    ''' <param name="rightValue"> The right value. </param>
    ''' <returns> A Double. </returns>
    Public Shared Function Series(ByVal leftValue As Double, ByVal rightValue As Double) As Double
        Return leftValue * rightValue / (leftValue + rightValue)
    End Function

    ''' <summary> Returns the relative current through the right parallel conductance. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="leftValue">  The left value. </param>
    ''' <param name="rightValue"> The right value. </param>
    ''' <returns> A Double. </returns>
    Public Shared Function OutputCurrent(ByVal leftValue As Double, ByVal rightValue As Double) As Double
        Return rightValue / (leftValue + rightValue)
    End Function

    ''' <summary> Returns the relative voltage across the right series conductance. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="leftValue">  The left value. </param>
    ''' <param name="rightValue"> The right value. </param>
    ''' <returns> A Double. </returns>
    Public Shared Function OutputVoltage(ByVal leftValue As Double, ByVal rightValue As Double) As Double
        Return leftValue / (leftValue + rightValue)
    End Function

#End Region

#Region " PAIR OF CONDUCTORS: WITH VALIDATIONS "

    ''' <summary> Gets or sets the open conductance. </summary>
    ''' <value> The open conductance. </value>
    Public Shared Property OpenConductance As Double = 0

    ''' <summary> Gets or sets the short conductance. </summary>
    ''' <value> The short conductance. </value>
    Public Shared Property ShortConductance As Double = Double.PositiveInfinity

    ''' <summary> Query if 'value' is open. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> <c>true</c> if open; otherwise <c>false</c> </returns>
    Public Shared Function IsOpen(ByVal value As Double) As Boolean
        Return value = Conductor.OpenConductance
    End Function

    ''' <summary> Query if 'value' is short. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> <c>true</c> if short; otherwise <c>false</c> </returns>
    Public Shared Function IsShort(ByVal value As Double) As Boolean
        Return value = Conductor.ShortConductance
    End Function

    ''' <summary> Returns the equivalent conductance of two parallel conductors. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="leftValue">  The left value. </param>
    ''' <param name="rightValue"> The right value. </param>
    ''' <returns> A Double. </returns>
    Public Shared Function ToParallel(ByVal leftValue As Double, ByVal rightValue As Double) As Double
        If leftValue < Conductor.OpenConductance Then Throw New ArgumentOutOfRangeException(NameOf(leftValue), "value must be non-negative")
        If rightValue < Conductor.OpenConductance Then Throw New ArgumentOutOfRangeException(NameOf(rightValue), "value must be non-negative")
        If Conductor.IsOpen(leftValue) Then
            Return rightValue
        ElseIf Conductor.IsOpen(rightValue) Then
            Return leftValue
        ElseIf Conductor.IsShort(leftValue) OrElse Conductor.IsShort(rightValue) Then
            Return Conductor.ShortConductance
        Else
            Return Conductor.Parallel(leftValue, rightValue)
        End If
    End Function

    ''' <summary> returns the equivalent series conductance of two conductors. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="leftValue">  The left value. </param>
    ''' <param name="rightValue"> The right value. </param>
    ''' <returns> A Double. </returns>
    Public Shared Function ToSeries(ByVal leftValue As Double, ByVal rightValue As Double) As Double
        If leftValue < Conductor.OpenConductance Then Throw New ArgumentOutOfRangeException(NameOf(leftValue), "value must be non-negative")
        If rightValue < Conductor.OpenConductance Then Throw New ArgumentOutOfRangeException(NameOf(rightValue), "value must be non-negative")
        If Conductor.IsShort(leftValue) Then
            Return rightValue
        ElseIf Conductor.IsShort(rightValue) Then
            Return leftValue
        ElseIf Conductor.IsOpen(leftValue) OrElse Conductor.IsOpen(rightValue) Then
            Return Conductor.OpenConductance
        Else
            Return Conductor.Series(leftValue, rightValue)
        End If
    End Function

    ''' <summary> Returns the equivalent conductance of a resistor and conductor in series. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="conductance"> The conductance. </param>
    ''' <param name="resistance">  The resistance. </param>
    ''' <returns> A Double. </returns>
    Public Shared Function SeriesResistor(ByVal conductance As Double, ByVal resistance As Double) As Double
        If conductance < Conductor.OpenConductance Then Throw New ArgumentOutOfRangeException(NameOf(conductance), "value must be non-negative")
        If Conductor.IsOpen(conductance) OrElse Resistor.IsOpen(resistance) Then
            Return Conductor.OpenConductance
        ElseIf Conductor.IsShort(conductance) Then
            Return If(Resistor.IsShort(resistance), Conductor.ShortConductance, 1.0 / resistance)
        ElseIf Resistor.IsShort(resistance) Then
            Return conductance
        Else
            Return ToSeries(conductance, 1 / resistance)
        End If
    End Function

    ''' <summary> Converts a conductance to a resistance. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="conductance"> The conductance. </param>
    ''' <returns> Conductance as a Double. </returns>
    Public Shared Function ToResistance(ByVal conductance As Double) As Double
        If Conductor.IsOpen(conductance) Then
            Return Resistor.OpenResistance
        ElseIf Conductor.IsShort(conductance) Then
            Return Resistor.ShortResistance
        Else
            Return 1 / conductance
        End If
    End Function

    ''' <summary> Returns the relative current through the right parallel conductance. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <exception cref="InvalidOperationException">   Thrown when the requested operation is
    '''                                                invalid. </exception>
    ''' <param name="leftValue">  The left value. </param>
    ''' <param name="rightValue"> The right value. </param>
    ''' <returns> A Double. </returns>
    Public Shared Function ToOutputCurrent(ByVal leftValue As Double, ByVal rightValue As Double) As Double
        If leftValue < Conductor.OpenConductance Then Throw New ArgumentOutOfRangeException(NameOf(leftValue), "value must be non-negative")
        If rightValue < Conductor.OpenConductance Then Throw New ArgumentOutOfRangeException(NameOf(rightValue), "value must be non-negative")
        If Conductor.IsShort(rightValue) Then
            If Conductor.IsShort(leftValue) Then
                ' two shorts cannot be used and cause an exception
                Throw New InvalidOperationException("Unable to assign output current for two parallel shorts.")
            Else
                Return 1
            End If
        ElseIf Conductor.IsShort(leftValue) Then
            Return 0
        ElseIf Conductor.IsOpen(leftValue) Then
            If Conductor.IsOpen(rightValue) Then
                ' two opens cannot be used and cause an exception
                Throw New InvalidOperationException("Unable to assign output current for two parallel opens.")
            Else
                Return 1
            End If
        ElseIf Conductor.IsOpen(rightValue) Then
            If Conductor.IsOpen(leftValue) Then
                Throw New InvalidOperationException("Unable to assign output current for two parallel opens.")
            Else
                Return 0
            End If
        Else
            Return Conductor.OutputCurrent(leftValue, rightValue)
        End If
    End Function

    ''' <summary> Returns the relative voltage across the right series conductance. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <exception cref="InvalidOperationException">   Thrown when the requested operation is
    '''                                                invalid. </exception>
    ''' <param name="leftValue">  The left value. </param>
    ''' <param name="rightValue"> The right value. </param>
    ''' <returns> The given data converted to a Double. </returns>
    Public Shared Function ToOutputVoltage(ByVal leftValue As Double, ByVal rightValue As Double) As Double
        If leftValue < Conductor.OpenConductance Then Throw New ArgumentOutOfRangeException(NameOf(leftValue), "value must be non-negative")
        If rightValue < Conductor.OpenConductance Then Throw New ArgumentOutOfRangeException(NameOf(rightValue), "value must be non-negative")
        If Conductor.IsOpen(leftValue) Then
            If Conductor.IsOpen(rightValue) Then
                ' two shorts cannot be used and cause an exception
                Throw New InvalidOperationException("Unable to assign output voltage for two opens.")
            Else
                ' if the left value is zero, all the current goes through it
                Return 0
            End If
        ElseIf Conductor.IsOpen(rightValue) Then
            ' if right value is zero, all the current goes through it.
            Return 1
        ElseIf Conductor.IsShort(leftValue) Then
            If Conductor.IsShort(rightValue) Then
                ' two shorts cannot be used and cause an exception
                Throw New InvalidOperationException("Unable to assign output voltage for two shorts.")
            Else
                Return 1
            End If
        ElseIf Conductor.IsShort(rightValue) Then
            Return 0
        Else
            Return Conductor.OutputVoltage(leftValue, rightValue)
        End If
    End Function

#End Region

End Class
