﻿''' <summary> A resistor. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/16/2017 </para>
''' </remarks>
Public NotInheritable Class Resistor

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Constructor that prevents a default instance of this class from being created.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Private Sub New()
        MyBase.New
    End Sub

#End Region

#Region " RESISTOR PAIR "

    ''' <summary> Returns the equivalent Resistance of two Resistors in parallel. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="leftValue">  The left value. </param>
    ''' <param name="rightValue"> The right value. </param>
    ''' <returns> A Double. </returns>
    Public Shared Function Parallel(ByVal leftValue As Double, ByVal rightValue As Double) As Double
        Return leftValue * rightValue / (leftValue + rightValue)
    End Function

    ''' <summary> Returns the equivalent Resistance of two Resistors in series. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="leftValue">  The left value. </param>
    ''' <param name="rightValue"> The right value. </param>
    ''' <returns> A Double. </returns>
    Public Shared Function Series(ByVal leftValue As Double, ByVal rightValue As Double) As Double
        Return leftValue + rightValue
    End Function

    ''' <summary> Returns the relative current through the right parallel Resistance. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="leftValue">  The left value. </param>
    ''' <param name="rightValue"> The right value. </param>
    ''' <returns> A Double. </returns>
    Public Shared Function OutputCurrent(ByVal leftValue As Double, ByVal rightValue As Double) As Double
        Return leftValue / (leftValue + rightValue)
    End Function

    ''' <summary> Returns the relative voltage across the right series Resistance. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="leftValue">  The left value. </param>
    ''' <param name="rightValue"> The right value. </param>
    ''' <returns> A Double. </returns>
    Public Shared Function OutputVoltage(ByVal leftValue As Double, ByVal rightValue As Double) As Double
        Return rightValue / (leftValue + rightValue)
    End Function

#End Region

#Region " PAIR OF RESISTORS: WITH VALIDATIONS "

    ''' <summary> Gets or sets the open Resistance. </summary>
    ''' <value> The open Resistance. </value>
    Public Shared Property OpenResistance As Double = Double.PositiveInfinity

    ''' <summary> Gets or sets the short Resistance. </summary>
    ''' <value> The short Resistance. </value>
    Public Shared Property ShortResistance As Double = 0

    ''' <summary> Query if 'value' is open. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> <c>true</c> if open; otherwise <c>false</c> </returns>
    Public Shared Function IsOpen(ByVal value As Double) As Boolean
        Return value = Resistor.OpenResistance
    End Function

    ''' <summary> Query if 'value' is short. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> <c>true</c> if short; otherwise <c>false</c> </returns>
    Public Shared Function IsShort(ByVal value As Double) As Boolean
        Return value = Resistor.ShortResistance
    End Function

    ''' <summary> Returns the equivalent resistance of a series resistor and conductor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="resistance">  The resistance. </param>
    ''' <param name="conductance"> The conductance. </param>
    ''' <returns> A Double. </returns>
    Public Shared Function SeriesConductor(ByVal resistance As Double, ByVal conductance As Double) As Double
        If conductance < Conductor.OpenConductance Then Throw New ArgumentOutOfRangeException(NameOf(conductance), "value must be non-negative")
        If Conductor.IsOpen(conductance) Then
            Return Resistor.OpenResistance
        ElseIf Conductor.IsShort(conductance) Then
            Return resistance
        ElseIf Resistor.IsOpen(resistance) Then
            Return Resistor.OpenResistance
        ElseIf Resistor.IsShort(resistance) Then
            Return 1 / conductance
        Else
            Return ToSeries(resistance, 1 / conductance)
        End If
    End Function

    ''' <summary> Returns the equivalent resistance of applying a shunt conductor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="resistance">  The resistance. </param>
    ''' <param name="conductance"> The conductance. </param>
    ''' <returns> A Double. </returns>
    Public Shared Function ShuntConductor(ByVal resistance As Double, ByVal conductance As Double) As Double
        If conductance < Conductor.OpenConductance Then Throw New ArgumentOutOfRangeException(NameOf(conductance), "value must be non-negative")
        If Conductor.IsOpen(conductance) Then
            Return ToParallel(resistance, Resistor.OpenResistance)
        ElseIf Conductor.IsShort(conductance) Then
            Return ToParallel(resistance, Resistor.ShortResistance)
        Else
            Return ToParallel(resistance, 1 / conductance)
        End If
    End Function

    ''' <summary> Returns the equivalent Resistance of two parallel Resistors. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="leftValue">  The left value. </param>
    ''' <param name="rightValue"> The right value. </param>
    ''' <returns> A Double. </returns>
    Public Shared Function ToParallel(ByVal leftValue As Double, ByVal rightValue As Double) As Double
        If leftValue < Resistor.ShortResistance Then Throw New ArgumentOutOfRangeException(NameOf(leftValue), "value must be non-negative")
        If rightValue < Resistor.ShortResistance Then Throw New ArgumentOutOfRangeException(NameOf(rightValue), "value must be non-negative")
        If Resistor.IsOpen(leftValue) Then
            Return rightValue
        ElseIf Resistor.IsOpen(rightValue) Then
            Return leftValue
        ElseIf Resistor.IsShort(leftValue) OrElse Resistor.IsShort(rightValue) Then
            Return Resistor.ShortResistance
        Else
            Return Resistor.Parallel(leftValue, rightValue)
        End If
    End Function

    ''' <summary> returns the equivalent series Resistance of two Resistors. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="leftValue">  The left value. </param>
    ''' <param name="rightValue"> The right value. </param>
    ''' <returns> A Double. </returns>
    Public Shared Function ToSeries(ByVal leftValue As Double, ByVal rightValue As Double) As Double
        If leftValue < Resistor.ShortResistance Then Throw New ArgumentOutOfRangeException(NameOf(leftValue), "value must be non-negative")
        If rightValue < Resistor.ShortResistance Then Throw New ArgumentOutOfRangeException(NameOf(rightValue), "value must be non-negative")
        If Resistor.IsShort(leftValue) Then
            Return rightValue
        ElseIf Resistor.IsShort(rightValue) Then
            Return leftValue
        ElseIf Resistor.IsOpen(leftValue) OrElse Resistor.IsOpen(rightValue) Then
            Return Resistor.OpenResistance
        Else
            Return Resistor.Series(leftValue, rightValue)
        End If
    End Function

    ''' <summary> Converts a resistance to a conductance. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="resistance"> The resistance. </param>
    ''' <returns> Resistance as a Double. </returns>
    Public Shared Function ToConductance(ByVal resistance As Double) As Double
        If Resistor.IsOpen(resistance) Then
            Return Conductor.OpenConductance
        ElseIf Resistor.IsShort(resistance) Then
            Return Conductor.ShortConductance
        Else
            Return 1 / resistance
        End If
    End Function

    ''' <summary> Returns the relative current through the right parallel Resistance. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <exception cref="InvalidOperationException">   Thrown when the requested operation is
    '''                                                invalid. </exception>
    ''' <param name="leftValue">  The left value. </param>
    ''' <param name="rightValue"> The right value. </param>
    ''' <returns> A Double. </returns>
    Public Shared Function ToOutputCurrent(ByVal leftValue As Double, ByVal rightValue As Double) As Double
        If leftValue < Resistor.ShortResistance Then Throw New ArgumentOutOfRangeException(NameOf(leftValue), "value must be non-negative")
        If rightValue < Resistor.ShortResistance Then Throw New ArgumentOutOfRangeException(NameOf(rightValue), "value must be non-negative")
        If Resistor.IsShort(rightValue) Then
            If Resistor.IsShort(leftValue) Then
                ' two shorts cannot be used and cause an exception
                Throw New InvalidOperationException("Unable to assign output current for two parallel shorts.")
            Else
                Return 1
            End If
        ElseIf Resistor.IsShort(leftValue) Then
            Return 0
        ElseIf Resistor.IsOpen(leftValue) Then
            If Resistor.IsOpen(rightValue) Then
                ' two opens cannot be used and cause an exception
                Throw New InvalidOperationException("Unable to assign output current for two parallel opens.")
            Else
                Return 1
            End If
        ElseIf Resistor.IsOpen(rightValue) Then
            Return 0
        Else
            Return Resistor.OutputCurrent(leftValue, rightValue)
        End If
    End Function

    ''' <summary> Returns the relative voltage across the right series Resistance. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <exception cref="InvalidOperationException">   Thrown when the requested operation is
    '''                                                invalid. </exception>
    ''' <param name="leftValue">  The left value. </param>
    ''' <param name="rightValue"> The right value. </param>
    ''' <returns> The given data converted to a Double. </returns>
    Public Shared Function ToOutputVoltage(ByVal leftValue As Double, ByVal rightValue As Double) As Double
        If leftValue < Resistor.ShortResistance Then Throw New ArgumentOutOfRangeException(NameOf(leftValue), "value must be non-negative")
        If rightValue < Resistor.ShortResistance Then Throw New ArgumentOutOfRangeException(NameOf(rightValue), "value must be non-negative")
        If Resistor.IsOpen(leftValue) Then
            If Resistor.IsOpen(rightValue) Then
                ' two opens cannot be used and cause an exception
                Throw New InvalidOperationException("Unable to assign output voltage for two opens.")
            Else
                ' if the left value is zero, all the current goes through it
                Return 0
            End If
        ElseIf Resistor.IsOpen(rightValue) Then
            ' if right value is zero, all the current goes through it.
            Return 1
        ElseIf Resistor.IsShort(leftValue) Then
            ' if left is open, we have zero
            If Resistor.IsShort(rightValue) Then
                ' two shorts cannot be used and cause an exception
                Throw New InvalidOperationException("Unable to assign output voltage for two shorts.")
            Else
                Return 1
            End If
        ElseIf Resistor.IsShort(rightValue) Then
            Return 0
        Else
            Return Resistor.OutputVoltage(leftValue, rightValue)
        End If
    End Function

#End Region

#Region " STANDARD RESISTOR TABLE "

    ''' <summary> Decade value. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="decadePosition"> The decade position. </param>
    ''' <param name="decadeCount">    Number of decades. </param>
    ''' <returns> A Double. </returns>
    Public Shared Function DecadeValue(ByVal decadePosition As Integer, ByVal decadeCount As Integer) As Double
        Return Math.Round(Math.Pow(10, decadePosition / decadeCount), CInt(Math.Floor(Math.Log10(decadeCount))))
    End Function

    ''' <summary> Converts the resistance to a decade value for the specified decade count. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="resistance">  The resistance. </param>
    ''' <param name="decadeCount"> Number of decades. </param>
    ''' <returns> The decade value for the specified decade count. </returns>
    Public Shared Function ToDecadeValue(ByVal resistance As Double, ByVal decadeCount As Integer) As Double
        Dim result As Double
        Dim i As Integer = CInt(Math.Round(decadeCount * (Math.Log10(resistance) - Math.Floor(Math.Log10(resistance)))))
        result = Resistor.DecadeValue(i, decadeCount)
        ' result = Math.Round(Math.Pow(10, i / decadeCount), 2)
        Return result
    End Function

    ''' <summary>
    ''' Converts the resistance to a standard resistance for the specified decade count.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="resistance">  The resistance. </param>
    ''' <param name="decadeCount"> Number of decades. </param>
    ''' <returns> The standard resistance for the specified decade count. </returns>
    Public Shared Function ToStandardResistance(ByVal resistance As Double, ByVal decadeCount As Integer) As Double
        Return Math.Pow(10, Math.Floor(Math.Log10(resistance))) * Resistor.ToDecadeValue(resistance, decadeCount)
    End Function

#End Region

End Class
