Imports System.ComponentModel

''' <summary> A Wheatstone bridge. </summary>
''' <remarks>
''' Without lose of generality, the Wheatstone bridge is laid out using 4 nodes and 4 edges
''' arranged in two, left and right arms. Also, the bridge is drawn as a square standing on one
''' node. The bridge voltage is applied at the top (+) and bottom (-) nodes. The bridge output is
''' measured at the left (+) and right (-) nodes. <para>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
''' Licensed under The MIT License.</para><para>
''' David, 9/14/2017 </para>
''' </remarks>
Public Class Wheatstone

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="topRight">              The top right  resistance value. </param>
    ''' <param name="bottomRight">           The bottom right  resistance value. </param>
    ''' <param name="bottomLeft">            The bottom left  resistance value. </param>
    ''' <param name="topLeft">               The top left  resistance value. </param>
    ''' <param name="relativeOffsetEpsilon"> The relative offset epsilon. </param>
    Public Sub New(ByVal topRight As Double, ByVal bottomRight As Double,
                   ByVal bottomLeft As Double, ByVal topLeft As Double, ByVal relativeOffsetEpsilon As Double)
        MyBase.New()
        Me.Initialize(topRight, bottomRight, bottomLeft, topLeft, relativeOffsetEpsilon)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="topRight">    The top right  resistance value. </param>
    ''' <param name="bottomRight"> The bottom right  resistance value. </param>
    ''' <param name="bottomLeft">  The bottom left  resistance value. </param>
    ''' <param name="topLeft">     The top left  resistance value. </param>
    Public Sub New(ByVal topRight As Double, ByVal bottomRight As Double,
                   ByVal bottomLeft As Double, ByVal topLeft As Double)
        Me.New(topRight, bottomRight, bottomLeft, topLeft, Wheatstone.DefaultOutputEpsilon)
    End Sub

    ''' <summary> Cloning Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="bridge"> The bridge. </param>
    Public Sub New(ByVal bridge As Wheatstone)
        Me.New(Wheatstone.ValidatedBridge(bridge).TopRight,
               Wheatstone.ValidatedBridge(bridge).BottomRight,
               Wheatstone.ValidatedBridge(bridge).BottomLeft,
               Wheatstone.ValidatedBridge(bridge).TopLeft,
               Wheatstone.ValidatedBridge(bridge).OutputEpsilon)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-07-04. </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="elements">              The elements. </param>
    ''' <param name="layout">                The bridge layout. </param>
    ''' <param name="relativeOffsetEpsilon"> The relative offset epsilon. </param>
    Public Sub New(ByVal elements As IEnumerable(Of Double), ByVal layout As WheatstoneLayout, ByVal relativeOffsetEpsilon As Double)
        MyBase.New()
        If layout = WheatstoneLayout.Clockwise Then
            Me.Initialize(elements(0), elements(1), elements(2), elements(3), relativeOffsetEpsilon)
        ElseIf layout = WheatstoneLayout.Counterclockwise Then
            Me.Initialize(elements(3), elements(2), elements(1), elements(0), relativeOffsetEpsilon)
        ElseIf layout = WheatstoneLayout.LeftRightTopBottom Then
            Me.Initialize(elements(1), elements(3), elements(2), elements(0), relativeOffsetEpsilon)
        Else
            Throw New ArgumentOutOfRangeException(NameOf(layout))
        End If
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="elements"> The elements. </param>
    ''' <param name="layout">   The bridge layout. </param>
    '''
    ''' ### <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are
    '''                                                    outside the required range. </exception>
    Public Sub New(ByVal elements As IEnumerable(Of Double), ByVal layout As WheatstoneLayout)
        Me.New(elements, layout, Wheatstone.DefaultOutputEpsilon)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As Double)
        Me.New(value, value, value, value)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value">          The value. </param>
    ''' <param name="relativeOffset"> The relative output offset. </param>
    Public Sub New(ByVal value As Double, ByVal relativeOffset As Double)
        Me.New(value * (1 + relativeOffset), value * (1 - relativeOffset),
               value * (1 + relativeOffset), value * (1 - relativeOffset))
    End Sub

    ''' <summary> Validated bridge. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="bridge"> The bridge. </param>
    ''' <returns> A Wheatstone. </returns>
    Public Shared Function ValidatedBridge(ByVal bridge As Wheatstone) As Wheatstone
        If bridge Is Nothing Then Throw New ArgumentNullException(NameOf(bridge))
        Return bridge
    End Function

#End Region

#Region " EQUALS "

    ''' <summary>
    ''' Determines whether the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
    '''                    <see cref="T:System.Object" />. </param>
    ''' <returns>
    ''' <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />; otherwise, <c>False</c>.
    ''' </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return Me.Equals(TryCast(obj, Wheatstone))
    End Function

    ''' <summary>
    ''' Compares two bridges. The bridges are compared using their LowerLimits and UpperLimits.
    ''' </summary>
    ''' <remarks>
    ''' The two bridges are the same if the have the same minimum and maximum values.
    ''' </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Function Equals(ByVal value As Double) As Boolean
        Return (Me.TopRight = value) AndAlso (Me.BottomRight = value) AndAlso (Me.BottomLeft = value) AndAlso (Me.TopLeft = value)
    End Function

    ''' <summary>
    ''' Compares two bridges. The bridges are compared using their LowerLimits and UpperLimits.
    ''' </summary>
    ''' <remarks>
    ''' The two bridges are the same if the have the same minimum and maximum values.
    ''' </remarks>
    ''' <param name="other"> Specifies the other <see cref="Wheatstone">Wheatstone</see>
    '''                      to compare for equality with this instance. </param>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Function Equals(ByVal other As Wheatstone) As Boolean
        Return other IsNot Nothing AndAlso (Me.TopRight = other.TopRight) AndAlso (Me.BottomRight = other.BottomRight) AndAlso
            (Me.BottomLeft = other.BottomLeft) AndAlso (Me.TopLeft = other.TopLeft)
    End Function

    ''' <summary>
    ''' Compares two bridges. The bridges are compared using their LowerLimits and UpperLimits.
    ''' </summary>
    ''' <remarks>
    ''' The two bridges are the same if the have the same minimum and maximum values.
    ''' </remarks>
    ''' <param name="other">     Specifies the other <see cref="Wheatstone">Wheatstone</see>
    '''                          to compare for equality with this instance. </param>
    ''' <param name="tolerance"> Specifies the relative tolerance for comparing the two values. The
    '''                          values are compared based on their end points. </param>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Function Equals(ByVal other As Wheatstone, ByVal tolerance As Double) As Boolean
        If other Is Nothing Then
            Return False
        ElseIf Me.Equals(other) Then
            Return True
        Else
            Return Math.Abs(other.TopRight - Me.TopRight) <= 0.5 * tolerance * (other.TopRight + Me.TopRight) AndAlso
                   Math.Abs(other.BottomRight - Me.BottomRight) <= 0.5 * tolerance * (other.BottomRight + Me.BottomRight) AndAlso
                   Math.Abs(other.BottomLeft - Me.BottomLeft) <= 0.5 * tolerance * (other.BottomLeft + Me.BottomLeft) AndAlso
                   Math.Abs(other.TopLeft - Me.TopLeft) <= 0.5 * tolerance * (other.TopLeft + Me.TopLeft)
        End If
    End Function

    ''' <summary> Implements the operator =. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Overloads Shared Function Equals(ByVal left As Wheatstone, ByVal right As Wheatstone) As Boolean
        Return (CObj(left) Is CObj(right)) OrElse (left IsNot Nothing AndAlso left.Equals(right))
    End Function

    ''' <summary>
    ''' Compares two bridges. The bridges are compared using their LowerLimits and UpperLimits.
    ''' </summary>
    ''' <remarks>
    ''' The two bridges are the same if the have the same minimum and maximum values.
    ''' </remarks>
    ''' <param name="left">      Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right">     Specifies the right hand side argument of the binary operation. </param>
    ''' <param name="tolerance"> Specifies the relative tolerance for comparing the two values. The
    '''                          values are compared based on their end points. </param>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Shared Function Equals(ByVal left As Wheatstone, ByVal right As Wheatstone, ByVal tolerance As Double) As Boolean
        Return ((left Is Nothing) AndAlso (right Is Nothing)) OrElse (left IsNot Nothing) AndAlso left.Equals(right, tolerance)
    End Function

    ''' <summary> Implements the operator =. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As Wheatstone, ByVal right As Wheatstone) As Boolean
        Return Wheatstone.Equals(left, right)
    End Operator

    ''' <summary> Implements the operator &lt;&gt;. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(ByVal left As Wheatstone, ByVal right As Wheatstone) As Boolean
        Return Not Wheatstone.Equals(left, right)
    End Operator

    ''' <summary> Creates a unique hash code. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> An <see cref="System.Int32">Int32</see> value. </returns>
    Public Overloads Overrides Function GetHashCode() As Int32
        Return Me.TopRight.GetHashCode Xor Me.BottomRight.GetHashCode Xor Me.BottomLeft.GetHashCode Xor Me.TopLeft.GetHashCode
    End Function

#End Region

#Region " BRIDGE ELEMENTS "

    ''' <summary> Gets or sets the top right resistance value. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The top right  resistance value. </value>
    Public ReadOnly Property TopRight As Double

    ''' <summary> Gets or sets the bottom right resistance value. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The bottom right  resistance value. </value>
    Public ReadOnly Property BottomRight As Double

    ''' <summary> Gets or sets the bottom left resistance value. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The bottom left  resistance value. </value>
    Public ReadOnly Property BottomLeft As Double

    ''' <summary> Gets or sets the top left resistance value. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The top left  resistance value. </value>
    Public ReadOnly Property TopLeft As Double

    ''' <summary> Select edge. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="edge"> The edge. </param>
    ''' <returns> A Double. </returns>
    Public Function SelectEdge(ByVal edge As WheatstoneEdge) As Double
        Dim value As Double = 0
        Select Case edge
            Case WheatstoneEdge.TopLeft
                value = Me.TopLeft
            Case WheatstoneEdge.TopRight
                value = Me.TopRight
            Case WheatstoneEdge.BottomLeft
                value = Me.BottomLeft
            Case WheatstoneEdge.BottomRight
                value = Me.BottomRight
        End Select
        Return value
    End Function

    ''' <summary> Invalid element. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Private Shared Function InvalidElement(ByVal value As Double) As Boolean
        Return Resistor.IsOpen(value) OrElse Resistor.IsShort(value)
    End Function

    ''' <summary> Determines if we can invalid bridge. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Private Function InvalidBridge() As Boolean
        Return Wheatstone.InvalidElement(Me.TopRight) OrElse Wheatstone.InvalidElement(Me.BottomRight) OrElse
               Wheatstone.InvalidElement(Me.BottomLeft) OrElse Wheatstone.InvalidElement(Me.TopLeft)
    End Function

    ''' <summary> Query if the bridge is valid. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> <c>true</c> if valid; otherwise <c>false</c> </returns>
    Public Overridable Function IsValid() As Boolean
        Return Not Me.InvalidBridge
    End Function

    ''' <summary> Initializes this object. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Private Sub Initialize()
        Me._Output = Me._BottomLeft / (Me._TopLeft + Me._BottomLeft) - Me._BottomRight / (Me._TopRight + Me._BottomRight)
        Me._IsOutputBalanced = Math.Abs(Me.Output) <= Me._OutputEpsilon
        Dim topRightBottomLeft As Double = Me.TopRight * Me.BottomLeft
        Dim topLeftBottomRight As Double = Me.TopLeft * Me.BottomRight
        Me._Balance = topRightBottomLeft / topLeftBottomRight
        Me._BalanceDeviation = Me._Balance - 1
        Me._IsBalanced = Math.Abs(Me._BalanceDeviation) <= Me._BalanceDeviationEpsilon
        Me._ProductImbalance = topRightBottomLeft - topLeftBottomRight
        Me._BridgeResistance = (Me.TopLeft + Me.BottomLeft) * (Me.TopRight + Me.BottomRight) / (Me.TopLeft + Me.BottomLeft + Me.TopRight + Me.BottomRight)
    End Sub

    ''' <summary> Initializes this object. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="topRight">              The top right  resistance value. </param>
    ''' <param name="bottomRight">           The bottom right  resistance value. </param>
    ''' <param name="bottomLeft">            The bottom left  resistance value. </param>
    ''' <param name="topLeft">               The top left  resistance value. </param>
    ''' <param name="relativeOffsetEpsilon"> The relative offset epsilon. </param>
    Private Sub Initialize(ByVal topRight As Double, ByVal bottomRight As Double,
                           ByVal bottomLeft As Double, ByVal topLeft As Double, ByVal relativeOffsetEpsilon As Double)
        Me._TopRight = topRight
        Me._BottomRight = bottomRight
        Me._BottomLeft = bottomLeft
        Me._TopLeft = topLeft
        Me._OutputEpsilon = relativeOffsetEpsilon
        Me._BalanceDeviationEpsilon = 4 * relativeOffsetEpsilon
        Me.Initialize()
    End Sub

    ''' <summary> Initializes this object. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="bridge"> The bridge. </param>
    Public Sub Initialize(ByVal bridge As Wheatstone)
        If bridge Is Nothing Then Throw New ArgumentNullException(NameOf(bridge))
        Me.Initialize(bridge.TopRight, bridge.BottomRight, bridge.BottomLeft, bridge.TopLeft, bridge.OutputEpsilon)
    End Sub

#End Region

#Region " BRIDGE OUTPUT "

    ''' <summary> Gets the bridge output for the specified bridge voltage. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="bridgeVoltage"> The bridge voltage. </param>
    ''' <returns> A Double. </returns>
    Public Function Output(ByVal bridgeVoltage As Double) As Double
        Return bridgeVoltage * Me.Output
    End Function

    ''' <summary> Gets the bridge output for the specified bridge voltage. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="voltageSource"> The voltage source. </param>
    ''' <returns> A Double. </returns>
    Public Function Output(ByVal voltageSource As VoltageSource) As Double
        If voltageSource Is Nothing Then Throw New ArgumentNullException(NameOf(voltageSource))
        Return Me.Output(Me.BridgeVoltage(voltageSource))
    End Function

    ''' <summary> Gets the bridge output for the specified bridge voltage. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="currentSource"> The current source. </param>
    ''' <returns> A Double. </returns>
    Public Function Output(ByVal currentSource As CurrentSource) As Double
        If currentSource Is Nothing Then Throw New ArgumentNullException(NameOf(currentSource))
        Return Me.Output(Me.BridgeVoltage(currentSource))
    End Function
    ''' <summary> The output. </summary>
    Private _Output As Double

    ''' <summary> Gets the bridge output for a unity input. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> A Double. </returns>
    Public Function Output() As Double
        Return Me._Output
    End Function

    ''' <summary> Gets or sets the default epsilon for assessing the bridge output. </summary>
    ''' <value> The default relative offset epsilon. </value>
    Public Shared Property DefaultOutputEpsilon As Double = 0.0001
    ''' <summary> The output epsilon. </summary>
    Private _OutputEpsilon As Double

    ''' <summary> Gets or sets the unity output epsilon. </summary>
    ''' <value> The unity output  epsilon. </value>
    Public Property OutputEpsilon As Double
        Get
            Return Me._OutputEpsilon
        End Get
        Set(value As Double)
            If value <> Me.OutputEpsilon Then
                Me._OutputEpsilon = value
                Me._IsOutputBalanced = Math.Abs(Me.Output) <= Me.OutputEpsilon
            End If
        End Set
    End Property

    ''' <summary>
    ''' Returns true if the absolute bridge <see cref="Output"/> is lower than the
    ''' <see cref="OutputEpsilon"/>.
    ''' </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> <c>true</c> if balanced; otherwise <c>false</c> </value>
    Public ReadOnly Property IsOutputBalanced() As Boolean

#End Region

#Region " BRIDGE VOLTAGE "

    ''' <summary> Bridge voltage. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="voltageSource"> The voltage source. </param>
    ''' <returns> A Double. </returns>
    Public Function BridgeVoltage(ByVal voltageSource As VoltageSource) As Double
        If voltageSource Is Nothing Then Throw New ArgumentNullException(NameOf(voltageSource))
        Return voltageSource.LoadVoltage(Me.BridgeResistance)
    End Function

    ''' <summary> Bridge voltage. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="currentSource"> The current source. </param>
    ''' <returns> A Double. </returns>
    Public Function BridgeVoltage(ByVal currentSource As CurrentSource) As Double
        If currentSource Is Nothing Then Throw New ArgumentNullException(NameOf(currentSource))
        Return currentSource.LoadVoltage(Me.BridgeResistance)
    End Function

    ''' <summary> Gets the bridge resistance as see from voltage or current source. </summary>
    ''' <value> The bridge resistance. </value>
    Public ReadOnly Property BridgeResistance() As Double

#End Region

#Region " BRIDGE IMBALANCE "

    ''' <summary> Gets the bridge product imbalance. </summary>
    ''' <remarks>
    ''' This is the difference between the product of the increasing values (top right and bottom
    ''' left) and decreasing value (top left and bottom right).
    ''' </remarks>
    ''' <value> A Double. </value>
    Public ReadOnly Property ProductImbalance() As Double

    ''' <summary> Gets a measure of the bridge balance. </summary>
    ''' <remarks>
    ''' The ratio of the Top-Right Left-Bottom diagonal product over the Top-Left Bottom-Right
    ''' product. The <see cref="Balance"/> is one if the bridge is balance. When greater than one,
    ''' the bridge is said to be positively imbalanced.
    ''' </remarks>
    ''' <value> The relative imbalance. </value>
    Public ReadOnly Property Balance() As Double

    ''' <summary> Gets the balance deviation. </summary>
    ''' <remarks>
    ''' This is a measure of the bridge deviation from balance. When zero, the bridge is said to be
    ''' balanced. When positive, the bridge is positively imbalanced, which means that its output is
    ''' also positive.
    ''' </remarks>
    ''' <value> The relative deviation. </value>
    Public ReadOnly Property BalanceDeviation() As Double

    ''' <summary> Gets the absolute balance deviation. </summary>
    ''' <value> The absolute balance deviation. </value>
    Public ReadOnly Property AbsoluteBalanceDeviation As Double
        Get
            Return Math.Abs(Me.BalanceDeviation)
        End Get
    End Property
    ''' <summary> The balance deviation epsilon. </summary>
    Private _BalanceDeviationEpsilon As Double

    ''' <summary> Gets or sets the balance deviation epsilon. </summary>
    ''' <remarks>
    ''' the <see cref="BalanceDeviationEpsilon"/> is four times the <see cref="OutputEpsilon"/>
    ''' </remarks>
    ''' <value> The balance deviation epsilon. </value>
    Public Property BalanceDeviationEpsilon As Double
        Get
            Return Me._BalanceDeviationEpsilon
        End Get
        Set(value As Double)
            If value <> Me.BalanceDeviationEpsilon OrElse Me.IsBalanced <> Me.CheckBalanced(value) Then
                Me._BalanceDeviationEpsilon = value
                Me._IsBalanced = Me.CheckBalanced(value)
            End If
        End Set
    End Property

    ''' <summary> Check balanced. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="epsilon"> The epsilon. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Private Function CheckBalanced(ByVal epsilon As Double) As Boolean
        Return Math.Abs(Me.BalanceDeviation) <= epsilon
    End Function

    ''' <summary>
    ''' Returns true if the absolute bridge <see cref="BalanceDeviation"/> is lower than the
    ''' <see cref="BalanceDeviationEpsilon"/>.
    ''' </summary>
    ''' <value> <c>true</c> if balanced; otherwise <c>false</c> </value>
    Public ReadOnly Property IsBalanced() As Boolean

#End Region

#Region " TO COMMA SEPARATED STRING "

    ''' <summary> Returns a comma-separated values string. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="values"> The values. </param>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <returns> The given data converted to a String. </returns>
    Private Shared Function ToCommaSeparatedString(ByVal values As IEnumerable(Of Double), ByVal format As String) As String
        Dim builder As New System.Text.StringBuilder()
        For i As Integer = 0 To values.Count - 1
            builder.Append($"{values(i).ToString(format)}")
            If i < (values.Count - 1) Then builder.Append(",")
        Next
        Return builder.ToString
    End Function

    ''' <summary> Returns a comma-separated values string. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <returns> The given data converted to a String. </returns>
    Public Overridable Function ToCommaSeparatedString(ByVal format As String) As String
        Return Wheatstone.ToCommaSeparatedString(New Double() {Me.TopRight, Me.BottomRight, Me.BottomLeft, Me.TopLeft}, format)
    End Function

    ''' <summary> Returns a comma-separated values string. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> The given data converted to a String. </returns>
    Public Overridable Function ToCommaSeparatedString() As String
        Return Me.ToCommaSeparatedString("G6")
    End Function

    ''' <summary> Returns a comma-separated header string. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="values"> The values. </param>
    ''' <returns> The given data converted to a String. </returns>
    Private Shared Function ToCommaSeparatedString(ByVal values As IEnumerable(Of WheatstoneEdge)) As String
        Dim builder As New System.Text.StringBuilder()
        For i As Integer = 0 To values.Count - 1
            builder.Append($"{values(i)}")
            If i < (values.Count - 1) Then builder.Append(",")
        Next
        Return builder.ToString
    End Function

    ''' <summary> Comma separated header. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> A String. </returns>
    Public Shared Function ToCommaSeparatedHeader() As String
        Return Wheatstone.ToCommaSeparatedString(New WheatstoneEdge() {WheatstoneEdge.TopRight, WheatstoneEdge.BottomRight,
                                                 WheatstoneEdge.BottomLeft, WheatstoneEdge.TopLeft})
    End Function

#End Region

#Region " TO STRING "

    ''' <summary> Gets a string representation of the edges. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="values"> The values. </param>
    ''' <returns> A String that represents this object. </returns>
    Public Overloads Shared Function ToString(ByVal values As IEnumerable(Of WheatstoneEdge)) As String
        Dim builder As New System.Text.StringBuilder("[")
        builder.Append(Wheatstone.ToCommaSeparatedString(values))
        builder.Append("]")
        Return builder.ToString
    End Function

    ''' <summary> Convert this object into a string representation. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="values"> The values. </param>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <returns> A String that represents this object. </returns>
    Public Overloads Shared Function ToString(ByVal values As IEnumerable(Of Double), ByVal format As String) As String
        Dim builder As New System.Text.StringBuilder("[")
        builder.Append(Wheatstone.ToCommaSeparatedString(values, format))
        builder.Append("]")
        Return builder.ToString
    End Function

    ''' <summary> Gets a string representation of the edges. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <returns> A String that represents this object. </returns>
    Public Overloads Function ToString(ByVal format As String) As String
        Return Wheatstone.ToString(New Double() {Me.TopRight, Me.BottomRight, Me.BottomLeft, Me.TopLeft}, format)
    End Function

    ''' <summary> Convert this object into a string representation. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> A String that represents this object. </returns>
    Public Overrides Function ToString() As String
        Return Me.ToString("G6")
    End Function

#End Region

End Class

''' <summary> A collection of Wheatstone bridges. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/25/2017 </para>
''' </remarks>
Public Class WheatstoneCollection
    Inherits ObjectModel.Collection(Of Wheatstone)

    ''' <summary> Total Imbalance squared. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> A Double. </returns>
    Public Function TotalImbalanceSquared() As Double
        Dim result As Double = 0
        For Each bridge As Wheatstone In Me
            Dim x As Double = bridge.ProductImbalance
            result += x * x
        Next
        Return result
    End Function

End Class

''' <summary> Values that represent Wheatstone bridge layout. </summary>
''' <remarks> David, 2020-09-23. </remarks>
Public Enum WheatstoneLayout
    ''' <summary> An enum constant representing the clockwise option starting at the top right position. </summary>
    <Description("Clockwise")> Clockwise
    ''' <summary> An enum constant representing the counterclockwise option starting at the top left position. </summary>
    <Description("Counter Clockwise")> Counterclockwise
    ''' <summary> An enum constant representing the left right top bottom option starting at the top left position. </summary>
    <Description("Left Right Top Bottom")> LeftRightTopBottom
End Enum

''' <summary> Values that represent Wheatstone edges. </summary>
''' <remarks> David, 2020-09-23. </remarks>
Public Enum WheatstoneEdge
    ''' <summary> An enum constant representing the top right option. </summary>
    <Description("Top Right")> TopRight
    ''' <summary> An enum constant representing the bottom right option. </summary>
    <Description("Bottom Right")> BottomRight
    ''' <summary> An enum constant representing the bottom left option. </summary>
    <Description("Bottom Left")> BottomLeft
    ''' <summary> An enum constant representing the top left option. </summary>
    <Description("Top Left")> TopLeft
End Enum


