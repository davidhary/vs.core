''' <summary> A Thread safe Observable Queue. </summary>
''' <remarks> David, 12/12/2016. </remarks>
<DebuggerDisplay("Count = {Count}")>
Public Class ThreadSafeObservableQueue(Of T)
    Inherits ThreadSafeObservableCollection(Of T)

    ''' <summary> Adds an object onto the end of this queue. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="item"> The item. </param>
    Public Sub Enqueue(ByVal item As T)
        Me.Add(item)
    End Sub

    ''' <summary> Removes the head object from this queue. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <returns> The head object from this queue. </returns>
    Public Function Dequeue() As T
        Me.ItemsLocker.EnterUpgradeableReadLock()
        Try
            Dim item As T = Me.BaseItem(0)
            Me.ItemsLocker.EnterWriteLock()
            Me.BaseRemoveItem(0)
            Return item
        Finally
            Me.ItemsLocker.ExitUpgradeableReadLock()
        End Try
    End Function

    ''' <summary> Returns the top-of-stack object without removing it. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <returns> The current top-of-stack object. </returns>
    Public Function Peek() As T
        Return Me.Item(0)
    End Function


End Class
