Imports System.Runtime.InteropServices

''' <summary>
''' An splay tree is a self-balancing binary search tree with the additional property that
''' recently accessed elements are quick to access again. It performs basic operations such as
''' insertion, look-up and removal in O(log n) amortized time.
''' </summary>
''' <remarks> David, 9/16/2019. </remarks>
Public Class SortedSplayTreeDictionary(Of TKey, TValue)
    Implements IDictionary(Of TKey, TValue)

    ''' <summary> The root. </summary>
    Private _Root As NodeThis
    ''' <summary> The comparer. </summary>
    Private ReadOnly _Comparer As IComparer(Of TKey)

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="comparer"> The comparer. </param>
    Public Sub New(ByVal comparer As IComparer(Of TKey))
        Me._Comparer = If(comparer, System.Collections.Generic.Comparer(Of TKey).[Default])
    End Sub

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Public Sub New()
        Me.New(Nothing)
    End Sub

    ''' <summary> Gets the number of. </summary>
    ''' <value> The count. </value>
    Public ReadOnly Property Count As Integer Implements IDictionary(Of TKey, TValue).Count
        Get
            Return Me.GetCountThis(Me._Root)
        End Get
    End Property

    ''' <summary> Gets or sets the element with the specified key. </summary>
    ''' <exception cref="KeyNotFoundException"> Thrown when a Key Not Found error condition occurs. </exception>
    ''' <value> The element with the specified key. </value>
    Default Public Property Item(ByVal key As TKey) As TValue Implements IDictionary(Of TKey, TValue).Item
        Get
            Dim value As TValue
            If Me.TryGetValue(key, value) Then Return value
            Throw New KeyNotFoundException()
        End Get
        Set(ByVal value As TValue)
            Dim n As NodeThis = Me.SplayThis(Me._Root, key)

            If n IsNot Nothing Then
                n.Value = value
            Else
                Me.Add(key, value)
            End If
        End Set
    End Property

    ''' <summary> Gets the is read only. </summary>
    ''' <value> The is read only. </value>
    Private ReadOnly Property IsReadOnly As Boolean Implements IDictionary(Of TKey, TValue).IsReadOnly
        Get
            Return False
        End Get
    End Property

    ''' <summary> Copies to. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="array">      The array. </param>
    ''' <param name="arrayIndex"> Zero-based index of the array. </param>
    Public Sub CopyTo(ByVal array As KeyValuePair(Of TKey, TValue)(), ByVal arrayIndex As Integer) Implements IDictionary(Of TKey, TValue).CopyTo
        CollectionExtensions.CopyTo(Me, array, arrayIndex)
        ' DictionaryUtility.CopyTo(Me, array, index)
    End Sub

    ''' <summary>
    ''' Determines whether the <see cref="T:System.Collections.Generic.IDictionary`2" /> contains an
    ''' element with the specified key.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="key"> The key to locate in the
    '''                    <see cref="T:System.Collections.Generic.IDictionary`2" />. </param>
    ''' <returns>
    ''' <see langword="true" /> if the <see cref="T:System.Collections.Generic.IDictionary`2" />
    ''' contains an element with the key; otherwise, <see langword="false" />.
    ''' </returns>
    Public Function ContainsKey(ByVal key As TKey) As Boolean Implements IDictionary(Of TKey, TValue).ContainsKey
        Me._Root = Me.SplayThis(Me._Root, key)
        Return 0 = Me._Comparer.Compare(Me._Root.Key, key)
    End Function

    ''' <summary> Query if this object contains the given item. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="item"> The item to remove. </param>
    ''' <returns> <c>true</c> if the object is in this collection; otherwise <c>false</c> </returns>
    Public Function Contains(ByVal item As KeyValuePair(Of TKey, TValue)) As Boolean Implements IDictionary(Of TKey, TValue).Contains
        Dim value As TValue
        Return Me.TryGetValue(item.Key, value) AndAlso Equals(item.Value, value)
    End Function

    ''' <summary> Gets the value associated with the specified key. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="key">   The key whose value to get. </param>
    ''' <param name="value"> When this method returns, the value associated with the
    ''' specified key, if the key is found; otherwise, the default value for 
    ''' the type of the <paramref name="value" /> parameter. This parameter 
    ''' is passed uninitialized. 
    ''' </param>
    ''' <returns>
    ''' <see langword="true" /> if the object that implements
    ''' <see cref="T:System.Collections.Generic.IDictionary`2" /> contains an element with the
    ''' specified key; otherwise, <see langword="false" />.
    ''' </returns>
    Public Function TryGetValue(ByVal key As TKey, <Out> ByRef value As TValue) As Boolean Implements IDictionary(Of TKey, TValue).TryGetValue
        If 0 = Me._Comparer.Compare(Me._Root.Key, key) Then
            value = Me._Root.Value
            Return True
        End If
        Me._Root = Me.SplayThis(Me._Root, key)
        If 0 = Me._Comparer.Compare(Me._Root.Key, key) Then
            value = Me._Root.Value
            Return True
        End If
        value = Nothing
        Return False
    End Function

    ''' <summary>
    ''' Adds an element with the provided key and value to the
    ''' <see cref="T:System.Collections.Generic.IDictionary`2" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="key">   The object to use as the key of the element to add. </param>
    ''' <param name="value"> The object to use as the value of the element to add. </param>
    Public Sub Add(ByVal key As TKey, ByVal value As TValue) Implements IDictionary(Of TKey, TValue).Add
        Me._Root = Me.AddThis(Me._Root, key, value)
    End Sub

    ''' <summary> Adds item. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="item"> The item to remove. </param>
    Public Sub Add(ByVal item As KeyValuePair(Of TKey, TValue)) Implements ICollection(Of KeyValuePair(Of TKey, TValue)).Add
        Me.Add(item.Key, item.Value)
    End Sub

    ''' <summary>
    ''' Removes the element with the specified key from the
    ''' <see cref="T:System.Collections.Generic.IDictionary`2" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="key"> The key of the element to remove. </param>
    ''' <returns>
    ''' <see langword="true" /> if the element is successfully removed; otherwise,
    ''' <see langword="false" />.  This method also returns <see langword="false" /> if
    ''' <paramref name="key" /> was not found in the original
    ''' <see cref="T:System.Collections.Generic.IDictionary`2" />.
    ''' </returns>
    Public Function Remove(ByVal key As TKey) As Boolean Implements IDictionary(Of TKey, TValue).Remove
        Dim temp As NodeThis = Nothing
        If Me.TryRemoveThis(Me._Root, key, temp) Then
            Me._Root = temp
            Return True
        End If

        Return False
    End Function

    ''' <summary> Removes the given item. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="item"> The item to remove. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function Remove(ByVal item As KeyValuePair(Of TKey, TValue)) As Boolean Implements IDictionary(Of TKey, TValue).Remove
        Dim value As TValue
        If Me.TryGetValue(item.Key, value) AndAlso Equals(item.Value, value) Then Return Me.Remove(item.Key)
        Return False
    End Function

    ''' <summary> Clears this object to its blank/initial state. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Public Sub Clear() Implements IDictionary(Of TKey, TValue).Clear
        Me._Root = Nothing
    End Sub

    ''' <summary> Creates a node. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="key">   The key. </param>
    ''' <param name="value"> The value. </param>
    ''' <returns> The new node. </returns>
    Private Shared Function CreateNodeThis(ByVal key As TKey, ByVal value As TValue) As NodeThis
        Dim result As New NodeThis With {
                .Key = key,
                .Value = value,
                .Right = Nothing,
                .Left = Nothing
            }
        Return result
    End Function

    ''' <summary> Rors the given x coordinate. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="x"> A _Node to process. </param>
    ''' <returns> A _Node. </returns>
    Private Shared Function RorThis(ByVal x As NodeThis) As NodeThis
        Dim y As NodeThis = x.Left
        x.Left = y.Right
        y.Right = x
        Return y
    End Function

    ''' <summary> Rols the given x coordinate. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="x"> A _Node to process. </param>
    ''' <returns> A _Node. </returns>
    Private Shared Function RolThis(ByVal x As NodeThis) As NodeThis
        Dim y As NodeThis = x.Right
        x.Right = y.Left
        y.Left = x
        Return y
    End Function

    ''' <summary> Splays. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="root"> The root. </param>
    ''' <param name="key">  The key. </param>
    ''' <returns> A _Node. </returns>
    Private Function SplayThis(ByVal root As NodeThis, ByVal key As TKey) As NodeThis
        Dim c As Integer = If(root Is Nothing, 0, Me._Comparer.Compare(root.Key, key))
        If c = 0 Then
            Return root
        ElseIf 0 < c Then
            If root.Left Is Nothing Then Return root
            c = Me._Comparer.Compare(root.Left.Key, key)

            If 0 < c Then
                root.Left.Left = Me.SplayThis(root.Left.Left, key)
                root = RorThis(root)
            ElseIf 0 > c Then
                root.Left.Right = Me.SplayThis(root.Left.Right, key)
                If root.Left.Right IsNot Nothing Then root.Left = RolThis(root.Left)
            End If

            Return If((root.Left Is Nothing), root, RorThis(root))
        ElseIf 0 > c Then
            If root.Right Is Nothing Then Return root
            c = Me._Comparer.Compare(root.Right.Key, key)

            If 0 < c Then
                root.Right.Left = Me.SplayThis(root.Right.Left, key)
                If root.Right.Left IsNot Nothing Then root.Right = RorThis(root.Right)
            ElseIf 0 > c Then
                root.Right.Right = Me.SplayThis(root.Right.Right, key)
                root = RolThis(root)
            End If
            Return If((root.Right Is Nothing), root, RolThis(root))
        Else
            Return root
        End If
    End Function

    ''' <summary> Adds root. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="root">  The root. </param>
    ''' <param name="key">   The key. </param>
    ''' <param name="value"> The value. </param>
    ''' <returns> A _Node. </returns>
    Private Function AddThis(ByVal root As NodeThis, ByVal key As TKey, ByVal value As TValue) As NodeThis
        If root Is Nothing Then Return CreateNodeThis(key, value)
        root = Me.SplayThis(root, key)
        Dim c As Integer = Me._Comparer.Compare(root.Key, key)
        If 0 = c Then Throw New ArgumentException("An item with the specified key is already present in the dictionary.", NameOf(key))
        Dim newnode As NodeThis = CreateNodeThis(key, value)

        If 0 < c Then
            newnode.Right = root
            newnode.Left = root.Left
            root.Left = Nothing
        Else
            newnode.Left = root
            newnode.Right = root.Right
            root.Right = Nothing
        End If

        Return newnode
    End Function

    ''' <summary> Try remove. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="root">   The root. </param>
    ''' <param name="key">    The key. </param>
    ''' <param name="result"> The out by reference result. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Private Function TryRemoveThis(ByVal root As NodeThis, ByVal key As TKey, <Out> ByRef result As NodeThis) As Boolean
        result = Nothing
        Dim temp As NodeThis
        If root Is Nothing Then Return False
        root = Me.SplayThis(root, key)

        If 0 <> Me._Comparer.Compare(key, root.Key) Then
            result = root
            Return False
        End If

        If root.Left Is Nothing Then
            root = root.Right
        Else
            temp = root
            root = Me.SplayThis(root.Left, key)
            root.Right = temp.Right
        End If

        result = root
        Return True
    End Function

    ''' <summary>
    ''' Gets an <see cref="T:System.Collections.Generic.ICollection`1" /> containing the keys of the
    ''' <see cref="T:System.Collections.Generic.IDictionary`2" />.
    ''' </summary>
    ''' <value>
    ''' An <see cref="T:System.Collections.Generic.ICollection`1" /> containing the keys of the
    ''' object that implements <see cref="T:System.Collections.Generic.IDictionary`2" />.
    ''' </value>
    Public ReadOnly Property Keys As ICollection(Of TKey) Implements IDictionary(Of TKey, TValue).Keys
        Get
            Return CollectionExtensions.CreateKeys(Me)
            ' Return DictionaryUtility.CreateKeys(Me)
        End Get
    End Property

    ''' <summary>
    ''' Gets an <see cref="T:System.Collections.Generic.ICollection`1" /> containing the values in
    ''' the <see cref="T:System.Collections.Generic.IDictionary`2" />.
    ''' </summary>
    ''' <value>
    ''' An <see cref="T:System.Collections.Generic.ICollection`1" /> containing the values in the
    ''' object that implements <see cref="T:System.Collections.Generic.IDictionary`2" />.
    ''' </value>
    Public ReadOnly Property Values As ICollection(Of TValue) Implements IDictionary(Of TKey, TValue).Values
        Get
            Return CollectionExtensions.CreateValues(Me)
            ' Return DictionaryUtility.CreateValues(Me)
        End Get
    End Property

    ''' <summary> Gets the enumerator. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <returns> The enumerator. </returns>
    Public Iterator Function GetEnumerator() As IEnumerator(Of KeyValuePair(Of TKey, TValue)) Implements IDictionary(Of TKey, TValue).GetEnumerator
        For Each Item As KeyValuePair(Of TKey, TValue) In Me.EnumNodesThis(Me._Root)
            Yield Item
        Next
    End Function

    ''' <summary> Gets the enumerator. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <returns> The enumerator. </returns>
    Private Function _GetEnumerator() As System.Collections.IEnumerator Implements IDictionary(Of TKey, TValue).GetEnumerator
        Return Me.GetEnumerator()
    End Function

    ''' <summary> Enumerates enum nodes in this collection. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="root"> The root. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process enum nodes in this collection.
    ''' </returns>
    Private Iterator Function EnumNodesThis(ByVal root As NodeThis) As IEnumerable(Of KeyValuePair(Of TKey, TValue))
        If root IsNot Nothing Then
            For Each Item As KeyValuePair(Of TKey, TValue) In Me.EnumNodesThis(root.Left)
                Yield Item
            Next
            For Each Item As KeyValuePair(Of TKey, TValue) In Me.EnumNodesThis(root.Right)
                Yield Item
            Next
            Yield New KeyValuePair(Of TKey, TValue)(root.Key, root.Value)
        End If
    End Function

    ''' <summary> Gets a count. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="root"> The root. </param>
    ''' <returns> The count. </returns>
    Private Function GetCountThis(ByVal root As NodeThis) As Integer
        If root Is Nothing Then Return 0
        Dim result As Integer = 1
        result += Me.GetCountThis(root.Left)
        result += Me.GetCountThis(root.Right)
        Return result
    End Function

    ''' <summary> A node. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Private NotInheritable Class NodeThis

        ''' <summary> Gets or sets the key. </summary>
        ''' <value> The key. </value>
        Public Property Key As TKey

        ''' <summary> Gets or sets the value. </summary>
        ''' <value> The value. </value>
        Public Property Value As TValue

        ''' <summary> Gets or sets the left. </summary>
        ''' <value> The left. </value>
        Public Property Left As NodeThis

        ''' <summary> Gets or sets the right. </summary>
        ''' <value> The right. </value>
        Public Property Right As NodeThis
    End Class

End Class
