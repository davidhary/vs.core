Imports System.Collections.Specialized

''' <summary> Implements a synchronized observable keyed collection. </summary>
''' <remarks>
''' David, 5/2/2017.  <para>
''' (c) 2011 MULJADI BUDIMAN. All rights reserved. </para><para>
''' http://geekswithblogs.net/NewThingsILearned/archive/2010/01/12/make-keyedcollectionlttkey-titemgt-to-work-properly-with-wpf-data-binding.aspx
''' </para>
''' </remarks>
Public MustInherit Class SynchronizedObservableKeyedCollection(Of TKey, TItem)
    Inherits System.Collections.Generic.SynchronizedKeyedCollection(Of TKey, TItem)
    Implements INotifyCollectionChanged

#Region " COLLECTION IMPLEMENTATION "

    ''' <summary> Replaces the item at the specified index with the specified item. </summary>
    ''' <remarks> Overrides and implements collection change notification. </remarks>
    ''' <param name="index"> The zero-based index of the item to be replaced. </param>
    ''' <param name="item">  The new item. </param>
    Protected Overrides Sub SetItem(ByVal index As Integer, ByVal item As TItem)
        Dim oldItem As TItem = Me.Items(index)
        MyBase.SetItem(index, item)
        Me.OnCollectionChanged(New NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, item, oldItem, index))
    End Sub

    ''' <summary>
    ''' Inserts an element into the <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" />
    ''' at the specified index.
    ''' </summary>
    ''' <remarks> Overrides and implements collection change notification. </remarks>
    ''' <param name="index"> The zero-based index at which <paramref name="item" /> should be
    '''                      inserted. </param>
    ''' <param name="item">  The object to insert. </param>
    Protected Overrides Sub InsertItem(ByVal index As Integer, ByVal item As TItem)
        MyBase.InsertItem(index, item)
        Me.OnCollectionChanged(New NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item, index))
    End Sub

    ''' <summary>
    ''' Removes all elements from the
    ''' <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" />.
    ''' </summary>
    ''' <remarks> Overrides and implements collection change notification. </remarks>
    Protected Overrides Sub ClearItems()
        MyBase.ClearItems()
        Me.OnCollectionChanged(New NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset))
    End Sub

    ''' <summary>
    ''' Removes the element at the specified index of the
    ''' <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" />.
    ''' </summary>
    ''' <remarks> Overrides and implements collection change notification. </remarks>
    ''' <param name="index"> The index of the element to remove. </param>
    Protected Overrides Sub RemoveItem(ByVal index As Integer)
        Dim item As TItem = Me.Items(index)
        MyBase.RemoveItem(index)
        Me.OnCollectionChanged(New NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item))
    End Sub

    ''' <summary> Adds a range. </summary>
    ''' <remarks> Overrides and implements a deferred collection change notification. </remarks>
    ''' <param name="items"> An IEnumerable(OfTItem) of items to append to this. </param>
    Public Sub AddRange(ByVal items As IList(Of TItem))
        If items?.Any Then
            Try
                Me.DeferNotifyCollectionChanged = True
                For Each Item As TItem In items
                    Me.Add(Item)
                Next Item
            Catch
                Throw
            Finally
                Me.DeferNotifyCollectionChanged = False
            End Try
            Me.OnCollectionChanged(New NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset))
        End If
    End Sub

#End Region

#Region " I NOTIFY COLLECTION CHANGE IMPLEMENTATION "

    ''' <summary> Event queue for all listeners interested in CollectionChanged events. </summary>
    Public Event CollectionChanged As NotifyCollectionChangedEventHandler Implements INotifyCollectionChanged.CollectionChanged

    ''' <summary> Gets or sets the defer notify collection changed. </summary>
    ''' <value> The defer notify collection changed. </value>
    Protected Property DeferNotifyCollectionChanged As Boolean

    ''' <summary> Raises the notify collection changed event. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnCollectionChanged(ByVal e As NotifyCollectionChangedEventArgs)
        If Not Me._DeferNotifyCollectionChanged Then
            Dim evt As NotifyCollectionChangedEventHandler = Me.CollectionChangedEvent
            evt?.Invoke(Me, e)
        End If
    End Sub

#End Region

End Class


