''' <summary> A value collection. </summary>
''' <remarks>
''' David, 2020-09-12.  <para>
''' (c) 2019 Honey the Code Witch, Inc. All rights reserved. </para><para>
''' https://www.codeproject.com/Tips/5239264/Bee-A-Suite-of-Self-Balancing-Binary-Tree-Based-Di.
''' </para>
''' </remarks>
Public Class ValuesCollection(Of TKey, TValue)
    Implements ICollection(Of TValue)

    ''' <summary> The parent dictionary. </summary>
    Private ReadOnly _Parent As IDictionary(Of TKey, TValue)

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="parent"> The parent dictionary. </param>
    Public Sub New(ByVal parent As IDictionary(Of TKey, TValue))
        MyBase.New
        Me._Parent = parent
    End Sub

    ''' <summary>
    ''' Determines whether the <see cref="T:System.Collections.Generic.ICollection`1" /> contains a
    ''' specific value.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="item"> The object to locate in the
    '''                     <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
    ''' <returns>
    ''' <see langword="true" /> if <paramref name="item" /> is found in the
    ''' <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise,
    ''' <see langword="false" />.
    ''' </returns>
    Public Function Contains(ByVal item As TValue) As Boolean Implements ICollection(Of TValue).Contains
        For Each kvp As KeyValuePair(Of TKey, TValue) In Me._Parent
            If Equals(kvp.Value, item) Then Return True
        Next
        Return False
    End Function

    ''' <summary>
    ''' Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1" />
    ''' is read-only.
    ''' </summary>
    ''' <value>
    ''' <see langword="true" /> if the <see cref="T:System.Collections.Generic.ICollection`1" /> is
    ''' read-only; otherwise, <see langword="false" />.
    ''' </value>
    Public ReadOnly Property IsReadOnly As Boolean Implements ICollection(Of TValue).IsReadOnly
        Get
            Return True
        End Get
    End Property

    ''' <summary> Message describing the read only. </summary>
    Private Const _ReadOnlyMsg As String = "The collection is read-only."

    ''' <summary>
    ''' Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="item"> The object to add to the
    '''                     <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
    Public Sub Add(ByVal item As TValue) Implements ICollection(Of TValue).Add
        Throw New InvalidOperationException(_ReadOnlyMsg)
    End Sub

    ''' <summary>
    ''' Removes the first occurrence of a specific object from the
    ''' <see cref="T:System.Collections.Generic.ICollection`1" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="item"> The object to remove from the
    '''                     <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
    ''' <returns>
    ''' <see langword="true" /> if <paramref name="item" /> was successfully removed from the
    ''' <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise,
    ''' <see langword="false" />. This method also returns <see langword="false" /> if
    ''' <paramref name="item" /> is not found in the original
    ''' <see cref="T:System.Collections.Generic.ICollection`1" />.
    ''' </returns>
    Public Function Remove(ByVal item As TValue) As Boolean Implements ICollection(Of TValue).Remove
        Throw New InvalidOperationException(_ReadOnlyMsg)
    End Function

    ''' <summary>
    ''' Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    Public Sub Clear() Implements ICollection(Of TValue).Clear
        Throw New InvalidOperationException(_ReadOnlyMsg)
    End Sub

    ''' <summary>
    ''' Gets the number of elements contained in the
    ''' <see cref="T:System.Collections.Generic.ICollection`1" />.
    ''' </summary>
    ''' <value>
    ''' The number of elements contained in the
    ''' <see cref="T:System.Collections.Generic.ICollection`1" />.
    ''' </value>
    Public ReadOnly Property Count As Integer Implements ICollection(Of TValue).Count
        Get
            Return Me._Parent.Count
        End Get
    End Property

    ''' <summary> Gets the enumerator. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <returns> The enumerator. </returns>
    Public Iterator Function GetEnumerator() As IEnumerator(Of TValue) Implements ICollection(Of TValue).GetEnumerator
        For Each item As KeyValuePair(Of TKey, TValue) In Me._Parent
            Yield item.Value
        Next
    End Function

    ''' <summary> Gets the enumerator. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <returns> The enumerator. </returns>
    Private Function _GetEnumerator() As System.Collections.IEnumerator Implements ICollection(Of TValue).GetEnumerator
        Return Me.GetEnumerator()
    End Function

    ''' <summary>
    ''' Copies the elements of the <see cref="T:System.Collections.Generic.ICollection`1" /> to an
    ''' <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="ArgumentException">           Thrown when one or more arguments have
    '''                                                unsupported or illegal values. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="array">      The one-dimensional <see cref="T:System.Array" /> that is the
    '''                           destination of the elements copied from
    '''                           <see cref="T:System.Collections.Generic.ICollection`1" />. The
    '''                           <see cref="T:System.Array" /> must have zero-based indexing. </param>
    ''' <param name="arrayIndex"> The zero-based index in <paramref name="array" /> at which copying
    '''                           begins. </param>
    Public Sub CopyTo(ByVal array As TValue(), ByVal arrayIndex As Integer) Implements ICollection(Of TValue).CopyTo
        If array Is Nothing Then Throw New ArgumentNullException(NameOf(array))
        Dim i As Integer = Me._Parent.Count
        If i > array.Length Then Throw New ArgumentException("The array is not big enough to hold the dictionary values.", NameOf(array))
        If 0 > arrayIndex OrElse i > array.Length + arrayIndex Then Throw New ArgumentOutOfRangeException(NameOf(arrayIndex))
        i = 0

        For Each item As KeyValuePair(Of TKey, TValue) In Me._Parent
            array(i + arrayIndex) = item.Value
            i += 1
        Next
    End Sub

End Class
