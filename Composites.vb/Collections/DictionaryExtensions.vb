Namespace CollectionExtensions

    Partial Public Module Methods

        ''' <summary> Copies from a collection to an array. </summary>
        ''' <remarks> David, 9/15/2019. </remarks>
        ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        '''                                                are null. </exception>
        ''' <exception cref="ArgumentException">           Thrown when one or more arguments have
        '''                                                unsupported or illegal values. </exception>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        '''                                                the required range. </exception>
        ''' <param name="source">     Another instance to copy. </param>
        ''' <param name="array">      The array. </param>
        ''' <param name="arrayIndex"> The zero-based index in <paramref name="array" /> at which copying
        '''                           begins. </param>
        <System.Runtime.CompilerServices.Extension>
        Public Sub CopyTo(Of TKey, TValue)(ByVal source As ICollection(Of KeyValuePair(Of TKey, TValue)),
                                           ByVal array As KeyValuePair(Of TKey, TValue)(), ByVal arrayIndex As Integer)
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            If array Is Nothing Then Throw New ArgumentNullException(NameOf(array))
            Dim i As Integer = source.Count
            If i > array.Length Then Throw New ArgumentException("The array is not big enough to hold the dictionary entries.", NameOf(array))
            If 0 > arrayIndex OrElse i > array.Length + arrayIndex Then Throw New ArgumentOutOfRangeException(NameOf(arrayIndex))
            i = 0
            For Each item As KeyValuePair(Of TKey, TValue) In source
                array(i + arrayIndex) = item
                i += 1
            Next
        End Sub

        ''' <summary> Creates the keys. </summary>
        ''' <remarks> David, 2020-09-12. </remarks>
        ''' <param name="parent"> The parent. </param>
        ''' <returns> The new keys. </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function CreateKeys(Of TKey, TValue)(ByVal parent As IDictionary(Of TKey, TValue)) As ICollection(Of TKey)
            Return New KeysCollection(Of TKey, TValue)(parent)
        End Function

        ''' <summary> Creates the values. </summary>
        ''' <remarks> David, 2020-09-12. </remarks>
        ''' <param name="parent"> The parent. </param>
        ''' <returns> The new values. </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function CreateValues(Of TKey, TValue)(ByVal parent As IDictionary(Of TKey, TValue)) As ICollection(Of TValue)
            Return New ValuesCollection(Of TKey, TValue)(parent)
        End Function

    End Module

End Namespace
