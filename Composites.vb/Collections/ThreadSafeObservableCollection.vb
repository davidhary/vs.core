Imports System.Threading
Imports System.Collections.Specialized

''' <summary>
''' A thread safe observable collection based upon
''' <see cref="ObjectModel.ObservableCollection(Of T)"/>
''' </summary>
''' <remarks>
''' David, 12/13/2016. <para>
''' TO_DO: Modify based on Observable Keyed Collection. </para>
''' </remarks>
<Serializable>
<DebuggerDisplay("Count = {Count}")>
Public Class ThreadSafeObservableCollection(Of T)
    Inherits ObjectModel.ObservableCollection(Of T)
    Implements IDisposable

#Region " CONSTRUCTION AND CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the
    ''' <see cref="T:System.Collections.ObjectModel.ObservableCollection`1" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the
    ''' <see cref="T:System.Collections.ObjectModel.ObservableCollection`1" /> class that contains
    ''' elements copied from the specified collection.
    ''' </summary>
    ''' <remarks> David, 2020-09-05. </remarks>
    ''' <param name="items"> The collection from which the elements are copied. </param>
    Public Sub New(ByVal items As IEnumerable(Of T))
        MyBase.New(items)
    End Sub

    ''' <summary> Gets the is disposed. </summary>
    ''' <value> The is disposed. </value>
    Protected Property IsDisposed As Boolean

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        Me.Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:Collection" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                                                   release only unmanaged resources. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overridable Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed Then
                If Me._ItemsLocker IsNot Nothing Then Me._ItemsLocker.Dispose() : Me._ItemsLocker = Nothing
                Me.RemovePropertyChangedEventHandlers()
            End If
        Catch
        Finally
            Me.IsDisposed = True
        End Try
    End Sub

#End Region

#Region " COLLECTION CHANGE NOTIFICATIONS "

    ''' <summary>
    ''' Raises the
    ''' <see cref="E:System.Collections.ObjectModel.ObservableCollection`1.CollectionChanged" />
    ''' event with the provided arguments.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="e"> Arguments of the event being raised. </param>
    Protected Overrides Sub OnCollectionChanged(e As NotifyCollectionChangedEventArgs)
        Me.NotifyCollectionChanged(e)
    End Sub

    ''' <summary> Removes the Collection Changed event handlers. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Protected Sub RemoveCollectionChangedEventHandler()
        Me._CollectionChangedHandlers.RemoveAll()
    End Sub

    ''' <summary> The collection changed handlers. </summary>
    <NonSerialized()>
    Private ReadOnly _CollectionChangedHandlers As New NotifyCollectionChangedEventContextCollection()

    ''' <summary> Event queue for all listeners interested in Collection Changed events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Shadows Custom Event CollectionChanged As NotifyCollectionChangedEventHandler

        AddHandler(ByVal value As NotifyCollectionChangedEventHandler)
            Me._CollectionChangedHandlers.Add(New NotifyCollectionChangedEventContext(value))
        End AddHandler

        RemoveHandler(ByVal value As NotifyCollectionChangedEventHandler)
            Me._CollectionChangedHandlers.RemoveValue(value)
        End RemoveHandler

        RaiseEvent(ByVal sender As Object, ByVal e As NotifyCollectionChangedEventArgs)
            Me._CollectionChangedHandlers.Post(sender, e)
        End RaiseEvent

    End Event

#Region " NOTIFY "

    ''' <summary>
    ''' Asynchronously notifies (posts) change on a different thread. Unsafe for cross threading;
    ''' fast return of control to the invoking function.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="e"> Collection Changed event information. </param>
    Protected Overridable Sub NotifyCollectionChanged(ByVal e As NotifyCollectionChangedEventArgs)
        Using MyBase.BlockReentrancy()
            Me._CollectionChangedHandlers.Post(Me, e)
        End Using
    End Sub

    ''' <summary>
    ''' Asynchronously notifies (posts) change on a different thread. Unsafe for cross threading;
    ''' fast return of control to the invoking function.
    ''' </summary>
    ''' <remarks>
    ''' Overhead of this method is 7 to 10 times larger than naked raise event. This has no advantage
    ''' even with slow handler functions.
    ''' </remarks>
    ''' <param name="e"> Collection Changed event information. </param>
    Protected Overridable Sub AsyncNotifyCollectionChanged(ByVal e As NotifyCollectionChangedEventArgs)
        Using MyBase.BlockReentrancy()
            Me._CollectionChangedHandlers.Post(Me, e)
        End Using
    End Sub

    ''' <summary>
    ''' Synchronously notifies (send) collection change on a different thread. Safe for cross
    ''' threading.
    ''' </summary>
    ''' <remarks>
    ''' Overhead of this method is 3 to 4 times larger than naked raise event. This is the best
    ''' approach.
    ''' </remarks>
    ''' <param name="e"> Collection Changed event information. </param>
    Protected Overridable Sub SyncNotifyCollectionChanged(ByVal e As NotifyCollectionChangedEventArgs)
        Using MyBase.BlockReentrancy()
            Me._CollectionChangedHandlers.Send(Me, e)
        End Using
    End Sub

    ''' <summary> Removes the notify collection changed event handler. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Protected Sub RemoveNotifyCollectionChangedEventHandler()
        Me._CollectionChangedHandlers.RemoveAll()
    End Sub

#End Region

#End Region

#Region " THREAD SYNC MANAGEMENT  "

    ''' <summary> Gets the items locker. </summary>
    ''' <value> The items locker. </value>
    <CodeAnalysis.SuppressMessage("Usage", "CA2235:Mark all non-serializable fields", Justification:="<Pending>")>
    Protected ReadOnly Property ItemsLocker As New ReaderWriterLockSlim()

    ''' <summary> The synchronize root. </summary>
    <NonSerialized>
    Private _SyncRoot As Object

    ''' <summary> Gets the synchronization root. </summary>
    ''' <remarks>
    ''' The Sync root helps super classes synchronously access the items. For details see reply by
    ''' Roman Zavalov in
    ''' https://stackoverflow.com/questions/728896/whats-the-use-of-the-syncroot-pattern.
    ''' </remarks>
    ''' <value> The synchronization root. </value>
    Public ReadOnly Property SyncRoot() As Object
        Get
            If Me._SyncRoot Is Nothing Then
                Me.ItemsLocker.EnterReadLock()
                Try
                    Dim c As ICollection = TryCast(Me.Items, ICollection)
                    If c IsNot Nothing Then
                        Me._SyncRoot = c.SyncRoot
                    Else
                        Interlocked.CompareExchange(Of [Object])(Me._SyncRoot, New [Object](), Nothing)
                    End If
                Finally
                    Me.ItemsLocker.ExitReadLock()
                End Try
            End If
            Return Me._SyncRoot
        End Get
    End Property

#End Region

#Region " CHECK METHODS  "

    ''' <summary> Gets the size of the is fixed. </summary>
    ''' <value> The size of the is fixed. </value>
    Public ReadOnly Property IsFixedSize() As Boolean
        Get
            Dim list As IList = TryCast(MyBase.Items, IList)
            Return If(list IsNot Nothing, list.IsFixedSize, Me.Items.IsReadOnly)
        End Get
    End Property

    ''' <summary> Gets the is read only. </summary>
    ''' <value> The is read only. </value>
    Public ReadOnly Property IsReadOnly() As Boolean
        Get
            Return Me.Items.IsReadOnly
        End Get
    End Property

    ''' <summary> Throws an exception if index is out of range. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="index"> Zero-based index of the. </param>
    Private Sub EnsureIndexRange(index As Integer)
        If index < 0 OrElse index >= Me.Items.Count Then
            Throw New ArgumentOutOfRangeException(NameOf(index), FormattableString.Invariant($"Must be between {0} and {Me.Items.Count}"))
        End If
    End Sub

    ''' <summary> Throws an exception if read only. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <exception cref="NotSupportedException"> Thrown when the requested operation is not supported. </exception>
    Private Sub EnsureNotReadOnly()
        If Me.Items.IsReadOnly Then
            Throw New NotSupportedException("Collection is read only")
        End If
    End Sub

    ''' <summary> Query if 'value' is compatible object. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> <c>true</c> if compatible object; otherwise <c>false</c> </returns>
    Public Shared Function IsCompatibleObject(value As Object) As Boolean
        ' Non-null values are fine.  Only accept nulls if T is a class or Nullable<U>.
        ' Note that default(T) is not equal to null for value types except when T is Nullable<U>. 
        Return (TypeOf value Is T) OrElse (value Is Nothing)
    End Function

#End Region

#Region " PUBLIC OVERLOADS "

    ''' <summary> Gets the number of elements. </summary>
    ''' <value> The count. </value>
    Public Overloads ReadOnly Property Count() As Integer
        Get
            Me.ItemsLocker.EnterReadLock()
            Try
                Return Me.Items.Count
            Finally
                Me.ItemsLocker.ExitReadLock()
            End Try
        End Get
    End Property

    ''' <summary>
    ''' Provides direct access to the base class item to permit synchronization for the super class.
    ''' </summary>
    ''' <value> The base item. </value>
    Protected Property BaseItem(ByVal index As Integer) As T
        Get
            Return MyBase.Item(index)
        End Get
        Set(value As T)
            MyBase.Item(index) = value
        End Set
    End Property

    ''' <summary> Gets or sets the element at the specified index. </summary>
    ''' <value> The element at the specified index. </value>
    Public Overloads Property Item(index As Integer) As T
        Get
            Me.ItemsLocker.EnterReadLock()
            Try
                Me.EnsureIndexRange(index)
                Return MyBase.Item(index)
            Finally
                Me.ItemsLocker.ExitReadLock()
            End Try
        End Get
        Set
            Me.ItemsLocker.EnterWriteLock()
            Try
                Me.EnsureNotReadOnly()
                Me.EnsureIndexRange(index)
                MyBase.Item(index) = Value
            Finally
                Me.ItemsLocker.ExitWriteLock()
            End Try
        End Set
    End Property

    ''' <summary>
    ''' Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />
    ''' .
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="item"> The object to add to the
    '''                     <see cref="T:System.Collections.Generic.ICollection`1" />
    '''                     . </param>
    Public Overloads Sub Add(ByVal item As T)
        Me.ItemsLocker.EnterWriteLock()
        Try
            Me.EnsureNotReadOnly()
            MyBase.Add(item)
        Finally
            Me.ItemsLocker.ExitWriteLock()
        End Try
    End Sub

    ''' <summary>
    ''' Copies the elements of the <see cref="T:System.Collections.Generic.ICollection`1" />
    '''  to an <see cref="T:System.Array" />
    ''' , starting at a particular <see cref="T:System.Array" />
    '''  index.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="array"> The one-dimensional <see cref="T:System.Array" />
    '''                       that is the destination of the elements copied from
    '''                       <see cref="T:System.Collections.Generic.ICollection`1" />
    '''                      . The <see cref="T:System.Array" />
    '''                       must have zero-based indexing. </param>
    ''' <param name="index"> The zero-based index in <paramref name="array" />
    '''                       at which copying begins. </param>
    Public Overloads Sub CopyTo(array As T(), index As Integer)
        Me.ItemsLocker.EnterReadLock()
        Try
            MyBase.CopyTo(array, index)
        Finally
            Me.ItemsLocker.ExitReadLock()
        End Try
    End Sub

    ''' <summary>
    ''' Determines whether the <see cref="T:System.Collections.Generic.ICollection`1" />
    '''  contains a specific value.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="item"> The object to locate in the
    '''                     <see cref="T:System.Collections.Generic.ICollection`1" />
    '''                     . </param>
    ''' <returns>
    ''' true if <paramref name="item" />
    '''  is found in the <see cref="T:System.Collections.Generic.ICollection`1" />
    ''' ; otherwise, false.
    ''' </returns>
    Public Overloads Function Contains(item As T) As Boolean
        Me.ItemsLocker.EnterReadLock()
        Try
            Return Me.Items.Contains(item)
        Finally
            Me.ItemsLocker.ExitReadLock()
        End Try
    End Function

    ''' <summary> Returns an enumerator that iterates through the collection. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <returns> An enumerator that can be used to iterate through the collection. </returns>
    Public Overloads Function GetEnumerator() As IEnumerator(Of T)
        Me.ItemsLocker.EnterReadLock()
        Try
            Return Me.Items.ToList().GetEnumerator()
        Finally
            Me.ItemsLocker.ExitReadLock()
        End Try
    End Function

    ''' <summary>
    ''' Determines the index of a specific item in the
    ''' <see cref="T:System.Collections.Generic.IList`1" />
    ''' .
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="item"> The object to locate in the
    '''                     <see cref="T:System.Collections.Generic.IList`1" />
    '''                     . </param>
    ''' <returns>
    ''' The index of <paramref name="item" />
    '''  if found in the list; otherwise, -1.
    ''' </returns>
    Public Overloads Function IndexOf(item As T) As Integer
        Me.ItemsLocker.EnterReadLock()
        Try
            Return Me.Items.IndexOf(item)
        Finally
            Me.ItemsLocker.ExitReadLock()
        End Try
    End Function

#End Region

#Region " PROTECTED OVERRIDES "

    ''' <summary> Removes all items from the collection. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Protected Overrides Sub ClearItems()
        Me.ItemsLocker.EnterWriteLock()
        Try
            Me.EnsureNotReadOnly()
            MyBase.ClearItems()
        Finally
            Me.ItemsLocker.ExitWriteLock()
        End Try
    End Sub

    ''' <summary> Inserts an item into the collection at the specified index. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="index"> The zero-based index at which <paramref name="item" />
    '''                       should be inserted. </param>
    ''' <param name="item">  The object to insert. </param>
    Protected Overrides Sub InsertItem(index As Integer, item As T)
        Me.ItemsLocker.EnterWriteLock()
        Try
            Me.EnsureNotReadOnly()
            Me.EnsureIndexRange(index)
            MyBase.InsertItem(index, item)
        Finally
            Me.ItemsLocker.ExitWriteLock()
        End Try
    End Sub

    ''' <summary> Base remove item. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="index"> Zero-based index of the. </param>
    Protected Sub BaseRemoveItem(ByVal index As Integer)
        MyBase.RemoveItem(index)
    End Sub

    ''' <summary> Removes the item at the specified index of the collection. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="index"> The zero-based index of the element to remove. </param>
    Protected Overrides Sub RemoveItem(ByVal index As Integer)
        Me.ItemsLocker.EnterWriteLock()
        Try
            Me.EnsureNotReadOnly()
            Me.EnsureIndexRange(index)
            MyBase.RemoveItem(index)
        Finally
            Me.ItemsLocker.ExitWriteLock()
        End Try
    End Sub

    ''' <summary> Replaces the element at the specified index. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="index"> The zero-based index of the element to replace. </param>
    ''' <param name="item">  The new value for the element at the specified index. </param>
    Protected Overrides Sub SetItem(index As Integer, item As T)
        Me.ItemsLocker.EnterWriteLock()
        Try
            Me.EnsureNotReadOnly()
            Me.EnsureIndexRange(index)
            MyBase.SetItem(index, item)
        Finally
            Me.ItemsLocker.ExitWriteLock()
        End Try
    End Sub

#End Region

End Class

