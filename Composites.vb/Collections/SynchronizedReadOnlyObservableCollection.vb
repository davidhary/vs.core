Imports System.Collections.ObjectModel
Imports System.Collections.Specialized
Imports System.ComponentModel
Imports System.Threading

''' <summary> Implements a synchronized read only observable keyed collection. </summary>
''' <remarks>
''' David, 5/2/2017.  <para>
''' (c) 2011 MULJADI BUDIMAN. All rights reserved. </para><para>
''' http://geekswithblogs.net/NewThingsILearned/archive/2010/01/12/make-keyedcollectionlttkey-titemgt-to-work-properly-with-wpf-data-binding.aspx
''' </para>
''' </remarks>
<DebuggerDisplay("Count = {Count}")>
Public Class SynchronizedReadOnlyObservableCollection(Of T)
    Inherits ReadOnlyCollection(Of T)
    Implements INotifyCollectionChanged, INotifyPropertyChanged

#Region " CONSTRCTION and CLEANUP "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="list"> The list. </param>
    Public Sub New(ByVal list As SynchronizedObservableCollection(Of T))
        MyBase.New(CType(list, IList(Of T)))
        Me._Context = SynchronizationContext.Current
        AddHandler CType(Me.Items, INotifyCollectionChanged).CollectionChanged, AddressOf Me.HandleCollectionChanged
        AddHandler CType(Me.Items, INotifyPropertyChanged).PropertyChanged, AddressOf Me.HandlePropertyChanged
    End Sub
#End Region

#Region " Public Events "

    ''' <summary> Event queue for all listeners interested in CollectionChanged events. </summary>
    Public Event CollectionChanged As NotifyCollectionChangedEventHandler Implements INotifyCollectionChanged.CollectionChanged

    ''' <summary> Handles the collection changed. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Notify collection changed event information. </param>
    Private Sub HandleCollectionChanged(ByVal sender As Object, ByVal e As NotifyCollectionChangedEventArgs)
        Me.OnCollectionChanged(e)
    End Sub

    ''' <summary> Raises the notify collection changed event. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Private Sub OnCollectionChanged(ByVal e As NotifyCollectionChangedEventArgs)
        Dim evt As NotifyCollectionChangedEventHandler = Me.CollectionChangedEvent
        If evt IsNot Nothing Then
            Me.Context.Send(Sub(state) evt(Me, e), Nothing)
        End If
    End Sub

    ''' <summary> Event queue for all listeners interested in PropertyChanged events. </summary>
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    ''' <summary> Handles the property changed. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property changed event information. </param>
    Private Sub HandlePropertyChanged(ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
        Me.OnPropertyChanged(e)
    End Sub

    ''' <summary> Gets or sets the context. </summary>
    ''' <value> The context. </value>
    Private ReadOnly Property Context As SynchronizationContext

    ''' <summary> Raises the property changed event. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Private Sub OnPropertyChanged(ByVal e As PropertyChangedEventArgs)
        Dim evt As PropertyChangedEventHandler = Me.PropertyChangedEvent
        If evt IsNot Nothing Then
            Me.Context.Send(Sub(state) evt(Me, e), Nothing)
        End If

    End Sub
#End Region

End Class
