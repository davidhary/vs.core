Imports System.ComponentModel

Partial Public Class ThreadSafeObservableCollection(Of T)

#Region " NOTIFY "

    ''' <summary>
    ''' Asynchronously notifies (posts) change on a different thread. Unsafe for cross threading;
    ''' fast return of control to the invoking function.
    ''' </summary>
    ''' <remarks>
    ''' Overhead of this method is 7 to 10 times larger than naked raise event. This has no advantage
    ''' even with slow handler functions.
    ''' </remarks>
    ''' <param name="e"> Property Changed event information. </param>
    Protected Overridable Sub NotifyPropertyChanged(ByVal e As PropertyChangedEventArgs)
        Me._PropertyChangedHandlers.Post(Me, e)
    End Sub

    ''' <summary>
    ''' Asynchronously notifies (posts) change on a different thread. Unsafe for cross threading;
    ''' fast return of control to the invoking function.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="name"> (Optional) Name of the caller member. </param>
    Protected Sub NotifyPropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.NotifyPropertyChanged(New PropertyChangedEventArgs(name))
        End If
    End Sub

    ''' <summary>
    ''' Asynchronously notifies (posts) property change on a different thread. Safe for cross
    ''' threading.
    ''' </summary>
    ''' <remarks>
    ''' Overhead of this method is 7 to 10 times larger than naked raise event. This has no advantage
    ''' even with slow handler functions.
    ''' </remarks>
    ''' <param name="e"> Property Changed event information. </param>
    Protected Overridable Sub AsyncNotifyPropertyChanged(ByVal e As PropertyChangedEventArgs)
        Me._PropertyChangedHandlers.Post(Me, e)
    End Sub

    ''' <summary>
    ''' Asynchronously notifies (posts) property change on a different thread. Safe for cross
    ''' threading.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="name"> (Optional) Name of the caller member. </param>
    Protected Sub AsyncNotifyPropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.AsyncNotifyPropertyChanged(New PropertyChangedEventArgs(name))
        End If
    End Sub

    ''' <summary>
    ''' Synchronously notifies (send) property change on a different thread. Safe for cross threading.
    ''' </summary>
    ''' <remarks>
    ''' Overhead of this method is 3 to 4 times larger than naked raise event. This is the best
    ''' approach.
    ''' </remarks>
    ''' <param name="e"> Property Changed event information. </param>
    Protected Overridable Sub SyncNotifyPropertyChanged(ByVal e As PropertyChangedEventArgs)
        Me._PropertyChangedHandlers.Send(Me, e)
    End Sub

    ''' <summary>
    ''' Synchronously notifies (send) property change on a different thread. Safe for cross threading.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="name"> (Optional) Name of the caller member. </param>
    Protected Sub SyncNotifyPropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.SyncNotifyPropertyChanged(New PropertyChangedEventArgs(name))
        End If
    End Sub

#End Region

#Region " RAISE (SEND) "

    ''' <summary> Raises (sends) the property changed event. </summary>
    ''' <remarks>
    ''' Overhead of this method is 3 to 4 times larger than naked raise event. This is equivalent
    ''' the<see cref="SyncNotifyPropertyChanged(PropertyChangedEventArgs)"/> method,.
    ''' </remarks>
    ''' <param name="name"> (Optional) Name of the caller member. </param>
    Protected Sub RaisePropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.RaisePropertyChanged(New PropertyChangedEventArgs(name))
        End If
    End Sub

    ''' <summary> Raises (sends) the property changed event. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub RaisePropertyChanged(ByVal e As PropertyChangedEventArgs)
        RaiseEvent PropertyChanged(Me, e)
    End Sub

#End Region

End Class

