Imports System.Runtime.InteropServices

''' <summary>
''' An AVL tree is a self-balancing Binary Search Tree (BST) where the difference between heights
''' of left and right subtrees cannot be more than one for all nodes.
''' </summary>
''' <remarks> David, 9/16/2019. </remarks>
Public Class SortedAvlTreeDictionary(Of TKey, TValue)
    Implements IDictionary(Of TKey, TValue)

    ''' <summary> An Avl tree node. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Private NotInheritable Class AvlTreeNode

        ''' <summary> Gets the key. </summary>
        ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        ''' <value> The key. </value>
        Public Property Key As TKey

        ''' <summary> Gets the value. </summary>
        ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        ''' <value> The value. </value>
        Public Property Value As TValue

        ''' <summary> Gets the left. </summary>
        ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        ''' <value> The left. </value>
        Public Property Left As AvlTreeNode

        ''' <summary> Gets the right. </summary>
        ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        ''' <value> The right. </value>
        Public Property Right As AvlTreeNode

        ''' <summary> Gets the height. </summary>
        ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        ''' <value> The height. </value>
        Public Property Height As Integer
    End Class

    ''' <summary> The root. </summary>
    Private _Root As AvlTreeNode
    ''' <summary> The comparer. </summary>
    Private ReadOnly _Comparer As IComparer(Of TKey)

    ''' <summary> Searches for the first match. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="x"> A TKey to process. </param>
    ''' <param name="t"> An AvlTreeNode to process. </param>
    ''' <returns> An AvlTreeNode. </returns>
    Private Function SearchThis(ByVal x As TKey, ByVal t As AvlTreeNode) As AvlTreeNode
        If t Is Nothing Then Return Nothing
        Dim c As Integer = Me._Comparer.Compare(x, t.Key)
        Return If(0 > c, If(0 = c, t, Me.SearchThis(x, t.Left)), If(0 = c, t, Me.SearchThis(x, t.Right)))
    End Function

    ''' <summary> Adds x. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="x"> A TKey to process. </param>
    ''' <param name="v"> A TValue to process. </param>
    ''' <param name="t"> An AvlTreeNode to process. </param>
    ''' <returns> An AvlTreeNode. </returns>
    Private Function AddThis(ByVal x As TKey, ByVal v As TValue, ByVal t As AvlTreeNode) As AvlTreeNode
        If t Is Nothing Then
            t = New AvlTreeNode With {.Key = x, .Value = v,
                    .Height = 0, .Right = Nothing, .Left = Nothing}
        Else
            Dim c As Integer = Me._Comparer.Compare(x, t.Key)
            If 0 > c Then
                t.Left = Me.AddThis(x, v, t.Left)

                If GetHeightThis(t.Left) - GetHeightThis(t.Right) = 2 Then

                    t = If(0 > Me._Comparer.Compare(x, t.Left.Key), RorThis(t), RorrThis(t))
                End If
            ElseIf 0 < c Then
                t.Right = Me.AddThis(x, v, t.Right)

                If GetHeightThis(t.Right) - GetHeightThis(t.Left) = 2 Then

                    t = If(0 < Me._Comparer.Compare(x, t.Right.Key), RolThis(t), RollThis(t))
                End If
            Else
                Throw New InvalidOperationException("An item with the specified key already exists in the dictionary.")
            End If
        End If
        t.Height = Math.Max(GetHeightThis(t.Left), GetHeightThis(t.Right)) + 1
        t.Value = v
        Return t
    End Function

    ''' <summary> RORs the given t. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="t"> An AvlTreeNode to process. </param>
    ''' <returns> An AvlTreeNode. </returns>
    Private Shared Function RorThis(ByVal t As AvlTreeNode) As AvlTreeNode
        If t.Left IsNot Nothing Then
            Dim u As AvlTreeNode = t.Left
            t.Left = u.Right
            u.Right = t
            t.Height = Math.Max(GetHeightThis(t.Left), GetHeightThis(t.Right)) + 1
            u.Height = Math.Max(GetHeightThis(u.Left), t.Height) + 1
            Return u
        End If
        Return t
    End Function

    ''' <summary> ROLs the given t. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="t"> An AvlTreeNode to process. </param>
    ''' <returns> An AvlTreeNode. </returns>
    Private Shared Function RolThis(ByVal t As AvlTreeNode) As AvlTreeNode
        If t.Right IsNot Nothing Then
            Dim u As AvlTreeNode = t.Right
            t.Right = u.Left
            u.Left = t
            t.Height = Math.Max(GetHeightThis(t.Left), GetHeightThis(t.Right)) + 1
            u.Height = Math.Max(GetHeightThis(t.Right), t.Height) + 1
            Return u
        End If

        Return t
    End Function

    ''' <summary> Rolls the given t. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="t"> An AvlTreeNode to process. </param>
    ''' <returns> An AvlTreeNode. </returns>
    Private Shared Function RollThis(ByVal t As AvlTreeNode) As AvlTreeNode
        t.Right = RorThis(t.Right)
        Return RolThis(t)
    End Function

    ''' <summary> Rorrs the given t. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="t"> An AvlTreeNode to process. </param>
    ''' <returns> An AvlTreeNode. </returns>
    Private Shared Function RorrThis(ByVal t As AvlTreeNode) As AvlTreeNode
        t.Left = RolThis(t.Left)
        Return RorThis(t)
    End Function

    ''' <summary> Gets left most. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="t"> An AvlTreeNode to process. </param>
    ''' <returns> The left most. </returns>
    Private Function GetLeftMostThis(ByVal t As AvlTreeNode) As AvlTreeNode
        If t Is Nothing Then
            Return Nothing
        ElseIf t.Left Is Nothing Then
            Return t
        Else
            Return Me.GetLeftMostThis(t.Left)
        End If
    End Function

    ''' <summary> Removes this object. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="x"> A TKey to process. </param>
    ''' <param name="t"> An AvlTreeNode to process. </param>
    ''' <returns> An AvlTreeNode. </returns>
    Private Function RemoveThis(ByVal x As TKey, ByVal t As AvlTreeNode) As AvlTreeNode
        Dim temp As AvlTreeNode

        If t Is Nothing Then
            Return Nothing
        ElseIf 0 > Me._Comparer.Compare(x, t.Key) Then
            t.Left = Me.RemoveThis(x, t.Left)
        ElseIf 0 < Me._Comparer.Compare(x, t.Key) Then
            t.Right = Me.RemoveThis(x, t.Right)
        ElseIf t.Left IsNot Nothing AndAlso t.Right IsNot Nothing Then
            temp = Me.GetLeftMostThis(t.Right)
            t.Key = temp.Key
            t.Value = temp.Value
            t.Right = Me.RemoveThis(t.Key, t.Right)
        Else

            If t.Left Is Nothing Then
                t = t.Right
            ElseIf t.Right Is Nothing Then
                t = t.Left
            End If
        End If

        If t Is Nothing Then Return t
        t.Height = Math.Max(GetHeightThis(t.Left), GetHeightThis(t.Right)) + 1

        If GetHeightThis(t.Left) - GetHeightThis(t.Right) = -2 Then

            Return If(GetHeightThis(t.Right.Right) - GetHeightThis(t.Right.Left) = 1, RolThis(t), RollThis(t))
        ElseIf GetHeightThis(t.Right) - GetHeightThis(t.Left) = 2 Then

            Return If(GetHeightThis(t.Left.Left) - GetHeightThis(t.Left.Right) = 1, RorThis(t), RorrThis(t))
        End If

        Return t
    End Function

    ''' <summary> Try remove. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="x"> A TKey to process. </param>
    ''' <param name="t"> An AvlTreeNode to process. </param>
    ''' <param name="s"> The output by reference s. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Private Function TryRemoveThis(ByVal x As TKey, ByVal t As AvlTreeNode, <Out> ByRef s As AvlTreeNode) As Boolean
        s = Nothing
        Dim temp As AvlTreeNode
        Dim res As Boolean

        If t Is Nothing Then
            Return False
        ElseIf 0 > Me._Comparer.Compare(x, t.Key) Then
            res = Me.TryRemoveThis(x, t.Left, s)
            If res Then t.Left = s
        ElseIf 0 < Me._Comparer.Compare(x, t.Key) Then
            res = Me.TryRemoveThis(x, t.Right, s)
            If res Then t.Right = s
        ElseIf t.Left IsNot Nothing AndAlso t.Right IsNot Nothing Then
            temp = Me.GetLeftMostThis(t.Right)
            t.Key = temp.Key
            t.Value = temp.Value
            res = Me.TryRemoveThis(t.Key, t.Right, s)
            If res Then t.Right = s
        Else

            If t.Left Is Nothing Then
                t = t.Right
            ElseIf t.Right Is Nothing Then
                t = t.Left
            End If

            res = True
        End If

        If t Is Nothing Then
            s = Nothing
            Return res
        End If

        t.Height = Math.Max(GetHeightThis(t.Left), GetHeightThis(t.Right)) + 1

        If GetHeightThis(t.Left) - GetHeightThis(t.Right) = -2 Then

            If GetHeightThis(t.Right.Right) - GetHeightThis(t.Right.Left) = 1 Then
                s = RolThis(t)
                Return True
            Else
                s = RollThis(t)
                Return True
            End If
        ElseIf GetHeightThis(t.Right) - GetHeightThis(t.Left) = 2 Then

            If GetHeightThis(t.Left.Left) - GetHeightThis(t.Left.Right) = 1 Then
                s = RorThis(t)
                Return True
            Else
                s = RorrThis(t)
                Return True
            End If
        End If

        s = t
        Return res
    End Function

    ''' <summary> Gets a height. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="t"> An AvlTreeNode to process. </param>
    ''' <returns> The height. </returns>
    Private Shared Function GetHeightThis(ByVal t As AvlTreeNode) As Integer
        Return If(t Is Nothing, -1, t.Height)
    End Function

    ''' <summary> Gets a balance. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="t"> An AvlTreeNode to process. </param>
    ''' <returns> The balance. </returns>
    Private Shared Function GetBalanceThis(ByVal t As AvlTreeNode) As Integer
        Return If(t Is Nothing, 0, GetHeightThis(t.Left) - GetHeightThis(t.Right))
    End Function

    ''' <summary> Enumerates enum nodes in this collection. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="t"> An AvlTreeNode to process. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process enum nodes in this collection.
    ''' </returns>
    Private Iterator Function EnumNodesThis(ByVal t As AvlTreeNode) As IEnumerable(Of KeyValuePair(Of TKey, TValue))
        If t IsNot Nothing Then

            For Each Item As KeyValuePair(Of TKey, TValue) In Me.EnumNodesThis(t.Left)
                Yield Item
            Next

            Yield New KeyValuePair(Of TKey, TValue)(t.Key, t.Value)

            For Each Item As KeyValuePair(Of TKey, TValue) In Me.EnumNodesThis(t.Right)
                Yield Item
            Next
        End If
    End Function

    ''' <summary> Gets a count. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="t"> An AvlTreeNode to process. </param>
    ''' <returns> The count. </returns>
    Private Function GetCountThis(ByVal t As AvlTreeNode) As Integer
        If t Is Nothing Then Return 0
        Dim result As Integer = 1
        result += Me.GetCountThis(t.Left)
        result += Me.GetCountThis(t.Right)
        Return result
    End Function

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="comparer"> The comparer. </param>
    Public Sub New(ByVal comparer As IComparer(Of TKey))
        Me._Comparer = If(comparer, System.Collections.Generic.Comparer(Of TKey).[Default])
        Me._Root = Nothing
    End Sub

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Public Sub New()
        Me.New(Nothing)
    End Sub

    ''' <summary> Gets the number of. </summary>
    ''' <value> The count. </value>
    Public ReadOnly Property Count As Integer Implements IDictionary(Of TKey, TValue).Count
        Get
            Return Me.GetCountThis(Me._Root)
        End Get
    End Property

    ''' <summary> Gets the is read only. </summary>
    ''' <value> The is read only. </value>
    Private ReadOnly Property IsReadOnly As Boolean Implements IDictionary(Of TKey, TValue).IsReadOnly
        Get
            Return False
        End Get
    End Property

    ''' <summary>
    ''' Determines whether the <see cref="T:System.Collections.Generic.IDictionary`2" /> contains an
    ''' element with the specified key.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="key"> The key to locate in the
    '''                    <see cref="T:System.Collections.Generic.IDictionary`2" />. </param>
    ''' <returns>
    ''' <see langword="true" /> if the <see cref="T:System.Collections.Generic.IDictionary`2" />
    ''' contains an element with the key; otherwise, <see langword="false" />.
    ''' </returns>
    Public Function ContainsKey(ByVal key As TKey) As Boolean Implements IDictionary(Of TKey, TValue).ContainsKey
        Dim n As AvlTreeNode = Me.SearchThis(key, Me._Root)
        Return n IsNot Nothing
    End Function

    ''' <summary> Gets or sets the element with the specified key. </summary>
    ''' <exception cref="KeyNotFoundException"> Thrown when a Key Not Found error condition occurs. </exception>
    ''' <value> The element with the specified key. </value>
    Default Public Property Item(ByVal key As TKey) As TValue Implements IDictionary(Of TKey, TValue).Item
        Get
            Dim value As TValue
            If Me.TryGetValue(key, value) Then Return value
            Throw New KeyNotFoundException()
        End Get
        Set(ByVal value As TValue)
            Dim n As AvlTreeNode = Me.SearchThis(key, Me._Root)
            If n IsNot Nothing Then
                n.Value = value
            Else
                Me.Add(key, value)
            End If
        End Set
    End Property

    ''' <summary> Gets the value associated with the specified key. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="key">   The key whose value to get. </param>
    ''' <param name="value"> When this method returns, the value associated with the
    ''' specified key, if the key is found; otherwise, the default value for the
    ''' type of the <paramref name="value" /> parameter. This parameter is passed
    ''' uninitialized. </param>
    ''' <returns>
    ''' <see langword="true" /> if the object that implements
    ''' <see cref="T:System.Collections.Generic.IDictionary`2" /> contains an element with the
    ''' specified key; otherwise, <see langword="false" />.
    ''' </returns>
    Public Function TryGetValue(ByVal key As TKey, <Out> ByRef value As TValue) As Boolean Implements IDictionary(Of TKey, TValue).TryGetValue
        Dim n As AvlTreeNode = Me.SearchThis(key, Me._Root)
        If n IsNot Nothing Then
            value = n.Value
            Return True
        End If
        value = Nothing
        Return False
    End Function

    ''' <summary> Query if this object contains the given item. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="item"> The item to remove. </param>
    ''' <returns> <c>true</c> if the object is in this collection; otherwise <c>false</c> </returns>
    Public Function Contains(ByVal item As KeyValuePair(Of TKey, TValue)) As Boolean Implements IDictionary(Of TKey, TValue).Contains
        Dim value As TValue
        If Me.TryGetValue(item.Key, value) AndAlso Equals(value, item.Value) Then Return True
        Return False
    End Function

    ''' <summary> Copies to. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="array">      The array. </param>
    ''' <param name="arrayIndex"> Zero-based index of the array. </param>
    Public Sub CopyTo(ByVal array As KeyValuePair(Of TKey, TValue)(), ByVal arrayIndex As Integer) Implements IDictionary(Of TKey, TValue).CopyTo
        CollectionExtensions.CopyTo(Me, array, arrayIndex)
        ' DictionaryUtility.CopyTo(Me, array, index)
    End Sub

    ''' <summary>
    ''' Adds an element with the provided key and value to the
    ''' <see cref="T:System.Collections.Generic.IDictionary`2" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="key">   The object to use as the key of the element to add. </param>
    ''' <param name="value"> The object to use as the value of the element to add. </param>
    Public Sub Add(ByVal key As TKey, ByVal value As TValue) Implements IDictionary(Of TKey, TValue).Add
        Me._Root = Me.AddThis(key, value, Me._Root)
    End Sub

    ''' <summary> Adds item. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="item"> The item to remove. </param>
    Public Sub Add(ByVal item As KeyValuePair(Of TKey, TValue)) Implements ICollection(Of KeyValuePair(Of TKey, TValue)).Add
        Me.Add(item.Key, item.Value)
    End Sub

    ''' <summary>
    ''' Removes the element with the specified key from the
    ''' <see cref="T:System.Collections.Generic.IDictionary`2" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="key"> The key of the element to remove. </param>
    ''' <returns>
    ''' <see langword="true" /> if the element is successfully removed; otherwise,
    ''' <see langword="false" />.  This method also returns <see langword="false" /> if
    ''' <paramref name="key" /> was not found in the original
    ''' <see cref="T:System.Collections.Generic.IDictionary`2" />.
    ''' </returns>
    Public Function Remove(ByVal key As TKey) As Boolean Implements IDictionary(Of TKey, TValue).Remove
        Dim s As AvlTreeNode = Nothing
        Dim res As Boolean = Me.TryRemoveThis(key, Me._Root, s)
        If res Then
            Me._Root = s
            Return True
        End If
        Return False
    End Function

    ''' <summary> Removes the given item. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="item"> The item to remove. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function Remove(ByVal item As KeyValuePair(Of TKey, TValue)) As Boolean Implements IDictionary(Of TKey, TValue).Remove
        Dim value As TValue
        If Me.TryGetValue(item.Key, value) AndAlso Equals(value, item.Value) Then Return Me.Remove(item.Key)
        Return False
    End Function

    ''' <summary> Clears this object to its blank/initial state. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Public Sub Clear() Implements IDictionary(Of TKey, TValue).Clear
        Me._Root = Nothing
    End Sub

    ''' <summary>
    ''' Gets an <see cref="T:System.Collections.Generic.ICollection`1" /> containing the keys of the
    ''' <see cref="T:System.Collections.Generic.IDictionary`2" />.
    ''' </summary>
    ''' <value>
    ''' An <see cref="T:System.Collections.Generic.ICollection`1" /> containing the keys of the
    ''' object that implements <see cref="T:System.Collections.Generic.IDictionary`2" />.
    ''' </value>
    Public ReadOnly Property Keys As ICollection(Of TKey) Implements IDictionary(Of TKey, TValue).Keys
        Get
            Return CollectionExtensions.CreateKeys(Me)
            ' Return DictionaryUtility.CreateKeys(Me)
        End Get
    End Property

    ''' <summary>
    ''' Gets an <see cref="T:System.Collections.Generic.ICollection`1" /> containing the values in
    ''' the <see cref="T:System.Collections.Generic.IDictionary`2" />.
    ''' </summary>
    ''' <value>
    ''' An <see cref="T:System.Collections.Generic.ICollection`1" /> containing the values in the
    ''' object that implements <see cref="T:System.Collections.Generic.IDictionary`2" />.
    ''' </value>
    Public ReadOnly Property Values As ICollection(Of TValue) Implements IDictionary(Of TKey, TValue).Values
        Get
            Return CollectionExtensions.CreateValues(Me)
            ' Return DictionaryUtility.CreateValues(Me)
        End Get
    End Property

    ''' <summary> Gets the enumerator. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <returns> The enumerator. </returns>
    Public Function GetEnumerator() As IEnumerator(Of KeyValuePair(Of TKey, TValue)) Implements IDictionary(Of TKey, TValue).GetEnumerator
        Return Me.EnumNodesThis(Me._Root).GetEnumerator()
    End Function

    ''' <summary> Gets the enumerator. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <returns> The enumerator. </returns>
    Private Function _GetEnumerator() As System.Collections.IEnumerator Implements IDictionary(Of TKey, TValue).GetEnumerator
        Return Me.GetEnumerator()
    End Function

End Class

