Imports System.Runtime.InteropServices

''' <summary>
''' A B-tree. A self-balancing tree data structure that maintains sorted data and allows searches,
''' sequential access, insertions, and deletions in logarithmic time. The B-tree is a
''' generalization of a binary search tree in that a node can have more than two children.
''' </summary>
''' <remarks> David, 9/16/2019. </remarks>
Public Class SortedBTreeDictionary(Of TKey, TValue)
    Implements IDictionary(Of TKey, TValue)

#Region " CONTRUCTION "

    ''' <summary> The comparer. </summary>
    Private ReadOnly _Comparer As IComparer(Of TKey)

    ''' <summary> The default mininum degree. </summary>
    Private Const _DefaultMininumDegree As Integer = 3

    ''' <summary> The root. </summary>
    Private _Root As BTreeNode

    ''' <summary> The minimum degree. </summary>
    Private ReadOnly _MinimumDegree As Integer

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="minimumDegree"> The minimum degree. </param>
    ''' <param name="comparer">      The comparer. </param>
    Public Sub New(ByVal minimumDegree As Integer, ByVal comparer As IComparer(Of TKey))
        MyBase.New()
        Me._Comparer = If(comparer, System.Collections.Generic.Comparer(Of TKey).[Default])
        Me._Root = Nothing
        Me._MinimumDegree = minimumDegree
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="minimumDegree"> The minimum degree. </param>
    Public Sub New(ByVal minimumDegree As Integer)
        Me.New(minimumDegree, Nothing)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="comparer"> The comparer. </param>
    Public Sub New(ByVal comparer As IComparer(Of TKey))
        Me.New(_DefaultMininumDegree, comparer)
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Public Sub New()
        Me.New(_DefaultMininumDegree)
    End Sub

#End Region

    ''' <summary> Returns an enumerator that iterates through the collection. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <returns> An enumerator that can be used to iterate through the collection. </returns>
    Public Iterator Function GetEnumerator() As IEnumerator(Of KeyValuePair(Of TKey, TValue)) Implements IDictionary(Of TKey, TValue).GetEnumerator
        If Me._Root IsNot Nothing Then
            For Each Item As KeyValuePair(Of TKey, TValue) In Me._Root
                Yield Item
            Next
        End If
    End Function

    ''' <summary> Gets the enumerator. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <returns> The enumerator. </returns>
    Private Function _GetEnumerator() As System.Collections.IEnumerator Implements IDictionary(Of TKey, TValue).GetEnumerator
        Return Me.GetEnumerator()
    End Function

    ''' <summary>
    ''' Copies the elements of the <see cref="T:System.Collections.Generic.ICollection`1" /> to an
    ''' <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="array">      The one-dimensional <see cref="T:System.Array" /> that is the
    '''                           destination of the elements copied from
    '''                           <see cref="T:System.Collections.Generic.ICollection`1" />. The
    '''                           <see cref="T:System.Array" /> must have zero-based indexing. </param>
    ''' <param name="arrayIndex"> The zero-based index in <paramref name="array" /> at which copying
    '''                           begins. </param>
    Public Sub CopyTo(ByVal array As KeyValuePair(Of TKey, TValue)(), ByVal arrayIndex As Integer) Implements IDictionary(Of TKey, TValue).CopyTo
        ' DictionaryUtility.CopyTo(Me, array, index)
        CollectionExtensions.CopyTo(Me, array, arrayIndex)
    End Sub

    ''' <summary>
    ''' Determines whether the <see cref="T:System.Collections.Generic.IDictionary`2" /> contains an
    ''' element with the specified key.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="key"> The key to locate in the
    '''                    <see cref="T:System.Collections.Generic.IDictionary`2" />. </param>
    ''' <returns>
    ''' <see langword="true" /> if the <see cref="T:System.Collections.Generic.IDictionary`2" />
    ''' contains an element with the key; otherwise, <see langword="false" />.
    ''' </returns>
    Public Function ContainsKey(ByVal key As TKey) As Boolean Implements IDictionary(Of TKey, TValue).ContainsKey
        Return Me._Root IsNot Nothing AndAlso Me._Root.ContainsKey(key)
    End Function

    ''' <summary>
    ''' Determines whether the <see cref="T:System.Collections.Generic.ICollection`1" /> contains a
    ''' specific value.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="item"> The object to locate in the
    '''                     <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
    ''' <returns>
    ''' <see langword="true" /> if <paramref name="item" /> is found in the
    ''' <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise,
    ''' <see langword="false" />.
    ''' </returns>
    Public Function Contains(ByVal item As KeyValuePair(Of TKey, TValue)) As Boolean Implements IDictionary(Of TKey, TValue).Contains
        Dim value As TValue
        Return Me.TryGetValue(item.Key, value) AndAlso Equals(value, item.Value)
    End Function

    ''' <summary> Gets the value associated with the specified key. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="key">   The key whose value to get. </param>
    ''' <param name="value"> When this method returns, the value associated with the
    '''  specified key, if the key is found; otherwise, the default value for the 
    '''  type of the <paramref name="value" /> parameter. This parameter is passed
    '''  uninitialized. 
    ''' </param>
    ''' <returns>
    ''' <see langword="true" /> if the object that implements
    ''' <see cref="T:System.Collections.Generic.IDictionary`2" /> contains an element with the
    ''' specified key; otherwise, <see langword="false" />.
    ''' </returns>
    Public Function TryGetValue(ByVal key As TKey, <Out> ByRef value As TValue) As Boolean Implements IDictionary(Of TKey, TValue).TryGetValue
        If Me._Root IsNot Nothing Then
            Dim node As BTreeNode = Me._Root.Search(key)
            If node IsNot Nothing Then Return node.TryGet(key, value)
        End If
        value = Nothing
        Return False
    End Function

    ''' <summary> Gets or sets the element with the specified key. </summary>
    ''' <exception cref="KeyNotFoundException"> Thrown when a Key Not Found error condition occurs. </exception>
    ''' <value> The element with the specified key. </value>
    Default Public Property Item(ByVal key As TKey) As TValue Implements IDictionary(Of TKey, TValue).Item
        Get
            If Me._Root IsNot Nothing Then
                Dim node As BTreeNode = Me._Root.Search(key)

                If node IsNot Nothing Then
                    Dim result As TValue
                    If node.TryGet(key, result) Then Return result
                End If
            End If
            Throw New KeyNotFoundException()
        End Get
        Set(ByVal value As TValue)

            If Me._Root IsNot Nothing Then
                Dim node As BTreeNode = Me._Root.Search(key)
                If node IsNot Nothing AndAlso node.TrySet(key, value) Then Return
            End If
            Me.AddThis(key, value)
        End Set
    End Property

    ''' <summary>
    ''' Gets the number of elements contained in the
    ''' <see cref="T:System.Collections.Generic.ICollection`1" />.
    ''' </summary>
    ''' <value>
    ''' The number of elements contained in the
    ''' <see cref="T:System.Collections.Generic.ICollection`1" />.
    ''' </value>
    Public ReadOnly Property Count As Integer Implements IDictionary(Of TKey, TValue).Count
        Get
            If Me._Root Is Nothing Then Return 0
            Return Me._Root.GetItemCount()
        End Get
    End Property

    ''' <summary>
    ''' Adds an element with the provided key and value to the
    ''' <see cref="T:System.Collections.Generic.IDictionary`2" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="key">   The object to use as the key of the element to add. </param>
    ''' <param name="value"> The object to use as the value of the element to add. </param>
    Public Sub Add(ByVal key As TKey, ByVal value As TValue) Implements IDictionary(Of TKey, TValue).Add
        If Me.ContainsKey(key) Then Throw New ArgumentException("The specified key already exists in the dictionary.", NameOf(key))
        Me.AddThis(key, value)
    End Sub

    ''' <summary> Adds this to 'value'. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="key">   The key of the element to remove. </param>
    ''' <param name="value"> The value. </param>
    Private Sub AddThis(ByVal key As TKey, ByVal value As TValue)
        If Me._Root Is Nothing Then
            Me._Root = New BTreeNode(Me._Comparer, Me._MinimumDegree, True)
            Me._Root.Items(0) = New KeyValuePair(Of TKey, TValue)(key, value)
            Me._Root.KeyCount = 1
        ElseIf Me._Root.KeyCount = 2 * Me._MinimumDegree - 1 Then
            Dim newRoot As BTreeNode = New BTreeNode(Me._Comparer, Me._MinimumDegree, False)
            newRoot.Children(0) = Me._Root
            newRoot.Split(0, Me._Root)
            Dim i As Integer = 0
            If 0 > Me._Comparer.Compare(newRoot.Items(0).Key, key) Then i += 1
            newRoot.Children(i).Insert(key, value)
            Me._Root = newRoot
        Else
            Me._Root.Insert(key, value)
        End If
    End Sub

    ''' <summary>
    ''' Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="item"> The object to add to the
    '''                     <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
    Public Sub Add(ByVal item As KeyValuePair(Of TKey, TValue)) Implements IDictionary(Of TKey, TValue).Add
        Me.Add(item.Key, item.Value)
    End Sub

    ''' <summary>
    ''' Removes the first occurrence of a specific object from the
    ''' <see cref="T:System.Collections.Generic.ICollection`1" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="item"> The object to remove from the
    '''                     <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
    ''' <returns>
    ''' <see langword="true" /> if <paramref name="item" /> was successfully removed from the
    ''' <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise,
    ''' <see langword="false" />. This method also returns <see langword="false" /> if
    ''' <paramref name="item" /> is not found in the original
    ''' <see cref="T:System.Collections.Generic.ICollection`1" />.
    ''' </returns>
    Public Function Remove(ByVal item As KeyValuePair(Of TKey, TValue)) As Boolean Implements IDictionary(Of TKey, TValue).Remove
        Dim value As TValue
        If Me.TryGetValue(item.Key, value) AndAlso Equals(item.Value, value) Then Return Me.Remove(item.Key)
        Return False
    End Function

    ''' <summary>
    ''' Removes the element with the specified key from the
    ''' <see cref="T:System.Collections.Generic.IDictionary`2" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="key"> The key of the element to remove. </param>
    ''' <returns>
    ''' <see langword="true" /> if the element is successfully removed; otherwise,
    ''' <see langword="false" />.  This method also returns <see langword="false" /> if
    ''' <paramref name="key" /> was not found in the original
    ''' <see cref="T:System.Collections.Generic.IDictionary`2" />.
    ''' </returns>
    Public Function Remove(ByVal key As TKey) As Boolean Implements IDictionary(Of TKey, TValue).Remove
        If Me._Root IsNot Nothing Then
            If Not Me._Root.Remove(key) Then
                If 0 = Me._Root.KeyCount Then
                    Me._Root = If(Me._Root.IsLeaf, Nothing, Me._Root.Children(0))
                End If
            End If
            Return True
        End If
        Return False
    End Function

    ''' <summary>
    ''' Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Public Sub Clear() Implements IDictionary(Of TKey, TValue).Clear
        Me._Root = Nothing
    End Sub

    ''' <summary>
    ''' Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1" />
    ''' is read-only.
    ''' </summary>
    ''' <value>
    ''' <see langword="true" /> if the <see cref="T:System.Collections.Generic.ICollection`1" /> is
    ''' read-only; otherwise, <see langword="false" />.
    ''' </value>
    Private ReadOnly Property IsReadOnly As Boolean Implements IDictionary(Of TKey, TValue).IsReadOnly
        Get
            Return False
        End Get
    End Property

    ''' <summary>
    ''' Gets an <see cref="T:System.Collections.Generic.ICollection`1" /> containing the keys of the
    ''' <see cref="T:System.Collections.Generic.IDictionary`2" />.
    ''' </summary>
    ''' <value>
    ''' An <see cref="T:System.Collections.Generic.ICollection`1" /> containing the keys of the
    ''' object that implements <see cref="T:System.Collections.Generic.IDictionary`2" />.
    ''' </value>
    Public ReadOnly Property Keys As ICollection(Of TKey) Implements IDictionary(Of TKey, TValue).Keys
        Get
            Return CollectionExtensions.CreateKeys(Me)
            ' Return DictionaryUtility.CreateKeys(Me)
        End Get
    End Property

    ''' <summary>
    ''' Gets an <see cref="T:System.Collections.Generic.ICollection`1" /> containing the values in
    ''' the <see cref="T:System.Collections.Generic.IDictionary`2" />.
    ''' </summary>
    ''' <value>
    ''' An <see cref="T:System.Collections.Generic.ICollection`1" /> containing the values in the
    ''' object that implements <see cref="T:System.Collections.Generic.IDictionary`2" />.
    ''' </value>
    Public ReadOnly Property Values As ICollection(Of TValue) Implements IDictionary(Of TKey, TValue).Values
        Get
            Return CollectionExtensions.CreateValues(Me)
            ' Return DictionaryUtility.CreateValues(Me)
        End Get
    End Property

    ''' <summary> A tree node. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Private NotInheritable Class BTreeNode
        Implements IEnumerable(Of KeyValuePair(Of TKey, TValue))
        ''' <summary> The comparer. </summary>
        Private ReadOnly _Comparer As IComparer(Of TKey)
        ''' <summary> The minimum degree. </summary>
        Private ReadOnly _MinimumDegree As Integer

        ''' <summary> Gets the items. </summary>
        ''' <value> The items. </value>
        Friend Property Items() As KeyValuePair(Of TKey, TValue)()

        ''' <summary> Gets the children. </summary>
        ''' <value> The children. </value>
        Friend Property Children() As BTreeNode()

        ''' <summary> Gets the number of keys. </summary>
        ''' <value> The number of keys. </value>
        Friend Property KeyCount As Integer

        ''' <summary> Gets the is leaf. </summary>
        ''' <value> The is leaf. </value>
        Friend Property IsLeaf As Boolean

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 2020-09-12. </remarks>
        ''' <param name="comparer">      The comparer. </param>
        ''' <param name="minimumDegree"> The minimum degree. </param>
        ''' <param name="isLeaf">        True if is leaf, false if not. </param>
        Public Sub New(ByVal comparer As IComparer(Of TKey), ByVal minimumDegree As Integer, ByVal isLeaf As Boolean)
            MyBase.New()
            Me._Comparer = comparer
            Me._MinimumDegree = minimumDegree
            Me.IsLeaf = isLeaf
            Me.Items = New KeyValuePair(Of TKey, TValue)(2 * Me._MinimumDegree - 1 - 1) {}
            Me.Children = New BTreeNode(2 * Me._MinimumDegree - 1) {}
            Me.KeyCount = 0
        End Sub

        ''' <summary> Inserts. </summary>
        ''' <remarks> David, 2020-09-12. </remarks>
        ''' <param name="key">   The key. </param>
        ''' <param name="value"> The value. </param>
        Friend Sub Insert(ByVal key As TKey, ByVal value As TValue)
            Dim i As Integer = Me.KeyCount - 1
            If i + 1 >= Me.Children.Count Then
                Stop
            End If
            If Me.IsLeaf Then
                While i >= 0 AndAlso 0 < Me._Comparer.Compare(Me.Items(i).Key, key)
                    Me.Items(i + 1) = Me.Items(i)
                    i -= 1
                End While
                Me.Items(i + 1) = New KeyValuePair(Of TKey, TValue)(key, value)
                Me.KeyCount += 1
            Else

                While i >= 0 AndAlso 0 < Me._Comparer.Compare(Me.Items(i).Key, key)
                    i -= 1
                End While

                If Me.Children(i + 1).KeyCount = 2 * Me._MinimumDegree - 1 Then
                    Me.Split(i + 1, Me.Children(i + 1))
                    If 0 > Me._Comparer.Compare(Me.Items(i + 1).Key, key) Then i += 1
                End If
                Me.Children(i + 1).Insert(key, value)
            End If
        End Sub

        ''' <summary> Splits. </summary>
        ''' <remarks> David, 2020-09-12. </remarks>
        ''' <param name="i">      Zero-based index of the. </param>
        ''' <param name="target"> Target for the. </param>
        Friend Sub Split(ByVal i As Integer, ByVal target As BTreeNode)
            Dim newNode As BTreeNode = New BTreeNode(Me._Comparer, target._MinimumDegree, target.IsLeaf) With {
                .KeyCount = Me._MinimumDegree - 1
            }

            For j As Integer = 0 To Me._MinimumDegree - 1 - 1
                newNode.Items(j) = target.Items(j + Me._MinimumDegree)
            Next

            If Not target.IsLeaf Then

                For j As Integer = 0 To Me._MinimumDegree - 1
                    newNode.Children(j) = target.Children(j + Me._MinimumDegree)
                Next
            End If

            target.KeyCount = Me._MinimumDegree - 1

            For j As Integer = Me.KeyCount To i + 1 Step -1
                Me.Children(j + 1) = Me.Children(j)
            Next

            Me.Children(i + 1) = newNode

            For j As Integer = Me.KeyCount - 1 To i Step -1
                Me.Items(j + 1) = Me.Items(j)
            Next

            Me.Items(i) = target.Items(Me._MinimumDegree - 1)
            Me.KeyCount += 1
        End Sub

        ''' <summary> Returns an enumerator that iterates through the collection. </summary>
        ''' <remarks> David, 2020-09-12. </remarks>
        ''' <returns> An enumerator that can be used to iterate through the collection. </returns>
        Public Iterator Function GetEnumerator() As IEnumerator(Of KeyValuePair(Of TKey, TValue)) Implements IEnumerable(Of KeyValuePair(Of TKey, TValue)).GetEnumerator
            Dim i As Integer

            For i = 0 To Me.KeyCount - 1

                If Not Me.IsLeaf Then

                    For Each Item As KeyValuePair(Of TKey, TValue) In Me.Children(i)
                        Yield Item
                    Next
                End If

                Yield Me.Items(i)
            Next

            If Not Me.IsLeaf Then

                For Each Item As KeyValuePair(Of TKey, TValue) In Me.Children(i)
                    Yield Item
                Next
            End If
        End Function

        ''' <summary> Gets the enumerator. </summary>
        ''' <remarks> David, 2020-09-12. </remarks>
        ''' <returns> The enumerator. </returns>
        Private Function _GetEnumerator() As System.Collections.IEnumerator Implements IEnumerable(Of KeyValuePair(Of TKey, TValue)).GetEnumerator
            Return Me.GetEnumerator()
        End Function

        ''' <summary> Searches for the first match for the given t key. </summary>
        ''' <remarks> David, 2020-09-12. </remarks>
        ''' <param name="k"> The k to remove. </param>
        ''' <returns> A BTreeNode. </returns>
        Public Function Search(ByVal k As TKey) As BTreeNode
            Dim i As Integer = 0

            While i < Me.KeyCount AndAlso 0 < Me._Comparer.Compare(k, Me.Items(i).Key)
                i += 1
            End While

            If i >= Me.KeyCount Then Return Nothing
            If 0 = Me._Comparer.Compare(Me.Items(i).Key, k) Then Return Me
            If Me.IsLeaf Then Return Nothing
            Return Me.Children(i).Search(k)
        End Function

        ''' <summary> Attempts to get a TValue from the given TKey. </summary>
        ''' <remarks> David, 2020-09-12. </remarks>
        ''' <param name="k"> The k to remove. </param>
        ''' <param name="v"> A TValue to process. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Friend Function TryGet(ByVal k As TKey, <Out> ByRef v As TValue) As Boolean
            Dim i As Integer = 0

            While i < Me.KeyCount AndAlso 0 < Me._Comparer.Compare(k, Me.Items(i).Key)
                i += 1
            End While

            Dim item As KeyValuePair(Of TKey, TValue) = Me.Items(i)

            If 0 = Me._Comparer.Compare(item.Key, k) Then
                v = item.Value
                Return True
            End If

            If Me.IsLeaf Then
                v = Nothing
                Return False
            End If

            Return Me.Children(i).TryGet(k, v)
        End Function

        ''' <summary> Attempts to set a TValue from the given TKey. </summary>
        ''' <remarks> David, 2020-09-12. </remarks>
        ''' <param name="k"> The k to remove. </param>
        ''' <param name="v"> A TValue to process. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Friend Function TrySet(ByVal k As TKey, ByVal v As TValue) As Boolean
            Dim i As Integer = 0

            While i < Me.KeyCount AndAlso 0 < Me._Comparer.Compare(k, Me.Items(i).Key)
                i += 1
            End While

            Dim item As KeyValuePair(Of TKey, TValue) = Me.Items(i)

            If 0 = Me._Comparer.Compare(item.Key, k) Then
                Me.Items(i) = New KeyValuePair(Of TKey, TValue)(k, v)
                Return True
            End If

            Return Not Me.IsLeaf AndAlso Me.Children(i).TrySet(k, v)
        End Function

        ''' <summary> Query if 'k' contains key. </summary>
        ''' <remarks> David, 2020-09-12. </remarks>
        ''' <param name="k"> The k to remove. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Public Function ContainsKey(ByVal k As TKey) As Boolean
            Dim i As Integer = 0

            While i < Me.KeyCount AndAlso 0 < Me._Comparer.Compare(k, Me.Items(i).Key)
                i += 1
            End While

            If i >= Me.KeyCount Then Return False
            Dim item As KeyValuePair(Of TKey, TValue) = Me.Items(i)
            If 0 = Me._Comparer.Compare(item.Key, k) Then Return True
            If Me.IsLeaf Then Return False
            Return Me.Children(i).ContainsKey(k)
        End Function

        ''' <summary> Gets index of key this. </summary>
        ''' <remarks> David, 2020-09-12. </remarks>
        ''' <param name="k"> The k to remove. </param>
        ''' <returns> The index of key this. </returns>
        Private Function GetIndexOfKeyThis(ByVal k As TKey) As Integer
            Dim idx As Integer = 0

            While idx < Me.KeyCount AndAlso 0 > Me._Comparer.Compare(Me.Items(idx).Key, k)
                idx += 1
            End While

            Return idx
        End Function

        ''' <summary> Removes the given k. </summary>
        ''' <remarks> David, 2020-09-12. </remarks>
        ''' <param name="k"> The k to remove. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Friend Function Remove(ByVal k As TKey) As Boolean
            Dim idx As Integer = Me.GetIndexOfKeyThis(k)

            If idx < Me.KeyCount AndAlso 0 = Me._Comparer.Compare(Me.Items(idx).Key, k) Then

                If Me.IsLeaf Then
                    Me.RemoveFromLeafThis(idx)
                Else
                    Me.RemoveFromNonLeafThis(idx)
                End If
            Else

                If Me.IsLeaf Then
                    Return False
                End If

                Dim flag As Boolean = idx = Me.KeyCount
                If Me.Children(idx).KeyCount < Me._MinimumDegree Then Me.FillThis(idx)

                If flag AndAlso idx > Me.KeyCount Then
                    Me.Children(idx - 1).Remove(k)
                Else
                    Me.Children(idx).Remove(k)
                End If
            End If

            Return True
        End Function

        ''' <summary> Removes from leaf this described by idx. </summary>
        ''' <remarks> David, 2020-09-12. </remarks>
        ''' <param name="idx"> Zero-based index of the. </param>
        Private Sub RemoveFromLeafThis(ByVal idx As Integer)
            For i As Integer = idx + 1 To Me.KeyCount - 1
                Me.Items(i - 1) = Me.Items(i)
            Next

            Me.KeyCount -= 1
            Return
        End Sub

        ''' <summary> Removes from non leaf this described by idx. </summary>
        ''' <remarks> David, 2020-09-12. </remarks>
        ''' <param name="idx"> Zero-based index of the. </param>
        Private Sub RemoveFromNonLeafThis(ByVal idx As Integer)
            Dim k As TKey = Me.Items(idx).Key

            If Me.Children(idx).KeyCount >= Me._MinimumDegree Then
                Dim pred As KeyValuePair(Of TKey, TValue) = Me.GetPreviousItemThis(idx)
                Me.Items(idx) = pred
                Me.Children(idx).Remove(pred.Key)
            ElseIf Me.Children(idx + 1).KeyCount >= Me._MinimumDegree Then
                Dim succ As KeyValuePair(Of TKey, TValue) = Me.GetNextItemThis(idx)
                Me.Items(idx) = succ
                Me.Children(idx + 1).Remove(succ.Key)
            Else
                Me.MergeThis(idx)
                Me.Children(idx).Remove(k)
            End If

            Return
        End Sub

        ''' <summary> Gets item count. </summary>
        ''' <remarks> David, 2020-09-12. </remarks>
        ''' <returns> The item count. </returns>
        Friend Function GetItemCount() As Integer
            Dim i As Integer
            Dim result As Integer = 0
            For i = 0 To Me.KeyCount - 1
                If Not Me.IsLeaf Then result += Me.Children(i).GetItemCount()
                result += 1
            Next
            If Not Me.IsLeaf Then result += Me.Children(i).GetItemCount()
            Return result
        End Function

        ''' <summary> Gets the previous item this. </summary>
        ''' <remarks> David, 2020-09-12. </remarks>
        ''' <param name="idx"> Zero-based index of the. </param>
        ''' <returns> The previous item this. </returns>
        Private Function GetPreviousItemThis(ByVal idx As Integer) As KeyValuePair(Of TKey, TValue)
            Dim cur As BTreeNode = Me.Children(idx)
            While Not cur.IsLeaf
                cur = cur.Children(cur.KeyCount)
            End While
            Return cur.Items(cur.KeyCount - 1)
        End Function

        ''' <summary> Gets the next item this. </summary>
        ''' <remarks> David, 2020-09-12. </remarks>
        ''' <param name="idx"> Zero-based index of the. </param>
        ''' <returns> The next item this. </returns>
        Private Function GetNextItemThis(ByVal idx As Integer) As KeyValuePair(Of TKey, TValue)
            Dim cur As BTreeNode = Me.Children(idx + 1)
            While Not cur.IsLeaf
                cur = cur.Children(0)
            End While
            Return cur.Items(0)
        End Function

        ''' <summary> Fill this. </summary>
        ''' <remarks> David, 2020-09-12. </remarks>
        ''' <param name="idx"> Zero-based index of the. </param>
        Private Sub FillThis(ByVal idx As Integer)
            If idx <> 0 AndAlso Me.Children(idx - 1).KeyCount >= Me._MinimumDegree Then
                Me.BorrowFromPreviousThis(idx)
            ElseIf idx <> Me.KeyCount AndAlso Me.Children(idx + 1).KeyCount >= Me._MinimumDegree Then
                Me.BorrowFromNextThis(idx)
            Else

                If idx <> Me.KeyCount Then
                    Me.MergeThis(idx)
                ElseIf 0 <> idx Then
                    Me.MergeThis(idx - 1)
                End If
            End If

            Return
        End Sub

        ''' <summary> Borrow from previous this. </summary>
        ''' <remarks> David, 2020-09-12. </remarks>
        ''' <param name="idx"> Zero-based index of the. </param>
        Private Sub BorrowFromPreviousThis(ByVal idx As Integer)
            Dim child As BTreeNode = Me.Children(idx)
            Dim sibling As BTreeNode = Me.Children(idx - 1)

            For i As Integer = child.KeyCount - 1 To 0 Step -1
                child.Items(i + 1) = child.Items(i)
            Next

            If Not child.IsLeaf Then

                For i As Integer = child.KeyCount To 0 Step -1
                    child.Children(i + 1) = child.Children(i)
                Next
            End If

            child.Items(0) = Me.Items(idx - 1)
            If Not child.IsLeaf Then child.Children(0) = sibling.Children(sibling.KeyCount)
            Me.Items(idx - 1) = sibling.Items(sibling.KeyCount - 1)
            child.KeyCount += 1
            sibling.KeyCount -= 1
            Return
        End Sub

        ''' <summary> Borrow from next this. </summary>
        ''' <remarks> David, 2020-09-12. </remarks>
        ''' <param name="idx"> Zero-based index of the. </param>
        Private Sub BorrowFromNextThis(ByVal idx As Integer)
            Dim child As BTreeNode = Me.Children(idx)
            Dim sibling As BTreeNode = Me.Children(idx + 1)
            child.Items(child.KeyCount) = Me.Items(idx)
            If Not child.IsLeaf Then child.Children((child.KeyCount) + 1) = sibling.Children(0)
            Me.Items(idx) = sibling.Items(0)

            For i As Integer = 1 To sibling.KeyCount - 1
                sibling.Items(i - 1) = sibling.Items(i)
            Next

            If Not sibling.IsLeaf Then

                For i As Integer = 1 To sibling.KeyCount
                    sibling.Children(i - 1) = sibling.Children(i)
                Next
            End If

            child.KeyCount += 1
            sibling.KeyCount -= 1
            Return
        End Sub

        ''' <summary> Merge this. </summary>
        ''' <remarks> David, 2020-09-12. </remarks>
        ''' <param name="idx"> Zero-based index of the. </param>
        Private Sub MergeThis(ByVal idx As Integer)
            Dim child As BTreeNode = Me.Children(idx)
            Dim sibling As BTreeNode = Me.Children(idx + 1)
            child.Items(Me._MinimumDegree - 1) = Me.Items(idx)

            For i As Integer = 0 To sibling.KeyCount - 1
                child.Items(i + Me._MinimumDegree) = sibling.Items(i)
            Next

            If Not child.IsLeaf Then

                For i As Integer = 0 To sibling.KeyCount
                    child.Children(i + Me._MinimumDegree) = sibling.Children(i)
                Next
            End If

            For i As Integer = idx + 1 To Me.KeyCount - 1
                Me.Items(i - 1) = Me.Items(i)
            Next

            For i As Integer = idx + 2 To Me.KeyCount
                Me.Children(i - 1) = Me.Children(i)
            Next

            child.KeyCount += sibling.KeyCount + 1
            Me.KeyCount -= 1
            Return
        End Sub
    End Class
End Class
