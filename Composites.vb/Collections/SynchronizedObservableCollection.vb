Imports System.Threading
Imports System.ComponentModel
Imports System.Collections.Specialized
Imports isr.Core.Models
Imports isr.Core.Constructs

''' <summary> A synchronized observable collection. </summary>
''' <remarks> David, 5/2/2017. TO_DO: Modify based on the Observable Keyed Collection. </remarks>
<Serializable, DebuggerDisplay("Count = {Count}")>
Public Class SynchronizedObservableCollection(Of T)
    Inherits CollectionChangedBase
    Implements IDisposable, IList(Of T), IList, IReadOnlyList(Of T), INotifyCollectionChanged, INotifyPropertyChanged

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="items"> Items to add to the collection. </param>
    Public Sub New(ByVal items As IList(Of T))
        Me.New()
        If items Is Nothing Then
            Throw New ArgumentNullException(NameOf(items), "'collection' cannot be null")
        End If
        For Each item As T In items
            Me._Items.Add(item)
        Next
    End Sub

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Me.Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary>
    ''' Gets the dispose status sentinel of the base class.  This applies to the derived class
    ''' provided proper implementation.
    ''' </summary>
    ''' <value> <c>True</c> if disposed; otherwise, <c>False</c>. </value>
    Protected Property IsDisposed() As Boolean

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks>
    ''' Executes in two distinct scenarios as determined by its disposing parameter:<para>
    ''' If True, the method has been called directly or indirectly by a user's code--managed and
    ''' unmanaged resources can be disposed.</para><para>
    ''' If False, the method has been called by the runtime from inside the finalizer and you should
    ''' not reference other objects--only unmanaged resources can be disposed.</para>
    ''' </remarks>
    ''' <param name="disposing"> <c>True</c> if this method releases both managed and unmanaged
    '''                                                                            resources;
    '''                                                                            False if this
    '''                                                                            method releases
    '''                                                                            only unmanaged
    '''                                                                            resources. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me._ItemsLocker IsNot Nothing Then Me._ItemsLocker.Dispose() : Me._ItemsLocker = Nothing
            End If
        Finally
            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True
        End Try
    End Sub

#End Region

#Region " NOTIFICATIONS "

#Region " COLLECTION CHANGE NOTIFICATIONS "

    ''' <summary> Raises the notify collection changed event. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="action"> The action. </param>
    ''' <param name="item">   The object to add to the
    '''                       <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
    ''' <param name="index">  Zero-based index of the. </param>
    Private Sub OnCollectionChanged(action As NotifyCollectionChangedAction, item As Object, index As Integer)
        Me.OnCollectionChanged(New NotifyCollectionChangedEventArgs(action, item, index))
    End Sub

    ''' <summary> Raises the notify collection changed event. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="action">   The action. </param>
    ''' <param name="item">     The object to add to the
    '''                         <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
    ''' <param name="index">    Zero-based index of the. </param>
    ''' <param name="oldIndex"> The old index. </param>
    Private Sub OnCollectionChanged(action As NotifyCollectionChangedAction, item As Object, index As Integer, oldIndex As Integer)
        Me.OnCollectionChanged(New NotifyCollectionChangedEventArgs(action, item, index, oldIndex))
    End Sub

    ''' <summary> Raises the notify collection changed event. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="action">  The action. </param>
    ''' <param name="oldItem"> The old item. </param>
    ''' <param name="newItem"> The new item. </param>
    ''' <param name="index">   Zero-based index of the. </param>
    Private Sub OnCollectionChanged(action As NotifyCollectionChangedAction, oldItem As Object, newItem As Object, index As Integer)
        Me.OnCollectionChanged(New NotifyCollectionChangedEventArgs(action, newItem, oldItem, index))
    End Sub

    ''' <summary> Raises the notify collection changed event. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Private Sub OnCollectionChanged(e As NotifyCollectionChangedEventArgs)
        Using Me.BlockReentrancy()
            Me.NotifyCollectionChanged(e)
        End Using
    End Sub

    ''' <summary> Executes the 'collection reset' action. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Private Sub OnCollectionReset()
        Me.OnCollectionChanged(New NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset))
    End Sub

#End Region

#End Region

#Region " THREAD SYNC MANAGEMENT  "

    ''' <summary> Gets the items locker. </summary>
    ''' <value> The items locker. </value>
    <CodeAnalysis.SuppressMessage("Usage", "CA2235:Mark all non-serializable fields", Justification:="<Pending>")>
    Private ReadOnly Property ItemsLocker As New ReaderWriterLockSlim()

    ''' <summary>
    ''' Gets a value indicating whether access to the <see cref="T:System.Collections.ICollection" />
    ''' is synchronized (thread safe).
    ''' </summary>
    ''' <value>
    ''' <see langword="true" /> if access to the <see cref="T:System.Collections.ICollection" /> is
    ''' synchronized (thread safe); otherwise, <see langword="false" />.
    ''' </value>
    Private ReadOnly Property IsSynchronized() As Boolean Implements ICollection.IsSynchronized
        Get
            Return True
        End Get
    End Property

    <NonSerialized>
    Private _SyncRoot As Object

    ''' <summary>
    ''' Gets an object that can be used to synchronize access to the
    ''' <see cref="T:System.Collections.ICollection" />.
    ''' </summary>
    ''' <value>
    ''' An object that can be used to synchronize access to the
    ''' <see cref="T:System.Collections.ICollection" />.
    ''' </value>
    Private ReadOnly Property SyncRoot() As Object Implements ICollection.SyncRoot
        Get
            If Me._SyncRoot Is Nothing Then
                Me.ItemsLocker.EnterReadLock()
                Try
                    Dim c As ICollection = TryCast(Me.Items, ICollection)
                    If c IsNot Nothing Then
                        Me._SyncRoot = c.SyncRoot
                    Else
                        Interlocked.CompareExchange(Of [Object])(Me._SyncRoot, New [Object](), Nothing)
                    End If
                Finally
                    Me._ItemsLocker.ExitReadLock()
                End Try
            End If
            Return Me._SyncRoot
        End Get
    End Property

#Region " CHECK RE-RENTY and VALIDATION METHODS "

    ''' <summary> Gets the monitor. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The monitor. </value>
    <CodeAnalysis.SuppressMessage("Usage", "CA2235:Mark all non-serializable fields", Justification:="<Pending>")>
    Private ReadOnly Property Monitor As New ThreadSafeMonitor()

    ''' <summary> Blocks reentrancy. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <returns> An IDisposable. </returns>
    Private Function BlockReentrancy() As IDisposable
        Me.Monitor.Enter()
        Return Me.Monitor
    End Function

    ''' <summary> Check reentrancy. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    Private Sub CheckReentrancy()
        If Me.Monitor.Busy Then
            Throw New InvalidOperationException("Reentrancy not allowed")
        End If
    End Sub

#End Region

#End Region

#Region " ITEMS AS T "

    ''' <summary> Gets the items. </summary>
    ''' <value> The items. </value>
    Private ReadOnly Property Items As IList(Of T) = New List(Of T)()

    ''' <summary>
    ''' Gets a value indicating whether the <see cref="T:System.Collections.IList" /> has a fixed
    ''' size.
    ''' </summary>
    ''' <value>
    ''' <see langword="true" /> if the <see cref="T:System.Collections.IList" /> has a fixed size;
    ''' otherwise, <see langword="false" />.
    ''' </value>
    Private ReadOnly Property IsFixedSize() As Boolean Implements IList.IsFixedSize
        Get
            Dim list As IList = TryCast(Me._Items, IList)
            Return If(list IsNot Nothing, list.IsFixedSize, Me.Items.IsReadOnly)
        End Get
    End Property

    ''' <summary>
    ''' Gets a value indicating whether the <see cref="T:System.Collections.IList" /> is read-only.
    ''' </summary>
    ''' <value>
    ''' <see langword="true" /> if the <see cref="T:System.Collections.IList" /> is read-only;
    ''' otherwise, <see langword="false" />.
    ''' </value>
    Private ReadOnly Property IsReadOnly() As Boolean Implements ICollection(Of T).IsReadOnly
        Get
            Return Me.Items.IsReadOnly
        End Get
    End Property

    ''' <summary> Gets the ilist is read only. </summary>
    ''' <value> The ilist is read only. </value>
    Private ReadOnly Property _Ilist_IsReadOnly() As Boolean Implements IList.IsReadOnly
        Get
            Return Me.Items.IsReadOnly
        End Get
    End Property

#End Region

#Region " LIST, LIST OF T and READ ONLY LIST OF T IMPLEMENTATION  "

    ''' <summary> Check index. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="index"> Zero-based index of the. </param>
    Private Sub CheckIndex(index As Integer)
        If index < 0 OrElse index >= Me.Items.Count Then
            Throw New ArgumentOutOfRangeException(NameOf(index), FormattableString.Invariant($"Must be between 0 and {Me._Items.Count}"))
        End If
    End Sub

    ''' <summary> Gets the number of elements. </summary>
    ''' <value> The count. </value>
    Public ReadOnly Property Count() As Integer Implements IList.Count, IList(Of T).Count, IReadOnlyList(Of T).Count
        Get
            Me.ItemsLocker.EnterReadLock()
            Try
                Return Me.Items.Count
            Finally
                Me.ItemsLocker.ExitReadLock()
            End Try
        End Get
    End Property

    ''' <summary> Gets or sets the element at the specified index. </summary>
    ''' <value> The element at the specified index. </value>
    Public Property Item(index As Integer) As T Implements IList(Of T).Item, IReadOnlyList(Of T).Item
        Get
            Me.ItemsLocker.EnterReadLock()
            Try
                Me.CheckIndex(index)
                Return Me.Items(index)
            Finally
                Me.ItemsLocker.ExitReadLock()
            End Try
        End Get
        Set
            Dim oldValue As T
            Me.ItemsLocker.EnterWriteLock()

            Try
                Me.CheckIsReadOnly()
                Me.CheckIndex(index)
                Me.CheckReentrancy()

                oldValue = Me(index)

                Me.Items(index) = Value
            Finally
                Me.ItemsLocker.ExitWriteLock()
            End Try

            Me.NotifyPropertyChanged(NameOf(SynchronizedObservableCollection(Of T).Item))
            Me.OnCollectionChanged(NotifyCollectionChangedAction.Replace, oldValue, Value, index)
        End Set
    End Property

    ''' <summary> Gets or sets the list item. </summary>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <value> The i list item. </value>
    Private Property IListItem(index As Integer) As Object Implements IList.Item
        Get
            Return Me.Item(index)
        End Get
        Set
            Try
                Me.Item(index) = CType(Value, T)
            Catch generatedExceptionName As InvalidCastException
                Throw New ArgumentException("'value' is the wrong type")
            End Try
        End Set
    End Property

    ''' <summary> Check is read only. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <exception cref="NotSupportedException"> Thrown when the requested operation is not supported. </exception>
    Private Sub CheckIsReadOnly()
        If Me.Items.IsReadOnly Then
            Throw New NotSupportedException("Collection is read only")
        End If
    End Sub

    ''' <summary> Query if 'value' is compatible object. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> <c>true</c> if compatible object; otherwise <c>false</c> </returns>
    Private Shared Function IsCompatibleObject(value As Object) As Boolean
        ' Non-null values are fine.  Only accept nulls if T is a class or Nullable<U>.
        ' Note that default(T) is not equal to null for value types except when T is Nullable<U>. 
        Return (TypeOf value Is T) OrElse (value Is Nothing)
    End Function

#End Region

#Region " PUBLIC METHODS "

    ''' <summary>
    ''' Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="item"> The object to add to the
    '''                     <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
    Public Sub Add(item As T) Implements IList(Of T).Add
        Me._ItemsLocker.EnterWriteLock()

        Dim index As Integer = -1

        Try
            Me.CheckIsReadOnly()
            Me.CheckReentrancy()

            index = Me.Items.Count

            Me.Items.Insert(index, item)
        Finally
            Me._ItemsLocker.ExitWriteLock()
        End Try

        Me.NotifyPropertyChanged(NameOf(SynchronizedObservableCollection(Of T).Count))
        Me.NotifyPropertyChanged(NameOf(SynchronizedObservableCollection(Of T).Item))
        Me.OnCollectionChanged(NotifyCollectionChangedAction.Add, item, index)
    End Sub

    ''' <summary> Adds an item to the <see cref="T:System.Collections.IList" />. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="value"> The object to add to the <see cref="T:System.Collections.IList" />. </param>
    ''' <returns>
    ''' The position into which the new element was inserted, or -1 to indicate that the item was not
    ''' inserted into the collection.
    ''' </returns>
    Private Function Add(value As Object) As Integer Implements IList.Add
        Me.ItemsLocker.EnterWriteLock()

        Dim index As Integer = -1
        Dim item As T

        Try
            Me.CheckIsReadOnly()
            Me.CheckReentrancy()
            index = Me.Items.Count
            item = CType(value, T)
            Me.Items.Insert(index, item)
        Catch generatedExceptionName As InvalidCastException
            Throw New ArgumentException(FormattableString.Invariant($"'{NameOf(value)}' is the wrong type"))
        Finally
            Me._ItemsLocker.ExitWriteLock()
        End Try

        Me.NotifyPropertyChanged(NameOf(SynchronizedObservableCollection(Of T).Count))
        Me.NotifyPropertyChanged(NameOf(SynchronizedObservableCollection(Of T).Item))
        Me.OnCollectionChanged(NotifyCollectionChangedAction.Add, item, index)
        Return index
    End Function

    ''' <summary> Removes all items from the <see cref="T:System.Collections.IList" />. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Public Sub Clear() Implements IList.Clear, IList(Of T).Clear
        Me.ItemsLocker.EnterWriteLock()

        Try
            Me.CheckIsReadOnly()
            Me.CheckReentrancy()
            Me.Items.Clear()
        Finally
            Me.ItemsLocker.ExitWriteLock()
        End Try

        Me.NotifyPropertyChanged(NameOf(SynchronizedObservableCollection(Of T).Count))
        Me.NotifyPropertyChanged(NameOf(SynchronizedObservableCollection(Of T).Item))
        Me.OnCollectionReset()
    End Sub

    ''' <summary>
    ''' Copies the elements of the <see cref="T:System.Collections.Generic.ICollection`1" /> to an
    ''' <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="array">      The one-dimensional <see cref="T:System.Array" /> that is the
    '''                           destination of the elements copied from
    '''                           <see cref="T:System.Collections.Generic.ICollection`1" />. The
    '''                           <see cref="T:System.Array" /> must have zero-based indexing. </param>
    ''' <param name="arrayIndex"> The zero-based index in <paramref name="array" /> at which copying
    '''                           begins. </param>
    Public Sub CopyTo(array As T(), arrayIndex As Integer) Implements IList(Of T).CopyTo
        Me.ItemsLocker.EnterReadLock()
        Try
            Me.Items.CopyTo(array, arrayIndex)
        Finally
            Me.ItemsLocker.ExitReadLock()
        End Try
    End Sub

    ''' <summary>
    ''' Copies the elements of the <see cref="T:System.Collections.ICollection" /> to an
    ''' <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="ArgumentException">           Thrown when one or more arguments have
    '''                                                unsupported or illegal values. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <exception cref="ArrayTypeMismatchException">  Thrown when an attempt is made to store an
    '''                                                object of the wrong type in the array. 
    ''' </exception>
    ''' <param name="array"> The one-dimensional <see cref="T:System.Array" /> that is the destination
    '''                      of the elements copied from
    '''                      <see cref="T:System.Collections.ICollection" />. The
    '''                      <see cref="T:System.Array" /> must have zero-based indexing. </param>
    ''' <param name="index"> The zero-based index in <paramref name="array" /> at which copying
    '''                      begins. </param>
    Private Sub CopyTo(array As Array, index As Integer) Implements ICollection.CopyTo
        Me.ItemsLocker.EnterReadLock()
        Try
            If array Is Nothing Then
                Throw New ArgumentNullException(NameOf(array), FormattableString.Invariant($"'{NameOf(array)}' cannot be null"))
            End If

            If array.Rank <> 1 Then
                Throw New ArgumentException("Multidimensional arrays are not supported", NameOf(array))
            End If

            If array.GetLowerBound(0) <> 0 Then
                Throw New ArgumentException("Non-zero lower bound arrays are not supported", NameOf(array))
            End If

            If index < 0 Then
                Throw New ArgumentOutOfRangeException(NameOf(index), FormattableString.Invariant($"'{NameOf(index)}' is out of range"))
            End If

            If array.Length - index < Me.Items.Count Then
                Throw New ArgumentException("Array is too small")
            End If

            Dim tArray As T() = TryCast(array, T())
            If tArray IsNot Nothing Then
                Me.Items.CopyTo(tArray, index)
            Else
                '
                ' Catch the obvious case assignment will fail.
                ' We can found all possible problems by doing the check though.
                ' For example, if the element type of the Array is derived from T,
                ' we can't figure out if we can successfully copy the element beforehand.
                '
                Dim targetType As System.Type = array.[GetType]().GetElementType()
                Dim sourceType As System.Type = GetType(T)
                If Not (targetType.IsAssignableFrom(sourceType) OrElse sourceType.IsAssignableFrom(targetType)) Then
                    Throw New ArrayTypeMismatchException("Invalid array type")
                End If

                '
                ' We can't cast array of value type to object[], so we don't support 
                ' widening of primitive types here.
                '
                Dim objects As Object() = TryCast(array, Object())
                If objects Is Nothing Then
                    Throw New ArrayTypeMismatchException("Invalid array type")
                End If

                Dim count As Integer = Me._Items.Count
                Try
                    Dim i As Integer = 0
                    While i < count
                        index += 1
                        objects(index) = Me.Items(i)
                        i += 1
                    End While
                Catch generatedExceptionName As ArrayTypeMismatchException
                    Throw New ArrayTypeMismatchException("Invalid array type")
                End Try
            End If
        Finally
            Me.ItemsLocker.ExitReadLock()
        End Try
    End Sub

    ''' <summary>
    ''' Determines whether the <see cref="T:System.Collections.Generic.ICollection`1" /> contains a
    ''' specific value.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="item"> The object to locate in the
    '''                     <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
    ''' <returns>
    ''' <see langword="true" /> if <paramref name="item" /> is found in the
    ''' <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise,
    ''' <see langword="false" />.
    ''' </returns>
    Public Function Contains(item As T) As Boolean Implements IList(Of T).Contains
        Me.ItemsLocker.EnterReadLock()
        Try
            Return Me.Items.Contains(item)
        Finally
            Me.ItemsLocker.ExitReadLock()
        End Try
    End Function

    ''' <summary>
    ''' Determines whether the <see cref="T:System.Collections.IList" /> contains a specific value.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="value"> The object to locate in the <see cref="T:System.Collections.IList" />. </param>
    ''' <returns>
    ''' <see langword="true" /> if the <see cref="T:System.Object" /> is found in the
    ''' <see cref="T:System.Collections.IList" />; otherwise, <see langword="false" />.
    ''' </returns>
    Private Function Contains(value As Object) As Boolean Implements IList.Contains
        If IsCompatibleObject(value) Then
            Me.ItemsLocker.EnterReadLock()
            Try
                Return Me.Items.Contains(CType(value, T))
            Finally
                Me.ItemsLocker.ExitReadLock()
            End Try
        End If

        Return False
    End Function

    ''' <summary> Returns an enumerator that iterates through the collection. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <returns> An enumerator that can be used to iterate through the collection. </returns>
    Public Function GetEnumerator() As IEnumerator(Of T) Implements IList(Of T).GetEnumerator
        Me.ItemsLocker.EnterReadLock()
        Try
            Return Me.Items.ToList().GetEnumerator()
        Finally
            Me.ItemsLocker.ExitReadLock()
        End Try
    End Function

    ''' <summary> Gets the enumerator. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <returns> The enumerator. </returns>
    Private Function _GetEnumerator() As IEnumerator Implements IEnumerable.GetEnumerator
        Me.ItemsLocker.EnterReadLock()
        Try
            Return CType(Me.Items.ToList(), IEnumerable).GetEnumerator()
        Finally
            Me.ItemsLocker.ExitReadLock()
        End Try
    End Function

    ''' <summary>
    ''' Determines the index of a specific item in the
    ''' <see cref="T:System.Collections.Generic.IList`1" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="item"> The object to locate in the
    '''                     <see cref="T:System.Collections.Generic.IList`1" />. </param>
    ''' <returns> The index of <paramref name="item" /> if found in the list; otherwise, -1. </returns>
    Public Function IndexOf(item As T) As Integer Implements IList(Of T).IndexOf
        Me.ItemsLocker.EnterReadLock()
        Try
            Return Me.Items.IndexOf(item)
        Finally
            Me.ItemsLocker.ExitReadLock()
        End Try
    End Function

    ''' <summary>
    ''' Determines the index of a specific item in the <see cref="T:System.Collections.IList" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="value"> The object to locate in the <see cref="T:System.Collections.IList" />. </param>
    ''' <returns>
    ''' The index of <paramref name="value" /> if found in the list; otherwise, -1.
    ''' </returns>
    Private Function IndexOf(value As Object) As Integer Implements IList.IndexOf
        If IsCompatibleObject(value) Then
            Me.ItemsLocker.EnterReadLock()
            Try
                Return Me.Items.IndexOf(CType(value, T))
            Finally
                Me.ItemsLocker.ExitReadLock()
            End Try
        End If

        Return -1
    End Function

    ''' <summary>
    ''' Inserts an item to the <see cref="T:System.Collections.Generic.IList`1" /> at the specified
    ''' index.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="index"> The zero-based index at which <paramref name="item" /> should be
    '''                      inserted. </param>
    ''' <param name="item">  The object to insert into the
    '''                      <see cref="T:System.Collections.Generic.IList`1" />. </param>
    Public Sub Insert(index As Integer, item As T) Implements IList(Of T).Insert
        Me.ItemsLocker.EnterWriteLock()
        Try
            Me.CheckIsReadOnly()
            If index < 0 OrElse index >= Me.Items.Count Then
                Throw New ArgumentOutOfRangeException(NameOf(index), FormattableString.Invariant($"Must be between {0} and {Me._Items.Count}"))
            End If
            Me.CheckReentrancy()
            Me.Items.Insert(index, item)
        Finally
            Me.ItemsLocker.ExitWriteLock()
        End Try
        Me.NotifyPropertyChanged(NameOf(SynchronizedObservableCollection(Of T).Count))
        Me.NotifyPropertyChanged(NameOf(SynchronizedObservableCollection(Of T).Item))
        Me.OnCollectionChanged(NotifyCollectionChangedAction.Add, item, index)
    End Sub

    ''' <summary>
    ''' Inserts an item to the <see cref="T:System.Collections.IList" /> at the specified index.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="index"> The zero-based index at which <paramref name="value" /> should be
    '''                      inserted. </param>
    ''' <param name="value"> The object to insert into the <see cref="T:System.Collections.IList" />. </param>
    Private Sub Insert(index As Integer, value As Object) Implements IList.Insert
        Try
            Me.Insert(index, CType(value, T))
        Catch generatedExceptionName As InvalidCastException
            Throw New ArgumentException(FormattableString.Invariant($"'{NameOf(value)}' is the wrong type"))
        End Try
    End Sub

    ''' <summary>
    ''' Removes the first occurrence of a specific object from the
    ''' <see cref="T:System.Collections.Generic.ICollection`1" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="item"> The object to remove from the
    '''                     <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
    ''' <returns>
    ''' <see langword="true" /> if <paramref name="item" /> was successfully removed from the
    ''' <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise,
    ''' <see langword="false" />. This method also returns <see langword="false" /> if
    ''' <paramref name="item" /> is not found in the original
    ''' <see cref="T:System.Collections.Generic.ICollection`1" />.
    ''' </returns>
    Public Function Remove(item As T) As Boolean Implements IList(Of T).Remove
        Dim index As Integer
        Dim value As T

        Me.ItemsLocker.EnterWriteLock()

        Try
            Me.CheckIsReadOnly()
            Me.CheckReentrancy()

            index = Me.Items.IndexOf(item)

            If index < 0 Then
                Return False
            End If

            value = Me.Items(index)
            Me.Items.RemoveAt(index)
        Finally
            Me.ItemsLocker.ExitWriteLock()
        End Try

        Me.NotifyPropertyChanged(NameOf(SynchronizedObservableCollection(Of T).Count))
        Me.NotifyPropertyChanged(NameOf(SynchronizedObservableCollection(Of T).Item))
        Me.OnCollectionChanged(NotifyCollectionChangedAction.Remove, value, index)

        Return True
    End Function

    ''' <summary>
    ''' Removes the first occurrence of a specific object from the
    ''' <see cref="T:System.Collections.IList" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="value"> The object to remove from the <see cref="T:System.Collections.IList" />. </param>
    Private Sub Remove(value As Object) Implements IList.Remove
        If IsCompatibleObject(value) Then
            Me.Remove(CType(value, T))
        End If
    End Sub

    ''' <summary>
    ''' Removes the <see cref="T:System.Collections.Generic.IList`1" /> item at the specified index.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="index"> The zero-based index of the item to remove. </param>
    Public Sub RemoveAt(index As Integer) Implements IList.RemoveAt, IList(Of T).RemoveAt

        Dim value As T

        Me.ItemsLocker.EnterWriteLock()

        Try
            Me.CheckIsReadOnly()
            Me.CheckIndex(index)
            Me.CheckReentrancy()

            value = Me.Items(index)

            Me.Items.RemoveAt(index)
        Finally
            Me._ItemsLocker.ExitWriteLock()
        End Try

        Me.NotifyPropertyChanged(NameOf(SynchronizedObservableCollection(Of T).Count))
        Me.NotifyPropertyChanged(NameOf(SynchronizedObservableCollection(Of T).Item))
        Me.OnCollectionChanged(NotifyCollectionChangedAction.Remove, value, index)
    End Sub
#End Region

End Class

