﻿Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Partial Public NotInheritable Class MyLibrary

        ''' <summary>
        ''' Constructor that prevents a default instance of this class from being created.
        ''' </summary>
        ''' <remarks> David, 2020-09-12. </remarks>
        Private Sub New()
            MyBase.New()
        End Sub

        ''' <summary> The assembly title. </summary>
        Public Const AssemblyTitle As String = "Core Composites Library"

        ''' <summary> Information describing the assembly. </summary>
        Public Const AssemblyDescription As String = "Core Composites Library"

        ''' <summary> The assembly product. </summary>
        Public Const AssemblyProduct As String = "Core.Composites"


    End Class

End Namespace

