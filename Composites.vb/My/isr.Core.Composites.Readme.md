## ISR Core Composites<sub>&trade;</sub>: Core Composites Class Library
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Revision History [](#){name=Revision-History}

*5.0.7280 12/07/19*  
Uses now Services, Model and Constructs created from
agnostic.

*3.2.6431 08/10/17*  
Renames hourly yield to hourly rate. Uses common code
for formatting time elements in Eon and Yield stopwatch.

*3.2.6428 08/07/17*  
Adds bins and increment function to the yield counter.

*3.2.6422 08/01/17*  
Fixes validation of bin numbers.

*3.2.6419 07/29/17*  
Adds Synchronized Keyed Observable collection. Uses
Pith Thread Safe Monitor.

*3.1.6382 06/22/17*  
Uses UTC for time keepers and yield counters.

*3.1.6352 05/23/17*  
Uses clear know state to set start time.

*3.1.6222 01/13/16*  
Adds wait function for timeout stopwatch.

*3.1.6190 12/12/16*  
Adds synchronized observable collections.

*3.1.6117 09/30/16*  
Renames count down stop watch to timeout stopwatch.
Adds Yield Counter. Adds Offset stopwatch.

*3.0.5866 01/23/16*  
Updates to .NET 4.6.1

*2.0.5126 01/13/14*  
Tagged as 2014.

*2.0.5019 09/28/13*  
Created based on the legacy core structures.

\(C\) 2009 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Collection Extensions](https://github.com/CoryCharlton/CCSWE.Core)
