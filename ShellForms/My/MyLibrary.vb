﻿Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    Partial Public NotInheritable Class MyLibrary

        ''' <summary> Constructor that prevents a default instance of this class from being created. </summary>
        Private Sub New()
            MyBase.New()
        End Sub

        Public Const AssemblyTitle As String = "Core Shell Forms Library"
        Public Const AssemblyDescription As String = "Core Shell Forms Library"
        Public Const AssemblyProduct As String = "isr.Core.Shell.Forms"

    End Class

End Namespace
