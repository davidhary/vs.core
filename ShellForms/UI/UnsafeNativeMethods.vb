﻿Friend Class UnsafeNativeMethods

    Declare Function ImageList_GetIcon Lib "comctl32" (ByVal himl As IntPtr, ByVal i As Integer, ByVal flags As Integer) As IntPtr

    ''' <Summary> Gets an IconSize from a ImagelistHandle </Summary>
    Public Declare Function ImageList_GetIconSize Lib "comctl32" (ByVal himl As IntPtr, ByRef cx As Integer, ByRef cy As Integer) As Integer

    <CodeAnalysis.SuppressMessage("Globalization", "CA2101:Specify marshaling for P/Invoke string arguments", Justification:="<Pending>")>
    Declare Auto Function RegisterClipboardFormat Lib "User32" (ByVal lpszFormat As String) As Integer

    Declare Auto Function RegisterDragDrop Lib "ole32.dll" (ByVal hWnd As IntPtr, ByVal IdropTgt As IDropTarget) As Integer

    Declare Auto Function RevokeDragDrop Lib "ole32.dll" (ByVal hWnd As IntPtr) As Integer

    ''' <Summary> Sends a message to some Window   </Summary>
    Public Declare Auto Function SendMessage Lib "user32" (ByVal hWnd As IntPtr, ByVal wMsg As Integer, ByVal wParam As Integer, ByVal lParam As IntPtr) As Integer


End Class
