<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ExplorerControl

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ExplorerControl))
        Me._DetailsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SmallIconsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._LargeIconsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ViewDropDownButton = New System.Windows.Forms.ToolStripDropDownButton()
        Me._ListToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SelectCDriveToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SetViewToDesktopToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._RefreshTreeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ChangeRootToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._FileDropDownButton = New System.Windows.Forms.ToolStripDropDownButton()
        Me._HistoryComboBox = New System.Windows.Forms.ComboBox()
        Me._ShellListView = New isr.Core.Shell.Forms.ShellListView()
        Me._StatusBar = New System.Windows.Forms.StatusBar()
        Me._ToolStrip = New System.Windows.Forms.ToolStrip()
        Me._ShellTreeView = New isr.Core.Shell.Forms.ShellTreeView()
        Me._ViewDetailsMenuItem = New System.Windows.Forms.MenuItem()
        Me._ViewListMenuItem = New System.Windows.Forms.MenuItem()
        Me._ViewSmallIconsMenuItem = New System.Windows.Forms.MenuItem()
        Me._ViewLargeIconsMenuItem = New System.Windows.Forms.MenuItem()
        Me._ViewMenuItem = New System.Windows.Forms.MenuItem()
        Me._ExitMenuItem = New System.Windows.Forms.MenuItem()
        Me._FileMenuItem = New System.Windows.Forms.MenuItem()
        Me._MainMenu = New System.Windows.Forms.MainMenu(Me.components)
        Me._SplitContainer = New System.Windows.Forms.SplitContainer()
        Me._ToolStrip.SuspendLayout()
        CType(Me._SplitContainer, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._SplitContainer.Panel1.SuspendLayout()
        Me._SplitContainer.Panel2.SuspendLayout()
        Me._SplitContainer.SuspendLayout()
        Me.SuspendLayout()
        '
        '_DetailsToolStripMenuItem
        '
        Me._DetailsToolStripMenuItem.Name = "_DetailsToolStripMenuItem"
        Me._DetailsToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me._DetailsToolStripMenuItem.Text = "&Details"
        '
        '_SmallIconsToolStripMenuItem
        '
        Me._SmallIconsToolStripMenuItem.Name = "_SmallIconsToolStripMenuItem"
        Me._SmallIconsToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me._SmallIconsToolStripMenuItem.Text = "S&mall Icons"
        '
        '_LargeIconsToolStripMenuItem
        '
        Me._LargeIconsToolStripMenuItem.Name = "_LargeIconsToolStripMenuItem"
        Me._LargeIconsToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me._LargeIconsToolStripMenuItem.Text = "Lar&ge Icons"
        '
        '_ViewDropDownButton
        '
        Me._ViewDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._ViewDropDownButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._LargeIconsToolStripMenuItem, Me._SmallIconsToolStripMenuItem, Me._ListToolStripMenuItem, Me._DetailsToolStripMenuItem})
        Me._ViewDropDownButton.Image = CType(resources.GetObject("_ViewDropDownButton.Image"), System.Drawing.Image)
        Me._ViewDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ViewDropDownButton.Name = "_ViewDropDownButton"
        Me._ViewDropDownButton.Size = New System.Drawing.Size(45, 22)
        Me._ViewDropDownButton.Text = "&View"
        '
        '_ListToolStripMenuItem
        '
        Me._ListToolStripMenuItem.Name = "_ListToolStripMenuItem"
        Me._ListToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me._ListToolStripMenuItem.Text = "&List"
        '
        '_SelectCDriveToolStripMenuItem
        '
        Me._SelectCDriveToolStripMenuItem.Name = "_SelectCDriveToolStripMenuItem"
        Me._SelectCDriveToolStripMenuItem.Size = New System.Drawing.Size(178, 22)
        Me._SelectCDriveToolStripMenuItem.Text = "Select &C:\"
        '
        '_SetViewToDesktopToolStripMenuItem
        '
        Me._SetViewToDesktopToolStripMenuItem.Name = "_SetViewToDesktopToolStripMenuItem"
        Me._SetViewToDesktopToolStripMenuItem.Size = New System.Drawing.Size(178, 22)
        Me._SetViewToDesktopToolStripMenuItem.Text = "Set View to &Desktop"
        '
        '_RefreshTreeToolStripMenuItem
        '
        Me._RefreshTreeToolStripMenuItem.Name = "_RefreshTreeToolStripMenuItem"
        Me._RefreshTreeToolStripMenuItem.Size = New System.Drawing.Size(178, 22)
        Me._RefreshTreeToolStripMenuItem.Text = "R&efresh Tree"
        '
        '_ChangeRootToolStripMenuItem
        '
        Me._ChangeRootToolStripMenuItem.Name = "_ChangeRootToolStripMenuItem"
        Me._ChangeRootToolStripMenuItem.Size = New System.Drawing.Size(178, 22)
        Me._ChangeRootToolStripMenuItem.Text = "Change &Root"
        '
        '_FileDropDownButton
        '
        Me._FileDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._FileDropDownButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ChangeRootToolStripMenuItem, Me._RefreshTreeToolStripMenuItem, Me._SetViewToDesktopToolStripMenuItem, Me._SelectCDriveToolStripMenuItem})
        Me._FileDropDownButton.Image = CType(resources.GetObject("_FileDropDownButton.Image"), System.Drawing.Image)
        Me._FileDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._FileDropDownButton.Name = "_FileDropDownButton"
        Me._FileDropDownButton.Size = New System.Drawing.Size(38, 22)
        Me._FileDropDownButton.Text = "&File"
        '
        '_HistoryComboBox
        '
        Me._HistoryComboBox.Dock = System.Windows.Forms.DockStyle.Top
        Me._HistoryComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._HistoryComboBox.Location = New System.Drawing.Point(0, 0)
        Me._HistoryComboBox.Name = "_HistoryComboBox"
        Me._HistoryComboBox.Size = New System.Drawing.Size(454, 21)
        Me._HistoryComboBox.TabIndex = 5
        '
        '_ShellListView
        '
        Me._ShellListView.Activation = System.Windows.Forms.ItemActivation.OneClick
        Me._ShellListView.Dock = System.Windows.Forms.DockStyle.Fill
        Me._ShellListView.HideSelection = False
        Me._ShellListView.Location = New System.Drawing.Point(0, 21)
        Me._ShellListView.MultiSelect = False
        Me._ShellListView.Name = "_ShellListView"
        Me._ShellListView.ShowSizeKiloBytes = False
        Me._ShellListView.Size = New System.Drawing.Size(454, 395)
        Me._ShellListView.TabIndex = 4
        Me._ShellListView.UseCompatibleStateImageBehavior = False
        '
        '_StatusBar
        '
        Me._StatusBar.Location = New System.Drawing.Point(0, 441)
        Me._StatusBar.Name = "_StatusBar"
        Me._StatusBar.Size = New System.Drawing.Size(667, 17)
        Me._StatusBar.TabIndex = 9
        Me._StatusBar.Text = "Ready"
        '
        '_ToolStrip
        '
        Me._ToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._ToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me._ToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._FileDropDownButton, Me._ViewDropDownButton})
        Me._ToolStrip.Location = New System.Drawing.Point(0, 0)
        Me._ToolStrip.Name = "_ToolStrip"
        Me._ToolStrip.Size = New System.Drawing.Size(667, 25)
        Me._ToolStrip.TabIndex = 10
        Me._ToolStrip.Text = "ToolStrip1"
        '
        '_ShellTreeView
        '
        Me._ShellTreeView.Cursor = System.Windows.Forms.Cursors.Default
        Me._ShellTreeView.Dock = System.Windows.Forms.DockStyle.Fill
        Me._ShellTreeView.HideSelection = False
        Me._ShellTreeView.Location = New System.Drawing.Point(0, 0)
        Me._ShellTreeView.Name = "_ShellTreeView"
        Me._ShellTreeView.ShowRootLines = False
        Me._ShellTreeView.Size = New System.Drawing.Size(209, 416)
        Me._ShellTreeView.TabIndex = 0
        '
        '_ViewDetailsMenuItem
        '
        Me._ViewDetailsMenuItem.Index = 3
        Me._ViewDetailsMenuItem.Text = "&Details"
        '
        '_ViewListMenuItem
        '
        Me._ViewListMenuItem.Index = 2
        Me._ViewListMenuItem.Text = "&List"
        '
        '_ViewSmallIconsMenuItem
        '
        Me._ViewSmallIconsMenuItem.Index = 1
        Me._ViewSmallIconsMenuItem.Text = "S&mall Icons"
        '
        '_ViewLargeIconsMenuItem
        '
        Me._ViewLargeIconsMenuItem.Index = 0
        Me._ViewLargeIconsMenuItem.Text = "Lar&ge Icons"
        '
        '_ViewMenuItem
        '
        Me._ViewMenuItem.Index = 1
        Me._ViewMenuItem.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me._ViewLargeIconsMenuItem, Me._ViewSmallIconsMenuItem, Me._ViewListMenuItem, Me._ViewDetailsMenuItem})
        Me._ViewMenuItem.Text = "&View"
        '
        '_ExitMenuItem
        '
        Me._ExitMenuItem.Index = 0
        Me._ExitMenuItem.Text = "E&xit"
        '
        '_FileMenuItem
        '
        Me._FileMenuItem.Index = 0
        Me._FileMenuItem.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me._ExitMenuItem})
        Me._FileMenuItem.Text = "&File"
        '
        '_MainMenu
        '
        Me._MainMenu.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me._FileMenuItem, Me._ViewMenuItem})
        '
        '_SplitContainer
        '
        Me._SplitContainer.Dock = System.Windows.Forms.DockStyle.Fill
        Me._SplitContainer.Location = New System.Drawing.Point(0, 25)
        Me._SplitContainer.Name = "_SplitContainer"
        '
        '_SplitContainer.Panel1
        '
        Me._SplitContainer.Panel1.Controls.Add(Me._ShellTreeView)
        '
        '_SplitContainer.Panel2
        '
        Me._SplitContainer.Panel2.Controls.Add(Me._ShellListView)
        Me._SplitContainer.Panel2.Controls.Add(Me._HistoryComboBox)
        Me._SplitContainer.Size = New System.Drawing.Size(667, 416)
        Me._SplitContainer.SplitterDistance = 209
        Me._SplitContainer.TabIndex = 6
        '
        'ExplorerControl
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me._SplitContainer)
        Me.Controls.Add(Me._StatusBar)
        Me.Controls.Add(Me._ToolStrip)
        Me.Name = "ExplorerControl"
        Me.Size = New System.Drawing.Size(667, 458)
        Me._ToolStrip.ResumeLayout(False)
        Me._ToolStrip.PerformLayout()
        Me._SplitContainer.Panel1.ResumeLayout(False)
        Me._SplitContainer.Panel2.ResumeLayout(False)
        CType(Me._SplitContainer, System.ComponentModel.ISupportInitialize).EndInit()
        Me._SplitContainer.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Private WithEvents _DetailsToolStripMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _SmallIconsToolStripMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _LargeIconsToolStripMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ViewDropDownButton As Windows.Forms.ToolStripDropDownButton
    Private WithEvents _ListToolStripMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _SelectCDriveToolStripMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _SetViewToDesktopToolStripMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _RefreshTreeToolStripMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ChangeRootToolStripMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _FileDropDownButton As Windows.Forms.ToolStripDropDownButton
    Private WithEvents _HistoryComboBox As Windows.Forms.ComboBox
    Private WithEvents _ShellListView As ShellListView
    Private WithEvents _StatusBar As Windows.Forms.StatusBar
    Private WithEvents _ToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _ShellTreeView As ShellTreeView
    Private WithEvents _ViewDetailsMenuItem As Windows.Forms.MenuItem
    Private WithEvents _ViewListMenuItem As Windows.Forms.MenuItem
    Private WithEvents _ViewSmallIconsMenuItem As Windows.Forms.MenuItem
    Private WithEvents _ViewLargeIconsMenuItem As Windows.Forms.MenuItem
    Private WithEvents _ViewMenuItem As Windows.Forms.MenuItem
    Private WithEvents _ExitMenuItem As Windows.Forms.MenuItem
    Private WithEvents _FileMenuItem As Windows.Forms.MenuItem
    Private WithEvents _MainMenu As Windows.Forms.MainMenu
    Private WithEvents _SplitContainer As Windows.Forms.SplitContainer
End Class
