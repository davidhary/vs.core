Imports System.Windows.Forms
Imports System.Threading
Imports System.Drawing
Imports isr.Core.Shell.ShellWrapper

''' <summary> Manager for system image lists. </summary>
''' <remarks> (c) 2012 James D. Parsells. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2019-07-06, 3.0.*. </para></remarks>
Public NotInheritable Class SystemImageListManager

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Constructor that prevents a default instance of this class from being created.
    ''' </summary>
    Private Sub New()
        MyBase.New
    End Sub

    ''' <summary> Gets or sets the initialized. </summary>
    ''' <value> The initialized. </value>
    Private Shared ReadOnly Property Initialized As Boolean = False

    ''' <summary>
    ''' Summary of Initializer.
    ''' </summary>
    Private Shared Sub Initializer()
        If SystemImageListManager.Initialized Then Return
        _SmallImageListHandle = ShellWrapper.GetImageListHandlers.SmallImageListHandle
        _LargeImageListHandle = ShellWrapper.GetImageListHandlers.LargeImageListHandle
#If False Then
        Dim dwFlag As Integer = SHGFI.USEFILEATTRIBUTES Or SHGFI.SYSICONINDEX Or SHGFI.SMALLICON
        Dim shfi As New SHFILEINFO()
        _SmallImageListHandle = SHGetFileInfo(".txt", FILE_ATTRIBUTE_NORMAL, shfi, cbFileInfo, dwFlag)
        Debug.Assert((Not _SmallImageListHandle.Equals(IntPtr.Zero)), "Failed to create Image Small ImageList")
        If _SmallImageListHandle.Equals(IntPtr.Zero) Then
            Throw New Exception("Failed to create Small ImageList")
        End If

        dwFlag = SHGFI.USEFILEATTRIBUTES Or SHGFI.SYSICONINDEX Or SHGFI.LARGEICON
        _LargeImageListHandle = SHGetFileInfo(".txt", FILE_ATTRIBUTE_NORMAL, shfi, cbFileInfo, dwFlag)
        Debug.Assert((Not _LargeImageListHandle.Equals(IntPtr.Zero)), "Failed to create Image Large ImageList")
        If _LargeImageListHandle.Equals(IntPtr.Zero) Then
            Throw New Exception("Failed to create Large ImageList")
        End If
#End If
        _Initialized = True
    End Sub

#End Region

#Region " ImageList Related Constants "
    ' For ImageList manipulation
    Private Const _LVM_FIRST As Integer = &H1000
    Private Const _LVM_SETIMAGELIST As Integer = (_LVM_FIRST + 3)

    Private Const _LVSIL_NORMAL As Integer = 0
    Private Const _LVSIL_SMALL As Integer = 1
    Private Const _LVSIL_STATE As Integer = 2

    Private Const _TV_FIRST As Integer = &H1100
    Private Const _TVM_SETIMAGELIST As Integer = (_TV_FIRST + 9)

    ' Private _TVSIL_NORMAL As Integer = 0
    ' Private _TVSIL_STATE As Integer = 2
#End Region

#Region " Public Properties"

    ''' <summary> Gets the handle of the small image list. </summary>
    ''' <value> The small image list handle. </value>
    Public Shared ReadOnly Property SmallImageListHandle() As IntPtr = IntPtr.Zero

    ''' <summary> Gets the handle of the large image list. </summary>
    ''' <value> The large image list handle. </value>
    Public Shared ReadOnly Property LargeImageListHandle() As IntPtr = IntPtr.Zero

#End Region

#Region " Public Methods"

    Private Shared ReadOnly HashTable As New Hashtable(128)

    Private Shared ReadOnly Mutex As New Mutex()

    ''' <summary>
    ''' Summary of GetIconIndex.
    ''' </summary>
    ''' <param name="item"></param>
    ''' <param name="getOpenIcon"></param>
    Public Shared Function GetIconIndex(ByRef item As ShellItem, Optional ByVal getOpenIcon As Boolean = False,
                                        Optional ByVal getSelectedIcon As Boolean = False) As Integer

        Initializer()
        Dim HasOverlay As Boolean = False  'true if it's an overlay
        Dim rVal As Integer     'The returned Index

        Dim dwflag As Integer = SHGFI.SYSICONINDEX Or SHGFI.PIDL Or SHGFI.ICON
        Dim dwAttr As Integer = 0
        'build Key into HashTable for this Item
        Dim Key As Integer = If(Not getOpenIcon, item.IconIndexNormal * 256, item.IconIndexOpen * 256)
        If item.IsLink Then
            Key = Key Or 1
            dwflag = dwflag Or SHGFI.LINKOVERLAY
            HasOverlay = True
        End If
        If item.IsShared Then
            Key = Key Or 2
            dwflag = dwflag Or SHGFI.ADDOVERLAYS
            HasOverlay = True
        End If
        If getSelectedIcon Then
            Key = Key Or 4
            dwflag = dwflag Or SHGFI.SELECTED
            HasOverlay = True   'not really an overlay, but handled the same
        End If
        If HashTable.ContainsKey(Key) Then
            rVal = CType(HashTable(Key), Integer)
            'mCnt += 1
        ElseIf Not HasOverlay Then  'for non-overlay icons, we already have
            rVal = Key \ 256        '  the right index -- put in table
            HashTable(Key) = rVal
            'bCnt += 1
        Else        'don't have icon index for an overlay, get it. 
            'This is the tricky part -- add overlaid Icon to system image list
            '  use of SmallImageList from Calum McLellan
            If item.IsFileSystem And Not item.IsDisk And Not item.IsFolder Then
                dwflag = dwflag Or SHGFI.USEFILEATTRIBUTES
                dwAttr = FILE_ATTRIBUTE_NORMAL
            End If
            Dim largeIconIndex As Integer
            Dim result As (SmallIconIndex As Integer, LargeIconIndex As Integer) = ShellWrapper.ReplaceIcons(Mutex, item.PIDL, FileInfoSize,
                                                                                                             _SmallImageListHandle, _LargeImageListHandle, dwflag, dwAttr)
            rVal = result.SmallIconIndex
            largeIconIndex = result.LargeIconIndex
#If False Then
                Dim shfi As New SHFILEINFO
                Dim shfi_small As New SHFILEINFO
                Dim HR As IntPtr
                Dim HR_SMALL As IntPtr
                HR = SHGetFileInfo(.PIDL, dwAttr, shfi, cbFileInfo, dwflag)
                HR_SMALL = SHGetFileInfo(.PIDL, dwAttr, shfi_small, cbFileInfo, dwflag Or SHGFI.SMALLICON)
                _Mutex.WaitOne()
                rVal = ImageList_ReplaceIcon(_SmallImageListHandle, -1, shfi_small.hIcon)
                Debug.Assert(rVal > -1, "Failed to add overlaid small icon")
                Dim rVal2 As Integer
                rVal2 = ImageList_ReplaceIcon(_LargeImageListHandle, -1, shfi.hIcon)
                Debug.Assert(rVal2 > -1, "Failed to add overlaid large icon")
                Debug.Assert(rVal = rVal2, "Small & Large IconIndices are Different")
                _Mutex.ReleaseMutex()
                DestroyIcon(shfi.hIcon)
                DestroyIcon(shfi_small.hIcon)
#End If
            If rVal < 0 OrElse rVal <> largeIconIndex Then Throw New ApplicationException($"Failed to add Icon for {item.DisplayName}")
            HashTable(Key) = rVal
        End If
        Return rVal
    End Function

    ''' <summary>
    ''' Returns a GDI+ copy of the icon from the ImageList
    ''' at the specified index.</summary>
    ''' <param name="index">The IconIndex of the desired Icon</param>
    ''' <param name="smallIcon">Optional, default = False. If True, return the
    '''   icon from the Small ImageList rather than the Large.</param>
    ''' <returns>The specified Icon or Nothing</returns>
    Public Shared Function GetIcon(ByVal index As Integer, Optional ByVal smallIcon As Boolean = False) As Icon
        Initializer()
        Dim icon As Icon = Nothing
        Dim hIcon As IntPtr = If(smallIcon,
            UnsafeNativeMethods.ImageList_GetIcon(_SmallImageListHandle, index, 0),
            UnsafeNativeMethods.ImageList_GetIcon(_LargeImageListHandle, index, 0))
        ' Customization to return a small image
        If Not IsNothing(hIcon) Then
            icon = System.Drawing.Icon.FromHandle(hIcon)
        End If
        Return icon
    End Function

    ''' <summary>
    ''' Associates a SysImageList with a ListView control
    ''' </summary>
    ''' <param name="listView">ListView control to associate ImageList with</param>
    ''' <param name="forLargeIcons">True=Set Large Icon List
    '''    False=Set Small Icon List</param>
    ''' <param name="forStateImages">Whether to add ImageList as StateImageList</param>
    Public Shared Sub SetListViewImageList(ByVal listView As ListView, ByVal forLargeIcons As Boolean, ByVal forStateImages As Boolean)

        Initializer()
        Dim wParam As Integer = _LVSIL_NORMAL
        Dim HImageList As IntPtr = _LargeImageListHandle
        If Not forLargeIcons Then
            wParam = _LVSIL_SMALL
            HImageList = _SmallImageListHandle
        End If
        If forStateImages Then
            wParam = _LVSIL_STATE
        End If
        UnsafeNativeMethods.SendMessage(listView.Handle, _LVM_SETIMAGELIST, wParam, HImageList)

    End Sub

    ''' <summary>
    ''' Associates a SysImageList with a TreeView control
    ''' Summary of SetTreeViewImageList.
    ''' </summary>
    ''' <param name="treeView">TreeView control to associated ImageList with</param>
    ''' <param name="forStateImages">Whether to add ImageList as StateImageList</param>
    Public Shared Sub SetTreeViewImageList(ByVal treeView As TreeView, ByVal forStateImages As Boolean)
        Initializer()
        Dim wParam As Integer = _LVSIL_NORMAL
        If forStateImages Then
            wParam = _LVSIL_STATE
        End If
        Dim HR As Integer
        HR = UnsafeNativeMethods.SendMessage(treeView.Handle, _TVM_SETIMAGELIST, wParam, _SmallImageListHandle)
    End Sub

#End Region

End Class

