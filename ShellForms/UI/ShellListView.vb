Imports System.ComponentModel
Imports System.IO
Imports System.Threading
Imports System.Windows.Forms
#Const Ver = 2005

''' <summary> A file list view. </summary>
''' <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2019-07-07 </para></remarks>
Public Class ShellListView
    Inherits System.Windows.Forms.ListView

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        Me.InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        SystemImageListManager.SetListViewImageList(Me, True, False)
        SystemImageListManager.SetListViewImageList(Me, False, False)

        ' post vs 2005, the below is perfectly safe. It is not recommended as
        ' a general solution. Feel free to modify this demo to properly do
        ' cross thread modification of the ListView. 
        Control.CheckForIllegalCrossThreadCalls = False
    End Sub

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso Me._Components IsNot Nothing Then
                Me._Components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " DESIGNER "

    'Required by the Windows Form Designer
    Private ReadOnly _Components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.NameColumnHeader = New System.Windows.Forms.ColumnHeader
        Me.SizeColumnHeader = New System.Windows.Forms.ColumnHeader
        Me.TypeColumnHeader = New System.Windows.Forms.ColumnHeader
        Me.ModifyDateColumnHeader = New System.Windows.Forms.ColumnHeader
        Me.AttributesColumnHeader = New System.Windows.Forms.ColumnHeader
        Me.SuspendLayout()
        '
        'lv1
        '
        Me.Activation = System.Windows.Forms.ItemActivation.OneClick
        Me.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.NameColumnHeader, Me.AttributesColumnHeader, Me.SizeColumnHeader, Me.TypeColumnHeader, Me.ModifyDateColumnHeader})
        Me.Dock = System.Windows.Forms.DockStyle.Fill
        Me.HideSelection = False
        Me.Location = New System.Drawing.Point(0, 0)
        Me.MultiSelect = False
        Me.Name = "lv1"
        Me.Size = New System.Drawing.Size(150, 150)
        Me.TabIndex = 0
        Me.UseCompatibleStateImageBehavior = False
        '
        '_NameColumnHeader
        '
        Me.NameColumnHeader.Text = "Name"
        Me.NameColumnHeader.Width = 180
        '
        '_AttributesColumnHeader
        '
        Me.AttributesColumnHeader.Text = "Attributes"
        Me.AttributesColumnHeader.Width = 72
        '
        '_SizeColumnHeader
        '
        Me.SizeColumnHeader.Text = "Size"
        Me.SizeColumnHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.SizeColumnHeader.Width = 80
        '
        '_TypeColumnHeader
        '
        Me.TypeColumnHeader.Text = "Type"
        Me.TypeColumnHeader.Width = 100
        '
        '_ModifyDateColumnHeader
        '
        Me.ModifyDateColumnHeader.Text = "Modified"
        Me.ModifyDateColumnHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ModifyDateColumnHeader.Width = 80
        '
        'FileListViewControl
        '
        Me.Name = "FileListViewControl"
        Me.ResumeLayout(False)

    End Sub

    Private WithEvents NameColumnHeader As System.Windows.Forms.ColumnHeader
    Private WithEvents SizeColumnHeader As System.Windows.Forms.ColumnHeader
    Private WithEvents TypeColumnHeader As System.Windows.Forms.ColumnHeader
    Private WithEvents ModifyDateColumnHeader As System.Windows.Forms.ColumnHeader
    Private WithEvents AttributesColumnHeader As System.Windows.Forms.ColumnHeader

#End Region

#Region " ON EVENT OVERRIDES "

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.Control.VisibleChanged" /> event.
    ''' </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnVisibleChanged(e As EventArgs)
        MyBase.OnVisibleChanged(e)
        If Me.Visible Then
            SystemImageListManager.SetListViewImageList(Me, True, False)
            SystemImageListManager.SetListViewImageList(Me, False, False)
        End If
    End Sub

    ''' <summary> Gets or sets the double click launch of a process is enabled. </summary>
    ''' <value> <c>True</c>  if a process launch is enabled on double. </value>
    Public Property ProcessLaunchEnabled As Boolean

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.Control.DoubleClick" /> event.
    ''' </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub OnDoubleClick(e As EventArgs)
        MyBase.OnDoubleClick(e)
        If Me.ProcessLaunchEnabled Then
            Dim csi As ShellItem = TryCast(Me.SelectedItems(0).Tag, ShellItem)
            If csi.IsFolder Then
                ' let the related tree node expand this folder.
            Else
                Try
                    Process.Start(csi.Path)
                Catch ex As Exception
                    MsgBox(ex.Message, MsgBoxStyle.OkOnly, "Error in starting application")
                End Try
            End If
        End If
    End Sub

#End Region

#Region " TREE NODE SELECTED: POPULATE "

    ''' <summary> Gets or sets the show size kilo bytes. </summary>
    ''' <value> The show size kilo bytes. </value>
    <Category("Options"), Description("Show size in Kilo Bytes."), DefaultValue(False), Browsable(True)>
    Public Property ShowSizeKiloBytes As Boolean

    ''' <summary> The empty last write time set to avoid Globalization problem. </summary>
    Private Shared ReadOnly EmptyLastWriteTime As New DateTime(1, 1, 1, 0, 0, 0)

    ''' <summary> The manual reset event. </summary>
    Private Shared ReadOnly ManualResetEvent As New ManualResetEvent(True)
    Private Const _UnknownCaption As String = "<n/a>"
    Private Const _DiskCaption As String = "<disk>"

    ''' <summary> Populates the given files and directories. </summary>
    ''' <param name="pathName">    Full pathname of the file. </param>
    ''' <param name="directories"> List of directories. </param>
    ''' <param name="files">       List of files. </param>
    Public Sub Populate(ByVal pathName As String, directories As ArrayList, files As ArrayList)

        Dim totalItems As Integer = directories.Count + files.Count
        ManualResetEvent.WaitOne()
        If totalItems > 0 Then
            Dim item As ShellItem
            directories.Sort()
            files.Sort()
            Me.Text = pathName
            Dim combList As New ArrayList(totalItems)
            combList.AddRange(directories)
            combList.AddRange(files)

            ' Build the ListViewItems & add to the list view
            Me.BeginUpdate()
            Me.Items.Clear()
            MyBase.Refresh()
            For Each item In combList
                Dim lvi As New ListViewItem(item.DisplayName)
                If Not item.IsDisk And item.IsFileSystem Then
                    Dim attr As FileAttributes = item.Attributes
                    Dim SB As New System.Text.StringBuilder()
                    If (attr And FileAttributes.System) = FileAttributes.System Then SB.Append("S")
                    If (attr And FileAttributes.Hidden) = FileAttributes.Hidden Then SB.Append("H")
                    If (attr And FileAttributes.ReadOnly) = FileAttributes.ReadOnly Then SB.Append("R")
                    If (attr And FileAttributes.Archive) = FileAttributes.Archive Then SB.Append("A")
                    lvi.SubItems.Add(SB.ToString)
                Else
                    lvi.SubItems.Add(ShellListView._UnknownCaption)
                End If
                If Not item.IsDisk And item.IsFileSystem And Not item.IsFolder Then
                    If Me.ShowSizeKiloBytes Then
                        If item.Length > 1024 Then
                            lvi.SubItems.Add($"{(item.Length / 1024):N0} K")
                        Else
                            lvi.SubItems.Add($"{item.Length:N0} B")
                        End If
                    Else
                        lvi.SubItems.Add(item.Length.ToString("N0", Globalization.CultureInfo.CurrentCulture))
                    End If
                Else
                    lvi.SubItems.Add(ShellListView._UnknownCaption)
                End If
                lvi.SubItems.Add(item.TypeName)
                If item.IsDisk Then
                    lvi.SubItems.Add(ShellListView._DiskCaption)
                Else
                    If item.LastWriteTime = ShellListView.EmptyLastWriteTime Then '"#0001-01-01 12:00:00 AM#" is empty
                        lvi.SubItems.Add(ShellListView._UnknownCaption)
                    Else
                        lvi.SubItems.Add(ShellListView.FormatFileDate(item.LastWriteTime))
                    End If
                End If
                '.ImageIndex = SystemImageListManager.GetIconIndex(item, False)
                lvi.Tag = item
                Me.Items.Add(lvi)
            Next
            Me.EndUpdate()
            Me.StartLoadingImages()
        Else
            Me.Items.Clear()
        End If
    End Sub

    ''' <summary> Formats the date using short date and time values. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns> The formatted date. </returns>
    Private Shared Function FormatFileDate(ByVal value As DateTime) As String
        Return $"{value.ToShortDateString()} {value.ToShortTimeString()}"
    End Function

#End Region

#Region " ICON INDEX LOADING THREAD"

    ''' <summary> Starts a thread for loading the list view images. </summary>
    Private Sub StartLoadingImages()
        Dim ts As New ThreadStart(AddressOf Me.LoadImages)
        Dim ot As New Thread(ts)
        ot.SetApartmentState(ApartmentState.STA)
        ShellListView.ManualResetEvent.Reset()
        ot.Start()
    End Sub

    ''' <summary> Loads the images. </summary>
    Private Sub LoadImages()
        Dim lvi As ListViewItem
        For Each lvi In Me.Items
            lvi.ImageIndex = SystemImageListManager.GetIconIndex(TryCast(lvi.Tag, ShellItem), False)
        Next
        ManualResetEvent.Set()
    End Sub

#End Region

End Class

