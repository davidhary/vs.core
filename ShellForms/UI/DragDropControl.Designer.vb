<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DragDropControl

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DragDropControl))
        Me._DetailsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SmallIconsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._LargeIconsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ViewDropDownButton = New System.Windows.Forms.ToolStripDropDownButton()
        Me._ListToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SetViewToDesktopToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._RefreshTreeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ChangeRootToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._FileDropDownButton = New System.Windows.Forms.ToolStripDropDownButton()
        Me._DropOnTextBox = New System.Windows.Forms.TextBox()
        Me._StatusBar = New System.Windows.Forms.StatusBar()
        Me._Splitter = New System.Windows.Forms.Splitter()
        Me._ShellTreeView = New isr.Core.Shell.Forms.ShellTreeView()
        Me._ShellListView = New isr.Core.Shell.Forms.ShellListView()
        Me._Panel2 = New System.Windows.Forms.Panel()
        Me._Panel1 = New System.Windows.Forms.Panel()
        Me._ToolStrip = New System.Windows.Forms.ToolStrip()
        Me._Panel2.SuspendLayout()
        Me._Panel1.SuspendLayout()
        Me._ToolStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_DetailsToolStripMenuItem
        '
        Me._DetailsToolStripMenuItem.Name = "_DetailsToolStripMenuItem"
        Me._DetailsToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me._DetailsToolStripMenuItem.Text = "&Details"
        '
        '_SmallIconsToolStripMenuItem
        '
        Me._SmallIconsToolStripMenuItem.Name = "_SmallIconsToolStripMenuItem"
        Me._SmallIconsToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me._SmallIconsToolStripMenuItem.Text = "S&mall Icons"
        '
        '_LargeIconsToolStripMenuItem
        '
        Me._LargeIconsToolStripMenuItem.Name = "_LargeIconsToolStripMenuItem"
        Me._LargeIconsToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me._LargeIconsToolStripMenuItem.Text = "Lar&ge Icons"
        '
        '_ViewDropDownButton
        '
        Me._ViewDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._ViewDropDownButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._LargeIconsToolStripMenuItem, Me._SmallIconsToolStripMenuItem, Me._ListToolStripMenuItem, Me._DetailsToolStripMenuItem})
        Me._ViewDropDownButton.Image = CType(resources.GetObject("_ViewDropDownButton.Image"), System.Drawing.Image)
        Me._ViewDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ViewDropDownButton.Name = "_ViewDropDownButton"
        Me._ViewDropDownButton.Size = New System.Drawing.Size(45, 22)
        Me._ViewDropDownButton.Text = "&View"
        '
        '_ListToolStripMenuItem
        '
        Me._ListToolStripMenuItem.Name = "_ListToolStripMenuItem"
        Me._ListToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me._ListToolStripMenuItem.Text = "&List"
        '
        '_SetViewToDesktopToolStripMenuItem
        '
        Me._SetViewToDesktopToolStripMenuItem.Name = "_SetViewToDesktopToolStripMenuItem"
        Me._SetViewToDesktopToolStripMenuItem.Size = New System.Drawing.Size(178, 22)
        Me._SetViewToDesktopToolStripMenuItem.Text = "Set View to &Desktop"
        '
        '_RefreshTreeToolStripMenuItem
        '
        Me._RefreshTreeToolStripMenuItem.Name = "_RefreshTreeToolStripMenuItem"
        Me._RefreshTreeToolStripMenuItem.Size = New System.Drawing.Size(178, 22)
        Me._RefreshTreeToolStripMenuItem.Text = "R&efresh Tree"
        '
        '_ChangeRootToolStripMenuItem
        '
        Me._ChangeRootToolStripMenuItem.Name = "_ChangeRootToolStripMenuItem"
        Me._ChangeRootToolStripMenuItem.Size = New System.Drawing.Size(178, 22)
        Me._ChangeRootToolStripMenuItem.Text = "Change &Root"
        '
        '_FileDropDownButton
        '
        Me._FileDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._FileDropDownButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ChangeRootToolStripMenuItem, Me._RefreshTreeToolStripMenuItem, Me._SetViewToDesktopToolStripMenuItem})
        Me._FileDropDownButton.Image = CType(resources.GetObject("_FileDropDownButton.Image"), System.Drawing.Image)
        Me._FileDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._FileDropDownButton.Name = "_FileDropDownButton"
        Me._FileDropDownButton.Size = New System.Drawing.Size(38, 22)
        Me._FileDropDownButton.Text = "&File"
        '
        '_DropOnTextBox
        '
        Me._DropOnTextBox.AllowDrop = True
        Me._DropOnTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._DropOnTextBox.Location = New System.Drawing.Point(0, 336)
        Me._DropOnTextBox.Multiline = True
        Me._DropOnTextBox.Name = "_DropOnTextBox"
        Me._DropOnTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me._DropOnTextBox.Size = New System.Drawing.Size(654, 222)
        Me._DropOnTextBox.TabIndex = 9
        '
        '_StatusBar
        '
        Me._StatusBar.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me._StatusBar.Location = New System.Drawing.Point(0, 558)
        Me._StatusBar.Name = "_StatusBar"
        Me._StatusBar.Size = New System.Drawing.Size(654, 18)
        Me._StatusBar.TabIndex = 8
        Me._StatusBar.Text = "Ready"
        '
        '_Splitter
        '
        Me._Splitter.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me._Splitter.Location = New System.Drawing.Point(212, 0)
        Me._Splitter.Name = "_Splitter"
        Me._Splitter.Size = New System.Drawing.Size(7, 311)
        Me._Splitter.TabIndex = 2
        Me._Splitter.TabStop = False
        '
        '_ShellTreeView
        '
        Me._ShellTreeView.Cursor = System.Windows.Forms.Cursors.Default
        Me._ShellTreeView.Dock = System.Windows.Forms.DockStyle.Left
        Me._ShellTreeView.HideSelection = False
        Me._ShellTreeView.Location = New System.Drawing.Point(0, 0)
        Me._ShellTreeView.Name = "_ShellTreeView"
        Me._ShellTreeView.ShowRootLines = False
        Me._ShellTreeView.Size = New System.Drawing.Size(212, 311)
        Me._ShellTreeView.TabIndex = 1
        '
        '_ShellListView
        '
        Me._ShellListView.Activation = System.Windows.Forms.ItemActivation.OneClick
        Me._ShellListView.AllowDrop = True
        Me._ShellListView.Dock = System.Windows.Forms.DockStyle.Fill
        Me._ShellListView.HideSelection = False
        Me._ShellListView.Location = New System.Drawing.Point(0, 0)
        Me._ShellListView.MultiSelect = False
        Me._ShellListView.Name = "_ShellListView"
        Me._ShellListView.ShowSizeKiloBytes = False
        Me._ShellListView.Size = New System.Drawing.Size(435, 311)
        Me._ShellListView.TabIndex = 5
        Me._ShellListView.UseCompatibleStateImageBehavior = False
        Me._ShellListView.View = System.Windows.Forms.View.Details
        '
        '_Panel2
        '
        Me._Panel2.Controls.Add(Me._ShellListView)
        Me._Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Panel2.Location = New System.Drawing.Point(219, 0)
        Me._Panel2.Name = "_Panel2"
        Me._Panel2.Size = New System.Drawing.Size(435, 311)
        Me._Panel2.TabIndex = 3
        '
        '_Panel1
        '
        Me._Panel1.Controls.Add(Me._Panel2)
        Me._Panel1.Controls.Add(Me._Splitter)
        Me._Panel1.Controls.Add(Me._ShellTreeView)
        Me._Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me._Panel1.Location = New System.Drawing.Point(0, 25)
        Me._Panel1.Name = "_Panel1"
        Me._Panel1.Size = New System.Drawing.Size(654, 311)
        Me._Panel1.TabIndex = 7
        '
        '_ToolStrip
        '
        Me._ToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._ToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me._ToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._FileDropDownButton, Me._ViewDropDownButton})
        Me._ToolStrip.Location = New System.Drawing.Point(0, 0)
        Me._ToolStrip.Name = "_ToolStrip"
        Me._ToolStrip.Size = New System.Drawing.Size(654, 25)
        Me._ToolStrip.TabIndex = 10
        Me._ToolStrip.Text = "ToolStrip1"
        '
        'DragDropControl
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me._DropOnTextBox)
        Me.Controls.Add(Me._StatusBar)
        Me.Controls.Add(Me._Panel1)
        Me.Controls.Add(Me._ToolStrip)
        Me.Name = "DragDropControl"
        Me.Size = New System.Drawing.Size(654, 576)
        Me._Panel2.ResumeLayout(False)
        Me._Panel1.ResumeLayout(False)
        Me._ToolStrip.ResumeLayout(False)
        Me._ToolStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Private WithEvents _DetailsToolStripMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _SmallIconsToolStripMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _LargeIconsToolStripMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ViewDropDownButton As Windows.Forms.ToolStripDropDownButton
    Private WithEvents _ListToolStripMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _SetViewToDesktopToolStripMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _RefreshTreeToolStripMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ChangeRootToolStripMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _FileDropDownButton As Windows.Forms.ToolStripDropDownButton
    Private WithEvents _DropOnTextBox As Windows.Forms.TextBox
    Private WithEvents _StatusBar As Windows.Forms.StatusBar
    Private WithEvents _Splitter As Windows.Forms.Splitter
    Private WithEvents _ShellTreeView As ShellTreeView
    Private WithEvents _ShellListView As ShellListView
    Private WithEvents _Panel2 As Windows.Forms.Panel
    Private WithEvents _Panel1 As Windows.Forms.Panel
    Private WithEvents _ToolStrip As Windows.Forms.ToolStrip
End Class
