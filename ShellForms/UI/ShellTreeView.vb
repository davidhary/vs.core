Imports System.ComponentModel
Imports System.Drawing
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Windows.Forms
Imports isr.Core.Shell.ShellItem

''' <summary> An explorer tree view. </summary>
''' <remarks> (c) 2012 James D. Parsells. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2019-07-06, 3.0.*. </para></remarks>
<DefaultProperty("StartUpDirectory"), DefaultEvent("TreeNodeSelected")>
Public Class ShellTreeView
    Inherits System.Windows.Forms.TreeView

#Region " DESIGNER "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me.HideSelection = False
        Me.ShowRootLines = False
    End Sub

    Protected Overrides Sub OnControlAdded(e As ControlEventArgs)
        MyBase.OnControlAdded(e)
        If Not Me.DesignMode Then
            'setting the image list here allows many good things to happen, but
            ' also one bad thing -- the "tool tip" like display of selectednode.text
            ' is made invisible.  This remains a problem to be solved.
            SystemImageListManager.SetTreeViewImageList(Me, False)

            ' AddHandler StartUpDirectoryChanged, AddressOf OnStartUpDirectoryChanged
            Me.OnStartUpDirectoryChanged(Me._StartUpDirectory)
        End If
    End Sub


    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Me.ExpandNodeTimer IsNot Nothing Then Me.ExpandNodeTimer.Dispose()
            Me.ExpandNodeTimer = Nothing
        End If
        MyBase.Dispose(disposing)
    End Sub

#End Region

#Region " PRIVATE MEMBERS "

    ' Public Event StartUpDirectoryChanged(ByVal newVal As StartDir)

    ' Public Event ExpTreeNodeSelected(ByVal SelPath As String, ByVal Item As ShellItem)

    Private _EnableEventPost As Boolean = True 'flag to suppress ExpTreeNodeSelected raising during refresh and 

    Private WithEvents DragDropHandler As TreeViewDragWrapper

#End Region

#Region " PUBLIC MEMBERS "

    ''' <summary> Event queue for all listeners interested in TreeNodeSelected events. </summary>
    Public Event TreeNodeSelected As EventHandler(Of TreeViewEventArgs)

    ''' <summary>
    ''' Gets or sets a value indicating whether the control can accept data that the user drags onto
    ''' it.
    ''' </summary>
    ''' <exception cref="ThreadStateException"> Thrown when a Thread State error condition occurs. </exception>
    ''' <value>
    ''' <see langword="true" /> if drag-and-drop operations are allowed in the control; otherwise,
    ''' <see langword="false" />. The default is <see langword="false" />.
    ''' </value>
    Public Overrides Property AllowDrop() As Boolean
        Get
            Return Not IsNothing(Me.DragDropHandler)
        End Get
        Set(ByVal value As Boolean)
            If value Then
                If IsNothing(Me.DragDropHandler) Then
                    If Me.IsHandleCreated Then
                        If Application.OleRequired = Threading.ApartmentState.STA Then
                            Me.DragDropHandler = New TreeViewDragWrapper(Me)
                            Dim res As Integer
                            res = UnsafeNativeMethods.RegisterDragDrop(Me.Handle, Me.DragDropHandler)
                            If Not (res = 0) Or (res = -2147221247) Then
                                Me.DragDropHandler.Dispose()
                                Me.DragDropHandler = Nothing
                                Marshal.ThrowExceptionForHR(res)
                                'Throw New Exception("Failed to Register DragDrop for " & Me.Name)
                            End If
                        Else
                            Throw New ThreadStateException("ThreadMustBeSTA")
                        End If
                    End If
                End If
            Else
                If Not IsNothing(Me.DragDropHandler) Then
                    Dim res As Integer
                    res = UnsafeNativeMethods.RevokeDragDrop(Me.Handle)
                    If res <> 0 Then
                        Debug.WriteLine("RevokeDragDrop returned " & res)
                    End If
                    Me.DragDropHandler.Dispose()
                    Me.DragDropHandler = Nothing
                End If
            End If
        End Set
    End Property

    Private _Root As TreeNode
    ''' <summary> Gets or sets the root item. RootItem is a Run-Time only Property
    ''' Setting this Item via an External call results in
    ''' re-setting the entire tree to be rooted in the
    ''' input Shell Item
    ''' The new Shell Item must be a valid Shell Item of some kind
    ''' of Folder (File Folder or System Folder)
    ''' Attempts to set it using a non-Folder Shell Item are ignored </summary>
    ''' <value> The root item. </value>
    <Browsable(False)>
    Public Property RootItem() As ShellItem
        Get
            Return TryCast(Me._Root.Tag, ShellItem)
        End Get
        Set(ByVal value As ShellItem)
            If Value?.IsFolder Then
                If Not IsNothing(Me._Root) Then Me.ClearTree()
                Me._Root = New TreeNode(Value.DisplayName)
                Me.BuildTree(Value.GetDirectories())
                Me._Root.ImageIndex = SystemImageListManager.GetIconIndex(Value, False)
                Me._Root.SelectedImageIndex = Me._Root.ImageIndex
                Me._Root.Tag = Value
                Me._Root.Name = Value.PIDL.ToString
                Me.Nodes.Add(Me._Root)
                Me._Root.Expand()
                Me.SelectedNode = Me._Root
            End If
        End Set
    End Property

    ''' <summary> Gets the selected item. </summary>
    ''' <value> The selected item. </value>
    <Browsable(False)>
    Public ReadOnly Property SelectedItem() As ShellItem
        Get
            Return If(Me.SelectedNode Is Nothing, Nothing, TryCast(Me.SelectedNode.Tag, ShellItem))
        End Get
    End Property

    ''' <summary> Show Hidden Directories. </summary>
    ''' <value> The show hidden folders. </value>
    <Category("Options"), Description("Show Hidden Directories."), DefaultValue(True), Browsable(True)>
    Public Property ShowHiddenFolders() As Boolean = True

    ''' <summary> Allow Collapse of Root Item. </summary>
    ''' <value> The show root lines. </value>
    <Category("Options"), Description("Allow Collapse of Root Item."), DefaultValue(True), Browsable(True)>
    Public Overloads Property ShowRootLines() As Boolean
        Get
            Return MyBase.ShowRootLines
        End Get
        Set(ByVal value As Boolean)
            If Value <> MyBase.ShowRootLines Then
                MyBase.ShowRootLines = Value
                MyBase.Refresh()
            End If
        End Set
    End Property

    Private _StartUpDirectory As ShellSpecialFolder = ShellSpecialFolder.Desktop

    <Category("Options"),
     Description("Sets the Initial Directory of the Tree"),
     DefaultValue(ShellSpecialFolder.Desktop), Browsable(True)>
    Public Property StartUpDirectory() As ShellSpecialFolder
        Get
            Return Me._StartUpDirectory
        End Get
        Set(ByVal value As ShellSpecialFolder)
            If Array.IndexOf([Enum].GetValues(Value.GetType), Value) >= 0 Then
                Me._StartUpDirectory = Value
                Me.OnStartUpDirectoryChanged(Value) ' RaiseEvent StartUpDirectoryChanged(Value)
            Else
                Throw New ApplicationException("Invalid Initial StartUpDirectory")
            End If
        End Set
    End Property

#End Region

#Region " PUBLIC METHODS "

    '''<Summary>Refresh Tree Method thanks to Calum McLellan</Summary>
    <Description("Refresh the Tree and all nodes through the currently selected item")>
    Public Sub RefreshTree(Optional ByVal rootCSI As ShellItem = Nothing)

        ' Modified to use ExpandANode(Shell Item) rather than ExpandANode(path)
        ' 
        ' Set refresh variable for BeforeExpand method
        Me._EnableEventPost = False

        Dim selnode As TreeNode = If(IsNothing(Me.SelectedNode), Me._Root, Me.SelectedNode)
        ' End modification
        ' 
        Try
            Me.BeginUpdate()
            Dim SelCSI As ShellItem = TryCast(selnode.Tag, ShellItem)
            'Set root node
            Me.RootItem = If(IsNothing(rootCSI), Me.RootItem, rootCSI)
            'Try to expand the node
            If Not Me.ExpandANode(SelCSI) Then
                Dim nodeList As New ArrayList()
                While Not IsNothing(selnode.Parent)
                    nodeList.Add(selnode.Parent)
                    selnode = selnode.Parent
                End While

                For Each selnode In nodeList
                    If Me.ExpandANode(CType(selnode.Tag, ShellItem)) Then Exit For
                Next
            End If
            'Reset refresh variable for BeforeExpand method
        Finally
            Me.EndUpdate()
        End Try
        Me._EnableEventPost = True

        ' We suppressed EventPosting during refresh, so give it one now
        Me.OnAfterSelect(New TreeViewEventArgs(Me.SelectedNode))

    End Sub

#End Region

#Region " EXPAND NODE "

    ''' <summary> Expand a node. </summary>
    ''' <param name="newPath"> Full pathname of the new file. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function ExpandANode(ByVal newPath As String) As Boolean
        ExpandANode = False     'assume failure
        Dim newItem As ShellItem
        Try
            newItem = GetShellItem(newPath)
            If newItem Is Nothing Then Exit Function
            If Not newItem.IsFolder Then Exit Function
        Catch
            Exit Function
        End Try
        Return Me.ExpandANode(newItem)
    End Function

    ''' <summary> Expand node. </summary>
    ''' <param name="listShellItem">       The list shell item. </param>
    ''' <param name="expandAncestorNodes"> True to expand ancestor nodes. </param>
    Public Sub ExpandNode(ByVal listShellItem As ShellItem, ByVal expandAncestorNodes As Boolean)
        ' find the corresponding tree node
        If listShellItem Is Nothing OrElse listShellItem.PIDL = CType(Me._Root.Tag, ShellItem).PIDL Then Return
        Dim foundNode As TreeNode = Me.FindNode(listShellItem)
        If foundNode IsNot Nothing Then
            Me.SelectedNode = foundNode
            Me.OnAfterSelect(New TreeViewEventArgs(Me.SelectedNode))
        End If
        If expandAncestorNodes Then
            Dim testShellItem As ShellItem = listShellItem
            Do Until testShellItem Is Nothing OrElse foundNode Is Nothing OrElse testShellItem.PIDL = CType(Me._Root.Tag, ShellItem).PIDL
                foundNode = Me.FindAncestorNode(testShellItem)
                If foundNode IsNot Nothing Then
                    Me.SelectedNode = foundNode
                    Me.OnAfterSelect(New TreeViewEventArgs(Me.SelectedNode))
                    testShellItem = CType(foundNode.Tag, ShellItem)
                End If
                DispatcherExtensions.DoEvents(Windows.Threading.Dispatcher.CurrentDispatcher)
            Loop
        End If
    End Sub

    ''' <summary> Expand a node. </summary>
    ''' <param name="newItem"> The new item. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    ''' <remarks> This does not work. 
    ''' TO_DO: This needs to be fix. Possibly, starting the expansion from the 
    ''' new item and looking for the Ancestor node each time </remarks>
    Public Function ExpandANode(ByVal newItem As ShellItem) As Boolean
        ExpandANode = False     'assume failure
        Dim baseNode As TreeNode = Me._Root
        Me.BeginUpdate()
        baseNode.Expand() 'Ensure base is filled in
        'do the drill down -- Node to expand must be included in tree
        Dim testNode As TreeNode
        Dim shellItem As ShellItem = TryCast(baseNode.Tag, ShellItem)
        If shellItem Is Nothing Then Return False
        Dim lim As Integer = ShellItem.PidlCount(newItem.PIDL) - ShellItem.PidlCount(shellItem.PIDL)
        Do While lim > 0
            For Each testNode In baseNode.Nodes
                If ShellItem.IsAncestorOf(shellItem, newItem, False) Then
                    baseNode = testNode
                    Me.RefreshNode(baseNode)   'ensure up-to-date
                    baseNode.Expand()
                    lim -= 1
                    GoTo NEXLEV
                End If
            Next
            GoTo XIT     ' on falling through For, we can't find it, so get out
NEXLEV: Loop
        'after falling through here, we have found & expanded the node
        Me.HideSelection = False
        Me.Select()
        Me.SelectedNode = baseNode
        ExpandANode = True
XIT:    Me.EndUpdate()
    End Function

#End Region

#Region " INITIAL DIR SET HANDLER "

    Private Sub OnStartUpDirectoryChanged(ByVal newVal As ShellSpecialFolder)
        If Not IsNothing(Me._Root) Then
            Me.ClearTree()
        End If
        ' TO_DO: See why new val is not used.
        Dim special As ShellItem
        ' special = ShellItem.GetShellItem(CType(Val(Me._StartUpDirectory), ShellWrapper.CSIDL))
        special = ShellItem.GetShellItem(CType(Val(newVal), CSIDL))
        Me._Root = New TreeNode(special.DisplayName) With {
            .ImageIndex = SystemImageListManager.GetIconIndex(special, False), .Name = special.PIDL.ToString
        }
        Me._Root.SelectedImageIndex = Me._Root.ImageIndex
        Me._Root.Tag = special
        Me.BuildTree(special.GetDirectories())
        Me.Nodes.Add(Me._Root)
        Me._Root.Expand()
    End Sub

    Private Sub BuildTree(ByVal l1 As ArrayList)
        l1.Sort()
        Dim CSI As ShellItem
        For Each CSI In l1
            If Not (CSI.IsHidden And Not Me.ShowHiddenFolders) Then
                Me._Root.Nodes.Add(ShellTreeView.MakeNode(CSI))
            End If
        Next
    End Sub

    ''' <summary>
    ''' Creates a TreeNode whose .Text is the DisplayName of the Shell Item.<br />
    ''' Sets the IconIndexes for that TreeNode from the Shell Item.<br />
    ''' Sets the Tag of the TreeNode to the Shell Item<br />
    ''' If the Shell Item (a Folder) has or may have sub-Folders (see Remarks), adds a Dummy node to
    '''   the TreeNode's .Nodes collection. This is always done if the input Shell Item represents a Removable device. Checking
    '''   further on such devices may cause unacceptable delays.
    ''' Returns the complete TreeNode.
    ''' </summary>
    ''' <param name="item">The Shell Item to make a TreeNode to represent.</param>
    ''' <returns>A TreeNode set up to represent the Shell Item.</returns>
    ''' <remarks>
    ''' This routine will not be called if the Shell Item (a Folder) is Hidden and ExpTree's ShowHidden Property is False.<br />
    ''' If the Folder is Hidden and ShowHidden is True, then this routine will be called.<br />
    ''' If the Folder is Hidden and it only contains Hidden Folders (files are not considered here), then, 
    ''' the HasSubFolders attribute may be returned False even though Hidden Folders exist. In that case, we 
    ''' must make an extra check to ensure that the TreeNode is expandable.<br />
    ''' 
    ''' There are additional complication with HasSubFolders. 
    ''' <ul>
    ''' <li>
    ''' On XP and earlier systems, HasSubFolders was always
    ''' returned True if the Folder was on a Remote system. On Vista and above, the OS would check and return an 
    ''' accurate value. This extra check can take a long time on Remote systems - approximately the same amount of time as checking
    ''' item.GetDirectories.Count. Versions 2.12 and above of ExpTreeLib have a modified HasSubFolders Property which will always
    ''' return True if the Folder is on a Remote system, restoring XP behavior.</li>
    ''' <li>
    ''' On XP and earlier systems, compressed files (.zip, .cab, etc) were treated as files. On Vista and above, they are treated
    ''' as Folders. ExpTreeLib continues to treat such files as files. The HasSubFolder attribute will report a Folder which
    ''' contains only compressed files as True. In MakeNode, I simply accept the Vista and above interpretation, setting a dummy
    ''' node in such a Folder. An attempt to expand such a TreeNode will just turn off the expansion marker.
    ''' </li>
    ''' </ul>
    ''' </remarks>
    Private Shared Function MakeNode(ByVal item As ShellItem) As TreeNode
        Dim newNode As New TreeNode(item.DisplayName) With {
            .Tag = item,
            .ImageIndex = SystemImageListManager.GetIconIndex(item, False),
            .SelectedImageIndex = SystemImageListManager.GetIconIndex(item, True),
            .Name = item.PIDL.ToString
        }
        If item.IsRemovable Then
            newNode.Nodes.Add(New TreeNode(" : "))
        ElseIf item.HasSubFolders Then
            newNode.Nodes.Add(New TreeNode(" : "))
        ElseIf item.IsHidden Then
            If item.GetDirectories.Count > 0 Then
                newNode.Nodes.Add(New TreeNode(" : "))
            End If
        End If
        Return newNode
    End Function

    Private Sub ClearTree()
        Me.Nodes.Clear()
        Me._Root = Nothing
    End Sub
#End Region

#Region " TREEVIEW BEFORE EXPAND EVENT "

    Protected Overrides Sub OnBeforeExpand(e As TreeViewCancelEventArgs)
        MyBase.OnBeforeExpand(e)
        Dim oldCursor As Cursor = Me.Cursor
        Me.Cursor = Cursors.WaitCursor
        If e.Node.Nodes.Count = 1 AndAlso e.Node.Nodes(0).Text.Equals(" : ") Then
            e.Node.Nodes.Clear()
            Dim CSI As ShellItem = TryCast(e.Node.Tag, ShellItem)
            Dim StTime As DateTime = Now()
            Dim D As ArrayList = CSI.GetDirectories()

            If D.Count > 0 Then
                D.Sort()    'uses the class comparer
                Dim item As ShellItem
                For Each item In D
                    If Not (item.IsHidden And Not Me.ShowHiddenFolders) Then
                        e.Node.Nodes.Add(MakeNode(item))
                    End If
                Next
            End If
            Debug.WriteLine("Expanding " & e.Node.Text & " " & Now().Subtract(StTime).TotalMilliseconds.ToString & "ms")
        Else    'Ensure content is accurate
            Me.RefreshNode(e.Node)
        End If
        Me.Cursor = oldCursor
    End Sub

#End Region

#Region " TREEVIEW AFTER SELECT EVENT "

    Protected Overrides Sub OnAfterSelect(e As TreeViewEventArgs)
        MyBase.OnAfterSelect(e)
        Dim node As TreeNode = e.Node
        Dim CSI As ShellItem = TryCast(e.Node.Tag, ShellItem)
        If CSI Is Me._Root.Tag AndAlso Not Me.ShowRootLines Then
            Try
                Me.BeginUpdate()
                Me.ShowRootLines = True
                Me.RefreshNode(node)
                Me.ShowRootLines = False
            Finally
                Me.EndUpdate()
            End Try
        Else
            Me.RefreshNode(node)
        End If
        If Me._EnableEventPost Then 'turned off during RefreshTree
            If CSI.Path.StartsWith(":") Then
                RaiseEvent TreeNodeSelected(Me, e) ' ExpTreeNodeSelected(CSI.DisplayName, CSI)
            Else
                RaiseEvent TreeNodeSelected(Me, e) ' RaiseEvent ExpTreeNodeSelected(CSI.Path, CSI)
            End If
        End If
    End Sub

#End Region

#Region " RefreshNode Sub"

    ''' <summary> Refresh node. </summary>
    ''' <param name="thisRoot"> this root. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub RefreshNode(ByVal thisRoot As TreeNode)
        If Not (thisRoot.Nodes.Count = 1 AndAlso thisRoot.Nodes(0).Text.Equals(" : ")) Then
            Dim thisItem As ShellItem = TryCast(thisRoot.Tag, ShellItem)
            If thisItem.RefreshDirectories Then   'RefreshDirectories True = the contained list of Directories has changed
                Dim curDirs As ArrayList = thisItem.GetDirectories(False) 'suppress 2nd refresh
                Dim delNodes As New ArrayList()
                Dim node As TreeNode
                For Each node In thisRoot.Nodes 'this is the old node contents
                    Dim i As Integer
                    For i = 0 To curDirs.Count - 1
                        If CType(curDirs(i), ShellItem).Equals(node.Tag) Then
                            curDirs.RemoveAt(i)   'found it, don't compare again
                            GoTo NXTOLD
                        End If
                    Next
                    'fall through = node no longer here
                    delNodes.Add(node)
NXTOLD:         Next
                If delNodes.Count + curDirs.Count > 0 Then  'had changes
                    Try
                        Me.BeginUpdate()
                        For Each node In delNodes 'dir not here anymore, delete node
                            thisRoot.Nodes.Remove(node)
                        Next
                        'any Shell Items remaining in curDirs is a new dir under thisRoot
                        Dim csi As ShellItem
                        For Each csi In curDirs
                            If Not (csi.IsHidden And Not Me.ShowHiddenFolders) Then
                                thisRoot.Nodes.Add(MakeNode(csi))
                            End If
                        Next
                        'we only need to resort if we added
                        'sort is based on Shell Item in .Tag
                        If curDirs.Count > 0 Then
                            Dim tmpA(thisRoot.Nodes.Count - 1) As TreeNode
                            thisRoot.Nodes.CopyTo(tmpA, 0)
                            Array.Sort(tmpA, New TagComparer())
                            thisRoot.Nodes.Clear()
                            thisRoot.Nodes.AddRange(tmpA)
                        End If
                    Catch ex As Exception
                        Debug.WriteLine($"Error in RefreshNode -- {ex}
{ex.StackTrace}")
                    Finally
                        Me.EndUpdate()
                    End Try
                End If
            End If
        End If
    End Sub

#End Region

#Region " TREEVIEW VISIBLE CHANGED EVENT "

    '''<Summary>When a form containing this control is Hidden and then re-Shown,
    ''' the association to the SystemImageList is lost.  Also lost is the
    ''' Expanded state of the various TreeNodes. 
    ''' The VisibleChanged Event occurs when the form is re-shown (and other times
    '''  as well).  
    ''' We re-establish the SystemImageList as the ImageList for the TreeView and
    ''' restore at least some of the Expansion.</Summary> 
    Protected Overrides Sub OnVisibleChanged(e As EventArgs)
        MyBase.OnVisibleChanged(e)
        If Me.Visible Then
            SystemImageListManager.SetTreeViewImageList(Me, False)
            If Not Me._Root Is Nothing Then
                Me._Root.Expand()
                If Not IsNothing(Me.SelectedNode) Then
                    Me.SelectedNode.Expand()
                Else
                    Me.SelectedNode = Me._Root
                End If
            End If
        End If
    End Sub

    '''<Summary>Should never occur since if the condition tested for is True,
    ''' the user should never be able to Collapse the node. However, it is
    ''' theoretically possible for the code to request a collapse of this node
    ''' If it occurs, cancel it</Summary>
    Private Sub HandleBeforeCollapse(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewCancelEventArgs) Handles Me.BeforeCollapse
        If Not Me.ShowRootLines AndAlso e.Node Is Me._Root Then
            e.Cancel = True
        End If
    End Sub

    Private Class SafeNativeMethods
        ''' <summary> Sets window theme. </summary>
        ''' <param name="hWnd">          The window. </param>
        ''' <param name="pszSubAppName"> Name of the sub application. </param>
        ''' <param name="pszSubIdList">  List of sub identifiers. </param>
        ''' <returns> An Integer. </returns>
        <DllImport("uxtheme.dll", CharSet:=CharSet.Unicode)>
        Public Shared Function SetWindowTheme(ByVal hWnd As IntPtr, ByVal pszSubAppName As String, ByVal pszSubIdList As String) As Integer
        End Function
    End Class

    ''' <summary>
    ''' Overrides <see cref="M:System.Windows.Forms.Control.OnHandleCreated(System.EventArgs)" />.
    ''' </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnHandleCreated(e As EventArgs)
        MyBase.OnHandleCreated(e)
        SafeNativeMethods.SetWindowTheme(Me.Handle, "explorer", Nothing)
    End Sub

    ''' <summary>
    ''' Overrides <see cref="M:System.Windows.Forms.Control.OnHandleDestroyed(System.EventArgs)" />.
    ''' </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnHandleDestroyed(e As EventArgs)
        MyBase.OnHandleDestroyed(e)
        Me.AllowDrop = False
    End Sub

#End Region

#Region " Find Ancestor Node "

    ''' <summary>
    ''' Given a Node Shell Item, find the TreeNode that belongs to the equivalent (matching PIDL)  Shell Item's
    ''' most immediate surviving ancestor. Note: referential comparison might not work since there is
    ''' no guarantee that the exact same Shell Item is stored in the tree.    
    ''' </summary>
    ''' <returns>
    ''' Me.Root if not found, otherwise the Tree Node whose .Tag is equivalent to the input Shell Item's
    ''' most immediate surviving ancestor.
    ''' </returns>
    Private Function FindAncestorNode(ByVal nodeShellItem As ShellItem) As TreeNode
        FindAncestorNode = Nothing
        If Not nodeShellItem.IsFolder Then Exit Function 'only folders in tree
        Dim baseNode As TreeNode = Me._Root
        Dim testNode As TreeNode

        Dim lim As Integer = PidlCount(nodeShellItem.PIDL) - PidlCount(TryCast(baseNode.Tag, ShellItem).PIDL)
        Do While lim > 1
            For Each testNode In baseNode.Nodes
                If ShellItem.IsAncestorOf(TryCast(testNode.Tag, ShellItem), nodeShellItem, False) Then
                    baseNode = testNode
                    baseNode.Expand()
                    lim -= 1
                    GoTo NEXTLEV
                End If
            Next
            'CSI's Ancestor may have moved or been deleted, return the last one
            ' found (if none, will return Me.Root)
            Return baseNode
NEXTLEV: Loop
        'on fall through, we have it
        Return baseNode
    End Function

    ''' <summary> Searches for the first node matching this shell item. </summary>
    ''' <param name="candidateAncestorNode"> The candidate ancestor node. </param>
    ''' <param name="lookupShellItem">       The node shell item to find. </param>
    ''' <returns> The found node. </returns>
    Public Function FindNode(ByVal candidateAncestorNode As TreeNode, ByVal lookupShellItem As ShellItem) As TreeNode
        Dim resultNode As TreeNode = Nothing
        For Each candidateNode As TreeNode In candidateAncestorNode.Nodes
            If candidateNode.Tag Is Nothing Then
            Else
                Dim candidateShellItem As ShellItem = TryCast(candidateNode.Tag, ShellItem)
                If candidateShellItem IsNot Nothing Then
                    If ShellItem.IsEqual(candidateShellItem.PIDL, lookupShellItem.PIDL) Then
                        resultNode = candidateNode
                    ElseIf candidateShellItem IsNot Nothing AndAlso ShellItem.IsAncestorOf(candidateShellItem, lookupShellItem, False) Then
                        candidateNode.Expand()
                        resultNode = Me.FindNode(candidateNode, lookupShellItem)
                    End If
                End If
            End If
            If resultNode IsNot Nothing Then Exit For
        Next
        Return resultNode
    End Function

    ''' <summary> Searches for the first node by iteration. </summary>
    ''' <param name="lookupShellItem"> The node shell item to find. </param>
    ''' <returns> The found node by iteration. </returns>
    Public Function FindNodeByIteration(ByVal lookupShellItem As ShellItem) As TreeNode
        Dim resultNode As TreeNode = Nothing
        For Each testNode As TreeNode In Me._Root.Nodes
            resultNode = Me.FindNode(testNode, lookupShellItem)
            If resultNode IsNot Nothing Then Exit For
        Next
        Return resultNode
    End Function

    ''' <summary> Searches for the first node by name. </summary>
    ''' <param name="lookupShellItem"> The node shell item to find. </param>
    ''' <returns> The found node by name. </returns>
    <Obsolete("Does not work.")>
    Public Function FindNodeByName(ByVal lookupShellItem As ShellItem) As TreeNode
        Dim resultNode As TreeNode = Nothing
        Dim foundNodes As TreeNode() = Me.Nodes.Find(lookupShellItem.PIDL.ToString, True)
        If foundNodes.Any Then
            resultNode = foundNodes.First
        End If
        Return resultNode
    End Function

    ''' <summary> Searches for the first node matching this shell item. </summary>
    ''' <param name="lookupShellItem"> The node shell item to find. </param>
    ''' <returns> The found node. </returns>
    Public Function FindNode(ByVal lookupShellItem As ShellItem) As TreeNode
        Dim resultNode As TreeNode = Me.FindNodeByIteration(lookupShellItem)
        Return resultNode
    End Function

#End Region

#Region " Drag/Drop From Tree Processing "

    Protected Overrides Sub OnItemDrag(e As ItemDragEventArgs)
        MyBase.OnItemDrag(e)
        'Primary (internal) data type
        Dim toDrag As New ArrayList()
        Dim csi As ShellItem = TryCast(CType(e.Item, TreeNode).Tag, ShellItem)
        toDrag.Add(csi)
        'also need Shell IDList Array
        Dim MS As System.IO.MemoryStream
        MS = ProcessDataObject.MakeShellIDArray(toDrag)
        'Fairly universal data type (must be an array)
        Dim strD(0) As String
        strD(0) = csi.Path
        'Build data to drag
        Dim dataObj As New DataObject()
        dataObj.SetData(toDrag)
        If Not IsNothing(MS) Then
            dataObj.SetData("Shell IDList Array", True, MS)
        End If
        dataObj.SetData("FileDrop", True, strD)
        'Do drag, allowing Copy and Move
        Dim ddeff As DragDropEffects
        ddeff = Me.DoDragDrop(dataObj, DragDropEffects.Copy Or DragDropEffects.Move)
        'the following line commented out, since we can't depend on Drag and Drop Effect
        'If Drag Drop Effect = DragDropEffects.None Then Exit Sub 'nothing happened
        Me.RefreshNode(Me.FindAncestorNode(csi))
    End Sub

#End Region

#Region " DragWrapper Event Handling"

    ' dropNode is the TreeNode that most recently was DraggedOver or
    '    Dropped onto.  
    Private _DropNode As TreeNode

    'expandNodeTimer is used to expand a node that is hovered over, with a delay
    Private WithEvents ExpandNodeTimer As New System.Windows.Forms.Timer()

    Private Sub ExpandNodeTimerTick(ByVal sender As Object, ByVal e As EventArgs) Handles ExpandNodeTimer.Tick
        Me.ExpandNodeTimer.Stop()
        If Not IsNothing(Me._DropNode) Then
            RemoveHandler Me.DragDropHandler.ShDragOver, AddressOf Me.DragDropHandlerShDragOver
            Try
                Me.BeginUpdate()
                Me._DropNode.Expand()
                Me._DropNode.EnsureVisible()
            Finally
                Me.EndUpdate()
            End Try
            AddHandler Me.DragDropHandler.ShDragOver, AddressOf Me.DragDropHandlerShDragOver
        End If
    End Sub


    '''<Summary>ShDragEnter does nothing. It is here for debug tracking</Summary>
    Private Sub DragDropHandlerShDragEnter(ByVal draglist As ArrayList, ByVal pDataObj As IntPtr,
                                        ByVal grfKeyState As Integer, ByVal pdwEffect As Integer) Handles DragDropHandler.ShDragEnter
    End Sub

    '''<Summary>Drag has left the control. Cleanup what we have to</Summary>
    Private Sub DragDropHandlerShDragLeave() Handles DragDropHandler.ShDragLeave
        Me.ExpandNodeTimer.Stop()    'shut off the dragging over nodes timer
        If Not IsNothing(Me._DropNode) Then
            Me.ResetTreeviewNodeColor(Me._DropNode)
        End If
        Me._DropNode = Nothing
    End Sub

    '''<Summary>ShDragOver manages the appearance of the TreeView.  Management of
    ''' the underlying FolderItem is done in DragWrapper
    ''' Credit to Cory Smith for TreeView colorizing technique and code,
    ''' at http://addressof.com/blog/archive/2004/955-10-01.aspx
    ''' Node expansion based on expandNodeTimer added by me.
    '''</Summary>
    Private Sub DragDropHandlerShDragOver(ByVal node As Object, ByVal pt As System.Drawing.Point,
                                       ByVal grfKeyState As Integer, ByVal pdwEffect As Integer) Handles DragDropHandler.ShDragOver
        Me.HandleDragWrapperShellDragOver(TryCast(node, TreeNode), pt)
    End Sub

    Private Sub HandleDragWrapperShellDragOver(ByVal node As TreeNode, ByVal pt As System.Drawing.Point)

        If IsNothing(node) Then  'clean up node stuff & fix color. Leave Drag info alone-cleaned up on DragLeave
            Me.ExpandNodeTimer.Stop()
            If Not Me._DropNode Is Nothing Then
                Me.ResetTreeviewNodeColor(Me._DropNode)
                Me._DropNode = Nothing
            End If
        Else  'Drag is Over a node - fix color & DragDropEffects
            If node Is Me._DropNode Then
                Exit Sub    'we've already done it all
            End If

            Me.ExpandNodeTimer.Stop() 'not over previous node anymore
            Try
                Me.BeginUpdate()
                Dim delta As Integer = Me.Height - pt.Y
                If delta < Me.Height / 2 And delta > 0 Then
                    If Not IsNothing(node) AndAlso Not (node.NextVisibleNode Is Nothing) Then
                        node.NextVisibleNode.EnsureVisible()
                    End If
                End If
                If delta > Me.Height / 2 And delta < Me.Height Then
                    If Not IsNothing(node) AndAlso Not (node.PrevVisibleNode Is Nothing) Then
                        node.PrevVisibleNode.EnsureVisible()
                    End If
                End If
                If Not node.BackColor.Equals(SystemColors.Highlight) Then
                    Me.ResetTreeviewNodeColor(Me.Nodes(0))
                    node.BackColor = SystemColors.Highlight
                    node.ForeColor = SystemColors.HighlightText
                End If
            Finally
                Me.EndUpdate()
            End Try
            Me._DropNode = node     'dropNode is the Saved Global version of Node
            If Not Me._DropNode.IsExpanded Then
                Me.ExpandNodeTimer.Interval = 1200
                Me.ExpandNodeTimer.Start()
            End If
        End If
    End Sub

    Private Sub DragWrapper_ShDragDrop(ByVal dragList As ArrayList, ByVal node As Object,
                                ByVal grfKeyState As Integer, ByVal pdwEffect As Integer) Handles DragDropHandler.ShDragDrop
        Me.ExpandNodeTimer.Stop()

        If Not IsNothing(Me._DropNode) Then
            Me.ResetTreeviewNodeColor(Me._DropNode)
        Else
            Me.ResetTreeviewNodeColor(Me.Nodes(0))
        End If
        ' If Directories were Moved, we must find and update the DragSource TreeNodes
        '  of course, it is possible that the Drag was external to the App and 
        '  the DragSource TreeNode might not exist in the Tree
        'All of this is somewhat chancy since we can't count on pdwEffect or
        '  on a Move having actually started, let alone finished
        Dim CSI As ShellItem      'that is what is in DragList
        For Each CSI In dragList
            If CSI.IsFolder Then    'only care about Folders
                Me.RefreshNode(Me.FindAncestorNode(CSI))
            End If
        Next
        If Me.SelectedNode Is Me._DropNode Then   'Fake a reselect
            Dim e As New System.Windows.Forms.TreeViewEventArgs(Me.SelectedNode, TreeViewAction.Unknown)
            Me.OnAfterSelect(e)      'will do a RefreshNode and raise AfterNodeSelect Event
        Else
            Me.RefreshNode(Me._DropNode)        'Otherwise, just refresh the Target
            If pdwEffect <> DragDropEffects.Copy AndAlso pdwEffect <> DragDropEffects.Link Then
                'it may have been a move. if so need to do an AfterSelect on the DragSource if it is SelectedNode
                If dragList.Count > 0 Then     'can't happen but check
                    If Not IsNothing(Me.SelectedNode) Then     'ditto
                        Dim csiSel As ShellItem = TryCast(Me.SelectedNode.Tag, ShellItem)
                        Dim csiSource As ShellItem = TryCast(dragList(0), ShellItem)  'assume all from same directory
                        If ShellItem.IsAncestorOf(csiSel, csiSource) Then 'also true for equality
                            Dim e As New System.Windows.Forms.TreeViewEventArgs(Me.SelectedNode, TreeViewAction.Unknown)
                            Me.OnAfterSelect(e)  'will do a RefreshNode and raise AfterNodeSelect Event
                        End If
                    End If
                End If
            End If
        End If
        Me._DropNode = Nothing
    End Sub

    Private Sub ResetTreeviewNodeColor(ByVal node As TreeNode)
        If Not node.BackColor.Equals(Color.Empty) Then
            node.BackColor = Color.Empty
            node.ForeColor = Color.Empty
        End If
        If Not node.FirstNode Is Nothing AndAlso node.IsExpanded Then
            Dim child As TreeNode
            For Each child In node.Nodes
                If Not child.BackColor.Equals(Color.Empty) Then
                    child.BackColor = Color.Empty
                    child.ForeColor = Color.Empty
                End If
                If Not child.FirstNode Is Nothing AndAlso child.IsExpanded Then
                    Me.ResetTreeviewNodeColor(child)
                End If
            Next
        End If
    End Sub

#End Region

#Region " TagComparer Class"

    '''<Summary> It is sometimes useful to sort a list of TreeNodes,
    ''' ListViewItems, or other objects in an order based on Shell Items in their Tag
    ''' use this <see cref="Icomparer"/> for that Sort</Summary>
    Friend Class TagComparer
        Implements IComparer
        Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements IComparer.Compare
            If x Is Nothing Then Throw New ArgumentNullException(NameOf(x))
            If y Is Nothing Then Throw New ArgumentNullException(NameOf(y))
            Dim xTreeNode As TreeNode = TryCast(x, TreeNode)
            If xTreeNode Is Nothing Then Throw New ArgumentNullException(NameOf(x), "X is not a tree node")
            Dim YTreeNode As TreeNode = TryCast(y, TreeNode)
            If YTreeNode Is Nothing Then Throw New ArgumentNullException(NameOf(x), "Y is not a tree node")

            Dim xShellItem As ShellItem = TryCast(xTreeNode.Tag, ShellItem)
            If xShellItem Is Nothing Then Throw New ArgumentNullException(NameOf(x), "X.Tag is not a shell item")

            Dim yShellItem As ShellItem = TryCast(YTreeNode.Tag, ShellItem)
            If yShellItem Is Nothing Then Throw New ArgumentNullException(NameOf(y), "Y.Tag is not a shell item")

            Return xShellItem.CompareTo(yShellItem)
        End Function
    End Class
#End Region

End Class

''' <summary> Values that represent shell special folders. </summary>
Public Enum ShellSpecialFolder As Integer
    <Description("Desktop")> Desktop = 0 ' CSIDL.DESKTOP
    <Description("Programs")> Programs = CSIDL.PROGRAMS
    <Description("Controls")> Controls = CSIDL.CONTROLS
    <Description("Printers")> Printers = CSIDL.PRINTERS
    <Description("Personal")> Personal = CSIDL.PERSONAL
    <Description("Favorites")> Favorites = CSIDL.FAVORITES
    <Description("Startup")> Startup = CSIDL.STARTUP
    <Description("Recent")> Recent = CSIDL.RECENT
    <Description("Send To")> SendTo = CSIDL.SENDTO
    <Description("Star tMenu")> StartMenu = CSIDL.STARTMENU
    <Description("My Documents")> MyDocuments = CSIDL.MYDOCUMENTS
    <Description("Desktop Directory")> DesktopDirectory = CSIDL.DESKTOPDIRECTORY
    <Description("My Computer")> MyComputer = CSIDL.DRIVES ' &H11
    <Description("My Network Places")> MyNetworkPlaces = CSIDL.NETWORK ' &H12
    <Description("Application Data")> ApplicatationData = CSIDL.APPDATA ' &H1A
    <Description("Internet Cache")> InternetCache = CSIDL.INTERNET_CACHE ' &H20
    <Description("Cookies")> Cookies = CSIDL.COOKIES ' &H21
    <Description("History")> History = CSIDL.HISTORY ' &H22
    <Description("Windows")> Windows = CSIDL.WINDOWS ' &H24
    <Description("System")> System = CSIDL.SYSTEM ' &H25
    <Description("Program Files")> ProgramFiles = CSIDL.PROGRAM_FILES ' &H26
    <Description("My Pictures")> MyPictures = CSIDL.MYPICTURES ' &H27
    <Description("Profile")> Profile = CSIDL.PROFILE ' &H28
    <Description("System x86")> SystemX86 = CSIDL.SYSTEMX86 ' &H29
    <Description("Program Files x86")> ProgramFilesX86 = CSIDL.PROGRAM_FILESX86 ' &H2A
    <Description("Common Files")> CommonFiles = CSIDL.PROGRAM_FILES_COMMON ' &H2B
    <Description("Common Files x86")> CommonFilesX86 = CSIDL.PROGRAM_FILES_COMMONX86 ' &H2C
    <Description("Admin Tools")> AdminTools = CSIDL.ADMINTOOLS ' &H30
End Enum

