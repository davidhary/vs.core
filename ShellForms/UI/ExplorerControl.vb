Imports System.Windows.Forms
Imports System.Threading
Imports isr.Core.Shell.ShellItem

''' <summary> A shell explorer control. </summary>
''' <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2019-07-08 </para></remarks>
Public Class ExplorerControl
    Inherits System.Windows.Forms.UserControl

#Region " CONSTRUCTION and CLEANUP "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        Me.InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        ' post vs 2005, the below is perfectly safe. It is not recommended as
        ' a general solution. Feel free to modify this demo to properly do
        ' cross thread modification of the ListView. 
        Control.CheckForIllegalCrossThreadCalls = False

        Me._ShellTreeView.RootItem = ShellItem.GetDeskTop

    End Sub

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso Me.components IsNot Nothing Then
                Me.components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " SHELL TREE VIEW EVENTS "

    Private Shared ReadOnly ManualResetEvent As New ManualResetEvent(True)

    ''' <summary> Event handler. Called by _ShellTreeView for tree node selected events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Tree view event information. </param>
    Private Sub ShellTreeViewTreeNodeSelected(ByVal sender As Object, ByVal e As TreeViewEventArgs) Handles _ShellTreeView.TreeNodeSelected
        Me.HandleTreeNodeChanged(e?.Node)
    End Sub

    ''' <summary> Event handler. Called by _ShellTreeView for tree node selected events. </summary>
    ''' <param name="node"> The node. </param>
    Private Sub HandleTreeNodeChanged(ByVal node As TreeNode)
        If node IsNot Nothing Then
            Dim CSI As ShellItem = TryCast(node.Tag, ShellItem)
            If CSI.Path.StartsWith(":", StringComparison.OrdinalIgnoreCase) Then
                Me.AfterNodeSelect(CSI.DisplayName, CSI)
            Else
                Me.AfterNodeSelect(CSI.Path, CSI)
            End If
        End If
    End Sub

    ''' <summary> After node select. </summary>
    ''' <param name="pathName">      Full pathname of the file. </param>
    ''' <param name="nodeShellItem"> The node shell item. </param>
    Private Sub AfterNodeSelect(ByVal pathName As String, ByVal nodeShellItem As ShellItem) ' Handles _ExplorerTree.ExpTreeNodeSelected
        Dim unused As New ArrayList()
        Dim files As New ArrayList()

        Dim directories As ArrayList
        If nodeShellItem.DisplayName.Equals(ShellItem.MyComputer) Then
            directories = nodeShellItem.GetDirectories ' avoid re-query since only has directories
        Else
            directories = nodeShellItem.GetDirectories
            files = nodeShellItem.GetFiles
        End If
        Me.SetUpComboBox(nodeShellItem)
        Dim totalItems As Integer = directories.Count + files.Count
        ManualResetEvent.WaitOne()
        If totalItems > 0 Then
            Me.Text = pathName
            Me._StatusBar.Text = $"{pathName}: {directories.Count} Directories; {files.Count} Files"
            Me._ShellListView.Populate(pathName, directories, files)
        Else
            Me._StatusBar.Text = $"{pathName} Has No Items"
        End If
    End Sub

#End Region

#Region " SHELL LIST VIEW EVENTS "

    ''' <summary> Gets or sets the change root on right mouse up. </summary>
    ''' <value> The change root on right mouse up. </value>
    Public Property ChangeRootOnRightMouseUp As Boolean

    ''' <summary> Gets or sets the expand node on left mouse up. </summary>
    ''' <value> The expand node on left mouse up. </value>
    Public Property ExpandNodeOnLeftMouseUp As Boolean

    Private Sub ShellListView_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles _ShellListView.MouseUp

        Dim selectedListViewItem As ListViewItem = Me._ShellListView.GetItemAt(e.X, e.Y)
        If selectedListViewItem Is Nothing Then Exit Sub
        If Me._ShellListView.SelectedItems Is Nothing OrElse Me._ShellListView.SelectedItems.Count < 1 Then Exit Sub
        Dim selectedShellItem As ShellItem = TryCast(Me._ShellListView.SelectedItems(0).Tag, ShellItem)
        If selectedShellItem?.IsFolder Then
            If e.Button = Windows.Forms.MouseButtons.Right Then
                If Me.ChangeRootOnRightMouseUp Then
                    ManualResetEvent.WaitOne()
                    Me.SetUpComboBox(selectedShellItem)
                    Me._ShellTreeView.RootItem = selectedShellItem
                End If
            ElseIf e.Button = Windows.Forms.MouseButtons.Left AndAlso Me.ExpandNodeOnLeftMouseUp Then
                ManualResetEvent.WaitOne()
                Me._ShellTreeView.ExpandNode(selectedShellItem, Me.SyncTreeWithListEnabled)
                ' this does not work: Me._ShellTreeView.ExpandANode(item)
            End If
        End If
    End Sub

    ''' <summary> Gets or sets the synchronization tree with list enabled. </summary>
    ''' <value> The synchronization tree with list enabled. </value>
    Public Property SyncTreeWithListEnabled As Boolean

    ''' <summary> Shell list view double click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ShellListView_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ShellListView.DoubleClick
        Dim selectedShellItem As ShellItem = TryCast(Me._ShellListView.SelectedItems(0).Tag, ShellItem)
        If selectedShellItem?.IsFolder Then
            ManualResetEvent.WaitOne()
            Me._ShellTreeView.ExpandNode(selectedShellItem, Me.SyncTreeWithListEnabled)
        End If
    End Sub

#End Region

#Region " HISTORY EVENTS "

    Private _BackupShellItems As ArrayList

    ''' <summary> Sets up the combo box. </summary>
    ''' <param name="item"> The item. </param>
    Private Sub SetUpComboBox(ByVal item As ShellItem)
        Me._BackupShellItems = New ArrayList()
        Me._HistoryComboBox.Items.Clear()
        Me._HistoryComboBox.Text = String.Empty
        Dim CSI As ShellItem = item
        Do While Not IsNothing(CSI.Parent)
            CSI = CSI.Parent
            Me._BackupShellItems.Add(CSI)
            Me._HistoryComboBox.Items.Add(CSI.DisplayName)
        Loop
        Me._HistoryComboBox.SelectedIndex = -1
        Me._ShellListView.Focus()
    End Sub

    Private Sub HistoryComboBox_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _HistoryComboBox.SelectedIndexChanged
        If Me._HistoryComboBox.SelectedIndex > -1 AndAlso Me._HistoryComboBox.SelectedIndex < Me._BackupShellItems.Count Then
            Dim item As ShellItem = TryCast(Me._BackupShellItems(Me._HistoryComboBox.SelectedIndex), ShellItem)
            Me._BackupShellItems = New ArrayList()
            Me._HistoryComboBox.Items.Clear()
            Me._ShellTreeView.RootItem = item
        End If
    End Sub

#End Region

#Region " VIEW MENU EVENT HANDLING "

    Private Sub ViewLargeIconsMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _LargeIconsToolStripMenuItem.Click
        Me._ShellListView.View = View.LargeIcon
    End Sub

    Private Sub ViewSmallIconsMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _SmallIconsToolStripMenuItem.Click
        Me._ShellListView.View = View.SmallIcon
    End Sub

    Private Sub ViewListMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ListToolStripMenuItem.Click
        Me._ShellListView.View = View.List
    End Sub

    Private Sub ViewDetailsMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _DetailsToolStripMenuItem.Click
        Me._ShellListView.View = View.Details
    End Sub

#End Region

#Region " FILE MENU EVENTS "

    Private Sub RefreshTreeMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _RefreshTreeToolStripMenuItem.Click
        If Me._ShellTreeView.SelectedItem IsNot Nothing Then Me._ShellTreeView.RefreshTree()
    End Sub

    Private Sub ChangeRootMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ChangeRootToolStripMenuItem.Click
        Me._ShellTreeView.RootItem = Me._ShellTreeView.SelectedItem
    End Sub

    Private Sub SetToDesktopMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _SetViewToDesktopToolStripMenuItem.Click
        Me._ShellTreeView.RootItem = ShellItem.GetDeskTop
    End Sub

    Private Sub SelectCDriverMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _SelectCDriveToolStripMenuItem.Click
        Dim cDriveShellItem As ShellItem = GetShellItem("C:\")
        If cDriveShellItem?.IsFolder Then Me._ShellTreeView.RootItem = cDriveShellItem
    End Sub

#End Region


End Class
