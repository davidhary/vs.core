Imports System.IO
Imports System.Windows.Forms
Imports System.Threading

''' <summary> A shell drag drop control. </summary>
''' <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2019-07-08 </para></remarks>
Public Class DragDropControl
    Inherits System.Windows.Forms.UserControl

#Region " CONSTRUCTION and CLEANUP "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        Me.InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        ' post vs 2005, the below is perfectly safe. It is not recommended as
        ' a general solution. Feel free to modify this demo to properly do
        ' cross thread modification of the ListView. 
        Windows.Forms.Control.CheckForIllegalCrossThreadCalls = False

        Me._ShellTreeView.RootItem = ShellItem.GetDeskTop

    End Sub

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso Me.components IsNot Nothing Then
                Me.components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " SHELL CONTROL EVENTS "

    Private Shared ReadOnly ManualResetEvent As New ManualResetEvent(True)

    ''' <summary> Event handler. Called by _ShellTreeView for tree node selected events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Tree view event information. </param>
    Private Sub ShellTreeViewTreeNodeSelected(ByVal sender As Object, ByVal e As TreeViewEventArgs) Handles _ShellTreeView.TreeNodeSelected
        Me.HandleTreeNodeChanged(e?.Node)
    End Sub

    ''' <summary> Event handler. Called by _ShellTreeView for tree node selected events. </summary>
    ''' <param name="node"> The node. </param>
    Private Sub HandleTreeNodeChanged(ByVal node As TreeNode)
        If node IsNot Nothing Then
            Dim CSI As ShellItem = TryCast(node.Tag, ShellItem)
            If CSI.Path.StartsWith(":", StringComparison.OrdinalIgnoreCase) Then
                Me.AfterNodeSelect(CSI.DisplayName, CSI)
            Else
                Me.AfterNodeSelect(CSI.Path, CSI)
            End If
        End If
    End Sub

    ''' <summary> After node select. </summary>
    ''' <param name="pathName">      Full pathname of the file. </param>
    ''' <param name="nodeShellItem"> The node shell item. </param>
    Private Sub AfterNodeSelect(ByVal pathName As String, ByVal nodeShellItem As ShellItem) ' Handles _ExplorerTree.ExpTreeNodeSelected
        Dim unused As New ArrayList()
        Dim fileList As New ArrayList()
        Dim TotalItems As Integer

        Dim dirList As ArrayList
        If nodeShellItem.DisplayName.Equals(ShellItem.MyComputer) Then
            dirList = nodeShellItem.GetDirectories 'avoid re-query since only has dirs
        Else
            dirList = nodeShellItem.GetDirectories
            fileList = nodeShellItem.GetFiles
        End If
        ManualResetEvent.WaitOne()
        TotalItems = dirList.Count + fileList.Count
        If TotalItems > 0 Then
            dirList.Sort()
            fileList.Sort()
            Me.Text = pathName
            Me._StatusBar.Text = $"{pathName}: {dirList.Count} Directories; {fileList.Count} Files"
            Me._ShellListView.Populate(pathName, dirList, fileList)
        Else
            Me._StatusBar.Text = $"{pathName} Has No Items"
        End If
    End Sub

#End Region

#Region " SHELL LIST VIEW EVENTS "

    ''' <summary> Gets or sets the synchronization tree with list enabled. </summary>
    ''' <value> The synchronization tree with list enabled. </value>
    Public Property SyncTreeWithListEnabled As Boolean

    ''' <summary> Shell list view double click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ShellListView_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ShellListView.DoubleClick
        Dim selectedShellItem As ShellItem = TryCast(Me._ShellListView.SelectedItems(0).Tag, ShellItem)
        If selectedShellItem?.IsFolder Then
            ManualResetEvent.WaitOne()
            Me._ShellTreeView.ExpandNode(selectedShellItem, Me.SyncTreeWithListEnabled)
        End If
    End Sub

#End Region

#Region " VIEW MENU EVENTS "

    Private Sub ViewLargeIconsMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _LargeIconsToolStripMenuItem.Click
        Me._ShellListView.View = View.LargeIcon
    End Sub

    Private Sub ViewSmallIconsMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _SmallIconsToolStripMenuItem.Click
        Me._ShellListView.View = View.SmallIcon
    End Sub

    Private Sub ViewListMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ListToolStripMenuItem.Click
        Me._ShellListView.View = View.List
    End Sub

    Private Sub ViewDetailsMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _DetailsToolStripMenuItem.Click
        Me._ShellListView.View = View.Details
    End Sub

#End Region

#Region " FILE MENU EVENTS "

    Private Sub RefreshTreeMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _RefreshTreeToolStripMenuItem.Click
        If Me._ShellTreeView.SelectedItem IsNot Nothing Then Me._ShellTreeView.RefreshTree()
    End Sub

    Private Sub ChangeRootMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ChangeRootToolStripMenuItem.Click
        Me._ShellTreeView.RootItem = Me._ShellTreeView.SelectedItem
    End Sub

    Private Sub SetToDesktopMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _SetViewToDesktopToolStripMenuItem.Click
        Me._ShellTreeView.RootItem = ShellItem.GetDeskTop
    End Sub

#End Region

#Region " DRAGGING "

    Private Sub ShellListView_ItemDrag(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemDragEventArgs) Handles _ShellListView.ItemDrag
        If Me._ShellListView.SelectedItems.Count > 0 Then
            Dim toDrag As New ArrayList()
            Dim lvItem As ListViewItem
            Dim strD(Me._ShellListView.SelectedItems.Count - 1) As String
            Dim i As Integer
            For Each lvItem In Me._ShellListView.SelectedItems
                toDrag.Add(lvItem.Tag)
                strD(i) = CType(lvItem.Tag, ShellItem).Path
                i += 1
            Next
            'NOTE: FileDrop allowing auto conversion will generate
            ' a Shell IDList Array on demand... but in some cases, the
            ' resultant PIDLs can be different from what we want, so
            ' do our own.
            Dim Dobj As New DataObject()
            Dim ms As MemoryStream
            ms = ProcessDataObject.MakeShellIDArray(toDrag)
            If Not ms Is Nothing Then
                Dobj.SetData("Shell IDList Array", True, ms)
            End If
            Dobj.SetData("FileDrop", True, strD)
            Dobj.SetData(toDrag)
            Dim dEff As DragDropEffects = If(e.Button = Windows.Forms.MouseButtons.Right,
                DragDropEffects.Copy Or DragDropEffects.Move Or DragDropEffects.Link,
                DragDropEffects.Copy Or DragDropEffects.Move)
            Dim res As DragDropEffects = Me._ShellListView.DoDragDrop(Dobj, dEff)
        End If
    End Sub

#End Region

#Region " DROPPING "

    ''' <summary> Drop on text box drag enter. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Drag event information. </param>
    Private Sub DropOnTextBox_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles _DropOnTextBox.DragEnter
        e.Effect = If(e.Data.GetDataPresent("FileDrop", True) And ((e.AllowedEffect And DragDropEffects.Copy) = DragDropEffects.Copy),
            DragDropEffects.Copy,
            DragDropEffects.None)
    End Sub

    ''' <summary> Drop on text box drag drop. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Drag event information. </param>
    Private Sub DropOnTextBox_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles _DropOnTextBox.DragDrop
        Dim names() As String = TryCast(e.Data.GetData("FileDrop", True), String())
        Dim builder As New System.Text.StringBuilder(Me._DropOnTextBox.Text)
        For Each entry As String In names
            builder.AppendLine(entry)
            ' _DropOnTextBox.Text += entry & vbCrLf
        Next
        Me._DropOnTextBox.Text = builder.ToString
        e.Effect = DragDropEffects.None
    End Sub

#End Region

End Class
