Imports System.Runtime.InteropServices
Imports System.Drawing
Imports isr.Core.Shell.ShellWrapper

''' <summary> A tree view drag wrapper. </summary>
''' <remarks> (c) 2012 James D. Parsells. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2019-07-06, 3.0.*. </para></remarks>
Public Class TreeViewDragWrapper
    Implements IDropTarget, IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructor. </summary>
    ''' <param name="treeView"> The tree view control. </param>
    Public Sub New(ByVal treeView As System.Windows.Forms.TreeView)
        Me._ViewControl = treeView
    End Sub

    Private _DisposedValue As Boolean = False

    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me._DisposedValue Then
                If disposing Then
                    If Me._MyDataObject IsNot Nothing Then Me._MyDataObject.Dispose()
                    Me._MyDataObject = Nothing
                End If
                Me.ResetPrevTarget()
            End If
        Finally
            Me._DisposedValue = True
        End Try
    End Sub

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    Public Sub Dispose() Implements IDisposable.Dispose
        Me.Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

#End Region

#Region " EVENTS "

    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly")>
    Public Event ShDragEnter(ByVal dragItemList As ArrayList, ByVal pDataObj As IntPtr, ByVal grfKeyState As Integer, ByVal pdwEffect As Integer)

    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly")>
    Public Event ShDragOver(ByVal node As Object, ByVal clientPoint As System.Drawing.Point, ByVal grfKeyState As Integer, ByVal pdwEffect As Integer)

    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly")>
    Public Event ShDragLeave()

    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly")>
    Public Event ShDragDrop(ByVal dragItemList As ArrayList, ByVal node As Object, ByVal grfKeyState As Integer, ByVal pdwEffect As Integer)

#End Region


#Region " Private Fields "
    Private ReadOnly _ViewControl As System.Windows.Forms.Control
    Private _OriginalEffect As Integer        'Save it
    Private _OriginalRefCount As Integer      'Set in DragEnter, used in DragDrop
    Private _DragDataObject As IntPtr         'Saved on DragEnter for use in DragOver
    Private _LastTarget As IDropTarget     'Of most recent Folder dragged over
    Private _LastNode As Object                'Most recent node dragged over
    Private _DropList As ArrayList             'Shell Items of Items dragged/dropped
    Private _MyDataObject As ProcessDataObject 'Does parsing of dragged IDataObject

#End Region

    Private Sub ResetPrevTarget()
        If Not IsNothing(Me._LastTarget) Then
            'ShowCnt("RSP Prior to LT.DragLeave")
            Dim hr As Integer = Me._LastTarget.DragLeave
            'ShowCnt("RSP After LT.DragLeave")
            Marshal.ReleaseComObject(Me._LastTarget)
            'ShowCnt("RSP After Release of LT")
            Me._LastTarget = Nothing
            Me._LastNode = Nothing
        End If
    End Sub

    Public Function DragEnter(ByVal pDataObj As IntPtr, ByVal grfKeyState As Integer,
                              ByVal pt As Point, ByRef pdwEffect As Integer) As Integer Implements IDropTarget.DragEnter
        Debug.WriteLine("In DragEnter: Effect = " & pdwEffect & " Keystate = " & grfKeyState)
        Me._OriginalEffect = pdwEffect
        Me._DragDataObject = pDataObj
        Me._OriginalRefCount = Marshal.AddRef(Me._DragDataObject)  'note: includes our count
        Debug.WriteLine("DragEnter: pDataObj RefCnt = " & Me._OriginalRefCount)

        Me._MyDataObject = New ProcessDataObject(pDataObj)

        If Me._MyDataObject.IsValid Then
            Me._DropList = Me._MyDataObject.DragList
            RaiseEvent ShDragEnter(Me._DropList, pDataObj, grfKeyState, pdwEffect)
        Else
            pdwEffect = System.Windows.Forms.DragDropEffects.None
        End If
        Return 0

    End Function

    'Private Sub ShowCnt(ByVal S As String)
    '    If Not IsNothing(m_DragData) Then
    '        Debug.WriteLine(S & " RefCnt = " & Marshal.AddRef(m_DragData) - 1)
    '        Marshal.Release(m_DragData)
    '    End If
    'End Sub

    ''' <summary> Drag over. </summary>
    ''' <param name="grfKeyState"> State of the grf key. </param>
    ''' <param name="pt">          The point. </param>
    ''' <param name="pdwEffect">   [in,out] The pdw effect. </param>
    ''' <returns> An Integer. </returns>
    Public Function DragOver(ByVal grfKeyState As Integer, ByVal pt As Point, ByRef pdwEffect As Integer) As Integer Implements IDropTarget.DragOver

        'Debug.WriteLine("In DragOver: Effect = " & pdwEffect & " Key state = " & grfKeyState)
        Dim tn As System.Windows.Forms.TreeNode
        Dim ptClient As System.Drawing.Point = Me._ViewControl.PointToClient(New System.Drawing.Point(pt.X, pt.Y))
        tn = CType(Me._ViewControl, System.Windows.Forms.TreeView).GetNodeAt(ptClient)
        If IsNothing(tn) Then  'not over a TreeNode
            Me.ResetPrevTarget()
        Else   'currently over Tree node
            If Not IsNothing(Me._LastNode) Then   'not the first, check if same
                If tn Is Me._LastNode Then
                    'Debug.WriteLine("DragOver: Same node")
                    Return 0        'all set up anyhow
                Else
                    Me.ResetPrevTarget()
                    Me._LastNode = tn
                End If
            Else    'is the first
                Me.ResetPrevTarget()   'just in case
                Me._LastNode = tn     'save current node
            End If

            'Drag is now over a new node with new capabilities

            Dim CSI As ShellItem = TryCast(tn.Tag, ShellItem)
            If CSI.IsDropTarget Then
                Me._LastTarget = TreeViewDragWrapper.GetDropTargetOf(CSI.Folder, Me._ViewControl)
                ' m_LastTarget = CSI.GetDropTargetOf(m_View)
                If Not IsNothing(Me._LastTarget) Then
                    pdwEffect = Me._OriginalEffect
                    'ShowCnt("Prior to LT.DragEnter")
                    Dim res As Integer = Me._LastTarget.DragEnter(Me._DragDataObject, grfKeyState, pt, pdwEffect)
                    If res = 0 Then
                        'ShowCnt("Prior to LT.DragOver")
                        res = Me._LastTarget.DragOver(grfKeyState, pt, pdwEffect)
                        'ShowCnt("After LT.DragOver")
                    End If
                    If res <> 0 Then
                        Marshal.ThrowExceptionForHR(res)
                    End If
                Else
                    pdwEffect = 0 'couldn't get IDropTarget, so report effect None
                End If
            Else
                pdwEffect = 0   'CSI not a drop target, so report effect None
            End If
            RaiseEvent ShDragOver(tn, ptClient, grfKeyState, pdwEffect)
        End If
        Return 0
    End Function

    ''' <summary> Get drop target of the given control. </summary>
    ''' <remarks>
    ''' Update 2011-01-28 - GetDropTargetOf replaced with version from 2.14 (slightly modified) to
    ''' eliminate a compiler Warning.
    ''' </remarks>
    ''' <param name="tn"> The tn. </param>
    ''' <returns> The drop target of. </returns>
    Public Shared Function GetDropTargetOf(ByVal folder As IShellFolder, ByVal tn As Windows.Forms.Control) As IDropTarget
        If folder Is Nothing Then Return Nothing
        Dim pInterface As IntPtr
        Dim theInterface As IDropTarget = Nothing
        Dim tnH As IntPtr = tn.Handle
        If folder.CreateViewObject(tnH, ShellWrapper.IID_IDropTarget, pInterface) = OkayValue Then
            theInterface = CType(Marshal.GetTypedObjectForIUnknown(pInterface, GetType(IDropTarget)), IDropTarget)
        End If
        Return theInterface
    End Function


    Public Function DragLeave() As Integer Implements IDropTarget.DragLeave
        'Debug.WriteLine("In DragLeave")
        Me._OriginalEffect = 0
        Me.ResetPrevTarget()
        Dim cnt As Integer = Marshal.Release(Me._DragDataObject)
        Debug.WriteLine($"DragLeave: N = {cnt}")
        Me._DragDataObject = IntPtr.Zero
        Me._OriginalRefCount = 0      'just in case
        Me._MyDataObject = Nothing
        RaiseEvent ShDragLeave()
        Return 0
    End Function

    Public Function DragDrop(ByVal pDataObj As IntPtr, ByVal grfKeyState As Integer, ByVal pt As Point, ByRef pdwEffect As Integer) As Integer Implements IDropTarget.DragDrop
        'Debug.WriteLine("In DragDrop: Effect = " & pdwEffect & " Key state = " & grfKeyState)
        Dim res As Integer
        If Not IsNothing(Me._LastTarget) Then
            res = Me._LastTarget.DragDrop(pDataObj, grfKeyState, pt, pdwEffect)
            'version 21 change 
            If res <> 0 AndAlso res <> 1 Then
                Debug.WriteLine("Error in dropping on DropTarget. res = " & Hex(res))
            End If 'No error on drop
            ' it is quite possible that the actual Drop has not completed.
            ' in fact it could be Canceled with nothing happening.
            ' All we are going to do is hope for the best
            ' The documented norm for Optimized Moves is pdwEffect=None, so leave it
            RaiseEvent ShDragDrop(Me._DropList, Me._LastNode, grfKeyState, pdwEffect)
        End If
        Me.ResetPrevTarget()
        Dim cnt As Integer = Marshal.Release(Me._DragDataObject)  'get rid of cnt added in DragEnter
        Me._DragDataObject = IntPtr.Zero
        Return 0
    End Function
End Class
