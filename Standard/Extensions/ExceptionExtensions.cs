using System;
using System.Collections;
using System.Text;

#pragma warning disable IDE1006 // Naming Styles
namespace isr.Core.Standard.ExceptionExtensions
#pragma warning restore IDE1006 // Naming Styles
{
    /// <summary> Includes extensions for <see cref="Exception">Exceptions</see>. </summary>
    /// <license> (c) 2017 Marco Bertschi.<para>
    /// Licensed under The MIT License. </para><para>
    /// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    /// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    /// NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    /// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    /// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    /// </para> </license>
    /// <history date="2017-03-30" by="David" revision="3.1.6298.x">
    /// https://www.codeproject.com/script/Membership/View.aspx?mid=8888914
    /// https://www.codeproject.com/Tips/1179564/A-Quick-Dirty-Extension-Method-to-Get-the-Full-Exc
    /// </history>
    public static partial class ExceptionExtensionMethods
    {

        # region " TO FULL BLOWN STRING "

        /// <summary> Converts this object to a full blown string. </summary>
        /// <param name="value">  The value. </param>
        /// <param name="addData"> Adds exception data. </param>
        /// <returns> The given data converted to a String. </returns>
        public static string ToFullBlownString( Exception value, Func<Exception, bool> addData )
        {
            return ToFullBlownString( value, int.MaxValue, addData );
        }

        /// <summary> Converts this object to a full blown string. </summary>
        /// <param name="value">  The value. </param>
        /// <param name="level">  The level. </param>
        /// <param name="addData"> Adds exception data. </param>
        /// <returns> The given data converted to a String. </returns>
        public static string ToFullBlownString( Exception value, int level, Func<Exception, bool> addData )
        {
            var builder = new StringBuilder();
            int counter = 1;
            _ = builder.AppendLine();
            while ( value is object && counter <= level )
            {
                if ( addData is object )
                {
                    _ = addData( value );
                }

                AppendFullBlownString( builder, value, counter );
                value = value.InnerException;
                counter += 1;
            }

            return builder.ToString().TrimEnd( Environment.NewLine.ToCharArray() );
        }

        /// <summary> Appends a full blown string. </summary>
        /// <param name="builder"> The builder. </param>
        /// <param name="value">   The value. </param>
        /// <param name="prefix">  The prefix. </param>
        private static void AppendFullBlownString( StringBuilder builder, Exception value, string prefix )
        {
            if ( value is null )
            {
                throw new ArgumentNullException( nameof( value ) );
            }

            if ( builder is null )
            {
                throw new ArgumentNullException( nameof( builder ) );
            }

            _ = builder.AppendLine( $"{prefix}Type: {value.GetType()}" );
            if ( !string.IsNullOrEmpty( value.Message ) )
            {
                _ = builder.AppendLine( $"{prefix}Message: {value.Message}" );
            }

            if ( !string.IsNullOrEmpty( value.Source ) )
            {
                _ = builder.AppendLine( $"{prefix}Source: {value.Source}" );
            }

            if ( value.TargetSite is object )
            {
                _ = builder.AppendLine( $"{prefix}Method: {value.TargetSite}" );
            }

            string userCallStack = value.StackTrace.ParseCallStack( 0, 0, prefix, true );
            if ( !string.IsNullOrEmpty( userCallStack ) )
            {
                _ = builder.AppendLine( userCallStack );
            }

            if ( value.Data is object )
            {
                foreach ( DictionaryEntry keyValuePair in value.Data )
                {
                    _ = builder.AppendLine( $"{prefix} Data: {keyValuePair.Key}: {keyValuePair.Value}" );
                }
            }
        }

        /// <summary> Appends a full blown string. </summary>
        /// <param name="builder"> The builder. </param>
        /// <param name="value">   The value. </param>
        /// <param name="counter"> The counter. </param>
        private static void AppendFullBlownString( StringBuilder builder, Exception value, int counter )
        {
            AppendFullBlownString( builder, value, $"{counter}-> " );
        }

        #endregion

        #region " CALL STACK PARSING "

        private static readonly string[] FrameworkLocations = new string[] { "at System", "at Microsoft" };

        /// <summary> Returns true if the line is a framework line. </summary>
        /// <param name="value">        The value. </param>
        /// <param name="locationName"> [in,out] Name of the location. </param>
        /// <returns> <c>True</c> if the line is a framework line; otherwise, <c>False</c>. </returns>
        public static bool IsFrameworkLine( this string value, ref string locationName )
        {
            foreach ( var location in FrameworkLocations )
            {
                if ( !string.IsNullOrWhiteSpace( value ) && value.TrimStart().StartsWith( location, StringComparison.OrdinalIgnoreCase ) )
                {
                    locationName = location;
                    return true;
                }
            }

            return false;
        }

        /// <summary> Parses the stack trace and returns the user or full call stack. </summary>
        /// <param name="stackTrace">    Normally formatted Stack Trace. </param>
        /// <param name="skipLines">     Specifies the number of lines to skip. If getting the stack trace
        /// from the <see cref="Environment.StackTrace">environment</see>, the first two lines include
        /// the environment methods. </param>
        /// <param name="totalLines">    Specifies the maximum number of lines to include in the trace.
        /// Use 0 to include all lines. </param>
        /// <returns> The full or user call stacks. </returns>
        public static string ParseCallStack( this string stackTrace, int skipLines, int totalLines, string prefix, bool userCallStack )
        {
            var userCallStackBuilder = new StringBuilder( 0xFFFF );
            var fullCallStackBuilder = new StringBuilder( 0xFFFF );
            if ( string.IsNullOrWhiteSpace( stackTrace ) )
            {
                return string.Empty;
            }

            var callStack = stackTrace.Split( Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries );
            if ( callStack.Length <= 0 )
            {
                return string.Empty;
            }

            bool isFrameworkBlock = false;
            int lineNumber = 0;
            int lineCount = 0;
            foreach ( string line in callStack )
            {
                if ( totalLines == 0 || lineCount <= totalLines )
                {
                    lineNumber += 1;
                    if ( lineNumber > skipLines )
                    {
                        string locationName = string.Empty;
                        string searchFor = " in ";
                        string stackLine = $"{prefix}{line.Replace( searchFor, $"{Environment.NewLine}{prefix}  {searchFor}" )}";
                        if ( line.IsFrameworkLine( ref locationName ) )
                        {
                            isFrameworkBlock = true;
                            _ = fullCallStackBuilder.AppendLine( stackLine );
                        }
                        else
                        {
                            if ( isFrameworkBlock && !string.IsNullOrWhiteSpace( locationName ) )
                            {
                                // if previously had external code, append
                                _ = userCallStackBuilder.AppendLine( $"{prefix}{locationName}" );
                            }

                            isFrameworkBlock = false;
                            _ = fullCallStackBuilder.AppendLine( stackLine );
                            _ = userCallStackBuilder.AppendLine( stackLine );
                            lineCount += 1;
                        }
                    }
                }
            }

            return userCallStack ? userCallStackBuilder.ToString().TrimEnd( Environment.NewLine.ToCharArray() ) : fullCallStackBuilder.ToString().TrimEnd( Environment.NewLine.ToCharArray() );
        }

        #endregion

    }
}
