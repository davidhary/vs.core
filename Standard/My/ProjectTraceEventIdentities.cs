﻿using System.ComponentModel;

namespace isr.Core.Standard.My
{

    /// <summary> Values that represent project trace event identifiers. </summary>
    public enum ProjectTraceEventId
    {
        [Description( "Not specified" )]
        None,
        [Description( "Standard" )]
        Standard = 0x10 * isr.TraceEventConstants.SmallSolutionNamespaceSize,
        [Description( "Present" )]
        Present = Standard + 0x1,
        [Description( "Tableaux" )]
        Tableaux = Standard + 0x2,
        [Description( "Forma" )]
        Forma = Standard + 0x3,
        [Description( "Controls" )]
        Controls = Standard + 0x4,
        [Description( "Models" )]
        Models = Standard + 0x5,
        [Description( "Constructs" )]
        Constructs = Standard + 0x6,
        [Description( "Diagnosis Tester" )]
        DiagnosisTester = Standard + 0x10,
        [Description( "My Blue Splash Screen" )]
        MyBlueSplashScreen = Standard + 0x11,
        [Description( "Exception Message Test" )]
        ExceptionMessageTest = Standard + 0x12,
        [Description( "My Exception Message Box Test" )]
        MyExceptionMessageBoxTest = Standard + 0x13,
        [Description( "Relic" )]
        Relic = Standard + 0xFF,
        [Description( "Automata" )]
        Automata = 0x20 * isr.TraceEventConstants.SmallSolutionNamespaceSize,
        [Description( "Data" )]
        Data = 0x21 * isr.TraceEventConstants.SmallSolutionNamespaceSize,
        [Description( "Digital I/O LAN" )]
        DigitalInputOutputLan = 0x22 * isr.TraceEventConstants.SmallSolutionNamespaceSize,
        [Description( "High Potential" )]
        HighPotential = 0x23 * isr.TraceEventConstants.SmallSolutionNamespaceSize,
        [Description( "IO" )]
        IO = 0x24 * isr.TraceEventConstants.SmallSolutionNamespaceSize,
        [Description( "Modbus" )]
        Modbus = 0x25 * isr.TraceEventConstants.SmallSolutionNamespaceSize,
        [Description( "Net" )]
        Net = 0x26 * isr.TraceEventConstants.SmallSolutionNamespaceSize,
        [Description( "Rich Text Box" )]
        RichTextBox = 0x27 * isr.TraceEventConstants.SmallSolutionNamespaceSize,
        [Description( "Open Layers" )]
        OpenLayers = 0x28 * isr.TraceEventConstants.SmallSolutionNamespaceSize,
        [Description( "Optima" )]
        Optima = 0x29 * isr.TraceEventConstants.SmallSolutionNamespaceSize,
        [Description( "Serial" )]
        Serial = 0x2A * isr.TraceEventConstants.SmallSolutionNamespaceSize,
        [Description( "Signals" )]
        Signals = 0x2B * isr.TraceEventConstants.SmallSolutionNamespaceSize,
        [Description( "Thermal Transient" )]
        ThermalTransient = 0x2C * isr.TraceEventConstants.SmallSolutionNamespaceSize,
        [Description( "Universal Library" )]
        UniversalLibrary = 0x2D * isr.TraceEventConstants.SmallSolutionNamespaceSize,
        [Description( "Visualizing" )]
        Visualizing = 0x2E * isr.TraceEventConstants.SmallSolutionNamespaceSize,
        [Description( "Wisdom" )]
        Wisdom = 0x2F * isr.TraceEventConstants.SmallSolutionNamespaceSize,
        [Description( "Initial State Client" )]
        InitialStateClient = 0x30 * isr.TraceEventConstants.SmallSolutionNamespaceSize,
        [Description( "Virtual Instruments" )]
        VirtualInstruments = 0x10 * isr.TraceEventConstants.SolutionNamespaceSize,
        [Description( "Application Namespace Identity" )]
        ApplicationNamespace = isr.TraceEventConstants.ApplicationNamespaceIdentity
    }
}