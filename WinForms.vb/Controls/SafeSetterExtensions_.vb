Imports System.Drawing
Imports System.Runtime.CompilerServices

Namespace ControlExtensions

    Partial Public Module Methods

        ''' <summary> Safe setter. </summary>
        ''' <remarks> David, 7/27/2020. </remarks>
        ''' <param name="control"> The control to enable or disable. </param>
        ''' <param name="setter">  The setter. </param>
        <Extension()>
        Public Sub SafeSetter(ByVal control As Control, ByVal setter As Action)
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of Control, Action)(AddressOf Methods.SafeSetter), New Object() {control, setter})
                ElseIf control.IsHandleCreated Then
                    setter.Invoke
                End If
            End If
        End Sub

        ''' <summary> Safe setter. </summary>
        ''' <remarks> David, 7/27/2020. </remarks>
        ''' <param name="control"> The control to enable or disable. </param>
        ''' <param name="setter">  The setter. </param>
        <Extension()>
        Public Sub SafeSetter(ByVal control As StatusBarPanel, ByVal setter As Action)
            If control IsNot Nothing AndAlso control.Parent IsNot Nothing Then
                If control.Parent.InvokeRequired Then
                    control.Parent.Invoke(New Action(Of Control, Action)(AddressOf Methods.SafeSetter), New Object() {control, setter})
                ElseIf control.Parent.IsHandleCreated Then
                    setter.Invoke
                End If
            End If
        End Sub

        ''' <summary> Safe setter. </summary>
        ''' <remarks> David, 7/27/2020. </remarks>
        ''' <param name="control"> The control to enable or disable. </param>
        ''' <param name="setter">  The setter. </param>
        <Extension()>
        Public Sub SafeSetter(ByVal control As ToolBarButton, ByVal setter As Action)
            If control IsNot Nothing AndAlso control.Parent IsNot Nothing Then
                If control.Parent.InvokeRequired Then
                    control.Parent.Invoke(New Action(Of Control, Action)(AddressOf Methods.SafeSetter), New Object() {control, setter})
                ElseIf control.Parent.IsHandleCreated Then
                    setter.Invoke
                End If
            End If
        End Sub

        ''' <summary> Safe setter. </summary>
        ''' <remarks> David, 7/27/2020. </remarks>
        ''' <param name="control"> The control to enable or disable. </param>
        ''' <param name="setter">  The setter. </param>
        <Extension()>
        Public Sub SafeSetter(ByVal control As ToolStripItem, ByVal setter As Action)
            If control IsNot Nothing AndAlso control.Owner IsNot Nothing Then
                If control.Owner.InvokeRequired Then
                    control.Owner.Invoke(New Action(Of Control, Action)(AddressOf Methods.SafeSetter), New Object() {control, setter})
                ElseIf control.Owner.IsHandleCreated Then
                    setter.Invoke
                End If
            End If
        End Sub

    End Module
End Namespace
