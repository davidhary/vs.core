Imports System.Runtime.CompilerServices
Namespace ControlContainerExtensions
    ''' <summary> Includes extensions for <see cref="Control"/>  and <see cref="ContainerControl">container control</see>. </summary>
    ''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 11/19/2010, 1.2.3975.x. </para></remarks>
    Public Module Methods

        ''' <summary> Validates the specified control. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="value">     Control to be validated. </param>
        ''' <param name="container"> specifies the instance of the container control which controls are to
        '''                          be validated. </param>
        ''' <returns>
        ''' Returns false if failed to validate. Returns true if control validated or not visible.
        ''' </returns>
        <Extension()>
        Public Function Validate(ByVal value As Control, ByVal container As System.Windows.Forms.ContainerControl) As Boolean

            If value Is Nothing OrElse container Is Nothing Then Return True
            ' focus on and validate the control
            If container.Visible AndAlso value.Visible Then
                value.Focus()
                Return container.Validate
            Else
                Return True
            End If

        End Function

        ''' <summary> Validates all controls in the container control. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="container"> specifies the instance of the container control which controls are to
        '''                          be validated. </param>
        ''' <returns> Returns false if any control failed to validated. </returns>
        <Extension()>
        Public Function ValidateControls(ByVal container As System.Windows.Forms.ContainerControl) As Boolean
            If container Is Nothing Then Return True
            ValidateControls = True
            Dim controls As System.Windows.Forms.Control() = ControlCollectionExtensions.RetrieveControls(container.Controls)
            For i As Integer = 0 To controls.Length - 1
                ' validate the selected control
                If Not controls(i).Validate(container) Then
                    ValidateControls = False
                End If
            Next

        End Function

    End Module
End Namespace
