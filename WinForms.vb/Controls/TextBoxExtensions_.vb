Imports System.Runtime.CompilerServices
Namespace TextBoxExtensions
    ''' <summary> Includes extensions for <see cref="TextBox">Text Box</see>. </summary>
    ''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 11/19/2010, 1.2.3975.x. </para></remarks>
    Public Module Methods

        ''' <summary>
        ''' Appends <paramref name="value">text</paramref> to the
        ''' <see cref="TextBoxBase">control</see>.
        ''' This setter is thread safe.
        ''' </summary>
        ''' <remarks> The value is set to empty if null or empty. </remarks>
        ''' <param name="control"> The <see cref="TextBoxBase"/> control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function Append(ByVal control As System.Windows.Forms.TextBoxBase, ByVal value As String) As String
            If control IsNot Nothing Then
                If String.IsNullOrWhiteSpace(value) Then
                    value = String.Empty
                Else
                    If control.InvokeRequired Then
                        control.Invoke(New Action(Of TextBoxBase, String)(AddressOf TextBoxExtensions.Append), New Object() {control, value})
                    Else
                        control.SelectionStart = control.Text.Length
                        control.SelectionLength = 0
                        control.SelectedText = If(control.SelectionStart = 0, value, value)
                        control.SelectionStart = control.Text.Length
                    End If
                End If
            End If
            Return value
        End Function

        ''' <summary>
        ''' Appends formatted text of the <see cref="TextBoxBase">control</see>. This setter is thread
        ''' safe.
        ''' </summary>
        ''' <remarks> The value is set to empty if null or empty. </remarks>
        ''' <param name="control"> The <see cref="TextBoxBase"/> control. </param>
        ''' <param name="format">  The text format. </param>
        ''' <param name="args">    The format arguments. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function Append(ByVal control As System.Windows.Forms.TextBoxBase, ByVal format As String, ByVal ParamArray args() As Object) As String
            Return Append(control, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
        End Function

        ''' <summary>
        ''' Prepends <paramref name="value">text</paramref> to the
        ''' <see cref="TextBoxBase">control</see>.
        ''' This setter is thread safe.
        ''' </summary>
        ''' <remarks> The value is set to empty if null or empty. </remarks>
        ''' <param name="control"> The <see cref="TextBoxBase"/> control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function Prepend(ByVal control As System.Windows.Forms.TextBoxBase, ByVal value As String) As String
            If control IsNot Nothing Then
                If String.IsNullOrWhiteSpace(value) Then
                    value = String.Empty
                Else
                    If control.InvokeRequired Then
                        control.Invoke(New Action(Of TextBoxBase, String)(AddressOf TextBoxExtensions.Append), New Object() {control, value})
                    Else
                        control.SelectionStart = 0
                        control.SelectionLength = 0
                        control.SelectedText = value
                        control.SelectionLength = 0
                    End If
                End If
            End If
            Return value
        End Function

        ''' <summary> Prepends the formatted text. This setter is thread safe. </summary>
        ''' <remarks> The value is set to empty if null or empty. </remarks>
        ''' <param name="control"> The <see cref="TextBoxBase"/> control. </param>
        ''' <param name="format">  The text format. </param>
        ''' <param name="args">    The format arguments. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function Prepend(ByVal control As System.Windows.Forms.TextBoxBase, ByVal format As String, ByVal ParamArray args() As Object) As String
            Return Prepend(control, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
        End Function

    End Module
End Namespace
