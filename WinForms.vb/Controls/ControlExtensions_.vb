Imports System.Drawing
Imports System.Runtime.CompilerServices

Namespace ControlExtensions
    ''' <summary> Includes extensions for controls. </summary>
    ''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 11/19/2010, 1.2.3975.x. </para></remarks>
    Public Module Methods

#Region " COPY PROPERTIES "

        ''' <summary> Copies properties from a source to a destination controls. </summary>
        ''' <remarks> Use this method to copy the properties of a control to a new control. </remarks>
        ''' <param name="source">      specifies an instance of the control which properties are copied
        '''                            from. </param>
        ''' <param name="destination"> specifies an instance of the control which properties are copied
        '''                            to. </param>
        ''' <example>
        ''' This example adds a button to a form.
        ''' <code>
        ''' Private buttons As New ArrayList()
        ''' Private Sub AddButton(ByVal sourceButton As Button)
        ''' Dim newButton As New Button()
        '''     WinFormsSupport.CopyControlProperties(sourceButton, newButton)
        '''     Me.Controls.Add(newButton)
        '''     AddHandler newButton.Click, AddressOf Me.ExitButton_Click
        '''     newButton.Name = String.Format( System.Globalization.CultureInfo.CurrentCulture, "newButton {0}", buttons.count.toString)
        '''     newButton.Text = newButton.Name
        '''     newButton.Top = newButton.Top + newButton.Height * (buttons.Count + 1)
        '''     buttons.Add(newButton)
        ''' End Sub
        ''' </code>
        ''' To run this example, paste the code fragment into the method region of a Visual Basic form.
        ''' Run the program by pressing F5.
        ''' </example>
        <Extension()>
        Public Sub CopyControlProperties(ByVal source As System.Windows.Forms.Control, ByVal destination As System.Windows.Forms.Control)

            If source Is Nothing OrElse destination Is Nothing Then Return
            Dim dontCopyNames() As String = {"WindowTarget"}
            Dim pinfos(), pinfo As System.Reflection.PropertyInfo
            If Not (source Is Nothing OrElse destination Is Nothing) Then
                pinfos = source.GetType.GetProperties()
                For Each pinfo In pinfos
                    ' copy only those properties without parameters - you'll rarely need indexed properties
                    If pinfo.GetIndexParameters().Length = 0 Then
                        ' skip properties that appear in the list of disallowed names.
                        If Array.IndexOf(dontCopyNames, pinfo.Name) < 0 Then
                            ' can only copy those properties that can be read and written.  
                            If pinfo.CanRead And pinfo.CanWrite Then
                                ' set the destination based on the source.
                                pinfo.SetValue(destination, pinfo.GetValue(source, Nothing), Nothing)
                            End If
                        End If
                    End If
                Next
            End If

        End Sub

#End Region

#Region " READ ONLY TAB STOPS "

        ''' <summary> Disables tab stops on read only text box. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="controls"> The controls. </param>
        <Extension()>
        Public Sub DisableReadOnlyTabStop(ByVal controls As System.Windows.Forms.Control.ControlCollection)
            If controls IsNot Nothing Then
                For Each tb As System.Windows.Forms.TextBoxBase In controls.OfType(Of System.Windows.Forms.TextBoxBase)()
                    tb.TabStop = Not tb.ReadOnly
                Next
            End If
        End Sub

        ''' <summary> Disables tab stops on read only text box. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="control"> The control to enable or disable. </param>
        <Extension()>
        Public Sub DisableReadOnlyTabStop(ByVal control As System.Windows.Forms.Control)
            If control IsNot Nothing AndAlso control.Controls IsNot Nothing Then
                control.Controls.DisableReadOnlyTabStop()
                For Each c As System.Windows.Forms.Control In control.Controls
                    c.DisableReadOnlyTabStop()
                Next
            End If
        End Sub

#End Region

#Region " ENABLE CONTROLS "

        ''' <summary> Recursively enables or disabled the control and all its children . </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="control"> The control to enable or disable. </param>
        ''' <param name="value">   Specifies a value to set. </param>
        <Extension()>
        Public Sub RecursivelyEnable(ByVal control As System.Windows.Forms.Control, ByVal value As Boolean)
            If control IsNot Nothing Then
                control.Enabled = value
                control.Controls.RecursivelyEnable(value)
            End If
        End Sub

        ''' <summary> Recursively enables or disabled the control and all its children . </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="controls"> The controls. </param>
        ''' <param name="value">    Specifies a value to set. </param>
        <Extension()>
        Public Sub RecursivelyEnable(ByVal controls As System.Windows.Forms.Control.ControlCollection, ByVal value As Boolean)
            If controls IsNot Nothing Then
                For Each c As System.Windows.Forms.Control In controls
                    c.RecursivelyEnable(value)
                Next
            End If
        End Sub

#End Region

#Region " MEASURE "

        ''' <summary> Measure text. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="control"> The control to enable or disable. </param>
        ''' <param name="text">    The text. </param>
        ''' <returns> A Drawing.SizeF. </returns>
        <Extension()>
        Public Function MeasureText(ByVal control As System.Windows.Forms.Control, ByVal text As String) As Drawing.SizeF
            If control Is Nothing Then Throw New ArgumentNullException(NameOf(control))
            Using graphics As System.Drawing.Graphics = control.CreateGraphics()
                Return graphics.MeasureString(text, control.Font)
            End Using
        End Function

        ''' <summary> Measure text. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="font"> The font. </param>
        ''' <param name="text"> The text. </param>
        ''' <returns> A Drawing.SizeF. </returns>
        <Extension()>
        Public Function MeasureText(ByVal font As Font, ByVal text As String) As Drawing.SizeF
            If font Is Nothing Then Throw New ArgumentNullException(NameOf(font))
            Using ctrl As New Control
                Using graphics As System.Drawing.Graphics = ctrl.CreateGraphics()
                    Return graphics.MeasureString(text, font)
                End Using
            End Using
        End Function

#End Region

#Region " TEXT "

        ''' <summary>
        ''' Sets the <see cref="TextBox">control</see> text to the
        ''' <paramref name="value">value</paramref>.
        ''' The control is disabled when set so that the handling of the changed event can be skipped.
        ''' </summary>
        ''' <remarks> The value is set to empty if null or empty. </remarks>
        ''' <param name="control"> The control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SilentTextSetter(ByVal control As Control, ByVal value As String) As String
            If control IsNot Nothing Then
                If String.IsNullOrWhiteSpace(value) Then
                    value = String.Empty
                End If
                Dim enabled As Boolean = control.Enabled
                control.Enabled = False
                control.Text = value
                control.Enabled = enabled
            End If
            Return value
        End Function

        ''' <summary>
        ''' Sets the <see cref="TextBox">control</see> text to the formatted text. The control is
        ''' disabled when set so that the handling of the changed event can be skipped.
        ''' </summary>
        ''' <remarks> The value is set to empty if null or empty. </remarks>
        ''' <param name="control"> The control. </param>
        ''' <param name="format">  The text format. </param>
        ''' <param name="args">    The format arguments. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SilentTextSetter(ByVal control As Control, ByVal format As String, ByVal ParamArray args() As Object) As String
            Return SilentTextSetter(control, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
        End Function

#End Region

#Region " DRAW TEXT "

        ''' <summary>
        ''' Draws text into a  <see cref="System.Windows.Forms.Control">windows control</see>
        ''' center middle using the control font.
        ''' </summary>
        ''' <remarks>
        ''' Call this method after changing the progress bar's value. If for some reason, the changing of
        ''' the progress bar's value doesn't refresh it and clear the previously drawn text, call the
        ''' Refresh method of the progress bar before calling this method.
        ''' </remarks>
        ''' <param name="control"> The target progress bar to add text into. </param>
        ''' <param name="value">   The text to add into the progress bar. Leave null or empty to
        '''                        automatically add the percent. </param>
        <Extension()>
        Public Sub DrawText(ByVal control As System.Windows.Forms.Control, ByVal value As String)
            If control IsNot Nothing Then
                DrawText(control, value, Drawing.ContentAlignment.MiddleCenter, control.Font)
            End If
        End Sub

        ''' <summary>
        ''' Draws text into a  <see cref="System.Windows.Forms.Control">windows control</see>.
        ''' </summary>
        ''' <remarks>
        ''' Call this method after changing the progress bar's value. If for some reason, the changing of
        ''' the progress bar's value doesn't refresh it and clear the previously drawn text, call the
        ''' Refresh method of the progress bar before calling this method.
        ''' </remarks>
        ''' <param name="control">   The target progress bar to add text into. </param>
        ''' <param name="value">     The text to add into the progress bar. Leave null or empty to
        '''                          automatically add the percent. </param>
        ''' <param name="textAlign"> Where the text is to be placed. </param>
        <Extension()>
        Public Sub DrawText(ByVal control As Control, ByVal value As String, ByVal textAlign As Drawing.ContentAlignment)
            If control IsNot Nothing Then
                DrawText(control, value, textAlign, control.Font)
            End If
        End Sub

        ''' <summary>
        ''' Draws text into a  <see cref="System.Windows.Forms.Control">windows control</see>.
        ''' </summary>
        ''' <remarks>
        ''' Call this method after changing the progress bar's value. If for some reason, the changing of
        ''' the progress bar's value doesn't refresh it and clear the previously drawn text, call the
        ''' Refresh method of the progress bar before calling this method.
        ''' </remarks>
        ''' <param name="control">   The target progress bar to add text into. </param>
        ''' <param name="value">     The text to add into the progress bar. Leave null or empty to
        '''                          automatically add the percent. </param>
        ''' <param name="textAlign"> Where the text is to be placed. </param>
        ''' <param name="textFont">  The font the text should be drawn in. </param>
        <Extension()>
        Public Sub DrawText(ByVal control As Control, ByVal value As String, ByVal textAlign As Drawing.ContentAlignment, ByVal textFont As System.Drawing.Font)
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of Control, String, Drawing.ContentAlignment, System.Drawing.Font)(AddressOf Methods.DrawText), New Object() {control, value, textAlign, textFont})
                Else
                    Using gr As Drawing.Graphics = control.CreateGraphics()
                        Const margin As Single = 0
                        Dim x As Single = margin
                        Dim y As Single = (control.Height - gr.MeasureString(value, textFont).Height) / 2.0F
                        If textAlign = Drawing.ContentAlignment.BottomCenter OrElse textAlign = Drawing.ContentAlignment.MiddleCenter OrElse textAlign = Drawing.ContentAlignment.TopCenter Then
                            x = (control.Width - gr.MeasureString(value, textFont).Width) / 2.0F
                        ElseIf textAlign = Drawing.ContentAlignment.BottomRight OrElse textAlign = Drawing.ContentAlignment.MiddleRight OrElse textAlign = Drawing.ContentAlignment.TopRight Then
                            x = control.Width - gr.MeasureString(value, textFont).Width - margin
                        End If
                        If textAlign = Drawing.ContentAlignment.BottomCenter OrElse textAlign = Drawing.ContentAlignment.BottomLeft OrElse textAlign = Drawing.ContentAlignment.BottomRight Then
                            y = control.Height - gr.MeasureString(value, textFont).Height - margin
                        ElseIf textAlign = Drawing.ContentAlignment.TopCenter OrElse textAlign = Drawing.ContentAlignment.TopLeft OrElse textAlign = Drawing.ContentAlignment.TopRight Then
                            y = margin
                        End If
                        y = Math.Max(y, margin)
                        x = Math.Max(x, margin)
                        Using sb As New Drawing.SolidBrush(control.ForeColor)
                            gr.DrawString(value, textFont, sb, New Drawing.PointF(x, y))
                        End Using
                    End Using
                End If
            End If
        End Sub

#End Region

#Region " IMAGE "

        ''' <summary> Take screen shot. </summary>
        ''' <remarks> David, 2020-09-01. </remarks>
        ''' <param name="control"> The control to enable or disable. </param>
        ''' <returns> A Bitmap. </returns>
        <Extension()>
        Public Function TakeScreenShot(ByVal control As Control) As Bitmap
            Dim tmpImg As New Bitmap(control.Width, control.Height)
            Using g As Graphics = Graphics.FromImage(tmpImg)
                g.CopyFromScreen(control.PointToScreen(New Point(0, 0)), New Point(0, 0), New Size(control.Width, control.Height))
            End Using
            Return tmpImg
        End Function

        ''' <summary> Saves a screen shot. </summary>
        ''' <remarks> David, 2020-09-01. </remarks>
        ''' <param name="control">     The control to enable or disable. </param>
        ''' <param name="filename">    Filename of the file. </param>
        ''' <param name="imageFormat"> The image format. </param>
        <Extension()>
        Public Sub SaveScreenShot(ByVal control As Control, ByVal filename As String, ByVal imageFormat As System.Drawing.Imaging.ImageFormat)
            Dim tmpImg As New Bitmap(control.Width, control.Height)
            Using g As Graphics = Graphics.FromImage(tmpImg)
                g.CopyFromScreen(control.PointToScreen(New Point(0, 0)), New Point(0, 0), New Size(control.Width, control.Height))
            End Using
            tmpImg.Save(filename, imageFormat)
        End Sub

#End Region

    End Module

End Namespace
