Imports System.Runtime.CompilerServices
Namespace ToolStripExtensions
    ''' <summary> Includes extensions for <see cref="ToolStrip">Tool Strip</see>. </summary>
    ''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 11/19/2010, 1.2.3975.x. </para></remarks>
    Public Module Methods

#Region " TOOL STRIP ITEM "

        ''' <summary>
        ''' Sets the <see cref=" ToolStripMenuItem">Control</see> checked value to the The setter
        ''' disables the control before altering the checked state allowing the control code to use the
        ''' enabled state for preventing the execution of the control checked change actions.
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="control"> Check box control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SilentCheckStateSetter(ByVal control As ToolStripMenuItem, ByVal value As CheckState) As CheckState
            If control IsNot Nothing Then
                Dim wasEnabled As Boolean = control.Enabled
                control.Enabled = False
                control.CheckState = value
                control.Enabled = wasEnabled
            End If
            Return value
        End Function

        ''' <summary>
        ''' Sets the <see cref=" ToolStripMenuItem">Control</see> checked value to the The setter
        ''' disables the control before altering the checked state allowing the control code to use the
        ''' enabled state for preventing the execution of the control checked change actions.
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="control"> Check box control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        <Extension()>
        Public Function SilentCheckedSetter(ByVal control As ToolStripMenuItem, ByVal value As Boolean) As Boolean
            If control IsNot Nothing Then
                Dim wasEnabled As Boolean = control.Enabled
                control.Enabled = False
                control.Checked = value
                control.Enabled = wasEnabled
            End If
            Return value
        End Function

#End Region

#Region " TOOL STRIP DROP DOWN ITEM "

        ''' <summary> Maximum drop down item width. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="control"> The tool strip progress bar control. </param>
        ''' <returns> An Integer. </returns>
        <Extension()>
        Public Function MaxDropDownItemWidth(ByVal control As ToolStripDropDownItem) As Integer
            If control Is Nothing Then Throw New ArgumentNullException(NameOf(control))
            Return control.DropDownItems.MaxItemWidth
        End Function

#End Region

#Region " TOOL STRIP ITEM COLLECTION "

        ''' <summary> Maximum item width. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="items"> The items. </param>
        ''' <returns> An Integer. </returns>
        <Extension()>
        Public Function MaxItemWidth(ByVal items As ToolStripItemCollection) As Integer
            Dim width As Integer = 0
            If items IsNot Nothing Then
                For Each item As ToolStripItem In items
                    width = If(width < item.Width, item.Width, width)
                Next
            End If
            Return width
        End Function

#End Region

#Region " TOOL STRIP PROGRESS BAR "

        ''' <summary>
        ''' Sets the <see cref="ProgressBar">control</see> value to the
        ''' <paramref name="value">value</paramref> limited by the control range.
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="control"> The tool strip progress bar control. </param>
        ''' <param name="value">   The candidate value. </param>
        ''' <returns> The value limited within the range of the progress bar. </returns>
        <Extension()>
        Public Function ValueSetter(ByVal control As ToolStripProgressBar, ByVal value As Integer) As Integer
            If control IsNot Nothing Then
                value = If(value < control.Minimum, control.Minimum, If(value > control.Maximum, control.Maximum, value))
                If control.Value <> value Then control.Value = value
                Return CInt(control.Value)
            End If
            Return value
        End Function

        ''' <summary>
        ''' Sets the <see cref="ProgressBar">
        ''' control</see>
        ''' value to the.
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="control"> The tool strip progress bar control. </param>
        ''' <param name="value">   The candidate value. </param>
        ''' <returns> The value limited within the range of the progress bar. </returns>
        <Extension()>
        Public Function ValueSetter(ByVal control As ToolStripProgressBar, ByVal value As Double) As Integer
            Return ValueSetter(control, CInt(value))
        End Function

#End Region

    End Module
End Namespace
