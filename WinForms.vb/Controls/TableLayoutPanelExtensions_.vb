Imports System.Runtime.CompilerServices
Namespace TableLayoutPanelExtensions

    ''' <summary> Includes extensions for <see cref="System.Windows.Forms.TableLayoutPanel">Table Layout Panel</see> control. </summary>
    ''' <remarks> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 02/21/2014, 2.1.5165.x. </para></remarks>
    Public Module Extensions

        ''' <summary> Adds a control to the layout if not contained. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="layout">  The layout. </param>
        ''' <param name="control"> The control. </param>
        ''' <param name="column">  The column. </param>
        ''' <param name="row">     The row. </param>
        <Extension()>
        Public Sub AddIfNotContained(ByVal layout As TableLayoutPanel, ByVal control As Control, ByVal column As Integer, ByVal row As Integer)
            If layout IsNot Nothing AndAlso Not layout.Controls.Contains(control) Then
                layout.Controls.Add(control, column, row)
            End If
        End Sub

        ''' <summary> Removes the control. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="layout">  The layout. </param>
        ''' <param name="control"> The control. </param>
        <Extension()>
        Public Sub RemoveIfContained(ByVal layout As TableLayoutPanel, ByVal control As Control)
            If layout IsNot Nothing AndAlso layout.Controls IsNot Nothing AndAlso layout.Controls.Contains(control) Then
                layout.Controls.Remove(control)
            End If
        End Sub

    End Module

End Namespace
