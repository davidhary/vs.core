Imports System.Runtime.CompilerServices
Namespace ListControlExtensions
    ''' <summary> Includes extensions for <see cref="ListControl">List Control</see>. </summary>
    ''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 11/19/2010, 1.2.3975.x. </para></remarks>
    Public Module Methods

        ''' <summary>
        ''' Selects the <see cref="Control">combo box</see> index by setting the Selected Index to the
        ''' <paramref name="value">value</paramref>. The control is disabled when set so that the
        ''' handling of the changed event can be skipped.
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="control"> The control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> The index. </returns>
        <Extension()>
        Public Function SilentSelectIndex(ByVal control As ListControl, ByVal value As Integer) As Integer
            If control IsNot Nothing Then
                Dim enabled As Boolean = control.Enabled
                control.Enabled = False
                If control.SelectedIndex <> value Then
                    control.SelectedIndex = value
                End If
                control.Enabled = enabled
            End If
            Return value
        End Function

        ''' <summary>
        ''' Gets the selected or default <paramref name="defaultValue">value</paramref>.
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="control">      The control. </param>
        ''' <param name="defaultValue"> The default value. </param>
        ''' <returns> ``0. </returns>
        <Extension()>
        Public Function GetSelectedValue(Of T)(ByVal control As ListControl, ByVal defaultValue As T) As T
            Return If(control Is Nothing OrElse control.SelectedValue Is Nothing, defaultValue, CType(control.SelectedValue, T))
        End Function

        ' Notes:
        ' Setting the Selected Value does not work!

    End Module
End Namespace
