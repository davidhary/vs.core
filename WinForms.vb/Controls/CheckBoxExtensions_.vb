Imports System.Runtime.CompilerServices
Namespace CheckBoxExtensions
    ''' <summary> Includes extensions for <see cref="CheckBox">check box</see>. </summary>
    ''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 11/19/2010, 1.2.3975.x. </para></remarks>
    Public Module Methods

        ''' <summary>
        ''' Sets the <see cref="Control">check box</see> checked value to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter disables the control before altering the checked state allowing the control code
        ''' to use the enabled state for preventing the execution of the control checked change actions.
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        ''' <param name="control"> Check box control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SilentCheckStateSetter(ByVal control As System.Windows.Forms.CheckBox, ByVal value As CheckState) As CheckState
            If control IsNot Nothing Then
                If control.ThreeState Then
                    Dim wasEnabled As Boolean = control.Enabled
                    control.Enabled = False
                    control.CheckState = value
                    control.Enabled = wasEnabled
                Else
                    Throw New InvalidOperationException("Attempted to set a two-state control with a three-state value.")
                End If
            End If
            Return value
        End Function

        ''' <summary>
        ''' Sets the <see cref="Control">check box</see> check state value to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter disables the control before altering the checked state allowing the control code
        ''' to use the enabled state for preventing the execution of the control checked change actions.
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="control"> Check box control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SilentCheckedSetter(ByVal control As System.Windows.Forms.CheckBox, ByVal value As Boolean) As Boolean
            If control IsNot Nothing Then
                Dim wasEnabled As Boolean = control.Enabled
                control.Enabled = False
                control.Checked = value
                control.Enabled = wasEnabled
            End If
            Return value
        End Function

        ''' <summary> Converts a value to a check state. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="value"> The value to convert to check state. </param>
        ''' <returns>
        ''' <see cref="CheckState.Indeterminate">Indeterminate</see> if nothing, otherwise, checked or
        ''' unchecked.
        ''' </returns>
        <Extension()>
        Public Function ToCheckState(ByVal value As Boolean?) As CheckState
            If Not value.HasValue Then
                Return CheckState.Indeterminate
            ElseIf value.Value Then
                Return CheckState.Checked
            Else
                Return CheckState.Unchecked
            End If
        End Function

        ''' <summary> Initializes this object from the given check state. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="value"> The value to set. </param>
        ''' <returns> A Boolean? </returns>
        <Extension()>
        Public Function FromCheckState(ByVal value As CheckState) As Boolean?
            If value = CheckState.Checked Then
                Return True
            ElseIf value = CheckState.Unchecked Then
                Return False
            Else
                Return New Boolean
            End If
        End Function

    End Module
End Namespace
