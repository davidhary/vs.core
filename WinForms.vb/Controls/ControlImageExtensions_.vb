Imports System.Drawing
Imports System.Runtime.CompilerServices
Namespace ControlImageExtensions
    ''' <summary> Includes extensions for <see cref="Control"/> and <see cref="Image"/> within this control. </summary>
    ''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 11/19/2010, 1.2.3975.x. </para></remarks>
    Public Module Methods

        ''' <summary>
        ''' Gets image size for locating the image within the specified <para>Control</para>.
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="control"> The control. </param>
        ''' <param name="image">   The image. </param>
        ''' <returns> The image size. </returns>
        <Extension()>
        Public Function GetImageSize(ByVal control As Control, ByVal image As Drawing.Image) As Size

            If image Is Nothing OrElse control Is Nothing Then
                Return Drawing.Size.Empty
            End If

            Dim w As Integer = Math.Min(control.Width, image.Width + 1)
            Dim h As Integer = Math.Min(control.Height, image.Height + 1)

            Return New Size(w, h)

        End Function

        ''' <summary>
        ''' Gets image location for position the image within the specified <para>Control</para>.
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="control">    The control. </param>
        ''' <param name="image">      The image. </param>
        ''' <param name="imageAlign"> The image align. </param>
        ''' <returns> The image location. </returns>
        <Extension()>
        Public Function GetImageLocation(ByVal control As Control, ByVal image As Drawing.Image, ByVal imageAlign As Drawing.ContentAlignment) As Point

            If image Is Nothing OrElse control Is Nothing Then
                Return Drawing.Point.Empty
            End If

            Dim size As Size = GetImageSize(control, image)

            ' get X
            Dim x As Integer

            Select Case imageAlign
                Case ContentAlignment.TopLeft, ContentAlignment.MiddleLeft, ContentAlignment.BottomLeft
                    x = 0
                Case ContentAlignment.TopCenter, ContentAlignment.MiddleCenter, ContentAlignment.BottomCenter
                    x = (control.Width - size.Width) \ 2
                    If (control.Width - size.Width) Mod 2 > 0 Then x += 1
                Case Else
                    x = control.Width - size.Width
            End Select
            If x > 0 Then x -= 1

            ' get y
            Dim y As Integer
            Select Case imageAlign
                Case ContentAlignment.TopLeft, ContentAlignment.TopCenter, ContentAlignment.TopRight
                    y = 0
                Case ContentAlignment.MiddleLeft, ContentAlignment.MiddleCenter, ContentAlignment.MiddleRight
                    y = (control.Height - size.Height) \ 2
                    If (control.Height - size.Height) Mod 2 > 0 Then y += 1
                Case Else
                    y = control.Height - size.Height
            End Select
            If y > 0 Then y -= 1

            Return New Point(x, y)

        End Function

        ''' <summary> Gets image rectangle within the specified <para>Control</para>. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="control">    The control. </param>
        ''' <param name="image">      The image. </param>
        ''' <param name="imageAlign"> The image align. </param>
        ''' <param name="fillHeight"> if set to <c>True</c> [fill height]. </param>
        ''' <returns> The image rectangle. </returns>
        <Extension()>
        Public Function GetImageRect(ByVal control As Control, ByVal image As Drawing.Image,
                                 ByVal imageAlign As Drawing.ContentAlignment, ByVal fillHeight As Boolean) As Rectangle

            If image Is Nothing OrElse control Is Nothing Then
                Return Drawing.Rectangle.Empty
            End If

            Dim size As Size = GetImageSize(control, image)
            Dim location As Point = GetImageLocation(control, image, imageAlign)
            Return If(fillHeight,
                New Rectangle(location.X, location.Y, size.Width, control.Height),
                New Rectangle(location.X, location.Y, size.Width, size.Height))


        End Function

    End Module
End Namespace
