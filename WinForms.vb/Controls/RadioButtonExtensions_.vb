Imports System.Runtime.CompilerServices
Namespace RadioButtonExtensions
    ''' <summary> Includes extensions for <see cref="System.Windows.Forms.RadioButton">Radio Button</see>. </summary>
    ''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 11/19/2010, 1.2.3975.x. </para></remarks>
    Public Module Methods

        ''' <summary>
        ''' Sets the <see cref="System.Windows.Forms.RadioButton">radio button</see> checked value to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter disables the control before altering the checked state allowing the control code
        ''' to use the enabled state for preventing the execution of the control checked change actions.
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="control"> Check box control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SilentCheckedSetter(ByVal control As System.Windows.Forms.RadioButton, ByVal value As Boolean) As Boolean
            If control IsNot Nothing Then
                Dim wasEnabled As Boolean = control.Enabled
                control.Enabled = False
                control.Checked = value
                control.Enabled = wasEnabled
            End If
            Return value
        End Function

    End Module
End Namespace
