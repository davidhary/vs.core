Imports System.Runtime.CompilerServices
Namespace TreeViewExtensions
    ''' <summary> Includes extensions for <see cref="System.Windows.Forms.TreeView">Tree View</see> control. </summary>
    ''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 11/19/2010, 1.2.3975.x. </para></remarks>
    Public Module Methods

#Region " TREE VIEW "

        ''' <summary> Selects a node. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="control">  The <see cref="TreeView"/> control. </param>
        ''' <param name="nodeName"> Name of the node. </param>
        <Runtime.CompilerServices.Extension()>
        Public Sub SelectNode(ByVal control As TreeView, ByVal nodeName As String)
            If control IsNot Nothing Then
                control.SelectedNode = control.Nodes(nodeName)
            End If
        End Sub

        ''' <summary> Selects a node. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="control">       The <see cref="TreeView"/> control. </param>
        ''' <param name="nodeHierarchy"> The hierarchy of node names. </param>
        <Runtime.CompilerServices.Extension()>
        Public Sub SelectNode(ByVal control As TreeView, ByVal nodeHierarchy As String())
            If control IsNot Nothing AndAlso nodeHierarchy IsNot Nothing AndAlso nodeHierarchy.Any Then
                Dim parentTreeNode As TreeNode = Nothing
                Dim childTreeNode As TreeNode
                For Each nodeName As String In nodeHierarchy
                    If parentTreeNode Is Nothing Then
                        parentTreeNode = control.Nodes(nodeName)
                        childTreeNode = parentTreeNode
                    ElseIf parentTreeNode.Nodes.Count > 0 Then
                        childTreeNode = parentTreeNode.Nodes(nodeName)
                    Else
                        childTreeNode = Nothing
                    End If
                    If childTreeNode Is Nothing Then
                        Exit For
                    Else
                        parentTreeNode = childTreeNode
                    End If
                Next
                control.SelectedNode = parentTreeNode
            End If
        End Sub

#End Region

    End Module
End Namespace
