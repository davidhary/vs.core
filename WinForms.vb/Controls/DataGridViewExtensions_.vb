Imports System.Drawing
Imports System.Runtime.CompilerServices
Imports System.Windows.Forms
Namespace DataGridViewExtensions
    ''' <summary> Includes extensions for <see cref="DataGridView">Data Grid View</see>. </summary>
    ''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 11/19/2010, 1.2.3975.x. </para></remarks>
    Public Module Methods

#Region " DISPLAY "

        ''' <summary> Set grid rendering as double buffered to reduce flickering. </summary>
        ''' <remarks>
        ''' From http://www.CodeProject.com/Tips/390496/Reducing-flicker-blinking-in- DataGridView.
        ''' </remarks>
        ''' <param name="grid">    The grid. </param>
        ''' <param name="enabled"> Enabled if set to <c>True</c>. </param>
        <System.Runtime.CompilerServices.Extension()>
        Public Sub DoubleBuffered(ByVal grid As DataGridView, ByVal enabled As Boolean)
            If grid IsNot Nothing Then
                Dim dgvType As Type = grid.[GetType]()
                Dim pi As Reflection.PropertyInfo = dgvType.GetProperty("DoubleBuffered",
                                                                        Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic)
                pi.SetValue(grid, enabled, Nothing)
            End If
        End Sub

        ''' <summary> Hides all columns. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="grid"> The <see cref="DataGridView">grid</see> </param>
        <Extension()>
        Public Sub HideColumns(ByVal grid As DataGridView)
            If grid IsNot Nothing Then
                For Each column As DataGridViewColumn In grid.Columns
                    column.Visible = False
                Next
            End If
        End Sub

        ''' <summary>
        ''' Hides the columns by the <paramref name="columnNames">column names</paramref>.
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="grid">        The grid. </param>
        ''' <param name="columnNames"> The column names. </param>
        <Extension()>
        Public Sub HideColumns(ByVal grid As DataGridView, ByVal columnNames As String())
            If grid IsNot Nothing AndAlso columnNames IsNot Nothing Then
                For Each column As DataGridViewColumn In grid.Columns
                    If Not columnNames.Contains(column.Name) Then
                        column.Visible = False
                    End If
                Next
            End If
        End Sub

        ''' <summary> Displays a row numbers described by grid. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="grid"> The grid. </param>
        <Extension()>
        Public Sub DisplayRowNumbers(ByVal grid As DataGridView)
            If grid Is Nothing Then Throw New ArgumentNullException(NameOf(grid))
            For Each row As DataGridViewRow In grid.Rows
                row.HeaderCell.Value = (row.Index + 1).ToString()
            Next
        End Sub

        ''' <summary> Visible column count. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="grid"> The grid. </param>
        ''' <returns> The number of visible columns. </returns>
        <Extension()>
        Public Function VisibleColumnCount(ByVal grid As DataGridView) As Integer
            Dim count As Integer = 0
            If grid IsNot Nothing Then
                For Each column As DataGridViewColumn In grid.Columns
                    If column.Visible Then
                        count += 1
                    End If
                Next
            End If
            Return count
        End Function

        ''' <summary> Scrolls while keeping the displayed section centered. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="grid"> The <see cref="DataGridView">grid</see> </param>
        <Extension()>
        Public Sub ScrollCenterRow(ByVal grid As Windows.Forms.DataGridView)
            If grid Is Nothing Then Throw New ArgumentNullException(NameOf(grid))
            Dim halfWay As Integer = grid.DisplayedRowCount(False) \ 2
            If grid.SelectedRows.Count = 0 Then
                grid.CurrentRow.Selected = True
            End If
            If grid.FirstDisplayedScrollingRowIndex + halfWay > grid.SelectedRows(0).Index OrElse
               (grid.FirstDisplayedScrollingRowIndex + grid.DisplayedRowCount(False) - halfWay) <= grid.SelectedRows(0).Index Then
                Dim targetRow As Integer = grid.SelectedRows(0).Index
                targetRow = Math.Max(targetRow - halfWay, 0)
                grid.FirstDisplayedScrollingRowIndex = targetRow
                grid.PerformLayout()
            End If
        End Sub

#End Region

#Region " COLUMN HEADER MANAGEMENT "

        ''' <summary>
        ''' Splits the string to words by adding spaces between lower and upper case characters.
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="value"> The <c>String</c> value to split. </param>
        ''' <returns> A <c>String</c> of words separated by spaces. </returns>
        Private Function SplitWords(ByVal value As String) As String
            If String.IsNullOrWhiteSpace(value) Then
                Return String.Empty
            Else
                Dim isSpace As Boolean = False
                Dim isLowerCase As Boolean = False
                Dim newValue As New System.Text.StringBuilder
                For Each c As Char In value
                    If Not isSpace AndAlso isLowerCase AndAlso Char.IsUpper(c) Then
                        newValue.Append(" ")
                    End If
                    isSpace = c.Equals(" "c)
                    isLowerCase = Not Char.IsUpper(c)
                    newValue.Append(c)
                Next
                Return newValue.ToString()
            End If
        End Function

        ''' <summary>
        ''' Replaces the column header text with one that has spaces between lower and upper case
        ''' characters.
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="column"> The column. </param>
        <Extension()>
        Public Sub ParseHeaderText(ByVal column As DataGridViewColumn)
            If column IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(column.HeaderText) Then
                column.HeaderText = SplitWords(column.HeaderText)
            End If
        End Sub

        ''' <summary>
        ''' Replaces the column headers text with one that has spaces between lower and upper case
        ''' characters.
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="grid"> The <see cref="DataGridView">grid</see> </param>
        <Extension()>
        Public Sub ParseHeaderText(ByVal grid As DataGridView)
            If grid IsNot Nothing Then
                For Each column As DataGridViewColumn In grid.Columns
                    DataGridViewExtensions.ParseHeaderText(column)
                Next
            End If
        End Sub

        ''' <summary>
        ''' Replaces the column headers text with one that has spaces between lower and upper case
        ''' characters. Operates only on visible headers.
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="grid"> The <see cref="DataGridView">grid</see> </param>
        <Extension()>
        Public Sub ParseVisibleHeaderText(ByVal grid As DataGridView)
            If grid IsNot Nothing Then
                For Each column As DataGridViewColumn In grid.Columns
                    If column.Visible Then
                        DataGridViewExtensions.ParseHeaderText(column)
                    End If
                Next
            End If
        End Sub

#End Region

#Region " COLUMN MANAGEMENT: Duplicate, Replace, Move, "

        ''' <summary>
        ''' Replaces an existing column with a combo box column and returns reference to this column.
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="grid">       The grid. </param>
        ''' <param name="columnName"> Name of the column. </param>
        ''' <returns> Replaced column. </returns>
        <Extension()>
        Public Function ReplaceWithComboColumn(ByVal grid As DataGridView, ByVal columnName As String) As DataGridViewComboBoxColumn
            If grid Is Nothing Then Throw New ArgumentNullException(NameOf(grid))
            If String.IsNullOrWhiteSpace(columnName) Then Throw New ArgumentNullException(NameOf(columnName))
            Dim column As DataGridViewColumn = grid.Columns.Item(columnName)
            Dim gridColumn As New DataGridViewComboBoxColumn
            Try
                gridColumn.Name = column.Name
                gridColumn.DataPropertyName = column.DataPropertyName
                gridColumn.HeaderText = column.HeaderText
                'gridColumn.IsDataBound = column.IsDataBound
                grid.Columns.Remove(column)
                ' this does not work right. Me._dataGridView.Columns.Insert(fieldColumn.Ordinal, gridColumn)
                grid.Columns.Insert(column.Index, gridColumn)
                Return gridColumn
            Catch
                gridColumn?.Dispose()
                Throw
            End Try
        End Function

        ''' <summary> Duplicates an existing column returns reference to this column. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="grid">       The grid. </param>
        ''' <param name="columnName"> Name of the column. </param>
        ''' <param name="newName">    Name of the new. </param>
        ''' <returns> Replaced column. </returns>
        <Extension()>
        Public Function DuplicateColumn(ByVal grid As DataGridView, ByVal columnName As String,
                                             ByVal newName As String) As DataGridViewColumn
            If grid Is Nothing Then Throw New ArgumentNullException(NameOf(grid))
            If String.IsNullOrWhiteSpace(columnName) Then Throw New ArgumentNullException(NameOf(columnName))
            Dim column As DataGridViewColumn = grid.Columns.Item(columnName)
            Dim gridColumn As New DataGridViewColumn
            Try
                gridColumn.Name = newName
                gridColumn.DataPropertyName = column.DataPropertyName
                gridColumn.HeaderText = column.HeaderText
                grid.Columns.Insert(column.Index, gridColumn)
                Return gridColumn
            Catch
                gridColumn?.Dispose()
                Throw
            End Try
        End Function

        ''' <summary> Duplicate combo column. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="grid">       The grid. </param>
        ''' <param name="columnName"> Name of the column. </param>
        ''' <param name="newName">    Name of the new. </param>
        ''' <returns> A  DataGridViewCheckBoxColumn. </returns>
        <Extension()>
        Public Function DuplicateComboColumn(ByVal grid As DataGridView, ByVal columnName As String,
                                             ByVal newName As String) As DataGridViewComboBoxColumn
            If grid Is Nothing Then Throw New ArgumentNullException(NameOf(grid))
            If String.IsNullOrWhiteSpace(columnName) Then Throw New ArgumentNullException(NameOf(columnName))
            Dim column As DataGridViewColumn = grid.Columns.Item(columnName)
            Dim gridColumn As New DataGridViewComboBoxColumn
            Try
                gridColumn.Name = newName
                gridColumn.DataPropertyName = column.DataPropertyName
                gridColumn.HeaderText = column.HeaderText
                grid.Columns.Insert(column.Index, gridColumn)
                Return gridColumn
            Catch
                gridColumn?.Dispose()
                Throw
            End Try
        End Function

        ''' <summary>
        ''' Replaces an existing column with a check box column and returns reference to this column.
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="grid">       The grid. </param>
        ''' <param name="columnName"> Name of the column. </param>
        ''' <returns> Replaced column. </returns>
        <Extension()>
        Public Function ReplaceWithCheckColumn(ByVal grid As DataGridView, ByVal columnName As String) As DataGridViewCheckBoxColumn
            If grid Is Nothing Then Throw New ArgumentNullException(NameOf(grid))
            If String.IsNullOrWhiteSpace(columnName) Then Throw New ArgumentNullException(NameOf(columnName))
            Dim column As DataGridViewColumn = grid.Columns.Item(columnName)
            Dim gridColumn As New DataGridViewCheckBoxColumn
            Try
                gridColumn.Name = column.Name
                gridColumn.DataPropertyName = column.DataPropertyName
                gridColumn.HeaderText = column.HeaderText
                gridColumn.ThreeState = True
                grid.Columns.Remove(column)
                grid.Columns.Insert(column.Index, gridColumn)
                Return gridColumn
            Catch
                gridColumn?.Dispose()
                Throw
            End Try
        End Function

        ''' <summary>
        ''' Replaces an existing column with a text box column and returns reference to this column.
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="grid">       The grid. </param>
        ''' <param name="columnName"> Name of the column. </param>
        ''' <returns> Replaced column. </returns>
        <Extension()>
        Public Function ReplaceWithTextColumn(ByVal grid As DataGridView, ByVal columnName As String) As DataGridViewTextBoxColumn
            If grid Is Nothing Then Throw New ArgumentNullException(NameOf(grid))
            If String.IsNullOrWhiteSpace(columnName) Then Throw New ArgumentNullException(NameOf(columnName))
            Dim column As DataGridViewColumn = grid.Columns.Item(columnName)
            Dim gridColumn As New DataGridViewTextBoxColumn
            Try
                gridColumn.Name = column.Name
                gridColumn.DataPropertyName = column.DataPropertyName
                gridColumn.HeaderText = column.HeaderText
                'gridColumn.IsDataBound = column.IsDataBound
                grid.Columns.Remove(column)
                ' this does not work right. Me._dataGridView.Columns.Insert(fieldColumn.Ordinal, gridColumn)
                grid.Columns.Insert(column.Index, gridColumn)
                Return gridColumn
            Catch
                gridColumn?.Dispose()
                Throw
            End Try
        End Function

        ''' <summary>
        ''' Replaces an existing column with a text box column and returns reference to this column.
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="grid">       The grid. </param>
        ''' <param name="columnName"> Name of the column. </param>
        ''' <param name="newName">    Name of the new. </param>
        ''' <returns> Replaced column. </returns>
        <Extension()>
        Public Function DuplicateTextColumn(ByVal grid As DataGridView, ByVal columnName As String, ByVal newName As String) As DataGridViewTextBoxColumn
            If grid Is Nothing Then Throw New ArgumentNullException(NameOf(grid))
            If String.IsNullOrWhiteSpace(columnName) Then Throw New ArgumentNullException(NameOf(columnName))
            Dim column As DataGridViewColumn = grid.Columns.Item(columnName)
            Dim gridColumn As New DataGridViewTextBoxColumn
            Try
                gridColumn.Name = newName
                gridColumn.DataPropertyName = column.DataPropertyName
                gridColumn.HeaderText = column.HeaderText
                grid.Columns.Insert(column.Index, gridColumn)
                Return gridColumn
            Catch
                gridColumn?.Dispose()
                Throw
            End Try
        End Function

        ''' <summary> Moves an existing column to the specified location. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="grid">       The grid. </param>
        ''' <param name="columnName"> Name of the column. </param>
        ''' <param name="location">   The location. </param>
        ''' <returns> The moved column. </returns>
        <Extension()>
        Public Function MoveTo(ByVal grid As DataGridView, ByVal columnName As String, ByVal location As Integer) As DataGridViewColumn
            If grid Is Nothing Then Throw New ArgumentNullException(NameOf(grid))
            If String.IsNullOrWhiteSpace(columnName) Then Throw New ArgumentNullException(NameOf(columnName))
            Dim column As DataGridViewColumn = grid.Columns.Item(columnName)
            grid.Columns.Remove(column)
            grid.Columns.Insert(location, column)
            Return column
        End Function

#End Region

#Region " ROW MANAGEMENT: Find, Select "

        ''' <summary> Returns the row selected based on the column value. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="grid">        The grid. </param>
        ''' <param name="columnName">  Name of the column. </param>
        ''' <param name="columnValue"> The column value. </param>
        ''' <returns> The found row. </returns>
        <Extension()>
        Public Function FindRow(Of T)(ByVal grid As DataGridView, ByVal columnName As String, ByVal columnValue As T) As DataGridViewRow
            If grid Is Nothing Then Throw New ArgumentNullException(NameOf(grid))
            If String.IsNullOrWhiteSpace(columnName) Then Throw New ArgumentNullException(NameOf(columnName))
            Dim foundRow As DataGridViewRow = Nothing
            For Each r As DataGridViewRow In grid.Rows
                If columnValue.Equals(r.Cells(columnName).Value) Then
                    foundRow = r
                    Exit For
                End If
            Next
            Return foundRow
        End Function

        ''' <summary> Selects the row. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="grid">        The grid. </param>
        ''' <param name="columnName">  Name of the column. </param>
        ''' <param name="columnValue"> The column value. </param>
        ''' <returns> <c>True</c> if the row was selected. </returns>
        <Extension()>
        Public Function SelectRow(Of T)(ByVal grid As DataGridView, ByVal columnName As String, ByVal columnValue As T) As Boolean
            If grid Is Nothing Then Throw New ArgumentNullException(NameOf(grid))
            If String.IsNullOrWhiteSpace(columnName) Then Throw New ArgumentNullException(NameOf(columnName))
            Dim foundRow As DataGridViewRow = Methods.FindRow(Of T)(grid, columnName, columnValue)
            If foundRow Is Nothing Then
                Return False
            Else
                foundRow.Selected = True
                Return True
            End If
        End Function

        ''' <summary>
        ''' Silently selects a grid row. The control is disabled when set so that the handling of the
        ''' changed event can be skipped.
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="grid">        The grid. </param>
        ''' <param name="columnName">  Name of the column. </param>
        ''' <param name="columnValue"> The column value. </param>
        ''' <returns> <c>True</c> if the row was selected. </returns>
        <Extension()>
        Public Function SilentSelectRow(Of T)(ByVal grid As DataGridView, ByVal columnName As String, ByVal columnValue As T) As Boolean
            If grid Is Nothing Then Throw New ArgumentNullException(NameOf(grid))
            If String.IsNullOrWhiteSpace(columnName) Then Throw New ArgumentNullException(NameOf(columnName))
            Dim enabled As Boolean = grid.Enabled
            Try
                grid.Enabled = False
                Return Methods.SelectRow(Of T)(grid, columnName, columnValue)
            Catch
                Throw
            Finally
                grid.Enabled = enabled
            End Try
        End Function

#End Region

#Region " STORE "

        ''' <summary> Stores the grid to file. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="grid">     The grid. </param>
        ''' <param name="fileName"> Filename of the file. </param>
        ''' <returns> An Integer. </returns>
        <Extension()>
        Public Function Store(ByVal grid As DataGridView, ByVal fileName As String) As Integer

            If grid Is Nothing Then Return 0
            Dim headers As String() = (From header As DataGridViewColumn In grid.Columns.Cast(Of DataGridViewColumn)() Select header.HeaderText).ToArray

            Dim rows As IEnumerable(Of String()) = From row As DataGridViewRow In grid.Rows.Cast(Of DataGridViewRow)()
                                                   Where Not row.IsNewRow
                                                   Select Array.ConvertAll(row.Cells.Cast(Of DataGridViewCell).ToArray, Function(c) If(c.Value IsNot Nothing, c.Value.ToString, ""))

            Using sw As New System.IO.StreamWriter(fileName)
                sw.WriteLine(String.Join(",", headers))
                For Each r As String() In rows
                    sw.WriteLine(String.Join(",", r))
                Next
            End Using
            Return rows.Count
        End Function

        ''' <summary> Stores the grid to file. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="grid">           The grid. </param>
        ''' <param name="fileName">       Filename of the file. </param>
        ''' <param name="newLineReplace"> The new line replace string. </param>
        ''' <returns> An Integer. </returns>
        <Extension()>
        Public Function Store(ByVal grid As DataGridView, ByVal fileName As String, ByVal newLineReplace As String) As Integer
            If grid Is Nothing Then Return 0
            Dim headers As String() = (From header As DataGridViewColumn In grid.Columns.Cast(Of DataGridViewColumn)() Select header.HeaderText).ToArray

            Dim rows As IEnumerable(Of String()) = From row As DataGridViewRow In grid.Rows.Cast(Of DataGridViewRow)()
                                                   Where Not row.IsNewRow
                                                   Select Array.ConvertAll(row.Cells.Cast(Of DataGridViewCell).ToArray, Function(c) If(c.Value IsNot Nothing, c.Value.ToString, ""))

            Using sw As New System.IO.StreamWriter(fileName)
                Dim s As String = String.Join(",", headers)
                If Not String.IsNullOrWhiteSpace(s) Then
                    sw.WriteLine(s.Replace(Environment.NewLine, newLineReplace))
                End If
                For Each r As String() In rows
                    s = String.Join(",", r)
                    If Not String.IsNullOrWhiteSpace(s) Then
                        sw.WriteLine(s.Replace(Environment.NewLine, newLineReplace))
                    End If
                Next
            End Using
            Return rows.Count
        End Function

#End Region

#Region " COPY "

        ''' <summary> Copies to clipboard described by grid. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="grid"> The <see cref=" DataGridView">grid</see> </param>
        <Extension()>
        Public Sub CopyToClipboard(ByVal grid As DataGridView)
            If grid IsNot Nothing AndAlso
                (grid.SelectedRows.Count > 0 OrElse grid.SelectedColumns.Count > 0 OrElse grid.SelectedCells.Count > 0) Then
                grid.SuspendLayout()
                'grid.RowHeadersVisible = False
                Windows.Forms.Clipboard.SetDataObject(grid.GetClipboardContent())
                'grid.ClearSelection()
                'grid.RowHeadersVisible = True
                grid.ResumeLayout()
            End If
        End Sub


#End Region

#Region " SORT "

        ''' <summary>
        ''' Sort on a number of columns when handling the on column header mouse click.
        ''' </summary>
        ''' <remarks> https://www.codeproject.com/Tips/1233057/Datagridview-with-Multisort. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="grid">       The <see cref="DataGridView">grid</see> </param>
        ''' <param name="dataSource"> The data source. </param>
        ''' <param name="e">          Data grid view cell mouse event information. </param>
        <Extension()>
        Public Sub MultipleSort(ByVal grid As Windows.Forms.DataGridView, ByVal dataSource As BindingSource, ByVal e As DataGridViewCellMouseEventArgs)
            If grid Is Nothing Then Throw New ArgumentNullException(NameOf(grid))
            If dataSource Is Nothing Then Throw New ArgumentNullException(NameOf(dataSource))
            If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
            If Control.ModifierKeys = Keys.Control Then
                If dataSource.Sort.Contains(grid.Columns(e.ColumnIndex).Name) Then
                    dataSource.Sort = If(dataSource.Sort.Contains(grid.Columns(e.ColumnIndex).Name + " ASC"),
                        dataSource.Sort.Replace(grid.Columns(e.ColumnIndex).Name +
                        " ASC", grid.Columns(e.ColumnIndex).Name + " DESC"),
                        dataSource.Sort.Replace(grid.Columns(e.ColumnIndex).Name +
                        " DESC", grid.Columns(e.ColumnIndex).Name + " ASC"))
                Else
                    dataSource.Sort += ", " & grid.Columns(e.ColumnIndex).Name + " ASC"
                End If

                Dim allSort() As String = Split(dataSource.Sort, ", ")
                For Each s As String In allSort
                    Dim iSortOrder() As String = Split(s, " ")
                    Dim cName As String = s.Replace(" " + iSortOrder(iSortOrder.Length - 1), "")
                    Select Case iSortOrder(iSortOrder.Length - 1)
                        Case "ASC"
                            grid.Columns(cName).HeaderCell.SortGlyphDirection = SortOrder.Ascending
                        Case "DESC"
                            grid.Columns(cName).HeaderCell.SortGlyphDirection = SortOrder.Descending
                    End Select
                Next
            Else
                dataSource.Sort = If(dataSource.Sort.Contains(grid.Columns(e.ColumnIndex).Name + " DESC"),
                    grid.Columns(e.ColumnIndex).Name + " ASC",
                    grid.Columns(e.ColumnIndex).Name + " DESC")
            End If
        End Sub

#End Region

#Region " PRINT "

        ''' <summary> Prints grid. </summary>
        ''' <remarks>
        ''' From https://stackoverflow.com/questions/24654790/how-to-print-datagridview-has-a- table-in-
        ''' vb.
        ''' </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="grid">         The <see cref="DataGridView">grid</see> </param>
        ''' <param name="e">            Print page event information. </param>
        ''' <param name="rowIndex">     [in,out] Zero-based index of the row. </param>
        ''' <param name="isNewPage">    [in,out] The is new page. </param>
        ''' <param name="bottomMargin"> [in,out] The bottom margin. </param>
        <Extension()>
        Public Sub Print(ByVal grid As DataGridView, e As System.Drawing.Printing.PrintPageEventArgs,
                         ByRef rowIndex As Integer, ByRef isNewPage As Boolean, ByRef bottomMargin As Single)
            If grid Is Nothing Then Throw New ArgumentNullException(NameOf(grid))
            If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
            Using fmt As StringFormat = New StringFormat(StringFormatFlags.LineLimit)

                fmt.LineAlignment = StringAlignment.Center
                fmt.Trimming = StringTrimming.EllipsisCharacter

                Dim y As Single = e.MarginBounds.Top
                If bottomMargin > 0 Then y = bottomMargin

                ' print column headers
                If isNewPage AndAlso rowIndex = 0 Then
                    isNewPage = False
                    Dim h As Single = 0
                    Dim x As Single = e.MarginBounds.Left
                    For Each column As DataGridViewColumn In grid.Columns
                        Dim rc As RectangleF = New RectangleF(x, y, column.HeaderCell.Size.Width, column.HeaderCell.Size.Height)
                        h = Math.Max(h, rc.Height)
                        If y + h > e.MarginBounds.Bottom Then
                            e.HasMorePages = True
                            Exit For
                        End If
                        e.Graphics.DrawRectangle(Pens.Black, rc.Left, rc.Top, rc.Width, rc.Height)
                        e.Graphics.DrawString(column.HeaderText, grid.Font, Brushes.Black, rc, fmt)
                        x += rc.Width
                    Next
                    y += h
                    bottomMargin = y
                End If

                ' print the row cells
                Do While rowIndex < grid.RowCount AndAlso Not e.HasMorePages
                    Dim row As DataGridViewRow = grid.Rows(rowIndex)
                    Dim x As Single = e.MarginBounds.Left
                    Dim h As Single = 0
                    For Each cell As DataGridViewCell In row.Cells
                        Dim rc As RectangleF = New RectangleF(x, y, cell.Size.Width, cell.Size.Height)
                        h = Math.Max(h, rc.Height)
                        If y + h > e.MarginBounds.Bottom Then
                            e.HasMorePages = True
                            Exit For
                        End If
                        e.Graphics.DrawRectangle(Pens.Black, rc.Left, rc.Top, rc.Width, rc.Height)
                        e.Graphics.DrawString(grid.Rows(cell.RowIndex).Cells(cell.ColumnIndex).FormattedValue.ToString(),
                                              grid.Font, Brushes.Black, rc, fmt)
                        x += rc.Width
                    Next
                    If Not e.HasMorePages Then
                        rowIndex += 1
                        y += h
                        bottomMargin = y
                    End If
                Loop
                If e.HasMorePages Then
                    isNewPage = True
                End If

            End Using

        End Sub

        ''' <summary> Prints grid. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="grid">                The <see cref="DataGridView">grid</see> </param>
        ''' <param name="selectedCellIndexes"> The selected cell indexes. </param>
        ''' <param name="e">                   Print page event information. </param>
        ''' <param name="rowIndex">            [in,out] Zero-based index of the row. </param>
        ''' <param name="isNewPage">           [in,out] The is new page. </param>
        <Extension()>
        Public Sub Print(ByVal grid As DataGridView, ByVal selectedCellIndexes As Integer(),
                         e As System.Drawing.Printing.PrintPageEventArgs, ByRef rowIndex As Integer, ByRef isNewPage As Boolean)
            If grid Is Nothing OrElse e Is Nothing OrElse selectedCellIndexes Is Nothing Then Return
            Using fmt As StringFormat = New StringFormat(StringFormatFlags.LineLimit)
                fmt.LineAlignment = StringAlignment.Center
                fmt.Trimming = StringTrimming.EllipsisCharacter
                Dim y As Single = e.MarginBounds.Top
                Do While rowIndex < grid.RowCount
                    Dim row As DataGridViewRow = grid.Rows(rowIndex)
                    Dim x As Single = e.MarginBounds.Left
                    Dim h As Single = 0
                    For Each cell As Integer In selectedCellIndexes
                        Dim rc As RectangleF = New RectangleF(x, y, row.Cells(cell).Size.Width, row.Cells(cell).Size.Height)
                        e.Graphics.DrawRectangle(Pens.Black, rc.Left, rc.Top, rc.Width, rc.Height)
                        If isNewPage Then
                            e.Graphics.DrawString(grid.Columns(cell).HeaderText, grid.Font, Brushes.Black, rc, fmt)
                        Else
                            e.Graphics.DrawString(grid.Rows(row.Cells(cell).RowIndex).Cells(cell).FormattedValue.ToString(),
                                                  grid.Font, Brushes.Black, rc, fmt)
                        End If
                        x += rc.Width
                        h = Math.Max(h, rc.Height)
                    Next
                    If Not isNewPage Then rowIndex += 1
                    isNewPage = False
                    y += h
                    If y + h > e.MarginBounds.Bottom Then
                        e.HasMorePages = True
                        rowIndex -= 1
                        isNewPage = True
                        Exit Sub
                    End If
                Loop
            End Using
            rowIndex = 0
        End Sub

#End Region

    End Module
End Namespace
