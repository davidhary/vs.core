Imports System.Runtime.CompilerServices
Namespace NumericUpDownExtensions
    ''' <summary> Includes extensions for <see cref="NumericUpDown">Numeric Up Down</see> control. </summary>
    ''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 11/19/2010, 1.2.3975.x. </para></remarks>
    Public Module Methods

#Region " RANGE "

        ''' <summary> Range setter. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="control"> The numeric up down control. </param>
        ''' <param name="minimum"> The minimum. </param>
        ''' <param name="maximum"> The maximum. </param>
        ''' <returns> A Tuple(Of Decimal, Decimal) </returns>
        <Extension()>
        Public Function RangeSetter(ByVal control As NumericUpDown, ByVal minimum As Decimal, ByVal maximum As Decimal) As Tuple(Of Decimal, Decimal)
            If control Is Nothing Then Throw New ArgumentNullException(NameOf(control))
            If control.Value > maximum Then control.Value = maximum
            If control.Value < minimum Then control.Value = minimum
            control.Maximum = maximum
            control.Minimum = minimum
            Return New Tuple(Of Decimal, Decimal)(control.Minimum, control.Maximum)
        End Function

        ''' <summary> Range setter. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="control"> The numeric up down control. </param>
        ''' <param name="minimum"> The minimum. </param>
        ''' <param name="maximum"> The maximum. </param>
        ''' <returns> A Tuple(Of Decimal, Decimal) </returns>
        <Extension()>
        Public Function RangeSetter(ByVal control As NumericUpDown, ByVal minimum As Double, ByVal maximum As Double) As Tuple(Of Decimal, Decimal)
            If control Is Nothing Then Throw New ArgumentNullException(NameOf(control))
            Return Methods.RangeSetter(control, If(minimum < Decimal.MinValue, Decimal.MinValue, CDec(minimum)), If(maximum > Decimal.MaxValue, Decimal.MaxValue, CDec(maximum)))
        End Function

#End Region

#Region " MAXIMUM "

        ''' <summary>
        ''' Sets the <see cref="NumericUpDown">control</see>
        ''' <see cref=" NumericUpDown.Maximum">Maximum</see>.
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="control"> The numeric up down control. </param>
        ''' <param name="value">   The candidate Maximum. </param>
        ''' <returns> The limited Maximum. </returns>
        <Extension()>
        Public Function MaximumSetter(ByVal control As NumericUpDown, ByVal value As Decimal) As Decimal
            If control IsNot Nothing Then
                If control.Value > value Then control.Value = value
                control.Maximum = value
            End If
            Return value
        End Function

        ''' <summary>
        ''' Sets the <see cref="NumericUpDown">control</see>
        ''' <see cref=" NumericUpDown.Maximum">Maximum</see>.
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="control"> The numeric up down control. </param>
        ''' <param name="value">   The candidate Maximum. </param>
        ''' <returns> A Decimal. </returns>
        <Extension()>
        Public Function MaximumSetter(ByVal control As NumericUpDown, ByVal value As Double) As Decimal
            Return Methods.MaximumSetter(control, If(value > Decimal.MaxValue, Decimal.MaxValue, CDec(value)))
        End Function

#End Region

#Region " MINIMUM "

        ''' <summary>
        ''' Sets the <see cref="NumericUpDown">control</see>
        ''' <see cref=" NumericUpDown.Minimum">Minimum</see>.
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="control"> The numeric up down control. </param>
        ''' <param name="value">   The candidate Minimum. </param>
        ''' <returns> The limited Minimum. </returns>
        <Extension()>
        Public Function MinimumSetter(ByVal control As NumericUpDown, ByVal value As Decimal) As Decimal
            If control IsNot Nothing Then
                If control.Value < value Then control.Value = value
                control.Minimum = value
            End If
            Return value
        End Function

        ''' <summary>
        ''' Sets the <see cref="NumericUpDown">control</see>
        ''' <see cref=" NumericUpDown.Minimum">Minimum</see>.
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="control"> The numeric up down control. </param>
        ''' <param name="value">   The candidate Maximum. </param>
        ''' <returns> The limited Minimum. </returns>
        <Extension()>
        Public Function MinimumSetter(ByVal control As NumericUpDown, ByVal value As Double) As Decimal
            Return Methods.MinimumSetter(control, If(value < Decimal.MinValue, Decimal.MinValue, CDec(value)))
        End Function


#End Region

#Region " VALUE "

        ''' <summary>
        ''' Sets the <see cref="NumericUpDown">control</see> value to the
        ''' <paramref name="value">value</paramref> scaled by the
        ''' <paramref name="scalar">scalar</paramref> and limited by the control range.
        ''' Returns limited value (control value divided by the scalar).
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="control"> The numeric up down control. </param>
        ''' <param name="scalar">  The scalar. </param>
        ''' <param name="value">   The candidate value. </param>
        ''' <returns> The control value divided by the scalar. </returns>
        <Extension()>
        Public Function ValueSetter(ByVal control As NumericUpDown, ByVal scalar As Decimal, ByVal value As Decimal) As Decimal
            Return ValueSetter(control, scalar * value) / scalar
        End Function

        ''' <summary>
        ''' Sets the <see cref="NumericUpDown">control</see> value to the
        ''' <paramref name="value">value</paramref> scaled by the
        ''' <paramref name="scalar">scalar</paramref> and limited by the control range.
        ''' Returns limited value (value within the control range divided by the scalar). The setter
        ''' disables the control before altering the checked state allowing the control code to use the
        ''' enabled state for preventing the execution of the control checked change actions.
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="control"> The numeric up down control. </param>
        ''' <param name="scalar">  The scalar. </param>
        ''' <param name="value">   The candidate value. </param>
        ''' <returns> The control value divided by the scalar. </returns>
        <Extension()>
        Public Function SilentValueSetter(ByVal control As NumericUpDown, ByVal scalar As Decimal, ByVal value As Decimal) As Decimal
            Return SilentValueSetter(control, scalar * value) / scalar
        End Function

        ''' <summary>
        ''' Sets the <see cref="NumericUpDown">control</see> value to the
        ''' <paramref name="value">value</paramref> limited by the control range.
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="control"> The numeric up down control. </param>
        ''' <param name="value">   The candidate value. </param>
        ''' <returns> The limited value. </returns>
        <Extension()>
        Public Function ValueSetter(ByVal control As NumericUpDown, ByVal value As Decimal) As Decimal
            If control IsNot Nothing Then
                value = If(value < control.Minimum, control.Minimum, If(value > control.Maximum, control.Maximum, value))
                If value <> control.Value Then control.Value = value
            End If
            Return value
        End Function

        ''' <summary>
        ''' Sets the <see cref="NumericUpDown">control</see> value to the
        ''' <paramref name="value">value</paramref> limited by the control range.
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="control"> The numeric up down control. </param>
        ''' <param name="value">   The candidate value. </param>
        ''' <returns> The limited value. </returns>
        <Extension()>
        Public Function ValueSetter(ByVal control As NumericUpDown, ByVal value As Decimal?) As Decimal?
            If control IsNot Nothing Then
                If value.HasValue Then
                    control.Value = If(value.Value < control.Minimum, control.Minimum, If(value.Value > control.Maximum, control.Maximum, value.Value))
                Else
                    control.Text = String.Empty
                End If
                Return control.Value
            Else
                Return value
            End If
        End Function

        ''' <summary>
        ''' Sets the <see cref="NumericUpDown">control</see> value to the
        ''' <paramref name="value">value</paramref> limited by the control range.
        ''' This setter disables the control before altering the checked state allowing the control code
        ''' to use the enabled state for preventing the execution of the control checked change actions.
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="control"> The numeric up down control. </param>
        ''' <param name="value">   The candidate value. </param>
        ''' <returns> The limited value. </returns>
        <Extension()>
        Public Function SilentValueSetter(ByVal control As NumericUpDown, ByVal value As Decimal) As Decimal
            If control IsNot Nothing Then
                Dim wasEnabled As Boolean = control.Enabled
                control.Enabled = False
                ValueSetter(control, value)
                control.Enabled = wasEnabled
                Return control.Value
            Else
                Return value
            End If
        End Function

        ''' <summary>
        ''' Sets the <see cref="NumericUpDown">control</see> value to the
        ''' <paramref name="value">value</paramref> limited by the control range.
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="control"> The numeric up down control. </param>
        ''' <param name="value">   The candidate value. </param>
        ''' <returns> The limited value. </returns>
        <Extension()>
        Public Function ValueSetter(ByVal control As NumericUpDown, ByVal value As Double) As Double
            Return ValueSetter(control, CDec(value))
        End Function

        ''' <summary>
        ''' Sets the <see cref="NumericUpDown">control</see> value to the
        ''' <paramref name="value">value</paramref> limited by the control range.
        ''' This setter disables the control before altering the checked state allowing the control code
        ''' to use the enabled state for preventing the execution of the control checked change actions.
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="control"> The numeric up down control. </param>
        ''' <param name="value">   The candidate value. </param>
        ''' <returns> The limited value. </returns>
        <Extension()>
        Public Function SilentValueSetter(ByVal control As NumericUpDown, ByVal value As Double) As Decimal
            If control IsNot Nothing Then
                Dim wasEnabled As Boolean = control.Enabled
                control.Enabled = False
                ValueSetter(control, CDec(value))
                control.Enabled = wasEnabled
                Return control.Value
            End If
            Return CDec(value)
        End Function

#End Region

    End Module
End Namespace
