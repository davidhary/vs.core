Imports System.Runtime.CompilerServices
Namespace ControlCollectionExtensions
    ''' <summary> Includes extensions for <see cref="Control.ControlCollection">Control Collection</see>. </summary>
    ''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 11/19/2010, 1.2.3975.x. </para></remarks>
    Public Module Methods

        ''' <summary> Adds a control to the control collection if not contained. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="controls"> The collection of controls. </param>
        ''' <param name="control">  The control. </param>
        <Extension()>
        Public Sub AddIfNotContained(ByVal controls As Control.ControlCollection, ByVal control As Control)
            If controls IsNot Nothing AndAlso Not controls.Contains(control) Then
                controls.Add(control)
            End If
        End Sub

        ''' <summary> Removes a control if it is contained in the control collections. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="controls"> The collection of controls. </param>
        ''' <param name="control">  The control to remove. </param>
        <Extension()>
        Public Sub RemoveIfContained(ByVal controls As Control.ControlCollection, ByVal control As Control)
            If controls IsNot Nothing AndAlso controls.Contains(control) Then
                controls.Remove(control)
            End If
        End Sub

        ''' <summary> Retrieve all controls and child controls. </summary>
        ''' <remarks>
        ''' Make sure to send control back at lowest depth first so that most child controls are checked
        ''' for things before container controls, e.g., a TextBox is checked before a GroupBox control.
        ''' </remarks>
        ''' <param name="controls"> . </param>
        ''' <returns> Returns an array of controls. </returns>
        <Extension()>
        Public Function RetrieveControls(ByVal controls As System.Windows.Forms.Control.ControlCollection) As System.Windows.Forms.Control()

            If controls Is Nothing Then Return Nothing

            Dim controlList As New ArrayList

            ' Dim allControls As ArrayList = New ArrayList
            Dim myQueue As Queue = New Queue
            ' add controls to the queue
            myQueue.Enqueue(controls)

            Dim myControls As System.Windows.Forms.Control.ControlCollection
            'Dim current As Object
            Do While myQueue.Count > 0
                ' remove and return the object at the beginning of the queue
                ' current = myQueue.Dequeue()
                If TypeOf myQueue.Peek Is System.Windows.Forms.Control.ControlCollection Then
                    myControls = CType(myQueue.Dequeue, System.Windows.Forms.Control.ControlCollection)
                    If myControls.Count > 0 Then
                        For i As Integer = 0 To myControls.Count - 1
                            Dim myControl As System.Windows.Forms.Control = myControls.Item(i)
                            ' add this control to the array list
                            controlList.Add(myControl)
                            ' check if this control has a collection
                            If myControl.Controls IsNot Nothing Then
                                ' if so, add to the queue
                                myQueue.Enqueue(myControl.Controls)
                            End If
                        Next
                        'For Each myControl As System.Windows.Forms.Control In myControls
                        ' add this control to the array list
                        'controlList.Add(myControl)
                        ' check if this control has a collection
                        'If Not IsNothing(myControl.Controls) Then
                        ' if so, add to the queue
                        ' myQueue.Enqueue(myControl.Controls)
                        'End If
                        'Next
                    End If
                Else
                    myQueue.Dequeue()
                End If
            Loop
            If controlList.Count > 0 Then
                Dim allControls(controlList.Count - 1) As System.Windows.Forms.Control
                controlList.CopyTo(allControls)
                Return allControls
            Else
                Return Array.Empty(Of Control)()
            End If

        End Function

        ''' <summary>
        ''' Sets the Tool tip for all form controls that inherit a <see cref="Control">control base.</see>
        ''' </summary>
        ''' <remarks>
        ''' This is required because setting a tool tip from the parent form does not show the tool tip
        ''' if hovering above children controls hosted by the user control.
        ''' </remarks>
        ''' <param name="parent">  Reference to the parent form or control. </param>
        ''' <param name="toolTip"> The parent form or control tool tip. </param>
        <Extension()>
        Public Sub ToolTipSetter(ByVal parent As System.Windows.Forms.Control, ByVal toolTip As ToolTip)
            If parent Is Nothing Then Return
            If toolTip Is Nothing Then Return
            toolTip.SetToolTip(parent, toolTip.GetToolTip(parent))
            If parent.HasChildren Then
                Methods.ToolTipSetter(parent.Controls, toolTip)
            End If
        End Sub

        ''' <summary>
        ''' Sets the Tool tip for all form controls that inherit a <see cref="Control">control base.</see>
        ''' </summary>
        ''' <remarks>
        ''' This is required because setting a tool tip from the parent form does not show the tool tip
        ''' if hovering above children controls hosted by the user control.
        ''' </remarks>
        ''' <param name="controls"> The collection of controls. </param>
        ''' <param name="toolTip">  The parent form or control tool tip. </param>
        <Extension()>
        Public Sub ToolTipSetter(ByVal controls As System.Windows.Forms.Control.ControlCollection, ByVal toolTip As ToolTip)
            If controls Is Nothing Then Return
            If toolTip Is Nothing Then Return
            For Each control As System.Windows.Forms.Control In controls
                Methods.ToolTipSetter(control, toolTip)
            Next
        End Sub

    End Module
End Namespace
