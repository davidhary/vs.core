Imports System.Runtime.CompilerServices
Namespace WindowsFormsExtensions

    ''' <summary> Extensions methods for <see cref="System.Windows.Forms"/> types. </summary>
    ''' <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para> </remarks>
    Public Module Methods

#Region " CHECK STATE "

        ''' <summary> Converts a value to a check state. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <returns> Value as a CheckState. </returns>
        <Extension>
        Public Function ToCheckState(ByVal value As Boolean?) As CheckState
            Return If(value.HasValue, If(value.Value, CheckState.Checked, CheckState.Unchecked), CheckState.Indeterminate)
        End Function

        ''' <summary> Converts a check state to nullable Boolean. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <returns> A Boolean? </returns>
        <Extension()>
        Public Function FromCheckState(ByVal value As CheckState) As Boolean?
            Return value = CheckState.Checked OrElse value <> CheckState.Unchecked AndAlso New Boolean?
        End Function

        ''' <summary> Converts this object to a check state caption. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="value">              The value. </param>
        ''' <param name="checkedValue">       The checked value. </param>
        ''' <param name="uncheckedValue">     The unchecked value. </param>
        ''' <param name="indeterminateValue"> The indeterminate value. </param>
        ''' <returns> The given data converted to a String. </returns>
        <Extension>
        Public Function ToCheckStateCaption(ByVal value As CheckState, ByVal checkedValue As String, ByVal uncheckedValue As String, ByVal indeterminateValue As String) As String
            Return If(value = CheckState.Checked, checkedValue, If(value = CheckState.Unchecked, uncheckedValue, indeterminateValue))
        End Function

#End Region

    End Module

End Namespace
