﻿Imports System.Drawing
Imports System.Runtime.CompilerServices
Imports System.Windows.Forms
Namespace CompactExtensions
    ''' <summary> Includes extensions for <see cref="String">String</see> Compacting. </summary>
    ''' <remarks> (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 04/09/2009, 1.1.3386.x. </para></remarks>
    Public Module Methods

        ''' <summary>
        ''' Returns a substring of the input string taking not of start index and length exceptions.
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="value">      The input string to sub string. </param>
        ''' <param name="startIndex"> The zero based starting index. </param>
        ''' <param name="length">     The total length. </param>
        ''' <returns>
        ''' A substring of the input string taking not of start index and length exceptions.
        ''' </returns>
        <Extension()>
        Public Function SafeSubstring(ByVal value As String, ByVal startIndex As Integer, ByVal length As Integer) As String

            If String.IsNullOrWhiteSpace(value) OrElse length <= 0 Then Return String.Empty

            Dim inputLength As Integer = value.Length
            If startIndex > inputLength Then
                Return String.Empty
            Else
                If startIndex + length > inputLength Then
                    length = inputLength - startIndex
                End If
                Return value.Substring(startIndex, length)
            End If

        End Function

        ''' <summary> Adds an ellipsis to the text. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="value">     Specifies the string to compact. </param>
        ''' <param name="format">    Specifies the desired <see cref="TextFormatFlags">formatting</see> </param>
        ''' <param name="trimCount"> Number of characters to trim. </param>
        ''' <returns> A String. </returns>
        Private Function AddEllipsis(ByVal value As String, ByVal format As TextFormatFlags, ByVal trimCount As Integer) As String
            If String.IsNullOrWhiteSpace(value) OrElse value.Length < trimCount Then
                Return value
            ElseIf value.Length = trimCount Then
                Return String.Empty
            ElseIf 0 <> (format And TextFormatFlags.PathEllipsis) Then
                ' center ellipsis
                Dim length As Integer = value.Length
                value = String.Join("...", value.SafeSubstring(0, length \ 2), value.SafeSubstring(length \ 2, length - length \ 2))
                Dim trimRight As Boolean = True
                Do Until value.Length <= length - trimCount
                    value = If(trimRight, value.SafeSubstring(0, value.Length - 1), value.SafeSubstring(1, value.Length - 1))
                    trimRight = Not trimRight
                Loop
                Return value
            ElseIf 0 <> (format And TextFormatFlags.EndEllipsis) Then
                Return $"{value.SafeSubstring(0, value.Length - trimCount)}..."
            Else
                Return $"...{value.SafeSubstring(trimCount, value.Length - trimCount)}"
            End If
        End Function

        ''' <summary>
        ''' Compacts the string to permit display within the given width. For example, using
        ''' <see cref="TextFormatFlags.PathEllipsis">Path Ellipsis</see>
        ''' as the <paramref name="format">format instruction</paramref>
        ''' the string <c>c:\program files\test app\RunMe.exe' might turn into 'c:\program files\...\
        ''' RunMe.exe'</c> depending on the font and width.
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        '''                                                are null. </exception>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        '''                                                the required range. </exception>
        ''' <param name="value">  Specifies the string to compact. </param>
        ''' <param name="width">  Specifies the width. </param>
        ''' <param name="font">   Specifies the <see cref="Drawing.Font">font</see> </param>
        ''' <param name="format"> Specifies the desired <see cref="TextFormatFlags">formatting</see> </param>
        ''' <returns> Compacted string. </returns>
        <Extension()>
        Public Function Compact(ByVal value As String, ByVal width As Integer, ByVal font As Drawing.Font, ByVal format As TextFormatFlags) As String
            If String.IsNullOrWhiteSpace(value) Then
                Return String.Empty
            End If
            If font Is Nothing Then
                Throw New ArgumentNullException(NameOf(font))
            End If
            If width <= 0 Then
                Throw New ArgumentOutOfRangeException(NameOf(width), width, "Must be non-zero")
            End If
            Dim result As String = String.Copy(value)
            Dim i As Integer = 0
            Do While System.Windows.Forms.TextRenderer.MeasureText(result, font, New Drawing.Size(width, font.Height), TextFormatFlags.Default).Width > width
                i += 1
                result = Methods.AddEllipsis(value, format, i)
            Loop
            Return result
        End Function

        ''' <summary>
        ''' Compacts the string to permit display within the given width. For example, the string
        ''' <c>'c:\program files\test app\RunMe.exe' might turn into 'c:\program files\...\RunMe.exe'</c>
        ''' depending on the font and width.
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="value"> Specifies the string to compact. </param>
        ''' <param name="width"> Specifies the width. </param>
        ''' <param name="font">  Specifies the <see cref="Drawing.Font">font</see> </param>
        ''' <returns> Compacted string. </returns>
        <Extension()>
        Public Function Compact(ByVal value As String, ByVal width As Integer, ByVal font As Drawing.Font) As String
            Return value.Compact(width, font, TextFormatFlags.PathEllipsis)
        End Function

        ''' <summary>
        ''' Compacts the string to permit display within the given control. For example, using
        ''' <see cref="TextFormatFlags.PathEllipsis">Path Ellipsis</see>
        ''' as the <paramref name="format">format instruction</paramref>
        ''' the string <c>'c:\program files\test app\RunMe.exe' might turn into 'c:\program files\...\
        ''' RunMe.exe'</c> depending on the font and width.
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value">   Specifies the text. </param>
        ''' <param name="control"> Specifies the control within which to format the text. </param>
        ''' <param name="format">  Specifies the desired <see cref="TextFormatFlags">formatting</see> </param>
        ''' <returns> Compacted string. </returns>
        <Extension()>
        Public Function Compact(ByVal value As String, ByVal control As System.Windows.Forms.Control, ByVal format As TextFormatFlags) As String
            If String.IsNullOrWhiteSpace(value) Then
                Return String.Empty
            End If
            If control Is Nothing Then
                Throw New ArgumentNullException(NameOf(control))
            End If
            Return If(control.AutoSize OrElse control.Width = 0, value, value.Compact(control.Width, control.Font, format))
        End Function

        ''' <summary>
        ''' Compacts the string to permit display within the given control. For example, the string
        ''' <c>'c:\program files\test app\RunMe.exe' might turn into 'c:\program files\...\RunMe.exe'</c>
        ''' depending on the font and width.
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value">   Specifies the string to compact. </param>
        ''' <param name="control"> Specifies the control receiving the string. </param>
        ''' <returns> Compacted string. </returns>
        <Extension()>
        Public Function Compact(ByVal value As String, ByVal control As Control) As String
            If String.IsNullOrWhiteSpace(value) Then
                Return String.Empty
            End If
            If control Is Nothing Then
                Throw New ArgumentNullException(NameOf(control))
            End If
            Return value.Compact(control, TextFormatFlags.PathEllipsis)
        End Function

        ''' <summary>
        ''' Compacts the string to permit display within the given width. For example, the string
        ''' <c>'c:\program files\test app\RunMe.exe' might turn into 'c:\program files\...\RunMe.exe'</c>
        ''' depending on the font and width.
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value">   Specifies the string to compact. </param>
        ''' <param name="control"> Specifies the control within which to format the text. </param>
        ''' <param name="format">  Specifies the desired <see cref="TextFormatFlags">formatting</see> </param>
        ''' <returns> Compacted string. </returns>
        <Extension()>
        Public Function Compact(ByVal value As String, ByVal control As ToolStripItem, ByVal format As TextFormatFlags) As String
            If String.IsNullOrWhiteSpace(value) Then
                Return String.Empty
            End If
            If control Is Nothing Then
                Throw New ArgumentNullException(NameOf(control))
            End If
            Return If(control.AutoSize OrElse control.Width = 0, value, value.Compact(control.Width, control.Font, format))
        End Function

        ''' <summary>
        ''' Compacts the string to permit display within the given control. For example, the string
        ''' <c>'c:\program files\test app\RunMe.exe' might turn into 'c:\program files\...\RunMe.exe'</c>
        ''' depending on the font and width.
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value">   Specifies the string to compact. </param>
        ''' <param name="control"> Specifies the control within which to format the text. </param>
        ''' <returns> Compacted string. </returns>
        <Extension()>
        Public Function Compact(ByVal value As String, ByVal control As ToolStripItem) As String
            If String.IsNullOrWhiteSpace(value) Then
                Return String.Empty
            End If
            If control Is Nothing Then
                Throw New ArgumentNullException(NameOf(control))
            End If
            Return value.Compact(control, TextFormatFlags.PathEllipsis)
        End Function

    End Module
End Namespace
