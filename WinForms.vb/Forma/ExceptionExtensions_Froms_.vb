Imports System.Runtime.CompilerServices

Namespace ExceptionExtensions

    ''' <summary> Full blown exception reporting extension methods. </summary>
    ''' <remarks> (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para> </remarks>
    Public Module Methods

        ''' <summary> Adds an exception data to 'Exception'. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="exception"> The exception. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        <Extension>
        Public Function AddExceptionData(ByVal exception As System.Exception) As Boolean
            Dim affirmative As Boolean = False
            affirmative = affirmative OrElse isr.Core.Controls.ExceptionExtensions.Methods.AddExceptionData(exception)
            Return affirmative
        End Function

        ''' <summary> Converts a value to a full blown string. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <returns> Value as a String. </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function ToFullBlownString(ByVal value As System.Exception) As String
            Return Methods.ToFullBlownString(value, Integer.MaxValue)
        End Function

        ''' <summary> Converts this object to a full blown string. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <param name="level"> The level. </param>
        ''' <returns> The given data converted to a String. </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function ToFullBlownString(ByVal value As System.Exception, ByVal level As Integer) As String
            Return isr.Core.ExceptionExtensions.Methods.ToFullBlownString(value, level, AddressOf Methods.AddExceptionData)
        End Function

    End Module
End Namespace

