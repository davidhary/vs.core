Imports System.Runtime.CompilerServices
Namespace PropertyDescriptorExtensions

    Public Module Methods

        ''' <summary> Gets list item properties. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="listAccessors"> The list accessors. </param>
        ''' <returns> The list item properties. </returns>
        <Extension>
        Public Function GetListItemProperties(Of T)(ByVal listAccessors() As ComponentModel.PropertyDescriptor) As ComponentModel.PropertyDescriptorCollection
            Return Methods.GetListItemProperties(listAccessors, GetType(T))
        End Function

        ''' <summary> Gets list item properties. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="listAccessors"> The list accessors. </param>
        ''' <param name="type">          The type. </param>
        ''' <returns> The list item properties. </returns>
        <Extension>
        Public Function GetListItemProperties(ByVal listAccessors() As ComponentModel.PropertyDescriptor, ByVal type As Type) As ComponentModel.PropertyDescriptorCollection
            Return System.Windows.Forms.ListBindingHelper.GetListItemProperties(type, listAccessors)
        End Function

    End Module

End Namespace

