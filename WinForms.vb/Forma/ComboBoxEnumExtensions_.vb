Imports System.Runtime.CompilerServices

Imports isr.Core.EnumExtensions
Namespace ComboBoxEnumExtensions

    ''' <summary> Encapsulates the Form related functionality of the <see cref="isr.Core.EnumExtender"/></summary>
    ''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 3/25/2015 </para><para>
    ''' David, 05/04/2008" by="Stewart and StageSoft" revision="1.2.3411.x"> <para>
    ''' From Strong-Type and Efficient .NET Enums By Hanna GIAT</para><para>
    ''' http://www.CodeProject.com/KB/cs/efficient_strong_enum.aspx. </para> </para></remarks>
    Public Module Methods

#Region " COMBO BOX ENUM VALUE SELECTION "

        ''' <summary> Returns the selected enum value. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="comboBox">     The combo box control. </param>
        ''' <param name="defaultValue"> The default value. </param>
        ''' <returns> The selected Enum. </returns>
        <Extension>
        Public Function SelectedEnumValue(ByVal comboBox As Windows.Forms.ComboBox, ByVal defaultValue As System.Enum) As System.Enum
            If comboBox IsNot Nothing AndAlso comboBox.SelectedItem IsNot Nothing AndAlso defaultValue IsNot Nothing Then
                Dim kvp As KeyValuePair(Of System.Enum, String) = CType(comboBox.SelectedItem, KeyValuePair(Of System.Enum, String))
                If [Enum].IsDefined(defaultValue.GetType, kvp.Key) Then defaultValue = kvp.Key
            End If
            Return defaultValue
        End Function

        ''' <summary> Returns the selected enum value. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="listControl"> The list control. </param>
        ''' <returns> The selected Enum. </returns>
        <Extension>
        Public Function SelectedEnumValue(Of T)(ByVal listControl As System.Windows.Forms.ListControl) As T
            If listControl Is Nothing Then Throw New ArgumentNullException(NameOf(listControl))
            Return CType(listControl.SelectedValue, T)
        End Function

        ''' <summary> Returns the selected enum value. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="listControl">  The list control. </param>
        ''' <param name="defaultValue"> The default value. </param>
        ''' <returns> The selected Enum. </returns>
        <Extension>
        Public Function SelectedEnumValue(Of T)(ByVal listControl As System.Windows.Forms.ListControl, ByVal defaultValue As T) As T
            If listControl Is Nothing Then Throw New ArgumentNullException(NameOf(listControl))
            Return If([Enum].IsDefined(GetType(T), listControl.SelectedValue), CType(listControl.SelectedValue, T), defaultValue)
        End Function

        ''' <summary> Returns the selected enum value. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="comboBox">     The tool strip combo box control. </param>
        ''' <param name="defaultValue"> The default value. </param>
        ''' <returns> The selected Enum. </returns>
        <Extension>
        Public Function SelectedEnumValue(ByVal comboBox As Windows.Forms.ToolStripComboBox, ByVal defaultValue As System.Enum) As System.Enum
            If comboBox IsNot Nothing AndAlso comboBox.SelectedItem IsNot Nothing AndAlso defaultValue IsNot Nothing Then
                Dim kvp As KeyValuePair(Of System.Enum, String) = CType(comboBox.SelectedItem, KeyValuePair(Of System.Enum, String))
                If [Enum].IsDefined(defaultValue.GetType, kvp.Key) Then defaultValue = kvp.Key
            End If
            Return defaultValue
        End Function

        ''' <summary> Returns the selected enum value. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox"> The tool strip combo box control. </param>
        ''' <returns> The selected Enum. </returns>
        <Extension>
        Public Function SelectedEnumValue(Of T)(ByVal comboBox As System.Windows.Forms.ToolStripComboBox) As T
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            Return CType(comboBox.ComboBox.SelectedValue, T)
        End Function

        ''' <summary> Returns the selected enum value. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox">     The tool strip combo box control. </param>
        ''' <param name="defaultValue"> The default value. </param>
        ''' <returns> The selected Enum. </returns>
        <Extension>
        Public Function SelectedEnumValue(Of T)(ByVal comboBox As System.Windows.Forms.ToolStripComboBox, ByVal defaultValue As T) As T
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            Return If([Enum].IsDefined(GetType(T), comboBox.ComboBox.SelectedValue), CType(comboBox.ComboBox.SelectedValue, T), defaultValue)
        End Function

        ''' <summary> Returns the selected enum item. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox"> The combo box control. </param>
        ''' <returns> A KeyValuePair(Of T, String) </returns>
        <Extension>
        Public Function SelectedEnumItem(Of T)(ByVal comboBox As System.Windows.Forms.ComboBox) As KeyValuePair(Of T, String)
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            Return CType(comboBox.SelectedItem, KeyValuePair(Of T, String))
        End Function

        ''' <summary> Returns the selected enum item. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="toolStripComboBox"> The tool strip combo box control. </param>
        ''' <returns> A KeyValuePair(Of T, String) </returns>
        <Extension>
        Public Function SelectedEnumItem(Of T)(ByVal toolStripComboBox As Windows.Forms.ToolStripComboBox) As KeyValuePair(Of T, String)
            If toolStripComboBox Is Nothing Then Throw New ArgumentNullException(NameOf(toolStripComboBox))
            Return CType(toolStripComboBox.SelectedItem, KeyValuePair(Of T, String))
        End Function

        ''' <summary> Selects an item from the control and return the selected value. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox"> The combo box control. </param>
        ''' <param name="item">     The item. </param>
        ''' <returns> A KeyValuePair(Of T, String) </returns>
        <Extension>
        Public Function SelectItem(Of T)(ByVal comboBox As System.Windows.Forms.ComboBox, ByVal item As KeyValuePair(Of T, String)) As KeyValuePair(Of T, String)
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            comboBox.SelectedItem = item
            Return CType(comboBox.SelectedItem, KeyValuePair(Of T, String))
        End Function

        ''' <summary> Selects a value and return the selected item. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox"> The combo box control. </param>
        ''' <param name="value">    Specifies the bit values which to add. </param>
        ''' <returns> A KeyValuePair(Of T, String) </returns>
        <Extension>
        Public Function SelectValue(Of T)(ByVal comboBox As System.Windows.Forms.ComboBox, ByVal value As T) As KeyValuePair(Of T, String)
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            comboBox.SelectedValue = value
            Return CType(comboBox.SelectedItem, KeyValuePair(Of T, String))
        End Function

        ''' <summary> Selects a combo box value and return the selected item. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox"> The combo box control. </param>
        ''' <param name="value">    Specifies the bit values which to add. </param>
        ''' <returns> A KeyValuePair(Of T, String) </returns>
        <Extension>
        Public Function SelectValue(ByVal comboBox As System.Windows.Forms.ComboBox, ByVal value As System.Enum) As KeyValuePair(Of System.Enum, String)
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            comboBox.SelectedValue = value
            Return CType(comboBox.SelectedItem, KeyValuePair(Of System.Enum, String))
        End Function

        ''' <summary> Selects a combo box value and return the selected item. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox"> The combo box control. </param>
        ''' <param name="value">    Specifies the bit values which to add. </param>
        ''' <returns> A KeyValuePair(Of T, String) </returns>
        <Extension>
        Public Function SelectValue(Of T)(ByVal comboBox As System.Windows.Forms.ToolStripComboBox, ByVal value As T) As KeyValuePair(Of T, String)
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            comboBox.ComboBox.SelectedValue = value
            Return CType(comboBox.SelectedItem, KeyValuePair(Of T, String))
        End Function

        ''' <summary> Selects a combo box value and return the selected item. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox"> The combo box control. </param>
        ''' <param name="value">    Specifies the bit values which to add. </param>
        ''' <returns> A KeyValuePair(Of T, String) </returns>
        <Extension>
        Public Function SelectValue(ByVal comboBox As System.Windows.Forms.ToolStripComboBox, ByVal value As System.Enum) As KeyValuePair(Of System.Enum, String)
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            comboBox.ComboBox.SelectedValue = value
            Return CType(comboBox.SelectedItem, KeyValuePair(Of System.Enum, String))
        End Function

        ''' <summary> Select value. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="control"> The control. </param>
        ''' <param name="value">   Specifies the bit values which to add. </param>
        <Extension>
        Public Sub SelectValue(Of T)(ByVal control As System.Windows.Forms.ListControl, ByVal value As T)
            If control Is Nothing Then Throw New ArgumentNullException(NameOf(control))
            control.SelectedValue = Methods.ToInteger(value)
        End Sub

        ''' <summary> Selects a value and return the selected item. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="control"> The control. </param>
        ''' <param name="value">   Specifies the bit values which to add. </param>
        ''' <returns> A KeyValuePair(Of T, String) </returns>
        <Extension>
        Public Function SelectValue(ByVal control As System.Windows.Forms.ListControl, ByVal value As System.Enum) As System.Enum
            If control Is Nothing Then Throw New ArgumentNullException(NameOf(control))
            control.SelectedValue = value
            Return CType(control.SelectedValue, System.Enum)
        End Function

#End Region

#Region " GENERTIC LIST CONTROL ENUM FUNCTIONS: LIST "

        ''' <summary> List enum names. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="listControl"> The list control. </param>
        ''' <returns> An Integer. </returns>
        <Extension>
        Public Function ListEnumNames(Of T)(ByVal listControl As System.Windows.Forms.ListControl) As Integer
            Return Methods.ListEnums(listControl, GetType(T).ValueNamePairs)
        End Function

        ''' <summary> Lists the enum names filtered by the provided masks. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="listControl"> The list control. </param>
        ''' <param name="includeMask"> The include mask. </param>
        ''' <param name="excludeMask"> The exclude mask. </param>
        <Extension>
        Public Sub ListEnumNames(ByVal listControl As Windows.Forms.ListControl, ByVal includeMask As System.Enum, ByVal excludeMask As System.Enum)
            Methods.ListEnums(listControl, includeMask.EnumValues.Filter(includeMask, excludeMask).ValueNamePairs)
        End Sub

        ''' <summary> Lists the enum names filtered by the provided masks. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="listControl"> The list control. </param>
        ''' <param name="includeMask"> The include mask. </param>
        ''' <param name="excludeMask"> The exclude mask. </param>
        <Extension>
        Public Sub ListEnumNames(Of T)(ByVal listControl As Windows.Forms.ListControl, ByVal includeMask As T, ByVal excludeMask As T)
            Methods.ListEnums(listControl, GetType(T).EnumValues(Of T).Filter(includeMask, excludeMask).ValueNamePairs)
        End Sub

        ''' <summary> List enum descriptions. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="listControl"> The list control. </param>
        ''' <returns> An Integer. </returns>
        <Extension>
        Public Function ListEnumDescriptions(Of T)(ByVal listControl As System.Windows.Forms.ListControl) As Integer
            Return Methods.ListEnums(listControl, GetType(T).ValueDescriptionPairs)
        End Function

        ''' <summary> Lists the enum descriptions. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="listControl"> The list control. </param>
        ''' <param name="includeMask"> The include mask. </param>
        ''' <param name="excludeMask"> The exclude mask. </param>
        <Extension>
        Public Sub ListEnumDescriptions(ByVal listControl As Windows.Forms.ListControl, ByVal includeMask As System.Enum, ByVal excludeMask As System.Enum)
            Methods.ListEnums(listControl, includeMask.EnumValues.Filter(includeMask, excludeMask).ValueDescriptionPairs)
        End Sub

        ''' <summary> Lists the enum descriptions. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="listControl"> The list control. </param>
        ''' <param name="includeMask"> The include mask. </param>
        ''' <param name="excludeMask"> The exclude mask. </param>
        <Extension>
        Public Sub ListEnumDescriptions(Of T)(ByVal listControl As Windows.Forms.ListControl, ByVal includeMask As T, ByVal excludeMask As T)
            Methods.ListEnums(listControl, GetType(T).EnumValues(Of T).Filter(includeMask, excludeMask).ValueDescriptionPairs)
        End Sub

        ''' <summary> List enum value and string pairs. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="listControl">   The list control. </param>
        ''' <param name="enumTextPairs"> The enum text pairs. </param>
        ''' <returns> An Integer. </returns>
        <Extension>
        Public Function ListEnums(ByVal listControl As System.Windows.Forms.ListControl, ByVal enumTextPairs As IEnumerable(Of KeyValuePair(Of [Enum], String))) As Integer
            If listControl Is Nothing Then Throw New ArgumentNullException(NameOf(listControl))
            If enumTextPairs Is Nothing Then Throw New ArgumentNullException(NameOf(enumTextPairs))
            Dim comboEnabled As Boolean = listControl.Enabled
            listControl.Enabled = False
            listControl.DataSource = Nothing
            listControl.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
            listControl.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
            ' converting to array ensures that values displayed in two controls are not connected.
            listControl.DataSource = enumTextPairs.ToList
            listControl.SelectedIndex = If(enumTextPairs.Any, 0, -1)
            listControl.Enabled = comboEnabled
            listControl.Invalidate()
            Return enumTextPairs.Count
        End Function

        ''' <summary> List enum value and string pairs. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="listControl">   The list control. </param>
        ''' <param name="enumTextPairs"> The enum text pairs. </param>
        ''' <returns> An Integer. </returns>
        <Extension>
        Public Function ListEnums(Of T)(ByVal listControl As System.Windows.Forms.ListControl, ByVal enumTextPairs As IEnumerable(Of KeyValuePair(Of T, String))) As Integer
            If listControl Is Nothing Then Throw New ArgumentNullException(NameOf(listControl))
            If enumTextPairs Is Nothing Then Throw New ArgumentNullException(NameOf(enumTextPairs))
            Dim comboEnabled As Boolean = listControl.Enabled
            listControl.Enabled = False
            listControl.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
            listControl.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
            ' converting to array ensures that values displayed in two controls are not connected.
            listControl.DataSource = enumTextPairs.ToList
            listControl.SelectedIndex = If(enumTextPairs.Any, 0, -1)
            listControl.Enabled = comboEnabled
            listControl.Invalidate()
            Return enumTextPairs.Count
        End Function

#End Region

#Region " GENERTIC LIST CONTROL DICTIONARY FUNCTIONS "

        ''' <summary> List values. </summary>
        ''' <remarks>
        ''' The first step is to add a description attribute to your Enum.
        ''' <code>
        ''' Public Enum SimpleEnum2
        ''' [System.ComponentModel.Description("Today")] Today
        ''' [System.ComponentModel.Description("Last 7 days")] Last7
        ''' [System.ComponentModel.Description("Last 14 days")] Last14
        ''' [System.ComponentModel.Description("Last 30 days")] Last30
        ''' [System.ComponentModel.Description("All")] All
        ''' End Enum
        ''' Public SimpleEfficientEnum As New EnumExtender(of SimpleEnum2)
        ''' Dim combo As ComboBox = New ComboBox()
        ''' combo.DataSource = SimpleEfficientEnum.GetValueDescriptionPairs(GetType(SimpleEnum2)).ToList
        ''' combo.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
        ''' combo.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
        ''' dim selectedEnum as SimpleEnum2 = SimpleEfficientEnum.ToObjectFromPair(combo.SelectedItem)
        ''' selectedEnum = combo.selectedValue(of SimpleEnum2)(SimpleEfficientEnum.ValueEnumPairs)
        ''' </code>
        ''' This works with any control that supports data binding, including the
        ''' <see cref="System.Windows.Forms.ToolStripComboBox">tool strip combo box</see>,
        ''' although you will need to cast the
        ''' <see cref="System.Windows.Forms.ToolStripComboBox.Control">control property</see>
        ''' to a <see cref="System.Windows.Forms.ComboBox">Combo Box</see> to get to the DataSource
        ''' property. In that case, you will also want to perform the same cast when you are referencing
        ''' the selected value to work with it as your Enum type.
        ''' </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="listControl">   The list control. </param>
        ''' <param name="enumTextPairs"> The enum text pairs. </param>
        ''' <returns> The item count. </returns>
        <Extension>
        Public Function ListValues(Of T)(ByVal listControl As System.Windows.Forms.ListControl, ByVal enumTextPairs As IDictionary(Of T, String)) As Integer
            If listControl Is Nothing Then Throw New ArgumentNullException(NameOf(listControl))
            If enumTextPairs Is Nothing Then Throw New ArgumentNullException(NameOf(enumTextPairs))
            Dim comboEnabled As Boolean = listControl.Enabled
            listControl.Enabled = False
            listControl.DataSource = Nothing
            listControl.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
            listControl.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
            ' converting to array ensures that values displayed in two controls are not connected.
            listControl.DataSource = enumTextPairs.ToList
            listControl.SelectedIndex = If(enumTextPairs.Any, 0, -1)
            listControl.Enabled = comboEnabled
            listControl.Invalidate()
            Return enumTextPairs.Count
        End Function

        ''' <summary> Selected value. </summary>
        ''' <remarks> David, 2020-09-04. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox">       The combo box control. </param>
        ''' <param name="valueEnumPairs"> The value enum pairs. </param>
        ''' <returns> The selected Enum. </returns>
        <Extension>
        Public Function SelectedValue(Of T)(ByVal comboBox As System.Windows.Forms.ComboBox, ByVal valueEnumPairs As IDictionary(Of Integer, T)) As T
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            Return Methods.ToObject(valueEnumPairs, comboBox.SelectedItem)
        End Function

        ''' <summary> Returns the value converted to Enum from the given value name pair. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="values"> The values. </param>
        ''' <param name="value">  The value. </param>
        ''' <returns> converted to Enum from the given value name pair. </returns>
        <Extension>
        Public Function ToObject(Of T)(ByVal values As IDictionary(Of Integer, T), ByVal value As Object) As T
            If value Is Nothing Then Return Nothing
            Return Methods.ToObject(values, CType(value, KeyValuePair(Of T, String)).Key)
        End Function

        ''' <summary> Converts a value to an integer. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
        '''                                          illegal values. </exception>
        ''' <param name="value"> The value. </param>
        ''' <returns> value as an Integer. </returns>
        <Extension>
        Public Function ToInteger(ByVal value As Object) As Integer
            If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
            Try
                Return CInt(Microsoft.VisualBasic.Fix(value))
            Catch e As InvalidCastException
                Throw New ArgumentException($"Cannot convert {value} to integer", NameOf(value))
            End Try
        End Function

#End Region

#Region " ENUM EXTENDER "

        ''' <summary> List values. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="listControl">  The list control. </param>
        ''' <param name="enumExtender"> The enum extender. </param>
        ''' <returns> The item count. </returns>
        <Extension>
        Public Function ListValues(Of T)(ByVal listControl As System.Windows.Forms.ListControl, ByVal enumExtender As EnumExtender(Of T)) As Integer
            If enumExtender Is Nothing Then Throw New ArgumentNullException(NameOf(enumExtender))
            Return listControl.ListValues(Of T)(enumExtender.ValueDescriptionPairs)
        End Function

        ''' <summary> Selected value. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox">     The combo box control. </param>
        ''' <param name="enumExtender"> The enum extender. </param>
        ''' <returns> The selected Enum. </returns>
        <Extension>
        Public Function SelectedValue(Of T)(ByVal comboBox As System.Windows.Forms.ComboBox, ByVal enumExtender As EnumExtender(Of T)) As T
            If enumExtender Is Nothing Then Throw New ArgumentNullException(NameOf(enumExtender))
            Return comboBox.SelectedValue(Of T)(enumExtender.ValueEnumValuePairs)
        End Function

#End Region

    End Module

End Namespace
