Imports System.Runtime.CompilerServices
Imports System.ComponentModel
Namespace ComboBoxBindingExtensions

    ''' <summary> Extensions methods binding combo box to a <see cref="ComponentModel.Bindinglist"/>A methods. </summary>
    ''' <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para> </remarks>
    Public Module Methods

        ''' <summary>
        ''' Displays a list of resource name selecting the existing name of existing in the list.
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox">      The combo box. </param>
        ''' <param name="resourceNames"> List of names of the resources. </param>
        <Extension>
        Public Sub DisplayResourceNames(ByVal comboBox As Windows.Forms.ComboBox, ByVal resourceNames As BindingList(Of String))
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            If resourceNames Is Nothing Then Throw New ArgumentNullException(NameOf(resourceNames))
            If resourceNames.Any Then
                Dim resourceName As String = comboBox.Text
                comboBox.DataSource = Nothing
                comboBox.Items.Clear()
                comboBox.DataSource = resourceNames
                If resourceNames.Contains(resourceName) Then comboBox.Text = resourceName
            End If
        End Sub
    End Module

End Namespace
