Imports System.Runtime.CompilerServices
Imports System.Windows.Forms

Imports isr.Core.EnumExtensions
Namespace EnumExtensions

    Partial Public Module Methods

        ''' <summary> Displays a value description pair. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="type">     The type. </param>
        ''' <param name="comboBox"> The combo box. </param>
        ''' <returns> An Integer. </returns>
        <Extension()>
        Public Function DisplayValueDescriptionPairs(ByVal type As Type, ByVal comboBox As ComboBox) As Integer
            If type Is Nothing Then Throw New ArgumentNullException(NameOf(type))
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            comboBox.DataSource = Nothing
            comboBox.Items.Clear()
            comboBox.DataSource = type.ValueDescriptionPairs.ToBindingList
            comboBox.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
            comboBox.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
            Return comboBox.Items.Count
        End Function

        ''' <summary> Displays a value description pair. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="enumItem"> An enum item representing the enumeration option. </param>
        ''' <param name="comboBox"> The combo box. </param>
        ''' <returns> An Integer. </returns>
        <Extension()>
        Public Function DisplayValueDescriptionPairs(ByVal enumItem As System.Enum, ByVal comboBox As ComboBox) As Integer
            If enumItem Is Nothing Then Throw New ArgumentNullException(NameOf(enumItem))
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            comboBox.DataSource = Nothing
            comboBox.Items.Clear()
            comboBox.DataSource = enumItem.ValueDescriptionPairs.ToBindingList
            comboBox.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
            comboBox.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
            Return comboBox.Items.Count
        End Function

        ''' <summary> Displays a value description pair. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="enumConstant"> The <see cref="T:System.Enum"/> constant. </param>
        ''' <param name="comboBox">     The combo box. </param>
        ''' <returns> An Integer. </returns>
        <Extension()>
        Public Function DisplayValueDescriptionPairs(ByVal enumConstant As System.Enum, ByVal comboBox As ToolStripComboBox) As Integer
            If enumConstant Is Nothing Then Throw New ArgumentNullException(NameOf(enumConstant))
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            Methods.DisplayValueDescriptionPairs(enumConstant, TryCast(comboBox.Control, ComboBox))
            Return comboBox.Items.Count
        End Function

    End Module
End Namespace
