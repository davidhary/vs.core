Imports System.Runtime.CompilerServices
Imports System.Windows.Forms
Namespace BindingExtensions

    ''' <summary>
    ''' Contains extension methods for <see cref="BindingsCollection">data binding</see>.
    ''' </summary>
    ''' <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para> </remarks>
    Public Module Methods

#Region " PARSE and DISPLAY "

        ''' <summary>
        ''' Toggle image; converts the event arguments value from boolean to true or false image.
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="convertEventArgs"> Convert event information. </param>
        ''' <param name="trueImage">        The true image. </param>
        ''' <param name="falseImage">       The false image. </param>
        <Extension()>
        Public Sub ToggleImage(ByVal convertEventArgs As ConvertEventArgs, ByVal trueImage As Drawing.Image, ByVal falseImage As Drawing.Image)
            If convertEventArgs IsNot Nothing AndAlso convertEventArgs.DesiredType Is GetType(Drawing.Image) Then
                convertEventArgs.Value = If(CType(convertEventArgs.Value, Boolean), trueImage, falseImage)
            End If
        End Sub

        ''' <summary>
        ''' Toggles caption; converts the event arguments value from boolean to true or false text value.
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="convertEventArgs"> Convert event information. </param>
        ''' <param name="trueValue">        The true value. </param>
        ''' <param name="falseValue">       The false value. </param>
        <Extension()>
        Public Sub ToggleCaption(ByVal convertEventArgs As ConvertEventArgs, ByVal trueValue As String, ByVal falseValue As String)
            If convertEventArgs IsNot Nothing AndAlso convertEventArgs.DesiredType Is GetType(String) Then
                convertEventArgs.Value = If(CType(convertEventArgs.Value, Boolean), trueValue, falseValue)
            End If
        End Sub

        ''' <summary> Displays <see cref="DateTimeOffset"/> as local date. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="convertEventArgs"> Convert event information. </param>
        <Extension()>
        Public Sub DisplayDateTimeOffsetDate(ByVal convertEventArgs As ConvertEventArgs)
            If convertEventArgs IsNot Nothing AndAlso convertEventArgs.DesiredType Is GetType(String) Then
                convertEventArgs.Value = CType(convertEventArgs.Value, DateTimeOffset).ToLocalTime.ToString("d")
            End If
        End Sub

        ''' <summary> Displays a date time offset time described by convertEventArgs. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="convertEventArgs"> Convert event information. </param>
        <Extension()>
        Public Sub DisplayDateTimeOffsetTime(ByVal convertEventArgs As ConvertEventArgs)
            If convertEventArgs IsNot Nothing AndAlso convertEventArgs.DesiredType Is GetType(String) Then
                convertEventArgs.Value = CType(convertEventArgs.Value, DateTimeOffset).ToLocalTime.ToString("t")
            End If
        End Sub

        ''' <summary> Displays a date time offset described by convertEventArgs. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="convertEventArgs"> Convert event information. </param>
        <Extension()>
        Public Sub DisplayDateTimeOffset(ByVal convertEventArgs As ConvertEventArgs)
            If convertEventArgs IsNot Nothing AndAlso convertEventArgs.DesiredType Is GetType(String) Then
                convertEventArgs.Value = CType(convertEventArgs.Value, DateTimeOffset).ToLocalTime.ToString("0")
            End If
        End Sub

        ''' <summary> Parse a local time string to date time offset. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="convertEventArgs"> Convert event information. </param>
        <Extension()>
        Public Sub ParseDateTimeOffset(ByVal convertEventArgs As ConvertEventArgs)
            If convertEventArgs IsNot Nothing AndAlso convertEventArgs.DesiredType Is GetType(Date) Then
                Dim value As DateTimeOffset = DateTimeOffset.Now
                If DateTimeOffset.TryParse(convertEventArgs.Value.ToString, value) Then
                End If
                convertEventArgs.Value = value
            End If
        End Sub

        ''' <summary> Displays universal time value as local date. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="convertEventArgs"> Convert event information. </param>
        <Extension()>
        Public Sub DisplayLocalDate(ByVal convertEventArgs As ConvertEventArgs)
            If convertEventArgs IsNot Nothing AndAlso convertEventArgs.DesiredType Is GetType(String) Then
                convertEventArgs.Value = CType(convertEventArgs.Value, Date).ToLocalTime.ToShortDateString
            End If
        End Sub

        ''' <summary> Displays universal time value as local time. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="convertEventArgs"> Convert event information. </param>
        <Extension()>
        Public Sub DisplayLocalTime(ByVal convertEventArgs As ConvertEventArgs)
            If convertEventArgs IsNot Nothing AndAlso convertEventArgs.DesiredType Is GetType(String) Then
                convertEventArgs.Value = CType(convertEventArgs.Value, DateTimeOffset).ToLocalTime.ToString("T")
            End If
        End Sub

        ''' <summary> Parse a local time string to universal time. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="convertEventArgs"> Convert event information. </param>
        <Extension()>
        Public Sub ParseLocalTime(ByVal convertEventArgs As ConvertEventArgs)
            If convertEventArgs IsNot Nothing AndAlso convertEventArgs.DesiredType Is GetType(Date) Then
                Dim value As DateTimeOffset = DateTimeOffset.Now
                convertEventArgs.Value = If(DateTimeOffset.TryParse(convertEventArgs.Value.ToString, value), value.ToUniversalTime, value.ToUniversalTime)
            End If
        End Sub

        ''' <summary> Displays a timespan described by convertEventArgs. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="convertEventArgs"> Convert event information. </param>
        <Extension()>
        Public Sub DisplayTimespan(ByVal convertEventArgs As ConvertEventArgs)
            If convertEventArgs IsNot Nothing AndAlso convertEventArgs.DesiredType Is GetType(String) Then
                convertEventArgs.Value = CType(convertEventArgs.Value, TimeSpan).ToString()
            End If
        End Sub

        ''' <summary> Displays a timespan described by convertEventArgs. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="convertEventArgs"> Convert event information. </param>
        ''' <param name="format">           Describes the format to use. </param>
        <Extension()>
        Public Sub DisplayTimespan(ByVal convertEventArgs As ConvertEventArgs, ByVal format As String)
            If convertEventArgs IsNot Nothing AndAlso convertEventArgs.DesiredType Is GetType(String) Then
                convertEventArgs.Value = CType(convertEventArgs.Value, TimeSpan).ToString(format)
            End If
        End Sub

        ''' <summary> Parse timespan. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="convertEventArgs"> Convert event information. </param>
        <Extension()>
        Public Sub ParseTimespan(ByVal convertEventArgs As ConvertEventArgs)
            If convertEventArgs IsNot Nothing AndAlso convertEventArgs.DesiredType Is GetType(TimeSpan) Then
                Dim value As TimeSpan = TimeSpan.Zero
                convertEventArgs.Value = If(TimeSpan.TryParse(convertEventArgs.Value.ToString, value), value, value)
            End If
        End Sub

        ''' <summary> Displays an enum value (numeric value) described by convertEventArgs. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="convertEventArgs"> Convert event information. </param>
        ''' <param name="format">           Describes the format to use. </param>
        <Extension()>
        Public Sub DisplayEnumValue(ByVal convertEventArgs As ConvertEventArgs, ByVal format As String)
            If convertEventArgs IsNot Nothing AndAlso convertEventArgs.DesiredType Is GetType(String) Then
                convertEventArgs.Value = String.Format(format, CInt(convertEventArgs.Value))
            End If
        End Sub

        ''' <summary> Parse enum value. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="convertEventArgs"> Convert event information. </param>
        <Extension()>
        Public Sub ParseEnumValue(Of T)(ByVal convertEventArgs As ConvertEventArgs)
            If convertEventArgs IsNot Nothing AndAlso convertEventArgs.DesiredType Is GetType(T) Then
                convertEventArgs.Value = CType(convertEventArgs.Value, T)
            End If
        End Sub

        ''' <summary> Writes an inverted described by convertEventArgs. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="convertEventArgs"> Convert event information. </param>
        <Extension()>
        Public Sub WriteInverted(ByVal convertEventArgs As ConvertEventArgs)
            If convertEventArgs IsNot Nothing AndAlso convertEventArgs.DesiredType Is GetType(Boolean) Then
                convertEventArgs.Value = Not CType(convertEventArgs.Value, Boolean)
            End If
        End Sub

        ''' <summary> Parse inverted. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="convertEventArgs"> Convert event information. </param>
        <Extension()>
        Public Sub ParseInverted(ByVal convertEventArgs As ConvertEventArgs)
            If convertEventArgs IsNot Nothing AndAlso convertEventArgs.DesiredType Is GetType(Boolean) Then
                Dim value As Boolean = True
                If Boolean.TryParse(convertEventArgs.Value.ToString, value) Then
                    value = Not value
                End If
                convertEventArgs.Value = value
            End If
        End Sub

#End Region

#Region " BINDING COLLECTIONS: REMOVE "

        ''' <summary> Clears all bindings. Verifies that binding indeed were cleared. </summary>
        ''' <remarks>
        ''' The base <see cref="ControlBindingsCollection.Clear">clear</see> may not clear fast enough
        ''' for the binding to be cleared when a new binding is added.
        ''' </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value"> The value. </param>
        <Extension()>
        Public Sub ClearAll(ByVal value As ControlBindingsCollection)
            If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
            Do While value.Count > 0
                Try
                    value.RemoveAt(0)
                    Windows.Forms.Application.DoEvents()
                Catch ex As ArgumentOutOfRangeException
                End Try
            Loop
        End Sub

        ''' <summary> Removes the specified binding based on the binding property name. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value">        The value. </param>
        ''' <param name="propertyName"> Name of the property. </param>
        ''' <returns> A Binding. </returns>
        <Extension()>
        Public Function RemoveAndVerify(ByVal value As ControlBindingsCollection, ByVal propertyName As String) As Binding
            If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
            If String.IsNullOrWhiteSpace(propertyName) Then Throw New ArgumentNullException(NameOf(propertyName))
            Dim result As Binding = Nothing
            Do While Methods.Exists(value, propertyName)
                For Each b As Binding In value
                    If String.Equals(b.PropertyName, propertyName, StringComparison.OrdinalIgnoreCase) Then
                        Try
                            result = b
                            value.Remove(b)
                            Windows.Forms.Application.DoEvents()
                        Catch ex As ArgumentOutOfRangeException
                            ' remove could cause an exception due to the way the binding is checked and released.
                            ' as it happens, the check for exists passes but the binding is no longer there when trying to
                            ' remove it.
                        End Try
                        Exit For
                    End If
                Next
            Loop
            Return result
        End Function

        ''' <summary> Removes the specified binding based on the binding property name. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value">   The value. </param>
        ''' <param name="binding"> The binding. </param>
        ''' <returns> A Binding. </returns>
        <Extension()>
        Public Function RemoveAndVerify(ByVal value As ControlBindingsCollection, ByVal binding As Binding) As Binding
            If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
            If binding Is Nothing Then Throw New ArgumentNullException(NameOf(binding))
            Return Methods.RemoveAndVerify(value, binding.PropertyName)
        End Function

#End Region

#Region " BINDING COLLECTIONS: FIND "

        ''' <summary> Checks if the binding already exists. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value">        The value. </param>
        ''' <param name="propertyName"> Name of the property. </param>
        ''' <returns> <c>True</c> if binding exists, <c>False</c> otherwise. </returns>
        <Extension()>
        Public Function Exists(ByVal value As ControlBindingsCollection, ByVal propertyName As String) As Boolean
            If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
            If String.IsNullOrWhiteSpace(propertyName) Then Throw New ArgumentNullException(NameOf(propertyName))
            For Each b As Binding In value
                If String.Equals(b.PropertyName, propertyName, StringComparison.OrdinalIgnoreCase) Then
                    Return True
                End If
            Next
            Return False
        End Function

        ''' <summary> Checks if the binding already exists. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value">   The value. </param>
        ''' <param name="binding"> The binding. </param>
        ''' <returns> <c>True</c> if binding exists, <c>False</c> otherwise. </returns>
        <Extension()>
        Public Function Exists(ByVal value As ControlBindingsCollection, ByVal binding As Binding) As Boolean
            If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
            If binding Is Nothing Then Throw New ArgumentNullException(NameOf(binding))
            Return Methods.Exists(value, binding.PropertyName)
        End Function

#End Region

#Region " BINDING COLLECTIONS: REPLACE "

        ''' <summary> Replaces the binding for the bound property of the control. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value">   The value. </param>
        ''' <param name="binding"> The binding. </param>
        ''' <returns> Binding. </returns>
        <Extension()>
        Public Function Replace(ByVal value As ControlBindingsCollection, ByVal binding As Binding) As Binding
            If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
            If binding Is Nothing Then Throw New ArgumentNullException(NameOf(binding))
            Methods.RemoveAndVerify(value, binding)
            value.Add(binding)
            Return binding
        End Function

#End Region

#Region " BINDING COLLECTIONS: SELECT "

        ''' <summary> Selects an existing binding if there. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="bindings"> The binding collection. </param>
        ''' <param name="binding">  The binding. </param>
        ''' <returns> Binding or nothing if not found. </returns>
        <Extension()>
        Public Function Find(ByVal bindings As ControlBindingsCollection, ByVal binding As Binding) As Binding
            If binding Is Nothing Then Throw New ArgumentNullException(NameOf(binding))
            Return Methods.Find(bindings, binding.PropertyName)
        End Function

        ''' <summary> Selects an existing binding if there. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="bindings">     The binding collection. </param>
        ''' <param name="propertyName"> Name of the property. </param>
        ''' <returns> A Binding. </returns>
        <Extension()>
        Public Function Find(ByVal bindings As BindingsCollection, ByVal propertyName As String) As Binding
            Dim result As Binding = Nothing
            If bindings IsNot Nothing Then
                For Each b As Binding In bindings
                    If String.Equals(b.PropertyName, propertyName, StringComparison.OrdinalIgnoreCase) Then
                        result = b
                        Exit For
                    End If
                Next
            End If
            Return result
        End Function

        ''' <summary> Select binding by property name. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="control">      The control. </param>
        ''' <param name="propertyName"> Name of the property. </param>
        ''' <returns> A Binding. </returns>
        <Extension()>
        Public Function SelectBinding(ByVal control As Control, ByVal propertyName As String) As Binding
            If control Is Nothing Then Throw New ArgumentNullException(NameOf(control))
            Return control.DataBindings.Find(propertyName)
        End Function

#End Region

#Region " BINDING COLLECTIONS: ADD "

        ''' <summary>
        ''' Adds a binding to the control. Disables the control while adding the binding to allow the
        ''' disabling of control events while the binding is added. Note that the control event of value
        ''' change occurs before the bound count increments so the bound count cannot be used to
        ''' determine the control bindable status.
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value">   The value. </param>
        ''' <param name="binding"> . </param>
        ''' <returns> The binding. </returns>
        <Extension()>
        Public Function SilentAdd(ByVal value As ControlBindingsCollection, ByVal binding As Binding) As Binding
            If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
            If binding Is Nothing Then Throw New ArgumentNullException(NameOf(binding))
            Dim isEnabled As Boolean = value.Control.Enabled
            value.Control.Enabled = False
            value.Add(binding)
            value.Control.Enabled = isEnabled
            Return binding
        End Function

        ''' <summary> Adds binding to a <see cref="IBindableComponent">bindable componenet</see> </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="component">       The bindable component. </param>
        ''' <param name="propertyName">    Name of the property. </param>
        ''' <param name="dataSource">      The data source. </param>
        ''' <param name="dataMember">      The data member. </param>
        ''' <param name="bindingComplete"> The binding complete. </param>
        ''' <returns> A Binding. </returns>
        <Extension()>
        Public Function AddBinding(ByVal component As IBindableComponent, ByVal propertyName As String,
                                   ByVal dataSource As Object, ByVal dataMember As String,
                                   ByVal bindingComplete As BindingCompleteEventHandler) As Binding
            Return Methods.AddRemoveBinding(component, True,
                                            New Binding(propertyName, dataSource, dataMember) With {.ControlUpdateMode = ControlUpdateMode.OnPropertyChanged,
                                                                                                    .DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged},
                                            bindingComplete)
        End Function

        ''' <summary>
        ''' Removes binding from a <see cref="IBindableComponent">bindable componenet</see>
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="component">       The bindable component. </param>
        ''' <param name="propertyName">    Name of the property. </param>
        ''' <param name="dataSource">      The data source. </param>
        ''' <param name="dataMember">      The data member. </param>
        ''' <param name="bindingComplete"> The binding complete. </param>
        ''' <returns> A Binding. </returns>
        <Extension()>
        Public Function RemoveBinding(ByVal component As IBindableComponent, ByVal propertyName As String,
                                      ByVal dataSource As Object, ByVal dataMember As String,
                                      ByVal bindingComplete As BindingCompleteEventHandler) As Binding
            Return Methods.AddRemoveBinding(component, False, New Binding(propertyName, dataSource, dataMember), bindingComplete)
        End Function

        ''' <summary>
        ''' Adds or removes binding from a <see cref="IBindableComponent">bindable componenet</see>
        ''' </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="component">       The bindable component. </param>
        ''' <param name="add">             True to add; otherwise, remove. </param>
        ''' <param name="binding">         The binding. </param>
        ''' <param name="bindingComplete"> The binding complete handler. </param>
        ''' <returns> A Binding. </returns>
        <Extension()>
        Public Function AddRemoveBinding(ByVal component As IBindableComponent, ByVal add As Boolean, ByVal binding As Binding,
                                         ByVal bindingComplete As BindingCompleteEventHandler) As Binding
            If component Is Nothing Then Throw New ArgumentNullException(NameOf(component))
            If binding Is Nothing Then Throw New ArgumentNullException(NameOf(binding))
            If add Then
                ' required to prevent cross thread exceptions. Invoke is required on the property change event.
                ' binding.ControlUpdateMode = ControlUpdateMode.OnPropertyChanged
                ' binding.DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged
                component.DataBindings.Add(binding)
                AddHandler binding.BindingComplete, bindingComplete
            Else
                ' .DataBindings.Remove(binding)
                Dim b As Binding = component.DataBindings.Find(binding.PropertyName)
                If b IsNot Nothing Then component.DataBindings.Remove(b)
                RemoveHandler binding.BindingComplete, bindingComplete
            End If
            Return binding
        End Function

#End Region

#Region " CONTROL WRITE BOUND VALUE "

        ''' <summary> Writes a bound value to the data source. </summary>
        ''' <remarks>
        ''' The combo box does not implement a property change for the selected item property. When
        ''' called from the Selected Value Changed event of the control, this method effects a change of
        ''' selected item when the selected value changes by writing the selected item to the data source
        ''' identified in the binding of the selected item data source property, if such item exists.
        ''' </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="control">      The control. </param>
        ''' <param name="propertyName"> Name of the property. </param>
        <Extension()>
        Public Sub WriteBoundValue(ByVal control As Control, ByVal propertyName As String)
            If control Is Nothing Then Throw New ArgumentNullException(NameOf(control))
            control.SelectBinding(propertyName)?.WriteValue()
        End Sub

        ''' <summary> Writes a bound selected item value to the data source. </summary>
        ''' <remarks> David, 2020-09-16. </remarks>
        ''' <param name="control"> The control. </param>
        <Extension()>
        Public Sub WriteBoundSelectedItem(ByVal control As ComboBox)
            Methods.WriteBoundValue(control, NameOf(ComboBox.SelectedItem))
        End Sub

#End Region

    End Module

End Namespace

