using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.Core.Forma.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.Core.Forma.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.Core.Forma.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]

[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( isr.Core.Forma.My.MyLibrary.TestAssemblyStrongName )]
