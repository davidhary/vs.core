using System.ComponentModel;

namespace isr.Core.Forma
{
    public partial class MessagesBox : INotifyPropertyChanged
    {

        #region " CUSTOM PROPERTY CHANGED EVENT IMPLEMENTATION "

        /// <summary> Removes the property changed event handlers. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        protected void RemovePropertyChangedEventHandlers()
        {
            this._PropertyChangedHandlers.RemoveAll();
        }

        /// <summary> The property changed handlers. </summary>
        private readonly PropertyChangeEventContextCollection _PropertyChangedHandlers = new();

        /// <summary> Event queue for all listeners interested in property changed events. </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event PropertyChangedEventHandler PropertyChanged
        {
            add {
                this._PropertyChangedHandlers.Add( new PropertyChangedEventContext( value ) );
            }

            remove {
                this._PropertyChangedHandlers.RemoveValue( value );
            }
        }

        private void OnPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            // Defaults to 'Send' to prevent cross thread exceptions.
            this._PropertyChangedHandlers.Send( sender, e );
        }


        #endregion

    }
}
