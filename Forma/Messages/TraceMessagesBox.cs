using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

namespace isr.Core.Forma
{

    /// <summary> A message logging text box. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2013-09-04, 1.2.4955. based on the legacy messages box. </para>
    /// </remarks>
    [DebuggerDisplay( "id={UniqueId}" )]
    [Description( "Trace Messages Text Box" )]
    public partial class TraceMessagesBox : MessagesBox
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructor for this class. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public TraceMessagesBox() : base()
        {
            this.InitializeComponent();
            this.TraceMessageFormat = TraceMessage.DefaultTraceMessageFormat;
            this._AlertLevel = TraceEventType.Warning;
            this._AlertAnnunciatorEvent = TraceEventType.Verbose;
            this._AlertSoundEvent = TraceEventType.Verbose;
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                // Invoke the base class dispose method        
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " DISPLAY AND MESSAGE SENTINELS "

        /// <summary> Suspend updates and release indicator controls. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public override void SuspendUpdatesReleaseIndicators()
        {
            base.SuspendUpdatesReleaseIndicators();
            this.AlertsToggleTreeNode = null;
            this.AlertsToggleControl = null;
        }

        /// <summary> The alert level. </summary>
        private TraceEventType _AlertLevel;

        /// <summary>
        /// Gets or sets the alert level. Message <see cref="TraceEventType">levels</see> equal or lower
        /// than this are tagged as alerts and set the <see cref="AlertAnnunciatorEvent">alert
        /// sentinel</see>.
        /// </summary>
        /// <value> The alert level. </value>
        [Category( "Appearance" )]
        [Description( "Level for notifying of alerts" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( "TraceEventType.Warning" )]
        public TraceEventType AlertLevel
        {
            get => this._AlertLevel;

            set {
                if ( value != this.AlertLevel )
                {
                    this._AlertLevel = value;
                    this.SyncNotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets the sentinel indicating that alert messages need to be announced. </summary>
        /// <value> The alerts announced. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool AlertsAnnounced => this.AlertAnnunciatorEvent <= this.AlertLevel;

        /// <summary> The alert annunciator event. </summary>
        private TraceEventType _AlertAnnunciatorEvent;

        /// <summary>
        /// Gets or sets or set the trace event type which toggles the alert annunciator.
        /// </summary>
        /// <value> The alert annunciator event. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public TraceEventType AlertAnnunciatorEvent
        {
            get => this._AlertAnnunciatorEvent;

            set {
                if ( value != this.AlertAnnunciatorEvent )
                {
                    this._AlertAnnunciatorEvent = value;
                    this.SyncNotifyPropertyChanged();
                    this.UpdateAlertToggleControl( this.AlertsToggleControl, value );
                    this.UpdateAlertToggleTreeNode( this.AlertsToggleTreeNode, value );
                }
            }
        }

        /// <summary> Gets the sentinel indicating that alert messages need to be voiced. </summary>
        /// <value> The alerts voiced. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool AlertsVoiced => this.AlertAnnunciatorEvent <= this.AlertLevel;

        /// <summary> The alert sound event. </summary>
        private TraceEventType _AlertSoundEvent;

        /// <summary> Gets or sets the highest alert event. </summary>
        /// <value> The highest alert event. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public TraceEventType AlertSoundEvent
        {
            get => this._AlertSoundEvent;

            set {
                if ( value != this.AlertSoundEvent )
                {
                    this._AlertSoundEvent = value;
                    this.SyncNotifyPropertyChanged();
                    this.PlayAlertIf( this.AlertsVoiced, value );
                }
            }
        }

        /// <summary> Updates the alerts described by level. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="level"> The level. </param>
        protected void UpdateAlerts( TraceEventType level )
        {
            if ( level <= this.AlertLevel )
            {
                // Updates the cached alert level if the level represents a higher alert level.
                if ( level < this.AlertAnnunciatorEvent )
                {
                    this.AlertAnnunciatorEvent = level;
                }

                if ( level < this.AlertSoundEvent )
                {
                    this.AlertSoundEvent = level;
                }
            }
            // Me._AlertsAdded = Not Me.UserVisible AndAlso (Me.AlertsAdded OrElse (level <= Me.AlertLevel))
            // 2016 moved to highest alert level: If Not Me.UserVisible AndAlso Me.AlertsAdded Then Me.PlayAlert(level)
        }

        /// <summary> Displays the available lines and clears the alerts and message sentinels. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public override void Display()
        {
            // turn off the alert annunciator when displaying the messages onto a visible screen.
            // the sound alert level is kept unchanged. 
            if ( this.UserVisible )
            {
                this.AlertAnnunciatorEvent = TraceEventType.Verbose;
            }

            base.Display();
        }

        /// <summary> Executes the clear action. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        protected override void OnClear()
        {
            this.AlertAnnunciatorEvent = TraceEventType.Verbose;
            this.AlertSoundEvent = TraceEventType.Verbose;
            base.OnClear();
        }

        /// <summary> Gets or sets the color of the alert toggle control back. </summary>
        /// <value> The color of the alert toggle control back. </value>
        private System.Drawing.Color AlertToggleControlBackColor { get; set; } = System.Drawing.SystemColors.Control;

        /// <summary> Gets or sets the alert toggle control text. </summary>
        /// <value> The alert toggle control text. </value>
        private string AlertToggleControlText { get; set; }

        /// <summary> The alerts toggle tree node. </summary>
        private TreeNode _AlertsToggleTreeNode;

        /// <summary> Gets or sets the alerts toggle TreeNode. </summary>
        /// <value> The alerts toggle TreeNode. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public TreeNode AlertsToggleTreeNode
        {
            get => this._AlertsToggleTreeNode;

            set {
                this._AlertsToggleTreeNode = value;
                if ( value is object )
                {
                    this.AlertToggleControlBackColor = value.BackColor;
                    this.AlertToggleControlText = value.Text;
                }
            }
        }

        /// <summary> Safe visible setter. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="treeNode">   The TreeNode. </param>
        /// <param name="alertEvent"> The alert event. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void UpdateAlertToggleTreeNode( TreeNode treeNode, TraceEventType alertEvent )
        {
            try
            {
                if ( treeNode is object )
                {
                    if ( treeNode.TreeView.InvokeRequired )
                    {
                        _ = treeNode.TreeView.Invoke( new Action<TreeNode, TraceEventType>( this.UpdateAlertToggleTreeNode ), new object[] { treeNode, alertEvent } );
                    }
                    else if ( alertEvent <= this.AlertLevel )
                    {
                        treeNode.Text = alertEvent.ToString();
                        switch ( alertEvent )
                        {
                            case var @case when @case <= TraceEventType.Error:
                                {
                                    treeNode.BackColor = System.Drawing.Color.Red;
                                    break;
                                }

                            case var case1 when case1 <= TraceEventType.Warning:
                                {
                                    treeNode.BackColor = System.Drawing.Color.Orange;
                                    break;
                                }

                            default:
                                {
                                    treeNode.BackColor = System.Drawing.Color.LightBlue;
                                    break;
                                }
                        }
                    }
                    else
                    {
                        treeNode.BackColor = this.AlertToggleControlBackColor;
                        treeNode.Text = this.AlertToggleControlText;
                    }
                }
            }
            catch
            {
            }
        }

        /// <summary> The alerts toggle control. </summary>
        private Control _AlertsToggleControl;

        /// <summary> Gets or sets the alerts toggle control. </summary>
        /// <value> The alerts toggle control. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public Control AlertsToggleControl
        {
            get => this._AlertsToggleControl;

            set {
                this._AlertsToggleControl = value;
                if ( value is object )
                {
                    this.AlertToggleControlBackColor = value.BackColor;
                    this.AlertToggleControlText = value.Text;
                }
            }
        }

        /// <summary> Safe visible setter. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="control">    The control. </param>
        /// <param name="alertEvent"> The alert event. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void UpdateAlertToggleControl( Control control, TraceEventType alertEvent )
        {
            try
            {
                if ( control is object )
                {
                    if ( control.InvokeRequired )
                    {
                        _ = control.Invoke( new Action<Control, TraceEventType>( this.UpdateAlertToggleControl ), new object[] { control, alertEvent } );
                    }
                    else
                    {
                        if ( alertEvent <= this.AlertLevel )
                        {
                            control.Text = alertEvent.ToString();
                            switch ( alertEvent )
                            {
                                case var @case when @case <= TraceEventType.Error:
                                    {
                                        control.BackColor = System.Drawing.Color.Red;
                                        break;
                                    }

                                case var case1 when case1 <= TraceEventType.Warning:
                                    {
                                        control.BackColor = System.Drawing.Color.Orange;
                                        break;
                                    }

                                default:
                                    {
                                        control.BackColor = System.Drawing.Color.LightBlue;
                                        break;
                                    }
                            }

                            if ( !(control is TabPage) )
                            {
                                control.Visible = true;
                            }
                        }
                        else
                        {
                            control.BackColor = this.AlertToggleControlBackColor;
                            control.Text = this.AlertToggleControlText;
                            if ( !(control is TabPage) )
                            {
                                control.Visible = false;
                            }
                        }

                        control.Invalidate();
                    }
                }
            }
            catch
            {
            }
        }

        /// <summary> Gets or sets the alert sound enabled. </summary>
        /// <value> The alert sound enabled. </value>
        [Category( "Appearance" )]
        [Description( "Enables playing alert sounds" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( false )]
        public bool AlertSoundEnabled { get; set; }

        /// <summary> Play alert if. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="condition"> true to condition. </param>
        /// <param name="level">     The level. </param>
        public void PlayAlertIf( bool condition, TraceEventType level )
        {
            if ( condition && this.AlertSoundEnabled )
            {
                PlayAlert( level );
            }
        }

        /// <summary> Play alert. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="level"> The level. </param>
        public static void PlayAlert( TraceEventType level )
        {
            if ( level == TraceEventType.Critical )
            {
                My.MyProject.Computer.Audio.PlaySystemSound( System.Media.SystemSounds.Exclamation );
            }
            else if ( level == TraceEventType.Error )
            {
                My.MyProject.Computer.Audio.PlaySystemSound( System.Media.SystemSounds.Exclamation );
            }
            else if ( level == TraceEventType.Warning )
            {
                My.MyProject.Computer.Audio.PlaySystemSound( System.Media.SystemSounds.Asterisk );
            }
        }

        #endregion

        #region " ADD MESSAGE "

        /// <summary> Prepends or appends a new value to the messages box. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> A String. </returns>
        public override string AddMessage( string value )
        {
            if ( string.IsNullOrWhiteSpace( value ) )
            {
                Debug.Assert( !Debugger.IsAttached, "Empty trace message @", new StackTrace( true ).ToString().Split( Environment.NewLine.ToCharArray() )[0] );
                value = string.Empty;
            }
            else
            {
                _ = base.AddMessage( value );
            }

            return value;
        }

        /// <summary> Adds a message to the display. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The value. </param>
        public void AddMessage( TraceMessage value )
        {
            if ( value is null || string.IsNullOrWhiteSpace( value.Details ) )
            {
                Debug.Assert( !Debugger.IsAttached, "Empty trace message @", new StackTrace( true ).ToString().Split( Environment.NewLine.ToCharArray() )[0] );
            }
            else
            {
                // sustains the alerts flag until cleared.
                this.UpdateAlerts( value.EventType );
                _ = this.AddMessage( value.ToString( this.TraceMessageFormat ) );
                this.PublishSynopsis( value );
            }
        }

        /// <summary> Gets or sets the default format for displaying the message. </summary>
        /// <remarks>
        /// The format must include 4 elements to display the first two characters of the trace event
        /// type, the id, timestamp and message details. For example,<code>
        /// "{0}, {1:X}, {2:HH:mm:ss.fff}, {3}"</code>.
        /// </remarks>
        /// <value> The trace message format. </value>
        [Category( "Appearance" )]
        [Description( "Formats the test message" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( TraceMessage.DefaultTraceMessageFormat )]
        public string TraceMessageFormat { get; set; }

        #endregion

        #region " STATUS PUBLISHER "

        /// <summary> The status prompt. </summary>
        private string _StatusPrompt;

        /// <summary> Gets or sets the status prompt. </summary>
        /// <value> The status bar label. </value>
        public string StatusPrompt
        {
            get => this._StatusPrompt;

            set {
                if ( !string.Equals( value, this.StatusPrompt ) )
                {
                    if ( string.IsNullOrEmpty( value ) )
                    {
                        value = string.Empty;
                    }

                    this._StatusPrompt = value;
                    this.SyncNotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets the length of the maximum synopsis. </summary>
        /// <value> The length of the maximum synopsis. </value>
        [Category( "Data" )]
        [Description( "Maximum synopsis message length" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( 0 )]
        public int MaxSynopsisLength { get; set; }

        /// <summary> Publishes status. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="traceMessage"> Message describing the trace. </param>
        public void PublishSynopsis( TraceMessage traceMessage )
        {
            if ( traceMessage is null )
            {
                return;
            }

            string value = TraceMessage.ExtractSynopsis( traceMessage.Details, TraceMessage.DefaultSynopsisDelimiter, this.MaxSynopsisLength );
            this.StatusPrompt = value;
        }

        #endregion

        #region " I MESSAGE LISTENER "

        /// <summary> Registers this trace level and updates alerts. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="level"> The level. </param>
        public override void Register( TraceEventType level )
        {
            base.Register( level );
            this.UpdateAlerts( level );
        }

        #endregion

    }
}
