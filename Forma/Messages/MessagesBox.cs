using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

using isr.Core.Forma.ExceptionExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Forma
{

    /// <summary> Messages display text box. </summary>
    /// <remarks>
    /// (c) 2002 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2002-09-21, 1.0.839 </para>
    /// </remarks>
    [Description( "Messages Test Box" )]
    public partial class MessagesBox : TextBox
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructor for this class. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public MessagesBox() : base()
        {

            // set defaults
            this._SyncLocker = new object();
            this._Lines = new List<string>();
            this.Multiline = true;
            this.ReadOnly = true;
            this.CausesValidation = false;
            this.ScrollBars = ScrollBars.Both;
            this.Size = new System.Drawing.Size( 150, 150 );
            this.ResetKnownStateThis();
            base.ContextMenuStrip = this.CreateContextMenuStrip();
            this.ContentsQueue = new ConcurrentQueue<string>();

            // enables update tasks
            this.CommenceUpdatesThis();
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.ContainerPanel = null;
                    this.ContainerTreeNode = null;
                    if ( this._TraceSourceListener is object )
                    {
                        this._TraceSourceListener.Listener = null;
                        this._TraceSourceListener.Dispose();
                        this._TraceSourceListener = null;
                    }

                    if ( this._TraceListener is object )
                    {
                        this._TraceListener.Listener = null;
                        this._TraceListener.Dispose();
                        this._TraceListener = null;
                    }

                    this.DisposeWorker();
                    this.CancellationTokenSource?.Cancel();
                    var sw = Stopwatch.StartNew();
                    while ( this.IsBusy() || sw.Elapsed < TimeSpan.FromMilliseconds( 200d ) )
                    {
                        Application.DoEvents();
                    }

                    if ( !this.IsBusy() )
                    {
                        if ( this.ActionTask is object )
                        {
                            this.ActionTask.Dispose();
                            this.ActionTask = null;
                        }

                        if ( this.AsyncTask is object )
                        {
                            this.AsyncTask.Dispose();
                            this.AsyncTask = null;
                        }
                    }

                    if ( this.CancellationTokenSource is object )
                    {
                        this.CancellationTokenSource.Dispose();
                        this.CancellationTokenSource = null;
                    }

                    this._Lines?.Clear();
                    this._Lines = null;
                    this._MyContextMenuStrip?.Dispose();
                    this._MyContextMenuStrip = null;
                    this.RemovePropertyChangedEventHandlers();
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        /// <summary> Resets the known state this. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void ResetKnownStateThis()
        {
            this.BackColor = System.Drawing.SystemColors.Info;
            this.Font = new System.Drawing.Font( "Consolas", 8.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte( 0 ) );
            this.ApplyTraceLevelThis( TraceEventType.Verbose );
            this.TabCaption = "Log";
            this.CaptionFormat = "{0} " + System.Text.Encoding.GetEncoding( 437 ).GetString( new byte[] { 240 } );
            this.ResetCount = 200;
            this.PresetCount = 100;
            this.Appending = false;
        }

        /// <summary> Resets the known state. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        protected virtual void ResetKnownState()
        {
            this.ResetKnownStateThis();
        }

        #endregion

        #region " LIST MANAGER "

        /// <summary> True to appending. </summary>
        private bool _Appending;

        /// <summary>
        /// Gets the appending sentinel. Items are appended to an appending list. Otherwise, items are
        /// added to the top of the list.
        /// </summary>
        /// <value> The ascending sentinel; True if the list is ascending or descending. </value>
        [Category( "Appearance" )]
        [Description( "True to add items to the bottom of the list" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( false )]
        public bool Appending
        {
            get => this._Appending;

            set {
                if ( value != this.Appending )
                {
                    this._Appending = value;
                    this.SyncNotifyPropertyChanged();
                }
            }
        }

        /// <summary> The synchronization locker. </summary>
        private readonly object _SyncLocker;

        /// <summary> Holds the list of lines to display. </summary>
        private List<string> _Lines;

        /// <summary>
        /// Adds the message to the message list. A message may include one or more lines.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="message"> The message to add. </param>
        private void AddMessageLines( string message )
        {
            if ( !string.IsNullOrWhiteSpace( message ) )
            {
                var values = message.Split( Conversions.ToChar( Environment.NewLine ) );
                if ( values.Count() == 1 )
                {
                    if ( this.Appending )
                    {
                        this._Lines.Add( message );
                    }
                    else
                    {
                        this._Lines.Insert( 0, message );
                    }
                }
                else if ( this.Appending )
                {
                    this._Lines.AddRange( values );
                }
                else
                {
                    this._Lines.InsertRange( 0, values );
                }
            }
        }

        /// <summary> Executes the clear action. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        protected virtual void OnClear()
        {
            this._Lines.Clear();
            base.Clear();
            this.NewMessagesAdded = false;
        }

        /// <summary> Clears all text from the text box control. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public new void Clear()
        {
            if ( this.InvokeRequired )
            {
                _ = this.Invoke( new Action( this.Clear ), Array.Empty<object>() );
            }
            else if ( !this.IsDisposed && this._Lines is object )
            {
                lock ( this._SyncLocker )
                {
                    this.OnClear();
                }
            }
        }

        /// <summary> Number of resets. </summary>
        private int _ResetCount;

        /// <summary> Gets or sets the reset count. </summary>
        /// <remarks>
        /// The message list gets reset to the preset count when the message count exceeds the reset
        /// count.
        /// </remarks>
        /// <value> <c>ResetSize</c>is an integer property. </value>
        [Category( "Appearance" )]
        [Description( "Number of lines at which to reset" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( "100" )]
        public int ResetCount
        {
            get => this._ResetCount;

            set {
                if ( this.ResetCount != value )
                {
                    this._ResetCount = value;
                    this.SyncNotifyPropertyChanged();
                }
            }
        }

        /// <summary> Number of presets. </summary>
        private int _PresetCount;

        /// <summary> Gets or sets the preset count. </summary>
        /// <remarks>
        /// The message list gets reset to the preset count when the message count exceeds the reset
        /// count.
        /// </remarks>
        /// <value> <c>PresetSize</c>is an integer property. </value>
        [Category( "Appearance" )]
        [Description( "Number of lines to reset to" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( "50" )]
        public int PresetCount
        {
            get => this._PresetCount;

            set {
                if ( this.PresetCount != value )
                {
                    this._PresetCount = value;
                    this.SyncNotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets the sentinel indicating if the control is visible to the user. </summary>
        /// <value> The showing. </value>
        protected bool UserVisible => this.Visible && this.Height > 100 && this.GetContainerControl() is object && this.GetContainerControl().ActiveControl is object && this.GetContainerControl().ActiveControl.Visible;

        /// <summary> Displays this object. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void DisplayThis()
        {
            if ( this.IsDisposed )
            {
            }
            else if ( this.InvokeRequired )
            {
                // the object could be disposed when the thread still has items to report. 
                // trying to detect the disposed object does not seem to work.
                try
                {
                    if ( !this.IsDisposed )
                    {
                        _ = this.Invoke( new Action( this.DisplayThis ) );
                    }
                }
                catch
                {
                }
            }
            else
            {
                try
                {
                    // this has produced a momentary error: Destination array not long enough.
                    this.Lines = this._Lines.ToArray();
                    this.NewMessagesAdded = false;
                    this.SelectionStart = 0;
                    this.SelectionLength = 0;
                }
                catch
                {
                }
            }
        }

        /// <summary>
        /// Displays the available lines and clear the <see cref="NewMessagesAdded">message
        /// sentinel</see>.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public virtual void Display()
        {
            if ( this.UserVisible )
            {
                this.DisplayThis();
            }
        }

        /// <summary>
        /// Raises the <see cref="E: Control.VisibleChanged" /> event. Updates the display.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnVisibleChanged( EventArgs e )
        {
            this.Display();
            base.OnVisibleChanged( e );
        }

        #endregion

        #region " ADD MESSAGE "

        /// <summary> Prepends or appends a new value to the messages box. </summary>
        /// <remarks>
        /// Starts a Background Worker by calling RunWorkerAsync. The Text property of the TextBox
        /// control is set when the Background Worker raises the RunWorkerCompleted event.
        /// </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> A String. </returns>
        public virtual string AddMessage( string value )
        {
            this.GetContentsQueue()?.Enqueue( value );
            this.FlushMessages();
            return value;
        }

        /// <summary> Adds an error message to the message box. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The <see cref="System.Exception">error</see> to add. </param>
        public void AddMessage( Exception value )
        {
            if ( value is object )
            {
                _ = this.AddMessage( value.ToFullBlownString() );
            }
        }

        #endregion

        #region " CAPTION "

        /// <summary> Suspend updates and release indicator controls. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public virtual void SuspendUpdatesReleaseIndicators()
        {
            this.SuspendUpdates();
            Thread.Sleep( 10 );
            this.ContainerPanel = null;
            this.ContainerTreeNode = null;
        }

        /// <summary> True if new messages added. </summary>
        private bool _NewMessagesAdded;

        /// <summary> Gets or sets the sentinel indicating that new messages were added. </summary>
        /// <value> The new messages available. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool NewMessagesAdded
        {
            get => this._NewMessagesAdded;

            set {
                if ( value != this.NewMessagesAdded )
                {
                    this._NewMessagesAdded = value;
                    this.SyncNotifyPropertyChanged();
                    this.UpdateCaption();
                }
            }
        }

        /// <summary> The tab caption. </summary>
        private string _TabCaption;

        /// <summary> Gets or sets the tab caption. </summary>
        /// <value> The tab caption. </value>
        [Category( "Appearance" )]
        [Description( "Default title for the parent tab" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( "Log" )]
        public string TabCaption
        {
            get => this._TabCaption;

            set {
                if ( (this.TabCaption ?? "") != (value ?? "") )
                {
                    this._TabCaption = value;
                    this.SyncNotifyPropertyChanged();
                    this.UpdateCaption();
                }
            }
        }

        /// <summary> The captionformat. </summary>
        private string _Captionformat;

        /// <summary> Gets or sets the caption format indicating that messages were added. </summary>
        /// <value> The tab caption format. </value>
        [Category( "Appearance" )]
        [Description( "Formats the tab caption with number of new messages" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( "{0} =" )]
        public string CaptionFormat
        {
            get => this._Captionformat;

            set {
                if ( string.IsNullOrEmpty( value ) )
                {
                    value = string.Empty;
                }

                if ( !value.Equals( this.CaptionFormat ) )
                {
                    this._Captionformat = value;
                    this.SyncNotifyPropertyChanged();
                    this.UpdateCaption();
                }
            }
        }

        /// <summary> The caption. </summary>
        private string _Caption;

        /// <summary> Gets or sets the caption. </summary>
        /// <value> The caption. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public string Caption
        {
            get => this._Caption;

            set {
                if ( !(string.IsNullOrEmpty( value ) || string.Equals( value, this.Caption )) )
                {
                    this._Caption = value;
                    this.NotifyPropertyChanged();
                    if ( this.ContainerPanel is object )
                    {
                        this.SafeTextSetter( this.ContainerPanel, this.Caption );
                    }

                    if ( this.ContainerTreeNode is object )
                    {
                        this.SafeTextSetter( this.ContainerTreeNode, this.Caption );
                    }
                }
            }
        }

        /// <summary> Updates the caption. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void UpdateCaption()
        {
            this.Caption = true == this.NewMessagesAdded ? string.Format( System.Globalization.CultureInfo.CurrentCulture, this.CaptionFormat, this.TabCaption ) : this.TabCaption;
        }

        /// <summary> Gets or sets the container panel. </summary>
        /// <value> The parent panel. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public Panel ContainerPanel { get; set; }

        /// <summary> Gets or sets the container tree node. </summary>
        /// <value> The container tree node. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public TreeNode ContainerTreeNode { get; set; }

        /// <summary> Updates the container panel caption. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="panel"> The container panel. </param>
        /// <param name="value"> The value. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SafeTextSetter( Panel panel, string value )
        {
            try
            {
                if ( panel.InvokeRequired )
                {
                    _ = panel.Invoke( new Action<Panel, string>( this.SafeTextSetter ), new object[] { panel, value } );
                }
                else
                {
                    panel.Text = value;
                    Application.DoEvents();
                }
            }
            catch
            {
            }
        }

        /// <summary> Updates the container tree node caption. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="node">  The container tree node. </param>
        /// <param name="value"> The value. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SafeTextSetter( TreeNode node, string value )
        {
            try
            {
                if ( node.TreeView?.InvokeRequired == true )
                {
                    _ = (node.TreeView?.Invoke( new Action<TreeNode, string>( this.SafeTextSetter ), new object[] { node, value } ));
                }
                else
                {
                    node.Text = value;
                    Application.DoEvents();
                }
            }
            catch
            {
            }
        }

        #endregion

        #region " CONTEXT MENU STRIP "

        /// <summary> my context menu strip. </summary>
        private ContextMenuStrip _MyContextMenuStrip;

        /// <summary> Creates a context menu strip. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> The new context menu strip. </returns>
        private ContextMenuStrip CreateContextMenuStrip()
        {

            // Create a new ContextMenuStrip control.
            this._MyContextMenuStrip = new ContextMenuStrip();

            // Attach an event handler for the 
            // ContextMenuStrip control's Opening event.
            this._MyContextMenuStrip.Opening += this.ContectMenuOpeningHandler;
            return this._MyContextMenuStrip;
        }

        /// <summary> Adds menu items. </summary>
        /// <remarks>
        /// This event handler is invoked when the <see cref="ContextMenuStrip"/> control's Opening event
        /// is raised.
        /// </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Cancel event information. </param>
        private void ContectMenuOpeningHandler( object sender, CancelEventArgs e )
        {
            this._MyContextMenuStrip = sender as ContextMenuStrip;

            // Clear the ContextMenuStrip control's Items collection.
            this._MyContextMenuStrip.Items.Clear();

            // Populate the ContextMenuStrip control with its default items.
            // myContextMenuStrip.Items.Add("-")
            _ = this._MyContextMenuStrip.Items.Add( new ToolStripMenuItem( "Clear &All", null, this.ClearAllHandler, "Clear" ) );
            _ = this._MyContextMenuStrip.Items.Add( new ToolStripMenuItem( "Flush &Queue", null, this.FlushQueuesHandler, "Flush" ) );
            if ( this.Logger is object )
            {
                _ = this._MyContextMenuStrip.Items.Add( new ToolStripMenuItem( "&Open Log File", null, this.RequestOpeningLogFile, "Open Log File" ) );
            }

            if ( this.Logger is object )
            {
                _ = this._MyContextMenuStrip.Items.Add( new ToolStripMenuItem( "Open Log &Folder", null, this.RequestOpeningLogFolder, "Open Log Folder" ) );
            }

            // Set Cancel to false. 
            // It is optimized to true based on empty entry.
            e.Cancel = false;
        }

        /// <summary> Applies the high point Output. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ClearAllHandler( object sender, EventArgs e )
        {
            this.Clear();
        }

        /// <summary> Flush messages and content. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void FlushQueuesHandler( object sender, EventArgs e )
        {
            // Me.FlushContent()
            this.FlushMessages();
        }

        #endregion

        #region " LOG FILE HANDLING "

        /// <summary> Request opening log File. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void RequestOpeningLogFile( object sender, EventArgs e )
        {
            _ = (this.Logger?.OpenLogFile());
        }

        /// <summary> Request opening log folder. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void RequestOpeningLogFolder( object sender, EventArgs e )
        {
            _ = (this.Logger?.OpenFolderLocation());
        }

        /// <summary> Gets or sets my log. </summary>
        /// <value> my log. </value>
        private Logger Logger { get; set; }

        /// <summary> Assign log. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The <see cref="System.Exception">
        /// error</see>
        /// to add. </param>
        public void AssignLog( Logger value )
        {
            this.Logger = value;
        }

        #endregion

        #region " ASYNC QUEUED MESSAGES "

        /// <summary> Suspend updates tasks. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public void SuspendUpdates()
        {
            this.CancellationTokenSource.Cancel();
        }

        /// <summary> Commence update task. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public void CommenceUpdates()
        {
            this.CommenceUpdatesThis();
        }

        /// <summary> Commence updates this. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void CommenceUpdatesThis()
        {
            // instantiates cancellation token
            this.CancellationTokenSource = new CancellationTokenSource();
            this.CancellationToken = this.CancellationTokenSource.Token;
        }

        /// <summary> The cancellation token source. </summary>
        /// <value> The cancellation token source. </value>
        private CancellationTokenSource CancellationTokenSource { get; set; }

        /// <summary> The cancellation token. </summary>
        /// <value> The cancellation token. </value>
        private CancellationToken CancellationToken { get; set; }

        /// <summary> Query if this object is cancellation requested. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> True if cancellation requested, false if not. </returns>
        private bool IsCancellationRequested()
        {
            return this.CancellationToken.IsCancellationRequested;
        }

        /// <summary> Gets or sets a queue of contents. </summary>
        /// <value> A queue of contents. </value>
        protected ConcurrentQueue<string> ContentsQueue { get; private set; }

        /// <summary> Gets contents queue. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> The contents queue. </returns>
        private ConcurrentQueue<string> GetContentsQueue()
        {
            return this.ContentsQueue;
        }

        /// <summary> Gets the content. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> The content. </returns>
        private string GetContent()
        {
            _ = this.ContentsQueue.TryDequeue( out string result );
            return result;
        }

        /// <summary>
        /// This event handler sets the Text property of the TextBox control. It is called on the thread
        /// that created the TextBox control, so the call is thread-safe.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void UpdateDisplay()
        {
            lock ( this._SyncLocker )
            {
                while ( (this.GetContentsQueue()?.Any()).GetValueOrDefault( false ) && !this.IsCancellationRequested() )
                {
                    this.AddMessageLines( this.GetContent() );
                }

                if ( this._Lines.Count > this.ResetCount )
                {
                    if ( this.Appending )
                    {
                        this._Lines.RemoveRange( 0, this._Lines.Count - this.PresetCount );
                    }
                    else
                    {
                        this._Lines.RemoveRange( this.PresetCount, this._Lines.Count - this.PresetCount );
                    }
                }

                this.NewMessagesAdded = !this.UserVisible;
            }

            if ( !this.IsCancellationRequested() )
            {
                this.Display();
            }

            this.Invalidate();
            Application.DoEvents();
        }

        /// <summary> Uses the message worker to flush any queued messages. </summary>
        /// <remarks>
        /// If the calling thread is different from the thread that created the TextBox control, this
        /// method creates a SetTextCallback and calls itself asynchronously using the Invoke method.
        /// </remarks>
        public void FlushMessages()
        {
            // Me.FlushMessagesWorker()
            this.FlushMessagesTask();
        }

        #endregion

        #region " ASYNC QUEUED MESSAGES: TASK "

        /// <summary> Gets or sets the awaiting task. </summary>
        /// <value> The awaiting task. </value>
        public Task AsyncTask { get; private set; }

        /// <summary> Gets or sets the action task. </summary>
        /// <value> The action task. </value>
        public Task ActionTask { get; private set; }

        /// <summary> Query if 'status' is task ended. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="status"> The status. </param>
        /// <returns> <c>true</c> if task ended; otherwise <c>false</c> </returns>
        private static bool IsTaskEnded( TaskStatus status )
        {
            return status == TaskStatus.RanToCompletion || status == TaskStatus.Canceled || status == TaskStatus.Faulted;
        }

        /// <summary> Query if this object is busy. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> <c>true</c> if busy; otherwise <c>false</c> </returns>
        private bool IsBusy()
        {
            return !this.IsDisposed && this.ActionTask is object && !IsTaskEnded( this.ActionTask.Status );
            // Return Not Me.IsDisposed AndAlso Me.ActionTask.IsTaskActive
        }

        /// <summary> Uses the message worker to flush any queued messages. </summary>
        /// <remarks>
        /// If the calling thread is different from the thread that created the TextBox control, this
        /// method creates a SetTextCallback and calls itself asynchronously using the Invoke method.
        /// </remarks>
        private void FlushMessagesTask()
        {
            if ( !(this.IsBusy() || this.IsCancellationRequested()) )
            {
                this.AsyncTask = this.StartAsyncTask();
            }
        }

        /// <summary> Start Asynchronous task. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> A Task. </returns>
        private async Task StartAsyncTask()
        {
            this.ActionTask = Task.Run( this.UpdateDisplay );
            await this.ActionTask;
        }

        #endregion

        #region " WORKER "

        /// <summary> The background worker is used for setting the messages text box
        /// in a thread safe way. </summary>
        private BackgroundWorker _Worker;

        private BackgroundWorker Worker
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._Worker;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._Worker != null )
                {
                    this._Worker.RunWorkerCompleted -= this.UpdateDisplay;
                }

                this._Worker = value;
                if ( this._Worker != null )
                {
                    this._Worker.RunWorkerCompleted += this.UpdateDisplay;
                }
            }
        }

        /// <summary> Dispose worker. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void DisposeWorker()
        {
            try
            {
                this.CancellationTokenSource?.Cancel();
                if ( this.Worker is object )
                {
                    this.Worker.CancelAsync();
                    if ( !(this.Worker.IsBusy || this.Worker.CancellationPending) )
                    {
                        this.Worker.Dispose();
                        this.Worker = null;
                    }
                }
            }
            catch
            {
            }
        }

        /// <summary>
        /// This event handler sets the Text property of the TextBox control. It is called on the thread
        /// that created the TextBox control, so the call is thread-safe.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Run worker completed event information. </param>
        private void UpdateDisplay( object sender, RunWorkerCompletedEventArgs e )
        {
            if ( !(this.IsDisposed || e.Cancelled || e.Error is object) )
            {
                this.UpdateDisplay();
            }
        }

        #endregion

        #region " MESSAGE TRACE LISTENER "

        /// <summary> The trace source listener. </summary>
        private MessageTraceSourceListener _TraceSourceListener;

        /// <summary> Listens the given source. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="source"> Source for the. </param>
        public void Listen( TraceSource source )
        {
            if ( source is object && this._TraceSourceListener is null )
            {
                // Me._TraceSourceListener = New isr.Core.Forma.MessageTraceSourceListener With {.Listener = Me}
                this._TraceSourceListener = MessageTraceSourceListener.Create();
                this._TraceSourceListener.Listener = this;
                _ = source.Listeners.Add( this._TraceSourceListener );
            }
        }

        /// <summary> The trace listener. </summary>
        private MessageTraceListener _TraceListener;

        /// <summary> Listens the given source. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public void Listen()
        {
            if ( this._TraceListener is null )
            {
                this._TraceListener = MessageTraceListener.Create();
                this._TraceListener.Listener = this;
                // Me._TraceListener = New MessageTraceListener With {.Listener = Me}
                _ = Trace.Listeners.Add( this._TraceListener );
            }
        }

        #endregion

    }
}
