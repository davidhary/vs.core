
using System.Diagnostics;

namespace isr.Core.Forma
{
    public partial class TraceMessagesBox : ITraceMessageListener
    {

        /// <summary> Trace event overriding the trace level. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The event message. </param>
        /// <returns> A String. </returns>
        public string TraceEventOverride( TraceMessage value )
        {
            if ( value is null )
            {
                return string.Empty;
            }
            else
            {
                this.AddMessage( value );
                System.Windows.Forms.Application.DoEvents();
                return value.Details;
            }
        }

        /// <summary> Writes a trace event to the trace listener overriding the trace level. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="id">        The identifier. </param>
        /// <param name="format">    The message format. </param>
        /// <param name="args">      Specified the message arguments. </param>
        /// <returns> The trace message details. </returns>
        public string TraceEventOverride( TraceEventType eventType, int id, string format, params object[] args )
        {
            return this.TraceEventOverride( new TraceMessage( eventType, id, format, args ) );
        }

        /// <summary> Writes a trace event to the trace listeners. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The event message. </param>
        /// <returns> The trace message details. </returns>
        public string TraceEvent( TraceMessage value )
        {
            if ( value is null )
            {
                return string.Empty;
            }
            else
            {
                if ( this.ShouldTrace( value.EventType ) )
                {
                    this.AddMessage( value );
                }

                System.Windows.Forms.Application.DoEvents();
                return value.Details;
            }
        }

        /// <summary> Writes a trace event to the trace listeners. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="id">        The identifier. </param>
        /// <param name="format">    The message format. </param>
        /// <param name="args">      Specified the message arguments. </param>
        /// <returns> The trace message details. </returns>
        public string TraceEvent( TraceEventType eventType, int id, string format, params object[] args )
        {
            return this.TraceEvent( new TraceMessage( eventType, id, format, args ) );
        }

    }
}
