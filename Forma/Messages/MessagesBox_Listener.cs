using System;
using System.Diagnostics;

namespace isr.Core.Forma
{
    public partial class MessagesBox : IMessageListener
    {

        #region " I MESSAGE LISTENER "

        /// <summary> Gets the is disposed. </summary>
        /// <value> The is disposed. </value>
        public new bool IsDisposed => base.IsDisposed;

        /// <summary> Writes. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="message"> The message to add. </param>
        public void Write( string message )
        {
            _ = this.AddMessage( message );
        }

        /// <summary> Writes a line. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="message"> The message to add. </param>
        public void WriteLine( string message )
        {
            _ = this.AddMessage( message );
        }

        /// <summary> Registers this event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="level"> The level. </param>
        public virtual void Register( TraceEventType level )
        {
        }

        /// <summary> Gets the trace level. </summary>
        /// <value> The trace level. </value>
        public virtual TraceEventType TraceLevel { get; set; }

        /// <summary> Checks if the log should trace the event type. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The <see cref="TraceEventType">event type</see>. </param>
        /// <returns> <c>True</c> If the log should trace; <c>False</c> otherwise. </returns>
        public bool ShouldTrace( TraceEventType value )
        {
            return value <= this.TraceLevel;
        }

        /// <summary> Gets the sentinel indicating this listener as thread safe. </summary>
        /// <value> True if thread safe. </value>
        public virtual bool IsThreadSafe => true;

        /// <summary> Gets a unique identifier. </summary>
        /// <value> The identifier of the unique. </value>
        public Guid UniqueId { get; private set; } = Guid.NewGuid();

        /// <summary> Tests if this ITraceMessageListener is considered equal to another. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="other"> The i trace message listener to compare to this object. </param>
        /// <returns>
        /// <c>true</c> if the objects are considered equal, <c>false</c> if they are not.
        /// </returns>
        public bool Equals( IMessageListener other )
        {
            return other is object && Equals( other.UniqueId, this.UniqueId );
        }

        /// <summary> Applies the trace level this described by value. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The <see cref="TraceEventType">trace level</see> value. </param>
        private void ApplyTraceLevelThis( TraceEventType value )
        {
            this.TraceLevel = value;
        }

        /// <summary> Applies the trace level. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The <see cref="TraceEventType">trace level</see> value. </param>
        public void ApplyTraceLevel( TraceEventType value )
        {
            this.ApplyTraceLevelThis( value );
        }

        /// <summary> Gets the type of the listener. </summary>
        /// <value> The type of the listener. </value>
        public ListenerType ListenerType => ListenerType.Display;

        #endregion


    }
}
