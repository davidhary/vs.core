﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace isr.Core.Forma
{
    public partial class MessagesBox
    {

        #region " NOTIFY "

        /// <summary>
        /// Asynchronously notifies (posts) property change on a different thread. Unsafe for cross
        /// threading; much faster.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> Property Changed event information. </param>
        protected virtual void NotifyPropertyChanged( PropertyChangedEventArgs e )
        {
            this._PropertyChangedHandlers.Post( this, e );
        }

        /// <summary>
        /// Synchronously notifies property change on the synchronization thread using 'Send' to prevent
        /// cross thread exceptions.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="name"> (Optional) Name of the runtime compiler services caller member.
        /// </param>
        protected void NotifyPropertyChanged( [CallerMemberName()] string name = null )
        {
            if ( !string.IsNullOrWhiteSpace( name ) )
            {
                this.NotifyPropertyChanged( new PropertyChangedEventArgs( name ) );
            }
        }

        /// <summary>
        /// Asynchronously notifies (posts) property change on a different thread. Unsafe for cross
        /// threading.
        /// </summary>
        /// <remarks>
        /// Overhead of this method is 7 to 10 times larger than naked raise event. This has no advantage
        /// even with slow handler functions.
        /// </remarks>
        /// <param name="e"> Property Changed event information. </param>
        protected virtual void AsyncNotifyPropertyChanged( PropertyChangedEventArgs e )
        {
            this._PropertyChangedHandlers.Post( this, e );
        }

        /// <summary>
        /// Asynchronously notifies (posts) property change on a different thread. Unsafe for cross
        /// threading; much faster.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="name"> (Optional) Name of the runtime compiler services caller member.
        /// </param>
        protected void AsyncNotifyPropertyChanged( [CallerMemberName()] string name = null )
        {
            if ( !string.IsNullOrWhiteSpace( name ) )
            {
                this.AsyncNotifyPropertyChanged( new PropertyChangedEventArgs( name ) );
            }
        }

        /// <summary>
        /// Synchronously notifies property change on the synchronization thread using 'Send' to prevent
        /// cross thread exceptions.
        /// </summary>
        /// <remarks>
        /// Overhead of this method is 3 to 4 times larger than naked raise event. This is the best
        /// approach.
        /// </remarks>
        /// <param name="e"> Property Changed event information. </param>
        protected virtual void SyncNotifyPropertyChanged( PropertyChangedEventArgs e )
        {
            this._PropertyChangedHandlers.Send( this, e );
        }

        /// <summary>
        /// Synchronously notifies property change on the synchronization thread using 'Send' to prevent
        /// cross thread exceptions.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="name"> (Optional) Name of the runtime compiler services caller member.
        /// </param>
        protected void SyncNotifyPropertyChanged( [CallerMemberName()] string name = null )
        {
            if ( !string.IsNullOrWhiteSpace( name ) )
            {
                this.SyncNotifyPropertyChanged( new PropertyChangedEventArgs( name ) );
            }
        }

        #endregion

        #region " RAISE (SEND) "

        /// <summary>
        /// Synchronously notifies property change on the synchronization thread using 'Send' to prevent
        /// cross thread exceptions.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="name"> (Optional) Name of the runtime compiler services caller member.
        /// </param>
        protected void RaisePropertyChanged( [CallerMemberName()] string name = null )
        {
            if ( !string.IsNullOrWhiteSpace( name ) )
            {
                this.RaisePropertyChanged( new PropertyChangedEventArgs( name ) );
            }
        }

        /// <summary>
        /// Synchronously notifies property change on the synchronization thread using 'Send' to prevent
        /// cross thread exceptions.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void RaisePropertyChanged( PropertyChangedEventArgs e )
        {
            this.OnPropertyChanged( this, e );
        }

        #endregion

    }
}