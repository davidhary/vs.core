using System;
using System.Diagnostics;

namespace isr.Core.Forma
{

    /// <summary> A message trace listener. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-03-24 </para>
    /// </remarks>
    public class MessageTraceListener : TraceListener
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public MessageTraceListener() : base()
        {
        }

        /// <summary> Creates a new MessageTraceListener. </summary>
        /// <remarks> This helps implement CA2000. </remarks>
        /// <returns> A list of. </returns>
        public static MessageTraceListener Create()
        {
            MessageTraceListener result = null;
            try
            {
                result = new MessageTraceListener();
            }
            catch
            {
                result?.Dispose();
                throw;
            }

            return result;
        }

        /// <summary> Gets a value indicating whether the trace listener is thread safe. </summary>
        /// <value>
        /// true if the trace listener is thread safe; otherwise, false. The default is false.
        /// </value>
        public override bool IsThreadSafe => this.Listener is object && this.Listener.IsThreadSafe;

        /// <summary> Gets the listener. </summary>
        /// <value> The listener. </value>
        public IMessageListener Listener { get; set; }

        /// <summary>
        /// When overridden in a derived class, writes the specified message to the listener you create
        /// in the derived class.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="message"> A message to write. </param>
        public override void Write( string message )
        {
            // this outputs an additional message.
            this.Listener?.Write( message );
        }

        /// <summary>
        /// When overridden in a derived class, writes a message to the listener you create in the
        /// derived class, followed by a line terminator.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="message"> A message to write. </param>
        public override void WriteLine( string message )
        {
            this.Listener?.WriteLine( message );
        }

        /// <summary>
        /// Writes trace information, a data object and event information to the listener specific output.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="eventCache"> A <see cref="T:System.Diagnostics.TraceEventCache" /> object that
        /// contains the current process ID, thread ID, and stack trace
        /// information. </param>
        /// <param name="source">     A name used to identify the output, typically the name of the
        /// application that generated the trace event. </param>
        /// <param name="eventType">  One of the <see cref="T:System.Diagnostics.TraceEventType" /> values
        /// specifying the type of event that has caused the trace. </param>
        /// <param name="id">         A numeric identifier for the event. </param>
        /// <param name="data">       The trace data to emit. </param>
        public override void TraceData( TraceEventCache eventCache, string source, TraceEventType eventType, int id, object data )
        {
            if ( this.Listener is null || this.Listener.TraceLevel >= eventType )
            {
                base.TraceData( eventCache, source, eventType, id, data );
                this.Listener.Register( eventType );
            }
        }

        /// <summary>
        /// Writes trace information, a message, and event information to the listener specific output.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="eventCache"> A <see cref="T:System.Diagnostics.TraceEventCache" /> object that
        /// contains the current process ID, thread ID, and stack trace
        /// information. </param>
        /// <param name="source">     A name used to identify the output, typically the name of the
        /// application that generated the trace event. </param>
        /// <param name="eventType">  One of the <see cref="T:System.Diagnostics.TraceEventType" /> values
        /// specifying the type of event that has caused the trace. </param>
        /// <param name="id">         A numeric identifier for the event. </param>
        /// <param name="message">    A message to write. </param>
        public override void TraceEvent( TraceEventCache eventCache, string source, TraceEventType eventType, int id, string message )
        {
            if ( this.Listener is object && this.Listener.TraceLevel >= eventType )
            {
                base.TraceEvent( eventCache, source, eventType, id, message );
                this.Listener.Register( eventType );
            }
        }

        /// <summary>
        /// Writes trace information, a formatted array of objects and event information to the listener
        /// specific output.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="eventCache"> A <see cref="T:System.Diagnostics.TraceEventCache" /> object that
        /// contains the current process ID, thread ID, and stack trace
        /// information. </param>
        /// <param name="source">     A name used to identify the output, typically the name of the
        /// application that generated the trace event. </param>
        /// <param name="eventType">  One of the <see cref="T:System.Diagnostics.TraceEventType" /> values
        /// specifying the type of event that has caused the trace. </param>
        /// <param name="id">         A numeric identifier for the event. </param>
        /// <param name="format">     The format of the message to write. </param>
        /// <param name="args">       An object array containing zero or more objects to format. </param>
        public override void TraceEvent( TraceEventCache eventCache, string source, TraceEventType eventType, int id, string format, params object[] args )
        {
            if ( this.Listener is object && this.Listener.TraceLevel >= eventType )
            {
                base.TraceEvent( eventCache, source, eventType, id, format, args );
                this.Listener.Register( eventType );
            }
        }
    }

    /// <summary> A message trace listener. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-03-24 </para>
    /// </remarks>
    public class MessageTraceSourceListener : TraceListener
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public MessageTraceSourceListener() : base()
        {
        }

        /// <summary> Creates a new MessageTraceListener. </summary>
        /// <remarks> This helps implement CA2000. </remarks>
        /// <returns> A list of. </returns>
        public static MessageTraceSourceListener Create()
        {
            MessageTraceSourceListener result = null;
            try
            {
                result = new MessageTraceSourceListener();
            }
            catch
            {
                result?.Dispose();
                throw;
            }

            return result;
        }

        /// <summary> Gets a value indicating whether the trace listener is thread safe. </summary>
        /// <value>
        /// true if the trace listener is thread safe; otherwise, false. The default is false.
        /// </value>
        public override bool IsThreadSafe => this.Listener is object && this.Listener.IsThreadSafe;

        /// <summary> Gets or sets the listener. </summary>
        /// <value> The listener. </value>
        public IMessageListener Listener { get; set; }

        /// <summary>
        /// When overridden in a derived class, writes the specified message to the listener you create
        /// in the derived class.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="message"> A message to write. </param>
        public override void Write( string message )
        {
            // this outputs an additional message.
            // Me.Listener?.Write(message)
        }

        /// <summary>
        /// When overridden in a derived class, writes a message to the listener you create in the
        /// derived class, followed by a line terminator.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="message"> A message to write. </param>
        public override void WriteLine( string message )
        {
            this.Listener?.WriteLine( message );
        }

        /// <summary>
        /// Writes trace information, a data object and event information to the listener specific output.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="eventCache"> A <see cref="T:System.Diagnostics.TraceEventCache" /> object that
        /// contains the current process ID, thread ID, and stack trace
        /// information. </param>
        /// <param name="source">     A name used to identify the output, typically the name of the
        /// application that generated the trace event. </param>
        /// <param name="eventType">  One of the <see cref="T:System.Diagnostics.TraceEventType" /> values
        /// specifying the type of event that has caused the trace. </param>
        /// <param name="id">         A numeric identifier for the event. </param>
        /// <param name="data">       The trace data to emit. </param>
        public override void TraceData( TraceEventCache eventCache, string source, TraceEventType eventType, int id, object data )
        {
            if ( this.Listener is null || this.Listener.TraceLevel >= eventType )
            {
                base.TraceData( eventCache, source, eventType, id, data );
                this.Listener.Register( eventType );
            }
        }

        /// <summary>
        /// Writes trace information, a message, and event information to the listener specific output.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="eventCache"> A <see cref="T:System.Diagnostics.TraceEventCache" /> object that
        /// contains the current process ID, thread ID, and stack trace
        /// information. </param>
        /// <param name="source">     A name used to identify the output, typically the name of the
        /// application that generated the trace event. </param>
        /// <param name="eventType">  One of the <see cref="T:System.Diagnostics.TraceEventType" /> values
        /// specifying the type of event that has caused the trace. </param>
        /// <param name="id">         A numeric identifier for the event. </param>
        /// <param name="message">    A message to write. </param>
        public override void TraceEvent( TraceEventCache eventCache, string source, TraceEventType eventType, int id, string message )
        {
            if ( this.Listener is object && this.Listener.TraceLevel >= eventType )
            {
                base.TraceEvent( eventCache, source, eventType, id, message );
                this.Listener.Register( eventType );
            }
        }

        /// <summary>
        /// Writes trace information, a formatted array of objects and event information to the listener
        /// specific output.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="eventCache"> A <see cref="T:System.Diagnostics.TraceEventCache" /> object that
        /// contains the current process ID, thread ID, and stack trace
        /// information. </param>
        /// <param name="source">     A name used to identify the output, typically the name of the
        /// application that generated the trace event. </param>
        /// <param name="eventType">  One of the <see cref="T:System.Diagnostics.TraceEventType" /> values
        /// specifying the type of event that has caused the trace. </param>
        /// <param name="id">         A numeric identifier for the event. </param>
        /// <param name="format">     The format of the message to write. </param>
        /// <param name="args">       An object array containing zero or more objects to format. </param>
        public override void TraceEvent( TraceEventCache eventCache, string source, TraceEventType eventType, int id, string format, params object[] args )
        {
            if ( this.Listener is object && this.Listener.TraceLevel >= eventType )
            {
                base.TraceEvent( eventCache, source, eventType, id, format, args );
                this.Listener.Register( eventType );
            }
        }
    }

    /// <summary>   A trace methods. </summary>
    /// <remarks>   David, 2020-09-25. </remarks>
    public static class TraceMethods
    {

        /// <summary> Publishes and logs the message. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="source">    Source for the. </param>
        /// <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
        /// <param name="id">        The identifier to use with the trace event. </param>
        /// <param name="format">    Describes the format to use. </param>
        /// <param name="args">      A variable-length parameters list containing arguments. </param>
        /// <returns> A String. </returns>
        public static string Publish( this TraceSource source, TraceEventType eventType, int id, string format, params object[] args )
        {
            return source.Publish( new TraceMessage( eventType, id, format, args ) );
        }

        /// <summary> Adds a message to the display. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="source"> Source for the. </param>
        /// <param name="value">  The value. </param>
        /// <returns> A String. </returns>
        public static string Publish( this TraceSource source, TraceMessage value )
        {
            string result = string.Empty;
            if ( value is null || string.IsNullOrWhiteSpace( value.Details ) )
            {
                Debug.Assert( !Debugger.IsAttached, "Empty trace message @", new StackTrace( true ).ToString().Split( Environment.NewLine.ToCharArray() )[0] );
            }
            else
            {
                result = value.ToString();
                source?.TraceEvent( value.EventType, value.Id, result );
            }

            return result;
        }

        /// <summary> Publishes and logs the message. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
        /// <param name="id">        The identifier to use with the trace event. </param>
        /// <param name="format">    Describes the format to use. </param>
        /// <param name="args">      A variable-length parameters list containing arguments. </param>
        /// <returns> A String. </returns>
        public static string Publish( TraceEventType eventType, int id, string format, params object[] args )
        {
            return Publish( new TraceMessage( eventType, id, format, args ) );
        }

        /// <summary> Adds a message to the display. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> A String. </returns>
        public static string Publish( TraceMessage value )
        {
            string result = string.Empty;
            if ( value is null || string.IsNullOrWhiteSpace( value.Details ) )
            {
                Debug.Assert( !Debugger.IsAttached, "Empty trace message @", new StackTrace( true ).ToString().Split( Environment.NewLine.ToCharArray() )[0] );
            }
            else
            {
                result = value.ToString();
                Trace.Write( result );
                if ( value.EventType <= TraceEventType.Error )
                {
                    Trace.TraceError( "" );
                }
                else if ( value.EventType <= TraceEventType.Warning )
                {
                    Trace.TraceWarning( "" );
                }
                else
                {
                    Trace.TraceInformation( "" );
                }
            }

            return result;
        }

        /// <summary> Adds a message to the display. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> A String. </returns>
        public static string Publish( string value )
        {
            Trace.Write( value );
            return value;
        }

    }
}
