using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Forma
{

    /// <summary> A trace message tool strip. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-03-21 </para>
    /// </remarks>
    public partial class TraceMessageToolStrip : ToolStrip
    {

        #region " CONSTRUCTION "

        /// <summary> Gets or sets the initializing component. </summary>
        /// <value> The initializing component. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0052:Remove unread private members", Justification = "<Pending>" )]
        private bool InitializingComponent { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Windows.Forms.ToolStrip" /> class.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public TraceMessageToolStrip() : base()
        {
            this.InitializingComponent = true;
            this.InitializeComponent();
            this.InitializingComponent = false;
            this.Renderer = new BackgroundRenderer();
            this.TraceMessagesStack = new TraceMessagesStack();
            this.ApplyTraceEvent( TraceEventType.Start );
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.ToolStrip" />
        /// and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> <see langword="true" /> to release both managed and unmanaged
        /// resources; <see langword="false" /> to release only unmanaged
        /// resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this._Components is object )
                    {
                        this._Components.Dispose();
                        this._Components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        /// <summary>   The details combo box. </summary>
        private ToolStripSpringComboBox _DetailsComboBox;

        /// <summary>   The message level split button. </summary>
        private ToolStripSplitButton _MessageLevelSplitButton;

        /// <summary>   The drop last message menu item. </summary>
        private ToolStripMenuItem _DropLastMessageMenuItem;


        /// <summary>   Gets or sets the drop last message menu item. </summary>
        /// <value> The drop last message menu item. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private ToolStripMenuItem DropLastMessageMenuItem
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._DropLastMessageMenuItem;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._DropLastMessageMenuItem != null )
                {
                    this._DropLastMessageMenuItem.Click -= this.DropLastMessageMenuItem_Click;
                }

                this._DropLastMessageMenuItem = value;
                if ( this._DropLastMessageMenuItem != null )
                {
                    this._DropLastMessageMenuItem.Click += this.DropLastMessageMenuItem_Click;
                }
            }
        }

        /// <summary> The with events. </summary>
        private ToolStripMenuItem _ClearHistoryMenuItem;


        /// <summary>   Gets or sets the clear history menu item. </summary>
        /// <value> The clear history menu item. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private ToolStripMenuItem ClearHistoryMenuItem
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._ClearHistoryMenuItem;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._ClearHistoryMenuItem != null )
                {

                    this._ClearHistoryMenuItem.Click -= this.ClearHistoryMenuItem_Click;
                }

                this._ClearHistoryMenuItem = value;
                if ( this._ClearHistoryMenuItem != null )
                {
                    this._ClearHistoryMenuItem.Click += this.ClearHistoryMenuItem_Click;
                }
            }
        }

        /// <summary> The components. </summary>
        private System.ComponentModel.IContainer _Components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this._Components = new System.ComponentModel.Container();
            this._MessageLevelSplitButton = new ToolStripSplitButton();
            this._DropLastMessageMenuItem = new ToolStripMenuItem();
            this._DropLastMessageMenuItem.Click += new EventHandler( this.DropLastMessageMenuItem_Click );
            this._ClearHistoryMenuItem = new ToolStripMenuItem();
            this._ClearHistoryMenuItem.Click += new EventHandler( this.ClearHistoryMenuItem_Click );
            this._DetailsComboBox = new ToolStripSpringComboBox();
            this.SuspendLayout();
            // 
            // _DetailsComboBox
            // 
            this._DetailsComboBox.Name = "_DetailsComboBox";
            this._DetailsComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            this._DetailsComboBox.Size = new Size( 121, 29 );
            this._DetailsComboBox.ToolTipText = "Message details";
            this._DetailsComboBox.AutoSize = true;
            // 
            // _MessageLevelSplitButton
            // 
            this._MessageLevelSplitButton.Alignment = ToolStripItemAlignment.Right;
            this._MessageLevelSplitButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this._MessageLevelSplitButton.DropDownItems.AddRange( new ToolStripItem[] { this._DropLastMessageMenuItem, this._ClearHistoryMenuItem } );
            this._MessageLevelSplitButton.Image = My.Resources.Resources.Empty;
            this._MessageLevelSplitButton.ImageScaling = ToolStripItemImageScaling.None;
            this._MessageLevelSplitButton.ImageTransparentColor = Color.Magenta;
            this._MessageLevelSplitButton.Name = "_MessageLevelSplitButton";
            this._MessageLevelSplitButton.Size = new Size( 38, 26 );
            this._MessageLevelSplitButton.AutoSize = false;
            this._MessageLevelSplitButton.ToolTipText = "Message level; Clear menu";
            // 
            // _DropLastMessageMenuItem
            // 
            this._DropLastMessageMenuItem.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this._DropLastMessageMenuItem.Image = My.Resources.Resources.Clear;
            this._DropLastMessageMenuItem.ImageScaling = ToolStripItemImageScaling.None;
            this._DropLastMessageMenuItem.Name = "__DropLastMessageMenuItem";
            this._DropLastMessageMenuItem.Size = new Size( 158, 28 );
            this._DropLastMessageMenuItem.ToolTipText = "Drops last message";
            // 
            // _ClearHistoryMenuItem
            // 
            this._ClearHistoryMenuItem.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this._ClearHistoryMenuItem.Image = My.Resources.Resources.ClearHistory;
            this._ClearHistoryMenuItem.ImageScaling = ToolStripItemImageScaling.None;
            this._ClearHistoryMenuItem.Name = "__ClearHistoryMenuItem";
            this._ClearHistoryMenuItem.Size = new Size( 158, 28 );
            this._ClearHistoryMenuItem.ToolTipText = "Clear History";
            // 
            // TraceMessageToolStrip
            // 
            this.Font = new Font( "Segoe UI", 9.75f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte( 0 ) );
            this.GripStyle = ToolStripGripStyle.Hidden;
            this.Stretch = true;
            this.Items.AddRange( new ToolStripItem[] { this._DetailsComboBox, this._MessageLevelSplitButton } );
            this.Size = new Size( 799, 29 );
            this.Text = "Trace Message Strip";
            this.ResumeLayout( false );
            this.PerformLayout();
        }

        #endregion

        #region " BACKGROUND RENDERER "

        /// <summary> A background renderer. </summary>
        /// <remarks>
        /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
        /// Licensed under The MIT License.</para><para>
        /// David, 2017-05-05 </para>
        /// </remarks>
        private class BackgroundRenderer : ToolStripProfessionalRenderer
        {

            /// <summary>
            /// Raises the <see cref="E:System.Windows.Forms.ToolStripRenderer.RenderLabelBackground" />
            /// event.
            /// </summary>
            /// <remarks> David, 2020-09-24. </remarks>
            /// <param name="e"> A <see cref="T:System.Windows.Forms.ToolStripItemRenderEventArgs" /> that
            /// contains the event data. </param>
            protected override void OnRenderLabelBackground( ToolStripItemRenderEventArgs e )
            {
                if ( e is object && e.Item.BackColor != SystemColors.ControlDark )
                {
                    using var brush = new SolidBrush( e.Item.BackColor );
                    e.Graphics.FillRectangle( brush, e.Item.ContentRectangle );
                }
            }
        }

        #endregion

        #region " STACK "

        /// <summary> Trace messages stack message pushed. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void TraceMessagesStack_MessagePushed( object sender, EventArgs e )
        {
            this.OnStackChanged( e );
        }

        /// <summary> The with events. </summary>
        private TraceMessagesStack _TraceMessagesStackThis;

        private TraceMessagesStack TraceMessagesStackThis
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._TraceMessagesStackThis;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._TraceMessagesStackThis != null )
                {
                    this._TraceMessagesStackThis.MessagePushed -= this.TraceMessagesStack_MessagePushed;
                }

                this._TraceMessagesStackThis = value;
                if ( this._TraceMessagesStackThis != null )
                {
                    this._TraceMessagesStackThis.MessagePushed += this.TraceMessagesStack_MessagePushed;
                }
            }
        }

        /// <summary> Gets or sets a stack of trace messages. </summary>
        /// <value> A Stack of trace messages. </value>
        public TraceMessagesStack TraceMessagesStack
        {
            get => this.TraceMessagesStackThis;

            set {
                this.TraceMessagesStackThis = value;
                this.OnStackChanged( EventArgs.Empty );
            }
        }

        /// <summary> Returns the top-of-stack object without removing it. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> The current top-of-stack object. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        public TraceMessage Peek()
        {
            return this.TraceMessagesStack.Any ? this.TraceMessagesStack.TryPeek() : TraceMessage.Empty;
        }

        /// <summary> Removes and returns the top-of-stack object. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public void Pop()
        {
            if ( this.TraceMessagesStack.Any )
            {
                _ = this.TraceMessagesStack.TryPop();
                this.OnStackChanged( EventArgs.Empty );
            }
        }

        /// <summary> Pushes an object onto this stack. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="message"> The message to push. </param>
        public void Push( TraceMessage message )
        {
            this.TraceMessagesStack.Push( message );
            this.OnStackChanged( EventArgs.Empty );
        }

        /// <summary> Pushes an object onto this stack. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="message">   The message to push. </param>
        public void Push( TraceEventType eventType, string message )
        {
            this.Push( new TraceMessage( eventType, 0, message ) );
        }

        #endregion

        #region " DISPLAY "

        /// <summary> Type of the last event. </summary>
        private TraceEventType _LastEventType;

        /// <summary> Applies the trace event described by value. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The value. </param>
        private void ApplyTraceEvent( TraceEventType value )
        {
            this._LastEventType = value;
            switch ( value )
            {
                case var @case when @case <= TraceEventType.Error:
                    {
                        this._MessageLevelSplitButton.Image = My.Resources.Resources.ErrorLevel;
                        break;
                    }

                case var case1 when case1 <= TraceEventType.Warning:
                    {
                        this._MessageLevelSplitButton.Image = My.Resources.Resources.WarningLevel;
                        break;
                    }

                case var case2 when case2 <= TraceEventType.Information:
                    {
                        this._MessageLevelSplitButton.Image = My.Resources.Resources.InformationLevel;
                        break;
                    }

                case var case3 when case3 <= TraceEventType.Verbose:
                    {
                        this._MessageLevelSplitButton.Image = My.Resources.Resources.VerboseLevel;
                        break;
                    }

                default:
                    {
                        this._MessageLevelSplitButton.Image = My.Resources.Resources.Empty;
                        break;
                    }
            }

            this._MessageLevelSplitButton.Invalidate();
            Application.DoEvents();
        }

        /// <summary> Gets or sets the type of the last event. </summary>
        /// <value> The type of the last event. </value>
        [System.ComponentModel.Browsable( false )]
        [System.ComponentModel.DesignerSerializationVisibility( System.ComponentModel.DesignerSerializationVisibility.Hidden )]
        private TraceEventType LastEventType
        {
            get => this._LastEventType;

            set {
                if ( value != this.LastEventType ) // 
                {
                    this.ApplyTraceEvent( value );
                }
            }
        }

        /// <summary> Displays the given message. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="message"> The message to push. </param>
        private void Display( TraceMessage message )
        {
            this._DetailsComboBox.Text = message.Details;
            this._DetailsComboBox.Invalidate();
            this.LastEventType = message.EventType;
        }

        /// <summary> Applies the back color described by color. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="color"> The color. </param>
        public void ApplyBackColor( Color color )
        {
            this._DetailsComboBox.BackColor = color;
        }

        #endregion

        #region " CLEAR "

        /// <summary> Handles the stack changed event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected void OnStackChanged( EventArgs e )
        {
            if ( e is null )
            {
                return;
            }

            if ( this.TraceMessagesStack.Any )
            {
                this._DetailsComboBox.ComboBox.DataSource = this.TraceMessagesStack.TraceMessages;
                this.Display( this.TraceMessagesStack.TryPeek() );
            }
            else
            {
                this.LastEventType = TraceEventType.Start;
                this._DetailsComboBox.ComboBox.DataSource = null;
                this._DetailsComboBox.ComboBox.Items.Clear();
                this._DetailsComboBox.Text = string.Empty;
            }

            this._DetailsComboBox.Invalidate();
        }

        /// <summary> Drops the last message. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public void DropLastMessage()
        {
            this.Pop();
        }

        /// <summary> Clears the history. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public void ClearHistory()
        {
            this.TraceMessagesStack.Clear();
            this.OnStackChanged( EventArgs.Empty );
        }

        private void ClearHistoryMenuItem_Click( object sender, EventArgs e )
        {
            this.ClearHistory();
        }

        private void DropLastMessageMenuItem_Click( object sender, EventArgs e )
        {
            this.DropLastMessage();
        }



        #endregion

    }
}
