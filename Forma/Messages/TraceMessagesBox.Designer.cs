﻿using System.Diagnostics;

namespace isr.Core.Forma
{
    [Microsoft.VisualBasic.CompilerServices.DesignerGenerated()]
    public partial class TraceMessagesBox
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            Multiline = true;
            ReadOnly = true;
            CausesValidation = false;
            ScrollBars = System.Windows.Forms.ScrollBars.Both;
            Size = new System.Drawing.Size(150, 150);
        }
    }
}