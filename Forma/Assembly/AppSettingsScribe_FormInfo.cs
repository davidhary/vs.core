using System;

namespace isr.Core.Forma.My
{
    /// <summary>
    /// Reads and writes application settings to the Application Setting section of the program
    /// configuration file.
    /// </summary>
    /// <remarks>
    /// (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2009-10-21, 1.0.3581 </para>
    /// </remarks>
    public partial class AppSettingsScribe : isr.Core.AppSettingsScribe
    {

        #region " FORM SIZE AND POSITION "

        /// <summary> Reads the application setting. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="key">          The key. </param>
        /// <param name="defaultValue"> The default value. </param>
        /// <returns> Windows.Forms.FormStartPosition. </returns>
        public static System.Windows.Forms.FormStartPosition ReadValue( string key, System.Windows.Forms.FormStartPosition defaultValue )
        {
            if ( !(Enum.TryParse( Get().ReadValue( key ), out System.Windows.Forms.FormStartPosition loadedStartPosition )) )
            {
                loadedStartPosition = defaultValue;
            }

            return loadedStartPosition;
        }

        /// <summary> Reads the application setting. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="key">          The key. </param>
        /// <param name="defaultValue"> The default value. </param>
        /// <returns> Windows.Forms.FormWindowState. </returns>
        public static System.Windows.Forms.FormWindowState ReadValue( string key, System.Windows.Forms.FormWindowState defaultValue )
        {
            if ( !(Enum.TryParse( Get().ReadValue( key ), out System.Windows.Forms.FormWindowState loadedWindowsState )) )
            {
                loadedWindowsState = defaultValue;
            }

            return loadedWindowsState;
        }

        /// <summary> Writes the application setting. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="key">   The key. </param>
        /// <param name="value"> The value. </param>
        public static void WriteValue( string key, System.Windows.Forms.FormWindowState value )
        {
            _ = Get().WriteValue( key, value.ToString() );
        }

        /// <summary> Writes the application setting. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="key">   The key. </param>
        /// <param name="value"> The value. </param>
        public static void WriteValue( string key, System.Windows.Forms.FormStartPosition value )
        {
            _ = Get().WriteValue( key, value.ToString() );
        }

        #endregion

    }
}
