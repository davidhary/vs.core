using System;
using System.Windows.Forms;

using isr.Core.Forma.ExceptionExtensions;

namespace isr.Core.Forma
{

    /// <summary> Dialog for setting the print preview. </summary>
    /// <remarks>
    /// David, 2020-09-10.
    /// https://stackoverflow.com/questions/40236241/how-to-add-print-dialog-to-the-printpreviewdialog.
    /// </remarks>
    public class PrintPreviewDialog : System.Windows.Forms.PrintPreviewDialog
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Windows.Forms.PrintPreviewDialog" />
        /// class.
        /// </summary>
        /// <remarks> David, 2020-09-08. </remarks>
        public PrintPreviewDialog() : base()
        {
            this.ReplacePrintDialogButton();
        }

        /// <summary> Replace print dialog button. </summary>
        /// <remarks> David, 2020-09-08. </remarks>
        private void ReplacePrintDialogButton()
        {
            if ( this.Controls[1] is ToolStrip printToolStrip )
            {
                var b = new ToolStripButton() { DisplayStyle = ToolStripItemDisplayStyle.Image };
                b.Click += this.PrintDialog_PrintClick;
                b.Image = printToolStrip.ImageList.Images[0];
                printToolStrip.Items.RemoveAt( 0 );
                printToolStrip.Items.Insert( 0, b );
            }
        }

        /// <summary> Event handler. Called by PrintDialog for print click events. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        private void PrintDialog_PrintClick( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = "instantiating the print dialog";
                using var pd = new PrintDialog() { Document = Document };
                activity = "showing the print dialog";
                if ( pd.ShowDialog() == DialogResult.OK )
                {
                    activity = "setting printer settings";
                    this.Document.PrinterSettings = pd.PrinterSettings;
                    activity = "printing";
                    this.Document.Print();
                }
            }
            catch ( Exception ex )
            {
                _ = System.Windows.Forms.MessageBox.Show( $"Exception {activity};. {ex.ToFullBlownString()}", "Print Dialog Exception", MessageBoxButtons.OK, MessageBoxIcon.Error );
            }
        }
    }
}
