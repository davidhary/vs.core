using System;
using System.Collections.Generic;
using System.Diagnostics;

using isr.Core.Forma.ExceptionExtensions;

namespace isr.Core.Forma
{
    public partial class ListenerUserFormBase : ITalker
    {

        #region " ASSIGNMENTS "

        /// <summary> The talker. </summary>
        private ITraceMessageTalker _Talker;

        /// <summary> Gets or sets the trace message talker. </summary>
        /// <value> The trace message talker. </value>
        public ITraceMessageTalker Talker
        {
            get => this._Talker;

            private set {
                if ( this._Talker is object )
                {
                    this.RemoveListeners();
                }

                this._Talker = value;
            }
        }

        /// <summary> True if is assigned talker, false if not. </summary>
        private bool _IsAssignedTalker;

        /// <summary> Assigns a talker. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="talker"> The talker. </param>
        public virtual void AssignTalker( ITraceMessageTalker talker )
        {
            this._IsAssignedTalker = talker is object;
            this.Talker = this._IsAssignedTalker ? talker : new TraceMessageTalker();
            this.IdentifyTalkers();
        }

        /// <summary> Identifies talkers. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public virtual void IdentifyTalkers()
        {
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        #endregion

        #region " LISTENERS "

        /// <summary>
        /// Removes the private listeners. Removes all listeners if the talker was not assigned.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public virtual void RemoveListeners()
        {
            this.RemovePrivateListeners();
            if ( !this._IsAssignedTalker )
            {
                this.Talker.RemoveListeners();
            }
        }

        /// <summary> The private listeners. </summary>
        private List<IMessageListener> _PrivateListeners;

        /// <summary> Gets the private listeners. </summary>
        /// <value> The private listeners. </value>
        public virtual IList<IMessageListener> PrivateListeners
        {
            get {
                if ( this._PrivateListeners is null )
                {
                    this._PrivateListeners = new List<IMessageListener>();
                }

                return this._PrivateListeners;
            }
        }

        /// <summary> Adds a private listener. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="listener"> The listener. </param>
        public virtual void AddPrivateListener( IMessageListener listener )
        {
            if ( this.PrivateListeners is object )
            {
                this._PrivateListeners.Add( listener );
            }

            this.AddListener( listener );
        }

        /// <summary> Adds private listeners. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="listeners"> The listeners. </param>
        public virtual void AddPrivateListeners( IList<IMessageListener> listeners )
        {
            if ( listeners is null )
            {
                throw new ArgumentNullException( nameof( listeners ) );
            }

            if ( this.PrivateListeners is object )
            {
                this._PrivateListeners.AddRange( listeners );
            }

            this.AddListeners( listeners );
        }

        /// <summary> Adds private listeners. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="talker"> The talker. </param>
        public virtual void AddPrivateListeners( ITraceMessageTalker talker )
        {
            if ( talker is null )
            {
                throw new ArgumentNullException( nameof( talker ) );
            }

            this.AddPrivateListeners( talker.Listeners );
        }

        /// <summary> Removes the private listener described by listener. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="listener"> The listener. </param>
        public virtual void RemovePrivateListener( IMessageListener listener )
        {
            this.RemoveListener( listener );
            if ( this.PrivateListeners is object )
            {
                _ = this._PrivateListeners.Remove( listener );
            }
        }

        /// <summary> Removes the private listeners. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public virtual void RemovePrivateListeners()
        {
            foreach ( IMessageListener listener in this.PrivateListeners )
            {
                this.RemoveListener( listener );
            }

            this._PrivateListeners.Clear();
        }

        /// <summary> Removes the listener described by listener. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="listener"> The listener. </param>
        public virtual void RemoveListener( IMessageListener listener )
        {
            this.Talker.RemoveListener( listener );
        }

        /// <summary> Removes the specified listeners. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="listeners"> The listeners. </param>
        public virtual void RemoveListeners( IList<IMessageListener> listeners )
        {
            if ( listeners is null )
            {
                throw new ArgumentNullException( nameof( listeners ) );
            }

            foreach ( IMessageListener listener in listeners )
            {
                this.RemoveListener( listener );
            }
        }

        /// <summary> Adds a listener. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="listener"> The listener. </param>
        public virtual void AddListener( IMessageListener listener )
        {
            this.Talker.AddListener( listener );
            this.IdentifyTalkers();
        }

        /// <summary> Adds the listeners. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="listeners"> The listeners. </param>
        public virtual void AddListeners( IList<IMessageListener> listeners )
        {
            if ( listeners is null )
            {
                throw new ArgumentNullException( nameof( listeners ) );
            }

            foreach ( IMessageListener listener in listeners )
            {
                this.AddListener( listener );
            }
        }

        /// <summary> Adds the listeners. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="talker"> The talker. </param>
        public virtual void AddListeners( ITraceMessageTalker talker )
        {
            if ( talker is null )
            {
                throw new ArgumentNullException( nameof( talker ) );
            }

            this.AddListeners( talker.Listeners );
        }

        /// <summary> Applies the trace level to all listeners to the specified type. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="listenerType"> Type of the listener. </param>
        /// <param name="value">        The value. </param>
        public virtual void ApplyListenerTraceLevel( ListenerType listenerType, TraceEventType value )
        {
            // this should apply only to the listeners associated with this form
            // Not this: Me.Talker.ApplyListenerTraceLevel(listenerType, value)
            this.IdentifyTalkers();
        }

        /// <summary> Applies the trace level type to all talkers. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="listenerType"> Type of the trace level. </param>
        /// <param name="value">        The value. </param>
        public void ApplyTalkerTraceLevel( ListenerType listenerType, TraceEventType value )
        {
            this.Talker.ApplyTalkerTraceLevel( listenerType, value );
        }

        /// <summary> Applies the talker trace levels described by talker. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="talker"> The talker. </param>
        public virtual void ApplyTalkerTraceLevels( ITraceMessageTalker talker )
        {
            this.Talker.ApplyTalkerTraceLevels( talker );
        }

        /// <summary> Applies the talker listeners trace levels described by talker. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="talker"> The talker. </param>
        public virtual void ApplyListenerTraceLevels( ITraceMessageTalker talker )
        {
            this.Talker.ApplyListenerTraceLevels( talker );
        }

        #endregion

        #region " TALKER PUBLISH "

        /// <summary>
        /// Uses the <see cref="Talker"/> to publish the message or log if <see cref="Talker"/> is
        /// nothing.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="message"> The message. </param>
        /// <returns> A String. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected string Publish( TraceMessage message )
        {
            if ( message is null )
            {
                return string.Empty;
            }

            string activity = string.Empty;
            string result = message.Details;
            try
            {
                if ( this.Talker is null )
                {
                    activity = $"logging unpublished message: {message}";
                    _ = My.MyLibrary.Appliance.LogUnpublishedMessage( message );
                }
                else
                {
                    activity = $"publishing: {message}";
                    _ = this.Talker.Publish( message );
                }
            }
            catch ( Exception ex )
            {
                if ( Debugger.IsAttached )
                {
                    Debug.Assert( !Debugger.IsAttached, $"Exception {activity};. {ex.ToFullBlownString()}" );
                }
                else
                {
                    _ = System.Windows.Forms.MessageBox.Show( null, $"Exception {activity};. {ex.ToFullBlownString()}", "Exception publishing message", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error, System.Windows.Forms.MessageBoxDefaultButton.Button1, System.Windows.Forms.MessageBoxOptions.DefaultDesktopOnly );
                }
            }

            return result;
        }

        /// <summary>
        /// Uses the <see cref="Talker"/> to publish the message or log if <see cref="Talker"/> is
        /// nothing.
        /// </summary>
        /// <remarks>
        /// This function will throw the exception if not overridden so that the Trace Event Id of the
        /// caller library is used. The unction is not defined as must override because form designers do
        /// not open when making forms non-creatable with the must override directives.
        /// </remarks>
        /// <exception cref="NotImplementedException"> Thrown when the requested operation is not
        /// overridden by the calling control. </exception>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected virtual string Publish( TraceEventType eventType, string activity )
        {
            throw new NotImplementedException( $"{this.Name} is a base class; this function must be overridden by the inheriting classes" );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks>
        /// This function will throw the exception if not overridden so that the Trace Event Id of the
        /// caller library is used. The unction is not defined as must override because form designers do
        /// not open when making forms non-creatable with the must override directives.
        /// </remarks>
        /// <exception cref="NotImplementedException"> Thrown when the requested operation is not
        /// overridden by the calling control. </exception>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected virtual string PublishException( string activity, Exception ex )
        {
            throw new NotImplementedException( $"{this.Name} is a base class; this function must be overridden by the inheriting classes" );
        }

        /// <summary>
        /// Uses the <see cref="Talker"/> to publish the message or log if <see cref="Talker"/> is
        /// nothing.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> Action event information. </param>
        /// <returns> A String. </returns>
        protected string Publish( ActionEventArgs e )
        {
            return e is null ? string.Empty : this.Publish( e.OutcomeEvent, e.Details );
        }

        /// <summary>
        /// Uses the <see cref="Talker"/> to publish the message or log if <see cref="Talker"/> is
        /// nothing.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="format">    Describes the format to use. </param>
        /// <param name="args">      A variable-length parameters list containing arguments. </param>
        /// <returns> A String. </returns>
        protected string Publish( TraceEventType eventType, string format, params object[] args )
        {
            return this.Publish( eventType, string.Format( format, args ) );
        }

        /// <summary> Publish warning. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <returns> A String. </returns>
        protected string PublishWarning( string activity )
        {
            return this.Publish( TraceEventType.Warning, activity );
        }

        /// <summary> Publish warning. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="format"> Describes the format to use. </param>
        /// <param name="args">   A variable-length parameters list containing arguments. </param>
        /// <returns> A String. </returns>
        protected string PublishWarning( string format, params object[] args )
        {
            return this.Publish( TraceEventType.Warning, format, args );
        }

        /// <summary> Publish verbose. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <returns> A String. </returns>
        protected string PublishVerbose( string activity )
        {
            return this.Publish( TraceEventType.Verbose, activity );
        }

        /// <summary> Publish verbose. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="format"> Describes the format to use. </param>
        /// <param name="args">   A variable-length parameters list containing arguments. </param>
        /// <returns> A String. </returns>
        protected string PublishVerbose( string format, params object[] args )
        {
            return this.Publish( TraceEventType.Verbose, format, args );
        }

        /// <summary> Publish information. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <returns> A String. </returns>
        protected string PublishInfo( string activity )
        {
            return this.Publish( TraceEventType.Information, activity );
        }

        /// <summary> Publish information. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="format"> Describes the format to use. </param>
        /// <param name="args">   A variable-length parameters list containing arguments. </param>
        /// <returns> A String. </returns>
        protected string PublishInfo( string format, params object[] args )
        {
            return this.Publish( TraceEventType.Information, format, args );
        }

        #endregion

    }
}
