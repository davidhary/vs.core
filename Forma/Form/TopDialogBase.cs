using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Forma
{

    /// <summary> Keeps dialog form on top. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2013-01-31, 6.1.4779 </para>
    /// </remarks>
    public class TopDialogBase : System.Windows.Forms.Form
    {

        #region " Windows Form Designer generated code "

        /// <summary> Specialized default constructor for use only by derived classes. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        protected TopDialogBase() : base()
        {

            // This call is required by the Windows Form Designer.
            this.InitializeComponent();

            // Add any initialization after the InitializeComponent() call

        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this._Components is object )
                    {
                        this._Components.Dispose();
                        this._Components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        private System.ComponentModel.IContainer _Components;

        /// <summary> The with events. </summary>
        private System.Windows.Forms.Timer _OnTopTimer;

        private System.Windows.Forms.Timer OnTopTimer
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._OnTopTimer;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._OnTopTimer != null )
                {
                    this._OnTopTimer.Tick -= this.OnTopTimer_Tick;
                }

                this._OnTopTimer = value;
                if ( this._OnTopTimer != null )
                {
                    this._OnTopTimer.Tick += this.OnTopTimer_Tick;
                }
            }
        }

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this._Components = new System.ComponentModel.Container();
            this._OnTopTimer = new System.Windows.Forms.Timer( this._Components );
            this._OnTopTimer.Tick += new EventHandler( this.OnTopTimer_Tick );
            this.SuspendLayout();
            // 
            // _onTopTimer
            // 
            this._OnTopTimer.Interval = 1000;
            // 
            // TopDialogBase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 7.0f, 17.0f );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size( 331, 343 );
            this.Font = new System.Drawing.Font( System.Drawing.SystemFonts.MessageBoxFont.FontFamily, 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte( 0 ) );
            this.Icon = My.Resources.Resources.favicon;
            this.Name = "TopDialogBase";
            this.Text = "Top Dialog Base";
            this.ResumeLayout( false );
        }

        #endregion

        /// <summary>
        /// Gets or sets the sentinel indicating that the form loaded without an exception. Should be set
        /// only if load did not fail.
        /// </summary>
        /// <value> The is loaded. </value>
        protected bool IsLoaded { get; set; }

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Form.Load" /> event . </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnLoad( EventArgs e )
        {
            try
            {
                this.IsLoaded = true;
            }
            catch
            {
                throw;
            }
            finally
            {
                base.OnLoad( e );
            }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Form.Shown" /> event after enabling the top most
        /// timer.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnShown( EventArgs e )
        {
            this.OnTopTimer.Enabled = true;
            base.OnShown( e );
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Form.Closing" /> event after Disabling the top
        /// most timer if not canceled.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.ComponentModel.CancelEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnClosing( System.ComponentModel.CancelEventArgs e )
        {
            if ( e is null || !e.Cancel )
            {
                this.OnTopTimer.Enabled = false;
            }

            base.OnClosing( e );
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Form.Closed" /> event after disabling the top
        /// most timer.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> The <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnClosed( EventArgs e )
        {
            if ( this.OnTopTimer is object )
            {
                this.OnTopTimer.Enabled = false;
            }

            base.OnClosed( e );
        }

        /// <summary>
        /// Handles the Tick event of the <see cref="TopDialogBase.OnTopTimer"/> control. 
        /// Sets <see cref="System.Windows.Forms.Form.TopMost"></see> true if not already true.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
        private void OnTopTimer_Tick( object sender, EventArgs e )
        {
            if ( sender is object )
            {
                if ( !(this.TopMost == true) )
                {
                    this.TopMost = true;
                }
            }
        }
    }
}
