﻿using System.Diagnostics;

namespace isr.Core.Forma
{
    [Microsoft.VisualBasic.CompilerServices.DesignerGenerated()]
    public partial class ModalMessageForm
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            MessageLabel = new System.Windows.Forms.Label();
            SuspendLayout();
            // 
            // _MessageLabel
            // 
            MessageLabel.AllowDrop = true;
            MessageLabel.AutoSize = true;
            MessageLabel.BackColor = System.Drawing.Color.Transparent;
            MessageLabel.BorderStyle = System.Windows.Forms.BorderStyle.None;
            MessageLabel.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            MessageLabel.ForeColor = System.Drawing.Color.Black;
            MessageLabel.Location = new System.Drawing.Point(27, 39);
            MessageLabel.Name = "_MessageLabel";
            MessageLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            MessageLabel.Size = new System.Drawing.Size(241, 16);
            MessageLabel.TabIndex = 0;
            MessageLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // ModalMessageForm
            // 
            AllowDrop = true;
            AutoScaleDimensions = new System.Drawing.SizeF(7f, 13f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BackColor = System.Drawing.SystemColors.Control;
            ClientSize = new System.Drawing.Size(295, 106);
            ControlBox = false;
            Controls.Add(MessageLabel);
            Font = new System.Drawing.Font("Segoe UI", 8.25f, System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            ForeColor = System.Drawing.SystemColors.WindowText;
            FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            KeyPreview = true;
            Location = new System.Drawing.Point(185, 163);
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "ModalMessageForm";
            RightToLeft = System.Windows.Forms.RightToLeft.No;
            ResumeLayout(false);
        }
    }
}