﻿using System;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Forma
{
    public partial class BlueSplash
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region "Windows Form Designer generated code"

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(BlueSplash));
            _SmallApplicationCaptionLabel = new System.Windows.Forms.Label();
            _LargeApplicationCaptionLabel = new System.Windows.Forms.Label();
            _CurrentTaskLabel = new System.Windows.Forms.Label();
            __CloseLabel = new System.Windows.Forms.Label();
            __CloseLabel.Click += new EventHandler(Close_Click);
            __CloseLabel.MouseHover += new EventHandler(Close_MouseHover);
            __CloseLabel.MouseLeave += new EventHandler(Close_MouseLeave);
            __MinimizeLabel = new System.Windows.Forms.Label();
            __MinimizeLabel.Click += new EventHandler(Minimize_Click);
            __MinimizeLabel.MouseHover += new EventHandler(Minimize_MouseHover);
            __MinimizeLabel.MouseLeave += new EventHandler(Minimize_MouseLeave);
            _MetroProgressBar = new MetroProgressBar();
            _FolderLabel = new System.Windows.Forms.Label();
            _TableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            _AuthorLabel = new System.Windows.Forms.Label();
            _TopPanel = new System.Windows.Forms.Panel();
            _ProgressLayout = new System.Windows.Forms.TableLayoutPanel();
            _TableLayoutPanel.SuspendLayout();
            _TopPanel.SuspendLayout();
            _ProgressLayout.SuspendLayout();
            SuspendLayout();
            // 
            // _SmallApplicationCaptionLabel
            // 
            _SmallApplicationCaptionLabel.AutoSize = true;
            _SmallApplicationCaptionLabel.BackColor = System.Drawing.Color.Transparent;
            _SmallApplicationCaptionLabel.Font = new System.Drawing.Font("Segoe UI", 11.25f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(238));
            _SmallApplicationCaptionLabel.ForeColor = System.Drawing.Color.White;
            _SmallApplicationCaptionLabel.Location = new System.Drawing.Point(32, 0);
            _SmallApplicationCaptionLabel.Name = "_SmallApplicationCaptionLabel";
            _SmallApplicationCaptionLabel.Size = new System.Drawing.Size(124, 20);
            _SmallApplicationCaptionLabel.TabIndex = 0;
            _SmallApplicationCaptionLabel.Text = "Application Title";
            // 
            // _LargeApplicationCaptionLabel
            // 
            _LargeApplicationCaptionLabel.AutoSize = true;
            _LargeApplicationCaptionLabel.BackColor = System.Drawing.Color.Transparent;
            _LargeApplicationCaptionLabel.Dock = System.Windows.Forms.DockStyle.Top;
            _LargeApplicationCaptionLabel.Font = new System.Drawing.Font("Segoe UI Semibold", 36.0f, System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(238));
            _LargeApplicationCaptionLabel.ForeColor = System.Drawing.Color.White;
            _LargeApplicationCaptionLabel.Location = new System.Drawing.Point(3, 104);
            _LargeApplicationCaptionLabel.Name = "_LargeApplicationCaptionLabel";
            _LargeApplicationCaptionLabel.Size = new System.Drawing.Size(506, 65);
            _LargeApplicationCaptionLabel.TabIndex = 1;
            _LargeApplicationCaptionLabel.Text = "Application";
            _LargeApplicationCaptionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _CurrentTaskLabel
            // 
            _CurrentTaskLabel.AutoSize = true;
            _CurrentTaskLabel.BackColor = System.Drawing.Color.Transparent;
            _CurrentTaskLabel.Dock = System.Windows.Forms.DockStyle.Top;
            _CurrentTaskLabel.Font = new System.Drawing.Font("Segoe UI", 18.0f, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(238));
            _CurrentTaskLabel.ForeColor = System.Drawing.Color.White;
            _CurrentTaskLabel.Location = new System.Drawing.Point(3, 268);
            _CurrentTaskLabel.Name = "_CurrentTaskLabel";
            _CurrentTaskLabel.Size = new System.Drawing.Size(506, 32);
            _CurrentTaskLabel.TabIndex = 2;
            _CurrentTaskLabel.Text = "current task";
            // 
            // _CloseLabel
            // 
            __CloseLabel.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            __CloseLabel.AutoSize = true;
            __CloseLabel.BackColor = System.Drawing.Color.Transparent;
            __CloseLabel.Font = new System.Drawing.Font("Webdings", 18.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(2));
            __CloseLabel.ForeColor = System.Drawing.Color.White;
            __CloseLabel.Location = new System.Drawing.Point(467, 0);
            __CloseLabel.Name = "__CloseLabel";
            __CloseLabel.Size = new System.Drawing.Size(36, 26);
            __CloseLabel.TabIndex = 3;
            __CloseLabel.Text = "r";
            // 
            // _MinimizeLable
            // 
            __MinimizeLabel.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            __MinimizeLabel.AutoSize = true;
            __MinimizeLabel.BackColor = System.Drawing.Color.Transparent;
            __MinimizeLabel.Font = new System.Drawing.Font("Webdings", 18.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(2));
            __MinimizeLabel.ForeColor = System.Drawing.Color.White;
            __MinimizeLabel.Location = new System.Drawing.Point(425, 0);
            __MinimizeLabel.Name = "__MinimizeLable";
            __MinimizeLabel.Size = new System.Drawing.Size(36, 26);
            __MinimizeLabel.TabIndex = 4;
            __MinimizeLabel.Text = "0";
            // 
            // _MetroProgressBar
            // 
            _MetroProgressBar.BackColor = System.Drawing.Color.Transparent;
            _MetroProgressBar.Location = new System.Drawing.Point(73, 4);
            _MetroProgressBar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            _MetroProgressBar.Name = "_MetroProgressBar";
            _MetroProgressBar.Size = new System.Drawing.Size(359, 6);
            _MetroProgressBar.TabIndex = 5;
            // 
            // _FolderLabel
            // 
            _FolderLabel.AutoSize = true;
            _FolderLabel.BackColor = System.Drawing.Color.Transparent;
            _FolderLabel.Font = new System.Drawing.Font("Wingdings", 11.25f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(2));
            _FolderLabel.ForeColor = System.Drawing.Color.White;
            _FolderLabel.Location = new System.Drawing.Point(3, 3);
            _FolderLabel.Name = "_FolderLabel";
            _FolderLabel.Size = new System.Drawing.Size(28, 16);
            _FolderLabel.TabIndex = 6;
            _FolderLabel.Text = "1";
            // 
            // _TableLayoutPanel
            // 
            _TableLayoutPanel.BackColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(43)), Conversions.ToInteger(Conversions.ToByte(86)), Conversions.ToInteger(Conversions.ToByte(154)));
            _TableLayoutPanel.ColumnCount = 1;
            _TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0f));
            _TableLayoutPanel.Controls.Add(_AuthorLabel, 0, 9);
            _TableLayoutPanel.Controls.Add(_TopPanel, 0, 1);
            _TableLayoutPanel.Controls.Add(_CurrentTaskLabel, 0, 7);
            _TableLayoutPanel.Controls.Add(_LargeApplicationCaptionLabel, 0, 3);
            _TableLayoutPanel.Controls.Add(_ProgressLayout, 0, 5);
            _TableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            _TableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            _TableLayoutPanel.Name = "_TableLayoutPanel";
            _TableLayoutPanel.RowCount = 10;
            _TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 3.0f));
            _TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10.0f));
            _TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 3.0f));
            _TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0f));
            _TableLayoutPanel.Size = new System.Drawing.Size(512, 324);
            _TableLayoutPanel.TabIndex = 7;
            // 
            // _AuthorLabel
            // 
            _AuthorLabel.AutoSize = true;
            _AuthorLabel.BackColor = System.Drawing.Color.Transparent;
            _AuthorLabel.Dock = System.Windows.Forms.DockStyle.Top;
            _AuthorLabel.Font = new System.Drawing.Font("Segoe UI", 12.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(238));
            _AuthorLabel.ForeColor = System.Drawing.Color.WhiteSmoke;
            _AuthorLabel.Location = new System.Drawing.Point(3, 303);
            _AuthorLabel.Name = "_AuthorLabel";
            _AuthorLabel.Size = new System.Drawing.Size(506, 21);
            _AuthorLabel.TabIndex = 9;
            _AuthorLabel.Text = "A product of Integrated Scientific Resources";
            _AuthorLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _TopPanel
            // 
            _TopPanel.BackColor = System.Drawing.Color.Transparent;
            _TopPanel.Controls.Add(__CloseLabel);
            _TopPanel.Controls.Add(_FolderLabel);
            _TopPanel.Controls.Add(__MinimizeLabel);
            _TopPanel.Controls.Add(_SmallApplicationCaptionLabel);
            _TopPanel.Dock = System.Windows.Forms.DockStyle.Top;
            _TopPanel.Location = new System.Drawing.Point(3, 6);
            _TopPanel.Name = "_TopPanel";
            _TopPanel.Size = new System.Drawing.Size(506, 26);
            _TopPanel.TabIndex = 0;
            // 
            // _ProgressLayout
            // 
            _ProgressLayout.ColumnCount = 3;
            _ProgressLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _ProgressLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _ProgressLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _ProgressLayout.Controls.Add(_MetroProgressBar, 1, 0);
            _ProgressLayout.Dock = System.Windows.Forms.DockStyle.Top;
            _ProgressLayout.Location = new System.Drawing.Point(3, 182);
            _ProgressLayout.Name = "_ProgressLayout";
            _ProgressLayout.RowCount = 1;
            _ProgressLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0f));
            _ProgressLayout.Size = new System.Drawing.Size(506, 14);
            _ProgressLayout.TabIndex = 8;
            // 
            // BlueSplash
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BackgroundImage = My.Resources.Resources.BlueSplash;
            ClientSize = new System.Drawing.Size(512, 324);
            Controls.Add(_TableLayoutPanel);
            FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
            Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            MaximumSize = new System.Drawing.Size(512, 324);
            MinimumSize = new System.Drawing.Size(512, 324);
            Name = "BlueSplash";
            StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            Text = "Splash";
            _TableLayoutPanel.ResumeLayout(false);
            _TableLayoutPanel.PerformLayout();
            _TopPanel.ResumeLayout(false);
            _TopPanel.PerformLayout();
            _ProgressLayout.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Label _SmallApplicationCaptionLabel;
        private System.Windows.Forms.Label _LargeApplicationCaptionLabel;
        private System.Windows.Forms.Label _CurrentTaskLabel;
        private System.Windows.Forms.Label __CloseLabel;

        private System.Windows.Forms.Label _CloseLabel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __CloseLabel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__CloseLabel != null)
                {
                    __CloseLabel.Click -= Close_Click;
                    __CloseLabel.MouseHover -= Close_MouseHover;
                    __CloseLabel.MouseLeave -= Close_MouseLeave;
                }

                __CloseLabel = value;
                if (__CloseLabel != null)
                {
                    __CloseLabel.Click += Close_Click;
                    __CloseLabel.MouseHover += Close_MouseHover;
                    __CloseLabel.MouseLeave += Close_MouseLeave;
                }
            }
        }

        private System.Windows.Forms.Label __MinimizeLabel;

        private System.Windows.Forms.Label _MinimizeLabel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __MinimizeLabel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__MinimizeLabel != null)
                {
                    __MinimizeLabel.Click -= Minimize_Click;
                    __MinimizeLabel.MouseHover -= Minimize_MouseHover;
                    __MinimizeLabel.MouseLeave -= Minimize_MouseLeave;
                }

                __MinimizeLabel = value;
                if (__MinimizeLabel != null)
                {
                    __MinimizeLabel.Click += Minimize_Click;
                    __MinimizeLabel.MouseHover += Minimize_MouseHover;
                    __MinimizeLabel.MouseLeave += Minimize_MouseLeave;
                }
            }
        }

        private MetroProgressBar _MetroProgressBar;
        private System.Windows.Forms.Label _FolderLabel;
        private System.Windows.Forms.TableLayoutPanel _TableLayoutPanel;
        private System.Windows.Forms.Panel _TopPanel;
        private System.Windows.Forms.TableLayoutPanel _ProgressLayout;
        private System.Windows.Forms.Label _AuthorLabel;
    }
}