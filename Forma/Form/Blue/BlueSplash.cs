using System;
using System.Drawing;

using isr.Core.Forma.ExceptionExtensions;

namespace isr.Core.Forma
{

    /// <summary> Blue splash. </summary>
    /// <remarks>
    /// (c) 2015 Magyar András. All rights reserved.<para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2012-05-08, from Magyar András  </para><para>
    /// http://www.codeproject.com/Articles/804316/Office-Style-Splash-Screen. </para>
    /// </remarks>
    public partial class BlueSplash : FormBase
    {

        #region " CONSTRUCTION "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public BlueSplash()
        {
            this.Shown += this.Form_Shown;
            this.InitializeComponent();
            this.UpdateInfo();
        }

        /// <summary>
        /// Disposes of the resources (other than memory) used by the
        /// <see cref="T:System.Windows.Forms.Form" />.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " EVENT HANDLERS "

        /// <summary>
        /// Does all the post processing after all the form controls are rendered as the user expects
        /// them.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void Form_Shown( object sender, EventArgs e )
        {
            System.Windows.Forms.Application.DoEvents();
            try
            {
                this.Cursor = System.Windows.Forms.Cursors.Hand;
                this.CurrentTask = "Starting...";

                // instantiate form objects
                this.UpdateInfo();
            }
            catch ( Exception ex )
            {
                _ = System.Windows.Forms.MessageBox.Show( ex.ToFullBlownString(), "Exception Occurred", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Exclamation, System.Windows.Forms.MessageBoxDefaultButton.Button1, System.Windows.Forms.MessageBoxOptions.DefaultDesktopOnly );
            }
            finally
            {
                this.Cursor = System.Windows.Forms.Cursors.Default;
                System.Windows.Forms.Application.DoEvents();
            }
        }

        /// <summary> Close Application. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void Close_Click( object sender, EventArgs e )
        {
            this.IsCloseRequested = true;
        }

        /// <summary> Minimize label effects. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void Minimize_Click( object sender, EventArgs e )
        {
            this.IsMinimizeRequested = true;
        }

        /// <summary> Closes label Mouse hover and leave effects </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void Close_MouseHover( object sender, EventArgs e )
        {
            this._CloseLabel.ForeColor = Color.Silver;
        }

        /// <summary> Closes mouse leave. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void Close_MouseLeave( object sender, EventArgs e )
        {
            this._CloseLabel.ForeColor = Color.White;
        }

        /// <summary> Minimize mouse hover. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void Minimize_MouseHover( object sender, EventArgs e )
        {
            this._MinimizeLabel.ForeColor = Color.Silver;
        }

        /// <summary> Minimize mouse leave. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void Minimize_MouseLeave( object sender, EventArgs e )
        {
            this._MinimizeLabel.ForeColor = Color.White;
        }
        #endregion

        #region " PROPERTIES and METHODS "

        /// <summary> Gets a value indicating whether this object is minimize requested. </summary>
        /// <value> <c>true</c> if this object is minimize requested; otherwise <c>false</c> </value>
        public bool IsMinimizeRequested { get; private set; }

        /// <summary> Gets a value indicating whether this object is close requested. </summary>
        /// <value> <c>true</c> if this object is close requested; otherwise <c>false</c> </value>
        public bool IsCloseRequested { get; private set; }

        /// <summary> Gets or sets the current task. </summary>
        /// <value> The current task. </value>
        public string CurrentTask
        {
            get => this._CurrentTaskLabel.Text;

            set {
                if ( !string.Equals( value, this.CurrentTask ) )
                {
                    _ = SafeTextSetter( this._CurrentTaskLabel, value );
                }
            }
        }

        /// <summary> true to topmost. </summary>
        private bool _Topmost;

        /// <summary> Sets the top most status in a thread safe way. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> true to value. </param>
        public void TopmostSetter( bool value )
        {
            this._Topmost = value;
            SafeTopMostSetter( this, value );
        }

        /// <summary> Displays a message on the splash screen. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The message. </param>
        public void DisplayMessage( string value )
        {
            this.CurrentTask = value;
        }

        /// <summary> Gets or sets the small application caption. </summary>
        /// <value> The small application caption. </value>
        public string SmallApplicationCaption
        {
            get => this._SmallApplicationCaptionLabel.Text;

            set {
                if ( !string.Equals( value, this.SmallApplicationCaption ) )
                {
                    _ = SafeTextSetter( this._SmallApplicationCaptionLabel, value );
                }
            }
        }

        /// <summary> Gets or sets the large application caption. </summary>
        /// <value> The large application caption. </value>
        public string LargeApplicationCaption
        {
            get => this._LargeApplicationCaptionLabel.Text;

            set {
                if ( !string.Equals( value, this.LargeApplicationCaption ) )
                {
                    _ = SafeTextSetter( this._LargeApplicationCaptionLabel, value );
                }
            }
        }

        /// <summary> Update the information on screen. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void UpdateInfo()
        {
            // Me._SmallApplicationCaptionLabel.Text = My.Application.Info.ProductName
            this._LargeApplicationCaptionLabel.Text = My.MyProject.Application.Info.AssemblyName;
            this._SmallApplicationCaptionLabel.Text = $"{My.MyProject.Application.Info.ProductName} {My.MyProject.Application.Info.Version}";
            System.Windows.Forms.Application.DoEvents();
            this.TopMost = this._Topmost;
            System.Windows.Forms.Application.DoEvents();
        }

        #endregion

        #region " THREAD SAFE METHODS "

        /// <summary> Safe height setter. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="control"> The control. </param>
        /// <param name="value">   true to value. </param>
        private static void SafeHeightSetter( System.Windows.Forms.Control control, int value )
        {
            if ( control is object )
            {
                if ( control.InvokeRequired )
                {
                    _ = control.Invoke( new Action<System.Windows.Forms.Control, int>( SafeHeightSetter ), new object[] { control, value } );
                }
                else
                {
                    control.Height = value;
                    control.Invalidate();
                    System.Windows.Forms.Application.DoEvents();
                }
            }
        }


        /// <summary>   Reads control text. </summary>
        /// <remarks>   David, 2020-09-24. </remarks>
        /// <param name="control">   The variable control. </param>
        /// <returns>   The control text. </returns>
        public static string SafeTextGetter( System.Windows.Forms.Control control )
        {
            if ( control.InvokeRequired )
            {
                return ( string ) control.Invoke(
                  new Func<String>( () => SafeTextGetter( control ) )
                );
            }
            else
            {
                string varText = control.Text;
                return varText;
            }
        }

        /// <summary>   Writes a control text. </summary>
        /// <remarks>   David, 2020-09-24. </remarks>
        /// <param name="control">   The variable control. </param>
        /// <param name="value">        The message. </param>
        /// <returns>   A string. </returns>
        public static string SafeTextSetter( System.Windows.Forms.Control control, string value )
        {
            if ( control is object )
            {

                if ( control.InvokeRequired )
                {
                    return ( string ) control.Invoke( new Func<String>( () => SafeTextSetter( control, value ) ) );
                }
                else
                {
                    control.Text = value;
                    control.Invalidate();
                    System.Windows.Forms.Application.DoEvents();
                    return control.Text;
                }
            }
            return value;
        }

        /// <summary>
        /// Sets the <see cref="System.Windows.Forms.Control">control</see> text to the
        /// <paramref name="value">value</paramref>.
        /// This setter is thread safe.
        /// </summary>
        /// <remarks> The value is set to empty if null or empty. </remarks>
        /// <param name="control"> The control. </param>
        /// <param name="value">   The value. </param>
        /// <returns> value. </returns>
        private static void SafeTextWriter( System.Windows.Forms.Control control, string value )
        {
            if ( control is object )
            {
                if ( string.IsNullOrWhiteSpace( value ) )
                {
                    value = string.Empty;
                }

                if ( control.InvokeRequired )
                {
                    _ = control.Invoke( new Action<System.Windows.Forms.Control, string>( SafeTextWriter ), new object[] { control, value } );
                }
                else
                {
                    control.Text = value;
                    control.Invalidate();
                    System.Windows.Forms.Application.DoEvents();
                }
            }
        }

        /// <summary> Safe top most setter. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="form">  The form. </param>
        /// <param name="value"> true to value. </param>
        private static void SafeTopMostSetter( System.Windows.Forms.Form form, bool value )
        {
            if ( form is object )
            {
                if ( form.InvokeRequired )
                {
                    _ = form.Invoke( new Action<System.Windows.Forms.Form, bool>( SafeTopMostSetter ), new object[] { form, value } );
                }
                else
                {
                    form.TopMost = value;
                    System.Windows.Forms.Application.DoEvents();
                }
            }
        }

        /// <summary> Display info. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public void DisplayInfo()
        {
            if ( this.InvokeRequired )
            {
                _ = this.Invoke( new Action( this.UpdateInfo ) );
            }
            else
            {
                this.UpdateInfo();
            }
        }

        #endregion

    }
}
