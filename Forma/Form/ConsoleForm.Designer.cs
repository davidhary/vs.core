﻿using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Forma
{
    [DesignerGenerated()]
    public partial class ConsoleForm
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;
        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _ToolTip = new System.Windows.Forms.ToolTip(components);
            _Tabs = new System.Windows.Forms.TabControl();
            _ViewTabPage = new System.Windows.Forms.TabPage();
            _Layout = new System.Windows.Forms.TableLayoutPanel();
            _MessagesTabPage = new System.Windows.Forms.TabPage();
            __TraceMessagesBox = new TraceMessagesBox();
            __TraceMessagesBox.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(TraceMessagesBoxPropertyChanged);
            StatusStrip = new System.Windows.Forms.StatusStrip();
            StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            _Tabs.SuspendLayout();
            _ViewTabPage.SuspendLayout();
            _MessagesTabPage.SuspendLayout();
            StatusStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _ToolTip
            // 
            _ToolTip.IsBalloon = true;
            // 
            // _Tabs
            // 
            _Tabs.Controls.Add(_ViewTabPage);
            _Tabs.Controls.Add(_MessagesTabPage);
            _Tabs.Dock = System.Windows.Forms.DockStyle.Fill;
            _Tabs.ItemSize = new System.Drawing.Size(42, 22);
            _Tabs.Location = new System.Drawing.Point(0, 0);
            _Tabs.Name = "_Tabs";
            _Tabs.SelectedIndex = 0;
            _Tabs.Size = new System.Drawing.Size(466, 541);
            _Tabs.TabIndex = 1;
            // 
            // _InstrumentTabPage
            // 
            _ViewTabPage.Controls.Add(_Layout);
            _ViewTabPage.Location = new System.Drawing.Point(4, 26);
            _ViewTabPage.Name = "_InstrumentTabPage";
            _ViewTabPage.Size = new System.Drawing.Size(458, 511);
            _ViewTabPage.TabIndex = 5;
            _ViewTabPage.Text = "Instrument";
            _ViewTabPage.ToolTipText = "Instrument";
            _ViewTabPage.UseVisualStyleBackColor = true;
            // 
            // _InstrumentLayout
            // 
            _Layout.ColumnCount = 3;
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 6.0f));
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0f));
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 6.0f));
            _Layout.Dock = System.Windows.Forms.DockStyle.Fill;
            _Layout.Location = new System.Drawing.Point(0, 0);
            _Layout.Name = "_InstrumentLayout";
            _Layout.RowCount = 3;
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 6.0f));
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0f));
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 6.0f));
            _Layout.Size = new System.Drawing.Size(458, 511);
            _Layout.TabIndex = 1;
            // 
            // _MessagesTabPage
            // 
            _MessagesTabPage.Controls.Add(__TraceMessagesBox);
            _MessagesTabPage.Location = new System.Drawing.Point(4, 26);
            _MessagesTabPage.Name = "_MessagesTabPage";
            _MessagesTabPage.Size = new System.Drawing.Size(458, 511);
            _MessagesTabPage.TabIndex = 2;
            _MessagesTabPage.Text = "Log";
            _MessagesTabPage.UseVisualStyleBackColor = true;
            // 
            // _TraceMessagesBox
            // 
            __TraceMessagesBox.AcceptsReturn = true;
            __TraceMessagesBox.AlertLevel = TraceEventType.Warning;
            __TraceMessagesBox.BackColor = System.Drawing.SystemColors.Info;
            __TraceMessagesBox.CaptionFormat = "{0} ≡";
            __TraceMessagesBox.CausesValidation = false;
            __TraceMessagesBox.Dock = System.Windows.Forms.DockStyle.Fill;
            __TraceMessagesBox.Font = new System.Drawing.Font("Consolas", 8.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __TraceMessagesBox.ForeColor = System.Drawing.SystemColors.WindowText;
            __TraceMessagesBox.Location = new System.Drawing.Point(0, 0);
            __TraceMessagesBox.MaxLength = 0;
            __TraceMessagesBox.Multiline = true;
            __TraceMessagesBox.Name = "__TraceMessagesBox";
            __TraceMessagesBox.PresetCount = 100;
            __TraceMessagesBox.ReadOnly = true;
            __TraceMessagesBox.ResetCount = 200;
            __TraceMessagesBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            __TraceMessagesBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            __TraceMessagesBox.Size = new System.Drawing.Size(458, 511);
            __TraceMessagesBox.TabIndex = 15;
            __TraceMessagesBox.TraceLevel = TraceEventType.Verbose;
            // 
            // _StatusStrip
            // 
            StatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { StatusLabel });
            StatusStrip.Location = new System.Drawing.Point(0, 541);
            StatusStrip.Name = "_StatusStrip";
            StatusStrip.Size = new System.Drawing.Size(466, 22);
            StatusStrip.TabIndex = 2;
            StatusStrip.Text = "Status Strip";
            // 
            // _StatusLabel
            // 
            StatusLabel.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            StatusLabel.Name = "_StatusLabel";
            StatusLabel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            StatusLabel.Size = new System.Drawing.Size(420, 17);
            StatusLabel.Spring = true;
            StatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ConsoleForm
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            BackColor = System.Drawing.SystemColors.Control;
            ClientSize = new System.Drawing.Size(466, 563);
            Controls.Add(_Tabs);
            Controls.Add(StatusStrip);
            Location = new System.Drawing.Point(297, 150);
            MaximizeBox = false;
            Name = "ConsoleForm";
            RightToLeft = System.Windows.Forms.RightToLeft.No;
            StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            Text = "Console";
            _Tabs.ResumeLayout(false);
            _ViewTabPage.ResumeLayout(false);
            _MessagesTabPage.ResumeLayout(false);
            _MessagesTabPage.PerformLayout();
            StatusStrip.ResumeLayout(false);
            StatusStrip.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        private TraceMessagesBox __TraceMessagesBox;

        private TraceMessagesBox _TraceMessagesBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __TraceMessagesBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__TraceMessagesBox != null)
                {
                    __TraceMessagesBox.PropertyChanged -= TraceMessagesBoxPropertyChanged;
                }

                __TraceMessagesBox = value;
                if (__TraceMessagesBox != null)
                {
                    __TraceMessagesBox.PropertyChanged += TraceMessagesBoxPropertyChanged;
                }
            }
        }

        private System.Windows.Forms.TabPage _MessagesTabPage;
        private System.Windows.Forms.TabControl _Tabs;
        private System.Windows.Forms.ToolTip _ToolTip;
        private System.Windows.Forms.TabPage _ViewTabPage;
        private System.Windows.Forms.TableLayoutPanel _Layout;
    }
}