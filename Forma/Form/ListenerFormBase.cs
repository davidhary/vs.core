using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Forma
{

    /// <summary> A form listening to trace messages. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-12-26, 2.1.5836. </para>
    /// </remarks>
    public partial class ListenerFormBase : Form
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Gets the initializing components sentinel. </summary>
        /// <value> The initializing components sentinel. </value>
        protected bool InitializingComponents { get; set; }

        /// <summary> Specialized default constructor for use only by derived classes. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        protected ListenerFormBase() : base()
        {
            this.InitializingComponents = true;
            this.InitializeComponent();
            this.InitializingComponents = false;
            this._Talker = new TraceMessageTalker();
        }

        /// <summary> Initializes the component. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            this.AutoScaleDimensions = new SizeF( 7.0f, 17.0f );
            this.AutoScaleMode = AutoScaleMode.Font;
            this.BackColor = SystemColors.Control;
            this.ClientSize = new Size( 331, 341 );
            this.Cursor = Cursors.Default;
            this.Icon = My.Resources.Resources.favicon;
            this.Font = new Font( SystemFonts.MessageBoxFont.FontFamily, 9.75f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte( 0 ) );
            this.Name = "FormBase";
            this.ResumeLayout( false );
        }

        /// <summary>
        /// Disposes of the resources (other than memory) used by the
        /// <see cref="T:System.Windows.Forms.Form" />.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.Talker = null;
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #region " CLASS STYLE "

        /// <summary> The enable drop shadow version. </summary>
        public const int EnableDropShadowVersion = 5;

        /// <summary> Gets the class style. </summary>
        /// <value> The class style. </value>
        protected ClassStyleConstants ClassStyle { get; set; } = ClassStyleConstants.None;

        /// <summary> Adds a drop shadow parameter. </summary>
        /// <remarks>
        /// From Code Project: http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx.
        /// </remarks>
        /// <value> Options that control the create. </value>
        protected override CreateParams CreateParams
        {
            [System.Security.Permissions.SecurityPermission( System.Security.Permissions.SecurityAction.Demand, Flags = System.Security.Permissions.SecurityPermissionFlag.UnmanagedCode )]
            get {
                var cp = base.CreateParams;
                cp.ClassStyle |= ( int ) this.ClassStyle;
                return cp;
            }
        }

        #endregion

        #endregion

        #region " SHOW "

        /// <summary>
        /// Shows the <see cref="RichTextBox">rich text box</see> form with these messages.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="mdiForm"> The MDI form. </param>
        /// <param name="owner">   The owner. </param>
        public void Show( Form mdiForm, IWin32Window owner )
        {
            if ( mdiForm is object && mdiForm.IsMdiContainer )
            {
                this.MdiParent = mdiForm;
                mdiForm.Show();
            }

            this.Show( owner );
        }

        /// <summary>
        /// Shows the <see cref="RichTextBox">rich text box</see> form with these messages.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="mdiForm"> The MDI form. </param>
        public void ShowDialog( Form mdiForm )
        {
            if ( mdiForm is object && mdiForm.IsMdiContainer )
            {
                this.MdiParent = mdiForm;
                mdiForm.Show();
            }

            _ = this.ShowDialog();
        }

        #endregion

    }

    /// <summary> Collection of listener forms. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-01-04 </para>
    /// </remarks>
    public class ListenerFormCollection : List<ListenerFormBase>
    {

        /// <summary> Adds and shows a new form,. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="form"> The form. </param>
        /// <param name="log">  The log. </param>
        public void ShowNew( ListenerFormBase form, IMessageListener log )
        {
            if ( form is null )
            {
                throw new ArgumentNullException( nameof( form ) );
            }

            form.AddListener( log );
            this.Add( form );
            form.FormClosed += this.OnClosed;
            form.Show();
        }

        /// <summary> Adds a form. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="form"> The form. </param>
        /// <returns> A ListenerFormBase. </returns>
        public ListenerFormBase AddForm( ListenerFormBase form )
        {
            if ( form is null )
            {
                throw new ArgumentNullException( nameof( form ) );
            }

            this.Add( form );
            form.FormClosed += this.OnClosed;
            return form;
        }

        /// <summary> Handles a member form closed event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information to send to registered event handlers. </param>
        private void OnClosed( object sender, EventArgs e )
        {
            ListenerFormBase f = sender as ListenerFormBase;
            f.FormClosed -= this.OnClosed;
            _ = this.Remove( f );
            if ( f is object )
            {
                f.Dispose();
                f = null;
            }
        }

        /// <summary> Removes the dispose described by value. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The value. </param>
        public void RemoveDispose( ListenerFormBase value )
        {
            var f = value;
            if ( f is object )
            {
                f.FormClosed -= this.OnClosed;
                _ = this.Remove( f );
                f.Dispose();
                f = null;
            }
        }

        /// <summary>
        /// Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public void ClearDispose()
        {
            while ( this.Any() )
            {
                this.RemoveDispose( this[0] );
                Application.DoEvents();
            }
        }
    }
}
