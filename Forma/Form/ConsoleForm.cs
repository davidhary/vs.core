using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

using isr.Core.Forma.ExceptionExtensions;

namespace isr.Core.Forma
{

    /// <summary>
    /// A console form to host <see cref="ModelViewBase"/> or <see cref="ModelViewTalkerBase"/>
    /// controls.
    /// </summary>
    /// <remarks>
    /// (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2005-02-07, 2.0.2597 </para>
    /// </remarks>
    public partial class ConsoleForm : ListenerFormBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public ConsoleForm() : base()
        {
            this.InitializingComponents = true;
            this.InitializeComponent();
            this.InitializingComponents = false;
            // set defaults for the messages box.
            this._TraceMessagesBox.ContainerPanel = this._MessagesTabPage;
            this._TraceMessagesBox.MaxSynopsisLength = 0;
            this._TraceMessagesBox.AlertSoundEnabled = false;
            this._TraceMessagesBox.AlertLevel = TraceEventType.Warning;
            // Me._TraceMessagesBox.AlertsToggleControl = Me._AlertsToolStripTextBox.TextBox
            this._TraceMessagesBox.TabCaption = "Log";
            this._TraceMessagesBox.CaptionFormat = "{0} " + System.Text.Encoding.GetEncoding( 437 ).GetString( new byte[] { 240 } );
            this._TraceMessagesBox.ResetCount = 1000;
            this._TraceMessagesBox.PresetCount = 500;
            this._TraceMessagesBox.AssignLog( My.MyLibrary.Logger );
            this._TraceMessagesBox.WordWrap = false;
            this._TraceMessagesBox.CommenceUpdates();
            this.__TraceMessagesBox.Name = "_TraceMessagesBox";
        }

        /// <summary>
        /// Disposes of the resources (other than memory) used by the
        /// <see cref="T:System.Windows.Forms.Form" />.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this._TraceMessagesBox.SuspendUpdatesReleaseIndicators();
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " FORM EVENT HANDLERS "

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Form.Closing" /> event. Releases all publishers.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.ComponentModel.CancelEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnClosing( CancelEventArgs e )
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                if ( e is object && !e.Cancel )
                {
                    // removes the private text box listener
                    this.RemovePrivateListener( this._TraceMessagesBox );
                    if ( this.PropertyNotifyControl is object )
                    {
                        this._Layout.Controls.Remove( this.PropertyNotifyControl );
                    }

                    if ( this._PropertyNotifyControlDisposeEnabled )
                    {
                        this.PropertyNotifyControl.Dispose();
                    }

                    if ( this._UserControlDisposeEnabled && this.UserControl is object )
                    {
                        this.UserControl.Dispose();
                    }

                    this.UserControl = null;
                    if ( this.ModelView is object )
                    {
                        // removes the private listeners of the talker control hosted in this form.
                        this.ModelView.RemovePrivateListeners();
                        this._Layout.Controls.Remove( this.ModelView );
                        if ( this._TalkerControlDisposeEnabled )
                        {
                            this.ModelView.Dispose();
                        }
                    }

                    this.ModelView = null;
                }
            }
            finally
            {
                Application.DoEvents();
                base.OnClosing( e );
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Called upon receiving the <see cref="E:System.Windows.Forms.Form.Load" /> event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnLoad( EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Name} loading";
                this.Cursor = Cursors.WaitCursor;
                Trace.CorrelationManager.StartLogicalOperation( System.Reflection.MethodBase.GetCurrentMethod().Name );

                // set the form caption
                activity = $"{this.Name} displaying title (text)";
                this.Text = $"{My.MyProject.Application.Info.ProductName} release {My.MyProject.Application.Info.Version}";

                // default to center screen.
                activity = $"{this.Name} loading; centering to screen";
                this.CenterToScreen();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
                if ( MyDialogResult.Abort == MyMessageBox.ShowDialogAbortIgnore( ex.ToFullBlownString(), "Exception Occurred", MyMessageBoxIcon.Error ) )
                {
                    Application.Exit();
                }
            }
            finally
            {
                base.OnLoad( e );
                Trace.CorrelationManager.StopLogicalOperation();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Called upon receiving the <see cref="E:System.Windows.Forms.Form.Shown" /> event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnShown( EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"showing {this.Name}";
                this.Cursor = Cursors.WaitCursor;
                Trace.CorrelationManager.StartLogicalOperation( System.Reflection.MethodBase.GetCurrentMethod().Name );

                // allow form rendering time to complete: process all messages currently in the queue.
                Application.DoEvents();
                if ( !this.DesignMode )
                {
                }

                activity = "Ready to open Visa Session";
                _ = this.PublishVerbose( $"{activity};. " );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
                if ( MyDialogResult.Abort == MyMessageBox.ShowDialogAbortIgnore( ex.ToFullBlownString(), "Exception Occurred", MyMessageBoxIcon.Error ) )
                {
                    Application.Exit();
                }
            }
            finally
            {
                base.OnShown( e );
                Trace.CorrelationManager.StopLogicalOperation();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Resize client area. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="title">              The title. </param>
        /// <param name="clientControl">      The client control. </param>
        /// <param name="usingTraceMessages"> True to using trace messages. </param>
        public void ResizeClientArea( string title, Control clientControl, bool usingTraceMessages )
        {
            if ( clientControl is null )
            {
                throw new ArgumentNullException( nameof( clientControl ) );
            }

            if ( usingTraceMessages )
            {
                this.ResizeClientArea( this._Tabs, this._TraceMessagesBox, clientControl );
                this._ViewTabPage.Text = title;
                this._ViewTabPage.ToolTipText = title;
                this._Layout.Controls.Add( clientControl, 1, 1 );
            }
            else
            {
                this.ResizeClientArea( clientControl );
            }
        }

        /// <summary> Resize client area. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="clientControl"> The client control. </param>
        private void ResizeClientArea( Control clientControl )
        {
            if ( clientControl is null )
            {
                throw new ArgumentNullException( nameof( clientControl ) );
            }

            this._Tabs.Visible = false;
            this.Controls.Add( clientControl );
            var clientMargins = new System.Drawing.Size( 0, 0 );
            var controlMargins = new System.Drawing.Size( 0, 0 );
            System.Drawing.Size newClientSize;
            clientControl.BringToFront();
            newClientSize = new System.Drawing.Size( clientControl.Width + clientMargins.Width, this.Height );
            newClientSize = new System.Drawing.Size( newClientSize.Width, clientControl.Height + clientMargins.Height + this.StatusStrip.Height );
            bool resizeRequired = true;
            if ( resizeRequired )
            {
                this.ClientSize = new System.Drawing.Size( newClientSize.Width + controlMargins.Width, newClientSize.Height + controlMargins.Height );
                this.Refresh();
            }
        }

        /// <summary> Resize client area. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="tabControl">    The tab control. </param>
        /// <param name="messagesBox">   The messages box control. </param>
        /// <param name="clientControl"> The client control. </param>
        private void ResizeClientArea( TabControl tabControl, Control messagesBox, Control clientControl )
        {
            if ( messagesBox is null )
            {
                throw new ArgumentNullException( nameof( messagesBox ) );
            }

            if ( tabControl is null )
            {
                throw new ArgumentNullException( nameof( tabControl ) );
            }

            tabControl.BringToFront();
            if ( clientControl is null )
            {
                throw new ArgumentNullException( nameof( clientControl ) );
            }

            var tabsMargins = new System.Drawing.Size( tabControl.Width - messagesBox.Width, tabControl.Height - messagesBox.Height );
            var controlMargins = new System.Drawing.Size( this.ClientSize.Width - this._Layout.Width, this.ClientSize.Height - this._Layout.Height );
            System.Drawing.Size newClientSize;
            newClientSize = new System.Drawing.Size( clientControl.Width + tabsMargins.Width, tabControl.Height );
            newClientSize = new System.Drawing.Size( newClientSize.Width, clientControl.Height + tabsMargins.Height );
            bool resizeRequired = true;
            if ( resizeRequired )
            {
                tabControl.Size = newClientSize;
                tabControl.Refresh();
                this.ClientSize = new System.Drawing.Size( newClientSize.Width + controlMargins.Width, newClientSize.Height + controlMargins.Height );
                this.Refresh();
            }
        }

        #endregion

        #region " USER CONTROL "

        /// <summary> true to enable, false to disable the disposal of the User Control. </summary>
        private bool _UserControlDisposeEnabled;

        /// <summary> The user control. </summary>
        private UserControl UserControl { get; set; }

        /// <summary> Adds a User Control. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The value. </param>
        public void AddUserControl( UserControl value )
        {
            this.AddUserControl( "UI", value, true );
        }

        /// <summary> Adds a User Control. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="title"> The title. </param>
        /// <param name="value"> The value. </param>
        public void AddUserControl( string title, UserControl value )
        {
            this.AddUserControl( title, value, true );
        }

        /// <summary> Adds a User Control. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="title">             The title. </param>
        /// <param name="value">             The value. </param>
        /// <param name="disposeEnabled">    true to enable, false to disable the dispose. </param>
        /// <param name="showTraceMessages"> True to show, false to hide the trace messages. </param>
        public void AddUserControl( string title, UserControl value, bool disposeEnabled, bool showTraceMessages )
        {
            this.ResizeClientArea( title, value, showTraceMessages );
            this.UserControl = value;
            if ( showTraceMessages )
            {
                this.AddPrivateListeners();
            }

            this._UserControlDisposeEnabled = disposeEnabled;
            this.UserControl.Dock = DockStyle.Fill;
            this.UserControl.TabIndex = 0;
            this.UserControl.BackColor = System.Drawing.Color.Transparent;
            this.UserControl.Font = new System.Drawing.Font( this.Font, System.Drawing.FontStyle.Regular );
            this.UserControl.Name = "_UserControl";
        }

        /// <summary> Adds a User Control. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="title">          The title. </param>
        /// <param name="value">          The value. </param>
        /// <param name="disposeEnabled"> true to enable, false to disable the dispose. </param>
        public void AddUserControl( string title, UserControl value, bool disposeEnabled )
        {
            this.AddUserControl( title, value, disposeEnabled, true );
        }

        #endregion

        #region " PROPERTY NOTIFY CONTROL "

        /// <summary> true to enable, false to disable the disposal of the property notify control. </summary>
        private bool _PropertyNotifyControlDisposeEnabled;

        /// <summary> The property notify control. </summary>
        private ModelViewBase _PropertyNotifyControl;

        private ModelViewBase PropertyNotifyControl
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._PropertyNotifyControl;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._PropertyNotifyControl != null )
                {
                    this._PropertyNotifyControl.PropertyChanged -= this.PropertyNotifyControlPropertyChanged;
                }

                this._PropertyNotifyControl = value;
                if ( this._PropertyNotifyControl != null )
                {
                    this._PropertyNotifyControl.PropertyChanged += this.PropertyNotifyControlPropertyChanged;
                }
            }
        }

        /// <summary> Adds a property notification control. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The value. </param>
        public void AddPropertyNotifyControl( ModelViewBase value )
        {
            this.AddPropertyNotifyControl( "Instrument", value, true );
        }

        /// <summary> Adds a property notification control. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="title"> The title. </param>
        /// <param name="value"> The value. </param>
        public void AddPropertyNotifyControl( string title, ModelViewBase value )
        {
            this.AddPropertyNotifyControl( title, value, true );
        }

        /// <summary> Adds a property notification control. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="title">             The title. </param>
        /// <param name="value">             The value. </param>
        /// <param name="disposeEnabled">    true to enable, false to disable the dispose. </param>
        /// <param name="showTraceMessages"> True to show, false to hide the trace messages. </param>
        public void AddPropertyNotifyControl( string title, ModelViewBase value, bool disposeEnabled, bool showTraceMessages )
        {
            this.ResizeClientArea( title, value, showTraceMessages );
            this.PropertyNotifyControl = value;
            if ( showTraceMessages )
            {
                this.AddPrivateListeners();
            }

            this._PropertyNotifyControlDisposeEnabled = disposeEnabled;
            this.PropertyNotifyControl.Dock = DockStyle.Fill;
            this.PropertyNotifyControl.TabIndex = 0;
            this.PropertyNotifyControl.BackColor = System.Drawing.Color.Transparent;
            this.PropertyNotifyControl.Font = new System.Drawing.Font( this.Font, System.Drawing.FontStyle.Regular );
            this.PropertyNotifyControl.Name = "_PropertyNotifyControl";
        }

        /// <summary> Adds a property notification control. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="title">          The title. </param>
        /// <param name="value">          The value. </param>
        /// <param name="disposeEnabled"> true to enable, false to disable the dispose. </param>
        public void AddPropertyNotifyControl( string title, ModelViewBase value, bool disposeEnabled )
        {
            this.AddPropertyNotifyControl( title, value, disposeEnabled, true );
        }

        /// <summary> Executes the property change action. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender">       The sender. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( ModelViewBase sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
            {
                return;
            }

            switch ( propertyName )
            {
                case (nameof( isr.Core.Forma.ModelViewBase.StatusPrompt )):
                    {
                        this.StatusPrompt = sender.StatusPrompt;
                        break;
                    }

                default:
                    {
                        break;
                    }
            }
        }

        /// <summary> Property view property changed. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void PropertyNotifyControlPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
            {
                return;
            }

            string activity = $"handling {nameof( ModelViewBase )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.PropertyNotifyControlPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as ModelViewBase, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " MODEL VIEW TALKER "

        /// <summary> true to enable, false to disable the disposal of a talker control. </summary>
        private bool _TalkerControlDisposeEnabled;

        /// <summary> The with events. </summary>
        private ModelViewTalkerBase _ModelView;

        private ModelViewTalkerBase ModelView
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._ModelView;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._ModelView != null )
                {
                    this._ModelView.PropertyChanged -= this.ModelViewPropertyChanged;
                }

                this._ModelView = value;
                if ( this._ModelView != null )
                {
                    this._ModelView.PropertyChanged += this.ModelViewPropertyChanged;
                }
            }
        }

        /// <summary> Adds a talker control panel. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The value. </param>
        public void AddTalkerControl( ModelViewTalkerBase value )
        {
            this.AddTalkerControl( "View", value, true );
        }

        /// <summary> Adds an talker control. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="title"> The title. </param>
        /// <param name="value"> The value. </param>
        public void AddTalkerControl( string title, ModelViewTalkerBase value )
        {
            this.AddTalkerControl( title, value, true );
        }

        /// <summary> Adds an talker control. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="title">             The title. </param>
        /// <param name="value">             The value. </param>
        /// <param name="disposeEnabled">    true to enable, false to disable the dispose. </param>
        /// <param name="showTraceMessages"> True to show, false to hide the trace messages. </param>
        public void AddTalkerControl( string title, ModelViewTalkerBase value, bool disposeEnabled, bool showTraceMessages )
        {
            this.ResizeClientArea( title, value, showTraceMessages );
            this.ModelView = value;
            if ( showTraceMessages )
            {
                this.AddPrivateListeners();
            }

            this._TalkerControlDisposeEnabled = disposeEnabled;
            this.ModelView.Dock = DockStyle.Fill;
            this.ModelView.TabIndex = 0;
            this.ModelView.BackColor = System.Drawing.Color.Transparent;
            this.ModelView.Font = new System.Drawing.Font( this.Font, System.Drawing.FontStyle.Regular );
            this.ModelView.Name = "_TalkerControl";
            this.ModelView.AssignTalker( this.Talker );
        }

        /// <summary> Adds an talker control. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="title">          The title. </param>
        /// <param name="value">          The value. </param>
        /// <param name="disposeEnabled"> true to enable, false to disable the dispose. </param>
        public void AddTalkerControl( string title, ModelViewTalkerBase value, bool disposeEnabled )
        {
            this.AddTalkerControl( title, value, disposeEnabled, true );
        }

        /// <summary> Executes the property change action. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender">       The sender. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( ModelViewTalkerBase sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
            {
                return;
            }

            switch ( propertyName )
            {
                case (nameof( isr.Core.Forma.ModelViewBase.StatusPrompt )):
                    {
                        this.StatusPrompt = sender.StatusPrompt;
                        break;
                    }

                default:
                    {
                        break;
                    }
            }

        }

        /// <summary> talker control property changed. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ModelViewPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
            {
                return;
            }

            string activity = $"handling {nameof( ModelViewTalkerBase )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.ModelViewPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as ModelViewTalkerBase, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " EXPOSED CONTROLS "

        /// <summary> Gets the status strip. </summary>
        /// <value> The status strip. </value>
        protected StatusStrip StatusStrip { get; private set; }

        /// <summary> Gets the status label. </summary>
        /// <value> The status label. </value>
        protected ToolStripStatusLabel StatusLabel { get; private set; }

        /// <summary> Gets or sets the status prompt. </summary>
        /// <value> The status prompt. </value>
        public string StatusPrompt
        {
            get => this.StatusLabel.ToolTipText;

            set {
                if ( !string.Equals( value, this.StatusPrompt, StringComparison.OrdinalIgnoreCase ) )
                {
                    this.StatusLabel.Text = WinForms.CompactExtensions.CompactExtensionMethods.Compact( value, this.StatusLabel );
                    this.StatusLabel.ToolTipText = value;
                }
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary> Adds the listeners such as the current trace messages box. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        protected void AddPrivateListeners()
        {
            base.AddPrivateListener( this._TraceMessagesBox );
            this.ModelView?.AddPrivateListeners( this.Talker );
            // Me._MyTalkerControl?.AddPrivateListeners(Me.Talker)
        }

        /// <summary> Adds the listeners such as the top level trace messages box and log. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="listener"> The listener. </param>
        public override void AddListener( IMessageListener listener )
        {
            this.ModelView?.AddListener( listener );
            // Me._MyTalkerControl?.AddListener(listener)
            base.AddListener( listener );
        }

        /// <summary> Removes the listeners if the talker was not assigned. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public override void RemoveListeners()
        {
            base.RemoveListeners();
            this.ModelView?.RemoveListeners();
            // Me._MyTalkerControl?.RemoveListeners()
        }

        /// <summary> Applies the trace level to all listeners to the specified type. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="listenerType"> Type of the listener. </param>
        /// <param name="value">        The value. </param>
        public override void ApplyListenerTraceLevel( ListenerType listenerType, TraceEventType value )
        {
            if ( listenerType == this._TraceMessagesBox.ListenerType )
            {
                this._TraceMessagesBox.ApplyTraceLevel( value );
            }

            this.ModelView?.ApplyListenerTraceLevel( listenerType, value );
            // Me._MyTalkerControl?.ApplyListenerTraceLevel(listenerType, value)
            // this should apply only to the listeners associated with this form
            base.ApplyListenerTraceLevel( listenerType, value );
        }

        /// <summary> Applies the trace level type to all talkers. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="listenerType"> Type of the trace level. </param>
        /// <param name="value">        The value. </param>
        public override void ApplyTalkerTraceLevel( ListenerType listenerType, TraceEventType value )
        {
            this.ModelView?.ApplyTalkerTraceLevel( listenerType, value );
            // Me._MyTalkerControl?.ApplyTalkerTraceLevel(listenerType, value)
            base.ApplyTalkerTraceLevel( listenerType, value );
        }

        /// <summary>
        /// Uses the <see cref="ITalker"/> to publish the message or log if <see cref="ITalker"/> is
        /// nothing.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

        #region " MESSAGE BOX EVENTS "

        /// <summary> Handles the <see cref="_TraceMessagesBox"/> property changed event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender">       Source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( TraceMessagesBox sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
            {
                return;
            }

            if ( string.Equals( propertyName, nameof( TraceMessagesBox.StatusPrompt ) ) )
            {
                this.StatusPrompt = sender.StatusPrompt;
            }
        }

        /// <summary> Handles Trace messages box property changed event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TraceMessagesBoxPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( sender is null || e is null )
            {
                return;
            }

            string activity = $"handling {nameof( TraceMessagesBox )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.TraceMessagesBoxPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as TraceMessagesBox, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

    }

    /// <summary> Collection of <see cref="ConsoleForm"/> forms. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-01-15 </para>
    /// </remarks>
    public class ConsoleFormCollection : ListenerFormCollection
    {

        #region " PROPERTY NOTIFY CONTROL BASE "

        /// <summary> Adds and shows the form. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="title"> The title. </param>
        /// <param name="form">  The form. </param>
        /// <param name="panel"> The panel. </param>
        /// <param name="log">   The Logger. </param>
        public void ShowNew( string title, ConsoleForm form, ModelViewBase panel, Logger log )
        {
            this.ShowNew( title, form, panel, log, false );
        }

        /// <summary> Adds and shows the form. </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="title">          The title. </param>
        /// <param name="form">           The form. </param>
        /// <param name="panel">          The panel. </param>
        /// <param name="log">            The Logger. </param>
        /// <param name="disposeEnabled"> True to enable, false to disable the dispose. </param>
        public void ShowNew( string title, ConsoleForm form, ModelViewBase panel, Logger log, bool disposeEnabled )
        {
            if ( form is null )
            {
                throw new ArgumentNullException( nameof( form ) );
            }

            form.AddPropertyNotifyControl( title, panel, disposeEnabled );
            this.ShowNew( form, log );
        }

        /// <summary> Adds and shows the form. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="title"> The title. </param>
        /// <param name="panel"> The panel. </param>
        /// <param name="log">   The Logger. </param>
        public void ShowNew( string title, ModelViewBase panel, Logger log )
        {
            ConsoleForm form = null;
            try
            {
                form = new ConsoleForm();
                this.ShowNew( title, form, panel, log );
            }
            catch
            {
                if ( form is object )
                {
                    form.Dispose();
                }

                throw;
            }
        }

        /// <summary> Adds and shows the form. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="form">  The form. </param>
        /// <param name="panel"> The panel. </param>
        /// <param name="log">   The Logger. </param>
        public void ShowNew( ConsoleForm form, ModelViewBase panel, Logger log )
        {
            if ( form is null )
            {
                throw new ArgumentNullException( nameof( form ) );
            }

            form.AddPropertyNotifyControl( panel );
            this.ShowNew( form, log );
        }

        /// <summary> Adds and shows the form. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="panel"> The panel. </param>
        /// <param name="log">   The Logger. </param>
        public void ShowNew( ModelViewBase panel, Logger log )
        {
            ConsoleForm form = null;
            try
            {
                form = new ConsoleForm();
                this.ShowNew( form, panel, log );
            }
            catch
            {
                if ( form is object )
                {
                    form.Dispose();
                }

                throw;
            }
        }

        #endregion

        #region " TALKER CONTROL BASE"

        /// <summary> Adds and shows the form. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="form">  The form. </param>
        /// <param name="panel"> The panel. </param>
        /// <param name="log">   The Logger. </param>
        public void ShowNew( ConsoleForm form, ModelViewTalkerBase panel, Logger log )
        {
            if ( form is null )
            {
                throw new ArgumentNullException( nameof( form ) );
            }

            form.AddTalkerControl( panel );
            this.ShowNew( form, log );
        }

        /// <summary> Adds and shows the form. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="panel"> The panel. </param>
        /// <param name="log">   The Logger. </param>
        public void ShowNew( ModelViewTalkerBase panel, Logger log )
        {
            ConsoleForm form = null;
            try
            {
                form = new ConsoleForm();
                this.ShowNew( form, panel, log );
            }
            catch
            {
                if ( form is object )
                {
                    form.Dispose();
                }

                throw;
            }
        }

        /// <summary> Adds and shows the form. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="title"> The title. </param>
        /// <param name="panel"> The panel. </param>
        /// <param name="log">   The Logger. </param>
        public void ShowNew( string title, ModelViewTalkerBase panel, Logger log )
        {
            ConsoleForm form = null;
            try
            {
                form = new ConsoleForm();
                this.ShowNew( title, form, panel, log );
            }
            catch
            {
                if ( form is object )
                {
                    form.Dispose();
                }

                throw;
            }
        }

        /// <summary> Adds and shows the form. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="title"> The title. </param>
        /// <param name="form">  The form. </param>
        /// <param name="panel"> The panel. </param>
        /// <param name="log">   The Logger. </param>
        public void ShowNew( string title, ConsoleForm form, ModelViewTalkerBase panel, Logger log )
        {
            this.ShowNew( title, form, panel, log, false );
        }

        /// <summary> Adds and shows the form. </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="title">           The title. </param>
        /// <param name="form">            The form. </param>
        /// <param name="panel">           The panel. </param>
        /// <param name="log">             The Logger. </param>
        /// <param name="disposedEnabled"> True to enable, false to disable the disposed. </param>
        public void ShowNew( string title, ConsoleForm form, ModelViewTalkerBase panel, Logger log, bool disposedEnabled )
        {
            if ( form is null )
            {
                throw new ArgumentNullException( nameof( form ) );
            }

            form.AddTalkerControl( title, panel, disposedEnabled );
            this.ShowNew( form, log );
        }

        #endregion

    }
}
