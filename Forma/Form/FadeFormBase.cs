using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

using isr.Core.Forma.ExceptionExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Forma
{

    /// <summary> Form with fade in and out capabilities. </summary>
    /// <remarks>
    /// Features: <para>
    /// fades in on open; </para><para>
    /// fades out on close; </para><para>
    /// partially fades out on focus lost;</para><para>
    /// fades in on focus; </para><para>
    /// fades out on minimize; </para><para>
    /// fades in on restore;</para><para>
    /// must not annoy the user. </para><para>
    /// Usage: To use FadeForm, just extend it instead of Form and you are ready to go. It is
    /// defaulted to use the fade-in from nothing on open and out to nothing on close. It will also
    /// fade to 85% opacity when not the active window. Fading can be disabled and enabled with two
    /// methods setting the default enable and disable modes.</para> (c) 2007 Integrated Scientific
    /// Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2007-08-13. 1.0.2781. from Nicholas Seward  </para><para>
    /// http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx.  </para><para>
    /// David, 2007-08-13, 1.0.2781. Convert from C#. </para>
    /// </remarks>
    public partial class FadeFormBase : Form
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Specialized default constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        protected FadeFormBase() : base()
        {
            this._FadeTime = 0.35d;
            this._ActiveOpacity = 1d;
            this._InactiveOpacity = 0.85d;
            this.InitializeComponent();
        }

        /// <summary> Initializes the component. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            this.AutoScaleDimensions = new SizeF( 7.0f, 17.0f );
            this.AutoScaleMode = AutoScaleMode.Font;
            this.ClientSize = new Size( 331, 341 );
            this.Font = new Font( SystemFonts.MessageBoxFont.FontFamily, 9.75f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte( 0 ) );
            this.Icon = My.Resources.Resources.favicon;
            this.Name = "FadeFormBase";
            this._TimerThis = new Timer() { Interval = 25 };
            this._TimerThis.Tick += new EventHandler( this.Timer_Tick );
            this.ResumeLayout( false );
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this.TimerThis is object )
                    {
                        this.TimerThis.Dispose();
                        this.TimerThis = null;
                    }
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #region " CLASS STYLE "

        /// <summary> The enable drop shadow version. </summary>
        public const int EnableDropShadowVersion = 5;

        /// <summary> Gets the class style. </summary>
        /// <value> The class style. </value>
        protected ClassStyleConstants ClassStyle { get; set; } = ClassStyleConstants.None;

        /// <summary> Adds a drop shadow parameter. </summary>
        /// <remarks>
        /// From Code Project: http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx.
        /// </remarks>
        /// <value> Options that control the create. </value>
        protected override CreateParams CreateParams
        {
            [System.Security.Permissions.SecurityPermission( System.Security.Permissions.SecurityAction.Demand, Flags = System.Security.Permissions.SecurityPermissionFlag.UnmanagedCode )]
            get {
                var cp = base.CreateParams;
                cp.ClassStyle |= ( int ) this.ClassStyle;
                return cp;
            }
        }

        #endregion

        #endregion

        #region " FADING PROPERTIES "

        /// <summary> The active opacity. </summary>
        private double _ActiveOpacity;

        /// <summary>
        /// Gets or sets the opacity that the form will transition to when the form gets focus.
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <value> The active opacity. </value>
        public double ActiveOpacity
        {
            get => this._ActiveOpacity;

            set {
                this._ActiveOpacity = value >= 0d && value <= 1d
                    ? value
                    : throw new ArgumentOutOfRangeException( nameof( value ), "The Active Opacity must be between 0 and 1." );

                if ( this.ContainsFocus )
                {
                    this.TargetOpacity = this._ActiveOpacity;
                }
            }
        }

        /// <summary> The fade time. </summary>
        private double _FadeTime;

        /// <summary>
        /// Gets or sets the time it takes to fade from 1 to 0 or the other way around.
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <value> The fade time. </value>
        public double FadeTime
        {
            get => this._FadeTime;

            set => this._FadeTime = value > 0d ? value : throw new ArgumentOutOfRangeException( nameof( value ), "The FadeTime must be a positive value." );
        }

        /// <summary> The inactive opacity. </summary>
        private double _InactiveOpacity;

        /// <summary>
        /// Gets or sets the opacity that the form will transition to when the form doesn't have focus.
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <value> The inactive opacity. </value>
        public double InactiveOpacity
        {
            get => this._InactiveOpacity;

            set {
                this._InactiveOpacity = value >= 0d && value <= 1d
                    ? value
                    : throw new ArgumentOutOfRangeException( nameof( value ), "The InactiveOpacity must be between 0 and 1." );

                if ( !this.ContainsFocus && this.WindowState != FormWindowState.Minimized )
                {
                    this.TargetOpacity = this._InactiveOpacity;
                }
            }
        }

        /// <summary> The minimized opacity. </summary>
        private double _MinimizedOpacity;

        /// <summary>
        /// Gets or sets the opacity that the form will transition to when the form is minimized.
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <value> The minimized opacity. </value>
        public double MinimizedOpacity
        {
            get => this._MinimizedOpacity;

            set {
                this._MinimizedOpacity = value >= 0d && value <= 1d
                    ? value
                    : throw new ArgumentOutOfRangeException( nameof( value ), "The MinimizedOpacity must be between 0 and 1." );

                if ( !this.ContainsFocus && this.WindowState != FormWindowState.Minimized )
                {
                    this.TargetOpacity = this._InactiveOpacity;
                }
            }
        }

        /// <summary> Target opacity. </summary>
        private double _TargetOpacity;

        /// <summary> Gets or sets the opacity the form is transitioning to. </summary>
        /// <remarks>
        /// Setting this value amounts also to a one-time fade to any value. The opacity is never
        /// actually 1. This is hack to keep the window from flashing black. The cause of this is not
        /// clear.
        /// </remarks>
        /// <value> The target opacity. </value>
        public double TargetOpacity
        {
            get => this._TargetOpacity;

            set {
                this._TargetOpacity = value;
                if ( !this.TimerThis.Enabled )
                {
                    this.TimerThis.Start();
                }
            }
        }

        #endregion

        #region " FADING CONTROL "

        /// <summary> Turns off opacity fading. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public void DisableFade()
        {
            this._ActiveOpacity = 1d;
            this._InactiveOpacity = 1d;
            this._MinimizedOpacity = 1d;
        }

        /// <summary> Turns on opacity fading. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public void EnableFadeDefaults()
        {
            this._ActiveOpacity = 1d;
            this._InactiveOpacity = 0.85d;
            this._MinimizedOpacity = 0d;
            this._FadeTime = 0.35d;
        }

        #endregion

        #region " FORM EVENT HANDLERS "

        /// <summary>
        /// Gets or sets the sentinel indicating that the form loaded without an exception. Should be set
        /// only if load did not fail.
        /// </summary>
        /// <value> The is loaded. </value>
        protected bool IsLoaded { get; set; }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Form.Load" /> event after reading the start
        /// position from the configuration file.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnLoad( EventArgs e )
        {
            try
            {
                this.Opacity = this.MinimizedOpacity;
                this.TargetOpacity = this.ActiveOpacity;
                this.IsLoaded = true;
            }
            catch
            {
                throw;
            }
            finally
            {
                base.OnLoad( e );
            }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Form.Activated" /> event. Fades in the form when
        /// it gains focus.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnActivated( EventArgs e )
        {
            this.TargetOpacity = this.ActiveOpacity;
            base.OnActivated( e );
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Form.Deactivate" /> event. Fades out the form
        /// when it losses focus.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> The <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnDeactivate( EventArgs e )
        {
            this.TargetOpacity = this.InactiveOpacity;
            base.OnDeactivate( e );
        }

        #endregion

        #region " OPACITY TIMER "

        /// <summary> Timer to aid in fade effects. </summary>
        private Timer _TimerThis;

        private Timer TimerThis
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._TimerThis;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._TimerThis != null )
                {
                    this._TimerThis.Tick -= this.Timer_Tick;
                }

                this._TimerThis = value;
                if ( this._TimerThis != null )
                {
                    this._TimerThis.Tick += this.Timer_Tick;
                }
            }
        }

        /// <summary> Performs fade increment. </summary>
        /// <remarks>
        /// The timer is stopped after reaching the target opacity. The timer gets started whenever the
        /// <see cref="TargetOpacity">target opacity</see> is set method. The timer is stopped to
        /// conserve processor power when not needed.
        /// </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void Timer_Tick( object sender, EventArgs e )
        {
            double fadeChangePerTick = this.TimerThis.Interval * 1.0d / 1000d / this._FadeTime;
            // Check to see if it is time to stop the timer
            if ( Math.Abs( this._TargetOpacity - this.Opacity ) < fadeChangePerTick )
            {
                // Stop the timer to save processor.
                this.TimerThis.Stop();
                // There is an ugly black flash if you set the Opacity to 1.0
                this.Opacity = this._TargetOpacity == 1d ? 0.999d : this.TargetOpacity;
                // Process held Windows Message.
                base.WndProc( ref this.heldMessage );
                this.heldMessage = new Message();
            }
            else if ( this.TargetOpacity > this.Opacity )
            {
                this.Opacity += fadeChangePerTick;
            }
            else if ( this.TargetOpacity < this.Opacity )
            {
                this.Opacity -= fadeChangePerTick;
            }
        }

        #endregion

    }
}
