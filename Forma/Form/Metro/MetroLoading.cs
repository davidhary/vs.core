using System.Windows.Forms;

namespace isr.Core.Forma
{

    /// <summary> A metro loading. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public partial class MetroLoading : UserControl
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public MetroLoading()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Releases the unmanaged resources used by the isr.Core.Controls.MetroLoading and optionally
        /// releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }
    }
}
