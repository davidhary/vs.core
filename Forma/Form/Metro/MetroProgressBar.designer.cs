namespace isr.Core.Forma
{
    public partial class MetroProgressBar
    {

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region "Component Designer generated code"

        /// <summary>
        /// Required method for Designer support - do not modify the contents of this method with the
        /// code editor.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(MetroProgressBar));
            _PictureBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)_PictureBox).BeginInit();
            SuspendLayout();
            // 
            // _PictureBox
            // 
            _PictureBox.BackColor = System.Drawing.Color.Transparent;
            _PictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            _PictureBox.Image = (System.Drawing.Image)resources.GetObject("_pictureBox.Image");
            _PictureBox.Location = new System.Drawing.Point(0, 0);
            _PictureBox.Name = "_PictureBox";
            _PictureBox.Size = new System.Drawing.Size(308, 20);
            _PictureBox.TabIndex = 0;
            _PictureBox.TabStop = false;
            // 
            // MetroProgressBar
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(6.0f, 13.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BackColor = System.Drawing.Color.Transparent;
            Controls.Add(_PictureBox);
            Name = "MetroProgressBar";
            Size = new System.Drawing.Size(308, 20);
            ((System.ComponentModel.ISupportInitialize)_PictureBox).EndInit();
            ResumeLayout(false);
        }

        #endregion

        /// <summary> The picture box control. </summary>
        private System.Windows.Forms.PictureBox _PictureBox;
    }
}
