using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace isr.Core.Forma
{

    /// <summary> Form for viewing the modal message. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-07-17 </para>
    /// </remarks>
    public partial class ModalMessageForm : Form
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public ModalMessageForm() : base()
        {

            // This method is required by the Windows Form Designer.
            this.InitializeComponent();
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " BEHAVIOR "

        /// <summary> Gets the message label. </summary>
        /// <value> The message label. </value>
        public Label MessageLabel { get; private set; }

        /// <summary> Gets or sets the display time. </summary>
        /// <value> The display time. </value>
        public TimeSpan DisplayTime { get; set; }

        /// <summary> Raises the activated event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnActivated( EventArgs e )
        {
            this.MessageLabel.Dock = DockStyle.None;
            Application.DoEvents();
            this.MessageLabel.Refresh();

            // Check size of message.
            if ( this.MessageLabel.Height > 3f * this.CreateGraphics().MeasureString( "M", this.Font ).Height )
            {

                // If label higher than three rows, than
                // set for width and height 10% more than the label.
                this.Width = ( int ) (this.MessageLabel.Width / 0.8d + this.Width - this.ClientRectangle.Width);
                this.Height = ( int ) (this.MessageLabel.Height / 0.8d + this.Height - this.ClientRectangle.Height);
            }

            // Center the form on the screen.
            this.SetBounds( (Screen.PrimaryScreen.Bounds.Width - this.Width) / 2, (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2, 0, 0, BoundsSpecified.X | BoundsSpecified.Y );
            this.Refresh();

            // Center the control on the screen.
            this.MessageLabel.SetBounds( (this.ClientRectangle.Width - this.MessageLabel.Width) / 2, (this.ClientRectangle.Height - this.MessageLabel.Height) / 2, 0, 0, BoundsSpecified.X | BoundsSpecified.Y );
            this.MessageLabel.Dock = DockStyle.Fill;
            this.MessageLabel.Refresh();
            this.Refresh();
            var sw = Stopwatch.StartNew();
            // Wait till past time.
            do
            {
                Application.DoEvents();
                Application.DoEvents();
                Application.DoEvents();
            }
            while ( sw.Elapsed < this.DisplayTime );

            // Then hide this form.
            this.Hide();

            // Let the events happen.
            Application.DoEvents();
        }

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.KeyDown" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.KeyEventArgs" /> that contains the event
        /// data. </param>
        protected override void OnKeyDown( KeyEventArgs e )
        {
            base.OnKeyDown( e );
            if ( e is null )
            {
                return;
            }

            int KeyCode = ( int ) e.KeyCode;
            try
            {
                this.Hide();
                Application.DoEvents();
            }
            finally
            {
                e.Handled = KeyCode == 0;
            }
        }

        #endregion

    }
}
