using System.Drawing;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Forma
{

    /// <summary> A form that persists user settings in the Application Settings file. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2013-01-31, 6.1.4779 </para>
    /// </remarks>
    public partial class UserFormBase : System.Windows.Forms.Form
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Specialized default constructor for use only by derived classes. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        protected UserFormBase() : base()
        {
            this.InitializeComponent();
            this.SaveSettingsOnClosing = true;
        }

        /// <summary> Initializes the component. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            this.AutoScaleDimensions = new SizeF( 7.0f, 17.0f );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new Size( 331, 341 );
            this.Font = new Font( SystemFonts.MessageBoxFont.FontFamily, 9.75f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte( 0 ) );
            this.Icon = My.Resources.Resources.favicon;
            this.Name = "UserFormBase";
            this.ResumeLayout( false );
        }

        #endregion

    }
}
