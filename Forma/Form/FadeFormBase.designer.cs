using System;
using System.Security.Permissions;
using System.Windows.Forms;

namespace isr.Core.Forma
{
    public partial class FadeFormBase
    {

        #region " WINDOWS MESSAGES "

        #region " WindowsMessageCodes "

        /// <summary> The Windows Message System Command. </summary>
        private const int WM_SYSCOMMAND = 0x112;

        /// <summary> The Windows Message command. </summary>
        private const int WM_COMMAND = 0x111;

        /// <summary> The screen minimize. </summary>
        private const int SC_MINIMIZE = 0xF020;

        /// <summary> The screen restore. </summary>
        private const int SC_RESTORE = 0xF120;

        /// <summary> The screen close. </summary>
        private const int SC_CLOSE = 0xF060;
        #endregion

        /// <summary>
        /// Gets or sets the WindowsMessage that is being held until the end of a transition.
        /// </summary>
        private Message heldMessage = new Message();

        /// <summary> Intercepts Window Messages before they are processed. </summary>
        /// <remarks>
        /// The minimize and close events require messaging because these actions have to be postponed
        /// until the fade transition is complete. Overriding WndProc catches the request for those
        /// actions and postpones the action until the transition is done.
        /// </remarks>
        /// <param name="m"> [in,out] Windows Message. </param>
        [SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.UnmanagedCode)]
        protected override void WndProc(ref Message m)
        {
            if (m.Msg == WM_SYSCOMMAND || m.Msg == WM_COMMAND)
            {
                // Fade to zero on minimize
                if (m.WParam == new IntPtr(SC_MINIMIZE))
                {
                    if (heldMessage.WParam != new IntPtr(SC_MINIMIZE))
                    {
                        heldMessage = m;
                        TargetOpacity = _MinimizedOpacity;
                    }
                    else
                    {
                        heldMessage = new Message();
                        TargetOpacity = _ActiveOpacity;
                    }

                    return;
                }

                // Fade in if the window is restored from the task bar
                else if (m.WParam == new IntPtr(SC_RESTORE) && WindowState == FormWindowState.Minimized)
                {
                    base.WndProc(ref m);
                    TargetOpacity = _ActiveOpacity;
                    return;
                }

                // Fade out if the window is closed.
                else if (m.WParam == new IntPtr(SC_CLOSE))
                {
                    heldMessage = m;
                    TargetOpacity = _MinimizedOpacity;
                    return;
                }
            }

            base.WndProc(ref m);
        }

        #endregion

    }
}
