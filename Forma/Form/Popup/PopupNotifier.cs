using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

using isr.Core.Forma.ExceptionExtensions;

namespace isr.Core.Forma
{

    /// <summary>
    /// Non-visual component to show a notification window in the right lower corner of the screen.
    /// </summary>
    /// <remarks>
    /// (c) 2011 Simon Baer.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-07-11 Created/modified in 2011 by Simon Baer.  </para><para>
    /// http://www.codeproject.com/KB/dialog/notificationwindow.aspx Based on the Code Project
    /// article by Nicolas Wälti: http://www.codeproject.com/KB/cpp/PopupNotifier.aspx  </para><para>
    /// David, 2014-07-11 Updated. </para>
    /// </remarks>
    [ToolboxBitmap( typeof( PopupNotifier ), "Icon.ico" )]
    [DefaultEvent( "Click" )]
    public class PopupNotifier : Component
    {

        /// <summary> Event that is raised when the text in the notification window is clicked. </summary>
        public event EventHandler<EventArgs> Click;

        /// <summary> Removes event handler. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The handler. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void RemoveClickEventHandler( EventHandler<EventArgs> value )
        {
            foreach ( Delegate d in value is null ? (Array.Empty<Delegate>()) : value.GetInvocationList() )
            {
                try
                {
                    Click -= ( EventHandler<EventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }
        }

        /// <summary> Event that is raised when the notification window is manually closed. </summary>
        public event EventHandler<EventArgs> Close;

        /// <summary> Removes event handler. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The handler. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void RemoveCloseEventHandler( EventHandler<EventArgs> value )
        {
            foreach ( Delegate d in value is null ? (Array.Empty<Delegate>()) : value.GetInvocationList() )
            {
                try
                {
                    Close -= ( EventHandler<EventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }
        }

        /// <summary> <c>true</c> if this object is disposed. </summary>
        private bool _IsDisposed = false;

        /// <summary> The popup form. </summary>
        private PopupNotifierForm _PopupForm;

        /// <summary> The animation timer. </summary>
        private readonly Timer _AnimationTimer;

        /// <summary> The wait timer. </summary>
        private readonly Timer _WaitTimer;

        /// <summary> <c>true</c> if this object is appearing. </summary>
        private bool _IsAppearing = true;

        /// <summary> <c>true</c> if mouse is on. </summary>
        private bool _MouseIsOn = false;

        /// <summary> The maximum position. </summary>
        private int _MaxPosition;

        /// <summary> The maximum opacity. </summary>
        private double _MaxOpacity;

        /// <summary> The position start. </summary>
        private int _PosStart;

        /// <summary> The position stop. </summary>
        private int _PosStop;

        /// <summary> The opacity start. </summary>
        private double _OpacityStart;

        /// <summary> The opacity stop. </summary>
        private double _OpacityStop;

        /// <summary> The software. </summary>
        private Stopwatch _StopWatch;

        #region " Properties "

        /// <summary> Color of the window header. </summary>
        /// <value> The color of the header. </value>
        [Category( "Header" )]
        [DefaultValue( typeof( Color ), "ControlDark" )]
        [Description( "Color of the window header." )]
        public Color HeaderColor { get; set; }

        /// <summary> Color of the window background. </summary>
        /// <value> The color of the body. </value>
        [Category( "Appearance" )]
        [DefaultValue( typeof( Color ), "Control" )]
        [Description( "Color of the window background." )]
        public Color BodyColor { get; set; }

        /// <summary> Color of the title text. </summary>
        /// <value> The color of the title. </value>
        [Category( "Title" )]
        [DefaultValue( typeof( Color ), "Gray" )]
        [Description( "Color of the title text." )]
        public Color TitleColor { get; set; }

        /// <summary> Color of the content text. </summary>
        /// <value> The color of the content. </value>
        [Category( "Content" )]
        [DefaultValue( typeof( Color ), "ControlText" )]
        [Description( "Color of the content text." )]
        public Color ContentColor { get; set; }

        /// <summary> Color of the window border. </summary>
        /// <value> The color of the border. </value>
        [Category( "Appearance" )]
        [DefaultValue( typeof( Color ), "WindowFrame" )]
        [Description( "Color of the window border." )]
        public Color BorderColor { get; set; }

        /// <summary> Border color of the close and options buttons when the mouse is over them. </summary>
        /// <value> The color of the button border. </value>
        [Category( "Buttons" )]
        [DefaultValue( typeof( Color ), "WindowFrame" )]
        [Description( "Border color of the close and options buttons when the mouse is over them." )]
        public Color ButtonBorderColor { get; set; }

        /// <summary>
        /// Background color of the close and options buttons when the mouse is over them.
        /// </summary>
        /// <value> The color of the button hover. </value>
        [Category( "Buttons" )]
        [DefaultValue( typeof( Color ), "Highlight" )]
        [Description( "Background color of the close and options buttons when the mouse is over them." )]
        public Color ButtonHoverColor { get; set; }

        /// <summary> Color of the content text when the mouse is hovering over it. </summary>
        /// <value> The color of the content hover. </value>
        [Category( "Content" )]
        [DefaultValue( typeof( Color ), "HotTrack" )]
        [Description( "Color of the content text when the mouse is hovering over it." )]
        public Color ContentHoverColor { get; set; }

        /// <summary> Gradient of window background color. </summary>
        /// <value> The gradient power. </value>
        [Category( "Appearance" )]
        [DefaultValue( 50 )]
        [Description( "Gradient of window background color." )]
        public int GradientPower { get; set; }

        /// <summary> Font of the content text. </summary>
        /// <value> The content font. </value>
        [Category( "Content" )]
        [Description( "Font of the content text." )]
        public Font ContentFont { get; set; }

        /// <summary> Font of the title. </summary>
        /// <value> The title font. </value>
        [Category( "Title" )]
        [Description( "Font of the title." )]
        public Font TitleFont { get; set; }

        /// <summary> Size of the image. </summary>
        private Size _ImageSize = new( 0, 0 );

        /// <summary> Gets or sets the size of the image. </summary>
        /// <value> The size of the image. </value>
        [Category( "Image" )]
        [Description( "Size of the icon image." )]
        public Size ImageSize
        {
            get => this._ImageSize.Width == 0 ? this.Image is object ? this.Image.Size : new Size( 0, 0 ) : this._ImageSize;

            set => this._ImageSize = value;
        }

        /// <summary> Resets the image size. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public void ResetImageSize()
        {
            this._ImageSize = Size.Empty;
        }

        /// <summary> Determine if we should serialize image size. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
        private bool ShouldSerializeImageSize()
        {
            return !this._ImageSize.Equals( Size.Empty );
        }

        /// <summary> Icon image to display. </summary>
        /// <value> The image. </value>
        [Category( "Image" )]
        [Description( "Icon image to display." )]
        public Image Image { get; set; }

        /// <summary> Whether to show a 'grip' image within the window header. </summary>
        /// <value> The show grip. </value>
        [Category( "Header" )]
        [DefaultValue( true )]
        [Description( "Whether to show a 'grip' image within the window header." )]
        public bool ShowGrip { get; set; }

        /// <summary> Whether to scroll the window or only fade it. </summary>
        /// <value> The scroll. </value>
        [Category( "Behavior" )]
        [DefaultValue( true )]
        [Description( "Whether to scroll the window or only fade it." )]
        public bool Scroll { get; set; }

        /// <summary> Content text to display. </summary>
        /// <value> The content text. </value>
        [Category( "Content" )]
        [Description( "Content text to display." )]
        public string ContentText { get; set; }

        /// <summary> Title text to display. </summary>
        /// <value> The title text. </value>
        [Category( "Title" )]
        [Description( "Title text to display." )]
        public string TitleText { get; set; }

        /// <summary> Padding of title text. </summary>
        /// <value> The title padding. </value>
        [Category( "Title" )]
        [Description( "Padding of title text." )]
        public Padding TitlePadding { get; set; }

        /// <summary> Resets the title padding. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public void ResetTitlePadding()
        {
            this.TitlePadding = Padding.Empty;
        }

        /// <summary> Determine if we should serialize title padding. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
        private bool ShouldSerializeTitlePadding()
        {
            return !this.TitlePadding.Equals( Padding.Empty );
        }

        /// <summary> Padding of content text. </summary>
        /// <value> The content padding. </value>
        [Category( "Content" )]
        [Description( "Padding of content text." )]
        public Padding ContentPadding { get; set; }

        /// <summary> Resets the content padding. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public void ResetContentPadding()
        {
            this.ContentPadding = Padding.Empty;
        }

        /// <summary> Determine if we should serialize content padding. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
        private bool ShouldSerializeContentPadding()
        {
            return !this.ContentPadding.Equals( Padding.Empty );
        }

        /// <summary> Padding of icon image. </summary>
        /// <value> The image padding. </value>
        [Category( "Image" )]
        [Description( "Padding of icon image." )]
        public Padding ImagePadding { get; set; }

        /// <summary> Resets the image padding. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public void ResetImagePadding()
        {
            this.ImagePadding = Padding.Empty;
        }

        /// <summary> Determine if we should serialize image padding. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
        private bool ShouldSerializeImagePadding()
        {
            return !this.ImagePadding.Equals( Padding.Empty );
        }

        /// <summary> Height of window header. </summary>
        /// <value> The height of the header. </value>
        [Category( "Header" )]
        [DefaultValue( 9 )]
        [Description( "Height of window header." )]
        public int HeaderHeight { get; set; }

        /// <summary> Whether to show the close button. </summary>
        /// <value> The show close button. </value>
        [Category( "Buttons" )]
        [DefaultValue( true )]
        [Description( "Whether to show the close button." )]
        public bool ShowCloseButton { get; set; }

        /// <summary> Whether to show the options button. </summary>
        /// <value> The show options button. </value>
        [Category( "Buttons" )]
        [DefaultValue( false )]
        [Description( "Whether to show the options button." )]
        public bool ShowOptionsButton { get; set; }

        /// <summary> Context menu to open when clicking on the options button. </summary>
        /// <value> The options menu. </value>
        [Category( "Behavior" )]
        [Description( "Context menu to open when clicking on the options button." )]
        public ContextMenuStrip OptionsMenu { get; set; }

        /// <summary> Time in milliseconds the window is displayed. </summary>
        /// <value> The delay. </value>
        [Category( "Behavior" )]
        [DefaultValue( 3000 )]
        [Description( "Time in milliseconds the window is displayed." )]
        public int Delay { get; set; }

        /// <summary> Time in milliseconds needed to make the window appear or disappear. </summary>
        /// <value> The animation duration. </value>
        [Category( "Behavior" )]
        [DefaultValue( 1000 )]
        [Description( "Time in milliseconds needed to make the window appear or disappear." )]
        public int AnimationDuration { get; set; }

        /// <summary> Interval in milliseconds used to draw the animation. </summary>
        /// <value> The animation interval. </value>
        [Category( "Behavior" )]
        [DefaultValue( 10 )]
        [Description( "Interval in milliseconds used to draw the animation." )]
        public int AnimationInterval { get; set; }

        /// <summary> Size of the window. </summary>
        /// <value> The size. </value>
        [Category( "Appearance" )]
        [Description( "Size of the window." )]
        public Size Size { get; set; }

        #endregion

        /// <summary> Create a new instance of the popup component. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public PopupNotifier()
        {
            // set default values
            this.HeaderColor = SystemColors.ControlDark;
            this.BodyColor = SystemColors.Control;
            this.TitleColor = Color.Gray;
            this.ContentColor = SystemColors.ControlText;
            this.BorderColor = SystemColors.WindowFrame;
            this.ButtonBorderColor = SystemColors.WindowFrame;
            this.ButtonHoverColor = SystemColors.Highlight;
            this.ContentHoverColor = SystemColors.HotTrack;
            this.GradientPower = 50;
            this.ContentFont = SystemFonts.DialogFont;
            this.TitleFont = SystemFonts.CaptionFont;
            this.ShowGrip = true;
            this.Scroll = true;
            this.TitlePadding = new Padding( 0 );
            this.ContentPadding = new Padding( 0 );
            this.ImagePadding = new Padding( 0 );
            this.HeaderHeight = 9;
            this.ShowCloseButton = true;
            this.ShowOptionsButton = false;
            this.Delay = 3000;
            this.AnimationInterval = 10;
            this.AnimationDuration = 1000;
            this.Size = new Size( 400, 100 );
            this._PopupForm = new PopupNotifierForm( this ) {
                TopMost = true,
                FormBorderStyle = FormBorderStyle.None,
                StartPosition = FormStartPosition.Manual
            };
            this._PopupForm.MouseEnter += this.PopupForm_MouseEnter;
            this._PopupForm.MouseLeave += this.PopupForm_MouseLeave;
            this._PopupForm.CloseClick += this.PopupForm_CloseClick;
            this._PopupForm.LinkClick += this.PopupForm_LinkClick;
            this._PopupForm.ContextMenuOpened += this.PopupForm_ContextMenuOpened;
            this._PopupForm.ContextMenuClosed += this.PopupForm_ContextMenuClosed;
            this._AnimationTimer = new Timer();
            this._AnimationTimer.Tick += this.AnimationTimer_Tick;
            this._WaitTimer = new Timer();
            this._WaitTimer.Tick += this.WaitTimer_Tick;
        }

        /// <summary>
        /// Show the notification window if it is not already visible. If the window is currently
        /// disappearing, it is shown again.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public void Popup()
        {
            if ( !this._IsDisposed )
            {
                if ( !this._PopupForm.Visible )
                {
                    this._PopupForm.Size = this.Size;
                    if ( this.Scroll )
                    {
                        this._PosStart = Screen.PrimaryScreen.WorkingArea.Bottom;
                        this._PosStop = Screen.PrimaryScreen.WorkingArea.Bottom - this._PopupForm.Height;
                    }
                    else
                    {
                        this._PosStart = Screen.PrimaryScreen.WorkingArea.Bottom - this._PopupForm.Height;
                        this._PosStop = Screen.PrimaryScreen.WorkingArea.Bottom - this._PopupForm.Height;
                    }

                    this._OpacityStart = 0d;
                    this._OpacityStop = 1d;
                    this._PopupForm.Opacity = this._OpacityStart;
                    this._PopupForm.Location = new Point( Screen.PrimaryScreen.WorkingArea.Right - this._PopupForm.Size.Width - 1, this._PosStart );
                    this._PopupForm.Show();
                    this._IsAppearing = true;
                    this._WaitTimer.Interval = this.Delay;
                    this._AnimationTimer.Interval = this.AnimationInterval;
                    this._AnimationTimer.Start();
                    this._StopWatch = Stopwatch.StartNew();
                    Debug.WriteLine( "Animation started." );
                }
                else
                {
                    if ( !this._IsAppearing )
                    {
                        this._PopupForm.Top = this._MaxPosition;
                        this._PopupForm.Opacity = this._MaxOpacity;
                        this._AnimationTimer.Stop();
                        Debug.WriteLine( "Animation stopped." );
                        this._WaitTimer.Stop();
                        this._WaitTimer.Start();
                        Debug.WriteLine( "Wait timer started." );
                    }

                    this._PopupForm.Invalidate();
                }
            }
        }

        /// <summary> Hide the notification window. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public void Hide()
        {
            Debug.WriteLine( "Animation stopped." );
            Debug.WriteLine( "Wait timer stopped." );
            this._AnimationTimer.Stop();
            this._WaitTimer.Stop();
            this._PopupForm.Hide();
        }

        /// <summary>
        /// The custom options menu has been closed. Restart the timer for closing the notification
        /// window.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> . </param>
        /// <param name="e">      . </param>
        private void PopupForm_ContextMenuClosed( object sender, EventArgs e )
        {
            Debug.WriteLine( "Menu closed." );
            if ( !this._MouseIsOn )
            {
                this._WaitTimer.Interval = this.Delay;
                this._WaitTimer.Start();
                Debug.WriteLine( "Wait timer started." );
            }
        }

        /// <summary>
        /// The custom options menu has been opened. The window must not be closed as long as the menu is
        /// open.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> . </param>
        /// <param name="e">      . </param>
        private void PopupForm_ContextMenuOpened( object sender, EventArgs e )
        {
            Debug.WriteLine( "Menu opened." );
            this._WaitTimer.Stop();
            Debug.WriteLine( "Wait timer stopped." );
        }

        /// <summary> The text has been clicked. Raise the 'Click' event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> . </param>
        /// <param name="e">      . </param>
        private void PopupForm_LinkClick( object sender, EventArgs e )
        {
            var evt = Click;
            evt?.Invoke( this, EventArgs.Empty );
        }

        /// <summary>
        /// The close button has been clicked. Hide the notification window and raise the 'Close' event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> . </param>
        /// <param name="e">      . </param>
        private void PopupForm_CloseClick( object sender, EventArgs e )
        {
            this.Hide();
            var evt = Close;
            evt?.Invoke( this, EventArgs.Empty );
        }

        /// <summary> Update form position and opacity to show/hide the window. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> . </param>
        /// <param name="e">      . </param>
        private void AnimationTimer_Tick( object sender, EventArgs e )
        {
            long elapsed = this._StopWatch.ElapsedMilliseconds;
            int posCurrent = ( int ) (this._PosStart + (this._PosStop - this._PosStart) * elapsed / this.AnimationDuration);
            bool neg = this._PosStop - this._PosStart < 0;
            if ( neg && posCurrent < this._PosStop || !neg && posCurrent > this._PosStop )
            {
                posCurrent = this._PosStop;
            }

            double opacityCurrent = this._OpacityStart + ( int ) ((this._OpacityStop - this._OpacityStart) * elapsed) / this.AnimationDuration;
            neg = this._OpacityStop - this._OpacityStart < 0d;
            if ( neg && opacityCurrent < this._OpacityStop || !neg && opacityCurrent > this._OpacityStop )
            {
                opacityCurrent = this._OpacityStop;
            }

            this._PopupForm.Top = posCurrent;
            this._PopupForm.Opacity = opacityCurrent;

            // animation has ended
            if ( elapsed > this.AnimationDuration )
            {
                int posTemp = this._PosStart;
                this._PosStart = this._PosStop;
                this._PosStop = posTemp;
                double opacityTemp = this._OpacityStart;
                this._OpacityStart = this._OpacityStop;
                this._OpacityStop = opacityTemp;
                this._StopWatch.Reset();
                this._AnimationTimer.Stop();
                Debug.WriteLine( "Animation stopped." );
                if ( this._IsAppearing )
                {
                    this._IsAppearing = false;
                    this._MaxPosition = this._PopupForm.Top;
                    this._MaxOpacity = this._PopupForm.Opacity;
                    if ( !this._MouseIsOn )
                    {
                        this._WaitTimer.Stop();
                        this._WaitTimer.Start();
                        Debug.WriteLine( "Wait timer started." );
                    }
                }
                else
                {
                    this._PopupForm.Hide();
                }
            }
        }

        /// <summary> The wait timer has elapsed, start the animation to hide the window. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> . </param>
        /// <param name="e">      . </param>
        private void WaitTimer_Tick( object sender, EventArgs e )
        {
            Debug.WriteLine( "Wait timer elapsed." );
            this._WaitTimer.Stop();
            this._AnimationTimer.Interval = this.AnimationInterval;
            this._AnimationTimer.Start();
            this._StopWatch.Restart();
            Debug.WriteLine( "Animation started." );
        }

        /// <summary> Start wait timer if the mouse leaves the form. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> . </param>
        /// <param name="e">      . </param>
        private void PopupForm_MouseLeave( object sender, EventArgs e )
        {
            Debug.WriteLine( "MouseLeave" );
            if ( this._PopupForm.Visible && (this.OptionsMenu is null || !this.OptionsMenu.Visible) )
            {
                this._WaitTimer.Interval = this.Delay;
                this._WaitTimer.Start();
                Debug.WriteLine( "Wait timer started." );
            }

            this._MouseIsOn = false;
        }

        /// <summary> Stop wait timer if the mouse enters the form. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> . </param>
        /// <param name="e">      . </param>
        private void PopupForm_MouseEnter( object sender, EventArgs e )
        {
            Debug.WriteLine( "MouseEnter" );
            if ( !this._IsAppearing )
            {
                this._PopupForm.Top = this._MaxPosition;
                this._PopupForm.Opacity = this._MaxOpacity;
                this._AnimationTimer.Stop();
                Debug.WriteLine( "Animation stopped." );
            }

            this._WaitTimer.Stop();
            Debug.WriteLine( "Wait timer stopped." );
            this._MouseIsOn = true;
        }

        /// <summary> Dispose the notification form. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> . </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this._IsDisposed && disposing )
                {
                    this.RemoveClickEventHandler( Click );
                    this.RemoveCloseEventHandler( Close );
                    if ( this._PopupForm is object )
                    {
                        this._PopupForm.Dispose();
                        this._PopupForm = null;
                    }
                }
            }
            finally
            {
                this._IsDisposed = true;
                base.Dispose( disposing );
            }
        }
    }
}
