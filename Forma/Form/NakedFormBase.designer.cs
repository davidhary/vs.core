using System;
using System.Runtime.InteropServices;

namespace isr.Core.Forma
{
    public partial class NakedFormBase
    {

        #region " SAFE NATIVE METHODS "

        /// <summary> A safe native methods. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private sealed class SafeNativeMethods
        {

            /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
            private SafeNativeMethods() : base()
            {
            }

            /// <summary> Sends a message. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="hWnd">   The window. </param>
        /// <param name="msg">    The message. </param>
        /// <param name="wParam"> The parameter. </param>
        /// <param name="lParam"> The parameter. </param>
        /// <returns> An Int32. </returns>
            [DllImport("user32.dll")]
            internal static extern IntPtr SendMessage(IntPtr hWnd, uint msg, UIntPtr wParam, IntPtr lParam);

            /// <summary> Releases the capture. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> A &lt;MarshalAs(UnmanagedType.Bool)&gt; </returns>
            [DllImport("user32.dll")]
            internal static extern bool ReleaseCapture();
        }

        private readonly uint WM_NCLBUTTONDOWN = 0xA1U;

        /// <summary> The height caption. </summary>
        private readonly UIntPtr HT_CAPTION = new UIntPtr(0x2U);

        // Private HTBORDER As UIntPtr = New UIntPtr(18)
        private readonly UIntPtr HTBOTTOM = new UIntPtr(15U);
        private readonly UIntPtr HTBOTTOMLEFT = new UIntPtr(16U);
        private readonly UIntPtr HTBOTTOMRIGHT = new UIntPtr(17U);
        private readonly UIntPtr HTLEFT = new UIntPtr(10U);
        private readonly UIntPtr HTRIGHT = new UIntPtr(11U);
        private readonly UIntPtr HTTOP = new UIntPtr(12U);
        private readonly UIntPtr HTTOPLEFT = new UIntPtr(13U);
        private readonly UIntPtr HTTOPRIGHT = new UIntPtr(14U);

        /// <summary> Values that represent on borders. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private enum OnBorder
        {
            /// <summary> An enum constant representing the none option. </summary>
            None = 0,
            /// <summary> An enum constant representing the top option. </summary>
            Top = 1,
            /// <summary> An enum constant representing the right option. </summary>
            Right = 2,
            /// <summary> An enum constant representing the bottom option. </summary>
            Bottom = 4,
            /// <summary> An enum constant representing the left option. </summary>
            Left = 8,
            /// <summary> An enum constant representing the top right option. </summary>
            TopRight = 3,
            /// <summary> An enum constant representing the right bottom option. </summary>
            RightBottom = 6,
            /// <summary> An enum constant representing the left top option. </summary>
            LeftTop = 9,
            /// <summary> An enum constant representing the bottom left option. </summary>
            BottomLeft = 12
        }

        #endregion

    }
}
