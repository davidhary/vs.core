using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

namespace isr.Core.Forma
{

    /// <summary>
    /// A user control base. Supports property change notifications. Useful for a settings publisher.
    /// </summary>
    /// <remarks>
    /// The <see cref="PropertyChangedEventContext"/> and the custom
    /// <see cref="PropertyChangedEventHandler"/> make the PropertyChanged event fire on the
    /// SynchronizationContext of the listening code. <para>
    /// The PropertyChanged event declaration is modified to a custom event handler. </para><para>
    /// As each handler may have a different context, the AddHandler block stores the handler being
    /// passed in as well as the SynchronizationContext to be used later when raising the
    /// event.</para> <para>
    /// (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2010-11-02, 1.2.3988 </para>
    /// </remarks>
    public partial class ModelViewBase : UserControl
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Gets the initializing components sentinel. </summary>
        /// <value> The initializing components sentinel. </value>
        protected bool InitializingComponents { get; set; }

        /// <summary>
        /// A private constructor for this class making it not publicly creatable. This ensure using the
        /// class as a singleton.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        protected ModelViewBase() : base()
        {
            this.InitializingComponents = true;
            this.InitializeComponent();
            this.InitializingComponents = false;
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.RemoveFormClosingEventHandlers();
                    this.RemovePropertyChangedEventHandlers();
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " FORM LOAD "

        /// <summary> Handles the <see cref="E:System.Windows.Forms.UserControl.Load" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnLoad( EventArgs e )
        {
            try
            {
                if ( !this.DesignMode )
                {
                    this.InfoProvider.Clear();
                    WinForms.ControlCollectionExtensions.ControlCollectionExtensionMethods.ToolTipSetter( this.Controls, this.ToolTip );
                }
            }
            finally
            {
                base.OnLoad( e );
            }
        }

        #endregion

        #region " CLOSING "

        /// <summary> Removes the FormClosing event handlers. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        protected void RemoveFormClosingEventHandlers()
        {
            this._FormClosingEventHandlers?.RemoveAll();
        }

        /// <summary> The FormClosing event handlers. </summary>
        private readonly EventHandlerContextCollection<CancelEventArgs> _FormClosingEventHandlers = new();

        /// <summary> Event queue for all listeners interested in FormClosing events. </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event EventHandler<CancelEventArgs> FormClosing
        {
            add {
                this._FormClosingEventHandlers.Add( new EventHandlerContext<CancelEventArgs>( value ) );
            }

            remove {
                this._FormClosingEventHandlers.RemoveValue( value );
            }
        }

        private void OnFormClosing( object sender, CancelEventArgs e )
        {
            this._FormClosingEventHandlers.Send( sender, e );
        }

        /// <summary>
        /// Safely and synchronously <see cref="EventHandlerContextCollection{TEventArgs}.Send">sends</see> or invokes the
        /// <see cref="FormClosing">FormClosing Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
        protected void SyncNotifyFormClosing( CancelEventArgs e )
        {
            this._FormClosingEventHandlers.Send( this, e );
        }

        /// <summary> Handles the container form closing when the parent form is closing. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Cancel event information. </param>
        private void HandleFormClosing( object sender, CancelEventArgs e )
        {
            this.OnFormClosing( e );
        }

        /// <summary>
        /// Handles the container form <see cref="E:System.Windows.Forms.Closing" /> event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:ComponentModel.CancelEventArgs" /> that contains the event
        /// data. </param>
        protected virtual void OnFormClosing( CancelEventArgs e )
        {
            this.SyncNotifyFormClosing( e );
        }

        /// <summary> Gets the ancestor form. </summary>
        /// <value> The ancestor form. </value>
        private Form AncestorForm { get; set; }

        /// <summary> Searches for the first ancestor form. </summary>
        /// <remarks>
        /// When called from the <see cref="Control.ParentChanged"/> event, the method returns the form
        /// associated with the top control.  The top control attends a form only after the form layout
        /// completes, so this method ends up being called a few times before the form is located.
        /// </remarks>
        /// <returns> The found ancestor form. </returns>
        private Form FindAncestorForm()
        {
            var parentControl = this.Parent;
            Form parentForm = parentControl as Form;
            while ( !(parentForm is object) && !(parentControl is null) )
            {
                parentControl = parentControl.Parent;
                parentForm = parentControl as Form;
            }

            return parentForm;
        }

        /// <summary> Adds closing handler. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void AddClosingHandler()
        {
            var candidateAncestorForm = this.FindAncestorForm();
            if ( this.AncestorForm is object )
            {
                this.AncestorForm.Closing -= this.HandleFormClosing;
            }

            this.AncestorForm = candidateAncestorForm;
            if ( this.AncestorForm is object )
            {
                this.AncestorForm.Closing += this.HandleFormClosing;
            }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.ParentChanged" /> event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnParentChanged( EventArgs e )
        {
            base.OnParentChanged( e );
            this.AddClosingHandler();
        }

        #endregion

        #region " CORE PROPERTIES  "

        /// <summary> Gets the information provider. </summary>
        /// <value> The information provider. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        protected InfoProvider InfoProvider { get; private set; }

        /// <summary> Gets the tool tip. </summary>
        /// <value> The tool tip. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        protected ToolTip ToolTip { get; private set; }

        /// <summary> The status prompt. </summary>
        private string _StatusPrompt;

        /// <summary> The status prompt. </summary>
        /// <value> The status prompt. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public string StatusPrompt
        {
            get => this._StatusPrompt;

            set {
                if ( !string.Equals( value, this.StatusPrompt ) )
                {
                    this._StatusPrompt = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " EVENT HANDLER ERROR "

        /// <summary>   Event queue for all listeners interested in EventHandlerError events. </summary>
        public event EventHandler<isr.Core.ErrorEventArgs> EventHandlerError;

        /// <summary>   Raises the isr. core. error event. </summary>
        /// <remarks>   David, 2021-05-03. </remarks>
        /// <param name="e">    Event information to send to registered event handlers. </param>
        protected virtual void OnEventHandlerError( isr.Core.ErrorEventArgs e )
        {
            EventHandler<isr.Core.ErrorEventArgs> evt = this.EventHandlerError;
            evt?.Invoke( this, e );
        }

        /// <summary>   Raises the isr. core. error event. </summary>
        /// <remarks>   David, 2021-05-03. </remarks>
        /// <param name="ex">   The exception. </param>
        protected virtual void OnEventHandlerError( Exception ex )
        {
            this.OnEventHandlerError( new isr.Core.ErrorEventArgs( ex ) );
        }

        #endregion

    }
}
