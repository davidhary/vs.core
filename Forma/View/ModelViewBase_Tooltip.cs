﻿using System.Windows.Forms;

namespace isr.Core.Forma
{
    public partial class ModelViewBase : UserControl
    {

        /// <summary>
        /// Sets the Tool tip for all form controls that inherit a <see cref="Control">control base.</see>
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="parent">  Reference to the parent form or control. </param>
        /// <param name="toolTip"> The parent form or control tool tip. </param>
        public static void ToolTipSetter( Control parent, ToolTip toolTip )
        {
            if ( parent is null )
            {
                return;
            }

            if ( toolTip is null )
            {
                return;
            }

            if ( parent is ModelViewBase view )
            {
                view.ToolTipSetter( toolTip );
            }
            else if ( parent.HasChildren )
            {
                foreach ( Control control in parent.Controls )
                {
                    ToolTipSetter( control, toolTip );
                }
            }
        }

        /// <summary>
        /// Sets a tool tip for all controls on the user control. Uses the message already set for this
        /// control.
        /// </summary>
        /// <remarks>
        /// This is required because setting a tool tip from the parent form does not show the tool tip
        /// if hovering above children controls hosted by the user control.
        /// </remarks>
        /// <param name="toolTip"> The tool tip control from the parent form. </param>
        public void ToolTipSetter( ToolTip toolTip )
        {
            if ( toolTip is object )
            {
                this.ApplyToolTipToChildControls( this, toolTip, toolTip.GetToolTip( this ) );
            }
        }

        /// <summary> Sets a tool tip for all controls on the user control. </summary>
        /// <remarks>
        /// This is required because setting a tool tip from the parent form does not show the tool tip
        /// if hovering above children controls hosted by the user control.
        /// </remarks>
        /// <param name="toolTip"> The tool tip control from the parent form. </param>
        /// <param name="message"> The tool tip message to apply to all the children controls and their
        /// children. </param>
        public void ToolTipSetter( ToolTip toolTip, string message )
        {
            this.ApplyToolTipToChildControls( this, toolTip, message );
        }

        /// <summary>
        /// Applies the tool tip to all control hosted by the parent as well as all the children with
        /// these control.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="parent">  The parent control. </param>
        /// <param name="toolTip"> The tool tip control from the parent form. </param>
        /// <param name="message"> The tool tip message to apply to all the children controls and their
        /// children. </param>
        private void ApplyToolTipToChildControls( Control parent, ToolTip toolTip, string message )
        {
            foreach ( Control control in parent.Controls )
            {
                toolTip.SetToolTip( control, message );
                if ( parent.HasChildren )
                {
                    this.ApplyToolTipToChildControls( control, toolTip, message );
                }
            }
        }
    }
}