using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;

using isr.Core.BindingLists;

namespace isr.Core.Forma
{
    public partial class ModelViewTalkerBase
    {

        #region " TRACE EVENTS: LOG AND SHOW "

        /// <summary> The trace events. </summary>
        private static List<KeyValuePair<TraceEventType, string>> _TraceEvents;

        /// <summary> Gets the trace events. </summary>
        /// <value> The trace events. </value>
        public static IEnumerable<KeyValuePair<TraceEventType, string>> TraceEvents
        {
            get {
                if ( _TraceEvents is null || !_TraceEvents.Any() )
                {
                    _TraceEvents = new List<KeyValuePair<TraceEventType, string>>() { ToTraceEvent( TraceEventType.Critical ), ToTraceEvent( TraceEventType.Error ), ToTraceEvent( TraceEventType.Warning ), ToTraceEvent( TraceEventType.Information ), ToTraceEvent( TraceEventType.Verbose ) };
                }

                return _TraceEvents;
            }
        }

        /// <summary> The trace log events. </summary>
        private InvokingBindingList<KeyValuePair<TraceEventType, string>> _TraceLogEvents;

        /// <summary> Gets a list of types of the trace events. </summary>
        /// <value> A list of types of the trace events. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public InvokingBindingList<KeyValuePair<TraceEventType, string>> TraceLogEvents
        {
            get {
                if ( this._TraceLogEvents is null || !this._TraceLogEvents.Any() )
                {
                    this._TraceLogEvents = new InvokingBindingList<KeyValuePair<TraceEventType, string>>( TraceEvents.ToArray() );
                }

                return this._TraceLogEvents;
            }
        }

        /// <summary> Converts a value to a trace event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> Value as a KeyValuePair(Of TraceEventType, String) </returns>
        public static KeyValuePair<TraceEventType, string> ToTraceEvent( TraceEventType value )
        {
            return new KeyValuePair<TraceEventType, string>( value, value.ToString() );
        }

        /// <summary> The trace log event. </summary>
        private KeyValuePair<TraceEventType, string> _TraceLogEvent;

        /// <summary> Gets or sets the trace log level. </summary>
        /// <value> The trace level. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public KeyValuePair<TraceEventType, string> TraceLogEvent
        {
            get => this._TraceLogEvent;

            set {
                if ( value.Key != this.TraceLogEvent.Key )
                {
                    this._TraceLogEvent = value;
                    this.TraceLogLevel = value.Key;
                    this.SyncNotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the trace log level. </summary>
        /// <value> The trace level. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public TraceEventType TraceLogLevel
        {
            get => this.Talker is null ? TraceEventType.Information : this.Talker.TraceLogLevel;

            set {
                if ( this.Talker is object && value != this.TraceLogLevel )
                {
                    this.ApplyTalkerTraceLevel( ListenerType.Logger, value );
                    this.SyncNotifyPropertyChanged();
                    this.TraceLogEvent = ToTraceEvent( value );
                }
            }
        }

        /// <summary> The trace show events. </summary>
        private BindingList<KeyValuePair<TraceEventType, string>> _TraceShowEvents;

        /// <summary> Gets a list of types of the trace events. </summary>
        /// <value> A list of types of the trace events. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public BindingList<KeyValuePair<TraceEventType, string>> TraceShowEvents
        {
            get {
                if ( this._TraceShowEvents is null || !this._TraceShowEvents.Any() )
                {
                    this._TraceShowEvents = new InvokingBindingList<KeyValuePair<TraceEventType, string>>( TraceEvents.ToArray() );
                }

                return this._TraceShowEvents;
            }
        }

        /// <summary> The trace show event. </summary>
        private KeyValuePair<TraceEventType, string> _TraceShowEvent;

        /// <summary> Gets or sets the trace Show level. </summary>
        /// <value> The trace level. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public KeyValuePair<TraceEventType, string> TraceShowEvent
        {
            get => this._TraceShowEvent;

            set {
                if ( !Equals( value, this.TraceShowEvent ) )
                {
                    this._TraceShowEvent = value;
                    this.TraceShowLevel = value.Key;
                    this.SyncNotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the trace Show level. </summary>
        /// <value> The trace level. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public TraceEventType TraceShowLevel
        {
            get => this.Talker is null ? TraceEventType.Information : this.Talker.TraceShowLevel;

            set {
                if ( this.Talker is object && value != this.TraceShowLevel )
                {
                    this.ApplyTalkerTraceLevel( ListenerType.Display, value );
                    this.SyncNotifyPropertyChanged();
                    this.TraceShowEvent = ToTraceEvent( value );
                }
            }
        }

        #endregion

        #region " TRACE EVENT COMBO BOX MANAGEMENT "

        /// <summary> List of types of the trace events. </summary>
        private static InvokingBindingList<TraceEventType> _TraceEventTypes;

        /// <summary> Gets a list of types of the trace events. </summary>
        /// <value> A list of types of the trace events. </value>
        public static BindingList<TraceEventType> TraceEventTypes
        {
            get {
                if ( _TraceEventTypes is null || !_TraceEventTypes.Any() )
                {
                    _TraceEventTypes = new InvokingBindingList<TraceEventType>() { TraceEventType.Critical, TraceEventType.Error, TraceEventType.Warning, TraceEventType.Information, TraceEventType.Verbose };
                }

                return _TraceEventTypes;
            }
        }

        /// <summary> List trace event levels. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="control"> The control. </param>
        public static void ListTraceEventLevels( System.Windows.Forms.ComboBox control )
        {
            if ( control is null )
            {
                throw new ArgumentNullException( nameof( control ) );
            }

            bool comboEnabled = control.Enabled;
            control.Enabled = false;
            control.DataSource = null;
            control.ValueMember = nameof( System.Collections.Generic.KeyValuePair<int, String>.Key );
            control.DisplayMember = nameof( System.Collections.Generic.KeyValuePair<int, String>.Value );
            control.Items.Clear();
            control.DataSource = TraceEvents;
            // This does not seem to work. It lists the items in the combo box, but the control items are empty.
            // control.DataSource = isr.Core.Services.EnumExtensions.ValueDescriptionPairs(TraceEventType.Critical).ToList
            // control.SelectedIndex = -1
            control.Enabled = comboEnabled;
            control.Invalidate();
        }

        /// <summary> Select item. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="control"> The control. </param>
        /// <param name="value">   The value. </param>
        public static void SelectItem( System.Windows.Forms.ToolStripComboBox control, TraceEventType value )
        {
            if ( control is object )
            {
                // control.SelectedItem = New KeyValuePair(Of TraceEventType, String)(value, value.ToString)
                control.SelectedItem = ToTraceEvent( value );
            }
        }

        /// <summary> Selected value. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="control">      The control. </param>
        /// <param name="defaultValue"> The default value. </param>
        /// <returns> A TraceEventType. </returns>
        public static TraceEventType SelectedValue( System.Windows.Forms.ToolStripComboBox control, TraceEventType defaultValue )
        {
            if ( control is object && control.SelectedItem is object )
            {
                KeyValuePair<TraceEventType, string> kvp = ( KeyValuePair<TraceEventType, string> ) control.SelectedItem;
                if ( Enum.IsDefined( typeof( TraceEventType ), kvp.Key ) )
                {
                    defaultValue = kvp.Key;
                }
            }

            return defaultValue;
        }

        #endregion

    }
}
