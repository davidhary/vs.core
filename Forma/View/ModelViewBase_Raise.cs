using System.ComponentModel;

namespace isr.Core.Forma
{
    public partial class ModelViewBase
    {

        #region " NOTIFY DEFAUILT: POST (ASYNC) "

        /// <summary>
        /// Asynchronously notifies (posts) property change on a different thread. Unsafe for cross
        /// threading; much faster.
        /// </summary>
        /// <remarks> David, 2020-07-11. </remarks>
        /// <param name="e"> Property Changed event information. </param>
        protected virtual void NotifyPropertyChanged( PropertyChangedEventArgs e )
        {
            this._PropertyChangedHandlers.Post( this, e );
        }

        /// <summary>
        /// Synchronously notifies property change on the synchronization thread using 'Send' to prevent
        /// cross thread exceptions.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="name"> (Optional) caller member name. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            if ( !string.IsNullOrWhiteSpace( name ) )
            {
                this.NotifyPropertyChanged( new PropertyChangedEventArgs( name ) );
            }
        }

        #endregion

        #region " ASYNC NOTIFY: POST "

        /// <summary>
        /// Asynchronously notifies (posts) property change on a different thread. Unsafe for cross
        /// threading; much faster.
        /// </summary>
        /// <remarks>
        /// Overhead of this method is 7 to 10 times larger than naked raise event. This has no advantage
        /// even with slow handler functions.
        /// </remarks>
        /// <param name="e"> Property Changed event information. </param>
        protected virtual void AsyncNotifyPropertyChanged( PropertyChangedEventArgs e )
        {
            this._PropertyChangedHandlers.Post( this, e );
        }

        /// <summary>
        /// Asynchronously notifies (posts) property change on a different thread. Unsafe for cross
        /// threading; much faster.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="name"> (Optional) caller member name. </param>
        protected void AsyncNotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            if ( !string.IsNullOrWhiteSpace( name ) )
            {
                this.AsyncNotifyPropertyChanged( new PropertyChangedEventArgs( name ) );
            }
        }

        #endregion

        #region " SYNC NOTIFY: SEND "

        /// <summary>
        /// Synchronously notifies property change on the synchronization thread using 'Send' to prevent
        /// cross thread exceptions.
        /// </summary>
        /// <remarks>
        /// Overhead of this method is 3 to 4 times larger than naked raise event. This is the best
        /// approach.
        /// </remarks>
        /// <param name="e"> Property Changed event information. </param>
        protected virtual void SyncNotifyPropertyChanged( PropertyChangedEventArgs e )
        {
            this._PropertyChangedHandlers.Send( this, e );
        }

        /// <summary>
        /// Synchronously notifies property change on the synchronization thread using 'Send' to prevent
        /// cross thread exceptions.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="name"> (Optional) caller member name. </param>
        protected void SyncNotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            if ( !string.IsNullOrWhiteSpace( name ) )
            {
                this.SyncNotifyPropertyChanged( new PropertyChangedEventArgs( name ) );
            }
        }

        #endregion

        #region " RAISE (SENDS) "

        /// <summary>
        /// Synchronously notifies property change on the synchronization thread using 'Send' to prevent
        /// cross thread exceptions.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="name"> (Optional) caller member name. </param>
        protected void RaisePropertyChanged( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            if ( !string.IsNullOrWhiteSpace( name ) )
            {
                this.RaisePropertyChanged( new PropertyChangedEventArgs( name ) );
            }
        }

        /// <summary>
        /// Synchronously notifies property change on the synchronization thread using 'Send' to prevent
        /// cross thread exceptions.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void RaisePropertyChanged( PropertyChangedEventArgs e )
        {
            this.OnPropertyChanged( this, e );
        }

        #endregion

        #region " POST DYNAMIC INVOKE "

        /// <summary> Asynchronously posts and dynamically invokes the property changed event. </summary>
        /// <remarks>
        /// Overhead of this method is 3 to 4 times larger than naked raise event. This is equivalent
        /// the<see cref="SyncNotifyPropertyChanged(PropertyChangedEventArgs)"/> method,.
        /// </remarks>
        /// <param name="name"> (Optional) caller member name. </param>
        protected void AsyncDynamicInvokePropertyChanged( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            if ( !string.IsNullOrWhiteSpace( name ) )
            {
                this.AsyncDynamicInvokePropertyChanged( new PropertyChangedEventArgs( name ) );
            }
        }

        /// <summary> Asynchronously posts and dynamically invokes the property changed event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void AsyncDynamicInvokePropertyChanged( PropertyChangedEventArgs e )
        {
            this._PropertyChangedHandlers.PostDynamicInvoke( this, e );
        }

        #endregion

    }
}
