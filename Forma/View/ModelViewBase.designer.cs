using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Forma
{
    public partial class ModelViewBase
    {

        // Required by the Windows Form Designer
        private IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new Container();
            InfoProvider = new InfoProvider(components);
            ToolTip = new ToolTip(components);
            ((ISupportInitialize)InfoProvider).BeginInit();
            SuspendLayout();
            // 
            // _InfoProvider
            // 
            InfoProvider.ContainerControl = this;
            // 
            // ModelViewBase
            // 
            AutoScaleDimensions = new SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Inherit;
            Font = new Font(SystemFonts.MessageBoxFont.FontFamily, 9.75f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            AutoSizeMode = AutoSizeMode.GrowAndShrink;
            Name = "ModelViewBase";
            Size = new Size(175, 173);
            ((ISupportInitialize)InfoProvider).EndInit();
            ResumeLayout(false);
        }
    }
}
