﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace isr.Core.Forma
{
    public partial class ModelViewTalkerBase
    {

        /// <summary> Gets or sets the publish binding success enabled. </summary>
        /// <value> The publish binding success enabled. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool PublishBindingSuccessEnabled { get; set; }

        /// <summary> Takes the binding failed actions. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="binding"> <see cref="Object"/> instance of this
        /// <see cref="Control"/> </param>
        /// <param name="e">       Binding complete event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnBindingFailed( Binding binding, BindingCompleteEventArgs e )
        {
            string activity = string.Empty;
            if ( binding is null || e is null )
            {
                return;
            }

            try
            {
                activity = "setting cancel state";
                e.Cancel = e.BindingCompleteState != BindingCompleteState.Success;
                activity = $"binding {e.Binding.BindingMemberInfo.BindingField}:{e.BindingCompleteContext}:{e.Binding.BindableComponent}:{e.BindingCompleteState}";
                if ( e.BindingCompleteState == BindingCompleteState.DataError )
                {
                    activity = $"data error; {activity}";
                    _ = this.PublishWarning( $"{activity};. {e.ErrorText}" );
                }
                else if ( e.BindingCompleteState == BindingCompleteState.Exception )
                {
                    if ( !string.IsNullOrWhiteSpace( e.ErrorText ) )
                    {
                        activity = $"{activity}; {e.ErrorText}";
                    }

                    _ = this.PublishException( activity, e.Exception );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Takes the binding succeeded actions. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="binding"> <see cref="Object"/> instance of this
        /// <see cref="Control"/> </param>
        /// <param name="e">       Event information to send to registered event handlers. </param>
        protected override void OnBindingSucceeded( Binding binding, BindingCompleteEventArgs e )
        {
            if ( this.PublishBindingSuccessEnabled && binding is object && e is object )
            {
                _ = this.PublishVerbose( $"binding {e.Binding.BindingMemberInfo.BindingField}:{e.BindingCompleteContext}:{e.Binding.BindableComponent}:{e.BindingCompleteState}" );
            }
        }

        /// <summary> Executes the binding exception action. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="activity">  The activity. </param>
        /// <param name="exception"> The exception. </param>
        protected override void OnBindingException( string activity, Exception exception )
        {
            _ = this.PublishException( activity, exception );
        }
    }
}