using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Forma
{

    /// <summary>
    /// A user control base. Supports property change notification and trace message broadcasting
    /// (talker).
    /// </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-12-26, 2.1.5836. </para>
    /// </remarks>
    public partial class ModelViewTalkerBase : ModelViewBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// A private constructor for this class making it not publicly creatable. This ensure using the
        /// class as a singleton.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        protected ModelViewTalkerBase() : this( new TraceMessageTalker() )
        {
        }

        /// <summary>
        /// A protected constructor for this class making it not publicly creatable. This ensure using
        /// the class as a base class.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="talker"> The talker. </param>
        protected ModelViewTalkerBase( ITraceMessageTalker talker ) : base()
        {
            this.InitializingComponents = true;
            this.InitializeComponent();
            this.InitializingComponents = false;
            this._Talker = talker;
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.RemovePropertyChangedEventHandlers();
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #region " Windows Form Designer generated code "

        // Required by the Windows Form Designer
#pragma warning disable IDE1006 // Naming Styles
        private IContainer components;
#pragma warning restore IDE1006 // Naming Styles

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // ModelViewTalkerBase
            // 
            this.AutoScaleDimensions = new SizeF( 7.0f, 17.0f );
            this.AutoScaleMode = AutoScaleMode.Inherit;
            this.Font = new Font( SystemFonts.MessageBoxFont.FontFamily, 9.75f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte( 0 ) );
            this.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            this.Name = "ModelViewTalkerBase";
            this.Size = new Size( 175, 173 );
            this.ResumeLayout( false );
        }

        #endregion

        #endregion

        #region " FORM EVENTS "

        /// <summary>
        /// Handles the container form <see cref="E:System.Windows.Forms.Closing" /> event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:ComponentModel.CancelEventArgs" /> that contains the event
        /// data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnFormClosing( CancelEventArgs e )
        {
            base.OnFormClosing( e );
            try
            {
                if ( e is object && !e.Cancel )
                {
                    this.RemovePrivateListeners();
                }
            }
            finally
            {
            }
        }

        #endregion

    }
}
