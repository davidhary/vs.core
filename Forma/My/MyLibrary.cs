
namespace isr.Core.Forma.My
{

    /// <summary> Provides assembly information for the class library. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-11-26 </para>
    /// </remarks>
    public sealed partial class MyLibrary
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private MyLibrary() : base()
        {
        }

        /// <summary> Identifier for the trace event. </summary>
        public const int TraceEventId = ( int ) ProjectTraceEventId.Forma;

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "Core Forma Library";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "Core Forma Library";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "Core.Forma";

        /// <summary> The Strong Name of the test assembly. </summary>
        public const string TestAssemblyStrongName = "isr.Core.FormaTests,PublicKey=" + isr.Core.My.SolutionInfo.PublicKey;
    }
}
