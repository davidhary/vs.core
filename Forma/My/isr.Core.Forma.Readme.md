## ISR Core Forma<sub>&trade;</sub>: Windows Forms Framework Class Library
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Revision History [](#){name=Revision-History}

*5.0.7280 2019-12-07*  
Uses now Services, Model and Constructs created from
agnostic.

*4.0.6959 2019-01-20*  
Changes project name and namespace and moves unused
code to the Relic project.

*3.4.6956 2019-01-17*  
Adds splash, popup and metro forms. Removes property
invokes.

*3.4.6944 2019-01-05*  
Adds binding management to base controls and forms.

*3.4.6898 2018-11-20*  
Fixes compact string extensions (Measure Text no
longer alters the string argument).

*3.4.6667 2018-04-03*  
2018 release.

*3.3.6439 2017-08-18*  
Revamps message listeners and talkers. Defaults
listeners to Verbose level and get the talkers to filter trace level
thus allowing to control how messages are published as well as listened
to.

*3.3.6438 2017-08-17*  
Adds functionality to the trace message talkers to
ensure trace levels are correctly updated.

*3.2.6436 2017-08-15*  
Changes trace message tool strip drop down to a drop
down list.

*3.2.6419 2017-06-24*  
Adds functionality to Enum extensions.

*3.1.6384 2017-06-24*  
Defaults to UTC time.

*3.1.6370 2017-06-10*  
Adds class for running tasks with dependencies.

*3.1.6354 2017-05-25*  
Uses thread save queue for messages box.

*3.1.6340 2017-05-11*  
Remove trim start from the default log file writer.
Fixes the default Full range.

*3.1.6332 2017-05-03*  
Uses thread safe queue for logging.

*3.1.6298 2017-03-30*  
Adds exception extension. Adds thread safe token.

*3.0.6093 2016-09-04*  
Adds post and send event handler extensions.

*3.0.6089 2016-09-02*  
Adds base and coding exceptions.

*3.0.5934 2016-03-31*  
Adds configuration editor form.

*3.0.5866 2016-01-23*  
Updates to .NET 4.6.1

*2.1.5828 2015-12-16 Moves trace and log from the Diagnosis class.

*2.1.5822 2015-12-10 Created from Core Publishers.

*2.1.5163 2014-02-19*  
Uses local sync and asynchronous safe event handlers.

*2.0.5126 2014-01-13*  
Tagged as 2014.

*2.0.5024 2013-09-04*  
Simplified Publisher interface and base class.

*2.0.4955 2013-09-04*  
Created based on the legacy core structures.

*1.2.4654 2012-09-28*  
Adds Property Changed Publisher base. Validates name
before invoking property changed notification. Validates values of
action results. Uses base values when adding action results.

\(C\) 2012 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Core Library](https://bitbucket.org/davidhary/vs.core)\
Safe events: ([Code Project](http://www.CodeProject.com/KB/cs/EventSafeTrigger.aspx)
[dream in code](http://www.DreamInCode.net/forums/user/334492-aeonhack/)
[dream in code](http://www.DreamInCode.net/code/snippet5016.htm))  
[Drop Shadow and Fade From](http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx)  
[Engineering Format](http://WallaceKelly.BlogSpot.com/)  
[Efficient Strong Enumerations](http://www.CodeProject.com/KB/cs/efficient_strong_enum.aspx)  
[Enumeration Extensions](http://www.CodeProject.com/Articles/37921/Enums-Flags-and-CSharp-Oh-my-bad-pun.aspx)  
[Exception Extension](https://www.codeproject.com/Tips/1179564/A-Quick-Dirty-Extension-Method-to-Get-the-Full-Exc)  
[String Enumerator](http://www.codeproject.com/Articles/17472/StringEnumerator)  
[Drop Shadow and Fade From](http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx)  
[Notification Window](http://www.codeproject.com/KB/dialog/notificationwindow.aspx)  
[Office Style Splash Screen](http://www.codeproject.com/Articles/804316/Office-Style-Splash-Screen)
