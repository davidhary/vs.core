Namespace My

    Partial Public NotInheritable Class MyLibrary

#Region " LOG "

        ''' <summary> The logger. </summary>
        Private Shared _Logger As Logger

        ''' <summary> Gets the Logger. </summary>
        ''' <value> The Logger. </value>
        Public Shared ReadOnly Property Logger As Logger
            Get
                If MyLibrary._Logger Is Nothing Then CreateLogger()
                Return MyLibrary._Logger
            End Get
        End Property

        ''' <summary> Creates the Logger. </summary>
        ''' <remarks> David, 2020-09-24. </remarks>
        Private Shared Sub CreateLogger()
            MyLibrary._Logger = Nothing
            Try
                MyLibrary._Logger = Logger.Create(Application.Info.ProductName)
                Dim listener As Microsoft.VisualBasic.Logging.FileLogTraceListener
                listener = MyLibrary.Logger.ReplaceDefaultTraceListener(True)
                If Not MyLibrary.Logger.LogFileExists Then MyLibrary.Logger.TraceEventOverride(ApplicationInfoTraceMessage)

                ' set the log for the application
                If Not String.Equals(Application.Log.DefaultFileLogWriter.FullLogFileName, listener.FullLogFileName, StringComparison.OrdinalIgnoreCase) Then
                    Application.Log.TraceSource.Listeners.Remove(CustomFileLogTraceListener.DefaultFileLogWriterName)
                    Application.Log.TraceSource.Listeners.Add(listener)
                    Application.Log.TraceSource.Switch.Level = SourceLevels.Verbose
                End If

                ' set the trace level.
                ApplyTraceLogLevel()
            Catch
                If MyLibrary._Logger IsNot Nothing Then MyLibrary._Logger.Dispose()
                MyLibrary._Logger = Nothing
                Throw
            End Try
        End Sub

        ''' <summary> Gets the trace level. </summary>
        ''' <value> The trace level. </value>
        Public Shared Property TraceLevel As TraceEventType = TraceEventType.Warning

#End Region

#Region " IDENTITY "

        ''' <summary> Identifies this library. </summary>
        ''' <remarks> David, 2020-09-24. </remarks>
        ''' <param name="talker"> The talker. </param>
        Public Shared Sub Identify(ByVal talker As ITraceMessageTalker)
            talker?.IdentifyTalker(IdentityTraceMessage())
        End Sub

        ''' <summary> Gets the identity. </summary>
        ''' <value> The identity. </value>
        Public Shared ReadOnly Property Identity() As String
            Get
                Return $"{AssemblyProduct} ID = {TraceEventId:X}"
            End Get
        End Property

        ''' <summary> Library log trace message. </summary>
        ''' <remarks> David, 2020-09-24. </remarks>
        ''' <returns> A TraceMessage. </returns>
        Public Shared Function LogTraceMessage() As TraceMessage
            Return New TraceMessage(TraceEventType.Information, TraceEventId, $"Log at;. {Logger.FullLogFileName}")
        End Function

        ''' <summary> Gets a message describing the identity trace. </summary>
        ''' <remarks> David, 2020-09-24. </remarks>
        ''' <returns> A TraceMessage. </returns>
        Public Shared Function IdentityTraceMessage() As TraceMessage
            Return IdentityTraceMessage(TraceEventType.Information)
        End Function

        ''' <summary> Gets a message describing the identity trace. </summary>
        ''' <remarks> David, 2020-09-24. </remarks>
        ''' <param name="eventType"> Type of the event. </param>
        ''' <returns> A TraceMessage. </returns>
        Public Shared Function IdentityTraceMessage(ByVal eventType As TraceEventType) As TraceMessage
            Return New TraceMessage(eventType, TraceEventId, Identity)
        End Function

        ''' <summary> Application information trace message. </summary>
        ''' <remarks> David, 2020-09-24. </remarks>
        ''' <returns> A TraceMessage. </returns>
        Public Shared Function ApplicationInfoTraceMessage() As TraceMessage
            Return New TraceMessage(TraceEventType.Information, TraceEventId, isr.Core.AssemblyExtensions.Methods.BuildProductTimeCaption(Application.Info))
        End Function

#End Region

#Region " UNPUBLISHED MESSAGES "

        ''' <summary> Gets or sets the unpublished identify date. </summary>
        ''' <value> The unpublished identify date. </value>
        Public Shared Property UnpublishedIdentifyDate As DateTimeOffset

        ''' <summary> Gets or sets the unpublished trace messages. </summary>
        ''' <value> The unpublished trace messages. </value>
        Public Shared ReadOnly Property UnpublishedTraceMessages As TraceMessagesQueue = New TraceMessagesQueue

        ''' <summary> Logs unpublished message. </summary>
        ''' <remarks> David, 2020-09-24. </remarks>
        ''' <param name="eventType"> Type of the event. </param>
        ''' <param name="message">   The message. </param>
        ''' <returns> A String. </returns>
        Public Shared Function LogUnpublishedMessage(ByVal eventType As TraceEventType, ByVal message As String) As String
            Return LogUnpublishedMessage(eventType, TraceEventId, message)
        End Function

        ''' <summary> Logs unpublished message. </summary>
        ''' <remarks> David, 2020-09-24. </remarks>
        ''' <param name="eventType"> Type of the event. </param>
        ''' <param name="format">    Describes the format to use. </param>
        ''' <param name="args">      A variable-length parameters list containing arguments. </param>
        ''' <returns> A String. </returns>
        Public Shared Function LogUnpublishedMessage(ByVal eventType As TraceEventType, ByVal format As String, ByVal ParamArray args() As Object) As String
            Return LogUnpublishedMessage(eventType, TraceEventId, format, args)
        End Function

        ''' <summary> Logs unpublished message. </summary>
        ''' <remarks> David, 2020-09-24. </remarks>
        ''' <param name="eventType"> Type of the event. </param>
        ''' <param name="id">        The identifier. </param>
        ''' <param name="format">    Describes the format to use. </param>
        ''' <param name="args">      A variable-length parameters list containing arguments. </param>
        ''' <returns> A String. </returns>
        Public Shared Function LogUnpublishedMessage(ByVal eventType As TraceEventType, ByVal id As Integer, ByVal format As String, ByVal ParamArray args() As Object) As String
            Return LogUnpublishedMessage(New TraceMessage(eventType, id, format, args))
        End Function

        ''' <summary> Logs unpublished message. </summary>
        ''' <remarks> David, 2020-09-24. </remarks>
        ''' <param name="message"> The message. </param>
        ''' <returns> A String. </returns>
        Public Shared Function LogUnpublishedMessage(ByVal message As TraceMessage) As String
            If message Is Nothing Then
                Return String.Empty
            Else
                If DateTimeOffset.Now.Date > UnpublishedIdentifyDate Then
                    Logger.TraceEventOverride(ApplicationInfoTraceMessage)
                    Logger.TraceEventOverride(LogTraceMessage)
                    Logger.TraceEventOverride(IdentityTraceMessage())
                    UnpublishedIdentifyDate = DateTimeOffset.Now.Date
                End If
                UnpublishedTraceMessages.Enqueue(message)
                Logger.TraceEvent(message)
                Return message.Details
            End If
        End Function

#End Region

    End Class

End Namespace


