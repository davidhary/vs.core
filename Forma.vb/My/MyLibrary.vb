<Assembly: Runtime.CompilerServices.InternalsVisibleTo(My.MyLibrary.TestAssemblyStrongName)>
Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    ''' <remarks>
    ''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 11/26/2015 </para>
    ''' </remarks>
    Public NotInheritable Class MyLibrary

        ''' <summary>
        ''' Constructor that prevents a default instance of this class from being created.
        ''' </summary>
        ''' <remarks> David, 2020-09-24. </remarks>
        Private Sub New()
            MyBase.New()
        End Sub
        ''' <summary> Identifier for the trace event. </summary>
        Public Const TraceEventId As Integer = ProjectTraceEventId.Forma
        ''' <summary> The assembly title. </summary>
        Public Const AssemblyTitle As String = "Core Forma Library"
        ''' <summary> Information describing the assembly. </summary>
        Public Const AssemblyDescription As String = "Core Forma Library"
        ''' <summary> The assembly product. </summary>
        Public Const AssemblyProduct As String = "Core.Forma"
        ''' <summary> Name of the test assembly strong. </summary>
        Public Const TestAssemblyStrongName As String = "isr.Core.FormaTests,PublicKey=" & isr.Core.Forma.My.SolutionInfo.PublicKey

    End Class

End Namespace

