## ISR Core Forma<sub>&trade;</sub>: Windows Forms Framework Class Library
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Revision History [](#){name=Revision-History}

*5.0.7280 12/07/19*  
Uses now Services, Model and Constructs created from
agnostic.

*4.0.6959 01/20/19*  
Changes project name and namespace and moves unused
code to the Relic project.

*3.4.6956 01/17/19*  
Adds splash, popup and metro forms. Removes property
invokes.

*3.4.6944 01/05/19*  
Adds binding management to base controls and forms.

*3.4.6898 11/20/18*  
Fixes compact string extensions (Measure Text no
longer alters the string argument).

*3.4.6667 04/03/18*  
2018 release.

*3.3.6439 08/18/17*  
Revamps message listeners and talkers. Defaults
listeners to Verbose level and get the talkers to filter trace level
thus allowing to control how messages are published as well as listened
to.

*3.3.6438 08/17/17*  
Adds functionality to the trace message talkers to
ensure trace levels are correctly updated.

*3.2.6436 08/15/17*  
Changes trace message tool strip drop down to a drop
down list.

*3.2.6419 06/24/17*  
Adds functionality to Enum extensions.

*3.1.6384 06/24/17*  
Defaults to UTC time.

*3.1.6370 06/10/17*  
Adds class for running tasks with dependencies.

*3.1.6354 05/25/17*  
Uses thread save queue for messages box.

*3.1.6340 05/11/17*  
Remove trim start from the default log file writer.
Fixes the default Full range.

*3.1.6332 05/03/17*  
Uses thread safe queue for logging.

*3.1.6298 03/30/17*  
Adds exception extension. Adds thread safe token.

*3.0.6093 09/04/16*  
Adds post and send event handler extensions.

*3.0.6089 09/02/16*  
Adds base and coding exceptions.

*3.0.5934 03/31/16*  
Adds configuration editor form.

*3.0.5866 01/23/16*  
Updates to .NET 4.6.1

*2.1.5828 12/16/15 Moves trace and log from the Diagnosis class.

*2.1.5822 12/10/15 Created from Core Publishers.

*2.1.5163 02/19/14*  
Uses local sync and asynchronous safe event handlers.

*2.0.5126 01/13/14*  
Tagged as 2014.

*2.0.5024 09/04/13*  
Simplified Publisher interface and base class.

*2.0.4955 09/04/13*  
Created based on the legacy core structures.

*1.2.4654 09/28/12*  
Adds Property Changed Publisher base. Validates name
before invoking property changed notification. Validates values of
action results. Uses base values when adding action results.

\(C\) 2012 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Core Library](https://bitbucket.org/davidhary/vs.core)\
Safe events: ([Code Project](http://www.CodeProject.com/KB/cs/EventSafeTrigger.aspx)
[dream in code](http://www.DreamInCode.net/forums/user/334492-aeonhack/)
[dream in code](http://www.DreamInCode.net/code/snippet5016.htm))  
[Drop Shadow and Fade From](http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx)  
[Engineering Format](http://WallaceKelly.BlogSpot.com/)  
[Efficient Strong Enumerations](http://www.CodeProject.com/KB/cs/efficient_strong_enum.aspx)  
[Enumeration Extensions](http://www.CodeProject.com/Articles/37921/Enums-Flags-and-CSharp-Oh-my-bad-pun.aspx)  
[Exception Extension](https://www.codeproject.com/Tips/1179564/A-Quick-Dirty-Extension-Method-to-Get-the-Full-Exc)  
[String Enumerator](http://www.codeproject.com/Articles/17472/StringEnumerator)  
[Drop Shadow and Fade From](http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx)  
[Notification
Window](http://www.codeproject.com/KB/dialog/notificationwindow.aspx)  
[Office Style Splash
Screen](http://www.codeproject.com/Articles/804316/Office-Style-Splash-Screen)
