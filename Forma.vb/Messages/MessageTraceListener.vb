﻿Imports System.Runtime.CompilerServices

''' <summary> A message trace listener. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 3/24/2016 </para>
''' </remarks>
Public Class MessageTraceListener
    Inherits TraceListener

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Creates a new MessageTraceListener. </summary>
    ''' <remarks> This helps implement CA2000. </remarks>
    ''' <returns> A list of. </returns>
    Public Shared Function Create() As MessageTraceListener
        Dim result As MessageTraceListener = Nothing
        Try
            result = New MessageTraceListener
        Catch
            result?.Dispose()
            Throw
        End Try
        Return result
    End Function

    ''' <summary> Gets a value indicating whether the trace listener is thread safe. </summary>
    ''' <value>
    ''' true if the trace listener is thread safe; otherwise, false. The default is false.
    ''' </value>
    Public Overrides ReadOnly Property IsThreadSafe As Boolean
        Get
            Return Me.Listener IsNot Nothing AndAlso Me.Listener.IsThreadSafe
        End Get
    End Property

    ''' <summary> Gets the listener. </summary>
    ''' <value> The listener. </value>
    Public Property Listener As IMessageListener

    ''' <summary>
    ''' When overridden in a derived class, writes the specified message to the listener you create
    ''' in the derived class.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="message"> A message to write. </param>
    Public Overrides Sub Write(message As String)
        ' this outputs an additional message.
        Me.Listener?.Write(message)
    End Sub

    ''' <summary>
    ''' When overridden in a derived class, writes a message to the listener you create in the
    ''' derived class, followed by a line terminator.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="message"> A message to write. </param>
    Public Overrides Sub WriteLine(message As String)
        Me.Listener?.WriteLine(message)
    End Sub

    ''' <summary>
    ''' Writes trace information, a data object and event information to the listener specific output.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="eventCache"> A <see cref="T:System.Diagnostics.TraceEventCache" /> object that
    '''                           contains the current process ID, thread ID, and stack trace
    '''                           information. </param>
    ''' <param name="source">     A name used to identify the output, typically the name of the
    '''                           application that generated the trace event. </param>
    ''' <param name="eventType">  One of the <see cref="T:System.Diagnostics.TraceEventType" /> values
    '''                           specifying the type of event that has caused the trace. </param>
    ''' <param name="id">         A numeric identifier for the event. </param>
    ''' <param name="data">       The trace data to emit. </param>
    Public Overrides Sub TraceData(eventCache As TraceEventCache, source As String, eventType As TraceEventType, id As Integer, data As Object)
        If Me.Listener Is Nothing OrElse Me.Listener.TraceLevel >= eventType Then
            MyBase.TraceData(eventCache, source, eventType, id, data)
            Me.Listener.Register(eventType)
        End If
    End Sub

    ''' <summary>
    ''' Writes trace information, a message, and event information to the listener specific output.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="eventCache"> A <see cref="T:System.Diagnostics.TraceEventCache" /> object that
    '''                           contains the current process ID, thread ID, and stack trace
    '''                           information. </param>
    ''' <param name="source">     A name used to identify the output, typically the name of the
    '''                           application that generated the trace event. </param>
    ''' <param name="eventType">  One of the <see cref="T:System.Diagnostics.TraceEventType" /> values
    '''                           specifying the type of event that has caused the trace. </param>
    ''' <param name="id">         A numeric identifier for the event. </param>
    ''' <param name="message">    A message to write. </param>
    Public Overrides Sub TraceEvent(eventCache As TraceEventCache, source As String, eventType As TraceEventType, id As Integer, message As String)
        If Me.Listener IsNot Nothing AndAlso Me.Listener.TraceLevel >= eventType Then
            MyBase.TraceEvent(eventCache, source, eventType, id, message)
            Me.Listener.Register(eventType)
        End If
    End Sub

    ''' <summary>
    ''' Writes trace information, a formatted array of objects and event information to the listener
    ''' specific output.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="eventCache"> A <see cref="T:System.Diagnostics.TraceEventCache" /> object that
    '''                           contains the current process ID, thread ID, and stack trace
    '''                           information. </param>
    ''' <param name="source">     A name used to identify the output, typically the name of the
    '''                           application that generated the trace event. </param>
    ''' <param name="eventType">  One of the <see cref="T:System.Diagnostics.TraceEventType" /> values
    '''                           specifying the type of event that has caused the trace. </param>
    ''' <param name="id">         A numeric identifier for the event. </param>
    ''' <param name="format">     The format of the message to write. </param>
    ''' <param name="args">       An object array containing zero or more objects to format. </param>
    Public Overrides Sub TraceEvent(eventCache As TraceEventCache, source As String, eventType As TraceEventType,
                                    id As Integer, format As String, ParamArray args As Object())
        If Me.Listener IsNot Nothing AndAlso Me.Listener.TraceLevel >= eventType Then
            MyBase.TraceEvent(eventCache, source, eventType, id, format, args)
            Me.Listener.Register(eventType)
        End If
    End Sub

End Class

''' <summary> A message trace listener. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 3/24/2016 </para>
''' </remarks>
Public Class MessageTraceSourceListener
    Inherits TraceListener

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Creates a new MessageTraceListener. </summary>
    ''' <remarks> This helps implement CA2000. </remarks>
    ''' <returns> A list of. </returns>
    Public Shared Function Create() As MessageTraceSourceListener
        Dim result As MessageTraceSourceListener = Nothing
        Try
            result = New MessageTraceSourceListener
        Catch
            result?.Dispose()
            Throw
        End Try
        Return result
    End Function

    ''' <summary> Gets a value indicating whether the trace listener is thread safe. </summary>
    ''' <value>
    ''' true if the trace listener is thread safe; otherwise, false. The default is false.
    ''' </value>
    Public Overrides ReadOnly Property IsThreadSafe As Boolean
        Get
            Return Me.Listener IsNot Nothing AndAlso Me.Listener.IsThreadSafe
        End Get
    End Property

    ''' <summary> Gets or sets the listener. </summary>
    ''' <value> The listener. </value>
    Public Property Listener As IMessageListener

    ''' <summary>
    ''' When overridden in a derived class, writes the specified message to the listener you create
    ''' in the derived class.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="message"> A message to write. </param>
    Public Overrides Sub Write(message As String)
        ' this outputs an additional message.
        ' Me.Listener?.Write(message)
    End Sub

    ''' <summary>
    ''' When overridden in a derived class, writes a message to the listener you create in the
    ''' derived class, followed by a line terminator.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="message"> A message to write. </param>
    Public Overrides Sub WriteLine(message As String)
        Me.Listener?.WriteLine(message)
    End Sub

    ''' <summary>
    ''' Writes trace information, a data object and event information to the listener specific output.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="eventCache"> A <see cref="T:System.Diagnostics.TraceEventCache" /> object that
    '''                           contains the current process ID, thread ID, and stack trace
    '''                           information. </param>
    ''' <param name="source">     A name used to identify the output, typically the name of the
    '''                           application that generated the trace event. </param>
    ''' <param name="eventType">  One of the <see cref="T:System.Diagnostics.TraceEventType" /> values
    '''                           specifying the type of event that has caused the trace. </param>
    ''' <param name="id">         A numeric identifier for the event. </param>
    ''' <param name="data">       The trace data to emit. </param>
    Public Overrides Sub TraceData(eventCache As TraceEventCache, source As String, eventType As TraceEventType, id As Integer, data As Object)
        If Me.Listener Is Nothing OrElse Me.Listener.TraceLevel >= eventType Then
            MyBase.TraceData(eventCache, source, eventType, id, data)
            Me.Listener.Register(eventType)
        End If
    End Sub

    ''' <summary>
    ''' Writes trace information, a message, and event information to the listener specific output.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="eventCache"> A <see cref="T:System.Diagnostics.TraceEventCache" /> object that
    '''                           contains the current process ID, thread ID, and stack trace
    '''                           information. </param>
    ''' <param name="source">     A name used to identify the output, typically the name of the
    '''                           application that generated the trace event. </param>
    ''' <param name="eventType">  One of the <see cref="T:System.Diagnostics.TraceEventType" /> values
    '''                           specifying the type of event that has caused the trace. </param>
    ''' <param name="id">         A numeric identifier for the event. </param>
    ''' <param name="message">    A message to write. </param>
    Public Overrides Sub TraceEvent(eventCache As TraceEventCache, source As String, eventType As TraceEventType, id As Integer, message As String)
        If Me.Listener IsNot Nothing AndAlso Me.Listener.TraceLevel >= eventType Then
            MyBase.TraceEvent(eventCache, source, eventType, id, message)
            Me.Listener.Register(eventType)
        End If
    End Sub

    ''' <summary>
    ''' Writes trace information, a formatted array of objects and event information to the listener
    ''' specific output.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="eventCache"> A <see cref="T:System.Diagnostics.TraceEventCache" /> object that
    '''                           contains the current process ID, thread ID, and stack trace
    '''                           information. </param>
    ''' <param name="source">     A name used to identify the output, typically the name of the
    '''                           application that generated the trace event. </param>
    ''' <param name="eventType">  One of the <see cref="T:System.Diagnostics.TraceEventType" /> values
    '''                           specifying the type of event that has caused the trace. </param>
    ''' <param name="id">         A numeric identifier for the event. </param>
    ''' <param name="format">     The format of the message to write. </param>
    ''' <param name="args">       An object array containing zero or more objects to format. </param>
    Public Overrides Sub TraceEvent(eventCache As TraceEventCache, source As String, eventType As TraceEventType,
                                    id As Integer, format As String, ParamArray args As Object())
        If Me.Listener IsNot Nothing AndAlso Me.Listener.TraceLevel >= eventType Then
            MyBase.TraceEvent(eventCache, source, eventType, id, format, args)
            Me.Listener.Register(eventType)
        End If
    End Sub

End Class

Public Module TraceMethods

#Region " PUBLISH "

    ''' <summary> Publishes and logs the message. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="source">    Source for the. </param>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <param name="id">        The identifier to use with the trace event. </param>
    ''' <param name="format">    Describes the format to use. </param>
    ''' <param name="args">      A variable-length parameters list containing arguments. </param>
    ''' <returns> A String. </returns>
    <Extension>
    Public Function Publish(ByVal source As TraceSource, ByVal eventType As TraceEventType, ByVal id As Integer,
                            ByVal format As String, ByVal ParamArray args() As Object) As String
        Return Publish(source, New TraceMessage(eventType, id, format, args))
    End Function

    ''' <summary> Adds a message to the display. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="source"> Source for the. </param>
    ''' <param name="value">  The value. </param>
    ''' <returns> A String. </returns>
    <Extension>
    Public Function Publish(source As TraceSource, ByVal value As TraceMessage) As String
        Dim result As String = String.Empty
        If value Is Nothing OrElse String.IsNullOrWhiteSpace(value.Details) Then
            Debug.Assert(Not Debugger.IsAttached, "Empty trace message @",
                         New StackTrace(True).ToString.Split(Environment.NewLine.ToCharArray())(0))
        Else
            result = value.ToString
            source?.TraceEvent(value.EventType, value.Id, result)
        End If
        Return result
    End Function

    ''' <summary> Publishes and logs the message. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <param name="id">        The identifier to use with the trace event. </param>
    ''' <param name="format">    Describes the format to use. </param>
    ''' <param name="args">      A variable-length parameters list containing arguments. </param>
    ''' <returns> A String. </returns>
    Public Function Publish(ByVal eventType As TraceEventType, ByVal id As Integer,
                            ByVal format As String, ByVal ParamArray args() As Object) As String
        Return Publish(New TraceMessage(eventType, id, format, args))
    End Function

    ''' <summary> Adds a message to the display. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> A String. </returns>
    Public Function Publish(ByVal value As TraceMessage) As String
        Dim result As String = String.Empty
        If value Is Nothing OrElse String.IsNullOrWhiteSpace(value.Details) Then
            Debug.Assert(Not Debugger.IsAttached, "Empty trace message @",
                         New StackTrace(True).ToString.Split(Environment.NewLine.ToCharArray())(0))
        Else
            result = value.ToString
            Trace.Write(result)
            If value.EventType <= TraceEventType.Error Then
                Trace.TraceError("")
            ElseIf value.EventType <= TraceEventType.Warning Then
                Trace.TraceWarning("")
            Else
                Trace.TraceInformation("")
            End If
        End If
        Return result
    End Function

    ''' <summary> Adds a message to the display. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> A String. </returns>
    Public Function Publish(ByVal value As String) As String
        Trace.Write(value)
        Return value
    End Function

#End Region

End Module