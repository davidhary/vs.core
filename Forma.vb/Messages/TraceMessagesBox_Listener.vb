
Partial Public Class TraceMessagesBox
    Implements ITraceMessageListener

#Region " I TRACE MESSAGE LISTENER "

    ''' <summary> Trace event overriding the trace level. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The event message. </param>
    ''' <returns> A String. </returns>
    Public Function TraceEventOverride(value As TraceMessage) As String Implements ITraceMessageListener.TraceEventOverride
        If value Is Nothing Then
            Return String.Empty
        Else
            Me.AddMessage(value)
            Windows.Forms.Application.DoEvents()
            Return value.Details
        End If
    End Function

    ''' <summary> Writes a trace event to the trace listener overriding the trace level. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="id">        The identifier. </param>
    ''' <param name="format">    The message format. </param>
    ''' <param name="args">      Specified the message arguments. </param>
    ''' <returns> The trace message details. </returns>
    Public Function TraceEventOverride(eventType As TraceEventType, id As Integer, format As String, ParamArray args() As Object) As String Implements ITraceMessageListener.TraceEventOverride
        Return Me.TraceEventOverride(New TraceMessage(eventType, id, format, args))
    End Function

    ''' <summary> Writes a trace event to the trace listeners. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The event message. </param>
    ''' <returns> The trace message details. </returns>
    Public Function TraceEvent(value As TraceMessage) As String Implements ITraceMessageListener.TraceEvent
        If value Is Nothing Then
            Return String.Empty
        Else
            If Me.ShouldTrace(value.EventType) Then Me.AddMessage(value)
            Windows.Forms.Application.DoEvents()
            Return value.Details
        End If
    End Function

    ''' <summary> Writes a trace event to the trace listeners. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="id">        The identifier. </param>
    ''' <param name="format">    The message format. </param>
    ''' <param name="args">      Specified the message arguments. </param>
    ''' <returns> The trace message details. </returns>
    Public Function TraceEvent(eventType As TraceEventType, id As Integer, format As String, ParamArray args() As Object) As String Implements ITraceMessageListener.TraceEvent
        Return Me.TraceEvent(New TraceMessage(eventType, id, format, args))
    End Function

#End Region

End Class
