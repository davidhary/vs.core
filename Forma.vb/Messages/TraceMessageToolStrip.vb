Imports System.Drawing
Imports System.Windows.Forms

''' <summary> A trace message tool strip. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 3/21/2017 </para>
''' </remarks>
Public Class TraceMessageToolStrip
    Inherits Windows.Forms.ToolStrip

#Region " CONSTRUCTION "

    ''' <summary> Gets or sets the initializing component. </summary>
    ''' <value> The initializing component. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private Property InitializingComponent As Boolean

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.Windows.Forms.ToolStrip" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        MyBase.New
        Me.InitializingComponent = True
        Me.InitializeComponent()
        Me.InitializingComponent = False
        Me.Renderer = New BackgroundRenderer
        Me.TraceMessagesStack = New TraceMessagesStack()
        Me.ApplyTraceEvent(TraceEventType.Start)
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.ToolStrip" />
    ''' and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="disposing"> <see langword="true" /> to release both managed and unmanaged
    '''                          resources; <see langword="false" /> to release only unmanaged
    '''                          resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me._Components IsNot Nothing Then Me._Components.Dispose() : Me._Components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

        Private WithEvents DetailsComboBox As isr.Core.Forma.ToolStripSpringComboBox

        Private WithEvents MessageLevelSplitButton As Windows.Forms.ToolStripSplitButton

        Private WithEvents DropLastMessageMenuItem As Windows.Forms.ToolStripMenuItem

        Private WithEvents ClearHistoryMenuItem As Windows.Forms.ToolStripMenuItem

    ''' <summary> The components. </summary>
    Private _Components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me._Components = New System.ComponentModel.Container()
        Me.MessageLevelSplitButton = New System.Windows.Forms.ToolStripSplitButton()
        Me.DropLastMessageMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClearHistoryMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DetailsComboBox = New isr.Core.Forma.ToolStripSpringComboBox
        Me.SuspendLayout()
        '
        '_DetailsComboBox
        '
        Me.DetailsComboBox.Name = "_DetailsComboBox"
        Me.DetailsComboBox.DropDownStyle = ComboBoxStyle.DropDownList
        Me.DetailsComboBox.Size = New System.Drawing.Size(121, 29)
        Me.DetailsComboBox.ToolTipText = "Message details"
        Me.DetailsComboBox.AutoSize = True
        '
        '_MessageLevelSplitButton
        '
        Me.MessageLevelSplitButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.MessageLevelSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.MessageLevelSplitButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DropLastMessageMenuItem, Me.ClearHistoryMenuItem})
        Me.MessageLevelSplitButton.Image = Global.isr.Core.Forma.My.Resources.Resources.Empty
        Me.MessageLevelSplitButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.MessageLevelSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.MessageLevelSplitButton.Name = "_MessageLevelSplitButton"
        Me.MessageLevelSplitButton.Size = New System.Drawing.Size(38, 26)
        Me.MessageLevelSplitButton.AutoSize = False
        Me.MessageLevelSplitButton.ToolTipText = "Message level; Clear menu"
        '
        '_DropLastMessageMenuItem
        '
        Me.DropLastMessageMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.DropLastMessageMenuItem.Image = Global.isr.Core.Forma.My.Resources.Resources.Clear
        Me.DropLastMessageMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.DropLastMessageMenuItem.Name = "_DropLastMessageMenuItem"
        Me.DropLastMessageMenuItem.Size = New System.Drawing.Size(158, 28)
        Me.DropLastMessageMenuItem.ToolTipText = "Drops last message"
        '
        '_ClearHistoryMenuItem
        '
        Me.ClearHistoryMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ClearHistoryMenuItem.Image = Global.isr.Core.Forma.My.Resources.Resources.ClearHistory
        Me.ClearHistoryMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ClearHistoryMenuItem.Name = "_ClearHistoryMenuItem"
        Me.ClearHistoryMenuItem.Size = New System.Drawing.Size(158, 28)
        Me.ClearHistoryMenuItem.ToolTipText = "Clear History"
        '
        'TraceMessageToolStrip
        '
        Me.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.Stretch = True
        Me.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DetailsComboBox, Me.MessageLevelSplitButton})
        Me.Size = New System.Drawing.Size(799, 29)
        Me.Text = "Trace Message Strip"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

#Region " BACKGROUND RENDERER "

    ''' <summary> A background renderer. </summary>
    ''' <remarks>
    ''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 5/5/2017 </para>
    ''' </remarks>
    Private Class BackgroundRenderer
        Inherits ToolStripProfessionalRenderer

        ''' <summary>
        ''' Raises the <see cref="E:System.Windows.Forms.ToolStripRenderer.RenderLabelBackground" />
        ''' event.
        ''' </summary>
        ''' <remarks> David, 2020-09-24. </remarks>
        ''' <param name="e"> A <see cref="T:System.Windows.Forms.ToolStripItemRenderEventArgs" /> that
        '''                  contains the event data. </param>
        Protected Overrides Sub OnRenderLabelBackground(ByVal e As ToolStripItemRenderEventArgs)
            If e IsNot Nothing AndAlso (e.Item.BackColor <> System.Drawing.SystemColors.ControlDark) Then
                Using brush As New SolidBrush(e.Item.BackColor)
                    e.Graphics.FillRectangle(brush, e.Item.ContentRectangle)
                End Using
            End If
        End Sub
    End Class

#End Region

#Region " STACK "

    ''' <summary> Trace messages stack message pushed. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub TraceMessagesStack_MessagePushed(sender As Object, e As EventArgs) Handles _TraceMessagesStack.MessagePushed
        Me.OnStackChanged(e)
    End Sub

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _TraceMessagesStack As TraceMessagesStack
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets or sets a stack of trace messages. </summary>
    ''' <value> A Stack of trace messages. </value>
    Public Property TraceMessagesStack As TraceMessagesStack
        Get
            Return Me._TraceMessagesStack
        End Get
        Set(value As TraceMessagesStack)
            Me._TraceMessagesStack = value
            Me.OnStackChanged(EventArgs.Empty)
        End Set
    End Property

    ''' <summary> Returns the top-of-stack object without removing it. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns> The current top-of-stack object. </returns>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Function Peek() As TraceMessage
        Return If(Me.TraceMessagesStack.Any, Me.TraceMessagesStack.TryPeek(), TraceMessage.Empty)
    End Function

    ''' <summary> Removes and returns the top-of-stack object. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub Pop()
        If Me.TraceMessagesStack.Any Then
            Me.TraceMessagesStack.TryPop()
            Me.OnStackChanged(System.EventArgs.Empty)
        End If
    End Sub

    ''' <summary> Pushes an object onto this stack. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="message"> The message to push. </param>
    Public Sub Push(ByVal message As TraceMessage)
        Me.TraceMessagesStack.Push(message)
        Me.OnStackChanged(System.EventArgs.Empty)
    End Sub

    ''' <summary> Pushes an object onto this stack. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="message">   The message to push. </param>
    Public Sub Push(ByVal eventType As TraceEventType, ByVal message As String)
        Me.Push(New TraceMessage(eventType, 0, message))
    End Sub

#End Region

#Region " DISPLAY "

    ''' <summary> Type of the last event. </summary>
    Private _LastEventType As TraceEventType

    ''' <summary> Applies the trace event described by value. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub ApplyTraceEvent(value As TraceEventType)
        Me._LastEventType = value
        Select Case value
            Case Is <= TraceEventType.Error
                Me.MessageLevelSplitButton.Image = My.Resources.ErrorLevel
            Case Is <= TraceEventType.Warning
                Me.MessageLevelSplitButton.Image = My.Resources.WarningLevel
            Case Is <= TraceEventType.Information
                Me.MessageLevelSplitButton.Image = My.Resources.InformationLevel
            Case Is <= TraceEventType.Verbose
                Me.MessageLevelSplitButton.Image = My.Resources.VerboseLevel
            Case Else
                Me.MessageLevelSplitButton.Image = My.Resources.Empty
        End Select
        Me.MessageLevelSplitButton.Invalidate()
        Windows.Forms.Application.DoEvents()
    End Sub

    ''' <summary> Gets or sets the type of the last event. </summary>
    ''' <value> The type of the last event. </value>
    <System.ComponentModel.Browsable(False),
        System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)>
    Private Property LastEventType As TraceEventType
        Get
            Return Me._LastEventType
        End Get
        Set(value As TraceEventType)
            If value <> Me.LastEventType Then '
                Me.ApplyTraceEvent(value)
            End If
        End Set
    End Property

    ''' <summary> Displays the given message. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="message"> The message to push. </param>
    Private Sub Display(ByVal message As TraceMessage)
        Me.DetailsComboBox.Text = message.Details
        Me.DetailsComboBox.Invalidate()
        Me.LastEventType = message.EventType
    End Sub

    ''' <summary> Applies the back color described by color. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="color"> The color. </param>
    Public Sub ApplyBackColor(ByVal color As Color)
        Me.DetailsComboBox.BackColor = color
    End Sub

#End Region

#Region " CLEAR "

    ''' <summary> Handles the stack changed event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Sub OnStackChanged(ByVal e As EventArgs)
        If e Is Nothing Then Return
        If Me.TraceMessagesStack.Any Then
            Me.DetailsComboBox.ComboBox.DataSource = Me.TraceMessagesStack.TraceMessages
            Me.Display(Me.TraceMessagesStack.TryPeek)
        Else
            Me.LastEventType = TraceEventType.Start
            Me.DetailsComboBox.ComboBox.DataSource = Nothing
            Me.DetailsComboBox.ComboBox.Items.Clear()
            Me.DetailsComboBox.Text = String.Empty
        End If
        Me.DetailsComboBox.Invalidate()
    End Sub

    ''' <summary> Drops the last message. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub DropLastMessage()
        Me.Pop()
    End Sub

    ''' <summary> Clears the history. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub ClearHistory()
        Me.TraceMessagesStack.Clear()
        Me.OnStackChanged(System.EventArgs.Empty)
    End Sub

    ''' <summary> Clears the history menu item click. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ClearHistoryMenuItem_Click(sender As Object, ByVal e As EventArgs) Handles ClearHistoryMenuItem.Click
        Me.ClearHistory()
    End Sub

    ''' <summary> Drop last message menu item click. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub DropLastMessageMenuItem_Click(sender As Object, e As EventArgs) Handles DropLastMessageMenuItem.Click
        Me.DropLastMessage()
    End Sub



#End Region

End Class
