Imports System.ComponentModel
Imports System.Windows.Forms

''' <summary> A message logging text box. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 09/04/2013, 1.2.4955. based on the legacy messages box. </para>
''' </remarks>
<System.Diagnostics.DebuggerDisplay("id={UniqueId}")>
<System.ComponentModel.Description("Trace Messages Text Box")>
Public Class TraceMessagesBox
    Inherits MessagesBox

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructor for this class. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        MyBase.New()
        Me.InitializeComponent()
        Me._TraceMessageFormat = TraceMessage.DefaultTraceMessageFormat
        Me._AlertLevel = TraceEventType.Warning
        Me._AlertAnnunciatorEvent = TraceEventType.Verbose
        Me._AlertSoundEvent = TraceEventType.Verbose
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            ' Invoke the base class dispose method        
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " DISPLAY AND MESSAGE SENTINELS "

    ''' <summary> Suspend updates and release indicator controls. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Overrides Sub SuspendUpdatesReleaseIndicators()
        MyBase.SuspendUpdatesReleaseIndicators()
        Me.AlertsToggleTreeNode = Nothing
        Me.AlertsToggleControl = Nothing
    End Sub

    ''' <summary> The alert level. </summary>
    Private _AlertLevel As TraceEventType

    ''' <summary>
    ''' Gets or sets the alert level. Message <see cref="TraceEventType">levels</see> equal or lower
    ''' than this are tagged as alerts and set the <see cref="AlertAnnunciatorEvent">alert
    ''' sentinel</see>.
    ''' </summary>
    ''' <value> The alert level. </value>
    <Category("Appearance"), Description("Level for notifying of alerts"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue("TraceEventType.Warning")>
    Public Property AlertLevel As TraceEventType
        Get
            Return Me._AlertLevel
        End Get
        Set(value As TraceEventType)
            If value <> Me.AlertLevel Then
                Me._AlertLevel = value
                Me.SyncNotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the sentinel indicating that alert messages need to be announced. </summary>
    ''' <value> The alerts announced. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public ReadOnly Property AlertsAnnounced As Boolean
        Get
            Return Me.AlertAnnunciatorEvent <= Me.AlertLevel
        End Get
    End Property

    ''' <summary> The alert annunciator event. </summary>
    Private _AlertAnnunciatorEvent As TraceEventType

    ''' <summary>
    ''' Gets or sets or set the trace event type which toggles the alert annunciator.
    ''' </summary>
    ''' <value> The alert annunciator event. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property AlertAnnunciatorEvent As TraceEventType
        Get
            Return Me._AlertAnnunciatorEvent
        End Get
        Set(value As TraceEventType)
            If value <> Me.AlertAnnunciatorEvent Then
                Me._AlertAnnunciatorEvent = value
                Me.SyncNotifyPropertyChanged()
                Me.UpdateAlertToggleControl(Me.AlertsToggleControl, value)
                Me.UpdateAlertToggleTreeNode(Me.AlertsToggleTreeNode, value)
            End If
        End Set
    End Property

    ''' <summary> Gets the sentinel indicating that alert messages need to be voiced. </summary>
    ''' <value> The alerts voiced. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public ReadOnly Property AlertsVoiced As Boolean
        Get
            Return Me.AlertAnnunciatorEvent <= Me.AlertLevel
        End Get
    End Property

    ''' <summary> The alert sound event. </summary>
    Private _AlertSoundEvent As TraceEventType

    ''' <summary> Gets or sets the highest alert event. </summary>
    ''' <value> The highest alert event. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property AlertSoundEvent As TraceEventType
        Get
            Return Me._AlertSoundEvent
        End Get
        Set(value As TraceEventType)
            If value <> Me.AlertSoundEvent Then
                Me._AlertSoundEvent = value
                Me.SyncNotifyPropertyChanged()
                Me.PlayAlertIf(Me.AlertsVoiced, value)
            End If
        End Set
    End Property

    ''' <summary> Updates the alerts described by level. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="level"> The level. </param>
    Protected Sub UpdateAlerts(ByVal level As TraceEventType)
        If level <= Me.AlertLevel Then
            ' Updates the cached alert level if the level represents a higher alert level.
            If level < Me.AlertAnnunciatorEvent Then Me.AlertAnnunciatorEvent = level
            If level < Me.AlertSoundEvent Then Me.AlertSoundEvent = level
        End If
        ' Me._AlertsAdded = Not Me.UserVisible AndAlso (Me.AlertsAdded OrElse (level <= Me.AlertLevel))
        ' 2016 moved to highest alert level: If Not Me.UserVisible AndAlso Me.AlertsAdded Then Me.PlayAlert(level)
    End Sub

    ''' <summary> Displays the available lines and clears the alerts and message sentinels. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Overrides Sub Display()
        ' turn off the alert annunciator when displaying the messages onto a visible screen.
        ' the sound alert level is kept unchanged. 
        If Me.UserVisible Then Me.AlertAnnunciatorEvent = TraceEventType.Verbose
        MyBase.Display()
    End Sub

    ''' <summary> Executes the clear action. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Protected Overrides Sub OnClear()
        Me.AlertAnnunciatorEvent = TraceEventType.Verbose
        Me.AlertSoundEvent = TraceEventType.Verbose
        MyBase.OnClear()
    End Sub

    ''' <summary> Gets or sets the color of the alert toggle control back. </summary>
    ''' <value> The color of the alert toggle control back. </value>
    Private ReadOnly Property AlertToggleControlBackColor As Drawing.Color = Drawing.SystemColors.Control

    ''' <summary> Gets or sets the alert toggle control text. </summary>
    ''' <value> The alert toggle control text. </value>
    Private ReadOnly Property AlertToggleControlText As String

    ''' <summary> The alerts toggle tree node. </summary>
    Private _AlertsToggleTreeNode As TreeNode

    ''' <summary> Gets or sets the alerts toggle TreeNode. </summary>
    ''' <value> The alerts toggle TreeNode. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property AlertsToggleTreeNode As TreeNode
        Get
            Return Me._AlertsToggleTreeNode
        End Get
        Set(value As TreeNode)
            Me._AlertsToggleTreeNode = value
            If value IsNot Nothing Then
                Me._AlertToggleControlBackColor = value.BackColor
                Me._AlertToggleControlText = value.Text
            End If
        End Set
    End Property

    ''' <summary> Safe visible setter. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="treeNode">   The TreeNode. </param>
    ''' <param name="alertEvent"> The alert event. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub UpdateAlertToggleTreeNode(ByVal treeNode As TreeNode, ByVal alertEvent As TraceEventType)
        Try
            If treeNode IsNot Nothing Then
                If treeNode.TreeView.InvokeRequired Then
                    treeNode.TreeView.Invoke(New Action(Of TreeNode, TraceEventType)(AddressOf Me.UpdateAlertToggleTreeNode), New Object() {treeNode, alertEvent})
                Else
                    If alertEvent <= Me.AlertLevel Then
                        treeNode.Text = alertEvent.ToString
                        Select Case alertEvent
                            Case <= TraceEventType.Error
                                treeNode.BackColor = Drawing.Color.Red
                            Case <= TraceEventType.Warning
                                treeNode.BackColor = Drawing.Color.Orange
                            Case Else
                                treeNode.BackColor = Drawing.Color.LightBlue
                        End Select
                    Else
                        treeNode.BackColor = Me.AlertToggleControlBackColor
                        treeNode.Text = Me.AlertToggleControlText
                    End If
                End If
            End If
        Catch
        End Try
    End Sub

    ''' <summary> The alerts toggle control. </summary>
    Private _AlertsToggleControl As Control

    ''' <summary> Gets or sets the alerts toggle control. </summary>
    ''' <value> The alerts toggle control. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property AlertsToggleControl As Control
        Get
            Return Me._AlertsToggleControl
        End Get
        Set(value As Control)
            Me._AlertsToggleControl = value
            If value IsNot Nothing Then
                Me._AlertToggleControlBackColor = value.BackColor
                Me._AlertToggleControlText = value.Text
            End If
        End Set
    End Property

    ''' <summary> Safe visible setter. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="control">    The control. </param>
    ''' <param name="alertEvent"> The alert event. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub UpdateAlertToggleControl(ByVal control As Control, ByVal alertEvent As TraceEventType)
        Try
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of Control, TraceEventType)(AddressOf Me.UpdateAlertToggleControl), New Object() {control, alertEvent})
                Else
                    If alertEvent <= Me.AlertLevel Then
                        control.Text = alertEvent.ToString
                        Select Case alertEvent
                            Case <= TraceEventType.Error
                                control.BackColor = Drawing.Color.Red
                            Case <= TraceEventType.Warning
                                control.BackColor = Drawing.Color.Orange
                            Case Else
                                control.BackColor = Drawing.Color.LightBlue
                        End Select
                        If Not TypeOf control Is TabPage Then control.Visible = True
                    Else
                        control.BackColor = Me.AlertToggleControlBackColor
                        control.Text = Me.AlertToggleControlText
                        If Not TypeOf control Is TabPage Then control.Visible = False
                    End If
                    control.Invalidate()
                End If
            End If
        Catch
        End Try
    End Sub

    ''' <summary> Gets or sets the alert sound enabled. </summary>
    ''' <value> The alert sound enabled. </value>
    <Category("Appearance"), Description("Enables playing alert sounds"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue(False)>
    Public Property AlertSoundEnabled As Boolean

    ''' <summary> Play alert if. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="condition"> true to condition. </param>
    ''' <param name="level">     The level. </param>
    Public Sub PlayAlertIf(ByVal condition As Boolean, ByVal level As TraceEventType)
        If condition AndAlso Me.AlertSoundEnabled Then TraceMessagesBox.PlayAlert(level)
    End Sub

    ''' <summary> Play alert. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="level"> The level. </param>
    Public Shared Sub PlayAlert(ByVal level As TraceEventType)
        If level = TraceEventType.Critical Then
            My.Computer.Audio.PlaySystemSound(Media.SystemSounds.Exclamation)
        ElseIf level = TraceEventType.Error Then
            My.Computer.Audio.PlaySystemSound(Media.SystemSounds.Exclamation)
        ElseIf level = TraceEventType.Warning Then
            My.Computer.Audio.PlaySystemSound(Media.SystemSounds.Asterisk)
        End If
    End Sub

#End Region

#Region " ADD MESSAGE "

    ''' <summary> Prepends or appends a new value to the messages box. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> A String. </returns>
    Public Overrides Function AddMessage(ByVal value As String) As String
        If String.IsNullOrWhiteSpace(value) Then
            Debug.Assert(Not Debugger.IsAttached, "Empty trace message @",
                         New StackTrace(True).ToString.Split(Environment.NewLine.ToCharArray())(0))
            value = String.Empty
        Else
            MyBase.AddMessage(value)
        End If
        Return value
    End Function

    ''' <summary> Adds a message to the display. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The value. </param>
    Public Overloads Sub AddMessage(ByVal value As TraceMessage)
        If value Is Nothing OrElse String.IsNullOrWhiteSpace(value.Details) Then
            Debug.Assert(Not Debugger.IsAttached, "Empty trace message @",
                         New StackTrace(True).ToString.Split(Environment.NewLine.ToCharArray())(0))
        Else
            ' sustains the alerts flag until cleared.
            Me.UpdateAlerts(value.EventType)
            Me.AddMessage(value.ToString(Me.TraceMessageFormat))
            Me.PublishSynopsis(value)
        End If
    End Sub

    ''' <summary> Gets or sets the default format for displaying the message. </summary>
    ''' <remarks>
    ''' The format must include 4 elements to display the first two characters of the trace event
    ''' type, the id, timestamp and message details. For example,<code>
    ''' "{0}, {1:X}, {2:HH:mm:ss.fff}, {3}"</code>.
    ''' </remarks>
    ''' <value> The trace message format. </value>
    <Category("Appearance"), Description("Formats the test message"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue(TraceMessage.DefaultTraceMessageFormat)>
    Public Property TraceMessageFormat As String

#End Region

#Region " STATUS PUBLISHER "

    ''' <summary> The status prompt. </summary>
    Private _StatusPrompt As String

    ''' <summary> Gets or sets the status prompt. </summary>
    ''' <value> The status bar label. </value>
    Public Property StatusPrompt As String
        Get
            Return Me._StatusPrompt
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.StatusPrompt) Then
                If String.IsNullOrEmpty(value) Then value = String.Empty
                Me._StatusPrompt = value
                Me.SyncNotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the length of the maximum synopsis. </summary>
    ''' <value> The length of the maximum synopsis. </value>
    <Category("Data"), Description("Maximum synopsis message length"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue(0)>
    Public Property MaxSynopsisLength As Integer

    ''' <summary> Publishes status. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="traceMessage"> Message describing the trace. </param>
    Public Sub PublishSynopsis(ByVal traceMessage As TraceMessage)
        If traceMessage Is Nothing Then Return
        Dim value As String = TraceMessage.ExtractSynopsis(traceMessage.Details,
                                                           TraceMessage.DefaultSynopsisDelimiter, Me.MaxSynopsisLength)
        Me.StatusPrompt = value
    End Sub

#End Region

#Region " I MESSAGE LISTENER "

    ''' <summary> Registers this trace level and updates alerts. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="level"> The level. </param>
    Public Overrides Sub Register(level As TraceEventType) Implements IMessageListener.Register
        MyBase.Register(level)
        Me.UpdateAlerts(level)
    End Sub

#End Region

End Class

