Imports System.ComponentModel

Partial Public Class MessagesBox
    Implements INotifyPropertyChanged

#Region " CUSTOM PROPERTY CHANGED EVENT IMPLEMENTATION "

    ''' <summary> Removes the property changed event handlers. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Protected Sub RemovePropertyChangedEventHandlers()
        Me._PropertyChangedHandlers.RemoveAll()
    End Sub

    ''' <summary> The property changed handlers. </summary>
    Private ReadOnly _PropertyChangedHandlers As New PropertyChangeEventContextCollection()

    ''' <summary> Event queue for all listeners interested in property changed events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

        AddHandler(ByVal value As PropertyChangedEventHandler)
            Me._PropertyChangedHandlers.Add(New PropertyChangedEventContext(value))
        End AddHandler

        RemoveHandler(ByVal value As PropertyChangedEventHandler)
            Me._PropertyChangedHandlers.RemoveValue(value)
        End RemoveHandler

        RaiseEvent(ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
            ' Defaults to 'Send' to prevent cross thread exceptions.
            Me._PropertyChangedHandlers.Send(sender, e)
        End RaiseEvent

    End Event


#End Region

End Class
