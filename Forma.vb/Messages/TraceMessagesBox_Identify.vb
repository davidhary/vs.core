
Partial Public Class TraceMessagesBox

    ''' <summary> List of dates of the talker identities. </summary>
    Private ReadOnly _TalkerIdentityDates As New Dictionary(Of Integer, DateTimeOffset)

    ''' <summary> The talker identity messages. </summary>
    Private ReadOnly _TalkerIdentityMessages As New Dictionary(Of Integer, TraceMessage)

    ''' <summary>
    ''' Query if talker with identity as specified in the trace message was identified.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value"> The value. </param>
    ''' <returns> <c>true</c> if talker identified; otherwise <c>false</c> </returns>
    Public Function IsTalkerIdentified(ByVal value As TraceMessage) As Boolean Implements ITraceMessageListener.IsTalkerIdentified
        If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
        Return Me._TalkerIdentityDates.ContainsKey(value.Id) AndAlso Me._TalkerIdentityDates(value.Id).Date >= DateTimeOffset.Now.Date
    End Function

    ''' <summary> Identify a talker using the talker project identity. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The event message. </param>
    Public Sub IdentifyTalker(ByVal value As TraceMessage) Implements ITraceMessageListener.IdentifyTalker
        If value IsNot Nothing Then
            If Me._TalkerIdentityDates.ContainsKey(value.Id) Then
                Me._TalkerIdentityDates.Item(value.Id) = DateTimeOffset.Now
            Else
                Me._TalkerIdentityDates.Add(value.Id, DateTimeOffset.Now)
            End If
            If Me._TalkerIdentityMessages.ContainsKey(value.Id) Then
                Me._TalkerIdentityMessages.Item(value.Id) = value
            Else
                Me._TalkerIdentityMessages.Add(value.Id, value)
            End If
        End If
    End Sub

    ''' <summary> Enumerates talker identity messages in this collection. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process talker identity messages in this
    ''' collection.
    ''' </returns>
    Public Function TalkerIdentityMessages() As IList(Of TraceMessage) Implements ITraceMessageListener.TalkerIdentityMessages
        Return Me._TalkerIdentityMessages.Values.ToList
    End Function

    ''' <summary> List of dates of the talker date messages. </summary>
    Private ReadOnly _TalkerDateMessagesDates As New Dictionary(Of Integer, DateTimeOffset)

    ''' <summary> The talker date messages. </summary>
    Private ReadOnly _TalkerDateMessages As New Dictionary(Of Integer, Dictionary(Of String, TraceMessage))

    ''' <summary> Query if 'value' is talker date message published. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value"> The event message. </param>
    ''' <returns> <c>true</c> if talker date message published; otherwise <c>false</c> </returns>
    Public Function IsTalkerDateMessagePublished(ByVal value As TraceMessage) As Boolean Implements ITraceMessageListener.IsTalkerDateMessagePublished
        If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
        Return Me._TalkerDateMessagesDates.ContainsKey(value.Id) AndAlso Me._TalkerDateMessagesDates(value.Id).Date >= DateTimeOffset.Now.Date AndAlso
                            Me._TalkerDateMessages(value.Id).ContainsKey(value.Details)
    End Function

    ''' <summary> Adds a date message. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The event message. </param>
    Public Sub AddDateMessage(ByVal value As TraceMessage) Implements ITraceMessageListener.AddDateMessage
        If value IsNot Nothing Then
            If Me._TalkerDateMessagesDates.ContainsKey(value.Id) Then
                Me._TalkerDateMessagesDates.Item(value.Id) = DateTimeOffset.Now
            Else
                Me._TalkerDateMessagesDates.Add(value.Id, DateTimeOffset.Now)
            End If
            If Not Me._TalkerDateMessages.ContainsKey(value.Id) Then
                Me._TalkerDateMessages.Add(value.Id, New Dictionary(Of String, TraceMessage))
            End If
            If Me._TalkerDateMessages(value.Id).ContainsKey(value.Details) Then
                Me._TalkerDateMessages(value.Id).Item(value.Details) = value
            Else
                Me._TalkerDateMessages(value.Id).Add(value.Details, value)
            End If
        End If
    End Sub

    ''' <summary> Enumerates talker date messages in this collection. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process talker date messages in this
    ''' collection.
    ''' </returns>
    Public Function TalkerDateMessages() As IList(Of TraceMessage) Implements ITraceMessageListener.TalkerDateMessages
        Dim l As New List(Of TraceMessage)
        For Each kvp As KeyValuePair(Of Integer, Dictionary(Of String, TraceMessage)) In Me._TalkerDateMessages
            For Each item As KeyValuePair(Of String, TraceMessage) In kvp.Value
                l.Add(item.Value)
            Next
        Next
        Return l
    End Function

End Class
