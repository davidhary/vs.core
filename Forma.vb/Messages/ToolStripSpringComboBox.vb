Imports System.Drawing
Imports System.Windows.Forms

''' <summary> A tool strip spring Combo box. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 3/22/2017 </para>
''' </remarks>
Public Class ToolStripSpringComboBox
    Inherits ToolStripComboBox

    ''' <summary> Gets preferred size. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="constrainingSize"> Size of the constraining. </param>
    ''' <returns> The preferred size. </returns>
    Public Overrides Function GetPreferredSize(ByVal constrainingSize As Size) As Size

        ' Use the default size if the text box is on the overflow menu
        ' or is on a vertical ToolStrip.
        If Me.IsOnOverflow Or Me.Owner.Orientation = Orientation.Vertical Then
            Return Me.DefaultSize
        End If

        ' Declare a variable to store the total available width as 
        ' it is calculated, starting with the display width of the 
        ' owning ToolStrip.
        Dim width As Int32 = Me.Owner.DisplayRectangle.Width

        ' Subtract the width of the overflow button if it is displayed. 
        If Me.Owner.OverflowButton.Visible Then
            width = width - Me.Owner.OverflowButton.Width -
                Me.Owner.OverflowButton.Margin.Horizontal()
        End If

        ' Declare a variable to maintain a count of ToolStripSpringTextBox 
        ' items currently displayed in the owning ToolStrip. 
        Dim springBoxCount As Int32 = 0

        For Each item As ToolStripItem In Me.Owner.Items

            ' Ignore items on the overflow menu.
            If item.IsOnOverflow Then Continue For

            If TypeOf item Is ToolStripSpringComboBox Then
                ' For ToolStripSpringTextBox items, increment the count and 
                ' subtract the margin width from the total available width.
                springBoxCount += 1
                width -= item.Margin.Horizontal
            Else
                ' For all other items, subtract the full width from the total
                ' available width.
                width = width - item.Width - item.Margin.Horizontal
            End If
        Next

        ' If there are multiple ToolStripSpringTextBox items in the owning
        ' ToolStrip, divide the total available width between them. 
        If springBoxCount > 1 Then width = CInt(width / springBoxCount)

        ' If the available width is less than the default width, use the
        ' default width, forcing one or more items onto the overflow menu.
        If width < Me.DefaultSize.Width Then width = Me.DefaultSize.Width

        ' Retrieve the preferred size from the base class, but change the
        ' width to the calculated width. 
        Dim preferredSize As Size = MyBase.GetPreferredSize(constrainingSize)
        preferredSize.Width = width
        Return preferredSize

    End Function

End Class
