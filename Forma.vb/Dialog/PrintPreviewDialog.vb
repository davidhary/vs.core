Imports System.Windows.Forms

Imports isr.Core.Forma.ExceptionExtensions

''' <summary> Dialog for setting the print preview. </summary>
''' <remarks>
''' David, 2020-09-10.
''' https://stackoverflow.com/questions/40236241/how-to-add-print-dialog-to-the-printpreviewdialog.
''' </remarks>
Public Class PrintPreviewDialog
    Inherits Windows.Forms.PrintPreviewDialog

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.Windows.Forms.PrintPreviewDialog" />
    ''' class.
    ''' </summary>
    ''' <remarks> David, 2020-09-08. </remarks>
    Public Sub New()
        MyBase.New
        Me.ReplacePrintDialogButton()
    End Sub

    ''' <summary> Replace print dialog button. </summary>
    ''' <remarks> David, 2020-09-08. </remarks>
    Private Sub ReplacePrintDialogButton()
        Dim printToolStrip As Windows.Forms.ToolStrip = TryCast(Me.Controls(1), Windows.Forms.ToolStrip)
        If printToolStrip IsNot Nothing Then
            Dim b As New Windows.Forms.ToolStripButton With {
                .DisplayStyle = Windows.Forms.ToolStripItemDisplayStyle.Image
            }
            AddHandler b.Click, AddressOf Me.PrintDialog_PrintClick
            b.Image = printToolStrip.ImageList.Images(0)
            printToolStrip.Items.RemoveAt(0)
            printToolStrip.Items.Insert(0, b)
        End If
    End Sub

    ''' <summary> Event handler. Called by PrintDialog for print click events. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Private Sub PrintDialog_PrintClick(sender As Object, e As EventArgs)
        Dim activity As String = String.Empty
        Try
            activity = "instantiating the print dialog"
            Using pd As New Windows.Forms.PrintDialog With {.Document = Me.Document}
                activity = "showing the print dialog"
                If pd.ShowDialog() = Windows.Forms.DialogResult.OK Then
                    activity = "setting printer settings"
                    Me.Document.PrinterSettings = pd.PrinterSettings
                    activity = "printing"
                    Me.Document.Print()
                End If
            End Using
        Catch ex As Exception
            Windows.Forms.MessageBox.Show($"Exception {activity};. {ex.ToFullBlownString}", "Print Dialog Exception", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub


End Class
