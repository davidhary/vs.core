''' <summary> Keeps dialog form on top. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 01/31/2013, 6.1.4779.x. </para>
''' </remarks>
Public Class TopDialogBase
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    ''' <summary> Specialized default constructor for use only by derived classes. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Protected Sub New()


        MyBase.New()

        'This call is required by the Windows Form Designer.
        Me.InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me._Components IsNot Nothing Then Me._Components.Dispose() : Me._Components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    Private _Components As System.ComponentModel.IContainer

        Private WithEvents OnTopTimer As System.Windows.Forms.Timer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me._Components = New System.ComponentModel.Container()
        Me.OnTopTimer = New System.Windows.Forms.Timer(Me._Components)
        Me.SuspendLayout()
        '
        '_onTopTimer
        '
        Me.OnTopTimer.Interval = 1000
        '
        'TopDialogBase
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font

        Me.ClientSize = New System.Drawing.Size(331, 343)
        Me.Font = New System.Drawing.Font(System.Drawing.SystemFonts.MessageBoxFont.FontFamily, 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = My.Resources.favicon
        Me.Name = "TopDialogBase"
        Me.Text = "Top Dialog Base"
        Me.ResumeLayout(False)

    End Sub

#End Region

    ''' <summary>
    ''' Gets or sets the sentinel indicating that the form loaded without an exception. Should be set
    ''' only if load did not fail.
    ''' </summary>
    ''' <value> The is loaded. </value>
    Protected Property IsLoaded As Boolean

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Form.Load" /> event . </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnLoad(e As System.EventArgs)
        Try
            Me.IsLoaded = True
        Catch
            Throw
        Finally
            MyBase.OnLoad(e)
        End Try
    End Sub

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.Form.Shown" /> event after enabling the top most
    ''' timer.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnShown(e As System.EventArgs)
        Me.OnTopTimer.Enabled = True
        MyBase.OnShown(e)
    End Sub

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.Form.Closing" /> event after Disabling the top
    ''' most timer if not canceled.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.ComponentModel.CancelEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnClosing(ByVal e As System.ComponentModel.CancelEventArgs)
        If e Is Nothing OrElse Not e.Cancel Then
            Me.OnTopTimer.Enabled = False
        End If
        MyBase.OnClosing(e)
    End Sub

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.Form.Closed" /> event after disabling the top
    ''' most timer.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> The <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnClosed(e As System.EventArgs)
        If Me.OnTopTimer IsNot Nothing Then
            Me.OnTopTimer.Enabled = False
        End If
        MyBase.OnClosed(e)
    End Sub

    ''' <summary>
    ''' Handles the Tick event of the _onTopTimer control. Sets <see cref="TopMost"></see>
    ''' true if not already true.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Private Sub OnTopTimer_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles OnTopTimer.Tick
        If sender IsNot Nothing Then
            If Not Me.TopMost = True Then
                Me.TopMost = True
            End If
        End If
    End Sub

End Class
