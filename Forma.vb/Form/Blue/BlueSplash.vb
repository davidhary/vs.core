Imports System.Drawing

Imports isr.Core.Forma.ExceptionExtensions

''' <summary> Blue splash. </summary>
''' <remarks>
''' (c) 2015 Magyar András. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/08/2012 from Magyar András </para><para>
''' http://www.codeproject.com/Articles/804316/Office-Style-Splash-Screen. </para>
''' </remarks>
Partial Public Class BlueSplash
    Inherits Forma.FormBase

#Region " CONSTRUCTION "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        Me.InitializeComponent()
        Me.UpdateInfo()
    End Sub

    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the
    ''' <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " EVENT HANDLERS "

    ''' <summary>
    ''' Does all the post processing after all the form controls are rendered as the user expects
    ''' them.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub Form_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown

        Windows.Forms.Application.DoEvents()

        Try

            Me.Cursor = System.Windows.Forms.Cursors.Hand

            Me.CurrentTask = "Starting..."

            ' instantiate form objects
            Me.UpdateInfo()

        Catch ex As Exception

            System.Windows.Forms.MessageBox.Show(ex.ToFullBlownString, "Exception Occurred",
                                                 System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Exclamation, System.Windows.Forms.MessageBoxDefaultButton.Button1, System.Windows.Forms.MessageBoxOptions.DefaultDesktopOnly)

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default
            Windows.Forms.Application.DoEvents()

        End Try

    End Sub

    ''' <summary> Close Application. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Close_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _CloseLabel.Click
        Me._IsCloseRequested = True
    End Sub

    ''' <summary> Minimize label effects. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Minimize_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _MinimizeLabel.Click
        Me._IsMinimized = True
    End Sub

    ''' <summary> Closes label Mouse hover and leave effects </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Close_MouseHover(ByVal sender As Object, ByVal e As EventArgs) Handles _CloseLabel.MouseHover
        Me._CloseLabel.ForeColor = Color.Silver
    End Sub

    ''' <summary> Closes mouse leave. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Close_MouseLeave(ByVal sender As Object, ByVal e As EventArgs) Handles _CloseLabel.MouseLeave
        Me._CloseLabel.ForeColor = Color.White
    End Sub

    ''' <summary> Minimize mouse hover. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Minimize_MouseHover(ByVal sender As Object, ByVal e As EventArgs) Handles _MinimizeLabel.MouseHover
        Me._MinimizeLabel.ForeColor = Color.Silver
    End Sub

    ''' <summary> Minimize mouse leave. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Minimize_MouseLeave(ByVal sender As Object, ByVal e As EventArgs) Handles _MinimizeLabel.MouseLeave
        Me._MinimizeLabel.ForeColor = Color.White
    End Sub
#End Region

#Region " PROPERTIES and METHODS "

    ''' <summary> True if is minimized, false if not. </summary>
    Private _IsMinimized As Boolean

    ''' <summary> Gets a value indicating whether this object is minimize requested. </summary>
    ''' <value> <c>true</c> if this object is minimize requested; otherwise <c>false</c> </value>
    Public ReadOnly Property IsMinimizeRequested As Boolean
        Get
            Return Me._IsMinimized
        End Get
    End Property

    ''' <summary> True if is close requested, false if not. </summary>
    Private _IsCloseRequested As Boolean

    ''' <summary> Gets a value indicating whether this object is close requested. </summary>
    ''' <value> <c>true</c> if this object is close requested; otherwise <c>false</c> </value>
    Public ReadOnly Property IsCloseRequested As Boolean
        Get
            Return Me._IsCloseRequested
        End Get
    End Property

    ''' <summary> Gets or sets the current task. </summary>
    ''' <value> The current task. </value>
    Public Property CurrentTask As String
        Get
            Return Me._CurrentTaskLabel.Text
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.CurrentTask) Then
                BlueSplash.SafeTextSetter(Me._CurrentTaskLabel, value)
            End If
        End Set
    End Property

    ''' <summary> true to topmost. </summary>
    Private _Topmost As Boolean

    ''' <summary> Sets the top most status in a thread safe way. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> true to value. </param>
    Public Sub TopmostSetter(ByVal value As Boolean)
        Me._Topmost = value
        BlueSplash.SafeTopMostSetter(Me, value)
    End Sub

    ''' <summary> Displays a message on the splash screen. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The message. </param>
    Public Sub DisplayMessage(ByVal value As String)
        Me.CurrentTask = value
    End Sub

    ''' <summary> Gets or sets the small application caption. </summary>
    ''' <value> The small application caption. </value>
    Public Property SmallApplicationCaption As String
        Get
            Return Me._SmallApplicationCaptionLabel.Text
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.SmallApplicationCaption) Then
                BlueSplash.SafeTextSetter(Me._SmallApplicationCaptionLabel, value)
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the large application caption. </summary>
    ''' <value> The large application caption. </value>
    Public Property LargeApplicationCaption As String
        Get
            Return Me._LargeApplicationCaptionLabel.Text
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.LargeApplicationCaption) Then
                BlueSplash.SafeTextSetter(Me._LargeApplicationCaptionLabel, value)
            End If
        End Set
    End Property

    ''' <summary> Update the information on screen. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Sub UpdateInfo()
        ' Me._SmallApplicationCaptionLabel.Text = My.Application.Info.ProductName
        Me._LargeApplicationCaptionLabel.Text = My.Application.Info.AssemblyName
        Me._SmallApplicationCaptionLabel.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "{0} {1}",
                                                              My.Application.Info.ProductName, My.Application.Info.Version)
        Windows.Forms.Application.DoEvents()
        Me.TopMost = Me._Topmost
        Windows.Forms.Application.DoEvents()
    End Sub

#End Region

#Region " THREAD SAFE METHODS "

    ''' <summary> Safe height setter. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="control"> The control. </param>
    ''' <param name="value">   true to value. </param>
    Private Shared Sub SafeHeightSetter(ByVal control As System.Windows.Forms.Control, ByVal value As Integer)
        If control IsNot Nothing Then
            If control.InvokeRequired Then
                control.Invoke(New Action(Of System.Windows.Forms.Control, Integer)(AddressOf BlueSplash.SafeHeightSetter), New Object() {control, value})
            Else
                control.Height = value
                control.Invalidate()
                Windows.Forms.Application.DoEvents()
            End If
        End If
    End Sub

    ''' <summary>
    ''' Sets the <see cref="System.Windows.Forms.Control">control</see> text to the
    ''' <paramref name="value">value</paramref>.
    ''' This setter is thread safe.
    ''' </summary>
    ''' <remarks> The value is set to empty if null or empty. </remarks>
    ''' <param name="control"> The control. </param>
    ''' <param name="value">   The value. </param>
    ''' <returns> value. </returns>
    Private Shared Function SafeTextSetter(ByVal control As System.Windows.Forms.Control, ByVal value As String) As String
        If control IsNot Nothing Then
            If String.IsNullOrWhiteSpace(value) Then
                value = String.Empty
            End If
            If control.InvokeRequired Then
                control.Invoke(New Action(Of System.Windows.Forms.Control, String)(AddressOf BlueSplash.SafeTextSetter), New Object() {control, value})
            Else
                control.Text = value
                control.Invalidate()
                Windows.Forms.Application.DoEvents()
            End If
        End If
        Return value
    End Function

    ''' <summary> Safe top most setter. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="form">  The form. </param>
    ''' <param name="value"> true to value. </param>
    Private Shared Sub SafeTopMostSetter(ByVal form As System.Windows.Forms.Form, ByVal value As Boolean)
        If form IsNot Nothing Then
            If form.InvokeRequired Then
                form.Invoke(New Action(Of System.Windows.Forms.Form, Boolean)(AddressOf BlueSplash.SafeTopMostSetter), New Object() {form, value})
            Else
                form.TopMost = value
                Windows.Forms.Application.DoEvents()
            End If
        End If
    End Sub

    ''' <summary> Display info. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub DisplayInfo()
        If Me.InvokeRequired Then
            Me.Invoke(New Action(AddressOf Me.UpdateInfo))
        Else
            Me.UpdateInfo()
        End If
    End Sub

#End Region

End Class
