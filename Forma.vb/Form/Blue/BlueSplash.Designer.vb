﻿Partial Public Class BlueSplash
    ''' <summary>
    ''' Required designer variable.
    ''' </summary>
    Private components As System.ComponentModel.IContainer = Nothing

#Region "Windows Form Designer generated code"

    ''' <summary>
    ''' Required method for Designer support - do not modify
    ''' the contents of this method with the code editor.
    ''' </summary>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(BlueSplash))
        Me._SmallApplicationCaptionLabel = New System.Windows.Forms.Label()
        Me._LargeApplicationCaptionLabel = New System.Windows.Forms.Label()
        Me._CurrentTaskLabel = New System.Windows.Forms.Label()
        Me._CloseLabel = New System.Windows.Forms.Label()
        Me._MinimizeLabel = New System.Windows.Forms.Label()
        Me._MetroProgressBar = New MetroProgressBar()
        Me._FolderLabel = New System.Windows.Forms.Label()
        Me._TableLayoutPanel = New System.Windows.Forms.TableLayoutPanel()
        Me._AuthorLabel = New System.Windows.Forms.Label()
        Me._TopPanel = New System.Windows.Forms.Panel()
        Me._ProgressLayout = New System.Windows.Forms.TableLayoutPanel()
        Me._TableLayoutPanel.SuspendLayout()
        Me._TopPanel.SuspendLayout()
        Me._ProgressLayout.SuspendLayout()
        Me.SuspendLayout()
        '
        '_SmallApplicationCaptionLabel
        '
        Me._SmallApplicationCaptionLabel.AutoSize = True
        Me._SmallApplicationCaptionLabel.BackColor = System.Drawing.Color.Transparent
        Me._SmallApplicationCaptionLabel.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me._SmallApplicationCaptionLabel.ForeColor = System.Drawing.Color.White
        Me._SmallApplicationCaptionLabel.Location = New System.Drawing.Point(32, 0)
        Me._SmallApplicationCaptionLabel.Name = "_SmallApplicationCaptionLabel"
        Me._SmallApplicationCaptionLabel.Size = New System.Drawing.Size(124, 20)
        Me._SmallApplicationCaptionLabel.TabIndex = 0
        Me._SmallApplicationCaptionLabel.Text = "Application Title"
        '
        '_LargeApplicationCaptionLabel
        '
        Me._LargeApplicationCaptionLabel.AutoSize = True
        Me._LargeApplicationCaptionLabel.BackColor = System.Drawing.Color.Transparent
        Me._LargeApplicationCaptionLabel.Dock = System.Windows.Forms.DockStyle.Top
        Me._LargeApplicationCaptionLabel.Font = New System.Drawing.Font("Segoe UI Semibold", 36.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me._LargeApplicationCaptionLabel.ForeColor = System.Drawing.Color.White
        Me._LargeApplicationCaptionLabel.Location = New System.Drawing.Point(3, 104)
        Me._LargeApplicationCaptionLabel.Name = "_LargeApplicationCaptionLabel"
        Me._LargeApplicationCaptionLabel.Size = New System.Drawing.Size(506, 65)
        Me._LargeApplicationCaptionLabel.TabIndex = 1
        Me._LargeApplicationCaptionLabel.Text = "Application"
        Me._LargeApplicationCaptionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        '_CurrentTaskLabel
        '
        Me._CurrentTaskLabel.AutoSize = True
        Me._CurrentTaskLabel.BackColor = System.Drawing.Color.Transparent
        Me._CurrentTaskLabel.Dock = System.Windows.Forms.DockStyle.Top
        Me._CurrentTaskLabel.Font = New System.Drawing.Font("Segoe UI", 18.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me._CurrentTaskLabel.ForeColor = System.Drawing.Color.White
        Me._CurrentTaskLabel.Location = New System.Drawing.Point(3, 268)
        Me._CurrentTaskLabel.Name = "_CurrentTaskLabel"
        Me._CurrentTaskLabel.Size = New System.Drawing.Size(506, 32)
        Me._CurrentTaskLabel.TabIndex = 2
        Me._CurrentTaskLabel.Text = "current task"
        '
        '_CloseLabel
        '
        Me._CloseLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._CloseLabel.AutoSize = True
        Me._CloseLabel.BackColor = System.Drawing.Color.Transparent
        Me._CloseLabel.Font = New System.Drawing.Font("Webdings", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me._CloseLabel.ForeColor = System.Drawing.Color.White
        Me._CloseLabel.Location = New System.Drawing.Point(467, 0)
        Me._CloseLabel.Name = "_CloseLabel"
        Me._CloseLabel.Size = New System.Drawing.Size(36, 26)
        Me._CloseLabel.TabIndex = 3
        Me._CloseLabel.Text = "r"
        '
        '_MinimizeLable
        '
        Me._MinimizeLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._MinimizeLabel.AutoSize = True
        Me._MinimizeLabel.BackColor = System.Drawing.Color.Transparent
        Me._MinimizeLabel.Font = New System.Drawing.Font("Webdings", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me._MinimizeLabel.ForeColor = System.Drawing.Color.White
        Me._MinimizeLabel.Location = New System.Drawing.Point(425, 0)
        Me._MinimizeLabel.Name = "_MinimizeLable"
        Me._MinimizeLabel.Size = New System.Drawing.Size(36, 26)
        Me._MinimizeLabel.TabIndex = 4
        Me._MinimizeLabel.Text = "0"
        '
        '_MetroProgressBar
        '
        Me._MetroProgressBar.BackColor = System.Drawing.Color.Transparent
        Me._MetroProgressBar.Location = New System.Drawing.Point(73, 4)
        Me._MetroProgressBar.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._MetroProgressBar.Name = "_MetroProgressBar"
        Me._MetroProgressBar.Size = New System.Drawing.Size(359, 6)
        Me._MetroProgressBar.TabIndex = 5
        '
        '_FolderLabel
        '
        Me._FolderLabel.AutoSize = True
        Me._FolderLabel.BackColor = System.Drawing.Color.Transparent
        Me._FolderLabel.Font = New System.Drawing.Font("Wingdings", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me._FolderLabel.ForeColor = System.Drawing.Color.White
        Me._FolderLabel.Location = New System.Drawing.Point(3, 3)
        Me._FolderLabel.Name = "_FolderLabel"
        Me._FolderLabel.Size = New System.Drawing.Size(28, 16)
        Me._FolderLabel.TabIndex = 6
        Me._FolderLabel.Text = "1"
        '
        '_TableLayoutPanel
        '
        Me._TableLayoutPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(43, Byte), Integer), CType(CType(86, Byte), Integer), CType(CType(154, Byte), Integer))
        Me._TableLayoutPanel.ColumnCount = 1
        Me._TableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._TableLayoutPanel.Controls.Add(Me._AuthorLabel, 0, 9)
        Me._TableLayoutPanel.Controls.Add(Me._TopPanel, 0, 1)
        Me._TableLayoutPanel.Controls.Add(Me._CurrentTaskLabel, 0, 7)
        Me._TableLayoutPanel.Controls.Add(Me._LargeApplicationCaptionLabel, 0, 3)
        Me._TableLayoutPanel.Controls.Add(Me._ProgressLayout, 0, 5)
        Me._TableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._TableLayoutPanel.Location = New System.Drawing.Point(0, 0)
        Me._TableLayoutPanel.Name = "_TableLayoutPanel"
        Me._TableLayoutPanel.RowCount = 10
        Me._TableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 3.0!))
        Me._TableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._TableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._TableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._TableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10.0!))
        Me._TableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._TableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._TableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._TableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 3.0!))
        Me._TableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me._TableLayoutPanel.Size = New System.Drawing.Size(512, 324)
        Me._TableLayoutPanel.TabIndex = 7
        '
        '_AuthorLabel
        '
        Me._AuthorLabel.AutoSize = True
        Me._AuthorLabel.BackColor = System.Drawing.Color.Transparent
        Me._AuthorLabel.Dock = System.Windows.Forms.DockStyle.Top
        Me._AuthorLabel.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me._AuthorLabel.ForeColor = System.Drawing.Color.WhiteSmoke
        Me._AuthorLabel.Location = New System.Drawing.Point(3, 303)
        Me._AuthorLabel.Name = "_AuthorLabel"
        Me._AuthorLabel.Size = New System.Drawing.Size(506, 21)
        Me._AuthorLabel.TabIndex = 9
        Me._AuthorLabel.Text = "A product of Integrated Scientific Resources"
        Me._AuthorLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        '_TopPanel
        '
        Me._TopPanel.BackColor = System.Drawing.Color.Transparent
        Me._TopPanel.Controls.Add(Me._CloseLabel)
        Me._TopPanel.Controls.Add(Me._FolderLabel)
        Me._TopPanel.Controls.Add(Me._MinimizeLabel)
        Me._TopPanel.Controls.Add(Me._SmallApplicationCaptionLabel)
        Me._TopPanel.Dock = System.Windows.Forms.DockStyle.Top
        Me._TopPanel.Location = New System.Drawing.Point(3, 6)
        Me._TopPanel.Name = "_TopPanel"
        Me._TopPanel.Size = New System.Drawing.Size(506, 26)
        Me._TopPanel.TabIndex = 0
        '
        '_ProgressLayout
        '
        Me._ProgressLayout.ColumnCount = 3
        Me._ProgressLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ProgressLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._ProgressLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ProgressLayout.Controls.Add(Me._MetroProgressBar, 1, 0)
        Me._ProgressLayout.Dock = System.Windows.Forms.DockStyle.Top
        Me._ProgressLayout.Location = New System.Drawing.Point(3, 182)
        Me._ProgressLayout.Name = "_ProgressLayout"
        Me._ProgressLayout.RowCount = 1
        Me._ProgressLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._ProgressLayout.Size = New System.Drawing.Size(506, 14)
        Me._ProgressLayout.TabIndex = 8
        '
        'BlueSplash
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.isr.Core.Forma.My.Resources.Resources.BlueSplash
        Me.ClientSize = New System.Drawing.Size(512, 324)
        Me.Controls.Add(Me._TableLayoutPanel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximumSize = New System.Drawing.Size(512, 324)
        Me.MinimumSize = New System.Drawing.Size(512, 324)
        Me.Name = "BlueSplash"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Splash"
        Me._TableLayoutPanel.ResumeLayout(False)
        Me._TableLayoutPanel.PerformLayout()
        Me._TopPanel.ResumeLayout(False)
        Me._TopPanel.PerformLayout()
        Me._ProgressLayout.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private _SmallApplicationCaptionLabel As System.Windows.Forms.Label
    Private _LargeApplicationCaptionLabel As System.Windows.Forms.Label
    Private _CurrentTaskLabel As System.Windows.Forms.Label
    Private WithEvents _CloseLabel As System.Windows.Forms.Label
    Private WithEvents _MinimizeLabel As System.Windows.Forms.Label
    Private _MetroProgressBar As MetroProgressBar
    Private _FolderLabel As System.Windows.Forms.Label
    Private WithEvents _TableLayoutPanel As System.Windows.Forms.TableLayoutPanel
    Private WithEvents _TopPanel As System.Windows.Forms.Panel
    Private WithEvents _ProgressLayout As System.Windows.Forms.TableLayoutPanel
    Private WithEvents _AuthorLabel As System.Windows.Forms.Label
End Class
