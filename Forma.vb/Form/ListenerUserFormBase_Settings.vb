Imports System.Configuration
Imports System.Security.Permissions
Imports System.Windows.Forms

Imports isr.Core.Forma.ExceptionExtensions

Partial Public Class ListenerUserFormBase

#Region " CLASS STYLE "

    ''' <summary> The enable drop shadow version. </summary>
    Public Const EnableDropShadowVersion As Integer = 5

    ''' <summary> Gets the class style. </summary>
    ''' <value> The class style. </value>
    Protected Property ClassStyle As ClassStyleConstants = ClassStyleConstants.None

    ''' <summary> Adds a drop shadow parameter. </summary>
    ''' <remarks>
    ''' From Code Project: http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx.
    ''' </remarks>
    ''' <value>
    ''' A <see cref="T:System.Windows.Forms.CreateParams" /> that contains the required creation
    ''' parameters when the handle to the control is created.
    ''' </value>
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        <System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Demand,
                                                        Flags:=System.Security.Permissions.SecurityPermissionFlag.UnmanagedCode)>
        Get
            Dim cp As System.Windows.Forms.CreateParams = MyBase.CreateParams
            cp.ClassStyle = cp.ClassStyle Or CInt(Me.ClassStyle)
            Return cp
        End Get
    End Property

#End Region

#Region " SETTING EVENTS "

    ''' <summary> Controls if settings are saved when closing. </summary>
    ''' <remarks> Set this property to false to disable saving form settings on closing. </remarks>
    ''' <value> <c>SaveSettingsOnClosing</c>is a Boolean property. </value>
    Public Property SaveSettingsOnClosing() As Boolean

    ''' <summary> This is called when the form is loaded before it is visible. </summary>
    ''' <remarks> Use this method to set form elements before the form is visible. </remarks>
    <PermissionSetAttribute(SecurityAction.Demand, Unrestricted:=True)>
    Protected Overridable Sub OnLoadSettings()
        If Me.SaveSettingsOnClosing Then
            Dim startPos As FormStartPosition = My.AppSettingsScribe.ReadValue(Me.StartPositionKey, Me.StartPosition)
            If Not Me.StartPosition.Equals(startPos) Then
                Me.StartPosition = startPos
            End If
        End If
    End Sub

    ''' <summary> This is called when the form is shown after it is visible. </summary>
    ''' <remarks> Use this method to set form elements after the form is visible. </remarks>
    <PermissionSetAttribute(SecurityAction.Demand, Unrestricted:=True)>
    Protected Overridable Sub OnShowSettings()
        If Me.SaveSettingsOnClosing Then
            If Me.StartPosition = FormStartPosition.Manual Then
                Me.WindowState = My.AppSettingsScribe.ReadValue(Me.WindowsStateKey, Me.WindowState)
                If Me.WindowState = FormWindowState.Normal Then
                    Dim size As Drawing.Size = My.AppSettingsScribe.Get.ReadValue(Me.SizeKey, Me.Size)
                    Dim loc As Drawing.Point = My.AppSettingsScribe.Get.ReadValue(Me.LocationKey, Me.Location)
                    If size.Width < Me.MinimumSize.Width AndAlso size.Height < Me.MinimumSize.Height Then
                        size = New System.Drawing.Size(Me.MinimumSize.Width, Me.MinimumSize.Height)
                    ElseIf size.Width < Me.MinimumSize.Width Then
                        size = New System.Drawing.Size(Me.MinimumSize.Width, size.Height)
                    ElseIf size.Height < Me.MinimumSize.Height Then
                        size = New System.Drawing.Size(size.Width, Me.MinimumSize.Height)
                    End If
                    If Not Me.Location.Equals(loc) AndAlso
                        loc.X < Screen.PrimaryScreen.WorkingArea.Width AndAlso
                        loc.X + size.Width > 0 AndAlso
                        loc.Y < Screen.PrimaryScreen.WorkingArea.Height AndAlso
                        loc.Y + size.Height > 0 Then
                        Me.Location = loc
                    End If
                    If Not Me.Size.Equals(size) Then
                        Me.Size = size
                    End If
                End If
            End If
        End If
    End Sub

    ''' <summary> Is called when the form unloads. </summary>
    ''' <remarks> Use Save settings. </remarks>
    <PermissionSetAttribute(SecurityAction.Demand, Unrestricted:=True)>
    Protected Overridable Sub OnSaveSettings()
        If Me.SaveSettingsOnClosing Then
            My.AppSettingsScribe.WriteValue(Me.StartPositionKey, Me.StartPosition)
            If Me.StartPosition = FormStartPosition.Manual Then
                My.AppSettingsScribe.WriteValue(Me.WindowsStateKey, Me.WindowState)
                If Me.WindowState = FormWindowState.Normal Then
                    My.AppSettingsScribe.Get.WriteValue(Me.LocationKey, Me.Location)
                    My.AppSettingsScribe.Get.WriteValue(Me.SizeKey, Me.Size)
                End If
            End If
            My.AppSettingsScribe.Get.Save()
        End If
    End Sub

#End Region

#Region " FORM EVENTS "

    ''' <summary>
    ''' Gets the sentinel indicating that the form loaded without an exception. Should be set only if
    ''' load did not fail.
    ''' </summary>
    ''' <value> The is loaded. </value>
    Protected Property IsLoaded As Boolean

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.Form.Load" /> event after reading the start
    ''' position from the configuration file.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnLoad(e As System.EventArgs)
        Try
            If Not Me.DesignMode Then
                Me.OnLoadSettings()
            End If
            Me.IsLoaded = True
        Catch
            Throw
        Finally
            MyBase.OnLoad(e)
        End Try
    End Sub

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.Form.Shown" /> event after positioning the form
    ''' based on the configuration settings.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnShown(e As System.EventArgs)
        If Not Me.IsLoaded Then Return
        Try
            If Not Me.DesignMode Then
                Me.OnShowSettings()
            End If
        Catch
            Throw
        Finally
            MyBase.OnShown(e)
        End Try
        Me.OnShownCompleted(e)
    End Sub

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.Form.Closing" /> event after saving the form
    ''' location settings.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.ComponentModel.CancelEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnClosing(ByVal e As System.ComponentModel.CancelEventArgs)
        Try
            If Not Me.DesignMode AndAlso Me.IsLoaded AndAlso e IsNot Nothing AndAlso Not e.Cancel Then
                Me.OnSaveSettings()
            End If
        Catch ex As ConfigurationErrorsException
            ' this error occurs when the system thinks that two managers accessed the configuration file.
            Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
        Catch
            Throw
        Finally
            MyBase.OnClosing(e)
        End Try
    End Sub

    ''' <summary> Event queue for all listeners interested in ShownCompleted events. </summary>
    Public Event ShownCompleted As EventHandler(Of EventArgs)

    ''' <summary> Raises the shown completed event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnShownCompleted(ByVal e As EventArgs)
        Dim evt As EventHandler(Of EventArgs) = Me.ShownCompletedEvent
        evt?.Invoke(Me, EventArgs.Empty)
    End Sub

#End Region

#Region " CONFIGURATION MEMBERS "

    ''' <summary> Gets the location key. </summary>
    ''' <value> The location key. </value>
    Private ReadOnly Property LocationKey As String
        Get
            Return $"{Me.Name}.Location"
        End Get
    End Property

    ''' <summary> Gets the size key. </summary>
    ''' <value> The size key. </value>
    Private ReadOnly Property SizeKey As String
        Get
            Return $"{Me.Name}.Size"
        End Get
    End Property

    ''' <summary> Gets the start position key. </summary>
    ''' <value> The windows state key. </value>
    Private ReadOnly Property StartPositionKey As String
        Get
            Return $"{Me.Name}.StartPosition"
        End Get
    End Property

    ''' <summary> Gets the windows state key. </summary>
    ''' <value> The windows state key. </value>
    Private ReadOnly Property WindowsStateKey As String
        Get
            Return $"{Me.Name}.FormWindowState"
        End Get
    End Property

#End Region

End Class
