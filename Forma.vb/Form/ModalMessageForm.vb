Imports System.Windows.Forms

''' <summary> Form for viewing the modal message. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 7/17/2019 </para>
''' </remarks>
Partial Public Class ModalMessageForm
    Inherits System.Windows.Forms.Form

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()

        MyBase.New()

        ' This method is required by the Windows Form Designer.
        Me.InitializeComponent()

    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " BEHAVIOR "

    ''' <summary> Gets the message label. </summary>
    ''' <value> The message label. </value>
    Public ReadOnly Property MessageLabel As Windows.Forms.Label
        Get
            Return Me._MessageLabel
        End Get
    End Property

    ''' <summary> Gets or sets the display time. </summary>
    ''' <value> The display time. </value>
    Public Property DisplayTime As TimeSpan

    ''' <summary> Raises the activated event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overrides Sub onActivated(ByVal e As EventArgs)

        Me.MessageLabel.Dock = Windows.Forms.DockStyle.None
        Windows.Forms.Application.DoEvents()
        Me.MessageLabel.Refresh()

        ' Check size of message.
        If Me._MessageLabel.Height > 3 * Me.CreateGraphics().MeasureString("M", Me.Font).Height Then

            ' If label higher than three rows, than
            ' set for width and height 10% more than the label.
            Me.Width = CInt(Me._MessageLabel.Width / 0.8 + Me.Width - Me.ClientRectangle.Width)
            Me.Height = CInt(Me._MessageLabel.Height / 0.8 + Me.Height - Me.ClientRectangle.Height)

        End If

        ' Center the form on the screen.
        Me.SetBounds((Screen.PrimaryScreen.Bounds.Width - Me.Width) \ 2,
                         (Screen.PrimaryScreen.Bounds.Height - Me.Height) \ 2, 0, 0, BoundsSpecified.X Or BoundsSpecified.Y)
        Me.Refresh()

        ' Center the control on the screen.
        Me._MessageLabel.SetBounds((Me.ClientRectangle.Width - Me._MessageLabel.Width) \ 2,
                                   (Me.ClientRectangle.Height - Me._MessageLabel.Height) \ 2, 0, 0, BoundsSpecified.X Or BoundsSpecified.Y)
        Me.MessageLabel.Dock = DockStyle.Fill
        Me.MessageLabel.Refresh()

        Me.Refresh()

        Dim sw As Stopwatch = Stopwatch.StartNew
        ' Wait till past time.
        Do : Application.DoEvents() : Application.DoEvents() : Application.DoEvents() : Loop While sw.Elapsed < Me.DisplayTime

        ' Then hide this form.
        Me.Hide()

        ' Let the events happen.
        Application.DoEvents()

    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.KeyDown" /> event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.KeyEventArgs" /> that contains the event
    '''                  data. </param>
    Protected Overrides Sub OnKeyDown(e As KeyEventArgs)
        MyBase.OnKeyDown(e)
        If e Is Nothing Then Return
        Dim KeyCode As Integer = e.KeyCode
        Try
            Me.Hide()
            Application.DoEvents()
        Finally
            e.Handled = KeyCode = 0
        End Try
    End Sub

#End Region

End Class
