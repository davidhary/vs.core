Imports System.Drawing
Imports System.Windows.Forms
Imports System.Security.Permissions
Imports isr.Core.Forma.ExceptionExtensions

Partial Public Class FadeFormBase

#Region " WINDOWS MESSAGES "

#Region " WindowsMessageCodes "

    ''' <summary> The Windows Message System Command. </summary>
    Private Const WM_SYSCOMMAND As Integer = &H112

    ''' <summary> The Windows Message command. </summary>
    Private Const WM_COMMAND As Integer = &H111

    ''' <summary> The screen minimize. </summary>
    Private Const SC_MINIMIZE As Integer = &HF020

    ''' <summary> The screen restore. </summary>
    Private Const SC_RESTORE As Integer = &HF120

    ''' <summary> The screen close. </summary>
    Private Const SC_CLOSE As Integer = &HF060
#End Region

    ''' <summary>
    ''' Gets or sets the WindowsMessage that is being held until the end of a transition.
    ''' </summary>
    Private heldMessage As Message = New Message()

    ''' <summary> Intercepts Window Messages before they are processed. </summary>
    ''' <remarks>
    ''' The minimize and close events require messaging because these actions have to be postponed
    ''' until the fade transition is complete. Overriding WndProc catches the request for those
    ''' actions and postpones the action until the transition is done.
    ''' </remarks>
    ''' <param name="m"> [in,out] Windows Message. </param>
    <SecurityPermission(SecurityAction.Demand, Flags:=SecurityPermissionFlag.UnmanagedCode)>
    Protected Overrides Sub WndProc(ByRef m As Message)
        If m.Msg = WM_SYSCOMMAND OrElse m.Msg = WM_COMMAND Then
            'Fade to zero on minimize
            If m.WParam = New IntPtr(SC_MINIMIZE) Then
                If heldMessage.WParam <> New IntPtr(SC_MINIMIZE) Then
                    heldMessage = m
                    Me.TargetOpacity = Me._MinimizedOpacity
                Else
                    heldMessage = New Message()
                    Me.TargetOpacity = Me._ActiveOpacity
                End If
                Return

                'Fade in if the window is restored from the task bar
            ElseIf m.WParam = New IntPtr(SC_RESTORE) AndAlso Me.WindowState = FormWindowState.Minimized Then
                MyBase.WndProc(m)
                Me.TargetOpacity = Me._ActiveOpacity
                Return

                'Fade out if the window is closed.
            ElseIf m.WParam = New IntPtr(SC_CLOSE) Then
                heldMessage = m
                Me.TargetOpacity = Me._MinimizedOpacity
                Return
            End If
        End If

        MyBase.WndProc(m)
    End Sub

#End Region

End Class

