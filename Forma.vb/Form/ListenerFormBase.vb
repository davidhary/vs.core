Imports System.Drawing
Imports System.Windows.Forms

''' <summary> A form listening to trace messages. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 12/26/2015, 2.1.5836. </para>
''' </remarks>
Public Class ListenerFormBase
    Inherits Windows.Forms.Form

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Gets the initializing components sentinel. </summary>
    ''' <value> The initializing components sentinel. </value>
    Protected Property InitializingComponents As Boolean

    ''' <summary> Specialized default constructor for use only by derived classes. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Protected Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        Me.InitializeComponent()
        Me.InitializingComponents = False
        Me._Talker = New TraceMessageTalker
    End Sub

    ''' <summary> Initializes the component. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Sub InitializeComponent()
        Me.SuspendLayout()
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(331, 341)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Icon = My.Resources.favicon
        Me.Font = New Font(SystemFonts.MessageBoxFont.FontFamily, 9.75!, FontStyle.Regular, GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "FormBase"
        Me.ResumeLayout(False)
    End Sub

    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the
    ''' <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.Talker = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#Region " CLASS STYLE "

    ''' <summary> The enable drop shadow version. </summary>
    Public Const EnableDropShadowVersion As Integer = 5

    ''' <summary> Gets the class style. </summary>
    ''' <value> The class style. </value>
    Protected Property ClassStyle As ClassStyleConstants = ClassStyleConstants.None

    ''' <summary> Adds a drop shadow parameter. </summary>
    ''' <remarks>
    ''' From Code Project: http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx.
    ''' </remarks>
    ''' <value> Options that control the create. </value>
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        <Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Demand,
                                                        Flags:=System.Security.Permissions.SecurityPermissionFlag.UnmanagedCode)>
        Get
            Dim cp As CreateParams = MyBase.CreateParams
            cp.ClassStyle = cp.ClassStyle Or CInt(Me.ClassStyle)
            Return cp
        End Get
    End Property

#End Region

#End Region

#Region " SHOW "

    ''' <summary>
    ''' Shows the <see cref="RichTextBox">rich text box</see> form with these messages.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="mdiForm"> The MDI form. </param>
    ''' <param name="owner">   The owner. </param>
    Public Overloads Sub Show(ByVal mdiForm As System.Windows.Forms.Form, ByVal owner As System.Windows.Forms.IWin32Window)
        If mdiForm IsNot Nothing AndAlso mdiForm.IsMdiContainer Then
            Me.MdiParent = mdiForm
            mdiForm.Show()
        End If
        MyBase.Show(owner)
    End Sub

    ''' <summary>
    ''' Shows the <see cref="RichTextBox">rich text box</see> form with these messages.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="mdiForm"> The MDI form. </param>
    Public Overloads Sub ShowDialog(ByVal mdiForm As System.Windows.Forms.Form)
        If mdiForm IsNot Nothing AndAlso mdiForm.IsMdiContainer Then
            Me.MdiParent = mdiForm
            mdiForm.Show()
        End If
        MyBase.ShowDialog()
    End Sub

#End Region

End Class

''' <summary> Collection of listener forms. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 1/4/2016 </para>
''' </remarks>
Public Class ListenerFormCollection
    Inherits Collections.Generic.List(Of isr.Core.Forma.ListenerFormBase)

    ''' <summary> Adds and shows a new form,. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="form"> The form. </param>
    ''' <param name="log">  The log. </param>
    Public Sub ShowNew(ByVal form As ListenerFormBase, ByVal log As IMessageListener)
        If form Is Nothing Then Throw New ArgumentNullException(NameOf(form))
        form.AddListener(log)
        Me.Add(form)
        AddHandler form.FormClosed, AddressOf Me.OnClosed
        form.Show()
    End Sub

    ''' <summary> Adds a form. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="form"> The form. </param>
    ''' <returns> A ListenerFormBase. </returns>
    Public Function AddForm(ByVal form As ListenerFormBase) As ListenerFormBase
        If form Is Nothing Then Throw New ArgumentNullException(NameOf(form))
        Me.Add(form)
        AddHandler form.FormClosed, AddressOf Me.OnClosed
        Return form
    End Function

    ''' <summary> Handles a member form closed event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information to send to registered event handlers. </param>
    Private Sub OnClosed(ByVal sender As Object, ByVal e As EventArgs)
        Dim f As ListenerFormBase = TryCast(sender, ListenerFormBase)
        RemoveHandler f.FormClosed, AddressOf Me.OnClosed
        Me.Remove(f)
        If f IsNot Nothing Then f.Dispose() : f = Nothing
    End Sub

    ''' <summary> Removes the dispose described by value. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub RemoveDispose(ByVal value As ListenerFormBase)
        Dim f As ListenerFormBase = value
        If f IsNot Nothing Then
            RemoveHandler f.FormClosed, AddressOf Me.OnClosed
            Me.Remove(f)
            f.Dispose()
            f = Nothing
        End If
    End Sub

    ''' <summary>
    ''' Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub ClearDispose()
        Do While Me.Any
            Me.RemoveDispose(Me.Item(0))
            Windows.Forms.Application.DoEvents()
        Loop
    End Sub

End Class

