<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ModalMessageForm

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Private WithEvents _MessageLabel As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me._MessageLabel = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        ' 
        '_MessageLabel
        ' 
        Me._MessageLabel.AllowDrop = True
        Me._MessageLabel.AutoSize = True
        Me._MessageLabel.BackColor = System.Drawing.Color.Transparent
        Me._MessageLabel.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me._MessageLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
        Me._MessageLabel.ForeColor = System.Drawing.Color.Black
        Me._MessageLabel.Location = New System.Drawing.Point(27, 39)
        Me._MessageLabel.Name = "_MessageLabel"
        Me._MessageLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._MessageLabel.Size = New System.Drawing.Size(241, 16)
        Me._MessageLabel.TabIndex = 0
        Me._MessageLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter
        ' 
        'ModalMessageForm
        ' 
        Me.AllowDrop = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7, 13)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(295, 106)
        Me.ControlBox = False
        Me.Controls.Add(Me._MessageLabel)
        Me.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
        Me.ForeColor = System.Drawing.SystemColors.WindowText
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.KeyPreview = True
		Me.Location = New System.Drawing.Point(185, 163)
		Me.MaximizeBox = False
		Me.MinimizeBox = False
        Me.Name = "ModalMessageForm"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ResumeLayout(False)
	End Sub

End Class