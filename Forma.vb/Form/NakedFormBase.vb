Imports System.Drawing
Imports System.Windows.Forms

''' <summary> Form with drop shadow. </summary>
''' <remarks>
''' (c) 2007 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 08/13/2007 1.0.2781 from Nicholas Seward </para><para>
''' http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx. </para><para>
''' http://www.codeproject.com/Articles/1108900/Resize-and-Drag-a-FormBorderStyle-None-Form-in-NET  </para><para>
''' David, 08/13/2007, 1.0.2781. Convert from C#. </para>
''' </remarks>
Partial Public Class NakedFormBase
    Inherits Form

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Specialized default constructor for use only by derived classes. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Protected Sub New()
        MyBase.New()
        Me.InitializeComponent()
    End Sub

    ''' <summary> Initializes the component. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Sub InitializeComponent()
        Me.SuspendLayout()
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(331, 341)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Font = New Font(SystemFonts.MessageBoxFont.FontFamily, 9.75!, FontStyle.Regular, GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = My.Resources.favicon
        Me.Name = "FormBase"
        Me.ResumeLayout(False)
    End Sub

#Region " CLASS STYLE "

    ''' <summary> The enable drop shadow version. </summary>
    Public Const EnableDropShadowVersion As Integer = 5

    ''' <summary> Gets the class style. </summary>
    ''' <value> The class style. </value>
    Protected Property ClassStyle As ClassStyleConstants = ClassStyleConstants.None

    ''' <summary> Adds a drop shadow parameter. </summary>
    ''' <remarks>
    ''' From Code Project: http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx.
    ''' </remarks>
    ''' <value> Options that control the create. </value>
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        <Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Demand,
                                                        Flags:=System.Security.Permissions.SecurityPermissionFlag.UnmanagedCode)>
        Get
            Dim cp As CreateParams = MyBase.CreateParams
            cp.ClassStyle = cp.ClassStyle Or CInt(Me.ClassStyle)
            Return cp
        End Get
    End Property

#End Region

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary> The test on border. </summary>
    Private _TestOnBorder As OnBorder

    ''' <summary> Width of the border. </summary>
    Private ReadOnly _BorderWidth As Integer = 8

    ''' <summary> In panel border. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="pos"> The position. </param>
    ''' <returns> An OnBorder. </returns>
    Private Function InPanelBorder(ByVal pos As Point) As OnBorder
        Dim pointMe As Point = New Point(0, 0)
        Dim result As OnBorder = OnBorder.None
        If pos.Y < pointMe.Y + Me._BorderWidth Then result = result Or OnBorder.Top
        If pos.Y > pointMe.Y + Me.Height - Me._BorderWidth Then result = result Or OnBorder.Bottom
        If pos.X < pointMe.X + Me._BorderWidth Then result = result Or OnBorder.Left
        If pos.X > pointMe.X + Me.Width - Me._BorderWidth Then result = result Or OnBorder.Right
        Return result
    End Function

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.MouseMove" /> event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnMouseMove(e As MouseEventArgs)
        If e Is Nothing Then Return
        Me._TestOnBorder = Me.InPanelBorder(e.Location)
        Select Case Me._TestOnBorder
            Case OnBorder.None
                Me.Cursor = Cursors.Arrow
            Case OnBorder.Top
                Me.Cursor = Cursors.SizeNS
            Case OnBorder.Right
                Me.Cursor = Cursors.SizeWE
            Case OnBorder.TopRight
                Me.Cursor = Cursors.SizeNESW
            Case OnBorder.RightBottom
                Me.Cursor = Cursors.SizeNWSE
            Case OnBorder.Bottom
                Me.Cursor = Cursors.SizeNS
            Case OnBorder.BottomLeft
                Me.Cursor = Cursors.SizeNESW
            Case OnBorder.Left
                Me.Cursor = Cursors.SizeWE
            Case OnBorder.LeftTop
                Me.Cursor = Cursors.SizeNWSE
        End Select
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.MouseDown" /> event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnMouseDown(e As MouseEventArgs)
        Dim dir As UIntPtr
        Select Case Me._TestOnBorder
            Case OnBorder.Top
                dir = Me.HTTOP
            Case OnBorder.TopRight
                dir = Me.HTTOPRIGHT
            Case OnBorder.Right
                dir = Me.HTRIGHT
            Case OnBorder.RightBottom
                dir = Me.HTBOTTOMRIGHT
            Case OnBorder.Bottom
                dir = Me.HTBOTTOM
            Case OnBorder.BottomLeft
                dir = Me.HTBOTTOMLEFT
            Case OnBorder.Left
                dir = Me.HTLEFT
            Case OnBorder.LeftTop
                dir = Me.HTTOPLEFT
            Case Else
                dir = Me.HT_CAPTION
        End Select
        SafeNativeMethods.ReleaseCapture()
        SafeNativeMethods.SendMessage(Me.Handle, Me.WM_NCLBUTTONDOWN, dir, New IntPtr(0))
    End Sub

#End Region

End Class

