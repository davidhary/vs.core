''' <summary> A form that persists user settings in the Application Settings file. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 01/31/2013, 6.1.4779.x. </para>
''' </remarks>
Public Class ListenerUserFormBase
    Inherits Windows.Forms.Form

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Specialized default constructor for use only by derived classes. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Protected Sub New()
        MyBase.New()
        Me._Talker = New isr.Core.TraceMessageTalker
    End Sub

    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the
    ''' <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.Talker = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

End Class

