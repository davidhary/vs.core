Imports System.Windows.Forms

Imports isr.Core.Forma.ExceptionExtensions
Imports isr.Core.WinForms.BindingExtensions

Partial Public Class FormBase

#Region " COMPLETE EVENT HANDLERS "

    ''' <summary> Takes the binding failed actions. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="binding"> <see cref="Object"/> instance of this
    '''                                              <see cref="Control"/> </param>
    ''' <param name="e">       Binding complete event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overridable Sub OnBindingFailed(ByVal binding As Binding, ByVal e As BindingCompleteEventArgs)
        Dim activity As String = String.Empty
        If binding Is Nothing OrElse e Is Nothing Then Return
        Try
            activity = "setting cancel state"
            e.Cancel = e.BindingCompleteState <> BindingCompleteState.Success
            activity = $"binding {e.Binding.BindingMemberInfo.BindingField}:{e.BindingCompleteContext}:{e.Binding.BindableComponent}:{e.BindingCompleteState}"
            If e.BindingCompleteState = BindingCompleteState.DataError Then
                activity = $"data error; {activity}"
                Debug.Assert(Not Debugger.IsAttached, $"{activity};. {e.ErrorText}")
            ElseIf e.BindingCompleteState = BindingCompleteState.Exception Then
                If Not String.IsNullOrWhiteSpace(e.ErrorText) Then
                    activity = $"{activity}; {e.ErrorText}"
                End If
                Debug.Assert(Not Debugger.IsAttached, $"{activity};. {e.Exception.ToFullBlownString}")
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, $"{activity};. {ex.ToFullBlownString}")
        End Try
    End Sub

    ''' <summary> Takes the binding succeeded actions. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="binding"> <see cref="Object"/> instance of this
    '''                                               <see cref="Control"/> </param>
    ''' <param name="e">       Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnBindingSucceeded(ByVal binding As Binding, ByVal e As BindingCompleteEventArgs)
    End Sub

    ''' <summary> Handles the binding complete event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> <see cref="Object"/> instance of this
    '''                                             <see cref="Control"/> </param>
    ''' <param name="e">      Binding complete event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub HandleBindingCompleteEvent(ByVal sender As Object, ByVal e As BindingCompleteEventArgs)
        Dim activity As String = String.Empty
        Dim binding As Binding = TryCast(sender, Binding)
        If binding Is Nothing OrElse e Is Nothing Then Return
        Try
            If e.BindingCompleteState = BindingCompleteState.Success Then
                activity = $"handling the binding {e.BindingCompleteState} event"
                Me.OnBindingSucceeded(binding, e)
            ElseIf TypeOf (e.Exception) Is System.InvalidOperationException AndAlso binding.Control.InvokeRequired Then
                activity = $"attempting to handle binding cross thread exception"
                ' try to handle the cross thread situation
                binding.Control.Invoke(Sub() binding.ReadValue())
            Else
                activity = $"handling the binding {e.BindingCompleteState} event"
                Me.OnBindingFailed(binding, e)
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, $"{activity};. {ex.ToFullBlownString}")
        End Try
    End Sub

#End Region

#Region " ADD and REMOVE "

    ''' <summary>
    ''' Adds or removes binding from a <see cref="IBindableComponent">bindable componenet</see>
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="component"> The bindable control. </param>
    ''' <param name="add">       True to add; otherwise, remove. </param>
    ''' <param name="binding">   The binding. </param>
    ''' <returns> A Binding. </returns>
    Public Function AddRemoveBinding(ByVal component As IBindableComponent, ByVal add As Boolean, ByVal binding As Binding) As Binding
        component.AddRemoveBinding(add, binding, AddressOf Me.HandleBindingCompleteEvent)
        Return binding
    End Function

    ''' <summary>
    ''' Adds a formatted binding to a <see cref="IBindableComponent">bindable componenet</see>
    ''' </summary>
    ''' <remarks> Enabling formatting also enables the binding complete event. </remarks>
    ''' <param name="component">    The bindable control. </param>
    ''' <param name="add">          True to add; otherwise, remove. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    ''' <param name="dataSource">   The data source. </param>
    ''' <param name="dataMember">   The data member. </param>
    ''' <returns> A Binding. </returns>
    Public Function AddRemoveBinding(ByVal component As IBindableComponent, ByVal add As Boolean, ByVal propertyName As String,
                                     ByVal dataSource As Object, ByVal dataMember As String) As Binding
        ' must set formatting enabled to true to enable the binding complete event.
        Return Me.AddRemoveBinding(component, add,
                                   New Binding(propertyName, dataSource, dataMember, True) With {.ControlUpdateMode = ControlUpdateMode.OnPropertyChanged,
                                                                                                 .DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged})
    End Function

    ''' <summary>
    ''' Adds a formatted binding to a <see cref="IBindableComponent">bindable componenet</see>
    ''' </summary>
    ''' <remarks> Enabling formatting also enables the binding complete event. </remarks>
    ''' <param name="component">            The bindable control. </param>
    ''' <param name="add">                  True to add; otherwise, remove. </param>
    ''' <param name="propertyName">         Name of the property. </param>
    ''' <param name="dataSource">           The data source. </param>
    ''' <param name="dataMember">           The data member. </param>
    ''' <param name="dataSourceUpdateMode"> The data source update mode. </param>
    ''' <returns> A Binding. </returns>
    Public Function AddRemoveBinding(ByVal component As IBindableComponent, ByVal add As Boolean, ByVal propertyName As String,
                                     ByVal dataSource As Object, ByVal dataMember As String, ByVal dataSourceUpdateMode As DataSourceUpdateMode) As Binding
        ' must set formatting enabled to true to enable the binding complete event.
        Return Me.AddRemoveBinding(component, add, New Binding(propertyName, dataSource, dataMember, True, dataSourceUpdateMode))
    End Function

    ''' <summary>
    ''' Adds a formatted binding to a <see cref="IBindableComponent">bindable componenet</see>
    ''' </summary>
    ''' <remarks> Enabling formatting also enables the binding complete event. </remarks>
    ''' <param name="component">            The bindable control. </param>
    ''' <param name="add">                  True to add; otherwise, remove. </param>
    ''' <param name="propertyName">         Name of the property. </param>
    ''' <param name="dataSource">           The data source. </param>
    ''' <param name="dataMember">           The data member. </param>
    ''' <param name="dataSourceUpdateMode"> The data source update mode. </param>
    ''' <param name="controlUpdateMode">    The control update mode. </param>
    ''' <returns> A Binding. </returns>
    Public Function AddRemoveBinding(ByVal component As IBindableComponent, ByVal add As Boolean, ByVal propertyName As String,
                                     ByVal dataSource As Object, ByVal dataMember As String,
                                     ByVal dataSourceUpdateMode As DataSourceUpdateMode, ByVal controlUpdateMode As ControlUpdateMode) As Binding
        ' must set formatting enabled to true to enable the binding complete event.
        Return Me.AddRemoveBinding(component, add, New Binding(propertyName, dataSource, dataMember, True, dataSourceUpdateMode) With {.ControlUpdateMode = controlUpdateMode})
    End Function

    ''' <summary>
    ''' Adds a formatted binding to a <see cref="IBindableComponent">bindable componenet</see>
    ''' </summary>
    ''' <remarks> Enabling formatting also enables the binding complete event. </remarks>
    ''' <param name="component">    The bindable control. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    ''' <param name="dataSource">   The data source. </param>
    ''' <param name="dataMember">   The data member. </param>
    ''' <returns> A Binding. </returns>
    Public Function AddBinding(ByVal component As IBindableComponent, ByVal propertyName As String, ByVal dataSource As Object, ByVal dataMember As String) As Binding
        Return Me.AddRemoveBinding(component, True,
                                   New Binding(propertyName, dataSource, dataMember, True) With {.ControlUpdateMode = ControlUpdateMode.OnPropertyChanged,
                                                                                                 .DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged})
    End Function

    ''' <summary>
    ''' Adds a formatted binding to a <see cref="IBindableComponent">bindable componenet</see>
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="component"> The bindable control. </param>
    ''' <param name="binding">   The binding. </param>
    ''' <returns> A Binding. </returns>
    Public Function AddBinding(ByVal component As IBindableComponent, ByVal binding As Binding) As Binding
        Return Me.AddRemoveBinding(component, True, binding)
    End Function

    ''' <summary>
    ''' removes binding from a <see cref="IBindableComponent">bindable componenet</see>
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="component">    The bindable component. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    ''' <param name="dataSource">   The data source. </param>
    ''' <param name="dataMember">   The data member. </param>
    ''' <returns> A Binding. </returns>
    Public Function RemoveBinding(ByVal component As IBindableComponent, ByVal propertyName As String, ByVal dataSource As Object, ByVal dataMember As String) As Binding
        Return Me.AddRemoveBinding(component, False, New Binding(propertyName, dataSource, dataMember, True))
    End Function

    ''' <summary>
    ''' removes binding from a <see cref="IBindableComponent">bindable componenet</see>
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="component"> The bindable component. </param>
    ''' <param name="binding">   The binding. </param>
    ''' <returns> A Binding. </returns>
    Public Function RemoveBinding(ByVal component As IBindableComponent, ByVal binding As Binding) As Binding
        Return Me.AddRemoveBinding(component, False, binding)
    End Function

#End Region

#Region " CONVERT EVENT HANDLERS "

    ''' <summary> Displays universal time value as local time. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> <see cref="Object"/> instance of this
    '''                                             <see cref="Control"/> </param>
    ''' <param name="e">      Convert event information. </param>
    Protected Sub DisplayLocalTime(sender As Object, e As ConvertEventArgs)
        e.DisplayLocalTime
    End Sub

    ''' <summary> Parse a local time string to universal time. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> <see cref="Object"/> instance of this
    '''                                             <see cref="Control"/> </param>
    ''' <param name="e">      Convert event information. </param>
    Protected Sub ParseLocalTime(sender As Object, e As ConvertEventArgs)
        e.ParseLocalTime
    End Sub

#End Region

End Class
