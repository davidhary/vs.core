Imports System.Drawing
Imports System.Windows.Forms

Imports isr.Core.Forma.ExceptionExtensions

''' <summary> Form with fade in and out capabilities. </summary>
''' <remarks>
''' Features: <para>
''' fades in on open; </para><para>
''' fades out on close; </para><para>
''' partially fades out on focus lost;</para><para>
''' fades in on focus; </para><para>
''' fades out on minimize; </para><para>
''' fades in on restore;</para><para>
''' must not annoy the user. </para><para>
''' Usage: To use FadeForm, just extend it instead of Form and you are ready to go. It is
''' defaulted to use the fade-in from nothing on open and out to nothing on close. It will also
''' fade to 85% opacity when not the active window. Fading can be disabled and enabled with two
''' methods setting the default enable and disable modes.</para> (c) 2007 Integrated Scientific
''' Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 08/13/2007 1.0.2781. from Nicholas Seward </para><para>
''' http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx.  </para><para>
''' David, 08/13/2007, 1.0.2781. Convert from C#. </para>
''' </remarks>
Partial Public Class FadeFormBase
    Inherits Form

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Specialized default constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Protected Sub New()
        MyBase.New()
        Me._FadeTime = 0.35
        Me._ActiveOpacity = 1
        Me._InactiveOpacity = 0.85
        Me.InitializeComponent()
    End Sub

    ''' <summary> Initializes the component. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Sub InitializeComponent()
        Me.SuspendLayout()
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(331, 341)
        Me.Font = New Font(SystemFonts.MessageBoxFont.FontFamily, 9.75!, FontStyle.Regular, GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = My.Resources.favicon
        Me.Name = "FadeFormBase"
        Me._Timer = New Timer() With {.Interval = 25}
        Me.ResumeLayout(False)
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me._Timer IsNot Nothing Then Me._Timer.Dispose() : Me._Timer = Nothing
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#Region " CLASS STYLE "

    ''' <summary> The enable drop shadow version. </summary>
    Public Const EnableDropShadowVersion As Integer = 5

    ''' <summary> Gets the class style. </summary>
    ''' <value> The class style. </value>
    Protected Property ClassStyle As ClassStyleConstants = ClassStyleConstants.None

    ''' <summary> Adds a drop shadow parameter. </summary>
    ''' <remarks>
    ''' From Code Project: http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx.
    ''' </remarks>
    ''' <value> Options that control the create. </value>
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        <System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Demand,
                                                        Flags:=System.Security.Permissions.SecurityPermissionFlag.UnmanagedCode)>
        Get
            Dim cp As CreateParams = MyBase.CreateParams
            cp.ClassStyle = cp.ClassStyle Or Me.ClassStyle
            Return cp
        End Get
    End Property

#End Region

#End Region

#Region " FADING PROPERTIES "

    ''' <summary> The active opacity. </summary>
    Private _ActiveOpacity As Double

    ''' <summary>
    ''' Gets or sets the opacity that the form will transition to when the form gets focus.
    ''' </summary>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <value> The active opacity. </value>
    Public Property ActiveOpacity() As Double
        Get
            Return Me._ActiveOpacity
        End Get
        Set(ByVal value As Double)
            If value >= 0 AndAlso value <= 1 Then
                Me._ActiveOpacity = value
            Else
                Throw New ArgumentOutOfRangeException(NameOf(value), "The Active Opacity must be between 0 and 1.")
            End If
            If Me.ContainsFocus Then
                Me.TargetOpacity = Me._ActiveOpacity
            End If
        End Set
    End Property

    ''' <summary> The fade time. </summary>
    Private _FadeTime As Double

    ''' <summary>
    ''' Gets or sets the time it takes to fade from 1 to 0 or the other way around.
    ''' </summary>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <value> The fade time. </value>
    Public Property FadeTime() As Double
        Get
            Return Me._FadeTime
        End Get
        Set(ByVal value As Double)
            If value > 0 Then
                Me._FadeTime = value
            Else
                Throw New ArgumentOutOfRangeException(NameOf(value), "The FadeTime must be a positive value.")
            End If
        End Set
    End Property

    ''' <summary> The inactive opacity. </summary>
    Private _InactiveOpacity As Double

    ''' <summary>
    ''' Gets or sets the opacity that the form will transition to when the form doesn't have focus.
    ''' </summary>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <value> The inactive opacity. </value>
    Public Property InactiveOpacity() As Double
        Get
            Return Me._InactiveOpacity
        End Get
        Set(ByVal value As Double)
            If value >= 0 AndAlso value <= 1 Then
                Me._InactiveOpacity = value
            Else
                Throw New ArgumentOutOfRangeException(NameOf(value), "The InactiveOpacity must be between 0 and 1.")
            End If
            If (Not Me.ContainsFocus) AndAlso Me.WindowState <> FormWindowState.Minimized Then
                Me.TargetOpacity = Me._InactiveOpacity
            End If
        End Set
    End Property

    ''' <summary> The minimized opacity. </summary>
    Private _MinimizedOpacity As Double

    ''' <summary>
    ''' Gets or sets the opacity that the form will transition to when the form is minimized.
    ''' </summary>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <value> The minimized opacity. </value>
    Public Property MinimizedOpacity() As Double
        Get
            Return Me._MinimizedOpacity
        End Get
        Set(ByVal value As Double)
            If value >= 0 AndAlso value <= 1 Then
                Me._MinimizedOpacity = value
            Else
                Throw New ArgumentOutOfRangeException(NameOf(value), "The MinimizedOpacity must be between 0 and 1.")
            End If
            If (Not Me.ContainsFocus) AndAlso Me.WindowState <> FormWindowState.Minimized Then
                Me.TargetOpacity = Me._InactiveOpacity
            End If
        End Set
    End Property

    ''' <summary> Target opacity. </summary>
    Private _TargetOpacity As Double

    ''' <summary> Gets or sets the opacity the form is transitioning to. </summary>
    ''' <remarks>
    ''' Setting this value amounts also to a one-time fade to any value. The opacity is never
    ''' actually 1. This is hack to keep the window from flashing black. The cause of this is not
    ''' clear.
    ''' </remarks>
    ''' <value> The target opacity. </value>
    Public Property TargetOpacity() As Double
        Get
            Return Me._TargetOpacity
        End Get
        Set(ByVal value As Double)
            Me._TargetOpacity = value
            If (Not Me._Timer.Enabled) Then
                Me._Timer.Start()
            End If
        End Set
    End Property

#End Region

#Region " FADING CONTROL "

    ''' <summary> Turns off opacity fading. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub DisableFade()
        Me._ActiveOpacity = 1
        Me._InactiveOpacity = 1
        Me._MinimizedOpacity = 1
    End Sub

    ''' <summary> Turns on opacity fading. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub EnableFadeDefaults()
        Me._ActiveOpacity = 1
        Me._InactiveOpacity = 0.85
        Me._MinimizedOpacity = 0
        Me._FadeTime = 0.35
    End Sub

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary>
    ''' Gets or sets the sentinel indicating that the form loaded without an exception. Should be set
    ''' only if load did not fail.
    ''' </summary>
    ''' <value> The is loaded. </value>
    Protected Property IsLoaded As Boolean

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.Form.Load" /> event after reading the start
    ''' position from the configuration file.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnLoad(e As System.EventArgs)
        Try
            Me.Opacity = Me.MinimizedOpacity
            Me.TargetOpacity = Me.ActiveOpacity
            Me.IsLoaded = True
        Catch
            Throw
        Finally
            MyBase.OnLoad(e)
        End Try
    End Sub

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.Form.Activated" /> event. Fades in the form when
    ''' it gains focus.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnActivated(e As System.EventArgs)
        Me.TargetOpacity = Me.ActiveOpacity
        MyBase.OnActivated(e)
    End Sub

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.Form.Deactivate" /> event. Fades out the form
    ''' when it losses focus.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> The <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnDeactivate(e As System.EventArgs)
        Me.TargetOpacity = Me.InactiveOpacity
        MyBase.OnDeactivate(e)
    End Sub

#End Region

#Region " OPACITY TIMER "

#Disable Warning IDE1006 ' Naming Styles
    ''' <summary> Timer to aid in fade effects. </summary>
    Private WithEvents _Timer As Timer
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Performs fade increment. </summary>
    ''' <remarks>
    ''' The timer is stopped after reaching the target opacity. The timer gets started whenever the
    ''' <see cref="TargetOpacity">target opacity</see> is set method. The timer is stopped to
    ''' conserve processor power when not needed.
    ''' </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Timer_Tick(ByVal sender As Object, ByVal e As EventArgs) Handles _Timer.Tick

        Dim fadeChangePerTick As Double = Me._Timer.Interval * 1.0 / 1000 / Me._FadeTime
        ' Check to see if it is time to stop the timer
        If Math.Abs(Me._TargetOpacity - Me.Opacity) < fadeChangePerTick Then
            'Stop the timer to save processor.
            Me._Timer.Stop()
            'There is an ugly black flash if you set the Opacity to 1.0
            Me.Opacity = If(Me._TargetOpacity = 1, 0.999, Me.TargetOpacity)
            'Process held Windows Message.
            MyBase.WndProc(Me.heldMessage)
            Me.heldMessage = New Message()
        ElseIf Me.TargetOpacity > Me.Opacity Then
            Me.Opacity += fadeChangePerTick
        ElseIf Me.TargetOpacity < Me.Opacity Then
            Me.Opacity -= fadeChangePerTick
        End If
    End Sub

#End Region

End Class

