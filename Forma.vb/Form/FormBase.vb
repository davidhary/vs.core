Imports System.Drawing
Imports System.Windows.Forms

''' <summary> Form with drop shadow. </summary>
''' <remarks>
''' (c) 2007 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 08/13/2007 1.0.2781. from Nicholas Seward. </para><para>
''' http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx.  </para><para>
''' David, 08/13/2007,1.0.2781. Convert from C#. </para>
''' </remarks>
Public Class FormBase
    Inherits Form

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Gets the initializing components sentinel. </summary>
    ''' <value> The initializing components sentinel. </value>
    Protected Property InitializingComponents As Boolean

    ''' <summary> Specialized default constructor for use only by derived classes. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Protected Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        Me.InitializeComponent()
        Me.InitializingComponents = False
    End Sub

    ''' <summary> Initializes the component. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Sub InitializeComponent()
        Me.SuspendLayout()
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(331, 341)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Font = New Font(SystemFonts.MessageBoxFont.FontFamily, 9.75!, FontStyle.Regular, GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = My.Resources.favicon
        Me.Name = "FormBase"
        Me.ResumeLayout(False)
    End Sub

#Region " CLASS STYLE "

    ''' <summary> The enable drop shadow version. </summary>
    Public Const EnableDropShadowVersion As Integer = 5

    ''' <summary> Gets the class style. </summary>
    ''' <value> The class style. </value>
    Protected Property ClassStyle As ClassStyleConstants = ClassStyleConstants.None

    ''' <summary> Adds a drop shadow parameter. </summary>
    ''' <remarks>
    ''' From Code Project: http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx.
    ''' </remarks>
    ''' <value> Options that control the create. </value>
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        <Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Demand,
                                                        Flags:=System.Security.Permissions.SecurityPermissionFlag.UnmanagedCode)>
        Get
            Dim cp As CreateParams = MyBase.CreateParams
            cp.ClassStyle = cp.ClassStyle Or CInt(Me.ClassStyle)
            Return cp
        End Get
    End Property

#End Region

#End Region

End Class

''' <summary> Values that represent class style constants. </summary>
''' <remarks> David, 2020-09-24. </remarks>
<Flags>
Public Enum ClassStyleConstants

    ''' <summary> . </summary>
    <System.ComponentModel.Description("Not Specified")> None = 0

    ''' <summary> . </summary>
    <System.ComponentModel.Description("No close button")> HideCloseButton = &H200

    ''' <summary> . </summary>
    <System.ComponentModel.Description("Drop Shadow")> DropShadow = &H20000 ' 131072
End Enum
