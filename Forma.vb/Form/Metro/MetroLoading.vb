﻿Imports System.Windows.Forms

''' <summary> A metro loading. </summary>
''' <remarks> David, 2020-09-24. </remarks>
Partial Public Class MetroLoading
    Inherits UserControl

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        Me.InitializeComponent()
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the isr.Core.Controls.MetroLoading and optionally
    ''' releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

End Class
