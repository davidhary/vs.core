
Partial Public Class MetroProgressBar

    ''' <summary> 
    ''' Required designer variable.
    ''' </summary>
    Private components As System.ComponentModel.IContainer = Nothing

#Region "Component Designer generated code"

    ''' <summary>
    ''' Required method for Designer support - do not modify the contents of this method with the
    ''' code editor.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MetroProgressBar))
        Me._PictureBox = New System.Windows.Forms.PictureBox()
        CType(Me._PictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_PictureBox
        '
        Me._PictureBox.BackColor = System.Drawing.Color.Transparent
        Me._PictureBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._PictureBox.Image = CType(resources.GetObject("_pictureBox.Image"), System.Drawing.Image)
        Me._PictureBox.Location = New System.Drawing.Point(0, 0)
        Me._PictureBox.Name = "_PictureBox"
        Me._PictureBox.Size = New System.Drawing.Size(308, 20)
        Me._PictureBox.TabIndex = 0
        Me._PictureBox.TabStop = False
        '
        'MetroProgressBar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Transparent
        Me.Controls.Add(Me._PictureBox)
        Me.Name = "MetroProgressBar"
        Me.Size = New System.Drawing.Size(308, 20)
        CType(Me._PictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    ''' <summary> The picture box control. </summary>
    Private _PictureBox As System.Windows.Forms.PictureBox
End Class
