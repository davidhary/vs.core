Imports System.Windows.Forms
Imports System.ComponentModel
Imports isr.Core.Forma.ExceptionExtensions

''' <summary>
''' A console form to host <see cref="ModelViewBase"/> or <see cref="ModelViewTalkerBase"/>
''' controls.
''' </summary>
''' <remarks>
''' (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 02/07/2005, 2.0.2597.x. </para>
''' </remarks>
Public Class ConsoleForm
    Inherits ListenerFormBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        Me.InitializeComponent()
        Me.InitializingComponents = False
        ' set defaults for the messages box.
        Me._TraceMessagesBox.ContainerPanel = Me._MessagesTabPage
        Me._TraceMessagesBox.MaxSynopsisLength = 0
        Me._TraceMessagesBox.AlertSoundEnabled = False
        Me._TraceMessagesBox.AlertLevel = TraceEventType.Warning
        ' Me._TraceMessagesBox.AlertsToggleControl = Me._AlertsToolStripTextBox.TextBox
        Me._TraceMessagesBox.TabCaption = "Log"
        Me._TraceMessagesBox.CaptionFormat = "{0} " & System.Text.Encoding.GetEncoding(437).GetString(New Byte() {240})
        Me._TraceMessagesBox.ResetCount = 1000
        Me._TraceMessagesBox.PresetCount = 500
        Me._TraceMessagesBox.AssignLog(My.MyLibrary.Logger)
        Me._TraceMessagesBox.WordWrap = False
        Me._TraceMessagesBox.CommenceUpdates()
    End Sub

    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the
    ''' <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me._TraceMessagesBox.SuspendUpdatesReleaseIndicators()
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.Form.Closing" /> event. Releases all publishers.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.ComponentModel.CancelEventArgs" /> that contains the
    '''                  event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub OnClosing(ByVal e As System.ComponentModel.CancelEventArgs)
        Try
            Me.Cursor = Cursors.WaitCursor
            If e IsNot Nothing AndAlso Not e.Cancel Then
                ' removes the private text box listener
                Me.RemovePrivateListener(Me._TraceMessagesBox)
                If Me.PropertyNotifyControl IsNot Nothing Then
                    Me._Layout.Controls.Remove(Me.PropertyNotifyControl)
                End If
                If Me._PropertyNotifyControlDisposeEnabled Then
                    Me.PropertyNotifyControl.Dispose()
                End If
                If Me._UserControlDisposeEnabled AndAlso Me.UserControl IsNot Nothing Then
                    Me.UserControl.Dispose()
                End If
                Me.UserControl = Nothing
                If Me.ModelView IsNot Nothing Then
                    ' removes the private listeners of the talker control hosted in this form.
                    Me.ModelView.RemovePrivateListeners()
                    Me._Layout.Controls.Remove(Me.ModelView)
                    If Me._TalkerControlDisposeEnabled Then
                        Me.ModelView.Dispose()
                    End If
                End If
                Me.ModelView = Nothing
            End If
        Finally
            Windows.Forms.Application.DoEvents()
            MyBase.OnClosing(e)
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary>
    ''' Called upon receiving the <see cref="E:System.Windows.Forms.Form.Load" /> event.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Name} loading"
            Me.Cursor = Cursors.WaitCursor
            Trace.CorrelationManager.StartLogicalOperation(Reflection.MethodInfo.GetCurrentMethod.Name)

            ' set the form caption
            activity = $"{Me.Name} displaying title (text)"
            Me.Text = $"{My.Application.Info.ProductName} release {My.Application.Info.Version}"

            ' default to center screen.
            activity = $"{Me.Name} loading; centering to screen"
            Me.CenterToScreen()

        Catch ex As Exception
            Me.PublishException(activity, ex)
            If MyDialogResult.Abort = MyMessageBox.ShowDialogAbortIgnore(ex.ToFullBlownString, "Exception Occurred", MyMessageBoxIcon.Error) Then
                Application.Exit()
            End If
        Finally
            MyBase.OnLoad(e)
            Trace.CorrelationManager.StopLogicalOperation()
            Me.Cursor = Cursors.Default
        End Try

    End Sub

    ''' <summary>
    ''' Called upon receiving the <see cref="E:System.Windows.Forms.Form.Shown" /> event.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub OnShown(e As System.EventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"showing {Me.Name}"
            Me.Cursor = Cursors.WaitCursor
            Trace.CorrelationManager.StartLogicalOperation(Reflection.MethodInfo.GetCurrentMethod.Name)

            ' allow form rendering time to complete: process all messages currently in the queue.
            Windows.Forms.Application.DoEvents()

            If Not Me.DesignMode Then
            End If
            activity = "Ready to open Visa Session"
            Me.PublishVerbose($"{activity};. ")

        Catch ex As Exception
            Me.PublishException(activity, ex)
            If MyDialogResult.Abort = MyMessageBox.ShowDialogAbortIgnore(ex.ToFullBlownString, "Exception Occurred", MyMessageBoxIcon.Error) Then
                Application.Exit()
            End If
        Finally
            MyBase.OnShown(e)
            Trace.CorrelationManager.StopLogicalOperation()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Resize client area. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="title">              The title. </param>
    ''' <param name="clientControl">      The client control. </param>
    ''' <param name="usingTraceMessages"> True to using trace messages. </param>
    Public Sub ResizeClientArea(ByVal title As String, ByVal clientControl As Control, ByVal usingTraceMessages As Boolean)
        If clientControl Is Nothing Then Throw New ArgumentNullException(NameOf(clientControl))
        If usingTraceMessages Then
            Me.ResizeClientArea(Me._Tabs, Me._TraceMessagesBox, clientControl)
            Me._ViewTabPage.Text = title
            Me._ViewTabPage.ToolTipText = title
            Me._Layout.Controls.Add(clientControl, 1, 1)
        Else
            Me.ResizeClientArea(clientControl)
        End If
    End Sub

    ''' <summary> Resize client area. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="clientControl"> The client control. </param>
    Private Sub ResizeClientArea(ByVal clientControl As Control)
        If clientControl Is Nothing Then Throw New ArgumentNullException(NameOf(clientControl))
        Me._Tabs.Visible = False
        Me.Controls.Add(clientControl)
        Dim clientMargins As New Drawing.Size(0, 0)
        Dim controlMargins As New Drawing.Size(0, 0)
        Dim newClientSize As Drawing.Size
        clientControl.BringToFront()
        newClientSize = New Drawing.Size(clientControl.Width + clientMargins.Width, Me.Height)
        newClientSize = New Drawing.Size(newClientSize.Width, clientControl.Height + clientMargins.Height + Me._StatusStrip.Height)
        Dim resizeRequired As Boolean = True
        If resizeRequired Then
            Me.ClientSize = New Drawing.Size(newClientSize.Width + controlMargins.Width, newClientSize.Height + controlMargins.Height)
            Me.Refresh()
        End If
    End Sub

    ''' <summary> Resize client area. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="tabControl">    The tab control. </param>
    ''' <param name="messagesBox">   The messages box control. </param>
    ''' <param name="clientControl"> The client control. </param>
    Private Sub ResizeClientArea(ByVal tabControl As TabControl, ByVal messagesBox As Control, ByVal clientControl As Control)
        If messagesBox Is Nothing Then Throw New ArgumentNullException(NameOf(messagesBox))
        If tabControl Is Nothing Then Throw New ArgumentNullException(NameOf(tabControl))
        tabControl.BringToFront()
        If clientControl Is Nothing Then Throw New ArgumentNullException(NameOf(clientControl))
        Dim tabsMargins As New Drawing.Size(tabControl.Width - messagesBox.Width, tabControl.Height - messagesBox.Height)
        Dim controlMargins As New Drawing.Size(Me.ClientSize.Width - Me._Layout.Width, Me.ClientSize.Height - Me._Layout.Height)
        Dim newClientSize As Drawing.Size
        newClientSize = New Drawing.Size(clientControl.Width + tabsMargins.Width, tabControl.Height)
        newClientSize = New Drawing.Size(newClientSize.Width, clientControl.Height + tabsMargins.Height)
        Dim resizeRequired As Boolean = True
        If resizeRequired Then
            tabControl.Size = newClientSize
            tabControl.Refresh()
            Me.ClientSize = New Drawing.Size(newClientSize.Width + controlMargins.Width, newClientSize.Height + controlMargins.Height)
            Me.Refresh()
        End If
    End Sub

#End Region

#Region " USER CONTROL "

    ''' <summary> true to enable, false to disable the disposal of the User Control. </summary>
    Private _UserControlDisposeEnabled As Boolean

    ''' <summary> The user control. </summary>
    Private WithEvents UserControl As UserControl

    ''' <summary> Adds a User Control. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub AddUserControl(ByVal value As UserControl)
        Me.AddUserControl("UI", value, True)
    End Sub

    ''' <summary> Adds a User Control. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="title"> The title. </param>
    ''' <param name="value"> The value. </param>
    Public Sub AddUserControl(ByVal title As String, ByVal value As UserControl)
        Me.AddUserControl(title, value, True)
    End Sub

    ''' <summary> Adds a User Control. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="title">             The title. </param>
    ''' <param name="value">             The value. </param>
    ''' <param name="disposeEnabled">    true to enable, false to disable the dispose. </param>
    ''' <param name="showTraceMessages"> True to show, false to hide the trace messages. </param>
    Public Sub AddUserControl(ByVal title As String, ByVal value As UserControl, ByVal disposeEnabled As Boolean, ByVal showTraceMessages As Boolean)
        Me.ResizeClientArea(title, value, showTraceMessages)
        Me.UserControl = value
        If showTraceMessages Then Me.AddPrivateListeners()
        Me._UserControlDisposeEnabled = disposeEnabled

        Me.UserControl.Dock = Windows.Forms.DockStyle.Fill
        Me.UserControl.TabIndex = 0
        Me.UserControl.BackColor = System.Drawing.Color.Transparent
        Me.UserControl.Font = New System.Drawing.Font(Me.Font, System.Drawing.FontStyle.Regular)
        Me.UserControl.Name = "_UserControl"
    End Sub

    ''' <summary> Adds a User Control. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="title">          The title. </param>
    ''' <param name="value">          The value. </param>
    ''' <param name="disposeEnabled"> true to enable, false to disable the dispose. </param>
    Public Sub AddUserControl(ByVal title As String, ByVal value As UserControl, ByVal disposeEnabled As Boolean)
        Me.AddUserControl(title, value, disposeEnabled, True)
    End Sub

#End Region

#Region " PROPERTY NOTIFY CONTROL "

    ''' <summary> true to enable, false to disable the disposal of the property notify control. </summary>
    Private _PropertyNotifyControlDisposeEnabled As Boolean

    ''' <summary> The property notify control. </summary>
    Private WithEvents PropertyNotifyControl As ModelViewBase

    ''' <summary> Adds a property notification control. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub AddPropertyNotifyControl(ByVal value As ModelViewBase)
        Me.AddPropertyNotifyControl("Instrument", value, True)
    End Sub

    ''' <summary> Adds a property notification control. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="title"> The title. </param>
    ''' <param name="value"> The value. </param>
    Public Sub AddPropertyNotifyControl(ByVal title As String, ByVal value As ModelViewBase)
        Me.AddPropertyNotifyControl(title, value, True)
    End Sub

    ''' <summary> Adds a property notification control. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="title">             The title. </param>
    ''' <param name="value">             The value. </param>
    ''' <param name="disposeEnabled">    true to enable, false to disable the dispose. </param>
    ''' <param name="showTraceMessages"> True to show, false to hide the trace messages. </param>
    Public Sub AddPropertyNotifyControl(ByVal title As String, ByVal value As ModelViewBase, ByVal disposeEnabled As Boolean, ByVal showTraceMessages As Boolean)
        Me.ResizeClientArea(title, value, showTraceMessages)
        Me.PropertyNotifyControl = value
        If showTraceMessages Then Me.AddPrivateListeners()
        Me._PropertyNotifyControlDisposeEnabled = disposeEnabled

        Me.PropertyNotifyControl.Dock = Windows.Forms.DockStyle.Fill
        Me.PropertyNotifyControl.TabIndex = 0
        Me.PropertyNotifyControl.BackColor = System.Drawing.Color.Transparent
        Me.PropertyNotifyControl.Font = New System.Drawing.Font(Me.Font, System.Drawing.FontStyle.Regular)
        Me.PropertyNotifyControl.Name = "_PropertyNotifyControl"
    End Sub

    ''' <summary> Adds a property notification control. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="title">          The title. </param>
    ''' <param name="value">          The value. </param>
    ''' <param name="disposeEnabled"> true to enable, false to disable the dispose. </param>
    Public Sub AddPropertyNotifyControl(ByVal title As String, ByVal value As ModelViewBase, ByVal disposeEnabled As Boolean)
        Me.AddPropertyNotifyControl(title, value, disposeEnabled, True)
    End Sub

    ''' <summary> Executes the property change action. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender">       The sender. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(sender As ModelViewBase, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(ModelViewBase.StatusPrompt)
                Me.StatusPrompt = sender.StatusPrompt
        End Select
    End Sub

    ''' <summary> Property view property changed. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub PropertyNotifyControlPropertyChanged(sender As Object, e As PropertyChangedEventArgs) Handles PropertyNotifyControl.PropertyChanged
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(Forma.ModelViewBase)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.PropertyNotifyControlPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, Forma.ModelViewBase), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " MODEL VIEW TALKER "

    ''' <summary> true to enable, false to disable the disposal of a talker control. </summary>
    Private _TalkerControlDisposeEnabled As Boolean

        Private WithEvents ModelView As ModelViewTalkerBase

    ''' <summary> Adds a talker control panel. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub AddTalkerControl(ByVal value As ModelViewTalkerBase)
        Me.AddTalkerControl("View", value, True)
    End Sub

    ''' <summary> Adds an talker control. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="title"> The title. </param>
    ''' <param name="value"> The value. </param>
    Public Sub AddTalkerControl(ByVal title As String, ByVal value As ModelViewTalkerBase)
        Me.AddTalkerControl(title, value, True)
    End Sub

    ''' <summary> Adds an talker control. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="title">             The title. </param>
    ''' <param name="value">             The value. </param>
    ''' <param name="disposeEnabled">    true to enable, false to disable the dispose. </param>
    ''' <param name="showTraceMessages"> True to show, false to hide the trace messages. </param>
    Public Sub AddTalkerControl(ByVal title As String, ByVal value As ModelViewTalkerBase, ByVal disposeEnabled As Boolean, ByVal showTraceMessages As Boolean)
        Me.ResizeClientArea(title, value, showTraceMessages)
        Me.ModelView = value
        If showTraceMessages Then Me.AddPrivateListeners()
        Me._TalkerControlDisposeEnabled = disposeEnabled

        Me.ModelView.Dock = Windows.Forms.DockStyle.Fill
        Me.ModelView.TabIndex = 0
        Me.ModelView.BackColor = System.Drawing.Color.Transparent
        Me.ModelView.Font = New System.Drawing.Font(Me.Font, System.Drawing.FontStyle.Regular)
        Me.ModelView.Name = "_TalkerControl"
        Me.ModelView.AssignTalker(Me.Talker)
    End Sub

    ''' <summary> Adds an talker control. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="title">          The title. </param>
    ''' <param name="value">          The value. </param>
    ''' <param name="disposeEnabled"> true to enable, false to disable the dispose. </param>
    Public Sub AddTalkerControl(ByVal title As String, ByVal value As ModelViewTalkerBase, ByVal disposeEnabled As Boolean)
        Me.AddTalkerControl(title, value, disposeEnabled, True)
    End Sub

    ''' <summary> Executes the property change action. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender">       The sender. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(sender As ModelViewTalkerBase, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(ModelViewBase.StatusPrompt)
                Me.StatusPrompt = sender.StatusPrompt
        End Select
    End Sub

    ''' <summary> talker control property changed. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ModelViewPropertyChanged(sender As Object, e As PropertyChangedEventArgs) Handles ModelView.PropertyChanged
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(Forma.ModelViewTalkerBase)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.ModelViewPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, Forma.ModelViewTalkerBase), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " EXPOSED CONTROLS "

    ''' <summary> Gets the status strip. </summary>
    ''' <value> The status strip. </value>
    Protected ReadOnly Property StatusStrip As StatusStrip
        Get
            Return Me._StatusStrip
        End Get
    End Property

    ''' <summary> Gets the status label. </summary>
    ''' <value> The status label. </value>
    Protected ReadOnly Property StatusLabel As ToolStripStatusLabel
        Get
            Return Me._StatusLabel
        End Get
    End Property

    ''' <summary> Gets or sets the status prompt. </summary>
    ''' <value> The status prompt. </value>
    Public Property StatusPrompt As String
        Get
            Return Me.StatusLabel.ToolTipText
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.StatusPrompt, StringComparison.OrdinalIgnoreCase) Then
                Me._StatusLabel.Text = isr.Core.WinForms.CompactExtensions.Methods.Compact(value, Me._StatusLabel)
                Me._StatusLabel.ToolTipText = value
            End If
        End Set
    End Property

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Protected Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Identify(Me.Talker)
    End Sub

    ''' <summary> Adds the listeners such as the current trace messages box. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Protected Overloads Sub AddPrivateListeners()
        MyBase.AddPrivateListener(Me._TraceMessagesBox)
        Me.ModelView?.AddPrivateListeners(Me.Talker)
        'Me._MyTalkerControl?.AddPrivateListeners(Me.Talker)
    End Sub

    ''' <summary> Adds the listeners such as the top level trace messages box and log. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="listener"> The listener. </param>
    Public Overrides Sub AddListener(ByVal listener As IMessageListener)
        Me.ModelView?.AddListener(listener)
        'Me._MyTalkerControl?.AddListener(listener)
        MyBase.AddListener(listener)
    End Sub

    ''' <summary> Removes the listeners if the talker was not assigned. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Overrides Sub RemoveListeners()
        MyBase.RemoveListeners()
        Me.ModelView?.RemoveListeners()
        'Me._MyTalkerControl?.RemoveListeners()
    End Sub

    ''' <summary> Applies the trace level to all listeners to the specified type. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="listenerType"> Type of the listener. </param>
    ''' <param name="value">        The value. </param>
    Public Overrides Sub ApplyListenerTraceLevel(ByVal listenerType As ListenerType, ByVal value As TraceEventType)
        If listenerType = Me._TraceMessagesBox.ListenerType Then Me._TraceMessagesBox.ApplyTraceLevel(value)
        Me.ModelView?.ApplyListenerTraceLevel(listenerType, value)
        'Me._MyTalkerControl?.ApplyListenerTraceLevel(listenerType, value)
        ' this should apply only to the listeners associated with this form
        MyBase.ApplyListenerTraceLevel(listenerType, value)
    End Sub

    ''' <summary> Applies the trace level type to all talkers. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="listenerType"> Type of the trace level. </param>
    ''' <param name="value">        The value. </param>
    Public Overrides Sub ApplyTalkerTraceLevel(ByVal listenerType As ListenerType, ByVal value As TraceEventType)
        Me.ModelView?.ApplyTalkerTraceLevel(listenerType, value)
        'Me._MyTalkerControl?.ApplyTalkerTraceLevel(listenerType, value)
        MyBase.ApplyTalkerTraceLevel(listenerType, value)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

#Region " MESSAGE BOX EVENTS "

    ''' <summary> Handles the <see cref="_TraceMessagesBox"/> property changed event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender">       Source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(sender As TraceMessagesBox, propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        If String.Equals(propertyName, NameOf(isr.Core.Forma.TraceMessagesBox.StatusPrompt)) Then
            Me.StatusPrompt = sender.StatusPrompt
        End If
    End Sub

    ''' <summary> Handles Trace messages box property changed event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub TraceMessagesBoxPropertyChanged(sender As Object, e As PropertyChangedEventArgs) Handles _TraceMessagesBox.PropertyChanged
        If sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(Core.Forma.TraceMessagesBox)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.TraceMessagesBoxPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, Core.Forma.TraceMessagesBox), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

End Class

''' <summary> Collection of <see cref="ConsoleForm"/> forms. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 1/15/2016 </para>
''' </remarks>
Public Class ConsoleFormCollection
    Inherits isr.Core.Forma.ListenerFormCollection

#Region " PROPERTY NOTIFY CONTROL BASE "

    ''' <summary> Adds and shows the form. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="title"> The title. </param>
    ''' <param name="form">  The form. </param>
    ''' <param name="panel"> The panel. </param>
    ''' <param name="log">   The Logger. </param>
    Public Overloads Sub ShowNew(ByVal title As String,
                                 ByVal form As ConsoleForm,
                                 ByVal panel As isr.Core.Forma.ModelViewBase,
                                 ByVal log As Logger)
        Me.ShowNew(title, form, panel, log, False)
    End Sub

    ''' <summary> Adds and shows the form. </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="title">          The title. </param>
    ''' <param name="form">           The form. </param>
    ''' <param name="panel">          The panel. </param>
    ''' <param name="log">            The Logger. </param>
    ''' <param name="disposeEnabled"> True to enable, false to disable the dispose. </param>
    Public Overloads Sub ShowNew(ByVal title As String,
                                 ByVal form As ConsoleForm,
                                 ByVal panel As isr.Core.Forma.ModelViewBase,
                                 ByVal log As Logger, ByVal disposeEnabled As Boolean)
        If form Is Nothing Then Throw New ArgumentNullException(NameOf(form))
        form.AddPropertyNotifyControl(title, panel, disposeEnabled)
        Me.ShowNew(form, log)
    End Sub

    ''' <summary> Adds and shows the form. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="title"> The title. </param>
    ''' <param name="panel"> The panel. </param>
    ''' <param name="log">   The Logger. </param>
    Public Overloads Sub ShowNew(ByVal title As String,
                                 ByVal panel As isr.Core.Forma.ModelViewBase,
                                 ByVal log As Logger)
        Dim form As ConsoleForm = Nothing
        Try
            form = New ConsoleForm
            Me.ShowNew(title, form, panel, log)
        Catch
            If form IsNot Nothing Then
                form.Dispose()
            End If

            Throw
        End Try
    End Sub

    ''' <summary> Adds and shows the form. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="form">  The form. </param>
    ''' <param name="panel"> The panel. </param>
    ''' <param name="log">   The Logger. </param>
    Public Overloads Sub ShowNew(ByVal form As ConsoleForm,
                                 ByVal panel As isr.Core.Forma.ModelViewBase,
                                 ByVal log As Logger)
        If form Is Nothing Then Throw New ArgumentNullException(NameOf(form))
        form.AddPropertyNotifyControl(panel)
        Me.ShowNew(form, log)
    End Sub

    ''' <summary> Adds and shows the form. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="panel"> The panel. </param>
    ''' <param name="log">   The Logger. </param>
    Public Overloads Sub ShowNew(ByVal panel As isr.Core.Forma.ModelViewBase,
                                 ByVal log As Logger)
        Dim form As ConsoleForm = Nothing
        Try
            form = New ConsoleForm
            Me.ShowNew(form, panel, log)
        Catch
            If form IsNot Nothing Then
                form.Dispose()
            End If

            Throw
        End Try
    End Sub

#End Region

#Region " TALKER CONTROL BASE"

    ''' <summary> Adds and shows the form. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="form">  The form. </param>
    ''' <param name="panel"> The panel. </param>
    ''' <param name="log">   The Logger. </param>
    Public Overloads Sub ShowNew(ByVal form As ConsoleForm,
                                 ByVal panel As isr.Core.Forma.ModelViewTalkerBase,
                                 ByVal log As Logger)
        If form Is Nothing Then Throw New ArgumentNullException(NameOf(form))
        form.AddTalkerControl(panel)
        Me.ShowNew(form, log)
    End Sub

    ''' <summary> Adds and shows the form. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="panel"> The panel. </param>
    ''' <param name="log">   The Logger. </param>
    Public Overloads Sub ShowNew(ByVal panel As isr.Core.Forma.ModelViewTalkerBase,
                                 ByVal log As Logger)
        Dim form As ConsoleForm = Nothing
        Try
            form = New ConsoleForm
            Me.ShowNew(form, panel, log)
        Catch
            If form IsNot Nothing Then
                form.Dispose()
            End If

            Throw
        End Try
    End Sub

    ''' <summary> Adds and shows the form. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="title"> The title. </param>
    ''' <param name="panel"> The panel. </param>
    ''' <param name="log">   The Logger. </param>
    Public Overloads Sub ShowNew(ByVal title As String,
                                 ByVal panel As isr.Core.Forma.ModelViewTalkerBase,
                                 ByVal log As Logger)
        Dim form As ConsoleForm = Nothing
        Try
            form = New ConsoleForm
            Me.ShowNew(title, form, panel, log)
        Catch
            If form IsNot Nothing Then
                form.Dispose()
            End If

            Throw
        End Try
    End Sub

    ''' <summary> Adds and shows the form. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="title"> The title. </param>
    ''' <param name="form">  The form. </param>
    ''' <param name="panel"> The panel. </param>
    ''' <param name="log">   The Logger. </param>
    Public Overloads Sub ShowNew(ByVal title As String,
                                 ByVal form As ConsoleForm,
                                 ByVal panel As isr.Core.Forma.ModelViewTalkerBase,
                                 ByVal log As Logger)
        Me.ShowNew(title, form, panel, log, False)
    End Sub

    ''' <summary> Adds and shows the form. </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="title">           The title. </param>
    ''' <param name="form">            The form. </param>
    ''' <param name="panel">           The panel. </param>
    ''' <param name="log">             The Logger. </param>
    ''' <param name="disposedEnabled"> True to enable, false to disable the disposed. </param>
    Public Overloads Sub ShowNew(ByVal title As String,
                                 ByVal form As ConsoleForm,
                                 ByVal panel As isr.Core.Forma.ModelViewTalkerBase,
                                 ByVal log As Logger, ByVal disposedEnabled As Boolean)
        If form Is Nothing Then Throw New ArgumentNullException(NameOf(form))
        form.AddTalkerControl(title, panel, disposedEnabled)
        Me.ShowNew(form, log)
    End Sub

#End Region

End Class

