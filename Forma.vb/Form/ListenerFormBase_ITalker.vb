Imports isr.Core.Forma.ExceptionExtensions

Partial Public Class ListenerFormBase
    Implements ITalker

#Region " ASSIGNMENT "

    ''' <summary> The talker. </summary>
    Private _Talker As ITraceMessageTalker

    ''' <summary> Gets or sets the trace message talker. </summary>
    ''' <value> The trace message talker. </value>
    Public Property Talker As ITraceMessageTalker Implements ITalker.Talker
        Get
            Return Me._Talker
        End Get
        Private Set(value As ITraceMessageTalker)
            If Me._Talker IsNot Nothing Then
                Me.RemoveListeners()
            End If
            Me._Talker = value
        End Set
    End Property

    ''' <summary> True if is assigned talker, false if not. </summary>
    Private _IsAssignedTalker As Boolean

    ''' <summary> Assigns a talker. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="talker"> The talker. </param>
    Public Overridable Sub AssignTalker(ByVal talker As ITraceMessageTalker) Implements ITalker.AssignTalker
        Me._IsAssignedTalker = talker IsNot Nothing
        Me.Talker = If(Me._IsAssignedTalker, talker, New TraceMessageTalker)
        Me.IdentifyTalkers()
    End Sub

    ''' <summary> Identifies talkers. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Protected Overridable Sub IdentifyTalkers() Implements ITalker.IdentifyTalkers
        My.MyLibrary.Identify(Me.Talker)
    End Sub

#End Region

#Region " LISTENERS "

    ''' <summary>
    ''' Removes the private listeners. Removes all listeners if the talker was not assigned.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Overridable Sub RemoveListeners() Implements ITalker.RemoveListeners
        Me.RemovePrivateListeners()
        If Not Me._IsAssignedTalker Then Me.Talker.RemoveListeners()
    End Sub

    ''' <summary> The private listeners. </summary>
    Private _PrivateListeners As List(Of IMessageListener)

    ''' <summary> Gets the private listeners. </summary>
    ''' <value> The private listeners. </value>
    Public Overridable ReadOnly Property PrivateListeners As IList(Of IMessageListener) Implements ITalker.PrivateListeners
        Get
            If Me._PrivateListeners Is Nothing Then Me._PrivateListeners = New List(Of IMessageListener)
            Return Me._PrivateListeners
        End Get
    End Property

    ''' <summary> Adds a private listener. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="listener"> The listener. </param>
    Public Overridable Sub AddPrivateListener(ByVal listener As IMessageListener) Implements ITalker.AddPrivateListener
        If Me.PrivateListeners IsNot Nothing Then
            Me._PrivateListeners.Add(listener)
        End If
        Me.AddListener(listener)
    End Sub

    ''' <summary> Adds private listeners. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="listeners"> The listeners. </param>
    Public Overridable Sub AddPrivateListeners(ByVal listeners As IList(Of IMessageListener)) Implements ITalker.AddPrivateListeners
        If listeners Is Nothing Then Throw New ArgumentNullException(NameOf(listeners))
        If Me.PrivateListeners IsNot Nothing Then
            Me._PrivateListeners.AddRange(listeners)
        End If
        Me.AddListeners(listeners)
    End Sub

    ''' <summary> Adds private listeners. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="talker"> The talker. </param>
    Public Overridable Sub AddPrivateListeners(ByVal talker As ITraceMessageTalker) Implements ITalker.AddPrivateListeners
        If talker Is Nothing Then Throw New ArgumentNullException(NameOf(talker))
        Me.AddPrivateListeners(talker.Listeners)
    End Sub

    ''' <summary> Removes the private listener described by listener. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="listener"> The listener. </param>
    Public Overridable Sub RemovePrivateListener(ByVal listener As IMessageListener) Implements ITalker.RemovePrivateListener
        Me.RemoveListener(listener)
        If Me.PrivateListeners IsNot Nothing Then
            Me._PrivateListeners.Remove(listener)
        End If
    End Sub

    ''' <summary> Removes the private listeners. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Public Overridable Sub RemovePrivateListeners() Implements ITalker.RemovePrivateListeners
        For Each listener As IMessageListener In Me.PrivateListeners
            Me.RemoveListener(listener)
        Next
        Me._PrivateListeners.Clear()
    End Sub

    ''' <summary> Removes the listener described by listener. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="listener"> The listener. </param>
    Public Overridable Sub RemoveListener(ByVal listener As IMessageListener) Implements ITalker.RemoveListener
        Me.Talker.RemoveListener(listener)
    End Sub

    ''' <summary> Removes the specified listeners. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="listeners"> The listeners. </param>
    Public Overridable Sub RemoveListeners(ByVal listeners As IList(Of IMessageListener)) Implements ITalker.RemoveListeners
        If listeners Is Nothing Then Throw New ArgumentNullException(NameOf(listeners))
        For Each listener As IMessageListener In listeners
            Me.RemoveListener(listener)
        Next
    End Sub

    ''' <summary> Adds a listener. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="listener"> The listener. </param>
    Public Overridable Sub AddListener(ByVal listener As IMessageListener) Implements ITalker.AddListener
        Me.Talker.AddListener(listener)
        Me.IdentifyTalkers()
    End Sub

    ''' <summary> Adds the specified listeners. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="listeners"> The listeners. </param>
    Public Overridable Sub AddListeners(ByVal listeners As IList(Of IMessageListener)) Implements ITalker.AddListeners
        If listeners Is Nothing Then Throw New ArgumentNullException(NameOf(listeners))
        For Each listener As IMessageListener In listeners
            Me.AddListener(listener)
        Next
    End Sub

    ''' <summary> Adds the listeners. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="talker"> The talker. </param>
    Public Overridable Sub AddListeners(ByVal talker As ITraceMessageTalker) Implements ITalker.AddListeners
        If talker Is Nothing Then Throw New ArgumentNullException(NameOf(talker))
        Me.AddListeners(talker.Listeners)
    End Sub

    ''' <summary> Applies the trace level to all listeners to the specified type. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="listenerType"> Type of the listener. </param>
    ''' <param name="value">        The value. </param>
    Public Overridable Sub ApplyListenerTraceLevel(ByVal listenerType As ListenerType, ByVal value As TraceEventType) Implements ITalker.ApplyListenerTraceLevel
        ' this should apply only to the listeners associated with this form
        ' Not this: Me.Talker.ApplyListenerTraceLevel(listenerType, value)
        Me.IdentifyTalkers()
    End Sub

    ''' <summary> Applies the trace level type to all talkers. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="listenerType"> Type of the trace level. </param>
    ''' <param name="value">        The value. </param>
    Public Overridable Sub ApplyTalkerTraceLevel(ByVal listenerType As ListenerType, ByVal value As TraceEventType) Implements ITalker.ApplyTalkerTraceLevel
        Me.Talker.ApplyTalkerTraceLevel(listenerType, value)
    End Sub

    ''' <summary> Applies the talker trace levels described by talker. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="talker"> The talker. </param>
    Public Overridable Sub ApplyTalkerTraceLevels(ByVal talker As ITraceMessageTalker) Implements ITalker.ApplyTalkerTraceLevels
        Me.Talker.ApplyTalkerTraceLevels(talker)
    End Sub

    ''' <summary> Applies the talker listeners trace levels described by talker. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="talker"> The talker. </param>
    Public Overridable Sub ApplyListenerTraceLevels(ByVal talker As ITraceMessageTalker) Implements ITalker.ApplyListenerTraceLevels
        Me.Talker.ApplyListenerTraceLevels(talker)
    End Sub
#End Region

#Region " TALKER PUBLISH "

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="message"> The message. </param>
    ''' <returns> A String. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overloads Function Publish(ByVal message As TraceMessage) As String
        If message Is Nothing Then Return String.Empty
        Dim activity As String = String.Empty
        Dim result As String = message.Details
        Try
            If Me.Talker Is Nothing Then
                activity = $"logging unpublished message: {message}"
                My.MyLibrary.LogUnpublishedMessage(message)
            Else
                activity = $"publishing: {message}"
                Me.Talker.Publish(message)
            End If
        Catch ex As Exception
            If Debugger.IsAttached Then
                Debug.Assert(Not Debugger.IsAttached, $"Exception {activity};. {ex.ToFullBlownString}")
            Else
                Windows.Forms.MessageBox.Show(Nothing, $"Exception {activity};. {ex.ToFullBlownString}",
                                              "Exception publishing message", Windows.Forms.MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Error,
                                              Windows.Forms.MessageBoxDefaultButton.Button1, Windows.Forms.MessageBoxOptions.DefaultDesktopOnly)
            End If
        End Try
        Return result
    End Function

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks>
    ''' This function will throw the exception if not overridden so that the Trace Event Id of the
    ''' caller library is used. The unction is not defined as must override because form designers do
    ''' not open when making forms non-creatable with the must override directives.
    ''' </remarks>
    ''' <exception cref="NotImplementedException"> Thrown when the requested operation is not
    '''                                            overridden by the calling control. </exception>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overridable Overloads Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Throw New NotImplementedException($"{Me.Name} is a base class; this function must be overridden by the inheriting classes")
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks>
    ''' This function will throw the exception if not overridden so that the Trace Event Id of the
    ''' caller library is used. The unction is not defined as must override because form designers do
    ''' not open when making forms non-creatable with the must override directives.
    ''' </remarks>
    ''' <exception cref="NotImplementedException"> Thrown when the requested operation is not
    '''                                            overridden by the calling control. </exception>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overridable Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Throw New NotImplementedException($"{Me.Name} is a base class; this function must be overridden by the inheriting classes")
    End Function

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> Action event information. </param>
    ''' <returns> A String. </returns>
    Protected Overloads Function Publish(ByVal e As ActionEventArgs) As String
        Return If(e Is Nothing, String.Empty, Me.Publish(e.OutcomeEvent, e.Details))
    End Function

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="format">    Describes the format to use. </param>
    ''' <param name="args">      A variable-length parameters list containing arguments. </param>
    ''' <returns> A String. </returns>
    Protected Overloads Function Publish(ByVal eventType As TraceEventType, ByVal format As String, ParamArray args() As Object) As String
        Return Me.Publish(eventType, String.Format(format, args))
    End Function

    ''' <summary> Publish warning. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <returns> A String. </returns>
    Protected Function PublishWarning(ByVal activity As String) As String
        Return Me.Publish(TraceEventType.Warning, activity)
    End Function

    ''' <summary> Publish warning. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <param name="args">   A variable-length parameters list containing arguments. </param>
    ''' <returns> A String. </returns>
    Protected Function PublishWarning(ByVal format As String, ParamArray args() As Object) As String
        Return Me.Publish(TraceEventType.Warning, format, args)
    End Function

    ''' <summary> Publish verbose. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <returns> A String. </returns>
    Protected Function PublishVerbose(ByVal activity As String) As String
        Return Me.Publish(TraceEventType.Verbose, activity)
    End Function

    ''' <summary> Publish verbose. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <param name="args">   A variable-length parameters list containing arguments. </param>
    ''' <returns> A String. </returns>
    Protected Function PublishVerbose(ByVal format As String, ParamArray args() As Object) As String
        Return Me.Publish(TraceEventType.Verbose, format, args)
    End Function

    ''' <summary> Publish information. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <returns> A String. </returns>
    Protected Function PublishInfo(ByVal activity As String) As String
        Return Me.Publish(TraceEventType.Information, activity)
    End Function

    ''' <summary> Publish information. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <param name="args">   A variable-length parameters list containing arguments. </param>
    ''' <returns> A String. </returns>
    Protected Function PublishInfo(ByVal format As String, ParamArray args() As Object) As String
        Return Me.Publish(TraceEventType.Information, format, args)
    End Function

#End Region

End Class

