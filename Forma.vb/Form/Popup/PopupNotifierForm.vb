﻿Imports System.Drawing.Drawing2D
Imports System.Drawing
Imports System.Windows.Forms
Imports isr.Core.Forma.ExceptionExtensions

''' <summary> This is the form of the actual notification window. </summary>
''' <remarks>
''' (c) 2011 Simon Baer.<para>
''' Licensed under The MIT License.</para><para>
''' David, 7/11/2014, Created/modified in 2011 by Simon Baer. </para><para>
''' http://www.codeproject.com/KB/dialog/notificationwindow.aspx Based on the Code Project
''' article by Nicolas Wälti: http://www.codeproject.com/KB/cpp/PopupNotifier.aspx </para><para> 
''' David, 7/11/2014, Updated. </para>
''' </remarks>
Friend Class PopupNotifierForm

    Inherits Forma.FormBase

    ''' <summary> Event that is raised when the text is clicked. </summary>
    Public Event LinkClick As EventHandler(Of System.EventArgs)

    ''' <summary> Removes event handler. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The handler. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub RemoveLinkClickEventHandler(ByVal value As EventHandler(Of EventArgs))
#Disable Warning CA1825 ' Avoid zero-length array allocations.
        For Each d As [Delegate] In If(value Is Nothing, New [Delegate]() {}, value.GetInvocationList)
#Enable Warning CA1825 ' Avoid zero-length array allocations.
            Try
                RemoveHandler Me.LinkClick, CType(d, EventHandler(Of EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

    ''' <summary> Event that is raised when the notification window is manually closed. </summary>
    Public Event CloseClick As EventHandler(Of System.EventArgs)

    ''' <summary> Removes event handler. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The handler. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub RemoveCloseClickEventHandler(ByVal value As EventHandler(Of EventArgs))
#Disable Warning CA1825 ' Avoid zero-length array allocations.
        For Each d As [Delegate] In If(value Is Nothing, New [Delegate]() {}, value.GetInvocationList)
#Enable Warning CA1825 ' Avoid zero-length array allocations.
            Try
                RemoveHandler Me.CloseClick, CType(d, EventHandler(Of EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

    ''' <summary> Event queue for all listeners interested in ContextMenuOpened events. </summary>
    Friend Event ContextMenuOpened As EventHandler(Of System.EventArgs)

    ''' <summary> Event queue for all listeners interested in ContextMenuClosed events. </summary>
    Friend Event ContextMenuClosed As EventHandler(Of System.EventArgs)

    ''' <summary> true to mouse on close. </summary>
    Private mouseOnClose As Boolean = False

    ''' <summary> true to mouse on link. </summary>
    Private mouseOnLink As Boolean = False

    ''' <summary> true to mouse on options. </summary>
    Private mouseOnOptions As Boolean = False

    ''' <summary> Height of the title. </summary>
    Private heightOfTitle As Integer

#Region " GDI objects "

    ''' <summary> <c>true</c> if GDI initialized. </summary>
    Private gdiInitialized As Boolean = False

    ''' <summary> The rectangle body. </summary>
    Private rcBody As Rectangle

    ''' <summary> The rectangle header. </summary>
    Private rcHeader As Rectangle

    ''' <summary> The rectangle form. </summary>
    Private rcForm As Rectangle

    ''' <summary> The brush body. </summary>
    Private brushBody As LinearGradientBrush

    ''' <summary> The brush header. </summary>
    Private brushHeader As LinearGradientBrush

    ''' <summary> The brush button hover. </summary>
    Private brushButtonHover As Brush

    ''' <summary> The pen button border. </summary>
    Private penButtonBorder As Pen

    ''' <summary> The pen content. </summary>
    Private penContent As Pen

    ''' <summary> The pen border. </summary>
    Private penBorder As Pen

    ''' <summary> The brush foreground color. </summary>
    Private brushForeColor As Brush

    ''' <summary> The brush link hover. </summary>
    Private brushLinkHover As Brush

    ''' <summary> The brush content. </summary>
    Private brushContent As Brush

    ''' <summary> The brush title. </summary>
    Private brushTitle As Brush

#End Region

    ''' <summary> Create a new instance. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="parent"> Popup Notifier. </param>
    Public Sub New(ByVal parent As PopupNotifier)
        MyBase.New()
        Me.InitializeComponent()
        Me.Parent = parent
        Me.SetStyle(ControlStyles.OptimizedDoubleBuffer, True)
        Me.SetStyle(ControlStyles.ResizeRedraw, True)
        Me.SetStyle(ControlStyles.AllPaintingInWmPaint, True)
        Me.ShowInTaskbar = False

        AddHandler Me.VisibleChanged, AddressOf Me.PopupNotifierForm_VisibleChanged
        AddHandler Me.MouseMove, AddressOf Me.PopupNotifierForm_MouseMove
        AddHandler Me.MouseUp, AddressOf Me.PopupNotifierForm_MouseUp
        AddHandler Me.Paint, AddressOf Me.PopupNotifierForm_Paint
    End Sub

    ''' <summary> The form is shown/hidden. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> . </param>
    ''' <param name="e">      . </param>
    Private Sub PopupNotifierForm_VisibleChanged(ByVal sender As Object, ByVal e As EventArgs)
        If Me.Visible Then
            Me.mouseOnClose = False
            Me.mouseOnLink = False
            Me.mouseOnOptions = False
        End If
    End Sub

    ''' <summary> Used in design mode. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Sub InitializeComponent()
        Me.SuspendLayout()
        Me.ClientSize = New System.Drawing.Size(392, 66)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "PopupNotifierForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.TopMost = True
        Me.ResumeLayout(False)
    End Sub

    ''' <summary> Gets the parent control. </summary>
    ''' <value> The parent. </value>
    Public Shadows Property Parent() As PopupNotifier

    ''' <summary> Add two values but do not return a value greater than 255. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="input"> first value. </param>
    ''' <param name="add">   value to add. </param>
    ''' <returns> sum of both values. </returns>
    Private Shared Function AddValueMax255(ByVal input As Integer, ByVal add As Integer) As Integer
        Return If(input + add < 256, input + add, 255)
    End Function

    ''' <summary> Subtract two values but do not returns a value below 0. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="input"> first value. </param>
    ''' <param name="ded">   value to subtract. </param>
    ''' <returns> first value minus second value. </returns>
    Private Shared Function DedValueMin0(ByVal input As Integer, ByVal ded As Integer) As Integer
        Return If(input - ded > 0, input - ded, 0)
    End Function

    ''' <summary> Returns a color which is darker than the given color. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="color"> Color. </param>
    ''' <returns> darker color. </returns>
    Private Function GetDarkerColor(ByVal color As Color) As Color
        Return System.Drawing.Color.FromArgb(255, DedValueMin0(CInt(color.R), Me.Parent.GradientPower), DedValueMin0(CInt(color.G), Me.Parent.GradientPower), DedValueMin0(CInt(color.B), Me.Parent.GradientPower))
    End Function

    ''' <summary> Returns a color which is lighter than the given color. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="color"> Color. </param>
    ''' <returns> lighter color. </returns>
    Private Function GetLighterColor(ByVal color As Color) As Color
        Return System.Drawing.Color.FromArgb(255, AddValueMax255(CInt(color.R), Me.Parent.GradientPower), AddValueMax255(CInt(color.G), Me.Parent.GradientPower), AddValueMax255(CInt(color.B), Me.Parent.GradientPower))
    End Function

    ''' <summary> Gets the rectangle of the content text. </summary>
    ''' <value> The rectangle content text. </value>
    Private ReadOnly Property RectContentText() As RectangleF
        Get
            Return If(Me.Parent.Image IsNot Nothing,
                New RectangleF(Me.Parent.ImagePadding.Left + Me.Parent.ImageSize.Width + Me.Parent.ImagePadding.Right + Me.Parent.ContentPadding.Left, Me.Parent.HeaderHeight + Me.Parent.TitlePadding.Top + Me.heightOfTitle + Me.Parent.TitlePadding.Bottom + Me.Parent.ContentPadding.Top, Me.Width - Me.Parent.ImagePadding.Left - Me.Parent.ImageSize.Width - Me.Parent.ImagePadding.Right - Me.Parent.ContentPadding.Left - Me.Parent.ContentPadding.Right - 16 - 5, Me.Height - Me.Parent.HeaderHeight - Me.Parent.TitlePadding.Top - Me.heightOfTitle - Me.Parent.TitlePadding.Bottom - Me.Parent.ContentPadding.Top - Me.Parent.ContentPadding.Bottom - 1),
                New RectangleF(Me.Parent.ContentPadding.Left, Me.Parent.HeaderHeight + Me.Parent.TitlePadding.Top + Me.heightOfTitle + Me.Parent.TitlePadding.Bottom + Me.Parent.ContentPadding.Top, Me.Width - Me.Parent.ContentPadding.Left - Me.Parent.ContentPadding.Right - 16 - 5, Me.Height - Me.Parent.HeaderHeight - Me.Parent.TitlePadding.Top - Me.heightOfTitle - Me.Parent.TitlePadding.Bottom - Me.Parent.ContentPadding.Top - Me.Parent.ContentPadding.Bottom - 1))
        End Get
    End Property

    ''' <summary> gets the rectangle of the close button. </summary>
    ''' <value> The rectangle close. </value>
    Private ReadOnly Property RectClose() As Rectangle
        Get
            Return New Rectangle(Me.Width - 5 - 16, Me.Parent.HeaderHeight + 3, 16, 16)
        End Get
    End Property

    ''' <summary> Gets the rectangle of the options button. </summary>
    ''' <value> Options that control the rectangle. </value>
    Private ReadOnly Property RectOptions() As Rectangle
        Get
            Return New Rectangle(Me.Width - 5 - 16, Me.Parent.HeaderHeight + 3 + 16 + 5, 16, 16)
        End Get
    End Property

    ''' <summary>
    ''' Update form to display hover styles when the mouse moves over the notification form.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> . </param>
    ''' <param name="e">      . </param>
    Private Sub PopupNotifierForm_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs)
        If Me.Parent.ShowCloseButton Then
            Me.mouseOnClose = Me.RectClose.Contains(e.X, e.Y)
        End If
        If Me.Parent.ShowOptionsButton Then
            Me.mouseOnOptions = Me.RectOptions.Contains(e.X, e.Y)
        End If
        Me.mouseOnLink = Me.RectContentText.Contains(e.X, e.Y)
        Me.Invalidate()
    End Sub

    ''' <summary> A mouse button has been released, check if something has been clicked. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> . </param>
    ''' <param name="e">      . </param>
    Private Sub PopupNotifierForm_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs)
        If e.Button = System.Windows.Forms.MouseButtons.Left Then
            If Me.RectClose.Contains(e.X, e.Y) Then
                Dim evt As EventHandler(Of System.EventArgs) = Me.CloseClickEvent
                evt?.Invoke(Me, EventArgs.Empty)
            End If
            If Me.RectContentText.Contains(e.X, e.Y) Then
                Dim evt As EventHandler(Of System.EventArgs) = Me.LinkClickEvent
                evt?.Invoke(Me, EventArgs.Empty)
            End If
            If Me.RectOptions.Contains(e.X, e.Y) AndAlso (Me.Parent.OptionsMenu IsNot Nothing) Then
                RaiseEvent ContextMenuOpened(Me, EventArgs.Empty)
                Me.Parent.OptionsMenu.Show(New Point(Me.RectOptions.Right - Me.Parent.OptionsMenu.Width, Me.RectOptions.Bottom))
                AddHandler Me.Parent.OptionsMenu.Closed, AddressOf Me.OptionsMenu_Closed
            End If
        End If
    End Sub

    ''' <summary> The options popup menu has been closed. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> . </param>
    ''' <param name="e">      Tool strip drop down closed event information. </param>
    Private Sub OptionsMenu_Closed(ByVal sender As Object, ByVal e As ToolStripDropDownClosedEventArgs)
        RemoveHandler Me.Parent.OptionsMenu.Closed, AddressOf Me.OptionsMenu_Closed
        Dim evt As EventHandler(Of System.EventArgs) = Me.ContextMenuClosedEvent
        evt?.Invoke(Me, EventArgs.Empty)
    End Sub

    ''' <summary> Create all GDI objects used to paint the form. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Sub AllocateGDIObjects()
        Me.rcBody = New Rectangle(0, 0, Me.Width, Me.Height)
        Me.rcHeader = New Rectangle(0, 0, Me.Width, Me.Parent.HeaderHeight)
        Me.rcForm = New Rectangle(0, 0, Me.Width - 1, Me.Height - 1)

        Me.brushBody = New LinearGradientBrush(Me.rcBody, Me.Parent.BodyColor, Me.GetLighterColor(Me.Parent.BodyColor), LinearGradientMode.Vertical)
        Me.brushHeader = New LinearGradientBrush(Me.rcHeader, Me.Parent.HeaderColor, Me.GetDarkerColor(Me.Parent.HeaderColor), LinearGradientMode.Vertical)
        Me.brushButtonHover = New SolidBrush(Me.Parent.ButtonHoverColor)
        Me.penButtonBorder = New Pen(Me.Parent.ButtonBorderColor)
        Me.penContent = New Pen(Me.Parent.ContentColor, 2)
        Me.penBorder = New Pen(Me.Parent.BorderColor)
        Me.brushForeColor = New SolidBrush(Me.ForeColor)
        Me.brushLinkHover = New SolidBrush(Me.Parent.ContentHoverColor)
        Me.brushContent = New SolidBrush(Me.Parent.ContentColor)
        Me.brushTitle = New SolidBrush(Me.Parent.TitleColor)
        Me.gdiInitialized = True
    End Sub

    ''' <summary> Free all GDI objects. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Sub DisposeGDIObjects()
        If Me.gdiInitialized Then
            Me.brushBody.Dispose()
            Me.brushHeader.Dispose()
            Me.brushButtonHover.Dispose()
            Me.penButtonBorder.Dispose()
            Me.penContent.Dispose()
            Me.penBorder.Dispose()
            Me.brushForeColor.Dispose()
            Me.brushLinkHover.Dispose()
            Me.brushContent.Dispose()
            Me.brushTitle.Dispose()
        End If
    End Sub

    ''' <summary> Draw the notification form. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> . </param>
    ''' <param name="e">      . </param>
    Private Sub PopupNotifierForm_Paint(ByVal sender As Object, ByVal e As PaintEventArgs)
        If Not Me.gdiInitialized Then
            Me.AllocateGDIObjects()
        End If

        ' draw window
        e.Graphics.FillRectangle(Me.brushBody, Me.rcBody)
        e.Graphics.FillRectangle(Me.brushHeader, Me.rcHeader)
        e.Graphics.DrawRectangle(Me.penBorder, Me.rcForm)
        If Me.Parent.ShowGrip Then
            e.Graphics.DrawImage(My.Resources.Grip, CInt((Me.Width - My.Resources.Grip.Width) \ 2), CInt(Fix((Me.Parent.HeaderHeight - 3) / 2)))
        End If
        If Me.Parent.ShowCloseButton Then
            If Me.mouseOnClose Then
                e.Graphics.FillRectangle(Me.brushButtonHover, Me.RectClose)
                e.Graphics.DrawRectangle(Me.penButtonBorder, Me.RectClose)
            End If
            e.Graphics.DrawLine(Me.penContent, Me.RectClose.Left + 4, Me.RectClose.Top + 4, Me.RectClose.Right - 4, Me.RectClose.Bottom - 4)
            e.Graphics.DrawLine(Me.penContent, Me.RectClose.Left + 4, Me.RectClose.Bottom - 4, Me.RectClose.Right - 4, Me.RectClose.Top + 4)
        End If
        If Me.Parent.ShowOptionsButton Then
            If Me.mouseOnOptions Then
                e.Graphics.FillRectangle(Me.brushButtonHover, Me.RectOptions)
                e.Graphics.DrawRectangle(Me.penButtonBorder, Me.RectOptions)
            End If
            e.Graphics.FillPolygon(Me.brushForeColor,
                                   New Point() {New Point(Me.RectOptions.Left + 4, Me.RectOptions.Top + 6),
                                                New Point(Me.RectOptions.Left + 12, Me.RectOptions.Top + 6),
                                                New Point(Me.RectOptions.Left + 8, Me.RectOptions.Top + 4 + 6)})
        End If

        ' draw icon
        If Me.Parent.Image IsNot Nothing Then
            e.Graphics.DrawImage(Me.Parent.Image, Me.Parent.ImagePadding.Left, Me.Parent.HeaderHeight + Me.Parent.ImagePadding.Top,
                                 Me.Parent.ImageSize.Width, Me.Parent.ImageSize.Height)
        End If

        ' calculate height of title
        Me.heightOfTitle = CInt(e.Graphics.MeasureString("A", Me.Parent.TitleFont).Height)
        Dim titleX As Integer = Me.Parent.TitlePadding.Left
        If Me.Parent.Image IsNot Nothing Then
            titleX += Me.Parent.ImagePadding.Left + Me.Parent.ImageSize.Width + Me.Parent.ImagePadding.Right
        End If

        ' draw title
        e.Graphics.DrawString(Me.Parent.TitleText, Me.Parent.TitleFont, Me.brushTitle, titleX, Me.Parent.HeaderHeight + Me.Parent.TitlePadding.Top)

        ' draw content text, optionally with a bold part
        Me.Cursor = If(Me.mouseOnLink, Cursors.Hand, Cursors.Default)
        Dim brushText As Brush = If(Me.mouseOnLink, Me.brushLinkHover, Me.brushContent)
        e.Graphics.DrawString(Me.Parent.ContentText, Me.Parent.ContentFont, brushText, Me.RectContentText)
    End Sub

    ''' <summary> Dispose GDI objects. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="disposing"> . </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.DisposeGDIObjects()
                Me.RemoveCloseClickEventHandler(Me.CloseClickEvent)
                Me.RemoveLinkClickEventHandler(Me.LinkClickEvent)
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

End Class

