Imports System.Runtime.InteropServices

Partial Public Class NakedFormBase

#Region " SAFE NATIVE METHODS "

    ''' <summary> A safe native methods. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private NotInheritable Class SafeNativeMethods

        ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        ''' <remarks> David, 2020-09-24. </remarks>
        Private Sub New()
            MyBase.New()
        End Sub

        ''' <summary> Sends a message. </summary>
        ''' <remarks> David, 2020-09-24. </remarks>
        ''' <param name="hWnd">   The window. </param>
        ''' <param name="msg">    The message. </param>
        ''' <param name="wParam"> The parameter. </param>
        ''' <param name="lParam"> The parameter. </param>
        ''' <returns> An Int32. </returns>
        <Runtime.InteropServices.DllImport("user32.dll")>
        Friend Shared Function SendMessage(ByVal hWnd As IntPtr,
                                        ByVal msg As UInt32,
                                        ByVal wParam As UIntPtr,
                                        ByVal lParam As IntPtr) As IntPtr
        End Function

        ''' <summary> Releases the capture. </summary>
        ''' <remarks> David, 2020-09-24. </remarks>
        ''' <returns> A &lt;MarshalAs(UnmanagedType.Bool)&gt; </returns>
        <Runtime.InteropServices.DllImportAttribute("user32.dll")>
        Friend Shared Function ReleaseCapture() As <MarshalAs(UnmanagedType.Bool)> Boolean
        End Function

    End Class

    Private ReadOnly WM_NCLBUTTONDOWN As UInt32 = &HA1

    ''' <summary> The height caption. </summary>
    Private ReadOnly HT_CAPTION As UIntPtr = New UIntPtr(&H2)

    ' Private HTBORDER As UIntPtr = New UIntPtr(18)
    Private ReadOnly HTBOTTOM As UIntPtr = New UIntPtr(15)

    Private ReadOnly HTBOTTOMLEFT As UIntPtr = New UIntPtr(16)

    Private ReadOnly HTBOTTOMRIGHT As UIntPtr = New UIntPtr(17)

    Private ReadOnly HTLEFT As UIntPtr = New UIntPtr(10)

    Private ReadOnly HTRIGHT As UIntPtr = New UIntPtr(11)

    Private ReadOnly HTTOP As UIntPtr = New UIntPtr(12)

    Private ReadOnly HTTOPLEFT As UIntPtr = New UIntPtr(13)

    Private ReadOnly HTTOPRIGHT As UIntPtr = New UIntPtr(14)

    ''' <summary> Values that represent on borders. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Enum OnBorder
        ''' <summary> An enum constant representing the none option. </summary>
        None = 0
        ''' <summary> An enum constant representing the top option. </summary>
        Top = 1
        ''' <summary> An enum constant representing the right option. </summary>
        Right = 2
        ''' <summary> An enum constant representing the bottom option. </summary>
        Bottom = 4
        ''' <summary> An enum constant representing the left option. </summary>
        Left = 8
        ''' <summary> An enum constant representing the top right option. </summary>
        TopRight = 3
        ''' <summary> An enum constant representing the right bottom option. </summary>
        RightBottom = 6
        ''' <summary> An enum constant representing the left top option. </summary>
        LeftTop = 9
        ''' <summary> An enum constant representing the bottom left option. </summary>
        BottomLeft = 12
    End Enum

#End Region

End Class
