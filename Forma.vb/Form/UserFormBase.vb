﻿Imports System.Drawing

''' <summary> A form that persists user settings in the Application Settings file. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 01/31/2013, 6.1.4779.x. </para>
''' </remarks>
Public Class UserFormBase
    Inherits System.Windows.Forms.Form

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Specialized default constructor for use only by derived classes. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Protected Sub New()
        MyBase.New()
        Me.InitializeComponent()
        Me.SaveSettingsOnClosing = True
    End Sub

    ''' <summary> Initializes the component. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Sub InitializeComponent()
        Me.SuspendLayout()
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(331, 341)
        Me.Font = New Font(SystemFonts.MessageBoxFont.FontFamily, 9.75!, FontStyle.Regular, GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = My.Resources.favicon
        Me.Name = "UserFormBase"
        Me.ResumeLayout(False)
    End Sub

#End Region

End Class
