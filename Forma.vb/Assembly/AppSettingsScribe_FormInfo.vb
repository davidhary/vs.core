Namespace My

    Public Class AppSettingsScribe

#Region " FORM SIZE AND POSITION "

        ''' <summary> Reads the application setting. </summary>
        ''' <remarks> David, 2020-09-24. </remarks>
        ''' <param name="key">          The key. </param>
        ''' <param name="defaultValue"> The default value. </param>
        ''' <returns> Windows.Forms.FormStartPosition. </returns>
        Public Shared Function ReadValue(ByVal key As String, ByVal defaultValue As System.Windows.Forms.FormStartPosition) As System.Windows.Forms.FormStartPosition
            Dim loadedStartPosition As System.Windows.Forms.FormStartPosition = defaultValue
            If [Enum].TryParse(Of System.Windows.Forms.FormStartPosition)(My.AppSettingsScribe.Get().ReadValue(key), loadedStartPosition) Then
            End If
            Return loadedStartPosition
        End Function

        ''' <summary> Reads the application setting. </summary>
        ''' <remarks> David, 2020-09-24. </remarks>
        ''' <param name="key">          The key. </param>
        ''' <param name="defaultValue"> The default value. </param>
        ''' <returns> Windows.Forms.FormWindowState. </returns>
        Public Shared Function ReadValue(ByVal key As String, ByVal defaultValue As System.Windows.Forms.FormWindowState) As System.Windows.Forms.FormWindowState
            Dim loadedWindowsState As System.Windows.Forms.FormWindowState = defaultValue
            If [Enum].TryParse(Of System.Windows.Forms.FormWindowState)(My.AppSettingsScribe.Get().ReadValue(key), loadedWindowsState) Then
            End If
            Return loadedWindowsState
        End Function

        ''' <summary> Writes the application setting. </summary>
        ''' <remarks> David, 2020-09-24. </remarks>
        ''' <param name="key">   The key. </param>
        ''' <param name="value"> The value. </param>
        Public Shared Sub WriteValue(ByVal key As String, ByVal value As System.Windows.Forms.FormWindowState)
            My.AppSettingsScribe.Get().WriteValue(key, value.ToString)
        End Sub

        ''' <summary> Writes the application setting. </summary>
        ''' <remarks> David, 2020-09-24. </remarks>
        ''' <param name="key">   The key. </param>
        ''' <param name="value"> The value. </param>
        Public Shared Sub WriteValue(ByVal key As String, ByVal value As System.Windows.Forms.FormStartPosition)
            My.AppSettingsScribe.Get().WriteValue(key, value.ToString)
        End Sub

#End Region

    End Class

End Namespace
