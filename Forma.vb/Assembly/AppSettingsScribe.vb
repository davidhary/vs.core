Namespace My

    ''' <summary>
    ''' Reads and writes application settings to the Application Setting section of the program
    ''' configuration file.
    ''' </summary>
    ''' <remarks>
    ''' (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 10/21/2009, 1.0.3581.x. </para>
    ''' </remarks>
    Partial Public Class AppSettingsScribe

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> Initializes a new instance of the <see cref="AppSettingsScribe"/> class. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        Public Sub New()
            Me.New(System.Configuration.ConfigurationUserLevel.None)
        End Sub

        ''' <summary> Initializes a new instance of the <see cref="AppSettingsScribe"/> class. </summary>
        ''' <remarks>
        ''' Application configuration is read only except with configuration level of
        ''' <see cref="System.Configuration.ConfigurationUserLevel.None">None</see>
        ''' Attempting to write with other configuration levels, causes a configuration exception:
        ''' "ConfigurationSection properties cannot be edited when locked.".
        ''' </remarks>
        ''' <param name="configurationUserLevel"> The configuration user level. </param>
        Public Sub New(ByVal configurationUserLevel As System.Configuration.ConfigurationUserLevel)
            Me.New(ApplicationInfo.BuildApplicationConfigFilePath(False, 2), configurationUserLevel)
        End Sub

        ''' <summary> Initializes a new instance of the <see cref="AppSettingsScribe"/> class. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="filePath">               The file path. </param>
        ''' <param name="configurationUserLevel"> The configuration user level. </param>
        Public Sub New(ByVal filePath As String, ByVal configurationUserLevel As System.Configuration.ConfigurationUserLevel)
            MyBase.New()
            Dim map As New Configuration.ExeConfigurationFileMap With {.ExeConfigFilename = filePath}
            ' Me._configuration = System.Configuration.ConfigurationManager.OpenExeConfiguration(configurationUserLevel)
            Me._Configuration = System.Configuration.ConfigurationManager.OpenMappedExeConfiguration(map, configurationUserLevel)
        End Sub

#End Region

#Region " SINGLETON "

        ''' <summary>
        ''' Gets a new pad lock to enforce thread safety when creating the singleton instance.
        ''' </summary>
        Private Shared ReadOnly SyncLocker As New Object

        ''' <summary> The instance. </summary>
        Private Shared _Instance As AppSettingsScribe

        ''' <summary> Instantiates the class. </summary>
        ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
        ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
        ''' <returns> A new or existing instance of the class. </returns>
        <System.Runtime.InteropServices.ComVisible(False)>
        Public Shared Function [Get]() As AppSettingsScribe
            If _Instance Is Nothing Then
                SyncLock SyncLocker
                    _Instance = New AppSettingsScribe(System.Configuration.ConfigurationUserLevel.None)
                End SyncLock
            End If
            Return _Instance
        End Function

#End Region

#Region " GENERAL I/O "

        ''' <summary> Gets or sets the configuration. </summary>
        ''' <value> The configuration. </value>
        Public ReadOnly Property Configuration As Global.System.Configuration.Configuration

        ''' <summary> Returns true if the key exists in the application settings. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="key"> . </param>
        ''' <returns>
        ''' <c>True</c> if the key exists in the application settings; otherwise, <c>False</c>.
        ''' </returns>
        Public Function KeyExists(ByVal key As String) As Boolean
            Return (Me.Configuration.AppSettings.Settings.AllKeys.Length > 0) AndAlso
                Me.Configuration.AppSettings.Settings.AllKeys.Contains(key)
        End Function

        ''' <summary> Saves the modified settings. </summary>
        ''' <remarks>
        ''' Refreshes the section so that it will be read the next time the class is instantiated.
        ''' </remarks>
        Public Sub Save()
            Me.Configuration.Save(Global.System.Configuration.ConfigurationSaveMode.Modified)
            System.Configuration.ConfigurationManager.RefreshSection("appSettings")
        End Sub

        ''' <summary> Gets a key value or empty if the key does not exist. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="key"> . </param>
        ''' <returns> The value. </returns>
        Public Function ReadValue(ByVal key As String) As String
            Return If(Me.KeyExists(key), Me.Configuration.AppSettings.Settings(key).Value, String.Empty)
        End Function

        ''' <summary> Sets the specified key value. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="key">   The key. </param>
        ''' <param name="value"> The value. </param>
        ''' <returns> System.String. </returns>
        Public Function WriteValue(ByVal key As String, ByVal value As String) As String
            If Me.KeyExists(key) Then
                Me.Configuration.AppSettings.Settings(key).Value = value
            Else
                Me.Configuration.AppSettings.Settings.Add(key, value)
            End If
            Return Me.ReadValue(key)
        End Function

#End Region

#Region " STRONG TYPED I/O "

        ''' <summary> Reads the application setting. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="key">          The key. </param>
        ''' <param name="defaultValue"> The default value. </param>
        ''' <returns> System.Drawing.Point. </returns>
        Public Function ReadValue(ByVal key As String, ByVal defaultValue As System.Drawing.Point) As System.Drawing.Point
            Dim p As System.Windows.Point = Me.ReadValue(key, New System.Windows.Point(defaultValue.X, defaultValue.Y))
            Return New System.Drawing.Point(CInt(p.X), CInt(p.Y))
        End Function

        ''' <summary> Reads the application setting. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="key">          The key. </param>
        ''' <param name="defaultValue"> The default value. </param>
        ''' <returns> System.Drawing.Point. </returns>
        Public Function ReadValue(ByVal key As String, ByVal defaultValue As System.Windows.Point) As System.Windows.Point
            Dim loadedValue As String = Me.ReadValue(key)
            Return If(String.IsNullOrWhiteSpace(loadedValue), defaultValue, System.Windows.Point.Parse(loadedValue))
        End Function

        ''' <summary> Reads the application setting. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="key">          The key. </param>
        ''' <param name="defaultValue"> The default value. </param>
        ''' <returns> System.Drawing.Point. </returns>
        Public Function ReadValue(ByVal key As String, ByVal defaultValue As System.Windows.Size) As System.Windows.Size
            Dim loadedValue As String = Me.ReadValue(key)
            Return If(String.IsNullOrWhiteSpace(loadedValue), defaultValue, System.Windows.Size.Parse(loadedValue))
        End Function

        ''' <summary> Reads the application setting. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="key">          The key. </param>
        ''' <param name="defaultValue"> The default value. </param>
        ''' <returns> System.Drawing.Point. </returns>
        Public Function ReadValue(ByVal key As String, ByVal defaultValue As System.Drawing.Size) As System.Drawing.Size
            Dim s As System.Windows.Size = Me.ReadValue(key, New System.Windows.Size(defaultValue.Width, defaultValue.Height))
            Return New System.Drawing.Size(CInt(s.Width), CInt(s.Height))
        End Function

        ''' <summary> Writes the application setting. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="key">   The key. </param>
        ''' <param name="value"> The value. </param>
        Public Sub WriteValue(ByVal key As String, ByVal value As System.Drawing.Point)
            Me.WriteValue(key, New System.Windows.Point(value.X, value.Y))
        End Sub

        ''' <summary> Writes the application setting. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="key">   The key. </param>
        ''' <param name="value"> The value. </param>
        Public Sub WriteValue(ByVal key As String, ByVal value As System.Windows.Point)
            Me.WriteValue(key, value.ToString)
        End Sub

        ''' <summary> Writes the application setting. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="key">   The key. </param>
        ''' <param name="value"> The value. </param>
        Public Sub WriteValue(ByVal key As String, ByVal value As System.Windows.Size)
            Me.WriteValue(key, value.ToString)
        End Sub

        ''' <summary> Writes the application setting. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="key">   The key. </param>
        ''' <param name="value"> The value. </param>
        Public Sub WriteValue(ByVal key As String, ByVal value As System.Drawing.Size)
            Me.WriteValue(key, New System.Windows.Size(value.Width, value.Height))
        End Sub

#End Region

    End Class

End Namespace
