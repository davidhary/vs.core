Imports System.ComponentModel
Imports System.Windows.Forms

Partial Public Class ModelViewTalkerBase

    ''' <summary> Gets or sets the publish binding success enabled. </summary>
    ''' <value> The publish binding success enabled. </value>
    <Browsable(False), DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)>
    Public Property PublishBindingSuccessEnabled As Boolean

    ''' <summary> Takes the binding failed actions. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="binding"> <see cref="Object"/> instance of this
    '''                                              <see cref="Control"/> </param>
    ''' <param name="e">       Binding complete event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub OnBindingFailed(ByVal binding As Binding, ByVal e As BindingCompleteEventArgs)
        Dim activity As String = String.Empty
        If binding Is Nothing OrElse e Is Nothing Then Return
        Try
            activity = "setting cancel state"
            e.Cancel = e.BindingCompleteState <> BindingCompleteState.Success
            activity = $"binding {e.Binding.BindingMemberInfo.BindingField}:{e.BindingCompleteContext}:{e.Binding.BindableComponent}:{e.BindingCompleteState}"
            If e.BindingCompleteState = BindingCompleteState.DataError Then
                activity = $"data error; {activity}"
                Me.PublishWarning($"{activity};. {e.ErrorText}")
            ElseIf e.BindingCompleteState = BindingCompleteState.Exception Then
                If Not String.IsNullOrWhiteSpace(e.ErrorText) Then
                    activity = $"{activity}; {e.ErrorText}"
                End If
                Me.PublishException(activity, e.Exception)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Takes the binding succeeded actions. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="binding"> <see cref="Object"/> instance of this
    '''                                               <see cref="Control"/> </param>
    ''' <param name="e">       Event information to send to registered event handlers. </param>
    Protected Overrides Sub OnBindingSucceeded(ByVal binding As Binding, ByVal e As BindingCompleteEventArgs)
        If Me.PublishBindingSuccessEnabled AndAlso binding IsNot Nothing AndAlso e IsNot Nothing Then
            Me.PublishVerbose($"binding {e.Binding.BindingMemberInfo.BindingField}:{e.BindingCompleteContext}:{e.Binding.BindableComponent}:{e.BindingCompleteState}")
        End If
    End Sub

    ''' <summary> Executes the binding exception action. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="activity">  The activity. </param>
    ''' <param name="exception"> The exception. </param>
    Protected Overrides Sub OnBindingException(ByVal activity As String, ByVal exception As Exception)
        Me.PublishException(activity, exception)
    End Sub

End Class
