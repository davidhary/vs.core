Imports System.ComponentModel
Imports System.Drawing
Imports System.Windows.Forms

Partial Public Class ModelViewBase

    ''' <summary> The with events control. </summary>

    Private WithEvents _ToolTip As Windows.Forms.ToolTip

        Private WithEvents _InfoProvider As InfoProvider

    'Required by the Windows Form Designer
    Private components As IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me._InfoProvider = New InfoProvider(Me.components)
        Me._ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        CType(Me._InfoProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._InfoProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_InfoProvider
        '
        Me._InfoProvider.ContainerControl = Me
        '
        'ModelViewBase
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = AutoScaleMode.Inherit
        Me.Font = New Font(SystemFonts.MessageBoxFont.FontFamily, 9.75!, FontStyle.Regular, GraphicsUnit.Point, CType(0, Byte))
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Name = "ModelViewBase"
        Me.Size = New System.Drawing.Size(175, 173)
        CType(Me._InfoProvider, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

End Class
