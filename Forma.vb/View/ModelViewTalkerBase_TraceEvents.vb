Imports System.ComponentModel

Imports isr.Core.Constructs

Partial Public Class ModelViewTalkerBase
    Implements ITalker

#Region " TRACE EVENTS: LOG AND SHOW "

    ''' <summary> The trace events. </summary>
    Private Shared _TraceEvents As List(Of KeyValuePair(Of TraceEventType, String))

    ''' <summary> Gets the trace events. </summary>
    ''' <value> The trace events. </value>
    Public Shared ReadOnly Property TraceEvents As IEnumerable(Of KeyValuePair(Of TraceEventType, String))
        Get
            If ModelViewTalkerBase._TraceEvents Is Nothing OrElse Not ModelViewTalkerBase._TraceEvents.Any Then
                ModelViewTalkerBase._TraceEvents = New List(Of KeyValuePair(Of TraceEventType, String)) From {
                                ModelViewTalkerBase.ToTraceEvent(TraceEventType.Critical),
                                ModelViewTalkerBase.ToTraceEvent(TraceEventType.Error),
                                ModelViewTalkerBase.ToTraceEvent(TraceEventType.Warning),
                                ModelViewTalkerBase.ToTraceEvent(TraceEventType.Information),
                                ModelViewTalkerBase.ToTraceEvent(TraceEventType.Verbose)}
            End If
            Return ModelViewTalkerBase._TraceEvents
        End Get
    End Property

    ''' <summary> The trace log events. </summary>
    Private _TraceLogEvents As InvokingBindingList(Of KeyValuePair(Of TraceEventType, String))

    ''' <summary> Gets a list of types of the trace events. </summary>
    ''' <value> A list of types of the trace events. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property TraceLogEvents As InvokingBindingList(Of KeyValuePair(Of TraceEventType, String))
        Get
            If Me._TraceLogEvents Is Nothing OrElse Not Me._TraceLogEvents.Any Then
                Me._TraceLogEvents = New InvokingBindingList(Of KeyValuePair(Of TraceEventType, String))(ModelViewTalkerBase.TraceEvents.ToArray)
            End If
            Return Me._TraceLogEvents
        End Get
    End Property

    ''' <summary> Converts a value to a trace event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> Value as a KeyValuePair(Of TraceEventType, String) </returns>
    Public Shared Function ToTraceEvent(ByVal value As TraceEventType) As KeyValuePair(Of TraceEventType, String)
        Return New KeyValuePair(Of TraceEventType, String)(value, value.ToString)
    End Function

    ''' <summary> The trace log event. </summary>
    Private _TraceLogEvent As KeyValuePair(Of TraceEventType, String)

    ''' <summary> Gets or sets the trace log level. </summary>
    ''' <value> The trace level. </value>
    <Browsable(False), DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)>
    Public Property TraceLogEvent As KeyValuePair(Of TraceEventType, String)
        Get
            Return Me._TraceLogEvent
        End Get
        Set(value As KeyValuePair(Of TraceEventType, String))
            If value.Key <> Me.TraceLogEvent.Key Then
                Me._TraceLogEvent = value
                Me.TraceLogLevel = value.Key
                Me.SyncNotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the trace log level. </summary>
    ''' <value> The trace level. </value>
    <Browsable(False), DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)>
    Public Property TraceLogLevel As TraceEventType
        Get
            Return If(Me.Talker Is Nothing, TraceEventType.Information, Me.Talker.TraceLogLevel)
        End Get
        Set(value As TraceEventType)
            If Me.Talker IsNot Nothing AndAlso value <> Me.TraceLogLevel Then
                Me.ApplyTalkerTraceLevel(ListenerType.Logger, value)
                Me.SyncNotifyPropertyChanged()
                Me.TraceLogEvent = ModelViewTalkerBase.ToTraceEvent(value)
            End If
        End Set
    End Property

    ''' <summary> The trace show events. </summary>
    Private _TraceShowEvents As BindingList(Of KeyValuePair(Of TraceEventType, String))

    ''' <summary> Gets a list of types of the trace events. </summary>
    ''' <value> A list of types of the trace events. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property TraceShowEvents As BindingList(Of KeyValuePair(Of TraceEventType, String))
        Get
            If Me._TraceShowEvents Is Nothing OrElse Not Me._TraceShowEvents.Any Then
                Me._TraceShowEvents = New InvokingBindingList(Of KeyValuePair(Of TraceEventType, String))(ModelViewTalkerBase.TraceEvents.ToArray)
            End If
            Return Me._TraceShowEvents
        End Get
    End Property

    ''' <summary> The trace show event. </summary>
    Private _TraceShowEvent As KeyValuePair(Of TraceEventType, String)

    ''' <summary> Gets or sets the trace Show level. </summary>
    ''' <value> The trace level. </value>
    <Browsable(False), DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)>
    Public Property TraceShowEvent As KeyValuePair(Of TraceEventType, String)
        Get
            Return Me._TraceShowEvent
        End Get
        Set(value As KeyValuePair(Of TraceEventType, String))
            If Not KeyValuePair(Of TraceEventType, String).Equals(value, Me.TraceShowEvent) Then
                Me._TraceShowEvent = value
                Me.TraceShowLevel = value.Key
                Me.SyncNotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the trace Show level. </summary>
    ''' <value> The trace level. </value>
    <Browsable(False), DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)>
    Public Property TraceShowLevel As TraceEventType
        Get
            Return TraceEventType.Information
            Return If(Me.Talker Is Nothing, TraceEventType.Information, Me.Talker.TraceShowLevel)
        End Get
        Set(value As TraceEventType)
            If Me.Talker IsNot Nothing AndAlso value <> Me.TraceShowLevel Then
                Me.ApplyTalkerTraceLevel(ListenerType.Display, value)
                Me.SyncNotifyPropertyChanged()
                Me.TraceShowEvent = ModelViewTalkerBase.ToTraceEvent(value)
            End If
        End Set
    End Property

#End Region

#Region " TRACE EVENT COMBO BOX MANAGEMENT "

    ''' <summary> List of types of the trace events. </summary>
    Private Shared _TraceEventTypes As InvokingBindingList(Of TraceEventType)

    ''' <summary> Gets a list of types of the trace events. </summary>
    ''' <value> A list of types of the trace events. </value>
    Public Shared ReadOnly Property TraceEventTypes As BindingList(Of TraceEventType)
        Get
            If ModelViewTalkerBase._TraceEventTypes Is Nothing OrElse Not ModelViewTalkerBase._TraceEventTypes.Any Then
                ModelViewTalkerBase._TraceEventTypes = New InvokingBindingList(Of TraceEventType) From {TraceEventType.Critical, TraceEventType.Error,
                                                                                                TraceEventType.Warning, TraceEventType.Information,
                                                                                                TraceEventType.Verbose}
            End If
            Return ModelViewTalkerBase._TraceEventTypes
        End Get
    End Property

    ''' <summary> List trace event levels. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="control"> The control. </param>
    Public Shared Sub ListTraceEventLevels(ByVal control As System.Windows.Forms.ComboBox)
        If control Is Nothing Then Throw New ArgumentNullException(NameOf(control))
        Dim comboEnabled As Boolean = control.Enabled
        control.Enabled = False
        control.DataSource = Nothing
        control.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
        control.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
        control.Items.Clear()
        control.DataSource = ModelViewTalkerBase.TraceEvents
        ' This does not seem to work. It lists the items in the combo box, but the control items are empty.
        ' control.DataSource = isr.Core.Services.EnumExtensions.ValueDescriptionPairs(TraceEventType.Critical).ToList
        ' control.SelectedIndex = -1
        control.Enabled = comboEnabled
        control.Invalidate()
    End Sub

    ''' <summary> Select item. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="control"> The control. </param>
    ''' <param name="value">   The value. </param>
    Public Shared Sub SelectItem(ByVal control As Windows.Forms.ToolStripComboBox, ByVal value As TraceEventType)
        If control IsNot Nothing Then
            ' control.SelectedItem = New KeyValuePair(Of TraceEventType, String)(value, value.ToString)
            control.SelectedItem = ModelViewTalkerBase.ToTraceEvent(value)
        End If
    End Sub

    ''' <summary> Selected value. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="control">      The control. </param>
    ''' <param name="defaultValue"> The default value. </param>
    ''' <returns> A TraceEventType. </returns>
    Public Shared Function SelectedValue(ByVal control As Windows.Forms.ToolStripComboBox, ByVal defaultValue As TraceEventType) As TraceEventType
        If control IsNot Nothing AndAlso control.SelectedItem IsNot Nothing Then
            Dim kvp As KeyValuePair(Of TraceEventType, String) = CType(control.SelectedItem, KeyValuePair(Of TraceEventType, String))
            If [Enum].IsDefined(GetType(TraceEventType), kvp.Key) Then
                defaultValue = kvp.Key
            End If
        End If
        Return defaultValue
    End Function

#End Region

End Class

