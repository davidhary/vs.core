Imports System.ComponentModel
Imports System.Drawing
Imports System.Windows.Forms

''' <summary>
''' A user control base. Supports property change notification and trace message broadcasting
''' (talker).
''' </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 12/26/2015, 2.1.5836. </para>
''' </remarks>
Public Class ModelViewTalkerBase
    Inherits ModelViewBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' A private constructor for this class making it not publicly creatable. This ensure using the
    ''' class as a singleton.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Protected Sub New()
        Me.New(New TraceMessageTalker)
    End Sub

    ''' <summary>
    ''' A protected constructor for this class making it not publicly creatable. This ensure using
    ''' the class as a base class.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="talker"> The talker. </param>
    Protected Sub New(ByVal talker As ITraceMessageTalker)
        MyBase.New()
        Me.InitializingComponents = True
        Me.InitializeComponent()
        Me.InitializingComponents = False
        Me._Talker = talker
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.RemovePropertyChangedEventHandlers()
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#Region " Windows Form Designer generated code "

    'Required by the Windows Form Designer
#Disable Warning IDE1006 ' Naming Styles
    ''' <summary> The components. </summary>
    Private components As IContainer
#Enable Warning IDE1006 ' Naming Styles

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.SuspendLayout()
        '
        'ModelViewTalkerBase
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = AutoScaleMode.Inherit
        Me.Font = New Font(SystemFonts.MessageBoxFont.FontFamily, 9.75!, FontStyle.Regular, GraphicsUnit.Point, CType(0, Byte))
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Name = "ModelViewTalkerBase"
        Me.Size = New System.Drawing.Size(175, 173)
        Me.ResumeLayout(False)

    End Sub

#End Region

#End Region

#Region " FORM EVENTS "

    ''' <summary>
    ''' Handles the container form <see cref="E:System.Windows.Forms.Closing" /> event.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> An <see cref="T:ComponentModel.CancelEventArgs" /> that contains the event
    '''                  data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub OnFormClosing(e As CancelEventArgs)
        MyBase.OnFormClosing(e)
        Try
            If e IsNot Nothing AndAlso Not e.Cancel Then
                Me.RemovePrivateListeners()
            End If
        Finally
        End Try
    End Sub

#End Region

End Class

