Imports System.Windows.Forms

Partial Public Class ModelViewBase
    Inherits UserControl

    ''' <summary>
    ''' Sets the Tool tip for all form controls that inherit a <see cref="Control">control base.</see>
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="parent">  Reference to the parent form or control. </param>
    ''' <param name="toolTip"> The parent form or control tool tip. </param>
    Public Shared Sub ToolTipSetter(ByVal parent As System.Windows.Forms.Control, ByVal toolTip As ToolTip)
        If parent Is Nothing Then Return
        If toolTip Is Nothing Then Return
        Dim view As ModelViewBase = TryCast(parent, ModelViewBase)
        If view IsNot Nothing Then
            view.ToolTipSetter(toolTip)
        ElseIf parent.HasChildren Then
            For Each control As System.Windows.Forms.Control In parent.Controls
                ToolTipSetter(control, toolTip)
            Next
        End If
    End Sub

    ''' <summary>
    ''' Sets a tool tip for all controls on the user control. Uses the message already set for this
    ''' control.
    ''' </summary>
    ''' <remarks>
    ''' This is required because setting a tool tip from the parent form does not show the tool tip
    ''' if hovering above children controls hosted by the user control.
    ''' </remarks>
    ''' <param name="toolTip"> The tool tip control from the parent form. </param>
    Public Sub ToolTipSetter(ByVal toolTip As ToolTip)
        If toolTip IsNot Nothing Then
            Me.ApplyToolTipToChildControls(Me, toolTip, toolTip.GetToolTip(Me))
        End If
    End Sub

    ''' <summary> Sets a tool tip for all controls on the user control. </summary>
    ''' <remarks>
    ''' This is required because setting a tool tip from the parent form does not show the tool tip
    ''' if hovering above children controls hosted by the user control.
    ''' </remarks>
    ''' <param name="toolTip"> The tool tip control from the parent form. </param>
    ''' <param name="message"> The tool tip message to apply to all the children controls and their
    '''                        children. </param>
    Public Sub ToolTipSetter(ByVal toolTip As ToolTip, ByVal message As String)
        Me.ApplyToolTipToChildControls(Me, toolTip, message)
    End Sub

    ''' <summary>
    ''' Applies the tool tip to all control hosted by the parent as well as all the children with
    ''' these control.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="parent">  The parent control. </param>
    ''' <param name="toolTip"> The tool tip control from the parent form. </param>
    ''' <param name="message"> The tool tip message to apply to all the children controls and their
    '''                        children. </param>
    Private Sub ApplyToolTipToChildControls(ByVal parent As Control, ByVal toolTip As ToolTip, ByVal message As String)
        For Each control As Control In parent.Controls
            toolTip.SetToolTip(control, message)
            If parent.HasChildren Then
                Me.ApplyToolTipToChildControls(control, toolTip, message)
            End If
        Next
    End Sub

End Class

