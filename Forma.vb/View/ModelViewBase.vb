Imports System.ComponentModel
Imports System.Windows.Forms

''' <summary>
''' A user control base. Supports property change notifications. Useful for a settings publisher.
''' </summary>
''' <remarks>
''' The <see cref="PropertyChangedEventContext"/> and the custom
''' <see cref="PropertyChangedEventHandler"/> make the PropertyChanged event fire on the
''' SynchronizationContext of the listening code. <para>
''' The PropertyChanged event declaration is modified to a custom event handler. </para><para>
''' As each handler may have a different context, the AddHandler block stores the handler being
''' passed in as well as the SynchronizationContext to be used later when raising the
''' event.</para> <para>
''' (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License. </para><para>  
''' David, 11/02/2010, 1.2.3988.x. </para>
''' </remarks>
Public Class ModelViewBase
    Inherits UserControl

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Gets the initializing components sentinel. </summary>
    ''' <value> The initializing components sentinel. </value>
    Protected Property InitializingComponents As Boolean

    ''' <summary>
    ''' A private constructor for this class making it not publicly creatable. This ensure using the
    ''' class as a singleton.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Protected Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        Me.InitializeComponent()
        Me.InitializingComponents = False
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.RemoveFormClosingEventHandlers()
                Me.RemovePropertyChangedEventHandlers()
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " FORM LOAD "

    ''' <summary> Handles the <see cref="E:System.Windows.Forms.UserControl.Load" /> event. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnLoad(e As EventArgs)
        Try
            If Not Me.DesignMode Then
                Me.InfoProvider.Clear()
                isr.Core.WinForms.ControlCollectionExtensions.Methods.ToolTipSetter(Me.Controls, Me.ToolTip)
            End If
        Finally
            MyBase.OnLoad(e)
        End Try
    End Sub

#End Region

#Region " CLOSING "

    ''' <summary> Removes the FormClosing event handlers. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Protected Sub RemoveFormClosingEventHandlers()
        Me._FormClosingEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The FormClosing event handlers. </summary>
    Private ReadOnly _FormClosingEventHandlers As New EventHandlerContextCollection(Of System.ComponentModel.CancelEventArgs)

    ''' <summary> Event queue for all listeners interested in FormClosing events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event FormClosing As EventHandler(Of System.ComponentModel.CancelEventArgs)
        AddHandler(value As EventHandler(Of System.ComponentModel.CancelEventArgs))
            Me._FormClosingEventHandlers.Add(New EventHandlerContext(Of System.ComponentModel.CancelEventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of System.ComponentModel.CancelEventArgs))
            Me._FormClosingEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As System.ComponentModel.CancelEventArgs)
            Me._FormClosingEventHandlers.Send(sender, e)
        End RaiseEvent
    End Event

    ''' <summary>
    ''' Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or invokes the
    ''' <see cref="FormClosing">FormClosing Event</see>.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Protected Sub SyncNotifyFormClosing(ByVal e As System.ComponentModel.CancelEventArgs)
        Me._FormClosingEventHandlers.Send(Me, e)
    End Sub

    ''' <summary> Handles the container form closing when the parent form is closing. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Cancel event information. </param>
    Private Sub HandleFormClosing(ByVal sender As Object, ByVal e As CancelEventArgs)
        Me.OnFormClosing(e)
    End Sub

    ''' <summary>
    ''' Handles the container form <see cref="E:System.Windows.Forms.Closing" /> event.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> An <see cref="T:ComponentModel.CancelEventArgs" /> that contains the event
    '''                  data. </param>
    Protected Overridable Sub OnFormClosing(e As CancelEventArgs)
        Me.SyncNotifyFormClosing(e)
    End Sub

    ''' <summary> Gets the ancestor form. </summary>
    ''' <value> The ancestor form. </value>
    Private ReadOnly Property AncestorForm As Form

    ''' <summary> Searches for the first ancestor form. </summary>
    ''' <remarks>
    ''' When called from the <see cref="ParentChanged"/> event, the method returns the form
    ''' associated with the top control.  The top control attends a form only after the form layout
    ''' completes, so this method ends up being called a few times before the form is located.
    ''' </remarks>
    ''' <returns> The found ancestor form. </returns>
    Private Function FindAncestorForm() As Form
        Dim parentControl As Control = Me.Parent
        Dim parentForm As Form = TryCast(parentControl, Form)
        Do Until parentForm IsNot Nothing OrElse parentControl Is Nothing
            parentControl = parentControl.Parent
            parentForm = TryCast(parentControl, Form)
        Loop
        Return parentForm
    End Function

    ''' <summary> Adds closing handler. </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    Private Sub AddClosingHandler()
        Dim candidateAncestorForm As Form = Me.FindAncestorForm
        If Me.AncestorForm IsNot Nothing Then
            RemoveHandler Me.AncestorForm.Closing, AddressOf Me.HandleFormClosing
        End If
        Me._AncestorForm = candidateAncestorForm
        If Me.AncestorForm IsNot Nothing Then
            AddHandler Me.AncestorForm.Closing, AddressOf Me.HandleFormClosing
        End If
    End Sub

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.Control.ParentChanged" /> event.
    ''' </summary>
    ''' <remarks> David, 2020-09-24. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnParentChanged(e As EventArgs)
        MyBase.OnParentChanged(e)
        Me.AddClosingHandler()
    End Sub

#End Region

#Region " CORE PROPERTIES  "

    ''' <summary> Gets the information provider. </summary>
    ''' <value> The information provider. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Protected ReadOnly Property InfoProvider As InfoProvider
        Get
            Return Me._InfoProvider
        End Get
    End Property

    ''' <summary> Gets the tool tip. </summary>
    ''' <value> The tool tip. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Protected ReadOnly Property ToolTip As ToolTip
        Get
            Return Me._ToolTip
        End Get
    End Property

    ''' <summary> The status prompt. </summary>
    Private _StatusPrompt As String

    ''' <summary> The status prompt. </summary>
    ''' <value> The status prompt. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property StatusPrompt As String
        Get
            Return Me._StatusPrompt
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.StatusPrompt) Then
                Me._StatusPrompt = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

End Class

