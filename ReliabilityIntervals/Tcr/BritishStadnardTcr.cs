using System;
using System.Collections.Generic;

namespace isr.Core.ReliabilityIntervals
{

    /// <summary>
    /// British Standard (BS EN 60062) Temperature coefficient acceptance intervals.
    /// </summary>
    /// <remarks>
    /// This class is sealed to ensure that the hash value of its elements is not used
    /// by two instances with different hash value set. (c) 2014 Integrated Scientific
    /// Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-05-19 </para>
    /// </remarks>
    public sealed class BritishStadnardTcr
    {

        /// <summary> The ppm. </summary>
        private const string _Ppm = "ppm";

        /// <summary> Gets or sets the units caption. </summary>
        /// <value> The units format. </value>
        public static string UnitsCaption { get; set; } = $"{_Ppm}/{Convert.ToChar( 0x2070 )}K";

        /// <summary> Gets or sets the caption format. </summary>
        /// <value> The caption format. </value>
        public static string CaptionFormat { get; set; } = $"±{{0}} {UnitsCaption}";

        /// <summary> Gets or sets the compound Key-Value caption format. </summary>
        /// <value> The compound caption format. </value>
        public static string KeyValueCaptionFormat { get; set; } = $"{{0}}: ±{{1}} {UnitsCaption}";

        /// <summary> Builds a caption. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns>
        /// A String value using the
        /// <see cref="BritishStadnardTcr.CaptionFormat">caption format</see>.
        /// </returns>
        public static string BuildCaption( double value )
        {
            double scaleFactor = 1d;
            if ( CaptionFormat.Contains( _Ppm ) )
            {
                scaleFactor = 1000000.0d;
            }

            return BuildCaption( value, scaleFactor, CaptionFormat );
        }

        /// <summary> Builds a caption. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value">       The value. </param>
        /// <param name="scaleFactor"> The scale factor. </param>
        /// <param name="format">      Describes the format to use. </param>
        /// <returns>
        /// A String value using the
        /// <see cref="BritishStadnardTcr.CaptionFormat">caption format</see>.
        /// </returns>
        public static string BuildCaption( double value, double scaleFactor, string format )
        {
            return string.Format( System.Globalization.CultureInfo.CurrentCulture, format, scaleFactor * value );
        }

        /// <summary> The unknown Coefficient value. </summary>
        /// <value> The unknown value. </value>
        public static KeyValuePair<string, double> UnknownValue { get; set; } = new KeyValuePair<string, double>( "?", 0.001d );

        /// <summary> Gets or sets the unknown code. </summary>
        /// <value> The unknown code. </value>
        public static string UnknownCode
        {
            get => UnknownValue.Key;

            set => UnknownValue = new KeyValuePair<string, double>( value, UnknownValue.Value );
        }

        /// <summary> The unknown Coefficient value. </summary>
        /// <value> The user value. </value>
        public static KeyValuePair<string, double> UserValue { get; set; } = new KeyValuePair<string, double>( "@", 0.001d );

        /// <summary> Gets or sets the user code. </summary>
        /// <value> The user code. </value>
        public static string UserCode
        {
            get => UserValue.Key;

            set => UserValue = new KeyValuePair<string, double>( value, UserValue.Value );
        }

        /// <summary> The dictionary. </summary>
        private static IDictionary<string, double> _Dictionary;

        /// <summary> Gets the <see cref="AcceptanceIntervalCollection">dictionary</see>. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> A Dictionary(Of String, CodedIntervalBase). </returns>
        public static IDictionary<string, double> Dictionary()
        {
            if ( _Dictionary is null || _Dictionary.Count == 0 )
            {
                BuildDictionary();
            }

            return _Dictionary;
        }

        /// <summary> Builds coded interval base dictionary. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="values"> The values. </param>
        /// 
        /// ### <exception cref="ArgumentNullException"> Thrown when one or more required arguments are
        /// null. </exception>
        public static void BuildDictionary( IDictionary<string, double> values )
        {
            _Dictionary = new Dictionary<string, double>();
            foreach ( KeyValuePair<string, double> value in values )
            {
                _Dictionary.Add( value.Key, value.Value );
            }
        }

        /// <summary> Builds Coefficient information dictionary. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        private static void BuildDictionary()
        {
            var dix = new Dictionary<string, double>() { { "K", 0.000001d }, { "M", 0.000005d }, { "N", 0.00001d }, { "Z", 0.00002d }, { "Q", 0.000025d }, { "P", 0.000015d }, { "R", 0.00005d }, { "S", 0.0001d }, { "U", 0.00025d }, { "1", 0.0001d }, { "5", 0.0001d }, { "2", 0.00005d }, { "6", 0.00005d }, { "3", 0.000025d }, { "7", 0.000025d }, { "4", 0.00025d }, { UnknownValue.Key, UnknownValue.Value }, { UserValue.Key, UserValue.Value } };  // TO_DO _ not right.()
            BuildDictionary( dix );
        }

        /// <summary> The compound captions. </summary>
        private static CompoundCaptionCollection _CompoundCaptions;

        /// <summary> Gets the compound captions. </summary>
        /// <value> The compound captions. </value>
        public static CompoundCaptionCollection CompoundCaptions
        {
            get {
                if ( _CompoundCaptions is null || _CompoundCaptions.Count == 0 )
                {
                    _CompoundCaptions = new CompoundCaptionCollection();
                    _CompoundCaptions.Populate( Dictionary(), KeyValueCaptionFormat );
                }

                return _CompoundCaptions;
            }
        }
    }
}
