namespace isr.Core.ReliabilityIntervals
{
    /// <summary> Nominal reliability interval. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-01-09 </para>
    /// </remarks>
    public class NominalReliabilityInterval : ReliabilityInterval
    {

        #region " CONSTRUCTOR"

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        public NominalReliabilityInterval() : base()
        {
            this.Epsilon = DefaultEpsilon;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="endPoint"> The end point of a symmetrical interval. </param>
        public NominalReliabilityInterval( double endPoint ) : base( endPoint, -endPoint, DefaultEpsilon )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="lowEndPoint">  The low end point. </param>
        /// <param name="highEndPoint"> The high end point. </param>
        public NominalReliabilityInterval( double lowEndPoint, double highEndPoint ) : base( lowEndPoint, highEndPoint, DefaultEpsilon )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> The value. </param>
        public NominalReliabilityInterval( NominalReliabilityInterval value ) : base( value )
        {
            this.Epsilon = DefaultEpsilon;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> The value. </param>
        public NominalReliabilityInterval( ReliabilityInterval value ) : base( value )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="lowEndPoint">  The low end point. </param>
        /// <param name="highEndPoint"> The high end point. </param>
        public NominalReliabilityInterval( double? lowEndPoint, double? highEndPoint ) : base( lowEndPoint, highEndPoint, DefaultEpsilon )
        {
        }

        /// <summary> Creates a new object that is a copy of the current instance. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> A new object that is a copy of this instance. </returns>
        public override object Clone()
        {
            return new NominalReliabilityInterval( this );
        }

        /// <summary> Gets the empty interval. </summary>
        /// <value> The empty. </value>
        public static new ReliabilityInterval Empty => new NominalReliabilityInterval( 0d, 0d );

        #endregion

        /// <summary> Gets or sets the default minimum detectable difference. </summary>
        /// <value> The epsilon (1E-6). </value>
        public static double DefaultEpsilon { get; set; } = 0.000001d;
    }
}
