using System;
using System.Collections.Generic;

namespace isr.Core.ReliabilityIntervals
{

    /// <summary> A sealed class defining the acceptance Interval. </summary>
    /// <remarks>
    /// Defines a class that encodes intervals based on offsets from a nominal value. (c) 2014
    /// Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-09-20, 2.1.5376. </para>
    /// </remarks>
    public abstract class AcceptanceInterval
    {

        /// <summary> Specialized default constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        protected AcceptanceInterval() : base()
        {
        }

        /// <summary> Constructor for symmetric margin factors. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="code">         The code. </param>
        /// <param name="marginFactor"> The margin factor. </param>
        /// <param name="caption">      The caption. </param>
        protected AcceptanceInterval( string code, ReliabilityInterval marginFactor, string caption ) : base()
        {
            this.Code = code;
            this.RelativeInterval = marginFactor is null ? ReliabilityInterval.Empty : ReliabilityInterval.CreateInstance( marginFactor );
            this.Caption = caption;
            this.CompoundCaption = BuildCompoundCaptionFormat( code, caption );
            this.Parsed = true;
        }

        /// <summary> The clone Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> The value. </param>
        protected AcceptanceInterval( AcceptanceInterval value ) : this( value.Code, value.RelativeInterval, value.Caption )
        {
        }

        /// <summary> Gets or sets the acceptance interval code. </summary>
        /// <value> The code. </value>
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the sentinel indicating that the interval was parsed using the interval code.
        /// </summary>
        /// <value> The sentinel indicating that the interval was parsed. </value>
        public bool Parsed { get; set; }

        /// <summary> The relative interval. </summary>
        private ReliabilityInterval _RelativeInterval;

        /// <summary>
        /// Gets or sets the relative interval. This is the interval, which actual values depend on the
        /// nominal value. The relative interval can be compared to the relative deviation from the
        /// nominal value. For example, +/- 5%.
        /// </summary>
        /// <value> The margin factor. </value>
        public ReliabilityInterval RelativeInterval
        {
            get => this._RelativeInterval;

            set {
                this._RelativeInterval = value;
                // update the acceptance interval.
                this.NominalValue = this._NominalValue;
            }
        }

        /// <summary> The nominal value. </summary>
        private double _NominalValue;

        /// <summary> Gets or sets the nominal value. </summary>
        /// <remarks> This also updates the interval based on the relative interval. </remarks>
        /// <value> The nominal value. </value>
        public double NominalValue
        {
            get => this._NominalValue;

            set {
                this._NominalValue = value;
                this.Interval = new NominalReliabilityInterval( value * (1d + this.RelativeInterval.LowEndPoint), value * (1d + this.RelativeInterval.HighEndPoint) );
            }
        }

        /// <summary>
        /// Gets the acceptance interval with its end points. The interval defines the actual lower and
        /// upper levels.
        /// </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The acceptance interval. </value>
        public ReliabilityInterval Interval { get; set; }

        /// <summary> Query if this object is empty; that is the limits range is zero. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> <c>true</c> if empty; otherwise <c>false</c> </returns>
        public bool IsEmpty()
        {
            return this.Interval.IsEmpty;
        }

        /// <summary> Gets the unknown code. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The unknown code. </value>
        public static string EmptyCode { get; set; } = string.Empty;

        /// <summary> Gets the sentinel indicating if the entered code is empty. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> <c>True</c> if empty code. </value>
        public abstract bool IsEmptyCode { get; }

        /// <summary> Gets the unknown code. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The unknown code. </value>
        public static string UnknownCode { get; set; } = "?";

        /// <summary> Gets the sentinel indicating if the entered code is unknown. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> <c>True</c> if unknown code. </value>
        public abstract bool IsUnknownCode { get; }

        /// <summary> Gets the user code. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The user code. </value>
        public static string UserCode { get; set; } = "@";

        /// <summary> Gets the sentinel indicating if the entered value is user defined. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> <c>True</c> if unknown code. </value>
        public abstract bool IsUserCode { get; }

        /// <summary> Checks if the acceptance interval contains the value. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns>
        /// <c>true</c> if the acceptance interval contains the value; false otherwise.
        /// </returns>
        public bool Contains( double value )
        {
            return this.Interval.Contains( value );
        }

        /// <summary> Gets the caption. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The caption. </value>
        public string Caption { get; set; }

        /// <summary> Gets the compound caption. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The compound caption. </value>
        public string CompoundCaption { get; set; }

        /// <summary> Gets the compound caption format. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The compound caption format. </value>
        public static string CompoundCaptionFormat { get; set; } = "{0}: {1}";

        /// <summary> Builds compound caption format. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="code">    The code. </param>
        /// <param name="caption"> The caption. </param>
        /// <returns> A String. </returns>
        public static string BuildCompoundCaptionFormat( string code, string caption )
        {
            return string.Equals( code, EmptyCode ) ? EmptyCode : string.Format( CompoundCaptionFormat, code, caption );
        }
    }

    /// <summary> Dictionary of acceptance intervals. </summary>
    /// <remarks>
    /// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-09-22 </para>
    /// </remarks>
    public class AcceptanceIntervalCollection : System.Collections.ObjectModel.KeyedCollection<string, AcceptanceInterval>
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        public AcceptanceIntervalCollection() : base()
        {
        }

        /// <summary> Gets key for item. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="item"> The item. </param>
        /// <returns> The key for item. </returns>
        protected override string GetKeyForItem( AcceptanceInterval item )
        {
            return item is null ? throw new ArgumentNullException( nameof( item ) ) : item.Code;
        }

        /// <summary> Populates the given values. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values"> The values. </param>
        public void Populate( AcceptanceIntervalCollection values )
        {
            if ( values is null )
            {
                throw new ArgumentNullException( nameof( values ) );
            }

            this.Clear();
            foreach ( AcceptanceInterval value in values )
            {
                this.Add( value );
            }
        }

        /// <summary> The compound captions. </summary>
        private CompoundCaptionCollection _CompoundCaptions;

        /// <summary> Gets the compound captions. </summary>
        /// <value> The compound captions. </value>
        public CompoundCaptionCollection CompoundCaptions
        {
            get {
                if ( this._CompoundCaptions is null || this._CompoundCaptions.Count == 0 )
                {
                    this._CompoundCaptions = new CompoundCaptionCollection();
                    this._CompoundCaptions.Populate( this );
                }

                return this._CompoundCaptions;
            }
        }
    }

    /// <summary> Collection of compound captions. </summary>
    /// <remarks>
    /// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-12-20 </para>
    /// </remarks>
    public class CompoundCaptionCollection : System.Collections.ObjectModel.Collection<KeyValuePair<string, string>>
    {

        /// <summary> Adds code. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="code">            The code. </param>
        /// <param name="compoundCaption"> The compound caption. </param>
        public void Add( string code, string compoundCaption )
        {
            this.Add( new KeyValuePair<string, string>( code, compoundCaption ) );
        }

        /// <summary> Adds code. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="acceptanceInterval"> The acceptance interval to add. </param>
        public void Add( AcceptanceInterval acceptanceInterval )
        {
            this.Add( acceptanceInterval.Code, acceptanceInterval.CompoundCaption );
        }

        /// <summary> Populates the given values. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="values"> The values. </param>
        public void Populate( AcceptanceIntervalCollection values )
        {
            this.Clear();
            if ( values is object )
            {
                foreach ( AcceptanceInterval value in values )
                {
                    this.Add( value );
                }
            }
        }

        /// <summary> Populates the given values. </summary>
        /// <remarks> David, 2020-04-20. </remarks>
        /// <param name="values">                The values. </param>
        /// <param name="keyValueCaptionFormat"> The compound format. </param>
        public void Populate( IDictionary<string, double> values, string keyValueCaptionFormat )
        {
            this.Clear();
            if ( values is object )
            {
                foreach ( KeyValuePair<string, double> value in values )
                {
                    this.Add( value.Key, string.Format( keyValueCaptionFormat, value.Key, value.Value ) );
                }
            }
        }
    }
}
