using System;

namespace isr.Core.ReliabilityIntervals
{

    /// <summary> Interval. </summary>
    /// <remarks>
    /// This class is set as Abstract to force setting the correct
    /// <see cref="ReliabilityInterval.Epsilon">epsilon</see>. (c) 2014 Integrated Scientific Resources, Inc.
    /// All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-11-25 </para>
    /// </remarks>
    public class ReliabilityInterval : ICloneable
    {

        #region " CONSTRUCTOR"

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        protected ReliabilityInterval() : base()
        {
            this.LowEndPoint = 0d;
            this.HighEndPoint = 0d;
            this.Epsilon = 1.0E-18d;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="lowEndPoint">  The low end point. </param>
        /// <param name="highEndPoint"> The high end point. </param>
        /// <param name="epsilon">      The epsilon. </param>
        protected ReliabilityInterval( double lowEndPoint, double highEndPoint, double epsilon ) : this()
        {
            this.HighEndPoint = highEndPoint;
            this.LowEndPoint = lowEndPoint;
            this.Epsilon = epsilon;
        }

        /// <summary> Creates the instance. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="lowEndPoint">  The low end point. </param>
        /// <param name="highEndPoint"> The high end point. </param>
        /// <param name="epsilon">      The epsilon. </param>
        /// <returns> The new instance. </returns>
        public static ReliabilityInterval CreateInstance( double lowEndPoint, double highEndPoint, double epsilon )
        {
            return new ReliabilityInterval( lowEndPoint, highEndPoint, epsilon );
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="lowEndPoint">  The low end point. </param>
        /// <param name="highEndPoint"> The high end point. </param>
        /// <param name="epsilon">      The epsilon. </param>
        protected ReliabilityInterval( double? lowEndPoint, double? highEndPoint, double epsilon ) : this()
        {
            this.HighEndPoint = highEndPoint.GetValueOrDefault( 0d );
            this.LowEndPoint = lowEndPoint.GetValueOrDefault( 0d );
            this.Epsilon = epsilon;
        }

        /// <summary> Creates the instance. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="lowEndPoint">  The low end point. </param>
        /// <param name="highEndPoint"> The high end point. </param>
        /// <param name="epsilon">      The epsilon. </param>
        /// <returns> The new instance. </returns>
        public static ReliabilityInterval CreateInstance( double? lowEndPoint, double? highEndPoint, double epsilon )
        {
            return new ReliabilityInterval( lowEndPoint, highEndPoint, epsilon );
        }

        /// <summary> Creates the instance. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> The new instance. </returns>
        public static ReliabilityInterval CreateInstance( ReliabilityInterval value )
        {
            return new ReliabilityInterval( value );
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> The value. </param>
        protected ReliabilityInterval( ReliabilityInterval value ) : this()
        {
            if ( value is object )
            {
                this.HighEndPoint = value.HighEndPoint;
                this.LowEndPoint = value.LowEndPoint;
                this.Epsilon = value.Epsilon;
            }
        }

        /// <summary> Gets the empty interval. </summary>
        /// <value> The empty. </value>
        public static ReliabilityInterval Empty => CreateInstance( 0d, 0d, 0.000001d );

        /// <summary> Creates a new object that is a copy of the current instance. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> A new object that is a copy of this instance. </returns>
        public virtual object Clone()
        {
            return CreateInstance( this );
        }

        #endregion

        #region " MEMBERS "

        /// <summary> Gets the epsilon. </summary>
        /// <value> The epsilon [1E-18]. </value>
        public double Epsilon { get; set; }

        /// <summary> Gets the low end point. </summary>
        /// <value> The low end point. </value>
        public double LowEndPoint { get; set; }

        /// <summary> Gets the high end point. </summary>
        /// <value> The high end point. </value>
        public double HighEndPoint { get; set; }

        #endregion

        #region " FUNCTIONS "

        /// <summary> Gets the size or length of the interval. </summary>
        /// <value> The size or length of the interval. </value>
        public double Size => this.HighEndPoint - this.LowEndPoint;

        /// <summary> Gets the center. </summary>
        /// <value> The center. </value>
        public double Center => 0.5d * (this.HighEndPoint + this.LowEndPoint);

        /// <summary> Gets a value indicating whether this object is symmetrical. </summary>
        /// <value> <c>true</c> if this object is symmetrical; otherwise <c>false</c>. </value>
        public bool IsSymmetrical => Math.Abs( this.Center ) < this.Epsilon;

        /// <summary> Gets a value indicating whether the interval is empty--having a zero size. </summary>
        /// <value> <c>true</c> if the interval is empty; otherwise <c>false</c>. </value>
        public bool IsEmpty => this.Size < this.Epsilon;

        /// <summary>
        /// Checks if the specified point is contained in the interval including its end points.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns>
        /// <c>true</c> if the point is contained in the interval including its endpoints; otherwise
        /// <c>false</c>.
        /// </returns>
        public bool Contains( double value )
        {
            return value >= this.LowEndPoint && value <= this.HighEndPoint;
        }

        /// <summary> Check if the value is inside the interval excluding its endpoints. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns>
        /// <c>true</c> if the point is contained in the interval excluding its endpoints; otherwise
        /// <c>false</c>.
        /// </returns>
        public bool Encloses( double value )
        {
            return value > this.LowEndPoint && value < this.HighEndPoint;
        }

        /// <summary> Returns a string that represents the current object. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> A string that represents the current object. </returns>
        public override string ToString()
        {
            return $"({this.LowEndPoint},{this.HighEndPoint})";
        }

        /// <summary> Returns a string that represents the current object. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="format"> Describes the format to use. </param>
        /// <returns> A string that represents the current object. </returns>
        public string ToString( string format )
        {
            return $"({this.LowEndPoint.ToString( format )},{this.HighEndPoint.ToString( format )})";
        }

        #endregion

        #region " EQUALS "

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
        /// <see cref="T:System.Object" />. </param>
        /// <returns>
        /// <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />; otherwise, <c>False</c>.
        /// </returns>
        public override bool Equals( object obj )
        {
            return this.Equals( obj as ReliabilityInterval );
        }

        /// <summary>
        /// Compares two Intervals. The Intervals are compared using their LowerLimits and UpperLimits.
        /// </summary>
        /// <remarks>
        /// The two Intervals are the same if the have the same minimum and maximum values.
        /// </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> A Boolean data type. </returns>
        public bool Equals( double value )
        {
            return Math.Abs( this.HighEndPoint - value ) < this.Epsilon;
        }

        /// <summary>
        /// Compares two Intervals. The Intervals are compared using their LowerLimits and UpperLimits.
        /// </summary>
        /// <remarks>
        /// The two Intervals are the same if the have the same minimum and maximum values.
        /// </remarks>
        /// <param name="other"> Specifies the other <see cref="ReliabilityInterval">Interval</see>
        /// to compare for equality with this instance. </param>
        /// <returns> A Boolean data type. </returns>
        public bool Equals( ReliabilityInterval other )
        {
            return this.Equals( other, this.Epsilon );
        }

        /// <summary>
        /// Compares two Intervals. The Intervals are compared using their LowerLimits and UpperLimits.
        /// </summary>
        /// <remarks>
        /// The two Intervals are the same if the have the same minimum and maximum values.
        /// </remarks>
        /// <param name="other">     Specifies the other <see cref="ReliabilityInterval">Interval</see>
        /// to compare for equality with this instance. </param>
        /// <param name="tolerance"> Specifies the relative tolerance for comparing the two values. The
        /// values are compared based on their end points. </param>
        /// <returns> A Boolean data type. </returns>
        public bool Equals( ReliabilityInterval other, double tolerance )
        {
            return !(other is null) && Math.Abs( other.HighEndPoint - this.HighEndPoint ) < tolerance && Math.Abs( other.LowEndPoint - this.LowEndPoint ) < tolerance;
        }

        /// <summary> Implements the operator =. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool Equals( ReliabilityInterval left, ReliabilityInterval right )
        {
            return ReferenceEquals( left, right ) || left is object && left.Equals( right );
        }

        /// <summary> Implements the operator =. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( ReliabilityInterval left, ReliabilityInterval right )
        {
            return Equals( left, right );
        }

        /// <summary> Implements the operator &lt;&gt;. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( ReliabilityInterval left, ReliabilityInterval right )
        {
            return !Equals( left, right );
        }

        /// <summary> Creates a unique hash code. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> An <see cref="System.Int32">Int32</see> value. </returns>
        public override int GetHashCode()
        {
            return this.HighEndPoint.GetHashCode() ^ this.LowEndPoint.GetHashCode();
        }

        #endregion

    }

}
