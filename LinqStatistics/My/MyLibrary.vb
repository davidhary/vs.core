﻿Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    Partial Public NotInheritable Class MyLibrary

        ''' <summary> Constructor that prevents a default instance of this class from being created. </summary>
        Private Sub New()
            MyBase.New()
        End Sub

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const AssemblyTitle As String = "Core Linq Statistics Library"
        Public Const AssemblyDescription As String = "Core Linq Statistics Library"
        Public Const AssemblyProduct As String = "Core.Linq.Statistics"

    End Class

End Namespace
