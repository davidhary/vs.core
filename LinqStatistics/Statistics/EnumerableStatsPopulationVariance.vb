﻿Namespace EnumerableStats

    Partial Public Module Methods

        ''' <summary> Computes The Population Variance of a sequence of nullable System.Decimal values. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source"> Source for the. </param>
        ''' <returns>
        ''' The Population Variance of the sequence of values, or null if the source sequence is empty or contains
        ''' only values that are null.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function PopulationVariance(ByVal source As IEnumerable(Of Decimal?)) As Decimal?
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Dim values As IEnumerable(Of Decimal) = source.Coalesce()
            Return If(values.Any(), values.PopulationVariance(), DirectCast(Nothing, Decimal?))
        End Function

        ''' <summary> Computes The Population Variance of a sequence of System.Decimal values. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source"> The source values. </param>
        ''' <returns> The Population Variance of the sequence of values. </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function PopulationVariance(ByVal source As IEnumerable(Of Decimal)) As Decimal
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Return CDec(source.Select(Function(x) CDbl(x)).PopulationVariance())
        End Function

        ''' <summary>
        ''' Computes The Population Variance of a sequence of nullable System.Double values.
        ''' </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source"> The source values. </param>
        ''' <returns>
        ''' The Population Variance of the sequence of values, or null if the source sequence is empty or
        ''' contains only values that are null.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function PopulationVariance(ByVal source As IEnumerable(Of Double?)) As Double?
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Dim values As IEnumerable(Of Double) = source.Coalesce()
            Return If(values.Any(), values.PopulationVariance(), DirectCast(Nothing, Double?))
        End Function

        ''' <summary> Computes The Population Variance of a sequence of System.Double values. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source"> Source for the. </param>
        ''' <returns> The Population Variance of the sequence of values. </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function PopulationVariance(ByVal source As IEnumerable(Of Double)) As Double
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            If Not source.Any Then Return 0
            Return source.SumSquareDifferences / source.Count
        End Function

        ''' <summary>
        ''' Computes The Population Variance of a sequence of nullable System.Single values.
        ''' </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source"> The source values. </param>
        ''' <returns>
        ''' The Population Variance of the sequence of values, or null if the source sequence is empty or
        ''' contains only values that are null.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function PopulationVariance(ByVal source As IEnumerable(Of Single?)) As Single?
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Dim values As IEnumerable(Of Single) = source.Coalesce()
            Return If(values.Any(), values.PopulationVariance(), DirectCast(Nothing, Single?))
        End Function

        ''' <summary>
        '''     Computes The Population Variance of a sequence of System.Single values.
        ''' </summary> 
        ''' <param name="source"> The source values. </param>
        ''' <returns>
        '''     The Population Variance of the sequence of values.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function PopulationVariance(ByVal source As IEnumerable(Of Single)) As Single
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Return CSng(source.Select(Function(x) CDbl(x)).PopulationVariance())
        End Function

        ''' <summary>
        '''     Computes The Population Variance of a sequence of nullable System.Int32 values.
        ''' </summary>
        ''' <param name="source"> The source values. </param>
        ''' <returns>
        '''     The Population Variance of the sequence of values, or null if the source sequence is
        '''     empty or contains only values that are null.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function PopulationVariance(ByVal source As IEnumerable(Of Integer?)) As Double?
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Dim values As IEnumerable(Of Integer) = source.Coalesce()
            Return If(values.Any(), values.PopulationVariance(), DirectCast(Nothing, Double?))
        End Function

        ''' <summary> Computes The Population Variance of a sequence of System.Int32 values. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source"> The source values. </param>
        ''' <returns> The Population Variance of the sequence of values. </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function PopulationVariance(ByVal source As IEnumerable(Of Integer)) As Double
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Return source.Select(Function(x) CDbl(x)).PopulationVariance()
        End Function

        ''' <summary>
        ''' Computes The Population Variance of a sequence of nullable System.Int64 values.
        ''' </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source"> The source values. </param>
        ''' <returns>
        ''' The Population Variance of the sequence of values, or null if the source sequence is empty or
        ''' contains only values that are null.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function PopulationVariance(ByVal source As IEnumerable(Of Long?)) As Double?
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Dim values As IEnumerable(Of Long) = source.Coalesce()
            Return If(values.Any(), values.PopulationVariance(), DirectCast(Nothing, Double?))
        End Function

        ''' <summary> Computes The Population Variance of a sequence of System.Int64 values. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source"> The source values. </param>
        ''' <returns> The Population Variance of the sequence of values. </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function PopulationVariance(ByVal source As IEnumerable(Of Long)) As Double
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Return source.Select(Function(x) CDbl(x)).PopulationVariance()
        End Function

        ''' <summary>
        ''' Computes The Population Variance of a sequence of nullable System.Decimal values that are
        ''' obtained by invoking a transform function on each element of the input sequence.
        ''' </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source">   The source values. </param>
        ''' <param name="selector"> A transform function to apply to each element. </param>
        ''' <returns>
        ''' The Population Variance of the sequence of values, or null if the source sequence is empty or
        ''' contains only values that are null.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function PopulationVariance(Of TSource)(ByVal source As IEnumerable(Of TSource), ByVal selector As Func(Of TSource, Decimal?)) As Decimal?
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Return source.Select(selector).PopulationVariance()
        End Function
        '
        ''' <summary>
        '''     Computes The Population Variance of a sequence of System.Decimal values that are obtained
        '''     by invoking a transform function on each element of the input sequence.
        ''' </summary>
        ''' <param name="source">   The source values. </param>
        ''' <param name="selector"> A transform function to apply to each element. </param>
        ''' <returns>
        '''     The Population Variance of the sequence of values.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function PopulationVariance(Of TSource)(ByVal source As IEnumerable(Of TSource), ByVal selector As Func(Of TSource, Decimal)) As Decimal
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Return source.Select(selector).PopulationVariance()
        End Function
        '
        ''' <summary>
        '''     Computes The Population Variance of a sequence of nullable System.Double values that
        '''     are obtained by invoking a transform function on each element of the input
        '''     sequence.
        ''' </summary>
        ''' <param name="source">   The source values. </param>
        ''' <param name="selector"> A transform function to apply to each element. </param>
        ''' <returns>
        '''     The Population Variance of the sequence of values, or null if the source sequence is
        '''     empty or contains only values that are null.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function PopulationVariance(Of TSource)(ByVal source As IEnumerable(Of TSource), ByVal selector As Func(Of TSource, Double?)) As Double?
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Return source.Select(selector).PopulationVariance()
        End Function

        ''' <summary>
        ''' Computes The Population Variance of a sequence of System.Double values that are obtained by
        ''' invoking a transform function on each element of the input sequence.
        ''' </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source">   The source values. </param>
        ''' <param name="selector"> A transform function to apply to each element. </param>
        ''' <returns> The Population Variance of the sequence of values. </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function PopulationVariance(Of TSource)(ByVal source As IEnumerable(Of TSource), ByVal selector As Func(Of TSource, Double)) As Double
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Return source.Select(selector).PopulationVariance()
        End Function

        ''' <summary>
        ''' Computes The Population Variance of a sequence of nullable System.Single values that are
        ''' obtained by invoking a transform function on each element of the input sequence.
        ''' </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source">   The source values. </param>
        ''' <param name="selector"> A transform function to apply to each element. </param>
        ''' <returns>
        ''' The Population Variance of the sequence of values, or null if the source sequence is empty or
        ''' contains only values that are null.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function PopulationVariance(Of TSource)(ByVal source As IEnumerable(Of TSource), ByVal selector As Func(Of TSource, Single?)) As Single?
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Return source.Select(selector).PopulationVariance()
        End Function

        ''' <summary>
        ''' Computes The Population Variance of a sequence of System.Single values that are obtained by
        ''' invoking a transform function on each element of the input sequence.
        ''' System.ArgumentNullException: source or selector is null.
        ''' </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source">   The source values. </param>
        ''' <param name="selector"> A transform function to apply to each element. </param>
        ''' <returns> The Population Variance of the sequence of values. </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function PopulationVariance(Of TSource)(ByVal source As IEnumerable(Of TSource), ByVal selector As Func(Of TSource, Single)) As Single
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Return source.Select(selector).PopulationVariance()
        End Function

        ''' <summary>
        ''' Computes The Population Variance of a sequence of nullable System.Int32 values that are
        ''' obtained by invoking a transform function on each element of the input sequence.
        ''' System.ArgumentNullException: source or selector is null.
        ''' </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source">   The source values. </param>
        ''' <param name="selector"> A transform function to apply to each element. </param>
        ''' <returns>
        ''' The Population Variance of the sequence of values, or null if the source sequence is empty or
        ''' contains only values that are null.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function PopulationVariance(Of TSource)(ByVal source As IEnumerable(Of TSource), ByVal selector As Func(Of TSource, Integer?)) As Double?
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Return source.Select(selector).PopulationVariance()
        End Function

        ''' <summary>
        ''' Computes The Population Variance of a sequence of System.Int32 values that are obtained by
        ''' invoking a transform function on each element of the input sequence.
        ''' System.ArgumentNullException: source or selector is null.
        ''' </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source">   The source values. </param>
        ''' <param name="selector"> A transform function to apply to each element. </param>
        ''' <returns> The Population Variance of the sequence of values. </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function PopulationVariance(Of TSource)(ByVal source As IEnumerable(Of TSource), ByVal selector As Func(Of TSource, Integer)) As Double
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Return source.Select(selector).PopulationVariance()
        End Function

        ''' <summary>
        ''' Computes The Population Variance of a sequence of nullable System.Int64 values that are
        ''' obtained by invoking a transform function on each element of the input sequence.
        ''' </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source">   The source values. </param>
        ''' <param name="selector"> A transform function to apply to each element. </param>
        ''' <returns>
        ''' The Population Variance of the sequence of values, or null if the source sequence is empty or
        ''' contains only values that are null.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function PopulationVariance(Of TSource)(ByVal source As IEnumerable(Of TSource), ByVal selector As Func(Of TSource, Long?)) As Double?
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Return source.Select(selector).PopulationVariance()
        End Function

        ''' <summary>
        '''     Computes The Population Variance of a sequence of System.Int64 values that are obtained
        '''     by invoking a transform function on each element of the input sequence.
        '''        '
        ''' </summary>
        ''' <param name="source">   The source values. </param>
        ''' <param name="selector"> A transform function to apply to each element. </param>
        ''' <returns>
        '''     The Population Variance of the sequence of values.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function PopulationVariance(Of TSource)(ByVal source As IEnumerable(Of TSource), ByVal selector As Func(Of TSource, Long)) As Double
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Return source.Select(selector).PopulationVariance()
        End Function
    End Module

End Namespace
