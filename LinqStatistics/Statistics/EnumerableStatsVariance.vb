﻿Namespace EnumerableStats
    Partial Public Module Methods

        ''' <summary> Computes the Variance of a sequence of nullable System.Decimal values. </summary>
        ''' <param name="source"> A sequence of nullable System.Decimal values to calculate the Variance of. </param>
        ''' <returns> The Variance of the sequence of values, or null if the source sequence is empty or contains only values that are null. </returns>
        ''' <exception cref="System.ArgumentNullException">source is null</exception>
        ''' <exception cref="System.OverflowException">The sum of the elements in the sequence is larger than System.Decimal.MaxValue. </exception>
        <System.Runtime.CompilerServices.Extension>
        Public Function Variance(ByVal source As IEnumerable(Of Decimal?)) As Decimal?
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Dim values As IEnumerable(Of Decimal) = source.Coalesce()
            Return If(values.Any(), values.Variance(), DirectCast(Nothing, Decimal?))
        End Function

        ''' <summary> Computes the Variance of a sequence of System.Decimal values. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source"> The source values. </param>
        ''' <returns> The Variance of the sequence of values. </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Variance(ByVal source As IEnumerable(Of Decimal)) As Decimal
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Return CDec(source.Select(Function(x) CDbl(x)).Variance())
        End Function

        ''' <summary> Computes the Variance of a sequence of nullable System.Double values. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source"> The source values. </param>
        ''' <returns>
        ''' The Variance of the sequence of values, or null if the source sequence is empty or contains
        ''' only values that are null.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Variance(ByVal source As IEnumerable(Of Double?)) As Double?
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Dim values As IEnumerable(Of Double) = source.Coalesce()
            Return If(values.Any(), values.Variance(), DirectCast(Nothing, Double?))
        End Function

        ''' <summary>
        ''' Computes the Variance of a sequence of System.Double values. System.ArgumentNullException:
        ''' source is null.
        ''' </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source"> A sequence of nullable System.Decimal values to calculate the Variance
        '''                                             of. </param>
        ''' <returns> The Variance of the sequence of values. </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Variance(source As IEnumerable(Of Double)) As Double
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            If Not source.Any Then Return 0
            If source.Count = 1 Then Return 0
            Return source.SumSquareDifferences() / (source.Count - 1)
        End Function

        ''' <summary> Computes the sum of square differences. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source"> A sequence of nullable System.Decimal values to calculate the Variance
        '''                       of. </param>
        ''' <returns> The total number of square differences. </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function SumSquareDifferences(source As IEnumerable(Of Double)) As Double
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            If Not source.Any Then Return 0
            Dim mean As Double = source.Average()
            Return source.SumSquareDifferences(mean)
        End Function

        ''' <summary> Computes the sum of square differences. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source"> A sequence of nullable System.Decimal values to calculate the Variance
        '''                                             of. </param>
        ''' <param name="mean">   The mean. </param>
        ''' <returns> The total number of square differences. </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function SumSquareDifferences(ByVal source As IEnumerable(Of Double), ByVal mean As Double) As Double
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            If Not source.Any Then Return 0
            Dim squaredDiffs As IEnumerable(Of Double) = source.[Select](Function(d) (d - mean) * (d - mean))
            Return squaredDiffs.Sum
        End Function

        ''' <summary>
        ''' Computes the Variance of a sequence of nullable System.Single values.
        ''' System.ArgumentNullException: source is null.
        ''' </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source"> A sequence of nullable System.Decimal values to calculate the Variance
        '''                       of. </param>
        ''' <returns>
        ''' The Variance of the sequence of values, or null if the source sequence is empty or contains
        ''' only values that are null.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Variance(ByVal source As IEnumerable(Of Single?)) As Single?
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Dim values As IEnumerable(Of Single) = source.Coalesce()
            Return If(values.Any(), values.Variance(), DirectCast(Nothing, Single?))
        End Function

        ''' <summary>
        ''' Computes the Variance of a sequence of System.Single values. System.ArgumentNullException:
        ''' source is null.
        ''' </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source"> A sequence of nullable System.Decimal values to calculate the Variance
        '''                       of. </param>
        ''' <returns> The Variance of the sequence of values. </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Variance(ByVal source As IEnumerable(Of Single)) As Single
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Return CSng(source.Select(Function(x) CDbl(x)).Variance())
        End Function

        ''' <summary>
        ''' Computes the Variance of a sequence of nullable System.Int32 values.
        ''' System.ArgumentNullException: source is null.
        ''' </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source"> The source values. </param>
        ''' <returns>
        ''' The Variance of the sequence of values, or null if the source sequence is empty or contains
        ''' only values that are null.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Variance(ByVal source As IEnumerable(Of Integer?)) As Double?
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Dim values As IEnumerable(Of Integer) = source.Coalesce()
            Return If(values.Any(), values.Variance(), DirectCast(Nothing, Double?))
        End Function

        ''' <summary>
        ''' Computes the Variance of a sequence of System.Int32 values. System.ArgumentNullException:
        ''' source is null.
        ''' </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source"> A sequence of nullable System.Decimal values to calculate the Variance
        '''                                                                   of. </param>
        ''' <returns> The Variance of the sequence of values. </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Variance(ByVal source As IEnumerable(Of Integer)) As Double
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Return source.Select(Function(x) CDbl(x)).Variance()
        End Function

        ''' <summary> Computes the Variance of a sequence of nullable System.Int64 values. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source"> A sequence of nullable System.Decimal values to calculate the Variance
        '''                                                                   of. </param>
        ''' <returns>
        ''' The Variance of the sequence of values, or null if the source sequence is empty or contains
        ''' only values that are null.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Variance(ByVal source As IEnumerable(Of Long?)) As Double?
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Dim values As IEnumerable(Of Long) = source.Coalesce()
            Return If(values.Any(), values.Variance(), DirectCast(Nothing, Double?))
        End Function

        ''' <summary> Computes the Variance of a sequence of System.Int64 values. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source"> A sequence of nullable System.Decimal values to calculate the Variance
        '''                                                                   of. </param>
        ''' <returns> The Variance of the sequence of values. </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Variance(ByVal source As IEnumerable(Of Long)) As Double
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Return source.Select(Function(x) CDbl(x)).Variance()
        End Function

        ''' <summary>
        ''' Computes the Variance of a sequence of nullable System.Decimal values that are obtained by
        ''' invoking a transform function on each element of the input sequence.
        ''' </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source">   A sequence of nullable System.Decimal values to calculate the Variance
        '''                                                                     of. </param>
        ''' <param name="selector"> A transform function to apply to each element. </param>
        ''' <returns>
        ''' The Variance of the sequence of values, or null if the source sequence is empty or contains
        ''' only values that are null.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Variance(Of TSource)(ByVal source As IEnumerable(Of TSource), ByVal selector As Func(Of TSource, Decimal?)) As Decimal?
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Return source.Select(selector).Variance()
        End Function

        ''' <summary>
        ''' Computes the Variance of a sequence of System.Decimal values that are obtained by invoking a
        ''' transform function on each element of the input sequence.
        ''' </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source">   A sequence of nullable System.Decimal values to calculate the Variance
        '''                                                                     of. </param>
        ''' <param name="selector"> A transform function to apply to each element. </param>
        ''' <returns> The Variance of the sequence of values. </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Variance(Of TSource)(ByVal source As IEnumerable(Of TSource), ByVal selector As Func(Of TSource, Decimal)) As Decimal
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Return source.Select(selector).Variance()
        End Function

        ''' <summary>
        ''' Computes the Variance of a sequence of nullable System.Double values that are obtained by
        ''' invoking a transform function on each element of the input sequence.
        ''' </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source">   A sequence of nullable System.Decimal values to calculate the Variance
        '''                                                                     of. </param>
        ''' <param name="selector"> A transform function to apply to each element. </param>
        ''' <returns>
        ''' The Variance of the sequence of values, or null if the source sequence is empty or contains
        ''' only values that are null.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Variance(Of TSource)(ByVal source As IEnumerable(Of TSource), ByVal selector As Func(Of TSource, Double?)) As Double?
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Return source.Select(selector).Variance()
        End Function

        ''' <summary>
        ''' Computes the Variance of a sequence of System.Double values that are obtained by invoking a
        ''' transform function on each element of the input sequence.
        ''' </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source">   A sequence of nullable System.Decimal values to calculate the Variance
        '''                                                                     of. </param>
        ''' <param name="selector"> A transform function to apply to each element. </param>
        ''' <returns> The Variance of the sequence of values. </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Variance(Of TSource)(ByVal source As IEnumerable(Of TSource), ByVal selector As Func(Of TSource, Double)) As Double
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Return source.Select(selector).Variance()
        End Function

        ''' <summary>
        ''' Computes the Variance of a sequence of nullable System.Single values that are obtained by
        ''' invoking a transform function on each element of the input sequence.
        ''' </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source">   A sequence of nullable System.Decimal values to calculate the Variance
        '''                                                                     of. </param>
        ''' <param name="selector"> A transform function to apply to each element. </param>
        ''' <returns>
        ''' The Variance of the sequence of values, or null if the source sequence is empty or contains
        ''' only values that are null.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Variance(Of TSource)(ByVal source As IEnumerable(Of TSource), ByVal selector As Func(Of TSource, Single?)) As Single?
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Return source.Select(selector).Variance()
        End Function

        ''' <summary>
        ''' Computes the Variance of a sequence of System.Single values that are obtained by invoking a
        ''' transform function on each element of the input sequence. System.ArgumentNullException:
        ''' source or selector is null.
        ''' </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source">   A sequence of nullable System.Decimal values to calculate the Variance
        '''                                                                                           of. </param>
        ''' <param name="selector"> A transform function to apply to each element. </param>
        ''' <returns> The Variance of the sequence of values. </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Variance(Of TSource)(ByVal source As IEnumerable(Of TSource), ByVal selector As Func(Of TSource, Single)) As Single
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Return source.Select(selector).Variance()
        End Function

        ''' <summary>
        ''' Computes the Variance of a sequence of nullable System.Int32 values that are obtained by
        ''' invoking a transform function on each element of the input sequence.
        ''' System.ArgumentNullException: source or selector is null.
        ''' </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source">   A sequence of nullable System.Decimal values to calculate the Variance
        '''                                                                                           of. </param>
        ''' <param name="selector"> A transform function to apply to each element. </param>
        ''' <returns>
        ''' The Variance of the sequence of values, or null if the source sequence is empty or contains
        ''' only values that are null.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Variance(Of TSource)(ByVal source As IEnumerable(Of TSource), ByVal selector As Func(Of TSource, Integer?)) As Double?
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Return source.Select(selector).Variance()
        End Function

        ''' <summary>
        ''' Computes the Variance of a sequence of System.Int32 values that are obtained by invoking a
        ''' transform function on each element of the input sequence. System.ArgumentNullException:
        ''' source or selector is null.
        ''' </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source">   A sequence of nullable System.Decimal values to calculate the Variance
        '''                                                                                           of. </param>
        ''' <param name="selector"> A transform function to apply to each element. </param>
        ''' <returns> The Variance of the sequence of values. </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Variance(Of TSource)(ByVal source As IEnumerable(Of TSource), ByVal selector As Func(Of TSource, Integer)) As Double
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Return source.Select(selector).Variance()
        End Function

        ''' <summary>
        ''' Computes the Variance of a sequence of nullable System.Int64 values that are obtained by
        ''' invoking a transform function on each element of the input sequence.
        ''' System.ArgumentNullException: source or selector is null.
        ''' </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source">   A sequence of nullable System.Decimal values to calculate the Variance
        '''                                                                                           of. </param>
        ''' <param name="selector"> A transform function to apply to each element. </param>
        ''' <returns>
        ''' The Variance of the sequence of values, or null if the source sequence is empty or contains
        ''' only values that are null.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Variance(Of TSource)(ByVal source As IEnumerable(Of TSource), ByVal selector As Func(Of TSource, Long?)) As Double?
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Return source.Select(selector).Variance()
        End Function

        ''' <summary>
        ''' Computes the Variance of a sequence of System.Int64 values that are obtained by invoking a
        ''' transform function on each element of the input sequence. System.ArgumentNullException:
        ''' source or selector is null.
        ''' </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source">   A sequence of nullable System.Decimal values to calculate the Variance
        '''                                                                                           of. </param>
        ''' <param name="selector"> A transform function to apply to each element. </param>
        ''' <returns> The Variance of the sequence of values. </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Variance(Of TSource)(ByVal source As IEnumerable(Of TSource), ByVal selector As Func(Of TSource, Long)) As Double
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Return source.Select(selector).Variance()
        End Function
    End Module
End Namespace
