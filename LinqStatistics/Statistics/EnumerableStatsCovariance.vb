Namespace EnumerableStats
    Partial Public Module Methods

        ''' <summary> Computes the Covariance of a sequence of nullable System.Decimal values. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source"> The source values. </param>
        ''' <param name="other">  The other. </param>
        ''' <returns>
        ''' The Covariance of the sequence of values, or null if the source sequence is empty or contains
        ''' only values that are null.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Covariance(ByVal source As IEnumerable(Of Decimal?), ByVal other As IEnumerable(Of Decimal?)) As Decimal?
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            If other Is Nothing Then Throw New ArgumentNullException(NameOf(other))
            Dim values As IEnumerable(Of Decimal) = source.Coalesce()
            Return If(values.Any(), values.Covariance(other.Coalesce()), DirectCast(Nothing, Decimal?))
        End Function

        ''' <summary> Computes the Covariance of a sequence of nullable System.Decimal values. </summary>
        ''' <remarks> David, 2020-09-05. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source"> The source values. </param>
        ''' <param name="other">  The other. </param>
        ''' <returns>
        ''' The Covariance of the sequence of values, or null if the source sequence is empty or contains
        ''' only values that are null.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Covariance(ByVal source As IList(Of Decimal?), ByVal other As IList(Of Decimal?)) As Decimal?
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            If other Is Nothing Then Throw New ArgumentNullException(NameOf(other))
            Dim values As IList(Of Decimal) = source.Coalesce()
            Return If(values.Any(), values.Covariance(other.Coalesce()), DirectCast(Nothing, Decimal?))
        End Function

        ''' <summary> Computes the Covariance of a sequence of System.Decimal values. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source"> The source values. </param>
        ''' <param name="other">  The other. </param>
        ''' <returns> The Covariance of the sequence of values. </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Covariance(ByVal source As IEnumerable(Of Decimal), ByVal other As IEnumerable(Of Decimal)) As Decimal
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            If other Is Nothing Then Throw New ArgumentNullException(NameOf(other))
            Return CDec(source.Select(Function(x) CDbl(x)).Covariance(other.Select(Function(x) CDbl(x))))
        End Function

        ''' <summary> Computes the Covariance of a sequence of System.Decimal values. </summary>
        ''' <remarks> David, 2020-09-05. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source"> The source values. </param>
        ''' <param name="other">  The other. </param>
        ''' <returns> The Covariance of the sequence of values. </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Covariance(ByVal source As IList(Of Decimal), ByVal other As IList(Of Decimal)) As Decimal
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            If other Is Nothing Then Throw New ArgumentNullException(NameOf(other))
            Return CDec(source.Select(Function(x) CDbl(x)).Covariance(other.Select(Function(x) CDbl(x))))
        End Function


        ''' <summary> Computes the Covariance of a sequence of nullable System.Double values. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source"> The source values. </param>
        ''' <param name="other">  The other. </param>
        ''' <returns>
        ''' The Covariance of the sequence of values, or null if the source sequence is empty or contains
        ''' only values that are null.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Covariance(ByVal source As IEnumerable(Of Double?), ByVal other As IEnumerable(Of Double?)) As Double?
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            If other Is Nothing Then Throw New ArgumentNullException(NameOf(other))
            Dim values As IEnumerable(Of Double) = source.Coalesce()
            Return If(values.Any(), values.Covariance(other.Coalesce()), DirectCast(Nothing, Double?))
        End Function

        ''' <summary> Computes the Covariance of a sequence of nullable System.Double values. </summary>
        ''' <remarks> David, 2020-09-05. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source"> The source values. </param>
        ''' <param name="other">  The other. </param>
        ''' <returns>
        ''' The Covariance of the sequence of values, or null if the source sequence is empty or contains
        ''' only values that are null.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Covariance(ByVal source As IList(Of Double?), ByVal other As IList(Of Double?)) As Double?
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            If other Is Nothing Then Throw New ArgumentNullException(NameOf(other))
            Dim values As IList(Of Double) = source.Coalesce()
            Return If(values.Any(), values.Covariance(other.Coalesce()), DirectCast(Nothing, Double?))
        End Function

        ''' <summary> Computes the Covariance of a sequence of System.Double values. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
        '''                                          illegal values. </exception>
        ''' <param name="source"> The source values. </param>
        ''' <param name="other">  The other. </param>
        ''' <returns> The Covariance of the sequence of values. </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Covariance(ByVal source As IEnumerable(Of Double), ByVal other As IEnumerable(Of Double)) As Double
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            If other Is Nothing Then Throw New ArgumentNullException(NameOf(other))
            Dim len As Integer = source.Count()

            If len <> other.Count() Then
                Throw New ArgumentException("Collections are not of the same length", NameOf(other))
            End If

            Dim avgSource As Double = source.Average()
            Dim avgOther As Double = other.Average()
            'INSTANT VB NOTE: The local variable covariance was renamed since Visual Basic will not allow local variables with the same name as their enclosing function or property:
            Dim covariance_Renamed As Double = 0

            For i As Integer = 0 To len - 1
                covariance_Renamed += (source.ElementAt(i) - avgSource) * (other.ElementAt(i) - avgOther)
            Next i

            Return covariance_Renamed / len
        End Function

        ''' <summary> Computes the Covariance of a sequence of nullable System.Single values. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source"> The source values. </param>
        ''' <param name="other">  The other. </param>
        ''' <returns>
        ''' The Covariance of the sequence of values, or null if the source sequence is empty or contains
        ''' only values that are null.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Covariance(ByVal source As IEnumerable(Of Single?), ByVal other As IEnumerable(Of Single?)) As Single?
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            If other Is Nothing Then Throw New ArgumentNullException(NameOf(other))
            Dim values As IEnumerable(Of Single) = source.Coalesce()
            Return If(values.Any(), values.Covariance(other.Coalesce()), DirectCast(Nothing, Single?))
        End Function

        ''' <summary>
        '''     Computes the Covariance of a sequence of System.Single values.
        ''' </summary> 
        ''' <param name="source">   The source values. </param>
        ''' <returns>
        '''     The Covariance of the sequence of values.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Covariance(ByVal source As IEnumerable(Of Single), ByVal other As IEnumerable(Of Single)) As Single
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            If other Is Nothing Then Throw New ArgumentNullException(NameOf(other))
            Return CSng(source.Select(Function(x) CDbl(x)).Covariance(other.Select(Function(x) CDbl(x))))
        End Function

        ''' <summary> Computes the Covariance of a sequence of nullable System.Int32 values. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source"> The source values. </param>
        ''' <param name="other">  The other. </param>
        ''' <returns>
        ''' The Covariance of the sequence of values, or null if the source sequence is empty or contains
        ''' only values that are null.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Covariance(ByVal source As IEnumerable(Of Integer?), ByVal other As IEnumerable(Of Integer?)) As Double?
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            If other Is Nothing Then Throw New ArgumentNullException(NameOf(other))
            Dim values As IEnumerable(Of Integer) = source.Coalesce()
            Return If(values.Any(), values.Covariance(other.Coalesce()), DirectCast(Nothing, Double?))
        End Function

        ''' <summary> Computes the Covariance of a sequence of System.Int32 values. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source"> The source values. </param>
        ''' <param name="other">  The other. </param>
        ''' <returns> The Covariance of the sequence of values. </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Covariance(ByVal source As IEnumerable(Of Integer), ByVal other As IEnumerable(Of Integer)) As Double
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            If other Is Nothing Then Throw New ArgumentNullException(NameOf(other))
            Return source.Select(Function(x) CDbl(x)).Covariance(other.Select(Function(x) CDbl(x)))
        End Function

        ''' <summary> Computes the Covariance of a sequence of nullable System.Int64 values. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source"> The source values. </param>
        ''' <param name="other">  The other. </param>
        ''' <returns>
        ''' The Covariance of the sequence of values, or null if the source sequence is empty or contains
        ''' only values that are null.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Covariance(ByVal source As IEnumerable(Of Long?), ByVal other As IEnumerable(Of Long?)) As Double?
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            If other Is Nothing Then Throw New ArgumentNullException(NameOf(other))
            Dim values As IEnumerable(Of Long) = source.Coalesce()
            Return If(values.Any(), values.Covariance(other.Coalesce()), DirectCast(Nothing, Double?))
        End Function

        ''' <summary> Computes the Covariance of a sequence of System.Int64 values. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source"> The source values. </param>
        ''' <param name="other">  The other. </param>
        ''' <returns> The Covariance of the sequence of values. </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Covariance(ByVal source As IEnumerable(Of Long), ByVal other As IEnumerable(Of Long)) As Double
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            If other Is Nothing Then Throw New ArgumentNullException(NameOf(other))
            Return source.Select(Function(x) CDbl(x)).Covariance(other.Select(Function(x) CDbl(x)))
        End Function

        ''' <summary>
        ''' Computes the Covariance of a sequence of nullable System.Decimal values that are obtained by
        ''' invoking a transform function on each element of the input sequence.
        ''' </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source">   The source values. </param>
        ''' <param name="other">    The other values. </param>
        ''' <param name="selector"> A transform function to apply to each element. </param>
        ''' <returns>
        ''' The Covariance of the sequence of values, or null if the source sequence is empty or contains
        ''' only values that are null.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Covariance(Of TSource)(ByVal source As IEnumerable(Of TSource), ByVal other As IEnumerable(Of TSource), ByVal selector As Func(Of TSource, Decimal?)) As Decimal?
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            If other Is Nothing Then Throw New ArgumentNullException(NameOf(other))
            Return source.Select(selector).Covariance(other.Select(selector))
        End Function

        ''' <summary>
        ''' Computes the Covariance of a sequence of System.Decimal values that are obtained by invoking
        ''' a transform function on each element of the input sequence.
        ''' </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source">   The source values. </param>
        ''' <param name="other">    The other values. </param>
        ''' <param name="selector"> A transform function to apply to each element. </param>
        ''' <returns> The Covariance of the sequence of values. </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Covariance(Of TSource)(ByVal source As IEnumerable(Of TSource), ByVal other As IEnumerable(Of TSource), ByVal selector As Func(Of TSource, Decimal)) As Decimal
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            If other Is Nothing Then Throw New ArgumentNullException(NameOf(other))
            Return source.Select(selector).Covariance(other.Select(selector))
        End Function

        ''' <summary>
        ''' Computes the Covariance of a sequence of nullable System.Double values that are obtained by
        ''' invoking a transform function on each element of the input sequence.
        ''' </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source">   The source values. </param>
        ''' <param name="other">    The other values. </param>
        ''' <param name="selector"> A transform function to apply to each element. </param>
        ''' <returns>
        ''' The Covariance of the sequence of values, or null if the source sequence is empty or contains
        ''' only values that are null.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Covariance(Of TSource)(ByVal source As IEnumerable(Of TSource), ByVal other As IEnumerable(Of TSource), ByVal selector As Func(Of TSource, Double?)) As Double?
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            If other Is Nothing Then Throw New ArgumentNullException(NameOf(other))
            Return source.Select(selector).Covariance(other.Select(selector))
        End Function

        ''' <summary>
        ''' Computes the Covariance of a sequence of System.Double values that are obtained by invoking a
        ''' transform function on each element of the input sequence.
        ''' </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source">   The source values. </param>
        ''' <param name="other">    The other values. </param>
        ''' <param name="selector"> A transform function to apply to each element. </param>
        ''' <returns> The Covariance of the sequence of values. </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Covariance(Of TSource)(ByVal source As IEnumerable(Of TSource), ByVal other As IEnumerable(Of TSource), ByVal selector As Func(Of TSource, Double)) As Double
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            If other Is Nothing Then Throw New ArgumentNullException(NameOf(other))
            Return source.Select(selector).Covariance(other.Select(selector))
        End Function

        ''' <summary>
        ''' Computes the Covariance of a sequence of nullable System.Single values that are obtained by
        ''' invoking a transform function on each element of the input sequence.
        ''' </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source">   The source values. </param>
        ''' <param name="other">    The other values. </param>
        ''' <param name="selector"> A transform function to apply to each element. </param>
        ''' <returns>
        ''' The Covariance of the sequence of values, or null if the source sequence is empty or contains
        ''' only values that are null.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Covariance(Of TSource)(ByVal source As IEnumerable(Of TSource), ByVal other As IEnumerable(Of TSource), ByVal selector As Func(Of TSource, Single?)) As Single?
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            If other Is Nothing Then Throw New ArgumentNullException(NameOf(other))
            Return source.Select(selector).Covariance(other.Select(selector))
        End Function

        ''' <summary>
        ''' Computes the Covariance of a sequence of System.Single values that are obtained by invoking a
        ''' transform function on each element of the input sequence.
        ''' </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source">   The source values. </param>
        ''' <param name="other">    The other values. </param>
        ''' <param name="selector"> A transform function to apply to each element. </param>
        ''' <returns> The Covariance of the sequence of values. </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Covariance(Of TSource)(ByVal source As IEnumerable(Of TSource), ByVal other As IEnumerable(Of TSource), ByVal selector As Func(Of TSource, Single)) As Single
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            If other Is Nothing Then Throw New ArgumentNullException(NameOf(other))
            Return source.Select(selector).Covariance(other.Select(selector))
        End Function

        ''' <summary>
        ''' Computes the Covariance of a sequence of nullable System.Int32 values that are obtained by
        ''' invoking a transform function on each element of the input sequence.
        ''' </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source">   The source values. </param>
        ''' <param name="other">    The other values. </param>
        ''' <param name="selector"> A transform function to apply to each element. </param>
        ''' <returns>
        ''' The Covariance of the sequence of values, or null if the source sequence is empty or contains
        ''' only values that are null.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Covariance(Of TSource)(ByVal source As IEnumerable(Of TSource), ByVal other As IEnumerable(Of TSource), ByVal selector As Func(Of TSource, Integer?)) As Double?
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            If other Is Nothing Then Throw New ArgumentNullException(NameOf(other))
            Return source.Select(selector).Covariance(other.Select(selector))
        End Function

        ''' <summary>
        ''' Computes the Covariance of a sequence of System.Int32 values that are obtained by invoking a
        ''' transform function on each element of the input sequence.
        ''' </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source">   The source values. </param>
        ''' <param name="other">    The other values. </param>
        ''' <param name="selector"> A transform function to apply to each element. </param>
        ''' <returns> The Covariance of the sequence of values. </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Covariance(Of TSource)(ByVal source As IEnumerable(Of TSource), ByVal other As IEnumerable(Of TSource), ByVal selector As Func(Of TSource, Integer)) As Double
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            If other Is Nothing Then Throw New ArgumentNullException(NameOf(other))
            Return source.Select(selector).Covariance(other.Select(selector))
        End Function

        ''' <summary>
        ''' Computes the Covariance of a sequence of nullable System.Int64 values that are obtained by
        ''' invoking a transform function on each element of the input sequence.
        ''' </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source">   The source values. </param>
        ''' <param name="other">    The other values. </param>
        ''' <param name="selector"> A transform function to apply to each element. </param>
        ''' <returns>
        ''' The Covariance of the sequence of values, or null if the source sequence is empty or contains
        ''' only values that are null.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Covariance(Of TSource)(ByVal source As IEnumerable(Of TSource), ByVal other As IEnumerable(Of TSource), ByVal selector As Func(Of TSource, Long?)) As Double?
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            If other Is Nothing Then Throw New ArgumentNullException(NameOf(other))
            Return source.Select(selector).Covariance(other.Select(selector))
        End Function

        ''' <summary>
        ''' Computes the Covariance of a sequence of System.Int64 values that are obtained by invoking a
        ''' transform function on each element of the input sequence.
        ''' </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source">   The source values. </param>
        ''' <param name="other">    The other values. </param>
        ''' <param name="selector"> A transform function to apply to each element. </param>
        ''' <returns> The Covariance of the sequence of values. </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Covariance(Of TSource)(ByVal source As IEnumerable(Of TSource), ByVal other As IEnumerable(Of TSource), ByVal selector As Func(Of TSource, Long)) As Double
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            If other Is Nothing Then Throw New ArgumentNullException(NameOf(other))
            Return source.Select(selector).Covariance(other.Select(selector))
        End Function
    End Module
End Namespace
