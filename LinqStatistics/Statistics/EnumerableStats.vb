Namespace EnumerableStats

    ''' <summary> An enumerable statistics. </summary>
    ''' <remarks> (c) 2012 Don Kackman. All rights reserved.<para>
	''' Licensed under The MIT License.</para><para>
    ''' David, 2016-07-30, 3.0.6055">
    ''' http://www.codeproject.com/Articles/42492/Using-LINQ-to-Calculate-Basic-Statistics. </para></remarks>
    Partial Public Module Methods

        ''' <summary> Enumerates coalesce in this collection. </summary>
        ''' <param name="source"> Source for the. </param>
        ''' <returns>
        ''' An enumerator that allows for each to be used to process coalesce in this collection.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Coalesce(Of T As Structure)(ByVal source As IEnumerable(Of T?)) As IEnumerable(Of T)
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Debug.Assert(source IsNot Nothing)
            Return source.Where(Function(x) x.HasValue).Select(Function(x) CType(x, T))
        End Function

        ''' <summary> Enumerates coalesce in this collection. </summary>
        ''' <param name="source"> Source for the. </param>
        ''' <returns>
        ''' An enumerator that allows for each to be used to process coalesce in this collection.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Coalesce(Of T As Structure)(ByVal source As IList(Of T?)) As IList(Of T)
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Debug.Assert(source IsNot Nothing)
            Return source.Where(Function(x) x.HasValue).Select(Function(x) CType(x, T)).ToList
        End Function
    End Module

End Namespace
