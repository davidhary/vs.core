﻿Namespace EnumerableStats

    Partial Public Module Methods

        ''' <summary> Enumerates modes in this collection. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source"> Source for the. </param>
        ''' <returns>
        ''' An enumerator that allows for each to be used to process modes in this collection.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Modes(Of T As Structure)(ByVal source As IEnumerable(Of T?)) As IEnumerable(Of T)
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Dim values As IEnumerable(Of T) = source.Coalesce()
            Return If(values.Any(), values.Modes(), Enumerable.Empty(Of T)())
        End Function

        ''' <summary> Enumerates modes in this collection. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source"> Source for the. </param>
        ''' <returns>
        ''' An enumerator that allows for each to be used to process modes in this collection.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Modes(Of T As Structure)(ByVal source As IEnumerable(Of T)) As IEnumerable(Of T)
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))

            Dim result As New List(Of T)()
            Dim current As IEnumerable(Of T) = source
            Dim value As T? = current.Mode()
            Do While value.HasValue AndAlso current.Count() > 1
                result.Add(CType(value, T))
                current = current.Where(Function(x) x.Equals(value) = False).ToArray()
                value = current.Mode()
            Loop
            Return result
        End Function

        ''' <summary> Modes the given source. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source"> Source for the. </param>
        ''' <returns> A T? </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Mode(Of T As Structure)(ByVal source As IEnumerable(Of T?)) As T?
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Dim values As IEnumerable(Of T) = source.Coalesce()
            Return If(values.Any(), values.Mode(), Nothing)
        End Function

        ''' <summary> Modes the given source. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source"> Source for the. </param>
        ''' <returns> A T? </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Mode(Of T As Structure)(ByVal source As IEnumerable(Of T)) As T?
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Dim sortedList As IEnumerable(Of T) = From number In source
                                                  Order By number
                                                  Select number
            Dim count As Integer = 0
            Dim max As Integer = 0
            Dim current As T = Nothing
            Dim result As New T?()

            For Each [next] As T In sortedList
                If current.Equals([next]) = False Then
                    current = [next]
                    count = 1
                Else
                    count += 1
                End If

                If count > max Then
                    max = count
                    result = current
                End If
            Next [next]

            Return If(max > 1, result, Nothing)
        End Function

        ''' <summary> Modes the given source. </summary>
        ''' <param name="source">   Source for the. </param>
        ''' <param name="selector"> The selector. </param>
        ''' <returns> A T? </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Mode(Of TSource, TMode As Structure)(source As IEnumerable(Of TSource),
                                                             selector As Func(Of TSource, TMode)) As System.Nullable(Of TMode)
            Return source.[Select](selector).Mode()
        End Function

        ''' <summary> Modes the given source. </summary>
        ''' <param name="source">   Source for the. </param>
        ''' <param name="selector"> The selector. </param>
        ''' <returns> A T? </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Mode(Of TSource, TMode As Structure)(source As IEnumerable(Of TSource),
                                                             selector As Func(Of TSource, System.Nullable(Of TMode))) As System.Nullable(Of TMode)
            Return source.[Select](selector).Mode()
        End Function

    End Module
End Namespace
