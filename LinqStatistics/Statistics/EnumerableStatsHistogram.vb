﻿Imports System.Windows
Namespace EnumerableStats
    Partial Public Module Methods

        ''' <summary> Enumerates histogram in this collection. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source">     Source for the. </param>
        ''' <param name="lowerBound"> The lower bound. </param>
        ''' <param name="upperBound"> The upper bound. </param>
        ''' <param name="binCount">   Number of bins. </param>
        ''' <param name="exclusive">  true to exclusive. </param>
        ''' <returns>
        ''' An enumerator that allows for each to be used to process histogram in this collection.
        ''' </returns>
            <System.Runtime.CompilerServices.Extension>
        Public Function Histogram(ByVal source As IEnumerable(Of Double),
                                   ByVal lowerBound As Double, ByVal upperBound As Double,
                                   ByVal binCount As Integer, ByVal exclusive As Boolean) As IEnumerable(Of KeyValuePair(Of Double, Integer))
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Dim binWidth As Double = (upperBound - lowerBound) / binCount
            Dim inverseWidth As Double = 1 / binWidth
            ' number of groups between values
            Dim histF As IEnumerable(Of KeyValuePair(Of Double, Integer))
            If exclusive Then
                ' includes only values within the region
                histF = From x In source
                        Let binValue = (inverseWidth * (x - lowerBound))
                        Where binValue >= 0 AndAlso binValue < binCount
                        Let binNumber = CInt(Fix(binValue))
                        Group x By binNumber Into gr = Group
                        Order By binNumber
                        Select New KeyValuePair(Of Double, Integer)(lowerBound + binWidth * binNumber, gr.Count())
            Else
                ' includes also values outside the region
                histF = From x In source
                        Let binValue = (inverseWidth * (x - lowerBound))
                        Let binNumber = If(binValue < 0, 0, If(binValue >= binCount, binCount + 1, 1 + CInt(Fix(binValue))))
                        Group x By binNumber Into gr = Group
                        Order By binNumber
                        Select New KeyValuePair(Of Double, Integer)(lowerBound + binWidth * binNumber, gr.Count())
            End If
            Return histF
        End Function

        ''' <summary> Enumerates histogram in this collection. </summary>
        ''' <remarks>
        ''' The first bin at X=<paramref name="lowerBound"/> counts all the values below the lower limit;
        ''' <para>
        ''' The second bin at X=<paramref name="lowerBound"/> + half the bin width counts the values higher and equal to the lower limit but lower than the bin
        ''' width;</para><para>
        ''' The next to last bin at X=<paramref name="upperBound"/> - half the bin width counts the values at the last bin;</para><para>
        ''' The last bin at <paramref name="upperBound"/> counts the number of values equal or higher than the high limit. </para>
        ''' </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source">     Source for the. </param>
        ''' <param name="lowerBound"> The lower bound. </param>
        ''' <param name="upperBound"> The upper bound. </param>
        ''' <param name="binCount">   Number of bins. </param>
        ''' <returns>
        ''' An enumerator that allows for each to be used to process histogram in this collection.
        ''' </returns>
            <System.Runtime.CompilerServices.Extension>
        Public Function Histogram(ByVal source As IEnumerable(Of Double),
                                   ByVal lowerBound As Double, ByVal upperBound As Double,
                                   ByVal binCount As Integer) As IEnumerable(Of Point)
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Dim binWidth As Double = (upperBound - lowerBound) / binCount
            Dim inverseWidth As Double = 1 / binWidth
            ' number of groups between values
            Dim histF As IEnumerable(Of Point)
            ' includes also values outside the region
            histF = From x In source
                    Let binValue = (inverseWidth * (x - lowerBound))
                    Let binNumber = If(binValue < 0, 0, If(binValue >= binCount, binCount + 1, 1 + CInt(Fix(binValue))))
                    Group x By binNumber Into gr = Group
                    Order By binNumber
                    Select New Point(If(binNumber = 0, lowerBound, If(binNumber = binCount + 1, upperBound, lowerBound + binWidth * (binNumber - 0.5))),
                                        gr.Count())
            Return histF
        End Function

        ''' <summary> Computes the histogram. </summary>
        ''' <remarks>
        ''' The first bin at X=<paramref name="lowerBound"/> counts all the values below the lower limit;
        ''' <para>
        ''' The second bin at X=<paramref name="lowerBound"/> + half the bin width counts the values higher and equal to the lower limit but lower than the bin
        ''' width;</para><para>
        ''' The next to last bin at X=<paramref name="upperBound"/> - half the bin width counts the values at the last bin;</para><para>
        ''' The last bin at <paramref name="upperBound"/> counts the number of values equal or higher than the high limit. </para>
        ''' </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source">     Source for the. </param>
        ''' <param name="lowerBound"> The lower bound. </param>
        ''' <param name="upperBound"> The upper bound. </param>
        ''' <param name="binCount">   Number of bins. </param>
        ''' <returns>
        ''' An enumerator that allows for each to be used to process histogram direct in this collection.
        ''' </returns>
            <System.Runtime.CompilerServices.Extension>
        Public Function HistogramDirect(ByVal source As IEnumerable(Of Double),
                                        ByVal lowerBound As Double, ByVal upperBound As Double,
                                        ByVal binCount As Integer) As IEnumerable(Of Point)
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Dim binWidth As Double = (upperBound - lowerBound) / binCount
            Dim inverseWidth As Double = 1 / binWidth
            Dim l As New List(Of Integer)
            For i As Integer = 0 To binCount + 1
                l.Add(0)
            Next
            For Each x As Double In source
                Dim binValue As Double = inverseWidth * (x - lowerBound)
                Dim binNumber As Integer = If(binValue < 0, 0, If(binValue >= binCount, binCount + 1, 1 + CInt(Fix(binValue))))
                l.Item(binNumber) += 1
            Next
            ' number of groups between values
            Dim histF As New List(Of Point)
            For i As Integer = 0 To l.Count - 1
                Dim x As Double
                If i = 0 Then
                    x = lowerBound
                ElseIf i = l.Count - 1 Then
                    x = upperBound
                Else
                    ' x = lowerBound + 0.5 * binWidth + binWidth * (i - 1)
                    x = lowerBound + binWidth * (i - 0.5)
                End If
                histF.Add(New Point(x, l(i)))
            Next
            Return histF.ToArray
        End Function

        ''' <summary> Enumerates a series of points with monotone X mid values to step line in this collection. </summary>
        ''' <param name="points"> The points. </param>
        ''' <returns> points as an IEnumerable(Of Point) </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function ToStepLine(ByVal points As IEnumerable(Of Point)) As IEnumerable(Of Point)
            Dim values As New List(Of Point)
            If points?.Any Then
                Dim p As Point = points(0)
                Dim x As Double = p.X
                values.Add(New Point(x, 0))
                values.Add(New Point(x, p.Y))

                If points.Count > 1 Then
                    Dim nextP As Point = points(1)

                    x = 0.5 * (p.X + nextP.X)
                    values.Add(New Point(x, p.Y))
                    values.Add(New Point(x, nextP.Y))
                    If points.Count = 2 Then
                        values.Add(New Point(nextP.X, nextP.Y))
                        values.Add(New Point(nextP.X, 0))
                    Else
                        p = points(1)
                        nextP = points(2)
                        x = 0.5 * (p.X + nextP.X)
                        values.Add(New Point(x, p.Y))
                        values.Add(New Point(x, nextP.Y))
                        If points.Count = 3 Then
                            values.Add(New Point(nextP.X, nextP.Y))
                            values.Add(New Point(nextP.X, 0))
                        End If
                        For i As Integer = 1 To points.Count - 2
                            p = points(i)
                            nextP = points(i + 1)
                            x = 0.5 * (p.X + nextP.X)
                            values.Add(New Point(x, p.Y))
                            values.Add(New Point(x, nextP.Y))
                        Next
                        values.Add(New Point(nextP.X, nextP.Y))
                        values.Add(New Point(nextP.X, 0))
                    End If
                End If
            End If
            Return values.ToArray
        End Function

    End Module


End Namespace
