using System;
using System.Drawing;
using System.Drawing.Printing;
using System.Runtime.InteropServices;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.WinForms.RichTextBoxExtensions
{
    public static partial class RichTextBoxExtensionMethods
    {

        /// <summary> An unsafe native methods. </summary>
        private sealed class UnsafeNativeMethods
        {
            private UnsafeNativeMethods() : base()
            {
            }

            // constants

            public const int WM_USER = 0x400;
            public const int EM_FORMATRANGE = WM_USER + 57;
            public const int EM_GETSCROLLPOS = WM_USER + 221;
            public const int EM_SETSCROLLPOS = WM_USER + 222;
            public const int EM_GETCHARFORMAT = WM_USER + 58;
            public const int EM_SETCHARFORMAT = WM_USER + 68;
            public const int WM_SETREDRAW = 0xB;

            // structures

            [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "<Pending>" )]
            [StructLayout( LayoutKind.Sequential )]
            public struct Rect
            {
                public int left;
                public int top;
                public int right;
                public int bottom;
            }

            [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "<Pending>" )]
            [StructLayout( LayoutKind.Sequential )]
            public struct CharRange
            {
                public int cpMin;
                public int cpMax;
            }

            [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "<Pending>" )]
            [StructLayout( LayoutKind.Sequential )]
            public struct FormatRangeStructure
            {
                public IntPtr hdc;
                public IntPtr hdcTarget;
                public Rect rc;
                public Rect rcPage;
                public CharRange chrg;
            }

            [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "<Pending>" )]
            [StructLayout( LayoutKind.Sequential )]
            public struct CHARFORMAT2_STRUCT
            {
                public uint cbSize;
                public uint dwMask;
                public uint dwEffects;
                public int yHeight;
                public int yOffset;
                public int crTextColor;
                public byte bCharSet;
                public byte bPitchAndFamily;
                [MarshalAs( UnmanagedType.ByValArray, SizeConst = 32 )]
                public char[] szFaceName;
                public ushort wWeight;
                public ushort sSpacing;
                public int crBackColor; // Color.ToArgb() -> int
                public int lcid;
                public int dwReserved;
                public short sStyle;
                public short wKerning;
                public byte bUnderlineType;
                public byte bAnimation;
                public byte bRevAuthor;
                public byte bReserved1;
            }

            // DLL calls for printing "

            /// <summary> Sends a message. </summary>
            /// <param name="hWnd">   The window. </param>
            /// <param name="msg">    The message. </param>
            /// <param name="wParam"> The parameter. </param>
            /// <param name="lParam"> The parameter. </param>
            /// <returns> An Int32. </returns>
            [DllImport( "user32.dll", CharSet = CharSet.Auto )]
            internal static extern IntPtr SendMessage( IntPtr hWnd, uint msg, UIntPtr wParam, IntPtr lParam );

            #region " for scrolling and auto-redraw setting "

            [DllImport( "user32.dll", CharSet = CharSet.Auto )]
            public static extern IntPtr SendMessage( IntPtr hWnd, uint msg, UIntPtr wParam, ref Point lParam );
            [DllImport( "user32.dll", EntryPoint = "GetScrollInfo" )]
            public static extern bool GetScrollInfo( IntPtr hwnd, int nBar, ref ScrollInfo lpsi );

            // enums for GetScrollInfo call (used by GetScrollBarInfo)
            [Flags()]
            public enum ScrollBarMask
            {
                /// <summary>   A binary constant representing the range flag. </summary>
                Range = 1 << 0,
                /// <summary>   A binary constant representing the page size flag. </summary>
                PageSize = 1 << 1,
                /// <summary>   A binary constant representing the position flag. </summary>
                Position = 1 << 2,
                /// <summary>   A binary constant representing the track position flag. </summary>
                TrackPosition = 1 << 46,
                /// <summary>   A binary constant representing the everything flag. </summary>
                Everything = Range | PageSize | Position | TrackPosition
            }

            /// <summary>   Values that represent scroll bar types. </summary>
            /// <remarks>   David, 2020-10-14. </remarks>
            public enum ScrollBarType
            {
                /// <summary>   An enum constant representing the horizontal option. </summary>
                Horizontal = 0,
                /// <summary>   An enum constant representing the vertical option. </summary>
                Vertical = 1,
                /// <summary>   ScrollInfo's nBar parameter when hwnd is a handle to a ScrollBar. </summary>
                Control = 2
            }

            /// <summary> structure for GetScrollInfo call (used by GetScrollBarInfo). </summary>
            /// <remarks> David, 2020-09-24. </remarks>
            [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "<Pending>" )]
            [StructLayout( LayoutKind.Sequential )]
            public struct ScrollInfo
            {
                // INPUT: What information to get
                public int SizeOfStructure; // infrastructure
                public int ScrollBarMask; // tells which fields below to retrieve information for
                // OUTPUT: Scrollbar information
                public int MinimumScrollPosition;
                public int MaximumScrollPosition;
                public int PageSize;
                public int Position;
                public int TrackPosition;
            }

            #endregion
        }


        /// <summary> Safe native methods. </summary>
        private sealed class SafeNativeMethods
        {
            private SafeNativeMethods() : base()
            {
            }

            /// <summary> Format range. </summary>
            /// <param name="handle">       The handle. </param>
            /// <param name="graphics">     The graphics. </param>
            /// <param name="pageSettings"> The page settings. </param>
            /// <param name="charFrom">     The character from. </param>
            /// <param name="charTo">       The character to. </param>
            /// <param name="renderText">   True to render text. </param>
            /// <returns> The formatted range. </returns>
            public static int FormatRange( IntPtr handle, Graphics graphics, PageSettings pageSettings, int charFrom, int charTo, bool renderText )
            {
                // define character range   
                UnsafeNativeMethods.CharRange cr;
                cr.cpMin = charFrom;
                cr.cpMax = charTo;
                // define margins
                UnsafeNativeMethods.Rect rc;
                rc.top = RichTextBoxExtensionMethods.HundredthInchToTwips( pageSettings.Bounds.Top + pageSettings.Margins.Top );
                rc.bottom = RichTextBoxExtensionMethods.HundredthInchToTwips( pageSettings.Bounds.Bottom - pageSettings.Margins.Bottom );
                rc.left = RichTextBoxExtensionMethods.HundredthInchToTwips( pageSettings.Bounds.Left + pageSettings.Margins.Left );
                rc.right = RichTextBoxExtensionMethods.HundredthInchToTwips( pageSettings.Bounds.Right - pageSettings.Margins.Right );
                // define page size
                UnsafeNativeMethods.Rect rcPage;
                rcPage.top = RichTextBoxExtensionMethods.HundredthInchToTwips( pageSettings.Bounds.Top );
                rcPage.bottom = RichTextBoxExtensionMethods.HundredthInchToTwips( pageSettings.Bounds.Bottom );
                rcPage.left = RichTextBoxExtensionMethods.HundredthInchToTwips( pageSettings.Bounds.Left );
                rcPage.right = RichTextBoxExtensionMethods.HundredthInchToTwips( pageSettings.Bounds.Right );
                // handle device context
                var hdc = graphics.GetHdc();
                // handle full format info
                UnsafeNativeMethods.FormatRangeStructure fr;
                fr.chrg = cr;
                fr.hdc = hdc;
                fr.hdcTarget = hdc;
                fr.rc = rc;
                fr.rcPage = rcPage;
                // prepare to render/measure page   
                int wParam;
                if ( renderText )
                {
                    wParam = 1; // render text
                }
                else
                {
                    wParam = 0;
                } // measure only

                IntPtr lParam;
                lParam = Marshal.AllocCoTaskMem( Marshal.SizeOf( fr ) );
                Marshal.StructureToPtr( fr, lParam, false );
                // render/measure page and return first char of next page
                int res = ( int ) UnsafeNativeMethods.SendMessage( handle, UnsafeNativeMethods.EM_FORMATRANGE, ( UIntPtr ) wParam, lParam );
                Marshal.FreeCoTaskMem( lParam );
                graphics.ReleaseHdc( hdc );
                return res;
            }

            /// <summary> Format range done. </summary>
            /// <param name="handle"> The handle. </param>
            public static void FormatRangeDone( IntPtr handle )
            {
                // flag printing done
                var lParam = new IntPtr( 0 );
                _ = UnsafeNativeMethods.SendMessage( handle, UnsafeNativeMethods.EM_FORMATRANGE, ( UIntPtr ) 0, lParam );
            }

            #region " SCROLLING/TEXT-WIDTH METHODS "

            /// <summary>
            /// Get scroll position
            /// </summary>
            /// <param name="handle">      A handler to control containing the scroll bars. </param>
            /// <returns>Point structure containing current horizontal (.x)
            /// and vertical (.y) scroll positions in pixels</returns>
            /// <remarks></remarks>
            public static Point GetScrollPosition( IntPtr handle )
            {
                Point rtbScrollPoint = default;
                _ = UnsafeNativeMethods.SendMessage( handle, UnsafeNativeMethods.EM_GETSCROLLPOS, ( UIntPtr ) 0, ref rtbScrollPoint );
                return rtbScrollPoint;
            }

            /// <summary>
            /// Set scroll position of RichTextBox
            /// </summary>
            /// <param name="handle">      A handler to control containing the scroll bars. </param>
            /// <param name="scrollPoint"> Point structure containing new horizontal (.x)
            /// and vertical (.y) scroll positions in pixels</param>
            public static void SetScrollPosition( IntPtr handle, Point scrollPoint )
            {
                _ = UnsafeNativeMethods.SendMessage( handle, UnsafeNativeMethods.EM_SETSCROLLPOS, ( UIntPtr ) 0, ref scrollPoint );
            }

            /// <summary>
            /// Get information about a RichTextBox scroll bar
            /// </summary>
            /// <param name="handle">      A handler to control containing the scroll bars. </param>
            /// <param name="scrollBarType">ScrollBarType value (.Horizontal or .Vertical)</param>
            /// <param name="scrollBarMask">ScrollBarMask flags indicating what to get
            /// (range, page size, position, track position; defaults to everything)</param>
            /// <returns>ScrollInfo structure with requested info</returns>
            /// <remarks></remarks>
            public static UnsafeNativeMethods.ScrollInfo GetScrollBarInfo( IntPtr handle, UnsafeNativeMethods.ScrollBarType scrollBarType, UnsafeNativeMethods.ScrollBarMask scrollBarMask = UnsafeNativeMethods.ScrollBarMask.Everything )
            {
                var si = new UnsafeNativeMethods.ScrollInfo();
                si.SizeOfStructure = Marshal.SizeOf( si ); // must alway be set to the size of a ScrollInfo structure
                si.ScrollBarMask = ( int ) scrollBarMask; // tells the GetScrollInfo what to get
                _ = UnsafeNativeMethods.GetScrollInfo( handle, ( int ) scrollBarType, ref si ); // horizontal or vertical?
                return si;
            }

            #endregion

            #region " DRAWING METHODS "

            /// <summary>
            /// Turns on or off redrawing of a control with the specified handle.
            /// This can be used to make multiple changes to the control while preventing
            /// the intermediate rendering that would cause flicker
            /// </summary>
            /// <param name="handle">      A handler to the control. </param>
            /// <param name="isOn">True to activate auto-redraw, False to deactivate it</param>
            /// <remarks>Specifying True forces the cumulative results
            /// of previous operations to be rendered</remarks>
            public static void SetRedrawMode( IntPtr handle, bool isOn )
            {
                _ = UnsafeNativeMethods.SendMessage( handle, UnsafeNativeMethods.WM_SETREDRAW, ( UIntPtr ) (Conversions.ToInteger( isOn ) & 1), ( IntPtr ) 1 );
            }

            #endregion

        }

    }
}
