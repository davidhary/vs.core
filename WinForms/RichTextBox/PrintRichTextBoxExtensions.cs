using System;
using System.Drawing.Printing;
using System.Windows.Forms;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.WinForms.RichTextBoxExtensions
{

    public static partial class RichTextBoxExtensionMethods
    {

        #region " PRINTOUT FUNCTIONS "

        private static RichTextBox _RichTextBox;
        private static int _TextLength;
        private static PrintDocument _PrintDocument;
        private static int _CurrentPage, _FromPage, _ToPage;
        private static int[] _PageIndexes;
        private static int _PageCount;

        // private static read only Font CharFont = null;


        /// <summary> Hundredth inch to Twips. </summary>
        /// <param name="n"> An Integer to process. </param>
        /// <returns> An Int32. </returns>
        private static int HundredthInchToTwips( int n )
        {
            // convert units
            return Convert.ToInt32( n * 144 / 10 );
        }

        /// <summary> Gets page indexes.
        /// Determines indexes for page beginnings, based on printer info</summary>
        private static void GetPageIndexes()
        {
            _PageCount = 0;
            int firstCharOnPage = 0;
            do
            {
                // store index for start of current page
                Array.Resize( ref _PageIndexes, _PageCount + 1 );
                _PageIndexes[_PageCount] = firstCharOnPage;
                // measure current page
                firstCharOnPage = RichTextBoxExtensionMethods.SafeNativeMethods.FormatRange( _RichTextBox.Handle, _PrintDocument.PrinterSettings.CreateMeasurementGraphics(), _PrintDocument.DefaultPageSettings, firstCharOnPage, _TextLength, false );
                // prepare for next page
                _PageCount += 1;
            }
            while ( firstCharOnPage < _TextLength );
        }

        /// <summary> Gets page number.
        /// Searches for page containing a given caret Position</summary>
        /// <param name="position"> The position. </param>
        /// <returns> The page number. </returns>
        private static int GetPageNumber( int position )
        {
            int pageNumber = Array.BinarySearch( _PageIndexes, position );
            if ( pageNumber < 0 )
            {
                pageNumber ^= -1; // caret is inside of page
            }
            else
            {
                pageNumber += 1;
            }                  // caret is at beginning of page

            return pageNumber;
        }

        /// <summary>   Sets an up. Prepare for print job. </summary>
        /// <remarks>   David, 2020-09-24. </remarks>
        /// <param name="richTextBox">      The rich text format control. </param>
        /// <param name="printDocument">    Instance of PrintDocument. </param>
        /// <param name="pageIndexesOnly">  True to page indexes only. </param>
        private static void SetUp( RichTextBox richTextBox, PrintDocument printDocument, bool pageIndexesOnly )
        {
            // prepare for print job
            _RichTextBox = richTextBox;
            _TextLength = richTextBox.TextLength;
            _PrintDocument = printDocument;
            GetPageIndexes();
            if ( pageIndexesOnly )
            {
                return; // leave with page indexes
            }
            // else prepare to preview/print
            {
                var withBlock = _PrintDocument;
                // wire up events
                // (RemoveHandler is used before AddHandler to guard against double-firing
                // of events in the event this routine is called multiple times)
                withBlock.BeginPrint -= BeginPrint; // remove any pre-existing handler
                withBlock.BeginPrint += BeginPrint;    // add a new handler
                withBlock.PrintPage -= PrintPage;
                withBlock.PrintPage += PrintPage;
                withBlock.EndPrint -= EndPrint;
                withBlock.EndPrint += EndPrint;
                // determine which pages to print/preview
                {
                    var withBlock1 = withBlock.PrinterSettings;
                    switch ( withBlock1.PrintRange )
                    {
                        case PrintRange.AllPages:
                            {
                                // all pages
                                _FromPage = 1;
                                _ToPage = _PageCount;
                                break;
                            }

                        case PrintRange.SomePages:
                            {
                                // range of pages
                                _FromPage = withBlock1.FromPage - withBlock1.MinimumPage + 1;
                                _ToPage = withBlock1.ToPage - withBlock1.MinimumPage + 1;
                                break;
                            }

                        case PrintRange.Selection:
                            {
                                // pages of selected text
                                _FromPage = GetPageNumber( _RichTextBox.SelectionStart );
                                if ( _RichTextBox.SelectionLength == 0 )
                                {
                                    _ToPage = _FromPage; // no selection
                                }
                                else
                                {
                                    _ToPage = GetPageNumber( _RichTextBox.SelectionStart + _RichTextBox.SelectionLength - 1 );
                                }

                                break;
                            }

                        default:
                            {
                                // page at caret position
                                _FromPage = GetPageNumber( _RichTextBox.SelectionStart );
                                _ToPage = _FromPage;
                                break;
                            }
                    }
                    // validate page range
                    if ( _FromPage < 1 )
                    {
                        _FromPage = 1; // page #'s are 1-based
                    }
                    else if ( _FromPage > _PageCount )
                    {
                        _FromPage = _PageCount;
                    }

                    if ( _ToPage < _FromPage )
                    {
                        _ToPage = _FromPage; // at least 1 page
                    }
                    else if ( _ToPage > _PageCount )
                    {
                        _ToPage = _PageCount;
                    }
                }
            }
        }

        #endregion

        #region " PRINT DOCUMENT EVENT PROCEDURES "

        /// <summary> Begins a print. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Print event information. </param>
        private static void BeginPrint( object sender, PrintEventArgs e )
        {
            // prepare to start printing   
            _CurrentPage = _FromPage;
        }

        /// <summary> Print page. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Print page event information. </param>
        private static void PrintPage( object sender, PrintPageEventArgs e )
        {
            // print current page
            int firstCharOnNextPage = RichTextBoxExtensionMethods.SafeNativeMethods.FormatRange( _RichTextBox.Handle, e.Graphics, e.PageSettings, _PageIndexes[_CurrentPage - 1], _TextLength, true );
            // prepare for next page; is it already the last page?
            _CurrentPage += 1;
            e.HasMorePages = _CurrentPage <= _ToPage && firstCharOnNextPage < _TextLength;
        }

        /// <summary> Ends a print. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Print event information. </param>
        private static void EndPrint( object sender, PrintEventArgs e )
        {
            // finish printing
            RichTextBoxExtensionMethods.SafeNativeMethods.FormatRangeDone( _RichTextBox.Handle );
        }

        #endregion

        #region " PRINT FUNCTIONS "

        /// <summary>
        /// Width of left-side "selection margin" for highlighting whole lines
        /// when a RichTextBox's ShowSelection property is True
        /// </summary>
        public const float SelectionMargin = 8.0f;

        /// <summary>   Print RichTextBox contents or a range of pages thereof. </summary>
        /// <remarks>   David, 2020-09-24. </remarks>
        /// <param name="richTextBox">      The rich text format control. </param>
        /// <param name="printDocument">    Instance of PrintDocument. </param>
        public static void Print( this RichTextBox richTextBox, PrintDocument printDocument )
        {
            // print document
            SetUp( richTextBox, printDocument, false );
            printDocument.Print();
        }

        /// <summary>   Preview RichTextBox contents or a range of pages thereof to be printed. </summary>
        /// <remarks>   David, 2020-09-24. </remarks>
        /// <param name="richTextBox">          The rich text format control. </param>
        /// <param name="printPreviewDialog">   Instance of PrintPreviewDialog. </param>
        /// <returns>   Result of Print Preview dialog. </returns>
        public static DialogResult PrintPreview( this RichTextBox richTextBox, PrintPreviewDialog printPreviewDialog )
        {
            SetUp( richTextBox, printPreviewDialog.Document, false );
            return printPreviewDialog.ShowDialog();
        }

        /// <summary>   Get array of indexes for beginnings of pages. </summary>
        /// <remarks>
        /// Pages are measured according to PrintDocument.DefaultPageSettings;
        /// no print job is performed. There is always at least one index (array element)
        /// returned, and the first index is always 0, representing the beginning of all text.
        /// </remarks>
        /// <param name="richTextBox">      The rich text format control. </param>
        /// <param name="printDocument">    Instance of PrintDocument. </param>
        /// <returns>   An int[]. </returns>
        public static int[] PageIndexes( this RichTextBox richTextBox, PrintDocument printDocument )
        {
            SetUp( richTextBox, printDocument, true );
            return _PageIndexes;
        }

        /// <summary>
        /// Set RightMargin property of RichTextBox to width of printer page (within horizontal margins)
        /// so that text wraps at the same position in the text box as on the printer.
        /// </summary>
        /// <remarks>   David, 2020-09-24. </remarks>
        /// <param name="richTextBox">  The rich text format control. </param>
        /// <param name="pageSettings"> Instance of PageSettings. </param>
        public static void SetRightMarginToPrinterWidth( this RichTextBox richTextBox, PageSettings pageSettings )
        {
            int pageWidth;
            // get page width in 1/100's of inches 
            pageWidth = pageSettings.Bounds.Width - pageSettings.Margins.Left - pageSettings.Margins.Right;
            // set rich-text-boxes .RightMargin property
            richTextBox.RightMargin = Conversions.ToInteger( pageWidth * richTextBox.CreateGraphics().DpiX / 100.0d );
        }

        /// <summary>
        /// Set PageSettings right margin to RichTextBox's RightMargin value so that text wraps at the
        /// same position on the printer as in the text box.
        /// </summary>
        /// <remarks>   David, 2020-09-24. </remarks>
        /// <param name="richTextBox">  The rich text format control. </param>
        /// <param name="pageSettings"> Instance of PageSettings. </param>
        public static void SetPrinterWidthToRightMargin( this RichTextBox richTextBox, PageSettings pageSettings )
        {
            int pageWidth;
            // get text box's.RightMargin property in 1/100's of inches 
            pageWidth = Conversions.ToInteger( richTextBox.RightMargin * 100.0d / richTextBox.CreateGraphics().DpiX );
            // set left- and right-hand margin of printer page
            int difference = pageSettings.Bounds.Width - pageWidth;
            pageSettings.Margins.Left = difference / 2;
            pageSettings.Margins.Right = difference - pageSettings.Margins.Left;
        }

        #endregion

    }
}
