using System;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.WinForms.RichTextBoxExtensions
{
    /// <summary> Rich text box extension print methods. </summary>
    /// <remarks> (c) 2019 Robert Gustafson. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-10-18, ">
    /// https://www.codeproject.com/Tips/1226034/General-purpose-RULER-Control-for-Use-with-RICH-TE.
    /// https://www.codeproject.com/Articles/868653/EXTENDED-Version-of-Extended-Rich-Text-Box-RichTex </para></remarks>
    public static partial class RichTextBoxExtensionMethods
    {

        #region " SCROLLING/TEXT-WIDTH FUNCTIONS "

        /// <summary>   Gets font at character position. </summary>
        /// <remarks>
        /// NOTE: Selection isn't changed if it's already where it needs to be, lest recursion result if
        /// GetRightMostCharacterPosition is called within the control's SelectionChanged event and
        /// recursion results!
        /// </remarks>
        /// <param name="richTextBox">  The rich text format control. </param>
        /// <param name="charPos">      The character position. </param>
        /// <returns>   The font at character position. </returns>
        private static Font GetFontAtCharacterPosition( RichTextBox richTextBox, int charPos )
        {
            Font charFont;
            if ( richTextBox.SelectionStart == charPos && richTextBox.SelectionLength == 0 )
            {
                // temporarily change selection
                int currentSS = richTextBox.SelectionStart;
                int currentSL = richTextBox.SelectionLength;
                richTextBox.SuspendLayout();
                richTextBox.Select( charPos, 0 );
                charFont = richTextBox.SelectionFont;
                richTextBox.Select( currentSS, currentSL );
                richTextBox.ResumeLayout();
            }
            else
            {
                charFont = richTextBox.SelectionFont;
            }

            return charFont;
        }

        /// <summary>   Gets right most character position. Gets width of widest line in text. </summary>
        /// <remarks>   David, 2020-09-24. </remarks>
        /// <param name="richTextBox">  The rich text format control. </param>
        /// <returns>   The right most character position. </returns>
        private static int GetRightMostCharacterPosition( RichTextBox richTextBox )
        {
            int rightMostPos = 0;
            int lineLength, lineWidth, lineIndex, charIndex, charWidth;
            string line, character;
            // go through each line
            for ( int LineNumber = 0, loopTo = richTextBox.Lines.GetUpperBound( 0 ); LineNumber <= loopTo; LineNumber++ )
            {
                // how long is this line?
                line = richTextBox.Lines[LineNumber];
                lineLength = line.Length;
                if ( lineLength > 0 )
                {
                    // get rightmost character in line
                    lineIndex = richTextBox.GetFirstCharIndexFromLine( LineNumber );
                    charIndex = lineIndex + lineLength - 1;
                    character = line.Substring( lineLength - 1, 1 );
                    // get width up to rightmost char
                    lineWidth = richTextBox.GetPositionFromCharIndex( charIndex ).X - richTextBox.GetPositionFromCharIndex( lineIndex ).X;
                    // get size of char
                    Font charFont = GetFontAtCharacterPosition( richTextBox, charIndex + 1 );
                    using ( var g = richTextBox.CreateGraphics() )
                    {
                        charWidth = ( int ) (richTextBox.CreateGraphics().MeasureString( character, charFont ).Width * richTextBox.ZoomFactor);
                    }

                    lineWidth += charWidth;
                    // see if new line is longer than previous ones
                    if ( lineWidth > rightMostPos )
                    {
                        rightMostPos = lineWidth;
                    }
                }
            }

            return rightMostPos;
        }

        #endregion

        #region " SCROLLING/TEXT-WIDTH METHODS "

        /// <summary>   Get scroll position of RichTextBox. </summary>
        /// <remarks>   David, 2020-09-24. </remarks>
        /// <param name="richTextBox">  The rich text format control. </param>
        /// <returns>
        /// Point structure containing current horizontal (.x)
        /// and vertical (.y) scroll positions in pixels.
        /// </returns>
        public static Point GetScrollPosition( this RichTextBox richTextBox )
        {
            return RichTextBoxExtensionMethods.SafeNativeMethods.GetScrollPosition( richTextBox.Handle );
        }

        /// <summary>   Set scroll position of RichTextBox. </summary>
        /// <remarks>   David, 2020-09-24. </remarks>
        /// <param name="richTextBox">  The rich text format control. </param>
        /// <param name="scrollPoint">  Point structure containing new horizontal (.x)
        ///                             and vertical (.y) scroll positions in pixels. </param>
        public static void SetScrollPosition( this RichTextBox richTextBox, Point scrollPoint )
        {
            RichTextBoxExtensionMethods.SafeNativeMethods.SetScrollPosition( richTextBox.Handle, scrollPoint );
        }

        /// <summary>   Get information about a RichTextBox scroll bar. </summary>
        /// <remarks>   David, 2020-09-24. </remarks>
        /// <param name="richTextBox">      The rich control. </param>
        /// <param name="scrollBarType">    ScrollBarType value (.Horizontal or .Vertical) </param>
        /// <param name="scrollBarMask">    (Optional) ScrollBarMask flags indicating what to get (range,
        ///                                 page size, position, track position; defaults to everything) </param>
        /// <returns>   ScrollInfo structure with requested info. </returns>
        private static RichTextBoxExtensionMethods.UnsafeNativeMethods.ScrollInfo GetScrollBarInfo( this RichTextBox richTextBox, RichTextBoxExtensionMethods.UnsafeNativeMethods.ScrollBarType scrollBarType, RichTextBoxExtensionMethods.UnsafeNativeMethods.ScrollBarMask scrollBarMask = RichTextBoxExtensionMethods.UnsafeNativeMethods.ScrollBarMask.Everything )
        {
            return RichTextBoxExtensionMethods.SafeNativeMethods.GetScrollBarInfo( richTextBox.Handle, scrollBarType, scrollBarMask );
        }


        /// <summary>   Get effective maximum text width of RichTextBox in pixels. </summary>
        /// <remarks>
        /// This value is calculated as follows:
        /// 1. If control's RightMargin property is non-zero, then that us used
        /// 2. Otherwise, if WordWrap is True, then the control's client-area width
        /// minus any left-edge "selection" margin is used
        /// 3. Otherwise, if horizontal scrollbars are enabled, then the "maximum horizontal
        /// scroll position" plus the client width, or the width of the longest physical line, whichever
        /// is longer, is used
        /// 4. Otherwise, the width of the longest physical line is used.
        /// </remarks>
        /// <param name="richTextBox">  The rich text format control. </param>
        /// <returns>
        /// Maximum available physical width for any text. (-1 if we're in a recursive loop--see remarks)
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0059:Unnecessary assignment of a value", Justification = "<Pending>" )]
        public static int GetMaximumWidth( this RichTextBox richTextBox )
        {
            // see if text width is fixed
            if ( richTextBox.RightMargin > 0 )
            {
                // yes, so return with fixed width
                return richTextBox.RightMargin;
            }
            // else start with width of text box
            int selectionOffset = +richTextBox.Padding.Left + 1;
            int maxTextWidth = richTextBox.ClientRectangle.Width;
            if ( richTextBox.ShowSelectionMargin )
            {
                // account for any selection margin
                selectionOffset += ( int ) (richTextBox.ZoomFactor * RichTextBoxExtensionMethods.SelectionMargin);
            }

            maxTextWidth -= ( int ) (richTextBox.ZoomFactor * RichTextBoxExtensionMethods.SelectionMargin);
            // determine maximum line width
            if ( !richTextBox.WordWrap )
            {
                if ( (richTextBox.ScrollBars & RichTextBoxScrollBars.Horizontal) == RichTextBoxScrollBars.Horizontal )
                {
                    // determine rightmost character position from scrollbar info
                    maxTextWidth += richTextBox.GetScrollBarInfo( RichTextBoxExtensionMethods.UnsafeNativeMethods.ScrollBarType.Horizontal ).MaximumScrollPosition;
                }
                // make sure value is at least as large as widest line
                maxTextWidth = Math.Max( maxTextWidth, GetRightMostCharacterPosition( richTextBox ) );
            }
            // return value
            return maxTextWidth;
        }

        #endregion

        #region " RTF METHODS "

        /// <summary>   Mark selected text in RichTextBox as a list, using ListStyle. </summary>
        /// <remarks>
        /// RichTextBox.SelectionBullet returns False for any list style other than bullets when read;
        /// there is no easy way to GET the specific list-style of selected text even when parsing RTF
        /// code (RichTextBox.SelRTF), since, for instance, "I" can just as easily be Roman numeral for 1
        /// OR letter "I", given the way the control defines lists.
        /// </remarks>
        /// <param name="richTextBox">  The rich text format control. </param>
        /// <param name="listStyle">    Style of list (no list, bullets, numbers, lowercase letters,
        ///                             uppercase letters, lowercase Roman numerals, or uppercase Roman
        ///                             numerals) </param>
        public static void SetListStyle( this RichTextBox richTextBox, RichTextFormatListStyle listStyle )
        {
            // get active control
            var activeControl = richTextBox.GetContainerControl().ActiveControl;
            // set keyboard focus to rich-text box
            richTextBox.SuspendLayout();
            richTextBox.Select();
            // set list style
            switch ( listStyle )
            {
                case RichTextFormatListStyle.NoList:
                    {
                        richTextBox.SelectionBullet = false;
                        break;
                    }

                case RichTextFormatListStyle.Bullets:
                    {
                        richTextBox.SelectionBullet = true;
                        break;
                    }

                default:
                    {
                        // cycle through styles until desired one is selected
                        richTextBox.SelectionBullet = true;
                        SendKeys.Send( "^+({L " + Conversions.ToInteger( listStyle ).ToString() + "})" );
                        break;
                    }
            }
            // restore original focus
            richTextBox.ResumeLayout();
            if ( activeControl is object )
            {
                activeControl.Select();
            }
        }



        /// <summary>   Convert plain-text string into RTF string. </summary>
        /// <remarks>   David, 2020-09-24. </remarks>
        /// <param name="richTextBox">  The rich text format control. </param>
        /// <param name="plainText">    Plain-text string. </param>
        /// <returns>   String with special characters ("{", "\", "}", and non-ASCII) escaped. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        public static string EscapedRTFText( this RichTextBox richTextBox, string plainText )
        {
            // escape any special RTF characters in string
            var builder = new StringBuilder( "" );
            for ( int CharIndex = 0, loopTo = plainText.Length - 1; CharIndex <= loopTo; CharIndex++ )
            {
                // parse plain-text string
                string character = plainText.Substring( CharIndex, 1 );
                switch ( character ?? "" )
                {
                    case " ":
                    case "{":
                    case @"\":
                    case "}":
                        {
                            // escape character
                            _ = builder.Append( @"\" + character );
                            break;
                        }

                    case var @case when Strings.AscW( @case ) < Strings.AscW( " " ):

                    case var case1 when Strings.AscW( case1 ) > Strings.AscW( "~" ):
                        {
                            // non-ASCII character
                            _ = builder.Append( @"\u" + Strings.AscW( character ).ToString() + "?" );
                            break;
                        }

                    default:
                        {
                            // normal character
                            _ = builder.Append( character );
                            break;
                        }
                }
            }
            // return escaped result
            return builder.ToString();
        }

        /// <summary>   Insert RTF text into rich-text box at a given position. </summary>
        /// <remarks>
        /// This is the "safe" way to insert, as it accounts for "template RTF" that's expected in any
        /// inserted RTF text.
        /// </remarks>
        /// <param name="richTextBox">      The rich text format control. </param>
        /// <param name="rtfTextToInsert">  RTF-format text to insert. </param>
        /// <param name="position">         (Optional) position in rich-text box to make insertion
        ///                                 (defaults to current selection position if omitted) </param>
        public static void InsertRtf( this RichTextBox richTextBox, string rtfTextToInsert, int position = -1 )
        {
            // position defaults to current selection point
            if ( position < 0 | position > richTextBox.TextLength )
            {
                position = richTextBox.SelectionStart;
            }
            else
            {
                richTextBox.SelectionLength = 0;
            } // arbitrary insertion
            // get RTF template from an empty selection and insert new RTF text
            int length = richTextBox.SelectionLength;
            richTextBox.Select( position, 0 );
            string rtfText = richTextBox.SelectedRtf;
            richTextBox.Select( position, length );
            int endBrace = rtfText.LastIndexOf( '}' ); // new text comes before RTF closing brace
            rtfText = rtfText.Insert( endBrace, rtfTextToInsert );
            richTextBox.SelectedRtf = rtfText;
        }

        #endregion

        #region " DRAWING METHODS "

        /// <summary>
        /// Turns on or off redrawing of rich-text box This can be used to make multiple changes to the
        /// text box while preventing the intermediate rendering that would cause flicker.
        /// </summary>
        /// <remarks>
        /// Specifying True forces the cumulative results of previous operations to be rendered.
        /// </remarks>
        /// <param name="control">  The control to act on. </param>
        /// <param name="isOn">     True to activate auto-redraw, False to deactivate it. </param>
        public static void SetRedrawMode( this Control control, bool isOn )
        {
            RichTextBoxExtensionMethods.SafeNativeMethods.SetRedrawMode( control.Handle, isOn );
            if ( isOn )
            {
                control.Invalidate(); // force redraw
            }
        }

        #endregion

    }


    /// <summary> Values that represent rich text box list styles. </summary>
    public enum RichTextFormatListStyle
    {

        /// <summary>   An enum constant representing the no list option. </summary>
        NoList = -1,

        /// <summary>   An enum constant representing the bullets option. </summary>
        Bullets,

        /// <summary>   An enum constant representing the numbers option. </summary>
        Numbers,

        /// <summary>   An enum constant representing the lowercase letters option. </summary>
        LowercaseLetters,

        /// <summary>   An enum constant representing the uppercase letters option. </summary>
        UppercaseLetters,

        /// <summary>   An enum constant representing the lowercase roman numerals option. </summary>
        LowercaseRomanNumerals,

        /// <summary>   An enum constant representing the uppercase roman numerals option. </summary>
        UppercaseRomanNumerals
    }

}
