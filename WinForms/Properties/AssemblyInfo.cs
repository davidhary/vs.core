using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.Core.WinForms.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.Core.WinForms.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.Core.WinForms.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]

[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( isr.Core.WinForms.My.MyLibrary.TestAssemblyStrongName )]
