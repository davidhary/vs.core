using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

using Microsoft.VisualBasic;

namespace isr.Core.WinForms.DataGridViewExtensions
{
    /// <summary> Includes extensions for <see cref="DataGridView">Data Grid View</see>. </summary>
    /// <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2010-11-19, 1.2.3975 </para></remarks>
    public static class DataGridViewExtensionMethods
    {

        #region " DISPLAY "

        /// <summary> Set grid rendering as double buffered to reduce flickering. </summary>
        /// <remarks>
        /// From http://www.CodeProject.com/Tips/390496/Reducing-flicker-blinking-in- DataGridView.
        /// </remarks>
        /// <param name="grid">    The grid. </param>
        /// <param name="enabled"> Enabled if set to <c>True</c>. </param>
        public static void DoubleBuffered( this DataGridView grid, bool enabled )
        {
            if ( grid is object )
            {
                var dgvType = grid.GetType();
                var pi = dgvType.GetProperty( "DoubleBuffered", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic );
                pi.SetValue( grid, enabled, null );
            }
        }

        /// <summary> Hides all columns. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="grid"> The <see cref="DataGridView">grid</see> </param>
        public static void HideColumns( this DataGridView grid )
        {
            if ( grid is object )
            {
                foreach ( DataGridViewColumn column in grid.Columns )
                {
                    column.Visible = false;
                }
            }
        }

        /// <summary>
        /// Hides the columns by the <paramref name="columnNames">column names</paramref>.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="grid">        The grid. </param>
        /// <param name="columnNames"> The column names. </param>
        public static void HideColumns( this DataGridView grid, string[] columnNames )
        {
            if ( grid is object && columnNames is object )
            {
                foreach ( DataGridViewColumn column in grid.Columns )
                {
                    if ( !columnNames.Contains( column.Name ) )
                    {
                        column.Visible = false;
                    }
                }
            }
        }

        /// <summary> Displays a row numbers described by grid. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="grid"> The grid. </param>
        public static void DisplayRowNumbers( this DataGridView grid )
        {
            if ( grid is null )
            {
                throw new ArgumentNullException( nameof( grid ) );
            }

            foreach ( DataGridViewRow row in grid.Rows )
            {
                row.HeaderCell.Value = (row.Index + 1).ToString();
            }
        }

        /// <summary> Visible column count. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="grid"> The grid. </param>
        /// <returns> The number of visible columns. </returns>
        public static int VisibleColumnCount( this DataGridView grid )
        {
            int count = 0;
            if ( grid is object )
            {
                foreach ( DataGridViewColumn column in grid.Columns )
                {
                    if ( column.Visible )
                    {
                        count += 1;
                    }
                }
            }

            return count;
        }

        /// <summary> Scrolls while keeping the displayed section centered. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="grid"> The <see cref="DataGridView">grid</see> </param>
        public static void ScrollCenterRow( this DataGridView grid )
        {
            if ( grid is null )
            {
                throw new ArgumentNullException( nameof( grid ) );
            }

            int halfWay = grid.DisplayedRowCount( false ) / 2;
            if ( grid.SelectedRows.Count == 0 )
            {
                grid.CurrentRow.Selected = true;
            }

            if ( grid.FirstDisplayedScrollingRowIndex + halfWay > grid.SelectedRows[0].Index || grid.FirstDisplayedScrollingRowIndex + grid.DisplayedRowCount( false ) - halfWay <= grid.SelectedRows[0].Index )
            {
                int targetRow = grid.SelectedRows[0].Index;
                targetRow = Math.Max( targetRow - halfWay, 0 );
                grid.FirstDisplayedScrollingRowIndex = targetRow;
                grid.PerformLayout();
            }
        }

        #endregion

        #region " COLUMN HEADER MANAGEMENT "

        /// <summary>
        /// Splits the string to words by adding spaces between lower and upper case characters.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="value"> The <c>String</c> value to split. </param>
        /// <returns> A <c>String</c> of words separated by spaces. </returns>
        private static string SplitWords( string value )
        {
            if ( string.IsNullOrWhiteSpace( value ) )
            {
                return string.Empty;
            }
            else
            {
                bool isSpace = false;
                bool isLowerCase = false;
                var newValue = new System.Text.StringBuilder();
                foreach ( char c in value )
                {
                    if ( !isSpace && isLowerCase && char.IsUpper( c ) )
                    {
                        _ = newValue.Append( " " );
                    }

                    isSpace = c.Equals( ' ' );
                    isLowerCase = !char.IsUpper( c );
                    _ = newValue.Append( c );
                }

                return newValue.ToString();
            }
        }

        /// <summary>
        /// Replaces the column header text with one that has spaces between lower and upper case
        /// characters.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="column"> The column. </param>
        public static void ParseHeaderText( this DataGridViewColumn column )
        {
            if ( column is object && !string.IsNullOrWhiteSpace( column.HeaderText ) )
            {
                column.HeaderText = SplitWords( column.HeaderText );
            }
        }

        /// <summary>
        /// Replaces the column headers text with one that has spaces between lower and upper case
        /// characters.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="grid"> The <see cref="DataGridView">grid</see> </param>
        public static void ParseHeaderText( this DataGridView grid )
        {
            if ( grid is object )
            {
                foreach ( DataGridViewColumn column in grid.Columns )
                {
                    column.ParseHeaderText();
                }
            }
        }

        /// <summary>
        /// Replaces the column headers text with one that has spaces between lower and upper case
        /// characters. Operates only on visible headers.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="grid"> The <see cref="DataGridView">grid</see> </param>
        public static void ParseVisibleHeaderText( this DataGridView grid )
        {
            if ( grid is object )
            {
                foreach ( DataGridViewColumn column in grid.Columns )
                {
                    if ( column.Visible )
                    {
                        column.ParseHeaderText();
                    }
                }
            }
        }

        #endregion

        #region " COLUMN MANAGEMENT: Duplicate, Replace, Move, "

        /// <summary>
        /// Replaces an existing column with a combo box column and returns reference to this column.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="grid">       The grid. </param>
        /// <param name="columnName"> Name of the column. </param>
        /// <returns> Replaced column. </returns>
        public static DataGridViewComboBoxColumn ReplaceWithComboColumn( this DataGridView grid, string columnName )
        {
            if ( grid is null )
            {
                throw new ArgumentNullException( nameof( grid ) );
            }

            if ( string.IsNullOrWhiteSpace( columnName ) )
            {
                throw new ArgumentNullException( nameof( columnName ) );
            }

            var column = grid.Columns[columnName];
            var gridColumn = new DataGridViewComboBoxColumn();
            try
            {
                gridColumn.Name = column.Name;
                gridColumn.DataPropertyName = column.DataPropertyName;
                gridColumn.HeaderText = column.HeaderText;
                // gridColumn.IsDataBound = column.IsDataBound
                grid.Columns.Remove( column );
                // this does not work right. Me._dataGridView.Columns.Insert(fieldColumn.Ordinal, gridColumn)
                grid.Columns.Insert( column.Index, gridColumn );
                return gridColumn;
            }
            catch
            {
                gridColumn?.Dispose();
                throw;
            }
        }

        /// <summary> Duplicates an existing column returns reference to this column. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="grid">       The grid. </param>
        /// <param name="columnName"> Name of the column. </param>
        /// <param name="newName">    Name of the new. </param>
        /// <returns> Replaced column. </returns>
        public static DataGridViewColumn DuplicateColumn( this DataGridView grid, string columnName, string newName )
        {
            if ( grid is null )
            {
                throw new ArgumentNullException( nameof( grid ) );
            }

            if ( string.IsNullOrWhiteSpace( columnName ) )
            {
                throw new ArgumentNullException( nameof( columnName ) );
            }

            var column = grid.Columns[columnName];
            var gridColumn = new DataGridViewColumn();
            try
            {
                gridColumn.Name = newName;
                gridColumn.DataPropertyName = column.DataPropertyName;
                gridColumn.HeaderText = column.HeaderText;
                grid.Columns.Insert( column.Index, gridColumn );
                return gridColumn;
            }
            catch
            {
                gridColumn?.Dispose();
                throw;
            }
        }

        /// <summary> Duplicate combo column. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="grid">       The grid. </param>
        /// <param name="columnName"> Name of the column. </param>
        /// <param name="newName">    Name of the new. </param>
        /// <returns> A  DataGridViewCheckBoxColumn. </returns>
        public static DataGridViewComboBoxColumn DuplicateComboColumn( this DataGridView grid, string columnName, string newName )
        {
            if ( grid is null )
            {
                throw new ArgumentNullException( nameof( grid ) );
            }

            if ( string.IsNullOrWhiteSpace( columnName ) )
            {
                throw new ArgumentNullException( nameof( columnName ) );
            }

            var column = grid.Columns[columnName];
            var gridColumn = new DataGridViewComboBoxColumn();
            try
            {
                gridColumn.Name = newName;
                gridColumn.DataPropertyName = column.DataPropertyName;
                gridColumn.HeaderText = column.HeaderText;
                grid.Columns.Insert( column.Index, gridColumn );
                return gridColumn;
            }
            catch
            {
                gridColumn?.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Replaces an existing column with a check box column and returns reference to this column.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="grid">       The grid. </param>
        /// <param name="columnName"> Name of the column. </param>
        /// <returns> Replaced column. </returns>
        public static DataGridViewCheckBoxColumn ReplaceWithCheckColumn( this DataGridView grid, string columnName )
        {
            if ( grid is null )
            {
                throw new ArgumentNullException( nameof( grid ) );
            }

            if ( string.IsNullOrWhiteSpace( columnName ) )
            {
                throw new ArgumentNullException( nameof( columnName ) );
            }

            var column = grid.Columns[columnName];
            var gridColumn = new DataGridViewCheckBoxColumn();
            try
            {
                gridColumn.Name = column.Name;
                gridColumn.DataPropertyName = column.DataPropertyName;
                gridColumn.HeaderText = column.HeaderText;
                gridColumn.ThreeState = true;
                grid.Columns.Remove( column );
                grid.Columns.Insert( column.Index, gridColumn );
                return gridColumn;
            }
            catch
            {
                gridColumn?.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Replaces an existing column with a text box column and returns reference to this column.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="grid">       The grid. </param>
        /// <param name="columnName"> Name of the column. </param>
        /// <returns> Replaced column. </returns>
        public static DataGridViewTextBoxColumn ReplaceWithTextColumn( this DataGridView grid, string columnName )
        {
            if ( grid is null )
            {
                throw new ArgumentNullException( nameof( grid ) );
            }

            if ( string.IsNullOrWhiteSpace( columnName ) )
            {
                throw new ArgumentNullException( nameof( columnName ) );
            }

            var column = grid.Columns[columnName];
            var gridColumn = new DataGridViewTextBoxColumn();
            try
            {
                gridColumn.Name = column.Name;
                gridColumn.DataPropertyName = column.DataPropertyName;
                gridColumn.HeaderText = column.HeaderText;
                // gridColumn.IsDataBound = column.IsDataBound
                grid.Columns.Remove( column );
                // this does not work right. Me._dataGridView.Columns.Insert(fieldColumn.Ordinal, gridColumn)
                grid.Columns.Insert( column.Index, gridColumn );
                return gridColumn;
            }
            catch
            {
                gridColumn?.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Replaces an existing column with a text box column and returns reference to this column.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="grid">       The grid. </param>
        /// <param name="columnName"> Name of the column. </param>
        /// <param name="newName">    Name of the new. </param>
        /// <returns> Replaced column. </returns>
        public static DataGridViewTextBoxColumn DuplicateTextColumn( this DataGridView grid, string columnName, string newName )
        {
            if ( grid is null )
            {
                throw new ArgumentNullException( nameof( grid ) );
            }

            if ( string.IsNullOrWhiteSpace( columnName ) )
            {
                throw new ArgumentNullException( nameof( columnName ) );
            }

            var column = grid.Columns[columnName];
            var gridColumn = new DataGridViewTextBoxColumn();
            try
            {
                gridColumn.Name = newName;
                gridColumn.DataPropertyName = column.DataPropertyName;
                gridColumn.HeaderText = column.HeaderText;
                grid.Columns.Insert( column.Index, gridColumn );
                return gridColumn;
            }
            catch
            {
                gridColumn?.Dispose();
                throw;
            }
        }

        /// <summary> Moves an existing column to the specified location. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="grid">       The grid. </param>
        /// <param name="columnName"> Name of the column. </param>
        /// <param name="location">   The location. </param>
        /// <returns> The moved column. </returns>
        public static DataGridViewColumn MoveTo( this DataGridView grid, string columnName, int location )
        {
            if ( grid is null )
            {
                throw new ArgumentNullException( nameof( grid ) );
            }

            if ( string.IsNullOrWhiteSpace( columnName ) )
            {
                throw new ArgumentNullException( nameof( columnName ) );
            }

            var column = grid.Columns[columnName];
            grid.Columns.Remove( column );
            grid.Columns.Insert( location, column );
            return column;
        }

        #endregion

        #region " ROW MANAGEMENT: Find, Select "

        /// <summary> Returns the row selected based on the column value. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="grid">        The grid. </param>
        /// <param name="columnName">  Name of the column. </param>
        /// <param name="columnValue"> The column value. </param>
        /// <returns> The found row. </returns>
        public static DataGridViewRow FindRow<T>( this DataGridView grid, string columnName, T columnValue )
        {
            if ( grid is null )
            {
                throw new ArgumentNullException( nameof( grid ) );
            }

            if ( string.IsNullOrWhiteSpace( columnName ) )
            {
                throw new ArgumentNullException( nameof( columnName ) );
            }

            DataGridViewRow foundRow = null;
            foreach ( DataGridViewRow r in grid.Rows )
            {
                if ( columnValue.Equals( r.Cells[columnName].Value ) )
                {
                    foundRow = r;
                    break;
                }
            }

            return foundRow;
        }

        /// <summary> Selects the row. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="grid">        The grid. </param>
        /// <param name="columnName">  Name of the column. </param>
        /// <param name="columnValue"> The column value. </param>
        /// <returns> <c>True</c> if the row was selected. </returns>
        public static bool SelectRow<T>( this DataGridView grid, string columnName, T columnValue )
        {
            if ( grid is null )
            {
                throw new ArgumentNullException( nameof( grid ) );
            }

            if ( string.IsNullOrWhiteSpace( columnName ) )
            {
                throw new ArgumentNullException( nameof( columnName ) );
            }

            var foundRow = grid.FindRow( columnName, columnValue );
            if ( foundRow is null )
            {
                return false;
            }
            else
            {
                foundRow.Selected = true;
                return true;
            }
        }

        /// <summary>
        /// Silently selects a grid row. The control is disabled when set so that the handling of the
        /// changed event can be skipped.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="grid">        The grid. </param>
        /// <param name="columnName">  Name of the column. </param>
        /// <param name="columnValue"> The column value. </param>
        /// <returns> <c>True</c> if the row was selected. </returns>
        public static bool SilentSelectRow<T>( this DataGridView grid, string columnName, T columnValue )
        {
            if ( grid is null )
            {
                throw new ArgumentNullException( nameof( grid ) );
            }

            if ( string.IsNullOrWhiteSpace( columnName ) )
            {
                throw new ArgumentNullException( nameof( columnName ) );
            }

            bool enabled = grid.Enabled;
            try
            {
                grid.Enabled = false;
                return grid.SelectRow( columnName, columnValue );
            }
            catch
            {
                throw;
            }
            finally
            {
                grid.Enabled = enabled;
            }
        }

        #endregion

        #region " STORE "

        /// <summary> Stores the grid to file. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="grid">     The grid. </param>
        /// <param name="fileName"> Filename of the file. </param>
        /// <returns> An Integer. </returns>
        public static int Store( this DataGridView grid, string fileName )
        {
            if ( grid is null )
            {
                return 0;
            }

            var headers = (from header in grid.Columns.Cast<DataGridViewColumn>()
                           select header.HeaderText).ToArray();
            var rows = from row in grid.Rows.Cast<DataGridViewRow>()
                       where !row.IsNewRow
                       select Array.ConvertAll( row.Cells.Cast<DataGridViewCell>().ToArray(), c => c.Value is object ? c.Value.ToString() : "" );
            using ( var sw = new System.IO.StreamWriter( fileName ) )
            {
                sw.WriteLine( string.Join( ",", headers ) );
                foreach ( string[] r in rows )
                {
                    sw.WriteLine( string.Join( ",", r ) );
                }
            }

            return rows.Count();
        }

        /// <summary> Stores the grid to file. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="grid">           The grid. </param>
        /// <param name="fileName">       Filename of the file. </param>
        /// <param name="newLineReplace"> The new line replace string. </param>
        /// <returns> An Integer. </returns>
        public static int Store( this DataGridView grid, string fileName, string newLineReplace )
        {
            if ( grid is null )
            {
                return 0;
            }

            var headers = (from header in grid.Columns.Cast<DataGridViewColumn>()
                           select header.HeaderText).ToArray();
            var rows = from row in grid.Rows.Cast<DataGridViewRow>()
                       where !row.IsNewRow
                       select Array.ConvertAll( row.Cells.Cast<DataGridViewCell>().ToArray(), c => c.Value is object ? c.Value.ToString() : "" );
            using ( var sw = new System.IO.StreamWriter( fileName ) )
            {
                string s = string.Join( ",", headers );
                if ( !string.IsNullOrWhiteSpace( s ) )
                {
                    sw.WriteLine( s.Replace( Environment.NewLine, newLineReplace ) );
                }

                foreach ( string[] r in rows )
                {
                    s = string.Join( ",", r );
                    if ( !string.IsNullOrWhiteSpace( s ) )
                    {
                        sw.WriteLine( s.Replace( Environment.NewLine, newLineReplace ) );
                    }
                }
            }

            return rows.Count();
        }

        #endregion

        #region " COPY "

        /// <summary> Copies to clipboard described by grid. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="grid"> The <see cref=" DataGridView">grid</see> </param>
        public static void CopyToClipboard( this DataGridView grid )
        {
            if ( grid is object && (grid.SelectedRows.Count > 0 || grid.SelectedColumns.Count > 0 || grid.SelectedCells.Count > 0) )
            {
                grid.SuspendLayout();
                // grid.RowHeadersVisible = False
                Clipboard.SetDataObject( grid.GetClipboardContent() );
                // grid.ClearSelection()
                // grid.RowHeadersVisible = True
                grid.ResumeLayout();
            }
        }


        #endregion

        #region " SORT "

        /// <summary>
        /// Sort on a number of columns when handling the on column header mouse click.
        /// </summary>
        /// <remarks> https://www.codeproject.com/Tips/1233057/Datagridview-with-Multisort. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="grid">       The <see cref="DataGridView">grid</see> </param>
        /// <param name="dataSource"> The data source. </param>
        /// <param name="e">          Data grid view cell mouse event information. </param>
        public static void MultipleSort( this DataGridView grid, BindingSource dataSource, DataGridViewCellMouseEventArgs e )
        {
            if ( grid is null )
            {
                throw new ArgumentNullException( nameof( grid ) );
            }

            if ( dataSource is null )
            {
                throw new ArgumentNullException( nameof( dataSource ) );
            }

            if ( e is null )
            {
                throw new ArgumentNullException( nameof( e ) );
            }

            if ( Control.ModifierKeys == Keys.Control )
            {
                if ( dataSource.Sort.Contains( grid.Columns[e.ColumnIndex].Name ) )
                {
                    dataSource.Sort = dataSource.Sort.Contains( grid.Columns[e.ColumnIndex].Name + " ASC" ) ? dataSource.Sort.Replace( grid.Columns[e.ColumnIndex].Name + " ASC", grid.Columns[e.ColumnIndex].Name + " DESC" ) : dataSource.Sort.Replace( grid.Columns[e.ColumnIndex].Name + " DESC", grid.Columns[e.ColumnIndex].Name + " ASC" );
                }
                else
                {
                    dataSource.Sort += ", " + grid.Columns[e.ColumnIndex].Name + " ASC";
                }

                var allSort = Strings.Split( dataSource.Sort, ", " );
                foreach ( string s in allSort )
                {
                    var iSortOrder = Strings.Split( s, " " );
                    string cName = s.Replace( " " + iSortOrder[iSortOrder.Length - 1], "" );
                    switch ( iSortOrder[iSortOrder.Length - 1] ?? "" )
                    {
                        case "ASC":
                            {
                                grid.Columns[cName].HeaderCell.SortGlyphDirection = SortOrder.Ascending;
                                break;
                            }

                        case "DESC":
                            {
                                grid.Columns[cName].HeaderCell.SortGlyphDirection = SortOrder.Descending;
                                break;
                            }
                    }
                }
            }
            else
            {
                dataSource.Sort = dataSource.Sort.Contains( grid.Columns[e.ColumnIndex].Name + " DESC" ) ? grid.Columns[e.ColumnIndex].Name + " ASC" : grid.Columns[e.ColumnIndex].Name + " DESC";
            }
        }

        #endregion

        #region " PRINT "

        /// <summary> Prints grid. </summary>
        /// <remarks>
        /// From https://stackoverflow.com/questions/24654790/how-to-print-datagridview-has-a- table-in-
        /// vb.
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="grid">         The <see cref="DataGridView">grid</see> </param>
        /// <param name="e">            Print page event information. </param>
        /// <param name="rowIndex">     [in,out] Zero-based index of the row. </param>
        /// <param name="isNewPage">    [in,out] The is new page. </param>
        /// <param name="bottomMargin"> [in,out] The bottom margin. </param>
        public static void Print( this DataGridView grid, System.Drawing.Printing.PrintPageEventArgs e, ref int rowIndex, ref bool isNewPage, ref float bottomMargin )
        {
            if ( grid is null )
            {
                throw new ArgumentNullException( nameof( grid ) );
            }

            if ( e is null )
            {
                throw new ArgumentNullException( nameof( e ) );
            }

            using var fmt = new StringFormat( StringFormatFlags.LineLimit ) {
                LineAlignment = StringAlignment.Center,
                Trimming = StringTrimming.EllipsisCharacter
            };
            float y = e.MarginBounds.Top;
            if ( bottomMargin > 0f )
            {
                y = bottomMargin;
            }

            // print column headers
            if ( isNewPage && rowIndex == 0 )
            {
                isNewPage = false;
                float h = 0f;
                float x = e.MarginBounds.Left;
                foreach ( DataGridViewColumn column in grid.Columns )
                {
                    var rc = new RectangleF( x, y, column.HeaderCell.Size.Width, column.HeaderCell.Size.Height );
                    h = Math.Max( h, rc.Height );
                    if ( y + h > e.MarginBounds.Bottom )
                    {
                        e.HasMorePages = true;
                        break;
                    }

                    e.Graphics.DrawRectangle( Pens.Black, rc.Left, rc.Top, rc.Width, rc.Height );
                    e.Graphics.DrawString( column.HeaderText, grid.Font, Brushes.Black, rc, fmt );
                    x += rc.Width;
                }

                y += h;
                bottomMargin = y;
            }

            // print the row cells
            while ( rowIndex < grid.RowCount && !e.HasMorePages )
            {
                var row = grid.Rows[rowIndex];
                float x = e.MarginBounds.Left;
                float h = 0f;
                foreach ( DataGridViewCell cell in row.Cells )
                {
                    var rc = new RectangleF( x, y, cell.Size.Width, cell.Size.Height );
                    h = Math.Max( h, rc.Height );
                    if ( y + h > e.MarginBounds.Bottom )
                    {
                        e.HasMorePages = true;
                        break;
                    }

                    e.Graphics.DrawRectangle( Pens.Black, rc.Left, rc.Top, rc.Width, rc.Height );
                    e.Graphics.DrawString( grid.Rows[cell.RowIndex].Cells[cell.ColumnIndex].FormattedValue.ToString(), grid.Font, Brushes.Black, rc, fmt );
                    x += rc.Width;
                }

                if ( !e.HasMorePages )
                {
                    rowIndex += 1;
                    y += h;
                    bottomMargin = y;
                }
            }

            if ( e.HasMorePages )
            {
                isNewPage = true;
            }
        }

        /// <summary> Prints grid. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="grid">                The <see cref="DataGridView">grid</see> </param>
        /// <param name="selectedCellIndexes"> The selected cell indexes. </param>
        /// <param name="e">                   Print page event information. </param>
        /// <param name="rowIndex">            [in,out] Zero-based index of the row. </param>
        /// <param name="isNewPage">           [in,out] The is new page. </param>
        public static void Print( this DataGridView grid, int[] selectedCellIndexes, System.Drawing.Printing.PrintPageEventArgs e, ref int rowIndex, ref bool isNewPage )
        {
            if ( grid is null || e is null || selectedCellIndexes is null )
            {
                return;
            }

            using ( var fmt = new StringFormat( StringFormatFlags.LineLimit ) )
            {
                fmt.LineAlignment = StringAlignment.Center;
                fmt.Trimming = StringTrimming.EllipsisCharacter;
                float y = e.MarginBounds.Top;
                while ( rowIndex < grid.RowCount )
                {
                    var row = grid.Rows[rowIndex];
                    float x = e.MarginBounds.Left;
                    float h = 0f;
                    foreach ( int cell in selectedCellIndexes )
                    {
                        var rc = new RectangleF( x, y, row.Cells[cell].Size.Width, row.Cells[cell].Size.Height );
                        e.Graphics.DrawRectangle( Pens.Black, rc.Left, rc.Top, rc.Width, rc.Height );
                        if ( isNewPage )
                        {
                            e.Graphics.DrawString( grid.Columns[cell].HeaderText, grid.Font, Brushes.Black, rc, fmt );
                        }
                        else
                        {
                            e.Graphics.DrawString( grid.Rows[row.Cells[cell].RowIndex].Cells[cell].FormattedValue.ToString(), grid.Font, Brushes.Black, rc, fmt );
                        }

                        x += rc.Width;
                        h = Math.Max( h, rc.Height );
                    }

                    if ( !isNewPage )
                    {
                        rowIndex += 1;
                    }

                    isNewPage = false;
                    y += h;
                    if ( y + h > e.MarginBounds.Bottom )
                    {
                        e.HasMorePages = true;
                        rowIndex -= 1;
                        isNewPage = true;
                        return;
                    }
                }
            }

            rowIndex = 0;
        }

        #endregion

    }
}
