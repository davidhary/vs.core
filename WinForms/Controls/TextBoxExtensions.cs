using System;
using System.Windows.Forms;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.WinForms.TextBoxExtensions
{
    /// <summary> Includes extensions for <see cref="TextBox">Text Box</see>. </summary>
    /// <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2010-11-19, 1.2.3975 </para></remarks>
    public static class TextBoxExtensionMethods
    {

        #region " APPEND and PREPEND "

        /// <summary>
        /// Appends <paramref name="value">text</paramref> to the
        /// <see cref="TextBoxBase">control</see>.
        /// This setter is thread safe.
        /// </summary>
        /// <remarks> The value is set to empty if null or empty. </remarks>
        /// <param name="control"> The <see cref="TextBoxBase"/> control. </param>
        /// <param name="value">   The value. </param>
        /// <returns> value. </returns>
        public static string Append( this TextBoxBase control, string value )
        {
            if ( control is object )
            {
                if ( string.IsNullOrWhiteSpace( value ) )
                {
                    value = string.Empty;
                }
                else if ( control.InvokeRequired )
                {
                    _ = control.Invoke( new Func<TextBoxBase, string, string>( TextBoxExtensionMethods.Append ), new object[] { control, value } );
                }
                else
                {
                    control.SelectionStart = control.Text.Length;
                    control.SelectionLength = 0;
                    control.SelectedText = control.SelectionStart == 0 ? value : value;
                    control.SelectionStart = control.Text.Length;
                }
            }

            return value;
        }

        /// <summary>
        /// Appends formatted text of the <see cref="TextBoxBase">control</see>. This setter is thread
        /// safe.
        /// </summary>
        /// <remarks> The value is set to empty if null or empty. </remarks>
        /// <param name="control"> The <see cref="TextBoxBase"/> control. </param>
        /// <param name="format">  The text format. </param>
        /// <param name="args">    The format arguments. </param>
        /// <returns> value. </returns>
        public static string Append( this TextBoxBase control, string format, params object[] args )
        {
            return control.Append( string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
        }

        /// <summary>
        /// Prepends <paramref name="value">text</paramref> to the
        /// <see cref="TextBoxBase">control</see>.
        /// This setter is thread safe.
        /// </summary>
        /// <remarks> The value is set to empty if null or empty. </remarks>
        /// <param name="control"> The <see cref="TextBoxBase"/> control. </param>
        /// <param name="value">   The value. </param>
        /// <returns> value. </returns>
        public static string Prepend( this TextBoxBase control, string value )
        {
            if ( control is object )
            {
                if ( string.IsNullOrWhiteSpace( value ) )
                {
                    value = string.Empty;
                }
                else if ( control.InvokeRequired )
                {
                    _ = control.Invoke( new Func<TextBoxBase, string, string>( TextBoxExtensionMethods.Prepend ), new object[] { control, value } );
                }
                else
                {
                    control.SelectionStart = 0;
                    control.SelectionLength = 0;
                    control.SelectedText = value;
                    control.SelectionLength = 0;
                }
            }

            return value;
        }

        /// <summary> Prepends the formatted text. This setter is thread safe. </summary>
        /// <remarks> The value is set to empty if null or empty. </remarks>
        /// <param name="control"> The <see cref="TextBoxBase"/> control. </param>
        /// <param name="format">  The text format. </param>
        /// <param name="args">    The format arguments. </param>
        /// <returns> value. </returns>
        public static string Prepend( this TextBoxBase control, string format, params object[] args )
        {
            return control.Prepend( string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
        }

        #endregion

        #region " SPECIAL CHARACTERS "

        /// <summary> Insert special characters based on the provided keystrokes. </summary>
        /// <param name="textBox"> The control. </param>
        /// <param name="e">  Key event information. </param>
        public static void InsertSpecialCharacters( this TextBox textBox, KeyEventArgs e )
        {
            string specialText = e.ObtainSpecialCharacters();
            // insert special character if one was given
            if ( !string.IsNullOrEmpty( specialText ) )
            {
                textBox.SelectedText = specialText;
                e.SuppressKeyPress = true;
            }
        }

        /// <summary> Obtain special characters based on the provided keystrokes. </summary>
        /// <param name="e">       Key event information. </param>
        /// <returns> A String. </returns>
        /// <remakrs>
        /// <list type="bullet">Special Character codes<item>
        /// [Ctrl] + [Alt] + [-]: em dash ("—") </item><item>
        /// [Ctrl] + [-]: ChrW(173) ¡ optional hyphen (display only when breaking line)</item><item>
        /// [Ctrl] + [Shift] + [~]: ChrW(8220) left double-quote</item><item>
        /// [Ctrl] + [Shift] + [~]: ChrW(8216) left single-quote</item><item>
        /// [Ctrl] + [Shift] + ["]: ChrW(8221) right double-quote</item><item>
        /// [Ctrl] + [']: : ChrW(8217) right single-quote</item><item>
        /// [Ctrl] + [Alt] + [R]: registered trademark ("®")</item><item>
        /// [Ctrl] + [Alt] + [C]: copyright ("©")</item><item>
        /// [Ctrl] + [Alt] + [T]: trademark ("™")</item></list>
        /// </remakrs>
        public static string ObtainSpecialCharacters( this KeyEventArgs e )
        {
            string specialText = string.Empty;
            // [Ctrl] not pressed
            if ( e is null || !e.Control )
            {
                return specialText;
            }

            switch ( e.KeyCode )
            {
                case Keys.OemMinus:
                case Keys.Subtract:
                    {
                        // hyphens/dashes
                        if ( e.Alt )
                        {
                            // em dash ("—")
                            // -- [Ctrl] + [Alt] + [-]
                            specialText = "—";
                        }
                        else
                        {
                            // optional hyphen (display only when breaking line)
                            // -- [Ctrl] + [-]
                            specialText = Conversions.ToString( '­' );
                        }

                        break;
                    }

                case Keys.Oemtilde:
                    {
                        // left quotes
                        if ( e.Shift )
                        {
                            // left double-quote
                            // -- [Ctrl] + [Shift] + [~]
                            specialText = Conversions.ToString( '“' );
                        }
                        else
                        {
                            // left single-quote
                            // -- [Ctrl] + [`]
                            specialText = Conversions.ToString( '‘' );
                        }

                        break;
                    }

                case Keys.OemQuotes:
                    {
                        // right quotes
                        if ( e.Shift )
                        {
                            // right double-quote
                            // -- [Ctrl] + [Shift] + ["]
                            specialText = Conversions.ToString( '”' );
                        }
                        else
                        {
                            // right single-quote
                            // -- [Ctrl] + [']
                            specialText = Conversions.ToString( '’' );
                        }

                        break;
                    }

                case Keys.C:
                    {
                        // copyright ("©")
                        // -- [Ctrl] + [Alt] + [C]
                        if ( e.Alt )
                        {
                            specialText = "©";
                        }

                        break;
                    }

                case Keys.R:
                    {
                        // registered trademark ("®")
                        // -- [Ctrl] + [Alt] + [R]
                        if ( e.Alt )
                        {
                            specialText = "®";
                        }

                        break;
                    }

                case Keys.T:
                    {
                        // trademark ("™")
                        // -- [Ctrl] + [Alt] + [T]
                        if ( e.Alt )
                        {
                            specialText = "™";
                        }

                        break;
                    }
            }

            return specialText;
        }

        #endregion

    }
}
