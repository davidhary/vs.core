using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace isr.Core.WinForms.ComboBoxExtensions
{
    /// <summary> Includes extensions for <see cref="System.Windows.Forms.ComboBox">Combo Box</see>. </summary>
    /// <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2010-11-19, 1.2.3975 </para></remarks>
    public static class ComboBoxExtensionMethods
    {

        #region " TEXT SETTER "

        /// <summary>
        /// Sets the <see cref="System.Windows.Forms.ComboBox">control</see> text to the
        /// <paramref name="value">value</paramref>.
        /// The control is disabled when set so that the handling of the changed event can be skipped.
        /// </summary>
        /// <remarks> The value is set to empty if null or empty. </remarks>
        /// <param name="control"> The combo box control. </param>
        /// <param name="value">   The value. </param>
        /// <returns> value. </returns>
        public static string SilentTextSetter( this ComboBox control, string value )
        {
            if ( control is object )
            {
                if ( string.IsNullOrWhiteSpace( value ) )
                {
                    value = string.Empty;
                }

                bool enabled = control.Enabled;
                control.Enabled = false;
                control.Text = value;
                control.Enabled = enabled;
            }

            return value;
        }

        #endregion

        #region " SEARCH AND SELECT "

        /// <summary> Searches the combo box and selects a located item. </summary>
        /// <remarks> Use this method to search and select combo box index. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="combo"> Specifies an instance of a ComboBox. </param>
        /// <param name="e">     Specifies an instance of the
        /// <see cref="System.Windows.Forms.KeyPressEventArgs">event arguments</see>,
        /// which specify the key pressed. </param>
        /// <returns> The found and select. </returns>
        public static int SearchAndSelect( this ComboBox combo, KeyPressEventArgs e )
        {
            if ( combo is null )
            {
                throw new ArgumentNullException( nameof( combo ) );
            }

            if ( e is object && char.IsControl( e.KeyChar ) )
            {
                return combo.SelectedIndex;
            }
            else
            {
                int cursorPosition = combo.SelectionStart;
                int selectionLength = combo.SelectionLength;
                int itemNumber = combo.FindString( combo.Text );
                if ( itemNumber >= 0 )
                {
                    // if we have a match, select the current item and reposition the cursor
                    combo.SelectedIndex = itemNumber;
                    combo.SelectionStart = cursorPosition;
                    combo.SelectionLength = selectionLength;
                    return itemNumber;
                }
                else
                {
                    return combo.SelectedIndex;
                }
            }
        }

        /// <summary> Searches the combo box and selects a located item upon releasing a key. </summary>
        /// <remarks> Use this method to search and select combo box index. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="combo"> Specifies an instance of a ComboBox. </param>
        /// <param name="e">     Specifies an instance of the
        /// <see cref="System.Windows.Forms.KeyEventArgs">event arguments</see>,
        /// which specify the key released. </param>
        /// <returns> The found and select. </returns>
        public static int SearchAndSelect( this ComboBox combo, KeyEventArgs e )
        {
            if ( combo is null )
            {
                throw new ArgumentNullException( nameof( combo ) );
            }

            if ( e is null )
            {
                throw new ArgumentNullException( nameof( e ) );
            }

            if ( e.KeyCode.ToString().Length > 1 )
            {
                // if a control character, skip
                return combo.SelectedIndex;
            }
            else
            {
                int cursorPosition = combo.SelectionStart;
                int selectionLength = combo.SelectionLength;
                int itemNumber = combo.FindString( combo.Text );
                if ( itemNumber >= 0 )
                {
                    // if we have a match, select the current item and reposition the cursor
                    combo.SelectedIndex = itemNumber;
                    combo.SelectionStart = cursorPosition;
                    combo.SelectionLength = selectionLength;
                    return itemNumber;
                }
                else
                {
                    return combo.SelectedIndex;
                }
            }
        }

        #endregion

        #region " SELECT ITEM "

        /// <summary> Determines whether the specified control contains display value. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="control">      The control. </param>
        /// <param name="displayValue"> The display value. </param>
        /// <returns>
        /// <c>True</c> if the specified control contains display value; otherwise, <c>False</c>.
        /// </returns>
        public static bool ContainsDisplayValue( this ComboBox control, string displayValue )
        {
            return control is null
                ? throw new ArgumentNullException( nameof( control ) )
                : displayValue is null ? throw new ArgumentNullException( nameof( displayValue ) ) : 0 <= control.FindStringExact( displayValue );
        }

        /// <summary>
        /// Gets the selected or default <paramref name="defaultValue">key value</paramref>.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control">      The control. </param>
        /// <param name="defaultValue"> The default value. </param>
        /// <returns> ``0. </returns>
        public static T GetSelectedItemKey<T>( this ComboBox control, T defaultValue )
        {
            if ( control is null || control.SelectedValue is null )
            {
                return defaultValue;
            }
            else
            {
                KeyValuePair<T, string> v = ( KeyValuePair<T, string> ) control.SelectedItem;
                return v.Key;
            }
        }

        /// <summary> Selects an item. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control">      The control. </param>
        /// <param name="displayValue"> The display value. </param>
        public static void SelectItem( this ComboBox control, string displayValue )
        {
            if ( control is object )
            {
                if ( control.SelectedValue is null || string.IsNullOrWhiteSpace( control.SelectedText ) || !control.Text.Equals( displayValue ) )
                {
                    if ( string.IsNullOrWhiteSpace( displayValue ) )
                    {
                        if ( control.SelectedIndex != -1 )
                        {
                            control.SelectedIndex = -1;
                        }
                    }
                    else
                    {
                        int i = control.FindStringExact( displayValue );
                        if ( control.SelectedIndex != i )
                        {
                            control.SelectedIndex = i;
                            if ( control.SelectedIndex == -1 && control.DropDownStyle == ComboBoxStyle.DropDown )
                            {
                                control.Text = displayValue;
                            }
                        }
                    }
                }
            }
        }

        /// <summary> Selects an item in a 'Silent' (control is disabled) way. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control">      The control. </param>
        /// <param name="displayValue"> The display value. </param>
        public static void SilentSelectItem( this ComboBox control, string displayValue )
        {
            if ( control is object )
            {
                if ( control.SelectedValue is null || string.IsNullOrWhiteSpace( control.SelectedText ) || !control.Text.Equals( displayValue ) )
                {
                    bool enabled = control.Enabled;
                    control.Enabled = false;
                    control.SelectItem( displayValue );
                    control.Enabled = enabled;
                }
            }
        }

        /// <summary>
        /// Selects the <see cref="System.Windows.Forms.ComboBox">combo box</see> item by setting the
        /// selected item to the <see cref="System.Collections.Generic.KeyValuePair{TKey, TValue}">key value
        /// pair</see>. Thhis setter disables the control before altering the checked state allowing the
        /// control code to use the enabled state for preventing the execution of the control checked
        /// change actions.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> Combo box control. </param>
        /// <param name="value">   The selected item value. </param>
        /// <returns> value. </returns>
        public static object SilentSelectItem( this ComboBox control, object value )
        {
            if ( control is object )
            {
                bool wasEnabled = control.Enabled;
                control.Enabled = false;
                control.SelectedItem = value;
                control.Enabled = wasEnabled;
            }

            return value;
        }

        /// <summary>
        /// Selects the <see Cref="System.Windows.Forms.ComboBox">combo box</see> item by setting the
        /// selected item to the <see cref="System.Collections.Generic.KeyValuePair{TKey, TValue}">key value
        /// pair</see>. This setter disables the control before altering the checked state allowing the
        /// control code to use the enabled state for preventing the execution of the control checked
        /// change actions.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> Combo box control. </param>
        /// <param name="key">     The selected item key. </param>
        /// <param name="value">   The selected item value. </param>
        /// <returns> value. </returns>
        public static object SilentSelectItem( this ComboBox control, Enum key, string value )
        {
            return control.SilentSelectItem( new KeyValuePair<Enum, string>( key, value ) );
        }

        #endregion

        #region " SELECT Value "

        /// <summary>
        /// Selects the <see cref="System.Windows.Forms.ComboBox">combo box</see> Value by setting the
        /// selected Value to the <see cref="System.Collections.Generic.KeyValuePair{TKey, TValue}">key value
        /// pair</see>. This setter disables the control before altering the checked state allowing the
        /// control code to use the enabled state for preventing the execution of the control checked
        /// change actions.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> Combo box control. </param>
        /// <param name="value">   The selected Value. </param>
        /// <returns> value. </returns>
        public static object SilentSelectValue( this ComboBox control, object value )
        {
            if ( control is object )
            {
                bool wasEnabled = control.Enabled;
                control.Enabled = false;
                control.SelectedValue = value;
                control.Enabled = wasEnabled;
            }

            return value;
        }

        #endregion

    }
}
