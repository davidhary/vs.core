using System;
using System.Drawing;
using System.Windows.Forms;

namespace isr.Core.WinForms.ControlImageExtensions
{
    /// <summary> Includes extensions for <see cref="Control"/> and <see cref="Image"/> within this control. </summary>
    /// <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2010-11-19, 1.2.3975 </para></remarks>
    public static class ControlImageExtensionMethods
    {

        /// <summary>
        /// Gets image size for locating the image within the specified <para>Control</para>.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> The control. </param>
        /// <param name="image">   The image. </param>
        /// <returns> The image size. </returns>
        public static Size GetImageSize( this Control control, Image image )
        {
            if ( image is null || control is null )
            {
                return Size.Empty;
            }

            int w = Math.Min( control.Width, image.Width + 1 );
            int h = Math.Min( control.Height, image.Height + 1 );
            return new Size( w, h );
        }

        /// <summary>
        /// Gets image location for position the image within the specified <para>Control</para>.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control">    The control. </param>
        /// <param name="image">      The image. </param>
        /// <param name="imageAlign"> The image align. </param>
        /// <returns> The image location. </returns>
        public static Point GetImageLocation( this Control control, Image image, ContentAlignment imageAlign )
        {
            if ( image is null || control is null )
            {
                return Point.Empty;
            }

            var size = control.GetImageSize( image );

            // get X
            int x;
            switch ( imageAlign )
            {
                case ContentAlignment.TopLeft:
                case ContentAlignment.MiddleLeft:
                case ContentAlignment.BottomLeft:
                    {
                        x = 0;
                        break;
                    }

                case ContentAlignment.TopCenter:
                case ContentAlignment.MiddleCenter:
                case ContentAlignment.BottomCenter:
                    {
                        x = (control.Width - size.Width) / 2;
                        if ( (control.Width - size.Width) % 2 > 0 )
                        {
                            x += 1;
                        }

                        break;
                    }

                default:
                    {
                        x = control.Width - size.Width;
                        break;
                    }
            }

            if ( x > 0 )
            {
                x -= 1;
            }

            // get y
            int y;
            switch ( imageAlign )
            {
                case ContentAlignment.TopLeft:
                case ContentAlignment.TopCenter:
                case ContentAlignment.TopRight:
                    {
                        y = 0;
                        break;
                    }

                case ContentAlignment.MiddleLeft:
                case ContentAlignment.MiddleCenter:
                case ContentAlignment.MiddleRight:
                    {
                        y = (control.Height - size.Height) / 2;
                        if ( (control.Height - size.Height) % 2 > 0 )
                        {
                            y += 1;
                        }

                        break;
                    }

                default:
                    {
                        y = control.Height - size.Height;
                        break;
                    }
            }

            if ( y > 0 )
            {
                y -= 1;
            }

            return new Point( x, y );
        }

        /// <summary> Gets image rectangle within the specified <para>Control</para>. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control">    The control. </param>
        /// <param name="image">      The image. </param>
        /// <param name="imageAlign"> The image align. </param>
        /// <param name="fillHeight"> if set to <c>True</c> [fill height]. </param>
        /// <returns> The image rectangle. </returns>
        public static Rectangle GetImageRect( this Control control, Image image, ContentAlignment imageAlign, bool fillHeight )
        {
            if ( image is null || control is null )
            {
                return Rectangle.Empty;
            }

            var size = control.GetImageSize( image );
            var location = control.GetImageLocation( image, imageAlign );
            return fillHeight ? new Rectangle( location.X, location.Y, size.Width, control.Height ) : new Rectangle( location.X, location.Y, size.Width, size.Height );
        }
    }
}
