using System.Windows.Forms;

namespace isr.Core.WinForms.RadioButtonExtensions
{
    /// <summary> Includes extensions for <see cref="System.Windows.Forms.RadioButton">Radio Button</see>. </summary>
    /// <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2010-11-19, 1.2.3975 </para></remarks>
    public static class RadioButtonExtensionMethods
    {

        /// <summary>
        /// Sets the <see cref="System.Windows.Forms.RadioButton">radio button</see> checked value to the
        /// <paramref name="value">value</paramref>.
        /// This setter disables the control before altering the checked state allowing the control code
        /// to use the enabled state for preventing the execution of the control checked change actions.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> Check box control. </param>
        /// <param name="value">   The value. </param>
        /// <returns> value. </returns>
        public static bool SilentCheckedSetter( this RadioButton control, bool value )
        {
            if ( control is object )
            {
                bool wasEnabled = control.Enabled;
                control.Enabled = false;
                control.Checked = value;
                control.Enabled = wasEnabled;
            }

            return value;
        }
    }
}
