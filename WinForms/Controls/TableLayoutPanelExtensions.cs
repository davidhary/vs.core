﻿using System.Windows.Forms;

namespace isr.Core.WinForms.TableLayoutPanelExtensions
{

    /// <summary> Includes extensions for <see cref="System.Windows.Forms.TableLayoutPanel">Table Layout Panel</see> control. </summary>
    /// <remarks> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-02-21, 2.1.5165 </para></remarks>
    public static class Extensions
    {

        /// <summary> Adds a control to the layout if not contained. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="layout">  The layout. </param>
        /// <param name="control"> The control. </param>
        /// <param name="column">  The column. </param>
        /// <param name="row">     The row. </param>
        public static void AddIfNotContained( this TableLayoutPanel layout, Control control, int column, int row )
        {
            if ( layout is object && !layout.Controls.Contains( control ) )
            {
                layout.Controls.Add( control, column, row );
            }
        }

        /// <summary> Removes the control. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="layout">  The layout. </param>
        /// <param name="control"> The control. </param>
        public static void RemoveIfContained( this TableLayoutPanel layout, Control control )
        {
            if ( layout is object && layout.Controls is object && layout.Controls.Contains( control ) )
            {
                layout.Controls.Remove( control );
            }
        }
    }
}