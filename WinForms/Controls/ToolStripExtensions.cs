using System;
using System.Windows.Forms;

namespace isr.Core.WinForms.ToolStripExtensions
{
    /// <summary> Includes extensions for <see cref="ToolStrip">Tool Strip</see>. </summary>
    /// <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2010-11-19, 1.2.3975 </para></remarks>
    public static class ToolStripExtensionMethods
    {

        #region " TOOL STRIP ITEM "

        /// <summary>
        /// Sets the <see cref=" ToolStripMenuItem">Control</see> checked value to the The setter
        /// disables the control before altering the checked state allowing the control code to use the
        /// enabled state for preventing the execution of the control checked change actions.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> Check box control. </param>
        /// <param name="value">   The value. </param>
        /// <returns> value. </returns>
        public static CheckState SilentCheckStateSetter( this ToolStripMenuItem control, CheckState value )
        {
            if ( control is object )
            {
                bool wasEnabled = control.Enabled;
                control.Enabled = false;
                control.CheckState = value;
                control.Enabled = wasEnabled;
            }

            return value;
        }

        /// <summary>
        /// Sets the <see cref=" ToolStripMenuItem">Control</see> checked value to the The setter
        /// disables the control before altering the checked state allowing the control code to use the
        /// enabled state for preventing the execution of the control checked change actions.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> Check box control. </param>
        /// <param name="value">   The value. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool SilentCheckedSetter( this ToolStripMenuItem control, bool value )
        {
            if ( control is object )
            {
                bool wasEnabled = control.Enabled;
                control.Enabled = false;
                control.Checked = value;
                control.Enabled = wasEnabled;
            }

            return value;
        }

        #endregion

        #region " TOOL STRIP DROP DOWN ITEM "

        /// <summary> Maximum drop down item width. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="control"> The tool strip progress bar control. </param>
        /// <returns> An Integer. </returns>
        public static int MaxDropDownItemWidth( this ToolStripDropDownItem control )
        {
            return control is null ? throw new ArgumentNullException( nameof( control ) ) : control.DropDownItems.MaxItemWidth();
        }

        #endregion

        #region " TOOL STRIP ITEM COLLECTION "

        /// <summary> Maximum item width. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="items"> The items. </param>
        /// <returns> An Integer. </returns>
        public static int MaxItemWidth( this ToolStripItemCollection items )
        {
            int width = 0;
            if ( items is object )
            {
                foreach ( ToolStripItem item in items )
                {
                    width = width < item.Width ? item.Width : width;
                }
            }

            return width;
        }

        #endregion

        #region " TOOL STRIP PROGRESS BAR "

        /// <summary>
        /// Sets the <see cref="ProgressBar">control</see> value to the
        /// <paramref name="value">value</paramref> limited by the control range.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> The tool strip progress bar control. </param>
        /// <param name="value">   The candidate value. </param>
        /// <returns> The value limited within the range of the progress bar. </returns>
        public static int ValueSetter( this ToolStripProgressBar control, int value )
        {
            if ( control is object )
            {
                value = value < control.Minimum ? control.Minimum : value > control.Maximum ? control.Maximum : value;
                if ( control.Value != value )
                {
                    control.Value = value;
                }

                return control.Value;
            }

            return value;
        }

        /// <summary>
        /// Sets the <see cref="ProgressBar">
        /// control</see>
        /// value to the.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> The tool strip progress bar control. </param>
        /// <param name="value">   The candidate value. </param>
        /// <returns> The value limited within the range of the progress bar. </returns>
        public static int ValueSetter( this ToolStripProgressBar control, double value )
        {
            return control.ValueSetter( ( int ) value );
        }

        #endregion

    }
}
