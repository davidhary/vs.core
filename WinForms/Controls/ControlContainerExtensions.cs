using System.Windows.Forms;

namespace isr.Core.WinForms.ControlContainerExtensions
{
    /// <summary> Includes extensions for <see cref="Control"/>  and <see cref="ContainerControl">container control</see>. </summary>
    /// <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2010-11-19, 1.2.3975 </para></remarks>
    public static class ControlContainerExtensionMethods
    {

        /// <summary> Validates the specified control. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="value">     Control to be validated. </param>
        /// <param name="container"> specifies the instance of the container control which controls are to
        /// be validated. </param>
        /// <returns>
        /// Returns false if failed to validate. Returns true if control validated or not visible.
        /// </returns>
        public static bool Validate( this Control value, ContainerControl container )
        {
            if ( value is null || container is null )
            {
                return true;
            }
            // focus on and validate the control
            if ( container.Visible && value.Visible )
            {
                _ = value.Focus();
                return container.Validate();
            }
            else
            {
                return true;
            }
        }

        /// <summary> Validates all controls in the container control. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="container"> specifies the instance of the container control which controls are to
        /// be validated. </param>
        /// <returns> Returns false if any control failed to validated. </returns>
        public static bool ValidateControls( this ContainerControl container )
        {
            if ( container is null )
            {
                return true;
            }

            bool validateControlsRet = true;
            var controls = ControlCollectionExtensions.ControlCollectionExtensionMethods.RetrieveControls( container.Controls );
            for ( int i = 0, loopTo = controls.Length - 1; i <= loopTo; i++ )
            {
                // validate the selected control
                if ( !controls[i].Validate( container ) )
                {
                    validateControlsRet = false;
                }
            }

            return validateControlsRet;
        }
    }
}
