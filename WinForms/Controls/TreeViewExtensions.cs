using System.Linq;
using System.Windows.Forms;

namespace isr.Core.WinForms.TreeViewExtensions
{
    /// <summary> Includes extensions for <see cref="System.Windows.Forms.TreeView">Tree View</see> control. </summary>
    /// <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2010-11-19, 1.2.3975 </para></remarks>
    public static class TreeViewExtensionMethods
    {

        #region " TREE VIEW "

        /// <summary> Selects a node. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control">  The <see cref="TreeView"/> control. </param>
        /// <param name="nodeName"> Name of the node. </param>
        public static void SelectNode( this TreeView control, string nodeName )
        {
            if ( control is object )
            {
                control.SelectedNode = control.Nodes[nodeName];
            }
        }

        /// <summary> Selects a node. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control">       The <see cref="TreeView"/> control. </param>
        /// <param name="nodeHierarchy"> The hierarchy of node names. </param>
        public static void SelectNode( this TreeView control, string[] nodeHierarchy )
        {
            if ( control is object && nodeHierarchy is object && nodeHierarchy.Any() )
            {
                TreeNode parentTreeNode = null;
                TreeNode childTreeNode;
                foreach ( string nodeName in nodeHierarchy )
                {
                    if ( parentTreeNode is null )
                    {
                        parentTreeNode = control.Nodes[nodeName];
                        childTreeNode = parentTreeNode;
                    }
                    else
                    {
                        childTreeNode = parentTreeNode.Nodes.Count > 0 ? parentTreeNode.Nodes[nodeName] : null;
                    }

                    if ( childTreeNode is null )
                    {
                        break;
                    }
                    else
                    {
                        parentTreeNode = childTreeNode;
                    }
                }

                control.SelectedNode = parentTreeNode;
            }
        }

        #endregion

    }
}
