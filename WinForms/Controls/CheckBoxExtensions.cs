using System;
using System.Windows.Forms;

namespace isr.Core.WinForms.CheckBoxExtensions
{
    /// <summary> Includes extensions for <see cref="CheckBox">check box</see>. </summary>
    /// <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2010-11-19, 1.2.3975 </para></remarks>
    public static class CheckBoxExtensionMethods
    {

        /// <summary>
        /// Sets the <see cref="Control">check box</see> checked value to the
        /// <paramref name="value">value</paramref>.
        /// This setter disables the control before altering the checked state allowing the control code
        /// to use the enabled state for preventing the execution of the control checked change actions.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="control"> Check box control. </param>
        /// <param name="value">   The value. </param>
        /// <returns> value. </returns>
        public static CheckState SilentCheckStateSetter( this CheckBox control, CheckState value )
        {
            if ( control is object )
            {
                if ( control.ThreeState )
                {
                    bool wasEnabled = control.Enabled;
                    control.Enabled = false;
                    control.CheckState = value;
                    control.Enabled = wasEnabled;
                }
                else
                {
                    throw new InvalidOperationException( "Attempted to set a two-state control with a three-state value." );
                }
            }

            return value;
        }

        /// <summary>
        /// Sets the <see cref="Control">check box</see> check state value to the
        /// <paramref name="value">value</paramref>.
        /// This setter disables the control before altering the checked state allowing the control code
        /// to use the enabled state for preventing the execution of the control checked change actions.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> Check box control. </param>
        /// <param name="value">   The value. </param>
        /// <returns> value. </returns>
        public static bool SilentCheckedSetter( this CheckBox control, bool value )
        {
            if ( control is object )
            {
                bool wasEnabled = control.Enabled;
                control.Enabled = false;
                control.Checked = value;
                control.Enabled = wasEnabled;
            }

            return value;
        }

        /// <summary> Converts a value to a check state. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="value"> The value to convert to check state. </param>
        /// <returns>
        /// <see cref="CheckState.Indeterminate">Indeterminate</see> if nothing, otherwise, checked or
        /// unchecked.
        /// </returns>
        public static CheckState ToCheckState( this bool? value )
        {
            return !value.HasValue ? CheckState.Indeterminate : value.Value ? CheckState.Checked : CheckState.Unchecked;
        }

        /// <summary> Initializes this object from the given check state. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="value"> The value to set. </param>
        /// <returns> A Boolean? </returns>
        public static bool? FromCheckState( this CheckState value )
        {
            return value == CheckState.Checked || value == CheckState.Unchecked && new bool();
        }
    }
}
