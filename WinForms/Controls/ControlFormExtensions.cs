using System.Windows.Forms;

namespace isr.Core.WinForms.ControlFormExtensions
{
    /// <summary> Includes extensions for <see cref="Control"/> and <see cref="Form"/>. </summary>
    /// <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2010-11-19, 1.2.3975 </para></remarks>
    public static class ControlFormExtensionMethods
    {

        /// <summary> Returns the control requesting help based on the mouse position clicked. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="container"> Specifies the container control requesting the help. </param>
        /// <param name="helpEvent"> Help event information. </param>
        /// <returns>
        /// Returns the control upon which the operator clicked with the help icon or nothing if no
        /// control was clicked.
        /// </returns>
        public static Control ControlRequestingHelp( this Control container, HelpEventArgs helpEvent )
        {
            if ( helpEvent is null || container is null )
            {
                return null;
            }

            // Convert screen coordinates to client coordinates
            var clientCoordinatePoint = container.PointToClient( helpEvent.MousePos );

            // look for the control upon which the operator clicked.
            for ( int i = 0, loopTo = container.Controls.Count; i <= loopTo; i++ )
            {
                if ( container.Controls[i].Bounds.Contains( clientCoordinatePoint ) )
                {
                    return container.Controls[i];
                }
            }

            // if no control was located, return nothing
            return null;
        }
    }
}
