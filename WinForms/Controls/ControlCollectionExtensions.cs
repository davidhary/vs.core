using System;
using System.Collections;
using System.Windows.Forms;

namespace isr.Core.WinForms.ControlCollectionExtensions
{
    /// <summary> Includes extensions for <see cref="Control.ControlCollection">Control Collection</see>. </summary>
    /// <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2010-11-19, 1.2.3975 </para></remarks>
    public static class ControlCollectionExtensionMethods
    {

        /// <summary> Adds a control to the control collection if not contained. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="controls"> The collection of controls. </param>
        /// <param name="control">  The control. </param>
        public static void AddIfNotContained( this Control.ControlCollection controls, Control control )
        {
            if ( controls is object && !controls.Contains( control ) )
            {
                controls.Add( control );
            }
        }

        /// <summary> Removes a control if it is contained in the control collections. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="controls"> The collection of controls. </param>
        /// <param name="control">  The control to remove. </param>
        public static void RemoveIfContained( this Control.ControlCollection controls, Control control )
        {
            if ( controls is object && controls.Contains( control ) )
            {
                controls.Remove( control );
            }
        }

        /// <summary> Retrieve all controls and child controls. </summary>
        /// <remarks>
        /// Make sure to send control back at lowest depth first so that most child controls are checked
        /// for things before container controls, e.g., a TextBox is checked before a GroupBox control.
        /// </remarks>
        /// <param name="controls"> . </param>
        /// <returns> Returns an array of controls. </returns>
        public static Control[] RetrieveControls( this Control.ControlCollection controls )
        {
            if ( controls is null )
            {
                return null;
            }

            var controlList = new ArrayList();

            // Dim allControls As ArrayList = New ArrayList
            var myQueue = new Queue();
            // add controls to the queue
            myQueue.Enqueue( controls );
            Control.ControlCollection myControls;
            // Dim current As Object
            while ( myQueue.Count > 0 )
            {
                // remove and return the object at the beginning of the queue
                // current = myQueue.Dequeue()
                if ( myQueue.Peek() is Control.ControlCollection )
                {
                    myControls = ( Control.ControlCollection ) myQueue.Dequeue();
                    if ( myControls.Count > 0 )
                    {
                        for ( int i = 0, loopTo = myControls.Count - 1; i <= loopTo; i++ )
                        {
                            var myControl = myControls[i];
                            // add this control to the array list
                            _ = controlList.Add( myControl );
                            // check if this control has a collection
                            if ( myControl.Controls is object )
                            {
                                // if so, add to the queue
                                myQueue.Enqueue( myControl.Controls );
                            }
                        }
                        // For Each myControl As System.Windows.Forms.Control In myControls
                        // add this control to the array list
                        // controlList.Add(myControl)
                        // check if this control has a collection
                        // If Not IsNothing(myControl.Controls) Then
                        // if so, add to the queue
                        // myQueue.Enqueue(myControl.Controls)
                        // End If
                        // Next
                    }
                }
                else
                {
                    _ = myQueue.Dequeue();
                }
            }

            if ( controlList.Count > 0 )
            {
                var allControls = new Control[controlList.Count];
                controlList.CopyTo( allControls );
                return allControls;
            }
            else
            {
                return Array.Empty<Control>();
            }
        }

        /// <summary>
        /// Sets the Tool tip for all form controls that inherit a <see cref="Control">control base.</see>
        /// </summary>
        /// <remarks>
        /// This is required because setting a tool tip from the parent form does not show the tool tip
        /// if hovering above children controls hosted by the user control.
        /// </remarks>
        /// <param name="parent">  Reference to the parent form or control. </param>
        /// <param name="toolTip"> The parent form or control tool tip. </param>
        public static void ToolTipSetter( this Control parent, ToolTip toolTip )
        {
            if ( parent is null )
            {
                return;
            }

            if ( toolTip is null )
            {
                return;
            }

            toolTip.SetToolTip( parent, toolTip.GetToolTip( parent ) );
            if ( parent.HasChildren )
            {
                parent.Controls.ToolTipSetter( toolTip );
            }
        }

        /// <summary>
        /// Sets the Tool tip for all form controls that inherit a <see cref="Control">control base.</see>
        /// </summary>
        /// <remarks>
        /// This is required because setting a tool tip from the parent form does not show the tool tip
        /// if hovering above children controls hosted by the user control.
        /// </remarks>
        /// <param name="controls"> The collection of controls. </param>
        /// <param name="toolTip">  The parent form or control tool tip. </param>
        public static void ToolTipSetter( this Control.ControlCollection controls, ToolTip toolTip )
        {
            if ( controls is null )
            {
                return;
            }

            if ( toolTip is null )
            {
                return;
            }

            foreach ( Control control in controls )
            {
                control.ToolTipSetter( toolTip );
            }
        }
    }
}
