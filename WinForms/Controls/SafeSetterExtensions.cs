using System;
using System.Windows.Forms;

namespace isr.Core.WinForms.ControlExtensions
{
    public static partial class ControlExtensionMethods
    {

        /// <summary>
        /// A <see cref="Control"/> extension method that safely invokes the specified
        /// <paramref name="actionMethod"/>.
        /// </summary>
        /// <remarks>   David, 2020-07-27. </remarks>
        /// <param name="control">      The control to enable or disable. </param>
        /// <param name="actionMethod"> The action method. </param>
        public static void SafelyInvoke( this Control control, Action actionMethod )
        {
            if ( control is object )
            {
                if ( control.InvokeRequired )
                {
                    _ = control.Invoke( new Action<Control, Action>( SafelyInvoke ), new object[] { control, actionMethod } );
                }
                else if ( control.IsHandleCreated )
                {
                    actionMethod.Invoke();
                }
            }
        }

        /// <summary>   A <see cref="Control"/> extension method that safe text writer. </summary>
        /// <remarks>   David, 2020-09-25. </remarks>
        /// <param name="control">  The control to enable or disable. </param>
        /// <param name="value">    The value. </param>
        public static void SafeTextWriter( this Control control, string value )
        {
            //                return ( string ) control.SafeSetter( new Func<String>( () => control.Text = value ) ); return value ;
            //
            control.SafelyInvoke( new Action( () => control.Text = value ) );
        }

        /// <summary>
        /// A <see cref="Control"/> extension method that safely invokes the specified
        /// <paramref name="functionMethod"/>.
        /// </summary>
        /// <remarks>   David, 2020-09-25. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="control">          The control to enable or disable. </param>
        /// <param name="functionMethod">   The function method. </param>
        /// <returns>   A <typeparamref name="T"/>. </returns>
        public static T SafelyInvoke<T>( this Control control, Func<T> functionMethod )
        {
#if false
            // left this code for debugging.
            if ( control.InvokeRequired )
            {
                return ( T ) control.Invoke( new Func<Control, Func<T>, T>( SafeSetter ), new object[] { control, setter } );
            }
            else
            {
                return ( T ) setter.Invoke();
            }
#else
            return !(control is object)
                ? throw new ArgumentNullException( nameof( control ) )
                : control.InvokeRequired
                ? ( T ) control.Invoke( new Func<Control, Func<T>, T>( SafelyInvoke ), new object[] { control, functionMethod } )
                : control.IsHandleCreated
                    ? ( T ) functionMethod.Invoke()
                    : throw new InvalidOperationException( $"{nameof( control )} handle was not created" );
#endif
        }

        /// <summary>   A <see cref="Control"/> extension method that safely sets a text value. </summary>
        /// <remarks>   David, 2020-09-25. </remarks>
        /// <param name="control">  The control to enable or disable. </param>
        /// <param name="value">    The value. </param>
        /// <returns>   A String. </returns>
        public static String SafeTextSetter( this Control control, string value )
        {
            return ( string ) control.SafelyInvoke( new Func<String>( () => { control.Text = value; return control.Text; } ) );
        }

        /// <summary>   A <see cref="StatusBarPanel"/> extension method that safely invokes the specified <paramref name="actionMethod"/> </summary>
        /// <remarks>   David, 2020-07-27. </remarks>
        /// <param name="control">      The control to enable or disable. </param>
        /// <param name="actionMethod"> The action method. </param>
        public static void SafelyInvoke( this StatusBarPanel control, Action actionMethod )
        {
            if ( control is object && control.Parent is object )
            {
                if ( control.Parent.InvokeRequired )
                {
                    _ = control.Parent.Invoke( new Action<Control, Action>( SafelyInvoke ), new object[] { control, actionMethod } );
                }
                else if ( control.Parent.IsHandleCreated )
                {
                    actionMethod.Invoke();
                }
            }
        }

        /// <summary>
        /// A  <see cref="ToolBarButton"/> extension method that safely invokes the specified
        /// <paramref name="actionMethod"/>.
        /// </summary>
        /// <remarks>   David, 2020-07-27. </remarks>
        /// <param name="control">      The control to enable or disable. </param>
        /// <param name="actionMethod"> The action method. </param>
        public static void SafelyInvoke( this ToolBarButton control, Action actionMethod )
        {
            if ( control is object && control.Parent is object )
            {
                if ( control.Parent.InvokeRequired )
                {
                    _ = control.Parent.Invoke( new Action<Control, Action>( SafelyInvoke ), new object[] { control, actionMethod } );
                }
                else if ( control.Parent.IsHandleCreated )
                {
                    actionMethod.Invoke();
                }
            }
        }

        /// <summary>   A <see cref="ToolStripItem"/> extension method that safely invokes the specified <paramref name="actionMethod"/>. </summary>
        /// <remarks>   David, 2020-07-27. </remarks>
        /// <param name="control">      The control to enable or disable. </param>
        /// <param name="actionMethod"> . </param>
        public static void SafelyInvoke( this ToolStripItem control, Action actionMethod )
        {
            if ( control is object && control.Owner is object )
            {
                if ( control.Owner.InvokeRequired )
                {
                    _ = control.Owner.Invoke( new Action<Control, Action>( SafelyInvoke ), new object[] { control, actionMethod } );
                }
                else if ( control.Owner.IsHandleCreated )
                {
                    actionMethod.Invoke();
                }
            }
        }
    }
}
