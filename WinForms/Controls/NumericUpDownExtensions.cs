using System;
using System.Windows.Forms;

namespace isr.Core.WinForms.NumericUpDownExtensions
{
    /// <summary> Includes extensions for <see cref="NumericUpDown">Numeric Up Down</see> control. </summary>
    /// <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2010-11-19, 1.2.3975 </para></remarks>
    public static class NumericUpDownExtensionMethods
    {

        #region " RANGE "

        /// <summary> Range setter. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="control"> The numeric up down control. </param>
        /// <param name="minimum"> The minimum. </param>
        /// <param name="maximum"> The maximum. </param>
        /// <returns> A Tuple(Of Decimal, Decimal) </returns>
        public static Tuple<decimal, decimal> RangeSetter( this NumericUpDown control, decimal minimum, decimal maximum )
        {
            if ( control is null )
            {
                throw new ArgumentNullException( nameof( control ) );
            }

            if ( control.Value > maximum )
            {
                control.Value = maximum;
            }

            if ( control.Value < minimum )
            {
                control.Value = minimum;
            }

            control.Maximum = maximum;
            control.Minimum = minimum;
            return new Tuple<decimal, decimal>( control.Minimum, control.Maximum );
        }

        /// <summary> Range setter. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="control"> The numeric up down control. </param>
        /// <param name="minimum"> The minimum. </param>
        /// <param name="maximum"> The maximum. </param>
        /// <returns> A Tuple(Of Decimal, Decimal) </returns>
        public static Tuple<decimal, decimal> RangeSetter( this NumericUpDown control, double minimum, double maximum )
        {
            return control is null
                ? throw new ArgumentNullException( nameof( control ) )
                : control.RangeSetter( minimum < ( double ) decimal.MinValue ? decimal.MinValue : ( decimal ) minimum, maximum > ( double ) decimal.MaxValue ? decimal.MaxValue : ( decimal ) maximum );
        }

        #endregion

        #region " MAXIMUM "

        /// <summary>
        /// Sets the <see cref="NumericUpDown">control</see>
        /// <see cref=" NumericUpDown.Maximum">Maximum</see>.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> The numeric up down control. </param>
        /// <param name="value">   The candidate Maximum. </param>
        /// <returns> The limited Maximum. </returns>
        public static decimal MaximumSetter( this NumericUpDown control, decimal value )
        {
            if ( control is object )
            {
                if ( control.Value > value )
                {
                    control.Value = value;
                }

                control.Maximum = value;
            }

            return value;
        }

        /// <summary>
        /// Sets the <see cref="NumericUpDown">control</see>
        /// <see cref=" NumericUpDown.Maximum">Maximum</see>.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> The numeric up down control. </param>
        /// <param name="value">   The candidate Maximum. </param>
        /// <returns> A Decimal. </returns>
        public static decimal MaximumSetter( this NumericUpDown control, double value )
        {
            return control.MaximumSetter( value > ( double ) decimal.MaxValue ? decimal.MaxValue : ( decimal ) value );
        }

        #endregion

        #region " MINIMUM "

        /// <summary>
        /// Sets the <see cref="NumericUpDown">control</see>
        /// <see cref=" NumericUpDown.Minimum">Minimum</see>.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> The numeric up down control. </param>
        /// <param name="value">   The candidate Minimum. </param>
        /// <returns> The limited Minimum. </returns>
        public static decimal MinimumSetter( this NumericUpDown control, decimal value )
        {
            if ( control is object )
            {
                if ( control.Value < value )
                {
                    control.Value = value;
                }

                control.Minimum = value;
            }

            return value;
        }

        /// <summary>
        /// Sets the <see cref="NumericUpDown">control</see>
        /// <see cref=" NumericUpDown.Minimum">Minimum</see>.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> The numeric up down control. </param>
        /// <param name="value">   The candidate Maximum. </param>
        /// <returns> The limited Minimum. </returns>
        public static decimal MinimumSetter( this NumericUpDown control, double value )
        {
            return control.MinimumSetter( value < ( double ) decimal.MinValue ? decimal.MinValue : ( decimal ) value );
        }


        #endregion

        #region " VALUE "

        /// <summary>
        /// Sets the <see cref="NumericUpDown">control</see> value to the
        /// <paramref name="value">value</paramref> scaled by the
        /// <paramref name="scalar">scalar</paramref> and limited by the control range.
        /// Returns limited value (control value divided by the scalar).
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> The numeric up down control. </param>
        /// <param name="scalar">  The scalar. </param>
        /// <param name="value">   The candidate value. </param>
        /// <returns> The control value divided by the scalar. </returns>
        public static decimal ValueSetter( this NumericUpDown control, decimal scalar, decimal value )
        {
            return control.ValueSetter( scalar * value ) / scalar;
        }

        /// <summary>
        /// Sets the <see cref="NumericUpDown">control</see> value to the
        /// <paramref name="value">value</paramref> scaled by the
        /// <paramref name="scalar">scalar</paramref> and limited by the control range.
        /// Returns limited value (value within the control range divided by the scalar). The setter
        /// disables the control before altering the checked state allowing the control code to use the
        /// enabled state for preventing the execution of the control checked change actions.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> The numeric up down control. </param>
        /// <param name="scalar">  The scalar. </param>
        /// <param name="value">   The candidate value. </param>
        /// <returns> The control value divided by the scalar. </returns>
        public static decimal SilentValueSetter( this NumericUpDown control, decimal scalar, decimal value )
        {
            return control.SilentValueSetter( scalar * value ) / scalar;
        }

        /// <summary>
        /// Sets the <see cref="NumericUpDown">control</see> value to the
        /// <paramref name="value">value</paramref> limited by the control range.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> The numeric up down control. </param>
        /// <param name="value">   The candidate value. </param>
        /// <returns> The limited value. </returns>
        public static decimal ValueSetter( this NumericUpDown control, decimal value )
        {
            if ( control is object )
            {
                value = value < control.Minimum ? control.Minimum : value > control.Maximum ? control.Maximum : value;
                if ( value != control.Value )
                {
                    control.Value = value;
                }
            }

            return value;
        }

        /// <summary>
        /// Sets the <see cref="NumericUpDown">control</see> value to the
        /// <paramref name="value">value</paramref> limited by the control range.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> The numeric up down control. </param>
        /// <param name="value">   The candidate value. </param>
        /// <returns> The limited value. </returns>
        public static decimal? ValueSetter( this NumericUpDown control, decimal? value )
        {
            if ( control is object )
            {
                if ( value.HasValue )
                {
                    control.Value = value.Value < control.Minimum ? control.Minimum : value.Value > control.Maximum ? control.Maximum : value.Value;
                }
                else
                {
                    control.Text = string.Empty;
                }

                return control.Value;
            }
            else
            {
                return value;
            }
        }

        /// <summary>
        /// Sets the <see cref="NumericUpDown">control</see> value to the
        /// <paramref name="value">value</paramref> limited by the control range.
        /// This setter disables the control before altering the checked state allowing the control code
        /// to use the enabled state for preventing the execution of the control checked change actions.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> The numeric up down control. </param>
        /// <param name="value">   The candidate value. </param>
        /// <returns> The limited value. </returns>
        public static decimal SilentValueSetter( this NumericUpDown control, decimal value )
        {
            if ( control is object )
            {
                bool wasEnabled = control.Enabled;
                control.Enabled = false;
                _ = control.ValueSetter( value );
                control.Enabled = wasEnabled;
                return control.Value;
            }
            else
            {
                return value;
            }
        }

        /// <summary>
        /// Sets the <see cref="NumericUpDown">control</see> value to the
        /// <paramref name="value">value</paramref> limited by the control range.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> The numeric up down control. </param>
        /// <param name="value">   The candidate value. </param>
        /// <returns> The limited value. </returns>
        public static double ValueSetter( this NumericUpDown control, double value )
        {
            return ( double ) control.ValueSetter( ( decimal ) value );
        }

        /// <summary>
        /// Sets the <see cref="NumericUpDown">control</see> value to the
        /// <paramref name="value">value</paramref> limited by the control range.
        /// This setter disables the control before altering the checked state allowing the control code
        /// to use the enabled state for preventing the execution of the control checked change actions.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> The numeric up down control. </param>
        /// <param name="value">   The candidate value. </param>
        /// <returns> The limited value. </returns>
        public static decimal SilentValueSetter( this NumericUpDown control, double value )
        {
            if ( control is object )
            {
                bool wasEnabled = control.Enabled;
                control.Enabled = false;
                _ = control.ValueSetter( ( decimal ) value );
                control.Enabled = wasEnabled;
                return control.Value;
            }

            return ( decimal ) value;
        }

        #endregion

    }
}
