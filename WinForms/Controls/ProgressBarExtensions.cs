using System.Windows.Forms;

namespace isr.Core.WinForms.ProgressBarExtensions
{
    /// <summary> Includes extensions for <see cref="ProgressBar">Progress Bar</see>. </summary>
    /// <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2010-11-19, 1.2.3975 </para></remarks>
    public static class ProgressBarExtensionMethods
    {

        #region " DRAW TEXT "

        /// <summary>
        /// Draws text into a  <see cref="System.Windows.Forms.ProgressBar">progress bar</see>
        /// center middle using the control font.
        /// </summary>
        /// <remarks>
        /// Call this method after changing the progress bar's value. If for some reason, the changing of
        /// the progress bar's value doesn't refresh it and clear the previously drawn text, call the
        /// Refresh method of the progress bar before calling this method.
        /// </remarks>
        /// <param name="control"> The target progress bar to add text into. </param>
        /// <param name="value">   The text to add into the progress bar. Leave null or empty to
        /// automatically add the percent. </param>
        public static void DrawText( this ProgressBar control, string value )
        {
            if ( control is object )
            {
                control.DrawText( value, System.Drawing.ContentAlignment.MiddleCenter, control.Font );
            }
        }

        /// <summary>
        /// Draws text into a  <see cref="System.Windows.Forms.ProgressBar">progress bar</see>.
        /// </summary>
        /// <remarks>
        /// Call this method after changing the progress bar's value. If for some reason, the changing of
        /// the progress bar's value doesn't refresh it and clear the previously drawn text, call the
        /// Refresh method of the progress bar before calling this method.
        /// </remarks>
        /// <param name="control">   The target progress bar to add text into. </param>
        /// <param name="value">     The text to add into the progress bar. Leave null or empty to
        /// automatically add the percent. </param>
        /// <param name="textAlign"> Where the text is to be placed. </param>
        public static void DrawText( this ProgressBar control, string value, System.Drawing.ContentAlignment textAlign )
        {
            if ( control is object )
            {
                if ( string.IsNullOrWhiteSpace( value ) )
                {
                    double fraction = (control.Value - control.Minimum) / ( double ) (control.Maximum - control.Minimum);
                    value = $"{fraction:0%}";
                }

                ControlExtensions.ControlExtensionMethods.DrawText( control, value, textAlign, control.Font );
            }
        }

        /// <summary>
        /// Draws text into a  <see cref="System.Windows.Forms.ProgressBar">progress bar</see>.
        /// </summary>
        /// <remarks>
        /// Call this method after changing the progress bar's value. If for some reason, the changing of
        /// the progress bar's value doesn't refresh it and clear the previously drawn text, call the
        /// Refresh method of the progress bar before calling this method.
        /// </remarks>
        /// <param name="control">   The target progress bar to add text into. </param>
        /// <param name="value">     The text to add into the progress bar. Leave null or empty to
        /// automatically add the percent. </param>
        /// <param name="textAlign"> Where the text is to be placed. </param>
        /// <param name="textFont">  The font the text should be drawn in. </param>
        public static void DrawText( this ProgressBar control, string value, System.Drawing.ContentAlignment textAlign, System.Drawing.Font textFont )
        {
            if ( control is object )
            {
                if ( string.IsNullOrWhiteSpace( value ) )
                {
                    control.Refresh();
                    double fraction = (control.Value - control.Minimum) / ( double ) (control.Maximum - control.Minimum);
                    value = $"{fraction:0%}";
                }

                ControlExtensions.ControlExtensionMethods.DrawText( control, value, textAlign, textFont );
            }
        }

        #endregion

        #region " VALUE SETTER  "

        /// <summary>
        /// Sets the <see cref="ProgressBar">control</see> value to the
        /// <paramref name="value">value</paramref> limited by the control range.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> The progress bar control. </param>
        /// <param name="value">   The candidate value. </param>
        /// <returns> The limited value. </returns>
        public static int ValueSetter( this ProgressBar control, int value )
        {
            if ( control is object )
            {
                value = value < control.Minimum ? control.Minimum : value > control.Maximum ? control.Maximum : value;
                if ( control.Value != value )
                {
                    control.Value = value;
                }
            }

            return value;
        }

        /// <summary>
        /// Sets the <see cref="ProgressBar">control</see> value to the
        /// <paramref name="value">value</paramref> limited by the control range.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> The progress bar control. </param>
        /// <param name="value">   The candidate value. </param>
        /// <returns> The limited value. </returns>
        public static int ValueUpdater( this ProgressBar control, int value )
        {
            return control is object ? control.Value != value ? control.ValueSetter( value ) : control.Value : value;
        }
        #endregion

    }
}
