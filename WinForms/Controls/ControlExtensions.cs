using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace isr.Core.WinForms.ControlExtensions
{
    /// <summary> Includes extensions for controls. </summary>
    /// <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2010-11-19, 1.2.3975 </para></remarks>
    public static partial class ControlExtensionMethods
    {

        #region " COPY PROPERTIES "

        /// <summary> Copies properties from a source to a destination controls. </summary>
        /// <remarks> Use this method to copy the properties of a control to a new control. </remarks>
        /// <param name="source">      specifies an instance of the control which properties are copied
        /// from. </param>
        /// <param name="destination"> specifies an instance of the control which properties are copied
        /// to. </param>
        /// <example>
        /// This example adds a button to a form.
        /// <code>
        /// Private buttons As New ArrayList()
        /// Private Sub AddButton(ByVal sourceButton As Button)
        /// Dim newButton As New Button()
        /// WinFormsSupport.CopyControlProperties(sourceButton, newButton)
        /// Me.Controls.Add(newButton)
        /// AddHandler newButton.Click, AddressOf Me.ExitButton_Click
        /// newButton.Name = $"newButton {buttons.count}"
        /// newButton.Text = newButton.Name
        /// newButton.Top = newButton.Top + newButton.Height * (buttons.Count + 1)
        /// buttons.Add(newButton)
        /// End Sub
        /// </code>
        /// To run this example, paste the code fragment into the method region of a Visual Basic form.
        /// Run the program by pressing F5.
        /// </example>
        public static void CopyControlProperties( this Control source, Control destination )
        {
            if ( source is null || destination is null )
            {
                return;
            }

            var dontCopyNames = new string[] { "WindowTarget" };
            System.Reflection.PropertyInfo[] pinfos;
            if ( !(source is null || destination is null) )
            {
                pinfos = source.GetType().GetProperties();
                foreach ( var pinfo in pinfos )
                {
                    // copy only those properties without parameters - you'll rarely need indexed properties
                    if ( pinfo.GetIndexParameters().Length == 0 )
                    {
                        // skip properties that appear in the list of disallowed names.
                        if ( Array.IndexOf( dontCopyNames, pinfo.Name ) < 0 )
                        {
                            // can only copy those properties that can be read and written.  
                            if ( pinfo.CanRead & pinfo.CanWrite )
                            {
                                // set the destination based on the source.
                                pinfo.SetValue( destination, pinfo.GetValue( source, null ), null );
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region " READ ONLY TAB STOPS "

        /// <summary> Disables tab stops on read only text box. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="controls"> The controls. </param>
        public static void DisableReadOnlyTabStop( this Control.ControlCollection controls )
        {
            if ( controls is object )
            {
                foreach ( TextBoxBase tb in controls.OfType<TextBoxBase>() )
                {
                    tb.TabStop = !tb.ReadOnly;
                }
            }
        }

        /// <summary> Disables tab stops on read only text box. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> The control to enable or disable. </param>
        public static void DisableReadOnlyTabStop( this Control control )
        {
            if ( control is object && control.Controls is object )
            {
                control.Controls.DisableReadOnlyTabStop();
                foreach ( Control c in control.Controls )
                {
                    c.DisableReadOnlyTabStop();
                }
            }
        }

        #endregion

        #region " ENABLE CONTROLS "

        /// <summary> Recursively enables or disabled the control and all its children . </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> The control to enable or disable. </param>
        /// <param name="value">   Specifies a value to set. </param>
        public static void RecursivelyEnable( this Control control, bool value )
        {
            if ( control is object )
            {
                control.Enabled = value;
                control.Controls.RecursivelyEnable( value );
            }
        }

        /// <summary> Recursively enables or disabled the control and all its children . </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="controls"> The controls. </param>
        /// <param name="value">    Specifies a value to set. </param>
        public static void RecursivelyEnable( this Control.ControlCollection controls, bool value )
        {
            if ( controls is object )
            {
                foreach ( Control c in controls )
                {
                    c.RecursivelyEnable( value );
                }
            }
        }

        #endregion

        #region " MEASURE "

        /// <summary> Measure text. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="control"> The control to enable or disable. </param>
        /// <param name="text">    The text. </param>
        /// <returns> A Drawing.SizeF. </returns>
        public static SizeF MeasureText( this Control control, string text )
        {
            if ( control is null )
            {
                throw new ArgumentNullException( nameof( control ) );
            }

            using var graphics = control.CreateGraphics();
            return graphics.MeasureString( text, control.Font );
        }

        /// <summary> Measure text. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="font"> The font. </param>
        /// <param name="text"> The text. </param>
        /// <returns> A Drawing.SizeF. </returns>
        public static SizeF MeasureText( this Font font, string text )
        {
            if ( font is null )
            {
                throw new ArgumentNullException( nameof( font ) );
            }

            using var ctrl = new Control();
            using var graphics = ctrl.CreateGraphics();
            return graphics.MeasureString( text, font );
        }

        #endregion

        #region " TEXT "

        /// <summary>
        /// Sets the <see cref="TextBox">control</see> text to the
        /// <paramref name="value">value</paramref>.
        /// The control is disabled when set so that the handling of the changed event can be skipped.
        /// </summary>
        /// <remarks> The value is set to empty if null or empty. </remarks>
        /// <param name="control"> The control. </param>
        /// <param name="value">   The value. </param>
        /// <returns> value. </returns>
        public static string SilentTextSetter( this Control control, string value )
        {
            if ( control is object )
            {
                if ( string.IsNullOrWhiteSpace( value ) )
                {
                    value = string.Empty;
                }

                bool enabled = control.Enabled;
                control.Enabled = false;
                control.Text = value;
                control.Enabled = enabled;
            }

            return value;
        }

        /// <summary>
        /// Sets the <see cref="TextBox">control</see> text to the formatted text. The control is
        /// disabled when set so that the handling of the changed event can be skipped.
        /// </summary>
        /// <remarks> The value is set to empty if null or empty. </remarks>
        /// <param name="control"> The control. </param>
        /// <param name="format">  The text format. </param>
        /// <param name="args">    The format arguments. </param>
        /// <returns> value. </returns>
        public static string SilentTextSetter( this Control control, string format, params object[] args )
        {
            return control.SilentTextSetter( string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
        }

        #endregion

        #region " DRAW TEXT "

        /// <summary>
        /// Draws text into a  <see cref="System.Windows.Forms.Control">windows control</see>
        /// center middle using the control font.
        /// </summary>
        /// <remarks>
        /// Call this method after changing the progress bar's value. If for some reason, the changing of
        /// the progress bar's value doesn't refresh it and clear the previously drawn text, call the
        /// Refresh method of the progress bar before calling this method.
        /// </remarks>
        /// <param name="control"> The target progress bar to add text into. </param>
        /// <param name="value">   The text to add into the progress bar. Leave null or empty to
        /// automatically add the percent. </param>
        public static void DrawText( this Control control, string value )
        {
            if ( control is object )
            {
                control.DrawText( value, ContentAlignment.MiddleCenter, control.Font );
            }
        }

        /// <summary>
        /// Draws text into a  <see cref="System.Windows.Forms.Control">windows control</see>.
        /// </summary>
        /// <remarks>
        /// Call this method after changing the progress bar's value. If for some reason, the changing of
        /// the progress bar's value doesn't refresh it and clear the previously drawn text, call the
        /// Refresh method of the progress bar before calling this method.
        /// </remarks>
        /// <param name="control">   The target progress bar to add text into. </param>
        /// <param name="value">     The text to add into the progress bar. Leave null or empty to
        /// automatically add the percent. </param>
        /// <param name="textAlign"> Where the text is to be placed. </param>
        public static void DrawText( this Control control, string value, ContentAlignment textAlign )
        {
            if ( control is object )
            {
                control.DrawText( value, textAlign, control.Font );
            }
        }

        /// <summary>
        /// Draws text into a  <see cref="System.Windows.Forms.Control">windows control</see>.
        /// </summary>
        /// <remarks>
        /// Call this method after changing the progress bar's value. If for some reason, the changing of
        /// the progress bar's value doesn't refresh it and clear the previously drawn text, call the
        /// Refresh method of the progress bar before calling this method.
        /// </remarks>
        /// <param name="control">   The target progress bar to add text into. </param>
        /// <param name="value">     The text to add into the progress bar. Leave null or empty to
        /// automatically add the percent. </param>
        /// <param name="textAlign"> Where the text is to be placed. </param>
        /// <param name="textFont">  The font the text should be drawn in. </param>
        public static void DrawText( this Control control, string value, ContentAlignment textAlign, Font textFont )
        {
            if ( control is object )
            {
                if ( control.InvokeRequired )
                {
                    _ = control.Invoke( new Action<Control, string, ContentAlignment, Font>( DrawText ), new object[] { control, value, textAlign, textFont } );
                }
                else
                {
                    using var gr = control.CreateGraphics();
                    const float margin = 0f;
                    float x = margin;
                    float y = (control.Height - gr.MeasureString( value, textFont ).Height) / 2.0f;
                    if ( textAlign == ContentAlignment.BottomCenter || textAlign == ContentAlignment.MiddleCenter || textAlign == ContentAlignment.TopCenter )
                    {
                        x = (control.Width - gr.MeasureString( value, textFont ).Width) / 2.0f;
                    }
                    else if ( textAlign == ContentAlignment.BottomRight || textAlign == ContentAlignment.MiddleRight || textAlign == ContentAlignment.TopRight )
                    {
                        x = control.Width - gr.MeasureString( value, textFont ).Width - margin;
                    }

                    if ( textAlign == ContentAlignment.BottomCenter || textAlign == ContentAlignment.BottomLeft || textAlign == ContentAlignment.BottomRight )
                    {
                        y = control.Height - gr.MeasureString( value, textFont ).Height - margin;
                    }
                    else if ( textAlign == ContentAlignment.TopCenter || textAlign == ContentAlignment.TopLeft || textAlign == ContentAlignment.TopRight )
                    {
                        y = margin;
                    }

                    y = Math.Max( y, margin );
                    x = Math.Max( x, margin );
                    using var sb = new SolidBrush( control.ForeColor );
                    gr.DrawString( value, textFont, sb, new PointF( x, y ) );
                }
            }
        }

        #endregion

        #region " IMAGE "

        /// <summary> Take screen shot. </summary>
        /// <remarks> David, 2020-09-01. </remarks>
        /// <param name="control"> The control to enable or disable. </param>
        /// <returns> A Bitmap. </returns>
        public static Bitmap TakeScreenShot( this Control control )
        {
            var tmpImg = new Bitmap( control.Width, control.Height );
            using ( var g = Graphics.FromImage( tmpImg ) )
            {
                g.CopyFromScreen( control.PointToScreen( new Point( 0, 0 ) ), new Point( 0, 0 ), new Size( control.Width, control.Height ) );
            }

            return tmpImg;
        }

        /// <summary> Saves a screen shot. </summary>
        /// <remarks> David, 2020-09-01. </remarks>
        /// <param name="control">     The control to enable or disable. </param>
        /// <param name="filename">    Filename of the file. </param>
        /// <param name="imageFormat"> The image format. </param>
        public static void SaveScreenShot( this Control control, string filename, System.Drawing.Imaging.ImageFormat imageFormat )
        {
            var tmpImg = new Bitmap( control.Width, control.Height );
            using ( var g = Graphics.FromImage( tmpImg ) )
            {
                g.CopyFromScreen( control.PointToScreen( new Point( 0, 0 ) ), new Point( 0, 0 ), new Size( control.Width, control.Height ) );
            }

            tmpImg.Save( filename, imageFormat );
        }

        #endregion

    }
}
