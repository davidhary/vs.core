using System.Windows.Forms;

namespace isr.Core.WinForms.ListControlExtensions
{
    /// <summary> Includes extensions for <see cref="ListControl">List Control</see>. </summary>
    /// <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2010-11-19, 1.2.3975 </para></remarks>
    public static class ListControlExtensionMethods
    {

        /// <summary>
        /// Selects the <see cref="Control">combo box</see> index by setting the Selected Index to the
        /// <paramref name="value">value</paramref>. The control is disabled when set so that the
        /// handling of the changed event can be skipped.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control"> The control. </param>
        /// <param name="value">   The value. </param>
        /// <returns> The index. </returns>
        public static int SilentSelectIndex( this ListControl control, int value )
        {
            if ( control is object )
            {
                bool enabled = control.Enabled;
                control.Enabled = false;
                if ( control.SelectedIndex != value )
                {
                    control.SelectedIndex = value;
                }

                control.Enabled = enabled;
            }

            return value;
        }

        /// <summary>
        /// Gets the selected or default <paramref name="defaultValue">value</paramref>.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="control">      The control. </param>
        /// <param name="defaultValue"> The default value. </param>
        /// <returns> ``0. </returns>
        public static T GetSelectedValue<T>( this ListControl control, T defaultValue )
        {
            return control is null || control.SelectedValue is null ? defaultValue : ( T ) control.SelectedValue;
        }

        // Notes:
        // Setting the Selected Value does not work!

    }
}
