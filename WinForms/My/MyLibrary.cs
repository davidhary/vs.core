
#pragma warning disable IDE1006 // Naming Styles
namespace isr.Core.WinForms.My
#pragma warning restore IDE1006 // Naming Styles
{

    /// <summary> Provides assembly information for the class library. </summary>
    /// <remarks> David, 2020-09-16. </remarks>
    public sealed partial class MyLibrary
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        private MyLibrary() : base()
        {
        }

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "Core Framework Controls Library";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "Core Framework Controls Library";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "Core.WinForms";

        /// <summary> The Strong Name of the test assembly. </summary>
        public const string TestAssemblyStrongName = "isr.Core.WinFormsTests,PublicKey=" + isr.Core.My.SolutionInfo.PublicKey;

    }
}
