using System.Windows.Forms;

#pragma warning disable IDE1006 // Naming Styles
namespace isr.Core.WinForms.WindowsFormsExtensions
#pragma warning restore IDE1006 // Naming Styles
{

    /// <summary> Extensions methods for <see cref="System.Windows.Forms"/> types. </summary>
    /// <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para> </remarks>
    public static class WindowsFormsExtensionMethods
    {

        #region " CHECK STATE "

        /// <summary> Converts a value to a check state. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> Value as a CheckState. </returns>
        public static CheckState ToCheckState( this bool? value )
        {
            return value.HasValue ? value.Value ? CheckState.Checked : CheckState.Unchecked : CheckState.Indeterminate;
        }

        /// <summary> Converts a check state to nullable Boolean. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> A Boolean? </returns>
        public static bool? FromCheckState( this CheckState value )
        {
            return value == CheckState.Indeterminate ? new bool?() : value == CheckState.Checked;
        }

        /// <summary> Converts this object to a check state caption. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="value">              The value. </param>
        /// <param name="checkedValue">       The checked value. </param>
        /// <param name="uncheckedValue">     The unchecked value. </param>
        /// <param name="indeterminateValue"> The indeterminate value. </param>
        /// <returns> The given data converted to a String. </returns>
        public static string ToCheckStateCaption( this CheckState value, string checkedValue, string uncheckedValue, string indeterminateValue )
        {
            return value == CheckState.Checked ? checkedValue : value == CheckState.Unchecked ? uncheckedValue : indeterminateValue;
        }

        #endregion

    }
}
