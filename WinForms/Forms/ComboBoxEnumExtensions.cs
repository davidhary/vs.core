using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

using isr.Core.EnumExtensions;

using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.WinForms.ComboBoxEnumExtensions
{

    /// <summary> Encapsulates the Form related functionality of the <see cref="isr.Core.EnumExtender{T}"/></summary>
    /// <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-03-25 </para><para>
    /// David, 2008-05-04" by="Stewart and StageSoft" revision="1.2.3411.x"> <para>
    /// From Strong-Type and Efficient .NET Enums By Hanna GIAT</para><para>
    /// http://www.CodeProject.com/KB/cs/efficient_strong_enum.aspx. </para> </para></remarks>
    public static class ComboBoxEnumExtensionMethods
    {

        #region " COMBO BOX ENUM VALUE SELECTION "

#if false
        /// <summary> Returns the selected enum value. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="comboBox">     The combo box control. </param>
        /// <param name="defaultValue"> The default value. </param>
        /// <returns> The selected Enum. </returns>
        public static Enum SelectedEnumValue( this ComboBox comboBox, Enum defaultValue )
        {
            if ( comboBox is object && comboBox.SelectedItem is object && defaultValue is object )
            {
                KeyValuePair<Enum, string> kvp = ( KeyValuePair<Enum, string> ) comboBox.SelectedItem;
                if ( Enum.IsDefined( defaultValue.GetType(), kvp.Key ) )
                {
                    defaultValue = kvp.Key;
                }
            }

            return defaultValue;
        }
#endif

        /// <summary> Returns the selected enum value. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="listControl"> The list control. </param>
        /// <returns> The selected Enum. </returns>
        public static T SelectedEnumValue<T>( this ListControl listControl )
        {
            return listControl is null ? throw new ArgumentNullException( nameof( listControl ) ) : ( T ) listControl.SelectedValue;
        }

        /// <summary> Returns the selected enum value. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="listControl">  The list control. </param>
        /// <param name="defaultValue"> The default value. </param>
        /// <returns> The selected Enum. </returns>
        public static T SelectedEnumValue<T>( this ListControl listControl, T defaultValue )
        {
            return listControl is null
                ? throw new ArgumentNullException( nameof( listControl ) )
                : Enum.IsDefined( typeof( T ), listControl.SelectedValue ) ? ( T ) listControl.SelectedValue : defaultValue;
        }

        /// <summary> Returns the selected enum value. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="comboBox">     The tool strip combo box control. </param>
        /// <param name="defaultValue"> The default value. </param>
        /// <returns> The selected Enum. </returns>
        public static Enum SelectedEnumValue( this ToolStripComboBox comboBox, Enum defaultValue )
        {
            if ( comboBox is object && comboBox.SelectedItem is object && defaultValue is object )
            {
                KeyValuePair<Enum, string> kvp = ( KeyValuePair<Enum, string> ) comboBox.SelectedItem;
                if ( Enum.IsDefined( defaultValue.GetType(), kvp.Key ) )
                {
                    defaultValue = kvp.Key;
                }
            }

            return defaultValue;
        }

        /// <summary> Returns the selected enum value. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="comboBox"> The tool strip combo box control. </param>
        /// <returns> The selected Enum. </returns>
        public static T SelectedEnumValue<T>( this ToolStripComboBox comboBox )
        {
            return comboBox is null ? throw new ArgumentNullException( nameof( comboBox ) ) : ( T ) comboBox.ComboBox.SelectedValue;
        }

        /// <summary> Returns the selected enum value. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="comboBox">     The tool strip combo box control. </param>
        /// <param name="defaultValue"> The default value. </param>
        /// <returns> The selected Enum. </returns>
        public static T SelectedEnumValue<T>( this ToolStripComboBox comboBox, T defaultValue )
        {
            return comboBox is null
                ? throw new ArgumentNullException( nameof( comboBox ) )
                : Enum.IsDefined( typeof( T ), comboBox.ComboBox.SelectedValue ) ? ( T ) comboBox.ComboBox.SelectedValue : defaultValue;
        }

        /// <summary> Returns the selected enum item. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="comboBox"> The combo box control. </param>
        /// <returns> A KeyValuePair(Of T, String) </returns>
        public static KeyValuePair<T, string> SelectedEnumItem<T>( this ComboBox comboBox )
        {
            return comboBox is null ? throw new ArgumentNullException( nameof( comboBox ) ) : ( KeyValuePair<T, string> ) comboBox.SelectedItem;
        }

        /// <summary> Returns the selected enum item. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="toolStripComboBox"> The tool strip combo box control. </param>
        /// <returns> A KeyValuePair(Of T, String) </returns>
        public static KeyValuePair<T, string> SelectedEnumItem<T>( this ToolStripComboBox toolStripComboBox )
        {
            return toolStripComboBox is null
                ? throw new ArgumentNullException( nameof( toolStripComboBox ) )
                : ( KeyValuePair<T, string> ) toolStripComboBox.SelectedItem;
        }

        /// <summary> Selects an item from the control and return the selected value. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="comboBox"> The combo box control. </param>
        /// <param name="item">     The item. </param>
        /// <returns> A KeyValuePair(Of T, String) </returns>
        public static KeyValuePair<T, string> SelectItem<T>( this ComboBox comboBox, KeyValuePair<T, string> item )
        {
            if ( comboBox is null )
            {
                throw new ArgumentNullException( nameof( comboBox ) );
            }

            comboBox.SelectedItem = item;
            return ( KeyValuePair<T, string> ) comboBox.SelectedItem;
        }

        /// <summary> Selects a value and return the selected item. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="comboBox"> The combo box control. </param>
        /// <param name="value">    Specifies the bit values which to add. </param>
        /// <returns> A KeyValuePair(Of T, String) </returns>
        public static KeyValuePair<T, string> SelectValue<T>( this ComboBox comboBox, T value )
        {
            if ( comboBox is null )
            {
                throw new ArgumentNullException( nameof( comboBox ) );
            }

            comboBox.SelectedValue = value;
            return ( KeyValuePair<T, string> ) comboBox.SelectedItem;
        }

        /// <summary> Selects a combo box value and return the selected item. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="comboBox"> The combo box control. </param>
        /// <param name="value">    Specifies the bit values which to add. </param>
        /// <returns> A KeyValuePair(Of T, String) </returns>
        public static KeyValuePair<Enum, string> SelectValue( this ComboBox comboBox, Enum value )
        {
            if ( comboBox is null )
            {
                throw new ArgumentNullException( nameof( comboBox ) );
            }

            comboBox.SelectedValue = value;
            return ( KeyValuePair<Enum, string> ) comboBox.SelectedItem;
        }

        /// <summary> Selects a combo box value and return the selected item. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="comboBox"> The combo box control. </param>
        /// <param name="value">    Specifies the bit values which to add. </param>
        /// <returns> A KeyValuePair(Of T, String) </returns>
        public static KeyValuePair<T, string> SelectValue<T>( this ToolStripComboBox comboBox, T value )
        {
            if ( comboBox is null )
            {
                throw new ArgumentNullException( nameof( comboBox ) );
            }

            comboBox.ComboBox.SelectedValue = value;
            return ( KeyValuePair<T, string> ) comboBox.SelectedItem;
        }

        /// <summary> Selects a combo box value and return the selected item. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="comboBox"> The combo box control. </param>
        /// <param name="value">    Specifies the bit values which to add. </param>
        /// <returns> A KeyValuePair(Of T, String) </returns>
        public static KeyValuePair<Enum, string> SelectValue( this ToolStripComboBox comboBox, Enum value )
        {
            if ( comboBox is null )
            {
                throw new ArgumentNullException( nameof( comboBox ) );
            }

            comboBox.ComboBox.SelectedValue = value;
            return ( KeyValuePair<Enum, string> ) comboBox.SelectedItem;
        }

        /// <summary> Select value. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="control"> The control. </param>
        /// <param name="value">   Specifies the bit values which to add. </param>
        public static void SelectValue<T>( this ListControl control, T value )
        {
            if ( control is null )
            {
                throw new ArgumentNullException( nameof( control ) );
            }

            control.SelectedValue = value.ToInteger();
        }

        /// <summary> Selects a value and return the selected item. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="control"> The control. </param>
        /// <param name="value">   Specifies the bit values which to add. </param>
        /// <returns> A KeyValuePair(Of T, String) </returns>
        public static Enum SelectValue( this ListControl control, Enum value )
        {
            if ( control is null )
            {
                throw new ArgumentNullException( nameof( control ) );
            }

            control.SelectedValue = value;
            return ( Enum ) control.SelectedValue;
        }

        #endregion

        #region " GENERTIC LIST CONTROL ENUM FUNCTIONS: LIST "

        /// <summary> List enum names. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="listControl"> The list control. </param>
        /// <returns> An Integer. </returns>
        public static int ListEnumNames<T>( this ListControl listControl )
        {
            return listControl.ListEnums( typeof( T ).ValueNamePairs() );
        }

        /// <summary> Lists the enum names filtered by the provided masks. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="listControl"> The list control. </param>
        /// <param name="includeMask"> The include mask. </param>
        /// <param name="excludeMask"> The exclude mask. </param>
        public static void ListEnumNames( this ListControl listControl, Enum includeMask, Enum excludeMask )
        {
            _ = listControl.ListEnums( includeMask.EnumValues().Filter( includeMask, excludeMask ).ValueNamePairs() );
        }

        /// <summary> Lists the enum names filtered by the provided masks. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="listControl"> The list control. </param>
        /// <param name="includeMask"> The include mask. </param>
        /// <param name="excludeMask"> The exclude mask. </param>
        public static void ListEnumNames<T>( this ListControl listControl, T includeMask, T excludeMask )
        {
            _ = listControl.ListEnums( typeof( T ).EnumValues<T>().Filter( includeMask, excludeMask ).ValueNamePairs() );
        }

        /// <summary> List enum descriptions. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="listControl"> The list control. </param>
        /// <returns> An Integer. </returns>
        public static int ListEnumDescriptions<T>( this ListControl listControl )
        {
            return listControl.ListEnums( typeof( T ).ValueDescriptionPairs() );
        }

        /// <summary> Lists the enum descriptions. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="listControl"> The list control. </param>
        /// <param name="includeMask"> The include mask. </param>
        /// <param name="excludeMask"> The exclude mask. </param>
        public static void ListEnumDescriptions( this ListControl listControl, Enum includeMask, Enum excludeMask )
        {
            _ = listControl.ListEnums( includeMask.EnumValues().Filter( includeMask, excludeMask ).ValueDescriptionPairs() );
        }

        /// <summary> Lists the enum descriptions. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="listControl"> The list control. </param>
        /// <param name="includeMask"> The include mask. </param>
        /// <param name="excludeMask"> The exclude mask. </param>
        public static void ListEnumDescriptions<T>( this ListControl listControl, T includeMask, T excludeMask )
        {
            _ = listControl.ListEnums( typeof( T ).EnumValues<T>().Filter( includeMask, excludeMask ).ValueDescriptionPairs() );
        }

        /// <summary> List enum value and string pairs. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="listControl">   The list control. </param>
        /// <param name="enumTextPairs"> The enum text pairs. </param>
        /// <returns> An Integer. </returns>
        public static int ListEnums( this ListControl listControl, IEnumerable<KeyValuePair<Enum, string>> enumTextPairs )
        {
            if ( listControl is null )
            {
                throw new ArgumentNullException( nameof( listControl ) );
            }

            if ( enumTextPairs is null )
            {
                throw new ArgumentNullException( nameof( enumTextPairs ) );
            }

            bool comboEnabled = listControl.Enabled;
            listControl.Enabled = false;
            listControl.DataSource = null;
            listControl.ValueMember = nameof( System.Collections.Generic.KeyValuePair<System.Enum, string>.Key );
            listControl.DisplayMember = nameof( System.Collections.Generic.KeyValuePair<System.Enum, string>.Value );

            // converting to array ensures that values displayed in two controls are not connected.
            listControl.DataSource = enumTextPairs.ToList();
            listControl.SelectedIndex = enumTextPairs.Any() ? 0 : -1;
            listControl.Enabled = comboEnabled;
            listControl.Invalidate();
            return enumTextPairs.Count();
        }

        /// <summary> List enum value and string pairs. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="listControl">   The list control. </param>
        /// <param name="enumTextPairs"> The enum text pairs. </param>
        /// <returns> An Integer. </returns>
        public static int ListEnums<T>( this ListControl listControl, IEnumerable<KeyValuePair<T, string>> enumTextPairs )
        {
            if ( listControl is null )
            {
                throw new ArgumentNullException( nameof( listControl ) );
            }

            if ( enumTextPairs is null )
            {
                throw new ArgumentNullException( nameof( enumTextPairs ) );
            }

            bool comboEnabled = listControl.Enabled;
            listControl.Enabled = false;
            listControl.ValueMember = nameof( System.Collections.Generic.KeyValuePair<System.Enum, string>.Key );
            listControl.DisplayMember = nameof( System.Collections.Generic.KeyValuePair<System.Enum, string>.Value );

            // converting to array ensures that values displayed in two controls are not connected.
            listControl.DataSource = enumTextPairs.ToList();
            listControl.SelectedIndex = enumTextPairs.Any() ? 0 : -1;
            listControl.Enabled = comboEnabled;
            listControl.Invalidate();
            return enumTextPairs.Count();
        }

        #endregion

        #region " GENERTIC LIST CONTROL DICTIONARY FUNCTIONS "

        /// <summary> List values. </summary>
        /// <remarks>
        /// The first step is to add a description attribute to your Enum.
        /// <code>
        /// Public Enum SimpleEnum2
        /// [System.ComponentModel.Description("Today")] Today
        /// [System.ComponentModel.Description("Last 7 days")] Last7
        /// [System.ComponentModel.Description("Last 14 days")] Last14
        /// [System.ComponentModel.Description("Last 30 days")] Last30
        /// [System.ComponentModel.Description("All")] All
        /// End Enum
        /// Public SimpleEfficientEnum As New EnumExtender(of SimpleEnum2)
        /// Dim combo As ComboBox = New ComboBox()
        /// combo.DataSource = SimpleEfficientEnum.GetValueDescriptionPairs(GetType(SimpleEnum2)).ToList
        /// combo.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
        /// combo.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
        /// dim selectedEnum as SimpleEnum2 = SimpleEfficientEnum.ToObjectFromPair(combo.SelectedItem)
        /// selectedEnum = combo.selectedValue(of SimpleEnum2)(SimpleEfficientEnum.ValueEnumPairs)
        /// </code>
        /// This works with any control that supports data binding, including the
        /// <see cref="System.Windows.Forms.ToolStripComboBox">tool strip combo box</see>,
        /// although you will need to cast the
        /// <see cref="System.Windows.Forms.ToolStripComboBox">control property</see>
        /// to a <see cref="System.Windows.Forms.ComboBox">Combo Box</see> to get to the DataSource
        /// property. In that case, you will also want to perform the same cast when you are referencing
        /// the selected value to work with it as your Enum type.
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="listControl">   The list control. </param>
        /// <param name="enumTextPairs"> The enum text pairs. </param>
        /// <returns> The item count. </returns>
        public static int ListValues<T>( this ListControl listControl, IDictionary<T, string> enumTextPairs )
        {
            if ( listControl is null )
            {
                throw new ArgumentNullException( nameof( listControl ) );
            }

            if ( enumTextPairs is null )
            {
                throw new ArgumentNullException( nameof( enumTextPairs ) );
            }

            bool comboEnabled = listControl.Enabled;
            listControl.Enabled = false;
            listControl.DataSource = null;
            listControl.ValueMember = nameof( System.Collections.Generic.KeyValuePair<System.Enum, string>.Key );
            listControl.DisplayMember = nameof( System.Collections.Generic.KeyValuePair<System.Enum, string>.Value );

            // converting to array ensures that values displayed in two controls are not connected.
            listControl.DataSource = enumTextPairs.ToList();
            listControl.SelectedIndex = enumTextPairs.Any() ? 0 : -1;
            listControl.Enabled = comboEnabled;
            listControl.Invalidate();
            return enumTextPairs.Count;
        }

        /// <summary> Selected value. </summary>
        /// <remarks> David, 2020-09-04. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="comboBox">       The combo box control. </param>
        /// <param name="valueEnumPairs"> The value enum pairs. </param>
        /// <returns> The selected Enum. </returns>
        public static T SelectedValue<T>( this ComboBox comboBox, IDictionary<int, T> valueEnumPairs )
        {
            return comboBox is null ? throw new ArgumentNullException( nameof( comboBox ) ) : valueEnumPairs.ToObject( comboBox.SelectedItem );
        }

        /// <summary> Returns the value converted to Enum from the given value name pair. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="values"> The values. </param>
        /// <param name="value">  The value. </param>
        /// <returns> converted to Enum from the given value name pair. </returns>
        public static T ToObject<T>( this IDictionary<int, T> values, object value )
        {
            return value is null ? default : values.ToObject( (( KeyValuePair<T, string> ) value).Key );
        }

        /// <summary> Converts a value to an integer. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="value"> The value. </param>
        /// <returns> value as an Integer. </returns>
        public static int ToInteger( this object value )
        {
            if ( value is null )
            {
                throw new ArgumentNullException( nameof( value ) );
            }

            try
            {
                return Conversions.ToInteger( Conversion.Fix( value ) );
            }
            catch ( InvalidCastException ex )
            {
                throw new ArgumentException( $"Cannot convert {value} to integer", nameof( value ), ex );
            }
        }

        #endregion

        #region " ENUM EXTENDER "

        /// <summary> List values. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="listControl">  The list control. </param>
        /// <param name="enumExtender"> The enum extender. </param>
        /// <returns> The item count. </returns>
        public static int ListValues<T>( this ListControl listControl, EnumExtender<T> enumExtender )
        {
            return enumExtender is null
                ? throw new ArgumentNullException( nameof( enumExtender ) )
                : listControl.ListValues( enumExtender.ValueDescriptionPairs );
        }

        /// <summary> Selected value. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="comboBox">     The combo box control. </param>
        /// <param name="enumExtender"> The enum extender. </param>
        /// <returns> The selected Enum. </returns>
        public static T SelectedValue<T>( this ComboBox comboBox, EnumExtender<T> enumExtender )
        {
            return enumExtender is null
                ? throw new ArgumentNullException( nameof( enumExtender ) )
                : comboBox.SelectedValue( enumExtender.ValueEnumValuePairs );
        }

        #endregion

    }
}
