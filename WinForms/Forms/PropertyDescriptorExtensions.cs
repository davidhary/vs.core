using System;
using System.Windows.Forms;

namespace isr.Core.WinForms.PropertyDescriptorExtensions
{
    /// <summary>   <see cref="PropertyDescriptorExtensions"/> methods. </summary>
    /// <remarks>   David, 2020-09-17. </remarks>
    public static class PropertyDescriptorExtensionMethods
    {

        /// <summary> Gets list item properties. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="listAccessors"> The list accessors. </param>
        /// <returns> The list item properties. </returns>
        public static System.ComponentModel.PropertyDescriptorCollection GetListItemProperties<T>( this System.ComponentModel.PropertyDescriptor[] listAccessors )
        {
            return listAccessors.GetListItemProperties( typeof( T ) );
        }

        /// <summary> Gets list item properties. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <param name="listAccessors"> The list accessors. </param>
        /// <param name="type">          The type. </param>
        /// <returns> The list item properties. </returns>
        public static System.ComponentModel.PropertyDescriptorCollection GetListItemProperties( this System.ComponentModel.PropertyDescriptor[] listAccessors, Type type )
        {
            return ListBindingHelper.GetListItemProperties( type, listAccessors );
        }
    }
}
