using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace isr.Core.WinForms.ComboBoxBindingExtensions
{

    /// <summary> Extensions methods binding combo box to a <see cref="System.ComponentModel.BindingList{T}"/>A methods. </summary>
    /// <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para> </remarks>
    public static class ComboBoxBindingExtensionMethods
    {

        /// <summary>
        /// Displays a list of resource name selecting the existing name of existing in the list.
        /// </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="comboBox">      The combo box. </param>
        /// <param name="resourceNames"> List of names of the resources. </param>
        public static void DisplayResourceNames( this ComboBox comboBox, BindingList<string> resourceNames )
        {
            if ( comboBox is null )
            {
                throw new ArgumentNullException( nameof( comboBox ) );
            }

            if ( resourceNames is null )
            {
                throw new ArgumentNullException( nameof( resourceNames ) );
            }

            if ( resourceNames.Any() )
            {
                string resourceName = comboBox.Text;
                comboBox.DataSource = null;
                comboBox.Items.Clear();
                comboBox.DataSource = resourceNames;
                if ( resourceNames.Contains( resourceName ) )
                {
                    comboBox.Text = resourceName;
                }
            }
        }
    }
}
