using System;
using System.Windows.Forms;

using isr.Core.EnumExtensions;

namespace isr.Core.WinForms.EnumExtensions
{
    /// <summary>   <see cref="EnumExtensions"/> methods. </summary>
    /// <remarks>   David, 2020-09-16. </remarks>
    public static partial class EnumExtensionMethods
    {

        /// <summary> Displays a value description pair. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="type">     The type. </param>
        /// <param name="comboBox"> The combo box. </param>
        /// <returns> An Integer. </returns>
        public static int DisplayValueDescriptionPairs( this Type type, ComboBox comboBox )
        {
            if ( type is null )
            {
                throw new ArgumentNullException( nameof( type ) );
            }

            if ( comboBox is null )
            {
                throw new ArgumentNullException( nameof( comboBox ) );
            }

            comboBox.DataSource = null;
            comboBox.Items.Clear();
            comboBox.DataSource = type.ValueDescriptionPairs().ToBindingList();
            comboBox.ValueMember = nameof( System.Collections.Generic.KeyValuePair<System.Enum, string>.Key );
            comboBox.DisplayMember = nameof( System.Collections.Generic.KeyValuePair<System.Enum, string>.Value );
            return comboBox.Items.Count;
        }

        /// <summary> Displays a value description pair. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="enumItem"> An enum item representing the enumeration option. </param>
        /// <param name="comboBox"> The combo box. </param>
        /// <returns> An Integer. </returns>
        public static int DisplayValueDescriptionPairs( this Enum enumItem, ComboBox comboBox )
        {
            if ( enumItem is null )
            {
                throw new ArgumentNullException( nameof( enumItem ) );
            }

            if ( comboBox is null )
            {
                throw new ArgumentNullException( nameof( comboBox ) );
            }

            comboBox.DataSource = null;
            comboBox.Items.Clear();
            comboBox.DataSource = enumItem.ValueDescriptionPairs().ToBindingList();
            comboBox.ValueMember = nameof( System.Collections.Generic.KeyValuePair<System.Enum, string>.Key );
            comboBox.DisplayMember = nameof( System.Collections.Generic.KeyValuePair<System.Enum, string>.Value );
            return comboBox.Items.Count;
        }

        /// <summary> Displays a value description pair. </summary>
        /// <remarks> David, 2020-09-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="enumConstant"> The <see cref="T:System.Enum"/> constant. </param>
        /// <param name="comboBox">     The combo box. </param>
        /// <returns> An Integer. </returns>
        public static int DisplayValueDescriptionPairs( this Enum enumConstant, ToolStripComboBox comboBox )
        {
            if ( enumConstant is null )
            {
                throw new ArgumentNullException( nameof( enumConstant ) );
            }

            if ( comboBox is null )
            {
                throw new ArgumentNullException( nameof( comboBox ) );
            }

            _ = enumConstant.DisplayValueDescriptionPairs( comboBox.Control as ComboBox );
            return comboBox.Items.Count;
        }
    }
}
