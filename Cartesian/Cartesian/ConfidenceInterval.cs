using System;

namespace isr.Core.Cartesian
{

    /// <summary> Implements a generic Confidence interval structure. </summary>
    /// <remarks>
    /// (c) 2006 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2006-04-10, 1.1.2291. </para>
    /// </remarks>
    public struct ConfidenceInterval<T> where T : IComparable<T>, IEquatable<T>, IFormattable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfidenceInterval{T}" /> class. The copy constructor.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="model"> The  <see cref="ConfidenceInterval{T}">interval bounds</see> object from which to copy. </param>
        public ConfidenceInterval( ConfidenceInterval<T> model ) : this( model.Min, model.Max )
        {
        }

        /// <summary> Constructs this class. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="minValue"> Min bound. </param>
        /// <param name="maxValue"> Max bound. </param>
        public ConfidenceInterval( T minValue, T maxValue )
        {
            if ( minValue.CompareTo( maxValue ) <= 0 )
            {
                this.Min = minValue;
                this.Max = maxValue;
            }
            else
            {
                this.Min = maxValue;
                this.Max = minValue;
            }
        }

        /// <summary> Sets the interval bounds based on the extrema. </summary>
        /// <param name="minValue"> Specified the minimum value of the interval bounds. </param>
        /// <param name="maxValue"> Specifies the maximum value of the interval bounds. </param>
        private void SetItervalBoundsInternal( T minValue, T maxValue )
        {
            if ( minValue.CompareTo( maxValue ) <= 0 )
            {
                this.Min = minValue;
                this.Max = maxValue;
            }
            else
            {
                this.Min = maxValue;
                this.Max = minValue;
            }
        }

        #endregion

        #region " EQUALS "

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns>
        /// <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />; otherwise, false.
        /// </returns>
        public static new bool Equals( object left, object right )
        {
            return Equals( ( ConfidenceInterval<T> ) left, ( ConfidenceInterval<T> ) right );
        }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns>
        /// <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />; otherwise, false.
        /// </returns>
        public static bool Equals( ConfidenceInterval<T> left, ConfidenceInterval<T> right )
        {
            return left.Min.Equals( right.Min ) && left.Max.Equals( right.Max );
        }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
        /// <see cref="T:System.Object" />. </param>
        /// <returns>
        /// <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />; otherwise, false.
        /// </returns>
        public override bool Equals( object obj )
        {
            return this.Equals( ( ConfidenceInterval<T> ) obj );
        }

        /// <summary> Compares two interval bounds. </summary>
        /// <remarks> The two interval bounds are the same if they have the min and max values. </remarks>
        /// <param name="other"> Specifies the other interval bounds to compare. </param>
        /// <returns> <c>True</c> if the interval bounds are equal. </returns>
        public bool Equals( ConfidenceInterval<T> other )
        {
            return Equals( this, other );
        }

        /// <summary> Creates a unique hash code. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> An <see cref="T:System.Int32">Int32</see> value. </returns>
        public override int GetHashCode()
        {
            return new Tuple<int, int>( this.Min.GetHashCode(), this.Max.GetHashCode() ).GetHashCode();
        }

        #endregion

        #region " OPERATORS "

        /// <summary> Implements the operator =. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( ConfidenceInterval<T> left, ConfidenceInterval<T> right )
        {
            return left.Equals( right );
        }

        /// <summary> Implements the operator &lt;&gt;. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( ConfidenceInterval<T> left, ConfidenceInterval<T> right )
        {
            return !left.Equals( right );
        }

        #endregion

        #region " METHODS AND PROPERTIES "

        /// <summary>
        /// Determines if the <paramref name="value">specified value</paramref>
        /// is within the interval bounds.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="value"> Specifies the value which to check as contained within the interval bounds. </param>
        /// <returns>
        /// <c>True</c> if the <paramref name="value">specified value</paramref> is within interval bounds.
        /// </returns>
        public bool Contains( T value )
        {
            return value.CompareTo( this.Min ) >= 0 && value.CompareTo( this.Max ) <= 0;
        }

        /// <summary>
        /// Returns a new interval bounds from the min of the two minima to the max of the two maxima.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="boundsA"> Specifies <see cref="ConfidenceInterval{T}"/> A. </param>
        /// <param name="boundsB"> Specifies <see cref="ConfidenceInterval{T}"/> B. </param>
        /// <returns> A new bounds from the min of the two minima to the max of the two maxima. </returns>
        public static ConfidenceInterval<T> Extend( ConfidenceInterval<T> boundsA, ConfidenceInterval<T> boundsB )
        {
            return boundsA.Min.CompareTo( boundsB.Min ) > 0
                ? boundsA.Max.CompareTo( boundsB.Max ) < 0 ? new ConfidenceInterval<T>( boundsB.Min, boundsB.Max ) : new ConfidenceInterval<T>( boundsB.Min, boundsA.Max )
                : boundsA.Max.CompareTo( boundsB.Max ) < 0 ? new ConfidenceInterval<T>( boundsA.Min, boundsB.Max ) : new ConfidenceInterval<T>( boundsA.Min, boundsA.Max );
        }

        /// <summary>
        /// Extends this interval bounds to include both its present values and the specified interval bounds.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="bounds"> A <see cref="ConfidenceInterval{T}"/> value. </param>
        /// <returns> A new Limits from the min of the two minima to the max of the two maxima. </returns>
        public ConfidenceInterval<T> ExtendBy( ConfidenceInterval<T> bounds )
        {
            if ( this.Min.CompareTo( bounds.Min ) > 0 )
            {
                this.SetBounds( bounds.Min, this.Min );
            }

            if ( this.Max.CompareTo( bounds.Max ) < 0 )
            {
                this.SetBounds( this.Min, bounds.Max );
            }

            return this;
        }

        /// <summary> Return the interval bounds of the specified data array. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values"> The data array. </param>
        /// <returns> The calculated interval bounds. </returns>
        public static ConfidenceInterval<T> GetBounds( T[] values )
        {

            // return the unit interval bounds if no data
            if ( values is null )
            {
                throw new ArgumentNullException( nameof( values ) );
            }

            // initialize the interval bounds values to the first value
            T temp;
            temp = values[0];
            var min = temp;
            var max = temp;

            // Loop over each point in the arrays
            for ( int i = 0, loopTo = values.Length - 1; i <= loopTo; i++ )
            {
                temp = values[i];
                if ( temp.CompareTo( min ) < 0 )
                {
                    min = temp;
                }
                else if ( temp.CompareTo( max ) > 0 )
                {
                    max = temp;
                }
            }

            return new ConfidenceInterval<T>( min, max );
        }

        /// <summary> Returns the end or maximum value of the interval bounds. </summary>
        /// <value> The maximum value. </value>
        public T Max { get; private set; }

        /// <summary> Returns the start or minimum value of the interval bounds. </summary>
        /// <value> The minimum value. </value>
        public T Min { get; private set; }

        /// <summary> Sets the interval bounds based on the extrema. </summary>
        /// <param name="minValue"> Specified the minimum value of the interval bounds. </param>
        /// <param name="maxValue"> Specifies the maximum value of the interval bounds. </param>
        public void SetBounds( T minValue, T maxValue )
        {
            this.SetItervalBoundsInternal( minValue, maxValue );
        }

        /// <summary> Returns the default string representation of the interval bounds. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> The formatted string representation of the interval bounds, e.g., '(min,max)'. </returns>
        public override string ToString()
        {
            return ToString( this.Min, this.Max );
        }

        /// <summary> Returns the default string representation of the interval bounds. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="min"> Returns the start or minimum value of the interval bounds. </param>
        /// <param name="max"> Returns the end or maximum value of the interval bounds. </param>
        /// <returns> The formatted string representation of the interval bounds, e.g., '(min,max)'. </returns>
        public static string ToString( T min, T max )
        {
            return $"({min},{max})";
        }

        #endregion

        #region " bounds checks "

        /// <summary> Query if 'point' is inside the range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="point"> A <see cref="T:System.Double">Double</see> point value&gt; </param>
        /// <returns> <c>true</c> if inside; otherwise <c>false</c> </returns>
        public bool Encloses( T point )
        {
            return (point.CompareTo( this.Min ) > 0) && (point.CompareTo( this.Max ) < 0);
        }

        /// <summary> Returns true if the point value is within the range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="point"> A <see cref="T:System.Double">Double</see> point value&gt; </param>
        /// <returns>
        /// <c>True</c> if value above or equal to minimum or below or equal to maximum.
        /// </returns>
        public bool Contains( double point )
        {
            return (point.CompareTo( this.Min ) >= 0) && (point.CompareTo( this.Max ) <= 0);
        }



        #endregion

    }
}
