using System;
using System.Collections.Generic;
using System.Linq;

namespace isr.Core.Cartesian
{

    /// <summary> Simple linear regression. </summary>
    /// <remarks>
    /// David, 2016-02-06. https://en.wikipedia.org/wiki/Simple_linear_regression <para>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
    /// Licensed under The MIT License.</para> <para></para>
    /// </remarks>
    public class LinearFit
    {

        #region " CONSTRUCTION "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        public LinearFit() : base()
        {
            this.GoodnessOfFit = double.NaN;
            this.LinearCoefficientStandardError = double.NaN;
            this.ConstantCoefficientStandardError = double.NaN;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="constantCoefficient"> The constant coefficient. </param>
        /// <param name="linearCoefficient">   The linear coefficient. </param>
        public LinearFit( double constantCoefficient, double linearCoefficient ) : this()
        {
            this.ConstantCoefficient = constantCoefficient;
            this.LinearCoefficient = linearCoefficient;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> Information describing the polynomial. </param>
        public LinearFit( LinearFit value ) : this()
        {
            if ( value is object )
            {
                this.ConstantCoefficient = value.ConstantCoefficient;
                this.LinearCoefficient = value.LinearCoefficient;
            }
        }

        /// <summary> Gets the constant coefficient. </summary>
        /// <value> The constant coefficient. </value>
        public double ConstantCoefficient { get; set; }

        /// <summary> Gets the linear coefficient. </summary>
        /// <value> The linear coefficient. </value>
        public double LinearCoefficient { get; set; }

        #endregion

        #region " EVALUATION "

        /// <summary> Evaluates the line at the specified value. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> The value for evaluating the list. </param>
        /// <returns> A Double. </returns>
        public double Evaluate( double value )
        {
            return this.ConstantCoefficient + value * this.LinearCoefficient;
        }

        #endregion

        #region " STATISTICS AND LINE VALUES "

        /// <summary> Gets the number of. </summary>
        /// <value> The count. </value>
        public int Count { get; private set; }

        /// <summary> Gets the ordinate mean. </summary>
        /// <value> The ordinate mean. </value>
        public double OrdinateMean { get; private set; }

        /// <summary> Gets the abscissa mean. </summary>
        /// <value> The abscissa mean. </value>
        public double AbscissaMean { get; private set; }

        /// <summary>
        /// Gets the residual sum of squares, or SSR (sum of squares residual); equals the sum of squares
        /// of the difference between the estimated and actual values<code>
        /// Sum{Squared[a + b*X.i - Y.i]}</code>
        /// </summary>
        /// <value> The residual sum of squares. </value>
        public double ResidualSumOfSquares { get; private set; }

        /// <summary>
        /// Gets the regression sum of squares or the model deviation from the mean ordinate. It measures
        /// how far the regression line is from the average ordinate value. <code>
        /// Sum{Squared[a + b*X.i - Average(Y.i)]}</code>
        /// </summary>
        /// <value> The regression sum of squares. </value>
        public double RegressionSumOfSquares { get; private set; }

        /// <summary> Gets the explained sum of squares. </summary>
        /// <value> The explained sum of squares. </value>
        public double ExplainedSumOfSquares => this.RegressionSumOfSquares;

        /// <summary>
        /// Gets the total sum of squares, SST equals the sum of squares of the ordinate values from the
        /// mean ordinate value<code>
        /// Sum{Squared[Y.i - Average(Y.i)]}</code>
        /// </summary>
        /// <value> The total number of sum of squares. </value>
        public double TotalSumOfSquares { get; private set; }

        /// <summary>
        /// Gets the sum of squares of the abscissa deviation from the mean:<code>
        /// Sum{Squared[X.i - Average(X.i)]}</code>
        /// </summary>
        /// <value> The abscissa sum of squares. </value>
        public double AbscissaSumOfSquareDeviations { get; private set; }

        /// <summary> Gets the abscissa sum of squares. </summary>
        /// <value> The abscissa sum of squares. </value>
        public double AbscissaSumOfSquares => this.AbscissaSumOfSquareDeviations + this.Count * this.AbscissaMean * this.AbscissaMean;

        /// <summary>
        /// Gets the sum of the product abscissa and ordinate deviations for their respective means<code>
        /// Sum{[X.i - Average(X.i)]*[Y.i - Average(Y.i)]}</code>
        /// </summary>
        /// <value> The total number of sum of squares. </value>
        public double CovarianceSum { get; private set; }

        /// <summary> Gets the degrees of freedom. </summary>
        /// <value> The degrees of freedom. </value>
        public int DegreesOfFreedom => this.Count - 2;

        /// <summary>   Gets the statistic. </summary>
        /// <remarks>
        /// Under H0: Slope = 0, the F statistic has an F distribution with (1, n-2)
        /// degrees of freedom. The P value is the area to the right for F statistic under the F(1,n-2) frequency curve. H0 is
        /// rejected if P is less than the significance level (alpha). 
        /// </remarks>
        /// <value> The f statistic or infinity if the fit is perfect or the number of decrees of freedom is one. </value>
        public double FStatistic => (this.ResidualSumOfSquares == 0)
                                        ? double.PositiveInfinity
                                        : this.DegreesOfFreedom * (this.TotalSumOfSquares / this.ResidualSumOfSquares - 1d);

        /// <summary> Gets the Student-T statistic. </summary>
        /// <value> The Student-T  statistic. </value>
        public double TStatistic => double.IsInfinity( this.FStatistic ) ? double.PositiveInfinity : Math.Sqrt( this.FStatistic );

        /// <summary>   Gets the standard error also called the standard error of the estimate. </summary>
        /// <remarks>
        /// This variance of the estimate is also called the deviation about regression (or residual)
        /// mean square.
        /// </remarks>
        /// <value> The standard error of the estimate. </value>
        public double StandardError => this.DegreesOfFreedom == 0 ? 0 : Math.Sqrt( this.ResidualSumOfSquares / this.DegreesOfFreedom );

        #endregion

        #region " GOODNESS OF FIT "

        /// <summary> Gets the standard error of the constant coefficient. </summary>
        /// <value> The constant coefficient standard error. </value>
        /// <remarks> Under H0, this has a student's t distribution with n - 2 degrees of freedom.
        /// </remarks>
        public double ConstantCoefficientStandardError { get; private set; }

        /// <summary> Gets the constant coefficient confidence interval. </summary>
        /// <remarks>
        /// The 95% confident interval of the coefficient is given by <code>mean +/- (std error) *
        /// t(0.975,n)</code>
        /// where t(0.975,n) is the 0.975 quantile of Student's t-distribution with n degrees of freedom,
        /// e.g., 2.1604 for 13 degrees of freedom. This property uses a ball park value of 2 for the
        /// 0.974 quartile.
        /// </remarks>
        /// <value> The constant coefficient confidence interval. </value>
        public ConfidenceInterval<double> ConstantCoefficientConfidenceInterval => new( this.ConstantCoefficient - 2d * this.ConstantCoefficientStandardError, this.ConstantCoefficient + 2d * this.ConstantCoefficientStandardError );

        /// <summary> Gets the standard error of the linear coefficient. </summary>
        /// <remarks> This is also called the standard error of the regression coefficient.
        /// Under H0, this has a student's t distribution with n - 2 degrees of freedom.
        /// The 95% confident interval of the coefficient is given by <code>mean +/- (std error) *
        /// t(0.975,n)</code>
        /// where t(0.975,n) is the 0.975 quantile of Student's t-distribution with n degrees of freedom,
        /// e.g., 2.1604 for 13 degrees of freedom, and thus.
        /// </remarks>
        /// <value> The linear coefficient standard error. </value>
        public double LinearCoefficientStandardError { get; private set; }

        /// <summary> Gets the Linear coefficient confidence interval. </summary>
        /// <remarks>
        /// The 95% confident interval of the coefficient is given by <code>mean +/- (std error) *
        /// t(0.975,n)</code>
        /// where t(0.975,n) is the 0.975 quantile of Student's t-distribution with n degrees of freedom,
        /// e.g., 2.1604 for 13 degrees of freedom. This property uses a ball park value of 2 for the
        /// 0.974 quartile.
        /// </remarks>
        /// <value> The Linear coefficient confidence interval. </value>
        public ConfidenceInterval<double> LinearCoefficientConfidenceInterval => new( this.LinearCoefficient - 2d * this.LinearCoefficientStandardError, this.LinearCoefficient + 2d * this.LinearCoefficientStandardError );

        /// <summary> Gets the goodness of fit, or R-Squared; = 1 - SSR/SST. </summary>
        /// <value> The goodness of fit (R-Squared). </value>
        public double GoodnessOfFit { get; private set; }

        /// <summary> Gets the correlation coefficient. </summary>
        /// <value> The correlation coefficient. </value>
        public double CorrelationCoefficient => Math.Sqrt( this.GoodnessOfFit );

        /// <summary>
        /// Calculates the sum of squared deviations of the model from the actual values.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values"> The values. </param>
        /// <returns> The total number of squared deviations. </returns>
        public double CalculateResidualSumOfSquares( IList<CartesianPoint<double>> values )
        {
            if ( values is null )
            {
                throw new ArgumentNullException( nameof( values ) );
            }

            double result = 0d;
            foreach ( CartesianPoint<double> p in values )
            {
                double temp = p.Y - this.Evaluate( p.X );
                result += temp * temp;
            }

            return result;
        }

        /// <summary>
        /// Calculates the sum of squared deviations of the model from the actual values.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="abscissa"> The abscissa. </param>
        /// <param name="ordinate"> The ordinate. </param>
        /// <returns> The total number of squared deviations. </returns>
        public double CalculateResidualSumOfSquares( IList<double> abscissa, IList<double> ordinate )
        {
            if ( abscissa is null )
            {
                throw new ArgumentNullException( nameof( abscissa ) );
            }

            if ( ordinate is null )
            {
                throw new ArgumentNullException( nameof( ordinate ) );
            }

            if ( abscissa.Count != ordinate.Count )
            {
                throw new InvalidOperationException( $"Abscissa counts {abscissa.Count} must equal ordinate counts {ordinate.Count}" );
            }

            double result = 0d;
            if ( abscissa.Any() )
            {
                for ( int i = 0, loopTo = abscissa.Count - 1; i <= loopTo; i++ )
                {
                    double temp = ordinate[i] - this.Evaluate( abscissa[i] );
                    result += temp * temp;
                }
            }

            return result;
        }

        /// <summary>
        /// Calculates the sum of squares of the deviations of the ordinate from the mean.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values"> The values. </param>
        /// <returns> The total number of squared mean deviations. </returns>
        public static double CalculateSumOfOrdinateSquaredMeanDeviations( IList<CartesianPoint<double>> values )
        {
            if ( values is null )
            {
                throw new ArgumentNullException( nameof( values ) );
            }

            double mean = 0d;
            double result = 0d;
            if ( values.Any() )
            {
                foreach ( CartesianPoint<double> p in values )
                {
                    mean += p.Y;
                }

                mean /= values.Count;
                result = CalculateSumOfOrdinateSquaredMeanDeviations( values, mean );
            }

            return result;
        }

        /// <summary>
        /// Calculates the sum of squares of the deviations of the ordinate from the mean.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values"> The values. </param>
        /// <param name="mean">   The mean. </param>
        /// <returns> The total number of squared mean deviations. </returns>
        public static double CalculateSumOfOrdinateSquaredMeanDeviations( IList<CartesianPoint<double>> values, double mean )
        {
            if ( values is null )
            {
                throw new ArgumentNullException( nameof( values ) );
            }

            double result = 0d;
            foreach ( CartesianPoint<double> p in values )
            {
                double temp = p.Y - mean;
                result += temp * temp;
            }

            return result;
        }

        /// <summary> Calculates the sum of squares of  deviations from the mean. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values"> The ordinate. </param>
        /// <returns> The total number of squared mean deviations. </returns>
        public static double CalculateSumOfSquaredMeanDeviations( IList<double> values )
        {
            if ( values is null )
            {
                throw new ArgumentNullException( nameof( values ) );
            }

            double result = 0d;
            double mean = 0d;
            if ( values.Any() )
            {
                foreach ( double y in values )
                {
                    mean += y;
                }

                mean /= values.Count;
                result = CalculateSumOfSquaredMeanDeviations( values, mean );
            }

            return result;
        }

        /// <summary> Calculates the sum of squares of  deviations from the mean. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values"> The ordinate. </param>
        /// <param name="mean">   The mean. </param>
        /// <returns> The total number of squared mean deviations. </returns>
        public static double CalculateSumOfSquaredMeanDeviations( IList<double> values, double mean )
        {
            if ( values is null )
            {
                throw new ArgumentNullException( nameof( values ) );
            }

            double result = 0d;
            foreach ( double value in values )
            {
                double temp = value - mean;
                result += temp * temp;
            }

            return result;
        }

        /// <summary> Calculates the sum of squares of  deviations from the mean. </summary>
        /// <remarks> David, 2020-09-05. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values"> The ordinate. </param>
        /// <returns> The total number of squared mean deviations. </returns>
        public static double CalculateSumOfSquaredMeanDeviations( double[] values )
        {
            if ( values is null )
            {
                throw new ArgumentNullException( nameof( values ) );
            }

            double result = 0d;
            double mean = 0d;
            if ( values.Any() )
            {
                foreach ( double y in values )
                {
                    mean += y;
                }

                mean /= values.Length;
                result = CalculateSumOfSquaredMeanDeviations( values, mean );
            }

            return result;
        }

        /// <summary> Calculates the sum of squares of  deviations from the mean. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values"> The ordinate. </param>
        /// <param name="mean">   The mean. </param>
        /// <returns> The total number of squared mean deviations. </returns>
        public static double CalculateSumOfSquaredMeanDeviations( double[] values, double mean )
        {
            if ( values is null )
            {
                throw new ArgumentNullException( nameof( values ) );
            }

            double result = 0d;
            foreach ( double value in values )
            {
                double temp = value - mean;
                result += temp * temp;
            }

            return result;
        }


        #endregion

        #region " LINEAR FIT "

        /// <summary> Fits a line to the values. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        private void DoFit()
        {
            if ( this.AbscissaSumOfSquareDeviations > 0d )
            {
                this.LinearCoefficient = this.CovarianceSum / this.AbscissaSumOfSquareDeviations;
                this.ConstantCoefficient = this.OrdinateMean - this.LinearCoefficient * this.AbscissaMean;
            }
            else
            {
                this.LinearCoefficient = 0d;
                this.ConstantCoefficient = this.OrdinateMean - this.LinearCoefficient * this.AbscissaMean;
            }
        }

        /// <summary> Summarizes; Calculates residual and standard errors. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        private void Summarize()
        {
            if ( this.DegreesOfFreedom == 0 )
            {
                this.LinearCoefficientStandardError = 0;
                this.ConstantCoefficientStandardError = 0;
            }
            else if ( this.AbscissaSumOfSquareDeviations > 0d )
            {
                this.LinearCoefficientStandardError = Math.Sqrt( this.ResidualSumOfSquares / (this.DegreesOfFreedom * this.AbscissaSumOfSquareDeviations) );
                this.ConstantCoefficientStandardError = this.LinearCoefficientStandardError * Math.Sqrt( this.AbscissaSumOfSquares / this.Count );
            }
            else
            {
                this.LinearCoefficientStandardError = 0d;
                this.ConstantCoefficientStandardError = this.ConstantCoefficient;
            }

            if ( this.TotalSumOfSquares - this.ResidualSumOfSquares > float.Epsilon )
            {
                // otherwise, correlation coefficient could be negative
                this.GoodnessOfFit = this.TotalSumOfSquares > 0d ? 1d - this.ResidualSumOfSquares / this.TotalSumOfSquares : 0d;
            }
            else
            {
                this.GoodnessOfFit = 1d;
            }
        }

        #endregion

        #region " LINEAR FIT: ARRAYS "

        /// <summary> Initializes sum and sums of squares and initial values. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="abscissa"> The abscissa. </param>
        /// <param name="ordinate"> The ordinate. </param>
        private void Initialize( double[] abscissa, double[] ordinate )
        {
            if ( abscissa is null )
            {
                throw new ArgumentNullException( nameof( abscissa ) );
            }

            if ( ordinate is null )
            {
                throw new ArgumentNullException( nameof( ordinate ) );
            }

            if ( abscissa.Length != ordinate.Length )
            {
                throw new InvalidOperationException( $"Abscissa counts {abscissa.Length} must equal ordinate counts {ordinate.Length}" );
            }

            this.Count = abscissa.Length;
            this.ConstantCoefficient = 0d;
            this.LinearCoefficient = 0d;
            double x = 0d;
            double y = 0d;
            for ( int i = 0, loopTo = this.Count - 1; i <= loopTo; i++ )
            {
                y += ordinate[i];
                x += abscissa[i];
            }

            this.OrdinateMean = y / this.Count;
            this.AbscissaMean = x / this.Count;
            double cov = 0d;
            double xdev = 0d;
            for ( int i = 0, loopTo1 = this.Count - 1; i <= loopTo1; i++ )
            {
                x = abscissa[i];
                double temp = x - this.AbscissaMean;
                xdev += temp * temp;
                cov += temp * (ordinate[i] - this.OrdinateMean);
            }

            this.CovarianceSum = cov;
            this.AbscissaSumOfSquareDeviations = xdev;
            this.GoodnessOfFit = double.NaN;
            this.LinearCoefficientStandardError = double.NaN;
            this.ConstantCoefficientStandardError = double.NaN;
        }

        /// <summary> Executes the fit operation. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="abscissa"> The abscissa. </param>
        /// <param name="ordinate"> The ordinate. </param>
        /// <returns> A measure of the goodness of fit. </returns>
        public double DoFit( double[] abscissa, double[] ordinate )
        {
            if ( abscissa is null )
            {
                throw new ArgumentNullException( nameof( abscissa ) );
            }

            if ( ordinate is null )
            {
                throw new ArgumentNullException( nameof( ordinate ) );
            }

            if ( abscissa.Length != ordinate.Length )
            {
                throw new InvalidOperationException( $"Abscissa counts {abscissa.Length} must equal ordinate counts {ordinate.Length}" );
            }

            this.Initialize( abscissa, ordinate );
            this.DoFit();
            this.ResidualSumOfSquares = this.CalculateResidualSumOfSquares( abscissa, ordinate );
            this.TotalSumOfSquares = CalculateSumOfSquaredMeanDeviations( ordinate, this.OrdinateMean );
            this.Summarize();
            return this.GoodnessOfFit;
        }

        #endregion

        #region " LINEAR FIT: LIST "

        /// <summary> Initializes sum and sums of squares and initial values. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="abscissa"> The abscissa. </param>
        /// <param name="ordinate"> The ordinate. </param>
        private void Initialize( IList<double> abscissa, IList<double> ordinate )
        {
            if ( abscissa is null )
            {
                throw new ArgumentNullException( nameof( abscissa ) );
            }

            if ( ordinate is null )
            {
                throw new ArgumentNullException( nameof( ordinate ) );
            }

            if ( abscissa.Count != ordinate.Count )
            {
                throw new InvalidOperationException( $"Abscissa counts {abscissa.Count} must equal ordinate counts {ordinate.Count}" );
            }

            this.Count = abscissa.Count;
            this.ConstantCoefficient = 0d;
            this.LinearCoefficient = 0d;
            double x = 0d;
            double y = 0d;
            for ( int i = 0, loopTo = this.Count - 1; i <= loopTo; i++ )
            {
                y += ordinate[i];
                x += abscissa[i];
            }

            this.OrdinateMean = y / this.Count;
            this.AbscissaMean = x / this.Count;
            double cov = 0d;
            double xdev = 0d;
            for ( int i = 0, loopTo1 = this.Count - 1; i <= loopTo1; i++ )
            {
                x = abscissa[i];
                double temp = x - this.AbscissaMean;
                xdev += temp * temp;
                cov += temp * (ordinate[i] - this.OrdinateMean);
            }

            this.CovarianceSum = cov;
            this.AbscissaSumOfSquareDeviations = xdev;
            this.GoodnessOfFit = double.NaN;
            this.LinearCoefficientStandardError = double.NaN;
            this.ConstantCoefficientStandardError = double.NaN;
        }

        /// <summary> Executes the fit operation. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="abscissa"> The abscissa. </param>
        /// <param name="ordinate"> The ordinate. </param>
        /// <returns> A measure of the goodness of fit. </returns>
        public double DoFit( IList<double> abscissa, IList<double> ordinate )
        {
            if ( abscissa is null )
            {
                throw new ArgumentNullException( nameof( abscissa ) );
            }

            if ( ordinate is null )
            {
                throw new ArgumentNullException( nameof( ordinate ) );
            }

            if ( abscissa.Count != ordinate.Count )
            {
                throw new InvalidOperationException( $"Abscissa counts {abscissa.Count} must equal ordinate counts {ordinate.Count}" );
            }

            this.Initialize( abscissa, ordinate );
            this.DoFit();
            this.ResidualSumOfSquares = this.CalculateResidualSumOfSquares( abscissa, ordinate );
            this.TotalSumOfSquares = CalculateSumOfSquaredMeanDeviations( ordinate, this.OrdinateMean );
            this.Summarize();
            return this.GoodnessOfFit;
        }

        #endregion

        #region " LINEAR FIT - POINTS "

        /// <summary> Initializes sum and sums of squares and initial values. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="values"> The values. </param>
        private void Initialize( IList<CartesianPoint<double>> values )
        {
            if ( values is null )
            {
                throw new ArgumentNullException( nameof( values ) );
            }

            if ( !values.Any() )
            {
                throw new InvalidOperationException( "Input is empty." );
            }

            this.Count = values.Count;
            this.ConstantCoefficient = 0d;
            this.LinearCoefficient = 0d;
            double x = 0d;
            double y = 0d;
            foreach ( CartesianPoint<double> p in values )
            {
                y += p.Y;
                x += p.X;
            }

            this.OrdinateMean = y / this.Count;
            this.AbscissaMean = x / this.Count;
            double cov = 0d;
            double xdev = 0d;
            foreach ( CartesianPoint<double> p in values )
            {
                x = p.X;
                double temp = x - this.AbscissaMean;
                xdev += temp * temp;
                cov += temp * (p.Y - this.OrdinateMean);
            }

            this.CovarianceSum = cov;
            this.GoodnessOfFit = double.NaN;
            this.AbscissaSumOfSquareDeviations = xdev;
            this.LinearCoefficientStandardError = double.NaN;
            this.ConstantCoefficientStandardError = double.NaN;
        }

        /// <summary> Executes the fit operation. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="values"> The values. </param>
        /// <returns> A measure of the goodness of fit. </returns>
        public double DoFit( IList<CartesianPoint<double>> values )
        {
            if ( values is null )
            {
                throw new ArgumentNullException( nameof( values ) );
            }

            if ( !values.Any() )
            {
                throw new InvalidOperationException( "Input is empty." );
            }

            this.Initialize( values );
            this.DoFit();
            this.ResidualSumOfSquares = this.CalculateResidualSumOfSquares( values );
            this.TotalSumOfSquares = CalculateSumOfOrdinateSquaredMeanDeviations( values, this.OrdinateMean );
            this.Summarize();
            return this.GoodnessOfFit;
        }

        #endregion

    }
}
