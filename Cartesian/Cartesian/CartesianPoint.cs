using System;

namespace isr.Core.Cartesian
{

    /// <summary> Implements a generic Cartesian point structure. </summary>
    /// <remarks>
    /// (c) 2006 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2006-04-10, 1.1.2291. </para>
    /// </remarks>
    public struct CartesianPoint<T> where T : IComparable<T>, IEquatable<T>, IFormattable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="CartesianPoint{T}" /> structure. The copy constructor.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="model"> The  <see cref="CartesianPoint{T}">Point</see> object from which to copy. </param>
        public CartesianPoint( CartesianPoint<T> model ) : this( model.X, model.Y )
        {
        }

        /// <summary> Initializes a new instance of the <see cref="CartesianPoint{T}" /> structure. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="x"> Specifies the X coordinate of the point. </param>
        /// <param name="y"> Specifies the Y coordinate of the point. </param>
        public CartesianPoint( T x, T y )
        {
            this.X = x;
            this.Y = y;
        }

        #endregion

        #region " EQUALITY IMPLEMENTATION"

        /// <summary> Compares two points. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  Specifies the point to compare. </param>
        /// <param name="right"> Specifies the point to compare with. </param>
        /// <returns> <c>True</c> if the points are equal. </returns>
        public static new bool Equals( object left, object right )
        {
            return Equals( ( CartesianPoint<T> ) left, ( CartesianPoint<T> ) right );
        }

        /// <summary> Compares two points. </summary>
        /// <remarks> The two points are the same if they have the same X and Y coordinates. </remarks>
        /// <param name="left">  Specifies the point to compare. </param>
        /// <param name="right"> Specifies the point to compare with. </param>
        /// <returns> <c>True</c> if the points are equal. </returns>
        public static bool Equals( CartesianPoint<T> left, CartesianPoint<T> right )
        {
            return left.X.Equals( right.X ) && left.Y.Equals( right.Y );
        }

        /// <summary> Implements the operator =. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( CartesianPoint<T> left, CartesianPoint<T> right )
        {
            return Equals( left, right );
        }

        /// <summary> Implements the operator &lt;&gt;. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( CartesianPoint<T> left, CartesianPoint<T> right )
        {
            return !left.Equals( right );
        }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
        /// <see cref="T:System.Object" />. </param>
        /// <returns>
        /// <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />; otherwise, false.
        /// </returns>
        public override bool Equals( object obj )
        {
            return this.Equals( ( CartesianPoint<T> ) obj );
        }

        /// <summary> Compares two points. </summary>
        /// <remarks> The two points are the same if they have the same X and Y coordinates. </remarks>
        /// <param name="other"> Specifies the other point to compare. </param>
        /// <returns> <c>True</c> if the points are equal. </returns>
        public bool Equals( CartesianPoint<T> other )
        {
            return Equals( this, other );
        }

        /// <summary> Creates a unique hash code. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> An <see cref="T:System.Int32">Int32</see> value. </returns>
        public override int GetHashCode()
        {
            return new Tuple<int, int>( this.X.GetHashCode(), this.Y.GetHashCode() ).GetHashCode();
        }

        #endregion

        #region " METHODS AND PROPERTIES "

        /// <summary> Holds the Y coordinate of the point. </summary>
        /// <value> The y coordinate. </value>
        public T Y { get; set; }

        /// <summary> Holds the X coordinate of the point. </summary>
        /// <value> The x coordinate. </value>
        public T X { get; set; }

        /// <summary> Sets the point based on the coordinates. </summary>
        /// <remarks> Use this class to set the point. </remarks>
        /// <param name="x"> Specifies the X coordinate of the point. </param>
        /// <param name="y"> Specifies the Y coordinate of the point. </param>
        public void SetPoint( T x, T y )
        {
            this.X = x;
            this.Y = y;
        }

        /// <summary> Returns the default string representation of the point. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> The formatted string representation of the point, e.g., '(x,y)'. </returns>
        public override string ToString()
        {
            return ToString( this.X, this.Y );
        }

        /// <summary> Returns the default string representation of the point. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="x"> Specifies the X coordinate of the point. </param>
        /// <param name="y"> Specifies the Y coordinate of the point. </param>
        /// <returns> The formatted string representation of the point, e.g., '(x,y)'. </returns>
        public static string ToString( T x, T y )
        {
            return $"({x},{y})";
        }

        #endregion

    }
}
