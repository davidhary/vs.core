using System;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;

namespace isr.Core.Win32
{
    /// <summary>   A native methods. </summary>
    /// <remarks>   David, 2020-12-05. </remarks>
    internal class NativeMethods
    {

        /// <summary>   Closes a handle. Frees the kernel's file object (close the file). </summary>
        /// <remarks>   David, 2020-12-05. </remarks>
        /// <param name="handle">   The handle. </param>
        /// <returns>   An Integer. </returns>
        [DllImport( "kernel32", SetLastError = true )]
        [ReliabilityContract( Consistency.WillNotCorruptState, Cer.MayFail )]
        internal static extern bool CloseHandle( IntPtr handle );

    }
}
