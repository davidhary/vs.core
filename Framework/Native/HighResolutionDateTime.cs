using System;
using System.Runtime.InteropServices;

namespace isr.Core.Win32
{

    /// <summary>   A high resolution date time. </summary>
    /// <remarks>   David, 2020-12-07 from <para>
    /// [Sebastian Krysmanski](https://www.codeproject.com/articles/792410/high-resolution-clock-in-csharp) </para></remarks>
    public static class HighResolutionDateTime
    {
        /// <summary>   Gets or sets a value indicating whether this object is available. </summary>
        /// <value> True if this object is available, false if not. </value>
        public static bool IsAvailable { get; private set; }

        [DllImport( "Kernel32.dll", CallingConvention = CallingConvention.Winapi )]
        private static extern void GetSystemTimePreciseAsFileTime( out long filetime );

        /// <summary>   Gets the Date/Time of the UTC now. </summary>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <value> The UTC now. </value>
        public static DateTime UtcNow
        {
            get {
                if ( !IsAvailable )
                {
                    throw new InvalidOperationException( "High resolution clock isn't available." );
                }

                GetSystemTimePreciseAsFileTime( out long filetime );

                return DateTime.FromFileTimeUtc( filetime );
            }
        }

        /// <summary>   Static constructor. </summary>
        /// <remarks>   David, 2020-12-07. </remarks>
        static HighResolutionDateTime()
        {
            try
            {
                GetSystemTimePreciseAsFileTime( out long filetime );
                IsAvailable = true;
            }
            catch ( EntryPointNotFoundException )
            {
                // Not running Windows 8 or higher.
                IsAvailable = false;
            }
        }
    }

}
