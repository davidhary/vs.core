using System;
using System.Runtime.ConstrainedExecution;
using System.Security.Permissions;

using Microsoft.Win32.SafeHandles;

namespace isr.Core.Win32
{
    /// <summary>   Safe handle. </summary>
    /// <remarks>   David, 2020-05-01.
    /// https://docs.microsoft.com/en-us/dotnet/api/system.runtime.interopservices.safehandle?view=netframework-4.8
    /// when to use a safe handle:
    /// https://stackoverflow.com/questions/155780/safehandle-in-c-sharp
    /// </remarks>
    [SecurityPermission( SecurityAction.InheritanceDemand, UnmanagedCode = true )]
    [SecurityPermission( SecurityAction.Demand, UnmanagedCode = true )]
    public class SafeHandle : SafeHandleZeroOrMinusOneIsInvalid
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>
        /// David, 2020-12-05. <para>
        /// Creates a Safe Handle, informing the base class that this Safe Handle instance "owns" the
        /// handle, and therefore Safe Handle should call our ReleaseHandle method when the Safe Handle is
        /// no longer in use. </para>
        /// </remarks>
        public SafeHandle() : base( true )
        {
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>
        /// David, 2020-12-05. <para>
        /// Creates a Safe Handle, informing the base class that this Safe Handle instance "owns" the
        /// handle, and therefore Safe Handle should call our ReleaseHandle method when the Safe Handle is
        /// no longer in use. </para>
        /// </remarks>
        /// <param name="handle">   The handle. </param>
        public SafeHandle( IntPtr handle ) : base( true )
        {
            this.SetHandle( handle );
        }

        /// <summary>   Sets valid handle. </summary>
        /// <remarks>   David, 2020-12-05. </remarks>
        /// <param name="handle">   The handle. </param>
        public void SetValidHandle( IntPtr handle )
        {
            this.SetHandle( handle );
        }

        /// <summary>
        /// When overridden in a derived class, executes the code required to free the handle.
        /// </summary>
        /// <remarks>   David, 2020-12-05. </remarks>
        /// <returns>
        /// <see langword="true" /> if the handle is released successfully; otherwise, in the event of a
        /// catastrophic failure,<see langword=" false" />. In this case, it generates a
        /// releaseHandleFailed MDA Managed Debugging Assistant.
        /// </returns>
        [ReliabilityContract( Consistency.WillNotCorruptState, Cer.MayFail )]
        protected override bool ReleaseHandle()
        {
            // Here, we must obey all rules for constrained execution regions.
            return NativeMethods.CloseHandle( this.handle );
            // If ReleaseHandle failed, it can be reported via the
            // "releaseHandleFailed" managed debugging assistant (MDA).  This
            // MDA is disabled by default, but can be enabled in a debugger
            // or during testing to diagnose handle corruption problems.
            // We do not throw an exception because most code could not recover
            // from the problem.
        }
    }
}
