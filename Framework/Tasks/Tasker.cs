using System;
using System.ComponentModel;
using System.Threading.Tasks;

namespace isr.Core
{

    /// <summary> Encapsulates action and awaiting tasks. </summary>
    /// <remarks>
    /// David, 2020-07-17. (c) 2020 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public class Tasker : IDisposable
    {

        #region " CONSTRUCTION ADN CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks> David, 2020-07-17. </remarks>
        public Tasker() : base()
        {
        }

        /// <summary> True to disposed value. </summary>
        private bool _DisposedValue;

        /// <summary>
        /// Releases the unmanaged resources used by the isr.Core.Tasker and optionally releases
        /// the managed resources.
        /// </summary>
        /// <remarks> David, 2020-07-17. </remarks>
        /// <param name="disposing"> True to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected virtual void Dispose( bool disposing )
        {
            if ( !this._DisposedValue )
            {
                if ( disposing )
                {
                    this.ActionTask?.Dispose();
                    this.AsyncTask?.Dispose();
                }

                this._DisposedValue = true;
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-07-17. </remarks>
        public void Dispose()
        {
            this.Dispose( disposing: true );
            GC.SuppressFinalize( this );
        }

        #endregion

        /// <summary> Event queue for all listeners interested in AsyncCompleted events. </summary>
        public event AsyncCompletedEventHandler AsyncCompleted;

        /// <summary> Gets or sets the asynchronous task. </summary>
        /// <remarks> This property receives the return value from the task onsetting function. </remarks>
        /// <value> The asynchronous task. </value>
        private Task AsyncTask { get; set; }

        /// <summary> Gets or sets the action task. </summary>
        /// <remarks> This task can be monitored for status and can be awaited. </remarks>
        /// <value> The action task. </value>
        public Task ActionTask { get; private set; }

        /// <summary> Query if 'status' is task ended. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="status"> The status. </param>
        /// <returns> <c>true</c> if task ended; otherwise <c>false</c> </returns>
        private static bool IsTaskEnded( TaskStatus status )
        {
            return status == TaskStatus.RanToCompletion || status == TaskStatus.Canceled || status == TaskStatus.Faulted;
        }

        /// <summary> Query if this object is busy. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns> <c>true</c> if busy; otherwise <c>false</c> </returns>
        public bool IsBusy()
        {
            return !this._DisposedValue && this.ActionTask is object && !IsTaskEnded( this.ActionTask.Status );
        }

        /// <summary> Starts action asynchronous. </summary>
        /// <remarks>
        /// David, 2020-07-17. A continuation task is used to invoke the <see cref="AsyncCompleted"/> even
        /// in case of task fault. A continuation task, which was described on stack overflow, is
        /// augmented to invoke the completion event:
        /// https://stackoverflow.com/questions/32067034/how-to-handle-task-run-exception.
        /// </remarks>
        /// <param name="action"> The action. </param>
        /// <returns> An asynchronous result. </returns>
        public async Task StartActionAsync( Action action )
        {
            this.ActionTask = Task.Run( action ).ContinueWith( t => { var evt = AsyncCompleted; evt?.Invoke( this, new AsyncCompletedEventArgs( t.Exception, false, null ) ); } );
            await this.ActionTask;
        }

        /// <summary>   Starts action asynchronous. </summary>
        /// <remarks>
        /// David, 2020-07-17. A continuation task is used to invoke the <see cref="AsyncCompleted"/>
        /// even in case of task fault. A continuation task, which was described on stack overflow, is
        /// augmented to invoke the completion event:
        /// https://stackoverflow.com/questions/32067034/how-to-handle-task-run-exception.
        /// </remarks>
        /// <param name="action">                           The action. </param>
        /// <param name="invokeAsyncCompletedIfFaultOnly"> True to invoke asynchronous completed if
        ///                                                 fault only. </param>
        /// <returns>   An asynchronous result. </returns>
        public async Task StartActionAsync( Action action, bool invokeAsyncCompletedIfFaultOnly )
        {
            this.ActionTask = Task.Run( action ).ContinueWith( t => {
                if ( !invokeAsyncCompletedIfFaultOnly || (t.IsFaulted && invokeAsyncCompletedIfFaultOnly) )
                {
                    var evt = AsyncCompleted;
                    evt?.Invoke( this, new AsyncCompletedEventArgs( t.Exception, false, null ) );
                }
            } );
            await this.ActionTask;
        }

        /// <summary> Starts an action. </summary>
        /// <remarks> David, 2020-07-17. </remarks>
        /// <param name="action"> The action. </param>
        public void StartAction( Action action )
        {
            this.AsyncTask = this.StartActionAsync( action );
        }

        /// <summary> Awaits the given timeout for the task to idle. </summary>
        /// <remarks> David, 2020-07-17. </remarks>
        /// <param name="timeout"> The timeout. </param>
        /// <returns> True if the task idled within the given timespan, false if timeout. </returns>
        public bool AwaitTaskIdle( TimeSpan timeout )
        {
            return this.ActionTask.Wait( timeout );
        }

        /// <summary> Await completion. </summary>
        /// <remarks> David, 2020-08-05. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <exception cref="OperationFailedException">  Thrown when operation failed to execute. </exception>
        /// <param name="timeout"> The timeout. </param>
        /// <returns> The Threading.Tasks.TaskStatus. </returns>
        public TaskStatus AwaitCompletion( TimeSpan timeout )
        {
            return this.ActionTask is null
                ? throw new InvalidOperationException( "Asynchronous task has not been initiated" )
                : this.AwaitTaskIdle( timeout )
                    ? this.ActionTask.Status
                    : throw new OperationFailedException( $"Timeout awaiting completion of the asynchronous task" );
        }

        /// <summary>   Try await completion. </summary>
        /// <remarks>   David, 2021-06-21. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="timeout">  The timeout. </param>
        /// <returns>   A Tuple. </returns>
        public (bool Succes, TaskStatus Status) TryAwaitCompletion( TimeSpan timeout )
        {
            return this.ActionTask is null
                ? throw new InvalidOperationException( "Asynchronous task has not been initiated" )
                : this.AwaitTaskIdle( timeout )
                    ? (true, this.ActionTask.Status)
                    : (false, this.ActionTask.Status);
        }
    }
}
