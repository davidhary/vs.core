using System;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.NumericExtensions
{
    /// <summary> Includes rounding extension methods. </summary>
    /// <remarks> (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para> </remarks>
    public static partial class NumericExtensionMethods
    {

        #region " DOUBLE "

        /// <summary>
        /// Commercial round or round away from zero as implemented using the Excel Round() function.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> value which to use in the calculation. </param>
        /// <returns> A Double. </returns>
        public static double CommercialRound( this double value )
        {
            value = value - Math.Floor( value ) == 0.5d ? value > 0d ? value + 0.5d : value - 0.5d : Math.Round( value );

            return value;
        }

        /// <summary>
        /// Commercial round or round away from zero as implemented using the Excel Round() function.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value">         value which to use in the calculation. </param>
        /// <param name="decimalPlaces"> The number of decimal places beyond the most significant digit. </param>
        /// <returns> A Double. </returns>
        public static double CommercialRound( this double value, int decimalPlaces )
        {
            double decmalScale = Math.Pow( 10d, decimalPlaces );
            value = (value * decmalScale).CommercialRound() / decmalScale;
            return value;
        }

        /// <summary> Returns a value that is rounded up to the specified significant digit. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value">           value which to use in the calculation. </param>
        /// <param name="relativeDecimal"> The decimal digit relative to the most significant digit to
        /// use. </param>
        /// <returns> A rounded up value to the specified relative digit. </returns>
        /// <example>
        /// Unary.Ceiling(1.123456,2) returns 1.13.<p>
        /// Unary.Ceiling(-1.123456,2) returns -1.12.</p>
        /// </example>
        public static double Ceiling( this double value, int relativeDecimal )
        {
            double decimalDigitValue;

            // check if the value is zero
            if ( value == 0d )
            {

                // if so, return This value
                return value;
            }
            else
            {

                // get the representation of the significant digit
                decimalDigitValue = value.DecimalValue( -relativeDecimal );

                // get the new value
                return decimalDigitValue * Math.Ceiling( value / decimalDigitValue );
            }
        }

        /// <summary> Returns the number of decimal places for displaying the auto value. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> A <see cref="T:System.Double">Double</see> value. </param>
        /// <returns> An Integer. </returns>
        public static int DecimalPlaces( this double value )
        {
            int candidate = 0;
            value = Math.IEEERemainder( Math.Abs( value ), 1d );
            double minimum = value / 1000.0d;
            minimum = minimum < 0.0000001d ? 0.0000001d : minimum;
            while ( value > minimum )
            {
                candidate += 1;
                value = Math.IEEERemainder( 10d * value, 1d );
            }

            return candidate;
        }

        /// <summary> Returns the decimal value of a number. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value">            Value which to use in the calculation. </param>
        /// <param name="decimalLeftShift"> Decimal power by which to shift the digit. </param>
        /// <returns> The decimal value of the value shifted by the specified decimalPlaces. </returns>
        /// <example>
        /// Unary.DecimalValue(1.123,1) returns 10.<p>
        /// Unary.DecimalValue(11.23,1) returns 100.</p><p>
        /// Unary.DecimalValue(-1.123,1) returns 10.</p><p>
        /// Unary.DecimalValue(0.1234,1) returns 1.</p><p>
        /// Unary.DecimalValue(11.12,0) returns 10.</p>
        /// </example>
        public static double DecimalValue( this double value, int decimalLeftShift )
        {

            // check if the value is zero
            if ( value == 0d )
            {

                // if so, return This value
                return 0d;
            }
            else
            {

                // get the representation of the most significant digit
                return Math.Pow( 10d, value.Exponent() + decimalLeftShift );
            }
        }

        /// <summary>
        /// Returns a suggested fixed format based requested most significant decimalPlaces.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value">         Number to format. </param>
        /// <param name="decimalPlaces"> The number of decimal places beyond the most significant digit. </param>
        /// <returns> A fix format string. </returns>
        /// <example>
        /// Unary.FixedFormat(1.123,1) returns 0.0.<p>
        /// Unary.FixedFormat(11.23,1) returns 0.</p><p>
        /// Unary.FixedFormat(-1.123,1) returns 0.0.</p><p>
        /// Unary.FixedFormat(0.1234,1) returns 0.0.</p>
        /// </example>
        public static string FixedFormat( this double value, int decimalPlaces )
        {
            decimalPlaces -= value.Exponent();
            return decimalPlaces > 0 ? "0." + new string( '0', decimalPlaces ) : "0";
        }

        /// <summary> Returns a value that is rounded down to the specified significant digit. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value">           value which to use in the calculation. </param>
        /// <param name="relativeDecimal"> The decimal digit relative to the most significant digit to
        /// use. </param>
        /// <returns> A rounded down value to the specified relative digit. </returns>
        /// <example>
        /// Unary.Floor(1.123,1) returns 1.1.<p>
        /// Unary.Floor(11.23,1) returns 11.</p><p>
        /// Unary.Floor(-1.123,1) returns -1.3.</p><p>
        /// Unary.Floor(0.1234,1) returns 0.12.</p><p>
        /// Unary.Floor(11.12,0) returns 10.</p>
        /// </example>
        public static double Floor( this double value, int relativeDecimal )
        {
            double decimalDigitValue;
            if ( value == 0d )
            {
                // if zero, return this value
                return value;
            }
            else
            {
                // get the representation of the significant digit
                decimalDigitValue = value.DecimalValue( -relativeDecimal );

                // get the new value
                return decimalDigitValue * Math.Floor( value / decimalDigitValue );
            }
        }

        /// <summary> Calculate the modulus (remainder) in a safe manner so that divide by zero errors are
        /// avoided. </summary>
        /// <param name="value">    The divisor. </param>
        /// <param name="dividend"> The dividend. </param>
        /// <returns> the value of the modulus, or zero for the divide-by-zero case. </returns>
        public static double Mod( this double value, double dividend )
        {
            if ( dividend == 0d )
            {
                return 0d;
            }

            double temp = value / dividend;
            return dividend * (temp - Math.Floor( temp ));
        }

        #endregion

        #region " SINGLE "

        /// <summary>
        /// Commercial round or round away from zero as implemented using the Excel Round() function.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> value which to use in the calculation. </param>
        /// <returns> A Single. </returns>
        public static float CommercialRound( this float value )
        {
            return ( float ) (( double ) value).CommercialRound();
        }

        /// <summary>
        /// Commercial round or round away from zero as implemented using the Excel Round() function.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value">         value which to use in the calculation. </param>
        /// <param name="decimalPlaces"> The number of decimal places beyond the most significant digit. </param>
        /// <returns> A Single. </returns>
        public static float CommercialRound( this float value, int decimalPlaces )
        {
            return ( float ) (( double ) value).CommercialRound( decimalPlaces );
        }

        /// <summary> Returns a value that is rounded up to the specified significant digit. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value">           value which to use in the calculation. </param>
        /// <param name="relativeDecimal"> The decimal digit relative to the most significant digit to
        /// use. </param>
        /// <returns> A rounded up value to the specified relative digit. </returns>
        /// <example>
        /// Unary.Ceiling(1.123456,2) returns 1.13.<p>
        /// Unary.Ceiling(-1.123456,2) returns -1.12.</p>
        /// </example>
        public static float Ceiling( this float value, int relativeDecimal )
        {
            return ( float ) Conversions.ToDouble( value ).Ceiling( relativeDecimal );
        }

        /// <summary> Returns the number of decimal places for displaying the auto value. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> A <see cref="T:System.Single">Single</see> value. </param>
        /// <returns> An Integer. </returns>
        public static int DecimalPlaces( this float value )
        {
            return Conversions.ToDouble( value ).DecimalPlaces();
        }

        /// <summary> Returns the decimal value of a number. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value">            Value which to use in the calculation. </param>
        /// <param name="decimalLeftShift"> Decimal power by which to shift the digit. </param>
        /// <returns> The decimal value of the value shifted by the specified decimalPlaces. </returns>
        /// <example>
        /// Unary.DecimalValue(1.123,1) returns 10.<p>
        /// Unary.DecimalValue(11.23,1) returns 100.</p><p>
        /// Unary.DecimalValue(-1.123,1) returns 10.</p><p>
        /// Unary.DecimalValue(0.1234,1) returns 1.</p><p>
        /// Unary.DecimalValue(11.12,0) returns 10.</p>
        /// </example>
        public static double DecimalValue( this float value, int decimalLeftShift )
        {
            return (( double ) value).DecimalValue( decimalLeftShift );
        }

        /// <summary>
        /// Returns a suggested fixed format based requested most significant decimalPlaces.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value">         Number to format. </param>
        /// <param name="decimalPlaces"> The number of decimal places beyond the most significant digit. </param>
        /// <returns> A fix format string. </returns>
        /// <example>
        /// Unary.FixedFormat(1.123,1) returns 0.0.<p>
        /// Unary.FixedFormat(11.23,1) returns 0.</p><p>
        /// Unary.FixedFormat(-1.123,1) returns 0.0.</p><p>
        /// Unary.FixedFormat(0.1234,1) returns 0.0.</p>
        /// </example>
        public static string FixedFormat( this float value, int decimalPlaces )
        {
            return (( double ) value).FixedFormat( decimalPlaces );
        }

        /// <summary> Returns a value that is rounded down to the specified significant digit. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value">           value which to use in the calculation. </param>
        /// <param name="relativeDecimal"> The decimal digit relative to the most significant digit to
        /// use. </param>
        /// <returns> A rounded down value to the specified relative digit. </returns>
        /// <example>
        /// Unary.Floor(1.123,1) returns 1.1.<p>
        /// Unary.Floor(11.23,1) returns 11.</p><p>
        /// Unary.Floor(-1.123,1) returns -1.3.</p><p>
        /// Unary.Floor(0.1234,1) returns 0.12.</p><p>
        /// Unary.Floor(11.12,0) returns 10.</p>
        /// </example>
        public static double Floor( this float value, int relativeDecimal )
        {
            return (( double ) value).Floor( relativeDecimal );
        }

        /// <summary> Calculate the modulus (remainder) in a safe manner so that divide by zero errors are
        /// avoided. </summary>
        /// <param name="value">    The divisor. </param>
        /// <param name="dividend"> The dividend. </param>
        /// <returns> the value of the modulus, or zero for the divide-by-zero case. </returns>
        public static double Mod( this float value, float dividend )
        {
            return Conversions.ToDouble( value ).Mod( dividend );
        }

        #endregion

    }
}
