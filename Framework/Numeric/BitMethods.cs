namespace isr.Core.NumericExtensions
{

    /// <summary> Bit extension methods. </summary>
    /// <remarks> (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para> </remarks>
    public static partial class NumericExtensionMethods
    {

        /// <summary> Queries if 'status' bit is on. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="status"> The status. </param>
        /// <param name="bit">    The bit. </param>
        /// <returns> <c>true</c> if bit; otherwise <c>false</c> </returns>
        public static bool IsBit( this long status, int bit )
        {
            return (1L & status >> bit) == 1L;
        }

        /// <summary>
        /// Toggles the bits specified in the <paramref name="bit"/> based on the
        /// <paramref name="switch"/>.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="status"> The status. </param>
        /// <param name="bit">    The value. </param>
        /// <param name="switch"> True to set; otherwise, clear. </param>
        /// <returns> A Long. </returns>
        public static long Toggle( this long status, int bit, bool @switch )
        {
            status = @switch ? status | 1L << bit : status & ~(1L << bit);
            return status;
        }
    }
}
