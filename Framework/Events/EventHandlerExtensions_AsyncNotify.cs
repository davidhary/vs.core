
using System;
using System.Collections.Specialized;
using System.ComponentModel;

namespace isr.Core
{

    /// <summary>   An event handler extensions. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    public static partial class EventHandlerExtensions
    {

        /// <summary>
        /// Asynchronously notifies an <see cref="EventHandler{EventArgs}">Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="handler"> The event handler. </param>
        /// <param name="sender">  The sender of the event. </param>
        public static void AsyncNotify( this EventHandler<EventArgs> handler, object sender )
        {
            var evt = handler;
            if ( evt is object )
            {
                AsyncNotifyThis( evt, CurrentSyncContext(), sender, EventArgs.Empty );
            }
        }

        /// <summary>
        /// Asynchronously notifies an <see cref="EventHandler{EventArgs}">Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="handler"> The handler. </param>
        /// <param name="context"> The synchronization context. </param>
        /// <param name="sender">  The sender of the event. </param>
        /// <param name="e">       T event information. </param>
        public static void AsyncNotify<TEventArgs>( this EventHandler<TEventArgs> handler, System.Threading.SynchronizationContext context, object sender, TEventArgs e ) where TEventArgs : EventArgs
        {
            var evt = handler;
            if ( evt is object )
            {
                AsyncNotifyThis( evt, context, sender, e );
            }
        }

        /// <summary>
        /// Asynchronously notifies an <see cref="EventHandler{EventArgs}">Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="evt">     The handler. </param>
        /// <param name="context"> The synchronization context. </param>
        /// <param name="sender">  The sender of the event. </param>
        /// <param name="e">       T event information. </param>
        private static void AsyncNotifyThis<TEventArgs>( EventHandler<TEventArgs> evt, System.Threading.SynchronizationContext context, object sender, TEventArgs e ) where TEventArgs : EventArgs
        {
            context.Post( ( object status ) => evt( sender, e ), null );
        }

        /// <summary>
        /// Asynchronously notifies an <see cref="PropertyChangedEventHandler">Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="handler"> The handler. </param>
        /// <param name="sender">  The sender of the event. </param>
        /// <param name="e">       T event information. </param>
        public static void AsyncNotify( this PropertyChangedEventHandler handler, object sender, PropertyChangedEventArgs e )
        {
            var evt = handler;
            if ( evt is object )
            {
                AsyncNotifyThis( evt, CurrentSyncContext(), sender, e );
            }
        }

        /// <summary>
        /// Asynchronously notifies an <see cref="PropertyChangedEventHandler">Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="handler"> The handler. </param>
        /// <param name="context"> The synchronization context. </param>
        /// <param name="sender">  The sender of the event. </param>
        /// <param name="e">       Property changed event information. </param>
        public static void AsyncNotify( this PropertyChangedEventHandler handler, System.Threading.SynchronizationContext context, object sender, PropertyChangedEventArgs e )
        {
            var evt = handler;
            if ( evt is object )
            {
                AsyncNotifyThis( evt, context, sender, e );
            }
        }

        /// <summary>
        /// Asynchronously notifies an <see cref="PropertyChangedEventHandler">Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="evt">     The handler. </param>
        /// <param name="context"> The synchronization context. </param>
        /// <param name="sender">  The sender of the event. </param>
        /// <param name="e">       Property changed event information. </param>
        private static void AsyncNotifyThis( PropertyChangedEventHandler evt, System.Threading.SynchronizationContext context, object sender, PropertyChangedEventArgs e )
        {
            context.Post( ( object status ) => evt( sender, e ), null );
        }

        /// <summary>
        /// Asynchronously notifies an <see cref="PropertyChangingEventHandler">Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="handler"> The handler. </param>
        /// <param name="sender">  The sender of the event. </param>
        /// <param name="e">       T event information. </param>
        public static void AsyncNotify( this PropertyChangingEventHandler handler, object sender, PropertyChangingEventArgs e )
        {
            var evt = handler;
            if ( evt is object )
            {
                AsyncNotifyThis( evt, CurrentSyncContext(), sender, e );
            }
        }

        /// <summary>
        /// Asynchronously notifies an <see cref="PropertyChangingEventHandler">Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="handler"> The handler. </param>
        /// <param name="context"> The synchronization context. </param>
        /// <param name="sender">  The sender of the event. </param>
        /// <param name="e">       Property Changing event information. </param>
        public static void AsyncNotify( this PropertyChangingEventHandler handler, System.Threading.SynchronizationContext context, object sender, PropertyChangingEventArgs e )
        {
            var evt = handler;
            if ( evt is object )
            {
                AsyncNotifyThis( evt, context, sender, e );
            }
        }

        /// <summary>
        /// Asynchronously notifies an <see cref="PropertyChangingEventHandler">Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="evt">     The handler. </param>
        /// <param name="context"> The synchronization context. </param>
        /// <param name="sender">  The sender of the event. </param>
        /// <param name="e">       Property Changing event information. </param>
        private static void AsyncNotifyThis( PropertyChangingEventHandler evt, System.Threading.SynchronizationContext context, object sender, PropertyChangingEventArgs e )
        {
            context.Post( ( object status ) => evt( sender, e ), null );
        }

        /// <summary>
        /// Asynchronously notifies an <see cref="NotifyCollectionChangedEventHandler">Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="handler"> The handler. </param>
        /// <param name="sender">  The sender of the event. </param>
        /// <param name="e">       T event information. </param>
        public static void AsyncNotify( this NotifyCollectionChangedEventHandler handler, object sender, NotifyCollectionChangedEventArgs e )
        {
            var evt = handler;
            if ( evt is object )
            {
                AsyncNotifyThis( evt, CurrentSyncContext(), sender, e );
            }
        }

        /// <summary>
        /// Asynchronously notifies an <see cref="NotifyCollectionChangedEventHandler">Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="handler"> The handler. </param>
        /// <param name="context"> The synchronization context. </param>
        /// <param name="sender">  The sender of the event. </param>
        /// <param name="e">       Property changed event information. </param>
        public static void AsyncNotify( this NotifyCollectionChangedEventHandler handler, System.Threading.SynchronizationContext context, object sender, NotifyCollectionChangedEventArgs e )
        {
            var evt = handler;
            if ( evt is object )
            {
                AsyncNotifyThis( evt, context, sender, e );
            }
        }

        /// <summary>
        /// Asynchronously notifies an <see cref="NotifyCollectionChangedEventHandler">Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="evt">     The handler. </param>
        /// <param name="context"> The synchronization context. </param>
        /// <param name="sender">  The sender of the event. </param>
        /// <param name="e">       Property changed event information. </param>
        private static void AsyncNotifyThis( NotifyCollectionChangedEventHandler evt, System.Threading.SynchronizationContext context, object sender, NotifyCollectionChangedEventArgs e )
        {
            context.Post( ( object status ) => evt( sender, e ), null );
        }
    }
}
