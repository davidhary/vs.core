using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace isr.Core
{

    /// <summary> Executes actions on a context. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-01-29 </para>
    /// </remarks>
    public class ActionContext<T>
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="handler"> Event queue for all listeners interested in Action Handler events. </param>
        public ActionContext( Action<T> handler )
        {
            this.Handler = handler;
            this.Context = SynchronizationContext.Current;
        }

        /// <summary> Gets the synchronization context. </summary>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The context. </value>
        public SynchronizationContext Context { get; private set; }

        /// <summary> Event queue for all listeners interested in Action Handler events. </summary>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The handler. </value>
        public Action<T> Handler { get; private set; }

        /// <summary> Returns the current synchronization context. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown if the current synchronization thread is
        /// null. </exception>
        /// <returns> A Threading.SynchronizationContext. </returns>
        private static SynchronizationContext CurrentSyncContext()
        {
            if ( SynchronizationContext.Current is null )
            {
                SynchronizationContext.SetSynchronizationContext( new SynchronizationContext() );
            }

            return SynchronizationContext.Current is null
                ? throw new InvalidOperationException( "Current Synchronization Context not set;. Must be set before starting the thread." )
                : SynchronizationContext.Current;
        }

        /// <summary> Gets a context for the active. </summary>
        /// <value> The active context. </value>
        private SynchronizationContext ActiveContext => this.Context ?? CurrentSyncContext();

        /// <summary> Asynchronous invoke. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="argument"> The action argument. </param>
        public void AsyncInvoke( T argument )
        {
            var evt = this.Handler;
            if ( evt is object )
            {
                this.ActiveContext.Post( ( object status ) => evt( argument ), null );
            }
        }

        /// <summary> Synchronization invoke. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="argument"> The action argument. </param>
        public void SyncInvoke( T argument )
        {
            var evt = this.Handler;
            if ( evt is object )
            {
                this.ActiveContext.Send( ( object status ) => evt( argument ), ( object ) argument );
            }
        }

        /// <summary>
        /// Executes the given operation on a different thread, and waits for the result.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="argument"> The action argument. </param>
        public void Invoke( T argument )
        {
            var evt = this.Handler;
            evt?.Invoke( argument );
        }
    }

    /// <summary> A collection of <see cref="ActionContext{T}"/>. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-01-29 </para>
    /// </remarks>
    public class ActionContextCollection<T> : List<ActionContext<T>>
    {

        /// <summary> Executes (Sync Invokes) the event handler action. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="argument"> The action argument. </param>
        public void Raise( T argument )
        {
            foreach ( ActionContext<T> act in this )
            {
                act?.SyncInvoke( argument );
            }
        }

        /// <summary> Asynchronous invoke. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="argument"> The action argument. </param>
        public void AsyncInvoke( T argument )
        {
            foreach ( ActionContext<T> act in this )
            {
                act?.AsyncInvoke( argument );
            }
        }

        /// <summary> Synchronization invoke. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="argument"> The action argument. </param>
        public void SyncInvoke( T argument )
        {
            foreach ( ActionContext<T> act in this )
            {
                act?.SyncInvoke( argument );
            }
        }

        /// <summary>
        /// Executes the given operation on a different thread, and waits for the result.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="argument"> The action argument. </param>
        public void Invoke( T argument )
        {
            foreach ( ActionContext<T> act in this )
            {
                act?.Invoke( argument );
            }
        }

        /// <summary> Looks up a given key to find its associated last index. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> An Integer. </returns>
        private int LookupLastIndex( Action<T> value )
        {
            return this.ToList().FindLastIndex( x => x.Handler.Equals( value ) );
        }

        /// <summary> Removes the value described by value. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The value. </param>
        public void RemoveValue( Action<T> value )
        {
            if ( this.Any() )
            {
                int lastIndex = this.LookupLastIndex( value );
                if ( lastIndex >= 0 && lastIndex < this.Count && this.ElementAt( lastIndex ).Handler.Equals( value ) )
                {
                    this.RemoveAt( lastIndex );
                }
            }
        }

        /// <summary> Removes all. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        public void RemoveAll()
        {
            while ( this.Count > 0 )
            {
                this.RemoveAt( 0 );
            }
        }
    }
}
