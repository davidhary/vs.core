using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace isr.Core
{

    /// <summary> Action notifier. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-04-26 </para>
    /// </remarks>
    public class ActionNotifier<T>
    {

        #region " CONSTRUCTION "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        public ActionNotifier() : base()
        {
            this.NotifyAsyncActions = new List<Func<T, Task>>();
            this.ActionContexts = new ActionContextCollection<T>();
        }

        #endregion

        #region " SYNC NOTIFIERS "

        /// <summary> Gets or sets the action contexts. </summary>
        /// <value> The action contexts. </value>
        private ActionContextCollection<T> ActionContexts { get; set; } = new ActionContextCollection<T>();

        /// <summary> Event queue for all listeners interested in Notified events. </summary>
        public event Action<T> Notified
        {
            add {
                this.ActionContexts.Add( new ActionContext<T>( value ) );
            }

            remove {
                this.ActionContexts.RemoveValue( value );
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private void OnNotified( T argument )
        {
            this.ActionContexts.Raise( argument );
        }

        /// <summary> Asynchronous invoke. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="argument"> The action argument. </param>
        public void AsyncInvoke( T argument )
        {
            this.ActionContexts.AsyncInvoke( argument );
        }

        /// <summary> Synchronization invoke. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="argument"> The action argument. </param>
        public void SyncInvoke( T argument )
        {
            this.ActionContexts.SyncInvoke( argument );
        }

        /// <summary>
        /// Executes the given operation on a different thread, and waits for the result.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="argument"> The action argument. </param>
        public void Invoke( T argument )
        {
            this.ActionContexts.Invoke( argument );
        }

        /// <summary> Registers an action to invoke. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="action"> The action. </param>
        public void Register( Action<T> action )
        {
            Notified += action;
        }

        /// <summary> Unregisters an action. </summary>
        /// <remarks> David, 2020-08-26. </remarks>
        /// <param name="action"> The action. </param>
        public void Unregister( Action<T> action )
        {
            Notified -= action;
        }

        #endregion

        #region " ASYNC TASK NOTIFIERS "

        /// <summary> Gets or sets the notify asynchronous actions. </summary>
        /// <value> The notify asynchronous actions. </value>
        private List<Func<T, Task>> NotifyAsyncActions { get; set; } = new List<Func<T, Task>>();

        /// <summary> Executes the asynchronous on a different thread, and waits for the result. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="argument"> The action argument. </param>
        /// <returns> A Task. </returns>
        public async Task InvokeAsync( T argument )
        {
            this.Invoke( argument );
            foreach ( Func<T, Task> callback in this.NotifyAsyncActions )
            {
                await callback( argument );
            }
        }

        /// <summary> Registers this object. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="action"> The action. </param>
        public void Register( Func<T, Task> action )
        {
            this.NotifyAsyncActions.Add( action );
        }

        #endregion

    }
}
