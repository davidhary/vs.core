using System;
using System.Threading;

namespace isr.Core
{
    public partial class EventHandlerContext<TEventArgs> where TEventArgs : EventArgs
    {

        #region " ACTIVE CONTEXT "

        /// <summary> Returns the current synchronization context. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <returns> A Threading.SynchronizationContext. </returns>
        private static SynchronizationContext CurrentSyncContext()
        {
            if ( SynchronizationContext.Current is null )
            {
                SynchronizationContext.SetSynchronizationContext( new SynchronizationContext() );
            }

            return SynchronizationContext.Current;
        }


        /// <summary> Gets a context for the active. </summary>
        /// <value> The active context. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private SynchronizationContext ActiveContext => this.Context ?? CurrentSyncContext();

        #endregion

        #region " POST or Invoke / SEND or Invoke "

        /// <summary>
        /// Asynchronously raises (Posts) the <see cref="EventHandler{TEventArgs}"/>. It does all the
        /// checking to see if the SynchronizationContext is nothing or not, and invokes the delegate
        /// accordingly. Note that, unlike the synchronous Send action, although the event is posted on
        /// the synchronization context, cross thread exceptions still occurred, ostensibly, due to the
        /// changing of the sync context between the time the action was elicited and the time it was
        /// actually invoked.
        /// </summary>
        /// <remarks> David, 2020-07-27. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      event information. </param>
        public void Post( object sender, TEventArgs e )
        {
            var evt = this.Handler;
            if ( evt is object )
            {
                var context = this.Context;
                if ( context is null )
                {
                    // if not UI or active context, invoke the event
                    evt.Invoke( sender, e );
                }
                else
                {
                    context.Post( ( object state ) => evt.Invoke( sender, e ), null );
                }
            }
        }

        /// <summary>
        /// Synchronously raises (sends) the <see cref="EventHandler{TEventArgs}"/>. It does all the
        /// checking to see if the SynchronizationContext is nothing or not, and invokes the delegate
        /// accordingly.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property changed event information. </param>
        public void Send( object sender, TEventArgs e )
        {
            var evt = this.Handler;
            if ( evt is object )
            {
                var context = this.Context;
                if ( this.Context is null )
                {
                    // if not UI or active context, invoke the event
                    evt.Invoke( sender, e );
                }
                else
                {
                    // if UI or active context, send
                    context.Send( ( object state ) => evt.Invoke( sender, e ), null );
                }
            }
        }

        #endregion

    }
}
