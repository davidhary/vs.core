using System;
using System.Collections.Specialized;
using System.ComponentModel;

namespace isr.Core
{
    /// <summary>   An event handler extensions. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    public static partial class EventHandlerExtensions
    {

        /// <summary>
        /// Synchronously notifies an <see cref="EventHandler{EventArgs}">Event</see>.
        /// </summary>
        /// <remarks>
        /// <para>
        /// 20190111: Based on new benchmarks.; the dynamic post is best when handling long events. For
        /// property changes involving minimal front end duration, a simple post will do.</para><para>
        /// Also, note that calling the Windows Form or Dispatcher (not tested yet) Do Events from within
        /// the event handler, 'tells' the synchronization context to call the next event on the
        /// invocation list. </para>
        /// <para>Test Results posting events to a class and a panel with 10 ms sleep on both
        /// targets:</para>
        /// <para>Async Invoke (Post): .15- .24 ms; Regain Control:   .41 -  .58 ms. loop over
        /// </para>
        /// <para>Async Invoke (Post): 7.5 - 7.7 ms; Regain Control:   .34 -  .58 ms. Single Sync
        /// context call. </para>
        /// <para> Sync invoke (send): 7.7 - 8.5 ms; Regain Control: 10.7 - 11.3 ms. </para>
        /// <para>Unsafe Invoke (Sync): Invoke: 6.7 - 7.5 ms; Regain Control: 10.2 - 11.2 ms. </para>
        /// <para>Conclusion: Using safe invoke has little if any negative effects. </para>
        /// </remarks>
        /// <param name="handler"> The event handler. </param>
        /// <param name="sender">  The sender of the event. </param>
        public static void SyncNotify( this EventHandler<EventArgs> handler, object sender )
        {
            var evt = handler;
            if ( evt is object )
            {
                SyncNotifyThis( evt, CurrentSyncContext(), sender, EventArgs.Empty );
            }
        }

        /// <summary>
        /// Synchronously notifies an <see cref="EventHandler{EventArgs}">Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="handler"> The event handler. </param>
        /// <param name="sender">  The sender of the event. </param>
        /// <param name="e">       The arguments for the event. </param>
        public static void SyncNotify<TEventArgs>( this EventHandler<TEventArgs> handler, object sender, TEventArgs e ) where TEventArgs : EventArgs
        {
            var evt = handler;
            if ( evt is object )
            {
                SyncNotifyThis( evt, CurrentSyncContext(), sender, e );
            }
        }

        /// <summary>
        /// Synchronously notifies an <see cref="EventHandler{EventArgs}">Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="handler"> The event handler. </param>
        /// <param name="context"> The synchronization context. </param>
        /// <param name="sender">  The sender of the event. </param>
        /// <param name="e">       The arguments for the event. </param>
        public static void SyncNotify<TEventArgs>( this EventHandler<TEventArgs> handler, System.Threading.SynchronizationContext context, object sender, TEventArgs e ) where TEventArgs : EventArgs
        {
            var evt = handler;
            if ( evt is object )
            {
                SyncNotifyThis( evt, context, sender, e );
            }
        }

        /// <summary>
        /// Synchronously notifies an <see cref="EventHandler{EventArgs}">Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="evt">     The event handler. </param>
        /// <param name="context"> The synchronization context. </param>
        /// <param name="sender">  The sender of the event. </param>
        /// <param name="e">       The arguments for the event. </param>
        private static void SyncNotifyThis<TEventArgs>( EventHandler<TEventArgs> evt, System.Threading.SynchronizationContext context, object sender, TEventArgs e ) where TEventArgs : EventArgs
        {
            context.Send( ( object status ) => evt( sender, e ), null );
            // same speed as the single call.
            // For Each d As [Delegate] In evt.GetInvocationList: Methods.CurrentSyncContext.Send(Sub(arguments) d.DynamicInvoke(New Object() {sender, e}), e): Next
        }

        /// <summary>
        /// Synchronously notifies an <see cref="PropertyChangedEventHandler">Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="handler"> The handler. </param>
        /// <param name="sender">  The sender of the event. </param>
        /// <param name="e">       Property Changed event information. </param>
        public static void SyncNotify( this PropertyChangedEventHandler handler, object sender, PropertyChangedEventArgs e )
        {
            var evt = handler;
            if ( evt is object )
            {
                SyncNotifyThis( evt, CurrentSyncContext(), sender, e );
            }
        }

        /// <summary>
        /// Synchronously notifies an <see cref="PropertyChangedEventHandler">Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="handler"> The handler. </param>
        /// <param name="context"> The synchronization context. </param>
        /// <param name="sender">  The sender of the event. </param>
        /// <param name="e">       Property Changed event information. </param>
        public static void SyncNotify( this PropertyChangedEventHandler handler, System.Threading.SynchronizationContext context, object sender, PropertyChangedEventArgs e )
        {
            var evt = handler;
            if ( evt is object )
            {
                SyncNotifyThis( evt, context, sender, e );
            }
        }

        /// <summary>
        /// Synchronously notifies an <see cref="PropertyChangedEventHandler">Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="evt">     The handler. </param>
        /// <param name="context"> The synchronization context. </param>
        /// <param name="sender">  The sender of the event. </param>
        /// <param name="e">       Property Changed event information. </param>
        private static void SyncNotifyThis( PropertyChangedEventHandler evt, System.Threading.SynchronizationContext context, object sender, PropertyChangedEventArgs e )
        {
            context.Send( ( object status ) => evt( sender, e ), null );
            // same speed as the single call.
            // For Each d As [Delegate] In evt.GetInvocationList: Methods.CurrentSyncContext.Send(Sub(arguments) d.DynamicInvoke(New Object() {sender, e}), e): Next
        }

        /// <summary>
        /// Synchronously notifies an <see cref="PropertyChangingEventHandler">Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="handler"> The handler. </param>
        /// <param name="sender">  The sender of the event. </param>
        /// <param name="e">       Property changing event information. </param>
        public static void SyncNotify( this PropertyChangingEventHandler handler, object sender, PropertyChangingEventArgs e )
        {
            var evt = handler;
            if ( evt is object )
            {
                SyncNotifyThis( evt, CurrentSyncContext(), sender, e );
            }
        }

        /// <summary>
        /// Synchronously notifies an <see cref="PropertyChangingEventHandler">Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="handler"> The handler. </param>
        /// <param name="context"> The synchronization context. </param>
        /// <param name="sender">  The sender of the event. </param>
        /// <param name="e">       Property Changing event information. </param>
        public static void SyncNotify( this PropertyChangingEventHandler handler, System.Threading.SynchronizationContext context, object sender, PropertyChangingEventArgs e )
        {
            var evt = handler;
            if ( evt is object )
            {
                SyncNotifyThis( evt, context, sender, e );
            }
        }

        /// <summary>
        /// Synchronously notifies an <see cref="PropertyChangingEventHandler">Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="evt">     The handler. </param>
        /// <param name="context"> The synchronization context. </param>
        /// <param name="sender">  The sender of the event. </param>
        /// <param name="e">       Property Changing event information. </param>
        private static void SyncNotifyThis( PropertyChangingEventHandler evt, System.Threading.SynchronizationContext context, object sender, PropertyChangingEventArgs e )
        {
            context.Send( ( object status ) => evt( sender, e ), null );
            // same speed as the single call.
            // For Each d As [Delegate] In evt.GetInvocationList : Methods.CurrentSyncContext.Send(Sub(arguments) d.DynamicInvoke(New Object() {sender, e}), e) : Next
        }

        /// <summary>
        /// Synchronously notifies an <see cref="NotifyCollectionChangedEventHandler">Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="handler"> The handler. </param>
        /// <param name="sender">  The sender of the event. </param>
        /// <param name="e">       Notify collection changed event information. </param>
        public static void SyncNotify( this NotifyCollectionChangedEventHandler handler, object sender, NotifyCollectionChangedEventArgs e )
        {
            var evt = handler;
            if ( evt is object )
            {
                SyncNotifyThis( evt, CurrentSyncContext(), sender, e );
            }
        }

        /// <summary>
        /// Synchronously notifies an <see cref="NotifyCollectionChangedEventHandler">Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="handler"> The handler. </param>
        /// <param name="context"> The synchronization context. </param>
        /// <param name="sender">  The sender of the event. </param>
        /// <param name="e">       Property Changed event information. </param>
        public static void SyncNotify( this NotifyCollectionChangedEventHandler handler, System.Threading.SynchronizationContext context, object sender, NotifyCollectionChangedEventArgs e )
        {
            var evt = handler;
            if ( evt is object )
            {
                SyncNotifyThis( evt, context, sender, e );
            }
        }

        /// <summary>
        /// Synchronously notifies an <see cref="NotifyCollectionChangedEventHandler">Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="evt">     The handler. </param>
        /// <param name="context"> The synchronization context. </param>
        /// <param name="sender">  The sender of the event. </param>
        /// <param name="e">       Property Changed event information. </param>
        private static void SyncNotifyThis( NotifyCollectionChangedEventHandler evt, System.Threading.SynchronizationContext context, object sender, NotifyCollectionChangedEventArgs e )
        {
            context.Send( ( object status ) => evt( sender, e ), null );
            // same speed as the single call.
            // For Each d As [Delegate] In evt.GetInvocationList : Methods.CurrentSyncContext.Send(Sub(arguments) d.DynamicInvoke(New Object() {sender, e}), e) : Next
        }
    }
}
