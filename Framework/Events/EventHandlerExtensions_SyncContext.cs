using System;

namespace isr.Core
{
    /// <summary>   An event handler extensions. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    public static partial class EventHandlerExtensions
    {

        /// <summary> Returns the current synchronization context. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown if the current synchronization thread is
        /// null. </exception>
        /// <returns> A Threading.SynchronizationContext. </returns>
        private static System.Threading.SynchronizationContext CurrentSyncContext()
        {
            if ( System.Threading.SynchronizationContext.Current is null )
            {
                System.Threading.SynchronizationContext.SetSynchronizationContext( new System.Threading.SynchronizationContext() );
            }

            return System.Threading.SynchronizationContext.Current is null
                ? throw new InvalidOperationException( "Current Synchronization Context not set;. Must be set before starting the thread." )
                : System.Threading.SynchronizationContext.Current;
        }
    }
}
