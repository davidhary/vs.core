using System;

namespace isr.Core.SubstringExtensions
{
    /// <summary> Includes 'Substring' extensions for <see cref="String">String</see>. </summary>
    /// <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2010-11-19, 1.2.3975 </para></remarks>
    public static class SubstringExtensionMethods
    {

        /// <summary>
        /// Returns a substring of the input string taking not of start index and length exceptions.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value">      The input string to sub string. </param>
        /// <param name="startIndex"> The zero based starting index. </param>
        /// <param name="length">     The total length. </param>
        /// <returns>
        /// A substring of the input string taking not of start index and length exceptions.
        /// </returns>
        public static string SafeSubstring( this string value, int startIndex, int length )
        {
            if ( string.IsNullOrWhiteSpace( value ) || length <= 0 )
            {
                return string.Empty;
            }

            int inputLength = value.Length;
            if ( startIndex > inputLength )
            {
                return string.Empty;
            }
            else
            {
                if ( startIndex + length > inputLength )
                {
                    length = inputLength - startIndex;
                }

                return value.Substring( startIndex, length );
            }
        }

        /// <summary> Returns the substring after the last occurrence of the specified string. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="source"> The string to substring. </param>
        /// <param name="search"> The string to search for. </param>
        /// <returns> The substring after the last occurrence of the specified string. </returns>
        public static string SubstringAfter( this string source, string search )
        {
            if ( string.IsNullOrWhiteSpace( source ) )
            {
                return string.Empty;
            }

            if ( string.IsNullOrWhiteSpace( search ) )
            {
                return string.Empty;
            }

            int location = source.LastIndexOf( search, StringComparison.OrdinalIgnoreCase );
            return location >= 0 ? location + search.Length < source.Length ? source.Substring( location + search.Length ) : string.Empty : string.Empty;
        }

        /// <summary> Returns the substring before the last occurrence of the specified string. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="source"> The string to substring. </param>
        /// <param name="search"> The string to search for. </param>
        /// <returns> The substring before the last occurrence of the specified string. </returns>
        public static string SubstringBefore( this string source, string search )
        {
            if ( string.IsNullOrWhiteSpace( source ) )
            {
                return string.Empty;
            }

            if ( string.IsNullOrWhiteSpace( search ) )
            {
                return string.Empty;
            }

            int location = source.LastIndexOf( search, StringComparison.OrdinalIgnoreCase );
            return location >= 0 ? source.Substring( 0, location ) : string.Empty;
        }

        /// <summary> Returns the substring after the last occurrence of the specified string. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="source">         The string to substring. </param>
        /// <param name="startDelimiter"> The start delimiter to search for. </param>
        /// <param name="endDelimiter">   The end delimiter to search for. </param>
        /// <returns> The substring after the last occurrence of the specified string. </returns>
        public static string SubstringBetween( this string source, string startDelimiter, string endDelimiter )
        {
            if ( string.IsNullOrWhiteSpace( source ) )
            {
                return string.Empty;
            }

            if ( string.IsNullOrWhiteSpace( startDelimiter ) )
            {
                return string.Empty;
            }

            int startLocation = source.LastIndexOf( startDelimiter, StringComparison.OrdinalIgnoreCase ) + startDelimiter.Length;
            int endLocation = source.LastIndexOf( endDelimiter, StringComparison.OrdinalIgnoreCase );
            return startLocation >= 0 && startLocation < endLocation ? source.Substring( startLocation, endLocation - startLocation ) : string.Empty;
        }

        /// <summary>
        /// Returns a new string consisting of all character preceding the
        /// <paramref name="startingIndex"/>.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="source">        The string to substring. </param>
        /// <param name="startingIndex"> Zero-based index of the starting. </param>
        /// <returns> A String. </returns>
        public static string SafeRemove( this string source, int startingIndex )
        {
            return string.IsNullOrWhiteSpace( source ) ? string.Empty : startingIndex >= source.Length ? source : source.Remove( startingIndex );
        }

        /// <summary>
        /// Returns a new string in which the specified <paramref name="count"/>"/&gt; number of
        /// characters are remove starting at <paramref name="startingIndex"/>
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="source">        The string to substring. </param>
        /// <param name="startingIndex"> Zero-based index of the starting. </param>
        /// <param name="count">         Number of. </param>
        /// <returns> A String. </returns>
        public static string SafeRemove( this string source, int startingIndex, int count )
        {
            return string.IsNullOrWhiteSpace( source )
                ? string.Empty
                : count == 0
                ? source
                : startingIndex >= source.Length
                ? source
                : startingIndex + count > source.Length ? source.Remove( startingIndex ) : source.Remove( startingIndex, count );
        }
    }
}
