using System;

namespace isr.Core.ContainsExtensions
{

    /// <summary> Includes 'Contains' extensions for <see cref="String">String</see>. </summary>
    /// <remarks> (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2009-04-09, 1.1.3386 </para></remarks>
    public static class ContainsExtensionMethods
    {

        /// <summary> Returns true if the find string is in the search string. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="search">         The item to search. </param>
        /// <param name="find">           The item to find. </param>
        /// <param name="comparisonType"> Type of the comparison. </param>
        /// <returns> <c>True</c> the find string is in the search string. </returns>
        public static bool Contains( this string search, string find, StringComparison comparisonType )
        {
            return search.Location( find, comparisonType ) >= 0;
        }

        /// <summary> Returns the location of the find string in the search string. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="search">         The item to search. </param>
        /// <param name="find">           The item to find. </param>
        /// <param name="comparisonType"> Type of the comparison. </param>
        /// <returns> The location of the find string in the search string. </returns>
        public static int Location( this string search, string find, StringComparison comparisonType )
        {
            return string.IsNullOrWhiteSpace( search ) ? 0 : search.IndexOf( find, comparisonType );
        }
    }
}
