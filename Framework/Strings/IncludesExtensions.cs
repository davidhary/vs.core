using System;

namespace isr.Core.IncludesExtensions
{
    /// <summary> Includes 'include' extensions for <see cref="String">String</see>. </summary>
    /// <remarks> (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2009-04-09, 1.1.3386 </para></remarks>
    public static class IncludesExtensionMethods
    {

        /// <summary> The illegal file characters. </summary>
        public const string IllegalFileCharacters = @"/\\:*?""<>|";

        /// <summary> Returns True if the validated string contains only alpha characters. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="source"> The string to validate. </param>
        /// <returns>
        /// <c>True</c> if the validated string contains only alpha; otherwise, <c>False</c>.
        /// </returns>
        public static bool IncludesIllegalFileCharacters( this string source )
        {
            return source.IncludesCharacters( IllegalFileCharacters );
        }

        /// <summary> Returns true if the string includes the specified characters. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="source">     The string that is being searched. </param>
        /// <param name="characters"> The characters to search for. </param>
        /// <returns>
        /// <c>True</c> if the string includes the specified characters; otherwise, <c>False</c>.
        /// </returns>
        public static bool IncludesCharacters( this string source, string characters )
        {
            if ( source is null )
            {
                throw new ArgumentNullException( nameof( source ) );
            }

            if ( characters is null )
            {
                throw new ArgumentNullException( nameof( characters ) );
            }

            string expression = $"[{characters}]";
            var r = new System.Text.RegularExpressions.Regex( expression, System.Text.RegularExpressions.RegexOptions.IgnoreCase );
            return r.IsMatch( source );
        }

        /// <summary> Returns true if the string includes any of the specified characters. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="source">     The string that is being searched. </param>
        /// <param name="characters"> The characters to search for. </param>
        /// <returns> null if it fails, else. </returns>
        public static bool IncludesAny( this string source, string characters )
        {
            if ( source is null )
            {
                throw new ArgumentNullException( nameof( source ) );
            }

            if ( characters is null )
            {
                throw new ArgumentNullException( nameof( characters ) );
            }

            // 2954: was ^[{0}]+$ 
            string expression = $"[{characters}]";
            var r = new System.Text.RegularExpressions.Regex( expression, System.Text.RegularExpressions.RegexOptions.IgnoreCase );
            return r.IsMatch( source );
        }
    }
}
