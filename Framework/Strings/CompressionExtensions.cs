using System;

namespace isr.Core.CompressionExtensions
{
    /// <summary> Includes Compression extensions for <see cref="String">String</see>. </summary>
    /// <remarks> (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2009-04-09, 1.1.3386 </para></remarks>
    public static class CompressionExtensionMethods
    {

        /// <summary> Returns a compressed value. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The string being chopped. </param>
        /// <returns> Compressed value. </returns>
        public static string Compress( this string value )
        {
            if ( string.IsNullOrWhiteSpace( value ) )
            {
                return string.Empty;
            }

            string result = string.Empty;

            // Compress the byte array
            using ( var memoryStream = new System.IO.MemoryStream() )
            {
                using var compressedStream = new System.IO.Compression.GZipStream( memoryStream, System.IO.Compression.CompressionMode.Compress );

                // Convert the uncompressed string into a byte array
                var values = System.Text.Encoding.UTF8.GetBytes( value );
                compressedStream.Write( values, 0, values.Length );

                // Don't FLUSH here - it possibly leads to data loss!
                compressedStream.Close();
                var compressedValues = memoryStream.ToArray();

                // Convert the compressed byte array back to a string
                result = Convert.ToBase64String( compressedValues );
                memoryStream.Close();
            }

            return result;
        }

        /// <summary> Returns the decompressed string of the value. </summary>
        /// <remarks>
        /// David, 2009-04-09, 1.1.3516.x. Bug fix in getting the size. Changed  memoryStream.Length - 5
        /// to  memoryStream.Length - 4.
        /// </remarks>
        /// <param name="value"> The string being chopped. </param>
        /// <returns> Decompressed value. </returns>
        public static string Decompress( this string value )
        {
            if ( string.IsNullOrWhiteSpace( value ) )
            {
                return string.Empty;
            }

            string result = string.Empty;

            // Convert the compressed string into a byte array
            var compressedValues = Convert.FromBase64String( value );

            // Decompress the byte array
            using ( var memoryStream = new System.IO.MemoryStream( compressedValues ) )
            {
                using var compressedStream = new System.IO.Compression.GZipStream( memoryStream, System.IO.Compression.CompressionMode.Decompress );

                // it looks like we are getting a bogus size.
                var sizeBytes = new byte[4];
                memoryStream.Position = memoryStream.Length - 4L;
                _ = memoryStream.Read( sizeBytes, 0, 4 );
                int outputSize = BitConverter.ToInt32( sizeBytes, 0 );
                memoryStream.Position = 0L;
                var values = new byte[outputSize];
                _ = compressedStream.Read( values, 0, outputSize );

                // Convert the decompressed byte array back to a string
                result = System.Text.Encoding.UTF8.GetString( values );
            }

            return result;
        }
    }
}
