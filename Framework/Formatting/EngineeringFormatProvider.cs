using System;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core
{
    /// <summary>
    /// Custom numeric string formatting, including engineering notation. I needed to display numbers
    /// in an engineering application. The customer wanted the numbers formatted nicely, with about
    /// four significant digits and engineering notation. .NET has very flexible number formatting.
    /// However, it lacks standard formatting into engineering notation. The extension point for
    /// formatting numeric strings is through the FormatProvider and CustomFormatter interfaces. I
    /// implemented a format provider for the engineering notation. However, exclusively using
    /// engineering notation was awkward in our application. As an alternative to exclusively using
    /// engineering notation, I decided on the following format, which mixes four significant digits,
    /// engineering notation, and thousand separators.
    /// </summary>
    /// <remarks> (c) 2010 Wallace Kelly . http://WallaceKelly.BlogSpot.com/. </remarks>
    public class EngineeringFormatProvider : IFormatProvider, ICustomFormatter
    {

        /// <summary>
        /// Returns an object that provides formatting services for the specified type.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="formatType"> Type of the format. </param>
        /// <returns>
        /// An instance of the object specified by <paramref name="formatType" />, if the
        /// <see cref="T:System.IFormatProvider" /> implementation can supply that type of object;
        /// otherwise, null.
        /// </returns>
        public object GetFormat( Type formatType )
        {
            return ReferenceEquals( formatType, typeof( ICustomFormatter ) ) ? this : null;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this object using thousands separator.
        /// </summary>
        /// <remarks> Applies between 1000 and only up to 99,999. </remarks>
        /// <value> <c>True</c> if use thousands separator; otherwise <c>false</c>. </value>
        public bool UsingThousandsSeparator { get; set; }

        /// <summary> Formats using engineering notation. </summary>
        /// <remarks>
        /// Example: String.Format(new EngineeringFormatProvider(), "{0}", number);
        /// Example: String.Format(new EngineeringFormatProvider(), "{0:G4}", number);
        /// </remarks>
        /// <param name="format1">        Specifies the format. This format string is stripped from the
        /// original format. For example, the {0} string yield 'nothing'
        /// whereas {0:G4} yields G4. </param>
        /// <param name="arg">            The value to format. </param>
        /// <param name="formatProvider"> . </param>
        /// <returns>
        /// The formatted value as a <see cref="T:String">string</see>
        /// The empty string is returned if the value is null.
        /// </returns>
        public string Format( string format1, object arg, IFormatProvider formatProvider )
        {
            if ( string.IsNullOrWhiteSpace( format1 ) )
            {
                format1 = "G4";
            }

            string engineeringFormat = string.Format( System.Globalization.CultureInfo.CurrentCulture, "{0}{1}{2}", "{0:", format1, "}e{1}{2:00}" );
            string defaultFormat = string.Format( System.Globalization.CultureInfo.CurrentCulture, "{0}{1}{2}", "{0:", format1, "}" );

            // for doubles, store the value of the double
            double val = double.NaN;
            if ( arg is null )
            {
                return string.Empty;
            }
            else if ( arg is double )
            {
                val = Conversions.ToDouble( arg );
            }
            else
            {
                // for other types, try to convert to a double
                var typeConverter = System.ComponentModel.TypeDescriptor.GetConverter( arg );
                if ( typeConverter.CanConvertTo( typeof( double ) ) )
                {
                    try
                    {
                        val = Conversions.ToDouble( typeConverter.ConvertTo( arg, typeof( double ) ) );
                    }
                    catch
                    {
                        // ignore
                    }
                }

                // if cannot convert, return a default value
                if ( double.IsNaN( val ) )
                {
                    return arg is null ? string.Empty : arg.ToString();
                }
            }

            // for special cases, just write out the string
            if ( val == 0.0d || double.IsNaN( val ) || double.IsInfinity( val ) )
            {
                return val.ToString( System.Globalization.CultureInfo.CurrentCulture );
            }
            else
            {
                // calculate the exponents, as a power of 3
                double exp = Math.Log10( Math.Abs( val ) );
                int exp3 = ( int ) (3d * Math.Floor( exp / 3.0d ));

                // calculate the coefficient
                double coef = val / Math.Pow( 10d, exp3 );

                // special case, for example 0.3142
                if ( exp3 == -3 && Math.Abs( coef / 1000.0d ) < 1d && Math.Abs( coef / 1000.0d ) > 0.1d )
                {
                    return string.Format( System.Globalization.CultureInfo.CurrentCulture, defaultFormat, val );
                }

                // for "small" numbers
                if ( exp3 <= -3 )
                {
                    if ( exp3 > 0 )
                    {
                        // Return String.Format(Globalization.CultureInfo.CurrentCulture, format1 & "e{1}{2:00}", coefficient, "+", exp3)
                        return string.Format( System.Globalization.CultureInfo.CurrentCulture, engineeringFormat, coef, "+", exp3 );
                    }
                    else
                    {
                        return string.Format( System.Globalization.CultureInfo.CurrentCulture, engineeringFormat, coef, "", exp3 );
                    }
                }

                // for "large" numbers
                if ( exp < 6d && exp >= 3d && this.UsingThousandsSeparator )
                {
                    // for numbers needing thousand separators
                    return $"{val:N0}";
                }
                else if ( exp >= 3d )
                {
                    return exp3 > 0 ? string.Format( System.Globalization.CultureInfo.CurrentCulture, engineeringFormat, coef, "+", exp3 ) :
                                      string.Format( System.Globalization.CultureInfo.CurrentCulture, engineeringFormat, coef, "", exp3 );
                }

                // default
                return string.Format( System.Globalization.CultureInfo.CurrentCulture, defaultFormat, val );
            }
        }
    }
}
