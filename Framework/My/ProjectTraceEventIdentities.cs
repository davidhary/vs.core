using System.ComponentModel;

namespace isr.Core
{

    /// <summary> Values that represent project trace event identifiers. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    public enum ProjectTraceEventId
    {

        /// <summary>  Not specified. </summary>
        [Description( "not specified" )]
        None = 0,

        /// <summary>  The Project Trace Event ID for the Framework project. </summary>
        [Description( "Framework" )]
        Framework = 0x10 * TraceEventConstants.SmallSolutionNamespaceSize,

        /// <summary>  The Project Trace Event ID for the Framework Controls project. </summary>
        [Description( "Framework Controls" )]
        FrameworkControls = Framework + 1,

        /// <summary>  The Project Trace Event ID for the Services project. </summary>
        [Description( "Services" )]
        WinFormsViews = Framework + 2,

        /// <summary>  The Project Trace Event ID for the Presentation project. </summary>
        [Description( "Present" )]
        Present = Framework + 3,

        /// <summary>  The Project Trace Event ID for the Windows UI project. </summary>
        [Description( "Tableaux" )]
        Tableaux = Framework + 4,

        /// <summary>  The Project Trace Event ID for the Windows Forms project. </summary>
        [Description( "Forma" )]
        Forma = Framework + 5,

        /// <summary>  The Project Trace Event ID for the Controls project. </summary>
        [Description( "Controls" )]
        Controls = Framework + 6,

        /// <summary>  The Project Trace Event ID for the Models project. </summary>
        [Description( "Models" )]
        Models = Framework + 7,

        /// <summary>  The Project Trace Event ID for the Constructs project. </summary>
        [Description( "Constructs" )]
        Constructs = Framework + 8,

        /// <summary>  The Project Trace Event ID for the Services project. </summary>
        [Description( "Services" )]
        Services = Framework + 9,

        /// <summary>  The Project Trace Event ID for the Diagnosis Tester project. </summary>
        [Description( "Diagnosis Tester" )]
        DiagnosisTester = Framework + 16,

        /// <summary>  The Project Trace Event ID for the My Blue Splash Screen project. </summary>
        [Description( "My Blue Splash Screen" )]
        MyBlueSplashScreen = Framework + 17,

        /// <summary>  The Project Trace Event ID for the exception message test project. </summary>
        [Description( "Exception Message Test" )]
        ExceptionMessageTest = Framework + 18,

        /// <summary>  The Project Trace Event ID for the My Exception Message box test project. </summary>
        [Description( "My Exception Message Box Test" )]
        MyExceptionMessageBoxTest = Framework + 19,

        // SMALL SOLUTION IDENTITIES

        /// <summary>  The Project Trace Event ID for the Automata project. </summary>
        [Description( "Automata" )]
        Automata = 0x20 * TraceEventConstants.SmallSolutionNamespaceSize,

        /// <summary>  The Project Trace Event ID for the Dapper project. </summary>
        [Description( "Dapper" )]
        Dapper = 0x21 * TraceEventConstants.SmallSolutionNamespaceSize,

        /// <summary>  The Project Trace Event ID for the Data project. </summary>
        [Description( "Data" )]
        Data = 0x22 * TraceEventConstants.SmallSolutionNamespaceSize,

        /// <summary>  The Project Trace Event ID for the Digital I/O Lan project. </summary>
        [Description( "Digital I/O LAN" )]
        DigitalInputOutputLan = 0x23 * TraceEventConstants.SmallSolutionNamespaceSize,

        /// <summary>  The Project Trace Event ID for the High Potential project. </summary>
        [Description( "High Potential" )]
        HighPotential = 0x24 * TraceEventConstants.SmallSolutionNamespaceSize,

        /// <summary>  The Project Trace Event ID for the Initial State project. </summary>
        [Description( "Initial State Client" )]
        InitialStateClient = 0x25 * TraceEventConstants.SmallSolutionNamespaceSize,

        /// <summary>  The Project Trace Event ID for the IO project. </summary>
        [Description( "IO" )]
        IO = 0x26 * TraceEventConstants.SmallSolutionNamespaceSize,

        /// <summary>  The Project Trace Event ID for the Modbus project. </summary>
        [Description( "Modbus" )]
        Modbus = 0x27 * TraceEventConstants.SmallSolutionNamespaceSize,

        /// <summary>  The Project Trace Event ID for the Net project. </summary>
        [Description( "Net" )]
        Net = 0x28 * TraceEventConstants.SmallSolutionNamespaceSize,

        /// <summary>  The Project Trace Event ID for the Rich Text Box project. </summary>
        [Description( "Rich Text Box" )]
        RichTextBox = 0x29 * TraceEventConstants.SmallSolutionNamespaceSize,

        /// <summary>  The Project Trace Event ID for the Open Layers project. </summary>
        [Description( "Open Layers" )]
        OpenLayers = 0x2A * TraceEventConstants.SmallSolutionNamespaceSize,

        /// <summary>  The Project Trace Event ID for the Optima project. </summary>
        [Description( "Optima" )]
        Optima = 0x2B * TraceEventConstants.SmallSolutionNamespaceSize,

        /// <summary>  The Project Trace Event ID for the Serial project. </summary>
        [Description( "Serial" )]
        Serial = 0x2C * TraceEventConstants.SmallSolutionNamespaceSize,

        /// <summary>  The Project Trace Event ID for the Signals project. </summary>
        [Description( "Signals" )]
        Signals = 0x2D * TraceEventConstants.SmallSolutionNamespaceSize,

        /// <summary>  The Project Trace Event ID for the Universal Library project. </summary>
        [Description( "Universal Library" )]
        UniversalLibrary = 0x2E * TraceEventConstants.SmallSolutionNamespaceSize,

        /// <summary>  The Project Trace Event ID for the Visuals project. </summary>
        [Description( "Visuals" )]
        Visuals = 0x2F * TraceEventConstants.SmallSolutionNamespaceSize,

        /// <summary>  The Project Trace Event ID for the Visuals project. </summary>
        [Description( "GNU Plot" )]
        GnuPLot = 0x30 * TraceEventConstants.SmallSolutionNamespaceSize,

        /// <summary>  The Project Trace Event ID for the Virtual Instruments project. </summary>
        // LARGE SOLUTION IDENTITIES
        [Description( "Virtual Instruments" )]
        VirtualInstruments = 0x10 * TraceEventConstants.SolutionNamespaceSize,

        /// <summary>  The Project Trace Event ID for the Application Namespace Identity for solution projects. </summary>
        [Description( "Application Namespace Identity" )]
        ApplicationNamespace = TraceEventConstants.ApplicationNamespaceIdentity
    }
}
