
namespace isr.Core.My
{

    /// <summary> Provides assembly information for the class library. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    public sealed partial class MyLibrary
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        private MyLibrary() : base()
        {
        }

        /// <summary> Gets the identifier of the trace source. </summary>
        public const int TraceEventId = ( int ) ProjectTraceEventId.Framework;

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "Core Framework Library";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "Core Framework Library";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "Core.Framework";

        /// <summary> The Strong Name of the test assembly. </summary>
        public const string TestAssemblyStrongName = "isr.Core.FrameworkTests,PublicKey=" + isr.Core.My.SolutionInfo.PublicKey;

    }
}
