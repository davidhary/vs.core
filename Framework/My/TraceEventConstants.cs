
namespace isr.Core
{
    /// <summary> A trace event constants. </summary>
    /// <remarks> (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para> </remarks>
    internal static class TraceEventConstants
    {

        /// <summary> The large solution namespace size. Multiply this by the base trace event identity factor,
        /// e.g., Hex 7 for the VI library, to get the initial library event id, e.g., 700 hex for the VI library.
        /// Each such solution can have up to 256 projects.
        /// These could be either libraries, test libraries, or test applications. </summary>
        public const int SolutionNamespaceSize = 0x100;

        /// <summary> The Large solution namespace identity. Multiply this by the large solution namespace value to get
        /// the first identity of the solution. This allows having 256 project share the namespace of this solution.
        /// The range of the large solution identities is between Hex 1000 and FF00. </summary>
        public const int SolutionNamespaceIdentity = 0x10;

        /// <summary> The small solution namespace size. Multiply this by the <see cref="SmallSolutionNamespaceIdentity"/> to get
        /// the first identity of the small solution. This allows having 256 solutions each with 16 projects sharing
        /// the solution namespace. </summary>
        public const int SmallSolutionNamespaceSize = 0x10;

        /// <summary> The small solution namespace identity. Multiply this by the <see cref="SmallSolutionNamespaceSize"/> to get
        /// the first identity of the small solution. This allows having 256 solutions each with 16 projects sharing
        /// the solution namespace.
        /// The range of the large solution identities is between
        /// <see cref="SmallSolutionNamespaceIdentity"/> * <see cref="SmallSolutionNamespaceSize"/> / <see cref="SolutionNamespaceSize"/>
        /// and <see cref="SolutionNamespaceIdentity"/> or hex H100 and hex FF0 </summary>
        public const int SmallSolutionNamespaceIdentity = 0x10;

        /// <summary> Use this base identity for the custom solution applications event identities.</summary>
        public const int ApplicationNamespaceIdentity = 0xFFE * SmallSolutionNamespaceSize;
    }
}
