﻿
using System.Diagnostics;

namespace isr.Core
{
    public partial class TraceMessagesQueueListener : ITraceMessageListener
    {

        /// <summary> Trace event overriding the trace level. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="value"> The event message. </param>
        /// <returns> A String. </returns>
        public string TraceEventOverride( TraceMessage value )
        {
            if ( value is null )
            {
                return string.Empty;
            }
            else
            {
                this.Enqueue( value );
                // TO_DO: See if needed; Forms Do Events has slowed down things quite a bit.
                // Dispatcher.CurrentDispatcher.DoEvents()
                return value.Details;
            }
        }

        /// <summary> Writes a trace event to the trace listener overriding the trace level. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="id">        The identifier. </param>
        /// <param name="format">    The message format. </param>
        /// <param name="args">      Specified the message arguments. </param>
        /// <returns> The trace message details. </returns>
        public string TraceEventOverride( TraceEventType eventType, int id, string format, params object[] args )
        {
            return this.TraceEventOverride( new TraceMessage( eventType, id, format, args ) );
        }

        /// <summary> Writes a trace event to the trace listeners. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="value"> The event message. </param>
        /// <returns> The trace message details. </returns>
        public string TraceEvent( TraceMessage value )
        {
            if ( value is null )
            {
                return string.Empty;
            }
            else
            {
                if ( this.ShouldTrace( value.EventType ) )
                {
                    this.Enqueue( value );
                }
                // TO_DO: See if needed; Forms Do Events has slowed down things quite a bit.
                // Dispatcher.CurrentDispatcher.DoEvents()
                return value.Details;
            }
        }

        /// <summary> Writes a trace event to the trace listeners. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="id">        The identifier. </param>
        /// <param name="format">    The message format. </param>
        /// <param name="args">      Specified the message arguments. </param>
        /// <returns> The trace message details. </returns>
        public string TraceEvent( TraceEventType eventType, int id, string format, params object[] args )
        {
            return this.TraceEvent( new TraceMessage( eventType, id, format, args ) );
        }
    }
}