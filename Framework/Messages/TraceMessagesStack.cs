using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using isr.Core.StackTraceExtensions;

namespace isr.Core
{

    /// <summary> Stack of trace messages. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-03-23 </para>
    /// </remarks>
    public class TraceMessagesStack
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructor for this class. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        public TraceMessagesStack() : base()
        {
            this.ContentsStack = new ConcurrentStack<TraceMessage>();
            this.ClearThis();
        }

        #endregion

        #region " PEEK "

        /// <summary> Gets the content. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns> The content. </returns>
        public TraceMessage TryPeek()
        {
            _ = this.GetContentsStack().TryPeek( out TraceMessage result );
            return result;
        }

        #endregion

        #region " PUSH "

        /// <summary> Gets any. </summary>
        /// <value> any. </value>
        public bool Any => this.GetContentsStack().Any();

        /// <summary> Gets the trace messages. </summary>
        /// <value> The trace messages. </value>
        public IEnumerable<TraceMessage> TraceMessages => this.GetContentsStack().ToArray();

        /// <summary> Gets or sets a stack of contents. </summary>
        /// <value> A stack of contents. </value>
        protected ConcurrentStack<TraceMessage> ContentsStack { get; private set; }

        /// <summary> Gets contents Stack. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns> The contents Stack. </returns>
        private ConcurrentStack<TraceMessage> GetContentsStack()
        {
            return this.ContentsStack;
        }

        /// <summary> Gets the content. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns> The content. </returns>
        public TraceMessage TryPop()
        {
            _ = this.GetContentsStack().TryPop( out TraceMessage result );
            return result;
        }

        /// <summary> Query if the contents Stack is empty. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns> <c>true</c> the contents Stack is empty; otherwise <c>false</c> </returns>
        public bool IsEmpty()
        {
            return this.GetContentsStack().IsEmpty;
        }

        /// <summary> Fetches the content. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns> The content. </returns>
        public string PopContent()
        {
            int messageNumber = 0;
            var builder = new System.Text.StringBuilder();
            do
            {
                var value = this.TryPop();
                if ( value is object )
                {
                    messageNumber += 1;
                    _ = builder.AppendLine( $"{messageNumber}:>{value}" );
                }
            }
            while ( !this.GetContentsStack().IsEmpty );
            return builder.ToString().TrimEnd( Environment.NewLine.ToCharArray() );
        }

        #endregion

        #region " PUSH "

        /// <summary> Clears this object to its blank/initial state. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        private void ClearThis()
        {
            this.ContentsStack.Clear();
            this.LastMessagePushed = new TraceMessage( TraceEventType.Verbose, 0, "Cleared" );
            this._MessagePushedEventHandlers?.Send( this, EventArgs.Empty );
        }

        /// <summary> Clears this object to its blank/initial state. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        public void Clear()
        {
            this.Clear();
        }

        /// <summary> Gets or sets the last message pushed. </summary>
        /// <value> The last message pushed. </value>
        public TraceMessage LastMessagePushed { get; private set; }

        /// <summary> Adds a message. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> A String. </returns>
        protected string Push( string value )
        {
            if ( string.IsNullOrWhiteSpace( value ) )
            {
                value = string.Empty;
            }

            this.Push( new TraceMessage( TraceEventType.Information, My.MyLibrary.TraceEventId, value ) );
            return value;
        }

        /// <summary> Adds a message. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="value"> The value. </param>
        public void Push( TraceMessage value )
        {
            if ( value is null || string.IsNullOrWhiteSpace( value.Details ) )
            {
                Debug.Assert( !Debugger.IsAttached, "Empty trace message", new StackTrace( true ).UserCallStack( 0, 0 ) );
            }
            else
            {
                long lastCount = (this.GetContentsStack()?.LongCount()).GetValueOrDefault( 0L );
                this.GetContentsStack()?.Push( value );
                if ( (this.GetContentsStack()?.LongCount()).GetValueOrDefault( 0L ) > lastCount )
                {
                    this._MessagePushedEventHandlers?.Send( this, EventArgs.Empty );
                }
            }
        }

        #region " MESSAGE PUSHED "

        /// <summary> Removes the MessagePushed event handlers. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        protected void RemoveMessagePushedEventHandlers()
        {
            this._MessagePushedEventHandlers?.RemoveAll();
        }

        /// <summary> The MessagePushed event handlers. </summary>
        private readonly EventHandlerContextCollection<EventArgs> _MessagePushedEventHandlers = new();

        /// <summary> Event Stack for all listeners interested in MessagePushed events. </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event EventHandler<EventArgs> MessagePushed
        {
            add {
                this._MessagePushedEventHandlers.Add( new EventHandlerContext<EventArgs>( value ) );
            }

            remove {
                this._MessagePushedEventHandlers.RemoveValue( value );
            }
        }

        private void OnMessagePushed( object sender, EventArgs e )
        {
            this._MessagePushedEventHandlers.Send( sender, e );
        }

        /// <summary>
        /// Safely and synchronously <see cref="EventHandlerContext{TEventArgs}.Send">sends</see> or invokes the
        /// <see cref="MessagePushed">MessagePushed Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
        protected void SyncNotifyMessagePushed( EventArgs e )
        {
            this._MessagePushedEventHandlers.Send( this, e );
        }

        #endregion

        #endregion

    }
}
