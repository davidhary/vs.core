using System;
using System.Diagnostics;

namespace isr.Core
{
    /// <summary> Defines a Trace Message. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2013-09-04, 1.2.4955. based on the legacy extended message. </para>
    /// </remarks>
    public class TraceMessage
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
        /// <param name="id">        The identifier to use with the trace event. </param>
        /// <param name="format">    Describes the format to use. </param>
        /// <param name="args">      A variable-length parameters list containing arguments. </param>
        public TraceMessage( TraceEventType eventType, int id, string format, params object[] args ) : this( eventType, id, string.Format( format, args ) )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
        /// <param name="id">        The identifier to use with the trace event. </param>
        /// <param name="details">   The details. </param>
        public TraceMessage( TraceEventType eventType, int id, string details ) : base()
        {
            this.Initialize( eventType, id, details );
        }

        /// <summary> Initializes this object. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
        /// <param name="id">        The identifier to use with the trace event. </param>
        /// <param name="details">   The details. </param>
        private void Initialize( TraceEventType eventType, int id, string details )
        {
            this.Timestamp = DateTimeOffset.Now;
            this.EventType = eventType;
            this.Id = id;
            this.SynopsisDelimiter = DefaultSynopsisDelimiter;
            this.TraceMessageFormat = DefaultTraceMessageFormat;
            this.Details = details;
        }

        /// <summary> Makes a deep copy of this object. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="value"> The value. </param>
        private void DeepCopy( TraceMessage value )
        {
            if ( value is null )
            {
                this.Clone( Empty );
            }
            else
            {
                this.Initialize( value.EventType, value.Id, value.Details );
                this.Timestamp = value.Timestamp;
            }
        }

        /// <summary> Makes a deep copy of this object. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="value"> The value. </param>
        public void Clone( TraceMessage value )
        {
            this.DeepCopy( value );
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="value"> The value. </param>
        public TraceMessage( TraceMessage value ) : this( TraceEventType.Information, 0, string.Empty )
        {
            this.Clone( value );
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        private TraceMessage() : this( TraceEventType.Information, 0, string.Empty )
        {
        }

        /// <summary> Gets an empty <see cref="TraceMessage">Trace Message</see>. </summary>
        /// <value> The empty. </value>
        public static TraceMessage Empty => new();

        #endregion

        #region " EQUALS "

        /// <summary> Tests if two TraceMessage objects are considered equal. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="value"> The value. </param>
        /// <param name="same">  Trace message to be compared. </param>
        /// <returns> <c>true</c> if the objects are considered equal, false if they are not. </returns>
        public static bool Equals( TraceMessage value, TraceMessage same )
        {
            return value is null && same is null || value is object && same is object && string.Equals( value.Details, same.Details );
        }

        #endregion

        #region " SYNOPSIS "

        /// <summary> The default synopsis delimiter. </summary>
        public const string DefaultSynopsisDelimiter = ";. ";

        /// <summary> Gets or sets the synopsis delimiter. </summary>
        /// <value> The synopsis delimiter. </value>
        public string SynopsisDelimiter { get; set; }

        /// <summary> Synopsis index. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="value">     The value. </param>
        /// <param name="delimiter"> The delimiter. </param>
        /// <returns> An Integer. </returns>
        public static int SynopsisIndex( string value, string delimiter )
        {
            return string.IsNullOrWhiteSpace( delimiter ) || string.IsNullOrWhiteSpace( value ) ? 0 : value.IndexOf( delimiter, StringComparison.OrdinalIgnoreCase );
        }

        /// <summary> Extracts the synopsis. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="value">     The value. </param>
        /// <param name="delimiter"> The delimiter. </param>
        /// <param name="maxLength"> The maximum length. </param>
        /// <returns> The extracted synopsis. </returns>
        public static string ExtractSynopsis( string value, string delimiter, int maxLength )
        {
            int synopsisIndex = SynopsisIndex( value, delimiter );
            if ( synopsisIndex > 0 )
            {
                if ( maxLength > 0 && synopsisIndex > maxLength )
                {
                    synopsisIndex = maxLength;
                }

                return value.Substring( 0, synopsisIndex );
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary> Gets or sets the length of the maximum synopsis. </summary>
        /// <value> The length of the maximum synopsis. </value>
        public int MaxSynopsisLength { get; set; }

        /// <summary> Extracts the synopsis. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns> The extracted synopsis. </returns>
        public string ExtractSynopsis()
        {
            return ExtractSynopsis( this.Details, this.SynopsisDelimiter, this.MaxSynopsisLength );
        }

        /// <summary> Extracts the synopsis. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="maxLength"> The maximum length. </param>
        /// <returns> The extracted synopsis. </returns>
        public string ExtractSynopsis( int maxLength )
        {
            return ExtractSynopsis( this.Details, this.SynopsisDelimiter, maxLength );
        }

        #endregion

        #region " DETAILS "

        /// <summary> Gets or sets the Trace Message. </summary>
        /// <value> The details. </value>
        public string Details { get; set; }

        /// <summary> Gets or sets the Trace Message time stamp as <see cref="DateTimeOffset"/>. </summary>
        /// <value> The timestamp. </value>
        public DateTimeOffset Timestamp { get; private set; }

        /// <summary> Gets or sets the <see cref="TraceEventType">event type</see>. </summary>
        /// <value> The <see cref="TraceEventType">event type</see>. </value>
        public TraceEventType EventType { get; set; }

        /// <summary> Gets or sets the identifier to use with the trace event. </summary>
        /// <value> The identifier to use with the trace event. </value>
        public int Id { get; set; }

        #endregion

        #region " TO STRING "

        /// <summary> The default trace message format. </summary>
        public const string DefaultTraceMessageFormat = "{0},{1,5:X},{2:HH:mm:ss.fff}Z,{3}";

        /// <summary> Gets or sets the default format for displaying the message. </summary>
        /// <remarks>
        /// The format must include 4 elements to display the first two characters of the trace event
        /// type, the id, timestamp and message details. For example,<code>
        /// "{0},{1,5:X},{2:HH:mm:ss.fff}Z,{3}"</code>.
        /// </remarks>
        /// <value> The trace message format. </value>
        public string TraceMessageFormat { get; set; }

        /// <summary> Returns a message based on the default format. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns>
        /// A representation of the trace message based on the <see cref="TraceMessageFormat">message
        /// format</see>.
        /// </returns>
        public override string ToString()
        {
            return string.IsNullOrWhiteSpace( this.TraceMessageFormat ) ? this.ToString( DefaultTraceMessageFormat ) : this.ToString( this.TraceMessageFormat );
        }

        /// <summary> Returns a message based on the default format. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="format"> Describes the format to use. </param>
        /// <returns>
        /// A representation of the trace message based on the <see cref="TraceMessageFormat">message
        /// format</see>.
        /// </returns>
        public string ToString( string format )
        {
            return string.Format( System.Globalization.CultureInfo.CurrentCulture, format, this.EventType.ToString().Substring( 0, 2 ), this.Id, this.Timestamp.UtcDateTime, this.Details );
        }

        #endregion

    }
}
