using System.Diagnostics;

namespace isr.Core
{
    /// <summary> Interface for a trace message publisher. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public interface ITraceMessageTalker : IPublisher
    {

        /// <summary> Gets or sets a message describing the trace. </summary>
        /// <value> A message describing the trace. </value>
        TraceMessage TraceMessage { get; }

        /// <summary> Publishes the message. </summary>
        /// <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
        /// <returns> A String. </returns>
        string Publish( TraceMessage value );

        /// <summary> Publishes the message. </summary>
        /// <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
        /// <param name="id">        The identifier to use with the trace event. </param>
        /// <param name="format">    Describes the format to use. </param>
        /// <param name="args">      A variable-length parameters list containing arguments. </param>
        /// <returns> A String. </returns>
        string Publish( TraceEventType eventType, int id, string format, params object[] args );

        /// <summary> Publishes overriding the listeners trace level. </summary>
        /// <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
        /// <returns> A String. </returns>
        string PublishOverride( TraceMessage value );

        /// <summary> Publishes overriding the listeners trace level. </summary>
        /// <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
        /// <param name="id">        The identifier to use with the trace event. </param>
        /// <param name="format">    Describes the format to use. </param>
        /// <param name="args">      A variable-length parameters list containing arguments. </param>
        /// <returns> A String. </returns>
        string PublishOverride( TraceEventType eventType, int id, string format, params object[] args );

        /// <summary> Publishes date message. </summary>
        /// <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
        /// <param name="id">        The identifier to use with the trace event. </param>
        /// <param name="format">    Describes the format to use. </param>
        /// <param name="args">      A variable-length parameters list containing arguments. </param>
        /// <returns> A String. </returns>
        string PublishDateMessage( TraceEventType eventType, int id, string format, params object[] args );

        /// <summary> Publishes date message. </summary>
        /// <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
        /// <returns> A String. </returns>
        string PublishDateMessage( TraceMessage value );

        /// <summary>
        /// Determines if the message with the specified level is publishable on the listener type.
        /// </summary>
        /// <param name="listenerType"> Type of the listener. </param>
        /// <param name="value">        The value. </param>
        /// <returns> <c>true</c> if publishable; otherwise <c>false</c> </returns>
        bool Publishable( ListenerType listenerType, TraceEventType value );

        /// <summary> Gets or sets the listeners. </summary>
        /// <value> The listeners. </value>
        MessageListenerCollection Listeners { get; }

        /// <summary> Gets or sets the trace log level. </summary>
        /// <value> The trace level. </value>
        TraceEventType TraceLogLevel { get; }

        /// <summary> Gets or sets the trace Show level. </summary>
        /// <value> The trace level. </value>
        TraceEventType TraceShowLevel { get; }

        /// <summary> Identify talker. </summary>
        /// <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
        void IdentifyTalker( TraceMessage value );
    }
}
