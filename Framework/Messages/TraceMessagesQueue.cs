using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Linq;

using isr.Core.StackTraceExtensions;

namespace isr.Core
{

    /// <summary> Queue of trace messages. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-03-23 </para>
    /// </remarks>
    public class TraceMessagesQueue
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructor for this class. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        public TraceMessagesQueue() : base()
        {
            this.ContentsQueue = new ConcurrentQueue<TraceMessage>();
        }

        #endregion

        #region " DEQUEUE "

        /// <summary> Gets any. </summary>
        /// <value> any. </value>
        public bool Any => this.ContentsQueue.Any();

        /// <summary> Gets the number of. </summary>
        /// <value> The count. </value>
        public int Count => this.ContentsQueue.Count;

        /// <summary> Gets or sets a queue of contents. </summary>
        /// <value> A queue of contents. </value>
        protected ConcurrentQueue<TraceMessage> ContentsQueue { get; private set; }

        /// <summary> Gets contents queue. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns> The contents queue. </returns>
        private ConcurrentQueue<TraceMessage> GetContentsQueue()
        {
            return this.ContentsQueue;
        }

        /// <summary> Gets the content. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns> The content. </returns>
        public TraceMessage TryDequeue()
        {
            _ = this.GetContentsQueue().TryDequeue( out TraceMessage result );
            return result;
        }

        /// <summary>   Try peek. </summary>
        /// <remarks>   David, 2020-09-17. </remarks>
        /// <returns>   A TraceMessage. </returns>
        public TraceMessage TryPeek()
        {
            _ = this.GetContentsQueue().TryPeek( out TraceMessage result );
            return result;
        }

        /// <summary> Query if the contents queue is empty. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns> <c>true</c> the contents queue is empty; otherwise <c>false</c> </returns>
        public bool IsEmpty()
        {
            return this.GetContentsQueue().IsEmpty;
        }

        /// <summary> Fetches the content. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns> The content. </returns>
        public string DequeueContent()
        {
            int messageNumber = 0;
            var builder = new System.Text.StringBuilder();
            do
            {
                var value = this.TryDequeue();
                if ( value is object )
                {
                    messageNumber += 1;
                    _ = builder.AppendLine( $"{messageNumber}:>{value}" );
                }
            }
            while ( !this.GetContentsQueue().IsEmpty );
            return builder.ToString().TrimEnd( Environment.NewLine.ToCharArray() );
        }

        #endregion

        #region " ENQUEUE "

        /// <summary> Adds a message. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> A String. </returns>
        protected string Enqueue( string value )
        {
            if ( string.IsNullOrWhiteSpace( value ) )
            {
                value = string.Empty;
            }

            this.Enqueue( new TraceMessage( TraceEventType.Information, My.MyLibrary.TraceEventId, value ) );
            return value;
        }

        /// <summary> Adds a message. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="value"> The value. </param>
        public void Enqueue( TraceMessage value )
        {
            if ( value is null || string.IsNullOrWhiteSpace( value.Details ) )
            {
                Debug.Assert( !Debugger.IsAttached, "Empty trace message", new StackTrace( true ).UserCallStack( 0, 0 ) );
            }
            else
            {
                long lastCount = (this.GetContentsQueue()?.LongCount()).GetValueOrDefault( 0L );
                this.GetContentsQueue()?.Enqueue( value );
                if ( (this.GetContentsQueue()?.LongCount()).GetValueOrDefault( 0L ) > lastCount )
                {
                    this._MessageEnqueuedEventHandlers?.Send( this, EventArgs.Empty );
                }
            }
        }

        #region " MESSAGE ENQUEUED "

        /// <summary> Removes the MessageEnqueued event handlers. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        protected void RemoveMessageEnqueuedEventHandlers()
        {
            this._MessageEnqueuedEventHandlers?.RemoveAll();
        }

        /// <summary> The MessageEnqueued event handlers. </summary>
        private readonly EventHandlerContextCollection<EventArgs> _MessageEnqueuedEventHandlers = new();

        /// <summary> Event queue for all listeners interested in MessageEnqueued events. </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event EventHandler<EventArgs> MessageEnqueued
        {
            add {
                this._MessageEnqueuedEventHandlers.Add( new EventHandlerContext<EventArgs>( value ) );
            }

            remove {
                this._MessageEnqueuedEventHandlers.RemoveValue( value );
            }
        }

        private void OnMessageEnqueued( object sender, EventArgs e )
        {
            this._MessageEnqueuedEventHandlers.Send( sender, e );
        }

        /// <summary>
        /// Safely and synchronously <see cref="EventHandlerContext{TEventArgs}.Send(object, TEventArgs)">sends</see> or invokes the
        /// <see cref="MessageEnqueued">MessageEnqueued Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
        protected void SyncNotifyMessageEnqueued( EventArgs e )
        {
            this._MessageEnqueuedEventHandlers.Send( this, e );
        }

        #endregion

        #endregion

    }
}
