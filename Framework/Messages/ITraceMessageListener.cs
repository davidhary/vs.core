using System.Collections.Generic;
using System.Diagnostics;

namespace isr.Core
{
    /// <summary> Interface for trace message listener. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public interface ITraceMessageListener : IMessageListener
    {

        /// <summary> Trace event. </summary>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="id">        The identifier. </param>
        /// <param name="format">    Describes the format to use. </param>
        /// <param name="args">      A variable-length parameters list containing arguments. </param>
        /// <returns> A String. </returns>
        string TraceEvent( TraceEventType eventType, int id, string format, params object[] args );

        /// <summary> Writes a trace event to the trace listeners. </summary>
        /// <param name="value"> The event message. </param>
        /// <returns> The trace message details. </returns>
        string TraceEvent( TraceMessage value );

        /// <summary> Trace event overriding the trace level. </summary>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="id">        The identifier. </param>
        /// <param name="format">    Describes the format to use. </param>
        /// <param name="args">      A variable-length parameters list containing arguments. </param>
        /// <returns> A String. </returns>
        string TraceEventOverride( TraceEventType eventType, int id, string format, params object[] args );

        /// <summary> Trace event overriding the trace level. </summary>
        /// <param name="value"> The event message. </param>
        /// <returns> A String. </returns>
        string TraceEventOverride( TraceMessage value );

        /// <summary>
        /// Query if talker with identity as specified in the trace message was identified.
        /// </summary>
        /// <param name="value"> The event message. </param>
        /// <returns> <c>true</c> if talker identified; otherwise <c>false</c> </returns>
        bool IsTalkerIdentified( TraceMessage value );

        /// <summary> Identify a talker using the talker identity. </summary>
        /// <param name="value"> The event message. </param>
        void IdentifyTalker( TraceMessage value );

        /// <summary> Enumerates talker identity messages in this collection. </summary>
        /// <returns>
        /// An enumerator that allows for each to be used to process talker identity messages in this
        /// collection.
        /// </returns>
        IList<TraceMessage> TalkerIdentityMessages();

        /// <summary> Query if 'value' is talker date message published. </summary>
        /// <param name="value"> The event message. </param>
        /// <returns> <c>true</c> if talker date message published; otherwise <c>false</c> </returns>
        bool IsTalkerDateMessagePublished( TraceMessage value );

        /// <summary> Adds a date message. </summary>
        /// <param name="value"> The event message. </param>
        void AddDateMessage( TraceMessage value );

        /// <summary> Enumerates talker date messages in this collection. </summary>
        /// <returns>
        /// An enumerator that allows foreach to be used to process talker date messages in this
        /// collection.
        /// </returns>
        IList<TraceMessage> TalkerDateMessages();
    }
}
