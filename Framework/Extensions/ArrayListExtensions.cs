using System.Collections;

namespace isr.Core.ArrayListExtensions
{

    /// <summary> Includes extensions for <see cref="ArrayList">array lists</see>. </summary>
    /// <remarks> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-11-07, 2.1.5425 </para></remarks>
    public static class ArrayListExtensionMethods
    {

        /// <summary> Concatenates. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="values">    The values. </param>
        /// <param name="delimiter"> The delimiter. </param>
        /// <returns> A String. </returns>
        public static string Concatenate( this ArrayList values, string delimiter )
        {
            var builder = new System.Text.StringBuilder();
            if ( values is object )
            {
                foreach ( string v in values )
                {
                    if ( builder.Length > 0 )
                    {
                        _ = builder.Append( delimiter );
                    }

                    _ = builder.Append( v );
                }
            }

            return builder.ToString();
        }
    }
}
