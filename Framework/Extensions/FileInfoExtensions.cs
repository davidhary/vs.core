using System;

namespace isr.Core.FileInfoExtensions
{
    /// <summary> Includes extensions for  <see cref="T:System.IO.FileInfo">file info</see>. </summary>
    /// <remarks> (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2009-04-09, 1.1.3386 </para></remarks>
    public static class FileInfoExtensionMethods
    {

        /// <summary> Returns the file size, -2 if path not specified or -1 if file not found. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="path"> The path. </param>
        /// <returns> System.Int64. </returns>
        public static long FileSize( string path )
        {
            if ( string.IsNullOrWhiteSpace( path ) )
            {
                return 0L;
            }

            var info = new System.IO.FileInfo( path );
            return info.FileSize();
        }

        /// <summary> Returns the file size, -2 if path not specified or -1 if file not found. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns>
        /// -2 if value Is Nothing or the file name is empty; the size if the file exists; otherwise, -1.
        /// </returns>
        public static long FileSize( this System.IO.FileInfo value )
        {
            return value is null ? -2 : value.Exists ? value.Length : string.IsNullOrWhiteSpace( value.Name ) ? -2 : -1;
        }

        /// <summary> Move to folder. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="value">    The value. </param>
        /// <param name="folder">   Pathname of the folder. </param>
        /// <param name="override"> true to override. If file exists, move is not done. </param>
        public static void MoveToFolder( this System.IO.FileInfo value, string folder, bool @override )
        {
            if ( value is null )
            {
                throw new ArgumentNullException( nameof( value ) );
            }
            else if ( string.IsNullOrWhiteSpace( folder ) )
            {
                throw new ArgumentNullException( nameof( folder ) );
            }
            else
            {
                string destinationFullName = System.IO.Path.Combine( folder, value.Name );
                if ( System.IO.File.Exists( destinationFullName ) )
                {
                    if ( @override )
                    {
                        System.IO.File.Delete( destinationFullName );
                    }
                    else
                    {
                        return;
                    }
                }

                value.MoveTo( destinationFullName );
            }
        }

        /// <summary> Parses file title. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="name">      The name. </param>
        /// <param name="extension"> The extension. </param>
        /// <returns> A String. </returns>
        private static string ParseFileTitle( string name, string extension )
        {
            return string.IsNullOrWhiteSpace( name ) ? string.Empty : string.IsNullOrWhiteSpace( extension ) ? name : name.Substring( 0, name.LastIndexOf( extension, StringComparison.OrdinalIgnoreCase ) );
        }

        /// <summary> Returns the file name without extension. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> The file name or white space if the file not found. </returns>
        public static string Title( this System.IO.FileInfo value )
        {
            return value is null ? string.Empty : ParseFileTitle( value.Name, value.Extension );
        }
    }
}
