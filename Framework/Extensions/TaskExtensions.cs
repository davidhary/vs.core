using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace isr.Core.TaskExtensions
{
    /// <summary> Task extension methods. </summary>
    /// <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para> </remarks>
    public static class TaskExtensionMethods
    {

        /// <summary> Queries if a task is active. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="task"> The task. </param>
        /// <returns> <c>true</c> if the action task is active; otherwise <c>false</c> </returns>
        public static bool IsTaskActive( this Task task )
        {
            return task is object && !(task.IsCanceled || task.IsCompleted || task.IsFaulted);
        }

        /// <summary> Query if task ended. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="task"> The task. </param>
        /// <returns> <c>true</c> if task ended; otherwise <c>false</c> </returns>
        public static bool IsTaskEnded( this Task task )
        {
            return task is null || task.IsCanceled || task.IsCompleted || task.IsFaulted;
        }

        /// <summary> Queries if a task is active. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="status"> The status. </param>
        /// <returns> <c>true</c> if a task is active; otherwise <c>false</c> </returns>
        public static bool IsTaskActive( this TaskStatus status )
        {
            return status == TaskStatus.Created || status == TaskStatus.Running;
        }

        /// <summary> Query if task ended. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="status"> The status. </param>
        /// <returns> <c>true</c> if task ended; otherwise <c>false</c> </returns>
        public static bool IsTaskEnded( this TaskStatus status )
        {
            return status == TaskStatus.RanToCompletion || status == TaskStatus.Canceled || status == TaskStatus.Faulted;
        }

        /// <summary> Wait task active. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="task">    The task. </param>
        /// <param name="timeout"> The timeout. </param>
        public static void WaitTaskActive( this Task task, TimeSpan timeout )
        {
            var sw = Stopwatch.StartNew();
            while ( !task.IsTaskActive() && sw.Elapsed <= timeout )
            {
                ApplianceBase.DoEvents();
            }
        }

        /// <summary> Wait idle. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="task">    The task. </param>
        /// <param name="timeout"> The timeout. </param>
        public static void WaitTaskIdle( Task task, TimeSpan timeout )
        {
            var sw = Stopwatch.StartNew();
            while ( !task.IsTaskEnded() && sw.Elapsed <= timeout )
            {
                ApplianceBase.DoEvents();
            }
        }
    }
}
