using System;

namespace isr.Core.DateTimeExtensions
{
    /// <summary>   A date time extension methods. </summary>
    /// <remarks>   David, 2020-12-07. </remarks>
    public static class DateTimeExtensionMethods
    {
        /// <summary>
        /// The number of ticks per microsecond.
        /// </summary>
        public const int TicksPerMicrosecond = 10;
        /// <summary>
        /// The number of ticks per Nanosecond.
        /// </summary>
        public const int NanosecondsPerTick = 100;

        /// <summary>   Gets the microsecond fraction of a DateTime. </summary>
        /// <remarks>   David, 2020-12-07. </remarks>
        /// <param name="self"> . </param>
        /// <returns>   An int. </returns>
        public static int Microseconds( this DateTime self )
        {
            return ( int ) Math.Floor(
               (self.Ticks
               % TimeSpan.TicksPerMillisecond)
               / ( double ) TicksPerMicrosecond );
        }
        /// <summary>
        /// Gets the Nanosecond fraction of a DateTime.  Note that the DateTime
        /// object can only store nanoseconds at resolution of 100 nanoseconds.
        /// </summary>
        /// <param name="self">The DateTime object.</param>
        /// <returns>the number of Nanoseconds.</returns>
        public static int Nanoseconds( this DateTime self )
        {
            return ( int ) (self.Ticks % TimeSpan.TicksPerMillisecond % TicksPerMicrosecond) * NanosecondsPerTick;
        }
        /// <summary>   Adds a number of microseconds to this DateTime object. </summary>
        /// <remarks>   David, 2020-12-07. </remarks>
        /// <param name="self">         The DateTime object. </param>
        /// <param name="microseconds"> The number of milliseconds to add. </param>
        /// <returns>   A DateTime. </returns>
        public static DateTime AddMicroseconds( this DateTime self, double microseconds )
        {
            return self.AddTicks( ( long ) (microseconds * TicksPerMicrosecond) );
        }
        /// <summary>
        /// Adds a number of nanoseconds to this DateTime object.  Note: this object only stores
        /// nanoseconds of resolutions of 100 seconds. Any nanoseconds passed in lower than that will be
        /// rounded using the default rounding algorithm in Math.Round().
        /// </summary>
        /// <remarks>   David, 2020-12-07. </remarks>
        /// <param name="self">         The DateTime object. </param>
        /// <param name="nanoseconds">  The number of nanoseconds to add. </param>
        /// <returns>   A DateTime. </returns>
        public static DateTime AddNanoseconds( this DateTime self, double nanoseconds )
        {
            return self.AddTicks( ( long ) Math.Round( nanoseconds / ( double ) NanosecondsPerTick ) );
        }

    }
}
