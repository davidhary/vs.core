using System;
using System.Windows.Input;

namespace isr.Core.ICommandExtensions
{
    /// <summary>
    /// Extensions to the <see cref="System.Windows.Input.ICommand">interface</see>.
    /// </summary>
    /// <remarks>
    /// https://stackoverflow.com/questions/10126968/call-command-from-code-behind.
    /// (c) 2017 Stefan Vasiljevic. All rights reserved.<para>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
    /// Licensed under The MIT License.</para> </remarks>
    public static class ICommandExtensionMethods
    {

        /// <summary>
        /// Casts the <see cref="Object"/> to an <see cref="ICommand"/> and executes the command if
        /// enabled (can execute).
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="command"> The command. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool CheckBeginExecute( this object command )
        {
            return command is ICommand iCommand && CheckBeginExecuteCommand( iCommand );
        }

        /// <summary> Executes the command if enabled (can execute). </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="command"> The command. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool CheckBeginExecute( this ICommand command )
        {
            return CheckBeginExecuteCommand( command );
        }

        /// <summary> Executes the command if enabled (can execute). </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="command"> The command. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        private static bool CheckBeginExecuteCommand( ICommand command )
        {
            if ( command is null )
            {
                throw new ArgumentNullException( nameof( command ) );
            }

            bool canExecute = false;
            lock ( command )
            {
                canExecute = command.CanExecute( null );
                if ( canExecute )
                {
                    command.Execute( null );
                }
            }

            return canExecute;
        }
    }
}
