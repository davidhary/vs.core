// Requires: Reference to Windows Base DLL

using System;
using System.Diagnostics;
using System.Windows.Threading;

namespace isr.Core.DispatcherExtensions
{

    /// <summary> Dispatcher extension methods. </summary>
    /// <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para> </remarks>
    public static class DispatcherExtensionMethods
    {

        /// <summary>   Lets Windows process all the messages currently in the message queue. </summary>
        /// <remarks>
        /// https://stackoverflow.com/questions/4502037/where-is-the-application-doevents-in-wpf. Well, I
        /// just hit a case where I start work on a method that runs on the Dispatcher thread, and it
        /// needs to block without blocking the UI Thread. Turns out that MSDN topic on the Dispatcher
        /// Push Frame method explains how to implement a DoEvents() based on the Dispatcher itself:
        /// https://goo.gl/WGFZEU <para>
        /// Note that the first call to <see cref="DispatcherExtensionMethods.DoEvents(Dispatcher)"/> takes some extra 90
        /// ms.</para>
        /// <code>
        /// VB:
        /// Imports System.Windows.Threading
        /// Imports isr.Core.DispatcherExtensions
        /// isr.Core.ApplianceBase.DoEvents()
        /// C#:
        /// using System.Windows.Threading
        /// using isr.Core.DispatcherExtensions
        /// isr.Core.ApplianceBase.DoEvents()
        /// </code>
        /// <list type="bullet">Benchmarks<item>
        /// 1000 iterations of Application Do Events:   7.8,  8.0, 11,  8.4 us </item><item>
        /// 1000 iterations of Dispatcher Do Events:   57.6, 92.5, 93, 61.6 us </item></list>
        /// </remarks>
        /// <param name="dispatcher">   The dispatcher. </param>
        public static void DoEvents( this Dispatcher dispatcher )
        {
            var frame = new DispatcherFrame();
            _ = dispatcher.BeginInvoke( DispatcherPriority.Background, new DispatcherOperationCallback( ( f ) => {
                (f as DispatcherFrame).Continue = false;
                return null;
            } ), frame );
            Dispatcher.PushFrame( frame );
        }

        /// <summary>
        /// Executes the specified delegate on the <see cref="DispatcherPriority.Render"/> priority.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="dispatcher"> The dispatcher. </param>
        /// <param name="act">        The act. </param>
        public static void Render( this Dispatcher dispatcher, Action act )
        {
            if ( dispatcher is null )
            {
                throw new ArgumentNullException( nameof( dispatcher ) );
            }

            _ = dispatcher.Invoke( DispatcherPriority.Render, act );
        }

        /// <summary>   Waits while the stop watch is running until its elapsed time expires. </summary>
        /// <remarks>   David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="dispatcher">   The dispatcher. </param>
        /// <param name="stopwatch">    The stop watch. </param>
        /// <param name="elapsedTime">  The total time to elapse including the Stopwatch elapsed time. </param>
        /// <returns>   The elapsed <see cref="TimeSpan"/> </returns>
        public static TimeSpan LetElapse( this Dispatcher dispatcher, Stopwatch stopwatch, TimeSpan elapsedTime )
        {
            if ( dispatcher is null )
            {
                throw new ArgumentNullException( nameof( dispatcher ) );
            }

            if ( stopwatch is object && elapsedTime > TimeSpan.Zero && stopwatch.IsRunning )
            {
                do
                {
                    dispatcher.DoEvents();
                }
                while ( stopwatch.Elapsed <= elapsedTime );
                return stopwatch.Elapsed;
            }
            else
            {
                return TimeSpan.Zero;
            }
        }
    }
}
