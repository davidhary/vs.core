using System;
using System.Linq;

namespace isr.Core.BinaryWriterExtensions
{
    /// <summary> Includes extensions for <see cref="System.IO.BinaryWriter">binary writer</see>. </summary>
    /// <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-01-23, 2.0.5136.x. based on I/O Library binary reader. </para></remarks>
    public static class BinaryWriterExtensionMethods
    {

        /// <summary> Closes the binary Writer and base stream. </summary>
        /// <remarks> Use this method to close the instance. </remarks>
        /// <param name="writer"> The writer. </param>
        public static void CloseWriter( this System.IO.BinaryWriter writer )
        {
            if ( writer is object )
            {
                writer.Close();
                if ( writer is object && writer.BaseStream is object )
                {
                    writer.BaseStream.Close();
                }
            }
        }

        /// <summary> Opens a stream. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="filePathName"> Specifies the name of the binary file which to read. </param>
        /// <returns> A System.IO.FileStream. </returns>
        public static System.IO.FileStream OpenStream( string filePathName )
        {
            System.IO.FileStream fileStream = null;
            System.IO.FileStream tempFileStream = null;
            if ( !string.IsNullOrWhiteSpace( filePathName ) )
            {
                try
                {
                    tempFileStream = new System.IO.FileStream( filePathName, System.IO.FileMode.Open, System.IO.FileAccess.Write );
                    fileStream = tempFileStream;
                }
                catch
                {
                    tempFileStream?.Dispose();
                    throw;
                }
            }

            return fileStream;
        }

        /// <summary>
        /// Opens a binary file for Writing and returns a reference to the Writer. The file is
        /// <see cref="System.IO.FileMode.Open">opened</see> in
        /// <see cref="System.IO.FileAccess.Write">Write access</see>.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="filePathName"> Specifies the file name. </param>
        /// <returns> A reference to an open <see cref="System.IO.BinaryWriter">binary Writer</see>. </returns>
        public static System.IO.BinaryWriter OpenWriter( string filePathName )
        {
            if ( string.IsNullOrWhiteSpace( filePathName ) )
            {
                throw new ArgumentNullException( nameof( filePathName ) );
            }

            System.IO.BinaryWriter tempWriter = null;
            System.IO.FileStream stream = null;
            System.IO.BinaryWriter writer;
            try
            {
                stream = OpenStream( filePathName );
                tempWriter = new System.IO.BinaryWriter( stream );
                writer = tempWriter;
            }
            catch
            {
                stream?.Dispose();
                tempWriter?.Dispose();
                throw;
            }

            return writer;
        }

        #region " DOUBLE "

        /// <summary>
        /// Writes a single-dimension <see cref="Double">double-precision</see>
        /// array to the values file.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="writer"> The writer. </param>
        /// <param name="values"> Holds the values to write. </param>
        public static void Write( this System.IO.BinaryWriter writer, double[] values )
        {
            if ( writer is null )
            {
                throw new ArgumentNullException( nameof( writer ) );
            }

            if ( values is null )
            {
                throw new ArgumentNullException( nameof( values ) );
            }

            // write the values size for verifying size and, indirectly also location, upon read
            writer.Write( Convert.ToInt32( values.Length ) );

            // write values to the file
            if ( values.Any() )
            {
                for ( int i = values.GetLowerBound( 0 ), loopTo = values.GetUpperBound( 0 ); i <= loopTo; i++ )
                {
                    writer.Write( values[i] );
                }
            }
        }

        /// <summary> Writes a double-precision value to the values file. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="writer">   The writer. </param>
        /// <param name="value">    Specifies the value to write. </param>
        /// <param name="location"> Specifies the file location. </param>
        public static void Write( this System.IO.BinaryWriter writer, double value, long location )
        {
            if ( writer is null )
            {
                throw new ArgumentNullException( nameof( writer ) );
            }

            _ = writer.BaseStream.Seek( location, System.IO.SeekOrigin.Begin );
            writer.Write( value );
        }

        #endregion

        #region " INT32 "

        /// <summary>
        /// Writes a single-dimension <see cref="Int32">Integer</see>
        /// array to the values file.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="writer"> The writer. </param>
        /// <param name="values"> Holds the values to write. </param>
        public static void Write( this System.IO.BinaryWriter writer, int[] values )
        {
            if ( writer is null )
            {
                throw new ArgumentNullException( nameof( writer ) );
            }

            if ( values is null )
            {
                throw new ArgumentNullException( nameof( values ) );
            }

            // write the values size for verifying size and, indirectly also location, upon read
            writer.Write( Convert.ToInt32( values.Length ) );

            // write values to the file
            if ( values.Any() )
            {
                for ( int i = values.GetLowerBound( 0 ), loopTo = values.GetUpperBound( 0 ); i <= loopTo; i++ )
                {
                    writer.Write( values[i] );
                }
            }
        }

        /// <summary> Writes an Int32 value to the values file. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="writer">   The writer. </param>
        /// <param name="value">    Specifies the value to write. </param>
        /// <param name="location"> Specifies the file location. </param>
        public static void Write( this System.IO.BinaryWriter writer, int value, long location )
        {
            if ( writer is null )
            {
                throw new ArgumentNullException( nameof( writer ) );
            }

            _ = writer.BaseStream.Seek( location, System.IO.SeekOrigin.Begin );
            writer.Write( value );
        }

        #endregion

        #region " INT64 "

        /// <summary>
        /// Writes a single-dimension <see cref="Int64">Long Integer</see>
        /// array to the values file.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="writer"> The writer. </param>
        /// <param name="values"> Holds the values to write. </param>
        public static void Write( this System.IO.BinaryWriter writer, long[] values )
        {
            if ( writer is null )
            {
                throw new ArgumentNullException( nameof( writer ) );
            }

            if ( values is null )
            {
                throw new ArgumentNullException( nameof( values ) );
            }

            // write the values size for verifying size and, indirectly also location, upon read
            writer.Write( Convert.ToInt32( values.Length ) );

            // write values to the file
            if ( values.Any() )
            {
                for ( int i = values.GetLowerBound( 0 ), loopTo = values.GetUpperBound( 0 ); i <= loopTo; i++ )
                {
                    writer.Write( values[i] );
                }
            }
        }

        /// <summary> Writes a Int64 value to the values file. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="writer">   The writer. </param>
        /// <param name="value">    Specifies the value to write. </param>
        /// <param name="location"> Specifies the file location. </param>
        public static void Write( this System.IO.BinaryWriter writer, long value, long location )
        {
            if ( writer is null )
            {
                throw new ArgumentNullException( nameof( writer ) );
            }

            _ = writer.BaseStream.Seek( location, System.IO.SeekOrigin.Begin );
            writer.Write( value );
        }

        #endregion

        #region " SINGLE "

        /// <summary>
        /// Writes a single-dimension <see cref="Single">single-precision</see>
        /// array to the values file.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="writer"> The writer. </param>
        /// <param name="values"> Holds the values to write. </param>
        public static void Write( this System.IO.BinaryWriter writer, float[] values )
        {
            if ( writer is null )
            {
                throw new ArgumentNullException( nameof( writer ) );
            }

            if ( values is null )
            {
                throw new ArgumentNullException( nameof( values ) );
            }

            // write the values size for verifying size and, indirectly also location, upon read
            writer.Write( Convert.ToInt32( values.Length ) );

            // write values to the file
            if ( values.Any() )
            {
                for ( int i = values.GetLowerBound( 0 ), loopTo = values.GetUpperBound( 0 ); i <= loopTo; i++ )
                {
                    writer.Write( values[i] );
                }
            }
        }

        /// <summary> Writes a single-precision value to the values file. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="writer">   The writer. </param>
        /// <param name="value">    Specifies the value to write. </param>
        /// <param name="location"> Specifies the file location. </param>
        public static void Write( this System.IO.BinaryWriter writer, float value, long location )
        {
            if ( writer is null )
            {
                throw new ArgumentNullException( nameof( writer ) );
            }

            _ = writer.BaseStream.Seek( location, System.IO.SeekOrigin.Begin );
            writer.Write( value );
        }

        #endregion

        #region " STRING "

        /// <summary>
        /// Writes a string to the binary file padding it with spaces as necessary to fill the length.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="writer"> The writer. </param>
        /// <param name="value">  Holds the value to write. </param>
        /// <param name="length"> The length to write. </param>
        public static void Write( this System.IO.BinaryWriter writer, string value, int length )
        {
            if ( writer is null )
            {
                throw new ArgumentNullException( nameof( writer ) );
            }

            if ( value is null )
            {
                throw new ArgumentNullException( nameof( value ) );
            }

            // pad but make sure not to exceed length.
            writer.Write( value.PadRight( length ).Substring( 0, length ) );
        }

        /// <summary> Writes a string value to the values file. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="writer">   The writer. </param>
        /// <param name="value">    Specifies the value to write. </param>
        /// <param name="location"> Specifies the file location. </param>
        public static void Write( this System.IO.BinaryWriter writer, string value, long location )
        {
            if ( writer is null )
            {
                throw new ArgumentNullException( nameof( writer ) );
            }

            if ( value is null )
            {
                throw new ArgumentNullException( nameof( value ) );
            }

            _ = writer.BaseStream.Seek( location, System.IO.SeekOrigin.Begin );
            writer.Write( value );
        }

        #endregion

        #region " DATE TIME "

        /// <summary> Writes a <see cref="System.DateTime">date time</see> value the file. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="writer"> The writer. </param>
        /// <param name="value">  Specifies the value to write. </param>
        public static void Write( this System.IO.BinaryWriter writer, DateTime value )
        {
            if ( writer is null )
            {
                throw new ArgumentNullException( nameof( writer ) );
            }

            writer.Write( BitConverter.GetBytes( value.ToOADate() ) );
        }

        /// <summary> Writes a <see cref="System.DateTime">date time</see> value the file. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="writer">   The writer. </param>
        /// <param name="value">    Specifies the value to write. </param>
        /// <param name="location"> Specifies the file location. </param>
        public static void Write( this System.IO.BinaryWriter writer, DateTime value, long location )
        {
            if ( writer is null )
            {
                throw new ArgumentNullException( nameof( writer ) );
            }

            _ = writer.BaseStream.Seek( location, System.IO.SeekOrigin.Begin );
            writer.Write( value );
        }


        #endregion

        #region " TIME SPAN "

        /// <summary> Writes a <see cref="TimeSpan">time span</see> value the file. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="writer"> The writer. </param>
        /// <param name="value">  Specifies the value to write. </param>
        public static void Write( this System.IO.BinaryWriter writer, TimeSpan value )
        {
            if ( writer is null )
            {
                throw new ArgumentNullException( nameof( writer ) );
            }

            writer.Write( value.TotalMilliseconds );
        }

        /// <summary> Writes a a <see cref="TimeSpan">time span</see> value the file. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="writer">   The writer. </param>
        /// <param name="value">    Specifies the value to write. </param>
        /// <param name="location"> Specifies the file location. </param>
        public static void Write( this System.IO.BinaryWriter writer, TimeSpan value, long location )
        {
            if ( writer is null )
            {
                throw new ArgumentNullException( nameof( writer ) );
            }

            _ = writer.BaseStream.Seek( location, System.IO.SeekOrigin.Begin );
            writer.Write( value );
        }

        #endregion


    }
}
