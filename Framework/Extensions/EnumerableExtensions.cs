using System;
using System.Collections.Generic;
using System.Linq;

namespace isr.Core.EnumerableExtensions
{
    /// <summary> Includes extensions for <see cref="Enumerable">Enumerable</see>. </summary>
    /// <remarks> (c) 2016 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 08/167/2016, 3.0.6072 </para></remarks>
    public static class EnumerableExtensionMethods
    {
        /// <summary>
        /// Finds the first index of the element that is lower or equal to the search value where the
        /// next element is equal or higher then the search value.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values">      The values. </param>
        /// <param name="searchValue"> The search value. </param>
        /// <returns> The index of the highest lower value. </returns>
        public static int BinaryFindBoundingIndex( this IEnumerable<double> values, double searchValue )
        {
            if ( values is null )
            {
                throw new ArgumentNullException( nameof( values ) );
            }

            int result = -1;
            if ( !values.Any() )
            {
                return result;
            }

            int first = 0;
            int last = values.Count() - 1;
            int middle;

            // assume searches failed
            result = first - 1;
            do
            {
                middle = (first + last) / 2;
                if ( values.ElementAtOrDefault( middle ) <= searchValue && values.ElementAtOrDefault( middle + 1 ) >= searchValue )
                {
                    result = middle;
                    break;
                }
                else if ( values.ElementAtOrDefault( middle ) < searchValue )
                {
                    first = middle;
                }
                else
                {
                    last = middle;
                }
            }
            while ( first < last );
            return result;
        }

        /// <summary> Determines if the two specified arrays have the same values. </summary>
        /// <remarks>
        /// <see cref="T:Array"/> or <see cref="T:Arraylist"/> equals methods cannot be used because it
        /// expects the two entities to be the same for equality.
        /// </remarks>
        /// <param name="left">  The left value. </param>
        /// <param name="right"> The right value. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool ValueEquals<T>( this IEnumerable<T> left, IEnumerable<T> right )
        {
            bool result = true;
            if ( left is null )
            {
                result = right is null;
            }
            else if ( right is null )
            {
                result = false;
            }
            else
            {
                for ( int i = 0, loopTo = left.Count() - 1; i <= loopTo; i++ )
                {
                    if ( !left.ElementAtOrDefault( i ).Equals( right.ElementAtOrDefault( i ) ) )
                    {
                        result = false;
                        break;
                    }
                }
            }

            return result;
        }
    }
}
