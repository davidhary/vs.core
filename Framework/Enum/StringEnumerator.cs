using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;

namespace isr.Core
{

    /// <summary>
    /// Utility class can be used to treat <c>string</c> arrays as <c>Enum</c>'s.  Use this class to
    /// parse a string to an Enum or serialize an Enum to a string.  Unlike direct use of an Enum,
    /// this class allows the Enum values to be decorated with <c>Description</c> attributes which
    /// can be used when serializing the Enum. Modified to user with SCPI enumerations.
    /// </summary>
    /// <remarks>
    /// (c) 2007 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2007-02-03, 1.0.2590. Converted to Visual Basic .NET. By Rudy RIHANI, Code Net.  </para><para>
    /// http://www.codeproject.com/Articles/17472/StringEnumerator
    /// </para>
    /// </remarks>
    public class StringEnumerator<T> where T : struct
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="isr.Core.StringEnumerator{T}"/> class. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        public StringEnumerator() : base()
        {
        }

        #endregion

        /// <summary>
        /// Parses the specified value string to Enum type.  If the string can not be parsed then and
        /// exception is thrown.  This method will first attempt to parse the string by matching it
        /// directly to the string representation of the Enum value.  If a match is not found, then it
        /// will attempt to match the Enum against each DescriptionAttribute applied to that Enum (if
        /// any).
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="InvalidCastException"> . </exception>
        /// <param name="value"> The Enum string. </param>
        /// <returns> An Enum value of type T. </returns>
        public T Parse( string value )
        {
            var enumType = typeof( T );
            foreach ( FieldInfo fi in enumType.GetFields() )
            {
                if ( string.Compare( fi.Name, value, true, System.Globalization.CultureInfo.CurrentCulture ) == 0 )
                {
                    return ( T ) fi.GetValue( null );
                }
                else
                {
                    var fieldAttributes = fi.GetCustomAttributes( typeof( DescriptionAttribute ), false );
                    foreach ( DescriptionAttribute attr in fieldAttributes )
                    {
                        if ( string.Compare( attr.Description, value, true, System.Globalization.CultureInfo.CurrentCulture ) == 0 )
                        {
                            return ( T ) fi.GetValue( null );
                        }
                    }
                }
            }

            throw new InvalidCastException( $"Can't convert {value} to {enumType}" );
        }

        /// <summary>
        /// Parses the specified value string to Enum type.  If the string can not be parsed then and
        /// exception is thrown.  This method will first attempt to parse the string by matching it
        /// directly to the string representation of the Enum value.  If a match is not found, then it
        /// will attempt to match the Enum against each DescriptionAttribute applied to that Enum (if
        /// any).
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="InvalidCastException"> . </exception>
        /// <param name="value"> The Enum string. </param>
        /// <returns> An Enum value of type T. </returns>
        public T ParseContained( string value )
        {
            var enumType = typeof( T );
            foreach ( FieldInfo fi in enumType.GetFields() )
            {
                if ( string.Compare( fi.Name, value, true, System.Globalization.CultureInfo.CurrentCulture ) == 0 )
                {
                    return ( T ) fi.GetValue( null );
                }
                else
                {
                    var fieldAttributes = fi.GetCustomAttributes( typeof( DescriptionAttribute ), false );
                    foreach ( DescriptionAttribute attr in fieldAttributes )
                    {
                        if ( attr.Description.Contains( value ) )
                        {
                            return ( T ) fi.GetValue( null );
                        }
                    }
                }
            }

            throw new InvalidCastException( $"Can't convert {value} to {enumType}" );
        }

        /// <summary>
        /// Parses the specified value string to Enum type.  If the string can not be parsed then
        /// <c>False</c> is returned. This method will first attempt to parse the string by matching it
        /// directly to the string representation of the Enum value.  If a match is not found, then it
        /// will attempt to match the Enum against each DescriptionAttribute applied to that Enum (if
        /// any).
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value">      The value to parse. </param>
        /// <param name="enumerator"> [in,out] The enumerator type. </param>
        /// <returns> <c>True</c> if the string value was successfully parsed. </returns>
        public bool TryParse( string value, ref T enumerator )
        {
            if ( Enum.TryParse( value, out enumerator ) )
            {
                return true;
            }
            else
            {
                var enumType = typeof( T );
                foreach ( FieldInfo fi in enumType.GetFields() )
                {
                    if ( string.Compare( fi.Name, value, true, System.Globalization.CultureInfo.CurrentCulture ) == 0 )
                    {
                        enumerator = ( T ) fi.GetValue( null );
                        return true;
                    }
                    else
                    {
                        var fieldAttributes = fi.GetCustomAttributes( typeof( DescriptionAttribute ), false );
                        foreach ( DescriptionAttribute attr in fieldAttributes )
                        {
                            if ( string.Compare( attr.Description, value, true, System.Globalization.CultureInfo.CurrentCulture ) == 0 )
                            {
                                enumerator = ( T ) fi.GetValue( null );
                                return true;
                            }
                        }
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Uses the enumeration DescriptionAttribute to generate the string translation of the Enum
        /// value.  If no such attribute has been applied then the method will simply call T.ToString()
        /// on the enum.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="enumValue"> The Enum value. </param>
        /// <returns> The string translation of the Enum value. </returns>
        public string ToString( T enumValue )
        {
            var enumType = typeof( T );
            var fi = enumType.GetField( enumValue.ToString() );

            // Get the Description attribute that has been applied to this enum
            var fieldAttributes = fi.GetCustomAttributes( typeof( DescriptionAttribute ), false );
            if ( fieldAttributes.Length > 0 )
            {
                if ( fieldAttributes[0] is DescriptionAttribute descAttr )
                {
                    return descAttr.Description;
                }
            }
            // Enum does not have Description attribute so we return default string representation.
            return enumValue.ToString();
        }

        /// <summary>
        /// Returns an array of strings that represent the values of the enumerator.  If any of the Enum
        /// values have a <see cref="T:System.ComponentModel.DescriptionAttribute">Description
        /// Attribute</see>
        /// applied to them, then the value of the description is used in the List element.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <returns> Array of strings that represent the values of the enumerator. </returns>
        public IEnumerable<string> ToArray()
        {
            return ToList();
        }

        /// <summary>
        /// Returns a collection of strings that represent the values of the enumerator.  If any of the
        /// Enum values have <see cref="T:System.ComponentModel.DescriptionAttribute">Description
        /// Attribute</see>
        /// applied to them, then the value of the description is used in the List element.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <returns> A collection that represent the values of the enumerator. </returns>
        public System.Collections.ObjectModel.ReadOnlyCollection<string> ToReadOnlyCollection()
        {
            return new System.Collections.ObjectModel.ReadOnlyCollection<string>( ToList() );
        }

        /// <summary>
        /// Returns a list of strings that represent the values of the enumerator.  If any of the Enum
        /// values have <see cref="T:System.ComponentModel.DescriptionAttribute">Description
        /// Attribute</see>
        /// applied to them, then the value of the description is used in the List element.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <returns> List of strings that represent the values of the enumerator. </returns>
        private static IList<string> ToList()
        {
            var enumValues = new List<string>();
            var enumType = typeof( T );
            foreach ( FieldInfo fi in enumType.GetFields() )
            {
                if ( fi.IsSpecialName == false )
                {
                    // Get the Description attribute that has been applied to this enum
                    var fieldAttributes = fi.GetCustomAttributes( typeof( DescriptionAttribute ), false );
                    if ( fieldAttributes.Length > 0 )
                    {
                        if ( fieldAttributes[0] is DescriptionAttribute descAttr )
                        {
                            enumValues.Add( descAttr.Description );
                        }
                    }
                    else
                    {
                        // Enum does not have Description attribute so we return default string representation.
                        T enumValue = ( T ) fi.GetValue( null );
                        enumValues.Add( enumValue.ToString() );
                    }
                }
            }

            return enumValues;
        }

        /// <summary> Returns a dictionary that maps enumeration values to their descriptions. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <returns> A dictionary that maps enumeration values to their descriptions. </returns>
        public IDictionary<T, string> ToDictionary()
        {
            IDictionary<T, string> enumDictionary = new Dictionary<T, string>();
            var enumType = typeof( T );
            foreach ( FieldInfo fi in enumType.GetFields() )
            {
                if ( fi.IsSpecialName == false )
                {
                    T enumValue = ( T ) fi.GetValue( null );

                    // Get the Description attribute that has been applied to this enum
                    var fieldAttributes = fi.GetCustomAttributes( typeof( DescriptionAttribute ), false );
                    if ( fieldAttributes.Length > 0 )
                    {
                        if ( fieldAttributes[0] is DescriptionAttribute descAttr )
                        {
                            enumDictionary.Add( enumValue, descAttr.Description );
                        }
                    }
                    else
                    {
                        // Enum does not have Description attribute so we return default string representation.
                        enumDictionary.Add( enumValue, enumValue.ToString() );
                    }
                }
            }

            return enumDictionary;
        }

        /// <summary>
        /// Returns a list that contains all of the Enum types.  This list can be enumerated against to
        /// get each Enum value.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <returns> A list that contains all of the Enum types. </returns>
        public IList<T> GetKeys()
        {
            var keys = new List<T>();
            var enumType = typeof( T );
            foreach ( FieldInfo fi in enumType.GetFields() )
            {
                if ( fi.IsSpecialName == false )
                {
                    T enumValue = ( T ) fi.GetValue( null );
                    keys.Add( enumValue );
                }
            }

            return keys;
        }
    }
}
