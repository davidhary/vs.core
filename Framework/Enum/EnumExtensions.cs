using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.EnumExtensions
{
    /// <summary> Includes extensions for enumerations and enumerated types. </summary>
    /// <remarks> (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2009-04-09, 1.1.3386 </para><para>
    /// David, 2009-07-07, 1.2.3475.x"> <para>
    /// http://www.CodeProject.com/Articles/37921/Enums-Flags-and-CSharp-Oh-my-bad-pun.aspx </para><para>
    /// Under The Creative Commons Attribution-ShareAlike 2.5 License.</para>
    /// </para></remarks>
    public static class EnumExtensionsMethods
    {

        #region " PRIVATE FUNCTIONS "

        /// <summary> Converts a value to a long. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The <see cref="T:System.Enum"/> value. </param>
        /// <returns> Value as a Long. </returns>
        private static long ToLong( Enum value )
        {
            return Convert.ToInt64( value, System.Globalization.CultureInfo.CurrentCulture );
        }

        /// <summary> Converts a value to a long. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="value"> The <see cref="T:System.Enum"/> value. </param>
        /// <returns> Value as a Long. </returns>
        public static long ToLong( this object value )
        {
            if ( value is null )
            {
                throw new ArgumentNullException( nameof( value ) );
            }

            try
            {
                return Convert.ToInt64( value, System.Globalization.CultureInfo.CurrentCulture );
            }
            catch ( InvalidCastException ex )
            {
                throw new ArgumentException( $"Cannot convert {value} to integer", nameof( value ), ex );
            }
        }

        #endregion

        #region " VALUES: FILTER "

        /// <summary> Returns a filtered list of this collection. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values">        The values. </param>
        /// <param name="inclusionMask"> Specifies a mask for selecting the items to include in the list. </param>
        /// <param name="exclusionMask"> Specifies a mask for selecting the items to Exclude in the list. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process filter in this collection.
        /// </returns>
        public static IEnumerable<T> Filter<T>( this IEnumerable<T> values, T inclusionMask, T exclusionMask )
        {
            return values is null
                ? throw new ArgumentNullException( nameof( values ) )
                : values.Where( x => 0L != ((( object ) x).ToLong() & (( object ) inclusionMask).ToLong() & ~(( object ) exclusionMask).ToLong()) );
        }

        /// <summary> Returns a filtered list of this collection. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values">        The values. </param>
        /// <param name="inclusionMask"> Specifies a mask for selecting the items to include in the list. </param>
        /// <param name="exclusionMask"> Specifies a mask for selecting the items to Exclude in the list. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process filter in this collection.
        /// </returns>
        public static IEnumerable<Enum> Filter( this IEnumerable<Enum> values, Enum inclusionMask, Enum exclusionMask )
        {
            return values is null
                ? throw new ArgumentNullException( nameof( values ) )
                : values.Where( x => 0L != (ToLong( x ) & ToLong( inclusionMask ) & ~ToLong( exclusionMask )) );
        }

        #endregion

        #region " VALUES: INCLUDE FILTER "

        /// <summary>
        /// Filters the this collection to include only item keys matching the inclusion mask.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values">        The values. </param>
        /// <param name="inclusionMask"> Specifies a mask for selecting the items to include in the list. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process include filter in this collection.
        /// </returns>
        public static IEnumerable<long> IncludeFilter( this IEnumerable<long> values, long inclusionMask )
        {
            return values is null ? throw new ArgumentNullException( nameof( values ) ) : values.Where( x => 0L != (x & inclusionMask) );
        }

        /// <summary>
        /// Filters the this collection to include only item keys matching the inclusion mask.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values">        The values. </param>
        /// <param name="inclusionMask"> Specifies a mask for selecting the items to include in the list. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process include filter in this collection.
        /// </returns>
        public static IEnumerable<Enum> IncludeFilter( this IEnumerable<Enum> values, long inclusionMask )
        {
            return values is null ? throw new ArgumentNullException( nameof( values ) ) : values.Where( x => 0L != (ToLong( x ) & inclusionMask) );
        }

        /// <summary>
        /// Filters the this collection to include only item keys matching the inclusion mask.
        /// </summary>
        /// <remarks>   David, 2020-09-18. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="values">           The values. </param>
        /// <param name="includedValues">   The included values. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process include filter in this collection.
        /// </returns>
        public static IEnumerable<Enum> IncludeFilter( this IEnumerable<Enum> values, Enum[] includedValues )
        {
            return values is null
                ? throw new ArgumentNullException( nameof( values ) )
                : includedValues is null
                ? throw new ArgumentNullException( nameof( includedValues ) )
                : values.Where( x => includedValues.Contains( x ) ).ToList();
        }

        /// <summary>
        /// Filters the this collection to include only item keys matching the inclusion mask.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values">         The values. </param>
        /// <param name="includedValues"> The included values. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process include filter in this collection.
        /// </returns>
        public static IEnumerable<Enum> IncludeFilter( this IEnumerable<Enum> values, IEnumerable<Enum> includedValues )
        {
            return values is null
                ? throw new ArgumentNullException( nameof( values ) )
                : includedValues is null
                ? throw new ArgumentNullException( nameof( includedValues ) )
                : values.Where( x => includedValues.Contains( x ) ).ToList();
        }

        #endregion

        #region " VALUES: EXCLUDE FILTER "

        /// <summary>
        /// Filters the this collection to Exclude item keys matching the exclusion mask.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values">        The values. </param>
        /// <param name="exclusionMask"> Specifies a mask for selecting the items to Exclude in the list. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process Exclude filter in this collection.
        /// </returns>
        public static IEnumerable<long> ExcludeFilter( this IEnumerable<long> values, long exclusionMask )
        {
            return values is null ? throw new ArgumentNullException( nameof( values ) ) : values.Where( x => 0L == (x & exclusionMask) ).ToList();
        }

        /// <summary>
        /// Filters the this collection to Exclude only item keys matching the exclusion mask.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values">        The values. </param>
        /// <param name="exclusionMask"> Specifies a mask for selecting the items to Exclude in the list. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process Exclude filter in this collection.
        /// </returns>
        public static IEnumerable<Enum> ExcludeFilter( this IEnumerable<Enum> values, long exclusionMask )
        {
            return values is null
                ? throw new ArgumentNullException( nameof( values ) )
                : values.Where( x => 0L == (ToLong( x ) & exclusionMask) ).ToList();
        }

        /// <summary>
        /// Filters the this collection to Exclude only item keys matching the exclusion mask.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values">         The values. </param>
        /// <param name="excludedValues"> The Excluded values. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process Exclude filter in this collection.
        /// </returns>
        public static IEnumerable<Enum> ExcludeFilter( this IEnumerable<Enum> values, IEnumerable<Enum> excludedValues )
        {
            return values is null
                ? throw new ArgumentNullException( nameof( values ) )
                : excludedValues is null
                ? throw new ArgumentNullException( nameof( excludedValues ) )
                : values.Where( x => !excludedValues.Contains( x ) ).ToList();
        }

        #endregion

        #region " TO BINDING LIST "

        /// <summary>
        /// Converts enumerated
        /// <see cref="KeyValuePair{TKey, TValue}">key value
        /// pairs</see> to a binding list.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="pairs"> key value pairs. </param>
        /// <returns>
        /// A list of <see cref="T:System.Enum">enumeration</see> and
        /// <see cref="T:System.String">string</see>.
        /// </returns>
        public static BindingList<KeyValuePair<Enum, string>> ToBindingList( this IEnumerable<KeyValuePair<Enum, string>> pairs )
        {
            if ( pairs is null )
            {
                throw new ArgumentNullException( nameof( pairs ) );
            }

            var result = new BindingList<KeyValuePair<Enum, string>>();
            foreach ( KeyValuePair<Enum, string> pair in pairs )
            {
                result.Add( pair );
            }

            return result;
        }

        #endregion

        #region " VALUES: ENUM "

        /// <summary> Enumerates enum values in this collection. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="enumItem"> An enum item representing the enumeration option. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process enum values in this collection.
        /// </returns>
        public static IEnumerable<Enum> EnumValues( this Enum enumItem )
        {
            return enumItem is null ? throw new ArgumentNullException( nameof( enumItem ) ) : enumItem.GetType().EnumValues();
        }

        /// <summary> Enumerates enum values in this collection. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="enumType"> The <see cref="T:System.Enum"/> type. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process enum values in this collection.
        /// </returns>
        public static IEnumerable<Enum> EnumValues( this Type enumType )
        {
            return Enum.GetValues( enumType ).Cast<Enum>();
        }

        /// <summary> Enumerates enum values in this collection. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="enumType"> The <see cref="T:System.Enum"/> type. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process enum values in this collection.
        /// </returns>
        public static IEnumerable<T> EnumValues<T>( this Type enumType )
        {
            return Enum.GetValues( enumType ).Cast<T>();
        }

        /// <summary> Enumerates enum values in this collection. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <returns>
        /// An enumerator that allows for each to be used to process enum values in this collection.
        /// </returns>
        public static IEnumerable<T> EnumValues<T>()
        {
            return Enum.GetValues( typeof( T ) ).Cast<T>();
        }

        #endregion

        #region " VALUES: LONG "

        /// <summary> Enumerates values for the Enum type of the supplied Enum constant. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="enumItem"> An enum item representing the enumeration option. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process values in this collection.
        /// </returns>
        public static IEnumerable<long> Values( this Enum enumItem )
        {
            return enumItem is null ? throw new ArgumentNullException( nameof( enumItem ) ) : enumItem.GetType().Values();
        }

        /// <summary> Enumerates values in this collection. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="enumType"> The <see cref="T:System.Enum"/> type. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process values in this collection.
        /// </returns>
        public static IEnumerable<long> Values( this Type enumType )
        {
            return Enum.GetValues( enumType ).Cast<Enum>().Select( x => Convert.ToInt64( x, System.Globalization.CultureInfo.CurrentCulture ) );
        }

        #endregion

        #region " VALUES : INTEGER "

        /// <summary> Enumerates values for the Enum type of the supplied Enum constant. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="enumItem"> An enum item representing the enumeration option. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process values in this collection.
        /// </returns>
        public static IEnumerable<int> IntegerValues( this Enum enumItem )
        {
            return enumItem is null ? throw new ArgumentNullException( nameof( enumItem ) ) : enumItem.GetType().IntegerValues();
        }

        /// <summary> Enumerates values in this collection. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="enumType"> The <see cref="T:System.Enum"/> type. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process values in this collection.
        /// </returns>
        public static IEnumerable<int> IntegerValues( this Type enumType )
        {
            return Enum.GetValues( enumType ).Cast<Enum>().Select( x => Convert.ToInt32( x, System.Globalization.CultureInfo.CurrentCulture ) );
        }

        #endregion

        #region " TO VALUES "

        /// <summary>
        /// Converts enumerated
        /// <see cref="System.Collections.Generic.KeyValuePair{TKey, TValue}">key value
        /// pairs</see> to a <see cref="IEnumerable{T}">list</see>
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="pairs"> key value pairs. </param>
        /// <returns> A list of string values. </returns>
        public static IEnumerable<string> ToValues( this IEnumerable<KeyValuePair<Enum, string>> pairs )
        {
            return pairs is null ? throw new ArgumentNullException( nameof( pairs ) ) : pairs.Select( x => x.Value );
        }

        /// <summary> Enumerates to keys in this collection. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="pairs"> key value pairs. </param>
        /// <returns> Pairs as an IEnumerable(Of System.Enum) </returns>
        public static IEnumerable<Enum> ToKeys( this IEnumerable<KeyValuePair<Enum, string>> pairs )
        {
            return pairs is null ? throw new ArgumentNullException( nameof( pairs ) ) : pairs.Select( x => x.Key );
        }

        /// <summary> Query if 'pairs' contains key. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="pairs"> The pairs. </param>
        /// <param name="key">   An enum constant representing the key option. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool ContainsKey( this IEnumerable<KeyValuePair<Enum, string>> pairs, Enum key )
        {
            return pairs is null ? throw new ArgumentNullException( nameof( pairs ) ) : pairs.ToKeys().Contains( key );
        }

        /// <summary> Select pair. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="pairs">       The pairs. </param>
        /// <param name="defaultKey">  An enum constant representing the default key option. </param>
        /// <param name="description"> The description. </param>
        /// <returns> A KeyValuePair(Of System.Enum, String) </returns>
        public static KeyValuePair<Enum, string> SelectPair( this IEnumerable<KeyValuePair<Enum, string>> pairs, Enum defaultKey, string description )
        {
            if ( pairs is null )
            {
                throw new ArgumentNullException( nameof( pairs ) );
            }

            var def = new KeyValuePair<Enum, string>( defaultKey, defaultKey.Description() );
            var found = pairs.FirstOrDefault( x => string.Equals( x.Value, description ) );
            return string.IsNullOrWhiteSpace( found.Value ) ? def : found;
        }

        /// <summary> Select description. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="enumType">     The <see cref="T:System.Enum"/>
        /// type. </param>
        /// <param name="defaultValue"> An enum constant representing the default value option. </param>
        /// <param name="description">  The description. </param>
        /// <returns> A System.Enum. </returns>
        public static Enum SelectDescription( this Type enumType, Enum defaultValue, string description )
        {
            return enumType.ValueDescriptionPairs().SelectPair( defaultValue, description ).Key;
        }

        #endregion

        #region " VALUE DESCRIPTION PAIRS "

        /// <summary> Gets a Key Value Pair description item. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="enumItem"> The <see cref="T:System.Enum">enumeration</see> </param>
        /// <returns> A list of. </returns>
        public static KeyValuePair<Enum, string> ValueDescriptionPair( this Enum enumItem )
        {
            return enumItem is null
                ? throw new ArgumentNullException( nameof( enumItem ) )
                : new KeyValuePair<Enum, string>( enumItem, enumItem.Description() );
        }

        /// <summary> Gets a Key Value Pair description item. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="enumItem"> The <see cref="T:System.Enum">enumeration</see> </param>
        /// <returns> A list of. </returns>
        public static KeyValuePair<T, string> ValueDescriptionPair<T>( this T enumItem )
        {
            return enumItem is null
                ? throw new ArgumentNullException( nameof( enumItem ) )
                : new KeyValuePair<T, string>( enumItem, enumItem.Description() );
        }

        /// <summary> Enumerates the value description pairs of the <see cref="T:System.Enum"/> </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values"> The values. </param>
        /// <returns>
        /// An <see cref="IEnumerable{KeyValuePair}"/> containing the enumerated type value (key) and description
        /// (value) pairs.
        /// </returns>
        public static IEnumerable<KeyValuePair<Enum, string>> ValueDescriptionPairs( this IEnumerable<Enum> values )
        {
            return values is null ? throw new ArgumentNullException( nameof( values ) ) : values.Select( x => x.ValueDescriptionPair() );
        }

        /// <summary> Enumerates the value description pairs of the <see cref="T:System.Enum"/> </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values"> The values. </param>
        /// <returns>
        /// An <see cref="IEnumerable{KeyValuePair}"/> containing the enumerated type value (key) and description
        /// (value) pairs.
        /// </returns>
        public static IEnumerable<KeyValuePair<T, string>> ValueDescriptionPairs<T>( this IEnumerable<T> values )
        {
            return values is null ? throw new ArgumentNullException( nameof( values ) ) : values.Select( x => x.ValueDescriptionPair() );
        }

        /// <summary> Enumerates the value description pairs of the <see cref="T:System.Enum"/> </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="enumItem"> An enum item representing the enumeration option. </param>
        /// <returns>
        /// An <see cref="IEnumerable{KeyValuePair}"/> containing the enumerated type value (key) and description
        /// (value) pairs.
        /// </returns>
        public static IEnumerable<KeyValuePair<Enum, string>> ValueDescriptionPairs( this Enum enumItem )
        {
            return enumItem is null ? throw new ArgumentNullException( nameof( enumItem ) ) : enumItem.EnumValues().ValueDescriptionPairs();
        }

        /// <summary> Enumerates the value description pairs of the <see cref="T:System.Enum"/> </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="enumType"> The <see cref="T:System.Enum"/> type. </param>
        /// <returns>
        /// An <see cref="IEnumerable{KeyValuePair}"/> containing the enumerated type value (key) and description
        /// (value) pairs.
        /// </returns>
        /// <example>
        /// Create an enumeration with descriptions:
        /// <code>
        /// Public Enum SimpleEnum2
        /// [System.ComponentModel.Description("Today")] Today
        /// [System.ComponentModel.Description("Last 7 days")] Last7
        /// [System.ComponentModel.Description("Last 14 days")] Last14
        /// [System.ComponentModel.Description("Last 30 days")] Last30
        /// [System.ComponentModel.Description("All")] All
        /// End Enum
        /// Dim combo As ComboBox = New ComboBox()
        /// combo.DataSource = GetType(SimpleEnum2).ValueDescriptionPairs().ToList
        /// combo.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
        /// combo.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
        /// </code>
        /// </example>
        public static IEnumerable<KeyValuePair<Enum, string>> ValueDescriptionPairs( this Type enumType )
        {
            return enumType.EnumValues().ValueDescriptionPairs();
        }

        #endregion

        #region " DESCRIPTIONS "

        /// <summary>
        /// Gets the <see cref="DescriptionAttribute"/> of an <see cref="T:System.Enum"/> type value.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="value"> The <see cref="T:System.Enum"/> value. </param>
        /// <returns> A string containing the text of the <see cref="DescriptionAttribute"/>. </returns>
        public static string Description( this Enum value )
        {
            if ( value is null )
            {
                throw new ArgumentNullException( nameof( value ) );
            }

            string candidate = value.Name();
            var fieldInfo = value.GetType().GetField( candidate );
            DescriptionAttribute[] attributes = ( DescriptionAttribute[] ) fieldInfo.GetCustomAttributes( typeof( DescriptionAttribute ), false );
            if ( attributes is object && attributes.Length > 0 )
            {
                candidate = attributes[0].Description;
            }

            return candidate;
        }

        /// <summary>
        /// Gets the <see cref="DescriptionAttribute"/> of an <see cref="T:System.Enum"/> type value.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="value"> The <see cref="T:System.Enum"/> value. </param>
        /// <returns> A string containing the text of the <see cref="DescriptionAttribute"/>. </returns>
        public static string Description<T>( this T value )
        {
            if ( value is null )
            {
                throw new ArgumentNullException( nameof( value ) );
            }

            string candidate = value.Name();
            var fieldInfo = value.GetType().GetField( candidate );
            DescriptionAttribute[] attributes = ( DescriptionAttribute[] ) fieldInfo.GetCustomAttributes( typeof( DescriptionAttribute ), false );
            if ( attributes is object && attributes.Length > 0 )
            {
                candidate = attributes[0].Description;
            }

            return candidate;
        }

        /// <summary>
        /// An extension method that returns the description or descriptions (in case of Flags Enum) of
        /// an Enum value.
        /// </summary>
        /// <remarks>   David, 2020-10-28. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="value">    The <see cref="T:System.Enum"/> value. </param>
        /// <returns>   Value as a string. </returns>
        public static string Descriptions<T>( this T value )
        {
            if ( value is null )
            {
                throw new ArgumentNullException( nameof( value ) );
            }

            string[] enumNames = value.ToString().Split( ',' );
            System.Text.StringBuilder builder = new();
            string description;
            foreach ( string enumName in enumNames )
            {
                // use the enum name in lieu if no description attribute exists.
                description = enumName;
                var fieldInfo = value.GetType().GetField( enumName.Trim() );
                DescriptionAttribute[] attributes = ( DescriptionAttribute[] ) fieldInfo.GetCustomAttributes( typeof( DescriptionAttribute ), false );
                if ( attributes is object && attributes.Length > 0 )
                {
                    description = attributes[0].Description;
                }
                if ( builder.Length > 0 ) { _ = builder.Append( ", " ); }
                _ = builder.Append( description );
            }

            return builder.ToString();
        }

        /// <summary> Returns the descriptions of an enumeration. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values"> The values. </param>
        /// <returns> A list of strings. </returns>
        public static IEnumerable<string> Descriptions( this IEnumerable<Enum> values )
        {
            return values is null ? throw new ArgumentNullException( nameof( values ) ) : values.Select( x => x.Description() );
        }

        /// <summary> Returns the descriptions of an enumeration. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="enumItem"> An enum constant representing the enum type option. </param>
        /// <returns> A list of strings. </returns>
        public static IEnumerable<string> Descriptions( this Enum enumItem )
        {
            return enumItem is null ? throw new ArgumentNullException( nameof( enumItem ) ) : enumItem.GetType().EnumValues().Descriptions();
        }

        /// <summary> Returns the descriptions of an enumeration. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="enumType"> The <see cref="T:System.Enum"/> type. </param>
        /// <returns> A list of strings. </returns>
        public static IEnumerable<string> Descriptions( this Type enumType )
        {
            return enumType.EnumValues().Descriptions();
        }

        /// <summary> Query if 'type' has description. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="enumType">    The <see cref="T:System.Enum"/>
        /// type. </param>
        /// <param name="description"> The description. </param>
        /// <returns> <c>true</c> if description; otherwise <c>false</c> </returns>
        public static bool HasDescription( this Type enumType, string description )
        {
            return enumType.Descriptions().Contains( description, StringComparer.OrdinalIgnoreCase );
        }

        #endregion

        #region " STRING VALUE "

        /// <summary>   An Enum extension method that gets the name or names (if Flags type) using (ToString()) of the given value. </summary>
        /// <remarks>   David, 2020-10-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="value">    The <see cref="T:System.Enum"/> value. </param>
        /// <returns>   A string. </returns>
        public static string Names( this Enum value )
        {
            return value is null ? throw new ArgumentNullException( nameof( value ) ) : value.ToString();
        }

        /// <summary>   An Enum extension method that gets the name or names (if Flags type) using (ToString()) of the given value. </summary>
        /// <remarks>   David, 2020-10-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="value">    The <see cref="T:System.Enum"/> value. </param>
        /// <returns>   A string. </returns>
        public static string Names<T>( this T value )
        {
            return value is null ? throw new ArgumentNullException( nameof( value ) ) : value.ToString();
        }

        #endregion

        #region " NAME "

        /// <summary>   An Enum extension method that gets the name of the given value. </summary>
        /// <remarks>   David, 2020-10-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="value">    The <see cref="T:System.Enum"/> value. </param>
        /// <returns>   A string. </returns>
        public static string Name( this Enum value )
        {
            return value is null ? throw new ArgumentNullException( nameof( value ) ) : Enum.GetName( value.GetType(), value );
        }

        /// <summary>   An Enum extension method that gets the name the given value. </summary>
        /// <remarks>   David, 2020-10-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="value">    The <see cref="T:System.Enum"/> value. </param>
        /// <returns>   A string. </returns>
        public static string Name<T>( this T value )
        {
            return value is null ? throw new ArgumentNullException( nameof( value ) ) : Enum.GetName( value.GetType(), value );
        }

        #endregion

        #region " NAMES "


        /// <summary> Gets a Key Value Pair Name item. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="value"> The <see cref="T:System.Enum">enumeration</see> item. </param>
        /// <returns> a value name pair. </returns>
        public static KeyValuePair<Enum, string> ValueNamePair( this Enum value )
        {
            return value is null ? throw new ArgumentNullException( nameof( value ) ) : new KeyValuePair<Enum, string>( value, value.ToString() );
        }

        /// <summary> Gets a Key Value Pair Name item. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="value"> The <see cref="T:System.Enum">enumeration</see> item. </param>
        /// <returns> a value name pair. </returns>
        public static KeyValuePair<T, string> ValueNamePair<T>( this T value )
        {
            return value is null ? throw new ArgumentNullException( nameof( value ) ) : new KeyValuePair<T, string>( value, value.ToString() );
        }

        /// <summary> Enumerates the value name pairs of the <see cref="T:System.Enum"/> </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values"> The values. </param>
        /// <returns>
        /// An <see cref="IEnumerable{KeyValuePair}"/> containing the enumerated type value (key) and Name (value)
        /// pairs.
        /// </returns>
        public static IEnumerable<KeyValuePair<Enum, string>> ValueNamePairs( this IEnumerable<Enum> values )
        {
            return values is null ? throw new ArgumentNullException( nameof( values ) ) : values.Select( x => x.ValueNamePair() );
        }

        /// <summary> Enumerates the value name pairs of the <see cref="T:System.Enum"/> </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values"> The values. </param>
        /// <returns>
        /// An <see cref="IEnumerable{KeyValuePair}"/> containing the enumerated type value (key) and Name (value)
        /// pairs.
        /// </returns>
        public static IEnumerable<KeyValuePair<T, string>> ValueNamePairs<T>( this IEnumerable<T> values )
        {
            return values is null ? throw new ArgumentNullException( nameof( values ) ) : values.Select( x => x.ValueNamePair() );
        }

        /// <summary>
        /// Converts the <see cref="T:System.Enum"/> type to an <see cref="IEnumerable{KeyValuePair}"/> compatible
        /// object.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="enumType"> The <see cref="T:System.Enum"/> type. </param>
        /// <returns>
        /// An <see cref="IEnumerable{KeyValuePair}"/> containing the enumerated type value (key) and Name (value)
        /// pairs.
        /// </returns>
        /// <example>
        /// The first step is to add a Name attribute to your Enum.
        /// <code>
        /// Public Enum SimpleEnum2
        /// [System.ComponentModel.Name("Today")] Today
        /// [System.ComponentModel.Name("Last 7 days")] Last7
        /// [System.ComponentModel.Name("Last 14 days")] Last14
        /// [System.ComponentModel.Name("Last 30 days")] Last30
        /// [System.ComponentModel.Name("All")] All
        /// End Enum
        /// Dim combo As ComboBox = New ComboBox()
        /// combo.DataSource = GetType(SimpleEnum2).ValueNamePairs().ToList
        /// combo.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
        /// combo.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
        /// </code>
        /// </example>
        public static IEnumerable<KeyValuePair<Enum, string>> ValueNamePairs( this Type enumType )
        {
            return enumType.EnumValues().ValueNamePairs();
        }

        #endregion

        #region " DELIMITED DESCRIPTION "

        /// <summary> The start delimiter. </summary>
        private const char _StartDelimiter = '(';

        /// <summary> The end delimiter. </summary>
        private const char _EndDelimiter = ')';

        /// <summary> The embedded value format. </summary>
        private const string _EmbeddedValueFormat = "({0})";

        /// <summary>
        /// Builds a delimited value. This helps parsing enumerated values that include delimited value
        /// strings in their descriptions.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> A String. </returns>
        public static string BuildDelimitedValue( this string value )
        {
            return string.Format( System.Globalization.CultureInfo.CurrentCulture, _EmbeddedValueFormat, value );
        }

        /// <summary> Returns the description up to the <paramref name="startDelimiter"/> </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value">          The value. </param>
        /// <param name="startDelimiter"> The start delimiter. </param>
        /// <returns> The description up to the <paramref name="startDelimiter"/> </returns>
        public static string DescriptionUntil( this string value, char startDelimiter )
        {
            if ( string.IsNullOrWhiteSpace( value ) )
            {
                return string.Empty;
            }
            else
            {
                int startingIndex = value.IndexOf( startDelimiter );
                return startingIndex > 0 ? value.Substring( 0, startingIndex ).TrimEnd() : value;
            }
        }

        /// <summary> Returns the description up to the <see cref="_StartDelimiter"/> </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> The description up to the <see cref="_StartDelimiter"/> </returns>
        public static string DescriptionUntil( this string value )
        {
            return value.DescriptionUntil( _StartDelimiter );
        }

        /// <summary> Returns the description up to the <paramref name="startDelimiter"/> </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value">          The <see cref="T:System.Enum"/> item. </param>
        /// <param name="startDelimiter"> The start delimiter. </param>
        /// <returns> The description up to the <paramref name="startDelimiter"/> </returns>
        public static string DescriptionUntil( this Enum value, char startDelimiter )
        {
            return value is null ? string.Empty : value.Description().DescriptionUntil( startDelimiter );
        }

        /// <summary> Returns the description up to the <see cref="_StartDelimiter"/> </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The <see cref="T:System.Enum">enumeration</see> </param>
        /// <returns> The description up to the <see cref="_StartDelimiter"/> </returns>
        public static string DescriptionUntil( this Enum value )
        {
            return value is null ? string.Empty : value.DescriptionUntil( _StartDelimiter );
        }

        /// <summary> Extracts a delimited value. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value">          The value. </param>
        /// <param name="startDelimiter"> The start delimiter. </param>
        /// <param name="endDelimiter">   The end delimiter. </param>
        /// <returns> The extracted between. </returns>
        public static string ExtractBetween( this string value, char startDelimiter, char endDelimiter )
        {
            if ( string.IsNullOrWhiteSpace( value ) )
            {
                return string.Empty;
            }
            else
            {
                int startingIndex = value.IndexOf( startDelimiter );
                int endingIndex = value.LastIndexOf( endDelimiter );
                return startingIndex > 0 && endingIndex > startingIndex ? value.Substring( startingIndex + 1, endingIndex - startingIndex - 1 ) : value;
            }
        }

        /// <summary> Extracts a delimited value from value. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> The extracted between. </returns>
        public static string ExtractBetween( this string value )
        {
            return value.ExtractBetween( _StartDelimiter, _EndDelimiter );
        }

        /// <summary>
        /// Extracts a delimited value from the description of the relevant enumerated value.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value">          The <see cref="T:System.Enum"/> item. </param>
        /// <param name="startDelimiter"> The start delimiter. </param>
        /// <param name="endDelimiter">   The end delimiter. </param>
        /// <returns> The extracted between. </returns>
        public static string ExtractBetween( this Enum value, char startDelimiter, char endDelimiter )
        {
            return value is null ? string.Empty : value.Description().ExtractBetween( startDelimiter, endDelimiter );
        }

        /// <summary>
        /// Extracts a delimited value from the description of the relevant enumerated value.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The <see cref="T:System.Enum">enumeration</see> </param>
        /// <returns> The extracted between. </returns>
        public static string ExtractBetween( this Enum value )
        {
            return value is null ? string.Empty : value.ExtractBetween( _StartDelimiter, _EndDelimiter );
        }

        #endregion

        #region " WEBDEV_HB. UNDER LICENSE FROM "

        /// <summary> Returns true if the specified bits are set. </summary>
        /// <remarks> David, 2010-06-05, 1.2.3807.x. Returns true if equals (IS). </remarks>
        /// <param name="enumItem"> The <see cref="T:System.Enum">enumeration</see> </param>
        /// <param name="value">    Specifies the bit values which to compare. </param>
        /// <returns> <c>true</c> if set; otherwise <c>false</c> </returns>
        public static bool IsSet<T>( this Enum enumItem, T value )
        {
            return enumItem.Has( value );
        }

        /// <summary> Returns true if the enumerated flag has the specified bits. </summary>
        /// <remarks>
        /// David, 2009-07-07, 1.2.3475. <para>
        /// http://www.CodeProject.com/Articles/37921/Enums-Flags-and-CSharp-Oh-my-bad-pun.aspx Under The
        /// Creative Commons Attribution-ShareAlike 2.5 License  </para><para>
        /// David, 2010-06-05, 1.2.3807. Returns true if equals (IS). </para>
        /// </remarks>
        /// <param name="enumItem"> The <see cref="T:System.Enum"/> item. </param>
        /// <param name="value">    Specifies the bit values which to compare. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public static bool Has<T>( this Enum enumItem, T value )
        {
            try
            {
                int oldValue = ConvertToInteger<T>( enumItem );
                int bitsValue = ConvertToInteger( value );
                return oldValue == bitsValue || (oldValue & bitsValue) == bitsValue;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>  Returns true if value is only the provided type. </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumItem">  The <see cref="T:System.Enum"/> item.</param>
        /// <param name="value"> Specifies the bit values which to compare.</param>
        /// <remarks> David, 2009-07-07, 1.2.3475.x.
        /// http://www.CodeProject.com/Articles/37921/Enums-Flags-and-CSharp-Oh-my-bad-pun.aspx
        /// Under The Creative Commons Attribution-ShareAlike 2.5 License. </remarks>
        public static bool Is<T>( this Enum enumItem, T value )
        {
            try
            {
                int oldValue = ConvertToInteger<T>( enumItem );
                int bitsValue = ConvertToInteger( value );
                return oldValue == bitsValue;
            }
            catch
            {
                return false;
            }
        }

        /// <summary> Set the specified bits. </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumItem">  The <see cref="T:System.Enum"/> item.</param>
        /// <param name="value">     Specifies the bit values which to add.</param>
        public static T Set<T>( this Enum enumItem, T value )
        {
            return enumItem.Add( value );
        }

        /// <summary> Appends a value. </summary>
        /// <remarks>
        /// David, 2009-07-07, 1.2.3475.x.
        /// http://www.CodeProject.com/Articles/37921/Enums-Flags-and-CSharp-Oh-my-bad-pun.aspx Under The
        /// Creative Commons Attribution-ShareAlike 2.5 License.
        /// </remarks>
        /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="enumItem"> The <see cref="T:System.Enum"/> item. </param>
        /// <param name="value">    Specifies the bit values which to add. </param>
        /// <returns> A T. </returns>
        public static T Add<T>( this Enum enumItem, T value )
        {
            try
            {
                int oldValue = ConvertToInteger<T>( enumItem );
                int bitsValue = ConvertToInteger( value );
                int newValue = oldValue | bitsValue;
                return ( T ) ( object ) newValue;
            }
            catch ( Exception ex )
            {
                throw new ArgumentException( $"Could not append value from enumerated type '{typeof( T ).Name}'.", ex );
            }
        }

        /// <summary> Clears the specified bits. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="enumConstant"> The <see cref="T:System.Enum"/> constant. </param>
        /// <param name="value">        Specifies the bit values which to remove. </param>
        /// <returns> A T. </returns>
        public static T Clear<T>( this Enum enumConstant, T value )
        {
            return enumConstant.Remove( value );
        }

        /// <summary> Completely removes the value. </summary>
        /// <remarks>
        /// David, 2009-07-07, 1.2.3475.x.
        /// http://www.CodeProject.com/Articles/37921/Enums-Flags-and-CSharp-Oh-my-bad-pun.aspx Under The
        /// Creative Commons Attribution-ShareAlike 2.5 License.
        /// </remarks>
        /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="enumItem"> The <see cref="T:System.Enum"/> item. </param>
        /// <param name="value">    Specifies the bit values which to remove. </param>
        /// <returns> A T. </returns>
        public static T Remove<T>( this Enum enumItem, T value )
        {
            try
            {
                int oldValue = ConvertToInteger<T>( enumItem );
                int bitsValue = ConvertToInteger( value );
                int newValue = oldValue & ~bitsValue;
                return ( T ) ( object ) newValue;
            }
            catch ( Exception ex )
            {
                throw new ArgumentException( $"Could not remove value from enumerated type '{typeof( T ).Name}'.", ex );
            }
        }

        /// <summary> Converts a value to an integer. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> Specifies the bit values which to compare. </param>
        /// <returns> The given data converted to an integer. </returns>
        private static int ConvertToInteger<T>( T value )
        {
            return Conversions.ToInteger( value );
        }

        /// <summary> Converts a type to an integer. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="enumConstant"> The <see cref="T:System.Enum"/> constant. </param>
        /// <returns> The given data converted to an integer. </returns>
        private static int ConvertToInteger<T>( Enum enumConstant )
        {
            return Conversions.ToInteger( enumConstant );
        }

        #endregion

        #region " PARSE "

        /// <summary> Converts a value to an enum. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> Value as a T. </returns>
        public static T ToEnum<T>( this string value )
        {
            return ( T ) Enum.Parse( typeof( T ), value );
        }

        /// <summary> Converts a value to an enum. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> Value as a T. </returns>
        public static T ToEnum<T>( this int value )
        {
            return ( T ) Enum.Parse( typeof( T ), value.ToString(), true );
        }

        /// <summary> Converts a value to a nullable enum based on the underlying enum type. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The <see cref="T:System.Enum"/> value. </param>
        /// <returns> Value as a Nullable(Of T) </returns>
        public static T? ToNullableEnum<T>( this long value ) where T : struct
        {
            switch ( Enum.GetUnderlyingType( typeof( T ) ) )
            {
                case var @case when @case == typeof( int ):
                case var case1 when case1 == typeof( int ):
                    {
                        return ToNullableEnumThis<T>( ( int ) value );
                    }

                case var case2 when case2 == typeof( short ):
                    {
                        return ToNullableEnumThis<T>( ( short ) value );
                    }

                case var case3 when case3 == typeof( long ):
                case var case4 when case4 == typeof( long ):
                    {
                        return ToNullableEnumThis<T>( value );
                    }

                case var case5 when case5 == typeof( byte ):
                    {
                        return ToNullableEnumThis<T>( ( byte ) value );
                    }

                default:
                    {
                        return ToNullableEnumThis<T>( ( int ) value );
                    }
            }
        }

        /// <summary> Converts a value to a nullable enum based on the underlying enum type. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The <see cref="T:System.Enum"/> value. </param>
        /// <returns> Value as a Nullable(Of T) </returns>
        public static T? ToNullableEnum<T>( this int value ) where T : struct
        {
            switch ( Enum.GetUnderlyingType( typeof( T ) ) )
            {
                case var @case when @case == typeof( int ):
                case var case1 when case1 == typeof( int ):
                    {
                        return ToNullableEnumThis<T>( value );
                    }

                case var case2 when case2 == typeof( short ):
                    {
                        return ToNullableEnumThis<T>( ( short ) value );
                    }

                case var case3 when case3 == typeof( long ):
                case var case4 when case4 == typeof( long ):
                    {
                        return ToNullableEnumThis<T>( ( long ) value );
                    }

                case var case5 when case5 == typeof( byte ):
                    {
                        return ToNullableEnumThis<T>( ( byte ) value );
                    }

                default:
                    {
                        return ToNullableEnumThis<T>( value );
                    }
            }
        }

        /// <summary> Converts long to enum assuming enum underlying type is long. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The <see cref="T:System.Enum"/> value. </param>
        /// <returns> The enum converted long. </returns>
        private static T? ToNullableEnumThis<T>( long value ) where T : struct
        {
            return Enum.IsDefined( typeof( T ), value ) ? ( T ) Enum.ToObject( typeof( T ), value ) : new T?();
        }

        /// <summary> Converts integer to enum assuming enum underlying type is integer. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The <see cref="T:System.Enum"/> value. </param>
        /// <returns> The enum converted integer. </returns>
        private static T? ToNullableEnumThis<T>( int value ) where T : struct
        {
            return Enum.IsDefined( typeof( T ), value ) ? ( T ) Enum.ToObject( typeof( T ), value ) : new T?();
        }

        /// <summary> Converts Short to enum assuming enum underlying type is Short. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The <see cref="T:System.Enum"/> value. </param>
        /// <returns> The enum converted Short. </returns>
        private static T? ToNullableEnumThis<T>( short value ) where T : struct
        {
            return Enum.IsDefined( typeof( T ), value ) ? ( T ) Enum.ToObject( typeof( T ), value ) : new T?();
        }

        /// <summary> Converts Byte to enum assuming enum underlying type is Byte. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The <see cref="T:System.Enum"/> value. </param>
        /// <returns> The enum converted Byte. </returns>
        private static T? ToNullableEnumThis<T>( byte value ) where T : struct
        {
            return Enum.IsDefined( typeof( T ), value ) ? ( T ) Enum.ToObject( typeof( T ), value ) : new T?();
        }

        #endregion

    }
}
