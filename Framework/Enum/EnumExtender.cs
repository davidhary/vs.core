using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core
{

    /// <summary>
    /// A class to encapsulate specific enumerations in cases where speed improvements are desired.
    /// Reported 100000 operations time reduced from 3.9 to .57 million ticks. With using a creatable
    /// class, improvement increased to 10 fold.
    /// </summary>
    /// <remarks>
    /// (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2015-03-25 </para><para>
    /// David, 2008-05-04 1.2.3411 from Stewart and StageSoft <para>
    /// From Strong-Type and Efficient .NET Enums By Hanna GIAT</para><para>
    /// http://www.CodeProject.com/KB/cs/efficient_strong_enum.aspx. </para> </para>
    /// </remarks>
    public class EnumExtender<T>
    {

        /// <summary>
        /// constructor for the class instantiates all elements. With built-in elements, subsequent
        /// operations are fairly speedy.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        public EnumExtender() : base()
        {
            this._Type = typeof( T );
            this.Values = Enum.GetValues( this._Type );
            this.Names = Enum.GetNames( this._Type );

            // Me._descriptions = isr.Core.EnumExtensions.Descriptions(T)
            var values = new List<string>();
            foreach ( Enum value in Enum.GetValues( this._Type ) )
            {
                values.Add( this.Description( value ) );
            }

            this.Descriptions = values.ToArray();
            this.NameValuePairs = new Dictionary<string, T>( this.Names.Count() );
            this.LowerCaseNameValuePairs = new Dictionary<string, T>( this.Names.Count() );
            this.ValueDescriptionPairs = new Dictionary<T, string>( this.Names.Count() );
            this.ValueEnumValuePairs = new Dictionary<int, T>( this.Names.Count() );
            this.ValueNamePairs = new Dictionary<T, string>( this.Names.Count() );
            int index = 0;
            foreach ( string name in this.Names )
            {
                T enumValue = ( T ) Enum.Parse( this._Type, name );
                this.NameValuePairs.Add( name, enumValue );
                this.ValueDescriptionPairs.Add( enumValue, this.Descriptions[index] );
                this.ValueNamePairs.Add( enumValue, name );
                this.LowerCaseNameValuePairs.Add( name.ToLower( System.Globalization.CultureInfo.CurrentCulture ), enumValue );
                this.ValueEnumValuePairs.Add( Convert.ToInt32( enumValue, System.Globalization.CultureInfo.CurrentCulture ), enumValue );
                index += 1;
            }
        }

        /// <summary>
        /// Gets the <see cref="DescriptionAttribute"/> of an <see cref="T:System.Enum"/> type value.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The <see cref="T:System.Enum"/> type value. </param>
        /// <returns> A string containing the text of the <see cref="DescriptionAttribute"/>. </returns>
        public string Description( Enum value )
        {
            if ( value is null )
            {
                return string.Empty;
            }

            string candidate = value.ToString();
            var fieldInfo = value.GetType().GetField( candidate );
            DescriptionAttribute[] attributes = ( DescriptionAttribute[] ) fieldInfo.GetCustomAttributes( typeof( DescriptionAttribute ), false );
            if ( attributes is object && attributes.Length > 0 )
            {
                candidate = attributes[0].Description;
            }

            return candidate;
        }

        /// <summary> Gets the descriptions. </summary>
        /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <value> The descriptions. </value>
        public IList<string> Descriptions { get; private set; }

        /// <summary>
        /// Returns the descriptions of an flags enumerated list masked by the specified value.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="mask"> Allows returning only selected items. </param>
        /// <returns> The descriptions. </returns>
        public IList<string> FilteredDescriptions( int mask )
        {
            int index = 0;
            var values = new List<string>();
            foreach ( object value in this.Values )
            {
                if ( (Conversions.ToInteger( value ) & mask) != 0 )
                {
                    values.Add( this.Descriptions[index] );
                }

                index += 1;
            }

            return values;
        }

        /// <summary> Gets the Name of the Enum value. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="value"> The value. </param>
        /// <returns> The name. </returns>
        public string Name( object value )
        {
            // We can hold an additional dictionary to map T -> string.
            // In my specific usage, this usages is rare, so I selected the lower performance option
            if ( value is null )
            {
                return string.Empty;
            }

            try
            {
                T enumValue = ( T ) value;
                foreach ( KeyValuePair<string, T> pair in this.NameValuePairs )
                {
                    // Dim x As Integer = Convert.ToInt32(pair.Value, Globalization.CultureInfo.CurrentCulture)
                    if ( pair.Value.Equals( enumValue ) )
                    {
                        return pair.Key;
                    }
                }
            }
            catch
            {
                throw new ArgumentException( "Cannot convert " + value.ToString() + " to " + this._Type.ToString(), nameof( value ) );
            }

            return null; // should never happen
        }

        /// <summary> Returns the names. </summary>
        /// <value> The names. </value>
        public IEnumerable<string> Names { get; private set; }

        /// <summary>
        /// Returns the Names of an flags enumerated list masked by the specified value.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="mask"> Allows returning only selected items. </param>
        /// <returns> The names. </returns>
        public IEnumerable<string> FilteredNames( int mask )
        {
            int index = 0;
            var values = new List<string>();
            foreach ( object value in this.Values )
            {
                if ( (Conversions.ToInteger( value ) & mask) != 0 )
                {
                    values.Add( this.Names.ElementAtOrDefault( index ) );
                }

                index += 1;
            }

            return values;
        }

        /// <summary> Returns the array of Enum values. </summary>
        /// <value> The values. </value>
        public Array Values { get; private set; }

        /// <summary>
        /// Returns the Values of an flags enumerated list masked by the specified value.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="mask"> Allows returning only selected items. </param>
        /// <returns> The values. </returns>
        public IEnumerable<int> FilteredValues( int mask )
        {
            int index = 0;
            var v = new List<int>();
            foreach ( object value in this.Values )
            {
                if ( (Conversions.ToInteger( value ) & mask) != 0 )
                {
                    v.Add( Conversions.ToInteger( value ) );
                }

                index += 1;
            }

            return v;
        }

        /// <summary>
        /// Returns a <see cref="IDictionary{Tkey, TValue}"/> of value and description pairs.
        /// </summary>
        /// <remarks>
        /// The first step is to add a description attribute to your Enum.
        /// <code>
        /// Public Enum SimpleEnum2
        /// [System.ComponentModel.Description("Today")] Today
        /// [System.ComponentModel.Description("Last 7 days")] Last7
        /// [System.ComponentModel.Description("Last 14 days")] Last14
        /// [System.ComponentModel.Description("Last 30 days")] Last30
        /// [System.ComponentModel.Description("All")] All
        /// End Enum
        /// Public SimpleEfficientEnum As New EnumExtender(of SimpleEnum2)
        /// Dim combo As ComboBox = New ComboBox()
        /// combo.DataSource = SimpleEfficientEnum.ValueDescriptionPairs(GetType(SimpleEnum2)).ToList
        /// combo.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
        /// combo.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
        /// dim selectedEnum as SimpleEnum2 = SimpleEfficientEnum.ToObjectFromPair(combo.SelectedItem)
        /// </code>
        /// </remarks>
        /// <value>
        /// A <see cref="IDictionary{Tkey, TValue}"/> containing the enumerated type value and description.
        /// </value>
        public IDictionary<T, string> ValueDescriptionPairs { get; private set; }

        /// <summary> Returns a <see cref="IDictionary{Tkey, TValue}"/> of value and Name pairs. </summary>
        /// <value>
        /// A <see cref="IDictionary{Tkey, TValue}"/> containing the enumerated type value and Name.
        /// </value>
        public IDictionary<T, string> ValueNamePairs { get; private set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        private readonly Type _Type;

        /// <summary> Gets the underlying type. </summary>
        /// <value> The underlying type. </value>
        public Type UnderlyingType =>
                // Seems like this is good enough. 
                Enum.GetUnderlyingType( this._Type );

        /// <summary> Returns True if the enumerated value is defined in this enumeration. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> <c>True</c> if the enumerated value is defined in this enumeration. </returns>
        public bool IsDefined( object value )
        {
            if ( value is null )
            {
                return false;
            }

            try
            {
                return this.IsDefined( Conversions.ToInteger( Conversion.Fix( value ) ) );
            }
            catch
            {
                return false;
            }
        }

        /// <summary> Returns True if the enumerated value is defined in this enumeration. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> <c>True</c> if the enumerated value is defined in this enumeration. </returns>
        public bool IsDefined( int value )
        {
            try
            {
                return this.ValueEnumValuePairs.ContainsKey( value );
            }
            catch
            {
                return false;
            }
        }

        /// <summary> Dictionary of name value pairs used for parsing case-sensitive values. </summary>
        /// <value> The name value pairs. </value>
        private IDictionary<string, T> NameValuePairs { get; set; }

        /// <summary> Returns the Enum value for the given name. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> Enum value for the given name. </returns>
        public T Parse( string value )
        {
            if ( string.IsNullOrWhiteSpace( value ) )
            {
                return default;
            }

            return this.NameValuePairs[value]; // Exception will be thrown if the value not found
        }

        /// <summary>
        /// Will serve us in the Parse method, with ignoreCase == true. It is possible not to hold this
        /// member and to define a Comparer class - But my main goal here is the performance at runtime.
        /// </summary>
        /// <value> The lower case name value pairs. </value>
        private IDictionary<string, T> LowerCaseNameValuePairs { get; set; }

        /// <summary> Returns the Enum value for the given name. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value">      The value. </param>
        /// <param name="ignoreCase"> . </param>
        /// <returns> Enum value for the given name. </returns>
        public T Parse( string value, bool ignoreCase )
        {
            if ( string.IsNullOrWhiteSpace( value ) )
            {
                return default;
            }

            if ( ignoreCase )
            {
                string valLower = value.ToLower( System.Globalization.CultureInfo.CurrentCulture );
                return this.LowerCaseNameValuePairs[valLower];
            }
            else
            {
                return this.Parse( value );
            }
        }

        /// <summary> Return a new dictionary excluding the specified values. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="excluded"> The excluded values. </param>
        /// <returns> An IDictionary(Of T, String) </returns>
        public IDictionary<T, string> Exclude( T[] excluded )
        {
            var v = new Dictionary<T, string>();
            foreach ( KeyValuePair<T, string> vv in this.ValueDescriptionPairs )
            {
                if ( !excluded.Contains( vv.Key ) )
                {
                    v.Add( vv.Key, vv.Value );
                }
            }

            return v;
        }

        #region " BIT MANIPULATION "

        /// <summary> Appends a value. </summary>
        /// <remarks>
        /// David, 2009-07-07, 1.2.3475.x.
        /// http://www.CodeProject.com/Articles/37921/Enums-Flags-and-CSharp-Oh-my-bad-pun.aspx Under The
        /// Creative Commons Attribution-ShareAlike 2.5 License.
        /// </remarks>
        /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="value"> Specifies the bit values which to add. </param>
        /// <returns> A T. </returns>
        public T Add( T value )
        {
            try
            {
                int oldValue = this.ToInteger( this._Type );
                int bitsValue = this.ToInteger( value );
                int newValue = oldValue | bitsValue;
                return ( T ) ( object ) newValue;
            }
            catch ( Exception ex )
            {
                throw new ArgumentException( $"Could not append value from enumerated type '{typeof( T ).Name}'.", ex );
            }
        }

        /// <summary> Set the specified bits. </summary>
        /// <param name="value"> Specifies the bit values which to add.</param>
        public T Set( T value )
        {
            return this.Add( value );
        }

        /// <summary> Returns true if the enumerated flag has the specified bits. </summary>
        /// <remarks>
        /// David, 2009-07-07, 1.2.3475.x
        /// http://www.CodeProject.com/Articles/37921/Enums-Flags-and-CSharp-Oh-my-bad-pun.aspx Under The
        /// Creative Commons Attribution-ShareAlike 2.5 License <para>
        /// David, 2010-06-05, 1.2.3807.x, Returns true if equals (IS).
        /// </para>
        /// </remarks>
        /// <param name="value"> Specifies the bit values which to compare. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public bool Has( T value )
        {
            try
            {
                int oldValue = this.ToInteger( this._Type );
                int bitsValue = this.ToInteger( value );
                return oldValue == bitsValue || (oldValue & bitsValue) == bitsValue;
            }
            catch
            {
                return false;
            }
        }

        /// <summary> Returns true if the specified bits are set. </summary>
        /// <remarks> David, 2010-06-05, 1.2.3807.x Returns true if equals (IS). </remarks>
        /// <param name="value"> Specifies the bit values which to compare. </param>
        /// <returns> True if set, false if not. </returns>
        public bool IsSet( T value )
        {
            return this.Has( value );
        }

        /// <summary>  Returns true if value is only the provided type. </summary>
        /// <param name="value"> Specifies the bit values which to compare.</param>
        /// <remarks> David, 2009-07-07, 1.2.3475.x.
        /// http://www.CodeProject.com/Articles/37921/Enums-Flags-and-CSharp-Oh-my-bad-pun.aspx
        /// Under The Creative Commons Attribution-ShareAlike 2.5 License </remarks>
        public bool Is( T value )
        {
            try
            {
                int oldValue = this.ToInteger( this._Type );
                int bitsValue = this.ToInteger( value );
                return oldValue == bitsValue;
            }
            catch
            {
                return false;
            }
        }

        /// <summary> Clears the specified bits. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> Specifies the bit values which to remove. </param>
        /// <returns> A T. </returns>
        public T Clear( T value )
        {
            return this.Remove( value );
        }

        /// <summary> Completely removes the value. </summary>
        /// <remarks>
        /// David, 2009-07-07, 1.2.3475.x
        /// http://www.CodeProject.com/Articles/37921/Enums-Flags-and-CSharp-Oh-my-bad-pun.aspx Under The
        /// Creative Commons Attribution-ShareAlike 2.5 License.
        /// </remarks>
        /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="value"> Specifies the bit values which to remove. </param>
        /// <returns> A T. </returns>
        public T Remove( T value )
        {
            try
            {
                int oldValue = this.ToInteger( this._Type );
                int bitsValue = this.ToInteger( value );
                int newValue = oldValue & ~bitsValue;
                return ( T ) ( object ) newValue;
            }
            catch ( Exception ex )
            {
                throw new ArgumentException( $"Could not remove value from enumerated type '{typeof( T ).Name}'.", ex );
            }
        }


        #endregion

        #region " CONVERSIONS "

        /// <summary> Returns the value converted to Enum. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="value"> The value. </param>
        /// <returns> value converted to Enum. </returns>
        public T ToObject( object value )
        {
            if ( value is null )
            {
                return default;
            }

            try
            {
                int wholeValue = Conversions.ToInteger( Conversion.Fix( value ) );
                return this.ToObject( wholeValue );
            }
            catch ( InvalidCastException ex )
            {
                throw new ArgumentException( $"Cannot convert {value} to integer", nameof( value ), ex );
            }
            // If an exception is coming from ToObject(value) do not catch it here.
        }

        /// <summary> Converts a value to an integer. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="value"> The value. </param>
        /// <returns> value as an Integer. </returns>
        public int ToInteger( object value )
        {
            if ( value is null )
            {
                throw new ArgumentNullException( nameof( value ) );
            }

            try
            {
                return Conversions.ToInteger( Conversion.Fix( value ) );
            }
            catch ( InvalidCastException ex )
            {
                throw new ArgumentException( $"Cannot convert {value} to integer", nameof( value ), ex );
            }
        }

        /// <summary> Will serve us in the <see cref="ToObject(int)"/> method. </summary>
        /// <value> The value enum value pairs. </value>
        public IDictionary<int, T> ValueEnumValuePairs { get; private set; }

        /// <summary> Returns the value converted to Enum. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> value converted to Enum. </returns>
        public T ToObject( int value )
        {
            return this.ValueEnumValuePairs[value];
        }

        /// <summary> Returns the value converted to Enum from the given value name pair. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> converted to Enum from the given value name pair. </returns>
        public T ToObjectFromPair( object value )
        {
            return value is null ? default : this.ToObject( (( KeyValuePair<T, string> ) value).Key );
        }

        /// <summary> Converts a type to an integer. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="type"> The <see cref="T:System.Enum"/> type. </param>
        /// <returns> The given data converted to an integer. </returns>
        public int ToInteger( Enum type )
        {
            return this.ToInteger( type );
        }

        /// <summary> Returns the value converted to Enum from the given value name pair. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="values"> The values. </param>
        /// <param name="value">  The value. </param>
        /// <returns> converted to Enum from the given value name pair. </returns>
        public T ToObjectFromPair( IDictionary<int, T> values, object value )
        {
            return value is null ? default : this.ToObject( values, (( KeyValuePair<T, string> ) value).Key );
        }

        /// <summary> Returns the value converted to Enum. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="values"> The values. </param>
        /// <param name="value">  The value. </param>
        /// <returns> value converted to Enum. </returns>
        public T ToObject( IDictionary<int, T> values, object value )
        {
            if ( value is null )
            {
                return default;
            }

            try
            {
                int wholeValue = Conversions.ToInteger( Conversion.Fix( value ) );
                return this.ToObject( values, wholeValue );
            }
            catch ( InvalidCastException ex )
            {
                throw new ArgumentException( $"Cannot convert {value} to integer", nameof( value ), ex );
            }
            // If an exception is coming from ToObject(value) do not catch it here.
        }

        /// <summary> Returns the value converted to Enum. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values"> The values. </param>
        /// <param name="value">  The value. </param>
        /// <returns> value converted to Enum. </returns>
        public T ToObject( IDictionary<int, T> values, int value )
        {
            return values is null ? throw new ArgumentNullException( nameof( values ) ) : values[value];
        }

        #endregion

        #region " BINDING LIST "

        /// <summary> Converts the pairs to a binding list. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="pairs"> The pairs. </param>
        /// <returns> Pairs as a ComponentModel.BindingList(Of KeyValuePair(Of T, String)) </returns>
        public BindingList<KeyValuePair<T, string>> ToBindingList( IDictionary<T, string> pairs )
        {
            var bl = new BindingList<KeyValuePair<T, string>>();
            this.Populate( bl, pairs );
            return bl;
        }

        /// <summary> Converts the pairs to a binding list. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="pairs"> The pairs. </param>
        /// <returns> Pairs as a ComponentModel.BindingList(Of KeyValuePair(Of T, String)) </returns>
        public BindingList<KeyValuePair<T, string>> ToBindingList( IList pairs )
        {
            var bl = new BindingList<KeyValuePair<T, string>>();
            this.Populate( bl, pairs );
            return bl;
        }

        /// <summary> Populates the binding list with key value pairs. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="bindingList"> List of bindings. </param>
        /// <param name="pairs">       The pairs. </param>
        public void Populate( BindingList<KeyValuePair<T, string>> bindingList, IDictionary<T, string> pairs )
        {
            if ( bindingList is null )
            {
                throw new ArgumentNullException( nameof( bindingList ) );
            }

            if ( pairs is null )
            {
                throw new ArgumentNullException( nameof( pairs ) );
            }

            foreach ( KeyValuePair<T, string> pair in pairs )
            {
                bindingList.Add( pair );
            }
        }

        /// <summary> Populates the binding list with key value pairs. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="bindingList"> List of bindings. </param>
        /// <param name="pairs">       The pairs. </param>
        public void Populate( BindingList<KeyValuePair<T, string>> bindingList, IList pairs )
        {
            if ( bindingList is null )
            {
                throw new ArgumentNullException( nameof( bindingList ) );
            }

            if ( pairs is null )
            {
                throw new ArgumentNullException( nameof( pairs ) );
            }

            foreach ( KeyValuePair<T, string> pair in pairs )
            {
                bindingList.Add( pair );
            }
        }

        #endregion

    }
}
