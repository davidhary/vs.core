﻿using System.ComponentModel;

namespace isr.Core
{

    /// <summary> A list changed base. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    public partial class ListChangedBase
    {

        #region " List changed event handler "

        /// <summary> Removes the List Changed event handlers. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        protected void RemoveListChangedEventHandler()
        {
            this._ListChangedHandlers.RemoveAll();
        }

        /// <summary> The list changed handlers. </summary>
        private readonly NotifyListChangedEventContextCollection _ListChangedHandlers = new();

        /// <summary> Event queue for all listeners interested in List Changed events. </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>

        public event ListChangedEventHandler ListChanged
        {
            add {
                this._ListChangedHandlers.Add( new NotifyListChangedEventContext( value ) );
            }

            remove {
                this._ListChangedHandlers.RemoveValue( value );
            }
        }

        private void OnListChanged( object sender, ListChangedEventArgs e )
        {
            this._ListChangedHandlers.Raise( sender, e );
        }

        #endregion

        #region " Notify "

        /// <summary>
        /// Asynchronously notifies (posts) change on a different thread. Unsafe for cross threading;
        /// fast return of control to the invoking function.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="e"> List Changed event information. </param>
        protected virtual void NotifyListChanged( ListChangedEventArgs e )
        {
            this._ListChangedHandlers.Post( this, e );
        }

        /// <summary>
        /// Asynchronously notifies (posts) change on a different thread. Unsafe for cross threading;
        /// fast return of control to the invoking function.
        /// </summary>
        /// <remarks>
        /// Overhead of this method is 7 to 10 times larger than naked raise event. This has no advantage
        /// even with slow handler functions.
        /// </remarks>
        /// <param name="e"> List Changed event information. </param>
        protected virtual void AsyncNotifyListChanged( ListChangedEventArgs e )
        {
            this._ListChangedHandlers.Post( this, e );
        }

        /// <summary>
        /// Synchronously notifies (send) list change on a different thread. Safe for cross threading.
        /// </summary>
        /// <remarks>
        /// Overhead of this method is 3 to 4 times larger than naked raise event. This is the best
        /// approach.
        /// </remarks>
        /// <param name="e"> List Changed event information. </param>
        protected virtual void SyncNotifyListChanged( ListChangedEventArgs e )
        {
            this._ListChangedHandlers.Send( this, e );
        }

        #endregion

    }
}