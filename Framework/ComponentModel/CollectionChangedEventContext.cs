using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading;

namespace isr.Core
{

    /// <summary> A notify collection changed event context. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-01-17 </para>
    /// </remarks>
    public class NotifyCollectionChangedEventContext
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="handler"> The handler. </param>
        public NotifyCollectionChangedEventContext( NotifyCollectionChangedEventHandler handler )
        {
            this.Handler = handler;
            this.Context = SynchronizationContext.Current;
        }

        /// <summary> Gets the synchronization context. </summary>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The context. </value>
        private SynchronizationContext Context { get; set; }

        /// <summary> Gets the handler. </summary>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The handler. </value>
        public NotifyCollectionChangedEventHandler Handler { get; private set; }

        #region " INVOKE "

        /// <summary>
        /// Executes the given operation directly irrespective of the <see cref="Context"/>.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Collection Changed event information. </param>
        public void UnsafeInvoke( object sender, NotifyCollectionChangedEventArgs e )
        {
            var evt = this.Handler;
            evt?.Invoke( sender, e );
        }

        #endregion

        #region " ACTIVE CONTEXT "

        /// <summary> Returns the current synchronization context. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown if the current synchronization thread is
        /// null. </exception>
        /// <returns> A Threading.SynchronizationContext. </returns>
        private static SynchronizationContext CurrentSyncContext()
        {
            if ( SynchronizationContext.Current is null )
            {
                SynchronizationContext.SetSynchronizationContext( new SynchronizationContext() );
            }

            return SynchronizationContext.Current is null
                ? throw new InvalidOperationException( "Current Synchronization Context not set;. Must be set before starting the thread." )
                : SynchronizationContext.Current;
        }

        /// <summary> Gets a context for the active. </summary>
        /// <value> The active context. </value>
        private SynchronizationContext ActiveContext => this.Context ?? CurrentSyncContext();

        #endregion

        #region " SEND POST "

        /// <summary>
        /// Asynchronously raises (Posts) the <see cref="NotifyCollectionChangedEventHandler"/>. It does
        /// all the checking to see if the SynchronizationContext is nothing or not, and invokes the
        /// delegate accordingly. Note that, unlike the synchronous Send action, although the event is
        /// posted on the synchronization context, cross thread exceptions still occurred, ostensibly,
        /// due to the changing of the sync context between the time the action was elicited and the time
        /// it was actually invoked.
        /// </summary>
        /// <remarks> David, 2020-07-27. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Collection Changed event information. </param>
        public void Post( object sender, NotifyCollectionChangedEventArgs e )
        {
            var evt = this.Handler;
            if ( evt is object )
            {
                this.ActiveContext.Post( ( object state ) => evt( sender, e ), null );
            }
        }

        /// <summary>
        /// Synchronously raises (sends) the <see cref="NotifyCollectionChangedEventHandler"/>. It does
        /// all the checking to see if the SynchronizationContext is nothing or not, and invokes the
        /// delegate accordingly.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Collection Changed event information. </param>
        public void Send( object sender, NotifyCollectionChangedEventArgs e )
        {
            var evt = this.Handler;
            if ( evt is object )
            {
                this.ActiveContext.Send( ( object state ) => evt( sender, e ), null );
            }
        }

        #endregion

    }

    /// <summary> Collection of Notify Collection Changed event contexts. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-11 </para>
    /// </remarks>
    public class NotifyCollectionChangedEventContextCollection : List<NotifyCollectionChangedEventContext>
    {

        /// <summary>
        /// Executes the given operation directly irrespective of the
        /// <see cref="SynchronizationContext"/>.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Collection Changed event information. </param>
        public void UnsafeInvoke( object sender, NotifyCollectionChangedEventArgs e )
        {
            foreach ( NotifyCollectionChangedEventContext evt in this )
            {
                evt?.UnsafeInvoke( sender, e );
            }
        }

        /// <summary> Executes (sends) the event handler action synchronously (thread safe). </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Collection Changed event information. </param>
        public void Raise( object sender, NotifyCollectionChangedEventArgs e )
        {
            foreach ( NotifyCollectionChangedEventContext evt in this )
            {
                evt?.Send( sender, e );
            }
        }

        /// <summary> Executes (posts) the event handler action asynchronously (thread unsafe). </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Collection Changed event information. </param>
        public void Post( object sender, NotifyCollectionChangedEventArgs e )
        {
            foreach ( NotifyCollectionChangedEventContext evt in this )
            {
                evt?.Post( sender, e );
            }
        }

        /// <summary> Executes (sends) the event handler action synchronously (thread safe). </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Collection Changed event information. </param>
        public void Send( object sender, NotifyCollectionChangedEventArgs e )
        {
            foreach ( NotifyCollectionChangedEventContext evt in this )
            {
                evt?.Send( sender, e );
            }
        }

        /// <summary> Looks up a given key to find its associated last index. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> An Integer. </returns>
        private int LookupLastIndex( NotifyCollectionChangedEventHandler value )
        {
            return this.ToList().FindLastIndex( x => x.Handler.Equals( value ) );
        }

        /// <summary> Removes the value described by value. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="value"> The value. </param>
        public void RemoveValue( NotifyCollectionChangedEventHandler value )
        {
            if ( this.Any() )
            {
                int lastIndex = this.LookupLastIndex( value );
                if ( lastIndex >= 0 && lastIndex < this.Count && this.ElementAt( lastIndex ).Handler.Equals( value ) )
                {
                    this.RemoveAt( lastIndex );
                }
            }
        }

        /// <summary> Removes all. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        public void RemoveAll()
        {
            while ( this.Count > 0 )
            {
                this.RemoveAt( 0 );
            }
        }
    }
}
