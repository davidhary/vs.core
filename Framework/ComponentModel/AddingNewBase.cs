﻿using System.ComponentModel;

namespace isr.Core
{

    /// <summary> An adding new base. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    public partial class AddingNewBase
    {

        #region " Adding New event handler "

        /// <summary> Removes the Adding New event handlers. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        protected void RemoveAddingNewEventHandler()
        {
            this._AddingNewHandlers.RemoveAll();
        }

        /// <summary> The adding new handlers. </summary>
        private readonly NotifyAddingNewEventContextCollection _AddingNewHandlers = new();

        /// <summary> Event queue for all listeners interested in Adding New events. </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>

        public event AddingNewEventHandler AddingNew
        {
            add {
                this._AddingNewHandlers.Add( new NotifyAddingNewEventContext( value ) );
            }

            remove {
                this._AddingNewHandlers.RemoveValue( value );
            }
        }

        private void OnAddingNew( object sender, AddingNewEventArgs e )
        {
            this._AddingNewHandlers.Raise( sender, e );
        }

        #endregion

        #region " Notify "

        /// <summary>
        /// Asynchronously notifies (posts) change on a different thread. Unsafe for cross threading;
        /// fast return of control to the invoking function.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="e"> Adding New event information. </param>
        protected virtual void NotifyAddingNew( AddingNewEventArgs e )
        {
            this._AddingNewHandlers.Post( this, e );
        }

        /// <summary>
        /// Asynchronously notifies (posts) change on a different thread. Unsafe for cross threading;
        /// fast return of control to the invoking function.
        /// </summary>
        /// <remarks>
        /// Overhead of this method is 7 to 10 times larger than naked raise event. This has no advantage
        /// even with slow handler functions.
        /// </remarks>
        /// <param name="e"> Adding New event information. </param>
        protected virtual void AsyncNotifyAddingNew( AddingNewEventArgs e )
        {
            this._AddingNewHandlers.Post( this, e );
        }

        /// <summary>
        /// Synchronously notifies (send) adding new on the calling thread. Safe for cross threading.
        /// </summary>
        /// <remarks>
        /// Overhead of this method is 3 to 4 times larger than naked raise event. This is the best
        /// approach.
        /// </remarks>
        /// <param name="e"> Adding New event information. </param>
        protected virtual void SyncNotifyAddingNew( AddingNewEventArgs e )
        {
            this._AddingNewHandlers.Send( this, e );
        }

        #endregion

    }
}