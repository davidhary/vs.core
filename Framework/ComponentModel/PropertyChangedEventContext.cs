using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;

namespace isr.Core
{

    /// <summary> A property changed event context. </summary>
    /// <remarks>
    /// The <see cref="PropertyChangedEventContext"/> and the custom
    /// <see cref="PropertyChangedEventHandler"/> make the PropertyChanged event fire on the
    /// SynchronizationContext of the listening code. <para>
    /// The PropertyChanged event declaration is modified to a custom event handler. </para><para>
    /// As each handler may have a different context, the AddHandler block stores the handler being
    /// passed in as well as the SynchronizationContext to be used later when raising the event.
    /// .</para> <para>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2009-10-01 from Bill McCarthy </para><para>
    /// https://visualstudiomagazine.com/Articles/2009/10/01/Threading-and-the-UI.aspx?Page=2. </para>
    /// </remarks>
    public partial class PropertyChangedEventContext
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="handler"> The handler. </param>
        public PropertyChangedEventContext( PropertyChangedEventHandler handler )
        {
            this.Handler = handler;
            this.Context = SynchronizationContext.Current;
        }

        /// <summary> Gets or sets the synchronization context. </summary>
        /// <value> The context. </value>
        private SynchronizationContext Context { get; set; }

        /// <summary> Gets or sets the handler. </summary>
        /// <value> The handler. </value>
        public PropertyChangedEventHandler Handler { get; private set; }

        #region " INVOKE "

        /// <summary>
        /// Executes the given operation directly irrespective of the <see cref="Context"/>.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property changed event information. </param>
        public void UnsafeInvoke( object sender, PropertyChangedEventArgs e )
        {
            var evt = this.Handler;
            evt?.Invoke( sender, e );
        }

        #endregion

    }

    /// <summary> Collection of property change event contexts. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-11 </para>
    /// </remarks>
    public class PropertyChangeEventContextCollection : List<PropertyChangedEventContext>
    {

        /// <summary>
        /// Executes the given operation directly irrespective of the
        /// <see cref="SynchronizationContext"/>.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property changed event information. </param>
        public void UnsafeInvoke( object sender, PropertyChangedEventArgs e )
        {
            foreach ( PropertyChangedEventContext evt in this )
            {
                evt?.UnsafeInvoke( sender, e );
            }
        }

        /// <summary> Executes (sends) the event handler action synchronously (thread safe). </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property changed event information. </param>
        public void Raise( object sender, PropertyChangedEventArgs e )
        {
            foreach ( PropertyChangedEventContext evt in this )
            {
                evt?.Send( sender, e );
            }
        }

        /// <summary> Executes (posts) the event handler action. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property changed event information. </param>
        public void Post( object sender, PropertyChangedEventArgs e )
        {
            foreach ( PropertyChangedEventContext evt in this )
            {
                evt?.Post( sender, e );
            }
        }

        /// <summary> Executes (sends) the event handler action synchronously (thread safe). </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property changed event information. </param>
        public void Send( object sender, PropertyChangedEventArgs e )
        {
            foreach ( PropertyChangedEventContext evt in this )
            {
                evt?.Send( sender, e );
            }
        }

        /// <summary> Posts a dynamic invoke of the event delegate. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property changed event information. </param>
        public void PostDynamicInvoke( object sender, PropertyChangedEventArgs e )
        {
            foreach ( PropertyChangedEventContext evt in this )
            {
                evt?.PostDynamicInvoke( sender, e );
            }
        }

        /// <summary> Looks up a given key to find its associated last index. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> An Integer. </returns>
        private int LookupLastIndex( PropertyChangedEventHandler value )
        {
            return this.FindLastIndex( x => x.Handler.Equals( value ) );
        }

        /// <summary> Removes the value described by value. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="value"> The value. </param>
        public void RemoveValue( PropertyChangedEventHandler value )
        {
            if ( this.Any() )
            {
                int lastIndex = this.LookupLastIndex( value );
                if ( lastIndex >= 0 && lastIndex < this.Count && this.ElementAt( lastIndex ).Handler.Equals( value ) )
                {
                    this.RemoveAt( lastIndex );
                }
            }
        }

        /// <summary> Removes all. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        public void RemoveAll()
        {
            while ( this.Count > 0 )
            {
                this.RemoveAt( 0 );
            }
        }
    }
}
