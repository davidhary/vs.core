
using System;
using System.Diagnostics;

namespace isr.Core
{
    public partial class Logger : ITraceMessageListener
    {

        /// <summary> Gets the thread enabled. </summary>
        /// <value> The thread enabled. </value>
        public bool ThreadEnabled { get; set; } = true;

        /// <summary> Writes a trace event to the trace listeners. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="value"> The event message. </param>
        /// <returns> The trace message details. </returns>
        public string TraceEvent( TraceMessage value )
        {
            if ( value is null )
            {
                return string.Empty;
            }
            else
            {
                if ( this.ThreadEnabled && this.ShouldTrace( value.EventType ) )
                {
                    this.GetContentsQueue()?.Enqueue( value );
                    this.FlushMessagesTask();
                }
                else if ( this.ShouldTrace( value.EventType ) )
                {
                    this.WriteLogEntry( value );
                }

                return value.Details;
            }
        }

        /// <summary> Writes a trace event to the trace listeners. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="id">        The identifier. </param>
        /// <param name="format">    The message format. </param>
        /// <param name="args">      Specified the message arguments. </param>
        /// <returns> The trace message details. </returns>
        public string TraceEvent( TraceEventType eventType, int id, string format, params object[] args )
        {
            return this.TraceEvent( new TraceMessage( eventType, id, format, args ) );
        }

        /// <summary> Writes a trace event to the trace listeners. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="value"> The event message. </param>
        /// <returns> The trace message details. </returns>
        public string TraceEventOverride( TraceMessage value )
        {
            if ( value is null )
            {
                return string.Empty;
            }
            else
            {
                if ( this.ThreadEnabled )
                {
                    this.GetContentsQueue()?.Enqueue( value );
                    this.FlushMessagesTask();
                }
                else
                {
                    this.WriteLogEntry( value );
                }

                return value.Details;
            }
        }

        /// <summary> Writes a trace event to the trace listeners. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="id">        The identifier. </param>
        /// <param name="format">    The message format. </param>
        /// <param name="args">      Specified the message arguments. </param>
        /// <returns> The trace message details. </returns>
        public string TraceEventOverride( TraceEventType eventType, int id, string format, params object[] args )
        {
            return this.TraceEventOverride( new TraceMessage( eventType, id, format, args ) );
        }

        /// <summary> Checks if the log should trace the event type. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="value"> The <see cref="TraceEventType">event type</see>. </param>
        /// <returns> <c>True</c> If the log should trace; <c>False</c> otherwise. </returns>
        public bool ShouldTrace( TraceEventType value )
        {
            return value <= this.TraceLevel;
        }

        /// <summary> Gets a unique identifier. </summary>
        /// <value> The identifier of the unique. </value>
        public Guid UniqueId { get; private set; } = Guid.NewGuid();

        /// <summary> Gets the type of the listener. </summary>
        /// <value> The type of the listener. </value>
        public ListenerType ListenerType => ListenerType.Logger;

        /// <summary> Tests if this ITraceMessageListener is considered equal to another. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="other"> The i trace message listener to compare to this object. </param>
        /// <returns>
        /// <c>true</c> if the objects are considered equal, <c>false</c> if they are not.
        /// </returns>
        public bool Equals( IMessageListener other )
        {
            return other is object && Equals( other.UniqueId, this.UniqueId );
        }

    }
}
