using System.Collections.Generic;
using System.Diagnostics;

namespace isr.Core.DiagnosticsExtensions
{
    public static partial class DiagnosticsExtensionMethods
    {

        /// <summary> Builds trace level trace event type hash. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns> A Dictionary for translating trace levels to trace event types. </returns>
        public static IDictionary<TraceLevel, TraceEventType> BuildTraceLevelTraceEventTypeHash()
        {
            var dix2 = new Dictionary<TraceLevel, TraceEventType>();
            var dix3 = dix2;
            dix3.Add( System.Diagnostics.TraceLevel.Error, TraceEventType.Error );
            dix3.Add( System.Diagnostics.TraceLevel.Info, TraceEventType.Information );
            dix3.Add( System.Diagnostics.TraceLevel.Off, TraceEventType.Stop );
            dix3.Add( System.Diagnostics.TraceLevel.Verbose, TraceEventType.Verbose );
            dix3.Add( System.Diagnostics.TraceLevel.Warning, TraceEventType.Warning );
            return dix2;
        }

        /// <summary> The trace level trace event type hash. </summary>
        private static IDictionary<TraceLevel, TraceEventType> _TraceLevelTraceEventTypeHash;

        /// <summary> Trace level trace event type hash. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns> An IDictionary(Of TraceLevel, TraceEventType) </returns>
        private static IDictionary<TraceLevel, TraceEventType> TraceLevelTraceEventTypeHash()
        {
            if ( _TraceLevelTraceEventTypeHash is null )
            {
                _TraceLevelTraceEventTypeHash = BuildTraceLevelTraceEventTypeHash();
            }

            return _TraceLevelTraceEventTypeHash;
        }

        /// <summary> Derives a <see cref="TraceEventType">trace event type</see>. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="value"> A <see cref="System.Diagnostics.TraceLevel">trace level</see> of a trace switch. Specifies
        /// what messages to output for the
        /// <see cref="System.Diagnostics.Debug">debug</see>
        /// and <see cref="System.Diagnostics.Trace">trace</see> and
        /// <see cref="System.Diagnostics.TraceSwitch">trace switch</see> classes.
        /// </param>
        /// <returns> The <see cref="TraceEventType">event type</see>. </returns>
        public static TraceEventType ToTraceEventType( this TraceLevel value )
        {
            return TraceLevelTraceEventTypeHash().ContainsKey( value ) ? TraceLevelTraceEventTypeHash()[value] : TraceEventType.Information;
        }

    }
}
