using System.Diagnostics;

namespace isr.Core.DiagnosticsExtensions
{

    /// <summary> Extends the <see cref="Microsoft.VisualBasic.Logging">Logging</see> functionality. </summary>
    /// <remarks> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-02-09, 2.0.5152 </para></remarks>
    public static partial class DiagnosticsExtensionMethods
    {

        #region " TRACE LEVEL "

        /// <summary> Applies the trace level. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="traceSource"> The <see cref="TraceSource">Trace Source</see>. </param>
        /// <param name="value">       The <see cref="TraceEventType">trace level</see> value. </param>
        public static void ApplyTraceLevel( this TraceSource traceSource, TraceEventType value )
        {
            if ( traceSource is object && traceSource.Switch is object )
            {
                traceSource.Switch.ApplyTraceLevel( value );
            }
        }

        /// <summary> Returns the trace level. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="traceSource"> The <see cref="TraceSource">Trace Source</see>. </param>
        /// <returns>
        /// The trace level; <see cref="TraceEventType.Information">information</see> if nothing.
        /// </returns>
        public static TraceEventType TraceLevel( this TraceSource traceSource )
        {
            return traceSource is null || traceSource.Switch is null ? TraceEventType.Information : traceSource.Switch.TraceLevel();
        }

        /// <summary> Checks if the log should trace the event type. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="traceSource"> The <see cref="TraceSource">Trace Source</see>. </param>
        /// <param name="eventType">   The <see cref="TraceEventType">event type</see>. </param>
        /// <returns>
        /// <c>True</c>&gt; If the log should trace the  c&gt;TrueThe trace level;
        /// <see cref="TraceEventType.Information">information</see> if log is nothing.
        /// </returns>
        public static bool ShouldTrace( this TraceSource traceSource, TraceEventType eventType )
        {
            return eventType <= traceSource.TraceLevel();
        }

        #endregion

        #region " FILE  "

        /// <summary> Returns the default file log writer. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="traceSource"> The <see cref="TraceSource">Trace Source</see>. </param>
        /// <returns>
        /// The default <see cref="Microsoft.VisualBasic.Logging.FileLogTraceListener">file log writer</see>.
        /// </returns>
        public static Microsoft.VisualBasic.Logging.FileLogTraceListener DefaultFileLogWriter( this TraceSource traceSource )
        {
            return traceSource is null ? null :
                   ( Microsoft.VisualBasic.Logging.FileLogTraceListener ) traceSource.Listeners[CustomFileLogTraceListener.DefaultFileLogWriterName];
        }

        /// <summary> Returns the filename of the default file log writer. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="traceSource"> The <see cref="TraceSource">Trace Source</see>. </param>
        /// <returns> The filename of the log file. </returns>
        public static string DefaultFileLogWriterFilePath( this TraceSource traceSource )
        {
            return traceSource is null
                ? string.Empty
                : traceSource.DefaultFileLogWriter() is null
                    ? string.Empty
                    : traceSource.DefaultFileLogWriter() is object ? traceSource.DefaultFileLogWriter().FullLogFileName : string.Empty;
        }

        /// <summary> Checks if the default file log writer file exists. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="traceSource"> The <see cref="TraceSource">Trace Source</see>. </param>
        /// <returns> <c>True</c> if the log file exists. </returns>
        public static bool DefaultFileLogWriterFileExists( this TraceSource traceSource )
        {
            return CustomFileLogTraceListener.FileSize( traceSource.DefaultFileLogWriterFilePath() ) > 2L;
        }

        #endregion

        #region " REPLACE TRACE LISTENER "

        /// <summary> Replaces the default file log trace listener with a new one. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="traceSource"> The <see cref="TraceSource">Trace Source</see>. </param>
        /// <param name="logWriter">   The <see cref="Microsoft.VisualBasic.Logging.FileLogTraceListener">log writer</see>. </param>
        /// <returns>
        /// The <see cref="Microsoft.VisualBasic.Logging.FileLogTraceListener">file log trace listener. </see>
        /// </returns>
        public static Microsoft.VisualBasic.Logging.FileLogTraceListener ReplaceDefaultTraceListener( this TraceSource traceSource, Microsoft.VisualBasic.Logging.FileLogTraceListener logWriter )
        {
            if ( traceSource is object )
            {
                traceSource.Listeners.Remove( CustomFileLogTraceListener.DefaultFileLogWriterName );
                _ = traceSource.Listeners.Add( logWriter );
            }

            return logWriter;
        }

        /// <summary>
        /// Replaces the default file log trace listener with a new one for the current user.
        /// </summary>
        /// <remarks> The current user application data folder is used. </remarks>
        /// <param name="traceSource"> The <see cref="TraceSource">Trace Source</see>. </param>
        /// <returns>
        /// The <see cref="Microsoft.VisualBasic.Logging.FileLogTraceListener">file log trace listener. </see>
        /// </returns>
        public static Microsoft.VisualBasic.Logging.FileLogTraceListener ReplaceDefaultTraceListener( this TraceSource traceSource )
        {
            return traceSource.ReplaceDefaultTraceListener( false );
        }

        /// <summary> Replaces the default file log trace listener with a new one. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="traceSource"> The <see cref="TraceSource">Trace Source</see>. </param>
        /// <param name="allUsers">    True to select the folder for
        /// <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.AllUsersApplicationData"/>
        /// all users; otherwise, use using current user; otherwise, use the
        /// folder for
        /// <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.CurrentUserApplicationData"/>
        /// </param>
        /// <returns>
        /// The <see cref="Microsoft.VisualBasic.Logging.FileLogTraceListener">file log trace listener. </see>
        /// </returns>
        public static Microsoft.VisualBasic.Logging.FileLogTraceListener ReplaceDefaultTraceListener( this TraceSource traceSource, bool allUsers )
        {
            Microsoft.VisualBasic.Logging.FileLogTraceListener tempListener = null;
            Microsoft.VisualBasic.Logging.FileLogTraceListener listener = null;
            try
            {
                tempListener = new CustomFileLogTraceListener( allUsers );
                listener = tempListener;
                if ( traceSource is object )
                {
                    _ = traceSource.ReplaceDefaultTraceListener( listener );
                }
            }
            finally
            {
                if ( tempListener is object )
                {
                    tempListener.Dispose();
                }
            }

            return listener;
        }

        #endregion

    }
}
