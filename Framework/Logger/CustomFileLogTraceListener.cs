using System;
using System.Collections;
using System.Diagnostics;
using System.Globalization;
using System.Security.Permissions;

namespace isr.Core
{

    /// <summary> A custom file log trace listener for replacing the default listener. </summary>
    /// <remarks>
    /// (c) 2011 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2011-08-08 </para>
    /// </remarks>
    public class CustomFileLogTraceListener : Microsoft.VisualBasic.Logging.FileLogTraceListener
    {

        /// <summary> Gets the default file log trace listener name. </summary>
        public const string DefaultFileLogWriterName = "FileLog";

        /// <summary> The default source name. </summary>
        public const string DefaultSourceName = "DefaultSource";

        /// <summary> The default folder title. </summary>
        public const string DefaultFolderTitle = "_log";

        /// <summary> The default time format. </summary>
        public const string DefaultTimeFormat = "HH:mm:ss.fff";

        /// <summary> The default delimiter. </summary>
        public const string DefaultDelimiter = ",";

        /// <summary> The default trace identifier format. </summary>
        public const string DefaultTraceIdFormat = "{0,5:X}";

        /// <summary> Gets or sets the trace identifier format. </summary>
        /// <value> The trace identifier format. </value>
        public string TraceIdFormat { get; set; }

        /// <summary> The default process identifier format. </summary>
        public const string DefaultProcessIdFormat = "{0:X}";

        /// <summary> Gets or sets the process identifier format. </summary>
        /// <value> The process identifier format. </value>
        public string ProcessIdIdFormat { get; set; }

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Logging.FileLogTraceListener">file log trace
        /// listener</see>.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="allUsers"> True to select the folder for
        /// <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.AllUsersApplicationData"/>
        /// all users; otherwise, use using current user; otherwise, use the
        /// folder for
        /// <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.CurrentUserApplicationData"/>
        /// </param>
        public CustomFileLogTraceListener( bool allUsers ) : this( DefaultFileLogWriterName, DefaultFolderTitle, allUsers )
        {
        }

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Logging.FileLogTraceListener">file log trace
        /// listener</see> using the application folder or the specified user level application data
        /// folder if the application folder is protected.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="name">        The name of the
        /// <see cref="T:System.Diagnostics.TraceListener"></see>. </param>
        /// <param name="folderTitle"> Name of the folder. </param>
        /// <param name="allUsers">    True to select the folder for
        /// <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.AllUsersApplicationData"/>
        /// all users; otherwise, use using current user; otherwise, use the
        /// folder for
        /// <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.CurrentUserApplicationData"/>
        /// </param>
        public CustomFileLogTraceListener( string name, string folderTitle, bool allUsers ) : this( name, BuildCustomFolder( folderTitle, allUsers ) )
        {
        }

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Logging.FileLogTraceListener">file log trace
        /// listener</see> using the application folder or the specified user level application data
        /// folder if the application folder is protected.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="name">   The name of the
        /// <see cref="T:System.Diagnostics.TraceListener"></see>. </param>
        /// <param name="folder"> Pathname of the folder. </param>
        public CustomFileLogTraceListener( string name, string folder ) : base( name )
        {
            this.TraceIdFormat = DefaultTraceIdFormat;
            this.TimeFormat = DefaultTimeFormat;
            this.UsingUniversalTime = true;
            this.TraceIdFormat = DefaultTraceIdFormat;
            this.ProcessIdIdFormat = DefaultProcessIdFormat;

            // set the default properties of the log file name 
            this.LogFileCreationSchedule = Microsoft.VisualBasic.Logging.LogFileCreationScheduleOption.Daily;
            this.Location = Microsoft.VisualBasic.Logging.LogFileLocation.Custom;
            this.Delimiter = DefaultDelimiter;
            this.CustomLocation = folder;
            this.IncludeHostName = false;
            // MUST BE SET TRUE for logging to work with legacy Logger. Not sure why.
            this.AutoFlush = false;
            this.TraceOutputOptions = TraceOptions.DateTime | TraceOptions.LogicalOperationStack;

            // note that the trace event cache time format is not set correctly to -8 hours offset.
            // Rather, it gives us a -4 hours offset.
        }

        /// <summary> Creates a listener. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="allUsers"> True to select the folder for
        /// <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.AllUsersApplicationData"/>
        /// all users; otherwise, use using current user; otherwise, use the
        /// folder for
        /// <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.CurrentUserApplicationData"/>
        /// </param>
        /// <returns> The new listener. </returns>
        public static Microsoft.VisualBasic.Logging.FileLogTraceListener CreateListener( bool allUsers )
        {
            Microsoft.VisualBasic.Logging.FileLogTraceListener tempListener = null;
            try
            {
                tempListener = new CustomFileLogTraceListener( allUsers );
                var listener = tempListener;
                return listener;
            }
            catch
            {
                throw;
            }
            finally
            {
                if ( tempListener is object )
                {
                    tempListener.Dispose();
                }
            }
        }

        #region " WRITE "

        /// <summary> The universal time suffix. </summary>
        private const string _UniversalTimeSuffix = "Z";

        /// <summary> true to using universal time. </summary>
        private bool _UsingUniversalTime;

        /// <summary>
        /// Gets or sets the condition for using universal time.  When using universal time, the format
        /// is automatically modified to add a <see cref="_UniversalTimeSuffix"/> suffix if not set.
        /// Defaults to True.
        /// </summary>
        /// <value> <c>True</c> if [using universal time]; otherwise, <c>False</c>. </value>
        public bool UsingUniversalTime
        {
            get => this._UsingUniversalTime;

            set {
                this._UsingUniversalTime = value;
                if ( value && !this.TimeFormat.EndsWith( _UniversalTimeSuffix, StringComparison.OrdinalIgnoreCase ) )
                {
                    this.TimeFormat += _UniversalTimeSuffix;
                }
                else if ( !value && this.TimeFormat.EndsWith( _UniversalTimeSuffix, StringComparison.OrdinalIgnoreCase ) )
                {
                    this.TimeFormat = this.TimeFormat.Substring( 0, this.TimeFormat.Length - 1 );
                }
            }
        }

        /// <summary> Gets the time format. </summary>
        /// <value> The time format. </value>
        public string TimeFormat { get; set; }

        /// <summary> Builds the partial log message not including ID, event type and timestamp. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="eventCache"> A <see cref="T:System.Diagnostics.TraceEventCache"></see> object that
        /// contains the current process ID, thread ID, and stack trace
        /// information. </param>
        /// <param name="message">    A message to write. </param>
        /// <returns> The trace message record. </returns>
        private string BuildPartialMessage( TraceEventCache eventCache, string message )
        {
            var builder = new System.Text.StringBuilder();

            if ( (this.TraceOutputOptions & TraceOptions.ProcessId) == TraceOptions.ProcessId )
            {
                _ = builder.AppendFormat( CultureInfo.CurrentCulture, this.ProcessIdIdFormat, eventCache.ProcessId );
                _ = builder.Append( this.Delimiter );
            }

            if ( (this.TraceOutputOptions & TraceOptions.ThreadId) == TraceOptions.ThreadId )
            {
                _ = builder.Append( $"{ eventCache.ThreadId}{this.Delimiter }" );
            }

            if ( (this.TraceOutputOptions & TraceOptions.Timestamp) == TraceOptions.Timestamp )
            {
                _ = builder.Append( $"{eventCache.Timestamp}{this.Delimiter}" );
            }

            _ = builder.Append( message );
            if ( (this.TraceOutputOptions & TraceOptions.LogicalOperationStack) == TraceOptions.LogicalOperationStack )
            {
                _ = builder.Append( $"{this.Delimiter}{StackToString( eventCache.LogicalOperationStack )}" );
            }

            if ( (this.TraceOutputOptions & TraceOptions.Callstack) == TraceOptions.Callstack )
            {
                _ = builder.Append( $"{this.Delimiter}{eventCache.Callstack}" );
            }

            return builder.ToString();
        }

        /// <summary>   Builds the full log message including ID and timestamp. </summary>
        /// <remarks>   David, 2020-09-17. </remarks>
        /// <param name="eventCache">       A <see cref="T:System.Diagnostics.TraceEventCache"></see>
        ///                                 object that contains the current process ID, thread ID, and
        ///                                 stack trace information. </param>
        /// <param name="source">           A name of the trace source that invoked this method. </param>
        /// <param name="eventType">        One of the
        ///                                 <see cref="T:System.Diagnostics.TraceEventType"></see>
        ///                                 enumeration values. </param>
        /// <param name="id">               A numeric identifier for the event. </param>
        /// <param name="message">          A message to write. </param>
        /// <param name="prependEventInfo"> True to prepend event information. </param>
        /// <returns>   The trace message record. </returns>
        private string BuildMessage( TraceEventCache eventCache, string source, TraceEventType eventType, int id, string message, bool prependEventInfo )
        {
            var builder = new System.Text.StringBuilder();

            if ( prependEventInfo )
            {
                _ = builder.Append( $"{eventType.ToString().Substring( 0, 2 )}{this.Delimiter}" );
                if ( id == 0 )
                {
                    id = ( int ) eventType;
                }

                _ = builder.AppendFormat( CultureInfo.CurrentCulture, this.TraceIdFormat, id );
                _ = builder.Append( this.Delimiter );
                if ( this.IncludeHostName )
                {
                    _ = builder.Append( $"{this.HostName}.{source}{this.Delimiter}" );
                }

                if ( (this.TraceOutputOptions & TraceOptions.DateTime) == TraceOptions.DateTime )
                {
                    var logTime = eventCache.DateTime;
                    // event cache log time is expressed as UTC. 
                    // Adding the local time offset, converts the UCT time to local time
                    if ( !this.UsingUniversalTime )
                    {
                        logTime = logTime.Add( DateTimeOffset.Now.Offset );
                    }

                    _ = builder.Append( $"{logTime.ToString( this.TimeFormat, CultureInfo.CurrentCulture )}{this.Delimiter}" );
                }
            }

            _ = builder.Append( this.BuildPartialMessage( eventCache, message ) );

            return builder.ToString();
        }

        /// <summary>
        /// Writes trace information, a message and event information to the output file or stream.
        /// </summary>
        /// <remarks>
        /// The <see cref="Microsoft.VisualBasic.Logging.Log">Visual Basic Log</see> use the
        /// <see cref="Switch">switch</see> to Determine if the event cache should be traced using the
        /// switch <see cref="TraceEventType">event type</see>. The
        /// <see cref="Microsoft.VisualBasic.Logging.FileLogTraceListener">filter</see> is Overridable and can be set as
        /// follows if additional filtering is required; otherwise, it is Nothing:
        /// <code>
        /// ' apply the trace level to the trace listener.
        /// Dim listener As Diagnostics.TraceListener = log.TraceSource.Listeners(DefaultFileLogTraceListener.DefaultTraceListenerName)
        /// If listener IsNot Nothing Then
        /// listener.Filter = New EventTypeFilter(log.TraceSource.Switch.Level)
        /// End If
        /// </code>
        /// 20170511: change trim to trim end to preserve the full blown structure.
        /// </remarks>
        /// <param name="eventCache"> A <see cref="T:System.Diagnostics.TraceEventCache"></see> object that
        /// contains the current process ID, thread ID, and stack trace
        /// information. </param>
        /// <param name="source">     A name of the trace source that invoked this method. </param>
        /// <param name="eventType">  One of the <see cref="T:System.Diagnostics.TraceEventType"></see>
        /// enumeration values. </param>
        /// <param name="id">         A numeric identifier for the event. </param>
        /// <param name="message">    A message to write. </param>
        [HostProtection( SecurityAction.LinkDemand, Synchronization = true )]
        public override void TraceEvent( TraceEventCache eventCache, string source, TraceEventType eventType, int id, string message )
        {
            if ( eventCache is null || string.IsNullOrWhiteSpace( message ) )
            {
                return;
            }

            if ( this.Filter is null || this.Filter.ShouldTrace( eventCache, source, eventType, id, message, null, null, null ) )
            {
                if ( message.Contains( Environment.NewLine ) )
                {
                    foreach ( string line in message.Split( Environment.NewLine.ToCharArray() ) )
                    {
                        this.TraceEvent( eventCache, source, eventType, id, line.TrimEnd() );
                    }

                    return;
                }

                string record = this.BuildMessage( eventCache, source, eventType, id, message, true );
                this.WriteLine( record );
            }
        }

        /// <summary> Name of the host. </summary>
        private string _HostName;

        /// <summary> Gets the name of the host. </summary>
        /// <value> The name of the host. </value>
        private string HostName
        {
            get {
                if ( string.IsNullOrEmpty( this._HostName ) )
                {
                    this._HostName = Environment.MachineName;
                }

                return this._HostName;
            }
        }

        /// <summary> Stacks to string. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="stack"> The stack. </param>
        /// <returns> System.String. </returns>
        private static string StackToString( Stack stack )
        {
            IEnumerator enumerator = null;
            int length = ", ".Length;
            var builder = new System.Text.StringBuilder();
            try
            {
                enumerator = stack.GetEnumerator();
                while ( enumerator.MoveNext() )
                {
                    _ = builder.Append( $"{enumerator.Current}, " );
                }
            }
            finally
            {
                if ( enumerator is IDisposable )
                {
                    (enumerator as IDisposable).Dispose();
                }
            }

            _ = builder.Replace( "\"", "\"\"" );
            if ( builder.Length >= length )
            {
                _ = builder.Remove( builder.Length - length, length );
            }

            return "\"" + builder.ToString() + "\"";
        }

        #endregion

        #region " FOLDER "

        /// <summary> Determines whether the specified folder path is writable. </summary>
        /// <remarks>
        /// Uses a temporary random file name to test if the file can be created. The file is deleted
        /// thereafter.
        /// </remarks>
        /// <param name="path"> The path. </param>
        /// <returns> <c>True</c> if the specified path is writable; otherwise, <c>False</c>. </returns>
        public static bool IsFolderWritable( string path )
        {
            string filePath = string.Empty;
            bool affirmative = false;
            try
            {
                filePath = System.IO.Path.Combine( path, System.IO.Path.GetRandomFileName() );
                using ( var s = System.IO.File.Open( filePath, System.IO.FileMode.OpenOrCreate ) )
                {
                }

                affirmative = true;
            }
            catch
            {
            }
            finally
            {
                // SS reported an exception from this test possibly indicating that Windows allowed writing the file 
                // by failed report deletion. Or else, Windows raised another exception type.
                try
                {
                    if ( System.IO.File.Exists( filePath ) )
                    {
                        System.IO.File.Delete( filePath );
                    }
                }
                catch
                {
                }
            }

            return affirmative;
        }

        /// <summary> Returns the default folder name for tracing. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="defaultFolderTitle"> The default folder title. </param>
        /// <param name="allUsers">           True to select the folder for
        /// <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.AllUsersApplicationData"/>
        /// all users; otherwise, use using current user; otherwise,
        /// use the folder for
        /// <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.CurrentUserApplicationData"/>
        /// </param>
        /// <returns> The default path for the log file. </returns>
        public static string BuildCustomFolder( string defaultFolderTitle, bool allUsers )
        {
            string candidatePath = My.MyProject.Application.Info.DirectoryPath;
            return IsFolderWritable( candidatePath )
                ? System.IO.Path.Combine( candidatePath, @"..\" + defaultFolderTitle )
                : allUsers
                    ? System.IO.Path.Combine( My.MyProject.Computer.FileSystem.SpecialDirectories.AllUsersApplicationData, defaultFolderTitle )
                    : System.IO.Path.Combine( My.MyProject.Computer.FileSystem.SpecialDirectories.CurrentUserApplicationData, defaultFolderTitle );
        }

        #endregion

        #region " FILE SIZE "

        /// <summary> Returns the file size, -2 if path not specified or -1 if file not found. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="path"> The path. </param>
        /// <returns> System.Int64. </returns>
        public static long FileSize( string path )
        {
            if ( string.IsNullOrWhiteSpace( path ) )
            {
                return 0L;
            }

            var info = new System.IO.FileInfo( path );
            return FileSize( info );
        }

        /// <summary> Returns the file size, -2 if path not specified or -1 if file not found. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> System.Int64. </returns>
        public static long FileSize( System.IO.FileInfo value )
        {
            return value is null ? -2 : value.Exists ? value.Length : string.IsNullOrWhiteSpace( value.Name ) ? -2 : -1;
        }

        /// <summary> Gets the file log trace listener. </summary>
        /// <value> The file log trace listener. </value>
        public static Microsoft.VisualBasic.Logging.FileLogTraceListener FileLogTraceListener => My.MyProject.Application.Log.TraceSource.Listeners[DefaultFileLogWriterName] as Microsoft.VisualBasic.Logging.FileLogTraceListener;

        /// <summary> Returns true if log file exists; Otherwise, <c>False</c>. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns> <c>True</c> if okay, <c>False</c> otherwise. </returns>
        public static bool LogFileExists()
        {
            return FileSize( FileLogTraceListener?.FullLogFileName ) > 2L;
        }

        #endregion

    }
}
