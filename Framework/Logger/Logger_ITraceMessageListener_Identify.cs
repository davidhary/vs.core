
using System;
using System.Collections.Generic;
using System.Linq;

namespace isr.Core
{
    public partial class Logger : ITraceMessageListener
    {

        /// <summary> List of dates of the talker identities. </summary>
        private readonly Dictionary<int, DateTimeOffset> _TalkerIdentityDates = new();

        /// <summary> The talker identity messages. </summary>
        private readonly Dictionary<int, TraceMessage> _TalkerIdentityMessages = new();

        /// <summary>
        /// Query if talker with identity as specified in the trace message was identified.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="value"> The value. </param>
        /// <returns> <c>true</c> if talker identified; otherwise <c>false</c> </returns>
        public bool IsTalkerIdentified( TraceMessage value )
        {
            return value is null
                ? throw new ArgumentNullException( nameof( value ) )
                : this._TalkerIdentityDates.ContainsKey( value.Id ) && this._TalkerIdentityDates[value.Id].Date >= DateTimeOffset.Now.Date;
        }

        /// <summary> Identify a talker using the talker project identity. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="value"> The event message. </param>
        public void IdentifyTalker( TraceMessage value )
        {
            if ( value is object )
            {
                if ( this._TalkerIdentityDates.ContainsKey( value.Id ) )
                {
                    this._TalkerIdentityDates[value.Id] = DateTimeOffset.Now;
                }
                else
                {
                    this._TalkerIdentityDates.Add( value.Id, DateTimeOffset.Now );
                }

                if ( this._TalkerIdentityMessages.ContainsKey( value.Id ) )
                {
                    this._TalkerIdentityMessages[value.Id] = value;
                }
                else
                {
                    this._TalkerIdentityMessages.Add( value.Id, value );
                }
            }
        }

        /// <summary> Enumerates talker identity messages in this collection. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns>
        /// An enumerator that allows for each to be used to process talker identity messages in this
        /// collection.
        /// </returns>
        public IList<TraceMessage> TalkerIdentityMessages()
        {
            return this._TalkerIdentityMessages.Values.ToList();
        }

        /// <summary> List of dates of the talker date messages. </summary>
        private readonly Dictionary<int, DateTimeOffset> _TalkerDateMessagesDates = new();

        /// <summary> The talker date messages. </summary>
        private readonly Dictionary<int, IDictionary<string, TraceMessage>> _TalkerDateMessages = new();

        /// <summary> Query if 'value' is talker date message published. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="value"> The event message. </param>
        /// <returns> <c>true</c> if talker date message published; otherwise <c>false</c> </returns>
        public bool IsTalkerDateMessagePublished( TraceMessage value )
        {
            return value is null
                ? throw new ArgumentNullException( nameof( value ) )
                : this._TalkerDateMessagesDates.ContainsKey( value.Id ) && this._TalkerDateMessagesDates[value.Id].Date >= DateTimeOffset.Now.Date && this._TalkerDateMessages[value.Id].ContainsKey( value.Details );
        }

        /// <summary> Adds a date message. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="value"> The event message. </param>
        public void AddDateMessage( TraceMessage value )
        {
            if ( value is object )
            {
                if ( this._TalkerDateMessagesDates.ContainsKey( value.Id ) )
                {
                    this._TalkerDateMessagesDates[value.Id] = DateTimeOffset.Now;
                }
                else
                {
                    this._TalkerDateMessagesDates.Add( value.Id, DateTimeOffset.Now );
                }

                if ( !this._TalkerDateMessages.ContainsKey( value.Id ) )
                {
                    this._TalkerDateMessages.Add( value.Id, new Dictionary<string, TraceMessage>() );
                }

                if ( this._TalkerDateMessages[value.Id].ContainsKey( value.Details ) )
                {
                    this._TalkerDateMessages[value.Id][value.Details] = value;
                }
                else
                {
                    this._TalkerDateMessages[value.Id].Add( value.Details, value );
                }
            }
        }

        /// <summary> Enumerates talker date messages in this collection. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns>
        /// An enumerator that allows for each to be used to process talker date messages in this
        /// collection.
        /// </returns>
        public IList<TraceMessage> TalkerDateMessages()
        {
            var l = new List<TraceMessage>();
            foreach ( KeyValuePair<int, IDictionary<string, TraceMessage>> kvp in this._TalkerDateMessages )
            {
                foreach ( KeyValuePair<string, TraceMessage> item in kvp.Value )
                {
                    l.Add( item.Value );
                }
            }

            return l;
        }
    }
}
