using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using isr.Core.DiagnosticsExtensions;
using isr.Core.ExceptionExtensions;

using Microsoft.VisualBasic;

namespace isr.Core
{

    /// <summary> Extends the <see cref="Microsoft.VisualBasic.Logging.Log">log</see>. </summary>
    /// <remarks>
    /// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-02-16 </para>
    /// </remarks>
    [DebuggerDisplay( "id={UniqueId}" )]
    public partial class Logger : Microsoft.VisualBasic.Logging.Log, IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        private Logger() : this( My.MyLibrary.AssemblyProduct )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="name"> The name. </param>
        private Logger( string name ) : base( name )
        {
            this.ResetKnownStateThis();
        }

        /// <summary> Creates a new Logger. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="name"> The name. </param>
        /// <returns> A Logger. </returns>
        public static Logger Create( string name )
        {
            if ( Instance is null )
            {
                lock ( SyncLocker )
                {
                    Instance = new Logger( name );
                }
            }

            return Instance;
        }

        /// <summary> Resets to known state. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        private void ResetKnownStateThis()
        {
            this.ContentsQueue = new ConcurrentQueue<TraceMessage>();
            // instantiate cancellation token
            this.CancellationTokenSource = new CancellationTokenSource();
            this.CancellationToken = this.CancellationTokenSource.Token;
            this.FileLogWriter = base.TraceSource.DefaultFileLogWriter();

            // a private internal trace level is now used instead of the trace source to facilitate overriding the trace source level.
            this.ApplyTraceLevelThis( TraceEventType.Verbose );
        }

        #region " SINGLETON "

        /// <summary>
        /// The locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        private static readonly object SyncLocker = new();

        /// <summary> Gets the instance. </summary>
        /// <value> The instance. </value>
        public static Logger Instance { get; private set; }

        /// <summary> Returns a new or existing instance of this class. </summary>
        /// <remarks>
        /// Use this property to get an instance of this class. Returns the default instance of this form
        /// allowing to use this form as a singleton for.
        /// </remarks>
        /// <value> <c>A</c> new or existing instance of the class. </value>
        public static Logger NewInstance( string name )
        {
            if ( Instance is null )
            {
                lock ( SyncLocker )
                {
                    Instance = new Logger( name );
                }
            }

            return Instance;
        }

        /// <summary> Returns true if the instance is nothing or was disposed. </summary>
        /// <value> The <c>True</c> if instance or nothing or was disposed. </value>
        public static bool InstanceDisposed => Instance is null || Instance.IsDisposed;

        #endregion

        #region " Disposable Support "

        /// <summary> Gets the disposed indicator. </summary>
        /// <value> The disposed indicator. </value>
        public bool IsDisposed { get; private set; }

        /// <summary>
        /// Releases the unmanaged resources used by the isr.Core.Logger and optionally releases
        /// the managed resources.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="disposing"> True to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.CancellationTokenSource?.Cancel();
                    _ = (this.ActionTask?.Wait( TimeSpan.FromSeconds( 2d ) ));
                    if ( this.CancellationTokenSource is object )
                    {
                        this.CancellationTokenSource.Dispose();
                        this.CancellationTokenSource = null;
                    }

                    if ( this.ActionTask is object )
                    {
                        this.ActionTask.Dispose();
                        this.ActionTask = null;
                    }

                    if ( this.AsyncTask is object )
                    {
                        this.AsyncTask.Dispose();
                        this.AsyncTask = null;
                    }
                }
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {
            this.Dispose( true );
            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called. It gives the base
        /// class the opportunity to finalize. Do not provide destructors in types derived from this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        ~Logger()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal for readability and maintainability.
            this.Dispose( false );
        }

        #endregion

        #endregion

        #region " WRITE LOG ENTRY "

        /// <summary> Writes a message to the application log listeners. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
        /// <param name="details">  The message details. </param>
        /// <returns> Message or empty string. </returns>
        public string WriteLogEntry( TraceEventType severity, string details )
        {
            if ( details is object )
            {
                this.WriteEntry( details, severity );
                return details;
            }

            return string.Empty;
        }

        /// <summary> Writes a message to the application log listeners. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
        /// <param name="id">       The identifier. </param>
        /// <param name="details">  The message details. </param>
        /// <returns> Message or empty string. </returns>
        public string WriteLogEntry( TraceEventType severity, int id, string details )
        {
            if ( details is object )
            {
                this.WriteEntry( details, severity, id );
                return details;
            }

            return string.Empty;
        }

        /// <summary> Writes a message to the application log listeners. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
        /// <param name="format">   The message format. </param>
        /// <param name="args">     Specified the message arguments. </param>
        /// <returns> The message details or empty. </returns>
        public string WriteLogEntry( TraceEventType severity, string format, params object[] args )
        {
            if ( format is object )
            {
                _ = this.WriteLogEntry( severity, string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
            }

            return string.Empty;
        }

        /// <summary> Writes a message to the application log listeners. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
        /// <param name="id">       The identifier. </param>
        /// <param name="format">   The message format. </param>
        /// <param name="args">     Specified the message arguments. </param>
        /// <returns> The message details or empty. </returns>
        public string WriteLogEntry( TraceEventType severity, int id, string format, params object[] args )
        {
            if ( format is object )
            {
                _ = this.WriteLogEntry( severity, id, string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
            }

            return string.Empty;
        }

        /// <summary> Writes a message to the application log listeners. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
        /// <param name="messages"> Message information to me. </param>
        /// <returns> The message details or empty. </returns>
        public string WriteLogEntry( TraceEventType severity, string[] messages )
        {
            return messages is object ? this.WriteLogEntry( severity, string.Join( ",", messages ) ) : string.Empty;
        }

        /// <summary> Writes a message to the application log listeners. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
        /// <param name="id">       The identifier. </param>
        /// <param name="messages"> Message information to me. </param>
        /// <returns> The message details or empty. </returns>
        public string WriteLogEntry( TraceEventType severity, int id, string[] messages )
        {
            return messages is object ? this.WriteLogEntry( severity, id, string.Join( ",", messages ) ) : string.Empty;
        }

        #endregion

        #region " WRITE EXCEPTION DETAILS "

        /// <summary> Writes an exception. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="ex"> The exception. </param>
        public new void WriteException( Exception ex )
        {
            this.WriteException( ex, TraceEventType.Error, "", 0 );
        }

        /// <summary> Writes an exception. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="ex">             The exception. </param>
        /// <param name="severity">       The <see cref="TraceEventType">event type</see> of the trace
        /// data. </param>
        /// <param name="additionalInfo"> Additional information. </param>
        public new void WriteException( Exception ex, TraceEventType severity, string additionalInfo )
        {
            this.WriteException( ex, severity, additionalInfo, 0 );
        }

        /// <summary> Writes an exception. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="ex">             The exception. </param>
        /// <param name="severity">       The <see cref="TraceEventType">event type</see> of the trace
        /// data. </param>
        /// <param name="additionalInfo"> Additional information. </param>
        /// <param name="id">             The identifier. </param>
        public new void WriteException( Exception ex, TraceEventType severity, string additionalInfo, int id )
        {
            if ( ex is object )
            {
                var builder = new System.Text.StringBuilder();
                _ = builder.Append( ex.ToFullBlownString() );
                if ( !string.IsNullOrWhiteSpace( additionalInfo ) )
                {
                    _ = builder.Append( "; " );
                    _ = builder.Append( additionalInfo );
                }

                this.WriteEntry( builder.ToString(), severity, id );
            }
        }

        /// <summary> Writes a message to the application log listeners. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="ex">             The exception. </param>
        /// <param name="severity">       The <see cref="TraceEventType">event type</see> of the trace
        /// data. </param>
        /// <param name="additionalInfo"> Additional information. </param>
        public void WriteExceptionDetails( Exception ex, TraceEventType severity, string additionalInfo )
        {
            if ( ex is object )
            {
                this.WriteException( ex, severity, additionalInfo );
            }
        }

        /// <summary> Writes a message to the application log listeners. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="ex">             The exception. </param>
        /// <param name="severity">       The <see cref="TraceEventType">event type</see> of the trace
        /// data. </param>
        /// <param name="id">             The identifier. </param>
        /// <param name="additionalInfo"> Additional information. </param>
        public void WriteExceptionDetails( Exception ex, TraceEventType severity, int id, string additionalInfo )
        {
            if ( ex is object )
            {
                this.WriteException( ex, severity, additionalInfo, id );
            }
        }

        /// <summary> Writes a message to the application log listeners. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="ex">       The exception. </param>
        /// <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
        /// <param name="format">   The additional information format. </param>
        /// <param name="args">     The additional information arguments. </param>
        public void WriteExceptionDetails( Exception ex, TraceEventType severity, string format, params object[] args )
        {
            this.WriteExceptionDetails( ex, severity, string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
        }

        /// <summary> Writes a message to the application log listeners. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="ex">       The exception. </param>
        /// <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
        /// <param name="id">       The identifier. </param>
        /// <param name="format">   The additional information format. </param>
        /// <param name="args">     The additional information arguments. </param>
        public void WriteExceptionDetails( Exception ex, TraceEventType severity, int id, string format, params object[] args )
        {
            this.WriteExceptionDetails( ex, severity, id, string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
        }

        /// <summary> Writes a message to the application log listeners. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="ex"> The exception. </param>
        public void WriteExceptionDetails( Exception ex )
        {
            this.WriteExceptionDetails( ex, TraceEventType.Error, "" );
        }

        /// <summary> Writes a message to the application log listeners. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="ex"> The exception. </param>
        /// <param name="id"> The identifier. </param>
        public void WriteExceptionDetails( Exception ex, int id )
        {
            this.WriteExceptionDetails( ex, TraceEventType.Error, id, "" );
        }

        #endregion

        #region " WRITE LOG ENTRY -- OVERRIDE TRACE LEVEL "

        /// <summary>
        /// Writes a message to the application log listeners. Overrides the current log level.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
        /// <param name="details">  The message details. </param>
        /// <returns> Message or empty string. </returns>
        public string WriteLogEntryOverride( TraceEventType severity, string details )
        {
            if ( details is object )
            {
                // save the current trace level.
                var lastSourceLevel = this.TraceSource.Switch.Level;
                // set the requested level.
                this.TraceSource.Switch.Level = severity.ToSourceLevel();
                // write the entry.
                this.WriteEntry( details, severity );
                // restore the level.
                this.TraceSource.Switch.Level = lastSourceLevel;
                return details;
            }

            return string.Empty;
        }

        /// <summary>
        /// Writes a message to the application log listeners. Overrides the current log level.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
        /// <param name="id">       The identifier. </param>
        /// <param name="details">  The message details. </param>
        /// <returns> Message or empty string. </returns>
        public string WriteLogEntryOverride( TraceEventType severity, int id, string details )
        {
            if ( details is object )
            {
                // save the current trace level.
                var lastSourceLevel = this.TraceSource.Switch.Level;
                // set the requested level.
                this.TraceSource.Switch.Level = severity.ToSourceLevel();
                // write the entry.
                this.WriteEntry( details, severity, id );
                // restore the level.
                this.TraceSource.Switch.Level = lastSourceLevel;
                return details;
            }

            return string.Empty;
        }

        /// <summary>
        /// Writes a message to the application log listeners. Overrides the current log level.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
        /// <param name="format">   The message details. </param>
        /// <param name="args">     The arguments. </param>
        /// <returns> Message or empty string. </returns>
        public string WriteLogEntryOverride( TraceEventType severity, string format, params object[] args )
        {
            return format is object ? this.WriteLogEntryOverride( severity, string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) ) : string.Empty;
        }

        /// <summary>
        /// Writes a message to the application log listeners. Overrides the current log level.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
        /// <param name="id">       The identifier. </param>
        /// <param name="format">   The message details. </param>
        /// <param name="args">     The arguments. </param>
        /// <returns> Message or empty string. </returns>
        public string WriteLogEntryOverride( TraceEventType severity, int id, string format, params object[] args )
        {
            return format is object ? this.WriteLogEntryOverride( severity, id, string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) ) : string.Empty;
        }

        #endregion

        #region " LOG TRACE MESSAGES "

        /// <summary> Writes a message to the application log listeners. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="message"> The message. </param>
        public void WriteLogEntry( TraceMessage message )
        {
            if ( message is object && !string.IsNullOrWhiteSpace( message.Details ) )
            {
                _ = this.WriteLogEntry( message.EventType, message.Id, message.Details );
            }
        }

        /// <summary> Writes a message to the application log listeners. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="message">        The message. </param>
        /// <param name="additionalInfo"> Information describing the additional. </param>
        public void WriteLogEntry( TraceMessage message, string additionalInfo )
        {
            if ( message is object )
            {
                if ( string.IsNullOrWhiteSpace( additionalInfo ) )
                {
                    this.WriteLogEntry( message );
                }
                else if ( !string.IsNullOrWhiteSpace( message.Details ) )
                {
                    _ = this.WriteLogEntry( message.EventType, message.Id, "{0},{1}", message.Details, additionalInfo );
                }
            }
        }

        #endregion

        #region " FILE  "

        /// <summary> Gets the file log trace listener. </summary>
        /// <value> The file log trace listener. </value>
        public Microsoft.VisualBasic.Logging.FileLogTraceListener FileLogTraceListener => this.FileLogWriter;

        /// <summary> Gets the filename of the full log file. </summary>
        /// <value> The filename of the full log file. </value>
        public string FullLogFileName => this.FileLogTraceListener.FullLogFileName;

        /// <summary> Gets the size of the default file log writer file. </summary>
        /// <value> The size of the file. </value>
        public long FileSize => CustomFileLogTraceListener.FileSize( this.FullLogFileName );

        /// <summary> Checks if the default file log writer file exists. </summary>
        /// <value> <c>True</c> if the log file exists. </value>
        public bool LogFileExists => CustomFileLogTraceListener.FileSize( this.FullLogFileName ) > 2L;

        /// <summary> Opens log file. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns> The Process. </returns>
        public Process OpenLogFile()
        {
            string proc = "explorer.exe";
            string args = $"{ControlChars.Quote}{this.FullLogFileName}{ControlChars.Quote}";
            return Process.Start( proc, args );
        }

        /// <summary> Opens folder location. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns> The Process. </returns>
        public Process OpenFolderLocation()
        {
            string proc = "explorer.exe";
            var fi = new System.IO.FileInfo( this.FullLogFileName );
            string args = $"{ControlChars.Quote}{fi.DirectoryName}{ControlChars.Quote}";
            return Process.Start( proc, args );
        }

        /// <summary>   Flushes all messages to the file. </summary>
        /// <remarks>   David, 2020-09-17. </remarks>
        public void Flush()
        {
            base.TraceSource.Flush();
        }

        #endregion

        #region " REPLACE TRACE LISTENER "

        /// <summary> Replaces the default file log trace listener with a new one. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="logWriter"> The
        /// <see cref="Microsoft.VisualBasic.Logging.FileLogTraceListener">log writer</see>. </param>
        /// <returns>
        /// The <see cref="Microsoft.VisualBasic.Logging.FileLogTraceListener">file log trace listener. </see>
        /// </returns>
        public Microsoft.VisualBasic.Logging.FileLogTraceListener ReplaceDefaultTraceListener( Microsoft.VisualBasic.Logging.FileLogTraceListener logWriter )
        {
            if ( this.TraceSource is object )
            {
                _ = this.TraceSource.ReplaceDefaultTraceListener( logWriter );
                _ = base.TraceSource.ReplaceDefaultTraceListener( logWriter );
            }

            return logWriter;
        }

        /// <summary>
        /// Replaces the default file log trace listener with a new one for the current user.
        /// </summary>
        /// <remarks> The current user application data folder is used. </remarks>
        /// <returns>
        /// The <see cref="Microsoft.VisualBasic.Logging.FileLogTraceListener">file log trace listener. </see>
        /// </returns>
        public Microsoft.VisualBasic.Logging.FileLogTraceListener ReplaceDefaultTraceListener()
        {
            return this.ReplaceDefaultTraceListener( false );
        }

        /// <summary>   Gets or sets the file log writer. </summary>
        /// <value> The file log writer. </value>
        public Microsoft.VisualBasic.Logging.FileLogTraceListener FileLogWriter
        { get; private set; }

        /// <summary> Replaces the default file log trace listener with a new one. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="allUsers"> True to select the folder for
        /// <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.AllUsersApplicationData"/>
        /// all users; otherwise, use using current user; otherwise, use the
        /// folder for
        /// <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.CurrentUserApplicationData"/>
        /// </param>
        /// <returns>
        /// The <see cref="Microsoft.VisualBasic.Logging.FileLogTraceListener">file log trace listener. </see>
        /// </returns>
        public Microsoft.VisualBasic.Logging.FileLogTraceListener ReplaceDefaultTraceListener( bool allUsers )
        {
            this.FileLogWriter = new CustomFileLogTraceListener( allUsers );
            _ = this.ReplaceDefaultTraceListener( this.FileLogWriter );
            return this.FileLogWriter;
        }

        #endregion

        #region " ASYNC QUEUED MESSAGES "

        /// <summary> The cancellation token source. </summary>
        /// <value> The cancellation token source. </value>
        private CancellationTokenSource CancellationTokenSource { get; set; }

        /// <summary> The cancellation token. </summary>
        /// <value> The cancellation token. </value>
        private CancellationToken CancellationToken { get; set; }

        /// <summary> Query if this object is cancellation requested. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns> True if cancellation requested, false if not. </returns>
        private bool IsCancellationRequested()
        {
            return this.CancellationToken.IsCancellationRequested;
        }

        /// <summary> Gets or sets a queue of contents. </summary>
        /// <value> A queue of contents. </value>
        protected ConcurrentQueue<TraceMessage> ContentsQueue { get; private set; }

        /// <summary> Gets contents queue. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns> The contents queue. </returns>
        private ConcurrentQueue<TraceMessage> GetContentsQueue()
        {
            return this.ContentsQueue;
        }

        /// <summary> Gets the content. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns> The content. </returns>
        private TraceMessage GetContent()
        {
            _ = this.ContentsQueue.TryDequeue( out TraceMessage result );
            return result;
        }

        /// <summary> Uses the message worker to flush any queued messages. </summary>
        /// <remarks>
        /// If the calling thread is different from the thread that created the TextBox control, this
        /// method creates a SetTextCallback and calls itself asynchronously using the Invoke method.
        /// </remarks>
        public void FlushMessages()
        {
            this.FlushMessagesTask();
        }

        /// <summary>
        /// Flushes the trace source. Flushes all trace listeners. This should flush the file buffered
        /// messages to disk.
        /// </summary>
        /// <remarks>
        /// David, 2020-09-17. Under C#, the file size changes only after Unit tests terminate. As a
        /// result, unit testing does not reflect the flushed messages.
        /// </remarks>
        public void FlushTraceSource()
        {
            this.TraceSource.Flush();
        }

        #endregion

        #region " ASYNC QUEUED MESSAGES: TASK "

        /// <summary> Gets or sets the asynchronous task. </summary>
        /// <value> The asynchronous task. </value>
        protected Task AsyncTask { get; private set; }

        /// <summary> Gets or sets the action task. </summary>
        /// <value> The action task. </value>
        protected Task ActionTask { get; private set; }

        /// <summary> Queries if a task is active. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="task"> The task. </param>
        /// <returns> <c>true</c> if a task is active; otherwise <c>false</c> </returns>
        private static bool IsTaskActive( Task task )
        {
            return task is object && !(task.IsCanceled || task.IsCompleted || task.IsFaulted);
        }

        /// <summary> Query if this object is busy. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns> <c>true</c> if busy; otherwise <c>false</c> </returns>
        private bool IsBusy()
        {
            return !this.IsDisposed && IsTaskActive( this.ActionTask );
        }

        /// <summary> Uses the message worker to flush any queued messages. </summary>
        /// <remarks>
        /// If the calling thread is different from the thread that created the TextBox control, this
        /// method creates a SetTextCallback and calls itself asynchronously using the Invoke method.
        /// </remarks>
        private void FlushMessagesTask()
        {
            if ( !this.IsBusy() )
            {
                this.AsyncTask = this.StartAsyncTask();
            }
        }

        /// <summary> Start Asynchronous task. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns> A Task. </returns>
        private async Task StartAsyncTask()
        {
            this.ActionTask = Task.Run( this.TraceQueue );
            await this.ActionTask;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the flush on trace queue is enabled.
        /// </summary>
        /// <value> True if flush on trace queue enabled, false if not. </value>
        public bool FlushOnTraceQueueEnabled { get; set; } = true;

        /// <summary>
        /// This event handler sets the Text property of the TextBox control. It is called on the thread
        /// that created the TextBox control, so the call is thread-safe.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        private void TraceQueue()
        {
            while ( (this.GetContentsQueue()?.Any()).GetValueOrDefault( false ) && !this.IsCancellationRequested() )
            {
                // the check for tracing is now down when adding the event to the queue.
                // Dim value As TraceMessage = Me.GetContent: If value IsNot Nothing AndAlso Me.ShouldTrace(value.EventType) Then Me.TraceSource.TraceEvent(value)
                this.WriteLogEntry( this.GetContent() );
            }
            if ( this.FlushOnTraceQueueEnabled ) { this.Flush(); }

        }

        #endregion

    }
}

#if false
        public Microsoft.VisualBasic.Logging.FileLogTraceListener ReplaceDefaultTraceListener(bool allUsers)
        {
            Microsoft.VisualBasic.Logging.FileLogTraceListener tempListener = null;
            Microsoft.VisualBasic.Logging.FileLogTraceListener listener = null;
            try
            {
                tempListener = new DefaultFileLogTraceListener(allUsers);
                listener = tempListener;
                _ = this.ReplaceDefaultTraceListener( listener );
            }
            finally
            {
                if (tempListener is object)
                {
                    tempListener.Dispose();
                }
            }

            return listener;
        }

                public Microsoft.VisualBasic.Logging.FileLogTraceListener FileLogTraceListener => this.TraceSource.DefaultFileLogWriter();

        /// <summary> The default file log trace listener name. </summary>
        public const string DefaultFileLogTraceListenerName = "FileLog";

        /// <summary> Writes an entry. </summary>
        /// <remarks> Overloads the underlying method. </remarks>
        /// <param name="message">  The message details. </param>
        /// <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
        public new void WriteEntry1(string message, TraceEventType severity)
        {
            _ = this.WriteLogEntry( severity, message );
        }

        /// <summary> Writes an entry. </summary>
        /// <remarks> Overloads the underlying method. </remarks>
        /// <param name="message">  The message details. </param>
        /// <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
        /// <param name="id">       The identifier. </param>
        public new void WriteEntry1(string message, TraceEventType severity, int id)
        {
            _ = this.WriteLogEntry( severity, id, message );
        }


#endif
