using System;
using System.Diagnostics;

using isr.Core.DiagnosticsExtensions;
using isr.Core.ExceptionExtensions;

namespace isr.Core.VisualBasicLoggingExtensions
{

    ///  <summary> Extends the <see cref="Microsoft.VisualBasic.Logging.Log">application log</see> functionality. </summary>
    /// <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2010-11-19, 1.2.3975 </para></remarks>
    public static class VisualBasicLoggingExtensionMethods

    {

        #region " TRACE LEVEL "

        /// <summary> Applies the trace level. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="log">   The <see cref="Microsoft.VisualBasic.Logging.Log">application log</see>. </param>
        /// <param name="value"> The <see cref="TraceEventType">trace level</see> value. </param>
        public static void ApplyTraceLevel( this Microsoft.VisualBasic.Logging.Log log, TraceEventType value )
        {
            if ( log is object && log.TraceSource is object )
            {
                log.TraceSource.ApplyTraceLevel( value );
            }
        }

        /// <summary> Returns the trace level. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="log"> The <see cref="Microsoft.VisualBasic.Logging.Log">application log</see>. </param>
        /// <returns>
        /// The trace level; <see cref="TraceEventType.Information">information</see> if log is nothing.
        /// </returns>
        public static TraceEventType TraceLevel( this Microsoft.VisualBasic.Logging.Log log )
        {
            return log is null || log.TraceSource is null ? TraceEventType.Information : log.TraceSource.TraceLevel();
        }

        /// <summary> Checks if the log should trace the event type. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="log">       The log. </param>
        /// <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
        /// <returns>
        /// <c>True</c>&gt; If the log should trace the  c&gt;TrueThe trace level;
        /// <see cref="TraceEventType.Information">information</see> if log is nothing.
        /// </returns>
        public static bool ShouldTrace( this Microsoft.VisualBasic.Logging.Log log, TraceEventType eventType )
        {
            return eventType <= log.TraceLevel();
        }

        #endregion

        #region " WRITE LOG ENTRY "

        /// <summary> Writes a message to the application log listeners. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="log">      The <see cref="Microsoft.VisualBasic.Logging.Log">application log</see>. </param>
        /// <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
        /// <param name="details">  The message details. </param>
        /// <returns> Message or empty string. </returns>
        public static string WriteLogEntry( this Microsoft.VisualBasic.Logging.Log log, TraceEventType severity, string details )
        {
            if ( details is object )
            {
                if ( log is object )
                {
                    log.WriteEntry( details, severity );
                }

                return details;
            }

            return string.Empty;
        }

        /// <summary> Writes a message to the application log listeners. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="log">      The <see cref="Microsoft.VisualBasic.Logging.Log">application log</see>. </param>
        /// <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
        /// <param name="id">       The identifier. </param>
        /// <param name="details">  The message details. </param>
        /// <returns> Message or empty string. </returns>
        public static string WriteLogEntry( this Microsoft.VisualBasic.Logging.Log log, TraceEventType severity, int id, string details )
        {
            if ( details is object )
            {
                if ( log is object )
                {
                    log.WriteEntry( details, severity, id );
                }

                return details;
            }

            return string.Empty;
        }

        /// <summary> Writes a message to the application log listeners. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="log">      The <see cref="Microsoft.VisualBasic.Logging.Log">application log</see>. </param>
        /// <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
        /// <param name="format">   The message format. </param>
        /// <param name="args">     Specified the message arguments. </param>
        /// <returns> The message details or empty. </returns>
        public static string WriteLogEntry( this Microsoft.VisualBasic.Logging.Log log, TraceEventType severity, string format, params object[] args )
        {
            if ( format is object )
            {
                _ = log.WriteLogEntry( severity, string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
            }

            return string.Empty;
        }

        /// <summary> Writes a message to the application log listeners. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="log">      The <see cref="Microsoft.VisualBasic.Logging.Log">application log</see>. </param>
        /// <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
        /// <param name="id">       The identifier. </param>
        /// <param name="format">   The message format. </param>
        /// <param name="args">     Specified the message arguments. </param>
        /// <returns> The message details or empty. </returns>
        public static string WriteLogEntry( this Microsoft.VisualBasic.Logging.Log log, TraceEventType severity, int id, string format, params object[] args )
        {
            if ( format is object )
            {
                _ = log.WriteLogEntry( severity, id, string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
            }

            return string.Empty;
        }

        /// <summary> Writes a message to the application log listeners. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="log">      The <see cref="Microsoft.VisualBasic.Logging.Log">application log</see>. </param>
        /// <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
        /// <param name="messages"> Message information to log. </param>
        /// <returns> The message details or empty. </returns>
        public static string WriteLogEntry( this Microsoft.VisualBasic.Logging.Log log, TraceEventType severity, string[] messages )
        {
            return messages is object ? log.WriteLogEntry( severity, string.Join( ",", messages ) ) : string.Empty;
        }

        /// <summary> Writes a message to the application log listeners. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="log">      The <see cref="Microsoft.VisualBasic.Logging.Log">application log</see>. </param>
        /// <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
        /// <param name="id">       The identifier. </param>
        /// <param name="messages"> Message information to log. </param>
        /// <returns> The message details or empty. </returns>
        public static string WriteLogEntry( this Microsoft.VisualBasic.Logging.Log log, TraceEventType severity, int id, string[] messages )
        {
            return messages is object ? log.WriteLogEntry( severity, id, string.Join( ",", messages ) ) : string.Empty;
        }

        #endregion

        #region " WRITE EXCEPTION DETAILS "

        /// <summary> Writes a message to the application log listeners. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="log">            The <see cref="Microsoft.VisualBasic.Logging.Log">application log</see>. </param>
        /// <param name="ex">             The exception. </param>
        /// <param name="severity">       The <see cref="TraceEventType">event type</see> of the trace
        /// data. </param>
        /// <param name="additionalInfo"> Additional information. </param>
        public static void WriteExceptionDetails( this Microsoft.VisualBasic.Logging.Log log, Exception ex, TraceEventType severity, string additionalInfo )
        {
            if ( ex is object && log is object )
            {
                log.WriteExceptionDetails( ex, severity, 0, additionalInfo );
            }
        }

        /// <summary> Writes a message to the application log listeners. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="log">            The <see cref="Microsoft.VisualBasic.Logging.Log">application log</see>. </param>
        /// <param name="ex">             The exception. </param>
        /// <param name="severity">       The <see cref="TraceEventType">event type</see> of the trace
        /// data. </param>
        /// <param name="id">             The identifier. </param>
        /// <param name="additionalInfo"> Additional information. </param>
        public static void WriteExceptionDetails( this Microsoft.VisualBasic.Logging.Log log, Exception ex, TraceEventType severity, int id, string additionalInfo )
        {
            if ( ex is object && log is object )
            {
                var builder = new System.Text.StringBuilder();
                _ = builder.Append( ex.ToFullBlownString() );
                if ( !string.IsNullOrWhiteSpace( additionalInfo ) )
                {
                    _ = builder.Append( "; " );
                    _ = builder.Append( additionalInfo );
                }

                log.WriteEntry( builder.ToString(), severity, id );
            }
        }

        /// <summary> Writes a message to the application log listeners. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="log">      The <see cref="Microsoft.VisualBasic.Logging.Log">application log</see>. </param>
        /// <param name="ex">       The exception. </param>
        /// <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
        /// <param name="format">   The additional information format. </param>
        /// <param name="args">     The additional information arguments. </param>
        public static void WriteExceptionDetails( this Microsoft.VisualBasic.Logging.Log log, Exception ex, TraceEventType severity, string format, params object[] args )
        {
            if ( log is object )
            {
                log.WriteExceptionDetails( ex, severity, string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
            }
        }

        /// <summary> Writes a message to the application log listeners. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="log">      The <see cref="Microsoft.VisualBasic.Logging.Log">application log</see>. </param>
        /// <param name="ex">       The exception. </param>
        /// <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
        /// <param name="id">       The identifier. </param>
        /// <param name="format">   The additional information format. </param>
        /// <param name="args">     The additional information arguments. </param>
        public static void WriteExceptionDetails( this Microsoft.VisualBasic.Logging.Log log, Exception ex, TraceEventType severity, int id, string format, params object[] args )
        {
            if ( log is object )
            {
                log.WriteExceptionDetails( ex, severity, id, string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
            }
        }

        /// <summary> Writes a message to the application log listeners. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="log"> The <see cref="Microsoft.VisualBasic.Logging.Log">application log</see>. </param>
        /// <param name="ex">  The exception. </param>
        public static void WriteExceptionDetails( this Microsoft.VisualBasic.Logging.Log log, Exception ex )
        {
            if ( log is object )
            {
                log.WriteExceptionDetails( ex, TraceEventType.Error, "" );
            }
        }

        /// <summary> Writes a message to the application log listeners. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="log"> The <see cref="Microsoft.VisualBasic.Logging.Log">application log</see>. </param>
        /// <param name="ex">  The exception. </param>
        /// <param name="id">  The identifier. </param>
        public static void WriteExceptionDetails( this Microsoft.VisualBasic.Logging.Log log, Exception ex, int id )
        {
            if ( log is object )
            {
                log.WriteExceptionDetails( ex, TraceEventType.Error, id, "" );
            }
        }

        #endregion

        #region " WRITE LOG ENTRY -- OVERRIDE TRACE LEVEL "

        /// <summary>
        /// Writes a message to the application log listeners. Overrides the current log level.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="log">      The <see cref="Microsoft.VisualBasic.Logging.Log">application log</see>. </param>
        /// <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
        /// <param name="details">  The message details. </param>
        /// <returns> Message or empty string. </returns>
        public static string WriteLogEntryOverride( this Microsoft.VisualBasic.Logging.Log log, TraceEventType severity, string details )
        {
            if ( details is object )
            {
                if ( log is object )
                {
                    // save the current trace level.
                    var lastSourceLevel = log.TraceSource.Switch.Level;
                    // set the requested level.
                    log.TraceSource.Switch.Level = severity.ToSourceLevel();
                    // write the entry.
                    log.WriteEntry( details, severity );
                    // restore the level.
                    log.TraceSource.Switch.Level = lastSourceLevel;
                }

                return details;
            }

            return string.Empty;
        }

        /// <summary>
        /// Writes a message to the application log listeners. Overrides the current log level.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="log">      The <see cref="Microsoft.VisualBasic.Logging.Log">application log</see>. </param>
        /// <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
        /// <param name="id">       The identifier. </param>
        /// <param name="details">  The message details. </param>
        /// <returns> Message or empty string. </returns>
        public static string WriteLogEntryOverride( this Microsoft.VisualBasic.Logging.Log log, TraceEventType severity, int id, string details )
        {
            if ( details is object )
            {
                if ( log is object )
                {
                    // save the current trace level.
                    var lastSourceLevel = log.TraceSource.Switch.Level;
                    // set the requested level.
                    log.TraceSource.Switch.Level = severity.ToSourceLevel();
                    // write the entry.
                    log.WriteEntry( details, severity, id );
                    // restore the level.
                    log.TraceSource.Switch.Level = lastSourceLevel;
                }

                return details;
            }

            return string.Empty;
        }

        /// <summary>
        /// Writes a message to the application log listeners. Overrides the current log level.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="log">      The <see cref="Microsoft.VisualBasic.Logging.Log">application log</see>. </param>
        /// <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
        /// <param name="format">   The message details. </param>
        /// <param name="args">     The arguments. </param>
        /// <returns> Message or empty string. </returns>
        public static string WriteLogEntryOverride( this Microsoft.VisualBasic.Logging.Log log, TraceEventType severity, string format, params object[] args )
        {
            return format is object ? log.WriteLogEntryOverride( severity, string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) ) : string.Empty;
        }

        /// <summary>
        /// Writes a message to the application log listeners. Overrides the current log level.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="log">      The <see cref="Microsoft.VisualBasic.Logging.Log">application log</see>. </param>
        /// <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
        /// <param name="id">       The identifier. </param>
        /// <param name="format">   The message details. </param>
        /// <param name="args">     The arguments. </param>
        /// <returns> Message or empty string. </returns>
        public static string WriteLogEntryOverride( this Microsoft.VisualBasic.Logging.Log log, TraceEventType severity, int id, string format, params object[] args )
        {
            return format is object ? log.WriteLogEntryOverride( severity, id, string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) ) : string.Empty;
        }

        #endregion

        #region " LOG TRACE MESSAGES "

        /// <summary> Writes a message to the application log listeners. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="log">     The <see cref="Microsoft.VisualBasic.Logging.Log">application log</see>. </param>
        /// <param name="message"> The message. </param>
        public static void WriteLogEntry( this Microsoft.VisualBasic.Logging.Log log, TraceMessage message )
        {
            if ( log is object && message is object && !string.IsNullOrWhiteSpace( message.Details ) )
            {
                _ = log.WriteLogEntry( message.EventType, message.Id, message.Details );
            }
        }

        /// <summary> Writes a message to the application log listeners. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="log">            The <see cref="Microsoft.VisualBasic.Logging.Log">application log</see>. </param>
        /// <param name="message">        The message. </param>
        /// <param name="additionalInfo"> Information describing the additional. </param>
        public static void WriteLogEntry( this Microsoft.VisualBasic.Logging.Log log, TraceMessage message, string additionalInfo )
        {
            if ( log is object && message is object )
            {
                if ( string.IsNullOrWhiteSpace( additionalInfo ) )
                {
                    log.WriteLogEntry( message );
                }
                else if ( !string.IsNullOrWhiteSpace( message.Details ) )
                {
                    _ = log.WriteLogEntry( message.EventType, message.Id, "{0},{1}", message.Details, additionalInfo );
                }
            }
        }

        #endregion

        #region " FILE  "

        /// <summary> Returns the filename of the default file log trace writer log file. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="log"> The <see cref="Microsoft.VisualBasic.Logging.Log">application log</see>. </param>
        /// <returns> The filename of the log file. </returns>
        public static string DefaultFileLogWriterFilePath( this Microsoft.VisualBasic.Logging.Log log )
        {
            return log is null
                ? string.Empty
                : log.DefaultFileLogWriter is null
                    ? string.Empty
                    : log.DefaultFileLogWriter is object ? log.DefaultFileLogWriter.FullLogFileName : string.Empty;
        }

        /// <summary> Checks if the default file log writer file exists. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="log"> The log. </param>
        /// <returns> <c>True</c> if the log file exists. </returns>
        public static bool DefaultFileLogWriterFileExists( this Microsoft.VisualBasic.Logging.Log log )
        {
            return CustomFileLogTraceListener.FileSize( log.DefaultFileLogWriterFilePath() ) > 2L;
        }

        #endregion

        #region " REPLACE TRACE LISTENER "

        /// <summary> Replaces the default file log trace listener with a new one. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="log">       The <see cref="Microsoft.VisualBasic.Logging.Log">application log</see>. </param>
        /// <param name="logWriter"> The
        /// <see cref="Microsoft.VisualBasic.Logging.FileLogTraceListener">log writer</see>. </param>
        /// <returns>
        /// The <see cref="Microsoft.VisualBasic.Logging.FileLogTraceListener">file log trace listener. </see>
        /// </returns>
        public static Microsoft.VisualBasic.Logging.FileLogTraceListener ReplaceDefaultTraceListener( this Microsoft.VisualBasic.Logging.Log log, Microsoft.VisualBasic.Logging.FileLogTraceListener logWriter )
        {
            if ( log is object && log.TraceSource is object )
            {
                _ = log.TraceSource.ReplaceDefaultTraceListener( logWriter );
            }

            return logWriter;
        }

        /// <summary>
        /// Replaces the default file log trace listener with a new one for the current user.
        /// </summary>
        /// <remarks> The current user application data folder is used. </remarks>
        /// <param name="log"> The <see cref="Microsoft.VisualBasic.Logging.Log">application log</see>. </param>
        /// <returns>
        /// The <see cref="Microsoft.VisualBasic.Logging.FileLogTraceListener">file log trace listener. </see>
        /// </returns>
        public static Microsoft.VisualBasic.Logging.FileLogTraceListener ReplaceDefaultTraceListener( this Microsoft.VisualBasic.Logging.Log log )
        {
            return log.ReplaceDefaultTraceListener( false );
        }

        /// <summary> Replaces the default file log trace listener with a new one. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="log">      The <see cref="Microsoft.VisualBasic.Logging.Log">application log</see>. </param>
        /// <param name="allUsers"> True to select the folder for
        /// <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.AllUsersApplicationData"/>
        /// all users; otherwise, use using current user; otherwise, use the
        /// folder for
        /// <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.CurrentUserApplicationData"/>
        /// </param>
        /// <returns>
        /// The <see cref="Microsoft.VisualBasic.Logging.FileLogTraceListener">file log trace listener. </see>
        /// </returns>
        public static Microsoft.VisualBasic.Logging.FileLogTraceListener ReplaceDefaultTraceListener( this Microsoft.VisualBasic.Logging.Log log, bool allUsers )
        {
            Microsoft.VisualBasic.Logging.FileLogTraceListener tempListener = null;
            Microsoft.VisualBasic.Logging.FileLogTraceListener listener = null;
            try
            {
                tempListener = new isr.Core.CustomFileLogTraceListener( allUsers );
                listener = tempListener;
                if ( log is object )
                {
                    _ = log.ReplaceDefaultTraceListener( listener );
                }
                else
                {
                }
            }
            finally
            {
                if ( tempListener is object )
                {
                    tempListener.Dispose();
                }
            }

            return listener;
        }

        #endregion

    }
}
