using System.Diagnostics;

namespace isr.Core.DiagnosticsExtensions
{
    public static partial class DiagnosticsExtensionMethods
    {

        /// <summary> Applies the trace level. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="switch"> The <see cref="SourceSwitch">Source switch</see>. </param>
        /// <param name="value">  The <see cref="TraceEventType">trace level</see> value. </param>
        public static void ApplyTraceLevel( this SourceSwitch @switch, TraceEventType value )
        {
            if ( @switch is object )
            {
                @switch.Level = value.ToSourceLevel();
            }
        }

        /// <summary> Returns the trace level. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="switch"> The <see cref="SourceSwitch">Source switch</see>. </param>
        /// <returns>
        /// The trace level; <see cref="TraceEventType.Information">information</see> if nothing.
        /// </returns>
        public static TraceEventType TraceLevel( this SourceSwitch @switch )
        {
            return @switch is null ? TraceEventType.Verbose : @switch.Level.ToTraceEventType();
        }

    }
}
