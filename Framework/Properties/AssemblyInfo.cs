using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.Core.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.Core.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.Core.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]

[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( isr.Core.My.MyLibrary.TestAssemblyStrongName )]
