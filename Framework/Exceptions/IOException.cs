using System;

namespace isr.Core
{
    /// <summary> Handles Application Settings Input Output exceptions. </summary>
    /// <remarks> Use This class to handle exceptions when reading or writing settings files.  <para>
    /// requires: <see cref="ExceptionBase"/> </para><para>
    /// (c) 2003 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2014-01-21,Documented. </para></remarks>
    public class IOException : ExceptionBase
    {


        #region " CONSTRUCTION and CLEANUP "

        /// <summary> The default message. </summary>
        private const string _DefaultMessage = "Failed accessing I/O.";

        /// <summary> Initializes a new instance of the <see cref="IOException" /> class. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        public IOException() : this( _DefaultMessage )
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:IOException" /> class with a specified error
        /// message.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="message"> The message that describes the error. </param>
        public IOException( string message ) : base( message )
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:IOException" /> class with a specified error
        /// message and a reference to the inner exception that is the cause of this exception.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="message">        The error message that explains the reason for the exception. </param>
        /// <param name="innerException"> The exception that is the cause of the current exception, or a
        /// null reference (Nothing in Visual Basic) if no inner
        /// exception is specified. </param>
        public IOException( string message, Exception innerException ) : base( message, innerException )
        {
        }

        /// <summary> Initializes a new instance of the <see cref="T:IOException" /> class. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="format"> The format. </param>
        /// <param name="args">   The arguments. </param>
        public IOException( string format, params object[] args ) : base( format, args )
        {
        }

        /// <summary> Initializes a new instance of the <see cref="T:IOException" /> class. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="innerException"> Specifies the InnerException. </param>
        /// <param name="format">         Specifies the exception formatting. </param>
        /// <param name="args">           Specifies the message arguments. </param>
        public IOException( Exception innerException, string format, params object[] args ) : base( innerException, format, args )
        {
        }

        /// <summary> Initializes a new instance of the class with serialized data. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="info">    The <see cref="T:System.Runtime.Serialization.SerializationInfo" />
        /// that holds the serialized object data about the exception being
        /// thrown. </param>
        /// <param name="context"> The <see cref="T:System.Runtime.Serialization.StreamingContext" />
        /// that contains contextual information about the source or destination.
        /// </param>
        protected IOException( System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context ) : base( info, context )
        {
        }

        #endregion

        #region " GET OBJECT DATA "

        /// <summary> Overrides the GetObjectData method to serialize custom values. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="info">    Represents the SerializationInfo of the exception. </param>
        /// <param name="context"> Represents the context information of the exception. </param>
        [System.Security.Permissions.SecurityPermission( System.Security.Permissions.SecurityAction.Demand, SerializationFormatter = true )]
        public override void GetObjectData( System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context )
        {
            base.GetObjectData( info, context );
        }

        #endregion

    }
}
