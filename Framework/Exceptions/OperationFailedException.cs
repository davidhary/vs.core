
using System;

namespace isr.Core
{
    /// <summary> Handles operation failed exceptions. </summary>
    /// <remarks> Use this class to handle exceptions that might be thrown exercising open, close,
    /// hardware access, and other similar operations.  <para>
    /// (c) 2003 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2003-07-29, 1.0.1305 </para></remarks>
    public class OperationFailedException : ExceptionBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> The default message. </summary>
        private const string _DefaultMessage = "Operation failed.";

        /// <summary>
        /// Initializes a new instance of the <see cref="T:OperationFailedException" /> class. Uses the
        /// internal default message.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        public OperationFailedException() : base( _DefaultMessage )
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:OperationFailedException" /> class.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="message"> The message. </param>
        public OperationFailedException( string message ) : base( message )
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:OperationFailedException" /> class.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="message">        The message. </param>
        /// <param name="innerException"> Specifies the exception that was trapped for throwing this
        /// exception. </param>
        public OperationFailedException( string message, Exception innerException ) : base( message, innerException )
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:OperationFailedException" /> class.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="format"> The format. </param>
        /// <param name="args">   The arguments. </param>
        public OperationFailedException( string format, params object[] args ) : base( format, args )
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:OperationFailedException" /> class.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="innerException"> Specifies the InnerException. </param>
        /// <param name="format">         Specifies the exception formatting. </param>
        /// <param name="args">           Specifies the message arguments. </param>
        public OperationFailedException( Exception innerException, string format, params object[] args ) : base( innerException, format, args )
        {
        }

        /// <summary> Initializes a new instance of the class with serialized data. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="info">    The <see cref="T:System.Runtime.Serialization.SerializationInfo" />
        /// that holds the serialized object data about the exception being
        /// thrown. </param>
        /// <param name="context"> The <see cref="T:System.Runtime.Serialization.StreamingContext" />
        /// that contains contextual information about the source or destination.
        /// </param>
        protected OperationFailedException( System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context ) : base( info, context )
        {
        }

        #endregion

        #region " GET OBJECT DATA "

        /// <summary> Overrides the GetObjectData method to serialize custom values. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="info">    Represents the SerializationInfo of the exception. </param>
        /// <param name="context"> Represents the context information of the exception. </param>
        [System.Security.Permissions.SecurityPermission( System.Security.Permissions.SecurityAction.Demand, SerializationFormatter = true )]
        public override void GetObjectData( System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context )
        {
            base.GetObjectData( info, context );
        }

        #endregion

    }
}
