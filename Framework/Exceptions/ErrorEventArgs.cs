using System;

namespace isr.Core
{
    /// <summary>
    /// Summary:
    ///     Provides data for the System.IO.FileSystemWatcher.Error event.
    /// </summary>
    /// <remarks>   David, 2021-05-03. </remarks>
    public class ErrorEventArgs : EventArgs
    {

        /// <summary>   Initializes a new instance of the System.IO.ErrorEventArgs class. </summary>
        /// <remarks>   David, 2021-05-03. </remarks>
        /// <param name="exception">    An System.Exception that represents the error that occurred. </param>
        public ErrorEventArgs( Exception exception )
        {
            this._Exception = exception;
        }


        private readonly Exception _Exception;

        /// <summary>   Gets the System.Exception that represents the error that occurred. </summary>
        /// <remarks>   David, 2021-05-03. </remarks>
        /// <returns>   An System.Exception that represents the error that occurred. </returns>
        public virtual Exception GetException()
        {
            return this._Exception;
        }
    }
}
