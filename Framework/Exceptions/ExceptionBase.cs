using System;

namespace isr.Core
{
    /// <summary> An inheritable exception for use by framework classes and applications. </summary>
    /// <remarks>
    /// Inherits from System.Exception per design rule CA1958 which specifies that "Types do not
    /// extend inheritance vulnerable types" and further explains that "This [Application Exception]
    /// base exception type does not provide any additional value for framework classes.  <para>
    /// (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2014-01-21, x.x.5134. Based on legacy base exception. </para><para>
    /// David, 2005-01-15, 1.0.1841 </para>
    /// </remarks>
    public abstract class ExceptionBase : Exception
    {

        #region " CONSTRUCTION "

        /// <summary> Initializes a new instance of the <see cref="ExceptionBase" /> class. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        protected ExceptionBase() : base()
        {
            this.ObtainEnvironmentInformation();
        }

        /// <summary> Initializes a new instance of the <see cref="ExceptionBase" /> class. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="message"> The message. </param>
        protected ExceptionBase( string message ) : base( message )
        {
            this.ObtainEnvironmentInformation();
        }

        /// <summary> Initializes a new instance of the <see cref="ExceptionBase" /> class. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="message">        The message. </param>
        /// <param name="innerException"> The inner exception. </param>
        protected ExceptionBase( string message, Exception innerException ) : base( message, innerException )
        {
            this.ObtainEnvironmentInformation();
        }

        /// <summary> Initializes a new instance of the <see cref="ExceptionBase" /> class. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="format"> The format. </param>
        /// <param name="args">   The arguments. </param>
        protected ExceptionBase( string format, params object[] args ) : base( string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) )
        {
            this.ObtainEnvironmentInformation();
        }

        /// <summary> Initializes a new instance of the <see cref="ExceptionBase" /> class. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="innerException"> Specifies the InnerException. </param>
        /// <param name="format">         Specifies the exception formatting. </param>
        /// <param name="args">           Specifies the message arguments. </param>
        protected ExceptionBase( Exception innerException, string format, params object[] args ) : base( string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ), innerException )
        {
        }

        #endregion

        #region " SERIALIZATION "

        /// <summary> Initializes a new instance of the class with serialized data. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="info">    The <see cref="T:System.Runtime.Serialization.SerializationInfo" />
        /// that holds the serialized object data about the exception being
        /// thrown. </param>
        /// <param name="context"> The <see cref="T:System.Runtime.Serialization.StreamingContext" />
        /// that contains contextual information about the source or destination.
        /// </param>
        protected ExceptionBase( System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context ) : base( info, context )
        {
            if ( info is null )
            {
                return;
            }

            this.MachineName = info.GetString( nameof( this.MachineName ) );
            this._CreatedDateTime = info.GetDateTime( nameof( this.CreatedDateTime ) );
            this.AppDomainName = info.GetString( nameof( this.AppDomainName ) );
            this.ThreadIdentityName = info.GetString( nameof( this.ThreadIdentityName ) );
            this.WindowsIdentityName = info.GetString( nameof( this.WindowsIdentityName ) );
            this.OSVersion = info.GetString( nameof( this.OSVersion ) );
            this._AdditionalInformation = ( System.Collections.Specialized.NameValueCollection ) info.GetValue( nameof( this.AdditionalInformation ), typeof( System.Collections.Specialized.NameValueCollection ) );
        }

        /// <summary>
        /// Overrides the <see cref="GetObjectData" /> method to serialize custom values.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="info">    The <see cref="System.Runtime.Serialization.SerializationInfo">serialization
        /// information</see>. </param>
        /// <param name="context"> The <see cref="System.Runtime.Serialization.StreamingContext">streaming
        /// context</see> for the exception. </param>
        [System.Security.Permissions.SecurityPermission( System.Security.Permissions.SecurityAction.Demand, SerializationFormatter = true )]
        public override void GetObjectData( System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context )
        {
            if ( info is null )
            {
                return;
            }

            info.AddValue( nameof( this.MachineName ), this.MachineName, typeof( string ) );
            info.AddValue( nameof( this.CreatedDateTime ), this.CreatedDateTime );
            info.AddValue( nameof( this.AppDomainName ), this.AppDomainName, typeof( string ) );
            info.AddValue( nameof( this.ThreadIdentityName ), this.ThreadIdentityName, typeof( string ) );
            info.AddValue( nameof( this.WindowsIdentityName ), this.WindowsIdentityName, typeof( string ) );
            info.AddValue( nameof( this.OSVersion ), this.OSVersion, typeof( string ) );
            info.AddValue( nameof( this.AdditionalInformation ), this.AdditionalInformation, typeof( System.Collections.Specialized.NameValueCollection ) );
            base.GetObjectData( info, context );
        }

        #endregion

        #region " ADDITIONAL INFORMATION "

        /// <summary> Information describing the additional. </summary>
        private System.Collections.Specialized.NameValueCollection _AdditionalInformation;

        /// <summary> Collection allowing additional information to be added to the exception. </summary>
        /// <value> The additional information. </value>
        public System.Collections.Specialized.NameValueCollection AdditionalInformation
        {
            get {
                if ( this._AdditionalInformation is null )
                {
                    this._AdditionalInformation = new System.Collections.Specialized.NameValueCollection();
                }

                return this._AdditionalInformation;
            }
        }

        /// <summary> AppDomain name where the Exception occurred. </summary>
        /// <value> The name of the application domain. </value>
        public string AppDomainName { get; private set; }

        /// <summary> The created date time. </summary>
        private DateTimeOffset _CreatedDateTime = DateTimeOffset.Now;

        /// <summary> Date and Time <see cref="DateTimeOffset"/> the exception was created. </summary>
        /// <value> The created date time. </value>
        public DateTimeOffset CreatedDateTime => this._CreatedDateTime;

        /// <summary> Machine name where the Exception occurred. </summary>
        /// <value> The name of the machine. </value>
        public string MachineName { get; private set; }

        /// <summary> Gets the OS version. </summary>
        /// <value> The OS version. </value>
        public string OSVersion { get; private set; }

        /// <summary> Identity of the executing thread on which the exception was created. </summary>
        /// <value> The name of the thread identity. </value>
        public string ThreadIdentityName { get; private set; }

        /// <summary> Windows identity under which the code was running. </summary>
        /// <value> The name of the windows identity. </value>
        public string WindowsIdentityName { get; private set; }

        /// <summary> Gathers environment information safely. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        private void ObtainEnvironmentInformation()
        {
            const string unknown = "N/A";
            this._CreatedDateTime = DateTimeOffset.Now;
            this.MachineName = Environment.MachineName;
            if ( this.MachineName.Length == 0 )
            {
                this.MachineName = unknown;
            }

            this.ThreadIdentityName = System.Threading.Thread.CurrentPrincipal.Identity.Name;
            if ( this.ThreadIdentityName.Length == 0 )
            {
                this.ThreadIdentityName = unknown;
            }

            this.WindowsIdentityName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            if ( this.WindowsIdentityName.Length == 0 )
            {
                this.WindowsIdentityName = unknown;
            }

            this.AppDomainName = AppDomain.CurrentDomain.FriendlyName;
            if ( this.AppDomainName.Length == 0 )
            {
                this.AppDomainName = unknown;
            }

            this.OSVersion = Environment.OSVersion.ToString();
            if ( this.OSVersion.Length == 0 )
            {
                this.OSVersion = unknown;
            }

            this._AdditionalInformation = new System.Collections.Specialized.NameValueCollection() { { AdditionalInfoItem.MachineName.ToString(), My.MyProject.Computer.Name }, { AdditionalInfoItem.Timestamp.ToString(), DateTimeOffset.Now.ToString( System.Globalization.CultureInfo.CurrentCulture ) }, { AdditionalInfoItem.FullName.ToString(), System.Reflection.Assembly.GetExecutingAssembly().FullName }, { AdditionalInfoItem.AppDomainName.ToString(), AppDomain.CurrentDomain.FriendlyName }, { AdditionalInfoItem.ThreadIdentity.ToString(), My.MyProject.User.Name } }; // Thread.CurrentPrincipal.Identity.Name
            try
            {
                // WMI may not be installed on the computer
                this._AdditionalInformation.Add( AdditionalInfoItem.WindowsIdentity.ToString(), My.MyProject.Computer.Info.OSFullName );
                this._AdditionalInformation.Add( AdditionalInfoItem.OSVersion.ToString(), My.MyProject.Computer.Info.OSVersion );
            }
            catch
            {
            }
        }

        #endregion

        /// <summary> Specifies the contents of the additional information. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        private enum AdditionalInfoItem
        {
            /// <summary>
            /// The none
            /// </summary>
            None,
            /// <summary>
            /// The machine name
            /// </summary>
            MachineName,
            /// <summary>
            /// The timestamp
            /// </summary>
            Timestamp,
            /// <summary>
            /// The full name
            /// </summary>
            FullName,
            /// <summary>
            /// The application domain name
            /// </summary>
            AppDomainName,
            /// <summary>
            /// The thread identity
            /// </summary>
            ThreadIdentity,
            /// <summary>
            /// The windows identity
            /// </summary>
            WindowsIdentity,
            /// <summary>
            /// The OS version
            /// </summary>
            OSVersion
        }
    }
}
