using System;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core
{
    /// <summary> A converter. </summary>
    /// <remarks>
    /// (c) 2016 Cory Charlton.<para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2016-12-12, 3.1.6192. https://github.com/CoryCharlton/CCSWE.Core. </para>
    /// </remarks>
    public sealed class Converter
    {

        /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        private Converter()
        {
        }

        /// <summary> Converts. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> An Object. </returns>
        public static T Convert<T>( object value )
        {
            return ( T ) Convert( value, typeof( T ) );
        }

        /// <summary> Converts. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="value"> The value. </param>
        /// <param name="type">  The type. </param>
        /// <returns> An Object. </returns>
        public static object Convert( object value, Type type )
        {
            if ( type is null )
            {
                throw new ArgumentNullException( nameof( type ) );
            }

            if ( type.IsGenericType && ReferenceEquals( type.GetGenericTypeDefinition(), typeof( object ) ) )
            {
                if ( value is null )
                {
                    return null;
                }

                type = type.GetGenericArguments()[0];
            }
            // Return If(type.IsEnum, If(value Is Nothing, Nothing, If(TypeOf value Is String, System.Enum.Parse(type, CStr(value)), System.Enum.ToObject(type, value))), System.Convert.ChangeType(value, type))
            return type.IsEnum ? value ?? (value is string ? Enum.Parse( type, Conversions.ToString( value ) ) : Enum.ToObject( type, value )) : System.Convert.ChangeType( value, type );
        }

        /// <summary> Convert value. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="input"> The input. </param>
        /// <returns> The value converted. </returns>
        public static T ConvertValue<T>( object input )
        {
            if ( input is null )
            {
                return default;
            }

            var type = typeof( T );
            if ( type.IsGenericType && ReferenceEquals( type.GetGenericTypeDefinition(), typeof( object ) ) )
            {
                type = type.GetGenericArguments()[0];
            }

            return type.IsEnum ? input is string ? ( T ) Enum.Parse( typeof( T ), input.ToString().Trim() ) : ( T ) Enum.ToObject( type, input ) : ( T ) System.Convert.ChangeType( input, type );
        }

        /// <summary> Safe convert. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value">        The value. </param>
        /// <param name="defaultValue"> The default value. </param>
        /// <returns> A T. </returns>
        public static T SafeConvert<T>( object value, T defaultValue )
        {
            try
            {
                return Convert<T>( value );
            }
            catch
            {
                return defaultValue;
            }
        }
    }
}
