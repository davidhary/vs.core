using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;

namespace isr.Core
{
    /// <summary> The process methods. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-08-15 </para>
    /// </remarks>
    public sealed class ProcessMethods
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        private ProcessMethods() : base()
        {
        }

        #endregion

        #region " DESIGN MODE "

        /// <summary>
        /// Gets a value indicating whether the application is running under the IDE in design mode.
        /// </summary>
        /// <value>
        /// <c>True</c> if the application is running under the IDE in design mode; otherwise,
        /// <c>False</c>.
        /// </value>
        public static bool InDesignMode => Debugger.IsAttached;

        /// <summary> List of names of the designer process. </summary>
        private static readonly string[] DesignerProcessNames = new string[] { "xdesproc", "devenv" };

        /// <summary> True to running from visual studio designer? </summary>
        private static bool? _RunningFromVisualStudioDesigner = default;

        /// <summary> <see langword="True"/> if running from visual studio designer. </summary>
        /// <value> The running from visual studio designer. </value>
        public static bool RunningFromVisualStudioDesigner
        {
            get {
                if ( !_RunningFromVisualStudioDesigner.HasValue )
                {
                    using var currentProcess = Process.GetCurrentProcess();
                    _RunningFromVisualStudioDesigner = DesignerProcessNames.Contains( currentProcess.ProcessName.Trim(), StringComparer.OrdinalIgnoreCase );
                }

                return _RunningFromVisualStudioDesigner.Value;
            }
        }

        #endregion

        #region " CHECK 64 Bits "

        /// <summary> Query if 'process' is 64 bit. </summary>
        /// <remarks>
        /// see https://msdn.microsoft.com/en-us/library/windows/desktop/ms684139%28v=vs.85%29.aspx.
        /// </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="process"> The process. </param>
        /// <returns> <c>true</c> if 64 bit; otherwise <c>false</c> </returns>
        public static bool Is64Bit( Process process )
        {
            if ( process is null )
            {
                throw new ArgumentNullException( nameof( process ) );
            }

            if ( !Environment.Is64BitOperatingSystem )
            {
                return false;
            }
            // if this method is not available in your version of .NET, use GetNativeSystemInfo via P/Invoke instead
            return !NativeMethods.IsWow64Process( process.Handle, out bool isWow64 ) ? throw new InvalidOperationException() : !isWow64;
        }

        /// <summary> Query if 'process' is 64 bit. </summary>
        /// <remarks>
        /// see https://msdn.microsoft.com/en-us/library/windows/desktop/ms684139%28v=vs.85%29.aspx.
        /// </remarks>
        /// <returns> <c>true</c> if 64 bit; otherwise <c>false</c> </returns>
        public static bool Is64Bit()
        {
            return Is64Bit( Process.GetCurrentProcess() );
        }

        /// <summary> A native methods. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        private sealed class NativeMethods
        {

            /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
            /// <remarks> David, 2020-09-15. </remarks>
            private NativeMethods()
            {
            }

            /// <summary> Is wow 64 process. </summary>
            /// <remarks> David, 2020-09-15. </remarks>
            /// <param name="process"> [In] a pointer to the process. </param>
            /// <param name="wow64Process"> [Out] True if WOW 64 process. </param>
            /// <returns> True if success </returns>
            [DllImport( "kernel32.dll", SetLastError = true, CallingConvention = CallingConvention.Winapi )]
            internal static extern bool IsWow64Process( [In()] IntPtr process, [MarshalAs( UnmanagedType.Bool )] out bool wow64Process );
        }

        #endregion

    }
}
