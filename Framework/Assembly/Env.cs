using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core
{

    /// <summary> An environment. </summary>
    /// <remarks>
    /// (c) 2019 TONERDO. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-10-16  </para><para>
    /// https://github.com/tonerdo/dotnet-env. </para>
    /// </remarks>
    public sealed class Env
    {

        #region " CONSTRUCTION "

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        private Env() : base()
        {
        }

        /// <summary> The default environment file name. </summary>
        public const string DefaultEnvFileName = ".env";

        /// <summary> Loads the given options. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="lines">   The lines. </param>
        /// <param name="options"> The options to load. </param>
        public static void Load( string[] lines, EnvLoadOptions options )
        {
            if ( options is null )
            {
                throw new ArgumentNullException( nameof( options ) );
            }

            var envFile = Parser.Parse( lines, options.TrimWhiteSpace, options.IsEmbeddedHashComment, options.UnescapeQuotedValues );
            envFile.SetEnvironmentVariables( options.ClobberExistingValues );
        }

        /// <summary> Loads the given options. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="path">    Full pathname of the file. </param>
        /// <param name="options"> The options to load. </param>
        public static void Load( string path, EnvLoadOptions options )
        {
            if ( options is null )
            {
                throw new ArgumentNullException( nameof( options ) );
            }

            if ( !options.RequireEnvFile && !File.Exists( path ) )
            {
                return;
            }

            Load( File.ReadAllLines( path ), options );
        }

        /// <summary> Loads the given options. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="file">    The file. </param>
        /// <param name="options"> The options to load. </param>
        public static void Load( Stream file, EnvLoadOptions options )
        {
            if ( options is null )
            {
                throw new ArgumentNullException( nameof( options ) );
            }

            var lines = new List<string>();
            string currentLine = string.Empty;
            using ( var reader = new StreamReader( file ) )
            {
                while ( currentLine is object )
                {
                    currentLine = reader.ReadLine();
                    if ( currentLine is object )
                    {
                        lines.Add( currentLine );
                    }
                }
            }

            Load( lines.ToArray(), options );
        }

        /// <summary> Loads the given options. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="options"> The options to load. </param>
        public static void Load( EnvLoadOptions options )
        {
            Load( Path.Combine( Directory.GetCurrentDirectory(), DefaultEnvFileName ), options );
        }

        #endregion

        #region " RETRIEVE "

        /// <summary> Retrieves the environment value of the specified key. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="key">          The key. </param>
        /// <param name="defaultValue"> (Optional) The default value. </param>
        /// <returns> An Integer. </returns>
        public static string RetrieveValue( string key, string defaultValue = null )
        {
            return Environment.GetEnvironmentVariable( key ) ?? defaultValue;
        }

        /// <summary> Retrieves the environment value of the specified key. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="key">          The key. </param>
        /// <param name="defaultValue"> (Optional) The default value. </param>
        /// <returns> An Integer. </returns>
        public static bool? RetrieveBoolean( string key, bool? defaultValue = default )
        {
            string keyValue = Environment.GetEnvironmentVariable( key );
            return bool.TryParse( keyValue, out bool parsedValue ) ? parsedValue : defaultValue;
        }

        /// <summary> Retrieves the environment value of the specified key. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="key">          The key. </param>
        /// <param name="defaultValue"> (Optional) The default value. </param>
        /// <returns> An Integer. </returns>
        public static double? RetrieveDouble( string key, double? defaultValue = default )
        {
            string keyValue = Environment.GetEnvironmentVariable( key );
            return double.TryParse( keyValue, out double parsedValue ) ? parsedValue : defaultValue;
        }

        /// <summary> Retrieves the environment value of the specified key. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="key">          The key. </param>
        /// <param name="defaultValue"> (Optional) The default value. </param>
        /// <returns> An Integer. </returns>
        public static int? RetrieveInteger( string key, int? defaultValue = default )
        {
            string keyValue = Environment.GetEnvironmentVariable( key );
            return int.TryParse( keyValue, out int parsedValue ) ? parsedValue : defaultValue;
        }

        #endregion

        #region " Dictionary "

        /// <summary> Dictionary of environment variables. </summary>
        /// <remarks>
        /// (c) 2019 TONERDO. All rights reserved.<para>
        /// Licensed under The MIT License.</para><para>
        /// David, 2019-10-16 </para><para>
        /// https://github.com/tonerdo/dotnet-env. </para>
        /// </remarks>
        private class EnvironmentVariableDictionary : Dictionary<string, string>
        {

            /// <summary> Sets environment variables. </summary>
            /// <remarks> David, 2020-09-15. </remarks>
            /// <param name="clobberExistingVars"> (Optional) True to clobber existing variables. </param>
            public void SetEnvironmentVariables( bool clobberExistingVars = true )
            {
                foreach ( KeyValuePair<string, string> keyValuePair in this )
                {
                    if ( clobberExistingVars || Environment.GetEnvironmentVariable( keyValuePair.Key ) is null )
                    {
                        Environment.SetEnvironmentVariable( keyValuePair.Key, keyValuePair.Value );
                    }
                }
            }
        }

        #endregion

        #region " Parser "

        /// <summary> A parser. </summary>
        /// <remarks>
        /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
        /// Licensed under The MIT License.</para><para>
        /// David, 2019-10-16 </para><para>
        ///  https://github.com/tonerdo/dotnet-env. </para>
        /// </remarks>
        private sealed class Parser
        {

            /// <summary>
            /// Constructor that prevents a default instance of this class from being created.
            /// </summary>
            /// <remarks> David, 2020-09-15. </remarks>
            private Parser() : base()
            {
            }

            /// <summary> The export RegEx. </summary>
            private static readonly Regex ExportRegex = new( @"^\s*export\s+" );

            /// <summary> Query if 'line' is comment. </summary>
            /// <remarks> David, 2020-09-15. </remarks>
            /// <param name="line"> The line. </param>
            /// <returns> <c>true</c> if comment; otherwise <c>false</c> </returns>
            private static bool IsComment( string line )
            {
                return line.Trim().StartsWith( "#", StringComparison.OrdinalIgnoreCase );
            }

            /// <summary> Removes the inline comment described by line. </summary>
            /// <remarks> David, 2020-09-15. </remarks>
            /// <param name="line"> The line. </param>
            /// <returns> A String. </returns>
            private static string RemoveInlineComment( string line )
            {
                int pos = line.IndexOf( '#' );
                return pos >= 0 ? line.Substring( 0, pos ) : line;
            }

            /// <summary> Removes the export keyword described by line. </summary>
            /// <remarks> David, 2020-09-15. </remarks>
            /// <param name="line"> The line. </param>
            /// <returns> A String. </returns>
            private static string RemoveExportKeyword( string line )
            {
                var match = ExportRegex.Match( line );
                return match.Success ? line.Substring( match.Length ) : line;
            }

            /// <summary> Parses. </summary>
            /// <remarks> David, 2020-09-15. </remarks>
            /// <param name="lines">                 The lines. </param>
            /// <param name="trimWhitespace">        (Optional) True to trim whitespace. </param>
            /// <param name="isEmbeddedHashComment"> (Optional) True if is embedded hash comment, false if
            /// not. </param>
            /// <param name="unescapeQuotedValues">  (Optional) True to un-escape quoted values. </param>
            /// <returns> An EnvironmentVariableDictionary. </returns>
            public static EnvironmentVariableDictionary Parse( string[] lines, bool trimWhitespace = true, bool isEmbeddedHashComment = true, bool unescapeQuotedValues = true )
            {
                var vars = new EnvironmentVariableDictionary();
                for ( int i = 0, loopTo = lines.Length - 1; i <= loopTo; i++ )
                {
                    string line = lines[i];

                    // skip comments
                    if ( IsComment( line ) )
                    {
                        continue;
                    }

                    if ( isEmbeddedHashComment )
                    {
                        line = RemoveInlineComment( line );
                    }

                    line = RemoveExportKeyword( line );
                    var keyValuePair = line.Split( new char[] { '=' }, 2 );

                    // skip malformed lines
                    if ( keyValuePair.Length != 2 )
                    {
                        continue;
                    }

                    if ( trimWhitespace )
                    {
                        keyValuePair[0] = keyValuePair[0].Trim();
                        keyValuePair[1] = keyValuePair[1].Trim();
                    }

                    if ( unescapeQuotedValues && IsQuoted( keyValuePair[1] ) )
                    {
                        keyValuePair[1] = Unescape( keyValuePair[1].Substring( 1, keyValuePair[1].Length - 2 ) );
                    }

                    vars.Add( keyValuePair[0], keyValuePair[1] );
                }

                return vars;
            }

            /// <summary> Query if 's' is quoted. </summary>
            /// <remarks> David, 2020-09-15. </remarks>
            /// <param name="s"> The string. </param>
            /// <returns> <c>true</c> if quoted; otherwise <c>false</c> </returns>
            private static bool IsQuoted( string s )
            {
                return s.Length > 1 && (string.IsNullOrEmpty( Conversions.ToString( s[0] ) ) && string.IsNullOrEmpty( Conversions.ToString( s[s.Length - 1] ) ) || s[0] == '\'' && s[s.Length - 1] == '\'');
            }

            /// <summary> Removes escape notations. </summary>
            /// <remarks>
            /// copied from
            /// https://stackoverflow.com/questions/6629020/evaluate-escaped-string/25471811#25471811.
            /// </remarks>
            /// <param name="s"> The string. </param>
            /// <returns> A String. </returns>
            private static string Unescape( string s )
            {
                var sb = new StringBuilder();
                var r = new Regex( @"\\[abfnrtv?""'\\]|\\[0-3]?[0-7]{1,2}|\\u[0-9a-fA-F]{4}|\\U[0-9a-fA-F]{8}|." );
                var mc = r.Matches( s, 0 );
                foreach ( Match m in mc )
                {
                    if ( m.Length == 1 )
                    {
                        _ = sb.Append( m.Value );
                    }
                    else if ( m.Value[1] >= '0' && m.Value[1] <= '7' )
                    {
                        int i = Convert.ToInt32( m.Value.Substring( 1 ), 8 );
                        _ = sb.Append( ( char ) i );
                    }
                    else if ( m.Value[1] == 'u' )
                    {
                        int i = Convert.ToInt32( m.Value.Substring( 2 ), 16 );
                        _ = sb.Append( ( char ) i );
                    }
                    else if ( m.Value[1] == 'U' )
                    {
                        int i = Convert.ToInt32( m.Value.Substring( 2 ), 16 );
                        _ = sb.Append( char.ConvertFromUtf32( i ) );
                    }
                    else
                    {
                        switch ( m.Value[1] )
                        {
                            case 'a':
                                {
                                    _ = sb.Append( @"\a" );
                                    break;
                                }

                            case 'b':
                                {
                                    _ = sb.Append( ControlChars.Back );
                                    break;
                                }

                            case 'f':
                                {
                                    _ = sb.Append( ControlChars.FormFeed );
                                    break;
                                }

                            case 'n':
                                {
                                    _ = sb.Append( ControlChars.Lf );
                                    break;
                                }

                            case 'r':
                                {
                                    _ = sb.Append( ControlChars.Cr );
                                    break;
                                }

                            case 't':
                                {
                                    _ = sb.Append( ControlChars.Tab );
                                    break;
                                }

                            case 'v':
                                {
                                    _ = sb.Append( ControlChars.VerticalTab );
                                    break;
                                }

                            default:
                                {
                                    _ = sb.Append( m.Value[1] );
                                    break;
                                }
                        }
                    }
                }

                return sb.ToString();
            }
        }

        #endregion

    }

    /// <summary> The <see cref="Env"/> load options. </summary>
    /// <remarks>
    /// (c) 2019 TONERDO. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-10-16 </para><para>
    /// https://github.com/tonerdo/dotnet-env. </para>
    /// </remarks>
    public class EnvLoadOptions
    {

        /// <summary> Gets or sets the trim white space. </summary>
        /// <value> The trim white space. </value>
        public bool TrimWhiteSpace { get; private set; }

        /// <summary> Gets or sets the is embedded hash comment. </summary>
        /// <value> The is embedded hash comment. </value>
        public bool IsEmbeddedHashComment { get; private set; }

        /// <summary> Gets or sets the unescape quoted values. </summary>
        /// <value> The unescape quoted values. </value>
        public bool UnescapeQuotedValues { get; private set; }

        /// <summary> Gets or sets the clobber existing values. </summary>
        /// <value> The clobber existing values. </value>
        public bool ClobberExistingValues { get; private set; }

        /// <summary> Gets or sets the require environment file. </summary>
        /// <value> The require environment file. </value>
        public bool RequireEnvFile { get; private set; }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="trimWhiteSpace">        (Optional) True to trim white space. </param>
        /// <param name="isEmbeddedHashComment"> (Optional) True if is embedded hash comment, false if
        /// not. </param>
        /// <param name="unescapeQuotedValues">  (Optional) True to unescape quoted values. </param>
        /// <param name="clobberExistingValues"> (Optional) True to clobber existing values. </param>
        /// <param name="requireEnvFile">        (Optional) True to require environment file. </param>
        public EnvLoadOptions( bool trimWhiteSpace = true, bool isEmbeddedHashComment = true, bool unescapeQuotedValues = true, bool clobberExistingValues = true, bool requireEnvFile = true )
        {
            this.TrimWhiteSpace = trimWhiteSpace;
            this.IsEmbeddedHashComment = isEmbeddedHashComment;
            this.UnescapeQuotedValues = unescapeQuotedValues;
            this.ClobberExistingValues = clobberExistingValues;
            this.RequireEnvFile = requireEnvFile;
        }
    }
}
