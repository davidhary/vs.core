using System;
using System.Diagnostics;

using Microsoft.VisualBasic.ApplicationServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core
{
    /// <summary>   Information about my assembly. </summary>
    /// <remarks>   David, 2020-09-15. 
    /// Requires: <see cref="AssemblyExtensions"/> </remarks>
    public partial class MyAssemblyInfo : AssemblyInfo
    {

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-09-15. </remarks>
        /// <param name="assembly"> The assembly. </param>
        public MyAssemblyInfo( System.Reflection.Assembly assembly ) : base( assembly )
        {
            this.Assembly = assembly;
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-09-15. </remarks>
        /// <param name="value">    The value. </param>
        public MyAssemblyInfo( AssemblyInfo value ) : this( AssemblyExtensions.AssemblyExtensionMethods.GetAssembly( value ) )
        {
        }

        #region " EXTENDED INFORMATION "

        /// <summary> Gets the assembly. </summary>
        /// <value> The assembly. </value>
        public System.Reflection.Assembly Assembly { get; set; }

        /// <summary> Gets the pathname of the application folder. </summary>
        /// <value> The pathname of the application folder. </value>
        public string ApplicationFolder => this.DirectoryPath;

        /// <summary> Returns the application file name (with extension). </summary>
        /// <value> The name of the application. </value>
        public string ApplicationFileName => this.FileInfo.Name;

        /// <summary> Returns the application file name (without extension). </summary>
        /// <value> The name of the application. </value>
        public string ApplicationFileTitle => this.AssemblyName;

        /// <summary> Returns the application Title. </summary>
        /// <value> The Title of the application. </value>
        public string ApplicationTitle => this.Title;

        private System.IO.FileInfo _FileInfo;

        /// <summary> Gets the file info for the assembly file. </summary>
        /// <value> Information describing the file version. </value>
        public System.IO.FileInfo FileInfo
        {
            get {
                if ( this._FileInfo is null )
                {
                    this._FileInfo = new System.IO.FileInfo( this.Location );
                }

                return this._FileInfo;
            }
        }

        private FileVersionInfo _FileVersionInfo;

        /// <summary> Gets information describing the file version. </summary>
        /// <value> Information describing the file version. </value>
        public FileVersionInfo FileVersionInfo
        {
            get {
                if ( this._FileVersionInfo is null )
                {
                    this._FileVersionInfo = AssemblyExtensions.AssemblyExtensionMethods.GetFileVersionInfo( this.Assembly );
                }

                return this._FileVersionInfo;
            }
        }

        /// <summary> Gets the location. </summary>
        /// <value> The location. </value>
        public string Location => this.Assembly.Location;

        /// <summary> Gets the public key. </summary>
        /// <value> The public key. </value>
        public string PublicKey => AssemblyExtensions.AssemblyExtensionMethods.GetPublicKey( this.Assembly );

        /// <summary> Gets the public key token. </summary>
        /// <value> The public key token. </value>
        public string PublicKeyToken => AssemblyExtensions.AssemblyExtensionMethods.GetPublicKeyToken( this.Assembly );

        /// <summary> Gets the product version. </summary>
        /// <returns> The product version. </returns>
        public string ProductVersion( string format )
        {
            return AssemblyExtensions.AssemblyExtensionMethods.ProductVersion( this, format );
        }

        /// <summary> Returns the application product version. </summary>
        /// <returns> The product version. </returns>
        public string ProductVersion( int fieldCount )
        {
            return this.Version.ToString( fieldCount );
        }

        #endregion

        #region " CAPTION "

        /// <summary> Gets the release candidate prefix. </summary>
        /// <value> The release candidate prefix. </value>
        public string ReleaseCandidatePrefix { get; set; } = "RC";

        /// <summary> Gets the alpha version prefix. </summary>
        /// <value> The alpha version prefix. </value>
        public string AlphaVersionPrefix { get; set; } = "Alpha";

        /// <summary> Gets the beta version prefix. </summary>
        /// <value> The beta version prefix. </value>
        public string BetaVersionPrefix { get; set; } = "Beta";

        /// <summary> Gets the gold version prefix. </summary>
        /// <value> The gold version prefix. </value>
        public string GoldVersionPrefix { get; set; } = "Gold";

        /// <summary>   Builds the Application Title caption. </summary>
        /// <remarks>   David, 2020-09-15. </remarks>
        /// <param name="title">    The title. </param>
        /// <param name="subtitle"> The subtitle. </param>
        /// <returns>   System.String. </returns>
        private string BuildCaption( string title, string subtitle )
        {
            var builder = new System.Text.StringBuilder();
            _ = builder.Append( title );
            _ = builder.Append( " " );
            _ = builder.Append( this.ProductVersion( 3 ) );
            if ( this.Version.Major < 1 )
            {
                _ = builder.Append( "." );
                switch ( this.Version.Minor )
                {
                    case 0:
                        {
                            _ = builder.Append( this.AlphaVersionPrefix );
                            break;
                        }

                    case 1:
                        {
                            _ = builder.Append( this.BetaVersionPrefix );
                            break;
                        }

                    case var @case when 2 <= @case && @case <= 8:
                        {
                            _ = builder.Append( $"{this.ReleaseCandidatePrefix}{this.Version.Minor - 1}" );
                            break;
                        }

                    default:
                        {
                            _ = builder.Append( this.GoldVersionPrefix );
                            break;
                        }
                }
            }

            if ( !string.IsNullOrWhiteSpace( subtitle ) )
            {
                _ = builder.Append( ": " );
                _ = builder.Append( subtitle );
            }

            return builder.ToString();
        }

        /// <summary> Builds the application caption using the application title. </summary>
        /// <param name="subtitle"> The subtitle. </param>
        /// <returns> System.String. </returns>
        public string BuildDefaultCaption( string subtitle )
        {
            return this.BuildCaption( this.Title, subtitle );
        }

        /// <summary> Builds the application caption using the application title. </summary>
        /// <param name="subtitle"> The subtitle. </param>
        /// <returns> System.String. </returns>
        public string BuildTitleCaption( string subtitle )
        {
            return this.BuildCaption( this.Title, subtitle );
        }

        /// <summary> Builds the application caption using the application title. </summary>
        /// <param name="subtitle"> The subtitle. </param>
        /// <returns> System.String. </returns>
        public string BuildDescriptionCaption( string subtitle )
        {
            return this.BuildCaption( this.Description, subtitle );
        }

        #endregion

        #region " PRODUCT PROCESS CAPTION "

        /// <summary> Prefix process name to the product name. </summary>
        /// <returns> A String. </returns>
        public string PrefixProcessName()
        {
            string name = this.ProductName;
            string processName = Process.GetCurrentProcess().ProcessName;
            if ( !name.StartsWith( processName, StringComparison.OrdinalIgnoreCase ) )
            {
                name = $"{processName}.{name}";
            }

            return name;
        }

        /// <summary>   Builds time caption using <see cref="DateTimeOffset"/> showing local time and offset. </summary>
        /// <remarks>
        /// <list type="bullet">Use the following format options: <item>
        /// u - UTC - 2019-09-10 19:27:04Z</item><item>
        /// r - GMT - Tue, 10 May 2019 19:26:42 GMT</item><item>
        /// o - ISO - 2019-09-10T12:12:29.7552627-07:00</item><item>
        /// s - ISO - 2019-09-10T12:24:47</item><item>
        /// empty   - 2019-09-10 16:57:24 -07:00</item><item>
        /// s + zzz - 2019-09-10T12:24:47-07:00</item></list>
        /// </remarks>
        /// <param name="timeCaptionFormat">    The time caption format. </param>
        /// <param name="kindFormat">           The kind format. </param>
        /// <returns>   A string. </returns>
        public static string BuildLocalTimeCaption( string timeCaptionFormat, string kindFormat )
        {
            string result = string.IsNullOrWhiteSpace( timeCaptionFormat )
                ? $"{DateTimeOffset.Now}"
                : string.IsNullOrWhiteSpace( kindFormat )
                    ? $"{DateTimeOffset.Now.ToString( timeCaptionFormat )}"
                    : $"{DateTimeOffset.Now.ToString( timeCaptionFormat )}{DateTimeOffset.Now.ToString( kindFormat )}";
            return result;
        }

        /// <summary> Builds product time caption. </summary>
        /// <param name="versionElements">   The number of version elements. </param>
        /// <param name="timeCaptionFormat"> The time caption format. </param>
        /// <param name="kindFormat">        The kind format. </param>
        /// <returns> A String. </returns>
        /// <remarks>
        /// <list type="bullet">Use the following format options: <item>
        /// u - UTC - 2019-09-10 19:27:04Z</item><item>
        /// r - GMT - Tue, 10 May 2019 19:26:42 GMT</item><item>
        /// o - ISO - 2019-09-10T12:12:29.7552627-07:00</item><item>
        /// s - ISO - 2019-09-10T12:24:47</item><item>
        /// empty   - 2019-09-10 16:57:24 -07:00</item><item>
        /// s + zzz - 2019-09-10T12:24:47-07:00</item></list>
        /// </remarks>
        public string BuildProductTimeCaption( int versionElements, string timeCaptionFormat, string kindFormat )
        {
            return $"{this.PrefixProcessName()}.r.{this.ProductVersion( versionElements )} {MyAssemblyInfo.BuildLocalTimeCaption( timeCaptionFormat, kindFormat )}";
        }

        /// <summary> Builds product time caption using full version and local time plus kind format. </summary>
        /// <returns> A String. </returns>
        public string BuildProductTimeCaption()
        {
            return this.BuildProductTimeCaption( 4, string.Empty, string.Empty );
        }

        #endregion

        #region " APPLICATION DATA FOLDER "

        /// <summary>
        /// Gets the application data folder Path. This seems to create the folder if it does not exist.
        /// </summary>
        /// <param name="allUsers"> True to select the folder for
        /// <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.AllUsersApplicationData"/>
        /// all users; otherwise, use using current user; otherwise, use the
        /// folder for
        /// <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.CurrentUserApplicationData"/> </param>
        /// <returns> System.String. </returns>
        public static string OpenApplicationDataFolderPath( bool allUsers )
        {
            return allUsers ? My.MyProject.Computer.FileSystem.SpecialDirectories.AllUsersApplicationData : My.MyProject.Computer.FileSystem.SpecialDirectories.CurrentUserApplicationData;
        }

        #endregion

        #region " APPLICATION CONFIGURATION FILE "

        /// <summary>
        /// The default configuration extension
        /// </summary>
        private readonly string _DefaultConfigExtension = ".config";

        /// <summary> Returns the file path of the default application configuration file. The folder is
        /// created if it does not exists. </summary>
        /// <param name="allUsers">           True to select the folder for
        /// <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.AllUsersApplicationData"/>
        /// all users; otherwise, use using current user; otherwise,
        /// use the folder for
        /// <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.CurrentUserApplicationData"/> </param>
        /// <param name="significantVersionElements"> The significant version elements. A value between 1
        /// and 4 indicating the version level to use. For example, with 1, only the major version of the
        /// folder name is used; whereas with 4 all version elements are used. Defaults to 4. </param>
        /// <returns> The configuration file full path. Uses the application folder if this folder is
        /// writable. Otherwise the <see cref="OpenApplicationConfigFolderPath">application data
        /// folder</see> is used. System.String. </returns>
        public string BuildApplicationConfigFilePath( bool allUsers, int significantVersionElements )
        {
            return this.BuildApplicationConfigFilePath( allUsers, this._DefaultConfigExtension, significantVersionElements );
        }

        /// <summary>
        /// Returns the file path of the application configuration file. The folder is created if it does
        /// not exists.
        /// </summary>
        /// <param name="allUsers">                   True to select the folder for
        /// <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.AllUsersApplicationData"/>
        /// all users; otherwise, use using current user;
        /// otherwise, use the folder for
        /// <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.CurrentUserApplicationData"/> </param>
        /// <param name="extension">                  The extension. </param>
        /// <param name="significantVersionElements"> The significant version elements. A value between 1
        /// and 4 indicating the version level to use. For
        /// example, with 1, only the major version of the
        /// folder name is used; whereas with 4 all version
        /// elements are used. Defaults to 4. </param>
        /// <returns>
        /// The configuration file full path. Uses the application folder if this folder is writable.
        /// Otherwise the <see cref="OpenApplicationConfigFolderPath">application data folder</see> is
        /// used. System.String.
        /// </returns>
        public string BuildApplicationConfigFilePath( bool allUsers, string extension, int significantVersionElements )
        {
            return System.IO.Path.Combine( OpenApplicationConfigFolderPath( allUsers, significantVersionElements ), this.FileInfo.Name + extension );
        }

        /// <summary> Opens the application configuration folder path. </summary>
        /// <param name="allUsers">                   True to select the folder for
        /// <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.AllUsersApplicationData"/>
        /// all users; otherwise, use using current user;
        /// otherwise, use the folder for
        /// <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.CurrentUserApplicationData"/> </param>
        /// <param name="significantVersionElements"> The significant version elements. A value between 1
        /// and 4 indicating the version level to use. For
        /// example, with 1, only the major version of the
        /// folder name is used; whereas with 4 all version
        /// elements are used. Defaults to 4. </param>
        /// <returns>
        /// The configuration folder full path. Uses the application folder if this folder is writable.
        /// Otherwise the <see cref="OpenApplicationDataFolderPath(bool, int)">application data folder</see> is
        /// used. System.String.
        /// </returns>
        public static string OpenApplicationConfigFolderPath( bool allUsers, int significantVersionElements )
        {
            string candidatePath = My.MyProject.Application.Info.DirectoryPath;
            if ( !IsFolderWritable( candidatePath ) )
            {
                candidatePath = OpenApplicationDataFolderPath( allUsers, significantVersionElements );
            }

            return candidatePath;
        }

        #endregion

        #region " APPLICATION DATA FOLDER -- CUSTOMIZED "


        /// <summary> Returns the Folder Path with the relevant version information elements replaces with
        /// '0'. Creates the folder if it does not exist. </summary>
        /// <param name="dataFolderPath">             Name of the data path. </param>
        /// <param name="significantVersionElements"> The significant version elements. A value between 1
        /// and 4 indicating the version level to use. For example, with 1, only the major version of the
        /// folder name is used; whereas with 4 all version elements are used. Defaults to 4. </param>
        /// <returns> A folder name with the revision numbers (if there) truncated based on the number of
        /// significant elements. For example, with two significant digits we get: ...major.minor.0.0;
        /// or. </returns>
        public static string OpenDataFolderPath( string dataFolderPath, int significantVersionElements )
        {
            significantVersionElements = significantVersionElements < 1 ? 1 : significantVersionElements > 4 ? 4 : significantVersionElements;
            var di = new System.IO.DirectoryInfo( ReplaceTrailingElements( dataFolderPath, '.', 4 - significantVersionElements, "0" ) );
            if ( !di.Exists )
            {
                di.Create();
            }

            return di.FullName;
        }

        /// <summary>
        /// Gets the application data folder Path. The folder is created if it does not exist.
        /// </summary>
        /// <param name="allUsers">                   True to select the folder for
        /// <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.AllUsersApplicationData"/>
        /// all users; otherwise, use using current user;
        /// otherwise, use the folder for
        /// <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.CurrentUserApplicationData"/> </param>
        /// <param name="significantVersionElements"> The significant version elements. A value between 1
        /// and 4 indicating the version level to use. For
        /// example, with 1, only the major version of the
        /// folder name is used; whereas with 4 all version
        /// elements are used. Defaults to 4. </param>
        /// <returns>
        /// The application data folder for the product. With All Users:<para>
        /// </para><para>
        /// In Windows XP: .\ Application Data\&lt;company&gt;\&lt;product name&gt;\major.minor.0.0;</para><para>
        /// or In Windows 7 or 8: C:\ProgramData\&lt;company&gt;\&lt;product name&gt;\major.minor.0.0</para><para>
        /// For the Current User:</para><para>
        /// In Windows XP: .\&lt;user&gt;\documents and settings\AppData\&lt;company&gt;\&lt;product name&gt;\major.minor.0.0; </para><para>
        /// or In Windows 7 or 8: C:\Users\&lt;user&gt;\AppData\Roaming\\&lt;company&gt;\&lt;product name&gt;\major.minor.0.0.</para><para>
        /// The version information is based on the Assembly File Version.</para>
        /// </returns>
        public static string OpenApplicationDataFolderPath( bool allUsers, int significantVersionElements )
        {
            return OpenDataFolderPath( OpenApplicationDataFolderPath( allUsers ), significantVersionElements );
        }

        #endregion

        #region " HELPER FUNCTIONS "

        /// <summary> Determines whether the specified folder path is writable. </summary>
        /// <remarks> Uses a temporary random file name to test if the file can be created. The file is
        /// deleted thereafter. </remarks>
        /// <param name="path"> The path. </param>
        /// <returns> <c>True</c> if the specified path is writable; otherwise, <c>False</c>. </returns>
        public static bool IsFolderWritable( string path )
        {
            string filePath = string.Empty;
            bool affirmative = false;
            try
            {
                filePath = System.IO.Path.Combine( path, System.IO.Path.GetRandomFileName() );
                using ( var s = System.IO.File.Open( filePath, System.IO.FileMode.OpenOrCreate ) )
                {
                }

                affirmative = true;
            }
            catch
            {
            }
            finally
            {
                // SS reported an exception from this test possibly indicating that Windows allowed writing the file 
                // by failed report deletion. Or else, Windows raised another exception type.
                try
                {
                    if ( System.IO.File.Exists( filePath ) )
                    {
                        System.IO.File.Delete( filePath );
                    }
                }
                catch
                {
                }
            }

            return affirmative;
        }

        /// <summary> Drops the trailing elements. </summary>
        /// <param name="value">     The value. </param>
        /// <param name="delimiter"> The delimiter. </param>
        /// <param name="count">     The count. </param>
        /// <returns> System.String. </returns>
        public static string DropTrailingElements( string value, char delimiter, int count )
        {
            if ( string.IsNullOrWhiteSpace( value ) )
            {
                return value;
            }
            else if ( string.IsNullOrWhiteSpace( Conversions.ToString( delimiter ) ) )
            {
                return value;
            }
            else
            {
                while ( count > 0 )
                {
                    count -= 1;
                    int i = value.LastIndexOf( delimiter );
                    if ( i > 0 )
                    {
                        value = value.Substring( 0, i );
                    }
                    else
                    {
                        count = 0;
                    }
                }

                return value;
            }
        }

        /// <summary> Drops the trailing elements. </summary>
        /// <param name="value">     The value. </param>
        /// <param name="delimiter"> The delimiter. </param>
        /// <param name="count">     The count. </param>
        /// <param name="replace">   The replace. </param>
        /// <returns> System.String. </returns>
        public static string ReplaceTrailingElements( string value, char delimiter, int count, string replace )
        {
            if ( string.IsNullOrWhiteSpace( value ) )
            {
                return value;
            }
            else if ( string.IsNullOrWhiteSpace( Conversions.ToString( delimiter ) ) )
            {
                return value;
            }
            else
            {
                int replaceCount = 0;
                while ( count > 0 )
                {
                    count -= 1;
                    int i = value.LastIndexOf( delimiter );
                    if ( i > 0 )
                    {
                        replaceCount += 1;
                        value = value.Substring( 0, i );
                    }
                    else
                    {
                        count = 0;
                    }
                }

                var builder = new System.Text.StringBuilder( value );
                if ( replaceCount > 0 )
                {
                    for ( int i = 1, loopTo = replaceCount; i <= loopTo; i++ )
                    {
                        _ = builder.Append( delimiter );
                        _ = builder.Append( replace );
                    }
                }

                return builder.ToString();
            }
        }

        #endregion

        #region " TIME ZONE INFO "

        private static string _LocalTimeZoneId;

        /// <summary> Gets the identifier of the local Time zone. </summary>
        /// <value> The identifier of the local Time zone. </value>
        public static string LocalTimeZoneId
        {
            get {
                if ( string.IsNullOrWhiteSpace( _LocalTimeZoneId ) )
                {
                    _LocalTimeZoneId = TimeZoneInfo.Local.Id;
                }

                return _LocalTimeZoneId;
            }
        }

        private static TimeZoneInfo _LocalTimeZoneInfo;

        /// <summary> Gets information describing the local Time zone. </summary>
        /// <value> Information describing the local Time zone. </value>
        public static TimeZoneInfo LocalTimeZoneInfo
        {
            get {
                if ( _LocalTimeZoneInfo is null )
                {
                    _LocalTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById( LocalTimeZoneId );
                }

                return _LocalTimeZoneInfo;
            }
        }

        #endregion

    }
}
