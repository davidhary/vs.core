using System;
using System.Configuration;

namespace isr.Core
{

    #region " SINGLETON INSTANCE "

    /// <summary>   A properties. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    public static class Properties
    {

        /// <summary>
        /// Returns a singleton instance of the <see cref="AppSettingsReader">App Settings Reader</see>.
        /// </summary>
        /// <value> The application settings reader. </value>
        public static AppSettingsReader AppSettingsReader => AppSettingsReader.Get();
    }

    #endregion

    /// <summary>
    /// Defines a singleton class to provide application settings reading for this project.
    /// </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 10/1252017, 1.0.6538 </para>
    /// </remarks>
    public class AppSettingsReader
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        public AppSettingsReader() : base()
        {
            _ = ConfigurationManager.GetSection( "appSettings" );
        }

        #endregion

        #region " SINGLETON "

        /// <summary> Creates a new default instance of this class. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        public static void NewDefault()
        {
            lock ( SyncLocker )
            {
                _Instance = new AppSettingsReader();
            }
        }

        /// <summary> The locking object to enforce thread safety when creating the singleton instance. </summary>
        private static readonly object SyncLocker = new();

        /// <summary> The singleton instance </summary>
        private static AppSettingsReader _Instance;

        /// <summary> Instantiates the class. </summary>
        /// <remarks> Use this property to instantiate a single instance of this class. This class uses
        /// lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
        /// <returns> A new or existing instance of the class. </returns>
        public static AppSettingsReader Get()
        {
            if ( !Instantiated )
            {
                NewDefault();
            }

            return _Instance;
        }

        /// <summary> Gets True if the singleton instance was instantiated. </summary>
        /// <value> The instantiated. </value>
        public static bool Instantiated
        {
            get {
                lock ( SyncLocker )
                {
                    return _Instance is object;
                }
            }
        }

        #endregion

        #region " READER "

        /// <summary> Reads application setting. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="name"> (Optional) Name of the caller member. </param>
        /// <returns> The application setting. </returns>
        public string AppSettingValue( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return ConfigurationManager.AppSettings[name];
        }

        /// <summary> Reads a boolean. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="name"> (Optional) Name of the caller member. </param>
        /// <returns> A boolean value. </returns>
        public bool AppSettingBoolean( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return Convert.ToBoolean( ConfigurationManager.AppSettings[name] );
        }

        /// <summary> Reads an application setting double value. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="name"> (Optional) Name of the caller member. </param>
        /// <returns> A Double value. </returns>
        public double AppSettingDouble( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return Convert.ToDouble( ConfigurationManager.AppSettings[name] );
        }

        /// <summary> Application setting nullable double. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="name"> (Optional) Name of the caller member. </param>
        /// <returns> A Double? </returns>
        public double? AppSettingNullableDouble( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return ToNullableDouble( ConfigurationManager.AppSettings[name] );
        }

        /// <summary> Application setting int 8. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="name"> (Optional) Name of the caller member. </param>
        /// <returns> A Byte. </returns>
        public byte AppSettingInt8( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return Convert.ToByte( ConfigurationManager.AppSettings[name] );
        }

        /// <summary> Reads an application setting Integer value. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="name"> (Optional) Name of the caller member. </param>
        /// <returns> A Integer value. </returns>
        public int AppSettingInt32( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return Convert.ToInt32( ConfigurationManager.AppSettings[name] );
        }

        /// <summary> Reads an application setting long Integer value. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="name"> (Optional) Name of the caller member. </param>
        /// <returns> A long  Integer value. </returns>
        public long AppSettingInt64( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return Convert.ToInt64( ConfigurationManager.AppSettings[name] );
        }

        /// <summary> Application setting enum. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="name"> (Optional) Name of the caller member. </param>
        /// <returns> An ENUM value. </returns>
        public T AppSettingEnum<T>( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return ( T ) Enum.Parse( typeof( T ), ConfigurationManager.AppSettings[name] );
        }

        #endregion

        #region " CONVERSIONS "

        /// <summary> Converts a value to a nullable double. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> Value as a Double? </returns>
        public static double? ToNullableDouble( string value )
        {
            return string.IsNullOrWhiteSpace( value ) ? new double?() : Convert.ToDouble( value );
        }

        #endregion

    }
}
