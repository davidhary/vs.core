using System;
using System.Management;

using Microsoft.VisualBasic.CompilerServices;
using Microsoft.Win32;

namespace isr.Core
{

    /// <summary> A sealed class designed to provide machine information for the assembly. </summary>
    /// <remarks>
    /// <para>Machine identification (c) 2016 Zeev Goldstein
    /// http://www.codeproject.com/Tips/1125745/Machine-Finger-Print-The-Right-and-Efficient-Way#_comments
    /// </para>. (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-09-08 </para>
    /// </remarks>
    public sealed class MachineInfo
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="MachineInfo" /> class. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        private MachineInfo() : base()
        {
        }

        #endregion

        #region " REGISTERY ACCESS "

        /// <summary> Reads a key from the registry. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="hive">          The hive. </param>
        /// <param name="keyFolderName"> Pathname of the key folder. </param>
        /// <param name="keyName">       Name of the key. </param>
        /// <param name="defaultValue">  The default value. </param>
        /// <returns> The key. </returns>
        public static string ReadRegistry( RegistryHive hive, string keyFolderName, string keyName, string defaultValue )
        {
            var rv = Environment.Is64BitOperatingSystem ? RegistryView.Registry64 : RegistryView.Registry32;
            object readValue = null;
            using ( var regKeyBase = RegistryKey.OpenBaseKey( hive, rv ) )
            {
                using var regKey = regKeyBase.OpenSubKey( keyFolderName, RegistryKeyPermissionCheck.ReadSubTree );
                readValue = regKey.GetValue( keyName, defaultValue );
            }

            if ( readValue is object )
            {
                defaultValue = readValue.ToString();
            }

            return defaultValue;
        }

        #endregion

        #region " MACHINE IDENTIFICATION (c) 2016 Zeev Goldstein "

        /// <summary> Unique identifier for the windows. </summary>
        private static string _WindowsGuid;

        /// <summary> Windows unique identifier. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <returns> A String. </returns>
        public static string WindowsGuid()
        {
            try
            {
                if ( string.IsNullOrEmpty( _WindowsGuid ) )
                {
                    var rv = RegistryView.Registry32;
                    rv = Environment.Is64BitOperatingSystem ? RegistryView.Registry64 : RegistryView.Registry32;
                    object readValue = null;
                    string defaultValue = "defaultValue";
                    using ( var regKeyBase = RegistryKey.OpenBaseKey( RegistryHive.LocalMachine, rv ) )
                    {
                        using var regKey = regKeyBase.OpenSubKey( @"SOFTWARE\Microsoft\Cryptography", RegistryKeyPermissionCheck.ReadSubTree );
                        readValue = regKey.GetValue( "MachineGuid", defaultValue );
                    }

                    if ( readValue is object && readValue.ToString() != "defaultValue" )
                    {
                        _WindowsGuid = readValue.ToString();
                    }
                }
            }
            catch ( Exception )
            {
                _WindowsGuid = string.Empty;
            }

            return _WindowsGuid;
        }

        /// <summary> The machine uuid. </summary>
        private static string _MachineUUID;

        /// <summary> Machine UUID. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <returns> A String. </returns>
        public static string MachineUuid()
        {
            try
            {
                if ( string.IsNullOrEmpty( _MachineUUID ) )
                {
                    using var searcher = new ManagementObjectSearcher( @"root\CIMV2", "SELECT * FROM Win32_ComputerSystemProduct" );
                    foreach ( ManagementObject queryObj in searcher.Get() )
                    {
                        _MachineUUID += Conversions.ToString( queryObj["UUID"] );
                    }
                }
            }
            catch ( ManagementException )
            {
            }

            return _MachineUUID;
        }

        /// <summary> Builds machine unique identifier. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="preSalt">   The pre-salt. </param>
        /// <param name="postSalt">  The post salt. </param>
        /// <param name="algorithm"> The algorithm. </param>
        /// <returns> A String. </returns>
        public static string BuildMachineUniqueId( string preSalt, string postSalt, System.Security.Cryptography.HashAlgorithm algorithm )
        {
            if ( algorithm is null )
            {
                throw new ArgumentNullException( nameof( algorithm ) );
            }

            var hashedUniqueId = new System.Text.StringBuilder();
            string uniqueId = $"{preSalt}{WindowsGuid()}{MachineUuid()}{postSalt}";
            var bytesToHash = System.Text.Encoding.ASCII.GetBytes( uniqueId );
            bytesToHash = algorithm.ComputeHash( bytesToHash );
            foreach ( byte b in bytesToHash )
            {
                _ = hashedUniqueId.Append( b.ToString( "x2" ) );
            }

            return hashedUniqueId.ToString();
        }

        /// <summary> The Machine unique MD5 identifier. </summary>
        private static string _MachineUniqueIdMD5;

        /// <summary> Builds a machine unique identifier using insecure MD5 hash. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <returns> A String. </returns>
        public static string BuildMachineUniqueIdMD5()
        {
            if ( string.IsNullOrWhiteSpace( _MachineUniqueIdMD5 ) )
            {
                using var algorithm = new System.Security.Cryptography.MD5CryptoServiceProvider();
                _MachineUniqueIdMD5 = BuildMachineUniqueId( "20", "16", algorithm );
            }

            return _MachineUniqueIdMD5;
        }

        /// <summary> The first machine unique identifier sha. </summary>
        private static string _MachineUniqueIdSha1;

        /// <summary> Builds a machine unique identifier using SHA1. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <returns> A String. </returns>
        public static string BuildMachineUniqueIdSha1()
        {
            if ( string.IsNullOrWhiteSpace( _MachineUniqueIdSha1 ) )
            {
                using var algorithm = new System.Security.Cryptography.SHA1CryptoServiceProvider();
                _MachineUniqueIdSha1 = BuildMachineUniqueId( "20", "16", algorithm );
            }

            return _MachineUniqueIdSha1;
        }

        #endregion

    }
}
