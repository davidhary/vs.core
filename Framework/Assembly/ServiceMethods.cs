using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.ServiceProcess;

using Microsoft.VisualBasic.CompilerServices;
using Microsoft.Win32;

namespace isr.Core
{
    /// <summary> A service methods. </summary>
    /// <remarks>
    /// (c) 2018 Clifford Nelson. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-06-13 from Clifford Nelson  </para><para>
    /// https://www.codeproject.com/Tips/1248302/Methods-to-Help-Working-with-Services.
    /// </para>
    /// </remarks>
    public sealed class ServiceMethods
    {

        /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        private ServiceMethods() : base()
        {
        }

        /// <summary> Installs the service. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="installServicePath"> The install service path. </param>
        public static void InstallService( string installServicePath )
        {
            InstallService( installServicePath, false );
        }

        /// <summary> Uninstall service. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="installServicePath"> The install service path. </param>
        public static void UninstallService( string installServicePath )
        {
            InstallService( installServicePath, true );
        }

        /// <summary> Gets or sets the filename of the install utility file. </summary>
        /// <value> The filename of the install utility file. </value>
        public static string InstallUtilityFileName { get; set; } = "InstallUtil.exe";

        /// <summary> Builds install command. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="installServicePath"> The install service path. </param>
        /// <param name="uninstall">          True to uninstall. </param>
        /// <returns> A String. </returns>
        public static string BuildInstallCommand( string installServicePath, bool uninstall )
        {
            return $"/{installServicePath} {(uninstall ? "u" : "i")}";
        }

        /// <summary> Installs the service. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="FileNotFoundException">    Thrown when the requested file is not present. </exception>
        /// <exception cref="OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <param name="installServicePath"> The install service path. </param>
        /// <param name="uninstall">          True to uninstall. </param>
        public static void InstallService( string installServicePath, bool uninstall )
        {
            string installUtilPath = Path.Combine( RuntimeEnvironment.GetRuntimeDirectory(), InstallUtilityFileName );
            if ( !File.Exists( installUtilPath ) )
            {
                throw new FileNotFoundException( $"Install Utility path not found: {installUtilPath }" );
            }

            if ( !File.Exists( installServicePath ) )
            {
                throw new FileNotFoundException( $"Install Service path not found: {installServicePath }" );
            }

            var processStartInfo = new ProcessStartInfo( installUtilPath, BuildInstallCommand( installServicePath, uninstall ) ) {
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false
            };
            using var process = new Process {
                StartInfo = processStartInfo
            };
            _ = process.Start();
            process.WaitForExit();
            if ( process.ExitCode == -1 )
            {
                throw new OperationFailedException( process.StandardOutput.ReadToEnd() );
            }
        }

        /// <summary> Queries if a given service exists. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="serviceName"> Name of the service. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool ServiceExists( string serviceName )
        {
            // Avoids exceptions this way
            return ServiceController.GetServices().Any( i => (i.ServiceName ?? "") == (serviceName ?? "") );
        }

        /// <summary> Service start. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <param name="serviceName"> Name of the service. </param>
        public static void ServiceStart( string serviceName )
        {
            using var service = new ServiceController( serviceName );
            try
            {
                if ( service.Status == ServiceControllerStatus.Running )
                {
                    return;
                }

                service.Start();
                service.WaitForStatus( ServiceControllerStatus.Running );
            }
            catch ( Exception ex )
            {
                throw new OperationFailedException( $"Failed to start the Windows Service {serviceName}", ex );
            }
        }

        /// <summary> Service stop. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <param name="serviceName"> Name of the service. </param>
        public static void ServiceStop( string serviceName )
        {
            using var service = new ServiceController( serviceName );
            try
            {
                if ( service.Status == ServiceControllerStatus.Stopped )
                {
                    return;
                }

                service.Stop();
                service.WaitForStatus( ServiceControllerStatus.Stopped );
            }
            catch ( Exception ex )
            {
                throw new OperationFailedException( $"Failed to stop the Windows Service {serviceName}", ex );
            }
        }

        /// <summary> Service refresh. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <param name="serviceName"> Name of the service. </param>
        public static void ServiceRefresh( string serviceName )
        {
            using var service = new ServiceController( serviceName );
            try
            {
                if ( service.Status == ServiceControllerStatus.Running )
                {
                    service.Stop();
                    service.WaitForStatus( ServiceControllerStatus.Stopped );
                }

                service.Start();
                service.WaitForStatus( ServiceControllerStatus.Running );
            }
            catch ( Exception ex )
            {
                throw new OperationFailedException( $"Failed to restart the Windows Service {serviceName}", ex );
            }
        }

        /// <summary> Service path. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <param name="serviceName"> Name of the service. </param>
        /// <returns> A String. </returns>
        public static string ServicePath( string serviceName )
        {
            try
            {
                string registryPath = @"SYSTEM\CurrentControlSet\Services\" + serviceName;
                var key = Registry.LocalMachine.OpenSubKey( registryPath );
                string value = key.GetValue( "ImagePath" ).ToString();
                key.Close();
                return string.IsNullOrEmpty( Conversions.ToString( value[0] ) ) ? value.Substring( 1, value.Length - 2 ) : value;
            }
            catch ( Exception ex )
            {
                throw new OperationFailedException( $"Error in attempting to get Registry key for service {serviceName}", ex );
            }
        }

        /// <summary> Service version. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="serviceName"> Name of the service. </param>
        /// <returns> A FileVersionInfo. </returns>
        public static FileVersionInfo ServiceVersion( string serviceName )
        {
            return FileVersionInfo.GetVersionInfo( ServicePath( serviceName ) );
        }
    }
}
