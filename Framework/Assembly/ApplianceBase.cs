using System;
using System.Diagnostics;
using System.Linq;
using System.Windows.Threading;

using isr.Core.ExceptionExtensions;

namespace isr.Core
{
    /// <summary>   An appliance base. </summary>
    /// <remarks>
    /// David, 2020-09-29. <para>
    /// A base class for providing some standard assembly services including Logging, Timezone, Delay
    /// and Do Event, Assembly identification, and storage of messages that need to be published
    /// (logged or displayed). The base class is intended for use for Libraries or applications.
    /// </para>
    /// </remarks>
    public abstract class ApplianceBase
    {

        #region " CONSTRUCTION "

        /// <summary>   Specialized constructor for use only by derived class. </summary>
        /// <remarks>   David, 2020-09-29. </remarks>
        /// <param name="application">  The application. </param>
        protected ApplianceBase( Microsoft.VisualBasic.ApplicationServices.ApplicationBase application )
        {
            this.Application = application;
        }

        #endregion

        #region " INFORMATION "

        /// <summary> Gets the identifier of the trace source. </summary>
        public abstract int TraceEventId { get; }

        /// <summary> The assembly title. </summary>
        public abstract string AssemblyTitle { get; }

        /// <summary> Information describing the assembly. </summary>
        public abstract string AssemblyDescription { get; }

        /// <summary> The assembly product. </summary>
        public abstract string AssemblyProduct { get; }

        #endregion

        #region " APPLICATION "

        /// <summary>   Gets the application. </summary>
        /// <value> The application. </value>
        protected Microsoft.VisualBasic.ApplicationServices.ApplicationBase Application { get; }

        /// <summary> Information describing my application. </summary>
        private MyAssemblyInfo _MyApplicationInfo;

        /// <summary> Gets an object that provides information about the application's assembly. </summary>
        /// <value> The assembly information object. </value>
        public MyAssemblyInfo Info
        {
            get {
                if ( this._MyApplicationInfo is null )
                {
                    this._MyApplicationInfo = new MyAssemblyInfo( this.Application.Info );
                }

                return this._MyApplicationInfo;
            }
        }

        /// <summary> The current process name. </summary>
        private static string _CurrentProcessName;

        /// <summary> Gets the current process name. </summary>
        /// <value> The name of the current process. </value>
        public static string CurrentProcessName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _CurrentProcessName ) )
                {
                    _CurrentProcessName = Process.GetCurrentProcess().ProcessName;
                }

                return _CurrentProcessName;
            }
        }

        /// <summary> Gets the number of current process threads. </summary>
        /// <value> The number of current process threads. </value>
        public static int CurrentProcessThreadCount => Process.GetCurrentProcess().Threads.Count;

        /// <summary>
        /// Gets a value indicating whether the application is running under the IDE in design mode.
        /// </summary>
        /// <value>
        /// <c>True</c> if the application is running under the IDE in design mode; otherwise,
        /// <c>False</c>.
        /// </value>
        public static bool InDesignMode => Debugger.IsAttached;

        /// <summary> List of names of the designer process. </summary>
        private static readonly string[] DesignerProcessNames = new string[] { "xdesproc", "devenv" };

        /// <summary> True to running from visual studio designer? </summary>
        private static bool? _RunningFromVisualStudioDesigner = default;

        /// <summary> <see langword="True"/> if running from visual studio designer. </summary>
        /// <value> The running from visual studio designer. </value>
        [Obsolete( "After Visual studio 2017, the IDE launches the executable, which is unaware of the Visual Studio host" )]
        public static bool RunningFromVisualStudioDesigner
        {
            get {
                if ( !_RunningFromVisualStudioDesigner.HasValue )
                {
                    using var currentProcess = Process.GetCurrentProcess();
                    _RunningFromVisualStudioDesigner = DesignerProcessNames.Contains( currentProcess.ProcessName.ToLower().Trim() );
                }

                return _RunningFromVisualStudioDesigner.Value;
            }
        }

        #endregion

        #region " DISPATCHER "

        /// <summary>   Lets Windows process all the messages currently in the message queue. </summary>
        /// <remarks>
        /// David, 2020-09-15. Note that the first call to <see cref="ApplianceBase.DoEvents()"/> takes
        /// some extra 90 ms. Requires <see cref="DispatcherExtensions"/> and
        /// <see cref="TimeSpanExtensions"/>
        /// </remarks>
        public static void DoEvents()
        {
            DispatcherExtensions.DispatcherExtensionMethods.DoEvents( Dispatcher.CurrentDispatcher );
        }

        /// <summary>
        /// Delays operations by the given delay time selecting the delay clock which resolution exceeds
        /// 0.2 times the delay time. T.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="delayMilliseconds"> The delay in milliseconds. </param>
        public static void Delay( double delayMilliseconds )
        {
            Delay( TimeSpanExtensions.TimeSpanExtensionMethods.FromMilliseconds( delayMilliseconds ) );
        }

        /// <summary>
        /// Delays operations by the given delay time selecting the delay clock which resolution exceeds
        /// 0.2 times the delay time. sions.DoEvents(Dispatcher)"/> to release messages currently in the
        /// message queue.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="delayTime"> The delay time. </param>
        public static void Delay( TimeSpan delayTime )
        {
            TimeSpanExtensions.TimeSpanExtensionMethods.StartDelayTask( delayTime ).Wait();
        }

        /// <summary>
        /// Delays operations by the given delay time selecting the delay clock which resolution exceeds
        /// <paramref name="resolution"/> times the delay time.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="delayTime">  The delay time. </param>
        /// <param name="resolution"> The resolution. </param>
        public static void Delay( TimeSpan delayTime, double resolution )
        {
            TimeSpanExtensions.TimeSpanExtensionMethods.StartDelayTask( delayTime, resolution ).Wait();
        }

        /// <summary>
        /// Executes the specified delegate on the <see cref="DispatcherPriority.Render"/> priority.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="act"> The act. </param>
        public static void Render( Action act )
        {
            DispatcherExtensions.DispatcherExtensionMethods.Render( Dispatcher.CurrentDispatcher, act );
        }

        #endregion

        #region " wait "

        /// <summary>   Spin waits for the specified time. </summary>
        /// <remarks>   David, 2020-11-14. </remarks>
        /// <param name="delayTime">    The delay time. </param>
        public static void SpinWait( TimeSpan delayTime )
        {
            TimeSpanExtensions.TimeSpanExtensionMethods.SpinWait( delayTime );
        }

        /// <summary>
        /// Lets Windows process all the messages currently in the message queue during a wait.
        /// </summary>
        /// <remarks>   David, 2020-11-14. </remarks>
        /// <param name="delayTime">    The delay time. </param>
        public static void DoEventsWait( TimeSpan delayTime )
        {
            TimeSpanExtensions.TimeSpanExtensionMethods.DoEventsWait( delayTime );
        }

        /// <summary>
        /// Lets Windows process all the messages currently in the message queue during a wait.
        /// </summary>
        /// <remarks>   David, 2020-11-20. </remarks>
        /// <param name="delayTime">    The delay time. </param>
        /// <returns>   A TimeSpan. </returns>
        public static TimeSpan DoEventsWaitElapsed( TimeSpan delayTime )
        {
            Stopwatch sw = Stopwatch.StartNew();
            TimeSpanExtensions.TimeSpanExtensionMethods.DoEventsWait( delayTime );
            return sw.Elapsed;
        }

        /// <summary>
        /// Lets Windows process all the messages currently in the message queue during a wait after each
        /// sleep interval.
        /// </summary>
        /// <remarks>   David, 2020-11-20. </remarks>
        /// <param name="sleepInterval">    The sleep interval. </param>
        /// <param name="timeout">          The maximum wait time. </param>
        /// <returns>   A TimeSpan. </returns>
        public static TimeSpan DoEventsWaitElapsed( TimeSpan sleepInterval, TimeSpan timeout )
        {

            Stopwatch sw = Stopwatch.StartNew();
            while ( sw.Elapsed <= timeout )
            {
                ApplianceBase.DoEvents();
                ApplianceBase.Delay( sleepInterval );
            }
            return sw.Elapsed;

        }

        /// <summary>
        /// Lets Windows process all the messages currently in the message queue during a wait for
        /// a timeout or an affirmative result.
        /// </summary>
        /// <remarks>   David, 2020-11-14. </remarks>
        /// <param name="timeout">      The maximum wait time. </param>
        /// <param name="predicate">    A predicate function returning true upon affirmation of a
        ///                             condition. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public static (bool Success, TimeSpan Elapse) DoEventsWaitUntil( TimeSpan timeout, Func<bool> predicate )
        {
            return ApplianceBase.DoEventsLetElapseUntil( Stopwatch.StartNew(), timeout, predicate );
        }

        /// <summary>
        /// Lets Windows process all the messages currently in the message queue during a wait for
        /// a timeout or an affirmative result.
        /// </summary>
        /// <remarks>   David, 2020-11-14. </remarks>
        /// <param name="stopwatch">    The stopwatch. </param>
        /// <param name="timeout">      The maximum wait time. </param>
        /// <param name="predicate">    A predicate function returning true upon affirmation of a
        ///                             condition. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public static (bool Success, TimeSpan Elapse) DoEventsLetElapseUntil( Stopwatch stopwatch, TimeSpan timeout, Func<bool> predicate )
        {

            while ( stopwatch.Elapsed <= timeout && !predicate() )
            {
                ApplianceBase.DoEvents();
            }
            return (stopwatch.Elapsed < timeout, stopwatch.Elapsed);

        }

        /// <summary>
        /// Lets Windows process all the messages currently in the message queue during a wait for
        /// a timeout or an affirmative result polling every poll interval.
        /// </summary>
        /// <remarks>   David, 2020-11-16. </remarks>
        /// <param name="pollInterval"> The poll interval. </param>
        /// <param name="timeout">      The maximum wait time. </param>
        /// <param name="predicate">    A predicate function returning true upon affirmation of a
        ///                             condition. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public static (bool Success, TimeSpan Elapse) DoEventsWaitUntil( TimeSpan pollInterval, TimeSpan timeout, Func<bool> predicate )
        {
            return ApplianceBase.DoEventsLetElapseUntil( Stopwatch.StartNew(), pollInterval, timeout, predicate );
        }

        /// <summary>
        /// Lets Windows process all the messages currently in the message queue during a wait for
        /// a timeout or an affirmative result polling every poll interval.
        /// </summary>
        /// <remarks>   David, 2020-11-16. </remarks>
        /// <param name="stopwatch">    The stopwatch. </param>
        /// <param name="pollInterval"> The poll interval. </param>
        /// <param name="timeout">      The maximum wait time. </param>
        /// <param name="predicate">    A predicate function returning true upon affirmation of a
        ///                             condition. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public static (bool Success, TimeSpan Elapse) DoEventsLetElapseUntil( Stopwatch stopwatch, TimeSpan pollInterval, TimeSpan timeout, Func<bool> predicate )
        {

            while ( stopwatch.Elapsed <= timeout && !predicate() )
            {
                ApplianceBase.DoEvents();
                ApplianceBase.Delay( pollInterval );
            }
            return (stopwatch.Elapsed < timeout, stopwatch.Elapsed);

        }

        #endregion

        #region " LOG "

        /// <summary> my log. </summary>
        private Logger _Logger;

        /// <summary> Gets the Logger. </summary>
        /// <value> The Logger. </value>
        public Logger Logger
        {
            get {
                if ( this._Logger is null )
                {
                    this.CreateLogger();
                }

                return this._Logger;
            }
        }

        /// <summary>   Gets the application Logger. </summary>
        /// <value> The application Logger. </value>
        public Microsoft.VisualBasic.Logging.Log ApplicationLog
        {
            get {
                if ( this._Logger is null )
                {
                    this.CreateLogger();
                }

                return this.Application.Log;
            }
        }

        /// <summary> Creates the logger. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        private void CreateLogger()
        {
            this._Logger = null;
            try
            {
                this._Logger = Logger.Create( this.Application.Info.ProductName );
                Microsoft.VisualBasic.Logging.FileLogTraceListener listener;
                listener = this.Logger.ReplaceDefaultTraceListener( true );
                if ( !this.Logger.LogFileExists )
                {
                    _ = this.Logger.TraceEventOverride( this.ApplicationInfoTraceMessage() );
                }

                // set the log for the application
                if ( !string.Equals( this.Application.Log.DefaultFileLogWriter.FullLogFileName, listener.FullLogFileName, StringComparison.OrdinalIgnoreCase ) )
                {
                    this.Application.Log.TraceSource.Listeners.Remove( CustomFileLogTraceListener.DefaultFileLogWriterName );
                    _ = this.Application.Log.TraceSource.Listeners.Add( listener );
                    this.Application.Log.TraceSource.Switch.Level = System.Diagnostics.SourceLevels.Verbose;
                }

                // set the trace level.
                this.ApplyTraceLogLevel();
            }
            catch
            {
                if ( this._Logger is object )
                {
                    this._Logger.Dispose();
                }

                this._Logger = null;
                throw;
            }
        }

        /// <summary> Gets the trace level. </summary>
        /// <value> The trace level. </value>
        public TraceEventType TraceLevel { get; set; } = TraceEventType.Warning;

        #endregion

        #region " IDENTITY "

        /// <summary>   Application base type label. </summary>
        /// <remarks>   David, 2020-10-01. </remarks>
        /// <returns>   A string. </returns>
        public string ApplicationBaseTypeLabel()
        {
            return (this.Application is Microsoft.VisualBasic.ApplicationServices.WindowsFormsApplicationBase) ? "Windows Forms Application" :
                   (this.Application is Microsoft.VisualBasic.ApplicationServices.ConsoleApplicationBase) ? "Console Applications" : "Class Library";
        }

        /// <summary> Identifies this assembly. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="talker"> The talker. </param>
        public void Identify( ITraceMessageTalker talker )
        {
            if ( this.Application is Microsoft.VisualBasic.ApplicationServices.WindowsFormsApplicationBase ||
                 this.Application is Microsoft.VisualBasic.ApplicationServices.ConsoleApplicationBase )
            {
                // the top level application gets to identify a new log file
                _ = talker.PublishDateMessage( this.ApplicationInfoTraceMessage() );
                _ = talker.PublishDateMessage( this.LogTraceMessage() );
            }
            talker.IdentifyTalker( this.IdentityTraceMessage() );
        }

        /// <summary> Gets the identity. </summary>
        /// <value> The identity. </value>
        public string Identity => $"{this.AssemblyProduct} ID = {this.TraceEventId:X}";

        /// <summary> Library log trace message. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns> A TraceMessage. </returns>
        public TraceMessage LogTraceMessage()
        {
            return new TraceMessage( TraceEventType.Information, this.TraceEventId, $"Log at;. {this.Logger.FullLogFileName}" );
        }

        /// <summary> Gets a message describing the identity trace. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns> A TraceMessage. </returns>
        public TraceMessage IdentityTraceMessage()
        {
            return this.IdentityTraceMessage( TraceEventType.Information );
        }

        /// <summary> Gets a message describing the identity trace. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <returns> A TraceMessage. </returns>
        public TraceMessage IdentityTraceMessage( TraceEventType eventType )
        {
            return new TraceMessage( eventType, this.TraceEventId, this.Identity );
        }

        /// <summary> Application information trace message. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns> A TraceMessage. </returns>
        public TraceMessage ApplicationInfoTraceMessage()
        {
            return new TraceMessage( TraceEventType.Information, this.TraceEventId,
                                     AssemblyExtensions.AssemblyExtensionMethods.BuildProductTimeCaption( this.Application.Info ) );
        }

        #endregion

        #region " UNPUBLISHED MESSAGES "

        /// <summary> Gets or sets the unpublished identify date. </summary>
        /// <value> The unpublished identify date. </value>
        public DateTimeOffset UnpublishedIdentifyDate { get; set; }

        /// <summary> Gets or sets the unpublished trace messages. </summary>
        /// <value> The unpublished trace messages. </value>
        public TraceMessagesQueue UnpublishedTraceMessages { get; private set; } = new TraceMessagesQueue();

        /// <summary> Logs unpublished message. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="message">   The message. </param>
        /// <returns> A String. </returns>
        public string LogUnpublishedMessage( TraceEventType eventType, string message )
        {
            return this.LogUnpublishedMessage( eventType, this.TraceEventId, message );
        }

        /// <summary> Logs unpublished message. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="format">    Describes the format to use. </param>
        /// <param name="args">      A variable-length parameters list containing arguments. </param>
        /// <returns> A String. </returns>
        public string LogUnpublishedMessage( TraceEventType eventType, string format, params object[] args )
        {
            return this.LogUnpublishedMessage( eventType, this.TraceEventId, format, args );
        }

        /// <summary> Logs unpublished message. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="id">        The identifier. </param>
        /// <param name="format">    Describes the format to use. </param>
        /// <param name="args">      A variable-length parameters list containing arguments. </param>
        /// <returns> A String. </returns>
        public string LogUnpublishedMessage( TraceEventType eventType, int id, string format, params object[] args )
        {
            return this.LogUnpublishedMessage( new TraceMessage( eventType, id, format, args ) );
        }

        /// <summary> Logs unpublished message. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="message"> The message. </param>
        /// <returns> A String. </returns>
        public string LogUnpublishedMessage( TraceMessage message )
        {
            if ( message is null )
            {
                return string.Empty;
            }
            else
            {
                if ( DateTimeOffset.Now.Date > this.UnpublishedIdentifyDate )
                {
                    _ = this.Logger.TraceEventOverride( this.ApplicationInfoTraceMessage() );
                    _ = this.Logger.TraceEventOverride( this.LogTraceMessage() );
                    _ = this.Logger.TraceEventOverride( this.IdentityTraceMessage() );
                    this.UnpublishedIdentifyDate = DateTimeOffset.Now.Date;
                }

                this.UnpublishedTraceMessages.Enqueue( message );
                _ = this.Logger.TraceEvent( message );
                return message.Details;
            }
        }

        #endregion

        #region " OVERRIDABLES "

        /// <summary> Logs unpublished exception. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="activity">  The activity. </param>
        /// <param name="exception"> The exception. </param>
        public virtual void LogUnpublishedException( string activity, Exception exception )
        {
            _ = this.LogUnpublishedMessage( new TraceMessage( TraceEventType.Error, this.TraceEventId, $"Exception {activity};. {exception.ToFullBlownString()}" ) );
        }

        /// <summary> Assigns a <see cref="Core.Logger"/> to this library. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="value"> The value. </param>
        public virtual void Apply( Logger value )
        {
            this._Logger = value;
        }

        /// <summary> Applies the trace level described by value. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="value"> The value. </param>
        public virtual void ApplyTraceLogLevel( TraceEventType value )
        {
            this.TraceLevel = value;
            this.Logger.ApplyTraceLevel( value );
        }

        /// <summary> Applies the trace level described by value. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        public virtual void ApplyTraceLogLevel()
        {
            this.ApplyTraceLogLevel( this.TraceLevel );
        }

        #endregion

        #region " TIMEZONE "

        /// <summary> Identifier for the local time zone. </summary>
        private string _LocalTimeZoneId;

        /// <summary> Gets the identifier of the local Time zone. </summary>
        /// <value> The identifier of the local Time zone. </value>
        public string LocalTimeZoneId
        {
            get {
                if ( string.IsNullOrWhiteSpace( this._LocalTimeZoneId ) )
                {
                    this._LocalTimeZoneId = TimeZoneInfo.Local.Id;
                }

                return this._LocalTimeZoneId;
            }
        }

        /// <summary> Information describing the local time zone. </summary>
        private TimeZoneInfo _LocalTimeZoneInfo;

        /// <summary> Gets information describing the local Time zone. </summary>
        /// <value> Information describing the local Time zone. </value>
        public TimeZoneInfo LocalTimeZoneInfo
        {
            get {
                if ( this._LocalTimeZoneInfo is null )
                {
                    this._LocalTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById( this.LocalTimeZoneId );
                }

                return this._LocalTimeZoneInfo;
            }
        }

        /// <summary> Gets or sets the identifier of the Platform Time zone. </summary>
        /// <value> The identifier of the Platform Time zone. </value>
        public string PlatformTimeZoneId { get; set; }

        /// <summary> Information describing the platform time zone. </summary>
        private TimeZoneInfo _PlatformTimeZoneInfo;

        /// <summary> Gets or sets information describing the Platform Time zone. </summary>
        /// <value> Information describing the Platform Time zone. </value>
        public TimeZoneInfo PlatformTimeZoneInfo
        {
            get {
                if ( this._PlatformTimeZoneInfo is null )
                {
                    if ( string.IsNullOrWhiteSpace( this.PlatformTimeZoneId ) )
                    {
                        this.PlatformTimeZoneId = TimeZoneInfo.Local.Id;
                    }

                    this._PlatformTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById( this.PlatformTimeZoneId );
                }

                return this._PlatformTimeZoneInfo;
            }

            set => this._PlatformTimeZoneInfo = value;
        }

        /// <summary> Gets the platform time now. </summary>
        /// <value> The platform time now. </value>
        public DateTime PlatformTimeNow => TimeZoneInfo.ConvertTime( DateTime.Now, this.PlatformTimeZoneInfo );

        /// <summary> Gets the platform time UTC now. </summary>
        /// <value> The platform time UTC now. </value>
        public DateTime PlatformTimeUtcNow => TimeZoneInfo.ConvertTimeToUtc( DateTime.Now, this.PlatformTimeZoneInfo );

        #endregion

        #region " CONFIGURATIONS "

        /// <summary>   Enumerates configuration locations. </summary>
        /// <remarks>
        /// David, 2020-11-21.
        /// <list type="bullet"><listheader>Configuration Levels:</listheader><item>
        /// <see cref="System.Configuration.ConfigurationUserLevel.None"/> All Users </item><item>
        /// <see cref="System.Configuration.ConfigurationUserLevel.PerUserRoaming"/> User Roaming
        /// </item><item>
        /// <see cref="System.Configuration.ConfigurationUserLevel.PerUserRoamingAndLocal"/> User Local
        /// </item> </list>
        /// </remarks>
        /// <param name="configurationLevel">   The configuration level. </param>
        /// <returns>   A string[]. </returns>
        public static string[] EnumerateConfigurationLocations( System.Configuration.ConfigurationUserLevel configurationLevel )
        {
            System.Configuration.Configuration configuration = System.Configuration.ConfigurationManager.OpenExeConfiguration( configurationLevel );
            var l = new System.Collections.Generic.List<string>();
            if ( configuration.Locations.Count > 0 )
            {
                foreach ( System.Configuration.ConfigurationLocation s in configuration.Locations )
                    l.Add( s.Path );
            }

            return l.ToArray();
        }

        #endregion

    }
}
