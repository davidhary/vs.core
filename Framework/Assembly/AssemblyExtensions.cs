
using System;
using System.Diagnostics;
using System.Linq;

using Microsoft.VisualBasic.ApplicationServices;

namespace isr.Core.AssemblyExtensions
{
    /// <summary> Includes extensions for <see cref="T:System.Reflection.Assembly">Assembly</see>
    /// and <see cref="T:System.Reflection.Assembly">assembly info</see>. </summary>
    /// <remarks> (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para> </remarks>
    public static class AssemblyExtensionMethods
    {

        /// <summary> Returns the assembly public key. </summary>
        /// <remarks>
        /// <examples>
        /// <code>
        /// Dim pk As String = System.Reflection.Assembly.GetExecutingAssembly.GetPublicKey()
        /// </code></examples>
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="assembly"> The assembly. </param>
        /// <returns> The public key of the assembly. </returns>
        public static string GetPublicKey( this System.Reflection.Assembly assembly )
        {
            if ( assembly is null )
            {
                throw new ArgumentNullException( nameof( assembly ) );
            }

            var values = assembly.GetName().GetPublicKey();
            return values.Aggregate( string.Empty, ( current, value ) => current + value.ToString( "X2" ) );
        }

        /// <summary> Gets public key token. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="assembly"> The assembly. </param>
        /// <returns> The public key token of the assembly. </returns>
        public static string GetPublicKeyToken( this System.Reflection.Assembly assembly )
        {
            if ( assembly is null )
            {
                throw new ArgumentNullException( nameof( assembly ) );
            }

            var values = assembly.GetName().GetPublicKeyToken();
            return values.Aggregate( string.Empty, ( current, value ) => current + value.ToString( "X2" ) );
        }

        /// <summary> Gets the application <see cref="FileVersionInfo">File Version Info.</see> </summary>
        /// <remarks>
        /// Use this version information source for the application so that it won't be affected by any
        /// changes that might occur in the <see cref="MyAssemblyInfo">application info</see> code.
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="assembly"> The assembly. </param>
        /// <returns> The application <see cref="FileVersionInfo">File Version Info</see>. </returns>
        [System.Security.Permissions.SecurityPermission( System.Security.Permissions.SecurityAction.Demand, Flags = System.Security.Permissions.SecurityPermissionFlag.UnmanagedCode )]
        public static FileVersionInfo GetFileVersionInfo( this System.Reflection.Assembly assembly )
        {
            return assembly is null ? throw new ArgumentNullException( nameof( assembly ) ) : FileVersionInfo.GetVersionInfo( assembly.Location );
        }

        /// <summary> Gets a unique identifier. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="assembly"> The assembly. </param>
        /// <returns> The unique identifier. </returns>
        public static Guid GetGuid( this System.Reflection.Assembly assembly )
        {
            if ( assembly is null )
            {
                throw new ArgumentNullException( nameof( assembly ) );
            }

            System.Runtime.InteropServices.GuidAttribute attribute = ( System.Runtime.InteropServices.GuidAttribute ) assembly.GetCustomAttributes( typeof( System.Runtime.InteropServices.GuidAttribute ), true )[0];
            return new Guid( attribute.Value );
        }

        /// <summary> Directory path. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="assembly"> The assembly. </param>
        /// <returns> A String. </returns>
        public static string DirectoryPath( this System.Reflection.Assembly assembly )
        {
            return assembly is null ? throw new ArgumentNullException( nameof( assembly ) ) : System.IO.Path.GetDirectoryName( assembly.Location );
        }

        /// <summary> Gets the assembly. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="assemblyInfo"> Information describing the assembly. </param>
        /// <returns> The application <see cref="T:System.Reflection.Assembly">Assembly</see>. </returns>
        public static System.Reflection.Assembly GetAssembly( this AssemblyInfo assemblyInfo )
        {
            if ( assemblyInfo is null )
            {
                throw new ArgumentNullException( nameof( assemblyInfo ) );
            }

            System.Reflection.Assembly value = null;
            foreach ( System.Reflection.Assembly a in assemblyInfo.LoadedAssemblies )
            {
                if ( a.GetName().Name.Equals( assemblyInfo.AssemblyName ) )
                {
                    value = a;
                    break;
                }
            }

            return value;
        }

        /// <summary> Gets the folder where application files are located. </summary>
        /// <remarks>
        /// <examples>
        /// <code>
        /// Dim folder As String = My.Application.Info.ApplicationFolder()
        /// </code></examples>
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="assemblyInfo"> Information describing the assembly. </param>
        /// <returns> The application folder path. </returns>
        public static string ApplicationFolder( this AssemblyInfo assemblyInfo )
        {
            return assemblyInfo is null ? throw new ArgumentNullException( nameof( assemblyInfo ) ) : assemblyInfo.DirectoryPath;
        }

        /// <summary> The default version format. </summary>
        public const string DefaultVersionFormat = "{0}.{1:00}.{2:0000}";

        /// <summary> Gets the product version. </summary>
        /// <remarks>
        /// <examples>
        /// <code>
        /// Dim version As String = My.Application.Info.ProducVersion("")
        /// </code></examples>
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="assemblyInfo"> Information describing the assembly. </param>
        /// <param name="format">       The version format. Set to empty to use the
        /// <see cref="DefaultVersionFormat">default version format</see>.
        /// </param>
        /// <returns> The application version. </returns>
        public static string ProductVersion( this AssemblyInfo assemblyInfo, string format )
        {
            if ( assemblyInfo is null )
            {
                throw new ArgumentNullException( nameof( assemblyInfo ) );
            }

            if ( string.IsNullOrWhiteSpace( format ) )
            {
                format = DefaultVersionFormat;
            }

            return string.Format( System.Globalization.CultureInfo.CurrentCulture, format, assemblyInfo.Version.Major, assemblyInfo.Version.Minor, assemblyInfo.Version.Build );
        }

        #region " PRODUCT PROCESS CAPTION "

        /// <summary> Prefix process name to the product name. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="assemblyInfo"> Information describing the assembly. </param>
        /// <returns> A String. </returns>
        public static string PrefixProcessName( this AssemblyInfo assemblyInfo )
        {
            if ( assemblyInfo is null )
            {
                throw new ArgumentNullException( nameof( assemblyInfo ) );
            }

            string name = assemblyInfo.ProductName;
            string processName = Process.GetCurrentProcess().ProcessName;
            if ( !name.StartsWith( processName, StringComparison.OrdinalIgnoreCase ) )
            {
                name = $"{processName}.{name}";
            }

            return name;
        }

        /// <summary> Builds product time caption. </summary>
        /// <remarks>
        /// <list type="bullet">Use the following format options: <item>
        /// u - UTC - 2019-09-10 19:27:04Z</item><item>
        /// r - GMT - Tue, 10 May 2019 19:26:42 GMT</item><item>
        /// o - ISO - 2019-09-10T12:12:29.7552627-07:00</item><item>
        /// s - ISO - 2019-09-10T12:24:47</item><item>
        /// empty   - 2019-09-10 16:57:24 -07:00</item><item>
        /// s + zzz - 2019-09-10T12:24:47-07:00</item></list>
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="assemblyInfo">      Information describing the assembly. </param>
        /// <param name="versionElements">   The version elements. </param>
        /// <param name="timeCaptionFormat"> The time caption format. </param>
        /// <param name="kindFormat">        The kind format. </param>
        /// <returns> A String. </returns>
        public static string BuildProductTimeCaption( this AssemblyInfo assemblyInfo, int versionElements, string timeCaptionFormat, string kindFormat )
        {
            return assemblyInfo is null
                ? throw new ArgumentNullException( nameof( assemblyInfo ) )
                : $"{assemblyInfo.PrefixProcessName()}.r.{assemblyInfo.Version.ToString( versionElements )} {MyAssemblyInfo.BuildLocalTimeCaption( timeCaptionFormat, kindFormat )}";
        }

        /// <summary>
        /// Builds product time caption using full version and local time plus kind format.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="assemblyInfo"> Information describing the assembly. </param>
        /// <returns> A String. </returns>
        public static string BuildProductTimeCaption( this AssemblyInfo assemblyInfo )
        {
            return assemblyInfo.BuildProductTimeCaption( 4, string.Empty, string.Empty );
        }

        #endregion

    }
}
