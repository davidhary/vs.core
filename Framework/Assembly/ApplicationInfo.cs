using System;
using System.Diagnostics;
using System.Linq;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core
{
    /// <summary> A sealed class designed to provide application information for the assembly. </summary>
    /// <remarks> Requires: Microsoft.VisualBasic; System.Linq <para>
    /// (c) 2005 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2005-01-15, 1.0.1841 </para></remarks>
    public sealed class ApplicationInfo
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="ApplicationInfo" /> class. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        private ApplicationInfo() : base()
        {
        }

        #endregion

        #region " APPLICATION INFORMATION "

        /// <summary> Full pathname of the application folder file. </summary>
        private static string _ApplicationFolder;

        /// <summary> Gets or sets or Sets the folder where application files are located. </summary>
        /// <value> The application folder path. </value>
        public static string ApplicationFolder
        {
            get {
                if ( string.IsNullOrEmpty( _ApplicationFolder ) )
                {
                    _ApplicationFolder = My.MyProject.Application.Info.DirectoryPath;
                }

                return _ApplicationFolder;
            }

            set {
                if ( !string.IsNullOrEmpty( value ) )
                {
                    _ApplicationFolder = value;
                }
            }
        }

        /// <summary> Gets the application filename. </summary>
        /// <value> The application filename. </value>
        public static string FileName =>
                // Return $"{My.Application.Info.AssemblyName}.exe"
                System.IO.Path.Combine( My.MyProject.Application.Info.DirectoryPath, $"{My.MyProject.Application.Info.AssemblyName}.exe" );

        /// <summary> Returns the application public key. </summary>
        /// <value> The public key of the application. </value>
        public static string ApplicationPublicKey
        {
            get {
                var values = System.Reflection.Assembly.GetExecutingAssembly().GetName().GetPublicKey();
                return values.Aggregate( string.Empty, ( current, value ) => current + value.ToString( "X2" ) );
            }
        }

        /// <summary> Returns the application name. </summary>
        /// <value> The name of the application. </value>
        public static string ApplicationName => My.MyProject.Application.Info.AssemblyName;

        /// <summary> The application Description. </summary>
        private static string _ApplicationDescription;

        /// <summary>
        /// Gets or sets or Sets the application Description.  This is required for setting the error
        /// sources.
        /// </summary>
        /// <value> The application Description. </value>
        public static string ApplicationDescription
        {
            get {
                if ( string.IsNullOrEmpty( _ApplicationDescription ) )
                {
                    _ApplicationDescription = My.MyProject.Application.Info.Description;
                }

                return _ApplicationDescription;
            }

            set {
                if ( !string.IsNullOrEmpty( value ) )
                {
                    _ApplicationDescription = value;
                }
            }
        }

        /// <summary> The application title. </summary>
        private static string _ApplicationTitle;

        /// <summary>
        /// Gets or sets or Sets the application title.  This is required for setting the error sources.
        /// </summary>
        /// <value> The application title. </value>
        public static string ApplicationTitle
        {
            get {
                if ( string.IsNullOrEmpty( _ApplicationTitle ) )
                {
                    _ApplicationTitle = My.MyProject.Application.Info.Title;
                }

                return _ApplicationTitle;
            }

            set {
                if ( !string.IsNullOrEmpty( value ) )
                {
                    _ApplicationTitle = value;
                }
            }
        }

        /// <summary> The application version. </summary>
        private static string _ApplicationVersion;

        /// <summary> Gets or sets or Sets the folder where application files are located. </summary>
        /// <value> The application version. </value>
        public static string ApplicationVersion
        {
            get {
                if ( string.IsNullOrEmpty( _ApplicationVersion ) )
                {
                    _ApplicationVersion = $"{My.MyProject.Application.Info.Version.Major}.{My.MyProject.Application.Info.Version.Minor:00}.{My.MyProject.Application.Info.Version.Build:0000}";
                }

                return _ApplicationVersion;
            }

            set {
                if ( !string.IsNullOrEmpty( value ) )
                {
                    _ApplicationVersion = value;
                }
            }
        }

        /// <summary> Returns the application file version. </summary>
        /// <value> The file version of the application. </value>
        public static string FileVersion
        {
            [System.Security.Permissions.SecurityPermission( System.Security.Permissions.SecurityAction.Demand, Flags = System.Security.Permissions.SecurityPermissionFlag.UnmanagedCode )]
            get => FileVersionInfo.FileVersion;
        }

        /// <summary> Information describing the file version. </summary>
        private static FileVersionInfo _FileVersionInfo;

        /// <summary> Gets the application <see cref="FileVersionInfo">File Version Info.</see> </summary>
        /// <remarks>
        /// Use this version information source for the application so that it won't be affected by any
        /// changes that might occur in the <see cref="MyAssemblyInfo">application info</see> code.
        /// </remarks>
        /// <value> The application <see cref="FileVersionInfo">File Version Info</see>. </value>
        public static FileVersionInfo FileVersionInfo
        {
            [System.Security.Permissions.SecurityPermission( System.Security.Permissions.SecurityAction.Demand, Flags = System.Security.Permissions.SecurityPermissionFlag.UnmanagedCode )]
            get {
                if ( _FileVersionInfo is null )
                {
                    _FileVersionInfo = FileVersionInfo.GetVersionInfo( FileName );
                }

                return _FileVersionInfo;
            }
        }

        /// <summary> Returns the application product name. </summary>
        /// <value> The product name of the application. </value>
        public static string ProductName => My.MyProject.Application.Info.ProductName;

        /// <summary> Returns the application product version. </summary>
        /// <value> The product version of the application. </value>
        public static string ProductVersion => My.MyProject.Application.Info.Version.ToString( 4 );

        #endregion

        #region " APPLICATION CONFIGURATION FILE "

        /// <summary> Returns the application configuration file name. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="extension"> The extension. </param>
        /// <returns> System.String. </returns>
        public static string BuildApplicationConfigFileName( string extension )
        {
            return $"{My.MyProject.Application.Info.AssemblyName}.exe{extension}";
        }

        /// <summary>
        /// Returns the file path of the default application configuration file. The folder is created if
        /// it does not exists.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="allUsers">                   True to select the folder for
        /// <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.AllUsersApplicationData"/>
        /// all users; otherwise, use using current user;
        /// otherwise, use the folder for
        /// <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.CurrentUserApplicationData"/>
        /// </param>
        /// <param name="significantVersionElements"> The significant version elements. A value between 1
        /// and 4 indicating the version level to use. For
        /// example, with 1, only the major version of the
        /// folder name is used; whereas with 4 all version
        /// elements are used. Defaults to 4. </param>
        /// <returns>
        /// The configuration file full path. Uses the application folder if this folder is writable.
        /// Otherwise the <see cref="OpenApplicationConfigFolderPath">application data folder</see> is
        /// used. System.String.
        /// </returns>
        public static string BuildApplicationConfigFilePath( bool allUsers, int significantVersionElements )
        {
            return BuildApplicationConfigFilePath( allUsers, ".config", significantVersionElements );
        }

        /// <summary>
        /// Returns the file path of the application configuration file. The folder is created if it does
        /// not exists.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="allUsers">                   True to select the folder for
        /// <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.AllUsersApplicationData"/>
        /// all users; otherwise, use using current user;
        /// otherwise, use the folder for
        /// <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.CurrentUserApplicationData"/>
        /// </param>
        /// <param name="extension">                  The extension. </param>
        /// <param name="significantVersionElements"> The significant version elements. A value between 1
        /// and 4 indicating the version level to use. For
        /// example, with 1, only the major version of the
        /// folder name is used; whereas with 4 all version
        /// elements are used. Defaults to 4. </param>
        /// <returns>
        /// The configuration file full path. Uses the application folder if this folder is writable.
        /// Otherwise the <see cref="OpenApplicationConfigFolderPath">application data folder</see> is
        /// used. System.String.
        /// </returns>
        public static string BuildApplicationConfigFilePath( bool allUsers, string extension, int significantVersionElements )
        {
            return System.IO.Path.Combine( OpenApplicationConfigFolderPath( allUsers, significantVersionElements ), BuildApplicationConfigFileName( extension ) );
        }

        /// <summary> Determines whether the specified folder path is writable. </summary>
        /// <remarks>
        /// Uses a temporary random file name to test if the file can be created. The file is deleted
        /// thereafter.
        /// </remarks>
        /// <param name="path"> The path. </param>
        /// <returns> <c>True</c> if the specified path is writable; otherwise, <c>False</c>. </returns>
        public static bool IsFolderWritable( string path )
        {
            string filePath = string.Empty;
            bool affirmative = false;
            try
            {
                filePath = System.IO.Path.Combine( path, System.IO.Path.GetRandomFileName() );
                using ( var s = System.IO.File.Open( filePath, System.IO.FileMode.OpenOrCreate ) )
                {
                }

                affirmative = true;
            }
            catch
            {
            }
            finally
            {
                // SS reported an exception from this test possibly indicating that Windows allowed writing the file 
                // by failed report deletion. Or else, Windows raised another exception type.
                try
                {
                    if ( System.IO.File.Exists( filePath ) )
                    {
                        System.IO.File.Delete( filePath );
                    }
                }
                catch
                {
                }
            }

            return affirmative;
        }

        /// <summary> Opens the application configuration folder path. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="allUsers">                   True to select the folder for
        /// <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.AllUsersApplicationData"/>
        /// all users; otherwise, use using current user;
        /// otherwise, use the folder for
        /// <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.CurrentUserApplicationData"/>
        /// </param>
        /// <param name="significantVersionElements"> The significant version elements. A value between 1
        /// and 4 indicating the version level to use. For
        /// example, with 1, only the major version of the
        /// folder name is used; whereas with 4 all version
        /// elements are used. Defaults to 4. </param>
        /// <returns>
        /// The configuration folder full path. Uses the application folder if this folder is writable.
        /// Otherwise the <see cref="OpenApplicationDataFolderPath(bool)">application data folder</see> is
        /// used. System.String.
        /// </returns>
        public static string OpenApplicationConfigFolderPath( bool allUsers, int significantVersionElements )
        {
            string candidatePath = My.MyProject.Application.Info.DirectoryPath;
            if ( !IsFolderWritable( candidatePath ) )
            {
                candidatePath = OpenApplicationDataFolderPath( allUsers, significantVersionElements );
            }

            return candidatePath;
        }

        #endregion

        #region " APPLICATION DATA FOLDER -- CUSTOMIZED "

        /// <summary> Drops the trailing elements. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value">     The value. </param>
        /// <param name="delimiter"> The delimiter. </param>
        /// <param name="count">     The count. </param>
        /// <returns> System.String. </returns>
        public static string DropTrailingElements( string value, char delimiter, int count )
        {
            if ( string.IsNullOrEmpty( value ) )
            {
                return value;
            }
            else if ( string.IsNullOrEmpty( Conversions.ToString( delimiter ) ) )
            {
                return value;
            }
            else
            {
                while ( count > 0 )
                {
                    count -= 1;
                    int i = value.LastIndexOf( delimiter );
                    if ( i > 0 )
                    {
                        value = value.Substring( 0, i );
                    }
                    else
                    {
                        count = 0;
                    }
                }

                return value;
            }
        }

        /// <summary> Drops the trailing elements. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value">     The value. </param>
        /// <param name="delimiter"> The delimiter. </param>
        /// <param name="count">     The count. </param>
        /// <param name="replace">   The replace. </param>
        /// <returns> System.String. </returns>
        public static string ReplaceTrailingElements( string value, char delimiter, int count, string replace )
        {
            if ( string.IsNullOrEmpty( value ) )
            {
                return value;
            }
            else if ( string.IsNullOrEmpty( Conversions.ToString( delimiter ) ) )
            {
                return value;
            }
            else
            {
                int replaceCount = 0;
                while ( count > 0 )
                {
                    count -= 1;
                    int i = value.LastIndexOf( delimiter );
                    if ( i > 0 )
                    {
                        replaceCount += 1;
                        value = value.Substring( 0, i );
                    }
                    else
                    {
                        count = 0;
                    }
                }

                var builder = new System.Text.StringBuilder( value );
                if ( replaceCount > 0 )
                {
                    for ( int i = 1, loopTo = replaceCount; i <= loopTo; i++ )
                    {
                        _ = builder.Append( delimiter );
                        _ = builder.Append( replace );
                    }
                }

                return builder.ToString();
            }
        }

        /// <summary>
        /// Returns the Folder Path with the relevant version information elements replaces with '0'.
        /// Creates the folder if it does not exist.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="dataFolderPath">             Name of the data path. </param>
        /// <param name="significantVersionElements"> The significant version elements. A value between 1
        /// and 4 indicating the version level to use. For
        /// example, with 1, only the major version of the
        /// folder name is used; whereas with 4 all version
        /// elements are used. Defaults to 4. </param>
        /// <returns>
        /// A folder name with the revision numbers (if there) truncated based on the number of
        /// significant elements. For example, with two significant digits we get: ...major.minor.0.0;
        /// or.
        /// </returns>
        public static string OpenDataFolderPath( string dataFolderPath, int significantVersionElements )
        {
            significantVersionElements = Math.Max( 1, Math.Min( 4, significantVersionElements ) );
            var di = new System.IO.DirectoryInfo( ReplaceTrailingElements( dataFolderPath, '.', 4 - significantVersionElements, "0" ) );
            if ( !di.Exists )
            {
                di.Create();
            }

            return di.FullName;
        }

        /// <summary>
        /// Gets the application data folder Path. The folder is created if it does not exist.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="allUsers">                   True to select the folder for
        /// <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.AllUsersApplicationData"/>
        /// all users; otherwise, use using current user;
        /// otherwise, use the folder for
        /// <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.CurrentUserApplicationData"/>
        /// </param>
        /// <param name="significantVersionElements"> The significant version elements. A value between 1
        /// and 4 indicating the version level to use. For
        /// example, with 1, only the major version of the
        /// folder name is used; whereas with 4 all version
        /// elements are used. Defaults to 4. </param>
        /// <returns>
        /// The application data folder for the product. With All Users: In Windows XP: .\ Application
        /// Data\&lt;company&gt;\&lt;product name&gt;\major.minor.0.0; or In Windows 7 or 8: C:\
        /// ProgramData\&lt;company&gt;\&lt;product name&gt;\major.minor.0.0 For the Current User: In
        /// Windows XP: .\&lt;user&gt;\documents and settings\AppData\&lt;company&gt;\&lt;product name&gt;
        /// \major.minor.0.0; or In Windows 7 or 8: C:\Users\&lt;user&gt;\AppData\Roaming\\&lt;company&gt;
        /// \&lt;product name&gt;\major.minor.0.0.
        /// 
        /// The version information is based on the Assembly File Version.
        /// </returns>
        public static string OpenApplicationDataFolderPath( bool allUsers, int significantVersionElements )
        {
            return OpenDataFolderPath( OpenApplicationDataFolderPath( allUsers ), significantVersionElements );
        }

        #endregion

        #region " APPLICATION DATA FOLDER "

        /// <summary>
        /// Gets the application data folder Path. This seems to create the folder if it does not exist.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="allUsers"> True to select the folder for
        /// <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.AllUsersApplicationData"/>
        /// all users; otherwise, use using current user;
        /// otherwise, use the folder for
        /// <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.CurrentUserApplicationData"/>
        /// </param>
        /// <returns> System.String. </returns>
        public static string OpenApplicationDataFolderPath( bool allUsers )
        {
            return allUsers ? My.MyProject.Computer.FileSystem.SpecialDirectories.AllUsersApplicationData : My.MyProject.Computer.FileSystem.SpecialDirectories.CurrentUserApplicationData;
        }

        #endregion

        #region " CAPTION "

        /// <summary> Builds application description caption. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="subtitle"> The subtitle. </param>
        /// <returns> A String. </returns>
        public static string BuildApplicationDescriptionCaption( string subtitle )
        {
            return BuildCaption( ApplicationDescription, subtitle );
        }

        /// <summary> Builds application title caption. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="subtitle"> The subtitle. </param>
        /// <returns> A String. </returns>
        public static string BuildApplicationTitleCaption( string subtitle )
        {
            return BuildCaption( ApplicationTitle, subtitle );
        }

        /// <summary> Gets or sets the release candidate prefix. </summary>
        /// <value> The release candidate prefix. </value>
        public static string ReleaseCandidatePrefix { get; set; } = "RC";

        /// <summary> Gets or sets the alpha version prefix. </summary>
        /// <value> The alpha version prefix. </value>
        public static string AlphaVersionPrefix { get; set; } = "Alpha";

        /// <summary> Gets or sets the beta version prefix. </summary>
        /// <value> The beta version prefix. </value>
        public static string BetaVersionPrefix { get; set; } = "Beta";

        /// <summary> Gets or sets the gold version prefix. </summary>
        /// <value> The gold version prefix. </value>
        public static string GoldVersionPrefix { get; set; } = "Gold";

        /// <summary> Builds the default caption. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="title">    The title. </param>
        /// <param name="subtitle"> The subtitle. </param>
        /// <returns> System.String. </returns>
        public static string BuildCaption( string title, string subtitle )
        {
            var builder = new System.Text.StringBuilder();
            _ = builder.Append( title );
            _ = builder.Append( " " );
            _ = builder.Append( ApplicationVersion );
            if ( My.MyProject.Application.Info.Version.Major < 1 )
            {
                _ = builder.Append( "." );
                switch ( My.MyProject.Application.Info.Version.Minor )
                {
                    case 0:
                        {
                            _ = builder.Append( AlphaVersionPrefix );
                            break;
                        }

                    case 1:
                        {
                            _ = builder.Append( BetaVersionPrefix );
                            break;
                        }

                    case var @case when 2 <= @case && @case <= 8:
                        {
                            _ = builder.Append( $"{ReleaseCandidatePrefix}{My.MyProject.Application.Info.Version.Minor - 1}" );
                            break;
                        }

                    default:
                        {
                            _ = builder.Append( GoldVersionPrefix );
                            break;
                        }
                }
            }

            if ( !string.IsNullOrEmpty( subtitle ) )
            {
                _ = builder.Append( ": " );
                _ = builder.Append( subtitle );
            }

            return builder.ToString();
        }

        /// <summary> Gets or sets the simulate suffix. </summary>
        /// <value> The simulate suffix. </value>
        public static string SimulateSuffix { get; set; } = ".Simulate";

        /// <summary> Gets or sets the retrieve only suffix. </summary>
        /// <value> The retrieve only suffix. </value>
        public static string RetrieveOnlySuffix { get; set; } = ".Retrieve Only";

        /// <summary> Builds the default caption. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="title">           The title. </param>
        /// <param name="subtitle">        The subtitle. </param>
        /// <param name="usingDevices">    if set to <c>True</c> using devices. </param>
        /// <param name="usingSimulation"> if set to <c>True</c> using simulation. </param>
        /// <returns> System.String. </returns>
        public static string BuildCaption( string title, string subtitle, bool usingDevices, bool usingSimulation )
        {
            // Set the caption to the Application title
            var newCaption = new System.Text.StringBuilder();
            _ = newCaption.Append( BuildCaption( title, subtitle ) );
            _ = usingDevices
                ? newCaption.Append( "." )
                : usingSimulation ? newCaption.Append( SimulateSuffix ) : newCaption.Append( RetrieveOnlySuffix );

            return newCaption.ToString();
        }

        #endregion

    }
}
