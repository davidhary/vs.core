using System;

namespace isr.Core
{
    /// <summary> Information about the program. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    public class ProgramInfo
    {

        #region " CONSTRUCTION "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        public ProgramInfo() : base()
        {
            this.Lines = new System.Collections.ObjectModel.Collection<ProgramInfoLine>();
        }

        #endregion

        #region " DISPLAY "

        /// <summary> Gets or sets the lines. </summary>
        /// <value> The lines. </value>
        public System.Collections.ObjectModel.Collection<ProgramInfoLine> Lines { get; private set; }

        /// <summary> Clears this object to its blank/initial state. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        public void Clear()
        {
            this.Lines.Clear();
        }

        /// <summary> Appends a line. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="text"> The text. </param>
        /// <param name="font"> The font. </param>
        public void AppendLine( string text, System.Drawing.Font font )
        {
            this.Lines.Add( new ProgramInfoLine( text, font ) );
        }

        /// <summary> Updates the display contents. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="headerFont">  The header font. </param>
        /// <param name="contentFont"> The content font. </param>
        public void BuildContents( System.Drawing.Font headerFont, System.Drawing.Font contentFont )
        {
            this.Clear();
            this.AppendLine( "Product Name:", headerFont );
            this.AppendLine( My.MyProject.Application.Info.ProductName, contentFont );
            this.AppendLine( string.Empty, contentFont );
            this.AppendLine( "Product Title:", headerFont );
            this.AppendLine( My.MyProject.Application.Info.Title, contentFont );
            this.AppendLine( string.Empty, contentFont );
            this.AppendLine( "Product Name:", headerFont );
            this.AppendLine( My.MyProject.Application.Info.AssemblyName, contentFont );
            this.AppendLine( string.Empty, contentFont );
            this.AppendLine( "Product Version:", headerFont );
            this.AppendLine( My.MyProject.Application.Info.Version.ToString(), contentFont );
            this.AppendLine( string.Empty, contentFont );
            this.AppendLine( "Product Folder:", headerFont );
            this.AppendLine( My.MyProject.Application.Info.DirectoryPath, contentFont );
            this.AppendLine( string.Empty, contentFont );
            this.AppendLine( "Program Data Folder:", headerFont );
            this.AppendLine( My.MyProject.Computer.FileSystem.SpecialDirectories.AllUsersApplicationData, contentFont );
            this.AppendLine( string.Empty, contentFont );
            this.AppendLine( "User Program Data Folder:", headerFont );
            this.AppendLine( My.MyProject.Computer.FileSystem.SpecialDirectories.CurrentUserApplicationData, contentFont );
            this.AppendLine( string.Empty, contentFont );
            var configuration = System.Configuration.ConfigurationManager.OpenExeConfiguration( System.Configuration.ConfigurationUserLevel.None );
            this.AppendLine( "All Users Configuration File:", headerFont );
            this.AppendLine( configuration.FilePath, contentFont );
            if ( configuration.Locations.Count > 0 )
            {
                this.AppendLine( "Configuration Locations:", headerFont );
                this.AppendLine( configuration.FilePath, contentFont );
                foreach ( System.Configuration.ConfigurationLocation s in configuration.Locations )
                {
                    this.AppendLine( s.Path, contentFont );
                }
            }

            this.AppendLine( string.Empty, contentFont );
            configuration = System.Configuration.ConfigurationManager.OpenExeConfiguration( System.Configuration.ConfigurationUserLevel.PerUserRoaming );
            this.AppendLine( "User Roaming Configuration File:", headerFont );
            this.AppendLine( configuration.FilePath, contentFont );
            if ( configuration.Locations.Count > 0 )
            {
                this.AppendLine( "Configuration Locations:", headerFont );
                this.AppendLine( configuration.FilePath, contentFont );
                foreach ( System.Configuration.ConfigurationLocation s in configuration.Locations )
                {
                    this.AppendLine( s.Path, contentFont );
                }
            }

            this.AppendLine( string.Empty, contentFont );
            _ = System.Configuration.ConfigurationManager.OpenExeConfiguration( System.Configuration.ConfigurationUserLevel.PerUserRoamingAndLocal );
            configuration = System.Configuration.ConfigurationManager.OpenExeConfiguration( System.Configuration.ConfigurationUserLevel.PerUserRoaming );
            this.AppendLine( "User Local  Configuration File:", headerFont );
            this.AppendLine( configuration.FilePath, contentFont );
            if ( configuration.Locations.Count > 0 )
            {
                this.AppendLine( "Configuration Locations:", headerFont );
                this.AppendLine( configuration.FilePath, contentFont );
                foreach ( System.Configuration.ConfigurationLocation s in configuration.Locations )
                {
                    this.AppendLine( s.Path, contentFont );
                }
            }

            this.AppendLine( string.Empty, contentFont );

            // allows the calling application to add data.
            this.OnProgramInfoRequested( EventArgs.Empty );
            this.AppendLine( "Product Copyrights:", headerFont );
            this.AppendLine( My.MyProject.Application.Info.Copyright, contentFont );
            this.AppendLine( string.Empty, contentFont );
        }

        #endregion

        #region " PROGRAM INFO REQUESTED "

        /// <summary> Notifies of the <see cref="ProgramInfoRequested"/> event. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void OnProgramInfoRequested( EventArgs e )
        {
            this.SyncNotifyProgramInfoRequested( e );
        }

        /// <summary> Removes the  <see cref="ProgramInfoRequested"/> event handlers. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        protected void RemoveProgramInfoRequestedEventHandlers()
        {
            this._ProgramInfoRequestedEventHandlers?.RemoveAll();
        }

        /// <summary> The  <see cref="ProgramInfoRequested"/> event handlers. </summary>
        private readonly EventHandlerContextCollection<EventArgs> _ProgramInfoRequestedEventHandlers = new();

        /// <summary> Event queue for all listeners interested in  <see cref="ProgramInfoRequested"/> events. </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event EventHandler<EventArgs> ProgramInfoRequested
        {
            add {
                this._ProgramInfoRequestedEventHandlers.Add( new EventHandlerContext<EventArgs>( value ) );
            }

            remove {
                this._ProgramInfoRequestedEventHandlers.RemoveValue( value );
            }
        }

        private void OnProgramInfoRequested( object sender, EventArgs e )
        {
            this._ProgramInfoRequestedEventHandlers.Send( sender, e );
        }

        /// <summary>
        /// Safely and synchronously <see cref="EventHandlerContext{TEventArgs}.Send(object, TEventArgs)">sends</see> or invokes the
        /// <see cref="ProgramInfoRequested">ProgramInfoRequested Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
        protected void SyncNotifyProgramInfoRequested( EventArgs e )
        {
            this._ProgramInfoRequestedEventHandlers.Send( this, e );
        }

        #endregion

        #region " REFRESH REQUESTED "

        /// <summary> Notifies of the <see cref="RefreshRequested"/> event. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        public void NotifyRefreshRequested()
        {
            this.SyncNotifyRefreshRequested( EventArgs.Empty );
        }

        /// <summary> Removes the<see cref="RefreshRequested"/> event handlers. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        protected void RemoveRefreshRequestedEventHandlers()
        {
            this._RefreshRequestedEventHandlers?.RemoveAll();
        }

        /// <summary> The <see cref="RefreshRequested"/> event handlers. </summary>
        private readonly EventHandlerContextCollection<EventArgs> _RefreshRequestedEventHandlers = new();

        /// <summary> Event queue for all listeners interested in <see cref="RefreshRequested"/> events. </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event EventHandler<EventArgs> RefreshRequested
        {
            add {
                this._RefreshRequestedEventHandlers.Add( new EventHandlerContext<EventArgs>( value ) );
            }

            remove {
                this._RefreshRequestedEventHandlers.RemoveValue( value );
            }
        }

        private void OnRefreshRequested( object sender, EventArgs e )
        {
            this._RefreshRequestedEventHandlers.Send( sender, e );
        }

        /// <summary>
        /// Safely and synchronously <see cref="EventHandlerContext{TEventArgs}.Send(object, TEventArgs)">sends</see> or invokes the
        /// <see cref="RefreshRequested">RefreshRequested Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
        protected void SyncNotifyRefreshRequested( EventArgs e )
        {
            this._RefreshRequestedEventHandlers.Send( this, e );
        }

        #endregion

    }

    /// <summary> A program information line. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    public struct ProgramInfoLine
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="text"> The text. </param>
        /// <param name="font"> The font. </param>
        public ProgramInfoLine( string text, System.Drawing.Font font )
        {
            this.Text = text;
            this.Font = font;
        }

        /// <summary> Gets or sets the font. </summary>
        /// <value> The font. </value>
        public System.Drawing.Font Font { get; set; }

        /// <summary> Gets or sets the text. </summary>
        /// <value> The text. </value>
        public string Text { get; set; }
    }
}
