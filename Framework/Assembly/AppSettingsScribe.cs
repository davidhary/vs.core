using System.Linq;

namespace isr.Core
{

    /// <summary>
    /// Reads and writes application settings to the Application Setting section of the program
    /// configuration file.
    /// </summary>
    /// <remarks>
    /// (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2009-10-21, 1.0.3581 </para>
    /// </remarks>
    public partial class AppSettingsScribe
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="AppSettingsScribe"/> class. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        public AppSettingsScribe() : this( System.Configuration.ConfigurationUserLevel.None )
        {
        }

        /// <summary> Initializes a new instance of the <see cref="AppSettingsScribe"/> class. </summary>
        /// <remarks>
        /// Application configuration is read only except with configuration level of
        /// <see cref="System.Configuration.ConfigurationUserLevel.None">None</see>
        /// Attempting to write with other configuration levels, causes a configuration exception:
        /// "ConfigurationSection properties cannot be edited when locked.".
        /// </remarks>
        /// <param name="configurationUserLevel"> The configuration user level. </param>
        public AppSettingsScribe( System.Configuration.ConfigurationUserLevel configurationUserLevel ) : this( ApplicationInfo.BuildApplicationConfigFilePath( false, 2 ), configurationUserLevel )
        {
        }

        /// <summary> Initializes a new instance of the <see cref="AppSettingsScribe"/> class. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="filePath">               The file path. </param>
        /// <param name="configurationUserLevel"> The configuration user level. </param>
        public AppSettingsScribe( string filePath, System.Configuration.ConfigurationUserLevel configurationUserLevel ) : base()
        {
            var map = new System.Configuration.ExeConfigurationFileMap() { ExeConfigFilename = filePath };
            // Me._configuration = System.Configuration.ConfigurationManager.OpenExeConfiguration(configurationUserLevel)
            this.Configuration = System.Configuration.ConfigurationManager.OpenMappedExeConfiguration( map, configurationUserLevel );
        }

        #endregion

        #region " SINGLETON "

        /// <summary>
        /// Gets a new pad lock to enforce thread safety when creating the singleton instance.
        /// </summary>
        private static readonly object SyncLocker = new ();

        /// <summary> The instance. </summary>
        private static AppSettingsScribe _Instance;

        /// <summary> Instantiates the class. </summary>
        /// <remarks> Use this property to instantiate a single instance of this class. This class uses
        /// lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
        /// <returns> A new or existing instance of the class. </returns>
        [System.Runtime.InteropServices.ComVisible( false )]
        public static AppSettingsScribe Get()
        {
            if ( _Instance is null )
            {
                lock ( SyncLocker )
                {
                    _Instance = new AppSettingsScribe( System.Configuration.ConfigurationUserLevel.None );
                }
            }

            return _Instance;
        }

        #endregion

        #region " GENERAL I/O "

        /// <summary> Gets or sets the configuration. </summary>
        /// <value> The configuration. </value>
        public System.Configuration.Configuration Configuration { get; private set; }

        /// <summary> Returns true if the key exists in the application settings. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="key"> . </param>
        /// <returns>
        /// <c>True</c> if the key exists in the application settings; otherwise, <c>False</c>.
        /// </returns>
        public bool KeyExists( string key )
        {
            return this.Configuration.AppSettings.Settings.AllKeys.Length > 0 && this.Configuration.AppSettings.Settings.AllKeys.Contains( key );
        }

        /// <summary> Saves the modified settings. </summary>
        /// <remarks>
        /// Refreshes the section so that it will be read the next time the class is instantiated.
        /// </remarks>
        public void Save()
        {
            this.Configuration.Save( System.Configuration.ConfigurationSaveMode.Modified );
            System.Configuration.ConfigurationManager.RefreshSection( "appSettings" );
        }

        /// <summary> Gets a key value or empty if the key does not exist. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="key"> . </param>
        /// <returns> The value. </returns>
        public string ReadValue( string key )
        {
            return this.KeyExists( key ) ? this.Configuration.AppSettings.Settings[key].Value : string.Empty;
        }

        /// <summary> Sets the specified key value. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="key">   The key. </param>
        /// <param name="value"> The value. </param>
        /// <returns> System.String. </returns>
        public string WriteValue( string key, string value )
        {
            if ( this.KeyExists( key ) )
            {
                this.Configuration.AppSettings.Settings[key].Value = value;
            }
            else
            {
                this.Configuration.AppSettings.Settings.Add( key, value );
            }

            return this.ReadValue( key );
        }

        #endregion

        #region " STRONG TYPED I/O "

        /// <summary> Reads the application setting. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="key">          The key. </param>
        /// <param name="defaultValue"> The default value. </param>
        /// <returns> System.Drawing.Point. </returns>
        public System.Drawing.Point ReadValue( string key, System.Drawing.Point defaultValue )
        {
            var p = this.ReadValue( key, new System.Windows.Point( defaultValue.X, defaultValue.Y ) );
            return new System.Drawing.Point( ( int ) p.X, ( int ) p.Y );
        }

        /// <summary> Reads the application setting. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="key">          The key. </param>
        /// <param name="defaultValue"> The default value. </param>
        /// <returns> System.Drawing.Point. </returns>
        public System.Windows.Point ReadValue( string key, System.Windows.Point defaultValue )
        {
            string loadedValue = this.ReadValue( key );
            return string.IsNullOrWhiteSpace( loadedValue ) ? defaultValue : System.Windows.Point.Parse( loadedValue );
        }

        /// <summary> Reads the application setting. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="key">          The key. </param>
        /// <param name="defaultValue"> The default value. </param>
        /// <returns> System.Drawing.Point. </returns>
        public System.Windows.Size ReadValue( string key, System.Windows.Size defaultValue )
        {
            string loadedValue = this.ReadValue( key );
            return string.IsNullOrWhiteSpace( loadedValue ) ? defaultValue : System.Windows.Size.Parse( loadedValue );
        }

        /// <summary> Reads the application setting. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="key">          The key. </param>
        /// <param name="defaultValue"> The default value. </param>
        /// <returns> System.Drawing.Point. </returns>
        public System.Drawing.Size ReadValue( string key, System.Drawing.Size defaultValue )
        {
            var s = this.ReadValue( key, new System.Windows.Size( defaultValue.Width, defaultValue.Height ) );
            return new System.Drawing.Size( ( int ) s.Width, ( int ) s.Height );
        }

        /// <summary> Writes the application setting. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="key">   The key. </param>
        /// <param name="value"> The value. </param>
        public void WriteValue( string key, System.Drawing.Point value )
        {
            this.WriteValue( key, new System.Windows.Point( value.X, value.Y ) );
        }

        /// <summary> Writes the application setting. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="key">   The key. </param>
        /// <param name="value"> The value. </param>
        public void WriteValue( string key, System.Windows.Point value )
        {
            _ = this.WriteValue( key, value.ToString() );
        }

        /// <summary> Writes the application setting. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="key">   The key. </param>
        /// <param name="value"> The value. </param>
        public void WriteValue( string key, System.Windows.Size value )
        {
            _ = this.WriteValue( key, value.ToString() );
        }

        /// <summary> Writes the application setting. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="key">   The key. </param>
        /// <param name="value"> The value. </param>
        public void WriteValue( string key, System.Drawing.Size value )
        {
            this.WriteValue( key, new System.Windows.Size( value.Width, value.Height ) );
        }

        #endregion

    }
}
