Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Partial Public NotInheritable Class MyLibrary

        ''' <summary>
        ''' Constructor that prevents a default instance of this class from being created.
        ''' </summary>
        ''' <remarks> David, 2020-09-12. </remarks>
        Private Sub New()
            MyBase.New()
        End Sub

        ''' <summary> Identifier for the trace event. </summary>
        Public Const TraceEventId As Integer = &H103

        ''' <summary> The assembly title. </summary>
        Public Const AssemblyTitle As String = "Core Message Box Library"

        ''' <summary> Information describing the assembly. </summary>
        Public Const AssemblyDescription As String = "Core Message Box Library"
        ''' <summary> The assembly product. </summary>
        Public Const AssemblyProduct As String = "Core.Message.Box"

    End Class

End Namespace
