<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class CompoundInputBox

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.TextBox = New System.Windows.Forms.TextBox()
        Me._CancelButton = New System.Windows.Forms.Button()
        Me._AcceptButton = New System.Windows.Forms.Button()
        Me.TextBoxLabel = New System.Windows.Forms.Label()
        Me._Layout = New System.Windows.Forms.TableLayoutPanel()
        Me._ButtonLayout = New System.Windows.Forms.TableLayoutPanel()
        Me.NumericUpDownLabel = New System.Windows.Forms.Label()
        Me.DropDownBoxLabel = New System.Windows.Forms.Label()
        Me.NumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.DropDownBox = New System.Windows.Forms.ComboBox()
        Me._Layout.SuspendLayout()
        Me._ButtonLayout.SuspendLayout()
        CType(Me.NumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TextBox
        '
        Me.TextBox.Dock = System.Windows.Forms.DockStyle.Top
        Me.TextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me.TextBox.Location = New System.Drawing.Point(9, 35)
        Me.TextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.TextBox.Name = "TextBox"
        Me.TextBox.Size = New System.Drawing.Size(252, 25)
        Me.TextBox.TabIndex = 28
        Me.TextBox.Text = "TextBox1"
        '
        '_cancelButton
        '
        Me._CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me._CancelButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._CancelButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._CancelButton.Location = New System.Drawing.Point(36, 4)
        Me._CancelButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._CancelButton.Name = "_CancelButton"
        Me._CancelButton.Size = New System.Drawing.Size(87, 30)
        Me._CancelButton.TabIndex = 27
        Me._CancelButton.Text = "&Cancel"
        '
        '_acceptButton
        '
        Me._AcceptButton.Enabled = False
        Me._AcceptButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._AcceptButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._AcceptButton.Location = New System.Drawing.Point(129, 4)
        Me._AcceptButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._AcceptButton.Name = "_AcceptButton"
        Me._AcceptButton.Size = New System.Drawing.Size(87, 30)
        Me._AcceptButton.TabIndex = 26
        Me._AcceptButton.Text = "&OK"
        '
        'TextBoxLabel
        '
        Me.TextBoxLabel.AutoSize = True
        Me.TextBoxLabel.BackColor = System.Drawing.SystemColors.Control
        Me.TextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBoxLabel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.TextBoxLabel.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.TextBoxLabel.ForeColor = System.Drawing.SystemColors.WindowText
        Me.TextBoxLabel.Location = New System.Drawing.Point(9, 14)
        Me.TextBoxLabel.Name = "TextBoxLabel"
        Me.TextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.TextBoxLabel.Size = New System.Drawing.Size(252, 17)
        Me.TextBoxLabel.TabIndex = 25
        Me.TextBoxLabel.Text = "Enter text: "
        Me.TextBoxLabel.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        '_Layout
        '
        Me._Layout.ColumnCount = 3
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 6.0!))
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 6.0!))
        Me._Layout.Controls.Add(Me.DropDownBoxLabel, 1, 7)
        Me._Layout.Controls.Add(Me._ButtonLayout, 1, 10)
        Me._Layout.Controls.Add(Me.TextBox, 1, 2)
        Me._Layout.Controls.Add(Me.TextBoxLabel, 1, 1)
        Me._Layout.Controls.Add(Me.NumericUpDownLabel, 1, 4)
        Me._Layout.Controls.Add(Me.NumericUpDown, 1, 5)
        Me._Layout.Controls.Add(Me.DropDownBox, 1, 8)
        Me._Layout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Layout.Location = New System.Drawing.Point(0, 0)
        Me._Layout.Name = "_Layout"
        Me._Layout.RowCount = 12
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 44.0!))
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me._Layout.Size = New System.Drawing.Size(270, 261)
        Me._Layout.TabIndex = 29
        '
        '_ButtonLayout
        '
        Me._ButtonLayout.ColumnCount = 4
        Me._ButtonLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ButtonLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._ButtonLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._ButtonLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ButtonLayout.Controls.Add(Me._AcceptButton, 2, 1)
        Me._ButtonLayout.Controls.Add(Me._CancelButton, 1, 1)
        Me._ButtonLayout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._ButtonLayout.Location = New System.Drawing.Point(9, 205)
        Me._ButtonLayout.Name = "_ButtonLayout"
        Me._ButtonLayout.RowCount = 3
        Me._ButtonLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ButtonLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._ButtonLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ButtonLayout.Size = New System.Drawing.Size(252, 38)
        Me._ButtonLayout.TabIndex = 30
        '
        'NumericUpDownLabel
        '
        Me.NumericUpDownLabel.AutoSize = True
        Me.NumericUpDownLabel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.NumericUpDownLabel.Location = New System.Drawing.Point(9, 78)
        Me.NumericUpDownLabel.Name = "NumericUpDownLabel"
        Me.NumericUpDownLabel.Size = New System.Drawing.Size(252, 17)
        Me.NumericUpDownLabel.TabIndex = 31
        Me.NumericUpDownLabel.Text = "Enter a number:"
        '
        'DropDownBoxLabel
        '
        Me.DropDownBoxLabel.AutoSize = True
        Me.DropDownBoxLabel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.DropDownBoxLabel.Location = New System.Drawing.Point(9, 140)
        Me.DropDownBoxLabel.Name = "DropDownBoxLabel"
        Me.DropDownBoxLabel.Size = New System.Drawing.Size(252, 17)
        Me.DropDownBoxLabel.TabIndex = 30
        Me.DropDownBoxLabel.Text = "Select item:"
        '
        'NumericUpDown
        '
        Me.NumericUpDown.Dock = System.Windows.Forms.DockStyle.Top
        Me.NumericUpDown.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown.Location = New System.Drawing.Point(9, 98)
        Me.NumericUpDown.Name = "NumericUpDown"
        Me.NumericUpDown.Size = New System.Drawing.Size(252, 25)
        Me.NumericUpDown.TabIndex = 32
        '
        'DropDownBox
        '
        Me.DropDownBox.Dock = System.Windows.Forms.DockStyle.Top
        Me.DropDownBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DropDownBox.FormattingEnabled = True
        Me.DropDownBox.Location = New System.Drawing.Point(9, 160)
        Me.DropDownBox.Name = "DropDownBox"
        Me.DropDownBox.Size = New System.Drawing.Size(252, 25)
        Me.DropDownBox.TabIndex = 33
        '
        'CompoundInputBox
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.ClientSize = New System.Drawing.Size(270, 261)
        Me.ControlBox = False
        Me.Controls.Add(Me._Layout)
        Me.Name = "CompoundInputBox"
        Me.Text = "Compound Input Box"
        Me._Layout.ResumeLayout(False)
        Me._Layout.PerformLayout()
        Me._ButtonLayout.ResumeLayout(False)
        CType(Me.NumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents TextBox As System.Windows.Forms.TextBox
    Private WithEvents _CancelButton As System.Windows.Forms.Button
    Private WithEvents _AcceptButton As System.Windows.Forms.Button
    Private WithEvents _ButtonLayout As System.Windows.Forms.TableLayoutPanel
    Public WithEvents TextBoxLabel As System.Windows.Forms.Label
    Public WithEvents DropDownBoxLabel As System.Windows.Forms.Label
    Public WithEvents NumericUpDownLabel As System.Windows.Forms.Label
    Public WithEvents NumericUpDown As System.Windows.Forms.NumericUpDown
    Public WithEvents DropDownBox As System.Windows.Forms.ComboBox
    Private WithEvents _Layout As System.Windows.Forms.TableLayoutPanel
End Class
