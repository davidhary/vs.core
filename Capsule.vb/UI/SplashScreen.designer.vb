<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class SplashScreen

    ''' <summary> The components. </summary>
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SplashScreen))
        Me._RightPanel = New System.Windows.Forms.Panel()
        Me._ProductNameLabel = New System.Windows.Forms.Label()
        Me._ProductTitleLabel = New System.Windows.Forms.Label()
        Me._ProductVersionLabel = New System.Windows.Forms.Label()
        Me._LicenseeLabel = New System.Windows.Forms.Label()
        Me._PlatformTitleLabel = New System.Windows.Forms.Label()
        Me._ProductFamilyLabel = New System.Windows.Forms.Label()
        Me._CompanyNameLabel = New System.Windows.Forms.Label()
        Me._LeftPanel = New System.Windows.Forms.Panel()
        Me._DisksPictureBox = New System.Windows.Forms.PictureBox()
        Me._CompanyLogoPictureBox = New System.Windows.Forms.PictureBox()
        Me._CancelButton = New System.Windows.Forms.Button()
        Me._StatusLabel = New System.Windows.Forms.Label()
        Me._CopyrightLabel = New System.Windows.Forms.Label()
        Me._CopyrightWarningLabel = New System.Windows.Forms.Label()
        Me._RightPanel.SuspendLayout()
        Me._LeftPanel.SuspendLayout()
        CType(Me._DisksPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._CompanyLogoPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_RightPanel
        '
        Me._RightPanel.Controls.Add(Me._ProductNameLabel)
        Me._RightPanel.Controls.Add(Me._ProductTitleLabel)
        Me._RightPanel.Controls.Add(Me._ProductVersionLabel)
        Me._RightPanel.Controls.Add(Me._LicenseeLabel)
        Me._RightPanel.Controls.Add(Me._PlatformTitleLabel)
        Me._RightPanel.Controls.Add(Me._ProductFamilyLabel)
        Me._RightPanel.Controls.Add(Me._CompanyNameLabel)
        Me._RightPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._RightPanel.Location = New System.Drawing.Point(95, 0)
        Me._RightPanel.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._RightPanel.Name = "_RightPanel"
        Me._RightPanel.Size = New System.Drawing.Size(426, 262)
        Me._RightPanel.TabIndex = 33
        '
        '_ProductNameLabel
        '
        Me._ProductNameLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(232, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(255, Byte), Integer))
        Me._ProductNameLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._ProductNameLabel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._ProductNameLabel.Font = New System.Drawing.Font("Segoe UI", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ProductNameLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(214, Byte), Integer), CType(CType(159, Byte), Integer), CType(CType(66, Byte), Integer))
        Me._ProductNameLabel.Location = New System.Drawing.Point(0, 90)
        Me._ProductNameLabel.Name = "_ProductNameLabel"
        Me._ProductNameLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ProductNameLabel.Size = New System.Drawing.Size(426, 43)
        Me._ProductNameLabel.TabIndex = 13
        Me._ProductNameLabel.Tag = "Product"
        Me._ProductNameLabel.Text = "Product"
        Me._ProductNameLabel.UseMnemonic = False
        '
        '_ProductTitleLabel
        '
        Me._ProductTitleLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(232, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(255, Byte), Integer))
        Me._ProductTitleLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._ProductTitleLabel.Dock = System.Windows.Forms.DockStyle.Top
        Me._ProductTitleLabel.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ProductTitleLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(214, Byte), Integer))
        Me._ProductTitleLabel.Location = New System.Drawing.Point(0, 59)
        Me._ProductTitleLabel.Name = "_ProductTitleLabel"
        Me._ProductTitleLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ProductTitleLabel.Size = New System.Drawing.Size(426, 31)
        Me._ProductTitleLabel.TabIndex = 26
        Me._ProductTitleLabel.Text = "Title"
        Me._ProductTitleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me._ProductTitleLabel.UseMnemonic = False
        '
        '_ProductVersionLabel
        '
        Me._ProductVersionLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(232, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(255, Byte), Integer))
        Me._ProductVersionLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._ProductVersionLabel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._ProductVersionLabel.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ProductVersionLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(148, Byte), Integer))
        Me._ProductVersionLabel.Location = New System.Drawing.Point(0, 133)
        Me._ProductVersionLabel.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._ProductVersionLabel.Name = "_ProductVersionLabel"
        Me._ProductVersionLabel.Padding = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._ProductVersionLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ProductVersionLabel.Size = New System.Drawing.Size(426, 39)
        Me._ProductVersionLabel.TabIndex = 16
        Me._ProductVersionLabel.Tag = "Version"
        Me._ProductVersionLabel.Text = "Version {0}.{1:00}.{2}"
        Me._ProductVersionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._ProductVersionLabel.UseMnemonic = False
        '
        '_LicenseeLabel
        '
        Me._LicenseeLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(232, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(255, Byte), Integer))
        Me._LicenseeLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._LicenseeLabel.Dock = System.Windows.Forms.DockStyle.Top
        Me._LicenseeLabel.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._LicenseeLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(214, Byte), Integer))
        Me._LicenseeLabel.Location = New System.Drawing.Point(0, 17)
        Me._LicenseeLabel.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._LicenseeLabel.Name = "_LicenseeLabel"
        Me._LicenseeLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._LicenseeLabel.Size = New System.Drawing.Size(426, 42)
        Me._LicenseeLabel.TabIndex = 25
        Me._LicenseeLabel.Text = "Licensee"
        Me._LicenseeLabel.UseMnemonic = False
        '
        '_PlatformTitleLabel
        '
        Me._PlatformTitleLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(232, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(255, Byte), Integer))
        Me._PlatformTitleLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._PlatformTitleLabel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._PlatformTitleLabel.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._PlatformTitleLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(148, Byte), Integer))
        Me._PlatformTitleLabel.Location = New System.Drawing.Point(0, 172)
        Me._PlatformTitleLabel.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._PlatformTitleLabel.Name = "_PlatformTitleLabel"
        Me._PlatformTitleLabel.Padding = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._PlatformTitleLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._PlatformTitleLabel.Size = New System.Drawing.Size(426, 27)
        Me._PlatformTitleLabel.TabIndex = 20
        Me._PlatformTitleLabel.Tag = "Platform"
        Me._PlatformTitleLabel.Text = "Platform"
        Me._PlatformTitleLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me._PlatformTitleLabel.UseMnemonic = False
        '
        '_ProductFamilyLabel
        '
        Me._ProductFamilyLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(232, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(255, Byte), Integer))
        Me._ProductFamilyLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._ProductFamilyLabel.Dock = System.Windows.Forms.DockStyle.Top
        Me._ProductFamilyLabel.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ProductFamilyLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(214, Byte), Integer))
        Me._ProductFamilyLabel.Location = New System.Drawing.Point(0, 0)
        Me._ProductFamilyLabel.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._ProductFamilyLabel.Name = "_ProductFamilyLabel"
        Me._ProductFamilyLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ProductFamilyLabel.Size = New System.Drawing.Size(426, 17)
        Me._ProductFamilyLabel.TabIndex = 20
        Me._ProductFamilyLabel.Text = "An Integrated Scientific Resources Product"
        Me._ProductFamilyLabel.UseMnemonic = False
        '
        '_CompanyNameLabel
        '
        Me._CompanyNameLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(232, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(255, Byte), Integer))
        Me._CompanyNameLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._CompanyNameLabel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._CompanyNameLabel.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._CompanyNameLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(214, Byte), Integer), CType(CType(159, Byte), Integer), CType(CType(66, Byte), Integer))
        Me._CompanyNameLabel.Location = New System.Drawing.Point(0, 199)
        Me._CompanyNameLabel.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._CompanyNameLabel.Name = "_CompanyNameLabel"
        Me._CompanyNameLabel.Padding = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._CompanyNameLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._CompanyNameLabel.Size = New System.Drawing.Size(426, 63)
        Me._CompanyNameLabel.TabIndex = 28
        Me._CompanyNameLabel.Tag = "Company"
        Me._CompanyNameLabel.Text = "Company"
        Me._CompanyNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._CompanyNameLabel.UseMnemonic = False
        '
        '_LeftPanel
        '
        Me._LeftPanel.Controls.Add(Me._DisksPictureBox)
        Me._LeftPanel.Controls.Add(Me._CompanyLogoPictureBox)
        Me._LeftPanel.Controls.Add(Me._CancelButton)
        Me._LeftPanel.Dock = System.Windows.Forms.DockStyle.Left
        Me._LeftPanel.Location = New System.Drawing.Point(0, 0)
        Me._LeftPanel.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._LeftPanel.Name = "_LeftPanel"
        Me._LeftPanel.Size = New System.Drawing.Size(95, 262)
        Me._LeftPanel.TabIndex = 34
        '
        '_DisksPictureBox
        '
        Me._DisksPictureBox.BackColor = System.Drawing.Color.Transparent
        Me._DisksPictureBox.Cursor = System.Windows.Forms.Cursors.Default
        Me._DisksPictureBox.Dock = System.Windows.Forms.DockStyle.Top
        Me._DisksPictureBox.Image = CType(resources.GetObject("_DisksPictureBox.Image"), System.Drawing.Image)
        Me._DisksPictureBox.Location = New System.Drawing.Point(0, 90)
        Me._DisksPictureBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._DisksPictureBox.Name = "_DisksPictureBox"
        Me._DisksPictureBox.Size = New System.Drawing.Size(95, 112)
        Me._DisksPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me._DisksPictureBox.TabIndex = 23
        Me._DisksPictureBox.TabStop = False
        '
        '_CompanyLogoPictureBox
        '
        Me._CompanyLogoPictureBox.BackColor = System.Drawing.Color.Transparent
        Me._CompanyLogoPictureBox.Cursor = System.Windows.Forms.Cursors.Default
        Me._CompanyLogoPictureBox.Dock = System.Windows.Forms.DockStyle.Top
        Me._CompanyLogoPictureBox.Image = CType(resources.GetObject("_CompanyLogoPictureBox.Image"), System.Drawing.Image)
        Me._CompanyLogoPictureBox.Location = New System.Drawing.Point(0, 0)
        Me._CompanyLogoPictureBox.Margin = New System.Windows.Forms.Padding(3, 13, 3, 4)
        Me._CompanyLogoPictureBox.Name = "_CompanyLogoPictureBox"
        Me._CompanyLogoPictureBox.Padding = New System.Windows.Forms.Padding(0, 4, 0, 0)
        Me._CompanyLogoPictureBox.Size = New System.Drawing.Size(95, 90)
        Me._CompanyLogoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me._CompanyLogoPictureBox.TabIndex = 23
        Me._CompanyLogoPictureBox.TabStop = False
        '
        '_CancelButton
        '
        Me._CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me._CancelButton.Location = New System.Drawing.Point(4, 130)
        Me._CancelButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._CancelButton.Name = "_CancelButton"
        Me._CancelButton.Size = New System.Drawing.Size(87, 30)
        Me._CancelButton.TabIndex = 0
        Me._CancelButton.Text = "Cancel"
        Me._CancelButton.UseVisualStyleBackColor = True
        '
        '_StatusLabel
        '
        Me._StatusLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(232, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(255, Byte), Integer))
        Me._StatusLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._StatusLabel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._StatusLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(148, Byte), Integer))
        Me._StatusLabel.Location = New System.Drawing.Point(0, 262)
        Me._StatusLabel.Name = "_StatusLabel"
        Me._StatusLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._StatusLabel.Size = New System.Drawing.Size(521, 35)
        Me._StatusLabel.TabIndex = 32
        Me._StatusLabel.Tag = "status"
        Me._StatusLabel.Text = "status"
        Me._StatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me._StatusLabel.UseMnemonic = False
        '
        '_CopyrightLabel
        '
        Me._CopyrightLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(232, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(255, Byte), Integer))
        Me._CopyrightLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._CopyrightLabel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._CopyrightLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(148, Byte), Integer))
        Me._CopyrightLabel.Location = New System.Drawing.Point(0, 297)
        Me._CopyrightLabel.Name = "_CopyrightLabel"
        Me._CopyrightLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._CopyrightLabel.Size = New System.Drawing.Size(521, 35)
        Me._CopyrightLabel.TabIndex = 31
        Me._CopyrightLabel.Tag = "Copyright"
        Me._CopyrightLabel.Text = "Copyright"
        Me._CopyrightLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me._CopyrightLabel.UseMnemonic = False
        '
        '_CopyrightWarningLabel
        '
        Me._CopyrightWarningLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(232, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(255, Byte), Integer))
        Me._CopyrightWarningLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._CopyrightWarningLabel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._CopyrightWarningLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(148, Byte), Integer))
        Me._CopyrightWarningLabel.Location = New System.Drawing.Point(0, 332)
        Me._CopyrightWarningLabel.Name = "_CopyrightWarningLabel"
        Me._CopyrightWarningLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._CopyrightWarningLabel.Size = New System.Drawing.Size(521, 35)
        Me._CopyrightWarningLabel.TabIndex = 30
        Me._CopyrightWarningLabel.Tag = "trademarks"
        Me._CopyrightWarningLabel.Text = "trademarks"
        Me._CopyrightWarningLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me._CopyrightWarningLabel.UseMnemonic = False
        '
        'SplashScreen
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(232, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CancelButton = Me._CancelButton
        Me.ClientSize = New System.Drawing.Size(521, 367)
        Me.ControlBox = False
        Me.Controls.Add(Me._RightPanel)
        Me.Controls.Add(Me._LeftPanel)
        Me.Controls.Add(Me._StatusLabel)
        Me.Controls.Add(Me._CopyrightLabel)
        Me.Controls.Add(Me._CopyrightWarningLabel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "SplashScreen"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me._RightPanel.ResumeLayout(False)
        Me._LeftPanel.ResumeLayout(False)
        CType(Me._DisksPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._CompanyLogoPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
        Private WithEvents _RightPanel As System.Windows.Forms.Panel
    ''' <summary> The with events control. </summary>
    Private WithEvents _ProductNameLabel As System.Windows.Forms.Label
    ''' <summary> The with events control. </summary>
    Private WithEvents _ProductTitleLabel As System.Windows.Forms.Label
    ''' <summary> The with events control. </summary>
    Private WithEvents _ProductVersionLabel As System.Windows.Forms.Label
    ''' <summary> The with events control. </summary>
    Private WithEvents _LicenseeLabel As System.Windows.Forms.Label
    ''' <summary> The with events control. </summary>
    Private WithEvents _PlatformTitleLabel As System.Windows.Forms.Label
    ''' <summary> The with events control. </summary>
    Private WithEvents _ProductFamilyLabel As System.Windows.Forms.Label
    ''' <summary> The with events control. </summary>
    Private WithEvents _CompanyNameLabel As System.Windows.Forms.Label
        Private WithEvents _LeftPanel As System.Windows.Forms.Panel
    ''' <summary> The with events control. </summary>
    Private WithEvents _DisksPictureBox As System.Windows.Forms.PictureBox
    ''' <summary> The with events control. </summary>
    Private WithEvents _CompanyLogoPictureBox As System.Windows.Forms.PictureBox
    ''' <summary> The with events control. </summary>
    Private WithEvents _StatusLabel As System.Windows.Forms.Label
    ''' <summary> The with events control. </summary>
    Private WithEvents _CopyrightLabel As System.Windows.Forms.Label
    ''' <summary> The with events control. </summary>
    Private WithEvents _CopyrightWarningLabel As System.Windows.Forms.Label
    ''' <summary> The with events control. </summary>
    Private WithEvents _CancelButton As System.Windows.Forms.Button
End Class
