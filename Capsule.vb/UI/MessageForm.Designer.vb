Partial Public Class MessageForm

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MessageForm))
        Me._RichTextBox = New System.Windows.Forms.RichTextBox()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me._CustomButton = New System.Windows.Forms.ToolStripDropDownButton()
        Me._StatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me._ProgressBar = New System.Windows.Forms.ToolStripProgressBar()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        '_RichTextBox
        '
        Me._RichTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._RichTextBox.Location = New System.Drawing.Point(0, 0)
        Me._RichTextBox.Name = "_RichTextBox"
        Me._RichTextBox.ReadOnly = True
        Me._RichTextBox.Size = New System.Drawing.Size(386, 133)
        Me._RichTextBox.TabIndex = 0
        Me._RichTextBox.Text = String.Empty
        Me._RichTextBox.WordWrap = False
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._StatusLabel, Me._ProgressBar, Me._CustomButton})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 133)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(386, 23)
        Me.StatusStrip1.TabIndex = 2
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        '_CustomButton
        '
        Me._CustomButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._CustomButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._CustomButton.Image = CType(resources.GetObject("_CustomButton.Image"), System.Drawing.Image)
        Me._CustomButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._CustomButton.Name = "_CustomButton"
        Me._CustomButton.ShowDropDownArrow = False
        Me._CustomButton.Size = New System.Drawing.Size(45, 21)
        Me._CustomButton.Text = "Abort"
        '
        '_StatusLabel
        '
        Me._StatusLabel.Name = "_StatusLabel"
        Me._StatusLabel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me._StatusLabel.Size = New System.Drawing.Size(193, 18)
        Me._StatusLabel.Spring = True
        Me._StatusLabel.Text = "Status"
        '
        '_ProgressBar
        '
        Me._ProgressBar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._ProgressBar.Name = "_ProgressBar"
        Me._ProgressBar.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ProgressBar.Size = New System.Drawing.Size(100, 17)
        '
        'MessageForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.ClientSize = New System.Drawing.Size(386, 156)
        Me.Controls.Add(Me._RichTextBox)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "MessageForm"
        Me.Text = "Status"
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _StatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Private WithEvents _CustomButton As System.Windows.Forms.ToolStripDropDownButton
    Private WithEvents _ProgressBar As System.Windows.Forms.ToolStripProgressBar
    Private WithEvents _RichTextBox As System.Windows.Forms.RichTextBox
    Private WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip

End Class
