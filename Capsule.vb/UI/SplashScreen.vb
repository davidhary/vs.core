''' <summary> Displays a splash screen. </summary>
''' <remarks> David, 02/17/2004. </remarks>
Public Class SplashScreen
    Inherits FormBase


    #Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of this class. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        Me.InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        If Version.Parse(My.Computer.Info.OSVersion).Major <= EnableDropShadowVersion Then
            Me.ClassStyle = ClassStyleConstants.DropShadow
        End If
        Me._Status = String.Empty
        Me._LicenseHeader = "Licensed to:"
        Me._LicenseeName = "Integrated Scientific Resources, Inc."
        Me._ProductFamilyName = "An Integrated Scientific Resources Product"
    End Sub

    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the
    ''' <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    #End Region 


    #Region " PROPERTIES and METHODS "

    ''' <summary> true to topmost. </summary>
    Private _Topmost As Boolean

    ''' <summary> Sets the top most status in a thread safe way. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="value"> true to value. </param>
    Public Sub TopmostSetter(ByVal value As Boolean)
        Me._Topmost = value
        SplashScreen.SafeTopMostSetter(Me, value)
    End Sub

    ''' <summary> Gets or sets the header for displaying the licensee name. </summary>
    ''' <value> <c>LicenseHeader</c>is a String property. </value>
    Public Property LicenseHeader() As String

    ''' <summary> Gets or sets the license code. </summary>
    ''' <value> <c>LicenseCode</c>is a String property. </value>
    Public Property LicenseCode() As String

    ''' <summary> Name of the licensee. </summary>
    Private _LicenseeName As String

    ''' <summary> Gets or sets the licensee name. </summary>
    ''' <value> <c>LicenseeName</c>is a String property. </value>
    Public Property LicenseeName() As String
        Get
            Return Me._LicenseeName
        End Get
        Set(ByVal value As String)
            Me._LicenseeName = value
            ' Display license information
            SplashScreen.SafeTextSetter(Me._LicenseeLabel, Me.BuildLicenseInfo)
            SplashScreen.SafeHeightSetter(Me._LicenseeLabel, Convert.ToInt32(Me.CreateGraphics().MeasureString(Me._LicenseeLabel.Text,
                                                                                                               Me._LicenseeLabel.Font).Height))
        End Set
    End Property

    ''' <summary> The platform title. </summary>
    Private _PlatformTitle As String = String.Empty

    ''' <summary> Gets or sets the platform title, e.g., 'For 32 Bit operating Systems'. </summary>
    ''' <value> <c>PlatformTitle</c>is a String property. </value>
    Public Property PlatformTitle() As String
        Get
            If String.IsNullOrWhiteSpace(Me._PlatformTitle) Then
                Me._PlatformTitle = "for 32/64-Bit Operating Systems"
            End If
            Return Me._PlatformTitle
        End Get
        Set(ByVal value As String)
            Me._PlatformTitle = value
        End Set
    End Property

    ''' <summary> Gets reference to the primary Picture Box. </summary>
    ''' <value> The primary picture box. </value>
    Public ReadOnly Property PrimaryPictureBox() As System.Windows.Forms.PictureBox
        Get
            Return Me._CompanyLogoPictureBox
        End Get
    End Property

    ''' <summary> Gets reference to the secondary Picture Box. </summary>
    ''' <value> The secondary picture box. </value>
    Public ReadOnly Property SecondaryPictureBox() As System.Windows.Forms.PictureBox
        Get
            Return Me._DisksPictureBox
        End Get
    End Property

    ''' <summary> Gets or sets the product family name, such as MyCompany Components. </summary>
    ''' <value> <c>ProductFamilyName</c>is a String property. </value>
    Public Property ProductFamilyName() As String

    ''' <summary> Updates all information. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Public Sub UpdateAllInfo()
        Me.StartUpdateInfo()
    End Sub

    ''' <summary> The status. </summary>
    Private _Status As String

    ''' <summary> Displays a message on the splash screen. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="value"> The message. </param>
    Public Sub DisplayMessage(ByVal value As String)
        Me._Status = value
        SplashScreen.SafeTextSetter(Me._StatusLabel, value)
    End Sub

    ''' <summary> Builds license information. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <returns> The license information. </returns>
    Private Function BuildLicenseInfo() As String
        ' Display license information
        Dim licenseBuilder As System.Text.StringBuilder = New System.Text.StringBuilder("")
        If Not ((String.IsNullOrWhiteSpace(Me.LicenseeName) OrElse String.IsNullOrWhiteSpace(Me.LicenseeName.Trim)) AndAlso
                 (String.IsNullOrWhiteSpace(Me.LicenseCode) OrElse String.IsNullOrWhiteSpace(Me.LicenseCode.Trim))) Then
            If Not (String.IsNullOrWhiteSpace(Me.LicenseHeader) OrElse String.IsNullOrWhiteSpace(Me.LicenseHeader.Trim)) Then
                licenseBuilder.AppendFormat(Globalization.CultureInfo.CurrentCulture, "{1}{0}", Environment.NewLine, Me.LicenseHeader.Trim)
            End If
            If Not (String.IsNullOrWhiteSpace(Me.LicenseeName) OrElse String.IsNullOrWhiteSpace(Me.LicenseeName.Trim)) Then
                licenseBuilder.AppendFormat(Globalization.CultureInfo.CurrentCulture, "  {1}{0}", Environment.NewLine, Me.LicenseeName.Trim)
            End If
            If Not (String.IsNullOrWhiteSpace(Me.LicenseCode) OrElse String.IsNullOrWhiteSpace(Me.LicenseCode.Trim)) Then
                licenseBuilder.AppendFormat(Globalization.CultureInfo.CurrentCulture, "  {1}{0}", Environment.NewLine, Me.LicenseCode.Trim)
            End If
        ElseIf Not (String.IsNullOrWhiteSpace(Me.LicenseHeader) OrElse String.IsNullOrWhiteSpace(Me.LicenseHeader.Trim)) Then
            licenseBuilder.AppendFormat(Globalization.CultureInfo.CurrentCulture, "{1}{0}", Environment.NewLine, Me.LicenseHeader.Trim)
        Else
            licenseBuilder.AppendFormat(Globalization.CultureInfo.CurrentCulture, "{1}{0}", Environment.NewLine, My.Application.Info.CompanyName)
        End If
        Return licenseBuilder.ToString
    End Function

    ''' <summary> Update the information on screen. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Private Sub UpdateInfo()

        Me._ProductFamilyLabel.Text = Me.ProductFamilyName
        Me._ProductFamilyLabel.Invalidate()
        Me._ProductTitleLabel.Text = My.Application.Info.Title
        Me._ProductTitleLabel.Invalidate()
        Me._ProductNameLabel.Text = My.Application.Info.ProductName
        Me._ProductNameLabel.Invalidate()
        Me._ProductVersionLabel.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "Version {0}", My.Application.Info.Version)
        Me._ProductVersionLabel.Invalidate()
        Me._PlatformTitleLabel.Text = Me.PlatformTitle
        Me._PlatformTitleLabel.Invalidate()
        Me._CompanyNameLabel.Text = My.Application.Info.CompanyName
        Me._CompanyNameLabel.Invalidate()
        Me._CopyrightLabel.Text = My.Application.Info.Copyright
        Me._CopyrightLabel.Invalidate()
        Me._CopyrightWarningLabel.Text = My.Application.Info.Trademark
        Me._CopyrightWarningLabel.Invalidate()
        Windows.Forms.Application.DoEvents()

        ' Display license information
        Me._LicenseeLabel.Text = Me.BuildLicenseInfo
        Me._LicenseeLabel.Height = Convert.ToInt32(Me.CreateGraphics().MeasureString(Me._LicenseeLabel.Text, Me._LicenseeLabel.Font).Height)
        Me._LicenseeLabel.Invalidate()
        Windows.Forms.Application.DoEvents()

        Me._StatusLabel.Text = Me._Status
        Me._StatusLabel.Invalidate()

        Me.TopMost = Me._Topmost
        Windows.Forms.Application.DoEvents()

    End Sub

    #End Region 


    #Region " THREAD SAFE METHODS "

    ''' <summary> Safe height setter. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="control"> The control. </param>
    ''' <param name="value">   true to value. </param>
    Private Shared Sub SafeHeightSetter(ByVal control As System.Windows.Forms.Control, ByVal value As Integer)
        If control IsNot Nothing Then
            If control.InvokeRequired Then
                control.Invoke(New Action(Of System.Windows.Forms.Control, Integer)(AddressOf SplashScreen.SafeHeightSetter), New Object() {control, value})
            Else
                control.Height = value
                control.Invalidate()
                Windows.Forms.Application.DoEvents()
            End If
        End If
    End Sub

    ''' <summary>
    ''' Sets the <see cref="System.Windows.Forms.Control">control</see> text to the
    ''' <paramref name="value">value</paramref>.
    ''' This setter is thread safe.
    ''' </summary>
    ''' <remarks> The value is set to empty if null or empty. </remarks>
    ''' <param name="control"> The control. </param>
    ''' <param name="value">   The value. </param>
    ''' <returns> value. </returns>
    Private Shared Function SafeTextSetter(ByVal control As System.Windows.Forms.Control, ByVal value As String) As String
        If control IsNot Nothing Then
            If String.IsNullOrWhiteSpace(value) Then
                value = String.Empty
            End If
            If control.InvokeRequired Then
                control.Invoke(New Action(Of System.Windows.Forms.Control, String)(AddressOf SplashScreen.SafeTextSetter), New Object() {control, value})
                control.Invoke(New Func(Of System.Windows.Forms.Control, String, String)(AddressOf SplashScreen.SafeTextSetter), New Object() {control, value})
            Else
                control.Text = value
                control.Invalidate()
                Windows.Forms.Application.DoEvents()
            End If
        End If
        Return value
    End Function

    ''' <summary> Safe top most setter. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="form">  The form. </param>
    ''' <param name="value"> true to value. </param>
    Private Shared Sub SafeTopMostSetter(ByVal form As System.Windows.Forms.Form, ByVal value As Boolean)
        If form IsNot Nothing Then
            If form.InvokeRequired Then
                form.Invoke(New Action(Of System.Windows.Forms.Form, Boolean)(AddressOf SplashScreen.SafeTopMostSetter), New Object() {form, value})
            Else
                form.TopMost = value
                Windows.Forms.Application.DoEvents()
            End If
        End If
    End Sub

    ''' <summary> Start the information updating thread. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Private Sub StartUpdateInfo()
        If Me.InvokeRequired Then
            Me.Invoke(New Action(AddressOf Me.UpdateInfo))
        Else
            Me.UpdateInfo()
        End If
    End Sub

    #End Region 


    #Region " FORM EVENT HANDLERS "

    ''' <summary>
    ''' Does all the post processing after all the form controls are rendered as the user expects
    ''' them.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub Form_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        Windows.Forms.Application.DoEvents()
        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            ' instantiate form objects
            Me.StartUpdateInfo()
        Catch ex As Exception
            MyMessageBox.ShowDialog(ex)
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
            Windows.Forms.Application.DoEvents()
        End Try
    End Sub

    #End Region 


    #Region " CONTROL EVENT HANDLERS "

    ''' <summary> Hides the splash screen. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub CancelButtonClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _CancelButton.Click
        Me.Close()
    End Sub

    #End Region 

End Class
