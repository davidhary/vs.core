﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ConfigurationEditor

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me._PropertyGrid = New System.Windows.Forms.PropertyGrid()
        Me._MenuStrip = New System.Windows.Forms.MenuStrip()
        Me._FileMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ResetMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ReadMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SaveMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SaveExitMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._FileSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me._ExitMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._AcceptButton = New System.Windows.Forms.Button()
        Me._IgnoreButton = New System.Windows.Forms.Button()
        Me._MenuStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_PropertyGrid
        '
        Me._PropertyGrid.Dock = System.Windows.Forms.DockStyle.Fill
        Me._PropertyGrid.Location = New System.Drawing.Point(0, 24)
        Me._PropertyGrid.Name = "_PropertyGrid"
        Me._PropertyGrid.Size = New System.Drawing.Size(427, 383)
        Me._PropertyGrid.TabIndex = 0
        '
        '_MenuStrip
        '
        Me._MenuStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._FileMenuItem})
        Me._MenuStrip.Location = New System.Drawing.Point(0, 0)
        Me._MenuStrip.Name = "_MenuStrip"
        Me._MenuStrip.Size = New System.Drawing.Size(427, 24)
        Me._MenuStrip.TabIndex = 1
        Me._MenuStrip.Text = "Menu Strip"
        '
        '_FileMenuItem
        '
        Me._FileMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ResetMenuItem, Me._ReadMenuItem, Me._SaveMenuItem, Me._SaveExitMenuItem, Me._FileSeparator1, Me._ExitMenuItem})
        Me._FileMenuItem.Name = "_FileMenuItem"
        Me._FileMenuItem.Size = New System.Drawing.Size(37, 20)
        Me._FileMenuItem.Text = "&File"
        '
        '_ResetMenuItem
        '
        Me._ResetMenuItem.Name = "_ResetMenuItem"
        Me._ResetMenuItem.Size = New System.Drawing.Size(180, 22)
        Me._ResetMenuItem.Text = "R&eset"
        Me._ResetMenuItem.ToolTipText = "Restores the persisted application settings values to their corresponding default" &
    " properties"
        '
        '_ReadMenuItem
        '
        Me._ReadMenuItem.Name = "_ReadMenuItem"
        Me._ReadMenuItem.Size = New System.Drawing.Size(180, 22)
        Me._ReadMenuItem.Text = "&Read"
        '
        '_SaveMenuItem
        '
        Me._SaveMenuItem.Name = "_SaveMenuItem"
        Me._SaveMenuItem.Size = New System.Drawing.Size(180, 22)
        Me._SaveMenuItem.Text = "&Save"
        '
        '_SaveExitMenuItem
        '
        Me._SaveExitMenuItem.Name = "_SaveExitMenuItem"
        Me._SaveExitMenuItem.Size = New System.Drawing.Size(180, 22)
        Me._SaveExitMenuItem.Text = "S&ave and Exit"
        '
        '_FileSeparator1
        '
        Me._FileSeparator1.Name = "_FileSeparator1"
        Me._FileSeparator1.Size = New System.Drawing.Size(177, 6)
        '
        '_ExitMenuItem
        '
        Me._ExitMenuItem.Name = "_ExitMenuItem"
        Me._ExitMenuItem.Size = New System.Drawing.Size(180, 22)
        Me._ExitMenuItem.Text = "E&xit"
        '
        '_AcceptButton
        '
        Me._AcceptButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me._AcceptButton.Location = New System.Drawing.Point(305, 79)
        Me._AcceptButton.Name = "_AcceptButton"
        Me._AcceptButton.Size = New System.Drawing.Size(75, 34)
        Me._AcceptButton.TabIndex = 2
        Me._AcceptButton.Text = "OK"
        Me._AcceptButton.UseVisualStyleBackColor = True
        '
        '_IgnoreButton
        '
        Me._IgnoreButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me._IgnoreButton.Location = New System.Drawing.Point(305, 79)
        Me._IgnoreButton.Name = "_IgnoreButton"
        Me._IgnoreButton.Size = New System.Drawing.Size(75, 34)
        Me._IgnoreButton.TabIndex = 2
        Me._IgnoreButton.Text = "OK"
        Me._IgnoreButton.UseVisualStyleBackColor = True
        '
        'ConfigurationEditor
        '
        Me.AcceptButton = Me._AcceptButton
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me._IgnoreButton
        Me.ClientSize = New System.Drawing.Size(427, 407)
        Me.Controls.Add(Me._PropertyGrid)
        Me.Controls.Add(Me._MenuStrip)
        Me.Controls.Add(Me._AcceptButton)
        Me.Controls.Add(Me._IgnoreButton)
        Me.MainMenuStrip = Me._MenuStrip
        Me.Name = "ConfigurationEditor"
        Me.Text = "Configuration Editor"
        Me._MenuStrip.ResumeLayout(False)
        Me._MenuStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Private WithEvents _PropertyGrid As System.Windows.Forms.PropertyGrid
    Private WithEvents _MenuStrip As System.Windows.Forms.MenuStrip
    Private WithEvents _FileMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _ReadMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _FileSeparator1 As System.Windows.Forms.ToolStripSeparator
    Private WithEvents _ExitMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _AcceptButton As System.Windows.Forms.Button
    Private WithEvents _IgnoreButton As System.Windows.Forms.Button
    Private WithEvents _SaveMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _SaveExitMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _ResetMenuItem As Windows.Forms.ToolStripMenuItem
End Class
