''' <summary> The windows forms. </summary>
''' <remarks> David, 1/19/2019. </remarks>
Public NotInheritable Class WindowsForms

    #Region " CONSTRUCTION "

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Private Sub New()
        MyBase.New
    End Sub

    #End Region 

    #Region " ABOUT "

    ''' <summary> Displays an about. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Public Shared Sub DisplayAbout()
        ' display the application information
        Using aboutScreen As New About
            aboutScreen.TopMost = True
            aboutScreen.Icon = My.Resources.favicon
            aboutScreen.ShowDialog(System.Reflection.Assembly.GetExecutingAssembly,
                                   "Integrated Scientific Resources, Inc.", "ABC=123=ZXCA", "My System ID", "Licensed to:")
        End Using
    End Sub

    #End Region 


    #Region " MESAGE FORM "

    ''' <summary> Prompt while waiting for action to complete. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="timeout"> The timeout. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <param name="details"> The details. </param>
    ''' <param name="action">  The action. </param>
    Public Shared Sub ShowMessageForm(ByVal timeout As TimeSpan, ByVal caption As String, ByVal details As String, ByVal action As Action)
        If action Is Nothing Then Throw New ArgumentNullException(NameOf(action))
        Using msg As MessageForm = MessageForm.Get
            msg.Show(Nothing, caption, details, timeout)
            action.Invoke
        End Using
    End Sub

    ''' <summary> Prompt while waiting for a task to complete. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="timeout"> The timeout. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <param name="details"> The details. </param>
    ''' <param name="task">    The task. </param>
    Public Shared Sub ShowMessageForm(ByVal timeout As TimeSpan, ByVal caption As String, ByVal details As String, ByVal task As Task)
        If task Is Nothing Then Throw New ArgumentNullException(NameOf(task))
        Using msg As MessageForm = MessageForm.Get
            msg.Show(Nothing, caption, details)
            task.Wait(timeout)
        End Using
    End Sub

    #End Region 


    #Region " SPLASH "

    ''' <summary> Gets or sets the splash screen. </summary>
    ''' <value> The splash screen. </value>
    Private Shared ReadOnly Property SplashScreen As SplashScreen

    ''' <summary> Displays a splash screen. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Public Shared Sub ShowSplashScreen()
        WindowsForms._SplashScreen = New SplashScreen()
        SplashScreen.Show()
        SplashScreen.TopmostSetter(True)
    End Sub

    ''' <summary> Displays a splash message described by value. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="value"> The value. </param>
    Public Shared Sub DisplaySplashMessage(ByVal value As String)
        WindowsForms.SplashScreen?.DisplayMessage(value)
    End Sub

    ''' <summary> Closes splash screen. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Public Shared Sub CloseSplashScreen()
        If WindowsForms.SplashScreen IsNot Nothing Then
            WindowsForms.SplashScreen.Close()
            WindowsForms.SplashScreen.Dispose()
        End If
        WindowsForms._SplashScreen = Nothing
    End Sub

    ''' <summary> Displays a splash screen. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Public Shared Sub DisplaySplashScreen()
        Using splashScreen As SplashScreen = New SplashScreen()
            splashScreen.Show()
            splashScreen.TopmostSetter(True)
            WindowsForms.Display(splashScreen)
        End Using
    End Sub

    ''' <summary> Displays the given target. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="splashScreen"> The splash screen. </param>
    Private Shared Sub Display(ByVal splashScreen As SplashScreen)
        splashScreen.DisplayMessage($"Splash message {DateTimeOffset.Now:F)}")
        My.MyLibrary.DoEvents()
        My.MyLibrary.Delay(TimeSpan.FromMilliseconds(200))
        My.MyLibrary.DoEvents()
    End Sub

    ''' <summary> Send key strokes to the active application. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="keys"> The keys. </param>
    Public Shared Sub SendKeys(ByVal keys As String)
        System.Windows.Forms.SendKeys.Send(keys)
    End Sub

    ''' <summary>
    ''' Send the given keys to the active application and then wait for the message to be processed.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="keys"> The keys. </param>
    Public Shared Sub SendWait(ByVal keys As String)
        System.Windows.Forms.SendKeys.SendWait(keys)
    End Sub

    #End Region 

End Class
