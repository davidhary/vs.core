Imports System.Drawing
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class TextInputBox

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me._EnteredValueTextBox = New System.Windows.Forms.TextBox()
        Me._CancelButton = New System.Windows.Forms.Button()
        Me._AcceptButton = New System.Windows.Forms.Button()
        Me._EnteredValueTextBoxLabel = New System.Windows.Forms.Label()
        Me._ValidationErrorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        CType(Me._ValidationErrorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_EnteredValueTextBox
        '
        Me._EnteredValueTextBox.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._EnteredValueTextBox.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._EnteredValueTextBox.Location = New System.Drawing.Point(9, 27)
        Me._EnteredValueTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._EnteredValueTextBox.Multiline = True
        Me._EnteredValueTextBox.Name = "_EnteredValueTextBox"
        Me._EnteredValueTextBox.Size = New System.Drawing.Size(186, 20)
        Me._EnteredValueTextBox.TabIndex = 28
        '
        '_CancelButton
        '
        Me._CancelButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me._CancelButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._CancelButton.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._CancelButton.Location = New System.Drawing.Point(9, 59)
        Me._CancelButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._CancelButton.Name = "_CancelButton"
        Me._CancelButton.Size = New System.Drawing.Size(87, 30)
        Me._CancelButton.TabIndex = 27
        Me._CancelButton.Text = "&Cancel"
        '
        '_AcceptButton
        '
        Me._AcceptButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._AcceptButton.Enabled = False
        Me._AcceptButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._AcceptButton.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._AcceptButton.Location = New System.Drawing.Point(108, 59)
        Me._AcceptButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._AcceptButton.Name = "_AcceptButton"
        Me._AcceptButton.Size = New System.Drawing.Size(87, 30)
        Me._AcceptButton.TabIndex = 26
        Me._AcceptButton.Text = "&OK"
        '
        '_EnteredValueTextBoxLabel
        '
        Me._EnteredValueTextBoxLabel.AutoSize = True
        Me._EnteredValueTextBoxLabel.BackColor = System.Drawing.SystemColors.Control
        Me._EnteredValueTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._EnteredValueTextBoxLabel.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._EnteredValueTextBoxLabel.ForeColor = System.Drawing.SystemColors.WindowText
        Me._EnteredValueTextBoxLabel.Location = New System.Drawing.Point(9, 7)
        Me._EnteredValueTextBoxLabel.Name = "_EnteredValueTextBoxLabel"
        Me._EnteredValueTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._EnteredValueTextBoxLabel.Size = New System.Drawing.Size(91, 17)
        Me._EnteredValueTextBoxLabel.TabIndex = 25
        Me._EnteredValueTextBoxLabel.Text = "Enter a Value: "
        Me._EnteredValueTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        '_ValidationErrorProvider
        '
        Me._ValidationErrorProvider.ContainerControl = Me
        '
        'TextInputBox
        '
        Me.AcceptButton = Me._AcceptButton
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.CancelButton = Me._CancelButton
        Me.ClientSize = New System.Drawing.Size(203, 95)
        Me.ControlBox = False
        Me.Controls.Add(Me._EnteredValueTextBox)
        Me.Controls.Add(Me._CancelButton)
        Me.Controls.Add(Me._AcceptButton)
        Me.Controls.Add(Me._EnteredValueTextBoxLabel)
        Me.Name = "TextInputBox"
        Me.Text = "InputBox"
        CType(Me._ValidationErrorProvider, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _EnteredValueTextBox As System.Windows.Forms.TextBox
    Private WithEvents _CancelButton As System.Windows.Forms.Button
    Private WithEvents _AcceptButton As System.Windows.Forms.Button
    Private WithEvents _EnteredValueTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _ValidationErrorProvider As System.Windows.Forms.ErrorProvider
End Class
