Imports System.Security.Permissions

''' <summary> Displays assembly information. </summary>
''' <remarks>
''' David, 09/21/2002. This is the information form for assemblies. To open, instantiate the form
''' passing the new instance the module file version information reference.
''' </remarks>
Public Class About
    Inherits FormBase


    #Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Public Sub New()
        MyBase.New()

        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()

        ' Add any initialization after the InitializeComponent() call
        ' onInstantiate()
        If Version.Parse(My.Computer.Info.OSVersion).Major <= EnableDropShadowVersion Then
            Me.ClassStyle = ClassStyleConstants.DropShadow
        End If

    End Sub

    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the
    ''' <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub


    #End Region 


    #Region " METHODS "

    ''' <summary> Displays this module. </summary>
    ''' <remarks>
    ''' Use this method to display the application main form. You must also call Application.Run with
    ''' reference to this form because the method shows the form modeless.
    ''' </remarks>
    ''' <exception cref="ArgumentNullException" guarantee="strong"> Failed to show main form. </exception>
    ''' <param name="executionAssembly"> Specifies the
    '''                                  <see cref="System.Reflection.Assembly">calling
    '''                                  assembly</see>. </param>
    ''' <param name="licenseeName">      A <see cref="System.String">String</see> expression that
    '''                                  specifies the name of the licensee. </param>
    ''' <param name="licenseCode">       A <see cref="System.String">String</see> expression that
    '''                                  specifies the license string (serial number) </param>
    ''' <param name="systemId">          A <see cref="System.String">String</see> expression that
    '''                                  specifies the system id, e.g., the Product Name. </param>
    ''' <param name="licenseHeader">     A <see cref="System.String">String</see> expression that
    '''                                  specifies the license heading under which the licensee name
    '''                                  and code are displayed.  For example:  "This product is
    '''                                  licensed to:". </param>
    ''' <example>
    ''' This example displays the About form.
    ''' <code>
    ''' Private Sub DisplayAboutForm()
    ''' ' display the application information
    ''' Dim aboutScreen As New isr.WindowsForms.About
    ''' aboutScreen.ShowDialog(System.Reflection.Assembly.GetExecutingAssembly,
    ''' String.Empty, String.Empty, String.Empty, String.Empty)
    ''' End Sub
    ''' </code>
    ''' To run this example, paste the code fragment into the method region of a Visual Basic form.
    ''' Run the program by pressing F5.
    ''' </example>
    <FileIOPermission(SecurityAction.Demand, Unrestricted:=True)>
    Public Overloads Sub Show(ByVal executionAssembly As System.Reflection.Assembly,
                              ByVal licenseeName As String, ByVal licenseCode As String,
                              ByVal systemId As String, ByVal licenseHeader As String)

        If executionAssembly Is Nothing Then Throw New ArgumentNullException(NameOf(executionAssembly))
        If licenseeName Is Nothing Then Throw New ArgumentNullException(NameOf(licenseeName))
        If licenseCode Is Nothing Then Throw New ArgumentNullException(NameOf(licenseCode))
        If systemId Is Nothing Then Throw New ArgumentNullException(NameOf(systemId))
        If licenseHeader Is Nothing Then Throw New ArgumentNullException(NameOf(licenseHeader))

        ' process the form show methods
        Me.BeforeFormLoad(executionAssembly, licenseeName, licenseCode, systemId, licenseHeader, "Product Information")

        ' show the form
        Me.Show()

    End Sub

    ''' <summary> Displays this module. </summary>
    ''' <remarks>
    ''' Use this method to display the form. If you prefer to use the Show Form statement, make sure
    ''' that all the errors trapped in the Form_Load event are handled within the form as these
    ''' errors cannot be raised to the calling function.
    ''' </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="executionAssembly"> Specifies the
    '''                                  <see cref="System.Reflection.Assembly">calling
    '''                                  assembly</see>. </param>
    ''' <param name="licenseeName">      is an optional String expression that specifies the name of
    '''                                  the licensee. </param>
    ''' <param name="licenseCode">       is an optional String expression that specifies the license
    '''                                  string (serial number) </param>
    ''' <param name="systemId">          is an optional String expression that specifies the system id,
    '''                                  e.g., the Product Name. </param>
    ''' <param name="licenseHeader">     A <see cref="System.String">String</see> expression that
    '''                                  specifies the license heading under which the licensee name
    '''                                  and code are displayed.  For example:  "This product Is
    '''                                  licensed to". </param>
    ''' <returns> A Dialog Result. </returns>
    ''' <example>
    ''' This example displays the About form.
    ''' <code>
    ''' Private Sub DisplayAboutForm()
    ''' ' display the application information
    ''' Dim aboutScreen As New isr.WindowsForms.About
    ''' aboutScreen.ShowDialog(support.GetFileVersionInfo(),
    ''' String.Empty, String.Empty, String.Empty, String.Empty)
    ''' End Sub
    ''' </code>
    ''' To run this example, paste the code fragment into the method region of a Visual Basic form.
    ''' Run the program by pressing F5.
    ''' </example>
    <System.Security.Permissions.FileIOPermission(SecurityAction.Demand, Unrestricted:=True)>
    Public Overloads Function ShowDialog(ByVal executionAssembly As System.Reflection.Assembly,
                                         ByVal licenseeName As String,
                                         ByVal licenseCode As String,
                                         ByVal systemId As String,
                                         ByVal licenseHeader As String) As System.Windows.Forms.DialogResult

        If executionAssembly Is Nothing Then Throw New ArgumentNullException(NameOf(executionAssembly))
        If licenseeName Is Nothing Then Throw New ArgumentNullException(NameOf(licenseeName))
        If licenseCode Is Nothing Then Throw New ArgumentNullException(NameOf(licenseCode))
        If systemId Is Nothing Then Throw New ArgumentNullException(NameOf(systemId))
        If licenseHeader Is Nothing Then Throw New ArgumentNullException(NameOf(licenseHeader))

        ' process the form show methods
        Me.BeforeFormLoad(executionAssembly, licenseeName, licenseCode, systemId, licenseHeader, "Product Information")

        ' show the form
        Return Me.ShowDialog()

    End Function

    #End Region 


    #Region " PROPERTIES "

    ''' <summary> Gets or sets the status message. </summary>
    ''' <remarks> Use this property to get the status message generated by the object. </remarks>
    ''' <value> A <see cref="System.String">String</see>. </value>
    Public ReadOnly Property StatusMessage() As String

    ''' <summary> Gets or sets the image. </summary>
    ''' <value> The image. </value>
    Public Property Image() As System.Drawing.Icon
        Get
            Return Me.Icon
        End Get
        Set(ByVal value As System.Drawing.Icon)
            If value IsNot Nothing Then
                Me._IconPictureBox.Image = value.ToBitmap
                Me._IconPictureBox.Invalidate()
                If Me._IconPictureBox.Left + Me._IconPictureBox.Width > Me.Width Then
                    If Me._IconPictureBox.Width < 0.2 * Me.Width Then
                        Me._IconPictureBox.Left = Me.ClientSize.Width - Me._IconPictureBox.Width - 5
                    End If
                End If
            End If
        End Set
    End Property

    #End Region 


    #Region " CONTROL EVENT HANDLERS "

    ''' <summary> Occurs when the user selects the exit button. </summary>
    ''' <remarks> Use this method to exit. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                       <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ExitButtonClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ExitButton.Click
        ' Close this form
        Me.Close()
    End Sub

    #End Region 


    #Region " PRIVATE  and  PROTECTED "

    ''' <summary> Initializes the user interface and tool tips. </summary>
    ''' <remarks> Call this method from the form load method to set the user interface. </remarks>
    Private Sub InitializeUserInterface()
        Me._IconPictureBox.Image = Me.Icon.ToBitmap
    End Sub

    ''' <summary> Processes the display of this module. </summary>
    ''' <remarks>
    ''' David, 02/09/2008. Use File Description (Application Title Property) for the product name and
    ''' remove it from the comments section. Use product version instead of file version.
    ''' </remarks>
    ''' <exception cref="ArgumentNullException" guarantee="strong"> Failed to show main form. </exception>
    ''' <param name="executionAssembly"> Specifies the
    '''                                  <see cref="System.Reflection.Assembly">calling
    '''                                  assembly</see>. </param>
    ''' <param name="licenseeName">      A <see cref="System.String">String</see> expression that
    '''                                  specifies the name of the licensee. </param>
    ''' <param name="licenseCode">       A <see cref="System.String">String</see> expression that
    '''                                  specifies the license string (serial number) </param>
    ''' <param name="systemId">          A <see cref="System.String">String</see> expression that
    '''                                  specifies the system id, e.g., the Product Name. </param>
    ''' <param name="licenseHeader">     A <see cref="System.String">String</see> expression that
    '''                                  specifies the license heading under which the licensee name
    '''                                  and code are displayed.  For example:  "This product is
    '''                                  licensed to:". </param>
    ''' <param name="caption">           The caption. </param>
    <System.Security.Permissions.FileIOPermission(SecurityAction.Demand, Unrestricted:=True)>
    Private Sub BeforeFormLoad(ByVal executionAssembly As System.Reflection.Assembly,
                               ByVal licenseeName As String,
                               ByVal licenseCode As String,
                               ByVal systemId As String,
                               ByVal licenseHeader As String,
                               ByVal caption As String)

        If executionAssembly Is Nothing Then Throw New ArgumentNullException(NameOf(executionAssembly))
        If licenseeName Is Nothing Then Throw New ArgumentNullException(NameOf(licenseeName))
        If licenseCode Is Nothing Then Throw New ArgumentNullException(NameOf(licenseCode))
        If systemId Is Nothing Then Throw New ArgumentNullException(NameOf(systemId))
        If licenseHeader Is Nothing Then Throw New ArgumentNullException(NameOf(licenseHeader))

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' hide the cancel button
            Me._ExitButton.Top = -Me._ExitButton.Height

            Dim fileVersionInfo As System.Diagnostics.FileVersionInfo =
                System.Diagnostics.FileVersionInfo.GetVersionInfo(executionAssembly.Location)

            Dim rowTop As Int32
            Dim rowText As System.Text.StringBuilder = New System.Text.StringBuilder("")
            Dim topMargin As Int32 = 1
            ' Dim leftMargin As Int32 = 2
            Dim rowSpace As Int32

            ' set the caption
            Me.Text = If(caption.Length > 0, caption, "Registration Information")

            rowSpace = Me._ProductTitleLabel.Height \ 2
            Me._ProductTitleLabel.Text = executionAssembly.GetName.Name()
            Me._ProductTitleLabel.Refresh()
            Me._ProductTitleLabel.Top = topMargin
            ' Me._productTitleLabel.Left = leftMargin
            ' Me._productTitleLabel.Width = 1 + Int32.parse(Me.CreateGraphics().MeasureString(Me._productTitleLabel.text, Me._productTitleLabel.Font).Width, System.Globalization.CultureInfo.CurrentCulture)
            Me._ProductTitleLabel.Height = Convert.ToInt32(Me.CreateGraphics().MeasureString(Me._ProductTitleLabel.Text, Me._ProductTitleLabel.Font).Height, System.Globalization.CultureInfo.CurrentCulture)
            Me._ProductTitleLabel.Invalidate()
            rowTop = Me._ProductTitleLabel.Top + Me._ProductTitleLabel.Height + rowSpace

            Me._ProductNameLabel.Text = fileVersionInfo.FileDescription
            Me._ProductNameLabel.Refresh()
            ' Me._productNameLabel.Width = 1 + Int32.parse(Me._productNameLabel.CreateGraphics().MeasureString(Me._productNameLabel.text, Me._productNameLabel.Font).Width, System.Globalization.CultureInfo.CurrentCulture)
            Me._ProductNameLabel.Height = Convert.ToInt32(Me._ProductNameLabel.CreateGraphics().MeasureString(Me._ProductNameLabel.Text, Me._ProductNameLabel.Font).Height, System.Globalization.CultureInfo.CurrentCulture)
            ' Me._productNameLabel.Left = leftMargin
            Me._ProductNameLabel.Top = rowTop
            Me._ProductNameLabel.Invalidate()
            rowTop = Me._ProductNameLabel.Top + Me._ProductNameLabel.Height + rowSpace

            Me._IconPictureBox.Top = topMargin

            If fileVersionInfo.Comments.Trim.Length > 0 Then
                rowText.Append(fileVersionInfo.Comments.Trim)
            End If
            If systemId.Trim.Length > 0 Then
                rowText.AppendFormat(Globalization.CultureInfo.CurrentCulture, "{0}{1}", Environment.NewLine, systemId.Trim)
            End If
            rowText.AppendFormat(Globalization.CultureInfo.CurrentCulture, "{0}Version: {1} updated {2}", Environment.NewLine,
                fileVersionInfo.ProductVersion,
                System.IO.File.GetLastWriteTime(System.Windows.Forms.Application.StartupPath))

            Me._DescriptionLabel.Text = rowText.ToString
            Me._DescriptionLabel.Refresh()
            ' Me._descriptionLabel.Width = 1 + Int32.parse(Me.CreateGraphics().MeasureString(Me._descriptionLabel.text, Me._descriptionLabel.Font).Width, System.Globalization.CultureInfo.CurrentCulture)
            Me._DescriptionLabel.Height = Convert.ToInt32(Me.CreateGraphics().MeasureString(Me._DescriptionLabel.Text, Me._DescriptionLabel.Font).Height, System.Globalization.CultureInfo.CurrentCulture)
            ' Me._descriptionLabel.Left = leftMargin
            Me._DescriptionLabel.Top = rowTop
            Me._DescriptionLabel.Invalidate()
            rowTop = Me._DescriptionLabel.Top + Me._DescriptionLabel.Height + rowSpace

            ' Display license information
            rowText = New System.Text.StringBuilder("")
            If (licenseeName.Trim.Length > 0) Or
               (licenseCode.Trim.Length > 0) Then
                If licenseHeader.Trim.Length > 0 Then
                    rowText.Append(licenseHeader)
                End If
                If licenseeName.Trim.Length > 0 Then
                    If rowText.Length > 0 Then
                        rowText.Append(Environment.NewLine)
                    End If
                    rowText.AppendFormat(Globalization.CultureInfo.CurrentCulture, " {0}", licenseeName.Trim)
                End If
                If licenseCode.Trim.Length > 0 Then
                    If rowText.Length > 0 Then
                        rowText.Append(Environment.NewLine)
                    End If
                    rowText.AppendFormat(Globalization.CultureInfo.CurrentCulture, "  {0}", licenseCode.Trim)
                End If
            ElseIf licenseHeader.Trim.Length > 0 Then
                rowText.Append(licenseHeader)
            End If
            Me._LicenseLabel.Text = rowText.ToString
            Me._LicenseLabel.Refresh()
            ' Me._licenseLabel.Width = 1 + Int32.parse(Me.CreateGraphics().MeasureString(Me._licenseLabel.text, Me._licenseLabel.Font).Width, System.Globalization.CultureInfo.CurrentCulture)
            Me._LicenseLabel.Height = Convert.ToInt32(Me.CreateGraphics().MeasureString(Me._LicenseLabel.Text, Me._LicenseLabel.Font).Height, System.Globalization.CultureInfo.CurrentCulture)
            ' Me._licenseLabel.Left = leftMargin
            Me._LicenseLabel.Top = rowTop
            Me._LicenseLabel.Invalidate()
            rowTop = Me._LicenseLabel.Top + Me._LicenseLabel.Height + rowSpace

            Me._CopyrightLabel.Text = fileVersionInfo.LegalCopyright.Trim
            Me._CopyrightLabel.Refresh()
            ' Me._copyrightLabel.Width = Me.ClientSize.Width - leftMargin - leftMargin
            ' Me._copyrightLabel.Left = leftMargin
            Me._CopyrightLabel.Top = rowTop
            Me._CopyrightLabel.Invalidate()
            rowTop = Me._CopyrightLabel.Top + Me._CopyrightLabel.Height + rowSpace \ 2

            ' set form height to fit data.
            Me.Height = rowTop + (Me.Height - Me.ClientSize.Height)

        Catch

            Throw

        Finally

            ' Turn off the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    #End Region 


    #Region " FORM EVENT HANDLERS "

    ''' <summary>
    ''' Occurs when the form is loaded. Does all the processing before the form controls are rendered
    ''' as the user expects them.
    ''' </summary>
    ''' <remarks>
    ''' Use this method for doing any final initialization right before the form is shown. This is a
    ''' good place to change the Visible and ShowInTaskbar properties to start the form as hidden.  
    ''' Starting a form as hidden is useful for forms that need to be running but that should not
    ''' show themselves right away, such as forms with a notify icon in the task bar.
    ''' </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                       <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      <see cref="System.EventArgs"/> </param>
    Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' center the form
            Me.CenterToScreen()

        Catch

            ' Use throw without an argument in order to preserve the stack location 
            ' where the exception was initially raised.
            Throw

        Finally

            ' Turn off the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    ''' <summary>
    ''' Does all the post processing after all the form controls are rendered as the user expects
    ''' them.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                       <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub Form_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown

        ' allow form rendering time to complete: process all messages currently in the queue.
        Windows.Forms.Application.DoEvents()

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' instantiate form objects
            ' Me.instantiateObjects()

            ' set the form caption
            Me.Text = $"{My.Application.Info.AssemblyName}.{My.Application.Info.Version}"

            ' set tool tips
            Me.InitializeUserInterface()

            ' allow some events to occur for refreshing the display.
            Windows.Forms.Application.DoEvents()

        Catch ex As Exception

            MyMessageBox.ShowDialog(ex)

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    #End Region 

End Class

