<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class NumericInputBox

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me._CancelButton = New System.Windows.Forms.Button()
        Me._AcceptButton = New System.Windows.Forms.Button()
        Me._NumericUpDownLabel = New System.Windows.Forms.Label()
        Me.NumericUpDown = New System.Windows.Forms.NumericUpDown()
        CType(Me.NumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_CancelButton
        '
        Me._CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me._CancelButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._CancelButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._CancelButton.Location = New System.Drawing.Point(11, 59)
        Me._CancelButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._CancelButton.Name = "_CancelButton"
        Me._CancelButton.Size = New System.Drawing.Size(87, 30)
        Me._CancelButton.TabIndex = 27
        Me._CancelButton.Text = "&Cancel"
        '
        '_AcceptButton
        '
        Me._AcceptButton.Enabled = False
        Me._AcceptButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._AcceptButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._AcceptButton.Location = New System.Drawing.Point(108, 59)
        Me._AcceptButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._AcceptButton.Name = "_AcceptButton"
        Me._AcceptButton.Size = New System.Drawing.Size(87, 30)
        Me._AcceptButton.TabIndex = 26
        Me._AcceptButton.Text = "&OK"
        '
        '_NumericUpDownLabel
        '
        Me._NumericUpDownLabel.BackColor = System.Drawing.SystemColors.Control
        Me._NumericUpDownLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._NumericUpDownLabel.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._NumericUpDownLabel.ForeColor = System.Drawing.SystemColors.WindowText
        Me._NumericUpDownLabel.Location = New System.Drawing.Point(9, 5)
        Me._NumericUpDownLabel.Name = "_NumericUpDownLabel"
        Me._NumericUpDownLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._NumericUpDownLabel.Size = New System.Drawing.Size(156, 21)
        Me._NumericUpDownLabel.TabIndex = 25
        Me._NumericUpDownLabel.Text = "Enter a Value: "
        Me._NumericUpDownLabel.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'NumericUpDown
        '
        Me.NumericUpDown.Location = New System.Drawing.Point(11, 27)
        Me.NumericUpDown.Name = "NumericUpDown"
        Me.NumericUpDown.Size = New System.Drawing.Size(183, 25)
        Me.NumericUpDown.TabIndex = 28
        '
        'NumericInputBox
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.ClientSize = New System.Drawing.Size(209, 98)
        Me.ControlBox = False
        Me.Controls.Add(Me.NumericUpDown)
        Me.Controls.Add(Me._CancelButton)
        Me.Controls.Add(Me._AcceptButton)
        Me.Controls.Add(Me._NumericUpDownLabel)
        Me.Name = "NumericInputBox"
        Me.Text = "Numeric Input Box"
        CType(Me.NumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _CancelButton As System.Windows.Forms.Button
    Private WithEvents _AcceptButton As System.Windows.Forms.Button
    Private WithEvents _NumericUpDownLabel As System.Windows.Forms.Label
    Public WithEvents NumericUpDown As System.Windows.Forms.NumericUpDown
End Class
