''' <summary> A data entry form. </summary>
''' <remarks> David, 02/20/2006. </remarks>
Public Class NumericInputBox
    Inherits FormBase


    #Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of this class. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        Me.InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        If Version.Parse(My.Computer.Info.OSVersion).Major <= EnableDropShadowVersion Then
            Me.ClassStyle = ClassStyleConstants.DropShadow
        End If
        Me._AcceptButton.Enabled = True

    End Sub

    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the
    ''' <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    #End Region 


    #Region " FORM AND CONTROL EVENT HANDLERS "

    ''' <summary>
    ''' Closes and returns the <see cref="System.Windows.Forms.DialogResult.OK">OK</see>
    ''' dialog result.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub AcceptButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _AcceptButton.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    ''' <summary>
    ''' Closes and returns the <see cref="System.Windows.Forms.DialogResult.Cancel">Cancel</see>
    ''' dialog result.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub CancelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _CancelButton.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    #End Region 

End Class
