''' <summary> A data entry form for text with a numeric validator. </summary>
''' <remarks> David, 02/20/2006. </remarks>
Public Class TextInputBox
    Inherits FormBase


    #Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of this class. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        Me.InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        If Version.Parse(My.Computer.Info.OSVersion).Major <= EnableDropShadowVersion Then
            Me.ClassStyle = ClassStyleConstants.DropShadow
        End If
        Me._NumberStyle = Globalization.NumberStyles.None

    End Sub

    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the
    ''' <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    #End Region 


    #Region " PROPERTIES "

    ''' <summary> Gets or sets the use system password character. </summary>
    ''' <value> The use system password character. </value>
    Public Property UseSystemPasswordChar As Boolean
        Get
            Return Me._EnteredValueTextBox.UseSystemPasswordChar
        End Get
        Set(ByVal value As Boolean)
            Me._EnteredValueTextBox.UseSystemPasswordChar = value
        End Set
    End Property

    ''' <summary> Gets or sets the prompt. </summary>
    ''' <value> The prompt. </value>
    Public Property Prompt() As String
        Get
            Return Me._EnteredValueTextBoxLabel.Text
        End Get
        Set(ByVal value As String)
            Me._EnteredValueTextBoxLabel.Text = value
        End Set
    End Property

    ''' <summary> Returns the entered value. </summary>
    ''' <value> The entered value. </value>
    Public Property EnteredValue() As String
        Get
            Return Me._EnteredValueTextBox.Text
        End Get
        Set(ByVal value As String)
            Me._EnteredValueTextBox.Text = value
        End Set
    End Property

    ''' <summary> Gets or sets the default value. </summary>
    ''' <value> The default value. </value>
    Public Property DefaultValue() As String = String.Empty

    ''' <summary> Gets or sets the number style requested. </summary>
    ''' <value> The total number of style. </value>
    Public Property NumberStyle() As Globalization.NumberStyles

    #End Region 


    #Region " FORM AND CONTROL EVENT HANDLERS "

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Form.Shown" /> event. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub OnShown(e As EventArgs)
        Try
            Me._EnteredValueTextBox.Focus()
        Catch
        Finally
            MyBase.OnShown(e)
        End Try
    End Sub

    ''' <summary>
    ''' Closes and returns the <see cref="System.Windows.Forms.DialogResult.OK">OK</see>
    ''' dialog result.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub AcceptButtonClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles _AcceptButton.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    ''' <summary>
    ''' Closes and returns the <see cref="System.Windows.Forms.DialogResult.Cancel">Cancel</see>
    ''' dialog result.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub CancelButtonClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles _CancelButton.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    ''' <summary> Event handler. Called by _EnteredValueTextBox for text changed events. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub EnteredValueTextBoxTextChanged(sender As Object, e As EventArgs) Handles _EnteredValueTextBox.TextChanged
        If Me.NumberStyle = Globalization.NumberStyles.None Then
            Me._AcceptButton.Enabled = Not String.IsNullOrEmpty(Me._EnteredValueTextBox.Text)
        End If
    End Sub

    ''' <summary> Validates the entered value. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Cancel event information. </param>
    Private Sub EnteredValueTextBoxValidating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _EnteredValueTextBox.Validating
        Me._AcceptButton.Enabled = False
        Me._ValidationErrorProvider.SetError(TryCast(sender, System.Windows.Forms.Control), String.Empty)
        If Me.NumberStyle <> Globalization.NumberStyles.None Then
            Dim outcome As Double
            e.Cancel = Not Double.TryParse(Me._EnteredValueTextBox.Text, Me.NumberStyle, Globalization.CultureInfo.CurrentCulture, outcome)
        End If
    End Sub

    ''' <summary> Enables the OK button. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub EnteredValueTextBoxValidated(ByVal sender As Object, ByVal e As System.EventArgs) Handles _EnteredValueTextBox.Validated
        Me._AcceptButton.Enabled = True
    End Sub

    #End Region 

End Class

''' <summary> my text input box. </summary>
''' <remarks> David, 9/13/2019. </remarks>
Public NotInheritable Class MyTextInputBox


    #Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Constructor that prevents a default instance of this class from being created.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Private Sub New()
        MyBase.New()
    End Sub


    #End Region 


    #Region " SHARED "

    ''' <summary> Enter dialog. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="caption"> The caption. </param>
    ''' <returns> A (DialogResult As MyDialogResult, DialogValue As String) </returns>
    Public Shared Function EnterDialog(ByVal caption As String) As (DialogResult As MyDialogResult, DialogValue As String)
        Return EnterDialog(caption, "Enter a value:", String.Empty, False)
    End Function

    ''' <summary> Enter dialog. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="caption">               The caption. </param>
    ''' <param name="useSystemPasswordChar"> True to use system password character. </param>
    ''' <returns> A (DialogResult As MyDialogResult, DialogValue As String) </returns>
    '''
    ''' ### <param name="UseSystemPasswordChar"> True to use system password character. </param>
    Public Shared Function EnterDialog(ByVal caption As String, ByVal useSystemPasswordChar As Boolean) As (DialogResult As MyDialogResult, DialogValue As String)
        Return EnterDialog(caption, "Enter a value:", String.Empty, useSystemPasswordChar)
    End Function

    ''' <summary> Enter dialog. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="caption">               The caption. </param>
    ''' <param name="prompt">                The prompt. </param>
    ''' <param name="defaultValue">          The default value. </param>
    ''' <param name="useSystemPasswordChar"> True to use system password character. </param>
    ''' <returns> A (DialogResult As MyDialogResult, DialogValue As String) </returns>
    Public Shared Function EnterDialog(ByVal caption As String, ByVal prompt As String, ByVal defaultValue As String, ByVal useSystemPasswordChar As Boolean) As (DialogResult As MyDialogResult, DialogValue As String)
        Dim outcome As (DialogResult As MyDialogResult, DialogValue As String) = (MyDialogResult.Ok, String.Empty)
        Using box As New TextInputBox()
            box.UseSystemPasswordChar = useSystemPasswordChar
            box.DefaultValue = defaultValue
            box.Text = prompt
            box.Text = caption
            ' toggling visibility ensures that the form displays in unit testing debug mode
            box.Visible = True : Windows.Forms.Application.DoEvents() : box.Visible = False
            box.ShowDialog()
            outcome = (MessageBox.ToDialogResult(box.DialogResult), box.EnteredValue)
        End Using
        Return outcome
    End Function

    #End Region 

End Class
