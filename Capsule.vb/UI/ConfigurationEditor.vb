Imports isr.Core.ExceptionExtensions

''' <summary> Editor for configuration. </summary>
''' <remarks> David, 3/31/2016. </remarks>
Public Class ConfigurationEditor
    Inherits FormBase


    #Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="ConfigurationEditor" /> class from being
    ''' created.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Private Sub New()

        MyBase.New()

        ' This method is required by the Windows Form Designer.
        Me.InitializeComponent()

    End Sub

    ''' <summary>
    ''' Gets the locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    ''' <value> The sync locker. </value>
    Private Shared Property SyncLocker As New Object

    ''' <summary> Gets the instance. </summary>
    ''' <value> The instance. </value>
    Private Shared Property Instance As ConfigurationEditor

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    Public Shared Function [Get]() As ConfigurationEditor
        If Not Instantiated Then
            SyncLock SyncLocker
                Instance = New ConfigurationEditor
            End SyncLock
        End If
        Return Instance
    End Function

    ''' <summary> Returns true if an instance of the class was created and not disposed. </summary>
    ''' <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
    Friend Shared ReadOnly Property Instantiated() As Boolean
        Get
            SyncLock SyncLocker
                Return Instance IsNot Nothing AndAlso Not Instance.IsDisposed
            End SyncLock
        End Get
    End Property

    ''' <summary> Dispose instance. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Public Shared Sub DisposeInstance()
        SyncLock SyncLocker
            If Instance IsNot Nothing AndAlso Not Instance.IsDisposed Then
                Instance.Dispose()
                Instance = Nothing
            End If
        End SyncLock
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    #End Region 


    #Region " SHOW "

    ''' <summary> Hides the dialog. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="owner"> The owner. </param>
    ''' <returns> A DialogResult. </returns>
    <Obsolete("Illegal Call")>
    Public Shadows Function ShowDialog(owner As System.Windows.Forms.IWin32Window) As System.Windows.Forms.DialogResult
        Me.Text = "Illegal Call"
        Return MyBase.ShowDialog(owner)
    End Function

    ''' <summary> Hides the dialog. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <returns> A System.Windows.Forms.DialogResult. </returns>
    <Obsolete("Illegal Call")>
    Public Shadows Function ShowDialog() As System.Windows.Forms.DialogResult
        Me.Text = "Illegal Call"
        Return MyBase.ShowDialog()
    End Function

    ''' <summary> Shows the dialog. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="settings"> Options for controlling the operation. </param>
    ''' <returns> A DialogResult. </returns>
    Public Shadows Function ShowDialog(ByVal settings As System.Configuration.ApplicationSettingsBase) As System.Windows.Forms.DialogResult
        Return Me.ShowDialog(Nothing, settings)
    End Function

    ''' <summary> Shows the dialog. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="mdiForm">  The MDI form. </param>
    ''' <param name="settings"> Options for controlling the operation. </param>
    ''' <returns> A DialogResult. </returns>
    Public Shadows Function ShowDialog(ByVal mdiForm As System.Windows.Forms.Form, ByVal settings As System.Configuration.ApplicationSettingsBase) As System.Windows.Forms.DialogResult
        Me.Show(mdiForm, settings)
        Do While Me.Visible
            Windows.Forms.Application.DoEvents()
        Loop
        Return Me.DialogResult
    End Function

    ''' <summary> Displays this dialog with title 'illegal call'. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    <Obsolete("Illegal Call")>
    Public Shadows Sub Show()
        Me.Text = "Illegal Call"
        MyBase.Show()
    End Sub

    ''' <summary> Shows this form. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="settings"> Options for controlling the operation. </param>
    Public Shadows Sub Show(ByVal settings As System.Configuration.ApplicationSettingsBase)
        Me.Show(Nothing, settings)
    End Sub

    ''' <summary> Shows this form. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="mdiForm">  The MDI form. </param>
    ''' <param name="settings"> Options for controlling the operation. </param>
    Public Shadows Sub Show(ByVal mdiForm As System.Windows.Forms.Form, ByVal settings As System.Configuration.ApplicationSettingsBase)
        If mdiForm IsNot Nothing AndAlso mdiForm.IsMdiContainer Then
            Me.MdiParent = mdiForm
            mdiForm.Show()
        End If
        Me._Settings = settings
        MyBase.Show()
    End Sub

    #End Region 


    #Region " FORM EVENTS "

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Form.Shown" /> event. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub OnShown(e As EventArgs)
        Try
            Me._PropertyGrid.SelectedObject = Me._Settings
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
        Finally
            MyBase.OnShown(e)
        End Try
    End Sub

    #End Region 


    #Region " HANDLERS "

    ''' <summary> Cancel button click. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub IgnoreButton_Click(sender As Object, e As EventArgs) Handles _IgnoreButton.Click
        Me.Close()
    End Sub

    ''' <summary> Accept button click. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub AcceptButton_Click(sender As Object, e As EventArgs) Handles _AcceptButton.Click
        Me.SaveExit(sender, e)
    End Sub

    ''' <summary> Options for controlling the operation. </summary>
    Private _Settings As System.Configuration.ApplicationSettingsBase

    ''' <summary> Resets the menu item click. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ResetMenuItem_Click(sender As Object, e As EventArgs) Handles _ResetMenuItem.Click
        Try
            'Me._PropertyGrid.SelectedObject = Nothing
            Windows.Forms.Application.DoEvents()
            If Windows.Forms.MessageBox.Show("Restoring the persisted application settings values to their corresponding default properties; Are you sure?", "Reset Setting; Are you sure?!",
                                             Windows.Forms.MessageBoxButtons.YesNo, Windows.Forms.MessageBoxIcon.Question,
                                             Windows.Forms.MessageBoxDefaultButton.Button2,
                                             Windows.Forms.MessageBoxOptions.DefaultDesktopOnly) = Windows.Forms.DialogResult.Yes Then
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me._Settings.Reset()
                Me._PropertyGrid.SelectedObject = Me._Settings
            End If
        Catch ex As Exception
            Windows.Forms.MessageBox.Show(ex.ToFullBlownString, "Exception Resetting Settings",
                                          Windows.Forms.MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Error,
                                          Windows.Forms.MessageBoxDefaultButton.Button1, Windows.Forms.MessageBoxOptions.DefaultDesktopOnly)
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    ''' <summary> Reads menu item click. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ReadMenuItem_Click(sender As Object, e As EventArgs) Handles _ReadMenuItem.Click
        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            Me._PropertyGrid.SelectedObject = Nothing
            Windows.Forms.Application.DoEvents()
            Threading.Thread.Sleep(100)
            Me._PropertyGrid.SelectedObject = Me._Settings
        Catch ex As Exception
            Windows.Forms.MessageBox.Show(ex.ToFullBlownString, "Exception reading Settings",
                                          Windows.Forms.MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Error, Windows.Forms.MessageBoxDefaultButton.Button1, Windows.Forms.MessageBoxOptions.DefaultDesktopOnly)
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    ''' <summary> Saves a menu item click. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SaveMenuItem_Click(sender As Object, e As EventArgs) Handles _SaveMenuItem.Click
        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            Me._Settings.Save()
            Windows.Forms.Application.DoEvents()
            Threading.Thread.Sleep(100)
        Catch ex As Exception
            Windows.Forms.MessageBox.Show(ex.ToFullBlownString, "Exception saving Settings",
                                          Windows.Forms.MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Error, Windows.Forms.MessageBoxDefaultButton.Button1, Windows.Forms.MessageBoxOptions.DefaultDesktopOnly)
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    ''' <summary> Saves an exit. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SaveExit(sender As Object, e As EventArgs)
        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            Me._Settings.Save()
            Windows.Forms.Application.DoEvents()
            Threading.Thread.Sleep(100)
            Me.Close()
        Catch ex As Exception
            Windows.Forms.MessageBox.Show(ex.ToFullBlownString, "Exception saving Settings",
                                          Windows.Forms.MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Error, Windows.Forms.MessageBoxDefaultButton.Button1, Windows.Forms.MessageBoxOptions.DefaultDesktopOnly)
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    ''' <summary> Saves an exit menu item click. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SaveExitMenuItem_Click(sender As Object, e As EventArgs) Handles _SaveExitMenuItem.Click
        Me.SaveExit(sender, e)
    End Sub

    ''' <summary> Exit menu item click. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ExitMenuItem_Click(sender As Object, e As EventArgs) Handles _ExitMenuItem.Click
        Me.Close()
    End Sub

    #End Region 

End Class

''' <summary> A configuration edit pad. </summary>
''' <remarks> David, 1/19/2019. </remarks>
Public NotInheritable Class ConfigurationEditPad

    #Region " CONSTRUCTION "

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Private Sub New()
        MyBase.New
    End Sub

    #End Region 

    #Region " CONFIGURATION EDITOR "

    ''' <summary> Opens the settings editor. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="caption">  The caption. </param>
    ''' <param name="settings"> Options for controlling the operation. </param>
    Public Shared Sub Open(ByVal caption As String, ByVal settings As Configuration.ApplicationSettingsBase)
        Using f As ConfigurationEditor = ConfigurationEditor.Get
            f.Text = caption
            f.ShowDialog(settings)
        End Using
    End Sub

    #End Region 

End Class
