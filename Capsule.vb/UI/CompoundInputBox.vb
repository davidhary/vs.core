''' <summary> A data entry form. </summary>
''' <remarks> David, 02/20/2006. </remarks>
Public Class CompoundInputBox
    Inherits FormBase


    #Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of this class. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        Me.InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        If Version.Parse(My.Computer.Info.OSVersion).Major <= EnableDropShadowVersion Then
            Me.ClassStyle = ClassStyleConstants.DropShadow
        End If

        Me._AcceptButton.Enabled = True

        Me.TextBoxLabel.Text = String.Empty
        Me.NumericUpDownLabel.Text = String.Empty
        Me.DropDownBoxLabel.Text = String.Empty

    End Sub

    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the
    ''' <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub


    #End Region 


    #Region " FORM EVENTS "

    ''' <summary> Handles the shown event of the control. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="e"> Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub OnShown(ByVal e As System.EventArgs)
        Try
            If String.IsNullOrWhiteSpace(Me.TextBoxLabel.Text) Then
                Me.TextBoxLabel.Visible = False
                Me.TextBox.Visible = False
            End If
            If String.IsNullOrWhiteSpace(Me.NumericUpDownLabel.Text) Then
                Me.NumericUpDownLabel.Visible = False
                Me.NumericUpDown.Visible = False
            End If
            If String.IsNullOrWhiteSpace(Me.DropDownBoxLabel.Text) Then
                Me.DropDownBoxLabel.Visible = False
                Me.DropDownBox.Visible = False
            End If
        Catch
            Throw
        Finally
            MyBase.OnShown(e)
        End Try
    End Sub


    #End Region 


    #Region " FORM AND CONTROL EVENT HANDLERS "

    ''' <summary>
    ''' Closes and returns the <see cref="System.Windows.Forms.DialogResult.OK">OK</see>
    ''' dialog result.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub AcceptButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _AcceptButton.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    ''' <summary>
    ''' Closes and returns the <see cref="System.Windows.Forms.DialogResult.Cancel">Cancel</see>
    ''' dialog result.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub CancelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _CancelButton.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    ''' <summary> Enables the OK button. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub TextBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox.Validated
        Me._AcceptButton.Enabled = True
    End Sub

    #End Region 

End Class
