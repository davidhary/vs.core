Imports System.Windows.Forms

''' <summary> Form for displaying a message. </summary>
''' <remarks> David, 9/17/2014. </remarks>
Partial Public NotInheritable Class MessageForm
    Inherits FormBase


    #Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="MessageForm" /> class from being created.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Private Sub New()

        MyBase.New()

        ' This method is required by the Windows Form Designer.
        Me.InitializeComponent()

    End Sub

    ''' <summary>
    ''' Gets the locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    ''' <value> The sync locker. </value>
    Private Shared Property SyncLocker As New Object

    ''' <summary> Gets the instance. </summary>
    ''' <value> The instance. </value>
    Private Shared Property Instance As MessageForm

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    Public Shared Function [Get]() As MessageForm
        If Not Instantiated Then
            SyncLock SyncLocker
                Instance = New MessageForm
            End SyncLock
        End If
        Return Instance
    End Function

    ''' <summary> Returns true if an instance of the class was created and not disposed. </summary>
    ''' <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
    Friend Shared ReadOnly Property Instantiated() As Boolean
        Get
            SyncLock SyncLocker
                Return Instance IsNot Nothing AndAlso Not Instance.IsDisposed
            End SyncLock
        End Get
    End Property

    ''' <summary> Dispose instance. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Public Shared Sub DisposeInstance()
        SyncLock SyncLocker
            If Instance IsNot Nothing AndAlso Not Instance.IsDisposed Then
                Instance.Dispose()
                Instance = Nothing
            End If
        End SyncLock
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    #End Region 


    #Region " FORM EVENTS "

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Form.Shown" /> event. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnShown(e As System.EventArgs)
        MyBase.OnShown(e)
    End Sub

    ''' <summary> Displays this dialog with title 'illegal call'. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <returns> A MyDialogResult. </returns>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private Shadows Function ShowDialog() As MyDialogResult
        Me.Text = "Illegal Call"
        Return MessageBox.ToDialogResult(MyBase.ShowDialog())
    End Function

    ''' <summary> Shows the message box with 'illegal call' in the caption. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private Shadows Sub Show()
        Me.Status = String.Empty
        Me.Text = "Illegal Call"
        Me._RichTextBox.Text = "Illegal Call"
        Me.DialogResult = Windows.Forms.DialogResult.None
        MyBase.Show()
    End Sub

    ''' <summary> Shows the message box with these messages. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="mdiForm"> The MDI form. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <param name="details"> The details. </param>
    Public Shadows Sub Show(ByVal mdiForm As Form, ByVal caption As String, ByVal details As String)
        If mdiForm IsNot Nothing AndAlso mdiForm.IsMdiContainer Then
            Me.MdiParent = mdiForm
            mdiForm.Show()
        End If
        Me._ProgressBar.Visible = False
        Me.Status = String.Empty
        Me.Text = caption
        Me._RichTextBox.Text = details
        Me.DialogResult = Windows.Forms.DialogResult.None
        Windows.Forms.Application.DoEvents()
        MyBase.Show()
        Windows.Forms.Application.DoEvents()
    End Sub

    ''' <summary> Shows the message box with these messages. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="mdiForm">  The MDI form. </param>
    ''' <param name="caption">  The caption. </param>
    ''' <param name="details">  The details. </param>
    ''' <param name="duration"> The duration. </param>
    Public Shadows Sub Show(ByVal mdiForm As Form, ByVal caption As String, ByVal details As String, ByVal duration As TimeSpan)
        Me.Show(mdiForm, caption, details)
        Dim sw As Stopwatch = Stopwatch.StartNew
        Me._ProgressBar.Value = 100
        Me._ProgressBar.Visible = True
        Do Until sw.Elapsed > duration
            Windows.Forms.Application.DoEvents()
            Dim value As Integer = CInt(100 * (1 - sw.Elapsed.Ticks / duration.Ticks)) : value = If(value < 0, 0, If(value > 100, 100, value))
            Me._ProgressBar.Value = value
            Windows.Forms.Application.DoEvents()
        Loop
        Me._ProgressBar.Visible = False
        Me.Close()
    End Sub

    ''' <summary> Gets the rich text box. </summary>
    ''' <value> The rich text box. </value>
    <System.ComponentModel.Browsable(False),
        System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)>
    Public ReadOnly Property RichTextBox As System.Windows.Forms.RichTextBox
        Get
            Return Me._RichTextBox
        End Get
    End Property

    ''' <summary> Gets or sets the status. </summary>
    ''' <value> The status. </value>
    <System.ComponentModel.Browsable(False),
        System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)>
    Public Property Status As String
        Get
            Return Me._StatusLabel.Text
        End Get
        Set(value As String)
            Me._StatusLabel.Text = isr.Core.Controls.CompactExtensions.Compact(value, Me._StatusLabel)
            Me._StatusLabel.ToolTipText = value
            Windows.Forms.Application.DoEvents()
        End Set
    End Property

    ''' <summary> Gets the progress. </summary>
    ''' <value> The progress. </value>
    <System.ComponentModel.Browsable(False),
        System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)>
    Public ReadOnly Property Progress As ToolStripProgressBar
        Get
            Return Me._ProgressBar
        End Get
    End Property

    ''' <summary> Gets or sets the custom button text. </summary>
    ''' <value> The custom button text. </value>
    Public Property CustomButtonText As String
        Get
            Return Me._CustomButton.Text
        End Get
        Set(value As String)
            Me._CustomButton.Text = value
            Windows.Forms.Application.DoEvents()
        End Set
    End Property

    ''' <summary> Custom button click. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub CustomButton_Click(sender As Object, e As System.EventArgs) Handles _CustomButton.Click
        Me.DialogResult = Windows.Forms.DialogResult.OK
        Windows.Forms.Application.DoEvents()
        Me.Status = String.Format("{0} requested", Me._CustomButton.Text)
    End Sub

    #End Region 

End Class
