Imports System.Text
Imports System.Runtime.CompilerServices

Namespace ExceptionExtensions

    ''' <summary> Includes extensions for <see cref="Exception">Exceptions</see>. </summary>
    ''' <remarks>
    ''' David, 03/30/2017. <para>
    ''' (c) 2017 Marco Bertschi</para><para>
    ''' https://www.codeproject.com/script/Membership/View.aspx?mid=8888914
    ''' https://www.codeproject.com/Tips/1179564/A-Quick-Dirty-Extension-Method-to-Get-the-Full-Exc
    ''' </para>
    ''' </remarks>
    Public Module Methods


        #Region " ADD EXCEPTION DATA "

        ''' <summary> Adds an exception data to 'exception'. </summary>
        ''' <remarks>
        ''' For more info on the external exceptions see:
        ''' http://msdn.microsoft.com/en-us/library/system.runtime.interopservices.sehexception.aspx.
        ''' </remarks>
        ''' <param name="value">     The value. </param>
        ''' <param name="exception"> The exception. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        Private Function AddExceptionData(ByVal value As System.Exception, ByVal exception As System.Runtime.InteropServices.ExternalException) As Boolean
            If exception IsNot Nothing Then
                value.Data.Add($"{value.Data.Count}-External.Error.Code", $"{exception.ErrorCode}")
            End If
            Return exception IsNot Nothing
        End Function

        ''' <summary> Adds an exception data to 'exception'. </summary>
        ''' <remarks> David, 2020-09-12. </remarks>
        ''' <param name="value">     The value. </param>
        ''' <param name="exception"> The exception. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        Private Function AddExceptionData(ByVal value As System.Exception, ByVal exception As ArgumentOutOfRangeException) As Boolean
            If exception IsNot Nothing Then
                value.Data.Add($"{value.Data.Count}-Name+Value", $"{exception.ParamName}={exception.ActualValue}")
            End If
            Return exception IsNot Nothing
        End Function

        ''' <summary> Adds an exception data to 'exception'. </summary>
        ''' <remarks> David, 2020-09-12. </remarks>
        ''' <param name="value">     The value. </param>
        ''' <param name="exception"> The exception. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        Private Function AddExceptionData(ByVal value As System.Exception, ByVal exception As ArgumentException) As Boolean
            If exception IsNot Nothing Then
                value.Data.Add($"{value.Data.Count}-Name", exception.ParamName)
            End If
            Return exception IsNot Nothing
        End Function

        ''' <summary> Adds an exception data to 'Exception'. </summary>
        ''' <remarks> David, 2020-09-12. </remarks>
        ''' <param name="exception"> The exception. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        <Extension>
        Public Function AddExceptionData(ByVal exception As System.Exception) As Boolean
            Dim affirmative As Boolean = False
            affirmative = affirmative OrElse Methods.AddExceptionData(exception, TryCast(exception, ArgumentOutOfRangeException))
            affirmative = affirmative OrElse Methods.AddExceptionData(exception, TryCast(exception, ArgumentException))
            affirmative = affirmative OrElse Methods.AddExceptionData(exception, TryCast(exception, Runtime.InteropServices.ExternalException))
            Return affirmative
        End Function

        #End Region 


        #Region " TO FULL BLOWN STRING "

        ''' <summary> Converts a value to a full blown string. </summary>
        ''' <remarks> David, 2020-09-12. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <returns> Value as a String. </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function ToFullBlownString(ByVal value As System.Exception) As String
            Return Methods.ToFullBlownString(value, Integer.MaxValue)
        End Function

        ''' <summary> Converts this object to a full blown string. </summary>
        ''' <remarks> David, 2020-09-12. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <param name="level"> The level. </param>
        ''' <returns> The given data converted to a String. </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function ToFullBlownString(ByVal value As System.Exception, ByVal level As Integer) As String
            Return Methods.ToFullBlownString(value, level, AddressOf Methods.AddExceptionData)
        End Function

        ''' <summary> Converts this object to a full blown string. </summary>
        ''' <remarks> David, 2020-09-12. </remarks>
        ''' <param name="value">   The value. </param>
        ''' <param name="addData"> Adds exception data. </param>
        ''' <returns> The given data converted to a String. </returns>
        Public Function ToFullBlownString(ByVal value As System.Exception, addData As Action(Of Exception)) As String
            Return Methods.ToFullBlownString(value, Integer.MaxValue, addData)
        End Function

        ''' <summary> Converts this object to a full blown string. </summary>
        ''' <remarks> David, 2020-09-12. </remarks>
        ''' <param name="value">   The value. </param>
        ''' <param name="level">   The level. </param>
        ''' <param name="addData"> Adds exception data. </param>
        ''' <returns> The given data converted to a String. </returns>
        Public Function ToFullBlownString(ByVal value As System.Exception, ByVal level As Integer, addData As Action(Of Exception)) As String
            Dim builder As New StringBuilder()
            Dim counter As Integer = 1
            builder.AppendLine()
            Do While value IsNot Nothing AndAlso counter <= level
                If addData IsNot Nothing Then addData(value)
                Methods.AppendFullBlownString(builder, value, counter)
                value = value.InnerException
                counter += 1
            Loop
            Return builder.ToString().TrimEnd(Environment.NewLine.ToCharArray)
        End Function

        ''' <summary> Appends a full blown string. </summary>
        ''' <remarks> David, 2020-09-12. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="builder"> The builder. </param>
        ''' <param name="value">   The value. </param>
        ''' <param name="prefix">  The prefix. </param>
        Private Sub AppendFullBlownString(ByVal builder As StringBuilder, ByVal value As System.Exception, ByVal prefix As String)
            If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
            If builder Is Nothing Then Throw New ArgumentNullException(NameOf(builder))
            builder.AppendLine($"{prefix}Type: {value.GetType}")
            If Not String.IsNullOrEmpty(value.Message) Then builder.AppendLine($"{prefix}Message: {value.Message}")
            If Not String.IsNullOrEmpty(value.Source) Then builder.AppendLine($"{prefix}Source: {value.Source}")
            If value.TargetSite IsNot Nothing Then builder.AppendLine($"{prefix}Method: {value.TargetSite}")
            Dim userCallStack As String = StackTraceExtensions.Methods.UserCallStack(value, prefix)
            If Not String.IsNullOrEmpty(userCallStack) Then builder.AppendLine(userCallStack)
            If value.Data IsNot Nothing Then
                For Each keyValuePair As System.Collections.DictionaryEntry In value.Data
                    builder.AppendLine($"{prefix} Data: {keyValuePair.Key}: {keyValuePair.Value}")
                Next
            End If
        End Sub

        ''' <summary> Appends a full blown string. </summary>
        ''' <remarks> David, 2020-09-12. </remarks>
        ''' <param name="builder"> The builder. </param>
        ''' <param name="value">   The value. </param>
        ''' <param name="counter"> The counter. </param>
        Private Sub AppendFullBlownString(ByVal builder As StringBuilder, ByVal value As System.Exception, ByVal counter As Integer)
            Methods.AppendFullBlownString(builder, value, $"{counter}-> ")
        End Sub

        #End Region 

    End Module
End Namespace
