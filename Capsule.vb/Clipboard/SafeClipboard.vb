Imports System.Windows.Forms

''' <summary> Safe clipboard set data object. </summary>
''' <remarks>
''' David, 3/24/2015
''' http://stackoverflow.com/questions/899350/how-to-copy-the-contents-of-a-string-to-the-clipboard-in-c.
''' </remarks>
Public Class SafeClipboardSetDataObject
    Inherits SingleThreadApartmentBase

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="format"> Describes the <see cref="DataFormats">format</see> of the specified data. </param>
    ''' <param name="data">   The data. </param>
    Public Sub New(ByVal format As String, ByVal data As Object)
        MyBase.New()
        Me._Format = format
        Me._Data = data
    End Sub

    ''' <summary> Describes the format to use. </summary>
    Private ReadOnly _Format As String

    ''' <summary> The data. </summary>
    Private ReadOnly _Data As Object

    ''' <summary> Implemented in this class to do actual work. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Protected Overrides Sub Work()
        Dim obj As Object = New System.Windows.Forms.DataObject(Me._Format, Me._Data)
        Clipboard.SetDataObject(obj, True)
    End Sub

    ''' <summary> Copies text to the clipboard. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="text"> The text. </param>
    Public Shared Sub SetDataObject(ByVal text As String)
        Dim scr As SafeClipboardSetDataObject = New SafeClipboardSetDataObject(DataFormats.Text, text)
        scr.Go()
    End Sub

End Class


