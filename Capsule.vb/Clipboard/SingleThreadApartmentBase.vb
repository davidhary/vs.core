Imports System.Threading

''' <summary> Single thread apartment base. </summary>
''' <remarks>
''' David, 3/24/2015
''' http://stackoverflow.com/questions/899350/how-to-copy-the-contents-of-a-string-to-the-clipboard-in-c.
''' </remarks>
Public MustInherit Class SingleThreadApartmentBase

    ' 
    #Region " CONSTRUCTION "

    #Region " CONSTRUCTION "

    ''' <summary> Specialized default constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Protected Sub New()
        MyBase.New()
    End Sub

    #End Region 

    ' 
    #Region " WORKERS "

    #Region " WORKERS "

    ''' <summary> Executes the work as defined by the inheriting class. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Public Sub Go()
        Dim thread As Threading.Thread = New Thread(New ThreadStart(AddressOf Me.DoWork))
        thread.SetApartmentState(ApartmentState.STA)
        thread.Start()
    End Sub

    ''' <summary> The complete reset event. Notifies one or more waiting threads that an event has occurred. </summary>
    ''' <remarks> is used to block and release threads manually. It is created in the non-signaled state. 
    '''           <c>
    '''           https://msdn.microsoft.com/en-us/library/system.threading.manualresetevent%28v=vs.110%29.aspx
    '''           </c>
    '''           It seems that this does not do anything in this case as the event is not set to block, e.g., Wait One.
    '''           </remarks>
    Private ReadOnly _Complete As New ManualResetEvent(False)

    ''' <summary> The thread entry method. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub DoWork()
        Try
            ' sets the state of the event to not signaled causing threads to block
            Me._Complete.Reset()
            Me.Work()
        Catch ex As Exception
            If Me.RetryWorkOnFailed Then
                Try
                    Thread.Sleep(1000)
                    Me.Work()
                Catch
                    ' ex from first exception
                    My.Application.Log.WriteException(ex)
                End Try
            Else
                Throw
            End If
        Finally
            ' sets the state of the event to signaled, allowing waiting events to proceed 
            Me._Complete.Set()
        End Try
    End Sub

    ''' <summary> Gets or sets a value indicating whether to retry work on failed. </summary>
    ''' <value> <c>true</c> if retry work on failed; otherwise <c>false</c> </value>
    Public Property RetryWorkOnFailed() As Boolean

    ''' <summary> Implemented in the inheriting class to do actual work. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Protected MustOverride Sub Work()

    #End Region 

End Class

