Imports Microsoft.SqlServer.MessageBox

''' <summary>
''' Extends the <see cref="ExceptionMessageBox">Exception message dialog</see>.
''' </summary>
''' <remarks> David, 02/14/2014. </remarks>
Public Class MyMessageBox


    #Region " CONSTRUCTION "

#Disable Warning IDE1006 ' Naming Styles
        Private WithEvents _ExceptionMessageBox As MessageBox
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets the exception message box. </summary>
    ''' <value> The exception message box. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private ReadOnly Property ExceptionMessageBox As MessageBox
        Get
            Return Me._ExceptionMessageBox
        End Get
    End Property

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="exception"> The exception. </param>
    Public Sub New(ByVal exception As Exception)
        MyBase.New()
        Me._ExceptionMessageBox = New MessageBox(exception)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    Public Sub New(ByVal text As String, ByVal caption As String)
        MyBase.New()
        Me._ExceptionMessageBox = New MessageBox(text, caption)
    End Sub

    #End Region 

    #Region " FRIEND SHARED "

    ''' <summary> Builds message box icon symbol hash. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <returns>
    ''' A <see cref="Dictionary(Of MyMessageBoxIcon, ExceptionMessageBoxSymbol)">dictionary</see>.
    ''' </returns>
    Private Shared Function BuildMessageBoxIconSymbolHash() As IDictionary(Of MyMessageBoxIcon, ExceptionMessageBoxSymbol)
        Dim dix2 As New Dictionary(Of MyMessageBoxIcon, ExceptionMessageBoxSymbol)
        Dim dix3 As Dictionary(Of MyMessageBoxIcon, ExceptionMessageBoxSymbol) = dix2
        ' same as information: dix3.Add(MessageBoxIcon.Asterisk, ExceptionMessageBoxSymbol.Asterisk)
        ' same as warning: dix3.Add(MessageBoxIcon.Exclamation, ExceptionMessageBoxSymbol.Exclamation)
        dix3.Add(MyMessageBoxIcon.Error, ExceptionMessageBoxSymbol.Error)
        ' same as error: dix3.Add(MessageBoxIcon.Hand, ExceptionMessageBoxSymbol.Hand)
        dix3.Add(MyMessageBoxIcon.Information, ExceptionMessageBoxSymbol.Information)
        dix3.Add(MyMessageBoxIcon.None, ExceptionMessageBoxSymbol.None)
        dix3.Add(MyMessageBoxIcon.Question, ExceptionMessageBoxSymbol.Question)
        ' same as error: dix3.Add(MessageBoxIcon.Stop, ExceptionMessageBoxSymbol.Stop)
        dix3.Add(MyMessageBoxIcon.Warning, ExceptionMessageBoxSymbol.Warning)
        Return dix2
    End Function

    ''' <summary> Converts a value to a symbol. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> Symbol. </returns>
    Public Shared Function ToSymbol(ByVal value As MyMessageBoxIcon) As ExceptionMessageBoxSymbol
        Static hash As IDictionary(Of MyMessageBoxIcon, ExceptionMessageBoxSymbol)
        If hash Is Nothing Then
            hash = MyMessageBox.BuildMessageBoxIconSymbolHash
        End If
        Return If(hash.ContainsKey(value), hash(value), ExceptionMessageBoxSymbol.Information)
    End Function

    #End Region 

    #Region " SHARED "

    ''' <summary>
    ''' Synchronously Invokes the exception display on the apartment thread to permit using the
    ''' clipboard.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="exception"> The exception. </param>
    ''' <returns> <see cref="MyDialogResult">Dialog result</see>. </returns>
    Public Shared Function ShowDialog(ByVal exception As Exception) As MyDialogResult
        Dim box As New MessageBox(exception)
        Return box.ShowDialog()
    End Function

    ''' <summary>
    ''' Synchronously Invokes the exception display on the apartment thread to permit using the
    ''' clipboard.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <returns> <see cref="MyDialogResult">Dialog result</see>. </returns>
    Public Shared Function ShowDialog(ByVal text As String, ByVal caption As String) As MyDialogResult
        Dim box As New MessageBox(text, caption)
        Return box.ShowDialog()
    End Function

    ''' <summary>
    ''' Synchronously Invokes the exception display on the apartment thread to permit using the
    ''' clipboard.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="exception">     The exception. </param>
    ''' <param name="icon">          The icon. </param>
    ''' <param name="dialogResults"> The dialog results. </param>
    ''' <returns> <see cref="MyDialogResult">Dialog result</see>. </returns>
    Public Shared Function ShowDialog(ByVal exception As Exception, ByVal icon As MyMessageBoxIcon, ByVal dialogResults() As MyDialogResult) As MyDialogResult
        If dialogResults Is Nothing Then Throw New ArgumentNullException(NameOf(dialogResults))
        Dim box As New MessageBox(exception)
        Dim buttonText As New List(Of String)
        For Each d As MyDialogResult In dialogResults
            buttonText.Add(d.ToString)
        Next
        Return box.ShowDialog(buttonText.ToArray, MyMessageBox.ToSymbol(icon), ExceptionMessageBoxDefaultButton.Button1, dialogResults)
    End Function

    ''' <summary>
    ''' Synchronously Invokes the exception display on the apartment thread to permit using the
    ''' clipboard.
    ''' </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="text">          The exception. </param>
    ''' <param name="caption">       The caption. </param>
    ''' <param name="icon">          The icon. </param>
    ''' <param name="dialogResults"> The dialog results. </param>
    ''' <returns> <see cref="MyDialogResult">Dialog result</see>. </returns>
    Public Shared Function ShowDialog(ByVal text As String, ByVal caption As String, ByVal icon As MyMessageBoxIcon, ByVal dialogResults() As MyDialogResult) As MyDialogResult
        If dialogResults Is Nothing Then Throw New ArgumentNullException(NameOf(dialogResults))
        Dim box As New MessageBox(text, caption)
        Dim buttonText As New List(Of String)
        For Each d As MyDialogResult In dialogResults
            buttonText.Add(d.ToString)
        Next
        Return box.ShowDialog(buttonText.ToArray, MyMessageBox.ToSymbol(icon), ExceptionMessageBoxDefaultButton.Button1, dialogResults)
    End Function

    ''' <summary> Displays the message box. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="exception"> The exception. </param>
    ''' <param name="icon">      The icon. </param>
    ''' <returns> <see cref="MyDialogResult">Dialog result</see>. </returns>
    Public Shared Function ShowDialogAbortIgnore(ByVal exception As Exception, ByVal icon As MyMessageBoxIcon) As MyDialogResult
        Return MyMessageBox.ShowDialog(exception, icon, New MyDialogResult() {MyDialogResult.Abort, MyDialogResult.Ignore})
    End Function

    ''' <summary> Displays the message box. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="exception"> The exception. </param>
    ''' <returns> <see cref="MyDialogResult">Dialog result</see>. </returns>
    Public Shared Function ShowDialogAbortIgnore(ByVal exception As Exception) As MyDialogResult
        Return MyMessageBox.ShowDialog(exception, MyMessageBoxIcon.Error, New MyDialogResult() {MyDialogResult.Abort, MyDialogResult.Ignore})
    End Function

    ''' <summary> Displays the message box. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <param name="icon">    The icon. </param>
    ''' <returns> <see cref="MyDialogResult">Dialog result</see>. </returns>
    Public Shared Function ShowDialogAbortIgnore(ByVal text As String, ByVal caption As String, ByVal icon As MyMessageBoxIcon) As MyDialogResult
        Return MyMessageBox.ShowDialog(text, caption, icon, New MyDialogResult() {MyDialogResult.Abort, MyDialogResult.Ignore})
    End Function

    ''' <summary> Displays the message box. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="exception"> The exception. </param>
    ''' <param name="icon">      The icon. </param>
    ''' <returns>
    ''' Either <see cref="MyDialogResult.Ignore">ignore</see> or
    ''' <see cref="MyDialogResult.OK">Okay</see>.
    ''' </returns>
    Public Shared Function ShowDialogIgnoreExit(ByVal exception As Exception, ByVal icon As MyMessageBoxIcon) As MyDialogResult
        Dim box As New MyMessageBox(exception)
        Return box.ShowDialogIgnoreExit(icon)
    End Function

    ''' <summary> Displays the message box. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <param name="icon">    The icon. </param>
    ''' <returns>
    ''' Either <see cref="MyDialogResult.Ignore">ignore</see> or
    ''' <see cref="MyDialogResult.OK">Okay</see>.
    ''' </returns>
    Public Shared Function ShowDialogIgnoreExit(ByVal text As String, ByVal caption As String, ByVal icon As MyMessageBoxIcon) As MyDialogResult
        Dim box As New MyMessageBox(text, caption)
        Return box.ShowDialogIgnoreExit(icon)
    End Function

    ''' <summary> Displays the message box. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="exception"> The exception. </param>
    ''' <param name="icon">      The icon. </param>
    ''' <returns> <see cref="MyDialogResult.OK">Okay</see>. </returns>
    Public Shared Function ShowDialogExit(ByVal exception As Exception, ByVal icon As MyMessageBoxIcon) As MyDialogResult
        Dim box As New MyMessageBox(exception)
        Return box.ShowDialogExit(icon)
    End Function

    ''' <summary> Displays the message box. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <param name="icon">    The icon. </param>
    ''' <returns> <see cref="MyDialogResult.OK">Okay</see>. </returns>
    Public Shared Function ShowDialogExit(ByVal text As String, ByVal caption As String, ByVal icon As MyMessageBoxIcon) As MyDialogResult
        Dim box As New MyMessageBox(text, caption)
        Return box.ShowDialogExit(icon)
    End Function

    ''' <summary> Shows the okay dialog. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <param name="icon">    The icon. </param>
    ''' <returns> A DialogResult. </returns>
    Public Shared Function ShowDialogOkay(ByVal text As String, ByVal caption As String, ByVal icon As MyMessageBoxIcon) As MyDialogResult
        Dim box As New MyMessageBox(text, caption)
        Return box.ShowDialogExit(icon)
    End Function

    ''' <summary> Shows the okay dialog. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <returns> A DialogResult. </returns>
    Public Shared Function ShowDialogOkay(ByVal text As String, ByVal caption As String) As MyDialogResult
        Return MyMessageBox.ShowDialogOkay(text, caption, MyMessageBoxIcon.Information)
    End Function

    ''' <summary> Shows the okay/cancel dialog. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <param name="icon">    The icon. </param>
    ''' <returns> A DialogResult. </returns>
    Public Shared Function ShowDialogOkayCancel(ByVal text As String, ByVal caption As String, ByVal icon As MyMessageBoxIcon) As MyDialogResult
        Return MyMessageBox.ShowDialog(text, caption, icon, New MyDialogResult() {MyDialogResult.Ok, MyDialogResult.Cancel})
    End Function

    ''' <summary> Shows the okay/cancel dialog. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <returns> A DialogResult. </returns>
    Public Shared Function ShowDialogOkayCancel(ByVal text As String, ByVal caption As String) As MyDialogResult
        Return MyMessageBox.ShowDialogOkayCancel(text, caption, MyMessageBoxIcon.Information)
    End Function

    ''' <summary> Shows the Cancel/Okay dialog. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <param name="icon">    The icon. </param>
    ''' <returns> A DialogResult. </returns>
    Public Shared Function ShowDialogCancelOkay(ByVal text As String, ByVal caption As String, ByVal icon As MyMessageBoxIcon) As MyDialogResult
        Return MyMessageBox.ShowDialog(text, caption, icon, New MyDialogResult() {MyDialogResult.Cancel, MyDialogResult.Ok})
    End Function

    ''' <summary> Shows the Cancel/Okay dialog. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <returns> A DialogResult. </returns>
    Public Shared Function ShowDialogCancelOkay(ByVal text As String, ByVal caption As String) As MyDialogResult
        Return MyMessageBox.ShowDialogCancelOkay(text, caption, MyMessageBoxIcon.Information)
    End Function

    ''' <summary> Shows the yes/no dialog. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <param name="icon">    The icon. </param>
    ''' <returns> A DialogResult. </returns>
    Public Shared Function ShowDialogYesNo(ByVal text As String, ByVal caption As String, ByVal icon As MyMessageBoxIcon) As MyDialogResult
        Return MyMessageBox.ShowDialog(text, caption, icon, New MyDialogResult() {MyDialogResult.Yes, MyDialogResult.No})
    End Function

    ''' <summary> Shows the yes/no dialog. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <returns> A DialogResult. </returns>
    Public Shared Function ShowDialogYesNo(ByVal text As String, ByVal caption As String) As MyDialogResult
        Return MyMessageBox.ShowDialogYesNo(text, caption, MyMessageBoxIcon.Question)
    End Function

    ''' <summary> Shows the No/Yes dialog. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <param name="icon">    The icon. </param>
    ''' <returns> A DialogResult. </returns>
    Public Shared Function ShowDialogNoYes(ByVal text As String, ByVal caption As String, ByVal icon As MyMessageBoxIcon) As MyDialogResult
        Return MyMessageBox.ShowDialog(text, caption, icon, New MyDialogResult() {MyDialogResult.No, MyDialogResult.Yes})
    End Function

    ''' <summary> Shows the No/Yes dialog. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <returns> A DialogResult. </returns>
    Public Shared Function ShowDialogNoYes(ByVal text As String, ByVal caption As String) As MyDialogResult
        Return MyMessageBox.ShowDialogNoYes(text, caption, MyMessageBoxIcon.Question)
    End Function

    #End Region 

    #Region " SHOW DIALOG "

    ''' <summary> Shows the exception display. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <returns> <see cref="MyDialogResult">Dialog result</see>. </returns>
    ''' <example> Example 1: Simple Message <code>
    '''           Dim box As MyMessageBox = New MyMessageBox(message)
    '''           me.ShowDialog(owner)      </code></example>
    ''' <example> Example 2: Message box with exception message. <code>
    '''           Dim box As MyMessageBox = New MyMessageBox(exception)
    '''           me.ShowDialog(owner)      </code></example>
    Public Function ShowDialog() As MyDialogResult
        Return Me.ExceptionMessageBox.ShowDialog()
    End Function

    #End Region 


    #Region " SHOW DIALOG - PREDEFINED BUTTONS "

    ''' <summary> Displays the message box. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="icon"> The icon. </param>
    ''' <returns> <see cref="MyDialogResult">Dialog result</see>. </returns>
    Public Function ShowDialogAbortIgnore(ByVal icon As MyMessageBoxIcon) As MyDialogResult
        Return Me.ExceptionMessageBox.ShowDialog(New String() {"Abort", "Ignore"}, MyMessageBox.ToSymbol(icon),
                                                  ExceptionMessageBoxDefaultButton.Button1,
                                                  New MyDialogResult() {MyDialogResult.Abort, MyDialogResult.Ignore})
    End Function

    ''' <summary> Displays the message box. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="icon"> The icon. </param>
    ''' <returns>
    ''' Either <see cref="MyDialogResult.Ignore">ignore</see> or
    ''' <see cref="MyDialogResult.OK">Okay</see>.
    ''' </returns>
    Public Function ShowDialogIgnoreExit(ByVal icon As MyMessageBoxIcon) As MyDialogResult
        Return Me.ExceptionMessageBox.ShowDialog(New String() {"Ignore", "Exit"}, MyMessageBox.ToSymbol(icon),
                                                  ExceptionMessageBoxDefaultButton.Button1,
                                                  New MyDialogResult() {MyDialogResult.Ignore, MyDialogResult.Ok})
    End Function

    ''' <summary> Displays the message box. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="icon"> The icon. </param>
    ''' <returns> <see cref="MyDialogResult.OK">Okay</see>. </returns>
    Public Function ShowDialogExit(ByVal icon As MyMessageBoxIcon) As MyDialogResult
        Return Me.ExceptionMessageBox.ShowDialog(New String() {"Exit"}, MyMessageBox.ToSymbol(icon),
                                                  ExceptionMessageBoxDefaultButton.Button1, New MyDialogResult() {MyDialogResult.Ok})
    End Function

    #End Region 

End Class

''' <summary> Values that represent dialog results. </summary>
''' <remarks> David, 2020-09-12. </remarks>
Public Enum MyDialogResult
    ''' <summary> An enum constant representing the none option. </summary>
    None = Windows.Forms.DialogResult.None
    ''' <summary> An enum constant representing the ok option. </summary>
    Ok = Windows.Forms.DialogResult.OK
    ''' <summary> An enum constant representing the cancel option. </summary>
    Cancel = Windows.Forms.DialogResult.Cancel
    ''' <summary> An enum constant representing the abort option. </summary>
    Abort = Windows.Forms.DialogResult.Abort
    ''' <summary> An enum constant representing the retry option. </summary>
    Retry = Windows.Forms.DialogResult.Retry
    ''' <summary> An enum constant representing the ignore option. </summary>
    Ignore = Windows.Forms.DialogResult.Ignore
    ''' <summary> An enum constant representing the yes option. </summary>
    Yes = Windows.Forms.DialogResult.Yes
    ''' <summary> An enum constant representing the no option. </summary>
    No = Windows.Forms.DialogResult.No
End Enum

''' <summary> Values that represent my message box icons. </summary>
''' <remarks> David, 2020-09-12. </remarks>
<CodeAnalysis.SuppressMessage("Design", "CA1027:Mark enums with FlagsAttribute", Justification:="<Pending>")>
Public Enum MyMessageBoxIcon
    ''' <summary> An enum constant representing the none option. </summary>
    None = Windows.Forms.MessageBoxIcon.None
    ''' <summary> An enum constant representing the asterisk option. </summary>
    Asterisk = Windows.Forms.MessageBoxIcon.Asterisk
    ''' <summary> An enum constant representing the error] option. </summary>
    [Error] = Windows.Forms.MessageBoxIcon.Error
    ''' <summary> An enum constant representing the exclamation option. </summary>
    Exclamation = Windows.Forms.MessageBoxIcon.Exclamation
    ''' <summary> An enum constant representing the hand option. </summary>
    Hand = Windows.Forms.MessageBoxIcon.Hand
    ''' <summary> An enum constant representing the information option. </summary>
    Information = Windows.Forms.MessageBoxIcon.Information
    ''' <summary> An enum constant representing the question option. </summary>
    Question = Windows.Forms.MessageBoxIcon.Question
    ''' <summary> An enum constant representing the stop] option. </summary>
    [Stop] = Windows.Forms.MessageBoxIcon.Stop
    ''' <summary> An enum constant representing the warning option. </summary>
    Warning = Windows.Forms.MessageBoxIcon.Warning
End Enum
