Imports Microsoft.SqlServer.MessageBox

''' <summary>
''' Encapsulates the <see cref="Microsoft.SqlServer.MessageBox.ExceptionMessageBox"/> exception
''' message box.
''' </summary>
''' <remarks> David, 1/17/2019. </remarks>
Public Class MessageBox
    Inherits Microsoft.SqlServer.MessageBox.ExceptionMessageBox

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="exception"> The exception. </param>
    Public Sub New(ByVal exception As Exception)
        MyBase.New(exception)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    Public Sub New(ByVal text As String, ByVal caption As String)
        MyBase.New(text, caption)
    End Sub

    ''' <summary> My exception message box on copy to clipboard. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Copy to clipboard event information. </param>
    Private Sub MyExceptionMessageBox_OnCopyToClipboard(sender As Object, e As Microsoft.SqlServer.MessageBox.CopyToClipboardEventArgs) Handles Me.OnCopyToClipboard
        SafeClipboardSetDataObject.SetDataObject(e.ClipboardText)
        e.EventHandled = True
    End Sub

    ''' <summary> Converts a value to a dialog result. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> Value as a DialogResult. </returns>
    Friend Shared Function ToDialogResult(ByVal value As Windows.Forms.DialogResult) As MyDialogResult
        Return CType(value, MyDialogResult)
    End Function

    ''' <summary> Converts a value to a message box icon. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> Value as a MessageBoxIcon. </returns>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private Shared Function ToMessageBoxIcon(ByVal value As Windows.Forms.MessageBoxIcon) As MyMessageBoxIcon
        Return CType(value, MyMessageBoxIcon)
    End Function

    ''' <summary> Displays the message box. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <returns> <see cref="MyDialogResult">Dialog result</see>. </returns>
    Public Function ShowDialog() As MyDialogResult
        Dim r As MyDialogResult = MyDialogResult.Abort
        Dim oThread As Threading.Thread
        oThread = New Threading.Thread(New Threading.ParameterizedThreadStart(Sub() r = MessageBox.ToDialogResult(Me.Show(Nothing))))
        oThread.SetApartmentState(Threading.ApartmentState.STA)
        oThread.Start()
        oThread.Join()
        Do While oThread.IsAlive
            Threading.Thread.Sleep(1000)
        Loop
        Return r
    End Function

    ''' <summary> Displays the message box. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <exception cref="InvalidOperationException">   Thrown when the requested operation is
    '''                                                invalid. </exception>
    ''' <param name="buttonText">    The buttons text. </param>
    ''' <param name="symbol">        The symbol. </param>
    ''' <param name="defaultButton"> The default button. </param>
    ''' <param name="dialogResults"> The dialog results corresponding to the
    '''                              <paramref name="buttonText">buttons</paramref>. </param>
    ''' <returns> <see cref="MyDialogResult">Dialog result</see>. </returns>
    ''' <example>
    ''' Example 1: Simple Message
    ''' <code>
    ''' Dim box As ExceptionMessageBox = New ExceptionMessageBox(exception)
    ''' box.InvokeShow(owner, New String(){"A","B"},ExceptionMessageBoxSymbol.Asterisk,
    '''                                   ExceptionMessageBoxDefaultButton.Button1)
    ''' </code>
    ''' </example>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function ShowDialog(ByVal buttonText As String(), ByVal symbol As ExceptionMessageBoxSymbol,
                               ByVal defaultButton As ExceptionMessageBoxDefaultButton,
                               ByVal dialogResults As MyDialogResult()) As MyDialogResult

        If buttonText Is Nothing Then
            Throw New ArgumentNullException(NameOf(buttonText))
        ElseIf buttonText.Count = 0 OrElse buttonText.Count > 5 Then
            Throw New ArgumentOutOfRangeException(NameOf(buttonText), "Must have between 1 and 5 values")
        ElseIf dialogResults Is Nothing Then
            Throw New ArgumentNullException(NameOf(dialogResults))
        ElseIf dialogResults.Count = 0 OrElse dialogResults.Count > 5 Then
            Throw New ArgumentOutOfRangeException(NameOf(dialogResults), "Must have between 1 and 5 values")
        ElseIf dialogResults.Count <> buttonText.Count Then
            Throw New ArgumentOutOfRangeException(NameOf(dialogResults), "Must have the same count as button text")
        End If

        ' Set the names of the custom buttons.
        Select Case buttonText.Count
            Case 1
                Me.SetButtonText(buttonText(0))
            Case 2
                Me.SetButtonText(buttonText(0), buttonText(1))
            Case 3
                Me.SetButtonText(buttonText(0), buttonText(1), buttonText(2))
            Case 4
                Me.SetButtonText(buttonText(0), buttonText(1), buttonText(2), buttonText(3))
            Case 5
                Me.SetButtonText(buttonText(0), buttonText(1), buttonText(2), buttonText(4))
        End Select
        Me.DefaultButton = defaultButton
        Me.Symbol = symbol
        Me.Buttons = ExceptionMessageBoxButtons.Custom
        Me.ShowDialog()
        Select Case Me.CustomDialogResult
            Case ExceptionMessageBoxDialogResult.Button1
                Return dialogResults(0)
            Case ExceptionMessageBoxDialogResult.Button2
                Return dialogResults(1)
            Case ExceptionMessageBoxDialogResult.Button3
                Return dialogResults(2)
            Case ExceptionMessageBoxDialogResult.Button4
                Return dialogResults(3)
            Case ExceptionMessageBoxDialogResult.Button5
                Return dialogResults(4)
            Case Else
                Throw New InvalidOperationException($"Failed displaying the message box with {NameOf(ExceptionMessageBox.CustomDialogResult)}={Me.CustomDialogResult}")
        End Select

    End Function

End Class

