''' <summary> Dialog for selecting a file to open. </summary>
''' <remarks> David, 1/22/2019. </remarks>
Public Class OpenFileDialog
    Inherits FileDialogBase


    #Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs this class. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

    #End Region 


    #Region " OPEN DIALOG IMPLEMENTATION "

    ''' <summary> Opens the File Open dialog box and selects a file name. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <returns> A Windows.Forms.DialogResult. </returns>
    Public Overridable Function TrySelectFile() As Boolean
        Using fileDialog As New Windows.Forms.OpenFileDialog
            ' Use the common dialog box
            fileDialog.CheckFileExists = True
            fileDialog.CheckPathExists = True
            fileDialog.Title = Me.FileDialogTitle
            fileDialog.FileName = Me.FilePathName
            Dim fi As New System.IO.FileInfo(Me.FilePathName)
            If fi.Directory.Exists Then
                fileDialog.InitialDirectory = fi.DirectoryName
            End If
            fileDialog.RestoreDirectory = True
            fileDialog.Filter = Me.FileDialogFilter
            fileDialog.DefaultExt = Me.FileExtension
            Dim result As Boolean = Windows.Forms.DialogResult.OK = fileDialog.ShowDialog
            If result Then Me.FilePathName = fileDialog.FileName
            Return result
        End Using
    End Function

    ''' <summary> Try find file. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <param name="fileName"> Filename of the file. </param>
    ''' <returns> A String if file found or empty if not. </returns>
    Public Shared Function TryFindFile(ByVal fileName As String) As String
        Dim dialog As New isr.Core.OpenFileDialog With {
            .FilePathName = fileName
        }
        If Not dialog.FileExists Then
            fileName = If(dialog.TrySelectFile(), dialog.FilePathName, String.Empty)
        End If
        Return fileName
    End Function

    #End Region 

End Class
