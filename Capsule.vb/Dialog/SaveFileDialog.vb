''' <summary> Dialog for selecting a file name to save. </summary>
''' <remarks> David, 1/22/2019. </remarks>
Public Class SaveFileDialog
    Inherits FileDialogBase

    #Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs this class. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

    #End Region 


    #Region " SAVE DIALOG IMPLEMENTATION "

    ''' <summary> Opens the File Save dialog box and selects a file name to save. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <returns> A Windows.Forms.DialogResult. </returns>
    Public Overridable Function TrySelectFile() As Boolean
        Using fileDialog As New Windows.Forms.SaveFileDialog
            ' Use the common dialog box
            fileDialog.CheckFileExists = False
            fileDialog.CheckPathExists = True
            fileDialog.Title = Me.FileDialogTitle
            fileDialog.Filter = Me.FileDialogFilter
            fileDialog.DefaultExt = Me.FileExtension
            Dim fi As New System.IO.FileInfo(Me.FilePathName)
            If fi.Directory.Exists Then
                fileDialog.InitialDirectory = fi.DirectoryName
            End If
            fileDialog.RestoreDirectory = True
            Dim result As Boolean = Windows.Forms.DialogResult.OK = fileDialog.ShowDialog
            If result Then Me.FilePathName = fileDialog.FileName
            Return result
        End Using
    End Function

    #End Region 

End Class
