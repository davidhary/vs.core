''' <summary> A file dialog base. </summary>
''' <remarks> David, 1/22/2019. </remarks>
Public Class FileDialogBase

    ' 
    #Region " CONSTRUCTION and CLEANUP "

    #Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs this class. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

    #End Region 

    ' 
    #Region " DIALOG INFORMATION "

    #Region " DIALOG INFORMATION "

    ''' <summary> Gets or sets the file dialog filter. </summary>
    ''' <value> The file dialog filter. </value>
    Public Property FileDialogFilter As String

    ''' <summary> Gets or sets the file dialog title. </summary>
    ''' <value> The file dialog title. </value>
    Public Property FileDialogTitle As String

    #End Region 

    ' 
    #Region " FILE INFORMATION "

    #Region " FILE INFORMATION "

    ''' <summary> Gets or sets the filename of the file. </summary>
    ''' <value> The name of the file. </value>
    Public Property FileName As String

    ''' <summary> Queries if a given file exists. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FileExists() As Boolean
        Dim fi As New System.IO.FileInfo(Me.FilePathName)
        Return fi.Exists
    End Function

    ''' <summary> Gets or sets the file extension. </summary>
    ''' <value> The file extension. </value>
    Public Property FileExtension As String

    ''' <summary> Full pathname of the file. </summary>
    Private _FilePathName As String

    ''' <summary> Gets or sets the file name. </summary>
    ''' <remarks> Use this property to get or set the file name. </remarks>
    ''' <value> <c>FilePathName</c> is a String property. </value>
    Public Property FilePathName() As String
        Get
            If Me._FilePathName.Length = 0 Then
                ' set default file name if empty.
                Me._FilePathName = System.IO.Path.Combine(FileDialogBase.DefaultFolderPath,
                                                          My.Application.Info.AssemblyName & Me.FileExtension)
            End If
            Return Me._FilePathName
        End Get
        Set(ByVal value As String)
            Me._FilePathName = value
        End Set
    End Property

    #End Region 

    ' 
    #Region " IO "

    #Region " IO "

    ''' <summary> Determines whether the specified folder path is writable. </summary>
    ''' <remarks>
    ''' Uses a temporary random file name to test if the file can be created. The file is deleted
    ''' thereafter.
    ''' </remarks>
    ''' <param name="path"> The path. </param>
    ''' <returns> <c>True</c> if the specified path is writable; otherwise, <c>False</c>. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function IsFolderWritable(ByVal path As String) As Boolean
        Dim filePath As String = String.Empty
        Dim affirmative As Boolean = False
        Try
            filePath = System.IO.Path.Combine(path, System.IO.Path.GetRandomFileName())
            Using s As System.IO.FileStream = System.IO.File.Open(filePath, System.IO.FileMode.OpenOrCreate)
            End Using
            affirmative = True
        Catch
        Finally
            ' SS reported an exception from this test possibly indicating that Windows allowed writing the file 
            ' by failed report deletion. Or else, Windows raised another exception type.
            Try
                If System.IO.File.Exists(filePath) Then
                    System.IO.File.Delete(filePath)
                End If
            Catch
            End Try
        End Try
        Return affirmative
    End Function

    ''' <summary> Selects the default file path. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    ''' <returns>
    ''' The default file path: either the application folder or the user documents folder.
    ''' </returns>
    Public Shared Function DefaultFolderPath() As String
        Dim candidatePath As String = My.Application.Info.DirectoryPath
        If Not FileDialogBase.IsFolderWritable(candidatePath) Then
            candidatePath = My.Computer.FileSystem.SpecialDirectories.MyDocuments
        End If
        Return candidatePath
    End Function

    #End Region 

End Class
