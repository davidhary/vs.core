''' <summary> Dialog for print preview and print. </summary>
''' <remarks> David, 2020-09-10. </remarks>
Public Class PrintPreviewDialog
    Inherits FileDialogBase


    #Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs this class. </summary>
    ''' <remarks> David, 2020-09-12. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

    #End Region 


    #Region " PRINT PREVIEW DIALOG IMPLEMENTATION "

    ''' <summary> Opens the print preview dialog. </summary>
    ''' <remarks> David, 2020-09-10. </remarks>
    ''' <param name="document"> The document. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Public Overridable Sub ShowDialog(ByVal document As System.Drawing.Printing.PrintDocument)
        Dim activity As String = String.Empty
        Try
            activity = "preparing print preview dialog"
            Using ppd As New isr.Core.MyPrintPreviewDialog With {.Document = document}
                activity = "showing print preview dialog"
                ppd.ShowDialog()
            End Using
        Catch ex As Exception
            MyMessageBox.ShowDialog($"Exception {activity};. {ex}", "Print Dialog Exception", MyMessageBoxIcon.Error, New MyDialogResult() {MyDialogResult.Ok})
        End Try
    End Sub

    #End Region 

End Class
