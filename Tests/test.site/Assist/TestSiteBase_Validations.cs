using System;
using System.Data;
using System.Data.Common;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.MSTest
{
    public abstract partial class TestSiteBase
    {

        /// <summary> Validated data row. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        /// <returns> A DataRow. </returns>
        public static DataRow ValidatedDataRow( TestContext testContext )
        {
            return testContext is null ? throw new ArgumentNullException( nameof( testContext ) ) : ValidatedDataRow( testContext.DataRow );
        }

        /// <summary> Validated data row. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="dataRow"> The data row. </param>
        /// <returns> A DataRow. </returns>
        public static DataRow ValidatedDataRow( DataRow dataRow )
        {
            return dataRow is null ? throw new ArgumentNullException( nameof( dataRow ) ) : dataRow;
        }

        /// <summary> Validated connection. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        /// <returns> A DbConnection. </returns>
        public static DbConnection ValidatedConnection( TestContext testContext )
        {
            return testContext is null ? throw new ArgumentNullException( nameof( testContext ) ) : Validatedconnection( testContext.DataConnection );
        }

        /// <summary> Validated connection. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="connection"> The connection. </param>
        /// <returns> A connection. </returns>
        [CLSCompliant( false )]
        public static DbConnection Validatedconnection( DbConnection connection )
        {
            return connection is null ? throw new ArgumentNullException( nameof( connection ) ) : connection;
        }

    }
}
