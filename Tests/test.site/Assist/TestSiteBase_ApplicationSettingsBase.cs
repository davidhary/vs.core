using System;
using System.Collections.Generic;
using System.Linq;

namespace isr.Core.MSTest
{
    /// <summary> Extends the <see cref="System.Configuration.ApplicationSettingsBase"/>. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-02-12 </para>
    /// </remarks>
    public partial class TestSiteBase : System.Configuration.ApplicationSettingsBase
    {

        #region " CONVERSIONS "

        /// <summary> Converts a value to a nullable double. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> Value as a Double? </returns>
        public static double? ToNullableDouble( string value )
        {
            return string.IsNullOrWhiteSpace( value ) ? new double?() : Convert.ToDouble( value );
        }

        /// <summary> Converts a value to a nullable double. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> Value as a Double? </returns>
        public static double? ToNullableDouble( object value )
        {
            return value is null ? new double?() : Convert.ToDouble( value.ToString() );
        }

        /// <summary> Converts a value to a nullable Int32. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> Value as a Int32? </returns>
        public static int? ToNullableInt32( string value )
        {
            return string.IsNullOrWhiteSpace( value ) ? new int?() : Convert.ToInt32( value );
        }

        /// <summary> Converts a value to a nullable Int32. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> Value as a Int32? </returns>
        public static int? ToNullableInt32( object value )
        {
            return value is null ? new int?() : Convert.ToInt32( value.ToString() );
        }

        #endregion

        #region " SEPERATED VALUES SUPPORT "

        /// <summary> Gets or sets the rows delimiter. </summary>
        /// <value> The rows delimiter. </value>
        public char[] RowsDelimiter { get; set; } = Environment.NewLine.ToCharArray();


        /// <summary> Gets or sets the values delimiter. </summary>
        /// <value> The values delimiter. </value>
        public char[] ValuesDelimiter { get; set; } = new char[] { '|' };

        /// <summary> Splits  the <paramref name="values"/> to array of arrays. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="values">         The values. </param>
        /// <param name="itemDelimiter">  The item delimiter. </param>
        /// <param name="groupDelimiter"> The group delimiter. </param>
        /// <returns> A T()() </returns>
        public static T[][] Split<T>( string values, char[] itemDelimiter, char[] groupDelimiter )
        {
            T[][] results = null;
            if ( !string.IsNullOrWhiteSpace( values ) )
            {
                var rows = values.Split( groupDelimiter );
                results = new T[(rows.Count())][];
                int i = 0;
                foreach ( string row in rows )
                {
                    var l = new List<T>();
                    if ( !string.IsNullOrWhiteSpace( row ) )
                    {
                        foreach ( string v in row.Split( itemDelimiter ) )
                        {
                            l.Add( ( T ) Convert.ChangeType( v.Trim(), typeof( T ) ) );
                        }
                    }

                    results[i] = l.ToArray();
                    i += 1;
                }
            }

            return results;
        }

        /// <summary>
        /// Joins the <paramref name="values"/> to a string of values each separated by
        /// <paramref name="itemDelimiter"/> and then by <paramref name="groupDelimiter"/>
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="values">         The values array of arrays which to join. </param>
        /// <param name="itemDelimiter">  The item delimiter. </param>
        /// <param name="groupDelimiter"> The group delimiter. </param>
        /// <returns> A String. </returns>
        public static string Join<T>( T[][] values, char[] itemDelimiter, char[] groupDelimiter )
        {
            var result = new System.Text.StringBuilder();
            foreach ( T[] row in values )
            {
                foreach ( T v in row )
                {
                    _ = result.Append( $"{v}{itemDelimiter}" );
                }

                _ = result.Append( groupDelimiter );
            }

            return result.ToString().TrimEnd( groupDelimiter ).TrimEnd( itemDelimiter );
        }

        #endregion

        #region " BASE PROPERTIES "

        /// <summary>   Gets or sets the application setting <see cref="Boolean"/> value. </summary>
        /// <remarks>   David, 2020-09-15. </remarks>
        /// <param name="dummy"> The type identifier dummy value. </param>
        /// <param name="name">  (Optional) Name of the caller member. </param>
        /// <returns>   The application setting <see cref="Boolean"/> value. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "a type identifier" )]
        protected bool AppSettingGetter( bool dummy, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            return Convert.ToBoolean( this[name] );
        }

        /// <summary>   Application setting setter. </summary>
        /// <remarks>   David, 2020-09-15. </remarks>
        /// <param name="value">    The value. </param>
        /// <param name="name">     (Optional) Name of the caller member. </param>
        protected void AppSettingSetter( bool value, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            this[name] = value;
        }

        /// <summary> Gets or sets the application setting values. </summary>
        /// <param name="dummy"> The type identifier dummy value. </param>
        /// <param name="name">  (Optional) Name of the caller member. </param>
        /// <value> The application setting values. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "a type identifier" )]
        protected IEnumerable<bool> AppSettingGetter( IEnumerable<bool> dummy, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            string values = Convert.ToString( base.PropertyValues[name].SerializedValue );
            var l = new List<bool>();
            if ( !string.IsNullOrWhiteSpace( values ) )
            {
                foreach ( string v in values.Split( this.ValuesDelimiter ) )
                {
                    l.Add( Convert.ToBoolean( v.Trim() ) );
                }
            }

            return l;
        }

        /// <summary>   Application setting setter. </summary>
        /// <remarks>   David, 2020-09-15. </remarks>
        /// <param name="value">    The value. </param>
        /// <param name="name">     (Optional) Name of the caller member. </param>
        protected void AppSettingSetter( IEnumerable<bool> value, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            var builder = new System.Text.StringBuilder();
            foreach ( bool v in value )
            {
                _ = builder.Append( $"{v}{this.ValuesDelimiter}" );
            }

            base.PropertyValues[name].SerializedValue = builder.ToString().TrimEnd( this.ValuesDelimiter );
        }

        /// <summary> Gets or sets the application setting <see cref="Byte"/> value. </summary>
        /// <param name="dummy"> The type identifier dummy value. </param>
        /// <param name="name">  (Optional) Name of the caller member. </param>
        /// <value> The application setting <see cref="Byte"/> value. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "a type identifier" )]
        protected byte AppSettingGetter( byte dummy, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            return Convert.ToByte( this[name] );
        }

        /// <summary>   Application setting setter. </summary>
        /// <remarks>   David, 2020-09-15. </remarks>
        /// <param name="value">    The value. </param>
        /// <param name="name">     (Optional) Name of the caller member. </param>
        protected void AppSettingSetter( byte value, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            this[name] = value;
        }

        /// <summary> Gets or sets the application setting date time offset. </summary>
        /// <param name="dummy"> The type identifier dummy value. </param>
        /// <param name="name">  (Optional) Name of the caller member. </param>
        /// <value> The application setting date time offset. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "a type identifier" )]
        protected DateTimeOffset AppSettingGetter( DateTimeOffset dummy, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            return DateTimeOffset.Parse( this[name].ToString() );
        }

        /// <summary>   Application setting setter. </summary>
        /// <remarks>   David, 2020-09-15. </remarks>
        /// <param name="value">    The value. </param>
        /// <param name="name">     (Optional) Name of the caller member. </param>
        protected void AppSettingSetter( DateTimeOffset value, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            this[name] = value;
        }

        /// <summary> Gets or sets the application setting date time. </summary>
        /// <param name="dummy"> The type identifier dummy value. </param>
        /// <param name="name">  (Optional) Name of the caller member. </param>
        /// <value> The application setting date time. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "a type identifier" )]
        protected DateTime AppSettingGetter( DateTime dummy, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            return DateTime.Parse( this[name].ToString() );
        }

        /// <summary>   Application setting setter. </summary>
        /// <remarks>   David, 2020-09-15. </remarks>
        /// <param name="value">    The value. </param>
        /// <param name="name">     (Optional) Name of the caller member. </param>
        protected void AppSettingSetter( DateTime value, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            this[name] = value;
        }

        /// <summary> Gets or sets the application setting <see cref="Double"/> value. </summary>
        /// <param name="dummy"> The type identifier dummy value. </param>
        /// <param name="name">  (Optional) Name of the caller member. </param>
        /// <value> The application setting <see cref="Double"/> value. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "a type identifier" )]
        protected decimal AppSettingGetter( decimal dummy, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            return Convert.ToDecimal( this[name] );
        }

        /// <summary>   Application setting setter. </summary>
        /// <remarks>   David, 2020-09-15. </remarks>
        /// <param name="value">    The value. </param>
        /// <param name="name">     (Optional) Name of the caller member. </param>
        protected void AppSettingSetter( decimal value, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            this[name] = value;
        }

        /// <summary> Gets or sets the application setting <see cref="Double"/> value. </summary>
        /// <param name="dummy"> The type identifier dummy value. </param>
        /// <param name="name">  (Optional) Name of the caller member. </param>
        /// <value> The application setting <see cref="Double"/> value. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "a type identifier" )]
        protected double AppSettingGetter( double dummy, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            return Convert.ToDouble( this[name] );
        }

        /// <summary>   Application setting setter. </summary>
        /// <remarks>   David, 2020-09-15. </remarks>
        /// <param name="value">    The value. </param>
        /// <param name="name">     (Optional) Name of the caller member. </param>
        protected void AppSettingSetter( double value, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            this[name] = value;
        }

        /// <summary> Gets or sets the application setting nullable  <see cref="double"/> value. </summary>
        /// <param name="dummy"> The type identifier dummy value. </param>
        /// <param name="name">  (Optional) Name of the caller member. </param>
        /// <value> The application setting nullable <see cref="double">?</see> value. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "a type identifier" )]
        protected double? AppSettingGetter( double? dummy, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            return ToNullableDouble( this[name] );
        }

        /// <summary>   Application setting setter. </summary>
        /// <remarks>   David, 2020-09-15. </remarks>
        /// <param name="value">    The value. </param>
        /// <param name="name">     (Optional) Name of the caller member. </param>
        protected void AppSettingSetter( double? value, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            this[name] = value ?? new object();
        }

        /// <summary> Gets or sets the application setting <see cref="Double"/> value. </summary>
        /// <param name="dummy"> The type identifier dummy value. </param>
        /// <param name="name">  (Optional) Name of the caller member. </param>
        /// <value> The application setting <see cref="IEnumerable{T}"/> of <see cref="Double"/> value. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "a type identifier" )]
        protected IEnumerable<double> AppSettingGetter( IEnumerable<double> dummy, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            var l = new List<double>();
            string values = Convert.ToString( base.PropertyValues[name].SerializedValue );
            if ( !string.IsNullOrWhiteSpace( values ) )
            {
                foreach ( string v in values.Split( this.ValuesDelimiter ) )
                {
                    l.Add( Convert.ToDouble( v.Trim() ) );
                }
            }

            return l;
        }

        /// <summary>   Application setting setter. </summary>
        /// <remarks>   David, 2020-09-15. </remarks>
        /// <param name="value">    The value. </param>
        /// <param name="name">     (Optional) Name of the caller member. </param>
        protected void AppSettingSetter( IEnumerable<double> value, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            var builder = new System.Text.StringBuilder();
            foreach ( double v in value )
            {
                _ = builder.Append( $"{v}{this.ValuesDelimiter}" );
            }

            base.PropertyValues[name].SerializedValue = builder.ToString().TrimEnd( this.ValuesDelimiter );
        }

        /// <summary> Gets or sets the application setting double arrays. </summary>
        /// <param name="dummy"> The type identifier dummy value. </param>
        /// <param name="name">  (Optional) Name of the caller member. </param>
        /// <value> The application setting double arrays. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "a type identifier" )]
        protected double[][] AppSettingDoubleArrayGetter( double[][] dummy, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            return Split<double>( Convert.ToString( base.PropertyValues[name].SerializedValue ), this.ValuesDelimiter, this.RowsDelimiter );
        }

        /// <summary>   Application setting setter. </summary>
        /// <remarks>   David, 2020-09-15. </remarks>
        /// <param name="value">    The value. </param>
        /// <param name="name">     (Optional) Name of the caller member. </param>
        protected void AppSettingDoubleArraySetter( double[][] value, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            base.PropertyValues[name].SerializedValue = Join( value, this.ValuesDelimiter, this.RowsDelimiter );
        }

        /// <summary> Gets or sets the application setting <see cref="Int32"/> value. </summary>
        /// <param name="dummy"> The type identifier dummy value. </param>
        /// <param name="name">  (Optional) Name of the caller member. </param>
        /// <value> The application setting <see cref="Int32"/> value. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "a type identifier" )]
        protected int AppSettingGetter( int dummy, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            return Convert.ToInt32( this[name] );
        }

        /// <summary>   Application setting setter. </summary>
        /// <remarks>   David, 2020-09-15. </remarks>
        /// <param name="value">    The value. </param>
        /// <param name="name">     (Optional) Name of the caller member. </param>
        protected void AppSettingSetter( int value, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            this[name] = value;
        }

        /// <summary> Gets or sets the application setting nullable int 32. </summary>
        /// <param name="dummy"> The type identifier dummy value. </param>
        /// <param name="name">  (Optional) Name of the caller member. </param>
        /// <value> The application setting nullable int 32. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "a type identifier" )]
        protected int? AppSettingGetter( int? dummy, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            return ToNullableInt32( this[name] );
        }

        /// <summary>   Application setting setter. </summary>
        /// <remarks>   David, 2020-09-15. </remarks>
        /// <param name="value">    The value. </param>
        /// <param name="name">     (Optional) Name of the caller member. </param>
        protected void AppSettingSetter( int? value, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            this[name] = value ?? new object();
        }

        /// <summary> Gets or sets the application setting int 32 arrays. </summary>
        /// <param name="name">  (Optional) Name of the caller member. </param>
        /// <value> The application setting int 32 arrays. </value>
        protected int[][] AppSettingInt32ArrayGetter( [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            return Split<int>( Convert.ToString( base.PropertyValues[name].SerializedValue ), this.ValuesDelimiter, this.RowsDelimiter );
        }

        /// <summary>   Application setting setter. </summary>
        /// <remarks>   David, 2020-09-15. </remarks>
        /// <param name="value">    The value. </param>
        /// <param name="name">     (Optional) Name of the caller member. </param>
        protected void AppSettingInt32ArraySetter( int[][] value, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            base.PropertyValues[name].SerializedValue = Join( value, this.ValuesDelimiter, this.RowsDelimiter );
        }

        /// <summary> Gets or sets the application setting <see cref="Int64"/> value. </summary>
        /// <param name="dummy"> The type identifier dummy value. </param>
        /// <param name="name">  (Optional) Name of the caller member. </param>
        /// <value> The application setting <see cref="Int64"/> value. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "a type identifier" )]
        protected long AppSettingGetter( long dummy, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            return Convert.ToInt64( this[name] );
        }

        /// <summary>   Application setting setter. </summary>
        /// <remarks>   David, 2020-09-15. </remarks>
        /// <param name="value">    The value. </param>
        /// <param name="name">     (Optional) Name of the caller member. </param>
        protected void AppSettingSetter( long value, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            this[name] = value;
        }

        /// <summary> Gets or sets the application setting <see cref="Double"/> value. </summary>
        /// <param name="dummy"> The type identifier dummy value. </param>
        /// <param name="name">  (Optional) Name of the caller member. </param>
        /// <value> The application setting <see cref="Double"/> value. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "a type identifier" )]
        protected float AppSettingGetter( float dummy, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            return Convert.ToSingle( this[name] );
        }

        /// <summary>   Application setting setter. </summary>
        /// <remarks>   David, 2020-09-15. </remarks>
        /// <param name="value">    The value. </param>
        /// <param name="name">     (Optional) Name of the caller member. </param>
        protected void AppSettingSetter( float value, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            this[name] = value;
        }

        /// <summary> Gets or sets the application setting <see cref="Double"/> value. </summary>
        /// <param name="dummy"> The type identifier dummy value. </param>
        /// <param name="name">  (Optional) Name of the caller member. </param>
        /// <value> The application setting <see cref="Double"/> value. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "a type identifier" )]
        protected TimeSpan AppSettingGetter( TimeSpan dummy, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            return TimeSpan.Parse( this[name].ToString() );
        }

        /// <summary>   Application setting setter. </summary>
        /// <remarks>   David, 2020-09-15. </remarks>
        /// <param name="value">    The value. </param>
        /// <param name="name">     (Optional) Name of the caller member. </param>
        protected void AppSettingSetter( TimeSpan value, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            this[name] = value;
        }

        /// <summary> Gets or sets the application setting value. </summary>
        /// <param name="dummy"> The type identifier dummy value. </param>
        /// <param name="name">  (Optional) Name of the caller member. </param>
        /// <value> The application setting value. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "a type identifier" )]
        protected string AppSettingGetter( string dummy, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            return this[name].ToString();
        }

        /// <summary>   Application setting setter. </summary>
        /// <remarks>   David, 2020-09-15. </remarks>
        /// <param name="value">    The value. </param>
        /// <param name="name">     (Optional) Name of the caller member. </param>
        protected void AppSettingSetter( string value, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            this[name] = value;
        }

        /// <summary> Gets or sets the application setting values. </summary>
        /// <param name="dummy"> The type identifier dummy value. </param>
        /// <param name="name">  (Optional) Name of the caller member. </param>
        /// <value> The application setting values. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "a type identifier" )]
        protected IEnumerable<string> AppSettingGetter( IEnumerable<string> dummy, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            string values = Convert.ToString( base.PropertyValues[name].SerializedValue );
            var l = new List<string>();
            if ( !string.IsNullOrWhiteSpace( values ) )
            {
                foreach ( string v in values.Split( this.ValuesDelimiter ) )
                {
                    l.Add( v.Trim() );
                }
            }

            return l;
        }

        /// <summary>   Application setting setter. </summary>
        /// <remarks>   David, 2020-09-15. </remarks>
        /// <param name="value">    The value. </param>
        /// <param name="name">     (Optional) Name of the caller member. </param>
        protected void AppSettingSetter( IEnumerable<string> value, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            var builder = new System.Text.StringBuilder();
            foreach ( string v in value )
            {
                _ = builder.Append( $"{v}{this.ValuesDelimiter}" );
            }

            base.PropertyValues[name].SerializedValue = builder.ToString().TrimEnd( this.ValuesDelimiter );
        }

        /// <summary> Gets or sets the application setting values. </summary>
        /// <param name="dummy">     The type identifier dummy value. </param>
        /// <param name="delimiter"> The delimiter for splitting the settings value. </param>
        /// <param name="name">      (Optional) Name of the caller member. </param>
        /// <value> The application setting values. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "a type identifier" )]
        protected IEnumerable<string> AppSettingGetter( IEnumerable<string> dummy, char[] delimiter, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            string values = Convert.ToString( base.PropertyValues[name].SerializedValue );
            var l = new List<string>();
            if ( !string.IsNullOrWhiteSpace( values ) )
            {
                foreach ( string v in values.Split( delimiter ) )
                {
                    l.Add( v.Trim() );
                }
            }

            return l;
        }

        /// <summary>   Application setting setter. </summary>
        /// <remarks>   David, 2020-09-15. </remarks>
        /// <param name="value">     The value. </param>
        /// <param name="delimiter"> The delimiter for joining the settings value. </param>
        /// <param name="name">      (Optional) Name of the caller member. </param>
        protected void AppSettingSetter( IEnumerable<string> value, char[] delimiter, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            var builder = new System.Text.StringBuilder();
            foreach ( string v in value )
            {
                _ = builder.Append( $"{v}{delimiter}" );
            }

            base.PropertyValues[name].SerializedValue = builder.ToString().TrimEnd( delimiter );
        }

        /// <summary> Gets or sets the application setting arrays. </summary>
        /// <param name="dummy"> The type identifier dummy value. </param>
        /// <param name="name">  (Optional) Name of the caller member. </param>
        /// <value> The application setting arrays. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "a type identifier" )]
        protected string[][] AppSettingGetter( string[][] dummy, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            return Split<string>( Convert.ToString( base.PropertyValues[name].SerializedValue ), this.ValuesDelimiter, this.RowsDelimiter );
        }

        /// <summary>   Application setting setter. </summary>
        /// <remarks>   David, 2020-09-15. </remarks>
        /// <param name="value">    The value. </param>
        /// <param name="name">     (Optional) Name of the caller member. </param>
        protected void AppSettingSetter( string[][] value, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            base.PropertyValues[name].SerializedValue = Join( value, this.ValuesDelimiter, this.RowsDelimiter );
        }

        /// <summary> Application setting enum. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="name">  (Optional) Name of the caller member. </param>
        /// <returns> An ENUM value. </returns>
        protected T AppSettingEnum<T>( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return ( T ) Enum.Parse( typeof( T ), this[name].ToString() );
        }

        /// <summary>   Application setting enum setter. </summary>
        /// <remarks>   David, 2020-10-10. <para>
        /// Required for VB.Net</para></remarks>
        /// <param name="value">    The value. </param>
        /// <param name="name">     (Optional) Name of the caller member. </param>
        protected void AppSettingEnumSetter( object value, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            this[name] = value;
        }

        /// <summary> Gets or sets the application setting <see cref="Object"/>. </summary>
        /// <param name="dummy"> The type identifier dummy value. </param>
        /// <param name="name">  (Optional) Name of the caller member. </param>
        /// <value> The application setting <see cref="Object"/>. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "a type identifier" )]
        protected object AppSettingGetter( object dummy, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            return this[name];
        }

        /// <summary>   Application setting setter. </summary>
        /// <remarks>   David, 2020-09-15. </remarks>
        /// <param name="value">    The value. </param>
        /// <param name="name">     (Optional) Name of the caller member. </param>
        protected void AppSettingSetter( object value, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            this[name] = value;
        }

        #endregion

        #region " SAVE "

        /// <summary>   Refreshes and saved all settings. </summary>
        /// <remarks>   David, 2020-11-21. </remarks>
        public void SaveAll()
        {
            foreach ( System.Configuration.SettingsPropertyValue pv in this.PropertyValues )
                pv.PropertyValue = pv.PropertyValue;
            this.Save();
        }

        #endregion

    }
}
