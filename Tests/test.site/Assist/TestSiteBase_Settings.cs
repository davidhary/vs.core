
using System;

namespace isr.Core.MSTest
{
    public abstract partial class TestSiteBase
    {

        #region " TEST SITE CONFIGURATION "

        /// <summary> Returns true if test settings exist. </summary>
        /// <remarks> Default value set to false to test reading of the test settings. </remarks>
        /// <value> <c>True</c> if testing settings exit. </value>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "False" )]
        public virtual bool Exists
        {
            get => this.AppSettingGetter( false );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Returns true to output test messages at the verbose level. </summary>
        /// <value> The verbose messaging level. </value>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "False" )]
        public virtual bool Verbose
        {
            get => this.AppSettingGetter( false );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Returns true to enable this device. </summary>
        /// <value> The device enable option. </value>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "True" )]
        public virtual bool Enabled
        {
            get => this.AppSettingGetter( false );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Gets or sets all. </summary>
        /// <value> all. </value>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "True" )]
        public virtual bool All
        {
            get => this.AppSettingGetter( false );

            set => this.AppSettingSetter( value );
        }

        #endregion

        #region " TEST SITE LOCATION IDENTIFICATION "

        /// <summary> Gets or sets the candidate time zones of the test site. </summary>
        /// <value> The candidate time zones of the test site. </value>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "Pacific Standard Time|Central Standard Time" )]
        public virtual string TimeZones
        {
            get => this.AppSettingGetter( string.Empty );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Gets or sets the time zone offset of the test sites. </summary>
        /// <value> The time zone offsets. </value>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "-8|-6" )]
        public virtual string TimeZoneOffsets
        {
            get => this.AppSettingGetter( string.Empty );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Gets or sets the Title of the resource. </summary>
        /// <value> The Title of the resource. </value>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "192.168|10.1" )]
        public virtual string IPv4Prefixes
        {
            get => this.AppSettingGetter( string.Empty );

            set => this.AppSettingSetter( value );
        }

        /// <summary> The time zone. </summary>
        private string _TimeZone;

        /// <summary> Gets the time zone of the test site. </summary>
        /// <value> The time zone of the test site. </value>
        public string TimeZone
        {
            get {
                if ( string.IsNullOrWhiteSpace( this._TimeZone ) )
                {
                    this._TimeZone = this.TimeZones.Split( this.ValuesDelimiter )[this.HostInfoIndex];
                }

                return this._TimeZone;
            }
        }

        /// <summary> The time zone offset. </summary>
        private double _TimeZoneOffset = double.MinValue;

        /// <summary> Gets the time zone offset of the test site. </summary>
        /// <value> The time zone offset of the test site. </value>
        public double TimeZoneOffset
        {
            get {
                if ( this._TimeZoneOffset == double.MinValue )
                {
                    this._TimeZoneOffset = double.Parse( this.TimeZoneOffsets.Split( this.ValuesDelimiter )[this.HostInfoIndex] );
                }

                return this._TimeZoneOffset;
            }
        }

        /// <summary> Gets the host name of the test site. </summary>
        /// <value> The host name of the test site. </value>
        public string HostName { get; private set; }

        /// <summary> The host address. </summary>
        private System.Net.IPAddress _HostAddress;

        /// <summary> Gets the IP address of the test site. </summary>
        /// <value> The IP address of the test site. </value>
        public System.Net.IPAddress HostAddress
        {
            get {
                if ( this._HostAddress is null )
                {
                    this.HostName = System.Net.Dns.GetHostName();
                    foreach ( System.Net.IPAddress value in System.Net.Dns.GetHostEntry( this.HostName ).AddressList )
                    {
                        if ( value.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork )
                        {
                            this._HostAddress = value;
                            break;
                        }
                    }
                }

                return this._HostAddress;
            }
        }

        /// <summary> Zero-based index of the host information. </summary>
        private int _HostInfoIndex = -1;

        /// <summary> Gets the index into the host information strings. </summary>
        /// <value> The index into the host information strings. </value>
        public int HostInfoIndex
        {
            get {
                if ( this._HostInfoIndex < 0 )
                {
                    string ip = this.HostAddress.ToString();
                    int i = -1;
                    foreach ( string value in this.IPv4Prefixes.Split( this.ValuesDelimiter ) )
                    {
                        i += 1;
                        if ( ip.StartsWith( value, StringComparison.OrdinalIgnoreCase ) )
                        {
                            this._HostInfoIndex = i;
                            break;
                        }
                    }
                }

                return this._HostInfoIndex;
            }
        }

        #endregion

    }
}
