using System;
using System.Diagnostics;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.MSTest
{

    /// <summary> Test site base class. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-02-12 </para>
    /// </remarks>
    public abstract partial class TestSiteBase : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="TestSiteBase" /> class. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        protected TestSiteBase() : base()
        {
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose( true );

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary>
        /// Gets the dispose status sentinel of the base class.  This applies to the derived class
        /// provided proper implementation.
        /// </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> <c>True</c> if disposed; otherwise, <c>False</c>. </value>
        protected bool IsDisposed { get; set; }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter:<para>
        /// If True, the method has been called directly or indirectly by a user's code--managed and
        /// unmanaged resources can be disposed.</para><para>
        /// If False, the method has been called by the runtime from inside the finalizer and you should
        /// not reference other objects--only unmanaged resources can be disposed.</para>
        /// </remarks>
        /// <param name="disposing"> <c>True</c> if this method releases both managed and unmanaged
        /// resources;
        /// False if this method releases only unmanaged
        /// resources. </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing )
                    {
                        if ( this._TraceMessagesQueueListener is object )
                        {
                            this._TraceMessagesQueueListener.Dispose();
                        }

                        this._TraceMessagesQueueListener = null;
                    }

                    this._TraceMessagesQueues.Clear();
                }
            }
            finally
            {
                // set the sentinel indicating that the class was disposed.
                this.IsDisposed = true;
            }
        }

        #endregion

        #region " VALIDATIONS "

        /// <summary> Validated test context. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        /// <returns> A TestContext. </returns>
        [CLSCompliant( false )]
        public TestContext ValidatedTestContext( TestContext testContext )
        {
            return testContext is null ? throw new ArgumentNullException( nameof( testContext ) ) : testContext;
        }

        #endregion

        #region " MY APPLICATION "

        /// <summary>   my application. </summary>
        /// <remarks>   David, 2022-04-15. </remarks>
        internal partial class MyApplication : Microsoft.VisualBasic.ApplicationServices.ApplicationBase
        {
        }

        private static readonly Lazy<MyApplication> LazyApp = new();

        /// <summary>
        /// Gets the instance of the <see cref="MyApplication"/> <see cref="Microsoft.VisualBasic.ApplicationServices.ApplicationBase"/>
        /// class instance.
        /// </summary>
        /// <remarks>
        /// Use this property to instantiate a single instance of the <see cref="Microsoft.VisualBasic.ApplicationServices.ApplicationBase"/>
        /// class. This class uses lazy instantiation, meaning the instance isn't created until the first
        /// time it's retrieved.
        /// </remarks>
        /// <value> A new or existing instance of the <see cref="Microsoft.VisualBasic.ApplicationServices.ApplicationBase"/> class. </value>
        internal static MyApplication App => LazyApp.Value;

        /// <summary> Returns true if an instance of the <see cref="Microsoft.VisualBasic.ApplicationServices.ApplicationBase"/> class was created and not disposed. </summary>
        /// <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
        internal static bool AppInstantiated => LazyApp.IsValueCreated;

        #endregion

        #region " TRACE "

        /// <summary> Initializes the trace listener. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        public void InitializeTraceListener()
        {
            this.ReplaceTraceListener();
            Console.Out.WriteLine( App.Log.DefaultFileLogWriter.FullLogFileName );
        }

        /// <summary> Replace trace listener. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        public void ReplaceTraceListener()
        {
            App.Log.TraceSource.Listeners.Remove( CustomFileLogTraceListener.DefaultFileLogWriterName );
            _ = App.Log.TraceSource.Listeners.Add( CustomFileLogTraceListener.CreateListener( false ) );
            App.Log.TraceSource.Switch.Level = SourceLevels.Verbose;
        }

        /// <summary> Trace message. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="format"> Describes the format to use. </param>
        /// <param name="args">   A variable-length parameters list containing arguments. </param>
        public void TraceMessage( string format, params object[] args )
        {
            this.TraceMessage( string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
        }

        /// <summary> Trace message. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="message"> The message. </param>
        public void TraceMessage( string message )
        {
            App.Log.WriteEntry( message );
            // System.Diagnostics.Debug.WriteLine(message)
            Console.Out.WriteLine( message );
        }

        /// <summary> Verbose message. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="format"> Describes the format to use. </param>
        /// <param name="args">   A variable-length parameters list containing arguments. </param>
        public void VerboseMessage( string format, params object[] args )
        {
            if ( this.Verbose )
            {
                this.TraceMessage( format, args );
            }
        }

        /// <summary> Trace elapsed <see cref="DateTimeOffset"/>. </summary>
        /// <remarks> David, 2020-07-10. </remarks>
        /// <param name="startTime"> The start time. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> Current <see cref="DateTimeOffset"/>. </returns>
        public DateTimeOffset TraceElapsedTime( DateTimeOffset startTime, string activity )
        {
            this.TraceMessage( $"{activity} {DateTimeOffset.Now.Subtract( startTime ).TotalMilliseconds}ms" );
            return DateTimeOffset.Now;
        }

        #endregion

        #region " TRACE MESSAGES QUEUE "

        /// <summary> The trace messages queue listener. </summary>
        private TraceMessagesQueueListener _TraceMessagesQueueListener;

        /// <summary> Gets the trace message queue listener. </summary>
        /// <value> The trace message queue listener. </value>
        public TraceMessagesQueueListener TraceMessagesQueueListener
        {
            get {
                if ( this._TraceMessagesQueueListener is null )
                {
                    this._TraceMessagesQueueListener = new TraceMessagesQueueListener();
                    this._TraceMessagesQueueListener.ApplyTraceLevel( TraceEventType.Warning );
                }

                return this._TraceMessagesQueueListener;
            }
        }

        /// <summary> Assert message. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="traceMessage"> Message describing the trace. </param>
        public void AssertMessage( TraceMessage traceMessage )
        {
            if ( traceMessage is null )
            {
            }
            else if ( traceMessage.EventType == TraceEventType.Warning )
            {
                this.TraceMessage( $"Warning published: {traceMessage}" );
            }
            else if ( traceMessage.EventType == TraceEventType.Error )
            {
                Assert.Fail( $"Error published: {traceMessage}" );
            }
        }

        /// <summary> Assert message queue. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="queue"> The queue listener. </param>
        public void AssertMessageQueue( TraceMessagesQueue queue )
        {
            if ( queue is null )
            {
                throw new ArgumentNullException( nameof( queue ) );
            }

            while ( !queue.IsEmpty() )
            {
                this.AssertMessage( queue.TryDequeue() );
            }
        }

        #endregion

        #region " TRACE MESSAGES QUEUE COLLECTION "

        /// <summary> The trace messages queues. </summary>
        private TraceMessageQueueCollection _TraceMessagesQueues;

        /// <summary> Gets the collection of trace messages queues. </summary>
        /// <value> The trace messages queues. </value>
        public TraceMessageQueueCollection TraceMessagesQueues
        {
            get {
                if ( this._TraceMessagesQueues is null )
                {
                    this._TraceMessagesQueues = new TraceMessageQueueCollection();
                }

                return this._TraceMessagesQueues;
            }
        }

        /// <summary> Adds a trace messages queue. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="queue"> The queue listener. </param>
        public void AddTraceMessagesQueue( TraceMessagesQueue queue )
        {
            this.TraceMessagesQueues.Add( queue );
        }

        /// <summary> Assert message queue. THis clears the queues. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        public void AssertMessageQueue()
        {
            this.TraceMessagesQueues.AssertMessageQueue( this );
        }

        /// <summary> Clears the message queue. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <returns> A String. </returns>
        public string ClearMessageQueue()
        {
            return this.TraceMessagesQueues.ClearMessageQueue();
        }

        #endregion

        #region " APPLICATION DOMAIN DATA DIRECTORY "

        /// <summary> The name of the data directory application domain property. </summary>
        public const string ApplicationDomainDataDirectoryPropertyName = "DataDirectory";

        /// <summary> Modify application domain data directory path. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        public void ModifyApplicationDomainDataDirectoryPath()
        {
            this.ModifyApplicationDomainDataDirectoryPath( System.IO.Path.GetDirectoryName( System.Reflection.Assembly.GetExecutingAssembly().Location ) );
        }

        /// <summary> Modify application domain data directory path. </summary>
        /// <remarks>
        /// https://stackoverflow.com/questions/1833640/connection-string-with-relative-path-to-the-database-file.
        /// </remarks>
        /// <param name="path"> Full pathname of the file. </param>
        public void ModifyApplicationDomainDataDirectoryPath( string path )
        {
            AppDomain.CurrentDomain.SetData( ApplicationDomainDataDirectoryPropertyName, path );
        }

        /// <summary> Reads application domain data directory path. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <returns> The application domain data directory path. </returns>
        public string ReadApplicationDomainDataDirectoryPath()
        {
            string value = AppDomain.CurrentDomain.GetData( ApplicationDomainDataDirectoryPropertyName ) as string;
            return value ?? string.Empty;
        }

        #endregion

    }

    /// <summary> Collection of trace message queues. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-04-10 </para>
    /// </remarks>
    public partial class TraceMessageQueueCollection : System.Collections.ObjectModel.Collection<TraceMessagesQueue>
    {

        /// <summary> Query if this object has queued messages. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <returns> <c>true</c> if queued messages; otherwise <c>false</c> </returns>
        public bool HasQueuedMessages()
        {
            bool result = false;
            foreach ( TraceMessagesQueue traceMessageQueue in this )
            {
                result = result && traceMessageQueue.Any;
            }

            return result;
        }

        /// <summary> Count queued messages. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <returns> The total number of queued messages. </returns>
        public int CountQueuedMessages()
        {
            int result = 0;
            foreach ( TraceMessagesQueue traceMessageQueue in this )
            {
                result += traceMessageQueue.Count;
            }

            return result;
        }

        /// <summary> Assert message queue. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="testSite"> The test site. </param>
        public void AssertMessageQueue( TestSiteBase testSite )
        {
            foreach ( TraceMessagesQueue traceMessageQueue in this )
            {
                testSite?.AssertMessageQueue( traceMessageQueue );
            }
        }

        /// <summary> Appends a line. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="builder"> The builder. </param>
        /// <param name="value">   The value. </param>
        private static void AppendLine( System.Text.StringBuilder builder, string value )
        {
            if ( !string.IsNullOrWhiteSpace( value ) )
            {
                _ = builder.AppendLine( value );
            }
        }

        /// <summary> Clears the message queue. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <returns> A String. </returns>
        public string ClearMessageQueue()
        {
            var builder = new System.Text.StringBuilder();
            foreach ( TraceMessagesQueue traceMessageQueue in this )
            {
                AppendLine( builder, traceMessageQueue.DequeueContent() );
            }

            return builder.ToString();
        }
    }
}
