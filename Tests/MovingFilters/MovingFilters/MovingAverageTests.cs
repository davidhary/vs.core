using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.MovingFilters.MSTest
{

    /// <summary> Moving average tests. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-01-27 </para>
    /// </remarks>
    [TestClass()]
    public class MovingAverageTests
    {

        #region " CONSTRUCTION & CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void TestClassInitialize( TestContext testContext )
        {
            try
            {
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        #endregion

        /// <summary>   Evaluate statistics. </summary>
        /// <remarks>   David, 2021-03-20. </remarks>
        /// <param name="values">   The values. </param>
        /// <returns>   A Tuple. </returns>
        private static (double Mean, double Minimum, Double Maximum) EvaluateStats( IEnumerable<double> values )
        {
            double min = double.MaxValue;
            double max = double.MinValue;
            double sum = 0;
            foreach ( double value in values )
            {
                if ( value < min )
                    min = value;
                if ( value > max )
                    max = value;
                sum += value;
            }
            return (sum / values.Count(), min, max);
        }

        /// <summary> The default window length. </summary>
        private const int _DefaultWindowLength = 5;

        /// <summary>   Assert moving average. </summary>
        /// <remarks>   David, 2020-09-23. </remarks>
        /// <param name="count">            Number of. </param>
        /// <param name="movingAverage">    The moving average. </param>
        private static void AssertMovingAverage( int count, MovingAverage movingAverage )
        {
            (double Mean, double Minimum, Double Maximum) = EvaluateStats( movingAverage.GetValues() );
            if ( Minimum != movingAverage.Minimum )
                Assert.AreEqual( Minimum, movingAverage.Minimum, $"Minimum after adding {count} values" );
            if ( Maximum != movingAverage.Maximum )
                Assert.AreEqual( Maximum, movingAverage.Maximum, $"Maximum after adding {count} values" );
            Assert.AreEqual( Mean, movingAverage.Mean,  10e-15, $"Mean after adding {count} values" );
        }

        /// <summary> Adds a value. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="rng">           The random number generator. </param>
        /// <param name="queue">         The queue. </param>
        /// <param name="movingAverage"> The moving average. </param>
        private static void AddValue( Random rng, Queue<double> queue, MovingAverage movingAverage )
        {
            movingAverage.AddValue( rng.NextDouble() );
            queue.Enqueue( movingAverage.LastAveragedReading );
            while ( queue.Count > movingAverage.Length )
            {
                _ = queue.Dequeue();
            }
        }

        /// <summary>   A TimeSpan extension method that starts wait task. </summary>
        /// <remarks>   David, 2021-02-28. </remarks>
        /// <param name="delayTime">    The delayTime to act on. </param>
        /// <returns>   A TimeSpan. </returns>
        private static Task<TimeSpan> StartWaitTask( TimeSpan delayTime )
        {
            static TimeSpan swait( TimeSpan delay )
            {
                Stopwatch sw = Stopwatch.StartNew();
                while ( sw.Elapsed < delay )
                {
                }
                return sw.Elapsed;
            }
            return Task<TimeSpan>.Factory.StartNew( () => { return swait( delayTime ); } );
        }

        /// <summary> (Unit Test Method) moving average test multiple times. </summary>
        /// <remarks>
        /// Added because the moving average failed updating the range correctly when a adding a new
        /// value.
        /// </remarks>
        [TestMethod()]
        public void MovingAverageTestMultipleTimes()
        {
            for ( int i = 1; i <= 100; i++ )
            {
                this.MovingAverageTest();
                StartWaitTask( TimeSpan.FromMilliseconds( 1 ) ).Wait();
            }
        }

        /// <summary> (Unit Test Method) tests moving average. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void MovingAverageTest()
        {
            var movingAverage = new MovingAverage( _DefaultWindowLength );
            var rng = new Random( DateTimeOffset.Now.Second );
            var queue = new Queue<double>();
            int count = 0;
            count += 1;
            AddValue( rng, queue, movingAverage );
            AssertMovingAverage( count, movingAverage );

            count += 1;
            AddValue( rng, queue, movingAverage );
            AssertMovingAverage( count, movingAverage );

            count += 1;
            AddValue( rng, queue, movingAverage );
            AssertMovingAverage( count, movingAverage );

            count += 1;
            AddValue( rng, queue, movingAverage );
            AssertMovingAverage( count, movingAverage );

            count += 1;
            AddValue( rng, queue, movingAverage );
            AssertMovingAverage( count, movingAverage );

            count += 1;
            AddValue( rng, queue, movingAverage );
            AssertMovingAverage( count, movingAverage );
        }
    }
}
