using System;
using System.Collections.Generic;

namespace isr.Core.MovingFilters.MSTest
{
    internal partial class TestSite
    {

        /// <summary> Generates normally distributed doubles. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="seed">  The seed. </param>
        /// <param name="count"> Number of values to generate. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the random normal doubles in this
        /// collection.
        /// </returns>
        public static IList<double> GenerateRandomNormals( int seed, int count )
        {
            return GenerateRandomNormals( new Random( seed ), count );
        }

        /// <summary> Generates normally distributed doubles. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="generator"> The random number generator. </param>
        /// <param name="count">     Number of values to generate. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the random normal doubles in this
        /// collection.
        /// </returns>
        public static IList<double> GenerateRandomNormals( Random generator, int count )
        {
            var l = new List<double>();
            for ( int i = 1, loopTo = count; i <= loopTo; i++ )
            {
                l.Add( NextNormal( generator ) );
            }

            return l.ToArray();
        }

        /// <summary>
        /// Generates the next random normally-distributed mean zero unity standard deviation number.
        /// </summary>
        /// <remarks>   Uses the Box-Mueller method. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="random">   The pseudo random number generator. </param>
        /// <returns>   A Double. </returns>
        public static double NextNormal( Random random )
        {
            if ( random is null )
            {
                throw new ArgumentNullException( nameof( random ) );
            }

            double x, y, r2;
            do
            {
                x = 2.0d * random.NextDouble() - 1.0d;
                y = 2.0d * random.NextDouble() - 1.0d;
                r2 = x * x + y * y;
            }
            while ( r2 >= 1.0d || r2 == 0.0d );
            return x * Math.Sqrt( -2.0d * Math.Log( r2 ) / r2 );
        }

    }
}
