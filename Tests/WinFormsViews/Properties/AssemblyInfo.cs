﻿using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.Core.WinFormsViewsTests.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.Core.WinFormsViewsTests.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.Core.WinFormsViewsTests.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
