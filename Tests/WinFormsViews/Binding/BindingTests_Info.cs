using System;
using System.Diagnostics;

namespace isr.Core.WinFormsViewsTests
{
    /// <summary> Binding Tests. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-02-18. </para>
    /// </remarks>
    [System.Runtime.CompilerServices.CompilerGenerated()]
    [System.CodeDom.Compiler.GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "15.9.0.0")]
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)]
    internal class BindingTestsInfo : ApplicationSettingsBase
    {

        #region " SINGLETON "

        /// <summary>
        /// Initializes an instance of the <see cref="T:System.Configuration.ApplicationSettingsBase" />
        /// class to its default state.
        /// </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        private BindingTestsInfo() : base()
        {
        }

        /// <summary>
        /// Gets the locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        /// <value> The sync locker. </value>
        private static object _SyncLocker { get; set; } = new object();

        /// <summary> Gets the instance. </summary>
        /// <value> The instance. </value>
        private static BindingTestsInfo _Instance { get; set; }

        /// <summary> Instantiates the class. </summary>
        /// <remarks> Use this property to instantiate a single instance of this class. This class uses
        /// lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
        /// <returns> A new or existing instance of the class. </returns>
        public static BindingTestsInfo Get()
        {
            if (_Instance is null)
            {
                lock (_SyncLocker)
                    _Instance = (BindingTestsInfo)Synchronized(new BindingTestsInfo());
            }

            return _Instance;
        }

        /// <summary> Returns true if an instance of the class was created and not disposed. </summary>
        /// <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
        public static bool Instantiated
        {
            get
            {
                lock (_SyncLocker)
                    return _Instance is object;
            }
        }

        #endregion

        #region " SETTINGS EDITORS EXCLUDED "

        /// <summary> Opens the settings editor. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        public static void OpenSettingsEditor()
        {
            Core.WindowsForms.EditConfiguration($"{typeof(BindingTestsInfo)} Editor", Get());
        }

        #endregion

        #region " CONFIGURATION INFORMATION "

        /// <summary> Returns true if test settings exist. </summary>
        /// <value> <c>True</c> if testing settings exit. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>")]
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "True")]
        public bool Exists
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Returns true to output test messages at the verbose level. </summary>
        /// <value> The verbose messaging level. </value>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "False")]
        public bool Verbose
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary>
        /// Gets or sets the sentinel indicating of all data are to be used for a test.
        /// </summary>
        /// <value> <c>true</c> if all data are to be used for a test; otherwise <c>false</c>. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>")]
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "True")]
        public bool All
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> True if the test set is enabled. </summary>
        /// <value> The enabled option. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>")]
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "True")]
        public bool Enabled
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        #endregion

        #region " I7 TESTS SETTINGS "

        /// <summary> Gets or sets the number of items. </summary>
        /// <value> The number of items. </value>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "200000")]
        public int ItemCount
        {
            get
            {
                return AppSettingGetter(0);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the 7k 2600 create timespan. </summary>
        /// <value> The i 7k 2600 create timespan. </value>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "00:00:02.8250000")]
        public TimeSpan I7k2600CreateTimespan
        {
            get
            {
                return AppSettingGetter(TimeSpan.Zero);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the 7k 2600 sort internal timespan. </summary>
        /// <value> The i 7k 2600 sort internal timespan. </value>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "00:00:01.4790000")]
        public TimeSpan I7k2600SortInternalTimespan
        {
            get
            {
                return AppSettingGetter(TimeSpan.Zero);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the 7k 2600 sort extent time span. </summary>
        /// <value> The i 7k 2600 sort extent time span. </value>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "00:00:00.6540000")]
        public TimeSpan I7k2600SortExtTimeSpan
        {
            get
            {
                return AppSettingGetter(TimeSpan.Zero);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the 7k 2600 data table sort timespan. </summary>
        /// <value> The i 7k 2600 data table sort timespan. </value>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "00:00:01.2570000")]
        public TimeSpan I7k2600DataTableSortTimespan
        {
            get
            {
                return AppSettingGetter(TimeSpan.Zero);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the 7k 2600. </summary>
        /// <value> The i 7k 2600. </value>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "I7k2600")]
        public string I7k2600
        {
            get
            {
                return AppSettingGetter(string.Empty);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        #endregion

        #region " AGGREGATE FEED TESTS SETTINGS "

        /// <summary> Gets or sets URL of the news online feed. </summary>
        /// <value> The news online feed URL. </value>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "http://newsrss.bbc.co.uk/rss/newsonline_uk_edition/front_page/rss.xml" )]
        public string NewsOnlineFeedUrl
        {
            get {
                return AppSettingGetter( string.Empty );
            }

            set {
                AppSettingSetter( value );
            }
        }

        /// <summary> Gets or sets URL of the MSDN channel 9 feed. </summary>
        /// <value> The MSDN channel 9 feed URL. </value>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "http://channel9.msdn.com/rss.aspx" )]
        public string MsdnChannel9FeedUrl
        {
            get {
                return AppSettingGetter( string.Empty );
            }

            set {
                AppSettingSetter( value );
            }
        }

        /// <summary> Gets or sets URL of the MSDN blogs feed. </summary>
        /// <value> The MSDN blogs feed URL. </value>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "http://blogs.msdn.microsoft.com/feed" )]
        public string MsdnBlogsFeedUrl
        {
            get {
                return AppSettingGetter( string.Empty );
            }

            set {
                AppSettingSetter( value );
            }
        }

        #endregion

    }
}
