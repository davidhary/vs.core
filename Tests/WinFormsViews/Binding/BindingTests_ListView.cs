using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using isr.Core.WinFormsViews;
using System.Xml;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.WinFormsViewsTests
{
    /// <summary>   (Unit Test Class) a binding tests. </summary>
    /// <remarks>   David, 2020-09-18. </remarks>
    [TestClass()]
    public partial class BindingTests
    {

        #region " BINDING LIST VIEW "

        /// <summary> Compare action. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="leftHand">  The left hand. </param>
        /// <param name="rightHand"> The right hand. </param>
        /// <returns> An Integer. </returns>
        private int CompareAction( Foo leftHand, Foo rightHand )
        {
            return leftHand.Abscissa.CompareTo( rightHand.Abscissa );
        }

        /// <summary> A foo. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        private class Foo
        {

            /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
            /// <remarks> David, 2020-09-18. </remarks>
            public Foo()
            {
                var r = new Random();
                this.Abscissa = r.Next( 0, 100 );
                this.Ordinate = r.Next( 100, 1000 ).ToString();
            }

            /// <summary> Gets or sets the abscissa. </summary>
            /// <value> The abscissa. </value>
            public int Abscissa { get; private set; }

            /// <summary> Gets or sets the ordinate. </summary>
            /// <value> The ordinate. </value>
            public string Ordinate { get; private set; }
        }

        /// <summary> (Unit Test Method) tests binding list view. </summary>
        /// <remarks> tested 2019-05-15. </remarks>
        [TestMethod()]
        public void BindingListViewTest()
        {
            var list = new List<Foo>();
            for ( int i = 1, loopTo = BindingTestsInfo.Get().ItemCount; i <= loopTo; i++ )
            {
                list.Add( new Foo() );
            }

            Assert.AreEqual( BindingTestsInfo.Get().ItemCount, list.Count, "Expected list size" );
            var sw = new Stopwatch();
            var expectedTimeSpan = TimeSpan.Zero;
            var actualTimeSpan = TimeSpan.Zero;
            sw.Start();
            using ( var view = new BindingListView<Foo>( list ) )
            {
                sw.Stop();
                expectedTimeSpan = BindingTestsInfo.Get().I7k2600CreateTimespan;
                actualTimeSpan = sw.Elapsed;
                Assert.IsTrue( expectedTimeSpan > actualTimeSpan, $"BLV Create: expecting {BindingTestsInfo.Get().I7k2600} time span {expectedTimeSpan.TotalMilliseconds} > {actualTimeSpan.TotalMilliseconds}" );
                sw.Reset();
                sw.Start();
                view.Sort = nameof( Foo.Abscissa );
                sw.Stop();
                expectedTimeSpan = BindingTestsInfo.Get().I7k2600SortInternalTimespan;
                actualTimeSpan = sw.Elapsed;
                Assert.IsTrue( expectedTimeSpan > actualTimeSpan, $"BLV.Sort: expecting {BindingTestsInfo.Get().I7k2600} time span {expectedTimeSpan.TotalMilliseconds} > {actualTimeSpan.TotalMilliseconds}" );
                sw.Reset();
                sw.Start();
                view.ApplySort( this.CompareAction );
                sw.Stop();
                expectedTimeSpan = BindingTestsInfo.Get().I7k2600SortExtTimeSpan;
                actualTimeSpan = sw.Elapsed;
                Assert.IsTrue( expectedTimeSpan > actualTimeSpan, $"BLV.ApplySort(delegate): expecting {BindingTestsInfo.Get().I7k2600} {expectedTimeSpan.TotalMilliseconds} > {actualTimeSpan.TotalMilliseconds}" );
            }

            using var dataTable = new System.Data.DataTable {
                Locale = System.Globalization.CultureInfo.CurrentCulture
            };
            _ = dataTable.Columns.Add( nameof( Foo.Abscissa ), typeof( int ) );
            _ = dataTable.Columns.Add( nameof( Foo.Ordinate ), typeof( string ) );
            for ( int i = 1, loopTo1 = BindingTestsInfo.Get().ItemCount; i <= loopTo1; i++ )
            {
                var row = dataTable.NewRow();
                row[0] = list[i - 1].Abscissa;
                row[1] = list[i - 1].Ordinate;
                dataTable.Rows.Add( row );
            }

            sw.Start();
            dataTable.DefaultView.Sort = nameof( Foo.Abscissa );
            sw.Stop();
            expectedTimeSpan = BindingTestsInfo.Get().I7k2600DataTableSortTimespan;
            actualTimeSpan = sw.Elapsed;
            Assert.IsTrue( expectedTimeSpan > actualTimeSpan, $"{nameof( System.Data.DataTable )}.{nameof( System.Data.DataTable.DefaultView )}.Sort: expecting {BindingTestsInfo.Get().I7k2600} {expectedTimeSpan.TotalMilliseconds} > {actualTimeSpan.TotalMilliseconds}" );
        }

        #endregion

        #region " ADDING NEW TEST "

        /// <summary> Handles the adding new. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Adding new event information. </param>
        private void HandleAddingNew( object sender, System.ComponentModel.AddingNewEventArgs e )
        {
            if ( !(sender is BindingListView<string>) )
            {
                return;
            }

            e.NewObject = nameof( e.NewObject );
        }

        /// <summary> (Unit Test Method) tests binding list adding new handling. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void BindingListAddingNewHandlingTest()
        {
            var l = new List<string>();
            using var sender = new BindingListView<string>( l );
            sender.AddingNew += this.HandleAddingNew;
            int expectedNewItemsCount = 0;
            Assert.AreEqual( expectedNewItemsCount, sender.NewItemsList.Count, "expects no new items" );
            l.Add( $"Item #{l.Count + 1}" );
            expectedNewItemsCount += 1;
            Assert.AreEqual( expectedNewItemsCount, sender.NewItemsList.Count, "expects new items after adding one item" );
        }

        #endregion

        #region " AGGREGATE BINDING LIST VIEW "

        /// <summary> A feed. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        private class Feed
        {

            /// <summary> Gets or sets URL of the document. </summary>
            /// <value> The URL. </value>
            private string Url { get; set; }

            /// <summary> Constructor. </summary>
            /// <remarks> David, 2020-09-23. </remarks>
            /// <param name="url"> URL of the resource. </param>
            public Feed( string url )
            {
                this.Url = url;
                this.Items = new BindingList<FeedItem>();
            }

            /// <summary> Updates this object. </summary>
            /// <remarks> David, 2020-09-23. </remarks>
            public void Update()
            {
                var doc = new XmlDocument();
                doc.Load( this.Url );
                this._Title = doc.SelectSingleNode( "/rss/channel/title" ).InnerText;
                foreach ( XmlNode node in doc.SelectNodes( "//item" ) )
                {
                    var item = new FeedItem() {
                        Title = node["title"].InnerText,
                        Description = node["description"].InnerText,
                        PubDate = DateTime.Parse( node["pubDate"].InnerText )
                    };
                    this.Items.Add( item );
                }
            }

            /// <summary> Gets or sets the items. </summary>
            /// <value> The items. </value>
            public BindingList<FeedItem> Items { get; private set; }
            /// <summary> The title. </summary>
            private string _Title;

            /// <summary> Returns a string that represents the current object. </summary>
            /// <remarks> David, 2020-09-23. </remarks>
            /// <returns> A string that represents the current object. </returns>
            public override string ToString()
            {
                return this._Title ?? this.Url;
            }
        }

        /// <summary> A feed item. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        private class FeedItem
        {

            /// <summary> Gets or sets the title. </summary>
            /// <value> The title. </value>
            public string Title { get; set; }

            /// <summary> Gets or sets the description. </summary>
            /// <value> The description. </value>
            public string Description { get; set; }

            /// <summary> Gets or sets the pub date. </summary>
            /// <value> The pub date. </value>
            public DateTime PubDate { get; set; }
        }

        /// <summary> (Unit Test Method) tests aggregate binding list view. </summary>
        /// <remarks> tested 2019-05-15. </remarks>
        [TestMethod()]
        public void AggregateBindingListViewTest()
        {
            using var itemsView = new AggregateBindingListView<FeedItem>();
            using var panel = new System.Windows.Forms.Form();
            using var grid = new System.Windows.Forms.DataGridView();
            // grid.CreateControl()
            panel.Controls.Add( grid );
            grid.DataSource = itemsView;
            int expectedCount = itemsView.Count;
            int actualCount = grid.Rows.Count;
            Assert.AreEqual( expectedCount, actualCount, "empty items view" );

            var feed = new Feed( BindingTestsInfo.Get().NewsOnlineFeedUrl );
            feed.Update();
            _ = itemsView.SourceLists.Add( feed.Items );
            expectedCount = feed.Items.Count;
            actualCount = itemsView.Count;
            Assert.AreEqual( expectedCount, actualCount, "expected items view count" );
            _ = ApplianceBase.DoEventsWaitUntil( TimeSpan.FromMilliseconds( 100 ), () => grid.Rows.Count >= expectedCount );
            actualCount = grid.Rows.Count;
            Assert.AreEqual( expectedCount, actualCount, "expected grid row count" );

            feed = new Feed( BindingTestsInfo.Get().NewsOnlineFeedUrl );
            feed.Update();
            _ = itemsView.SourceLists.Add( feed.Items );
            expectedCount += feed.Items.Count;
            actualCount = itemsView.Count;
            Assert.AreEqual( expectedCount, actualCount, $"expected items view count after adding {feed.Items.Count} items" );
            _ = ApplianceBase.DoEventsWaitUntil( TimeSpan.FromMilliseconds( 100 ), () => grid.Rows.Count >= expectedCount );
            actualCount = grid.Rows.Count;
            Assert.AreEqual( expectedCount, actualCount, "expected grid row count" );

            itemsView.SourceLists.Remove( feed.Items );
            expectedCount -= feed.Items.Count;
            actualCount = itemsView.Count;
            Assert.AreEqual( expectedCount, actualCount, $"expected items view count after removing {feed.Items.Count} items" );
            _ = ApplianceBase.DoEventsWaitUntil( TimeSpan.FromMilliseconds( 100 ), () => grid.Rows.Count < expectedCount );
            actualCount = grid.Rows.Count;
            Assert.AreEqual( expectedCount, actualCount, "expected grid row count" );
        }

        #endregion


    }
}
