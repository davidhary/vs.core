#pragma warning disable IDE1006 // Naming Styles
namespace isr.Core.FrameworkControlsTests.My
#pragma warning restore IDE1006 // Naming Styles
{
    /// <summary>   my library. </summary>
    /// <remarks>   David, 2020-03-18. </remarks>
    internal class MyLibrary
    {
        public const string AssemblyTitle = "ISR Core Windows Forms Test Library";
        public const string AssemblyDescription = "ISR Core Windows Forms Test Library";
        public const string AssemblyProduct = "isr.Core.WinForms.Tests";
        public const string AssemblyConfiguration = "Debug";
    }
}
