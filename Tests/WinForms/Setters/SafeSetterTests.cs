using System;
using System.Windows.Forms;

using isr.Core.WinForms.ControlExtensions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.WinFormsTests
{

    [TestClass()]
    public class SafeSetterTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
            this._TextBox = new TextBox();
            _ = this._TextBox.Handle;
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            // text box cannot be disposed here as it needs to wait execution of the threaded test.
            // this._TextBox?.Dispose();
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        private TextBox _TextBox;


        [TestMethod()]
        public void SafeTextSetterTest()
        {
            string expectedValue = "test 1";
            string actualValue = this._TextBox.SafeTextSetter( expectedValue );
            Assert.AreEqual( expectedValue, actualValue, $"{ nameof( isr.Core.WinForms.ControlExtensions.ControlExtensionMethods.SafeTextSetter ) } value should match" );
            actualValue = this._TextBox.Text;
            Assert.AreEqual( expectedValue, actualValue, $"{ nameof( System.Windows.Forms.TextBox ) }.{ nameof( System.Windows.Forms.TextBox.Text ) } value should match" );
        }

        [TestMethod()]
        public void SafeStextSetterThreadTest()
        {
            var oThread = new System.Threading.Thread( new System.Threading.ThreadStart( this.SafeTextSetterTest ) );
            oThread.Start();
            // this blocks completion of this test.
            // oThread.Join();
        }
    }
}
