using System;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.Cartesian.MSTest
{

    /// <summary> Linear fit tests. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-02-19 </para>
    /// </remarks>
    [TestClass()]
    public class LinearFitTests
    {

        #region " CONSTRUCTION & CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void TestClassInitialize( TestContext testContext )
        {
            try
            {
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        #endregion

        /// <summary> (Unit Test Method) builds determinant test method. </summary>
        /// <remarks>
        /// Uses data from C:\My\LIBRARIES\VS\Core\Core\Tests\Cartesian\Math\PolyFitTests.cs.
        /// </remarks>
        [TestMethod()]
        public void LinearFitTest()
        {
            var linFit = new Cartesian.LinearFit();
            var height = new double[] { 1.47d, 1.5d, 1.52d, 1.55d, 1.57d, 1.6d, 1.63d, 1.65d, 1.68d, 1.7d, 1.73d, 1.75d, 1.78d, 1.8d, 1.83d };
            var mass = new double[] { 52.21d, 53.12d, 54.48d, 55.84d, 57.2d, 58.57d, 59.93d, 61.29d, 63.11d, 64.47d, 66.28d, 68.1d, 69.92d, 72.19d, 74.46d };
            var points = new List<CartesianPoint<double>>();
            for ( int i = 0, loopTo = height.Length - 1; i <= loopTo; i++ )
            {
                points.Add( new CartesianPoint<double>( height[i], mass[i] ) );
            }

            double slope = 61.272d;
            double intercept = -39.062d;
            double correlationCoefficient = 0.9945d;
            double goodness = correlationCoefficient * correlationCoefficient;
            double linearStandardError = 1.775d;
            double constantStandardError = 2.938d;
            _ = linFit.DoFit( height, mass );
            Assert.AreEqual( slope, linFit.LinearCoefficient, 0.001d, "Slope" );
            Assert.AreEqual( intercept, linFit.ConstantCoefficient, 0.001d, "intercept" );
            Assert.AreEqual( goodness, linFit.GoodnessOfFit, 0.001d, "Goodness of fit" );
            Assert.AreEqual( linearStandardError, linFit.LinearCoefficientStandardError, 0.001d, "Linear coefficient Standard error" );
            Assert.AreEqual( constantStandardError, linFit.ConstantCoefficientStandardError, 0.001d, "Constant coefficient Standard error" );
            _ = linFit.DoFit( points );
            Assert.AreEqual( slope, linFit.LinearCoefficient, 0.001d, "Slope" );
            Assert.AreEqual( intercept, linFit.ConstantCoefficient, 0.001d, "intercept" );
            Assert.AreEqual( goodness, linFit.GoodnessOfFit, 0.001d, "Goodness of fit" );
            Assert.AreEqual( linearStandardError, linFit.LinearCoefficientStandardError, 0.001d, "Linear coefficient Standard error" );
            Assert.AreEqual( constantStandardError, linFit.ConstantCoefficientStandardError, 0.001d, "Constant coefficient Standard error" );
        }
    }
}
