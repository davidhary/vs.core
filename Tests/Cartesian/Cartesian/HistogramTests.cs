using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using isr.Core.Cartesian.EnumerableStats;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.Cartesian.MSTest
{

    /// <summary> Summary description for HistogramTests. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestClass]
    public class HistogramTests
    {

        #region " CONSTRUCTION & CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void TestClassInitialize( TestContext testContext )
        {
            try
            {
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        #endregion

        /// <summary> Tests histogram array. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        private static void TestHistogramArray()
        {
            // use a fixed seed to get a predictable random.
            var source = isr.Core.Cartesian.MSTest.TestSite.GenerateRandomNormals( 1, 10000 ).ToArray();
            double lowerLimit = -2;
            double upperLimit = 2d;
            int count = 20;
            double binWidth = (upperLimit - lowerLimit) / count;
            IList<CartesianPoint<double>> result;
            var sw = Stopwatch.StartNew();
            _ = source.Histogram( lowerLimit, upperLimit, count );
            sw.Restart();
            // needs to run twice to make sure the code is compiled.
            result = source.Histogram( lowerLimit, upperLimit, count );
            long directSpeed = sw.ElapsedTicks;
            long linqSpeed = 33235L;
            Assert.IsTrue( directSpeed < linqSpeed, $"Expected speed {directSpeed} to be lower than {linqSpeed}" );


            // count test: There are two extra bins above the high and below the low limits.
            Assert.AreEqual( result.Count, count + 2 );

            // abscissa range test: First bin is at the low limit; last is at the high limit.
            Assert.AreEqual( lowerLimit, result[0].X, 0.1d * binWidth );
            Assert.AreEqual( upperLimit, result[count + 1].X, 0.1d * binWidth );

            // Second bin is at half the bin width past the low limit; last off by half bin width of the last bin.
            Assert.AreEqual( lowerLimit + 0.5d * binWidth, result[1].X, 0.1d * binWidth );
            Assert.AreEqual( upperLimit - 0.5d * binWidth, result[count].X, 0.1d * binWidth );

            // expected value assuming random returns the same values each time.
            int expectedLowCount = 208;
            Assert.AreEqual( expectedLowCount, ( int ) result[0].Y );
            int expectedHighCount = 230;
            Assert.AreEqual( expectedHighCount, ( int ) result[count + 1].Y );
        }

        /// <summary> Tests histogram list. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        private static void TestHistogramList()
        {
            // use a fixed seed to get a predictable random.
            var source = TestSite.GenerateRandomNormals( 1, 10000 );
            double lowerLimit = -2;
            double upperLimit = 2d;
            int count = 20;
            double binWidth = (upperLimit - lowerLimit) / count;
            IList<CartesianPoint<double>> result;
            var sw = Stopwatch.StartNew();
            _ = source.Histogram( lowerLimit, upperLimit, count );
            sw.Restart();
            // needs to run twice to make sure the code is compiled.
            result = source.Histogram( lowerLimit, upperLimit, count );
            long directSpeed = sw.ElapsedTicks;
            long linqSpeed = 33235L;
            Assert.IsTrue( directSpeed < linqSpeed, $"Expected speed {directSpeed} to be lower than {linqSpeed}" );


            // count test: There are two extra bins above the high and below the low limits.
            Assert.AreEqual( result.Count, count + 2 );

            // abscissa range test: First bin is at the low limit; last is at the high limit.
            Assert.AreEqual( lowerLimit, result[0].X, 0.1d * binWidth );
            Assert.AreEqual( upperLimit, result[count + 1].X, 0.1d * binWidth );

            // Second bin is at half the bin width past the low limit; last off by half bin width of the last bin.
            Assert.AreEqual( lowerLimit + 0.5d * binWidth, result[1].X, 0.1d * binWidth );
            Assert.AreEqual( upperLimit - 0.5d * binWidth, result[count].X, 0.1d * binWidth );

            // expected value assuming random returns the same values each time.
            int expectedLowCount = 208;
            Assert.AreEqual( expectedLowCount, ( int ) result[0].Y );
            int expectedHighCount = 230;
            Assert.AreEqual( expectedHighCount, ( int ) result[count + 1].Y );
        }

        /// <summary> (Unit Test Method) tests histogram. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod]
        public void HistogramTest()
        {
            TestHistogramList();
            TestHistogramArray();
            return;
        }
    }
}
