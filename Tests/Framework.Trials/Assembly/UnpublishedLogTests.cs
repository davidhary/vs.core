using System;
using System.Diagnostics;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.Framework.Trials
{

    /// <summary> This is a test class for unpublished messages. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestClass()]
    public class UnpublishedLogTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( UnpublishedLogTests.TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );

            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        /// <summary> Tests logging unpublished messages. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="expected">      The expected. </param>
        /// <param name="checkFileSize"> True to check file size. </param>
        public static void TestLoggingUnpublishedMessages( TraceEventType expected, bool checkFileSize )
        {
            string payload = "Message 1";
            var expectedMessage = new TraceMessage( expected, Core.My.MyLibrary.TraceEventId, payload );
            long initialLogFileSize = Core.My.MyLibrary.Logger.FileSize;
            string actualLogFileName = Core.My.MyLibrary.Logger.FullLogFileName;
            Assert.IsFalse( string.IsNullOrWhiteSpace( actualLogFileName ), $"Tests if log file name '{actualLogFileName}' is empty" );

            // clear trace message log
            _ = Core.My.MyLibrary.Appliance.UnpublishedTraceMessages.DequeueContent();

            // check content
            string actualContents = Core.My.MyLibrary.Appliance.UnpublishedTraceMessages.DequeueContent();
            Assert.IsTrue( string.IsNullOrWhiteSpace( actualContents ), "Unpublished messages should clear" );
            int expectedUnpublishedMessageCount = 0;
            int actualUnpublicMessageCount = 0;
            Assert.AreEqual( expectedUnpublishedMessageCount, actualUnpublicMessageCount, $"Initial unpublished message count should match" );
            Core.My.MyLibrary.Logger.DefaultFileLogWriter.AutoFlush = false;

            // log an unpublished trace message to the library.
            _ = Core.My.MyLibrary.Appliance.LogUnpublishedMessage( expectedMessage );
            expectedUnpublishedMessageCount += 1;
            actualUnpublicMessageCount = Core.My.MyLibrary.Appliance.UnpublishedTraceMessages.Count;
            Assert.AreEqual( expectedUnpublishedMessageCount, actualUnpublicMessageCount, $"Unpublished message count should match" );
            var actualMessage = Core.My.MyLibrary.Appliance.UnpublishedTraceMessages.TryPeek();
            Assert.AreEqual( expectedMessage.Id, actualMessage.Id, " Message trace events identities match" );
            _ = Core.My.MyLibrary.Appliance.LogUnpublishedMessage( expectedMessage );
            if ( checkFileSize )
            {
                // this no longer works. The log does not show the new messages until after the program closes.
                Core.My.MyLibrary.Logger.FlushMessages();
                ApplianceBase.DoEvents();
                ApplianceBase.Delay( 50d );
                ApplianceBase.DoEvents();
                Core.My.MyLibrary.Logger.DefaultFileLogWriter.Flush();
                ApplianceBase.Delay( 50d );
                ApplianceBase.DoEvents();
                long newLogFileSize = Core.My.MyLibrary.Logger.FileSize;
                var endTime = DateTimeOffset.Now.Add( TimeSpan.FromMilliseconds( 200d ) );
                while ( newLogFileSize <= initialLogFileSize && DateTimeOffset.Now <= endTime )
                {
                    ApplianceBase.Delay( 50d );
                    ApplianceBase.DoEvents();
                    Core.My.MyLibrary.Logger.DefaultFileLogWriter.Flush();
                    ApplianceBase.Delay( 50d );
                    ApplianceBase.DoEvents();
                    newLogFileSize = Core.My.MyLibrary.Logger.FileSize;
                }

                Assert.IsTrue( newLogFileSize > initialLogFileSize, $"New file Size {newLogFileSize} > initial file size {initialLogFileSize}" );
            }
        }

        /// <summary> (Unit Test Method) tests log unpublished messages. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void LogUnpublishedMessagesTest()
        {
            TestLoggingUnpublishedMessages( TraceEventType.Warning, Properties.Settings.Default.CheckUnpublishedMessageLogFileSize );
        }
    }
}
