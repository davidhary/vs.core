using System;
using System.Diagnostics;

using isr.Core.VisualBasicLoggingExtensions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.Framework.Trials
{

    /// <summary>
    /// This is a test class for VisualBasicLoggingExtensionsTest and is intended to contain all
    /// VisualBasicLoggingExtensionsTest Unit Tests.
    /// </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestClass()]
    public class LoggingExtensionsTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( LoggingExtensionsTests.TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );

            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        /// <summary> A test for ReplaceDefaultTraceListener with trace event. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        public static void AssertTraceEventLog( TraceEventType expected )
        {
            var actual = expected;
            Assert.AreEqual( expected, actual );
            _ = My.MyProject.Application.Log.ReplaceDefaultTraceListener( true );
            if ( !My.MyProject.Application.Log.DefaultFileLogWriterFileExists() )
            {
                My.MyProject.Application.Log.TraceSource.TraceEvent( TraceEventType.Critical, 1, "trace event into {0}", My.MyProject.Application.Log.DefaultFileLogWriterFilePath() );
            }

            My.MyProject.Application.Log.TraceSource.TraceEvent( TraceEventType.Verbose, 1, "trace event level {0}", My.MyProject.Application.Log.TraceLevel() );
            My.MyProject.Application.Log.TraceSource.TraceEvent( TraceEventType.Information, 1, "trace event level {0}", My.MyProject.Application.Log.TraceLevel() );
            My.MyProject.Application.Log.TraceSource.TraceEvent( TraceEventType.Warning, 1, "trace event level {0}", My.MyProject.Application.Log.TraceLevel() );
            My.MyProject.Application.Log.TraceSource.TraceEvent( TraceEventType.Error, 1, "trace event level {0}", My.MyProject.Application.Log.TraceLevel() );
            My.MyProject.Application.Log.TraceSource.TraceEvent( TraceEventType.Critical, 1, "setting trace level to {0}", expected );
            My.MyProject.Application.Log.ApplyTraceLevel( TraceEventType.Information );
            _ = My.MyProject.Application.Log.TraceLevel();
            My.MyProject.Application.Log.TraceSource.TraceEvent( TraceEventType.Verbose, 1, "trace event level {0}", My.MyProject.Application.Log.TraceLevel() );
            My.MyProject.Application.Log.TraceSource.TraceEvent( TraceEventType.Information, 1, "trace event level {0}", My.MyProject.Application.Log.TraceLevel() );
            My.MyProject.Application.Log.TraceSource.TraceEvent( TraceEventType.Warning, 1, "trace event level {0}", My.MyProject.Application.Log.TraceLevel() );
            My.MyProject.Application.Log.TraceSource.TraceEvent( TraceEventType.Error, 1, "trace event level {0}", My.MyProject.Application.Log.TraceLevel() );
        }

        /// <summary> A test for ReplaceDefaultTraceListener with trace event. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void TraceEventLogTestInformation()
        {
            AssertTraceEventLog( TraceEventType.Information );
        }

        /// <summary> A test for ReplaceDefaultTraceListener with logging log. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="expected"> The expected. </param>
        public static void AssertLoggingLog( TraceEventType expected )
        {
            var actual = expected;
            Assert.AreEqual( expected, actual );
            Microsoft.VisualBasic.Logging.FileLogTraceListener value = My.MyProject.Application.Log.ReplaceDefaultTraceListener( true );
            if ( !My.MyProject.Application.Log.DefaultFileLogWriterFileExists() )
            {
                My.MyProject.Application.Log.TraceSource.TraceEvent( TraceEventType.Critical, 1, "trace event into {0}", My.MyProject.Application.Log.DefaultFileLogWriterFilePath() );
            }

            _ = My.MyProject.Application.Log.WriteLogEntry( TraceEventType.Critical, "Tracing into {0}", value.FullLogFileName );
            _ = My.MyProject.Application.Log.WriteLogEntry( TraceEventType.Verbose, "trace level {0}", My.MyProject.Application.Log.TraceLevel() );
            _ = My.MyProject.Application.Log.WriteLogEntry( TraceEventType.Information, "trace level {0}", My.MyProject.Application.Log.TraceLevel() );
            _ = My.MyProject.Application.Log.WriteLogEntry( TraceEventType.Warning, "trace level {0}", My.MyProject.Application.Log.TraceLevel() );
            _ = My.MyProject.Application.Log.WriteLogEntry( TraceEventType.Error, "trace level {0}", My.MyProject.Application.Log.TraceLevel() );
            _ = My.MyProject.Application.Log.WriteLogEntry( TraceEventType.Critical, "setting trace level to {0}", expected );
            _ = My.MyProject.Application.Log.TraceLevel();
            _ = My.MyProject.Application.Log.WriteLogEntry( TraceEventType.Verbose, "trace level {0}", My.MyProject.Application.Log.TraceLevel() );
            _ = My.MyProject.Application.Log.WriteLogEntry( TraceEventType.Information, "trace level {0}", My.MyProject.Application.Log.TraceLevel() );
            _ = My.MyProject.Application.Log.WriteLogEntry( TraceEventType.Warning, "trace level {0}", My.MyProject.Application.Log.TraceLevel() );
            _ = My.MyProject.Application.Log.WriteLogEntry( TraceEventType.Error, "trace level {0}", My.MyProject.Application.Log.TraceLevel() );
        }

        /// <summary> A test for ReplaceDefaultTraceListener with logging log. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void LoggingLogTestInformation()
        {
            AssertLoggingLog( TraceEventType.Information );
        }
    }
}
