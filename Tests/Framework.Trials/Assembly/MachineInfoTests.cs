
using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.Framework.Trials
{

    /// <summary> A machine information tests. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestClass()]
    public class MachineInfoTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( MachineInfoTests.TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );

            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        /// <summary> A test for Machine ID Using insecure MD5 algorithm,. </summary>
        /// <remarks> Causes application domain exception with test agent. </remarks>
        [TestMethod()]
        public void MachineUniqueIdMD5Test()
        {
            string expected = "7ae637a47a35f672ed013462c7b92649";
            switch ( true )
            {
                case object _ when string.Equals( My.MyProject.Computer.Name, "LimeDevB", StringComparison.OrdinalIgnoreCase ):
                    {
                        expected = "6f32a7c3d4b883fb67303c53380b7859";
                        break;
                    }

                case object _ when string.Equals( My.MyProject.Computer.Name, "LimeDev", StringComparison.OrdinalIgnoreCase ):
                    {
                        expected = "f77145622be6795484b53c570731a5fc";
                        break;
                    }

                case object _ when string.Equals( My.MyProject.Computer.Name, "Fig10Dev", StringComparison.OrdinalIgnoreCase ):
                    {
                        expected = "7ae637a47a35f672ed013462c7b92649";
                        break;
                    }
            }
            // on FIG10Dev
            string actual = MachineInfo.BuildMachineUniqueIdMD5();
            Assert.AreEqual( expected, actual );
        }

        /// <summary> (Unit Test Method) machine unique identifier SHA 1. </summary>
        /// <remarks> Causes application domain exception with test agent. </remarks>
        [TestMethod()]
        public void MachineUniqueIdSha1()
        {
            string expected = "7ae637a47a35f672ed013462c7b92649";
            switch ( true )
            {
                case object _ when string.Equals( My.MyProject.Computer.Name, "LimeDevB", StringComparison.OrdinalIgnoreCase ):
                    {
                        expected = "be9be0ce1a031b079b0330be7e47f0dbcba2a1cd";
                        break;
                    }

                case object _ when string.Equals( My.MyProject.Computer.Name, "LimeDev", StringComparison.OrdinalIgnoreCase ):
                    {
                        expected = "e8744184d701a53a060f1502d40e2a8e09a61a31";
                        break;
                    }

                case object _ when string.Equals( My.MyProject.Computer.Name, "Fig10Dev", StringComparison.OrdinalIgnoreCase ):
                    {
                        expected = "85f9c966b0a3f128c41186a0160aa0c9cce68e65";
                        break;
                    }
            }

            string actual = MachineInfo.BuildMachineUniqueIdSha1();
            Assert.AreEqual( expected, actual );
        }
    }
}
