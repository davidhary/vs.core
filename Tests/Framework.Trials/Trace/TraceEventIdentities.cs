using System;
using System.Collections.Generic;

using Microsoft.VisualBasic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.Framework.Trials
{

    /// <summary> A trace event identities. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-03-17 </para>
    /// </remarks>
    [TestClass()]
    public class TraceEventIdentities
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TraceEventIdentities.TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );

            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion


        #region " SAVE TRACE EVENT IDENTITIES "

        /// <summary> Save the trace event identities. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="fileName"> Filename of the file. </param>
        /// <param name="openMode"> The open mode. </param>
        /// <param name="values">   The values. </param>
        private static void SaveTraceEventIdentities( string fileName, OpenMode openMode, IEnumerable<KeyValuePair<string, int>> values )
        {
            if ( values is null )
            {
                throw new ArgumentNullException( nameof( values ) );
            }

            int fileNo = FileSystem.FreeFile();
            if ( openMode == OpenMode.Output && System.IO.File.Exists( fileName ) )
            {
                System.IO.File.Delete( fileName );
            }

            FileSystem.FileOpen( fileNo, fileName, openMode );
            FileSystem.WriteLine( fileNo, "hex", "decimal", "description" );
            foreach ( KeyValuePair<string, int> value in values )
            {
                FileSystem.WriteLine( fileNo, value.Value.ToString( "X" ), value.Value.ToString(), value.Key );
            }

            FileSystem.FileClose( fileNo );
        }

        /// <summary> (Unit Test Method) enumerates test event identities. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void SaveTestEventIdentities()
        {
            string fileName = TestInfo.TraceEventProjectIdentitiesFileName;
            var values = new List<KeyValuePair<string, int>>();
            foreach ( ProjectTraceEventId value in Enum.GetValues( typeof( ProjectTraceEventId ) ) )
            {
                values.Add( new KeyValuePair<string, int>( EnumExtensions.EnumExtensionsMethods.Description( value ), ( int ) value ) );
            }

            SaveTraceEventIdentities( fileName, OpenMode.Output, values );
        }

        #endregion

    }
}
