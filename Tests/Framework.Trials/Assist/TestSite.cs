namespace isr.Core.Framework.Trials
{

    /// <summary> A test site class. </summary>
    /// <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-02-12 </para></remarks>
    [System.Runtime.CompilerServices.CompilerGenerated()]
    [System.CodeDom.Compiler.GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "16.7.0.0")]
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)]
    internal class TestSite : isr.Core.MSTest.TestSiteBase
    {

        /// <summary> Gets or sets the filename of the trace event project identities file. </summary>
        /// <value> The filename of the trace event project identities file. </value>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "True")]
        public virtual string TraceEventProjectIdentitiesFileName
        {
            get
            {
                return AppSettingGetter(string.Empty);
            }

            set
            {
                AppSettingSetter(value);
            }
        }
    }
}
