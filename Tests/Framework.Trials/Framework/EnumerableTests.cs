using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Diagnostics;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.Framework.Trials
{

    /// <summary> Tests for using Enumerable tests. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 09/021/2020 </para>
    /// </remarks>
    [TestClass()]
    public class EnumerableTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( EnumerableTests.TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );

            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " ENUMERABLE TESTS "

        /// <summary> Iterate array. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="objectName"> Name of the object. </param>
        /// <param name="a">          An IEnumerable(OfDouble) to process. </param>
        /// <returns> A TimeSpan. </returns>
        public static TimeSpan IterateArray( string objectName, double[] a )
        {
            var sw = Stopwatch.StartNew();
            var v = default( double );
            for ( int i = 0, loopTo = a.Length - 1; i <= loopTo; i++ )
            {
                v = a[i];
            }

            var elapsed = sw.Elapsed;
            TestInfo.TraceMessage( $"Iterate {objectName}(Of Double)[{a.Length}] cast as Array in {elapsed.TotalMilliseconds}ms" );
            a[0] = v;
            return elapsed;
        }

        /// <summary> For each array. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="objectName"> Name of the object. </param>
        /// <param name="a">          An IEnumerable(OfDouble) to process. </param>
        /// <returns> A TimeSpan. </returns>
        public static TimeSpan ForEachArray( string objectName, double[] a )
        {
            var sw = Stopwatch.StartNew();
            var v = default( double );
            foreach ( double item in a )
            {
                v = item;
            }

            var elapsed = sw.Elapsed;
            TestInfo.TraceMessage( $"For Each {objectName}(Of Double)[{a.Length}] cast as Array in {elapsed.TotalMilliseconds}ms" );
            a[0] = v;
            return elapsed;
        }

        /// <summary> Populates from array. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="objectName"> Name of the object. </param>
        /// <param name="a">          An IEnumerable(OfDouble) to process. </param>
        /// <returns> A TimeSpan. </returns>
        public static TimeSpan PopulateFromArray( string objectName, double[] a )
        {
            var col = new Collection<double>();
            var sw = Stopwatch.StartNew();
            foreach ( double item in a )
            {
                col.Add( item );
            }

            var elapsed = sw.Elapsed;
            TestInfo.TraceMessage( $"Populate Collection {objectName}(Of Double)[{a.Length}] cast as Array in {elapsed.TotalMilliseconds}ms" );
            return elapsed;
        }

        /// <summary> Iterate i list. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="objectName"> Name of the object. </param>
        /// <param name="a">          An IEnumerable(OfDouble) to process. </param>
        /// <returns> A TimeSpan. </returns>
        public static TimeSpan IterateIList( string objectName, IList<double> a )
        {
            var sw = Stopwatch.StartNew();
            for ( int i = 0, loopTo = a.Count - 1; i <= loopTo; i++ )
            {
                _ = a[i];
            }

            var elapsed = sw.Elapsed;
            TestInfo.TraceMessage( $"Iterate {objectName}(Of Double)[{a.Count}] cast as IList in {elapsed.TotalMilliseconds}ms" );
            a[0] = 0d;
            // collection is read only: a(0) = 0
            return elapsed;
        }

        /// <summary> For each i list. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="objectName"> Name of the object. </param>
        /// <param name="a">          An IEnumerable(OfDouble) to process. </param>
        /// <returns> A TimeSpan. </returns>
        public static TimeSpan ForEachIList( string objectName, IList<double> a )
        {
            var sw = Stopwatch.StartNew();
            double v;
            foreach ( double item in a )
            {
                v = item;
            }

            var elapsed = sw.Elapsed;
            TestInfo.TraceMessage( $"For Each {objectName}(Of Double)[{a.Count}] cast as IList in {elapsed.TotalMilliseconds}ms" );
            // collection is read only: a(0) = 0
            return elapsed;
        }

        /// <summary> Populates from i list. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="objectName"> Name of the object. </param>
        /// <param name="a">          An IEnumerable(OfDouble) to process. </param>
        /// <returns> A TimeSpan. </returns>
        public static TimeSpan PopulateFromIList( string objectName, IList<double> a )
        {
            var col = new Collection<double>();
            var sw = Stopwatch.StartNew();
            foreach ( double item in a )
            {
                col.Add( item );
            }

            var elapsed = sw.Elapsed;
            TestInfo.TraceMessage( $"Populate Collection {objectName}(Of Double)[{a.Count}] cast as IList in {elapsed.TotalMilliseconds}ms" );
            return elapsed;
        }

        /// <summary> Iterate i enumerable. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="objectName"> Name of the object. </param>
        /// <param name="a">          An IEnumerable(OfDouble) to process. </param>
        /// <returns> A TimeSpan. </returns>
        public static TimeSpan IterateIEnumerable( string objectName, IEnumerable<double> a )
        {
            var sw = Stopwatch.StartNew();
            for ( int i = 0, loopTo = a.Count() - 1; i <= loopTo; i++ )
            {
                _ = a.ElementAtOrDefault( i );
            }

            var elapsed = sw.Elapsed;
            TestInfo.TraceMessage( $"Iterate {objectName}(Of Double)[{a.Count()}] cast as IEnumerable in {elapsed.TotalMilliseconds}ms" );
            return elapsed;
        }

        /// <summary> For each i enumerable. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="objectName"> Name of the object. </param>
        /// <param name="a">          An IEnumerable(OfDouble) to process. </param>
        /// <returns> A TimeSpan. </returns>
        public static TimeSpan ForEachIEnumerable( string objectName, IEnumerable<double> a )
        {
            var sw = Stopwatch.StartNew();
            double v;
            foreach ( double item in a )
            {
                v = item;
            }

            var elapsed = sw.Elapsed;
            TestInfo.TraceMessage( $"For Each {objectName}(Of Double)[{a.Count()}] cast as IEnumerable in {elapsed.TotalMilliseconds}ms" );
            return elapsed;
        }

        /// <summary> Populates from i enumerable. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="objectName"> Name of the object. </param>
        /// <param name="a">          An IEnumerable(OfDouble) to process. </param>
        /// <returns> A TimeSpan. </returns>
        public static TimeSpan PopulateFromIEnumerable( string objectName, IEnumerable<double> a )
        {
            var col = new Collection<double>();
            var sw = Stopwatch.StartNew();
            foreach ( double item in a )
            {
                col.Add( item );
            }

            var elapsed = sw.Elapsed;
            TestInfo.TraceMessage( $"Populate Collection {objectName}(Of Double)[{a.Count()}] cast as IEnumerable in {elapsed.TotalMilliseconds}ms" );
            return elapsed;
        }

        /// <summary> Iterate i collection. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="objectName"> Name of the object. </param>
        /// <param name="a">          An IEnumerable(OfDouble) to process. </param>
        /// <returns> A TimeSpan. </returns>
        public static TimeSpan IterateICollection( string objectName, ICollection<double> a )
        {
            var sw = Stopwatch.StartNew();
            for ( int i = 0, loopTo = a.Count - 1; i <= loopTo; i++ )
            {
                _ = a.ElementAtOrDefault( i );
            }

            var elapsed = sw.Elapsed;
            TestInfo.TraceMessage( $"Iterate {objectName}(Of Double)[{a.Count}] cast as ICollection in {elapsed.TotalMilliseconds}ms" );
            return elapsed;
        }

        /// <summary> For each i collection. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="objectName"> Name of the object. </param>
        /// <param name="a">          An IEnumerable(OfDouble) to process. </param>
        /// <returns> A TimeSpan. </returns>
        public static TimeSpan ForEachICollection( string objectName, ICollection<double> a )
        {
            var sw = Stopwatch.StartNew();
            double v;
            foreach ( double item in a )
            {
                v = item;
            }

            var elapsed = sw.Elapsed;
            TestInfo.TraceMessage( $"ForEach {objectName}(Of Double)[{a.Count}] cast as ICollection in {elapsed.TotalMilliseconds}ms" );
            return elapsed;
        }

        /// <summary> Populates from i collection. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="objectName"> Name of the object. </param>
        /// <param name="a">          An IEnumerable(OfDouble) to process. </param>
        /// <returns> A TimeSpan. </returns>
        public static TimeSpan PopulateFromICollection( string objectName, ICollection<double> a )
        {
            var col = new Collection<double>();
            var sw = Stopwatch.StartNew();
            foreach ( double item in a )
            {
                col.Add( item );
            }

            var elapsed = sw.Elapsed;
            // expression is a value and cannot be a target of assignment: a(0) = 0
            TestInfo.TraceMessage( $"Populate Collection {objectName}(Of Double)[{a.Count}] cast as ICollection in {elapsed.TotalMilliseconds}ms" );
            return elapsed;
        }

        /// <summary> Enumerates array as i enumerable in this collection. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="a"> An IEnumerable(OfDouble) to process. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process array as i enumerable in this
        /// collection.
        /// </returns>
        public static IEnumerable<double> ArrayAsIEnumerable( IEnumerable<double> a )
        {
            var col = new Collection<double>();
            _ = Stopwatch.StartNew();
            foreach ( double item in a )
            {
                col.Add( item );
            }

            return col.ToArray();
        }

        /// <summary> Array as i list. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="a"> An IEnumerable(OfDouble) to process. </param>
        /// <returns> A list of. </returns>
        public static IList<double> ArrayAsIList( IEnumerable<double> a )
        {
            var col = new Collection<double>();
            _ = Stopwatch.StartNew();
            foreach ( double item in a )
            {
                col.Add( item );
            }

            return col.ToArray();
        }

        /// <summary> List as i list. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="a"> An IEnumerable(OfDouble) to process. </param>
        /// <returns> A list of. </returns>
        public static IList<double> ListAsIList( IEnumerable<double> a )
        {
            var l = new List<double>();
            _ = Stopwatch.StartNew();
            foreach ( double item in a )
            {
                l.Add( item );
            }

            return l;
        }

        /// <summary> Lists as i enumerables in this collection. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="a"> An IEnumerable(OfDouble) to process. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process as i enumerables in this collection.
        /// </returns>
        public static IEnumerable<double> ListAsIEnumerable( IEnumerable<double> a )
        {
            var l = new List<double>();
            _ = Stopwatch.StartNew();
            foreach ( double item in a )
            {
                l.Add( item );
            }

            return l;
        }

        /// <summary> Executes the tests. </summary>
        /// <remarks> David, 2020-09-07. </remarks>
        /// <param name="testLevel"> The test level. </param>
        private static void RunTests( TestLevel testLevel )
        {
            var l = new List<double>();
            var x = new Collection<double>();
            TimeSpan elapsed;
            int count = 10000000;
            var sw = Stopwatch.StartNew();
            for ( int i = 1, loopTo = count; i <= loopTo; i++ )
            {
                l.Add( i );
                x.Add( i );
            }

            elapsed = sw.Elapsed;
            TestInfo.TraceMessage( $"populate {count} in {elapsed.TotalMilliseconds}ms" );
            double[] a;
            sw.Restart();
            a = x.ToArray();
            elapsed = sw.Elapsed;
            TestInfo.TraceMessage( $".ToArray Collection(of Double)[{count}] in {elapsed.TotalMilliseconds}ms" );
            a[0] = 0d;
            sw.Restart();
            a = l.ToArray();
            elapsed = sw.Elapsed;
            TestInfo.TraceMessage( $".ToArray List(of Double)[{count}] in {elapsed.TotalMilliseconds}ms" );
            sw.Restart();
            IReadOnlyList<double> ro = new List<double>( a );
            elapsed = sw.Elapsed;
            TestInfo.TraceMessage( $".Read-Only List(of Double)[{count}] in {elapsed.TotalMilliseconds}ms" );
            if ( testLevel == TestLevel.None )
            {
                return;
            }

            if ( (testLevel & TestLevel.Iterate) != 0 )
            {
                _ = IterateArray( "Array", a );
                _ = IterateIList( "Array", a );
                _ = IterateIEnumerable( "Array", a );
                _ = IterateICollection( "Array", a );
                _ = IterateArray( "List", l.ToArray() );
                _ = IterateIList( "List", l );
                _ = IterateIEnumerable( "List", l );
                _ = IterateICollection( "List", l );
                _ = IterateArray( "Collection", x.ToArray() );
                _ = IterateIList( "Collection", x );
                _ = IterateIEnumerable( "Collection", x );
                _ = IterateICollection( "Collection", x );
                _ = IterateArray( "Read-Only List", ro.ToArray() );
                _ = IterateIList( "Read-Only List", ro.ToList() );
                _ = IterateIEnumerable( "Read-Only List", ro );
                _ = IterateICollection( "Read-Only List", ro.ToList() );
            }

            if ( (testLevel & TestLevel.ForEach) != 0 )
            {
                _ = ForEachArray( "Array", a );
                _ = ForEachIList( "Array", a );
                _ = ForEachIEnumerable( "Array", a );
                _ = ForEachICollection( "Array", a );
                _ = ForEachArray( "List", l.ToArray() );
                _ = ForEachIList( "List", l );
                _ = ForEachIEnumerable( "List", l );
                _ = ForEachICollection( "List", l );
                _ = ForEachArray( "Collection", x.ToArray() );
                _ = ForEachIList( "Collection", x );
                _ = ForEachIEnumerable( "Collection", x );
                _ = ForEachICollection( "Collection", x );
                _ = ForEachArray( "Read-Only List", ro.ToArray() );
                _ = ForEachIList( "Read-Only List", ro.ToList() );
                _ = ForEachIEnumerable( "Read-Only List", ro );
                _ = ForEachICollection( "Read-Only List", ro.ToList() );
            }

            if ( (testLevel & TestLevel.Populate) != 0 )
            {
                _ = PopulateFromArray( "Array", a );
                _ = PopulateFromIList( "Array", a );
                _ = PopulateFromIEnumerable( "Array", a );
                _ = PopulateFromICollection( "Array", a );
                _ = PopulateFromArray( "List", l.ToArray() );
                _ = PopulateFromIList( "List", l );
                _ = PopulateFromIEnumerable( "List", l );
                _ = PopulateFromICollection( "List", l );
                _ = PopulateFromArray( "Collection", x.ToArray() );
                _ = PopulateFromIList( "Collection", x );
                _ = PopulateFromIEnumerable( "Collection", x );
                _ = PopulateFromICollection( "Collection", x );
            }

            if ( (testLevel & TestLevel.ReturnValue) != 0 )
            {
                var lal = ListAsIList( a );
                var aae = ArrayAsIEnumerable( a );
                var aal = ArrayAsIList( a );
                var lae = ListAsIEnumerable( a );
                if ( (testLevel & TestLevel.Iterate) != 0 )
                {
                    _ = IterateArray( "Array as IEnumerable", aae.ToArray() );
                    _ = IterateIList( "Array as IEnumerable", aae.ToList() );
                    _ = IterateIEnumerable( "Array as IEnumerable", aae );
                    _ = IterateICollection( "Array as IEnumerable", aae.ToList() );
                    _ = IterateArray( "Array as IList", aal.ToArray() );
                    _ = IterateIList( "Array as IList", aal );
                    _ = IterateIEnumerable( "Array as IList", aal );
                    _ = IterateICollection( "Array as IList", aal );
                    _ = IterateArray( "List as IEnumerable", lae.ToArray() );
                    _ = IterateIList( "List as IEnumerable", aae.ToList() );
                    _ = IterateIEnumerable( "List as IEnumerable", aae );
                    _ = IterateICollection( "List as IEnumerable", aae.ToList() );
                    _ = IterateArray( "List as IList", lal.ToArray() );
                    _ = IterateIList( "List as IList", aal );
                    _ = IterateIEnumerable( "List as IList", aal );
                    _ = IterateICollection( "List as IList", aal );
                }

                if ( (testLevel & TestLevel.ForEach) != 0 )
                {
                    _ = ForEachArray( "Array as IEnumerable", aae.ToArray() );
                    _ = ForEachIList( "Array as IEnumerable", aae.ToList() );
                    _ = ForEachIEnumerable( "Array as IEnumerable", aae );
                    _ = ForEachICollection( "Array as IEnumerable", aae.ToList() );
                    _ = ForEachArray( "Array as IList", aal.ToArray() );
                    _ = ForEachIList( "Array as IList", aal );
                    _ = ForEachIEnumerable( "Array as IList", aal );
                    _ = ForEachICollection( "Array as IList", aal );
                    _ = ForEachArray( "List as IEnumerable", lae.ToArray() );
                    _ = ForEachIList( "List as IEnumerable", aae.ToList() );
                    _ = ForEachIEnumerable( "List as IEnumerable", aae );
                    _ = ForEachICollection( "List as IEnumerable", aae.ToList() );
                    _ = ForEachArray( "List as IList", lal.ToArray() );
                    _ = ForEachIList( "List as IList", aal );
                    _ = ForEachIEnumerable( "List as IList", aal );
                    _ = ForEachICollection( "List as IList", aal );
                }
            }
        }

        /// <summary> A bit-field of flags for specifying test levels. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [Flags]
        private enum TestLevel
        {
            /// <summary> An enum constant representing the none option. </summary>
            None = 1 << 0,
            /// <summary> An enum constant representing the iterate option. </summary>
            Iterate = 1 << 1,
            /// <summary> An enum constant representing for each option. </summary>
            ForEach = 1 << 2,
            /// <summary> An enum constant representing the populate option. </summary>
            Populate = 1 << 3,
            /// <summary> An enum constant representing the return value option. </summary>
            ReturnValue = 1 << 4
        }

        /// <summary> (Unit Test Method) iteration performance. </summary>
        /// <remarks>
        /// <list type="bullet"> <item>
        /// Times in ms                                             </item><item>
        /// populate 10000000 in 304                                </item><item>
        /// .ToArray    TO Read-Only</item><item>
        /// Collection(of Double)[10000000]    35                   </item><item>
        /// List(Of Double)[10000000]          23       23          </item><item>
        /// Populate    Iterate As      Iterate   For Each as   For Each</item><item>
        /// Item                                                Iterate   For Each  Collection  IEnumerable     As IList  IEnumerable   Return As
        /// IList</item><item>
        /// Array(Of Double)[10000000] cast as Array                 17      19     166          14              14         19          19
        /// </item><item>
        /// Array(Of Double)[10000000] cast as IList                 32      56     257          44              44
        /// 79          79 </item><item>
        /// Array(Of Double)[10000000] cast as IEnumerable          423      57     218         459
        /// 465         78          78 </item><item>
        /// Array(Of Double)[10000000] cast as ICollection          436      56     203         209
        /// 453         79          79 </item><item>
        /// 
        /// List(Of Double)[10000000] cast as Array                  14      19     197          14              14         19          18
        /// </item><item>
        /// List(Of Double)[10000000] cast as IList                  44      82     277          44              41
        /// 79          78    </item><item>
        /// List(Of Double)[10000000] cast as IEnumerable           200      87     335         469
        /// 402         82          78    </item><item>
        /// List(Of Double)[10000000] cast as ICollection           220      85     295         206
        /// 446         80          79    </item><item>
        /// </item><item>
        /// Collection(Of Double)[10000000] cast as Array            15      19     173</item><item>
        /// Collection(Of Double)[10000000] cast as IList            69      80     317</item><item>
        /// Collection(Of Double)[10000000] cast as IEnumerable     231      92     305</item><item>
        /// Collection(Of Double)[10000000] cast as ICollection     222      93     326</item><item>
        /// </item><item>
        /// Read-Only List(Of Double)[10000000] cast as Array        14      22</item><item>
        /// Read-Only List(Of Double)[10000000] cast as IList in     42      96</item><item>
        /// Read-Only List(Of Double)[10000000] cast as IEnumerable 197      90</item><item>
        /// Read-Only List(Of Double)[10000000] cast as ICollection 198     145</item><item>
        /// </item><item>
        /// Summary: Iterations using indexes has a significant penalty. </item><item>
        /// </item><item>
        /// </item><item>
        /// </item><item>
        /// </item><item>
        /// </item><item>
        /// </item><item>
        /// </item></list>
        /// </remarks>
        [TestMethod()]
        public void IterationPerformance()
        {
            RunTests( TestLevel.None );
        }

        #endregion

    }
}
