using System;
using System.Collections.Specialized;

using isr.Core.CollectionExtensions;
using isr.Core.MSTest;

using Microsoft.VisualBasic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.Framework.Trials
{

    /// <summary> A name value collection extensions tests. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestClass()]
    public class NameValueCollectionExtensionsTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( NameValueCollectionExtensionsTests.TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );

            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        /// <summary> The collection. </summary>
        private static readonly NameValueCollection Collection = new NameValueCollection() { { "ValidEntry", "ValidEntry" } };

        /// <summary> (Unit Test Method) does not throw exception when key does not exist. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void DoesNotThrowExceptionWhenKeyDoesNotExist()
        {
            Assert.AreEqual( Collection.GetValueAs( "InvalidEntry", "DefaultValue" ), "DefaultValue" );
        }

        /// <summary>
        /// (Unit Test Method) queries if a given does not throw exception when key exists.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void DoesNotThrowExceptionWhenKeyExists()
        {
            Assert.AreEqual( Collection.GetValueAs<string>( "ValidEntry" ), "ValidEntry" );
        }

        /// <summary> (Unit Test Method) throws exception when key is null or white space. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [TestMethod()]
        public void ThrowsExceptionWhenKeyIsNullOrWhiteSpace()
        {
            try
            {
                _ = Collection.GetValueAs<string>( null );
            }
            catch ( ArgumentException )
            {
            }
            catch
            {
                Assert.Fail( "expected exception was not thrown" );
            }

            try
            {
                _ = Collection.GetValueAs<string>( null );
            }
            catch ( ArgumentException )
            {
            }
            catch
            {
                Assert.Fail( "expected exception was not thrown" );
            }

            _ = Asserts.Instance.Throws<ArgumentException>( () => Collection.GetValueAs<string>( " " ) );
            _ = Asserts.Instance.Throws<ArgumentException>( () => Collection.GetValueAs<string>( Constants.vbTab ) );
            _ = Asserts.Instance.Throws<ArgumentException>( () => Collection.GetValueAs( null, string.Empty ) );
            _ = Asserts.Instance.Throws<ArgumentException>( () => Collection.GetValueAs( " ", string.Empty ) );
            _ = Asserts.Instance.Throws<ArgumentException>( () => Collection.GetValueAs( Constants.vbTab, string.Empty ) );
        }
    }
}
