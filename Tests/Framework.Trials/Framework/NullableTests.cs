using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.Framework.Trials
{

    /// <summary> Tests for using Nullable tests. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-11 </para>
    /// </remarks>
    [TestClass()]
    public class NullableTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( NullableTests.TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );

            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " NULLABLE TESTS "

        /// <summary> A test for Nullable equality. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void NullableBooleanTest()
        {
            var @bool = default( bool? );
            bool boolValue = true;
            Assert.AreEqual( true, @bool is null, "Initialized to nothing" );
            Assert.AreEqual( true, @bool.Equals( new bool?() ), "Nullable not set equals to a new Boolean" );
            Assert.AreEqual( false, @bool.Equals( boolValue ), "Nullable not set is not equal to {0}", ( object ) boolValue );
            // VB: Assert.AreEqual( default(bool?), @bool == boolValue, "Nullable '=' operator yields Nothing");
            Assert.AreEqual( false, @bool == boolValue, "Nullable '=' operator yields Nothing" );

            boolValue = false;
            Assert.AreEqual( false, @bool.Equals( boolValue ), "Nullable not set is not equal to True" );

            @bool = boolValue;
            Assert.AreEqual( true, @bool.HasValue, "Has value -- set to {0}", ( object ) boolValue );

            @bool = new bool?();
            Assert.AreEqual( true, @bool is null, "Nullable set to new Boolean is still nothing" );
            Assert.AreEqual( true, @bool.Equals( new bool?() ), "Nullable set to new Boolean equals to a new Boolean" );
            Assert.AreEqual( false, @bool.Equals( boolValue ), "Nullable set to new Boolean not equal to {0}", ( object ) boolValue );

            boolValue = false;
            Assert.AreEqual( false, @bool.Equals( boolValue ), "Nullable set to new Boolean not equal to True" );

            @bool = boolValue;
            Assert.AreEqual( true, @bool.HasValue, "Has value -- set to {0}", ( object ) boolValue );
        }

        /// <summary> A test for Nullable Integer equality. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void NullableIntegerTest()
        {
            int integerValue = 1;
            var nullInt = default( int? );
            Assert.AreEqual( true, nullInt is null, "Initialized to nothing" );
            nullInt = new int?();
            Assert.AreEqual( true, nullInt is null, "Nullable set to new Boolean is nothing" );
            Assert.AreEqual( true, nullInt.Equals( new int?() ), "Nullable set to new Integer equals to a new Integer" );
            Assert.AreEqual( false, nullInt.Equals( integerValue ), "Nullable set to new Integer not equal to {0}", ( object ) integerValue );
            Assert.AreEqual( false, nullInt.Equals( integerValue ), "Nullable set to new Integer not equal to {0}", ( object ) integerValue );
            nullInt = integerValue;
            Assert.AreEqual( true, nullInt.HasValue, "Set to {0}", ( object ) integerValue );
            Assert.AreEqual( integerValue, nullInt.Value, "Set to  {0}", ( object ) integerValue );
        }

        /// <summary>   (Unit Test Method) tests nullable. </summary>
        /// <remarks>   David, 2020-10-28. </remarks>
        [TestMethod()]
        public void NullableTest()
        {
            Assert.AreEqual( new bool?(), new bool?(), "? new bool?() = new bool?()" );
            Assert.AreNotEqual( new bool?( true ), new bool?(), "? new bool?()(true) = new bool?()" );
            Assert.IsNotNull( new bool?() == new bool?(), "? Null = new bool?() = new bool?()" );
        }

        #endregion

    }
}
