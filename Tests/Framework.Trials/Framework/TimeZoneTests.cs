using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.Framework.Trials
{

    /// <summary> A time zone tests. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-03-14 </para>
    /// </remarks>
    [TestClass()]
    public class TimeZoneTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TimeZoneTests.TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );

            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " TIME ZONE TESTS "

        /// <summary> Query if 'timeZone' is daylight saving time. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="timeZone">  The time zone. </param>
        /// <param name="dateValue"> The date value. </param>
        /// <returns> <c>true</c> if daylight saving time; otherwise <c>false</c> </returns>
        public static bool IsDaylightSavingTime( string timeZone, string dateValue )
        {
            return TimeZoneInfo.FindSystemTimeZoneById( timeZone ).IsDaylightSavingTime( DateTimeOffset.Parse( dateValue ) );
        }

        /// <summary> (Unit Test Method) tests Arizona time zone. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void ArizonaTimeZoneTest()
        {
            string dayString = "2017-01-01";
            string timeZone = "US Mountain Standard Time";
            bool expected = false;
            bool actual = IsDaylightSavingTime( timeZone, dayString );
            Assert.AreEqual( expected, actual );
            dayString = "2017-06-01";
            actual = IsDaylightSavingTime( timeZone, dayString );
            Assert.AreEqual( expected, actual );
        }

        /// <summary> (Unit Test Method) tests pacific time zone. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void PacificTimeZoneTest()
        {
            string dayString = "2017-01-01";
            string timeZone = "Pacific Standard Time";
            bool expected = false;
            bool actual = IsDaylightSavingTime( timeZone, dayString );
            Assert.AreEqual( expected, actual );
            dayString = "2017-06-01";
            expected = true;
            actual = IsDaylightSavingTime( timeZone, dayString );
            Assert.AreEqual( expected, actual );
        }

        /// <summary> (Unit Test Method) tests get Arizona time zone. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void GetArizonaTimeZoneTest()
        {
            string expected = "US Mountain Standard Time";
            string actual = TimeZoneInfo.FindSystemTimeZoneById( expected ).Id;
            Assert.AreEqual( expected, actual );
        }

        /// <summary> (Unit Test Method) tests local pacific standard time. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void LocalPacificStandardTimeTest()
        {
            var tz = TimeZoneInfo.Local;
            string expected = TestInfo.TimeZone;
            string actual = tz.Id;
            Assert.AreEqual( expected, actual );
        }

        /// <summary> Query if 'timeZone' is daylight saving time. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="dateValue"> The date value. </param>
        /// <returns> <c>true</c> if daylight saving time; otherwise <c>false</c> </returns>
        public static bool IsDaylightSavingTime( string dateValue )
        {
            var tz = TimeZoneInfo.Local;
            return tz.IsDaylightSavingTime( DateTimeOffset.Parse( dateValue ) );
        }

        /// <summary> Query if 'timeZone' is daylight saving time. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> <c>true</c> if daylight saving time; otherwise <c>false</c> </returns>
        public static bool IsDaylightSavingTime()
        {
            var tz = TimeZoneInfo.Local;
            return tz.IsDaylightSavingTime( DateTimeOffset.Now );
        }

        /// <summary> (Unit Test Method) tests daylight savings time affirmative. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void DaylightSavingsTimeAffirmativeTest()
        {
            bool expectedBoolean = false;
            bool actualBoolean = IsDaylightSavingTime( "2017-01-01" );
            Assert.AreEqual( expectedBoolean, actualBoolean );
        }

        /// <summary> (Unit Test Method) tests daylight savings time negative. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void DaylightSavingsTimeNegativeTest()
        {
            bool expectedBoolean = true;
            bool actualBoolean = IsDaylightSavingTime( "2017-06-01" );
            Assert.AreEqual( expectedBoolean, actualBoolean );
        }

        /// <summary> (Unit Test Method) tests UTC time offset. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void UtcTimeOffsetTest()
        {
            var expected = TimeSpan.FromHours( IsDaylightSavingTime() ? TestInfo.TimeZoneOffset + 1d : TestInfo.TimeZoneOffset );
            var actual = DateTimeOffset.Now.Offset;
            Assert.AreEqual( expected, actual );
        }

        /// <summary> (Unit Test Method) tests UTC time conversion. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void UtcTimeConversionTest()
        {
            var timeNow = DateTime.Now;
            var expected = timeNow.ToUniversalTime();
            var actual = timeNow.Subtract( DateTimeOffset.Now.Offset );
            Assert.AreEqual( expected, actual );
        }

        /// <summary> (Unit Test Method) tests UTC time. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void UtcTimeTest()
        {
            var timeNow = DateTime.Now;
            var expected = timeNow.ToUniversalTime();
            var actual = timeNow.Subtract( DateTimeOffset.Parse( timeNow.Date.ToShortDateString() ).Offset );
            Assert.AreEqual( expected, actual );
        }

        #endregion

    }
}
