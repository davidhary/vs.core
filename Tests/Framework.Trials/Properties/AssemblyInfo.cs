﻿using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.Core.Framework.Trials.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.Core.Framework.Trials.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.Core.Framework.Trials.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
