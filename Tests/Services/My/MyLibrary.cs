#pragma warning disable IDE1006 // Naming Styles
namespace isr.Core.ServicesTests.My
#pragma warning restore IDE1006 // Naming Styles
{
    /// <summary>   my library. </summary>
    /// <remarks>   David, 2020-03-18. </remarks>
    internal class MyLibrary
    {
        public const string AssemblyTitle = "ISR Core Services Test Library";
        public const string AssemblyDescription = "ISR Core Services Test Library";
        public const string AssemblyProduct = "isr.Core.Services.Tests";
        public const string AssemblyConfiguration = "Debug";
    }
}
