using System;
using System.Diagnostics;
using System.IO;

using isr.Core.FileInfoExtensions;
using isr.Core.VisualBasicLoggingExtensions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.ServicesTests
{

    /// <remarks> David, 2020-09-18. </remarks>
    [TestClass()]
    public class MyLibraryTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( isr.Core.Services.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " LOG TESTS  "

        /// <summary> Assert logging. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        public static void AssertLogging()
        {
            var logger = isr.Core.Services.My.MyLibrary.Appliance.ApplicationLog;
            var fi = new FileInfo( logger.DefaultFileLogWriterFilePath() );
            bool expectedAutoFlush = false;
            bool actualAutoFlush = logger.DefaultFileLogWriter.AutoFlush;
            Assert.AreEqual( expectedAutoFlush, actualAutoFlush, "initial auto flash should match" );

            expectedAutoFlush = false;
            logger.DefaultFileLogWriter.AutoFlush = expectedAutoFlush;
            actualAutoFlush = logger.DefaultFileLogWriter.AutoFlush;
            Assert.AreEqual( expectedAutoFlush, actualAutoFlush, "new auto flash should match" );

            long initialLogFileSize = fi.FileSize();
            logger.WriteEntry( $"message from {nameof( MyLibraryTests.AssertLogging )} {DateTimeOffset.Now:o}" );
            logger.WriteEntry( $"message from {nameof( MyLibraryTests.AssertLogging )} {DateTimeOffset.Now:o}", TraceEventType.Error, 1 );
            logger.DefaultFileLogWriter.Flush();
            isr.Core.ApplianceBase.Delay( 50d );
            fi.Refresh();
            long newLogFileSize = fi.FileSize();
            Assert.IsTrue( newLogFileSize > initialLogFileSize, $"New file Size {newLogFileSize} > initial file size {initialLogFileSize}" );
        }

        /// <summary> (Unit Test Method) tests application log. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void ApplicationLogTest()
        {
            AssertLogging();
        }

        /// <summary>   Assert logger file size. </summary>
        /// <remarks>   David, 2020-09-21. </remarks>
        /// <param name="logger">           The logger. </param>
        /// <param name="initialFileSize">  Initial size of the file. </param>
        /// <param name="preambleMessage">  Message describing the preamble. </param>
        public static void AssertLoggerFileSize( Logger logger, long initialFileSize, string preambleMessage )
        {
            logger.Flush();
            // logger.DefaultFileLogWriter.Flush();
            isr.Core.ApplianceBase.Delay( 50d );
            long newLogFileSize = logger.FileSize;
            Assert.IsTrue( newLogFileSize > initialFileSize, $"{preambleMessage}: file Size {newLogFileSize} should be larger than initial file size {initialFileSize}" );
        }


        /// <summary> Tests logging unpublished messages. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="expected"> The expected. </param>
        public static void AssertLogger( TraceEventType expected )
        {

            string methodName = System.Reflection.MethodInfo.GetCurrentMethod().Name;
            var logger = isr.Core.Services.My.MyLibrary.Appliance.Logger;
            long initialLogFileSize = logger.FileSize;
            string actualLogFileName = logger.FullLogFileName;
            Assert.IsFalse( string.IsNullOrWhiteSpace( actualLogFileName ), $"Tests if log file name '{actualLogFileName}' is empty" );

            bool expectedAutoFlush = false;
            bool actualAutoFlush = logger.DefaultFileLogWriter.AutoFlush;
            Assert.AreEqual( expectedAutoFlush, actualAutoFlush, "initial auto flash should match" );

            Assert.AreEqual( actualLogFileName, logger.DefaultFileLogWriter.FullLogFileName,
                            $"{nameof( logger.DefaultFileLogWriter )}.{nameof( logger.FullLogFileName )} should match" );

            expectedAutoFlush = false;
            logger.DefaultFileLogWriter.AutoFlush = expectedAutoFlush;
            actualAutoFlush = logger.DefaultFileLogWriter.AutoFlush;
            Assert.AreEqual( expectedAutoFlush, actualAutoFlush, "new auto flash should match" );

            string payload = $"message from {methodName} {DateTimeOffset.Now:o}";
            logger.WriteEntry( payload, expected, 1 );
            AssertLoggerFileSize( logger, initialLogFileSize, $"{nameof( Logger.WriteLogEntry )}(string)" );
            payload = $"trace message from {methodName} {DateTimeOffset.Now:o}";
            var expectedMessage = new TraceMessage( expected, isr.Core.Services.My.MyLibrary.Appliance.TraceEventId, payload );
            initialLogFileSize = logger.FileSize;
            logger.WriteLogEntry( expectedMessage );
            AssertLoggerFileSize( logger, initialLogFileSize, $"{nameof( Logger.WriteLogEntry )}(TraceMessage)" );
        }

        /// <summary> (Unit Test Method) tests application log. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void LoggerTest()
        {
            AssertLogger( TraceEventType.Warning );
        }

        /// <summary> Tests logging unpublished messages. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="expected">                           The expected. </param>
        /// <param name="checkUnpublishedMessageLogFileSize"> True to check unpublished message log file
        /// size. </param>
        public static void AssertLoggingUnpublishedMessages( TraceEventType expected )
        {
            string methodName = System.Reflection.MethodInfo.GetCurrentMethod().Name;
            var logger = isr.Core.Services.My.MyLibrary.Appliance.Logger;
            long initialLogFileSize = logger.FileSize;
            string actualLogFileName = logger.FullLogFileName;
            Assert.IsFalse( string.IsNullOrWhiteSpace( actualLogFileName ), $"Tests if log file name '{actualLogFileName}' is empty" );

            bool expectedAutoFlush = false;
            bool actualAutoFlush = logger.DefaultFileLogWriter.AutoFlush;
            Assert.AreEqual( expectedAutoFlush, actualAutoFlush, "initial auto flash should match" );

            string payload = $"message from {methodName} {DateTimeOffset.Now:o}";
            logger.WriteEntry( payload, expected, 1 );
            AssertLoggerFileSize( logger, initialLogFileSize, $"{nameof( Logger.WriteLogEntry )}(string)" );
            payload = $"trace message from {methodName} {DateTimeOffset.Now:o}";
            var expectedMessage = new TraceMessage( expected, isr.Core.Services.My.MyLibrary.Appliance.TraceEventId, payload );
            initialLogFileSize = logger.FileSize;
            logger.WriteLogEntry( expectedMessage );
            AssertLoggerFileSize( logger, initialLogFileSize, $"{nameof( Logger.WriteLogEntry )}(TraceMessage)" );

            payload = $"trace message from {methodName} {DateTimeOffset.Now:o}";
            expectedMessage = new TraceMessage( expected, isr.Core.Services.My.MyLibrary.Appliance.TraceEventId, payload );
            initialLogFileSize = logger.FileSize;
            _ = logger.TraceEvent( expectedMessage );
            AssertLoggerFileSize( logger, initialLogFileSize, $"{nameof( Logger.TraceEvent )}(TraceMessage)" );

            // clear trace message log
            _ = isr.Core.Services.My.MyLibrary.Appliance.UnpublishedTraceMessages.DequeueContent();

            // check content
            string actualContents = isr.Core.Services.My.MyLibrary.Appliance.UnpublishedTraceMessages.DequeueContent();
            Assert.IsTrue( string.IsNullOrWhiteSpace( actualContents ), "Unpublished messages should clear" );

            // log an unpublished trace message to the library.
            payload = $"trace message from {methodName} {DateTimeOffset.Now:o}";
            expectedMessage = new TraceMessage( expected, isr.Core.Services.My.MyLibrary.Appliance.TraceEventId + 1, payload );
            initialLogFileSize = logger.FileSize;
            logger.FlushOnTraceQueueEnabled = true;
            _ = isr.Core.Services.My.MyLibrary.Appliance.LogUnpublishedMessage( expectedMessage );
            var actualMessage = isr.Core.Services.My.MyLibrary.Appliance.UnpublishedTraceMessages.TryPeek();
            Assert.AreEqual( expectedMessage.Id, actualMessage.Id, " Message trace events identities match" );
            Assert.AreEqual( 1, isr.Core.Services.My.MyLibrary.Appliance.UnpublishedTraceMessages.Count, $"{nameof( isr.Core.Services.My.MyLibrary.Appliance.UnpublishedTraceMessages )} count should match" );
            AssertLoggerFileSize( logger, initialLogFileSize, $"{nameof( Logger.TraceEvent )}(Unpublished Messages)" );
        }

        /// <summary> (Unit Test Method) tests logging unpublished messages. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void LoggingUnpublishedMessagesTest()
        {
            AssertLoggingUnpublishedMessages( TraceEventType.Warning );
        }

        #endregion

    }
}
