using System;

namespace isr.Core.ElectricNominal.MSTest
{
    /// <summary>   An application settings. </summary>
    /// <remarks>   David, 2021-01-28. </remarks>
    public class AppSettings
    {

        #region " CONSTRUCTION "

        private AppSettings()
        {
            this.Settings = new Settings();
            this.TestSiteSettings = new TestSiteSettings();
            this.ReadSettings();
        }

        /// <summary>   The lazy instance. </summary>
        private static readonly Lazy<AppSettings> LazyInstance = new ( () => new AppSettings() );

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static AppSettings Instance => LazyInstance.Value;

        #endregion

        /// <summary>   Reads the settings. </summary>
        /// <remarks>   David, 2021-01-29. </remarks>
        public void ReadSettings()
        {
            if ( this.Settings.Exists() )
            {
                this.Settings.ReadSettings();
                this.TestSiteSettings.ReadSettings();
            }
        }

        /// <summary>   Saves the settings. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        public void SaveSettings()
        {
            this.Settings.SaveSettings( new object[] { this.Settings, this.TestSiteSettings } );
        }

        /// <summary>   Gets the filename of the application settings file. </summary>
        /// <value> The filename of the application settings file. </value>
        public static string AppSettingsFileName()
        {
            var name = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
            return System.IO.Path.Combine( AppContext.BaseDirectory, $"{name}.settings.json" );
        }

        /// <summary>   Gets or sets options for controlling the operation. </summary>
        /// <remarks>
        /// Note that the name of the field must match  class name or the name of the section as defined
        /// in the class <see cref="isr.Core.Json.SettingsSectionAttribute"/> or.
        /// </remarks>
        /// <value> The settings. </value>
        internal Settings Settings { get; }

        /// <summary>   Gets or sets the test site settings. </summary>
        /// <remarks>
        /// Note that the name of the field must match  class name or the name of the section as defined
        /// in the class <see cref="isr.Core.Json.SettingsSectionAttribute"/> or.
        /// </remarks>
        /// <value> The test site settings. </value>
        public TestSiteSettings TestSiteSettings { get; }

    }

}
