using System;

namespace isr.Core.ElectricNominal.MSTest
{

    /// <summary>   A test site settings. </summary>
    /// <remarks>   David, 2021-02-01. </remarks>
    [isr.Core.Json.SettingsSection( nameof( TestSiteSettings ) )]
    public class TestSiteSettings : isr.Core.Json.JsonSettingsBase
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        public TestSiteSettings() : base( Settings.BuildAppSettingsFileName() )
        { }

        #region " TEST SITE CONFIGURATION "

        /// <summary> Returns true to output test messages at the verbose level. </summary>
        /// <value> The verbose messaging level. </value>
        public virtual bool Verbose { get; set; }

        /// <summary> Returns true to enable this device. </summary>
        /// <value> The device enable option. </value>
        public virtual bool Enabled { get; set; }

        /// <summary> Gets or sets all. </summary>
        /// <value> all. </value>
        public virtual bool All { get; set; }

        #endregion

        #region " TEST SITE LOCATION IDENTIFICATION "

        /// <summary> Gets or sets the candidate time zones of the test site. </summary>
        /// <value> The candidate time zones of the test site. </value>
        public virtual string[] TimeZones { get; set; }

        /// <summary> Gets or sets the time zone offsets of the test sites. </summary>
        /// <value> The time zone offsets. </value>
        public virtual double[] TimeZoneOffsets { get; set; }

        /// <summary>   Gets or sets the IPv4 prefixes. </summary>
        /// <value> The IPv4 prefixes. </value>
        public virtual string[] IPv4Prefixes { get; set; }


        /// <summary> The time zone. </summary>
        private string _TimeZone;

        /// <summary> Gets the time zone of the test site. </summary>
        /// <value> The time zone of the test site. </value>
        public string TimeZone()
        {
            if ( string.IsNullOrWhiteSpace( this._TimeZone ) )
            {
                this._TimeZone = this.TimeZones[this.HostInfoIndex()];
            }
            return this._TimeZone;
        }

        /// <summary> The time zone offset. </summary>
        private double _TimeZoneOffset = double.MinValue;

        /// <summary> Gets the time zone offset of the test site. </summary>
        /// <value> The time zone offset of the test site. </value>
        public double TimeZoneOffset()
        {
            if ( this._TimeZoneOffset == double.MinValue )
            {
                this._TimeZoneOffset = this.TimeZoneOffsets[this.HostInfoIndex()];
            }
            return this._TimeZoneOffset;
        }

        /// <summary> Gets the host name of the test site. </summary>
        /// <value> The host name of the test site. </value>
        public static string HostName()
        {
            return System.Net.Dns.GetHostName();
        }

        /// <summary> The host address. </summary>
        private System.Net.IPAddress _HostAddress;

        /// <summary> Gets the IP address of the test site. </summary>
        /// <value> The IP address of the test site. </value>
        public System.Net.IPAddress HostAddress()
        {
            if ( this._HostAddress is null )
            {
                foreach ( System.Net.IPAddress value in System.Net.Dns.GetHostEntry( HostName() ).AddressList )
                {
                    if ( value.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork )
                    {
                        this._HostAddress = value;
                        break;
                    }
                }
            }
            return this._HostAddress;
        }

        /// <summary> Zero-based index of the host information. </summary>
        private int _HostInfoIndex = -1;

        /// <summary> Gets the index into the host information strings. </summary>
        /// <value> The index into the host information strings. </value>
        public int HostInfoIndex()
        {
            if ( this._HostInfoIndex < 0 )
            {
                string ip = this.HostAddress().ToString();
                int i = -1;
                foreach ( string value in this.IPv4Prefixes )
                {
                    i += 1;
                    if ( ip.StartsWith( value, StringComparison.OrdinalIgnoreCase ) )
                    {
                        this._HostInfoIndex = i;
                        break;
                    }
                }
            }

            return this._HostInfoIndex;
        }

        #endregion

    }
}
