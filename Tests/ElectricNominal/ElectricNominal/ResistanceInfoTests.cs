using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.ElectricNominal.MSTest
{

    /// <summary>
    /// This is a test class for ResistanceInfoTest and is intended to contain all ResistanceInfoTest
    /// Unit Tests.
    /// </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestClass()]
    public class ResistanceInfoTests
    {

        #region " CONSTRUCTION & CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void TestClassInitialize( TestContext testContext )
        {
            try
            {
                string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
                TraceListener = new isr.Core.Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
                _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
            System.Diagnostics.Trace.Listeners.Remove( TraceListener );
            TraceListener.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            TraceListener.Queue.Clear();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            if ( !TraceListener.Queue.IsEmpty )
                Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{String.Join( Environment.NewLine, TraceListener.Queue.ToArray() )}" );
        }

        /// <summary>   Gets or sets the trace listener. </summary>
        /// <value> The trace listener. </value>
        private static isr.Core.Tracing.TestWriterQueueTraceListener TraceListener { get; set; }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        #endregion

        /// <summary> A test for TryParse. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="resistanceCode">     The resistance code. </param>
        /// <param name="resistanceExpected"> The resistance expected. </param>
        /// <param name="expected">           True if expected. </param>
        public static void TryParse( string resistanceCode, double resistanceExpected, bool expected )
        {
            double resistance = 0.0d;
            string details = string.Empty;
            bool actual;
            actual = ResistanceInfo.TryParse( resistanceCode, ref resistance, ref details );
            Assert.AreEqual( resistanceExpected, resistance, details );
            Assert.AreEqual( expected, actual, details );
        }

        /// <summary> A test for TryParse. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void TryParseTest()
        {
            TryParse( "121", 120d, true );
            TryParse( "230", 23d, true );
            TryParse( "1000", 100d, true );
            TryParse( "2R67", 2.67d, true );
            TryParse( "2.67", 2.67d, true );
            TryParse( "12K6675", 12667.5d, true );
            TryParse( "100R5", 100.5d, true );
            TryParse( "R0015", 0.0015d, true );
            TryParse( "100R", 100d, true );
        }
    }
}
