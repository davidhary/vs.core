using System;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.FormaTests
{

    /// <summary> This is a test class for the blue splash screen. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestClass()]
    public class BlueSplashScreenTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );

            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        /// <summary> (Unit Test Method) displays a splash screen test. </summary>
        /// <remarks>
        /// This unit test causes a warning when ran from the test agent:
        /// This is an issue with the way the picture box displays a gif image.
        /// System.AppDomainUnloadedException: Attempted to access an unloaded AppDomain. This can happen
        /// if the test(s) started a thread but did not stop it. Make sure that all the threads started
        /// by the test(s) are stopped before completion.
        /// https://stackoverflow.com/questions/42979071/appdomainunloadedexception-when-unit-testing-a-winforms-form-with-an-animated-gi.
        /// </remarks>
        [TestMethod()]
        public void DisplaySplashScreenTest()
        {
            using var splashScreen = new Forma.BlueSplash();
            splashScreen.Show();
            splashScreen.TopmostSetter( true );
            Display( splashScreen );
        }

        /// <summary> Displays the given splashScreen. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="splashScreen"> The splash screen. </param>
        private static void Display( Forma.BlueSplash splashScreen )
        {
            for ( int i = 1; i <= 10; i++ )
            {
                splashScreen.DisplayMessage( $"Splash message {DateTimeOffset.Now}" );
                Task.Delay( 300 ).Wait();
            }
        }

        /// <summary> Tests the process exception on a another thread. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void DisplaySplashScreenThreadTest()
        {
            var oThread = new System.Threading.Thread( new System.Threading.ThreadStart( this.DisplaySplashScreenTest ) );
            oThread.Start();
            oThread.Join();
        }
    }
}
