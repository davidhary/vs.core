using System;
using System.Diagnostics;
using System.Linq;

using isr.Core.Constructs;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.FormaTests
{

    /// <summary> Collections Tests. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-02-18. </para>
    /// </remarks>
    [TestClass()]
    public class CollectionsTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Forma.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( FormaTests.CollectionsTests.TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( FormaTests.TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion


        #region " SORTABLE BINDING LIST "

        /// <summary> A sale. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        private class Sale
        {

            /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
            /// <remarks> David, 2020-09-23. </remarks>
            public Sale()
            {
                this.SaleDate = DateTimeOffset.Now;
            }

            /// <summary> Gets the sale details. </summary>
            /// <value> The sale details. </value>
            [System.Diagnostics.CodeAnalysis.SuppressMessage( "Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
            public SortableBindingList<SaleDetail> SaleDetails { get; set; }

            /// <summary> Gets the salesman. </summary>
            /// <value> The salesman. </value>
            [System.Diagnostics.CodeAnalysis.SuppressMessage( "Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
            public string Salesman { get; set; }

            /// <summary> Gets the client. </summary>
            /// <value> The client. </value>
            public string Client { get; set; }

            /// <summary> Gets the sale date. </summary>
            /// <value> The sale date. </value>
            [System.Diagnostics.CodeAnalysis.SuppressMessage( "Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
            public DateTimeOffset SaleDate { get; set; }

            /// <summary> Gets the total number of amount. </summary>
            /// <value> The total number of amount. </value>
            [System.Diagnostics.CodeAnalysis.SuppressMessage( "Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
            public decimal TotalAmount
            {
                get {
                    Debug.Assert( this.SaleDetails is object );
                    return this.SaleDetails.Sum( a => a.TotalAmount );
                }
            }
        }

        /// <summary> A sale detail. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        private class SaleDetail
        {

            /// <summary> Gets the product. </summary>
            /// <value> The product. </value>
            [System.Diagnostics.CodeAnalysis.SuppressMessage( "Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
            public string Product { get; set; }

            /// <summary> Gets the quantity. </summary>
            /// <value> The quantity. </value>
            public int Quantity { get; set; }

            /// <summary> Gets the unit price. </summary>
            /// <value> The unit price. </value>
            public decimal UnitPrice { get; set; }

            /// <summary> Gets the total number of amount. </summary>
            /// <value> The total number of amount. </value>
            public decimal TotalAmount => this.UnitPrice * this.Quantity;
        }

        /// <summary> Sortable list. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> A list of. </returns>
        private static SortableBindingList<Sale> SortableList()
        {
            var sales = new[] {
                new Sale()
                {
                    Client = "Jane Doe",
                    SaleDate = new DateTime(2008, 1, 10),
                    Salesman = "John",
                    SaleDetails = new SortableBindingList<SaleDetail>() { new SaleDetail() { Product = "Sportsman", Quantity = 1, UnitPrice = 80m }, new SaleDetail() { Product = "Tusker Malt", Quantity = 2, UnitPrice = 100m }, new SaleDetail() { Product = "Alvaro", Quantity = 1, UnitPrice = 50m } }
                },
                new Sale()
                {
                    Client = "Ben Jones",
                    SaleDate = new DateTime(2008, 1, 11),
                    Salesman = "Danny",
                    SaleDetails = new SortableBindingList<SaleDetail>() { new SaleDetail() { Product = "Embassy Kings", Quantity = 1, UnitPrice = 80m }, new SaleDetail() { Product = "Tusker", Quantity = 5, UnitPrice = 100m }, new SaleDetail() { Product = "Movie", Quantity = 3, UnitPrice = 50m } }
                },
                new Sale()
                {
                    Client = "Tim Kim",
                    SaleDate = new DateTime(2008, 1, 12),
                    Salesman = "Kaplan",
                    SaleDetails = new SortableBindingList<SaleDetail>() { new SaleDetail() { Product = "Citizen Special", Quantity = 10, UnitPrice = 30m }, new SaleDetail() { Product = "Burn", Quantity = 2, UnitPrice = 100m } }
                }
            };
            return new SortableBindingList<Sale>( sales.ToList() );
        }

        /// <summary> (Unit Test Method) tests sortable binding list. </summary>
        /// <remarks> Tested 2019-05-14. </remarks>
        [TestMethod()]
        public void SortableBindingListTest()
        {
            var sales = SortableList();
            var l = sales.OrderBy( x => x.Client ).ToList();
            Assert.IsTrue( string.Compare( l[0].Client, l[1].Client, StringComparison.CurrentCulture ) == -1, $"{l[0].Client} must come before {l[1].Client}" );
            l = sales.OrderByDescending( x => x.Client ).ToList();
            Assert.IsTrue( string.Compare( l[0].Client, l[1].Client, StringComparison.CurrentCulture ) == 1, $"{l[0].Client} must come after {l[1].Client}" );
            // sales.OrderByDescending(Of String)(Function(x) x.Client)
            string expectedFirstClient = "Abe";
            sales.Add( new Sale() {
                Client = expectedFirstClient,
                SaleDate = new DateTime( 2009, 1, 12 ),
                Salesman = "Kaplan",
                SaleDetails = new SortableBindingList<SaleDetail>() { new SaleDetail() { Product = "Pepsi", Quantity = 4, UnitPrice = 2.8m }, new SaleDetail() { Product = "Perrier", Quantity = 12, UnitPrice = 1.5m } }
            } );
            string expectedLastClient = "Yoda";
            sales.Add( new Sale() {
                Client = expectedLastClient,
                SaleDate = new DateTime( 2009, 1, 12 ),
                Salesman = "Kaplan",
                SaleDetails = new SortableBindingList<SaleDetail>() { new SaleDetail() { Product = "Cola", Quantity = 4, UnitPrice = 3.1m }, new SaleDetail() { Product = "Avian", Quantity = 12, UnitPrice = 1.5m } }
            } );
        }

        /// <summary> (Unit Test Method) tests data grid view sortable binding list. </summary>
        /// <remarks> Tested 2019-05-14. </remarks>
        [TestMethod]
        public void DataGridViewSortableBindingListTest()
        {
            using var panel = new System.Windows.Forms.Form();
            using var grid = new System.Windows.Forms.DataGridView();
            // grid.CreateControl()
            panel.Controls.Add( grid );
            // panel.PerformLayout()
            // grid.PerformLayout()
            var sales = SortableList();
            grid.DataSource = sales;
            // grid has a header row.
            int expectedItemCount = sales.Count + 1;
            Assert.AreEqual( expectedItemCount, grid.Rows.Count, "Expected row count" );
            string firstValue = grid.Rows[0].Cells[nameof( Sale.Client )].Value.ToString();
            string secondValue = grid.Rows[1].Cells[nameof( Sale.Client )].Value.ToString();
            Assert.IsTrue( string.Compare( firstValue, secondValue, StringComparison.CurrentCulture ) == 1, $"Before sorting {firstValue} must come before {secondValue}" );

            // test sort ascending
            var direction = System.ComponentModel.ListSortDirection.Ascending;
            grid.Sort( grid.Columns[nameof( Sale.Client )], direction );
            firstValue = grid.Rows[0].Cells[nameof( Sale.Client )].Value.ToString();
            secondValue = grid.Rows[1].Cells[nameof( Sale.Client )].Value.ToString();
            Assert.IsTrue( string.Compare( firstValue, secondValue, StringComparison.CurrentCulture ) == -1, $"After sorting {direction} {firstValue} must come before {secondValue}" );

            // test sort descending
            direction = System.ComponentModel.ListSortDirection.Descending;
            grid.Sort( grid.Columns[nameof( Sale.Client )], direction );
            firstValue = grid.Rows[0].Cells[nameof( Sale.Client )].Value.ToString();
            secondValue = grid.Rows[1].Cells[nameof( Sale.Client )].Value.ToString();
            Assert.IsTrue( string.Compare( firstValue, secondValue, StringComparison.CurrentCulture ) == 1, $"After sorting {direction} {firstValue} must come before {secondValue}" );
        }

        #endregion


    }
}
