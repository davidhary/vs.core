using System;
using System.Diagnostics;

using isr.Core.WinForms.ComboBoxEnumExtensions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.FormaTests
{

    /// <summary> Enum extensions tests. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-03-14 </para>
    /// </remarks>
    [TestClass()]
    public class EnumExtensionsTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Forma.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( FormaTests.EnumExtensionsTests.TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( FormaTests.TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );

            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion


        #region " COMBO BOX TESTS "

        /// <summary> (Unit Test Method) tests combo box trace event type enum. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod]
        public void ComboBoxTraceEventTypeEnumTest()
        {
            var includeMask = TraceEventType.Critical | TraceEventType.Error | TraceEventType.Information | TraceEventType.Verbose | TraceEventType.Warning;
            var excludeMask = TraceEventType.Critical;
            using var panel = new System.Windows.Forms.Form();
            using var comboBox = new System.Windows.Forms.ComboBox();
            // comboBox.CreateControl()
            panel.Controls.Add( comboBox );
            // panel.PerformLayout()
            // comboBox.PerformLayout()

            // list enum names
            comboBox.ListEnumNames( includeMask, excludeMask );
            int expectedItemCount = 4;
            Assert.AreEqual( expectedItemCount, comboBox.Items.Count, "Expected item count" );

            // selected item
            comboBox.SelectedIndex = 0;
            var expectedValue = TraceEventType.Error;
            var actualItem = comboBox.SelectedEnumItem<TraceEventType>();
            Assert.AreEqual( expectedValue, actualItem.Key, "Selected item at index zero" );

            // select value
            expectedValue = TraceEventType.Information;
            actualItem = comboBox.SelectValue( expectedValue );
            Assert.AreEqual( expectedValue, actualItem.Key, "Selected item by value" );
            actualItem = comboBox.SelectedEnumItem<TraceEventType>();
            Assert.AreEqual( expectedValue, actualItem.Key, "Selected item by key value pair" );
            Assert.AreEqual( expectedValue, comboBox.SelectedEnumValue<TraceEventType>(), "Generic selected value" );
            Assert.AreEqual( expectedValue, comboBox.SelectedEnumValue<TraceEventType>( TraceEventType.Warning ), "Generic selected value with default" );
        }

        /// <summary> (Unit Test Method) tests combo box notify synchronize level enum. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod]
        public void ComboBoxNotifySyncLevelEnumTest()
        {
            var includeMask = NotifySyncLevel.Async | NotifySyncLevel.Sync;
            var excludeMask = NotifySyncLevel.None;
            using var panel = new System.Windows.Forms.Form();
            using var comboBox = new System.Windows.Forms.ComboBox();
            // comboBox.CreateControl()
            panel.Controls.Add( comboBox );
            // panel.PerformLayout()
            // comboBox.PerformLayout()

            // list enum names
            comboBox.ListEnumNames( includeMask, excludeMask );
            int expectedItemCount = 2;
            Assert.AreEqual( expectedItemCount, comboBox.Items.Count, "Expected item count" );

            // selected item
            comboBox.SelectedIndex = 0;
            var expectedValue = NotifySyncLevel.Sync;
            var actualItem = comboBox.SelectedEnumItem<NotifySyncLevel>();
            Assert.AreEqual( expectedValue, actualItem.Key, "Selected item at index zero" );

            // select value
            expectedValue = NotifySyncLevel.Async;
            actualItem = comboBox.SelectValue( expectedValue );
            Assert.AreEqual( expectedValue, actualItem.Key, "Selected item by value" );
            actualItem = comboBox.SelectedEnumItem<NotifySyncLevel>();
            Assert.AreEqual( expectedValue, actualItem.Key, "Selected item by key value pair" );
            Assert.AreEqual( expectedValue, comboBox.SelectedEnumValue<NotifySyncLevel>(), "Generic selected value" );
            Assert.AreEqual( expectedValue, comboBox.SelectedEnumValue<NotifySyncLevel>( NotifySyncLevel.None ), "Generic selected value with default" );
        }

        /// <summary>
        /// (Unit Test Method) tests tool strip combo box notify synchronize level enum.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod]
        public void ToolStripComboBoxNotifySyncLevelEnumTest()
        {
            var includeMask = NotifySyncLevel.Async | NotifySyncLevel.Sync;
            var excludeMask = NotifySyncLevel.None;
            using var panel = new System.Windows.Forms.Form();
            using var toolStrip = new System.Windows.Forms.ToolStrip();
            panel.Controls.Add( toolStrip );
            using var toolStripComboBox = new System.Windows.Forms.ToolStripComboBox();
            _ = toolStrip.Items.Add( toolStripComboBox );
            toolStrip.ResumeLayout();
            toolStrip.PerformLayout();
            toolStrip.Invalidate();
            // list enum names
            toolStripComboBox.ComboBox.ListEnumNames( includeMask, excludeMask );
            int expectedItemCount = 2;
            Assert.AreEqual( expectedItemCount, toolStripComboBox.Items.Count, "Expected item count" );

            // selected item
            toolStripComboBox.SelectedIndex = 0;
            var expectedValue = NotifySyncLevel.Sync;
            var actualItem = toolStripComboBox.SelectedEnumItem<NotifySyncLevel>();
            Assert.AreEqual( expectedValue, actualItem.Key, "Selected item at index zero" );

            // select value
            expectedValue = NotifySyncLevel.Async;
            actualItem = toolStripComboBox.SelectValue( expectedValue );
            Assert.AreEqual( expectedValue, actualItem.Key, "Selected item by value" );
            actualItem = toolStripComboBox.SelectedEnumItem<NotifySyncLevel>();
            Assert.AreEqual( expectedValue, actualItem.Key, "Selected item by key value pair" );
            Assert.AreEqual( expectedValue, toolStripComboBox.SelectedEnumValue<NotifySyncLevel>(), "Generic selected value" );
            Assert.AreEqual( expectedValue, toolStripComboBox.SelectedEnumValue( NotifySyncLevel.None ), "Generic selected value with default" );
        }

        /// <summary> Values that represent notify Synchronization levels. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        public enum NotifySyncLevel
        {
            /// <summary> . </summary>
            [System.ComponentModel.Description( "No notification" )]
            None = 0,
            /// <summary> An enum constant representing the sync] option. </summary>
            [System.ComponentModel.Description( "Synchronize" )]
            Sync,
            /// <summary> An enum constant representing the async] option. </summary>
            [System.ComponentModel.Description( "A-Synchronize" )]
            Async
        }

        #endregion

    }
}
