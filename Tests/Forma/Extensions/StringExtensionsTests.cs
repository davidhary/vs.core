using System;

using isr.Core.WinForms.CompactExtensions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.FormaTests
{

    /// <summary> String extensions tests. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-03-14 </para>
    /// </remarks>
    [TestClass()]
    public class StringExtensionsTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Forma.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( FormaTests.StringExtensionsTests.TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( FormaTests.TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );

            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion


        #region " COMPACT TESTS "

        /// <summary> (Unit Test Method) tests compact string. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod]
        public void CompactStringTest()
        {
            string candidate = "Compact this string";
            string compactedValue = string.Empty;
            string expcetedValue;
            using ( var textBox = new System.Windows.Forms.TextBox() { Width = 100, Font = new System.Drawing.Font( System.Drawing.FontFamily.GenericSansSerif, 10f ), AutoSize = false } )
            {
                compactedValue = candidate.Compact( textBox, System.Windows.Forms.TextFormatFlags.EndEllipsis );
                expcetedValue = "Compact thi...";
                Assert.AreEqual( expcetedValue, compactedValue, $"Failed compacting in a text box with {System.Windows.Forms.TextFormatFlags.EndEllipsis}" );
                compactedValue = candidate.Compact( textBox, System.Windows.Forms.TextFormatFlags.PathEllipsis );
                expcetedValue = "pact t...his str";
                Assert.AreEqual( expcetedValue, compactedValue, $"Failed compacting in a text box with {System.Windows.Forms.TextFormatFlags.PathEllipsis}" );
                compactedValue = candidate.Compact( textBox, System.Windows.Forms.TextFormatFlags.WordEllipsis );
                expcetedValue = "...ct this string";
                Assert.AreEqual( expcetedValue, compactedValue, $"Failed compacting in a text box with {System.Windows.Forms.TextFormatFlags.WordEllipsis}" );
            }

            using ( var label = new System.Windows.Forms.ToolStripStatusLabel() { Width = 100, Font = new System.Drawing.Font( System.Drawing.FontFamily.GenericSansSerif, 10f ), AutoSize = false } )
            {
                compactedValue = candidate.Compact( label, System.Windows.Forms.TextFormatFlags.PathEllipsis );
            }

            expcetedValue = "pact t...his str";
            Assert.AreEqual( expcetedValue, compactedValue, $"Failed compacting in a tool strip label with {System.Windows.Forms.TextFormatFlags.PathEllipsis}" );
        }

        #endregion

    }
}
