using System;
using System.IO;
using System.Runtime.InteropServices;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.FrameworkTests
{

    /// <summary> An environment tests. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-10-16 </para>
    /// </remarks>
    [TestClass()]
    public class EnvTests
    {

        /// <summary> Query if this object is windows. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <returns> <c>true</c> if windows; otherwise <c>false</c> </returns>
        private static bool IsWindows()
        {
            return RuntimeInformation.IsOSPlatform( OSPlatform.Windows );
        }

        /// <summary> (Unit Test Method) tests is windows. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod]
        public void IsWindowsTest()
        {
            Assert.AreEqual( string.Equals( My.MyProject.Computer.Info.OSPlatform, "Win32NT", StringComparison.OrdinalIgnoreCase ), IsWindows(), $"expected {My.MyProject.Computer.Info.OSPlatform}" );
        }

        /// <summary> (Unit Test Method) tests load. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod]
        public void LoadTest()
        {
            Env.Load( "./env/.env1", new EnvLoadOptions() );
            Assert.AreEqual( "Toni", Environment.GetEnvironmentVariable( "NAME" ) );
            // unfortunately .NET removes empty env vars -- there can NEVER be an empty string env var value
            // https://msdn.microsoft.com/en-us/library/z46c489x(v=vs.110).aspx#Remarks
            Assert.AreEqual( null, Environment.GetEnvironmentVariable( "EMPTY" ) );
            Assert.AreEqual( "'", Environment.GetEnvironmentVariable( "QUOTE" ) );
            Assert.AreEqual( "https://github.com/tonerdo", Environment.GetEnvironmentVariable( "URL" ) );
            Assert.AreEqual( "user=test;password=secret", Environment.GetEnvironmentVariable( "CONNECTION" ) );
            Assert.AreEqual( "leading and trailing white space", Environment.GetEnvironmentVariable( "WHITEBOTH" ) );
            Assert.AreEqual( @"""SPECIAL STUFF---\nLONG-BASE64\ignore""slash""", Environment.GetEnvironmentVariable( "SSL_CERT" ) );
        }

        /// <summary> (Unit Test Method) tests load path. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod]
        public void LoadPathTest()
        {
            Env.Load( "./env/.env2", new EnvLoadOptions() );
            Assert.AreEqual( "127.0.0.1", Environment.GetEnvironmentVariable( "IP" ) );
            Assert.AreEqual( "8080", Environment.GetEnvironmentVariable( "PORT" ) );
            Assert.AreEqual( "example.com", Environment.GetEnvironmentVariable( "DOMAIN" ) );
            Assert.AreEqual( "some text export other text", Environment.GetEnvironmentVariable( "EMBEDEXPORT" ) );
            Assert.AreEqual( null, Environment.GetEnvironmentVariable( "COMMENTLEAD" ) );
            Assert.AreEqual( "leading white space followed by comment", Environment.GetEnvironmentVariable( "WHITELEAD" ) );
            Assert.AreEqual( "® 🚀 日本", Environment.GetEnvironmentVariable( "UNICODE" ) );
        }

        /// <summary> (Unit Test Method) tests load stream. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod]
        public void LoadStreamTest()
        {
            using ( var stream = File.OpenRead( "./env/.env1" ) )
            {
                Env.Load( stream, new EnvLoadOptions() );
            }

            Assert.AreEqual( "Toni", Environment.GetEnvironmentVariable( "NAME" ) );
            // unfortunately .NET removes empty env vars -- there can NEVER be an empty string env var value
            // https://msdn.microsoft.com/en-us/library/z46c489x(v=vs.110).aspx#Remarks
            Assert.AreEqual( null, Environment.GetEnvironmentVariable( "EMPTY" ) );
            Assert.AreEqual( "'", Environment.GetEnvironmentVariable( "QUOTE" ) );
            Assert.AreEqual( "https://github.com/tonerdo", Environment.GetEnvironmentVariable( "URL" ) );
            Assert.AreEqual( "user=test;password=secret", Environment.GetEnvironmentVariable( "CONNECTION" ) );
            Assert.AreEqual( "leading and trailing white space", Environment.GetEnvironmentVariable( "WHITEBOTH" ) );
            Assert.AreEqual( @"""SPECIAL STUFF---\nLONG-BASE64\ignore""slash""", Environment.GetEnvironmentVariable( "SSL_CERT" ) );
        }

        /// <summary> (Unit Test Method) tests load lines. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod]
        public void LoadLinesTest()
        {
            Env.Load( File.ReadAllLines( "./env/.env1" ), new EnvLoadOptions() );
            Assert.AreEqual( "Toni", Environment.GetEnvironmentVariable( "NAME" ) );
            // unfortunately .NET removes empty env vars -- there can NEVER be an empty string env var value
            // https://msdn.microsoft.com/en-us/library/z46c489x(v=vs.110).aspx#Remarks
            Assert.AreEqual( null, Environment.GetEnvironmentVariable( "EMPTY" ) );
            Assert.AreEqual( "'", Environment.GetEnvironmentVariable( "QUOTE" ) );
            Assert.AreEqual( "https://github.com/tonerdo", Environment.GetEnvironmentVariable( "URL" ) );
            Assert.AreEqual( "user=test;password=secret", Environment.GetEnvironmentVariable( "CONNECTION" ) );
            Assert.AreEqual( "leading and trailing white space", Environment.GetEnvironmentVariable( "WHITEBOTH" ) );
            Assert.AreEqual( @"""SPECIAL STUFF---\nLONG-BASE64\ignore""slash""", Environment.GetEnvironmentVariable( "SSL_CERT" ) );
        }

        /// <summary> (Unit Test Method) tests load arguments. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod]
        public void LoadArgsTest()
        {
            Env.Load( "./env/.env1", new EnvLoadOptions( trimWhiteSpace: true ) );
            Assert.AreEqual( "leading and trailing white space", Environment.GetEnvironmentVariable( "WHITEBOTH" ) );
            Env.Load( "./env/.env1", new EnvLoadOptions( false ) );
            Assert.AreEqual( "  leading and trailing white space   ", Environment.GetEnvironmentVariable( "  WHITEBOTH  " ) );
            Env.Load( "./env/.env1", new EnvLoadOptions( true, true ) );
            Assert.AreEqual( "Google", Environment.GetEnvironmentVariable( "PASSWORD" ) );
            Env.Load( "./env/.env1", new EnvLoadOptions( true, false ) );
            Assert.AreEqual( "Google#Facebook", Environment.GetEnvironmentVariable( "PASSWORD" ) );
            Env.Load( "./env/.env1", new EnvLoadOptions( true, true, true ) );
            // Assert.AreEqual("SPECIAL STUFF---" & vbLf & "LONG-BASE64\ignore""slash", Environment.GetEnvironmentVariable("SSL_CERT"))
            Assert.AreEqual( @"""SPECIAL STUFF---\nLONG-BASE64\ignore""slash""", Environment.GetEnvironmentVariable( "SSL_CERT" ) );
            Env.Load( "./env/.env1", new EnvLoadOptions( true, true, false ) );
            Assert.AreEqual( @"""SPECIAL STUFF---\nLONG-BASE64\ignore""slash""", Environment.GetEnvironmentVariable( "SSL_CERT" ) );
        }

        /// <summary> (Unit Test Method) tests load path arguments. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod]
        public void LoadPathArgsTest()
        {
            Env.Load( "./env/.env2", new EnvLoadOptions( true, true ) );
            Assert.AreEqual( "leading white space followed by comment", Environment.GetEnvironmentVariable( "WHITELEAD" ) );
            Env.Load( "./env/.env2", new EnvLoadOptions( false, true ) );
            Assert.AreEqual( "  leading white space followed by comment  ", Environment.GetEnvironmentVariable( "WHITELEAD" ) );
            Env.Load( "./env/.env2", new EnvLoadOptions( true, false ) );
            Assert.AreEqual( "leading white space followed by comment  # comment", Environment.GetEnvironmentVariable( "WHITELEAD" ) );
            Env.Load( "./env/.env2", new EnvLoadOptions( false, false ) );
            Assert.AreEqual( "  leading white space followed by comment  # comment", Environment.GetEnvironmentVariable( "WHITELEAD" ) );
            Env.Load( "./env/.env2", new EnvLoadOptions( false, false, true ) );
            Assert.AreEqual( "® 🚀 日本", Environment.GetEnvironmentVariable( "UNICODE" ) );
            Env.Load( "./env/.env2", new EnvLoadOptions( false, false, false ) );
            Assert.AreEqual( @"'\u00ae \U0001F680 日本'", Environment.GetEnvironmentVariable( "UNICODE" ) );
        }

        /// <summary> (Unit Test Method) tests load no clobber. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod]
        public void LoadNoClobberTest()
        {
            string expected = "totally the original value";
            Environment.SetEnvironmentVariable( "URL", expected );
            Env.Load( "./env/.env1", new EnvLoadOptions( false, false, false, false ) );
            Assert.AreEqual( expected, Environment.GetEnvironmentVariable( "URL" ) );
            Environment.SetEnvironmentVariable( "URL", "i'm going to be overwritten" );
            Env.Load( "./env/.env1", new EnvLoadOptions( false, false, false, true ) );
            Assert.AreEqual( "https://github.com/tonerdo", Environment.GetEnvironmentVariable( "URL" ) );
        }

        /// <summary> (Unit Test Method) tests load no require environment. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod]
        public void LoadNoRequireEnvTest()
        {
            string expected = "totally the original value";
            Environment.SetEnvironmentVariable( "URL", expected );
            // this env file Does Not Exist
            Env.Load( "./env/.envDNE", new EnvLoadOptions( requireEnvFile: false ) );
            Assert.AreEqual( expected, Environment.GetEnvironmentVariable( "URL" ) );
            // it didn't throw an exception and crash for a missing file
            _ = MyAssertProperty.MyAssert.Throws<FileNotFoundException>( () => Env.Load( "./.envDNE", new EnvLoadOptions( requireEnvFile: true ) ) );
        }

        /// <summary> (Unit Test Method) tests load operating system casing. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod]
        public void LoadOsCasingTest()
        {
            Environment.SetEnvironmentVariable( "CASING", "neither" );
            Env.Load( "./env/.env3", new EnvLoadOptions( clobberExistingValues: false ) );
            Assert.AreEqual( IsWindows() ? "neither" : "lower", Environment.GetEnvironmentVariable( "casing" ) );
            Assert.AreEqual( "neither", Environment.GetEnvironmentVariable( "CASING" ) );
            Env.Load( "./env/.env3", new EnvLoadOptions( clobberExistingValues: true ) );
            Assert.AreEqual( "lower", Environment.GetEnvironmentVariable( "casing" ) );
            Assert.AreEqual( IsWindows() ? "lower" : "neither", Environment.GetEnvironmentVariable( "CASING" ) );
            Environment.SetEnvironmentVariable( "CASING", null );
            Environment.SetEnvironmentVariable( "casing", "neither" );
            Env.Load( "./env/.env3", new EnvLoadOptions( clobberExistingValues: false ) );
            Assert.AreEqual( "neither", Environment.GetEnvironmentVariable( "casing" ) );
            Assert.AreEqual( IsWindows() ? "neither" : null, Environment.GetEnvironmentVariable( "CASING" ) );
            Env.Load( "./env/.env3", new EnvLoadOptions( clobberExistingValues: true ) );
            Assert.AreEqual( "lower", Environment.GetEnvironmentVariable( "casing" ) );
            Assert.AreEqual( IsWindows() ? "lower" : null, Environment.GetEnvironmentVariable( "CASING" ) );
        }
    }
}
