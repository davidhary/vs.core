using System;
using System.Diagnostics;

using isr.Core.TimeSpanExtensions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.FrameworkTests
{

    /// <summary> Tests for time span calculations. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-03-14 </para>
    /// </remarks>
    [TestClass()]
    public class ElapsedTimeTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                _TestSite = new TestSite();
                _TestSite.AddTraceMessagesQueue( _TestSite.TraceMessagesQueueListener );
                _TestSite.AddTraceMessagesQueue( isr.Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                _TestSite.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( _TestSite is object )
            {
                _TestSite.Dispose();
                _TestSite = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( isr.Core.FrameworkTests.TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> The test site. </summary>
        private static TestSite _TestSite;

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo
        {
            get {
                if ( _TestSite is null )
                {
                    _TestSite = new TestSite();
                    _TestSite.AddTraceMessagesQueue( _TestSite.TraceMessagesQueueListener );
                    _TestSite.AddTraceMessagesQueue( isr.Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                }

                return _TestSite;
            }
        }

        #endregion

        #region " ELAPSED TIME TESTS "

        /// <summary> Measure sleep time. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="sleepTime">       The sleep time. </param>
        /// <param name="firstStopWatch">  The first stop watch. </param>
        /// <param name="secondStopwatch"> The second stopwatch. </param>
        /// <returns>
        /// A (FirstStartTime As DateTimeOffset, FirstEndTime As DateTimeOffset, SecondStartTime As DateTimeOffset,
        /// SecondEndTime As DateTimeOffset)
        /// </returns>
        private static (DateTimeOffset FirstStartTime, DateTimeOffset FirstEndTime, DateTimeOffset SecondStartTime, DateTimeOffset SecondEndTime)
            MeasureSleepTime( TimeSpan sleepTime, Stopwatch firstStopWatch, Stopwatch secondStopwatch )
        {
            var firstStartTime = DateTimeOffset.Now;
            var secondStartTime = DateTimeOffset.Now;
            firstStopWatch.Restart();
            secondStopwatch.Restart();
            System.Threading.Thread.Sleep( sleepTime );
            secondStopwatch.Stop();
            firstStopWatch.Stop();
            var secondEndTime = DateTimeOffset.Now;
            var firstEndTime = DateTimeOffset.Now;
            return (firstStartTime, firstEndTime, secondStartTime, secondEndTime);
        }

        /// <summary> Measure do events time. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="doEventsCount">   Number of do events. </param>
        /// <param name="firstStopWatch">  The first stop watch. </param>
        /// <param name="secondStopwatch"> The second stopwatch. </param>
        /// <returns>
        /// A (FirstStartTime As DateTimeOffset, FirstEndTime As DateTimeOffset, SecondStartTime As DateTimeOffset,
        /// SecondEndTime As DateTimeOffset)
        /// </returns>
        private static (DateTimeOffset FirstStartTime, DateTimeOffset FirstEndTime, DateTimeOffset SecondStartTime, DateTimeOffset SecondEndTime)
            MeasureDoEventsTime( int doEventsCount, Stopwatch firstStopWatch, Stopwatch secondStopwatch )
        {
            var firstStartTime = DateTimeOffset.Now;
            var secondStartTime = DateTimeOffset.Now;
            firstStopWatch.Restart();
            secondStopwatch.Restart();
            if ( doEventsCount == 1 )
            {
                ApplianceBase.DoEvents();
            }
            else
            {
                for ( int i = 1, loopTo = doEventsCount; i <= loopTo; i++ )
                {
                    ApplianceBase.DoEvents();
                }
            }

            secondStopwatch.Stop();
            firstStopWatch.Stop();
            var secondEndTime = DateTimeOffset.Now;
            var firstEndTime = DateTimeOffset.Now;
            return (firstStartTime, firstEndTime, secondStartTime, secondEndTime);
        }

        /// <summary> Measure spin wait time. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="spinWaitCount">   Number of spin waits. </param>
        /// <param name="firstStopWatch">  The first stop watch. </param>
        /// <param name="secondStopwatch"> The second stopwatch. </param>
        /// <returns>
        /// A (FirstStartTime As DateTimeOffset, FirstEndTime As DateTimeOffset, SecondStartTime As DateTimeOffset,
        /// SecondEndTime As DateTimeOffset)
        /// </returns>
        private static (DateTimeOffset FirstStartTime, DateTimeOffset FirstEndTime, DateTimeOffset SecondStartTime, DateTimeOffset SecondEndTime)
            MeasureSpinWaitTime( int spinWaitCount, Stopwatch firstStopWatch, Stopwatch secondStopwatch )
        {
            var firstStartTime = DateTimeOffset.Now;
            var secondStartTime = DateTimeOffset.Now;
            firstStopWatch.Restart();
            secondStopwatch.Restart();
            System.Threading.Thread.SpinWait( spinWaitCount );
            secondStopwatch.Stop();
            firstStopWatch.Stop();
            var secondEndTime = DateTimeOffset.Now;
            var firstEndTime = DateTimeOffset.Now;
            return (firstStartTime, firstEndTime, secondStartTime, secondEndTime);
        }

        /// <summary> Measure thread yield wait time. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="firstStopWatch">  The first stop watch. </param>
        /// <param name="secondStopwatch"> The second stopwatch. </param>
        /// <returns>
        /// A (FirstStartTime As DateTimeOffset, FirstEndTime As DateTimeOffset, SecondStartTime As DateTimeOffset,
        /// SecondEndTime As DateTimeOffset)
        /// </returns>
        private static (DateTimeOffset FirstStartTime, DateTimeOffset FirstEndTime, DateTimeOffset SecondStartTime, DateTimeOffset SecondEndTime)
            MeasureThreadYieldWaitTime( Stopwatch firstStopWatch, Stopwatch secondStopwatch )
        {
            var firstStartTime = DateTimeOffset.Now;
            var secondStartTime = DateTimeOffset.Now;
            firstStopWatch.Restart();
            secondStopwatch.Restart();
            _ = System.Threading.Thread.Yield();
            secondStopwatch.Stop();
            firstStopWatch.Stop();
            var secondEndTime = DateTimeOffset.Now;
            var firstEndTime = DateTimeOffset.Now;
            return (firstStartTime, firstEndTime, secondStartTime, secondEndTime);
        }

        /// <summary> Assert stopwatch measurements. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="name">            The name. </param>
        /// <param name="firstStopwatch">  The first stopwatch. </param>
        /// <param name="secondStopwatch"> The second stopwatch. </param>
        private static void AssertStopwatchMeasurements( string name, Stopwatch firstStopwatch, Stopwatch secondStopwatch )
        {
            Assert.IsTrue( firstStopwatch.ElapsedTicks > 0L, $"{name}: Ticks of second stopwatch {secondStopwatch.ElapsedTicks} should be greater than zero" );
            Assert.IsTrue( secondStopwatch.ElapsedTicks > 0L, $"{name}: Ticks of second stopwatch {secondStopwatch.ElapsedTicks} should be greater than zero" );
            Assert.AreEqual( firstStopwatch.ElapsedTicks, TimeSpan.FromTicks( firstStopwatch.ElapsedTicks ).Ticks, $"{name}: First stopwatch time span from ticks to ticks should match" );
            Assert.AreEqual( secondStopwatch.ElapsedTicks, TimeSpan.FromTicks( secondStopwatch.ElapsedTicks ).Ticks, $"{name}: stopwatch time span from ticks to ticks should match" );
            Assert.IsTrue( secondStopwatch.ElapsedTicks <= firstStopwatch.ElapsedTicks, $"{name}: Ticks of second stopwatch {secondStopwatch.ElapsedTicks} should be <= first stop watch {firstStopwatch.ElapsedTicks}" );
            Assert.AreEqual( firstStopwatch.ElapsedTicks, firstStopwatch.Elapsed.Ticks, $"{name}: First stopwatch elapsed ticks should match elapsed time" );
            Assert.AreEqual( secondStopwatch.ElapsedTicks, secondStopwatch.Elapsed.Ticks, $"{name}: Second stopwatch elapsed ticks should match elapsed time" );
            Assert.AreEqual( firstStopwatch.ElapsedTicks - secondStopwatch.ElapsedTicks, firstStopwatch.Elapsed.Subtract( secondStopwatch.Elapsed ).Ticks, $"{name}: Tick difference must match timespan difference in ticks" );
        }

        /// <summary> Asserts date time measurements. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="name">    The name. </param>
        /// <param name="outcome"> The outcome. </param>
        private static void AssertDateTimeMeasurements( string name, (DateTimeOffset FirstStartTime, DateTimeOffset FirstEndTime,
                                                                      DateTimeOffset SecondStartTime, DateTimeOffset SecondEndTime) outcome )
        {
            if ( string.IsNullOrEmpty( name ) )
            {
                _ = outcome.FirstEndTime.AddTicks( 0L );
            }
        }

        /// <summary> (Unit Test Method) tests date time resolution. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void DateTimeResolutionTest()
        {
            var startTime = DateTimeOffset.Now;
            var endTime = startTime.Add( TimeSpan.FromTicks( 1L ) );
            Assert.AreEqual( 1L, endTime.Ticks - startTime.Ticks, $"Adding one tick timespan should add one tick to the date time" );
            Assert.AreEqual( TimeSpan.FromTicks( 1L ), (1.0d / Stopwatch.Frequency).FromSeconds(), $"Time span tick resolution should match the stopwatch frequency {1.0d / Stopwatch.Frequency}" );
        }

        /// <summary> (Unit Test Method) tests measure elapsed time. </summary>
        /// <remarks>
        /// This measurement asserts only the stop watch measurements because of the following:<para>
        /// https://stackoverflow.com/questions/307582/how-frequent-is-datetime-now-updated-or-is-there-a-more-precise-api-to-get-the
        /// </para><para>
        /// The problem with DateTime when dealing with milliseconds isn't due to the DateTime class at
        /// all, but rather, has to do with CPU ticks and thread slices. Essentially, when an operation
        /// is paused by the scheduler to allow other threads to execute, it must wait at a minimum of 1
        /// time slice before resuming which is around 15ms on modern Windows OSes. Therefore, any
        /// attempt to pause for less than this 15ms precision will lead to unexpected results.</para>
        /// </remarks>
        [TestMethod()]
        public void MeasureElapsedTimeTest()
        {
            var firstStopwatch = new Stopwatch();
            var secondStopwatch = new Stopwatch();
            (DateTimeOffset FirstStartTime, DateTimeOffset FirstEndTime, DateTimeOffset SecondStartTime, DateTimeOffset SecondEndTime) outcome;
            var sleepTimespan = TimeSpan.FromMilliseconds( 1d );

            outcome = MeasureSleepTime( sleepTimespan, firstStopwatch, secondStopwatch );
            AssertStopwatchMeasurements( $"Sleep {sleepTimespan.ToMilliseconds()}", firstStopwatch, secondStopwatch );
            AssertDateTimeMeasurements( $"Sleep {sleepTimespan.ToMilliseconds()}", outcome );
            int doEventsCount = 1;
            _ = MeasureDoEventsTime( doEventsCount, firstStopwatch, secondStopwatch );
            // Do Events 1 first stopwatch elapsed ticks 723877
            // Do Events 1 second stopwatch elapsed ticks 723876
            TestInfo.VerboseMessage( $"Do Events {doEventsCount} first stopwatch elapsed ticks {firstStopwatch.ElapsedTicks}" );
            TestInfo.VerboseMessage( $"Do Events {doEventsCount} second stopwatch elapsed ticks {secondStopwatch.ElapsedTicks}" );

            outcome = MeasureDoEventsTime( doEventsCount, firstStopwatch, secondStopwatch );
            // Do Events 1 first stopwatch elapsed ticks 2621
            // Do Events 1 first stopwatch elapsed ticks 2621
            // Do Events 1 second stopwatch elapsed ticks 2619
            TestInfo.VerboseMessage( $"Do Events {doEventsCount} first stopwatch elapsed ticks {firstStopwatch.ElapsedTicks}" );
            TestInfo.VerboseMessage( $"Do Events {doEventsCount} second stopwatch elapsed ticks {secondStopwatch.ElapsedTicks}" );
            AssertStopwatchMeasurements( $"Do Events {doEventsCount}", firstStopwatch, secondStopwatch );
            AssertDateTimeMeasurements( $"Do Events {doEventsCount}", outcome );

            outcome = MeasureThreadYieldWaitTime( firstStopwatch, secondStopwatch );
            AssertStopwatchMeasurements( $"Thread Yield", firstStopwatch, secondStopwatch );
            // Thread Yield first stopwatch elapsed ticks 57, 32
            // Thread Yield second stopwatch elapsed ticks 55, 31
            TestInfo.VerboseMessage( $"Thread Yield first stopwatch elapsed ticks {firstStopwatch.ElapsedTicks}" );
            TestInfo.VerboseMessage( $"Thread Yield second stopwatch elapsed ticks {secondStopwatch.ElapsedTicks}" );
            AssertDateTimeMeasurements( $"Thread Yield", outcome );
            int spinWaitCount = 10;

            outcome = MeasureSpinWaitTime( spinWaitCount, firstStopwatch, secondStopwatch );
            AssertStopwatchMeasurements( $"Spin wait {spinWaitCount}", firstStopwatch, secondStopwatch );
            // Spin wait 10 first stopwatch elapsed ticks 7 = 0.7 micro seconds
            // Spin wait 10 second stopwatch elapsed ticks 6
            TestInfo.VerboseMessage( $"Spin wait {spinWaitCount} first stopwatch elapsed ticks {firstStopwatch.ElapsedTicks}" );
            TestInfo.VerboseMessage( $"Spin wait {spinWaitCount} second stopwatch elapsed ticks {secondStopwatch.ElapsedTicks}" );
            AssertDateTimeMeasurements( $"Spin wait {spinWaitCount}", outcome );
        }

        #endregion

    }
}
