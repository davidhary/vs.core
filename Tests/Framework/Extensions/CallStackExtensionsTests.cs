using System;
using System.Diagnostics;

using isr.Core.StackTraceExtensions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.FrameworkTests
{

    /// <summary>
    /// This is a test class for ExtensionsTest and is intended to contain all ExtensionsTest Unit
    /// Tests.
    /// </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestClass()]
    public class CallStackExtensionsTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                _TestSite = new TestSite();
                _TestSite.AddTraceMessagesQueue( _TestSite.TraceMessagesQueueListener );
                _TestSite.AddTraceMessagesQueue( isr.Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                _TestSite.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( _TestSite is object )
            {
                _TestSite.Dispose();
                _TestSite = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( isr.Core.FrameworkTests.TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> The test site. </summary>
        private static TestSite _TestSite;

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo
        {
            get {
                if ( _TestSite is null )
                {
                    _TestSite = new TestSite();
                    _TestSite.AddTraceMessagesQueue( _TestSite.TraceMessagesQueueListener );
                    _TestSite.AddTraceMessagesQueue( isr.Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                }

                return _TestSite;
            }
        }

        #endregion

        #region " CALL STACK EXTENSION TESTS "

        /// <summary> A test for ParseCallStack for user. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void ParseUserCallStackFrameTest()
        {
            var trace = new StackTrace( true );
            var frame = new StackFrame( true );
            var callStackType = CallStackType.UserCallStack;
            string expected = "Tests.ParseUserCallStackFrameTest() in ";
            string actual = trace.ParseCallStack( frame, callStackType ).Trim();
            Assert.IsTrue( actual.Contains( expected ), $@"Actual trace 
{actual}
contains 
{expected}" );
        }

        /// <summary> A test for ParseCallStack for user. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void ParseFullCallStackFrameTest()
        {
            var trace = new StackTrace( true );
            var frame = new StackFrame( true );
            var callStackType = CallStackType.FullCallStack;
            string expected = string.Empty;
            string actual;
            actual = trace.ParseCallStack( frame, callStackType );
            Assert.AreNotEqual( expected, actual );
        }

        /// <summary> A test for ParseCallStack. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void ParseUserCallStackTest()
        {
            var trace = new StackTrace( true );
            int skipLines = 0;
            int totalLines = 0;
            var callStackType = CallStackType.UserCallStack;
            string expected = "s->   at isr.";
            string actual = trace.ParseCallStack( skipLines, totalLines, callStackType ).Trim();
            Assert.IsTrue( actual.StartsWith( expected, StringComparison.OrdinalIgnoreCase ), $"Trace results Actual: {actual.Substring( 0, expected.Length )} Starts with {expected}" );
        }

        /// <summary> A test for ParseCallStack. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void ParseFullCallStackTest()
        {
            var trace = new StackTrace( true );
            int skipLines = 0;
            int totalLines = 0;
            var callStackType = CallStackType.FullCallStack;
            string expected = string.Empty;
            string actual;
            actual = trace.ParseCallStack( skipLines, totalLines, callStackType );
            Assert.AreNotEqual( expected, actual );
        }
        #endregion

    }
}
