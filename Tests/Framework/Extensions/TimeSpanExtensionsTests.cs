using System;
using System.Diagnostics;

using isr.Core.TimeSpanExtensions;
using isr.Core.DateTimeExtensions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.FrameworkTests
{

    /// <summary> Time Span extensions tests. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-03-14 </para>
    /// </remarks>
    [TestClass()]
    public class TimeSpanExtensionsTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                _TestSite = new TestSite();
                _TestSite.AddTraceMessagesQueue( _TestSite.TraceMessagesQueueListener );
                _TestSite.AddTraceMessagesQueue( isr.Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                _TestSite.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( _TestSite is object )
            {
                _TestSite.Dispose();
                _TestSite = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( isr.Core.FrameworkTests.TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> The test site. </summary>
        private static TestSite _TestSite;

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo
        {
            get {
                if ( _TestSite is null )
                {
                    _TestSite = new TestSite();
                    _TestSite.AddTraceMessagesQueue( _TestSite.TraceMessagesQueueListener );
                    _TestSite.AddTraceMessagesQueue( isr.Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                }

                return _TestSite;
            }
        }

        #endregion

        #region " WAIT"

        [TestMethod()]
        public void TimeSpanWaitTest()
        {
            TimeSpan expectedTimespan;
            TimeSpan maximumError;
            TimeSpan waitTimespan;
            TimeSpan actualTimespan;
            Double[] testIntervals = new double[] { 1, 2, 5, 10, 20, 50, 100 };

            // initialize the dispatcher for do event functions
            TimeSpan.FromMilliseconds( 1 ).DoEventsWait();

            Stopwatch sw = Stopwatch.StartNew();
            foreach ( Double testInterval in testIntervals )
            {

                waitTimespan = TimeSpan.FromMilliseconds( testInterval );
                maximumError = waitTimespan.ExpectedWaitClockResolution();
                expectedTimespan = waitTimespan;
                sw.Restart();
                waitTimespan.SpinWait();
                sw.Stop();
                actualTimespan = sw.Elapsed;
                Asserts.Instance.AreEqual( expectedTimespan, actualTimespan, maximumError,
                    $"Spin Wait time interval should equal within error {maximumError}" );
                sw.Restart();
                waitTimespan.DoEventsWait();
                sw.Stop();
                actualTimespan = sw.Elapsed;
                Asserts.Instance.AreEqual( expectedTimespan, actualTimespan, maximumError,
                    $"Do Events Wait time interval should equal within error {maximumError}" );
            }

        }

        #endregion

        #region " PRECISE DATE TIME "

        /// <summary>   (Unit Test Method) tests date time resolution. </summary>
        /// <remarks>   David, 2020-12-07. </remarks>
        [TestMethod()]
        public void DateTimeResolutionTest()
        {
            string output = "";
            double sum = 0;
            double resolution;
            long ticks;
            long newTicks;
            int count = 20;
            for ( int ctr = 0; ctr <= count; ctr++ )
            {
                output += String.Format( $"{DateTime.Now.Ticks}\n" );

                // Introduce a delay loop.
                ticks = DateTime.UtcNow.Ticks;
                do
                { newTicks = DateTime.UtcNow.Ticks; }
                while ( ticks == newTicks );
                resolution = 0.0001 * (newTicks - ticks);
                sum += resolution;
                output += String.Format( $"Millisecond resolution = {resolution}\n" );

                if ( ctr == 10 )
                {
                    output += "Thread.Sleep called...\n";
                    System.Threading.Thread.Sleep( 5 );
                }

            }
            resolution = sum / count;
            output += String.Format( $"Average resolution = {resolution}\n" );
            Console.WriteLine( output );
            double expectedResolution = 1;
            double resolutionEpsilon = 0.1;
            Assert.AreEqual( expectedResolution, resolution, resolutionEpsilon, $"Date time resolution {resolution} ms should match within {resolutionEpsilon} ms" );
        }

        /// <summary>   (Unit Test Method) tests date time offset file time resolution. </summary>
        /// <remarks>   David, 2020-12-07. </remarks>
        [TestMethod()]
        public void DateTimeOffsetFileTimeResolutionTest()
        {
            string output = "";
            double sum = 0;
            double resolution;
            long ticks;
            long newTicks;
            int count = 20;
            for ( int ctr = 0; ctr <= count; ctr++ )
            {
                output += String.Format( $"{DateTimeOffset.UtcNow.ToFileTime()}\n" );

                // Introduce a delay loop.
                ticks = DateTimeOffset.UtcNow.ToFileTime();
                do
                { newTicks = DateTimeOffset.UtcNow.ToFileTime(); }
                while ( ticks == newTicks );
                resolution = 0.0001 * (newTicks - ticks);
                sum += resolution;
                output += String.Format( $"Millisecond resolution = {resolution}\n" );

                if ( ctr == 10 )
                {
                    output += "Thread.Sleep called...\n";
                    System.Threading.Thread.Sleep( 5 );
                }

            }
            resolution = sum / count;
            output += String.Format( $"Average resolution = {resolution}\n" );
            Console.WriteLine( output );
            double expectedResolution = 1;
            double resolutionEpsilon = 0.1;
            Assert.AreEqual( expectedResolution, resolution, resolutionEpsilon, $"Date time resolution {resolution} ms should match within {resolutionEpsilon} ms" );
        }

        /// <summary>   (Unit Test Method) tests high resolution date time resolution. </summary>
        /// <remarks>   David, 2020-12-07. </remarks>
        [TestMethod()]
        public void HighResolutionDateTimeResolutionTest()
        {
            string output = "";
            double sum = 0;
            double resolution;
            double minimumResolution = double.MaxValue;
            long ticks;
            long newTicks;
            int count = 40;
            for ( int ctr = 0; ctr <= count; ctr++ )
            {
                output += String.Format( $"{Win32.HighResolutionDateTime.UtcNow.ToFileTime()}\n" );

                // Introduce a delay loop.
                ticks = Win32.HighResolutionDateTime.UtcNow.ToFileTime();
                do
                { newTicks = Win32.HighResolutionDateTime.UtcNow.ToFileTime(); }
                while ( ticks == newTicks );
                resolution = 0.0001 * (newTicks - ticks);
                minimumResolution = resolution < minimumResolution ? resolution : minimumResolution;
                sum += resolution;
                output += String.Format( $"Millisecond resolution = {resolution}\n" );

                if ( ctr == 10 )
                {
                    output += "Thread.Sleep called...\n";
                    System.Threading.Thread.Sleep( 5 );
                }

            }
            // resolution = sum / count;
            resolution = minimumResolution;
            output += String.Format( $"Average resolution = {resolution}\n" );
            output += String.Format( $"Minimum resolution = {minimumResolution}\n" );
            Console.WriteLine( output );
            double expectedResolution = 4 * isr.Core.TimeSpanExtensions.TimeSpanExtensionMethods.MillisecondsPerTick;
            double resolutionEpsilon = expectedResolution;
            Assert.AreEqual( expectedResolution, resolution, resolutionEpsilon, $"Date time resolution {resolution} ms should match within {resolutionEpsilon} ms" );
        }

        /// <summary>   (Unit Test Method) tests exact date time. </summary>
        /// <remarks>   David, 2020-12-07. </remarks>
        [TestMethod()]
        public void ExactDateTimeTest()
        {
            double milliseconds = 0.001;
            TimeSpan expectedTimespan = TimeSpan.FromMilliseconds( milliseconds );
            TimeSpan actualTimespan = TimeSpan.Zero;
            // the C# extensions do not add fractions of milliseconds
            Assert.AreEqual( expectedTimespan, actualTimespan, $"{actualTimespan:s\\.fffffff} should equal {expectedTimespan:s\\.fffffff}" );

            double micorseconds = 1000 * milliseconds;
            expectedTimespan = TimeSpan.Zero.AddMicroseconds( micorseconds );
            DateTime utcNow = DateTime.UtcNow;
            DateTime exactUtcNow = utcNow.AddMicroseconds( micorseconds );
            actualTimespan = exactUtcNow.Subtract( utcNow );
            Assert.AreEqual( expectedTimespan, actualTimespan, $"{actualTimespan:s\\.fffffff} should equal {expectedTimespan:s\\.fffffff} after adding {micorseconds} microseconds" );

        }

        #endregion

        #region " TIMESPAN TIME "

        /// <summary> (Unit Test Method) tests exact time span. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void ExactTimeSpanTest()
        {
            TimeSpan expectedTimespan;
            TimeSpan actualTimespan;
            double expectedMilliseconds;
            double actualMilliseconds;
            double expectedMicroseconds;
            double actualMicroseconds;
            double expectedSeconds;
            double actualSeconds;
            long ticksEpsilon = 2L;
            long stepSize = 7L;
            long ticks = 1L;
            while ( ticks < 100000L )
            {
                expectedSeconds = ticks * TimeSpanExtensionMethods.SecondsPerTick;
                expectedTimespan = TimeSpan.FromTicks( ticks );
                actualTimespan = expectedSeconds.FromSeconds();
                Asserts.Instance.AreEqual( expectedTimespan, actualTimespan, TimeSpan.FromTicks( 1L ), $"Expected timespan from ticks should equal precise time span from seconds within {TimeSpan.FromTicks( 1L )}" );
                Assert.AreEqual( ticks, actualTimespan.Ticks, ticksEpsilon, $"Ticks of timespan converted to Seconds should match withing {ticksEpsilon} tick(s)" );
                Assert.AreEqual( expectedSeconds, actualTimespan.ToSeconds(), 2d * TimeSpanExtensionMethods.SecondsPerTick, $"Timespan converted to seconds should match" );
                actualSeconds = actualTimespan.ToSeconds();
                Assert.AreEqual( expectedSeconds, actualSeconds, 2d * TimeSpanExtensionMethods.SecondsPerTick, $"Timespan seconds should match within {2d * TimeSpanExtensionMethods.SecondsPerTick}s" );
                expectedMilliseconds = ticks * TimeSpanExtensionMethods.MillisecondsPerTick;
                expectedTimespan = TimeSpan.FromTicks( ticks );
                actualTimespan = expectedMilliseconds.FromMilliseconds();
                Asserts.Instance.AreEqual( expectedTimespan, actualTimespan, TimeSpan.FromTicks( 1L ), $"Expected timespan from ticks should equal precise time span from milliseconds within {TimeSpan.FromTicks( 1L )}" );
                Assert.AreEqual( ticks, actualTimespan.Ticks, ticksEpsilon, $"Ticks of timespan converted to milliseconds should match withing {ticksEpsilon} tick(s)" );
                Assert.AreEqual( 0.001d * expectedMilliseconds, actualTimespan.ToSeconds(), TimeSpanExtensionMethods.SecondsPerTick, $"Timespan converted to seconds should match" );
                actualMilliseconds = actualTimespan.ToMilliseconds();
                Assert.AreEqual( expectedMilliseconds, actualMilliseconds, TimeSpanExtensionMethods.MillisecondsPerTick, $"Timespan milliseconds should match within {TimeSpanExtensionMethods.MillisecondsPerTick}ms" );
                expectedMicroseconds = ticks * TimeSpanExtensionMethods.MicrosecondsPerTick;
                expectedTimespan = TimeSpan.FromTicks( ticks );
                actualTimespan = expectedMicroseconds.FromMicroseconds();
                Asserts.Instance.AreEqual( expectedTimespan, actualTimespan, TimeSpan.FromTicks( 1L ), $"Expected timespan from ticks should equal precise time span from Microseconds within {TimeSpan.FromTicks( 1L )}" );
                Assert.AreEqual( ticks, actualTimespan.Ticks, ticksEpsilon, $"Ticks of timespan converted to microseconds should match withing {ticksEpsilon} tick(s)" );
                Assert.AreEqual( 0.000001d * expectedMicroseconds, actualTimespan.ToSeconds(), TimeSpanExtensionMethods.SecondsPerTick, $"Timespan converted to seconds should match" );
                actualMicroseconds = actualTimespan.ToMicroseconds();
                Assert.AreEqual( expectedMicroseconds, actualMicroseconds, TimeSpanExtensionMethods.MicrosecondsPerTick, $"Timespan Microseconds should match within {TimeSpanExtensionMethods.MicrosecondsPerTick}µs" );
                expectedTimespan = actualMilliseconds.FromMilliseconds();
                actualTimespan = (1000d * actualMilliseconds).FromMicroseconds();
                Asserts.Instance.AreEqual( expectedTimespan, actualTimespan, TimeSpan.FromTicks( 2L ), $"Time spans converted from Milliseconds and Microseconds should match within {TimeSpan.FromTicks( 2L )}" );
                expectedTimespan = actualMilliseconds.FromMilliseconds();
                actualTimespan = (0.001d * actualMilliseconds).FromSeconds();
                Asserts.Instance.AreEqual( expectedTimespan, actualTimespan, TimeSpan.FromTicks( 2L ), $"Time spans converted from Milliseconds and seconds should match within {TimeSpan.FromTicks( 2L )}" );

                // Test timespan subtraction and additions
                expectedTimespan = (2d * actualMilliseconds).FromMilliseconds();
                actualTimespan = actualTimespan.Add( actualTimespan );
                Asserts.Instance.AreEqual( expectedTimespan, actualTimespan, TimeSpan.FromTicks( 2L ), $"Added time spans converted from Milliseconds should match within {TimeSpan.FromTicks( 2L )}" );
                expectedTimespan = actualMilliseconds.FromMilliseconds();
                actualTimespan = actualTimespan.Subtract( expectedTimespan );
                Asserts.Instance.AreEqual( expectedTimespan, actualTimespan, TimeSpan.FromTicks( 2L ), $"Subtracted time spans converted from Milliseconds should match within {TimeSpan.FromTicks( 2L )}" );
                expectedTimespan = TimeSpan.Zero;
                actualTimespan = actualTimespan.Subtract( actualTimespan );
                Asserts.Instance.AreEqual( expectedTimespan, actualTimespan, TimeSpan.Zero, $"Self subtracted time spans should be {TimeSpan.Zero}" );
                switch ( ticks )
                {
                    case var @case when @case < 100L:
                        {
                            stepSize = 7L;
                            break;
                        }

                    case var case1 when case1 < 1000L:
                        {
                            stepSize = 71L;
                            break;
                        }

                    case var case2 when case2 < 10000L:
                        {
                            stepSize = 711L;
                            break;
                        }

                    case var case3 when case3 < 100000L:
                        {
                            stepSize = 7101L;
                            break;
                        }
                }

                ticks += stepSize;
            }
        }

        #endregion

    }
}
