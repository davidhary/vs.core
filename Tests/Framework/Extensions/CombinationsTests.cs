using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.FrameworkTests
{

    /// <summary> Combinations tests. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-02-18. </para>
    /// </remarks>
    [TestClass()]
    public class CombinationsTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                _TestSite = new TestSite();
                _TestSite.AddTraceMessagesQueue( _TestSite.TraceMessagesQueueListener );
                _TestSite.AddTraceMessagesQueue( isr.Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                _TestSite.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( _TestSite is object )
            {
                _TestSite.Dispose();
                _TestSite = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( isr.Core.FrameworkTests.TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> The test site. </summary>
        private static TestSite _TestSite;

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo
        {
            get {
                if ( _TestSite is null )
                {
                    _TestSite = new TestSite();
                    _TestSite.AddTraceMessagesQueue( _TestSite.TraceMessagesQueueListener );
                    _TestSite.AddTraceMessagesQueue( isr.Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                }

                return _TestSite;
            }
        }

        #endregion

        #region " COMBINATION TEST "

        /// <summary> Builds the given values. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="values"> The values. </param>
        /// <returns> A String. </returns>
        private static string Build<T>( List<T> values )
        {
            var builder = new System.Text.StringBuilder();
            foreach ( T i in values )
            {
                _ = builder.Append( $"{i,3}" );
            }

            return builder.ToString();
        }

        /// <summary> (Unit Test Method) tests combination. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void CombinationTest()
        {
            var collectionOfSeries = new List<List<int>>() { new List<int>( new[] { 1, 2, 3, 4, 5 } ), new List<int>( new[] { 0, 1 } ), new List<int>( new[] { 6, 3 } ), new List<int>( new[] { 1, 3, 5 } ) };
            var combinations = CombinationsExtensions.CombinationsExtensionMethods.GenerateCombinations( collectionOfSeries );
            var expectedList = new List<int>( new[] { 1, 0, 6, 1 } );
            var actualList = combinations.First();
            Assert.IsTrue( expectedList.SequenceEqual( actualList ) );
            expectedList = new List<int>( new[] { 1, 0, 6, 3 } );
            actualList = combinations[1];
            Assert.IsTrue( expectedList.SequenceEqual( actualList ), $"actual: [{Build( actualList )}] <> expected: [{Build( actualList )}]" );
            expectedList = new List<int>( new[] { 5, 1, 3, 5 } );
            actualList = combinations.Last();
            Assert.IsTrue( expectedList.SequenceEqual( actualList ), $"actual: [{Build( actualList )}] <> expected: [{Build( actualList )}]" );
        }

        #endregion

    }
}
