
using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.FrameworkTests
{

    /// <summary>
    /// This is a test class for KnownFoldersTest and is intended
    /// to contain all KnownFoldersTest Unit Tests
    /// </summary>
    [TestClass()]
    public class KnownFoldersTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( isr.Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.IsTrue( System.Math.Abs( isr.Core.FrameworkTests.KnownFoldersTests.TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( isr.Core.FrameworkTests.TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        /// <summary> A test for GetPath. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="knownFolder"> A known folder enum value. </param>
        /// <param name="expected">    The expected value. </param>
        public static void TestGetPath( KnownFolder knownFolder, string expected )
        {
            string actual;
            actual = KnownFolders.GetPath( knownFolder );
            Assert.AreEqual( expected, actual, true, System.Globalization.CultureInfo.CurrentCulture );
        }

        /// <summary> A test for Get Path on multiple <see cref="KnownFolder"/> folders. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void GetPathTest()
        {
            TestGetPath( KnownFolder.Documents, @$"C:\Users\{Environment.UserName}\Documents" );
            TestGetPath( KnownFolder.UserProfiles, @"C:\Users" );
            TestGetPath( KnownFolder.ProgramData, @"C:\ProgramData" );
        }

        /// <summary> A test for Get Default Path. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="knownFolder"> A known folder enum value. </param>
        /// <param name="expected">    The expected value. </param>
        public static void TestGetDefaultPath( KnownFolder knownFolder, string expected )
        {
            string actual;
            actual = KnownFolders.GetDefaultPath( knownFolder );
            Assert.AreEqual( expected, actual, true, System.Globalization.CultureInfo.CurrentCulture );
        }

        /// <summary> A test for GetDefaultPath. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void GetDefaultPathTest()
        {
            TestGetDefaultPath( KnownFolder.Documents, @$"C:\Users\{Environment.UserName}\Documents" );
            TestGetDefaultPath( KnownFolder.UserProfiles, @"C:\Users" );
            TestGetDefaultPath( KnownFolder.ProgramData, @"C:\ProgramData" );
        }
    }
}
