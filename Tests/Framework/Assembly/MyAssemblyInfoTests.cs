using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.FrameworkTests
{
    /// <summary>
    /// This is a test class for MyAssemblyInfoTest and is intended
    /// to contain all MyAssemblyInfoTest Unit Tests
    /// </summary>
    [TestClass()]
    public class MyAssemblyInfoTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        /// <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( isr.Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( isr.Core.FrameworkTests.TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// </summary>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " MY ASSEMBLY INFO TESTS "

        /// <summary> A test for Public Key Token. </summary>
        [TestMethod()]
        public void PublicKeyTokenTest()
        {
            var target = new MyAssemblyInfo( typeof( MyAssemblyInfoTests ).Assembly );
            string expected = target.Assembly.GetName().ToString().Split( ',' )[3];
            expected = expected.Substring( expected.IndexOf( "=", StringComparison.OrdinalIgnoreCase ) + 1 );
            string actual = target.PublicKeyToken;
            Assert.AreEqual( expected, actual, true, System.Globalization.CultureInfo.CurrentCulture );
        }

        /// <summary>   The build number of the assembly. </summary>
        /// <remarks>   David, 2020-11-19. </remarks>
        /// <param name="assemblyInfo"> Information describing the assembly. </param>
        /// <returns>   The build number for today. </returns>
        public static int BuildNumber( MyAssemblyInfo assemblyInfo )
        {
            return ( int ) Math.Floor( assemblyInfo.FileInfo.LastWriteTime.ToUniversalTime().Subtract( DateTime.Parse( "2000-01-01" ) ).TotalDays );
        }

        /// <summary>   The build number of the assembly. </summary>
        /// <remarks>   David, 2021-08-21. </remarks>
        /// <param name="fileName"> Filename of the file. </param>
        /// <returns>   The build number for today. </returns>
        public static int BuildNumber( string fileName )
        {
            System.IO.FileInfo fi = new( fileName );
            return ( int ) Math.Floor( fi.LastWriteTime.ToUniversalTime().Subtract( DateTime.Parse( "2000-01-01" ) ).TotalDays );
        }

        /// <summary> A test for FileVersionInfo. </summary>
        [TestMethod()]
        public void FileVersionInfoTest()
        {
            string fileName = @".\Properties\AssemblyVersion.cs";
            int expected = MyAssemblyInfoTests.BuildNumber( fileName );
            var target = new MyAssemblyInfo( typeof( MyAssemblyInfoTests ).Assembly );
            int actual = target.FileVersionInfo.FileBuildPart;
            Assert.AreEqual( expected, actual, "Expected build number" );
        }

        /// <summary> A test for ProductVersion. </summary>
        [TestMethod()]
        public void ProductVersionTest()
        {
            string fileName = @".\Properties\AssemblyVersion.cs";
            string expected = MyAssemblyInfoTests.BuildNumber( fileName ).ToString();
            var target = new MyAssemblyInfo( typeof( MyAssemblyInfoTests ).Assembly );
            string actual = target.ProductVersion( "" ).Split( '.' )[2];
            Assert.AreEqual( expected, actual, "Product version" );
        }

        #endregion

    }
}
