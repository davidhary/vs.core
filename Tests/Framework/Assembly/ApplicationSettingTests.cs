using System;
using System.Drawing.Imaging;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.FrameworkTests
{
    /// <summary>
    /// This is a test class for MyAssemblyInfoTest and is intended
    /// to contain all MyAssemblyInfoTest Unit Tests
    /// </summary>
    [TestClass()]
    public class ApplicationSettingTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        /// <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( isr.Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            Assert.IsTrue( ApplicationSettingsTestsInfo.Get().Exists, $"{nameof( ApplicationSettingsTestsInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( isr.Core.FrameworkTests.TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// </summary>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " APPLICATION SETTINGS TESTS "

        /// <summary>   (Unit Test Method) tests application settings. </summary>
        /// <remarks>   David, 2020-10-10. </remarks>
        [TestMethod()]
        public void ApplicationSettingsTest()
        {
            var target = ApplicationSettingsTestsInfo.Get();

            // String Value
            // read existing value
            Assert.AreEqual( target.StringValueExpected, target.StringValue, $"initial {nameof( ApplicationSettingsTestsInfo.StringValue )} should match" );

            // write a new value
            target.StringValue = target.StringValueDefault;
            Assert.AreEqual( target.StringValueDefault, target.StringValue, $"modified {nameof( ApplicationSettingsTestsInfo.StringValue )} should match" );

            // restore expected values
            target.StringValue = target.StringValueExpected;
            Assert.AreEqual( target.StringValueExpected, target.StringValue, $"restored {nameof( ApplicationSettingsTestsInfo.StringValue )} should match" );

            // Integer Value
            // read existing value
            Assert.AreEqual( target.IntegerValueExpected, target.IntegerValue, $"initial {nameof( ApplicationSettingsTestsInfo.IntegerValue )} should match" );

            // write a new value
            target.IntegerValue = target.IntegerValueDefault;
            Assert.AreEqual( target.IntegerValueDefault, target.IntegerValue, $"modified {nameof( ApplicationSettingsTestsInfo.IntegerValue )} should match" );

            // restore expected values
            target.IntegerValue = target.IntegerValueExpected;
            Assert.AreEqual( target.IntegerValueExpected, target.IntegerValue, $"restored {nameof( ApplicationSettingsTestsInfo.IntegerValue )} should match" );

            // Byte Value
            // read existing value
            Assert.AreEqual( target.ByteValueExpected, target.ByteValue, $"initial {nameof( ApplicationSettingsTestsInfo.ByteValue )} should match" );

            // write a new value
            target.ByteValue = target.ByteValueDefault;
            Assert.AreEqual( target.ByteValueDefault, target.ByteValue, $"modified {nameof( ApplicationSettingsTestsInfo.ByteValue )} should match" );

            // restore expected values
            target.ByteValue = target.ByteValueExpected;
            Assert.AreEqual( target.ByteValueExpected, target.ByteValue, $"restored {nameof( ApplicationSettingsTestsInfo.ByteValue )} should match" );

            // Timespan Value
            // read existing value
            Assert.AreEqual( target.TimespanValueExpected, target.TimespanValue, $"initial {nameof( ApplicationSettingsTestsInfo.TimespanValue )} should match" );

            // write a new value
            target.TimespanValue = target.TimespanValueDefault;
            Assert.AreEqual( target.TimespanValueDefault, target.TimespanValue, $"modified {nameof( ApplicationSettingsTestsInfo.TimespanValue )} should match" );

            // restore expected values
            target.TimespanValue = target.TimespanValueExpected;
            Assert.AreEqual( target.TimespanValueExpected, target.TimespanValue, $"restored {nameof( ApplicationSettingsTestsInfo.TimespanValue )} should match" );

            // DateTime Value
            // read existing value
            Assert.AreEqual( target.DateTimeValueExpected, target.DateTimeValue, $"initial {nameof( ApplicationSettingsTestsInfo.DateTimeValue )} should match" );

            // write a new value
            target.DateTimeValue = target.DateTimeValueDefault;
            Assert.AreEqual( target.DateTimeValueDefault, target.DateTimeValue, $"modified {nameof( ApplicationSettingsTestsInfo.DateTimeValue )} should match" );

            // restore expected values
            target.DateTimeValue = target.DateTimeValueExpected;
            Assert.AreEqual( target.DateTimeValueExpected, target.DateTimeValue, $"restored {nameof( ApplicationSettingsTestsInfo.DateTimeValue )} should match" );

            // TraceEventType Value
            // read existing value
            Assert.AreEqual( target.TraceEventTypeValueExpected, target.TraceEventTypeValue, $"initial {nameof( ApplicationSettingsTestsInfo.TraceEventTypeValue )} should match" );

            // write a new value
            target.TraceEventTypeValue = target.TraceEventTypeValueDefault;
            Assert.AreEqual( target.TraceEventTypeValueDefault, target.TraceEventTypeValue, $"modified {nameof( ApplicationSettingsTestsInfo.TraceEventTypeValue )} should match" );

            // restore expected values
            target.TraceEventTypeValue = target.TraceEventTypeValueExpected;
            Assert.AreEqual( target.TraceEventTypeValueExpected, target.TraceEventTypeValue, $"restored {nameof( ApplicationSettingsTestsInfo.TraceEventTypeValue )} should match" );

        }

        #endregion

    }
}
