using System;
using System.Diagnostics;

namespace isr.Core.FrameworkTests
{
    /// <summary> Binding Tests. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-02-18. </para>
    /// </remarks>
    [System.Runtime.CompilerServices.CompilerGenerated()]
    [System.CodeDom.Compiler.GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "15.9.0.0")]
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)]
    internal class ApplicationSettingsTestsInfo : ApplicationSettingsBase
    {

        #region " SINGLETON "

        /// <summary>
        /// Initializes an instance of the <see cref="T:System.Configuration.ApplicationSettingsBase" />
        /// class to its default state.
        /// </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        private ApplicationSettingsTestsInfo() : base()
        {
        }

        /// <summary>
        /// Gets the locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        /// <value> The sync locker. </value>
        private static object _SyncLocker { get; set; } = new object();

        /// <summary> Gets the instance. </summary>
        /// <value> The instance. </value>
        private static ApplicationSettingsTestsInfo _Instance { get; set; }

        /// <summary> Instantiates the class. </summary>
        /// <remarks> Use this property to instantiate a single instance of this class. This class uses
        /// lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
        /// <returns> A new or existing instance of the class. </returns>
        public static ApplicationSettingsTestsInfo Get()
        {
            if (_Instance is null)
            {
                lock (_SyncLocker)
                    _Instance = (ApplicationSettingsTestsInfo)Synchronized(new ApplicationSettingsTestsInfo());
            }

            return _Instance;
        }

        /// <summary> Returns true if an instance of the class was created and not disposed. </summary>
        /// <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
        public static bool Instantiated
        {
            get
            {
                lock (_SyncLocker)
                    return _Instance is object;
            }
        }

        #endregion

        #region " CONFIGURATION INFORMATION "

        /// <summary> Returns true if test settings exist. </summary>
        /// <value> <c>True</c> if testing settings exit. </value>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "True")]
        public bool Exists
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Returns true to output test messages at the verbose level. </summary>
        /// <value> The verbose messaging level. </value>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "False")]
        public bool Verbose
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary>
        /// Gets or sets the sentinel indicating of all data are to be used for a test.
        /// </summary>
        /// <value> <c>true</c> if all data are to be used for a test; otherwise <c>false</c>. </value>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "True")]
        public bool All
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> True if the test set is enabled. </summary>
        /// <value> The enabled option. </value>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "True")]
        public bool Enabled
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        #endregion

        #region " CONFIGURATION SETTINGS "

        /// <summary>   The String value default. </summary>
        public readonly string StringValueDefault = "A";

        public readonly string StringValueExpected = "a";

        /// <summary>   Gets or sets the String value. </summary>
        /// <value> The String value. </value>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute(  "A" )]
        public string StringValue
        {
            get {
                return AppSettingGetter( this.StringValueDefault );
            }

            set {
                AppSettingSetter( value );
            }
        }

        /// <summary>   The integer value default. </summary>
        public readonly int IntegerValueDefault = 0;

        /// <summary>   The integer value expected. </summary>
        public readonly int IntegerValueExpected = 1;

        /// <summary>   Gets or sets the integer value. </summary>
        /// <value> The integer value. </value>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "0")]
        public int IntegerValue
        {
            get
            {
                return AppSettingGetter( this.IntegerValueDefault );
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary>   True if boolean value expected. </summary>
        public readonly bool BooleanValueExpected = true;

        /// <summary>   True to boolean value default. </summary>
        public readonly bool BooleanValueDefault = false;

        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "false" )]
        public bool BooleanValue
        {
            get {
                return AppSettingGetter( this.BooleanValueDefault );
            }

            set {
                AppSettingSetter( value );
            }
        }

        /// <summary>   The byte value expected. </summary>
        public readonly byte ByteValueExpected = ( byte ) 8;

        /// <summary>   The byte value default. </summary>
        public readonly byte ByteValueDefault = ( byte ) 0;

        /// <summary> Gets or sets the 7k 2600. </summary>
        /// <value> The i 7k 2600. </value>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "0" )]
        public byte ByteValue
        {
            get {
                return AppSettingGetter( this.ByteValueDefault );
            }

            set {
                AppSettingSetter( value );
            }
        }

        /// <summary>   The timespan value expected. </summary>
        public readonly TimeSpan TimespanValueExpected = TimeSpan.Parse( "00:00:02.8250000" );

        public readonly TimeSpan TimespanValueDefault = TimeSpan.Zero;

        /// <summary>   Gets or sets the timespan value. </summary>
        /// <value> The timespan value. </value>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "00:00:00.0000" )]
        public global::System.TimeSpan TimespanValue
        {
            get
            {
                return AppSettingGetter( this.TimespanValueDefault  );
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "00:00:00.0000" )]
        public global::System.TimeSpan TimespanValue1
        {
            get {
                return (( global::System.TimeSpan ) (this["TimespanValue1"]));
            }
            set {
                this["TimespanValue1"] = value;
            }
        }

        /// <summary>   The date time value expected. </summary>
        public readonly DateTime DateTimeValueExpected = System.DateTime.Parse( "2020-10-10 10:10:20" );

        /// <summary>   The date time value default. </summary>
        public readonly DateTime DateTimeValueDefault = System.DateTime.MinValue;

        /// <summary>   Gets or sets the date time value. </summary>
        /// <value> The date time value. </value>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "0001-01-01 00:00:00.0000")]
        public DateTime DateTimeValue
        {
            get
            {
                return AppSettingGetter( this.DateTimeValueDefault );
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        public readonly TraceEventType TraceEventTypeValueExpected = TraceEventType.Information;

        /// <summary>   The trace event type default. </summary>
        public readonly TraceEventType TraceEventTypeValueDefault = TraceEventType.Verbose;

        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "Verbose")]
        public TraceEventType TraceEventTypeValue
        {
            get
            {
                return AppSettingEnum<TraceEventType>();
            }

            set
            {
                AppSettingSetter( value );
            }
        }

        #endregion

    }
}
