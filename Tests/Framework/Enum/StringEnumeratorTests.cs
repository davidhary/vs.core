using System;
using System.Diagnostics;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.FrameworkTests
{

    /// <summary>
    /// This is a test class for StringEnumeratorTest and is intended to contain all
    /// StringEnumeratorTest Unit Tests.
    /// </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestClass()]
    public class StringEnumeratorTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( isr.Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( isr.Core.FrameworkTests.TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            // needs to add delay here to prevent an application domain exception.
            ApplianceBase.DoEvents();
            ApplianceBase.Delay( 200d );
            ApplianceBase.DoEvents();
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " STRING ENUMERATOR TESTS "

        /// <summary> A test for TryParse. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="value">             The value. </param>
        /// <param name="enumValueExpected"> The enum value expected. </param>
        public void TryParseTestHelper<T>( string value, T enumValueExpected ) where T : struct
        {
            var target = new StringEnumerator<T>();
            bool expected = true;
            bool actual;
            T enumValue = default;
            actual = target.TryParse( value, ref enumValue );
            Assert.AreEqual( enumValueExpected, enumValue );
            Assert.AreEqual( expected, actual );
        }

        /// <summary>
        /// (Unit Test Method) attempts to parse a test value test from the given data, returning a
        /// default value rather than throwing an exception if it fails.
        /// </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void TryParseTestValueTest()
        {
            this.TryParseTestHelper( "2", TestValue.Error );
        }

        /// <summary>
        /// (Unit Test Method) attempts to parse a color test from the given data, returning a default
        /// value rather than throwing an exception if it fails.
        /// </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void TryParseColorTest()
        {
            var colorStrings = new string[] { "0", "2", "8", "blue", "Blue", "Yellow", "Red, Green" };
            // var colorValues = new Colors[] { Colors.None, Colors.Green, Colors.Red | Colors.Green | Colors.Blue, Colors.Blue, Colors.Blue, Colors.None, Colors.Red | Colors.Green };
            foreach ( string colorString in colorStrings )
            {
                bool actual = Enum.TryParse( colorString, out Colors colorValue );
                if ( actual )
                {
                    if ( Enum.IsDefined( typeof( Colors ), colorValue ) | colorValue.ToString().Contains( "," ) )
                    {
                        Trace.Write( $"Converted '{colorString}' to {colorValue}." );
                    }
                    else
                    {
                        Trace.Write( $"{colorString } is not an underlying value of the Colors enumeration." );
                    }
                }
                else
                {
                    Trace.Write( $"{colorString} is not a member of the Colors enumeration." );
                }
            }
        }

        /// <summary> Values that represent test values. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        private enum TestValue
        {
            /// <summary> An enum constant representing the none option. </summary>
            None,
            /// <summary> An enum constant representing the Information option. </summary>
            Info,
            /// <summary> An enum constant representing the error] option. </summary>
            Error
        }

        /// <summary> A bit-field of flags for specifying colors. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [Flags()]
        public enum Colors : int
        {
            /// <summary> An enum constant representing the red option. </summary>
            Red = 1,
            /// <summary> An enum constant representing the green option. </summary>
            Green = 2,
            /// <summary> An enum constant representing the blue option. </summary>
            Blue = 4
        }
        #endregion

    }
}
