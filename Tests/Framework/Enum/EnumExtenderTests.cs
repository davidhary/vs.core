using System;
using System.Linq;

using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.FrameworkTests
{

    /// <summary>
    /// This is a test class for EfficientEnumTest and is intended to contain all EfficientEnumTest
    /// Unit Tests.
    /// </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestClass()]
    public class EnumExtenderTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( isr.Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( isr.Core.FrameworkTests.TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " ENUM EXTENDER TESTS "

        /// <summary> The color enum. </summary>
        private static readonly EnumExtender<Color> ColorEnum = new EnumExtender<Color>();

        /// <summary> Values that represent colors. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        private enum Color
        {
            /// <summary> An enum constant representing the white option. </summary>
            [System.ComponentModel.Description( "While Color" )]
            White,
            /// <summary> An enum constant representing the black option. </summary>
            [System.ComponentModel.Description( "Black Color" )]
            Black,
            /// <summary> An enum constant representing the red option. </summary>
            [System.ComponentModel.Description( "Red Color" )]
            Red,
            /// <summary> An enum constant representing the yellow option. </summary>
            [System.ComponentModel.Description( "Yellow Color" )]
            Yellow,
            /// <summary> An enum constant representing the blue option. </summary>
            [System.ComponentModel.Description( "Blue Color" )]
            Blue,
            /// <summary> An enum constant representing the green option. </summary>
            [System.ComponentModel.Description( "Green Color" )]
            Green,
            /// <summary> An enum constant representing the cyan option. </summary>
            [System.ComponentModel.Description( "Cyan Color" )]
            Cyan,
            /// <summary> An enum constant representing the magenta option. </summary>
            [System.ComponentModel.Description( "Magenta Color" )]
            Magenta,
            /// <summary> An enum constant representing the pink option. </summary>
            [System.ComponentModel.Description( "Pink Color" )]
            Pink,
            /// <summary> An enum constant representing the purple option. </summary>
            [System.ComponentModel.Description( "Purple Color" )]
            Purple,
            /// <summary> An enum constant representing the orange option. </summary>
            [System.ComponentModel.Description( "Orange Color" )]
            Orange,
            /// <summary> An enum constant representing the brown option. </summary>
            [System.ComponentModel.Description( "Brown Color" )]
            Brown
        }

        /// <summary> Values that represent Strings. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        private static readonly string[] EnumStrings = new string[] { "White", "Black", "Red", "Yellow", "Blue", "Green", "Cyan", "Magenta", "Pink", "Purple", "Orange", "Brown" };

        /// <summary> The iterations. </summary>
        private const int _Iterations = 100000;

        /// <summary> Main entry-point for this application. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        public static void Main()
        {
            var randomNumber = new Random();
            using ( var TempPerformanceMonitor = new PerformanceMonitor( "{Built-in Enum class}" ) )
            {
                for ( int i = 0; i <= _Iterations - 1; i++ )
                {
                    int index = randomNumber.Next( 0, 11 );
                    _ = ( Color ) Conversions.ToInteger( Enum.ToObject( typeof( Color ), index ) );
                    _ = ( Color ) Conversions.ToInteger( Enum.Parse( typeof( Color ), EnumStrings[index] ) );
                }
            }

            // Verify initialization of the data out of the comparative measurement.
            // As you can see, this initialization is the gain for the later efficiency.
            var colorEnum = new EnumExtender<Color>();
            using ( var TempPerformanceMonitor = new PerformanceMonitor( "{StrongQuickEnum<Color> class}" ) )
            {
                for ( int i = 0; i <= _Iterations - 1; i++ )
                {
                    int index = randomNumber.Next( 0, 11 );
                    _ = colorEnum.ToObject( index );
                    _ = colorEnum.Parse( EnumStrings[index] );
                }
            }

            _ = Console.ReadLine();
        }

        /// <summary> A performance monitor. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        internal class PerformanceMonitor : IDisposable
        {
            /// <summary> The time started. </summary>
            private readonly long _Timestarted;

            /// <summary> The name. </summary>
            private readonly string _Name;

            /// <summary> Constructor. </summary>
            /// <remarks> David, 2020-09-18. </remarks>
            /// <param name="name"> The name. </param>
            internal PerformanceMonitor( string name )
            {
                this._Name = name;
                this._Timestarted = DateTimeOffset.Now.Ticks;
            }

            /// <summary>
            /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
            /// resources.
            /// </summary>
            /// <remarks> David, 2020-09-18. </remarks>
            public void Dispose()
            {
                Console.WriteLine( "Operation " + this._Name + ":" + Constants.vbTab + Constants.vbTab + (DateTimeOffset.Now.Ticks - this._Timestarted).ToString() );
            }
        }

        /// <summary> (Unit Test Method) converts this object to an object test 1. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void ToObjectTest1()
        {
            int value = ( int ) Color.Cyan;
            var expected = Color.Cyan;
            var actual = ColorEnum.ToObject( value );
            Assert.AreEqual( expected, actual );
        }

        /// <summary> (Unit Test Method) converts this object to an object test 2. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void ToObjectTest2()
        {
            int value = ( int ) Color.Cyan;
            var expected = Color.Cyan;
            var actual = ColorEnum.ToObject( value );
            Assert.AreEqual( expected, actual );
        }

        /// <summary> (Unit Test Method) parse test 1. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void ParseTest1()
        {
            string value = nameof( Color.White );
            bool ignoreCase = false;
            var expected = Color.White;
            var actual = ColorEnum.Parse( value, ignoreCase );
            Assert.AreEqual( expected, actual );
        }

        /// <summary> A test for Parse. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        public static void ParseTestHelper<T>()
        {
            string value = nameof( Color.Cyan );
            var expected = Color.Cyan;
            var actual = ColorEnum.Parse( value );
            Assert.AreEqual( expected, actual );
        }

        /// <summary> (Unit Test Method) tests parse. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void ParseTest()
        {
            ParseTestHelper<Color>();
        }

        /// <summary> A test for IsDefined. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        public static void IsDefinedTestHelper<T>()
        {
            object value = Color.Cyan;
            bool expected = true;
            bool actual = ColorEnum.IsDefined( value );
            Assert.AreEqual( expected, actual );
        }

        /// <summary> (Unit Test Method) tests is defined. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void IsDefinedTest()
        {
            IsDefinedTestHelper<Color>();
        }

        /// <summary> A test for GetValues. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        public static void GetValuesTestHelper<T>()
        {
            var expected = Enum.GetValues( typeof( Color ) );
            var actual = ColorEnum.Values;
            var a = new int[expected.Length];
            actual.CopyTo( a, 0 );
            var b = new int[actual.Length];
            expected.CopyTo( b, 0 );
            Assert.IsTrue( a.Length == b.Length && a.Intersect( b ).Count() == a.Length, "Values are equal" );
        }

        /// <summary> (Unit Test Method) tests get values. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void GetValuesTest()
        {
            GetValuesTestHelper<Color>();
        }

        /// <summary> A test for GetUnderlyingType. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        public static void GetUnderlyingTypeTestHelper<T>()
        {
            var expected = typeof( int );
            var actual = ColorEnum.UnderlyingType;
            Assert.AreEqual( expected, actual );
        }

        /// <summary> (Unit Test Method) tests get underlying type. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void GetUnderlyingTypeTest()
        {
            GetUnderlyingTypeTestHelper<Color>();
        }

        /// <summary> A test for GetNames. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        public static void GetNamesTestHelper<T>()
        {
            var expected = EnumStrings;
            var actual = ColorEnum.Names.ToArray();
            Assert.IsTrue( expected.Length == actual.Length && expected.Intersect( actual ).Count() == expected.Length, "Names are equal" );
        }

        /// <summary> (Unit Test Method) tests get names. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void GetNamesTest()
        {
            GetNamesTestHelper<Color>();
        }

        /// <summary> A test for GetName. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        public static void GetNameTestHelper<T>()
        {
            var value = Color.Black;
            string expected = nameof( Color.Black );
            string actual = ColorEnum.Name( value );
            Assert.AreEqual( expected, actual, $"Name of {value}" );
        }

        /// <summary> (Unit Test Method) tests get name. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void GetNameTest()
        {
            GetNameTestHelper<Color>();
        }

        /// <summary> A test for EfficientEnum`1 Constructor. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        public static void EfficientEnumConstructorTestHelper<T>()
        {
            var target = new EnumExtender<T>();
            string expectedDesction = "Black Color";
            string actualDescription = target.Description( Color.Black );
            Assert.AreEqual( expectedDesction, actualDescription, $"Description of {Color.Black}" );
        }

        /// <summary> (Unit Test Method) tests efficient enum constructor. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void EfficientEnumConstructorTest()
        {
            EfficientEnumConstructorTestHelper<Color>();
        }
        #endregion

    }
}
