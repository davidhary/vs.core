namespace isr.Core.FrameworkTests.My
{
    /// <summary>   my library. </summary>
    /// <remarks>   David, 2020-03-18. </remarks>
    internal class MyLibrary
    {
        public const string AssemblyTitle = "ISR Core Framework Test Library";
        public const string AssemblyDescription = "ISR Core Framework Test Library";
        public const string AssemblyProduct = "isr.Core.Framework.Tests";
        public const string AssemblyConfiguration = "Debug";
    }
}
