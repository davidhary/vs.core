using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.FrameworkTests
{
    /// <summary> Assert. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-12-12 </para><para>
    /// https://stackoverflow.com/questions/933613/how-do-i-use-assert-to-verify-that-an-exception-has-been-thrown
    /// </para>
    /// </remarks>
    public sealed class Asserts
    {

        #region " SINGLETON "

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        public Asserts() : base()
        {
        }

        private static readonly Lazy<Asserts> LazyAsserts = new();

        /// <summary>   Gets the instance of the <see cref="Asserts"/> class. </summary>
        /// <remarks>
        /// Use this property to instantiate a single instance of this class. This class uses lazy
        /// instantiation, meaning the instance isn't created until the first time it's retrieved.
        /// </remarks>
        /// <value> A new or existing instance of the <see cref="Asserts"/> class. </value>
        public static Asserts Instance => LazyAsserts.Value;

        /// <summary> Returns true if an instance of the class was created and not disposed. </summary>
        /// <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
        public static bool Instantiated => LazyAsserts.IsValueCreated;

        #endregion

        #region " TIME SPAN "

        /// <summary> Are equal. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <exception cref="AssertFailedException"> Thrown when an Assert Failed error condition occurs. </exception>
        /// <param name="expected"> The expected. </param>
        /// <param name="actual">   The actual. </param>
        /// <param name="delta">    The delta. </param>
        /// <param name="message">  The message. </param>
        public void AreEqual( TimeSpan expected, TimeSpan actual, TimeSpan delta, string message )
        {
            if ( actual < expected.Subtract( delta ) || actual > expected.Add( delta ) )
            {
                throw new AssertFailedException( $"Expected {expected} <> actual {actual} by more than {delta}; {message}" );
            }
        }

        /// <summary> Are equal. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="expected"> The expected. </param>
        /// <param name="actual">   The actual. </param>
        /// <param name="delta">    The delta. </param>
        /// <param name="format">   Describes the format to use. </param>
        /// <param name="args">     A variable-length parameters list containing arguments. </param>
        public void AreEqual( TimeSpan expected, TimeSpan actual, TimeSpan delta, string format, params object[] args )
        {
            this.AreEqual( expected, actual, delta, string.Format( format, args ) );
        }

        #endregion

        #region " DATE TIME "

        /// <summary> Are equal. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <exception cref="AssertFailedException"> Thrown when an Assert Failed error condition occurs. </exception>
        /// <param name="expected"> The expected. </param>
        /// <param name="actual">   The actual. </param>
        /// <param name="delta">    The delta. </param>
        /// <param name="message">  The message. </param>
        public void AreEqual( DateTime expected, DateTime actual, TimeSpan delta, string message )
        {
            if ( actual < expected.Subtract( delta ) || actual > expected.Add( delta ) )
            {
                throw new AssertFailedException( $"Expected {expected} > actual {actual} by {expected.Subtract( actual ).Subtract( delta )} over {delta}; {message}" );
            }
        }

        /// <summary> Are equal. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="expected"> The expected. </param>
        /// <param name="actual">   The actual. </param>
        /// <param name="delta">    The delta. </param>
        /// <param name="format">   Describes the format to use. </param>
        /// <param name="args">     A variable-length parameters list containing arguments. </param>
        public void AreEqual( DateTime expected, DateTime actual, TimeSpan delta, string format, params object[] args )
        {
            this.AreEqual( expected, actual, delta, string.Format( format, args ) );
        }

        #endregion

        #region " DATE TIME OFFSET "

        /// <summary> Are equal. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <exception cref="AssertFailedException"> Thrown when an Assert Failed error condition occurs. </exception>
        /// <param name="expected"> The expected. </param>
        /// <param name="actual">   The actual. </param>
        /// <param name="delta">    The delta. </param>
        /// <param name="message">  The message. </param>
        public void AreEqual( DateTimeOffset expected, DateTimeOffset actual, TimeSpan delta, string message )
        {
            if ( actual < expected.Subtract( delta ) || actual > expected.Add( delta ) )
            {
                throw new AssertFailedException( $"Expected {expected:o} <> actual {actual:o} by more than {delta}; {message}" );
            }
        }

        /// <summary> Are equal. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="expected"> The expected. </param>
        /// <param name="actual">   The actual. </param>
        /// <param name="delta">    The delta. </param>
        /// <param name="format">   Describes the format to use. </param>
        /// <param name="args">     A variable-length parameters list containing arguments. </param>
        public void AreEqual( DateTimeOffset expected, DateTimeOffset actual, TimeSpan delta, string format, params object[] args )
        {
            this.AreEqual( expected, actual, delta, string.Format( format, args ) );
        }

        #endregion

        #region " IS SINGLE "

        /// <summary> Is single. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="items"> The items. </param>
        public void IsSingle( IEnumerable<object> items )
        {
            Assert.IsTrue( items is object );
            Assert.IsTrue( items.Any() );
            Assert.AreEqual( 1, items.Count() );
        }

        /// <summary> Is single. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="items">  The items. </param>
        /// <param name="format"> Describes the format to use. </param>
        /// <param name="args">   A variable-length parameters list containing arguments. </param>
        public void IsSingle( IEnumerable<object> items, string format, params object[] args )
        {
            Assert.IsTrue( items is object, format, args );
            Assert.IsTrue( items.Any(), format, args );
            Assert.AreEqual( 1, items.Count(), format, args );
        }

        #endregion

        #region " IS EMPY "

        /// <summary> Is single. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="items"> The items. </param>
        public void IsEmpty( IEnumerable<object> items )
        {
            if ( items is object )
            {
                Assert.IsFalse( items.Any() );
            }
        }

        /// <summary> Is single. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="items">  The items. </param>
        /// <param name="format"> Describes the format to use. </param>
        /// <param name="args">   A variable-length parameters list containing arguments. </param>
        public void IsEmpty( IEnumerable<object> items, string format, params object[] args )
        {
            if ( items is object )
            {
                Assert.IsFalse( items.Any(), format, args );
            }
        }

        #endregion

        #region " THROWS "

        /// <summary> Throws an exception to verify that an exception has been throw. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <exception cref="AssertFailedException"> Thrown when an Assert Failed error condition occurs. </exception>
        /// <param name="func"> The function. </param>
        /// <returns> A T. </returns>
        public T Throws<T>( Action func ) where T : Exception
        {
            bool exceptionThrown = false;
            T result = null;
            try
            {
                func?.Invoke();
            }
            catch ( T ex )
            {
                exceptionThrown = true;
                result = ex;
            }

            return !exceptionThrown ? throw new AssertFailedException( $"An exception of type {typeof( T )} was expected, but not thrown" ) : result;
        }

        /// <summary> Throws an exception to verify that an exception has been throw. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <exception cref="AssertFailedException"> Thrown when an Assert Failed error condition occurs. </exception>
        /// <param name="func">    The function. </param>
        /// <param name="message"> The message. </param>
        /// <returns> A T. </returns>
        public T Throws<T>( Action func, string message ) where T : Exception
        {
            bool exceptionThrown = false;
            T result = null;
            try
            {
                func?.Invoke();
            }
            catch ( T ex )
            {
                exceptionThrown = true;
                result = ex;
            }

            return !exceptionThrown
                ? throw new AssertFailedException( $"An exception of type {typeof( T )} was expected, but not thrown; {message}" )
                : result;
        }

        /// <summary> Throws an exception to verify that an exception has been throw. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="func">   The function. </param>
        /// <param name="format"> Describes the format to use. </param>
        /// <param name="args">   A variable-length parameters list containing arguments. </param>
        /// <returns> A T. </returns>
        public T Throws<T>( Action func, string format, params object[] args ) where T : Exception
        {
            return this.Throws<T>( func, string.Format( format, args ) );
        }
        #endregion

    }

    /// <summary>   my assert property. </summary>
    /// <remarks>   David, 2020-09-18. </remarks>
    public static class MyAssertProperty
    {

        /// <summary> Gets my assert. </summary>
        /// <value> my assert. </value>
        public static Asserts MyAssert => Asserts.Instance;
    }
}
