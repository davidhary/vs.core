using System;

using Microsoft.VisualBasic.CompilerServices;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.FrameworkTests
{

    /// <summary> This is a test class for event handling. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestClass()]
    public class EventHandlingTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( isr.Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( isr.Core.FrameworkTests.TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " EVENT HANDLER CONTEXT OF SYSTEM EVENT ARGS TEST "

        /// <summary> Handles the refresh requested. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void HandleRefreshRequested( object sender, EventArgs e )
        {
            if ( !(sender is ProgramInfo programInfoSender) )
            {
                return;
            }

            programInfoSender.AppendLine( $"Line #{programInfoSender.Lines.Count}", new System.Drawing.Font( "Consolas", 8.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte( 0 ) ) );
        }

        /// <summary> (Unit Test Method) tests event handler context. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void EventHandlerContextTest()
        {
            var sender = new ProgramInfo();
            sender.RefreshRequested += this.HandleRefreshRequested;
            int expectedLineCount = sender.Lines.Count + 1;
            sender.NotifyRefreshRequested();
            Assert.AreEqual( expectedLineCount, sender.Lines.Count, "line count should equal after handling the refresh requested event" );
        }

        #endregion

        #region " ACTION NOTIFY TEST "

        /// <summary> The actual notified value. </summary>
        private string _ActualNotifiedValue = string.Empty;

        /// <summary> Handles the action notify described by value. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="value"> The value. </param>
        private void HandleActionNotify( string value )
        {
            this._ActualNotifiedValue = value;
        }

        /// <summary> Registers the action. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="sender">             Source of the event. </param>
        /// <param name="engineFailedAction"> The engine failed action. </param>
        private void RegisterAction( ActionNotifier<string> sender, Action<string> engineFailedAction )
        {
            sender.Register( engineFailedAction );
        }

        /// <summary> (Unit Test Method) tests action notifier handling. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void ActionNotifierHandlingTest()
        {
            var sender = new ActionNotifier<string>();
            this.RegisterAction( sender, this.HandleActionNotify );
            string expectedNotificationValue = $"{nameof( ActionNotifier<string>.SyncInvoke )}";
            sender.SyncInvoke( expectedNotificationValue );
            Assert.AreEqual( expectedNotificationValue, this._ActualNotifiedValue, "expects a notification value" );
        }

        #endregion

    }
}
