using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.FrameworkTests
{

    /// <summary>
    /// This is a test class for EngineeringFormatProviderTest and is intended to contain all
    /// EngineeringFormatProviderTest Unit Tests.
    /// </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestClass()]
    public class EngineeringFormatProviderTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( isr.Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( isr.Core.FrameworkTests.TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " ENGINEERING FORMAT PROVIDER TESTS "

        /// <summary> A test for Formatting. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="value">                   The value. </param>
        /// <param name="format">                  Describes the format to use. </param>
        /// <param name="usingThousandsSeparator"> true to using thousands separator. </param>
        /// <param name="expectedValue">           The expected value. </param>
        public static void CustomFormatPiTest( double value, string format, bool usingThousandsSeparator, string expectedValue )
        {
            var target = new EngineeringFormatProvider() { UsingThousandsSeparator = usingThousandsSeparator };
            string actual = string.Format( target, format, value );
            Assert.AreEqual( expectedValue, actual );
        }

        /// <summary> A test for Formatting. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="value">         The value. </param>
        /// <param name="format">        Describes the format to use. </param>
        /// <param name="expectedValue"> The expected value. </param>
        public static void CustomFormatPiTest( double value, string format, string expectedValue )
        {
            IFormatProvider target = new EngineeringFormatProvider();
            string actual = string.Format( target, format, value );
            Assert.AreEqual( expectedValue, actual );
        }

        /// <summary> A test for Formatting PI. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void CustomFormatPiTest()
        {
            CustomFormatPiTest( Math.PI, "{0}", "3.142" );
        }

        /// <summary> A test for Formatting 0.1 * PI. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void CustomFormatDeciPiTest()
        {
            CustomFormatPiTest( 0.1d * Math.PI, "{0}", "0.3142" );
        }

        /// <summary> A test for Formatting 0.01 * PI. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void CustomFormatCentiPiTest()
        {
            CustomFormatPiTest( 0.01d * Math.PI, "{0}", "31.42e-03" );
        }

        /// <summary> A test for Formatting 0.001 * PI. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void CustomFormatMilliPiTest()
        {
            CustomFormatPiTest( 0.001d * Math.PI, "{0}", "3.142e-03" );
        }

        /// <summary> A test for Formatting 0.000001 * PI. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void CustomFormatMicroPiTest()
        {
            CustomFormatPiTest( 0.000001d * Math.PI, "{0}", "3.142e-06" );
        }

        /// <summary> A test for Formatting 1000 * PI. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void CustomFormatKiloPiTest()
        {
            CustomFormatPiTest( 1000d * Math.PI, "{0}", "3.142e+03" );
        }

        /// <summary> A test for Formatting 1000000 * PI. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void CustomFormatMegaPiTest()
        {
            CustomFormatPiTest( 1000000d * Math.PI, "{0}", "3.142e+06" );
        }

        /// <summary> A test for Formatting 1000 * PI. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void CustomFormatKiloWithSeparatorPiTest()
        {
            CustomFormatPiTest( 1000d * Math.PI, "{0}", true, "3,142" );
        }

        /// <summary> A test for Formatting 10000 * PI. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void CustomFormatDekaWithSeparatorPiTest()
        {
            CustomFormatPiTest( 10000d * Math.PI, "{0}", true, "31,416" );
        }

        /// <summary> A test for Formatting 100000 * PI. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void CustomFormatHectoWithSeparatorPiTest()
        {
            CustomFormatPiTest( 100000d * Math.PI, "{0}", true, "314,159" );
        }

        /// <summary> A test for Formatting 1000000 * PI. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void CustomFormatMegaWithSeparatorPiTest()
        {
            CustomFormatPiTest( 1000000d * Math.PI, "{0}", true, "3.142e+06" );
        }
        #endregion

    }
}
