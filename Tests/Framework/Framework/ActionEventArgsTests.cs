using System;
using System.Diagnostics;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.FrameworkTests
{

    /// <summary> Action Event Arguments tests. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-04-08 </para>
    /// </remarks>
    [TestClass()]
    public class ActionEventArgsTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( isr.Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " ACTION EVENT ARGMENTS TESTS "

        /// <summary> (Unit Test Method) tests action event. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void ActionEventTest()
        {
            var args = new ActionEventArgs( TraceEventType.Error );
            string message = "Information";
            var eventType = TraceEventType.Information;
            args.RegisterOutcomeEvent( eventType, message );
            Assert.IsFalse( args.Failed, $"Action {args.Details} at {eventType} failed" );
            Assert.AreEqual( eventType, args.OutcomeEvent, $"Action {args.Details} event unexpected" );
            Assert.IsTrue( args.HasDetails, $"Action {args.Details} at {eventType} reported not having details" );
            eventType = TraceEventType.Error;
            message = "Error";
            args.RegisterOutcomeEvent( eventType, message );
            Assert.IsTrue( args.Failed, $"Action {args.Details} at {eventType} did not failed" );
            Assert.AreEqual( eventType, args.OutcomeEvent, $"Action {args.Details} event unexpected" );
            eventType = TraceEventType.Critical;
            message = "Critical";
            args.RegisterOutcomeEvent( eventType, message );
            Assert.IsTrue( args.Failed, $"Action {args.Details} at {eventType} did not failed" );
            Assert.AreEqual( eventType, args.OutcomeEvent, $"Action {args.Details} event unexpected" );
        }

        /// <summary> (Unit Test Method) tests action event cancellation. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void ActionEventCancellationTest()
        {
            var args = new ActionEventArgs( TraceEventType.Error );
            string message = "Cancel";
            args.RegisterFailure( message );
            Assert.IsTrue( args.Failed, $"Action {args.Details} failed to cancel" );
        }

        #endregion

    }
}
