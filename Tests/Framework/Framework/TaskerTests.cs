using System;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.FrameworkTests
{

    /// <summary> A tasker tests. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestClass()]
    public class TaskerTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( isr.Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( isr.Core.FrameworkTests.TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " TASKER TESTS  "

        /// <summary> Assert tasker exception. </summary>
        /// <remarks> David, 2020-08-06. </remarks>
        /// <exception cref="OperationCanceledException"> Thrown when an Operation Canceled error condition
        /// occurs. </exception>
        public static void AssertTaskerException()
        {
            var tasker = new Tasker();
            // task should throw an exception
            string activity = "";
            activity = "Task started";
            try
            {
                activity = "Task action";
                tasker.StartAction( () => { throw new OperationCanceledException( "thrown from withing the task action" ); } );
                activity = "Task awaiting task idle";
                Assert.IsTrue( tasker.AwaitTaskIdle( TimeSpan.FromMilliseconds( 200d ) ), $"{nameof( Tasker.AwaitTaskIdle )} should timeout" );
            }
            catch ( Exception ex )
            {
                Console.Out.WriteLine( $"thrown from {activity}" );
                throw new OperationCanceledException( $"thrown from {activity}", ex );
            }
        }

        /// <summary>   Assert tasker asynchronous completed should report exception private. </summary>
        /// <remarks>   David, 2020-08-06. </remarks>
        /// <exception cref="OperationCanceledException">   Thrown when a thread cancels a running
        ///                                                 operation. </exception>
        private static void AssertTaskerAsyncCompletedShouldReportExceptionPrivate( bool expectsException = true )
        {
            var startTime = default( DateTimeOffset );
            var captureTimestamp = default( TimeSpan );
            var exceptionTimestamp = default( TimeSpan );
            var actionExitTimestamp = TimeSpan.Zero;
            TimeSpan taskDoneTimestamp;
            var handled = false;
            var isFaulted = default( bool );
            Exception exception = null;
            var tasker = new Tasker();
            tasker.AsyncCompleted += ( sender, e ) => {
                handled = true;
                isFaulted = e.Error is object;
                exception = e.Error;
                captureTimestamp = DateTimeOffset.Now.Subtract( startTime );
            };
            // task should throw an exception to be captured by the tasker before the wait occurred.
            string activity = "Task started";
            activity = "Task action";
            startTime = DateTimeOffset.Now;
            tasker.StartAction( () => {
                exceptionTimestamp = DateTimeOffset.Now.Subtract( startTime );
                if ( expectsException )
                    throw new OperationCanceledException( $"{activity} thrown from withing the task action" );
            } );
            activity = "Task awaiting task idle";
            Assert.IsTrue( tasker.AwaitTaskIdle( TimeSpan.FromMilliseconds( 400d ) ), $"{nameof( Tasker.AwaitTaskIdle )} should not timeout" );
            Assert.IsTrue( handled, $"{nameof( Tasker.AsyncCompleted )} should be handled" );
            Assert.AreEqual( expectsException, isFaulted, $"{nameof( Tasker.ActionTask )}.{nameof( Task.IsFaulted )} should match" );
            if ( expectsException )
            {
                Assert.IsNotNull( exception, $"{nameof( AsyncCompletedEventArgs )}.{nameof( AsyncCompletedEventArgs.Error )} should not be null" );
                Assert.IsNotNull( exception.InnerException as OperationCanceledException, $"{nameof( AsyncCompletedEventArgs )}.{nameof( AsyncCompletedEventArgs.Error )} should be {nameof( OperationCanceledException )}" );
            }
            else
            {
                Assert.IsNull( exception, $"{nameof( AsyncCompletedEventArgs )}.{nameof( AsyncCompletedEventArgs.Error )} should be null" );
            }
            Assert.IsFalse( tasker.ActionTask.IsFaulted, $"{nameof( Tasker.ActionTask )}.{nameof( Task.IsFaulted )} should match as {nameof( Tasker.ActionTask )} does not register the exception" );
            taskDoneTimestamp = DateTimeOffset.Now.Subtract( startTime );
            // Timestamps: Exception 0; Capture: 1.3379; action exit 0; Done 1.3379
            Console.Out.WriteLine( $"Timestamps {(expectsException ? "with" : "without")} error: Exception {exceptionTimestamp.TotalMilliseconds} ms; Capture: {captureTimestamp.TotalMilliseconds} ms; action exit {actionExitTimestamp.TotalMilliseconds} ms; Done {taskDoneTimestamp.TotalMilliseconds} ms" );
        }

        /// <summary>   Assert tasker asynchronous completed should report exception. </summary>
        /// <remarks>   David, 2020-07-17. </remarks>
        public static void AssertTaskerAsyncCompletedShouldReportException()
        {
            var tasker = new Tasker();
            var timeout = TimeSpan.FromMilliseconds( 100d );
            var delay = timeout.Subtract( TimeSpan.FromMilliseconds( 50d ) );
            tasker.StartAction( () => Thread.Sleep( delay ) );
            Assert.IsTrue( tasker.AwaitTaskIdle( timeout ), $"{nameof( Tasker.AwaitTaskIdle )} should not timeout" );
            delay = timeout.Add( TimeSpan.FromMilliseconds( 50d ) );
            tasker.StartAction( () => Thread.Sleep( delay ) );
            Assert.IsFalse( tasker.AwaitTaskIdle( timeout ), $"{nameof( Tasker.AwaitTaskIdle )} should timeout" );

            // MyAssertProperty.MyAssert.Throws(Of OperationCanceledException)(AddressOf AssertTaskerException)
            AssertTaskerAsyncCompletedShouldReportExceptionPrivate( true );
            AssertTaskerAsyncCompletedShouldReportExceptionPrivate( false );
        }

        /// <summary>
        /// (Unit Test Method) tasker asynchronous completed should report exception.
        /// </summary>
        /// <remarks>   David, 2020-07-17. </remarks>
        [TestMethod()]
        public void TaskerAsyncCompletedShouldReportException()
        {
            AssertTaskerAsyncCompletedShouldReportException();
        }

        /// <summary> Assert tasker of tuple. </summary>
        /// <remarks> David, 2020-07-17. </remarks>
        public static void AssertTaskerResultShouldEqualTuple()
        {
            var tasker = new Tasker<(bool Success, string Details)>();
            var timeout = TimeSpan.FromMilliseconds( 100d );
            var delay = timeout.Subtract( TimeSpan.FromMilliseconds( 50d ) );
            (bool Success, string Details) expectedResult = (true, "Success");
            tasker.StartAction( () => {
                Thread.Sleep( delay );
                return expectedResult;
            } );
            Assert.IsTrue( tasker.AwaitTaskIdle( timeout ), $"{nameof( Tasker.AwaitTaskIdle )} should not timeout" );
            var result = tasker.AwaitCompletion( timeout );
            Assert.AreEqual( TaskStatus.RanToCompletion, result.Status, $"{nameof( TaskStatus )} should match" );
            Assert.AreEqual( expectedResult.Details, result.Result.Details, $"{nameof( Tasker<(bool Success, string Details)> )} Result should match" );
            expectedResult = (false, "Failure");
            tasker.StartAction( () => {
                Thread.Sleep( delay );
                return expectedResult;
            } );
            result = tasker.AwaitCompletion( timeout );
            Assert.AreEqual( TaskStatus.RanToCompletion, result.Status, $"{nameof( TaskStatus )} should match" );
            Assert.AreEqual( expectedResult.Details, result.Result.Details, $"{nameof( Tasker<(bool Success, string Details)> )} Result should match" );
        }

        /// <summary>   (Unit Test Method) tasker result should equal tuple. </summary>
        /// <remarks>   David, 2020-07-17. </remarks>
        [TestMethod()]
        public void TaskerResultShouldEqualTuple()
        {
            AssertTaskerResultShouldEqualTuple();
        }

        #endregion

    }
}
