
#pragma warning disable IDE1006 // Naming Styles
namespace isr.Core.CapsuleTests.My
#pragma warning restore IDE1006 // Naming Styles
{

    /// <summary> Provides assembly information for the class library. </summary>
    internal sealed partial class MyLibrary
    {

        /// <summary> Constructor that prevents a default instance of this class from being created. </summary>
        private MyLibrary() : base()
        {
        }

        public const string AssemblyTitle = "Core Capsule Tests";
        public const string AssemblyDescription = "Unit Tests for the Core Capsule Library";
        public const string AssemblyProduct = "isr.Core.CapsuleTests";
    }
}
