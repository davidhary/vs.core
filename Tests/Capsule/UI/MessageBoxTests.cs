using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.CapsuleTests
{

    /// <summary> SQL Message Box tests. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-04-13 </para>
    /// </remarks>
    [TestClass()]
    public class MessageBoxTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( isr.Core.CapsuleTests.TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        /// <summary> Shows the dialog abort ignore. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <exception cref="DivideByZeroException"> Thrown when an attempt is made to divide a number by
        /// zero. </exception>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static void ShowDialogException()
        {
            try
            {
                throw new DivideByZeroException();
            }
            catch ( Exception ex )
            {
                var expected = MyDialogResult.Ok;
                MyDialogResult actual;
                ex.Data.Add( "@isr", "Testing exception message" );
                actual = MyMessageBox.ShowDialog( ex );
                Assert.AreEqual( expected, actual );
            }
        }

        /// <summary> (Unit Test Method) tests show exception dialog. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void ShowExceptionDialogTest()
        {
            var oThread = new System.Threading.Thread( new System.Threading.ThreadStart( ShowDialogException ) );
            oThread.Start();
            // a long delay was required...
            ApplianceBase.Delay( 2000d );
            WindowsForms.SendWait( "{ENTER}" );
            // This tabbed into another application.  isr.Core.Services.WindowsForms.SendWait("%{TAB}{ENTER}")
            oThread.Join();
        }

        /// <summary> Shows the dialog abort ignore. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <exception cref="DivideByZeroException"> Thrown when an attempt is made to divide a number by
        /// zero. </exception>
        public static void ShowDialogAbortIgnore()
        {
            try
            {
                throw new DivideByZeroException();
            }
            catch ( DivideByZeroException ex )
            {
                ex.Data.Add( "@isr", "Testing exception message" );
                var result = MyMessageBox.ShowDialogAbortIgnore( ex, MyMessageBoxIcon.Error );
                Assert.IsTrue( result == MyDialogResult.Abort || result == MyDialogResult.Ignore, $"{result} expected {MyDialogResult.Abort} or {MyDialogResult.Ignore}" );
            }
        }

        /// <summary> Tests showing dialog with abort and ignore buttons. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void ShowDialogAbortIgnoreTest()
        {
            var oThread = new System.Threading.Thread( new System.Threading.ThreadStart( ShowDialogAbortIgnore ) );
            oThread.Start();
            // a long delay was required...
            ApplianceBase.Delay( 2000d );
            WindowsForms.SendWait( "{ENTER}" );
            // This tabbed into another application.  isr.Core.Services.WindowsForms.SendWait("%{TAB}{ENTER}")
            oThread.Join();
        }
    }
}
