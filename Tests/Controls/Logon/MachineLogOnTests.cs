using System;
using System.Collections;
using System.Collections.Generic;

using isr.Core.Controls;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.ControlsTests
{

    /// <summary>
    /// This is a test class for MachineLogOnTest and is intended to contain all MachineLogOnTest
    /// Unit Tests.
    /// </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestClass()]
    public class MachineLogOnTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Controls.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( ControlsTests.MachineLogOnTests.TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( ControlsTests.TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        /// <summary> A test for Validate. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void ValidateRole()
        {
            if ( !TestInfo.MachineLogonTestsEnabled )
            {
                TestInfo.VerboseMessage( "Machine log-on tests disabled" );
                return;
            }

            string userName = WindowsForms.EnterInput( "Enter Admin user name or empty to cancel this test" );
            if ( string.IsNullOrEmpty( userName ) )
            {
                return;
            }

            string password = WindowsForms.EnterInput( "Enter Admin password or empty to cancel this test", true );
            if ( string.IsNullOrEmpty( password ) )
            {
                return;
            }

            using var target = new MachineLogOn();
            var roles = new List<string>() { "Administrators" };
            var allowedUserRoles = new ArrayList( roles );
            bool expected = true;
            bool actual = target.Authenticate( userName, password, allowedUserRoles );
            Assert.AreEqual( expected, actual, target.ValidationMessage );
        }

        /// <summary> A test for Validate. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void ValidateUser()
        {
            if ( !TestInfo.MachineLogonTestsEnabled )
            {
                TestInfo.VerboseMessage( "Machine log-on tests disabled" );
                return;
            }

            string userName = WindowsForms.EnterInput( "Enter a User name or empty to cancel this test" );
            if ( string.IsNullOrEmpty( userName ) )
            {
                return;
            }

            string password = WindowsForms.EnterInput( "Enter password or empty to cancel this test", true );
            if ( string.IsNullOrEmpty( password ) )
            {
                return;
            }

            using var target = new MachineLogOn();
            bool expected = true;
            bool actual;
            actual = target.Authenticate( userName, password );
            Assert.AreEqual( expected, actual, target.ValidationMessage );
        }
    }
}
