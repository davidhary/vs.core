﻿using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.Core.ControlsTests.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.Core.ControlsTests.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.Core.ControlsTests.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
