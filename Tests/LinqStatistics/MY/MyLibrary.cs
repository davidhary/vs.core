﻿
namespace isr.Core.LinqStatisticsTests.My
{

    /// <summary> Provides assembly information for the class library. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    internal sealed partial class MyLibrary
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        private MyLibrary() : base()
        {
        }

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "Core Linq Statistics Tests";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "Unit Tests for the Core Linq Statistics Library";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "isr.Core.Linq.StatisticsTests";
    }
}