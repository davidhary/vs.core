﻿using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.Core.LinqStatisticsTests.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.Core.LinqStatisticsTests.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.Core.LinqStatisticsTests.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
