﻿using System;
using System.Collections.Generic;

using isr.Core.RandomExtensions;

namespace isr.Core.LinqStatisticsTests
{

    /// <summary>
    /// Constructor that prevents a default instance of this class from being created.
    /// </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    internal sealed class TestData
    {

        /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        private TestData()
        {
        }

        /// <summary> Gets the ints in this collection. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the ints in this collection.
        /// </returns>
        public static IEnumerable<int> GetInts()
        {
            return new int[] { 2, 3, 4, 6 };
        }

        /// <summary> Gets the nullable ints in this collection. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the nullable ints in this collection.
        /// </returns>
        public static IEnumerable<int?> GetNullableInts()
        {
            return new int?[] { 2, default, 3, 4, 6 };
        }

        /// <summary> Gets the doubles in this collection. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the doubles in this collection.
        /// </returns>
        public static IEnumerable<double> GetDoubles()
        {
            return new double[] { 2.1d, 3.5d, 4.6d, 6.9d };
        }

        /// <summary> Gets the nullable doubles in this collection. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the nullable doubles in this
        /// collection.
        /// </returns>
        public static IEnumerable<double?> GetNullableDoubles()
        {
            return new double?[] { 2.1d, 3.5d, default, 4.6d, 6.9d };
        }

        /// <summary> Gets the floats in this collection. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the floats in this collection.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        public static IEnumerable<float> GetFloats()
        {
            return new float[] { 2.1f, 3.5f, 4.6f, 6.9f };
        }

        /// <summary> Gets the random normal doubles in this collection. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="count"> Number of. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the random normal doubles in this
        /// collection.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        public static IEnumerable<double> GetRandomNormalDoubles( int count )
        {
            return GetRandomNormalDoubles( new Random(), count );
        }

        /// <summary> Gets the random normal doubles in this collection. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="seed">  The seed. </param>
        /// <param name="count"> Number of. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the random normal doubles in this
        /// collection.
        /// </returns>
        public static IEnumerable<double> GetRandomNormalDoubles( int seed, int count )
        {
            return GetRandomNormalDoubles( new Random( seed ), count );
        }

        /// <summary> Gets the random normal doubles in this collection. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="rnd">   The random. </param>
        /// <param name="count"> Number of. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the random normal doubles in this
        /// collection.
        /// </returns>
        public static IEnumerable<double> GetRandomNormalDoubles( Random rnd, int count )
        {
            var l = new List<double>();
            for ( int i = 1, loopTo = count; i <= loopTo; i++ )
                l.Add( rnd.NextNormal() );
            return l.ToArray();
        }
    }
}