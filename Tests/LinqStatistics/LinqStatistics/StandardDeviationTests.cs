using System;

using isr.Core.LinqStatistics.EnumerableStats;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.LinqStatisticsTests
{

    /// <summary> A population standard deviation tests. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-04-13 </para>
    /// </remarks>
    [TestClass]
    public class PopulationStandardDeviationTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( LinqStatisticsTests.PopulationStandardDeviationTests.TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( LinqStatisticsTests.TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );

            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        /// <summary> (Unit Test Method) sigma double. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod]
        public void SigmaDouble()
        {
            var source = TestData.GetDoubles();
            double result = source.PopulationStandardDeviation();
            Assert.AreEqual( 1.75552698640608d, result, 0.000000000001d );
        }

        /// <summary> (Unit Test Method) sigma nullable double. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod]
        public void SigmaNullableDouble()
        {
            var source = TestData.GetNullableDoubles();
            var result = source.PopulationStandardDeviation();
            Assert.AreEqual( 1.75552698640608d, ( double ) result, 0.000000000001d );
        }

        /// <summary> (Unit Test Method) sigma int. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod]
        public void SigmaInt()
        {
            var source = TestData.GetInts();
            double result = source.PopulationStandardDeviation();
            Assert.AreEqual( 1.4790199457749d, result, 0.000000000001d );
        }

        /// <summary> (Unit Test Method) sigma nullable int. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod]
        public void SigmaNullableInt()
        {
            var source = TestData.GetNullableInts();
            var result = source.PopulationStandardDeviation();
            Assert.AreEqual( 1.4790199457749d, ( double ) result, 0.000000000001d );
        }

        /// <summary> (Unit Test Method) sigma population double. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod]
        public void SigmaPopulationDouble()
        {
            var source = TestData.GetDoubles();
            double result = source.PopulationStandardDeviation();
            Assert.AreEqual( 1.7555269864060763d, result, 0.000000000001d );
        }

        /// <summary> (Unit Test Method) sigma population nullable double. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod]
        public void SigmaPopulationNullableDouble()
        {
            var source = TestData.GetNullableDoubles();
            var result = source.PopulationStandardDeviation();
            Assert.AreEqual( 1.7555269864060763d, ( double ) result, 0.000000000001d );
        }

        /// <summary> (Unit Test Method) sigma population int. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod]
        public void SigmaPopulationInt()
        {
            var source = TestData.GetInts();
            double result = source.PopulationStandardDeviation();
            Assert.AreEqual( 1.479019945774904d, result, 0.000000000001d );
        }

        /// <summary> (Unit Test Method) sigma population nullable int. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod]
        public void SigmaPopulationNullableInt()
        {
            var source = TestData.GetNullableInts();
            var result = source.PopulationStandardDeviation();
            Assert.AreEqual( 1.479019945774904d, ( double ) result, 0.000000000001d );
        }
    }
}
