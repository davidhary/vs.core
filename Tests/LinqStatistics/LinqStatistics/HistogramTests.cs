using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Windows;

using isr.Core.LinqStatistics.EnumerableStats;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.LinqStatisticsTests
{

    /// <summary> Summary description for HistogramTests. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-04-13 </para>
    /// </remarks>
    [TestClass]
    public class HistogramTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( LinqStatisticsTests.HistogramTests.TestInfo.TimeZoneOffset ) < expectedUpperLimit,
                $"{nameof( LinqStatisticsTests.TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );

            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        /// <summary> (Unit Test Method) tests histogram. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "dummyResult" )]
        [TestMethod]
        public void HistogramTest()
        {
            bool useLinq = false;
            // use a fixed seed to get a predictable random.
            var source = TestData.GetRandomNormalDoubles( 1, 10000 );
            double lowerLimit = -2;
            double upperLimit = 2d;
            int count = 20;
            double binWidth = (upperLimit - lowerLimit) / count;
            var sw = Stopwatch.StartNew();
            IEnumerable<Point> result;
            long linqSpeed;
            long directSpeed;
            if ( useLinq )
            {
                result = source.Histogram( lowerLimit, upperLimit, count );
                linqSpeed = sw.ElapsedTicks;
                sw.Restart();
                _ = source.HistogramDirect( lowerLimit, upperLimit, count );
                directSpeed = sw.ElapsedTicks;
            }
            else
            {
                _ = source.Histogram( lowerLimit, upperLimit, count );
                linqSpeed = sw.ElapsedTicks;
                sw.Restart();
                result = source.HistogramDirect( lowerLimit, upperLimit, count );
                directSpeed = sw.ElapsedTicks;
            }
            // Linq speed 33235 could be over three times longer than direct 9143 speed.
            Assert.IsTrue( directSpeed < linqSpeed, $"Expected direct speed {directSpeed} to be lower than linq speed {linqSpeed}" );

            // count test: There are two extra bins above the high and below the low limits.
            Assert.AreEqual( result.Count(), count + 2 );

            // abscissa range test: First bin is at the low limit; last is at the high limit.
            Assert.AreEqual( lowerLimit, result.ElementAtOrDefault( 0 ).X, 0.1d * binWidth );
            Assert.AreEqual( upperLimit, result.ElementAtOrDefault( count + 1 ).X, 0.1d * binWidth );

            // Second bin is at half the bin width past the low limit; last off by half bin width of the last bin.
            Assert.AreEqual( lowerLimit + 0.5d * binWidth, result.ElementAtOrDefault( 1 ).X, 0.1d * binWidth );
            Assert.AreEqual( upperLimit - 0.5d * binWidth, result.ElementAtOrDefault( count ).X, 0.1d * binWidth );

            // expected value assuming random returns the same values each time.
            int expectedLowCount = 208;
            Assert.AreEqual( expectedLowCount, ( int ) result.ElementAtOrDefault( 0 ).Y );
            int expectedHighCount = 230;
            Assert.AreEqual( expectedHighCount, ( int ) result.ElementAtOrDefault( count + 1 ).Y );
        }
    }
}
