using System;
using System.Collections.Generic;
using System.Linq;

using isr.Core.LinqStatistics.EnumerableStats;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.LinqStatisticsTests
{

    /// <summary> A mode tests. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-04-13 </para>
    /// </remarks>
    [TestClass]
    public class ModeTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( LinqStatisticsTests.ModeTests.TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( LinqStatisticsTests.TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );

            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        /// <summary> (Unit Test Method) mode uniform. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod]
        public void ModeUniform()
        {
            IEnumerable<int> source = new int[] { 1, 1, 1 };
            var result = source.Mode();
            Assert.IsTrue( result.GetValueOrDefault( 0 ) == 1 );
        }

        /// <summary> (Unit Test Method) mode none. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod]
        public void ModeNone()
        {
            IEnumerable<int> source = new int[] { 1, 2, 3 };
            var result = source.Mode();
            Assert.IsFalse( result.HasValue );
        }

        /// <summary> (Unit Test Method) mode one. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod]
        public void ModeOne()
        {
            IEnumerable<int> source = new int[] { 1, 2, 2, 3 };
            var result = source.Mode();
            Assert.IsTrue( result.GetValueOrDefault( 0 ) == 2 );
        }

        /// <summary> (Unit Test Method) mode two. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod]
        public void ModeTwo()
        {
            IEnumerable<int> source = new int[] { 1, 2, 2, 3, 3, 3 };
            var result = source.Mode();
            Assert.IsTrue( result.GetValueOrDefault( 0 ) == 3 );
        }

        /// <summary> (Unit Test Method) mode three. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod]
        public void ModeThree()
        {
            IEnumerable<int> source = new int[] { 1, 2, 2, 3, 3 };
            var result = source.Mode();
            Assert.IsTrue( result.GetValueOrDefault( 0 ) == 2 );
        }

        /// <summary> (Unit Test Method) mode nullable. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod]
        public void ModeNullable()
        {
            IEnumerable<int?> source = new int?[] { 1, 3, 2, 3, default, 2, 3 };
            var result = source.Mode();
            Assert.IsTrue( result.GetValueOrDefault( 0 ) == 3 );
        }

        /// <summary> (Unit Test Method) mode multiple. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod]
        public void ModeMultiple()
        {
            IEnumerable<int> source = new int[] { 1, 3, 2, 2, 3 };
            var result = source.Modes();
            Assert.IsTrue( result.SequenceEqual( new int[] { 2, 3 } ) );
        }

        /// <summary> (Unit Test Method) modes multiple 2. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod]
        public void ModesMultiple2()
        {
            IEnumerable<int> source = new int[] { 1, 3, 2, 2, 3, 3 };
            var result = source.Modes();
            Assert.IsTrue( result.SequenceEqual( new int[] { 3, 2 } ) );
        }

        /// <summary> (Unit Test Method) modes multiple nullable. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod]
        public void ModesMultipleNullable()
        {
            IEnumerable<int?> source = new int?[] { 1, 2, default, 2, 3, 3, 3 };
            var result = source.Modes();
            Assert.IsTrue( result.SequenceEqual( new int[] { 3, 2 } ) );
        }

        /// <summary> (Unit Test Method) modes single value. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod]
        public void ModesSingleValue()
        {
            IEnumerable<int> source = new int[] { 1 };
            var result = source.Mode();
            Assert.IsTrue( result is null );
        }
    }
}
