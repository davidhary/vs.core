using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;

using isr.Core.LinqStatistics.EnumerableStats;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.LinqStatisticsTests
{

    /// <summary> A covariance tests. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-04-13 </para>
    /// </remarks>
    [TestClass]
    public class CovarianceTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( LinqStatisticsTests.CovarianceTests.TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( LinqStatisticsTests.TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        /// <summary> (Unit Test Method) covariances this object. </summary>
        /// <remarks>
        /// Covariance 1.4486ms Using IEnumerable from Double()
        /// Covariance 0.0017ms Using IEnumerable from Double()
        /// Covariance 0.0393ms Using IList.
        /// </remarks>
        [TestMethod]
        public void Covariance()
        {
            var source = TestData.GetDoubles();
            var other = TestData.GetDoubles();
            var sw = Stopwatch.StartNew();
            double result = source.Covariance( other );
            var elapsed = sw.Elapsed;
            TestInfo.TraceMessage( $"{nameof( LinqStatisticsTests.CovarianceTests.Covariance )} {elapsed.TotalMilliseconds}ms Using {nameof( System.Collections.Generic.IEnumerable<Double> )}" );
            Assert.AreEqual( result, 3.081875d, double.Epsilon );

            sw.Restart();
            result = source.Covariance( other );
            elapsed = sw.Elapsed;
            TestInfo.TraceMessage( $"{nameof( LinqStatisticsTests.CovarianceTests.Covariance )} {elapsed.TotalMilliseconds}ms Using {nameof( System.Collections.Generic.IEnumerable<Double> )} from Double()" );
            Assert.AreEqual( result, 3.081875d, double.Epsilon );
            IList<double> sourceList = TestData.GetDoubles().ToList();
            IList<double> otherList = TestData.GetDoubles().ToList();

            sw.Restart();
            result = sourceList.Covariance( otherList );
            elapsed = sw.Elapsed;
            TestInfo.TraceMessage( $"{nameof( LinqStatisticsTests.CovarianceTests.Covariance )} {elapsed.TotalMilliseconds}ms Using {nameof( System.Collections.Generic.IList<Double> )}" );
            Assert.AreEqual( result, 3.081875d, double.Epsilon );
        }

        /// <summary> (Unit Test Method) covariance 1. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod]
        public void Covariance1()
        {
            var source = TestData.GetDoubles();
            var other = TestData.GetInts().Select( x => ( double ) x );
            double result = source.Covariance( other );
            Assert.AreEqual( result, 2.59375d, double.Epsilon );
        }

        /// <summary> (Unit Test Method) pearson identity. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod]
        public void PearsonIdentity()
        {
            var source = TestData.GetDoubles();
            var other = TestData.GetDoubles();
            double result = source.Pearson( other );
            Assert.AreEqual( result, 1.0d, double.Epsilon );
        }

        /// <summary> (Unit Test Method) pearson 1. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod]
        public void Pearson1()
        {
            var source = TestData.GetDoubles();
            var other = TestData.GetInts().Select( x => ( double ) x );
            double result = source.Pearson( other );
            Assert.AreEqual( result, 0.998956491208287d, 0.000000000000001d );
        }
    }
}
