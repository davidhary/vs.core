using System;
using System.Diagnostics;
using System.Linq;

using isr.Core.Statistics;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.Statistics.MSTest
{

    /// <summary> tests of equalities. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-10 </para>
    /// </remarks>
    [TestClass()]
    public class SampleStatisticsTests
    {

        #region " CONSTRUCTION & CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        public static void TestClassInitialize( TestContext testContext )
        {
            try
            {
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }


        #endregion

        #region " SAMPLE STATISTICS TEST "

        /// <summary>   Gets sample data for dataset 212. </summary>
        /// <remarks>   David, 2021-03-20. </remarks>
        /// <returns>   An array of double. </returns>
        private static double[] GetSampleDataSet212()
        {
            return new double[]{ 129.522766, 128.54892, 133.528946, 133.528946, 130.80545, 131.497849, 129.822311, 130.619736, 130.619736, 131.314987,
                                 124.853378, 123.576088, 127.914642, 124.352127, 120.485474, 121.302933, 126.351425, 122.33313, 126.24984, 122.660942,
                                 127.323532, 124.516022, 121.652748, 108.289627, 104.939804, 134.836807, 134.836807, 133.528946, 127.209991 };
        }

        /// <summary>   Gets filter indexes 212. </summary>
        /// <remarks>   David, 2021-06-05. </remarks>
        /// <returns>   An array of int. </returns>
        private static int[] GetFilterIndexes212()
        {
            return new int[] { 9, 28, 19, 1 };
        }

        /// <summary>   Gets filtered data set 212. </summary>
        /// <remarks>   David, 2021-06-05. </remarks>
        /// <returns>   An array of double. </returns>
        private static double[] GetFilteredDataSet212()
        {
            return new double[]{ 129.522766, 133.528946, 133.528946, 130.80545, 131.497849, 129.822311, 130.619736, 130.619736,
                                 124.853378, 123.576088, 127.914642, 124.352127, 120.485474, 121.302933, 126.351425, 122.33313, 126.24984,
                                 127.323532, 124.516022, 121.652748, 108.289627, 104.939804, 134.836807, 134.836807, 133.528946 };
        }

        /// <summary>   Gets expected values for dataset 212. </summary>
        /// <remarks>   David, 2021-03-20. </remarks>
        /// <returns>   The expected values. </returns>
        private static (double mean, double median, double sigma, int count) GetExpectedValuesDataSet212()
        {
            return (126.45, 127.32, 6.94, 29);
        }

        /// <summary>   Gets the outliers for dataset 212. </summary>
        /// <remarks>   David, 2021-03-20. </remarks>
        /// <returns>   An array of double. </returns>
        private static double[] GetOutliersDataSet212()
        {
            return new double[] { 104.939804, 108.289627 };
        }

        private static double[] GetQuartilesOutliersDataSet212()
        {
            return new double[] { 108.289627, 104.939804 };
        }

        /// <summary>   Gets sample for data set 22A. </summary>
        /// <remarks>   David, 2021-03-20. </remarks>
        /// <returns>   An array of double. </returns>
        private static double[] GetSampleDataSet22A()
        {
            return new double[]{ 122.594383, 130.788406, 112.898605, 126.558243, 132.898605, 139.834351, 140.803284, 128.238678, 126.770103, 140.857407,
                                 140.128815, 136.775009, 147.673355, 137.050766, 140.433624, 134.918625, 140.433624, 135.168793, 142.932388, 131.171402,
                                 136.442169, 128.911728, 127.793159, 132.070755, 112.599800, 141.417984, 140.590134, 128.276871, 108.251389 };
        }

        /// <summary>   Gets expected values for dataset 22A. </summary>
        /// <remarks>   David, 2021-03-20. </remarks>
        /// <returns>   The expected values. </returns>
        private static (double mean, double median, double sigma, int count) GetExpectedValuesDataSet22A()
        {
            return (132.60, 134.92, 9.52, 29);
        }

        /// <summary>   Gets the outliers for dataset 22A. </summary>
        /// <remarks>   David, 2021-03-20. </remarks>
        /// <returns>   An array of double. </returns>
        private static double[] GetOutliersDataSet22A()
        {
            return new double[] { 108.251389, 112.599800, 112.898605 };
        }

        /// <summary>   Gets quartiles outliers data set 22 a. </summary>
        /// <remarks>   David, 2021-03-20. </remarks>
        /// <returns>   An array of double. </returns>
        private static double[] GetQuartilesOutliersDataSet22A()
        {
            return new double[] { 108.251389 };
        }


        /// <summary>   Assert removing by index. </summary>
        /// <remarks>   David, 2021-06-05. </remarks>
        /// <param name="originalData">     Information describing the original. </param>
        /// <param name="excludeIndexes">   The exclude indexes. </param>
        /// <param name="expectedData">     Information describing the expected. </param>
        private static void AssertRemovingByIndex( double[] originalData, int[] excludeIndexes, double[] expectedData )
        {
            isr.Core.Statistics.SampleStatistics sample = new();
            sample.AddValues( originalData );
            System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
            sample.RemoveValuesAt( excludeIndexes );
            sw.Stop();
            Console.Out.WriteLine( $"{nameof( SampleStatistics.RemoveValuesAt )} {sw.ElapsedTicks} ticks" );
            Assert.AreEqual( sample.ValuesArray().Length, expectedData.Length, $"{nameof( SampleStatistics.ValuesArray )} length should match after {nameof( SampleStatistics.RemoveValuesAt )}" );
            for ( int i = 0; i < sample.ValuesArray().Length; i++ )
            {
                Assert.AreEqual( sample.ValuesArray()[i], expectedData[i], $"{nameof( SampleStatistics.ValuesArray )} at index {i} should match after {nameof( SampleStatistics.RemoveValuesAt )}"[i] );
            }

            sample.ClearKnownState();
            sw.Restart();
            sample.AddValues( originalData, excludeIndexes );
            sw.Stop();
            Console.Out.WriteLine( $"{nameof( SampleStatistics.AddValues )} {sw.ElapsedTicks} ticks" );
            Assert.AreEqual( sample.ValuesArray().Length, expectedData.Length, $"{nameof( SampleStatistics.ValuesArray )} length should match after {nameof( SampleStatistics.AddValues )}" );
            for ( int i = 0; i < sample.ValuesArray().Length; i++ )
            {
                Assert.AreEqual( sample.ValuesArray()[i], expectedData[i], $"{nameof( SampleStatistics.ValuesArray )} at index {i} should match after {nameof( SampleStatistics.AddValues )}"[i] );
            }
        }

        /// <summary>   (Unit Test Method) data set 212 statistics should remove by index. </summary>
        /// <remarks>   David, 2021-06-05. </remarks>
        [TestMethod()]
        public void DataSet212StatisticsShouldRemoveByIndex()
        {
            AssertRemovingByIndex( GetSampleDataSet212(), GetFilterIndexes212(), GetFilteredDataSet212() );
        }

        /// <summary>   (Unit Test Method) mean median sigma count for dataset 212 should match. </summary>
        /// <remarks>   David, 2020-09-22. </remarks>
        [TestMethod()]
        public void DataSet212StatisticsShouldMatch()
        {
            AssetDatasetStatistics( GetSampleDataSet212(), GetOutliersDataSet212(), GetQuartilesOutliersDataSet212(), GetExpectedValuesDataSet212() );
        }

        /// <summary>   Asset dataset statistics. </summary>
        /// <remarks>   David, 2021-05-25. </remarks>
        /// <param name="sampleData">           Information describing the sample. </param>
        /// <param name="outlierData">          Information describing the outlier. </param>
        /// <param name="quartileOutlierData">  Information describing the quartile outlier. </param>
        /// <param name="expectedValues">       The expected values. </param>
        private static void AssetDatasetStatistics( double[] sampleData, double[] outlierData, double[] quartileOutlierData,
                                                    (double mean, double median, double sigma, int count) expectedValues )
        {
            isr.Core.Statistics.SampleQuartiles quartiles = new();
            quartiles.Sample.AddValues( sampleData );
            quartiles.Sample.Evaluate();
            quartiles.Evaluate();
            // (double mean, double median, double sigma, int count) = GetExpectedValuesDataSet22A();
            Assert.AreEqual( expectedValues.count, quartiles.Sample.Count, $"{nameof( SampleStatistics.Count )} should match" );
            Assert.AreEqual( expectedValues.mean, quartiles.Sample.Mean, 0.02, $"{nameof( SampleStatistics.Mean )} should match" );
            Assert.AreEqual( expectedValues.median, quartiles.Quartiles.Median, 0.02, $"{nameof( Quartiles.Median )} should match" );
            Assert.AreEqual( expectedValues.sigma, quartiles.Sample.Sigma, 0.02, $"{nameof( SampleStatistics.Sigma )} should match" );
            quartiles.FilterSample();
            Assert.AreEqual( quartileOutlierData.Length, quartiles.OutlierCount, $"{nameof( SampleQuartiles.OutlierCount )} should match" );
            for ( int i = 0; i < quartiles.OutlierIndexes().Length; i++ )
            {
                Assert.AreEqual( quartileOutlierData[i], sampleData[quartiles.OutlierIndexes()[i]], 0.000001, $"Quartile outlier value at {i} should match" );
            }

            (int[] indexs, double[] sortedValues) = SampleSkewness.FindOutliersSorted( sampleData, -0.66, 0.66 );
            foreach ( int i in indexs )
            {
                Assert.AreEqual( outlierData[i], sortedValues[i], 0.000001, $"skewness outlier value at {i} should match" );
            }

            indexs = SampleSkewness.FindOutliers( sampleData, -0.66, 0.66 ).ToArray();
            for ( int i = 0; i < indexs.Length; i++ )
            {
                Assert.AreEqual( outlierData[i], sampleData[indexs[i]], 0.000001, $"skewness outlier value at {i} should match" );
            }

            SampleSkewness sampleSkew = new() {
                LowerFence = -0.66,
                UpperFence = 0.66
            };
            sampleSkew.Sample.AddValues( sampleData );
            sampleSkew.FilterSample();
            for ( int i = 0; i < indexs.Length; i++ )
            {
                Assert.AreEqual( indexs[i], sampleSkew.OutlierIndexes()[i], $"skewness outlier indexes at {i} should match" );
            }
        }

        /// <summary>   (Unit Test Method) mean median sigma count for dataset 22A should match. </summary>
        /// <remarks>   David, 2020-09-22. </remarks>
        [TestMethod()]
        public void DataSet22AStatisticsShouldMatch()
        {
            AssetDatasetStatistics( GetSampleDataSet22A(), GetOutliersDataSet22A(), GetQuartilesOutliersDataSet22A(), GetExpectedValuesDataSet22A() );
        }


        #endregion

        #region " SKEWNESS "

        /// <summary>   (Unit Test Method) skewness should match. </summary>
        /// <remarks>   David, 2021-03-20. </remarks>
        [TestMethod()]
        public void SkewnessShouldMatch()
        {
            double[] values = new double[] { 13, 18, 25, 30, 52 };
            double expectedSkewness = 0.8529;
            double expectedSigma = 13.5144;
            (double skewness, double sigma) = SampleSkewness.EvaluateSkewness( values );
            Assert.AreEqual( expectedSigma, sigma, 0.0002, $"{nameof( SampleSkewness.Sigma )} should match" );
            Assert.AreEqual( expectedSkewness, skewness, 0.0002, $"{nameof( SampleSkewness.Skewness)} should match" );
        }

        /// <summary>   (Unit Test Method) critical values should match. </summary>
        /// <remarks>   David, 2021-03-20. </remarks>
        [TestMethod()]
        public void CriticalValuesShouldMatch()
        {
            double[] criticalValues = new double[] { 0.66, 1.0, 0.98, 0.13, 0.13, 0.625 };
            int[] sampleSizes = new int[] { 30, 5, 6, 1000, 1001, 35 };
            for ( int i = 0; i < criticalValues.Length; i++ )
            {
                int sampleSize = sampleSizes[i];
                double expectedCriticalValue = criticalValues[i];
                Assert.AreEqual( expectedCriticalValue, SampleSkewness.CriticalValue( sampleSize ), 0.001, $"{nameof( SampleSkewness.CriticalValue )} should match" );
            }
        }


        #endregion


    }
}
