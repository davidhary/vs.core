using System;

using isr.Core.Constructs;

using Microsoft.VisualStudio.TestTools.UnitTesting;

#pragma warning disable IDE1006 // Naming Styles
namespace isr.Core.ConstructsTests
#pragma warning restore IDE1006 // Naming Styles
{

    /// <summary> Functions tests. </summary>
    /// <remarks>
    /// David, 2019-02-03. https://github.com/polterguy/lizzie/. <para>
    /// Copyright (c) 2018 Thomas Hansen - thomas@gaiasoul.com </para><para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    [TestClass()]
    public class ChainTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Constructs.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( ChainTests.TestInfo.TimeZoneOffset ) < expectedUpperLimit,
                           $"{ nameof( ConstructsTests.TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " EVALUATE TESTS "

        /// <summary> (Unit Test Method) evaluates this object. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        [TestMethod()]
        public void Evaluate()
        {
            var chain = new Chain<string>() { input => input + "1", input => input + "2", input => input + "3" };
            string result = chain.EvaluateChain( "initial_" );
            Assert.AreEqual( "initial_123", result );
        }
        #endregion

    }
}
