using System;
using System.Threading;

using isr.Core.Constructs;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.ConstructsTests
{

    /// <summary> Functions tests. </summary>
    /// <remarks>
    /// David, 2019-02-03, https://github.com/polterguy/lizzie/. <para>
    /// Copyright (c) 2018 Thomas Hansen - thomas@gaiasoul.com </para><para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    [TestClass()]
    public class ActionsTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Constructs.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( ActionsTests.TestInfo.TimeZoneOffset ) < expectedUpperLimit,
                           $"{ nameof( ConstructsTests.TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " EXECUTE TESTS "

        /// <summary> (Unit Test Method) executes this object. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        [TestMethod()]
        public void Execute()
        {
            string result = string.Empty;
            var actions = new Actions() { () => result += "foo", () => result += "bar" };
            actions.Execute();
            Assert.AreEqual( "foobar", result );
        }

        /// <summary> (Unit Test Method) executes the arguments operation. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        [TestMethod()]
        public void ExecuteArgs()
        {
            var sequence = new Actions<Mutable<string>>() { arg => arg.Value += "foo", arg => arg.Value += "bar" };
            var mutable = new Mutable<string>( "initial_" );
            sequence.Execute( mutable );
            Assert.AreEqual( "initial_foobar", mutable.Value );
        }

        /// <summary> Executes the parallel operation. </summary>
        /// <remarks>
        /// Creates two lambdas that run in parallel. If you look carefully at this code, you’ll notice
        /// that the result of the evaluation doesn’t assume that any one of my lambdas is being executed
        /// before the other. The order of execution is not explicitly specified, and these lambdas are
        /// being executed on separate threads. This is because the Actions class in Figure 3lets you add
        /// delegates to it, so you can later decide if you want to execute the delegates in parallel or
        /// sequentially.
        /// </remarks>
        /// <param name="initialValue"> The initial value. </param>
        /// <param name="oneValue">     The one value. </param>
        /// <param name="otherValue">   The other value. </param>
        private static void ExecuteParallel( string initialValue, string oneValue, string otherValue )
        {
            string result = string.Empty;
            using ( var sync = new Synchronizer<string, string, string>( initialValue ) )
            {
                var actions = new Actions() { () => sync.Assign( res => res + oneValue ), () => sync.Assign( res => res + otherValue ) };
                actions.ExecuteParallel();
                sync.Read( ( val ) => result = val );
            }

            Assert.AreEqual( true, ($"{initialValue}{oneValue}{otherValue}" ?? "") == (result ?? "") || (result ?? "") == ($"{initialValue}{otherValue}{oneValue}" ?? "") );
        }

        /// <summary> (Unit Test Method) executes the parallel test 1 operation. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        [TestMethod()]
        public void ExecuteParallelTest1()
        {
            ExecuteParallel( "initial_", "foot", "bar" );
            using var sync = new Synchronizer<string, string, string>( "initial_" );
            var actions = new Actions() { () => sync.Assign( res => res + "foo" ), () => sync.Assign( res => res + "bar" ) };
            actions.ExecuteParallel();
            string result = null;
            sync.Read( ( val ) => result = val );
            Assert.AreEqual( true, "initial_foobar" == result || result == "initial_barfoo" );
        }

        /// <summary> (Unit Test Method) executes the parallel test 2 operation. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        [TestMethod()]
        public void ExecuteParallelTest2()
        {
            string result = string.Empty;
            var wait = new ManualResetEvent( false );
            var actions = new Actions()
            {
                () =>
                {
                    result += "foo";
                    _ = wait.Set();
                },
                () =>
                {
                    _ = wait.WaitOne();
                    result += "bar";
                }
            };
            actions.ExecuteParallel();
            Assert.AreEqual( "foobar", result );
        }

        /// <summary> (Unit Test Method) executes the parallel test 3 operation. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        [TestMethod()]
        public void ExecuteParallelTest3()
        {
            string result = string.Empty;
            var wait = new ManualResetEvent( false );
            var actions = new Actions()
            {
                () =>
                {
                    _ = wait.WaitOne();
                    result += "foo";
                },
                () =>
                {
                    result += "bar";
                    _ = wait.Set();
                }
            };
            actions.ExecuteParallel();
            Assert.AreEqual( "barfoo", result );
        }

        /// <summary> (Unit Test Method) executes the parallel unblocked operation. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        [TestMethod()]
        public void ExecuteParallelUnblocked()
        {
            string result = string.Empty;
            var waits = new ManualResetEvent[] { new ManualResetEvent( false ), new ManualResetEvent( false ), new ManualResetEvent( false ) };
            var actions = new Actions()
            {
                () =>
                {
                    result += "1";
                    _ = waits[0].Set();
                },
                () =>
                {
                    result += "2";
                    _ = waits[1].Set();
                },
                () =>
                {
                    result += "3";
                    _ = waits[2].Set();
                }
            };
            actions.ExecuteParallelUnblocked();
            foreach ( ManualResetEvent e in waits )
            {
                _ = e.WaitOne();
            }
            // this fails on STA threads: WaitHandle.WaitAll(waits)
            bool outcome = result == "123" || result == "132" || result == "231" || result == "213" || result == "312" || result == "321";
            Assert.AreEqual( true, outcome, $"{result} unexpected" );
        }

        /// <summary> (Unit Test Method) executes the parallel unblocked argument operation. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        [TestMethod()]
        public void ExecuteParallelUnblockedArg()
        {
            var waits = new EventWaitHandle[] { new EventWaitHandle( false, EventResetMode.ManualReset ), new EventWaitHandle( false, EventResetMode.ManualReset ), new EventWaitHandle( false, EventResetMode.ManualReset ) };
            var actions = new Actions<Synchronizer<string>>()
            {
                (input) =>
                {
                    _ = waits[1].WaitOne();
                    input.Assign(current => current + "1");
                    _ = waits[0].Set();
                },
                (input) =>
                {
                    _ = waits[2].WaitOne();
                    input.Assign(current => current + "2");
                    _ = waits[1].Set();
                },
                (input) =>
                {
                    input.Assign(current => current + "3");
                    _ = waits[2].Set();
                }
            };
            using var sync = new Synchronizer<string>( "initial_" );
            actions.ExecuteParallelUnblocked( sync );
            foreach ( EventWaitHandle e in waits )
            {
                _ = e.WaitOne();
            }
            // this fails on STA threads: WaitHandle.WaitAll(waits)
            string res = null;
            sync.Read( ( val ) => res = val );
            Assert.AreEqual( "initial_321", res );
        }
        #endregion

    }
}
