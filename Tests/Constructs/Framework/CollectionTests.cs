using System;
using System.Collections.Generic;
using System.Linq;

using isr.Core.Constructs;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.ConstructsTests
{

    /// <summary> tests of collections. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-22 </para>
    /// </remarks>
    [TestClass()]
    public class CollectionTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Constructs.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( CollectionTests.TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{ nameof( ConstructsTests.TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " LIST DICTIONARY  "

        /// <summary> Assert contains. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="dict">         The dictionary. </param>
        /// <param name="keyValuePair"> The key value pair. </param>
        private static void AssertContains( ListDictionary<string, int> dict, KeyValuePair<string, int> keyValuePair )
        {
            Assert.AreEqual( keyValuePair.Value, dict[keyValuePair.Key] );
            Assert.IsTrue( dict.ContainsKey( keyValuePair.Key ) );
            Assert.IsTrue( dict.Keys.Contains( keyValuePair.Key ) );
            Assert.IsTrue( dict.Values.Contains( keyValuePair.Value ) );
        }

        /// <summary> Assert first. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="dict">         The dictionary. </param>
        /// <param name="keyValuePair"> The key value pair. </param>
        private static void AssertFirst( ListDictionary<string, int> dict, KeyValuePair<string, int> keyValuePair )
        {
            Assert.AreEqual( keyValuePair.Key, dict.FirstKey );
            Assert.AreEqual( keyValuePair.Key, dict.Entries.First().Key );
            Assert.AreEqual( keyValuePair.Value, dict.Entries.First().Value );
        }

        /// <summary> Assert last. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="dict">         The dictionary. </param>
        /// <param name="keyValuePair"> The key value pair. </param>
        private static void AssertLast( ListDictionary<string, int> dict, KeyValuePair<string, int> keyValuePair )
        {
            Assert.AreEqual( keyValuePair.Key, dict.LastKey );
            Assert.AreEqual( keyValuePair.Key, dict.Entries.Last().Key );
            Assert.AreEqual( keyValuePair.Value, dict.Entries.Last().Value );
        }

        /// <summary> (Unit Test Method) tests list dictionary. </summary>
        /// <remarks> David, 2020-05-22. </remarks>
        [TestMethod()]
        public void ListDictionaryTest()
        {
            var dict = new ListDictionary<string, int>();
            int count = 0;
            var firstKeyValuePair = new KeyValuePair<string, int>( "foo", 33 );
            dict[firstKeyValuePair.Key] = firstKeyValuePair.Value;
            count += 1;
            var middleKeyValuePair = new KeyValuePair<string, int>( "art", 23 );
            dict[middleKeyValuePair.Key] = middleKeyValuePair.Value;
            count += 1;
            Assert.AreEqual( count, dict.Count );
            var lastKeyValuePair = new KeyValuePair<string, int>( "bar", 13 );
            dict[lastKeyValuePair.Key] = lastKeyValuePair.Value;
            count += 1;
            Assert.AreEqual( count, dict.Count );
            AssertContains( dict, firstKeyValuePair );
            AssertFirst( dict, firstKeyValuePair );
            AssertContains( dict, middleKeyValuePair );
            AssertContains( dict, lastKeyValuePair );
            AssertLast( dict, lastKeyValuePair );
            Assert.IsTrue( !dict.ContainsKey( "bar1" ) );

            // move first to last
            // firstKeyValuePair = New KeyValuePair(Of String, Integer)(middleKeyValuePair.Key, middleKeyValuePair.Value)
            // middleKeyValuePair = New KeyValuePair(Of String, Integer)(lastKeyValuePair.Key, lastKeyValuePair.Value)
            // lastKeyValuePair = New KeyValuePair(Of String, Integer)("foo", 42)

            firstKeyValuePair = new KeyValuePair<string, int>( "foo", 42 );
            dict[firstKeyValuePair.Key] = firstKeyValuePair.Value;
            AssertContains( dict, firstKeyValuePair );
            AssertFirst( dict, firstKeyValuePair );
            AssertContains( dict, middleKeyValuePair );
            AssertContains( dict, lastKeyValuePair );
            AssertLast( dict, lastKeyValuePair );
        }

        #endregion

    }
}

#if OldCode
ListDictionary<string, int> dict = new ListDictionary<string, int>();

dict["foo"] = 1;
dict["bar"] = 2;

Assert.AreEqual( 1, dict["foo"] );
Assert.AreEqual( 2, dict["bar"] );

Assert.AreEqual( 2, dict.Count );

Assert.AreEqual( "foo", dict.FirstKey );

Assert.AreEqual( "foo", dict.Entries.First().Key );
Assert.AreEqual( 1, dict.Entries.First().Value );

Assert.AreEqual( "bar", dict.Entries.Last().Key );
Assert.AreEqual( 2, dict.Entries.Last().Value );

Assert.IsTrue( dict.Keys.Contains( "foo" ) );
Assert.IsTrue( dict.Values.Contains( 1 ) );

Assert.IsTrue( dict.Keys.Contains( "bar" ) );
Assert.IsTrue( dict.Values.Contains( 2 ) );

Assert.IsTrue( dict.ContainsKey( "foo" ) );
Assert.IsTrue( dict.ContainsKey( "bar" ) );
Assert.IsTrue( !dict.ContainsKey( "let" ) );

dict["foo"] = 42;
Assert.AreEqual( 42, dict["foo"] );

#endif
