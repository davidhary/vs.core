using System;

using isr.Core.Constructs;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.ConstructsTests
{

    /// <summary> tests of equalities. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-10 </para>
    /// </remarks>
    [TestClass()]
    public class RangeTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Constructs.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( RangeTests.TestInfo.TimeZoneOffset ) < expectedUpperLimit,
                            $"{ nameof( ConstructsTests.TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " RANGE TESTS "

        /// <summary> (Unit Test Method) tests range equality. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        [TestMethod()]
        public void RangeEqualityTest()
        {
            var left = new RangeR( 0d, 0d );
            var right = new RangeR( 0d, 0d );
            Assert.IsTrue( left == right, $"Left.{EqualityTests.InfoGetter( left )} == right.{EqualityTests.InfoGetter( right )}" );
            Assert.IsFalse( ReferenceEquals( left, right ), $"a. Object.ReferenceEquals(Left.{EqualityTests.InfoGetter( left )}, Right.{EqualityTests.InfoGetter( right )})" );
            Assert.IsFalse( ReferenceEquals( left, right ), $"b. (Object)Left.{EqualityTests.InfoGetter( left )} Is (Object)Right.{EqualityTests.InfoGetter( right )}" );
            EqualityTests.EqualityTest( left, right );

            left = null;
            right = null;
            TestInfo.TraceMessage( $"Left.{EqualityTests.InfoGetter( left )} == right.{EqualityTests.InfoGetter( right )}" );
            Assert.IsTrue( left == right, $"Left.{EqualityTests.InfoGetter( left )} == right.{EqualityTests.InfoGetter( right )}" );
            Assert.IsTrue( ReferenceEquals( left, right ), $"a. Object.ReferenceEquals(Left.{EqualityTests.InfoGetter( left )}, Right.{EqualityTests.InfoGetter( right )})" );
            Assert.IsTrue( ReferenceEquals( left, right ), $"b. (Object)Left.{EqualityTests.InfoGetter( left )} Is (Object)Right.{EqualityTests.InfoGetter( right )}" );
            EqualityTests.EqualityTest( left, right );
        }

        /// <summary> (Unit Test Method) tests object inequality. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        [TestMethod()]
        public void RangeInequalityTest()
        {
            var left = new RangeR( 0d, 0d );
            var right = new RangeR( 1d, 0d );
            TestInfo.TraceMessage( $"Left.{EqualityTests.InfoGetter( left )} == right.{EqualityTests.InfoGetter( right )}" );
            Assert.IsFalse( left == right, $"Left.{EqualityTests.InfoGetter( left )} == right.{EqualityTests.InfoGetter( right )}" );
            EqualityTests.InequalityTest( left, right );

            left = new RangeR( 0d, 0d );
            right = null;
            TestInfo.TraceMessage( $"Left.{EqualityTests.InfoGetter( left )} == right.{EqualityTests.InfoGetter( right )}" );
            Assert.IsFalse( left == right, $"Left.{EqualityTests.InfoGetter( left )} == right.{EqualityTests.InfoGetter( right )}" );
            EqualityTests.InequalityTest( left, right );
        }

        #endregion

    }
}
