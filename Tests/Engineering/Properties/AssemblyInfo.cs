﻿using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.Core.EngineeringTests.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.Core.EngineeringTests.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.Core.EngineeringTests.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
