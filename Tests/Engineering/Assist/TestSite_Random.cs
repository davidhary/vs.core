using System;
using System.Collections.Generic;

using isr.Core.RandomExtensions;

namespace isr.Core.EngineeringTests
{
    internal partial class TestSite
    {

        /// <summary> Generates normally distributed doubles. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="seed">  The seed. </param>
        /// <param name="count"> Number of values to generate. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the random normal doubles in this
        /// collection.
        /// </returns>
        public static IList<double> GenerateRandomNormals( int seed, int count )
        {
            return GenerateRandomNormals( new Random( seed ), count );
        }

        /// <summary> Generates normally distributed doubles. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="generator"> The random number generator. </param>
        /// <param name="count">     Number of values to generate. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the random normal doubles in this
        /// collection.
        /// </returns>
        public static IList<double> GenerateRandomNormals( Random generator, int count )
        {
            var l = new List<double>();
            for ( int i = 1, loopTo = count; i <= loopTo; i++ )
            {
                l.Add( generator.NextNormal() );
            }

            return l.ToArray();
        }
    }
}
