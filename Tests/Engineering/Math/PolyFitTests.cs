using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows;

using Microsoft.VisualStudio.TestTools.UnitTesting;

#pragma warning disable IDE1006 // Naming Styles
namespace isr.Core.EngineeringTests
#pragma warning restore IDE1006 // Naming Styles
{

    /// <summary> A polygon fit tests. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-04-13 </para>
    /// </remarks>
    [TestClass()]
    public class PolyFitTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( EngineeringTests.PolyFitTests.TestInfo.TimeZoneOffset ) < expectedUpperLimit,
                                $"{nameof( EngineeringTests.TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            Assert.IsTrue( PolyFitTestInfo.Get().Exists, $"{typeof( PolyFitTestInfo )} settings should exist" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        /// <summary> Builds a determinant. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="values"> The values. </param>
        /// <returns> A Double(,) </returns>
        public static double[,] BuildDeterminant( IEnumerable<double> values )
        {
            int colCount = 3;
            int rowCount = 3;
            double[,] a;
            a = new double[colCount, rowCount];
            int i = 0;
            for ( int r = 0, loopTo = rowCount - 1; r <= loopTo; r++ )
            {
                for ( int c = 0, loopTo1 = colCount - 1; c <= loopTo1; c++ )
                {
                    a[c, r] = values.ElementAtOrDefault( i );
                    i += 1;
                }
            }

            return a;
        }

        /// <summary> (Unit Test Method) builds determinant test method. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void BuildDeterminantTestMethod()
        {
            var values = new[] { 2d, -3, -2, -6, 3d, 3d, -2, -3, -2 };
            var determinant = BuildDeterminant( values );
            int columnNumber = 0;
            int rowNumber = 0;
            double expectedValue = 2d;
            double actualValue = determinant[columnNumber, rowNumber];
            Assert.AreEqual( expectedValue, actualValue, $"(column,row)=({columnNumber},{rowNumber})" );
            columnNumber = 1;
            expectedValue = -3;
            actualValue = determinant[columnNumber, rowNumber];
            Assert.AreEqual( expectedValue, actualValue, $"(column,row)=({columnNumber},{rowNumber})" );
            rowNumber = 2;
            expectedValue = -3;
            actualValue = determinant[columnNumber, rowNumber];
            Assert.AreEqual( expectedValue, actualValue, $"(column,row)=({columnNumber},{rowNumber})" );

            // actualValue = QuadraticPolynomial.Determinant(Me.BuildDeterminant(values))
            // Assert.AreEqual(expectedValue, actualValue)
        }

        /// <summary> (Unit Test Method) calculates the determinant test method. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void CalculateDeterminantTestMethod()
        {
            var values = new[] { 2d, -3, -2, -6, 3d, 3d, -2, -3, -2 };
            double expectedValue = 12d;
            double actualValue = Engineering.QuadraticPolynomial.Determinant( BuildDeterminant( values ) );
            Assert.AreEqual( expectedValue, actualValue );
            values = new[] { -4, 5d, 2d, -3, 4d, 2d, -1, 2d, 5d };
            expectedValue = -3;
            actualValue = Engineering.QuadraticPolynomial.Determinant( BuildDeterminant( values ) );
            Assert.AreEqual( expectedValue, actualValue );
        }

        /// <summary> (Unit Test Method) Cramer substitution test method. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void CramerSubstitutionTestMethod()
        {
            var values = new[] { 2d, -1, 6d, -3, 4d, -5, 8d, -7, -9 };
            var constants = new[] { 10d, 11d, 12d };
            var coefficients = BuildDeterminant( values );
            double expectedValue = -141;
            double determinant = Engineering.QuadraticPolynomial.Determinant( coefficients );
            double actualValue = determinant;
            Assert.AreEqual( expectedValue, actualValue );
            var result = Engineering.QuadraticPolynomial.CramerSubstitution( 0, coefficients, constants );
            int columnNumber = 0;
            int rowNumber = 0;
            expectedValue = 10d;
            actualValue = result[columnNumber, rowNumber];
            Assert.AreEqual( expectedValue, actualValue, $"(column,row)=({columnNumber},{rowNumber})" );
            columnNumber = 1;
            expectedValue = -1;
            actualValue = result[columnNumber, rowNumber];
            Assert.AreEqual( expectedValue, actualValue, $"(column,row)=({columnNumber},{rowNumber})" );
            rowNumber = 2;
            expectedValue = -7;
            actualValue = result[columnNumber, rowNumber];
            Assert.AreEqual( expectedValue, actualValue, $"(column,row)=({columnNumber},{rowNumber})" );
        }

        /// <summary> (Unit Test Method) Cramer rule test method. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void CramerRuleTestMethod()
        {
            var values = new[] { 2d, -1, 6d, -3, 4d, -5, 8d, -7, -9 };
            var constants = new[] { 10d, 11d, 12d };
            var coefficients = BuildDeterminant( values );
            double expectedValue = -141;
            double determinant = Engineering.QuadraticPolynomial.Determinant( coefficients );
            double actualValue = determinant;
            Assert.AreEqual( expectedValue, actualValue );
            var result = Engineering.QuadraticPolynomial.CramerRule( coefficients, constants );
            expectedValue = -1499;
            actualValue = result[0];
            Assert.AreEqual( expectedValue, actualValue );
            expectedValue = -1492;
            actualValue = result[1];
            Assert.AreEqual( expectedValue, actualValue );
            expectedValue = 16d;
            actualValue = result[2];
            Assert.AreEqual( expectedValue, actualValue );
        }

        /// <summary> (Unit Test Method) polynomial fit test method three points. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void PolyFitTestMethodThreePoints()
        {
            var quadPoly = new Engineering.QuadraticPolynomial( 100d, 0.5d, 0.02d );
            var values = new List<Point>();
            double x = -50;
            values.Add( new Point( x, quadPoly.Evaluate( x ) ) );
            x = 1d;
            values.Add( new Point( x, quadPoly.Evaluate( x ) ) );
            x = 75d;
            values.Add( new Point( x, quadPoly.Evaluate( x ) ) );
            var fitPoly = new Engineering.QuadraticPolynomial();
            _ = fitPoly.PolyFit( values );
            double expectedValue = 1d;
            double actualValue = fitPoly.GoodnessOfFit;
            Assert.AreEqual( expectedValue, actualValue, 0.001d );
            expectedValue = 0d;
            actualValue = fitPoly.StandardError;
            Assert.AreEqual( expectedValue, actualValue, 0.001d );
            expectedValue = quadPoly.ConstantCoefficient;
            actualValue = fitPoly.ConstantCoefficient;
            Assert.AreEqual( expectedValue, actualValue, 0.001d );
            expectedValue = quadPoly.LinearCoefficient;
            actualValue = fitPoly.LinearCoefficient;
            Assert.AreEqual( expectedValue, actualValue, 0.001d );
            expectedValue = quadPoly.QuadraticCoefficient;
            actualValue = fitPoly.QuadraticCoefficient;
            Assert.AreEqual( expectedValue, actualValue, 0.001d );
        }

        /// <summary> (Unit Test Method) polygon fit test method four points. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void PolyFitTestMethodFourPoints()
        {
            var quadPoly = new Engineering.QuadraticPolynomial( 100d, 0.5d, 0.02d );
            var values = new List<Point>();
            double x = -50;
            values.Add( new Point( x, quadPoly.Evaluate( x ) ) );
            x = 1d;
            values.Add( new Point( x, quadPoly.Evaluate( x ) ) );
            x = 75d;
            values.Add( new Point( x, quadPoly.Evaluate( x ) ) );
            x = 125d;
            values.Add( new Point( x, quadPoly.Evaluate( x ) ) );
            var fitPoly = new Engineering.QuadraticPolynomial();
            _ = fitPoly.PolyFit( values );
            double expectedValue = 1d;
            double actualValue = fitPoly.GoodnessOfFit;
            Assert.AreEqual( expectedValue, actualValue, 0.001d );
            expectedValue = 0d;
            actualValue = fitPoly.StandardError;
            Assert.AreEqual( expectedValue, actualValue, 0.001d );
            expectedValue = quadPoly.ConstantCoefficient;
            actualValue = fitPoly.ConstantCoefficient;
            Assert.AreEqual( expectedValue, actualValue, 0.001d );
            expectedValue = quadPoly.LinearCoefficient;
            actualValue = fitPoly.LinearCoefficient;
            Assert.AreEqual( expectedValue, actualValue, 0.001d );
            expectedValue = quadPoly.QuadraticCoefficient;
            actualValue = fitPoly.QuadraticCoefficient;
            Assert.AreEqual( expectedValue, actualValue, 0.001d );
        }

        /// <summary> (Unit Test Method) polygon fit test method four points with error. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void PolyFitTestMethodFourPointsWithError()
        {
            var quadPoly = new Engineering.QuadraticPolynomial( 100d, 0.5d, 0.02d );
            var values = new List<Point>();
            double x = -50;
            double e = 0.1d;
            values.Add( new Point( x, e + quadPoly.Evaluate( x ) ) );
            double ssq = Math.Pow( e, 2d );
            x = 1d;
            e = 0.001d;
            values.Add( new Point( x, e + quadPoly.Evaluate( x ) ) );
            ssq += Math.Pow( e, 2d );
            x = 75d;
            e = 0.02d;
            values.Add( new Point( x, e + quadPoly.Evaluate( x ) ) );
            ssq += Math.Pow( e, 2d );
            x = 125d;
            e = 0.003d;
            values.Add( new Point( x, e + quadPoly.Evaluate( x ) ) );
            ssq += Math.Pow( e, 2d );
            var fitPoly = new Engineering.QuadraticPolynomial();
            _ = fitPoly.PolyFit( values );
            double expectedValue = 1d;
            double actualValue = fitPoly.GoodnessOfFit;
            Assert.AreEqual( expectedValue, actualValue, 0.001d );
            double delta = Math.Sqrt( ssq / values.Count );
            expectedValue = 0d;
            actualValue = fitPoly.StandardError;
            Assert.AreEqual( expectedValue, actualValue, delta );
            expectedValue = quadPoly.ConstantCoefficient;
            actualValue = fitPoly.ConstantCoefficient;
            Assert.AreEqual( expectedValue, actualValue, delta );
            expectedValue = quadPoly.LinearCoefficient;
            actualValue = fitPoly.LinearCoefficient;
            Assert.AreEqual( expectedValue, actualValue, delta );
            expectedValue = quadPoly.QuadraticCoefficient;
            actualValue = fitPoly.QuadraticCoefficient;
            Assert.AreEqual( expectedValue, actualValue, delta );
        }

        /// <summary> (Unit Test Method) polygon fit test method four data points. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void PolyFitTestMethodFourDataPoints()
        {
            var values = new List<Point>();
            double xo = 273d;
            var p1 = new Point( 303d, 1048d );
            values.Add( p1 );
            var p2 = new Point( 327d, 1161d );
            values.Add( p2 );
            var p3 = new Point( 345d, 1239d );
            values.Add( p3 );
            var p4 = new Point( 350d, 1257d );
            values.Add( p4 );
            var fitPoly = new Engineering.QuadraticPolynomial();
            _ = fitPoly.PolyFit( values );
            double expectedValue = 1d;
            double actualValue = fitPoly.GoodnessOfFit;
            Assert.AreEqual( expectedValue, actualValue, 0.1d );
            expectedValue = 0.01d + p1.X;
            double delta = fitPoly.StandardError;
            Assert.IsTrue( delta < expectedValue, $"Is low standard error {delta}<{expectedValue}" );
            delta = 3d * delta;
            expectedValue = fitPoly.Evaluate( p1.X );
            Assert.AreEqual( expectedValue, p1.Y, delta );
            expectedValue = fitPoly.Evaluate( p2.X );
            Assert.AreEqual( expectedValue, p2.Y, delta );
            expectedValue = fitPoly.Evaluate( p3.X );
            Assert.AreEqual( expectedValue, p3.Y, delta );
            expectedValue = fitPoly.Evaluate( p4.X );
            Assert.AreEqual( expectedValue, p4.Y, delta );
            expectedValue = 1000d;
            actualValue = fitPoly.Evaluate( xo + 25d );
            Assert.IsTrue( actualValue > expectedValue, $"Nominal value {actualValue}>{expectedValue}" );
            expectedValue = 0d;
            actualValue = fitPoly.Slope( p1.X );
            Assert.IsTrue( actualValue > expectedValue, $"Positive slope @{p1.X} {actualValue}>{expectedValue}" );
            actualValue = fitPoly.Slope( p2.X );
            Assert.IsTrue( actualValue > expectedValue, $"Positive slope @{p2.X} {actualValue}>{expectedValue}" );
            actualValue = fitPoly.Slope( p3.X );
            Assert.IsTrue( actualValue > expectedValue, $"Positive slope @{p3.X} {actualValue}>{expectedValue}" );
            actualValue = fitPoly.Slope( p4.X );
            Assert.IsTrue( actualValue > expectedValue, $"Positive slope @{p4.X} {actualValue}>{expectedValue}" );
        }
    }
}
