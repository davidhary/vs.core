using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.EngineeringTests
{

    /// <summary> Linear fit tests. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-02-19 </para>
    /// </remarks>
    [TestClass()]
    public class LinearFitTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( EngineeringTests.LinearFitTests.TestInfo.TimeZoneOffset ) < expectedUpperLimit,
                                $"{ nameof( EngineeringTests.TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        /// <summary> (Unit Test Method) builds determinant test method. </summary>
        /// <remarks>
        /// Uses data from C:\My\LIBRARIES\VS\Core\Core\Tests\Engineering\Math\PolyFitTests.cs.
        /// </remarks>
        [TestMethod()]
        public void LinearFitTest()
        {
            var linFit = new Engineering.LinearFit();
            var height = new double[] { 1.47d, 1.5d, 1.52d, 1.55d, 1.57d, 1.6d, 1.63d, 1.65d, 1.68d, 1.7d, 1.73d, 1.75d, 1.78d, 1.8d, 1.83d };
            var mass = new double[] { 52.21d, 53.12d, 54.48d, 55.84d, 57.2d, 58.57d, 59.93d, 61.29d, 63.11d, 64.47d, 66.28d, 68.1d, 69.92d, 72.19d, 74.46d };
            var points = new List<System.Windows.Point>();
            for ( int i = 0, loopTo = height.Count() - 1; i <= loopTo; i++ )
            {
                points.Add( new System.Windows.Point( height[i], mass[i] ) );
            }

            double slope = 61.272d;
            double intercept = -39.062d;
            double correlationCoefficient = 0.9945d;
            double goodness = correlationCoefficient * correlationCoefficient;
            double linearStandardError = 1.775d;
            double constantStandardError = 2.938d;
            _ = linFit.DoFit( height, mass );
            Assert.AreEqual( slope, linFit.LinearCoefficient, 0.001d, "Slope" );
            Assert.AreEqual( intercept, linFit.ConstantCoefficient, 0.001d, "intercept" );
            Assert.AreEqual( goodness, linFit.GoodnessOfFit, 0.001d, "Goodness of fit" );
            Assert.AreEqual( linearStandardError, linFit.LinearCoefficientStandardError, 0.001d, "Linear coefficient Standard error" );
            Assert.AreEqual( constantStandardError, linFit.ConstantCoefficientStandardError, 0.001d, "Constant coefficient Standard error" );
            _ = linFit.DoFit( points );
            Assert.AreEqual( slope, linFit.LinearCoefficient, 0.001d, "Slope" );
            Assert.AreEqual( intercept, linFit.ConstantCoefficient, 0.001d, "intercept" );
            Assert.AreEqual( goodness, linFit.GoodnessOfFit, 0.001d, "Goodness of fit" );
            Assert.AreEqual( linearStandardError, linFit.LinearCoefficientStandardError, 0.001d, "Linear coefficient Standard error" );
            Assert.AreEqual( constantStandardError, linFit.ConstantCoefficientStandardError, 0.001d, "Constant coefficient Standard error" );
        }
    }
}
