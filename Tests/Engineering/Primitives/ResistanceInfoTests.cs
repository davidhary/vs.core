using System;

using isr.Core.Engineering;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.EngineeringTests
{

    /// <summary>
    /// This is a test class for ResistanceInfoTest and is intended to contain all ResistanceInfoTest
    /// Unit Tests.
    /// </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestClass()]
    public class ResistanceInfoTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( EngineeringTests.ResistanceInfoTests.TestInfo.TimeZoneOffset ) < expectedUpperLimit,
                                $"{nameof( EngineeringTests.TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        /// <summary> A test for TryParse. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="resistanceCode">     The resistance code. </param>
        /// <param name="resistanceExpected"> The resistance expected. </param>
        /// <param name="expected">           True if expected. </param>
        public static void TryParse( string resistanceCode, double resistanceExpected, bool expected )
        {
            double resistance = 0.0d;
            string details = string.Empty;
            bool actual;
            actual = ResistanceInfo.TryParse( resistanceCode, ref resistance, ref details );
            Assert.AreEqual( resistanceExpected, resistance, details );
            Assert.AreEqual( expected, actual, details );
        }

        /// <summary> A test for TryParse. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void TryParseTest()
        {
            TryParse( "121", 120d, true );
            TryParse( "230", 23d, true );
            TryParse( "1000", 100d, true );
            TryParse( "2R67", 2.67d, true );
            TryParse( "2.67", 2.67d, true );
            TryParse( "12K6675", 12667.5d, true );
            TryParse( "100R5", 100.5d, true );
            TryParse( "R0015", 0.0015d, true );
            TryParse( "100R", 100d, true );
        }
    }
}
