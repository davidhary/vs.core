using System;

using isr.Core.Engineering;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.EngineeringTests
{

    /// <summary> A current source unit tests. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-09-18 </para>
    /// </remarks>
    [TestClass()]
    public class CurrentSourceTests
    {

        #region " SHARED CONSTANTS "

        /// <summary> Gets or sets the current source current. </summary>
        /// <value> The current. </value>
        public static double Current { get; set; } = 0.004d;

        /// <summary> Gets or sets the conductance of the current source. </summary>
        /// <value> The conductance. </value>
        public static double Conductance { get; set; } = 0.001d;

        /// <summary> Gets or sets the load resistance. </summary>
        /// <value> The load resistance. </value>
        public static double LoadResistance { get; set; } = 500d;

        /// <summary> Gets or sets the voltage of the voltage source. </summary>
        /// <value> The voltage. </value>
        public static double Voltage { get; set; } = 5d;

        /// <summary> Gets or sets the resistance of the voltage source. </summary>
        /// <value> The resistance. </value>
        public static double Resistance { get; set; } = 1000d;

        /// <summary> Gets or sets the short resistance. </summary>
        /// <value> The short resistance. </value>
        public static double ShortResistance { get; set; } = 0d;

        /// <summary> Gets or sets the open resistance. </summary>
        /// <value> The short resistance. </value>
        public static double OpenResistance { get; set; } = double.PositiveInfinity;

        /// <summary> Gets or sets the short conductance. </summary>
        /// <value> The short conductance. </value>
        public static double ShortConductance { get; set; } = double.PositiveInfinity;

        /// <summary> Gets or sets the open conductance. </summary>
        /// <value> The open conductance. </value>
        public static double OpenConductance { get; set; } = 0d;

        /// <summary> The current change scale. </summary>
        /// <value> The current change scale. </value>
        public static double CurrentChangeScale { get; set; } = 1.5d;

        /// <summary> The conductance change scale. </summary>
        /// <value> The conductance change scale. </value>
        public static double ConductanceChangeScale { get; set; } = 0.8d;

        /// <summary> Gets or sets the current to voltage level epsilon. </summary>
        /// <value> The current to voltage level epsilon. </value>
        public static double CurrentToVoltageLevelEpsilon { get; set; } = 0.000000001d;

        /// <summary> Gets or sets the current to voltage resistance epsilon. </summary>
        /// <value> The current to voltage resistance epsilon. </value>
        public static double CurrentToVoltageResistanceEpsilon { get; set; } = 0.000000001d;

        /// <summary> Gets or sets the current to voltage current level epsilon. </summary>
        /// <value> The current to voltage current level epsilon. </value>
        public static double CurrentToVoltageCurrentLevelEpsilon { get; set; } = 0.000000001d;

        /// <summary> Gets or sets the current to voltage conductance epsilon. </summary>
        /// <value> The current to voltage conductance epsilon. </value>
        public static double CurrentToVoltageConductanceEpsilon { get; set; } = 0.000000001d;

        #endregion

        #region " CONSTUCTOR TESTS "

        /// <summary> (Unit Test Method) tests build current source. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void BuildCurrentSourceTest()
        {
            var source = new CurrentSource( Current, Conductance );

            // test source Current
            double actualValue = source.Current;
            double expectedValue = Current;
            Assert.AreEqual( expectedValue, actualValue );

            // test source conductance
            actualValue = source.Conductance;
            expectedValue = Conductance;
            Assert.AreEqual( expectedValue, actualValue );
        }

        /// <summary> (Unit Test Method) tests current to voltage source. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void CurrentToVoltageSourceTest()
        {

            // create current source
            var currentSource = new CurrentSource( VoltageSourceTests.Current, VoltageSourceTests.Conductance );

            // convert to voltage source
            var source = currentSource.ToVoltageSource();

            // test source voltage
            double actualValue = source.Voltage;
            double expectedValue = currentSource.Current / currentSource.Conductance;
            Assert.AreEqual( expectedValue, actualValue, CurrentToVoltageLevelEpsilon );

            // test source resistance
            actualValue = source.Resistance;
            expectedValue = 1d / currentSource.Conductance;
            Assert.AreEqual( expectedValue, actualValue, CurrentToVoltageResistanceEpsilon );
            var finalCurrentSource = source.ToCurrentSource();
            Assert.AreEqual( currentSource.Current, finalCurrentSource.Current, CurrentToVoltageCurrentLevelEpsilon );
            Assert.AreEqual( currentSource.Conductance, finalCurrentSource.Conductance, CurrentToVoltageConductanceEpsilon );
        }

        /// <summary> (Unit Test Method) tests null voltage source exception. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        [ExpectedException( typeof( ArgumentNullException ), "Null voltage source inappropriately allowed" )]
        public void NullVoltageSourceExceptionTest()
        {

            // null voltage source
            VoltageSource voltageSource = null;

            // convert to current source
            var currentSource = new CurrentSource( Current, Conductance );
            currentSource.FromVoltageSource( voltageSource );
        }

        #endregion

        #region " CURRENT TESTS "

        /// <summary> (Unit Test Method) tests current source. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void CurrentSourceTest()
        {
            var source = new CurrentSource( Current, Conductance );

            // test load current
            double actualValue = source.LoadCurrent( 1d / LoadResistance );
            double expectedValue = Current * Resistor.Parallel( 1d / Conductance, LoadResistance ) / LoadResistance;
            Assert.AreEqual( expectedValue, actualValue );


            // test open voltage
            actualValue = source.LoadVoltage( OpenResistance );
            expectedValue = Current / Conductance;
            Assert.AreEqual( expectedValue, actualValue );

            // test short voltage
            actualValue = source.LoadVoltage( ShortResistance );
            expectedValue = 0d;
            Assert.AreEqual( expectedValue, actualValue );

            // test load voltage
            actualValue = source.LoadVoltage( LoadResistance );
            expectedValue = Current * Resistor.Parallel( 1d / Conductance, LoadResistance );
            Assert.AreEqual( expectedValue, actualValue );
        }

        /// <summary> (Unit Test Method) tests current source change. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void CurrentSourceChangeTest()
        {
            var source = new CurrentSource( Current, Conductance );
            source.Current *= CurrentChangeScale;
            // test open voltage
            double actualValue = source.LoadVoltage( OpenResistance );
            double expectedValue = CurrentChangeScale * Current / Conductance;
            Assert.AreEqual( expectedValue, actualValue );

            // test short voltage
            actualValue = source.LoadVoltage( ShortResistance );
            expectedValue = 0d;
            Assert.AreEqual( expectedValue, actualValue );

            // test load voltage
            actualValue = source.LoadVoltage( LoadResistance );
            expectedValue = CurrentChangeScale * Current * Resistor.Parallel( 1d / Conductance, LoadResistance );
            Assert.AreEqual( expectedValue, actualValue );

            // test load current
            actualValue = source.LoadCurrent( 1d / LoadResistance );
            expectedValue = CurrentChangeScale * Current * Resistor.Parallel( 1d / Conductance, LoadResistance ) / LoadResistance;
            Assert.AreEqual( expectedValue, actualValue );
            source.Current /= CurrentChangeScale;
            source.Conductance *= ConductanceChangeScale;

            // test open voltage
            actualValue = source.LoadVoltage( double.PositiveInfinity );
            expectedValue = Current / (ConductanceChangeScale * Conductance);
            Assert.AreEqual( expectedValue, actualValue );

            // test short voltage
            actualValue = source.LoadVoltage( ShortResistance );
            expectedValue = 0d;
            Assert.AreEqual( expectedValue, actualValue );

            // test load voltage
            actualValue = source.LoadVoltage( LoadResistance );
            expectedValue = Current * Resistor.Parallel( 1d / (ConductanceChangeScale * Conductance), LoadResistance );
            Assert.AreEqual( expectedValue, actualValue );

            // test load current
            actualValue = source.LoadCurrent( 1d / LoadResistance );
            expectedValue = Current * Resistor.Parallel( 1d / (ConductanceChangeScale * Conductance), LoadResistance ) / LoadResistance;
            Assert.AreEqual( expectedValue, actualValue );
        }

        #endregion

    }
}
