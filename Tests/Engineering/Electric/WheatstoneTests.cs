using System;

using isr.Core.Engineering;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.EngineeringTests
{

    /// <summary> ATests for a Wheatstone bridge. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-09-15 </para>
    /// </remarks>
    [TestClass()]
    public class WheatstoneTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( EngineeringTests.WheatstoneTests.TestInfo.TimeZoneOffset ) < expectedUpperLimit,
                                $"{nameof( EngineeringTests.TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " TEST INPUTS "

        /// <summary> Gets or sets the bridge resistance. </summary>
        /// <value> The bridge resistance. </value>
        public static double BridgeResistance { get; set; } = 1000d;

        /// <summary> Gets or sets the relative offset. </summary>
        /// <value> The relative offset. </value>
        public static double RelativeOffset { get; set; } = 0.002d;

        /// <summary> Gets or sets the zero bridge voltage. </summary>
        /// <value> The zero bridge voltage. </value>
        public static double ZeroBridgeVoltage { get; set; } = 0d;

        /// <summary> Gets or sets the low bridge voltage. </summary>
        /// <value> The low bridge voltage. </value>
        public static double LowBridgeVoltage { get; set; } = 1d;

        /// <summary> Gets or sets the medium bridge voltage. </summary>
        /// <value> The medium bridge voltage. </value>
        public static double MediumBridgeVoltage { get; set; } = 10d;

        /// <summary> Gets or sets the high bridge voltage. </summary>
        /// <value> The high bridge voltage. </value>
        public static double HighBridgeVoltage { get; set; } = 24d;

        /// <summary> Gets or sets the relative offset epsilon scale. </summary>
        /// <value> The relative offset epsilon scale. </value>
        public static double RelativeOffsetEpsilonScale { get; set; } = 0.006d;

        /// <summary> Gets or sets the relative offset epsilon. </summary>
        /// <value> The relative offset epsilon. </value>
        public static double RelativeOffsetEpsilon { get; set; } = 0.000000000001d;

        #endregion

        #region " CONSTUCTOR TESTS "

        /// <summary> (Unit Test Method) tests build of a balanced bridge. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void BuildBalancedBridgeTest()
        {

            // build a balanced bridge
            var bridge = new Wheatstone( BridgeResistance );

            // test value of all bridge elements
            double actualValue = bridge.TopRight;
            double expectedValue = BridgeResistance;
            Assert.AreEqual( expectedValue, actualValue );
            actualValue = bridge.BottomRight;
            expectedValue = BridgeResistance;
            Assert.AreEqual( expectedValue, actualValue );
            actualValue = bridge.BottomLeft;
            expectedValue = BridgeResistance;
            Assert.AreEqual( expectedValue, actualValue );
            actualValue = bridge.TopLeft;
            expectedValue = BridgeResistance;
            Assert.AreEqual( expectedValue, actualValue );

            // test validity
            bool actualValidity = bridge.IsValid();
            bool expectedValidity = true;
            Assert.AreEqual( expectedValidity, actualValidity );

            // test balance
            bool actualBalance = bridge.IsOutputBalanced;
            bool expectedBalance = true;
            Assert.AreEqual( expectedBalance, actualBalance );

            // test imbalance
            actualValue = bridge.ProductImbalance;
            expectedValue = bridge.BottomLeft * bridge.TopRight - bridge.BottomRight * bridge.TopLeft;
            Assert.AreEqual( expectedValue, actualValue, bridge.OutputEpsilon );

            // test equivalent resistance
            actualValue = bridge.BridgeResistance;
            expectedValue = (bridge.TopLeft + bridge.BottomLeft) * (bridge.TopRight + bridge.BottomRight) / (bridge.TopLeft + bridge.BottomLeft + bridge.TopRight + bridge.BottomRight);
            Assert.AreEqual( expectedValue, actualValue, bridge.OutputEpsilon );
        }

        /// <summary> (Unit Test Method) tests build of invalid bridges. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void BuildInvalidBridgeTest()
        {

            // build a balanced bridge
            var bridge = new Wheatstone( Resistor.ShortResistance, BridgeResistance, BridgeResistance, BridgeResistance );
            bool actualValidity = bridge.IsValid();
            bool expectedValidity = false;
            Assert.AreEqual( expectedValidity, actualValidity );
            bridge = new Wheatstone( Resistor.OpenResistance, BridgeResistance, BridgeResistance, BridgeResistance );
            actualValidity = bridge.IsValid();
            expectedValidity = false;
            Assert.AreEqual( expectedValidity, actualValidity );
            bridge = new Wheatstone( BridgeResistance, Resistor.ShortResistance, BridgeResistance, BridgeResistance );
            actualValidity = bridge.IsValid();
            expectedValidity = false;
            Assert.AreEqual( expectedValidity, actualValidity );
            bridge = new Wheatstone( BridgeResistance, Resistor.OpenResistance, BridgeResistance, BridgeResistance );
            actualValidity = bridge.IsValid();
            expectedValidity = false;
            Assert.AreEqual( expectedValidity, actualValidity );
            bridge = new Wheatstone( BridgeResistance, BridgeResistance, Resistor.ShortResistance, BridgeResistance );
            actualValidity = bridge.IsValid();
            expectedValidity = false;
            Assert.AreEqual( expectedValidity, actualValidity );
            bridge = new Wheatstone( BridgeResistance, BridgeResistance, Resistor.OpenResistance, BridgeResistance );
            actualValidity = bridge.IsValid();
            expectedValidity = false;
            Assert.AreEqual( expectedValidity, actualValidity );
            bridge = new Wheatstone( BridgeResistance, BridgeResistance, BridgeResistance, Resistor.ShortResistance );
            actualValidity = bridge.IsValid();
            expectedValidity = false;
            Assert.AreEqual( expectedValidity, actualValidity );
            bridge = new Wheatstone( BridgeResistance, BridgeResistance, BridgeResistance, Resistor.OpenResistance );
            actualValidity = bridge.IsValid();
            expectedValidity = false;
            Assert.AreEqual( expectedValidity, actualValidity );
        }

        /// <summary> (Unit Test Method) tests null bridge exception. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        [ExpectedException( typeof( ArgumentNullException ), "Null bridge inappropriately allowed" )]
        public void NullBridgeExceptionTest()
        {
            Wheatstone bridge = null;
            // try build the bridge using a null bridge; should issue the expected exception
            _ = new Wheatstone( bridge );
        }

        /// <summary> (Unit Test Method) tests build of positive offset bridge. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void BuildPositiveOffsetBridgeTest()
        {

            // build a bridge with known positive offset
            var bridge = new Wheatstone( BridgeResistance, RelativeOffset );

            // test value of all bridge elements
            double actualValue = bridge.TopRight;
            double expectedValue = BridgeResistance * (1d + RelativeOffset);
            Assert.AreEqual( expectedValue, actualValue );
            actualValue = bridge.BottomRight;
            expectedValue = BridgeResistance * (1d - RelativeOffset);
            Assert.AreEqual( expectedValue, actualValue );
            actualValue = bridge.BottomLeft;
            expectedValue = BridgeResistance * (1d + RelativeOffset);
            Assert.AreEqual( expectedValue, actualValue );
            actualValue = bridge.TopLeft;
            expectedValue = BridgeResistance * (1d - RelativeOffset);
            Assert.AreEqual( expectedValue, actualValue );

            // test validity
            bool actualValidity = bridge.IsValid();
            bool expectedValidity = true;
            Assert.AreEqual( expectedValidity, actualValidity );

            // test balance
            bool actualBalance = bridge.IsOutputBalanced;
            bool expectedBalance = false;
            Assert.AreEqual( expectedBalance, actualBalance );

            // test offset
            _ = bridge.Output();
            _ = RelativeOffset;
            Assert.AreEqual( expectedBalance, actualBalance );

            // test imbalance
            actualValue = bridge.ProductImbalance;
            expectedValue = bridge.BottomLeft * bridge.TopRight - bridge.BottomRight * bridge.TopLeft;
            Assert.AreEqual( expectedValue, actualValue, bridge.OutputEpsilon );

            // test equivalent resistance
            actualValue = bridge.BridgeResistance;
            expectedValue = (bridge.TopLeft + bridge.BottomLeft) * (bridge.TopRight + bridge.BottomRight) / (bridge.TopLeft + bridge.BottomLeft + bridge.TopRight + bridge.BottomRight);
            Assert.AreEqual( expectedValue, actualValue, bridge.OutputEpsilon );
        }

        /// <summary> (Unit Test Method) tests build negative offset bridge. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void BuildNegativeOffsetBridgeTest()
        {

            // build a bridge with known negative offset
            var bridge = new Wheatstone( BridgeResistance, -RelativeOffset );

            // test value of all bridge elements
            double actualValue = bridge.TopRight;
            double expectedValue = BridgeResistance * (1d - RelativeOffset);
            Assert.AreEqual( expectedValue, actualValue );
            actualValue = bridge.BottomRight;
            expectedValue = BridgeResistance * (1d + RelativeOffset);
            Assert.AreEqual( expectedValue, actualValue );
            actualValue = bridge.BottomLeft;
            expectedValue = BridgeResistance * (1d - RelativeOffset);
            Assert.AreEqual( expectedValue, actualValue );
            actualValue = bridge.TopLeft;
            expectedValue = BridgeResistance * (1d + RelativeOffset);
            Assert.AreEqual( expectedValue, actualValue );

            // test validity
            bool actualValidity = bridge.IsValid();
            bool expectedValidity = true;
            Assert.AreEqual( expectedValidity, actualValidity );

            // test balance
            bool actualBalance = bridge.IsOutputBalanced;
            bool expectedBalance = false;
            Assert.AreEqual( expectedBalance, actualBalance );

            // test offset
            _ = bridge.Output();
            _ = -RelativeOffset;
            Assert.AreEqual( expectedBalance, actualBalance );

            // test imbalance
            actualValue = bridge.ProductImbalance;
            expectedValue = bridge.BottomLeft * bridge.TopRight - bridge.BottomRight * bridge.TopLeft;
            Assert.AreEqual( expectedValue, actualValue, bridge.OutputEpsilon );

            // test equivalent resistance
            actualValue = bridge.BridgeResistance;
            expectedValue = (bridge.TopLeft + bridge.BottomLeft) * (bridge.TopRight + bridge.BottomRight) / (bridge.TopLeft + bridge.BottomLeft + bridge.TopRight + bridge.BottomRight);
            Assert.AreEqual( expectedValue, actualValue, bridge.OutputEpsilon );
        }

        #endregion

        #region " OUTPUT TESTS "

        /// <summary> (Unit Test Method) tests balanced bridge output. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void BalancedBridgeOutputTest()
        {

            // build a balanced bridge
            var bridge = new Wheatstone( BridgeResistance );

            // test bridge output: bridge is balanced: all outputs are to be zero
            double actualValue = bridge.Output( ZeroBridgeVoltage );
            double expectedValue = ZeroBridgeVoltage;
            Assert.AreEqual( expectedValue, actualValue );
            actualValue = bridge.Output( LowBridgeVoltage );
            Assert.AreEqual( expectedValue, actualValue );
            actualValue = bridge.Output( HighBridgeVoltage );
            Assert.AreEqual( expectedValue, actualValue );
            actualValue = bridge.Output( -LowBridgeVoltage );
            Assert.AreEqual( expectedValue, actualValue );
            actualValue = bridge.Output( -HighBridgeVoltage );
            Assert.AreEqual( expectedValue, actualValue );
        }

        /// <summary> (Unit Test Method) tests positive offset bridge output. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void PositiveOffsetBridgeOutputTest()
        {

            // build a bridge with known positive offset
            var bridge = new Wheatstone( BridgeResistance, RelativeOffset );

            // test bridge output
            _ = ZeroBridgeVoltage;
            double relativeOffset = RelativeOffset;
            double epsilon = Math.Max( RelativeOffsetEpsilon, RelativeOffsetEpsilonScale * bridge.OutputEpsilon );
            double bridgeVoltage = ZeroBridgeVoltage;
            double expectedValue = relativeOffset * bridgeVoltage;
            double actualValue = bridge.Output( bridgeVoltage );
            Assert.AreEqual( expectedValue, actualValue );
            bridgeVoltage = LowBridgeVoltage;
            expectedValue = relativeOffset * bridgeVoltage;
            actualValue = bridge.Output( bridgeVoltage );
            Assert.AreEqual( expectedValue, actualValue, epsilon );
            bridgeVoltage = HighBridgeVoltage;
            expectedValue = relativeOffset * bridgeVoltage;
            actualValue = bridge.Output( bridgeVoltage );
            Assert.AreEqual( expectedValue, actualValue, epsilon );
            bridgeVoltage = -ZeroBridgeVoltage;
            expectedValue = relativeOffset * bridgeVoltage;
            actualValue = bridge.Output( bridgeVoltage );
            Assert.AreEqual( expectedValue, actualValue, epsilon );
            bridgeVoltage = -LowBridgeVoltage;
            expectedValue = relativeOffset * bridgeVoltage;
            actualValue = bridge.Output( bridgeVoltage );
            Assert.AreEqual( expectedValue, actualValue, epsilon );
            bridgeVoltage = -HighBridgeVoltage;
            expectedValue = relativeOffset * bridgeVoltage;
            actualValue = bridge.Output( bridgeVoltage );
            Assert.AreEqual( expectedValue, actualValue, epsilon );
        }

        /// <summary> (Unit Test Method) tests negative offset bridge output. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void NegativeOffsetBridgeOutputTest()
        {

            // build a bridge with known positive offset
            var bridge = new Wheatstone( BridgeResistance, -RelativeOffset );

            // test bridge output
            _ = ZeroBridgeVoltage;
            double relativeOffset = -RelativeOffset;
            double epsilon = Math.Max( RelativeOffsetEpsilon, RelativeOffsetEpsilonScale * bridge.OutputEpsilon );
            double bridgeVoltage = ZeroBridgeVoltage;
            double expectedValue = relativeOffset * bridgeVoltage;
            double actualValue = bridge.Output( bridgeVoltage );
            Assert.AreEqual( expectedValue, actualValue );
            bridgeVoltage = LowBridgeVoltage;
            expectedValue = relativeOffset * bridgeVoltage;
            actualValue = bridge.Output( bridgeVoltage );
            Assert.AreEqual( expectedValue, actualValue, epsilon );
            bridgeVoltage = HighBridgeVoltage;
            expectedValue = relativeOffset * bridgeVoltage;
            actualValue = bridge.Output( bridgeVoltage );
            Assert.AreEqual( expectedValue, actualValue, epsilon );
            bridgeVoltage = -ZeroBridgeVoltage;
            expectedValue = relativeOffset * bridgeVoltage;
            actualValue = bridge.Output( bridgeVoltage );
            Assert.AreEqual( expectedValue, actualValue, epsilon );
            bridgeVoltage = -LowBridgeVoltage;
            expectedValue = relativeOffset * bridgeVoltage;
            actualValue = bridge.Output( bridgeVoltage );
            Assert.AreEqual( expectedValue, actualValue, epsilon );
            bridgeVoltage = -HighBridgeVoltage;
            expectedValue = relativeOffset * bridgeVoltage;
            actualValue = bridge.Output( bridgeVoltage );
            Assert.AreEqual( expectedValue, actualValue, epsilon );
        }

        #endregion

        #region " EQUALITY TESTS "

        /// <summary> (Unit Test Method) tests equals. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void EqualsTest()
        {

            // build identical bridges
            var bridge1 = new Wheatstone( BridgeResistance, -RelativeOffset );
            var bridge2 = new Wheatstone( BridgeResistance, -RelativeOffset );

            // test bridge equality
            bool actualValue = Wheatstone.Equals( bridge1, bridge2 );
            bool expectedValue = true;
            Assert.AreEqual( expectedValue, actualValue );
            actualValue = bridge1 == bridge2;
            expectedValue = true;
            Assert.AreEqual( expectedValue, actualValue );
            actualValue = bridge1 != bridge2;
            expectedValue = false;
            Assert.AreEqual( expectedValue, actualValue );
            Assert.AreEqual( bridge1, bridge2 );

            // change bridge 1 to a positive offset bridge
            bridge1 = new Wheatstone( BridgeResistance, +RelativeOffset );
            Assert.AreNotEqual( bridge1, bridge2 );

            // change bridge 2 to a positive offset bridge
            bridge2 = new Wheatstone( BridgeResistance, +RelativeOffset );
            Assert.AreEqual( bridge1, bridge2 );
        }

        #endregion

    }
}
