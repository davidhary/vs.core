using System;

using isr.Core.Engineering;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.EngineeringTests
{

    /// <summary> Unit tests for the attenuated voltage source. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-09-18 </para>
    /// </remarks>
    [TestClass()]
    public class AttenuatedVoltageSourceTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( EngineeringTests.AttenuatedVoltageSourceTests.TestInfo.TimeZoneOffset ) < expectedUpperLimit,
                            $"{nameof( EngineeringTests.TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            Assert.IsTrue( VoltageSourceTestInfo.Get().Exists, $"{typeof( VoltageSourceTestInfo )} settings should exist" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " CONSTRUCTION TESTS "

        /// <summary> (Unit Test Method) tests building an attenuated voltage source. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void BuildTest()
        {
            double attenuation = AttenuatedVoltageSource.ToAttenuation( VoltageSourceTestInfo.Get().SeriesResistance, VoltageSourceTestInfo.Get().ParallelConductance );
            double equivalenctResistance = Resistor.Parallel( VoltageSourceTestInfo.Get().SeriesResistance, VoltageSourceTestInfo.Get().ParallelResistance );

            // construct a voltage source 
            VoltageSource voltageSource;
            AttenuatedVoltageSource attenuatedSource = null;
            double doubleEpsilon = 0.0000000001d;
            string attenuatedSourceTarget = string.Empty;
            string sourceTarget = string.Empty;
            for ( int i = 1; i <= 3; i++ )
            {
                switch ( i )
                {
                    case 1:
                        {
                            attenuatedSourceTarget = "nominal attenuated voltage source";
                            attenuatedSource = new AttenuatedVoltageSource( ( decimal ) VoltageSourceTestInfo.Get().NominalVoltage, VoltageSourceTestInfo.Get().SeriesResistance, VoltageSourceTestInfo.Get().ParallelConductance );
                            sourceTarget = "voltage source from nominal";
                            break;
                        }

                    case 2:
                        {
                            attenuatedSourceTarget = "equivalent attenuated voltage source";
                            attenuatedSource = new AttenuatedVoltageSource( VoltageSourceTestInfo.Get().NominalVoltage / attenuation, VoltageSourceTestInfo.Get().SeriesResistance, VoltageSourceTestInfo.Get().ParallelConductance );
                            sourceTarget = "voltage source from equivalent";
                            break;
                        }

                    case 3:
                        {
                            attenuatedSourceTarget = "converted attenuated voltage source";
                            voltageSource = new VoltageSource( VoltageSourceTestInfo.Get().NominalVoltage / attenuation, equivalenctResistance );
                            attenuatedSource = VoltageSource.ToAttenuatedVoltageSource( voltageSource, attenuation );
                            sourceTarget = "voltage source from converted";
                            break;
                        }
                }

                voltageSource = attenuatedSource.ToVoltageSource();

                // test the attenuated voltage source
                string target = attenuatedSourceTarget;
                double actualValue = attenuatedSource.NominalVoltage;
                double expectedValue = VoltageSourceTestInfo.Get().NominalVoltage;
                string outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                string item = "nominal voltage";
                string message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                TestInfo.VerboseMessage( message );
                target = attenuatedSourceTarget;
                actualValue = attenuatedSource.Voltage;
                expectedValue = VoltageSourceTestInfo.Get().NominalVoltage / attenuation;
                outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                item = "open load voltage";
                message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                TestInfo.VerboseMessage( message );
                target = sourceTarget;
                actualValue = voltageSource.Voltage;
                expectedValue = VoltageSourceTestInfo.Get().NominalVoltage / attenuation;
                item = "open load voltage";
                outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                TestInfo.VerboseMessage( message );
                target = attenuatedSourceTarget;
                actualValue = attenuatedSource.Resistance;
                expectedValue = equivalenctResistance;
                item = "equivalent resistance";
                outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                TestInfo.VerboseMessage( message );
                target = sourceTarget;
                actualValue = voltageSource.Resistance;
                expectedValue = equivalenctResistance;
                item = "equivalent resistance";
                outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                TestInfo.VerboseMessage( message );
                target = attenuatedSourceTarget;
                actualValue = attenuatedSource.SeriesResistance;
                expectedValue = VoltageSourceTestInfo.Get().SeriesResistance;
                outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                item = "series resistance";
                message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                TestInfo.VerboseMessage( message );
                target = attenuatedSourceTarget;
                actualValue = attenuatedSource.Attenuation;
                expectedValue = attenuation;
                item = "attenuation";
                outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                TestInfo.VerboseMessage( message );
                target = attenuatedSourceTarget;
                actualValue = 1d / attenuatedSource.ParallelConductance;
                expectedValue = VoltageSourceTestInfo.Get().ParallelResistance;
                item = "parallel resistance";
                outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                TestInfo.VerboseMessage( message );
            }
        }

        /// <summary>
        /// (Unit Test Method) tests exception for converting a voltage source to an attenuated voltage
        /// source with invalid attenuation.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        [ExpectedException( typeof( ArgumentOutOfRangeException ), "Attenuation smaller than 1 inappropriately allowed" )]
        public void ConvertAttenuationExceptionTest()
        {
            double attenuation = AttenuatedVoltageSource.MinimumAttenuation - 0.01d;
            var voltageSource = new VoltageSource( VoltageSourceTestInfo.Get().NominalVoltage, VoltageSourceTestInfo.Get().SourceResistance );
            _ = VoltageSource.ToAttenuatedVoltageSource( voltageSource, attenuation );
        }

        /// <summary>
        /// (Unit Test Method) tests exception for converting to an attenuated source using a null source.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        [ExpectedException( typeof( ArgumentNullException ), "Conversion with null source allowed" )]
        public void ConvertNothingExceptionTest()
        {
            double attenuation = AttenuatedVoltageSource.MinimumAttenuation + 0.01d;
            VoltageSource voltageSource = null;
            _ = VoltageSource.ToAttenuatedVoltageSource( voltageSource, attenuation );
        }

        #endregion

        #region " LOAD AND PROPERTIES TESTS "

        /// <summary> (Unit Test Method) tests loading an attenuated voltage source. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void LoadTest()
        {
            var source = new AttenuatedVoltageSource( ( decimal ) VoltageSourceTestInfo.Get().NominalVoltage, VoltageSourceTestInfo.Get().SeriesResistance, VoltageSourceTestInfo.Get().ParallelConductance );
            double doubleEpsilon = 0.0000000001d;

            // test the attenuated voltage source
            string target = "attenuated voltage source";

            // test open load voltage
            string item = "open load voltage";
            double actualValue = source.LoadVoltage( Resistor.OpenResistance );
            double expectedValue = ( double ) ( decimal ) VoltageSourceTestInfo.Get().NominalVoltage / source.Attenuation;
            string outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            string message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            TestInfo.VerboseMessage( message );

            // test short load voltage
            item = "short load voltage";
            actualValue = source.LoadVoltage( 0d );
            expectedValue = 0d;
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            TestInfo.VerboseMessage( message );

            // test load voltage
            item = "load voltage";
            actualValue = source.LoadVoltage( VoltageSourceTestInfo.Get().LoadResistance );
            expectedValue = source.NominalVoltage * Resistor.OutputVoltage( VoltageSourceTestInfo.Get().SeriesResistance, Resistor.Parallel( VoltageSourceTestInfo.Get().LoadResistance, VoltageSourceTestInfo.Get().ParallelResistance ) );
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            TestInfo.VerboseMessage( message );


            // test load current
            item = "load current";
            actualValue = source.LoadCurrent( VoltageSourceTestInfo.Get().LoadResistance );
            expectedValue = source.NominalVoltage * Resistor.OutputVoltage( VoltageSourceTestInfo.Get().SeriesResistance, Resistor.Parallel( VoltageSourceTestInfo.Get().LoadResistance, VoltageSourceTestInfo.Get().ParallelResistance ) ) / VoltageSourceTestInfo.Get().LoadResistance;
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            TestInfo.VerboseMessage( message );
        }

        /// <summary>
        /// (Unit Test Method) tests changing properties of an attenuated voltage source.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void PropertiesChangeTest()
        {

            // test changing Attenuated voltage source properties
            var source = new AttenuatedVoltageSource( ( decimal ) VoltageSourceTestInfo.Get().NominalVoltage, VoltageSourceTestInfo.Get().SeriesResistance, VoltageSourceTestInfo.Get().ParallelConductance );
            double doubleEpsilon = 0.0000000001d;

            // test the attenuated voltage source
            string target = "attenuated voltage source";
            double voltageGain = 1.5d;

            // test open voltage
            string item = $"open voltage {voltageGain}={nameof( voltageGain )}";
            source.NominalVoltage *= voltageGain;
            double actualValue = source.LoadVoltage( Resistor.OpenResistance );
            double expectedValue = voltageGain * VoltageSourceTestInfo.Get().NominalVoltage / source.Attenuation;
            string outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            string message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            TestInfo.VerboseMessage( message );

            // test short voltage
            item = $"short voltage {voltageGain}={nameof( voltageGain )}";
            actualValue = source.LoadVoltage( 0d );
            expectedValue = 0d;
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            TestInfo.VerboseMessage( message );

            // test load voltage
            item = $"load voltage {voltageGain}={nameof( voltageGain )}";
            actualValue = source.LoadVoltage( VoltageSourceTestInfo.Get().LoadResistance );
            expectedValue = voltageGain * VoltageSourceTestInfo.Get().NominalVoltage * Resistor.OutputVoltage( VoltageSourceTestInfo.Get().SeriesResistance, Resistor.Parallel( VoltageSourceTestInfo.Get().LoadResistance, VoltageSourceTestInfo.Get().ParallelResistance ) );
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            TestInfo.VerboseMessage( message );

            // test load current
            item = $"load current {voltageGain}={nameof( voltageGain )}";
            actualValue = source.LoadCurrent( VoltageSourceTestInfo.Get().LoadResistance );
            expectedValue = voltageGain * VoltageSourceTestInfo.Get().NominalVoltage * Resistor.OutputVoltage( VoltageSourceTestInfo.Get().SeriesResistance, Resistor.Parallel( VoltageSourceTestInfo.Get().LoadResistance, VoltageSourceTestInfo.Get().ParallelResistance ) ) / VoltageSourceTestInfo.Get().LoadResistance;
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            TestInfo.VerboseMessage( message );
            source.NominalVoltage /= voltageGain;
            double seriesResistanceGain = 1.2d;
            source.SeriesResistance *= seriesResistanceGain;
            item = $"open voltage {seriesResistanceGain}={nameof( seriesResistanceGain )}";
            actualValue = source.LoadVoltage( Resistor.OpenResistance );
            expectedValue = VoltageSourceTestInfo.Get().NominalVoltage / source.Attenuation;
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            TestInfo.VerboseMessage( message );

            // test short voltage
            item = $"short voltage {seriesResistanceGain}={nameof( seriesResistanceGain )}";
            actualValue = source.LoadVoltage( 0d );
            expectedValue = 0d;
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            TestInfo.VerboseMessage( message );

            // test load voltage
            item = $"load voltage {seriesResistanceGain}={nameof( seriesResistanceGain )}";
            actualValue = source.LoadVoltage( VoltageSourceTestInfo.Get().LoadResistance );
            expectedValue = VoltageSourceTestInfo.Get().NominalVoltage * Resistor.OutputVoltage( seriesResistanceGain * VoltageSourceTestInfo.Get().SeriesResistance, Resistor.Parallel( VoltageSourceTestInfo.Get().LoadResistance, VoltageSourceTestInfo.Get().ParallelResistance ) );
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            TestInfo.VerboseMessage( message );

            // test load current
            _ = source.LoadCurrent( VoltageSourceTestInfo.Get().LoadResistance );
            _ = VoltageSourceTestInfo.Get().NominalVoltage * Resistor.OutputVoltage( seriesResistanceGain * VoltageSourceTestInfo.Get().SeriesResistance, Resistor.Parallel( VoltageSourceTestInfo.Get().LoadResistance, VoltageSourceTestInfo.Get().ParallelResistance ) ) / VoltageSourceTestInfo.Get().LoadResistance;
            item = $"load current {seriesResistanceGain}={nameof( seriesResistanceGain )}";
            actualValue = source.LoadCurrent( VoltageSourceTestInfo.Get().LoadResistance );
            expectedValue = VoltageSourceTestInfo.Get().NominalVoltage * Resistor.OutputVoltage( seriesResistanceGain * VoltageSourceTestInfo.Get().SeriesResistance, Resistor.Parallel( VoltageSourceTestInfo.Get().LoadResistance, VoltageSourceTestInfo.Get().ParallelResistance ) ) / VoltageSourceTestInfo.Get().LoadResistance;
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            TestInfo.VerboseMessage( message );
            double parallelConductanceGain = 1.2d;
            source.SeriesResistance /= seriesResistanceGain;
            source.ParallelConductance *= parallelConductanceGain;

            // test open voltage
            item = $"open voltage {parallelConductanceGain}={nameof( parallelConductanceGain )}";
            actualValue = source.LoadVoltage( Resistor.OpenResistance );
            expectedValue = VoltageSourceTestInfo.Get().NominalVoltage / source.Attenuation;
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            TestInfo.VerboseMessage( message );

            // test short voltage
            item = $"short voltage {parallelConductanceGain}={nameof( parallelConductanceGain )}";
            actualValue = source.LoadVoltage( 0d );
            expectedValue = 0d;
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            TestInfo.VerboseMessage( message );

            // test load voltage
            item = $"load voltage {parallelConductanceGain}={nameof( parallelConductanceGain )}";
            actualValue = source.LoadVoltage( VoltageSourceTestInfo.Get().LoadResistance );
            expectedValue = VoltageSourceTestInfo.Get().NominalVoltage * Resistor.OutputVoltage( VoltageSourceTestInfo.Get().SeriesResistance, Resistor.Parallel( VoltageSourceTestInfo.Get().LoadResistance, VoltageSourceTestInfo.Get().ParallelResistance / parallelConductanceGain ) );
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            TestInfo.VerboseMessage( message );

            // test load current
            item = $"load current {parallelConductanceGain}={nameof( parallelConductanceGain )}";
            actualValue = source.LoadCurrent( VoltageSourceTestInfo.Get().LoadResistance );
            expectedValue = VoltageSourceTestInfo.Get().NominalVoltage * Resistor.OutputVoltage( VoltageSourceTestInfo.Get().SeriesResistance, Resistor.Parallel( VoltageSourceTestInfo.Get().LoadResistance, VoltageSourceTestInfo.Get().ParallelResistance / parallelConductanceGain ) ) / VoltageSourceTestInfo.Get().LoadResistance;
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            TestInfo.VerboseMessage( message );
            double attenuationChange = 1.2d;
            source.ParallelConductance /= parallelConductanceGain;
            double newAttenuation = source.Attenuation * attenuationChange;
            double seriesR = newAttenuation * source.Resistance;
            double parallelR = seriesR / (newAttenuation - 1d);
            double newNominal = newAttenuation * VoltageSourceTestInfo.Get().NominalVoltage / source.Attenuation;
            source.Attenuation *= attenuationChange;

            // test open voltage
            item = $"open voltage {attenuationChange}={nameof( attenuationChange )}";
            actualValue = source.LoadVoltage( Resistor.OpenResistance );
            expectedValue = newNominal / source.Attenuation;
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            TestInfo.VerboseMessage( message );

            // test short voltage
            item = $"short voltage {attenuationChange}={nameof( attenuationChange )}";
            actualValue = source.LoadVoltage( 0d );
            expectedValue = 0d;
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            TestInfo.VerboseMessage( message );

            // test load voltage
            item = $"load voltage {attenuationChange}={nameof( attenuationChange )}";
            actualValue = source.LoadVoltage( VoltageSourceTestInfo.Get().LoadResistance );
            expectedValue = newNominal * Resistor.OutputVoltage( seriesR, Resistor.Parallel( VoltageSourceTestInfo.Get().LoadResistance, parallelR ) );
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            TestInfo.VerboseMessage( message );

            // test load current
            item = $"load current {attenuationChange}={nameof( attenuationChange )}";
            actualValue = source.LoadCurrent( VoltageSourceTestInfo.Get().LoadResistance );
            expectedValue = newNominal * Resistor.OutputVoltage( seriesR, Resistor.Parallel( VoltageSourceTestInfo.Get().LoadResistance, parallelR ) ) / VoltageSourceTestInfo.Get().LoadResistance;
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            TestInfo.VerboseMessage( message );
        }

        #endregion

    }
}
