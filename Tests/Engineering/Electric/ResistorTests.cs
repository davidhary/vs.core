using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

using isr.Core.Engineering;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.EngineeringTests
{

    /// <summary> A resistance unit tests. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-09-16 </para>
    /// </remarks>
    [TestClass()]
    public class ResistorTests
    {

        #region " TEST INPUTS "

        /// <summary> Gets or sets the left resistance. </summary>
        /// <value> The left resistance. </value>
        public static double LeftResistance { get; set; } = 1000d;

        /// <summary> Gets or sets the right resistance. </summary>
        /// <value> The right resistance. </value>
        public static double RightResistance { get; set; } = 500d;

        /// <summary> Gets or sets the load resistance. </summary>
        /// <value> The load resistance. </value>
        public static double LoadResistance { get; set; } = 250d;

        /// <summary> Gets or sets the negative resistance. </summary>
        /// <value> The negative resistance. </value>
        public static double NegativeResistance { get; set; } = -100;

        #endregion

        #region " PARALLEL TESTS "

        /// <summary> (Unit Test Method) tests parallel. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void ParallelTest()
        {
            double actualValue = Resistor.ToParallel( LeftResistance, RightResistance );
            double expectedValue = 1d / (1d / LeftResistance + 1d / RightResistance);
            Assert.AreEqual( expectedValue, actualValue );
            actualValue = Resistor.ShuntConductor( LeftResistance, 1d / RightResistance );
            Assert.AreEqual( expectedValue, actualValue );
        }

        /// <summary> (Unit Test Method) tests parallel short. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void ParallelShortTest()
        {
            double actualValue = Resistor.ToParallel( Resistor.ShortResistance, RightResistance );
            double expectedValue = Resistor.ShortResistance;
            Assert.AreEqual( expectedValue, actualValue );
            actualValue = Resistor.ToParallel( LeftResistance, Resistor.ShortResistance );
            expectedValue = Resistor.ShortResistance;
            Assert.AreEqual( expectedValue, actualValue );
            actualValue = Resistor.ToParallel( Resistor.ShortResistance, Resistor.ShortResistance );
            expectedValue = Resistor.ShortResistance;
            Assert.AreEqual( expectedValue, actualValue );
            actualValue = Resistor.ShuntConductor( LeftResistance, Conductor.ShortConductance );
            expectedValue = Resistor.ShortResistance;
            Assert.AreEqual( expectedValue, actualValue );
            actualValue = Resistor.ShuntConductor( Resistor.ShortResistance, Conductor.ShortConductance );
            expectedValue = Resistor.ShortResistance;
            Assert.AreEqual( expectedValue, actualValue );
        }

        /// <summary> (Unit Test Method) tests parallel open. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void ParallelOpenTest()
        {
            double actualValue = Resistor.ToParallel( Resistor.OpenResistance, RightResistance );
            double expectedValue = RightResistance;
            Assert.AreEqual( expectedValue, actualValue );
            actualValue = Resistor.ToParallel( LeftResistance, Resistor.OpenResistance );
            expectedValue = LeftResistance;
            Assert.AreEqual( expectedValue, actualValue );
            actualValue = Resistor.ToParallel( Resistor.OpenResistance, Resistor.OpenResistance );
            expectedValue = Resistor.OpenResistance;
            Assert.AreEqual( expectedValue, actualValue );
            actualValue = Resistor.ShuntConductor( LeftResistance, Conductor.OpenConductance );
            expectedValue = LeftResistance;
            Assert.AreEqual( expectedValue, actualValue );
            actualValue = Resistor.ShuntConductor( Resistor.OpenResistance, Conductor.OpenConductance );
            expectedValue = Resistor.OpenResistance;
            Assert.AreEqual( expectedValue, actualValue );
        }

        /// <summary> (Unit Test Method) tests parallel negative left resistance exception. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        [ExpectedException( typeof( ArgumentOutOfRangeException ), "Negative Resistance inappropriately allowed" )]
        public void ParallelNegativeLeftResistanceExceptionTest()
        {
            _ = Resistor.ToParallel( NegativeResistance, RightResistance );
        }

        /// <summary> (Unit Test Method) tests parallel negative right resistance exception. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        [ExpectedException( typeof( ArgumentOutOfRangeException ), "Negative Resistance inappropriately allowed" )]
        public void ParallelNegativeRightResistanceExceptionTest()
        {
            _ = Resistor.ToParallel( LeftResistance, NegativeResistance );
        }

        #endregion

        #region " SERIES TESTS "

        /// <summary> (Unit Test Method) tests series. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void SeriesTest()
        {
            double actualValue = Resistor.ToSeries( LeftResistance, RightResistance );
            double expectedValue = LeftResistance + RightResistance;
            Assert.AreEqual( expectedValue, actualValue );
        }

        /// <summary> (Unit Test Method) tests series short. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void SeriesShortTest()
        {
            double actualValue = Resistor.ToSeries( Resistor.ShortResistance, RightResistance );
            double expectedValue = RightResistance;
            Assert.AreEqual( expectedValue, actualValue );
            actualValue = Resistor.ToSeries( LeftResistance, Resistor.ShortResistance );
            expectedValue = LeftResistance;
            Assert.AreEqual( expectedValue, actualValue );
            actualValue = Resistor.ToSeries( Resistor.ShortResistance, Resistor.ShortResistance );
            expectedValue = Resistor.ShortResistance;
            Assert.AreEqual( expectedValue, actualValue );
        }

        /// <summary> (Unit Test Method) tests series open. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void SeriesOpenTest()
        {
            double actualValue = Resistor.ToSeries( Resistor.OpenResistance, RightResistance );
            double expectedValue = Resistor.OpenResistance;
            Assert.AreEqual( expectedValue, actualValue );
            actualValue = Resistor.ToSeries( LeftResistance, Resistor.OpenResistance );
            expectedValue = Resistor.OpenResistance;
            Assert.AreEqual( expectedValue, actualValue );
            actualValue = Resistor.ToSeries( Resistor.OpenResistance, Resistor.OpenResistance );
            expectedValue = Resistor.OpenResistance;
            Assert.AreEqual( expectedValue, actualValue );
        }

        /// <summary> (Unit Test Method) tests series negative left resistance exception. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        [ExpectedException( typeof( ArgumentOutOfRangeException ), "Negative Resistance inappropriately allowed" )]
        public void SeriesNegativeLeftResistanceExceptionTest()
        {
            _ = Resistor.ToSeries( NegativeResistance, RightResistance );
        }

        /// <summary> (Unit Test Method) tests series negative right resistance exception. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        [ExpectedException( typeof( ArgumentOutOfRangeException ), "Negative Resistance inappropriately allowed" )]
        public void SeriesNegativeRightResistanceExceptionTest()
        {
            _ = Resistor.ToSeries( LeftResistance, NegativeResistance );
        }

        #endregion

        #region " OUTPUT CURRENT TESTS "

        /// <summary> (Unit Test Method) tests output current. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void OutputCurrentTest()
        {
            double actualValue = Resistor.ToOutputCurrent( LeftResistance, RightResistance );
            double expectedValue = Resistor.Parallel( LeftResistance, RightResistance ) / RightResistance;
            Assert.AreEqual( expectedValue, actualValue );
        }

        /// <summary> (Unit Test Method) tests output current short. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void OutputCurrentShortTest()
        {
            double actualValue = Resistor.ToOutputCurrent( Resistor.ShortResistance, RightResistance );
            double expectedValue = 0d;
            Assert.AreEqual( expectedValue, actualValue );
            actualValue = Resistor.ToOutputCurrent( LeftResistance, Resistor.ShortResistance );
            expectedValue = 1d;
            Assert.AreEqual( expectedValue, actualValue );
        }

        /// <summary> (Unit Test Method) tests output current short exception. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        [ExpectedException( typeof( InvalidOperationException ), "Two shorts inappropriately allowed" )]
        public void OutputCurrentShortExceptionTest()
        {
            _ = Resistor.ToOutputCurrent( Resistor.ShortResistance, Resistor.ShortResistance );
        }

        /// <summary> (Unit Test Method) tests output current open. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void OutputCurrentOpenTest()
        {
            double actualValue = Resistor.ToOutputCurrent( Resistor.OpenResistance, RightResistance );
            double expectedValue = 1d;
            Assert.AreEqual( expectedValue, actualValue );
            actualValue = Resistor.ToOutputCurrent( LeftResistance, Resistor.OpenResistance );
            expectedValue = 0d;
            Assert.AreEqual( expectedValue, actualValue );
        }

        /// <summary> (Unit Test Method) tests output current opens exception. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        [ExpectedException( typeof( InvalidOperationException ), "Two opens inappropriately allowed" )]
        public void OutputCurrentOpensExceptionTest()
        {
            _ = Resistor.ToOutputCurrent( Resistor.OpenResistance, Resistor.OpenResistance );
        }

        /// <summary>
        /// (Unit Test Method) tests output current negative left resistance exception.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        [ExpectedException( typeof( ArgumentOutOfRangeException ), "Negative Resistance inappropriately allowed" )]
        public void OutputCurrentNegativeLeftResistanceExceptionTest()
        {
            _ = Resistor.ToOutputCurrent( NegativeResistance, RightResistance );
        }

        /// <summary>
        /// (Unit Test Method) tests output current negative right resistance exception.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        [ExpectedException( typeof( ArgumentOutOfRangeException ), "Negative Resistance inappropriately allowed" )]
        public void OutputCurrentNegativeRightResistanceExceptionTest()
        {
            _ = Resistor.ToOutputCurrent( LeftResistance, NegativeResistance );
        }

        #endregion

        #region " OUTPUT VOLTAGE TESTS "

        /// <summary> (Unit Test Method) tests output voltage. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void OutputVoltageTest()
        {
            double actualValue = Resistor.ToOutputVoltage( LeftResistance, RightResistance );
            double expectedValue = RightResistance / Resistor.ToSeries( LeftResistance, RightResistance );
            Assert.AreEqual( expectedValue, actualValue );
        }

        /// <summary> (Unit Test Method) tests output voltage short. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void OutputVoltageShortTest()
        {
            double actualValue = Resistor.ToOutputVoltage( Resistor.ShortResistance, RightResistance );
            double expectedValue = 1d;
            Assert.AreEqual( expectedValue, actualValue, $"Resistor.ToOutputVoltage({Resistor.ShortResistance},{RightResistance})" );
            actualValue = Resistor.ToOutputVoltage( LeftResistance, Resistor.ShortResistance );
            expectedValue = 0d;
            Assert.AreEqual( expectedValue, actualValue, $"Resistor.ToOutputVoltage({LeftResistance},{Resistor.ShortResistance})" );
        }

        /// <summary> (Unit Test Method) tests output voltage shorts exception. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        [ExpectedException( typeof( InvalidOperationException ), "Two Shorts inappropriately allowed" )]
        public void OutputVoltageShortsExceptionTest()
        {
            _ = Resistor.ToOutputVoltage( Resistor.ShortResistance, Resistor.ShortResistance );
        }

        /// <summary> (Unit Test Method) tests output voltage open. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void OutputVoltageOpenTest()
        {
            double actualValue = Resistor.ToOutputVoltage( Resistor.OpenResistance, RightResistance );
            double expectedValue = 0d;
            Assert.AreEqual( expectedValue, actualValue, $"Resistor.ToOutputVoltage({Resistor.OpenResistance},{RightResistance})" );
            actualValue = Resistor.ToOutputVoltage( LeftResistance, Resistor.OpenResistance );
            expectedValue = 1d;
            Assert.AreEqual( expectedValue, actualValue, $"Resistor.ToOutputVoltage({LeftResistance},{Resistor.OpenResistance})" );
        }

        /// <summary> (Unit Test Method) tests output voltage opens exception. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        [ExpectedException( typeof( InvalidOperationException ), "Two Opens inappropriately allowed" )]
        public void OutputVoltageOpensExceptionTest()
        {
            _ = Resistor.ToOutputVoltage( Resistor.OpenResistance, Resistor.OpenResistance );
        }

        /// <summary>
        /// (Unit Test Method) tests output voltage negative left resistance exception.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        [ExpectedException( typeof( ArgumentOutOfRangeException ), "Negative Resistance inappropriately allowed" )]
        public void OutputVoltageNegativeLeftResistanceExceptionTest()
        {
            _ = Resistor.ToOutputVoltage( NegativeResistance, RightResistance );
        }

        /// <summary>
        /// (Unit Test Method) tests output voltage negative right resistance exception.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        [ExpectedException( typeof( ArgumentOutOfRangeException ), "Negative Resistance inappropriately allowed" )]
        public void OutputVoltageNegativeRightResistanceExceptionTest()
        {
            _ = Resistor.ToOutputVoltage( LeftResistance, NegativeResistance );
        }

        #endregion

        #region " STANDARD RESISTANCE TESTS "

        /// <summary> Tests resistance decade value. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="resistance">          The resistance. </param>
        /// <param name="decadeCount">         Number of decades. </param>
        /// <param name="expectedDecadeValue"> The expected decade value. </param>
        public static void TestResistanceDecadeValue( double resistance, int decadeCount, double expectedDecadeValue )
        {
            double actualDecadeValue = Resistor.ToDecadeValue( resistance, decadeCount );
            Assert.AreEqual( expectedDecadeValue, actualDecadeValue, $"Decade value for standard {decadeCount} for resistance {resistance}" );
        }

        /// <summary> (Unit Test Method) tests resistance decade value. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void ResistanceDecadeValueTest()
        {
            TestResistanceDecadeValue( 0.1011d, 192, 1.01d );
            TestResistanceDecadeValue( 1.011d, 192, 1.01d );
            TestResistanceDecadeValue( 10.11d, 192, 1.01d );
            TestResistanceDecadeValue( 1011d, 192, 1.01d );
            TestResistanceDecadeValue( 2494d, 192, 2.49d );
            TestResistanceDecadeValue( 249.4d, 192, 2.49d );
            TestResistanceDecadeValue( 3340d, 192, 3.36d );
            TestResistanceDecadeValue( 3331d, 192, 3.32d );
            TestResistanceDecadeValue( 594d, 192, 5.97d );
        }

        /// <summary> Tests standard resistance. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="resistance">         The resistance. </param>
        /// <param name="decadeCount">        Number of decades. </param>
        /// <param name="expectedResistance"> The expected resistance. </param>
        public static void TestStandardResistance( double resistance, int decadeCount, double expectedResistance )
        {
            double actualResistance = Resistor.ToStandardResistance( resistance, decadeCount );
            Assert.AreEqual( expectedResistance, actualResistance, $"Standard resistance {decadeCount} for resistance {resistance}" );
        }

        /// <summary> Gets or sets the standard resistor list 192. </summary>
        /// <value> The standard resistor list 192. </value>
        private static List<double> StandardResistorList192 { get; set; }

        /// <summary> Enumerates standard resistors 192 in this collection. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns>
        /// An enumerator that allows for each to be used to process standard resistors 192 in this
        /// collection.
        /// </returns>
        public static IEnumerable<double> StandardResistors192()
        {
            if ( StandardResistorList192 is null )
            {
                StandardResistorList192 = new List<double>();
                StandardResistorList192.AddRange( new double[] { 1.0d, 1.01d, 1.02d, 1.04d, 1.05d, 1.06d, 1.07d, 1.09d, 1.1d, 1.11d, 1.13d, 1.14d } );
                StandardResistorList192.AddRange( new double[] { 1.15d, 1.17d, 1.18d, 1.2d, 1.21d, 1.23d, 1.24d, 1.26d, 1.27d, 1.29d, 1.3d, 1.32d } );
                StandardResistorList192.AddRange( new double[] { 1.33d, 1.35d, 1.37d, 1.38d, 1.4d, 1.42d, 1.43d, 1.45d, 1.47d, 1.49d, 1.5d, 1.52d } );
                StandardResistorList192.AddRange( new double[] { 1.54d, 1.56d, 1.58d, 1.6d, 1.62d, 1.64d, 1.65d, 1.67d, 1.69d, 1.72d, 1.74d, 1.76d } );
                StandardResistorList192.AddRange( new double[] { 1.78d, 1.8d, 1.82d, 1.84d, 1.87d, 1.89d, 1.91d, 1.93d, 1.96d, 1.98d, 2.0d, 2.03d } );
                StandardResistorList192.AddRange( new double[] { 2.05d, 2.08d, 2.1d, 2.13d, 2.15d, 2.18d, 2.21d, 2.23d, 2.26d, 2.29d, 2.32d, 2.34d } );
                StandardResistorList192.AddRange( new double[] { 2.37d, 2.4d, 2.43d, 2.46d, 2.49d, 2.52d, 2.55d, 2.58d, 2.61d, 2.64d, 2.67d, 2.71d } );
                StandardResistorList192.AddRange( new double[] { 2.74d, 2.77d, 2.8d, 2.84d, 2.87d, 2.91d, 2.94d, 2.98d, 3.01d, 3.05d, 3.09d, 3.12d } );
                StandardResistorList192.AddRange( new double[] { 3.16d, 3.2d, 3.24d, 3.28d, 3.32d, 3.36d, 3.4d, 3.44d, 3.48d, 3.52d, 3.57d, 3.61d } );
                StandardResistorList192.AddRange( new double[] { 3.65d, 3.7d, 3.74d, 3.79d, 3.83d, 3.88d, 3.92d, 3.97d, 4.02d, 4.07d, 4.12d, 4.17d } );
                StandardResistorList192.AddRange( new double[] { 4.22d, 4.27d, 4.32d, 4.37d, 4.42d, 4.48d, 4.53d, 4.59d, 4.64d, 4.7d, 4.75d, 4.81d } );
                StandardResistorList192.AddRange( new double[] { 4.87d, 4.93d, 4.99d, 5.05d, 5.11d, 5.17d, 5.23d, 5.3d, 5.36d, 5.42d, 5.49d, 5.56d } );
                StandardResistorList192.AddRange( new double[] { 5.62d, 5.69d, 5.76d, 5.83d, 5.9d, 5.97d, 6.04d, 6.12d, 6.19d, 6.26d, 6.34d, 6.42d } );
                StandardResistorList192.AddRange( new double[] { 6.49d, 6.57d, 6.65d, 6.73d, 6.81d, 6.9d, 6.98d, 7.06d, 7.15d, 7.23d, 7.32d, 7.41d } );
                StandardResistorList192.AddRange( new double[] { 7.5d, 7.59d, 7.68d, 7.77d, 7.87d, 7.96d, 8.06d, 8.16d, 8.25d, 8.35d, 8.45d, 8.56d } );
                StandardResistorList192.AddRange( new double[] { 8.66d, 8.76d, 8.87d, 8.98d, 9.09d, 9.19d, 9.31d, 9.42d, 9.53d, 9.65d, 9.76d, 9.88d } );
            }

            return StandardResistorList192;
        }

        /// <summary> (Unit Test Method) tests decade value. </summary>
        /// <remarks>
        /// Found three errors in the original Omega table. One error in the resistor guide web site.
        /// </remarks>
        [TestMethod()]
        public void DecadeValueTest()
        {
            for ( int i = 0, loopTo = StandardResistors192().Count() - 1; i <= loopTo; i++ )
            {
                Assert.AreEqual( StandardResistors192().ElementAtOrDefault( i ), Resistor.DecadeValue( i, StandardResistors192().Count() ), $"Decade value for {i}/{StandardResistors192().Count()}" );
            }
        }

        /// <summary> (Unit Test Method) tests standard resistance. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void StandardResistanceTest()
        {
            TestStandardResistance( 0.1011d, 192, 0.101d );
            TestStandardResistance( 1.011d, 192, 1.01d );
            TestStandardResistance( 10.11d, 192, 10.1d );
            TestStandardResistance( 10.16d, 192, 10.1d );
            TestStandardResistance( 1011d, 192, 1010d );
            TestStandardResistance( 1016d, 192, 1010d );
            TestStandardResistance( 594d, 192, 597d );
            TestStandardResistance( 906d, 192, 909d );
            TestStandardResistance( 9060d, 192, 9090d );
            TestStandardResistance( 90600d, 192, 90900d );
        }

        #endregion

    }
}
