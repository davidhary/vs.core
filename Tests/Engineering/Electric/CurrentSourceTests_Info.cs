﻿using isr.Core.Engineering;

namespace isr.Core.EngineeringTests
{

    /// <summary> Information about the current source test. </summary>
    /// <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-11-26 </para></remarks>
    [System.Runtime.CompilerServices.CompilerGenerated()]
    [System.CodeDom.Compiler.GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "16.7.0.0")]
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)]
    internal class CurrentSourceTestInfo : ApplicationSettingsBase
    {

        #region " SINGLETON "

        /// <summary>
        /// Initializes an instance of the <see cref="T:System.Configuration.ApplicationSettingsBase" />
        /// class to its default state.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        private CurrentSourceTestInfo() : base()
        {
        }

        /// <summary>
        /// Gets the locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        /// <value> The sync locker. </value>
        private static object _SyncLocker { get; set; } = new object();

        /// <summary> Gets the instance. </summary>
        /// <value> The instance. </value>
        private static CurrentSourceTestInfo _Instance { get; set; }

        /// <summary> Instantiates the class. </summary>
        /// <remarks> Use this property to instantiate a single instance of this class. This class uses
        /// lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
        /// <returns> A new or existing instance of the class. </returns>
        public static CurrentSourceTestInfo Get()
        {
            if (_Instance is null)
            {
                lock (_SyncLocker)
                    _Instance = (CurrentSourceTestInfo)Synchronized(new CurrentSourceTestInfo());
            }

            return _Instance;
        }

        /// <summary> Returns true if an instance of the class was created and not disposed. </summary>
        /// <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
        public static bool Instantiated
        {
            get
            {
                lock (_SyncLocker)
                    return _Instance is object;
            }
        }

        #endregion

        #region " CONFIGURATION EDITOR - EXCLUDED - NOT NEEDED BUT SHOWS HOW THIS WORKS "

        /// <summary> Opens the settings editor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        public static void OpenSettingsEditor()
        {
            WindowsForms.EditConfiguration($"{typeof(CurrentSourceTestInfo)} Editor", Get());
        }

        #endregion

        #region " CONFIGURATION INFORMATION "

        /// <summary> Returns true if test settings exist. </summary>
        /// <value> <c>True</c> if testing settings exit. </value>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "True")]
        public bool Exists
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Returns true to output test messages at the verbose level. </summary>
        /// <value> The verbose messaging level. </value>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "False")]
        public bool Verbose
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Returns true to enable this device. </summary>
        /// <value> The device enable option. </value>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "True")]
        public bool Enabled
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets all. </summary>
        /// <value> all. </value>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "True")]
        public bool All
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        #endregion

        #region " CURRENT SOURCE TEST SETTINGS "

        /// <summary> Gets or sets the nominal current. </summary>
        /// <value> The nominal current. </value>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "0.004")]
        public double NominalCurrent
        {
            get
            {
                return AppSettingGetter(double.NaN);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets source conductance. </summary>
        /// <value> The source conductance. </value>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "0.001")]
        public double SourceConductance
        {
            get
            {
                return AppSettingGetter(double.NaN);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the load resistance. </summary>
        /// <value> The load resistance. </value>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "700")]
        public double LoadResistance
        {
            get
            {
                return AppSettingGetter(double.NaN);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the nominal voltage. </summary>
        /// <value> The nominal voltage. </value>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "5.0")]
        public double NominalVoltage
        {
            get
            {
                return AppSettingGetter(double.NaN);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        #endregion

        #region " ATTENUETED CURRENT SOURCE TEST SETTINGS "

        /// <summary> Gets or sets the series resistance. </summary>
        /// <value> The series resistance. </value>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "500")]
        public double SeriesResistance
        {
            get
            {
                return AppSettingGetter(double.NaN);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the parallel resistance. </summary>
        /// <value> The parallel resistance. </value>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute( "1100")]
        public double ParallelResistance
        {
            get
            {
                return AppSettingGetter(double.NaN);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets the parallel conductance. </summary>
        /// <value> The parallel conductance. </value>
        public double ParallelConductance
        {
            get
            {
                return Resistor.ToConductance(ParallelResistance);
            }
        }

        #endregion

    }
}