using System;

using isr.Core.Engineering;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.EngineeringTests
{

    /// <summary> Unit tests for the attenuated current source. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-12-12 </para>
    /// </remarks>
    [TestClass()]
    public class AttenuatedCurrentSourceTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( EngineeringTests.AttenuatedCurrentSourceTests.TestInfo.TimeZoneOffset ) < expectedUpperLimit,
                                $"{nameof( EngineeringTests.TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            Assert.IsTrue( CurrentSourceTestInfo.Get().Exists, $"{typeof( CurrentSourceTestInfo )} settings should exist" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " CONSTRUCTION TESTS "

        /// <summary> (Unit Test Method) tests build. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void BuildTest()
        {
            double attenuation = AttenuatedCurrentSource.ToAttenuation( CurrentSourceTestInfo.Get().SeriesResistance, CurrentSourceTestInfo.Get().ParallelConductance );
            double equivalenctConductance = Conductor.SeriesResistor( CurrentSourceTestInfo.Get().ParallelConductance, CurrentSourceTestInfo.Get().SeriesResistance );


            // construct a current source 
            _ = new CurrentSource( CurrentSourceTestInfo.Get().NominalCurrent, equivalenctConductance );
            AttenuatedCurrentSource attenuatedSource = null;
            double doubleEpsilon = 0.0000000001d;
            string attenuatedSourcetarget = string.Empty;
            string sourceTarget = string.Empty;
            for ( int i = 1; i <= 3; i++ )
            {
                CurrentSource currentSource;
                switch ( i )
                {
                    case 1:
                        {
                            attenuatedSourcetarget = "nominal attenuated current source";
                            attenuatedSource = new AttenuatedCurrentSource( ( decimal ) CurrentSourceTestInfo.Get().NominalCurrent, CurrentSourceTestInfo.Get().SeriesResistance, CurrentSourceTestInfo.Get().ParallelConductance );
                            sourceTarget = "current source from nominal";
                            break;
                        }

                    case 2:
                        {
                            attenuatedSourcetarget = "equivalent attenuated current source";
                            attenuatedSource = new AttenuatedCurrentSource( CurrentSourceTestInfo.Get().NominalCurrent / attenuation, CurrentSourceTestInfo.Get().SeriesResistance, CurrentSourceTestInfo.Get().ParallelConductance );
                            sourceTarget = "current source from equivalent";
                            break;
                        }

                    case 3:
                        {
                            attenuatedSourcetarget = "converted attenuated current source";
                            currentSource = new CurrentSource( CurrentSourceTestInfo.Get().NominalCurrent / attenuation, equivalenctConductance );
                            attenuatedSource = CurrentSource.ToAttenuatedCurrentSource( currentSource, attenuation );
                            sourceTarget = "current source from converted";
                            break;
                        }
                }

                currentSource = attenuatedSource.ToCurrentSource();

                // test the attenuated voltage source
                string target = attenuatedSourcetarget;
                double actualValue = attenuatedSource.NominalCurrent;
                double expectedValue = CurrentSourceTestInfo.Get().NominalCurrent;
                string outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                string item = "nominal current";
                string message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                TestInfo.VerboseMessage( message );
                target = attenuatedSourcetarget;
                actualValue = attenuatedSource.Current;
                expectedValue = CurrentSourceTestInfo.Get().NominalCurrent / attenuation;
                outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                item = "short load current";
                message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                TestInfo.VerboseMessage( message );
                target = sourceTarget;
                actualValue = currentSource.Current;
                expectedValue = CurrentSourceTestInfo.Get().NominalCurrent / attenuation;
                outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                item = "short load current";
                message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                TestInfo.VerboseMessage( message );
                target = attenuatedSourcetarget;
                actualValue = attenuatedSource.Conductance;
                expectedValue = equivalenctConductance;
                item = "equivalent conductance";
                outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                TestInfo.VerboseMessage( message );
                target = sourceTarget;
                actualValue = attenuatedSource.Conductance;
                expectedValue = equivalenctConductance;
                item = "equivalent conductance";
                outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                TestInfo.VerboseMessage( message );
                target = attenuatedSourcetarget;
                actualValue = attenuatedSource.SeriesResistance;
                expectedValue = CurrentSourceTestInfo.Get().SeriesResistance;
                outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                item = "series resistance";
                message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                TestInfo.VerboseMessage( message );
                target = attenuatedSourcetarget;
                actualValue = attenuatedSource.Attenuation;
                expectedValue = attenuation;
                item = "attenuation";
                outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                TestInfo.VerboseMessage( message );
                target = attenuatedSourcetarget;
                actualValue = attenuatedSource.ParallelConductance;
                expectedValue = CurrentSourceTestInfo.Get().ParallelConductance;
                item = "parallel conductance";
                outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                TestInfo.VerboseMessage( message );
            }
        }

        #endregion

        #region " EXCEPTION CONVERSION TESTS "

        /// <summary>
        /// (Unit Test Method) tests exception for converting a current source to an attenuated current
        /// source with invalid attenuation.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        [ExpectedException( typeof( ArgumentOutOfRangeException ), "Attenuation smaller than 1 inappropriately allowed" )]
        public void ConvertAttenuationExceptionTest()
        {
            double attenuation = AttenuatedVoltageSource.MinimumAttenuation - 0.01d;
            var currentSource = new CurrentSource( CurrentSourceTestInfo.Get().NominalCurrent, CurrentSourceTestInfo.Get().SourceConductance );
            _ = CurrentSource.ToAttenuatedCurrentSource( currentSource, attenuation );
        }

        /// <summary>
        /// (Unit Test Method) tests exception for converting to an attenuated source using a null source.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        [ExpectedException( typeof( ArgumentNullException ), "Conversion with null source allowed" )]
        public void ConvertNothingExceptionTest()
        {
            double attenuation = AttenuatedVoltageSource.MinimumAttenuation + 0.01d;
            CurrentSource currentSource = null;
            _ = CurrentSource.ToAttenuatedCurrentSource( currentSource, attenuation );
        }

        #endregion

        #region " LOAD AND PROPERTY CHANGE TESTS "

        /// <summary> (Unit Test Method) tests loading an attenuated current source. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void LoadTest()
        {
            var source = new AttenuatedCurrentSource( ( decimal ) CurrentSourceTestInfo.Get().NominalCurrent, CurrentSourceTestInfo.Get().SeriesResistance, CurrentSourceTestInfo.Get().ParallelConductance );
            double doubleEpsilon = 0.0000000001d;

            // test the attenuated current source
            string target = "attenuated current source";

            // test open current
            string item = "open load current";
            double actualValue = source.LoadCurrent( Conductor.OpenConductance );
            double expectedValue = 0d;
            string outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            string message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            TestInfo.VerboseMessage( message );
            item = "short load current";
            actualValue = source.LoadCurrent( Conductor.ShortConductance );
            expectedValue = ( double ) ( decimal ) CurrentSourceTestInfo.Get().NominalCurrent / source.Attenuation;
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            TestInfo.VerboseMessage( message );

            // test short voltage
            item = "short voltage";
            actualValue = source.LoadVoltage( 0d );
            expectedValue = 0d;
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            TestInfo.VerboseMessage( message );

            // test load current
            item = "load current";
            actualValue = source.LoadCurrent( Resistor.ToConductance( CurrentSourceTestInfo.Get().LoadResistance ) );
            expectedValue = CurrentSourceTestInfo.Get().NominalCurrent * Conductor.OutputCurrent( CurrentSourceTestInfo.Get().ParallelConductance, Resistor.ToConductance( Resistor.ToSeries( CurrentSourceTestInfo.Get().LoadResistance, CurrentSourceTestInfo.Get().SeriesResistance ) ) );
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            TestInfo.VerboseMessage( message );

            // test load voltage
            item = "load voltage";
            actualValue = source.LoadVoltage( CurrentSourceTestInfo.Get().LoadResistance );
            expectedValue = CurrentSourceTestInfo.Get().NominalCurrent * CurrentSourceTestInfo.Get().LoadResistance * Conductor.OutputCurrent( CurrentSourceTestInfo.Get().ParallelConductance, Resistor.ToConductance( Resistor.ToSeries( CurrentSourceTestInfo.Get().LoadResistance, CurrentSourceTestInfo.Get().SeriesResistance ) ) );
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            TestInfo.VerboseMessage( message );
        }

        /// <summary>
        /// (Unit Test Method) tests changing properties of an attenuated voltage source.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void PropertiesChangeTest()
        {

            // test changing Attenuated voltage source properties
            var source = new AttenuatedCurrentSource( ( decimal ) CurrentSourceTestInfo.Get().NominalCurrent, CurrentSourceTestInfo.Get().SeriesResistance, CurrentSourceTestInfo.Get().ParallelConductance );
            double doubleEpsilon = 0.0000000001d;

            // test the attenuated voltage source
            string target = "attenuated current source";
            double currentGain = 1.5d;

            // test shot load current
            string item = $"short load current {currentGain}={nameof( currentGain )}";
            source.NominalCurrent *= currentGain;
            double actualValue = source.LoadCurrent( Conductor.ShortConductance );
            double expectedValue = currentGain * CurrentSourceTestInfo.Get().NominalCurrent / source.Attenuation;
            string outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            string message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            TestInfo.VerboseMessage( message );

            // test open current
            item = $"open current {currentGain}={nameof( currentGain )}";
            actualValue = source.LoadCurrent( Conductor.OpenConductance );
            expectedValue = 0d;
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            TestInfo.VerboseMessage( message );

            // test load current
            item = $"load current {currentGain}={nameof( currentGain )}";
            actualValue = source.LoadCurrent( Resistor.ToConductance( CurrentSourceTestInfo.Get().LoadResistance ) );
            expectedValue = currentGain * CurrentSourceTestInfo.Get().NominalCurrent * Conductor.OutputCurrent( CurrentSourceTestInfo.Get().ParallelConductance, Resistor.ToConductance( Resistor.ToSeries( CurrentSourceTestInfo.Get().LoadResistance, CurrentSourceTestInfo.Get().SeriesResistance ) ) );
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            TestInfo.VerboseMessage( message );
            source.NominalCurrent /= currentGain;
            double seriesResistanceGain = 1.2d;
            source.SeriesResistance *= seriesResistanceGain;
            item = $"short load current {seriesResistanceGain}={nameof( seriesResistanceGain )}";
            actualValue = source.LoadCurrent( Conductor.ShortConductance );
            expectedValue = CurrentSourceTestInfo.Get().NominalCurrent / source.Attenuation;
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            TestInfo.VerboseMessage( message );

            // test short current
            item = $"open load current {seriesResistanceGain}={nameof( seriesResistanceGain )}";
            actualValue = source.LoadCurrent( Conductor.OpenConductance );
            expectedValue = 0d;
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            TestInfo.VerboseMessage( message );

            // test load current
            item = $"load current{seriesResistanceGain}={nameof( seriesResistanceGain )}";
            actualValue = source.LoadCurrent( Resistor.ToConductance( CurrentSourceTestInfo.Get().LoadResistance ) );
            expectedValue = CurrentSourceTestInfo.Get().NominalCurrent * Conductor.OutputCurrent( CurrentSourceTestInfo.Get().ParallelConductance, Resistor.ToConductance( Resistor.ToSeries( CurrentSourceTestInfo.Get().LoadResistance, seriesResistanceGain * CurrentSourceTestInfo.Get().SeriesResistance ) ) );
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            TestInfo.VerboseMessage( message );

            // test load voltage
            item = $"load voltage {seriesResistanceGain}={nameof( seriesResistanceGain )}";
            actualValue = source.LoadVoltage( CurrentSourceTestInfo.Get().LoadResistance );
            expectedValue = CurrentSourceTestInfo.Get().LoadResistance * CurrentSourceTestInfo.Get().NominalCurrent * Conductor.OutputCurrent( CurrentSourceTestInfo.Get().ParallelConductance, Resistor.ToConductance( Resistor.ToSeries( CurrentSourceTestInfo.Get().LoadResistance, seriesResistanceGain * CurrentSourceTestInfo.Get().SeriesResistance ) ) );
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            TestInfo.VerboseMessage( message );
            double parallelConductanceGain = 1.2d;
            source.SeriesResistance /= seriesResistanceGain;
            source.ParallelConductance *= parallelConductanceGain;

            // test short current
            item = $"Short load current {parallelConductanceGain}={nameof( parallelConductanceGain )}";
            actualValue = source.LoadCurrent( Conductor.ShortConductance );
            expectedValue = CurrentSourceTestInfo.Get().NominalCurrent / source.Attenuation;
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            TestInfo.VerboseMessage( message );

            // test open current
            item = $"open load current {parallelConductanceGain}={nameof( parallelConductanceGain )}";
            actualValue = source.LoadCurrent( Conductor.OpenConductance );
            expectedValue = 0d;
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            TestInfo.VerboseMessage( message );

            // test load voltage
            item = $"load voltage {parallelConductanceGain}={nameof( parallelConductanceGain )}";
            actualValue = source.LoadVoltage( CurrentSourceTestInfo.Get().LoadResistance );
            expectedValue = CurrentSourceTestInfo.Get().LoadResistance * CurrentSourceTestInfo.Get().NominalCurrent * Conductor.OutputCurrent( parallelConductanceGain * CurrentSourceTestInfo.Get().ParallelConductance, Resistor.ToConductance( Resistor.ToSeries( CurrentSourceTestInfo.Get().LoadResistance, CurrentSourceTestInfo.Get().SeriesResistance ) ) );
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            TestInfo.VerboseMessage( message );

            // test load current
            item = $"load current {parallelConductanceGain}={nameof( parallelConductanceGain )}";
            actualValue = source.LoadCurrent( Resistor.ToConductance( CurrentSourceTestInfo.Get().LoadResistance ) );
            expectedValue = CurrentSourceTestInfo.Get().NominalCurrent * Conductor.OutputCurrent( parallelConductanceGain * CurrentSourceTestInfo.Get().ParallelConductance, Resistor.ToConductance( Resistor.ToSeries( CurrentSourceTestInfo.Get().LoadResistance, CurrentSourceTestInfo.Get().SeriesResistance ) ) );
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            TestInfo.VerboseMessage( message );
            double attenuationChange = 1.2d;
            source.ParallelConductance /= parallelConductanceGain;
            double newAttenuation = source.Attenuation * attenuationChange;
            double parallelR = Conductor.ToResistance( source.Conductance * newAttenuation );
            double seriesR = 1d / source.Conductance - parallelR;
            double newNominal = newAttenuation * CurrentSourceTestInfo.Get().NominalCurrent / source.Attenuation;
            source.Attenuation *= attenuationChange;

            // test short current
            item = $"short load current {attenuationChange}={nameof( attenuationChange )}";
            actualValue = source.LoadCurrent( Conductor.ShortConductance );
            expectedValue = newNominal / source.Attenuation;
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            TestInfo.VerboseMessage( message );

            // test open load current
            item = $"open load current {attenuationChange}={nameof( attenuationChange )}";
            actualValue = source.LoadCurrent( Conductor.OpenConductance );
            expectedValue = 0d;
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            TestInfo.VerboseMessage( message );

            // test load voltage
            item = $"load voltage {attenuationChange}={nameof( attenuationChange )}";
            actualValue = source.LoadVoltage( CurrentSourceTestInfo.Get().LoadResistance );
            expectedValue = CurrentSourceTestInfo.Get().LoadResistance * newNominal * Conductor.OutputCurrent( Resistor.ToConductance( parallelR ), Resistor.ToConductance( Resistor.ToSeries( CurrentSourceTestInfo.Get().LoadResistance, seriesR ) ) );
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            TestInfo.VerboseMessage( message );

            // test load current
            item = $"load current {attenuationChange}={nameof( attenuationChange )}";
            actualValue = source.LoadCurrent( Resistor.ToConductance( CurrentSourceTestInfo.Get().LoadResistance ) );
            expectedValue = newNominal * Conductor.OutputCurrent( Resistor.ToConductance( parallelR ), Resistor.ToConductance( Resistor.ToSeries( CurrentSourceTestInfo.Get().LoadResistance, seriesR ) ) );
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            TestInfo.VerboseMessage( message );
        }

        #endregion

    }
}
