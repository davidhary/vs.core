using System;

using isr.Core.Engineering;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.EngineeringTests
{

    /// <summary> A voltage source unit tests. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-09-18 </para>
    /// </remarks>
    [TestClass()]
    public class VoltageSourceTests
    {

        #region " TEST INPUTS "

        /// <summary> Gets or sets the voltage of the voltage source. </summary>
        /// <value> The voltage. </value>
        public static double Voltage { get; set; } = 5d;

        /// <summary> Gets or sets the resistance of the voltage source. </summary>
        /// <value> The resistance. </value>
        public static double Resistance { get; set; } = 1000d;

        /// <summary> The load resistance. </summary>
        /// <value> The load resistance. </value>
        public static double LoadResistance { get; set; } = 500d;

        /// <summary> Gets or sets the open resistance. </summary>
        /// <value> The short resistance. </value>
        public static double OpenResistance { get; set; } = Resistor.OpenResistance;

        /// <summary> Gets or sets the current of the current source. </summary>
        /// <value> The current. </value>
        public static double Current { get; set; } = 0.004d;

        /// <summary> Gets or sets the conductance or the current source. </summary>
        /// <value> The conductance. </value>
        public static double Conductance { get; set; } = 0.001d;

        /// <summary> The voltage change scale. </summary>
        /// <value> The voltage change scale. </value>
        public static double VoltageChangeScale { get; set; } = 1.5d;

        /// <summary> The resistance change scale. </summary>
        /// <value> The resistance change scale. </value>
        public static double ResistanceChangeScale { get; set; } = 0.8d;

        /// <summary> Gets or sets the load voltage epsilon. </summary>
        /// <value> The load voltage epsilon. </value>
        public static double LoadVoltageEpsilon { get; set; } = 0.000000001d;

        /// <summary> Gets or sets the voltage to current level epsilon. </summary>
        /// <value> The voltage to current level epsilon. </value>
        public static double VoltageToCurrentLevelEpsilon { get; set; } = 0.000000001d;

        /// <summary> Gets or sets the voltage to current conductance epsilon. </summary>
        /// <value> The voltage to current conductance epsilon. </value>
        public static double VoltageToCurrentConductanceEpsilon { get; set; } = 0.000000001d;

        /// <summary> Gets or sets the voltage to current voltage level epsilon. </summary>
        /// <value> The voltage to current voltage level epsilon. </value>
        public static double VoltageToCurrentVoltageLevelEpsilon { get; set; } = 0.000000001d;

        /// <summary> Gets or sets the voltage to current resistance epsilon. </summary>
        /// <value> The voltage to current resistance epsilon. </value>
        public static double VoltageToCurrentResistanceEpsilon { get; set; } = 0.000000001d;

        #endregion

        #region " CONSTUCTOR TESTS "

        /// <summary> (Unit Test Method) tests build voltage source. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void BuildVoltageSourceTest()
        {
            var source = new VoltageSource( Voltage, Resistance );

            // test source voltage
            double actualValue = source.Voltage;
            double expectedValue = Voltage;
            Assert.AreEqual( expectedValue, actualValue );

            // test source resistance
            actualValue = source.Resistance;
            expectedValue = Resistance;
            Assert.AreEqual( expectedValue, actualValue );
        }

        /// <summary> (Unit Test Method) converts this object to a current source test. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void ConvertToCurrentSourceTest()
        {

            // build voltage source
            var voltageSource = new VoltageSource( CurrentSourceTests.Voltage, CurrentSourceTests.Resistance );

            // Convert to current source
            var source = voltageSource.ToCurrentSource();

            // test source Current
            double actualValue = source.Current;
            double expectedValue = voltageSource.Voltage / voltageSource.Resistance;
            Assert.AreEqual( expectedValue, actualValue, VoltageToCurrentLevelEpsilon );

            // test source resistance
            actualValue = source.Conductance;
            expectedValue = 1d / voltageSource.Resistance;
            Assert.AreEqual( expectedValue, actualValue, VoltageToCurrentConductanceEpsilon );
            var finalVoltageSource = source.ToVoltageSource();
            Assert.AreEqual( voltageSource.Voltage, finalVoltageSource.Voltage, VoltageToCurrentVoltageLevelEpsilon );
            Assert.AreEqual( voltageSource.Resistance, finalVoltageSource.Resistance, VoltageToCurrentResistanceEpsilon );
        }

        /// <summary> (Unit Test Method) tests null current source exception. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        [ExpectedException( typeof( ArgumentNullException ), "Null current source inappropriately allowed" )]
        public void NullCurrentSourceExceptionTest()
        {

            // use a null current source
            CurrentSource currentSource = null;

            // convert to voltage source
            var source = new VoltageSource( Voltage, Resistance );

            // should throw the expected exception
            source.FromCurrentSource( currentSource );
        }

        #endregion

        #region " VOLTAGE TESTS "

        /// <summary> (Unit Test Method) tests voltage source. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void VoltageSourceTest()
        {
            var source = new VoltageSource( Voltage, Resistance );

            // test open voltage
            double actualValue = source.LoadVoltage( Resistor.OpenResistance );
            double expectedValue = Voltage;
            Assert.AreEqual( expectedValue, actualValue );

            // test short voltage
            actualValue = source.LoadVoltage( Resistor.ShortResistance );
            expectedValue = 0d;
            Assert.AreEqual( expectedValue, actualValue );

            // test load voltage
            actualValue = source.LoadVoltage( LoadResistance );
            expectedValue = Voltage * Resistor.OutputVoltage( Resistance, LoadResistance );
            Assert.AreEqual( expectedValue, actualValue, LoadVoltageEpsilon );

            // test load current
            actualValue = source.LoadCurrent( LoadResistance );
            expectedValue = Voltage / Resistor.Series( Resistance, LoadResistance );
            Assert.AreEqual( expectedValue, actualValue );

            // test open voltage
            actualValue = source.LoadVoltage( OpenResistance );
            expectedValue = source.Voltage;
            Assert.AreEqual( expectedValue, actualValue );
        }

        /// <summary> (Unit Test Method) tests voltage source change. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void VoltageSourceChangeTest()
        {
            var source = new VoltageSource( Voltage, Resistance );
            source.Voltage *= VoltageChangeScale;

            // test open voltage
            double actualValue = source.LoadVoltage( Resistor.OpenResistance );
            double expectedValue = VoltageChangeScale * Voltage;
            Assert.AreEqual( expectedValue, actualValue );

            // test short voltage
            actualValue = source.LoadVoltage( Resistor.ShortResistance );
            expectedValue = 0d;
            Assert.AreEqual( expectedValue, actualValue );

            // test load voltage
            actualValue = source.LoadVoltage( LoadResistance );
            expectedValue = VoltageChangeScale * Voltage * Resistor.OutputVoltage( Resistance, LoadResistance );
            Assert.AreEqual( expectedValue, actualValue );

            // test load current
            actualValue = source.LoadCurrent( LoadResistance );
            expectedValue = VoltageChangeScale * Voltage / Resistor.Series( Resistance, LoadResistance );
            Assert.AreEqual( expectedValue, actualValue );
            source.Voltage /= VoltageChangeScale;
            source.Resistance *= ResistanceChangeScale;

            // test open voltage
            actualValue = source.LoadVoltage( OpenResistance );
            expectedValue = source.Voltage;
            Assert.AreEqual( expectedValue, actualValue );

            // test short voltage
            actualValue = source.LoadVoltage( 0d );
            expectedValue = 0d;
            Assert.AreEqual( expectedValue, actualValue );

            // test load voltage
            actualValue = source.LoadVoltage( LoadResistance );
            expectedValue = Voltage * Resistor.OutputVoltage( ResistanceChangeScale * Resistance, LoadResistance );
            Assert.AreEqual( expectedValue, actualValue );

            // test load current
            actualValue = source.LoadCurrent( LoadResistance );
            expectedValue = Voltage / Resistor.Series( ResistanceChangeScale * Resistance, LoadResistance );
            Assert.AreEqual( expectedValue, actualValue );
        }

        #endregion

    }
}
