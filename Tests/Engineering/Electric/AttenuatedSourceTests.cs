using System;
using System.Collections.Generic;

using isr.Core.Engineering;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.EngineeringTests
{

    /// <summary> Unit tests for testing attenuated voltage and current sources. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-09-18 </para>
    /// </remarks>
    [TestClass()]
    public class AttenuatedSourceTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( EngineeringTests.AttenuatedSourceTests.TestInfo.TimeZoneOffset ) < expectedUpperLimit,
                            $"{ nameof( EngineeringTests.TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            Assert.IsTrue( VoltageSourceTestInfo.Get().Exists, $"{typeof( VoltageSourceTestInfo )} settings should exist" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " CONVERSION TESTS "

        /// <summary> (Unit Test Method) tests convert attenuated current source. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void ConvertAttenuatedCurrentSourceTest()
        {
            var attenuatedCurrentSource = new AttenuatedCurrentSource( ( decimal ) CurrentSourceTestInfo.Get().NominalCurrent, CurrentSourceTestInfo.Get().SeriesResistance, CurrentSourceTestInfo.Get().ParallelConductance );

            // create converted voltage sources using alternative conversions
            var sources = new List<AttenuatedVoltageSource>();
            var descriptions = new List<string>();
            sources.Add( attenuatedCurrentSource.ToAttenuatedVoltageSource( CurrentSourceTestInfo.Get().NominalVoltage ) );
            descriptions.Add( "Voltage source from attenuated current source" );
            sources.Add( new VoltageSource( attenuatedCurrentSource ).ToAttenuatedVoltageSource( ( decimal ) CurrentSourceTestInfo.Get().NominalVoltage ) );
            descriptions.Add( "Voltage source from voltage source" );
            sources.Add( attenuatedCurrentSource.ToVoltageSource().ToAttenuatedVoltageSource( ( decimal ) CurrentSourceTestInfo.Get().NominalVoltage ) );
            descriptions.Add( "Voltage source from current source" );
            double doubleEpsilon = 0.0000000001d;
            AttenuatedVoltageSource source;
            for ( int i = 0, loopTo = sources.Count - 1; i <= loopTo; i++ )
            {
                source = sources[i];
                string description = descriptions[i];
                string item = $"nominal voltage";
                double actualValue = source.NominalVoltage;
                double expectedValue = CurrentSourceTestInfo.Get().NominalVoltage;
                string outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                string message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                TestInfo.VerboseMessage( message );

                // the two sources have identical output resistance
                item = $"equivalent resistance";
                actualValue = source.Resistance;
                expectedValue = Conductor.ToResistance( attenuatedCurrentSource.Conductance );
                outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                TestInfo.VerboseMessage( message );

                // the sources have the same open load voltage
                item = $"open load voltage";
                actualValue = source.LoadVoltage( Resistor.OpenResistance );
                expectedValue = attenuatedCurrentSource.LoadVoltage( Resistor.OpenResistance );
                outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                TestInfo.VerboseMessage( message );

                // the sources have the same short load voltage
                item = $"short load voltage";
                actualValue = source.LoadVoltage( Resistor.ShortResistance );
                expectedValue = attenuatedCurrentSource.LoadVoltage( Resistor.ShortResistance );
                outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                TestInfo.VerboseMessage( message );

                // the sources have the same load voltage
                item = $"load voltage";
                actualValue = source.LoadVoltage( CurrentSourceTestInfo.Get().LoadResistance );
                expectedValue = attenuatedCurrentSource.LoadVoltage( CurrentSourceTestInfo.Get().LoadResistance );
                outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                TestInfo.VerboseMessage( message );
            }
        }

        /// <summary> (Unit Test Method) tests convert attenuated voltage source. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void ConvertAttenuatedVoltageSourceTest()
        {
            double mimnimumImpedance = CurrentSourceTestInfo.Get().NominalVoltage / CurrentSourceTestInfo.Get().NominalCurrent;
            double impedance = AttenuatedVoltageSource.ToEquivalentResistance( CurrentSourceTestInfo.Get().SeriesResistance, CurrentSourceTestInfo.Get().ParallelConductance );
            double minImpedanceFactor = 2d * mimnimumImpedance / impedance;
            // make sure that the voltage source impedance is high enough for conversion from 5v to 4 ma.
            var attenuatedVoltageSource = new AttenuatedVoltageSource( ( decimal ) CurrentSourceTestInfo.Get().NominalVoltage, minImpedanceFactor * CurrentSourceTestInfo.Get().SeriesResistance, minImpedanceFactor * CurrentSourceTestInfo.Get().ParallelConductance );

            // create converted current sources using alternative conversions
            var sources = new List<AttenuatedCurrentSource>();
            var descriptions = new List<string>();
            sources.Add( attenuatedVoltageSource.ToAttenuatedCurrentSource( CurrentSourceTestInfo.Get().NominalCurrent ) );
            descriptions.Add( "Current source from attenuated voltage source" );
            sources.Add( new CurrentSource( attenuatedVoltageSource ).ToAttenuatedCurrentSource( ( decimal ) CurrentSourceTestInfo.Get().NominalCurrent ) );
            descriptions.Add( "Current source from current source" );
            sources.Add( attenuatedVoltageSource.ToCurrentSource().ToAttenuatedCurrentSource( ( decimal ) CurrentSourceTestInfo.Get().NominalCurrent ) );
            descriptions.Add( "Current source from voltage source" );
            double doubleEpsilon = 0.0000000001d;
            AttenuatedCurrentSource source;
            for ( int i = 0, loopTo = sources.Count - 1; i <= loopTo; i++ )
            {
                source = sources[i];
                string description = descriptions[i];
                string item = $"nominal current";
                double actualValue = source.NominalCurrent;
                double expectedValue = CurrentSourceTestInfo.Get().NominalCurrent;
                string outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                string message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                TestInfo.VerboseMessage( message );

                // the two sources have identical output resistance
                item = $"equivalent resistance";
                actualValue = Conductor.ToResistance( source.Conductance );
                expectedValue = attenuatedVoltageSource.Resistance;
                outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                TestInfo.VerboseMessage( message );

                // the sources have the same short load current
                item = $"short load current";
                actualValue = source.LoadCurrent( Conductor.ShortConductance );
                expectedValue = attenuatedVoltageSource.LoadCurrent( Resistor.ShortResistance );
                outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                TestInfo.VerboseMessage( message );

                // the sources have the same open load current
                item = $"open load current";
                actualValue = source.LoadCurrent( Conductor.OpenConductance );
                expectedValue = attenuatedVoltageSource.LoadCurrent( Resistor.OpenResistance );
                outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                TestInfo.VerboseMessage( message );

                // the sources have the same load current
                item = $"load current";
                actualValue = source.LoadCurrent( Resistor.ToConductance( CurrentSourceTestInfo.Get().LoadResistance ) );
                expectedValue = attenuatedVoltageSource.LoadCurrent( CurrentSourceTestInfo.Get().LoadResistance );
                outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                TestInfo.VerboseMessage( message );
            }
        }

        #endregion

        #region " EXCEPTION CONVERSION TESTS "

        /// <summary> (Unit Test Method) tests low nominal voltage source exception. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        [ExpectedException( typeof( InvalidOperationException ), "Conversion from an attenuated current source to an attenuated voltage source with low nominal allowed" )]
        public void LowNominalVoltageSourceExceptionTest()
        {
            var currentSource = new AttenuatedCurrentSource( ( decimal ) CurrentSourceTestInfo.Get().NominalCurrent, CurrentSourceTestInfo.Get().SeriesResistance, CurrentSourceTestInfo.Get().ParallelConductance );
            _ = currentSource.ToAttenuatedVoltageSource( 0.99d * currentSource.LoadVoltage( Resistor.OpenResistance ) );
        }

        /// <summary> (Unit Test Method) tests low nominal current source exception. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        [ExpectedException( typeof( InvalidOperationException ), "Conversion from an attenuated voltage source to an attenuated current source with low nominal current allowed" )]
        public void LowNominalCurrentSourceExceptionTest()
        {
            var voltageSource = new AttenuatedVoltageSource( ( decimal ) CurrentSourceTestInfo.Get().NominalVoltage, CurrentSourceTestInfo.Get().SeriesResistance, CurrentSourceTestInfo.Get().ParallelResistance );
            _ = voltageSource.ToAttenuatedCurrentSource( 0.99d * voltageSource.LoadCurrent( Resistor.ShortResistance ) );
        }

        /// <summary> (Unit Test Method) tests null attenuated current source exception. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        [ExpectedException( typeof( ArgumentNullException ), "Null Attenuated Current Source inappropriately allowed" )]
        public void NullAttenuatedCurrentSourceExceptionTest()
        {
            AttenuatedCurrentSource currentSource = null;
            var source = new AttenuatedVoltageSource( ( decimal ) CurrentSourceTestInfo.Get().NominalVoltage, CurrentSourceTestInfo.Get().SeriesResistance, CurrentSourceTestInfo.Get().ParallelConductance );
            source.FromAttenuatedCurrentSource( currentSource, ( decimal ) CurrentSourceTestInfo.Get().NominalVoltage );
        }

        /// <summary> (Unit Test Method) tests null attenuated voltage source exception. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        [ExpectedException( typeof( ArgumentNullException ), "Null Attenuated Voltage Source inappropriately allowed" )]
        public void NullAttenuatedVoltageSourceExceptionTest()
        {
            AttenuatedVoltageSource VoltageSource = null;
            var source = new AttenuatedCurrentSource( ( decimal ) CurrentSourceTestInfo.Get().NominalCurrent, CurrentSourceTestInfo.Get().SeriesResistance, CurrentSourceTestInfo.Get().ParallelConductance );
            source.FromAttenuatedVoltageSource( VoltageSource, ( double ) ( decimal ) CurrentSourceTestInfo.Get().NominalCurrent );
        }


        #endregion

    }
}
