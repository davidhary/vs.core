﻿using isr.Core.Engineering;

namespace isr.Core.EngineeringTests
{

    /// <summary> Provides bridges for testing. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    internal sealed class TestBridges
    {

        /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        private TestBridges() : base()
        {
        }

        /// <summary> Gets the element epsilon. </summary>
        /// <value> The element epsilon. </value>
        public static double ElementEpsilon { get; set; } = 0.000001d;

        /// <summary> Gets the output voltage epsilon. </summary>
        /// <value> The output voltage epsilon. </value>
        public static double OutputVoltageEpsilon { get; set; } = 0.0001d;

        /// <summary> Gets the full scale voltage epsilon. </summary>
        /// <value> The full scale voltage epsilon. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        public static double FullScaleVoltageEpsilon { get; set; } = 0.0001d;

        /// <summary> Gets source resistance tolerance. </summary>
        /// <value> The source resistance tolerance. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        public static double SourceResistanceTolerance { get; set; } = 0.001d;

        /// <summary> The low temporary zero pressure bridge. </summary>
        private static SampleBridge _LowTempZeroPressureBridge;

        /// <summary> Gets the low temporary zero pressure bridge. </summary>
        /// <value> The low temporary zero pressure bridge. </value>
        public static SampleBridge LowTempZeroPressureBridge
        {
            get {
                // 636 this is calculated based on 5v, 0.004 v and the above resistances
                if ( _LowTempZeroPressureBridge is null )
                {
                    _LowTempZeroPressureBridge = new SampleBridge( new double[] { 369.59d, 363.04d, 357.69d, 377.06d }, WheatstoneLayout.Counterclockwise ) {
                        SupplyVoltage = 5.02058d,
                        SupplyVoltageDrop = 3.67312d,
                        OutputVoltage = 0.011728d
                    };
                    _LowTempZeroPressureBridge.BalanceBridge = new BalanceBridge( _LowTempZeroPressureBridge.Bridge, new double[] { Conductor.OpenConductance, 10.3d, Resistor.ShortResistance, 1d / 52357d }, BalanceLayout.TopShuntBottomSeries );
                    _LowTempZeroPressureBridge.EquivalentResistance = 782d;
                    _LowTempZeroPressureBridge.RelativeOffsetEpsilon = 0.005d;
                    _LowTempZeroPressureBridge.NominalVoltage = 5d;
                    _LowTempZeroPressureBridge.VoltageSourceSeriesResistance = 1538d;
                    _LowTempZeroPressureBridge.VoltageSourceParallelResistance = 1593d;
                    _LowTempZeroPressureBridge.NominalFullScaleVoltage = 0.02d;
                    _LowTempZeroPressureBridge.NominalCurrent = 0.004d;
                    _LowTempZeroPressureBridge.CurrentSourceSeriesResistance = 636d;
                    _LowTempZeroPressureBridge.CurrentSourceParallelResistance = 150d;
                }

                return _LowTempZeroPressureBridge;
            }
        }

        /// <summary> The low temporary full scale pressure bridge. </summary>
        private static SampleBridge _LowTempFullScalePressureBridge;

        /// <summary> Gets the low temporary full scale pressure bridge. </summary>
        /// <value> The low temporary full scale pressure bridge. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        public static SampleBridge LowTempFullScalePressureBridge
        {
            get {
                // 636 this is calculated based on 5v, 0.004 v and the above resistances
                if ( _LowTempFullScalePressureBridge is null )
                {
                    _LowTempFullScalePressureBridge = new SampleBridge( new double[] { 381.78d, 356.88d, 369.62d, 370.89d }, WheatstoneLayout.Counterclockwise ) {
                        SupplyVoltage = 5.02075d,
                        SupplyVoltageDrop = 3.66533d,
                        OutputVoltage = 0.02001d
                    };
                    _LowTempFullScalePressureBridge.BalanceBridge = new BalanceBridge( _LowTempFullScalePressureBridge.Bridge, new double[] { Conductor.OpenConductance, 10.3d, Resistor.ShortResistance, 1d / 52357d }, BalanceLayout.TopShuntBottomSeries );
                    _LowTempFullScalePressureBridge.EquivalentResistance = 782d;
                    _LowTempFullScalePressureBridge.RelativeOffsetEpsilon = 0.005d;
                    _LowTempFullScalePressureBridge.NominalVoltage = 5d;
                    _LowTempFullScalePressureBridge.VoltageSourceSeriesResistance = 1538d;
                    _LowTempFullScalePressureBridge.VoltageSourceParallelResistance = 1593d;
                    _LowTempFullScalePressureBridge.NominalFullScaleVoltage = 0.02d;
                    _LowTempFullScalePressureBridge.NominalCurrent = 0.004d;
                    _LowTempFullScalePressureBridge.CurrentSourceSeriesResistance = 636d;
                    _LowTempFullScalePressureBridge.CurrentSourceParallelResistance = 150d;
                }

                return _LowTempFullScalePressureBridge;
            }
        }

        /// <summary> The high temporary zero pressure bridge. </summary>
        private static SampleBridge _HighTempZeroPressureBridge;

        /// <summary> Gets the high temporary zero pressure bridge. </summary>
        /// <value> The high temporary zero pressure bridge. </value>
        public static SampleBridge HighTempZeroPressureBridge
        {
            get {
                if ( _HighTempZeroPressureBridge is null )
                {
                    _HighTempZeroPressureBridge = new SampleBridge( new double[] { 563.79d, 563.19d, 556.32d, 573.16d }, WheatstoneLayout.Counterclockwise ) {
                        SupplyVoltage = 5.02155d,
                        SupplyVoltageDrop = 3.21047d,
                        OutputVoltage = 0.013019d // possibly a transcription error: 0.011728
                    };
                    _HighTempZeroPressureBridge.BalanceBridge = new BalanceBridge( _HighTempZeroPressureBridge.Bridge, new double[] { Conductor.OpenConductance, 10.3d, Resistor.ShortResistance, 1d / 52357d }, BalanceLayout.TopShuntBottomSeries );
                    _HighTempZeroPressureBridge.EquivalentResistance = 782d;
                    _HighTempZeroPressureBridge.RelativeOffsetEpsilon = 0.006d;
                    _HighTempZeroPressureBridge.NominalVoltage = 5d;
                    _HighTempZeroPressureBridge.VoltageSourceSeriesResistance = 1538d;
                    _HighTempZeroPressureBridge.VoltageSourceParallelResistance = 1593d;
                    _HighTempZeroPressureBridge.NominalFullScaleVoltage = 0.02d;
                    _HighTempZeroPressureBridge.NominalCurrent = 0.004d;
                    // 636 this is calculated based on 5v, 0.004 v and the above resistances
                    _HighTempZeroPressureBridge.CurrentSourceSeriesResistance = 636d;
                    _HighTempZeroPressureBridge.CurrentSourceParallelResistance = 150d;
                }

                return _HighTempZeroPressureBridge;
            }
        }

        /// <summary> The high temporary full scale pressure bridge. </summary>
        private static SampleBridge _HighTempFullScalePressureBridge;

        /// <summary> Gets the high temporary full scale pressure bridge. </summary>
        /// <value> The high temporary full scale pressure bridge. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        public static SampleBridge HighTempFullScalePressureBridge
        {
            get {
                if ( _HighTempFullScalePressureBridge is null )
                {
                    _HighTempFullScalePressureBridge = new SampleBridge( new double[] { 577.21d, 555.43d, 569.81d, 565.23d }, WheatstoneLayout.Counterclockwise ) {
                        SupplyVoltage = 5.02152d,
                        SupplyVoltageDrop = 3.20471d,
                        OutputVoltage = 0.02001d
                    };
                    _HighTempFullScalePressureBridge.BalanceBridge = new BalanceBridge( _HighTempFullScalePressureBridge.Bridge, new double[] { Conductor.OpenConductance, 10.3d, Resistor.ShortResistance, 1d / 52357d }, BalanceLayout.TopShuntBottomSeries );
                    _HighTempFullScalePressureBridge.EquivalentResistance = 782d;
                    _HighTempFullScalePressureBridge.RelativeOffsetEpsilon = 0.005d;
                    _HighTempFullScalePressureBridge.NominalVoltage = 5d;
                    _HighTempFullScalePressureBridge.VoltageSourceSeriesResistance = 1538d;
                    _HighTempFullScalePressureBridge.VoltageSourceParallelResistance = 1593d;
                    _HighTempFullScalePressureBridge.NominalFullScaleVoltage = 0.02d;
                    _HighTempFullScalePressureBridge.NominalCurrent = 0.004d;
                    // 636 this is calculated based on 5v, 0.004 v and the above resistances
                    _HighTempFullScalePressureBridge.CurrentSourceSeriesResistance = 636d;
                    _HighTempFullScalePressureBridge.CurrentSourceParallelResistance = 150d;
                }

                return _HighTempFullScalePressureBridge;
            }
        }
    }
}