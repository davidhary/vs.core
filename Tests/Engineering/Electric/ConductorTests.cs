using System;

using isr.Core.Engineering;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.EngineeringTests
{

    /// <summary> A conductance unit tests. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-09-16 </para>
    /// </remarks>
    [TestClass()]
    public class ConductorTests
    {

        #region " TEST INPUTS "

        /// <summary> Gets or sets the left conductance. </summary>
        /// <value> The left conductance. </value>
        public static double LeftConductance { get; set; } = 0.001d;

        /// <summary> Gets or sets the right conductance. </summary>
        /// <value> The right conductance. </value>
        public static double RightConductance { get; set; } = 0.002d;

        /// <summary> Gets or sets the load conductance. </summary>
        /// <value> The load conductance. </value>
        public static double LoadConductance { get; set; } = 0.004d;

        /// <summary> Gets or sets the negative conductance. </summary>
        /// <value> The negative conductance. </value>
        public static double NegativeConductance { get; set; } = -0.001d;

        #endregion

        #region " PARALLEL TESTS "

        /// <summary> (Unit Test Method) tests parallel. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void ParallelTest()
        {
            double actualValue = Conductor.ToParallel( LeftConductance, RightConductance );
            double expectedValue = LeftConductance + RightConductance;
            Assert.AreEqual( expectedValue, actualValue );
        }

        /// <summary> (Unit Test Method) tests parallel short. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void ParallelShortTest()
        {
            double actualValue = Conductor.ToParallel( Conductor.ShortConductance, RightConductance );
            double expectedValue = Conductor.ShortConductance;
            Assert.AreEqual( expectedValue, actualValue );
            actualValue = Conductor.ToParallel( LeftConductance, Conductor.ShortConductance );
            expectedValue = Conductor.ShortConductance;
            Assert.AreEqual( expectedValue, actualValue );
            actualValue = Conductor.ToParallel( Conductor.ShortConductance, Conductor.ShortConductance );
            expectedValue = Conductor.ShortConductance;
            Assert.AreEqual( expectedValue, actualValue );
        }

        /// <summary> (Unit Test Method) tests parallel open. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void ParallelOpenTest()
        {
            double actualValue = Conductor.ToParallel( Conductor.OpenConductance, RightConductance );
            double expectedValue = RightConductance;
            Assert.AreEqual( expectedValue, actualValue );
            actualValue = Conductor.ToParallel( LeftConductance, Conductor.OpenConductance );
            expectedValue = LeftConductance;
            Assert.AreEqual( expectedValue, actualValue );
            actualValue = Conductor.ToParallel( Conductor.OpenConductance, Conductor.OpenConductance );
            expectedValue = Conductor.OpenConductance;
            Assert.AreEqual( expectedValue, actualValue );
        }

        /// <summary> (Unit Test Method) tests parallel negative left conductance exception. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        [ExpectedException( typeof( ArgumentOutOfRangeException ), "Negative conductance inappropriately allowed" )]
        public void ParallelNegativeLeftConductanceExceptionTest()
        {
            _ = Conductor.ToParallel( NegativeConductance, RightConductance );
        }

        /// <summary> (Unit Test Method) tests parallel negative right conductance exception. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        [ExpectedException( typeof( ArgumentOutOfRangeException ), "Negative conductance inappropriately allowed" )]
        public void ParallelNegativeRightConductanceExceptionTest()
        {
            _ = Conductor.ToParallel( LeftConductance, NegativeConductance );
        }

        #endregion

        #region " SERIES TESTS "

        /// <summary> (Unit Test Method) tests series. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void SeriesTest()
        {
            double actualValue = Conductor.ToSeries( LeftConductance, RightConductance );
            double expectedValue = 1d / (1d / LeftConductance + 1d / RightConductance);
            Assert.AreEqual( expectedValue, actualValue );
        }

        /// <summary> (Unit Test Method) tests series short. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void SeriesShortTest()
        {
            double actualValue = Conductor.ToSeries( Conductor.ShortConductance, RightConductance );
            double expectedValue = RightConductance;
            Assert.AreEqual( expectedValue, actualValue );
            actualValue = Conductor.ToSeries( LeftConductance, Conductor.ShortConductance );
            expectedValue = LeftConductance;
            Assert.AreEqual( expectedValue, actualValue );
            actualValue = Conductor.ToSeries( Conductor.ShortConductance, Conductor.ShortConductance );
            expectedValue = Conductor.ShortConductance;
            Assert.AreEqual( expectedValue, actualValue );
        }

        /// <summary> (Unit Test Method) tests series open. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void SeriesOpenTest()
        {
            double actualValue = Conductor.ToSeries( Conductor.OpenConductance, RightConductance );
            double expectedValue = Conductor.OpenConductance;
            Assert.AreEqual( expectedValue, actualValue );
            actualValue = Conductor.ToSeries( LeftConductance, Conductor.OpenConductance );
            expectedValue = Conductor.OpenConductance;
            Assert.AreEqual( expectedValue, actualValue );
            actualValue = Conductor.ToSeries( Conductor.OpenConductance, Conductor.OpenConductance );
            expectedValue = Conductor.OpenConductance;
            Assert.AreEqual( expectedValue, actualValue );
        }

        /// <summary> (Unit Test Method) tests series negative left conductance exception. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        [ExpectedException( typeof( ArgumentOutOfRangeException ), "Negative conductance inappropriately allowed" )]
        public void SeriesNegativeLeftConductanceExceptionTest()
        {
            _ = Conductor.ToSeries( NegativeConductance, RightConductance );
        }

        /// <summary> (Unit Test Method) tests series negative right conductance exception. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        [ExpectedException( typeof( ArgumentOutOfRangeException ), "Negative conductance inappropriately allowed" )]
        public void SeriesNegativeRightConductanceExceptionTest()
        {
            _ = Conductor.ToSeries( LeftConductance, NegativeConductance );
        }

        #endregion

        #region " OUTPUT CURRENT TESTS "

        /// <summary> (Unit Test Method) tests output current. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void OutputCurrentTest()
        {
            double actualValue = Conductor.ToOutputCurrent( LeftConductance, RightConductance );
            double expectedValue = RightConductance / Conductor.Parallel( LeftConductance, RightConductance );
            Assert.AreEqual( expectedValue, actualValue );
        }

        /// <summary> (Unit Test Method) tests output current short. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void OutputCurrentShortTest()
        {
            double actualValue = Conductor.ToOutputCurrent( Conductor.ShortConductance, RightConductance );
            double expectedValue = 0d;
            Assert.AreEqual( expectedValue, actualValue );
            actualValue = Conductor.ToOutputCurrent( LeftConductance, Conductor.ShortConductance );
            expectedValue = 1d;
            Assert.AreEqual( expectedValue, actualValue );
        }

        /// <summary> (Unit Test Method) tests output current short exception. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        [ExpectedException( typeof( InvalidOperationException ), "Two shorts inappropriately allowed" )]
        public void OutputCurrentShortsExceptionTest()
        {
            _ = Conductor.ToOutputCurrent( Conductor.ShortConductance, Conductor.ShortConductance );
        }

        /// <summary> (Unit Test Method) tests output current open. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void OutputCurrentOpenTest()
        {
            double actualValue = Conductor.ToOutputCurrent( Conductor.OpenConductance, RightConductance );
            double expectedValue = 1d;
            Assert.AreEqual( expectedValue, actualValue );
            actualValue = Conductor.ToOutputCurrent( LeftConductance, Conductor.OpenConductance );
            expectedValue = 0d;
            Assert.AreEqual( expectedValue, actualValue );
        }

        /// <summary> (Unit Test Method) tests output current open exception. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        [ExpectedException( typeof( InvalidOperationException ), "Two opens inappropriately allowed" )]
        public void OutputCurrentOpensExceptionTest()
        {
            _ = Conductor.ToOutputCurrent( Conductor.OpenConductance, Conductor.OpenConductance );
        }

        /// <summary>
        /// (Unit Test Method) tests output current negative left conductance exception.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        [ExpectedException( typeof( ArgumentOutOfRangeException ), "Negative conductance inappropriately allowed" )]
        public void OutputCurrentNegativeLeftConductanceExceptionTest()
        {
            _ = Conductor.ToOutputCurrent( NegativeConductance, RightConductance );
        }

        /// <summary>
        /// (Unit Test Method) tests output current negative right conductance exception.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        [ExpectedException( typeof( ArgumentOutOfRangeException ), "Negative conductance inappropriately allowed" )]
        public void OutputCurrentNegativeRightConductanceExceptionTest()
        {
            _ = Conductor.ToOutputCurrent( LeftConductance, NegativeConductance );
        }

        #endregion

        #region " OUTPUT VOLTAGE TESTS "

        /// <summary> (Unit Test Method) tests output voltage. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void OutputVoltageTest()
        {
            double actualValue = Conductor.ToOutputVoltage( LeftConductance, RightConductance );
            double expectedValue = 1d / RightConductance * Conductor.ToSeries( LeftConductance, RightConductance );
            Assert.AreEqual( expectedValue, actualValue );
        }

        /// <summary> (Unit Test Method) tests output voltage short. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void OutputVoltageShortTest()
        {
            double actualValue = Conductor.ToOutputVoltage( Conductor.ShortConductance, RightConductance );
            double expectedValue = 1d;
            Assert.AreEqual( expectedValue, actualValue );
            actualValue = Conductor.ToOutputVoltage( LeftConductance, Conductor.ShortConductance );
            expectedValue = 0d;
            Assert.AreEqual( expectedValue, actualValue );
        }

        /// <summary> (Unit Test Method) tests output voltage shorts exception. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        [ExpectedException( typeof( InvalidOperationException ), "Two shorts inappropriately allowed" )]
        public void OutputVoltageShortsExceptionTest()
        {
            _ = Conductor.ToOutputVoltage( Conductor.ShortConductance, Conductor.ShortConductance );
        }

        /// <summary> (Unit Test Method) tests output voltage open. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void OutputVoltageOpenTest()
        {
            double actualValue = Conductor.ToOutputVoltage( Conductor.OpenConductance, RightConductance );
            double expectedValue = 0d;
            Assert.AreEqual( expectedValue, actualValue );
            actualValue = Conductor.ToOutputVoltage( LeftConductance, Conductor.OpenConductance );
            expectedValue = 1d;
            Assert.AreEqual( expectedValue, actualValue );
        }

        /// <summary> (Unit Test Method) tests output voltage opens exception. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        [ExpectedException( typeof( InvalidOperationException ), "Two opens inappropriately allowed" )]
        public void OutputVoltageOpensExceptionTest()
        {
            _ = Conductor.ToOutputVoltage( Conductor.OpenConductance, Conductor.OpenConductance );
        }

        /// <summary>
        /// (Unit Test Method) tests output voltage negative left conductance exception.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        [ExpectedException( typeof( ArgumentOutOfRangeException ), "Negative conductance inappropriately allowed" )]
        public void OutputVoltageNegativeLeftConductanceExceptionTest()
        {
            _ = Conductor.ToOutputVoltage( NegativeConductance, RightConductance );
        }

        /// <summary>
        /// (Unit Test Method) tests output voltage negative right conductance exception.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        [ExpectedException( typeof( ArgumentOutOfRangeException ), "Negative conductance inappropriately allowed" )]
        public void OutputVoltageNegativeRightConductanceExceptionTest()
        {
            _ = Conductor.ToOutputVoltage( LeftConductance, NegativeConductance );
        }

        #endregion

    }
}
