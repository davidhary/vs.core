using System;
using System.Diagnostics;

using isr.Core.TimeSpanExtensions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.EngineeringTests.ExtensionsTests
{

    /// <summary> Time Span extensions tests. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-03-14 </para>
    /// </remarks>
    [TestClass()]
    public class TimeSpanExtensionsTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                _TestSite = new TestSite();
                _TestSite.AddTraceMessagesQueue( _TestSite.TraceMessagesQueueListener );
                _TestSite.AddTraceMessagesQueue( Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                _TestSite.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( _TestSite is object )
            {
                _TestSite.Dispose();
                _TestSite = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( EngineeringTests.ExtensionsTests.TimeSpanExtensionsTests.TestInfo.TimeZoneOffset ) < expectedUpperLimit,
                                $"{nameof( EngineeringTests.TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> The test site. </summary>
        private static TestSite _TestSite;

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo
        {
            get {
                if ( _TestSite is null )
                {
                    _TestSite = new TestSite();
                    _TestSite.AddTraceMessagesQueue( _TestSite.TraceMessagesQueueListener );
                    _TestSite.AddTraceMessagesQueue( Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                }

                return _TestSite;
            }
        }

        #endregion

        #region " TIMESPAN DELAY "

        /// <summary> Values that represent task options. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        private enum TaskOptions
        {
            /// <summary> An enum constant representing the no Operation option. </summary>
            NoOp,
            /// <summary> An enum constant representing the spin wait option. </summary>
            SpinWait,
            /// <summary> An enum constant representing the high Resource option. </summary>
            HighRes,
            /// <summary> An enum constant representing the thread task option. </summary>
            ThreadTask,
            /// <summary> An enum constant representing the task delay option. </summary>
            TaskDelay
        }

        /// <summary> Estimates task time. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="taskOption"> The task option. </param>
        /// <param name="spinCount">  Number of spins. </param>
        /// <param name="delay">      The delay. </param>
        /// <param name="count">      Number of. </param>
        /// <returns> A (Mean As Double, Sigma As Double) </returns>
        private static Engineering.SampleStatistics EstimateTaskTime( TaskOptions taskOption, int spinCount, TimeSpan delay, int count )
        {
            var stat = new Engineering.SampleStatistics();
            var sw = Stopwatch.StartNew();
            System.Threading.Tasks.Task task;
            for ( int i = 1, loopTo = count; i <= loopTo; i++ )
            {
                ApplianceBase.DoEvents();
                switch ( taskOption )
                {
                    case TaskOptions.NoOp:
                        {
                            sw.Restart();
                            task = TimeSpanExtensionMethods.NoOpTask();
                            task.Start();
                            task.Wait();
                            sw.Stop();
                            break;
                        }

                    case TaskOptions.SpinWait:
                        {
                            sw.Restart();
                            task = TimeSpanExtensionMethods.SpinWaitTask( spinCount );
                            task.Start();
                            task.Wait();
                            sw.Stop();
                            break;
                        }

                    case TaskOptions.TaskDelay:
                        {
                            sw.Restart();
                            _ = System.Threading.Tasks.Task.Delay( delay );
                            sw.Stop();
                            break;
                        }

                    case TaskOptions.ThreadTask:
                        {
                            sw.Restart();
                            task = delay.ThreadClockTask();
                            task.Start();
                            task.Wait();
                            sw.Stop();
                            break;
                        }

                    case TaskOptions.HighRes:
                        {
                            sw.Restart();
                            task = delay.HighResolutionClockTask();
                            task.Start();
                            task.Wait();
                            sw.Stop();
                            break;
                        }
                }

                stat.AddValue( sw.Elapsed.ToMilliseconds() );
            }

            stat.Evaluate();
            return stat;
        }

        /// <summary> Reports. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="name"> The name. </param>
        /// <param name="stat"> The stat. </param>
        /// <returns> A String. </returns>
        private static string Report( string name, Engineering.SampleStatistics stat )
        {
            return $"{name} {stat.Mean:0.0##}±{stat.Sigma:0.0##}ms {stat.Count}:[{stat.Minimum:0.0##},{stat.Maximum:0.0##}]";
        }

        /// <summary> Tests timespan system clock delay statistics. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="taskOption">     The task option. </param>
        /// <param name="removeOutliers"> True to remove outliers. </param>
        /// <param name="count">          Number of. </param>
        private static void TestTimespanSystemClockDelayStatistics( TaskOptions taskOption, bool removeOutliers, int count )
        {
            int spinCount = 1;
            var delay = TimeSpan.Zero;
            string message;
            double expectedMean;
            double actualMean;
            Engineering.SampleStatistics stat;
            Engineering.SampleQuartiles qrt;
            if ( taskOption == TaskOptions.NoOp )
            {
                stat = EstimateTaskTime( taskOption, spinCount, delay, count );
                if ( removeOutliers )
                {
                    expectedMean = 0.08d; // 0.021±0.017ms 88[0.004,0.069]
                    qrt = new Engineering.SampleQuartiles( stat );
                    qrt.Evaluate();
                    actualMean = qrt.FilteredSample.Mean;
                    message = Report( $"{taskOption}.Q".ToString(), qrt.FilteredSample );
                }
                else
                {
                    expectedMean = 0.5d; // 0.029±0.121ms 100:[0.004,1.211]
                    actualMean = stat.Mean;
                    message = Report( $"{taskOption}".ToString(), stat );
                }

                TestInfo.VerboseMessage( message );
                Assert.IsTrue( actualMean < expectedMean, $"Actual mean {actualMean:0.000}ms should be lower then the expected mean {expectedMean:0.000}ms: {message}" );
            }

            if ( taskOption == TaskOptions.SpinWait )
            {
                spinCount = 1;
                delay = TimeSpan.Zero;
                stat = EstimateTaskTime( taskOption, spinCount, delay, count );
                if ( removeOutliers )
                {
                    expectedMean = 0.09d; // SpinWait(1).Q 0.024±0.02ms 99: [0.004,0.093]
                    qrt = new Engineering.SampleQuartiles( stat );
                    qrt.Evaluate();
                    actualMean = qrt.FilteredSample.Mean;
                    message = Report( $"{taskOption}({spinCount}).Q", qrt.FilteredSample );
                }
                else
                {
                    expectedMean = 0.5d; // SpinWait(1) 0.039±0.142ms 100:[0.004,1.319]
                    actualMean = stat.Mean;
                    message = Report( $"{taskOption}({spinCount})", stat );
                }

                TestInfo.VerboseMessage( message );
                Assert.IsTrue( actualMean < expectedMean, $"Actual mean {actualMean:0.000}ms should be lower then the expected mean {expectedMean:0.000}ms: {message}" );
                spinCount = 9;
                delay = TimeSpan.Zero;
                stat = EstimateTaskTime( taskOption, spinCount, delay, count );
                if ( removeOutliers )
                {
                    expectedMean = 0.09d; // SpinWait(9).Q 0.025±0.018ms 98: [0.004,0.077]
                    qrt = new Engineering.SampleQuartiles( stat );
                    qrt.Evaluate();
                    actualMean = qrt.FilteredSample.Mean;
                    message = Report( $"{taskOption}({spinCount}).Q", qrt.FilteredSample );
                }
                else
                {
                    expectedMean = 0.5d; // SpinWait(9) 0.027±0.026ms 100:[0.004,0.174]
                    actualMean = stat.Mean;
                    message = Report( $"{taskOption}({spinCount})", stat );
                }

                TestInfo.VerboseMessage( message );
                Assert.IsTrue( actualMean < expectedMean, $"Actual mean {actualMean:0.000}ms should be lower then the expected mean {expectedMean:0.000}ms: {message}" );
            }

            if ( taskOption == TaskOptions.TaskDelay )
            {
                spinCount = 1;
                delay = TimeSpan.Zero;
                stat = EstimateTaskTime( taskOption, spinCount, delay, count );
                if ( removeOutliers )
                {
                    expectedMean = 0.005d; // TaskDelay(0.000).Q 0.0±0.0ms 73: [0.0,0.0]
                    qrt = new Engineering.SampleQuartiles( stat );
                    qrt.Evaluate();
                    actualMean = qrt.FilteredSample.Mean;
                    message = Report( $"{taskOption}({delay:s\\.fff}).Q", qrt.FilteredSample );
                }
                else
                {
                    expectedMean = 0.05d; // TaskDelay(0.000) 0.002±0.013ms 100:[0.0,0.135]
                    actualMean = stat.Mean;
                    message = Report( $"{taskOption}({delay:s\\.fff})", stat );
                }

                TestInfo.VerboseMessage( message );
                Assert.IsTrue( actualMean < expectedMean, $"Actual mean {actualMean:0.000}ms should be lower then the expected mean {expectedMean:0.000}ms: {message}" );
                taskOption = TaskOptions.TaskDelay;
                spinCount = 1;
                delay = 9.0d.FromMilliseconds();
                stat = EstimateTaskTime( taskOption, spinCount, delay, count );
                if ( removeOutliers )
                {
                    expectedMean = 20d; // TaskDelay(0.009).Q 15.065±4.965ms 100: [9.384,20.46]
                    qrt = new Engineering.SampleQuartiles( stat );
                    qrt.Evaluate();
                    actualMean = qrt.FilteredSample.Mean;
                    message = Report( $"{taskOption}({delay:s\\.fff}).Q", qrt.FilteredSample );
                }
                else
                {
                    expectedMean = 20d; // TaskDelay(0.009) 15.238±4.895ms 100:[9.302,20.502]
                    actualMean = stat.Mean;
                    message = Report( $"{taskOption}({delay:s\\.fff})", stat );
                }

                TestInfo.VerboseMessage( message );
                Assert.IsTrue( actualMean < expectedMean, $"Actual mean {actualMean:0.000}ms should be lower then the expected mean {expectedMean:0.000}ms: {message}" );
                taskOption = TaskOptions.TaskDelay;
                spinCount = 1;
                delay = 20.0d.FromMilliseconds();
                stat = EstimateTaskTime( taskOption, spinCount, delay, count );
                if ( removeOutliers )
                {
                    expectedMean = 35d; // TaskDelay(0.02).Q 31.715±1.11ms 87: [28.614,33.27]
                    qrt = new Engineering.SampleQuartiles( stat );
                    qrt.Evaluate();
                    actualMean = qrt.FilteredSample.Mean;
                    message = Report( $"{taskOption}({delay:s\\.fff}).Q", qrt.FilteredSample );
                }
                else
                {
                    expectedMean = 35d; // TaskDelay(0.02) 30.942±1.986ms 100:[25.768,34.789]
                    actualMean = stat.Mean;
                    message = Report( $"{taskOption}({delay:s\\.fff})", stat );
                }

                TestInfo.VerboseMessage( message );
                Assert.IsTrue( actualMean < expectedMean, $"Actual mean {actualMean:0.000}ms should be lower then the expected mean {expectedMean:0.000}ms: {message}" );
            }

            if ( taskOption == TaskOptions.ThreadTask )
            {
                spinCount = 1;
                delay = TimeSpan.Zero;
                stat = EstimateTaskTime( taskOption, spinCount, delay, count );
                if ( removeOutliers )
                {
                    // 2020-11-05 no longer the case:
                    // expectedMean = 0.08d; // ThreadTask(0.0000).Q 0.018±0.007ms 91: [0.008,0.042]
                    expectedMean = 0.16d; // ThreadTask(0.0000).Q 0.096±0.062ms 20:[0.017,0.211]
                    qrt = new Engineering.SampleQuartiles( stat );
                    qrt.Evaluate();
                    actualMean = qrt.FilteredSample.Mean;
                    message = Report( $"{taskOption}({delay:s\\.ffff}).Q", qrt.FilteredSample );
                }
                else
                {
                    expectedMean = 0.6d; // ThreadTask(0.0000) 0.033±0.16ms 100:[0.005,1.607]
                    actualMean = stat.Mean;
                    message = Report( $"{taskOption}({delay:s\\.ffff})", stat );
                }
                TestInfo.VerboseMessage( message );
                Assert.IsTrue( actualMean < expectedMean, $"Actual mean {actualMean:0.000}ms should be lower then the expected mean {expectedMean:0.000}ms: {message}" );

                taskOption = TaskOptions.ThreadTask;
                spinCount = 1;
                delay = 0.5d.FromMilliseconds();
                stat = EstimateTaskTime( taskOption, spinCount, delay, count );
                if ( removeOutliers )
                {
                    expectedMean = 0.2d; // ThreadTask(0.0005).Q 0.054±0.047ms 97: [0.009,0.165]
                    qrt = new Engineering.SampleQuartiles( stat );
                    qrt.Evaluate();
                    actualMean = qrt.FilteredSample.Mean;
                    message = Report( $"{taskOption}({delay:s\\.ffff}).Q", qrt.FilteredSample );
                }
                else
                {
                    expectedMean = 0.5d; // ThreadTask(0.0005) 0.027±0.021ms 100:[0.005,0.114]
                    actualMean = stat.Mean;
                    message = Report( $"{taskOption}({delay:s\\.ffff})", stat );
                }

                TestInfo.VerboseMessage( message );
                Assert.IsTrue( actualMean < expectedMean, $"Actual mean {actualMean:0.000}ms should be lower then the expected mean {expectedMean:0.000}ms: {message}" );

                double rtcResolution = 15.4;
                spinCount = 1;
                delay = 1.5d.FromMilliseconds();
                stat = EstimateTaskTime( taskOption, spinCount, delay, count );
                if ( removeOutliers )
                {
                    // 2020-11-05 no longer the case:
                    // expectedMean = 2.5d; // ThreadTask(0.0015).Q 1.877±0.079ms 78: [1.675,2.083]
                    expectedMean = rtcResolution + delay.ToMicroseconds(); // ThreadTask(0.0015).Q 15.663±0.432ms 19:[14.685,16.199]
                    qrt = new Engineering.SampleQuartiles( stat );
                    qrt.Evaluate();
                    actualMean = qrt.FilteredSample.Mean;
                    message = Report( $"{taskOption}({delay:s\\.ffff}).Q", qrt.FilteredSample );
                }
                else
                {
                    // 2020-11-05 no longer the case:
                    // expectedMean = 2.5d; // ThreadTask(0.0015) 1.88±0.174ms 100:[1.334,2.582]
                    expectedMean = rtcResolution + delay.ToMicroseconds(); // ThreadTask(0.0015) 15.235±2.602ms 20:[4.277,16.196]
                    actualMean = stat.Mean;
                    message = Report( $"{taskOption}({delay:s\\.ffff})", stat );
                }
                TestInfo.VerboseMessage( message );
                Assert.IsTrue( actualMean < expectedMean, $"Actual mean {actualMean:0.000}ms should be lower then the expected mean {expectedMean:0.000}ms: {message}" );

                spinCount = 1;
                delay = 1.9d.FromMilliseconds();
                stat = EstimateTaskTime( taskOption, spinCount, delay, count );
                if ( removeOutliers )
                {
                    // 2020-11-05 no longer the case:
                    // expectedMean = 2.5d; // ThreadTask(0.0019).Q 1.879±0.084ms 85: [1.627,2.078]
                    expectedMean = rtcResolution + delay.ToMicroseconds(); // ThreadTask(0.0019).Q 15.698±0.445ms 20:[14.845,16.276]
                    qrt = new Engineering.SampleQuartiles( stat );
                    qrt.Evaluate();
                    message = Report( $"{taskOption}({delay:s\\.ffff}).Q", qrt.FilteredSample );
                }
                else
                {
                    // 2020-11-05 no longer the case:
                    // expectedMean = 2.5d; // ThreadTask(0.0019) 1.851±0.184ms 100:[1.262,2.339]
                    expectedMean = rtcResolution + delay.ToMicroseconds(); // ThreadTask(0.0019) 15.644±0.314ms 20:[15.163,16.23]
                    actualMean = stat.Mean;
                    message = Report( $"{taskOption}({delay:s\\.ffff})", stat );
                }
                TestInfo.VerboseMessage( message );
                Assert.IsTrue( actualMean < expectedMean, $"Actual mean {actualMean:0.000}ms should be lower then the expected mean {expectedMean:0.000}ms: {message}" );

                spinCount = 1;
                delay = 2.2d.FromMilliseconds();
                stat = EstimateTaskTime( taskOption, spinCount, delay, count );
                if ( removeOutliers )
                {
                    // 2020-11-05 no longer the case:
                    // expectedMean = 3.5d; // ThreadTask(0.0022).Q 2.891±0.082ms 85: [2.647,3.107]
                    expectedMean = rtcResolution + delay.ToMicroseconds(); // ThreadTask(0.0022).Q 15.623±0.502ms 20:[14.721,16.221]
                    qrt = new Engineering.SampleQuartiles( stat );
                    qrt.Evaluate();
                    actualMean = qrt.FilteredSample.Mean;
                    message = Report( $"{taskOption}({delay:s\\.ffff}).Q", qrt.FilteredSample );
                }
                else
                {
                    // 2020-11-05 no longer the case:
                    // expectedMean = 4.5d; // ThreadTask(0.0022) 2.862±0.219ms 100:[2.197,3.441]
                    expectedMean = rtcResolution + delay.ToMicroseconds(); // ThreadTask(0.0022) 15.621±0.466ms 20:[14.926,16.315]
                    actualMean = stat.Mean;
                    message = Report( $"{taskOption}({delay:s\\.ffff})", stat );
                }

                TestInfo.VerboseMessage( message );
                Assert.IsTrue( actualMean < expectedMean, $"Actual mean {actualMean:0.000}ms should be lower then the expected mean {expectedMean:0.000}ms: {message}" );
            }

            if ( taskOption == TaskOptions.HighRes )
            {
                spinCount = 1;
                delay = TimeSpan.Zero;
                stat = EstimateTaskTime( taskOption, spinCount, delay, count );
                if ( removeOutliers )
                {
                    expectedMean = 0.05d; // HighRes(0.00000).Q 0.015±0.003ms 84: [0.009,0.023]
                    qrt = new Engineering.SampleQuartiles( stat );
                    qrt.Evaluate();
                    actualMean = qrt.FilteredSample.Mean;
                    message = Report( $"{taskOption}({delay:s\\.fffff}).Q", qrt.FilteredSample );
                }
                else
                {
                    expectedMean = 0.5d; // HighRes(0.00000) 0.031±0.137ms 100:[0.003,1.341]
                    actualMean = stat.Mean;
                    message = Report( $"{taskOption}({delay:s\\.fffff})", stat );
                }

                TestInfo.VerboseMessage( message );
                Assert.IsTrue( actualMean < expectedMean, $"Actual mean {actualMean:0.000}ms should be lower then the expected mean {expectedMean:0.000}ms: {message}" );
                spinCount = 1;
                delay = 10.0d.FromMicroseconds();
                stat = EstimateTaskTime( taskOption, spinCount, delay, count );
                if ( removeOutliers )
                {
                    expectedMean = 0.09d; // HighRes(0.00001).Q 0.025±0.004ms 86: [0.019,0.037] 
                    qrt = new Engineering.SampleQuartiles( stat );
                    qrt.Evaluate();
                    actualMean = qrt.FilteredSample.Mean;
                    message = Report( $"{taskOption}({delay:s\\.fffff}).Q", qrt.FilteredSample );
                }
                else
                {
                    expectedMean = 1.5d; // HighRes(0.00001) 0.036±0.033ms 100:[0.014,0.268]
                    actualMean = stat.Mean;
                    message = Report( $"{taskOption}({delay:s\\.fffff})", stat );
                }

                TestInfo.VerboseMessage( message );
                Assert.IsTrue( actualMean < expectedMean, $"Actual mean {actualMean:0.000}ms should be lower then the expected mean {expectedMean:0.000}ms: {message}" );
                spinCount = 1;
                delay = 100.0d.FromMicroseconds();
                stat = EstimateTaskTime( taskOption, spinCount, delay, count );
                if ( removeOutliers )
                {
                    expectedMean = 0.2d; // HighRes(0.0001).Q 0.159±0.008ms 84: [0.143,0.19] 
                    qrt = new Engineering.SampleQuartiles( stat );
                    qrt.Evaluate();
                    actualMean = qrt.FilteredSample.Mean;
                    message = Report( $"{taskOption}({delay:s\\.fffff}).Q", qrt.FilteredSample );
                }
                else
                {
                    expectedMean = 0.3d; // HighRes(0.0001) 0.171±0.033ms 100:[0.126,0.351]
                    actualMean = stat.Mean;
                    message = Report( $"{taskOption}({delay:s\\.fffff})", stat );
                }

                TestInfo.VerboseMessage( message );
                Assert.IsTrue( actualMean < expectedMean, $"Actual mean {actualMean:0.000}ms should be lower then the expected mean {expectedMean:0.000}ms: {message}" );
            }
        }

        /// <summary> (Unit Test Method) tests timespan system clock delay statistics. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        [TestMethod()]
        public void TimespanSystemClockDelayNoOpStatisticsTest()
        {
            TestTimespanSystemClockDelayStatistics( TaskOptions.NoOp, false, 20 );
            TestTimespanSystemClockDelayStatistics( TaskOptions.NoOp, true, 20 );
        }

        /// <summary> (Unit Test Method) tests timespan system clock delay spin wait statistics. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        [TestMethod()]
        public void TimespanSystemClockDelaySpinWaitStatisticsTest()
        {
            TestTimespanSystemClockDelayStatistics( TaskOptions.SpinWait, false, 20 );
            TestTimespanSystemClockDelayStatistics( TaskOptions.SpinWait, true, 20 );
        }

        /// <summary>
        /// (Unit Test Method) tests timespan system clock delay task delay statistics.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        [TestMethod()]
        public void TimespanSystemClockDelayTaskDelayStatisticsTest()
        {
            TestTimespanSystemClockDelayStatistics( TaskOptions.TaskDelay, false, 20 );
            TestTimespanSystemClockDelayStatistics( TaskOptions.TaskDelay, true, 20 );
        }

        /// <summary>
        /// (Unit Test Method) tests timespan system clock delay thread task statistics.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        [TestMethod()]
        public void TimespanSystemClockDelayThreadTaskStatisticsTest()
        {
            TestTimespanSystemClockDelayStatistics( TaskOptions.ThreadTask, false, 20 );
            TestTimespanSystemClockDelayStatistics( TaskOptions.ThreadTask, true, 20 );
        }

        /// <summary>
        /// (Unit Test Method) tests timespan system clock delay high resource statistics.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        [TestMethod()]
        public void TimespanSystemClockDelayHighResStatisticsTest()
        {
            TestTimespanSystemClockDelayStatistics( TaskOptions.HighRes, false, 20 );
            TestTimespanSystemClockDelayStatistics( TaskOptions.HighRes, true, 20 );
        }


        #endregion


    }
}
