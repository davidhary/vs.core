<?xml version="1.0" encoding="utf-8"?>
<configuration>

  <configSections>
    <sectionGroup name="userSettings" type="System.Configuration.UserSettingsGroup" >
      <section name="isr.Core.EngineeringTests.TestSite" type="System.Configuration.ClientSettingsSection" />
      <section name="isr.Core.EngineeringTests.PolyFitTestInfo" type="System.Configuration.ClientSettingsSection" />
      <section name="isr.Core.EngineeringTests.LowTempZeroPressureBridgeTestInfo" type="System.Configuration.ClientSettingsSection" />
      <section name="isr.Core.EngineeringTests.CurrentSourceTestInfo" type="System.Configuration.ClientSettingsSection" />
      <section name="isr.Core.EngineeringTests.VoltageSourceTestInfo" type="System.Configuration.ClientSettingsSection" />
    </sectionGroup>
  </configSections>

  <userSettings>

    <!-- TEST SITE SETTINGS -->
    <isr.Core.EngineeringTests.TestSite>
      <setting name="Exists" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="Verbose" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="All" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="IPv4Prefixes" serializeAs="String">
        <value>192.168|10.1</value>
      </setting>
      <setting name="TimeZones" serializeAs="String">
        <value>Pacific Standard Time|Central Standard Time</value>
      </setting>
      <setting name="TimeZoneOffsets" serializeAs="String">
        <value>-8|-6</value>
      </setting>
    </isr.Core.EngineeringTests.TestSite>

    <!-- BALANCE BRIDGE: Low Temp Zero Pressure -->
    <isr.Core.EngineeringTests.LowTempZeroPressureBridgeTestInfo>
      <!-- CONFIGURATION -->
      <setting name="Exists" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="Verbose" serializeAs="String">
        <value>False</value>
      </setting>
      <setting name="Enabled" serializeAs="String">
        <value>True</value>
      </setting>
      <!-- BALANCE BRIDGE -->
      <setting name="Layout" serializeAs="String">
        <value>1</value>
      </setting>
      <setting name="R1" serializeAs="String">
        <value>369.59</value>
      </setting>
      <setting name="R2" serializeAs="String">
        <value>363.04</value>
      </setting>
      <setting name="R3" serializeAs="String">
        <value>357.69</value>
      </setting>
      <setting name="R4" serializeAs="String">
        <value>377.06</value>
      </setting>
      
      <setting name="SupplyVoltage" serializeAs="String">
        <value>5.02058</value>
      </setting>
      <setting name="SupplyVoltageDrop" serializeAs="String">
        <value>3.67312</value>
      </setting>
      <setting name="OutputVoltage" serializeAs="String">
        <value>0.011728</value>
      </setting>
      <setting name="R1Balance" serializeAs="String">
        <value>0</value>
      </setting>
      <setting name="R2Balance" serializeAs="String">
        <value>10.3</value>
      </setting>
      <setting name="R3Balance" serializeAs="String">
        <value>0</value>
      </setting>
      <setting name="R4Balance" serializeAs="String">
        <value>190.996E-6</value>
      </setting>
      <setting name="EquivalentResistance" serializeAs="String">
        <value>782</value>
      </setting>
      <setting name="RelativeOffsetEpsilon" serializeAs="String">
        <value>0.005</value>
      </setting>
      <setting name="NominalVoltage" serializeAs="String">
        <value>5</value>
      </setting>
      <setting name="VoltageSourceSeriesResistance" serializeAs="String">
        <value>1538</value>
      </setting>
      <setting name="VoltageSourceParallelResistance" serializeAs="String">
        <value>1593</value>
      </setting>
      <setting name="NominalFullScaleVoltage" serializeAs="String">
        <value>0.02</value>
      </setting>
      <setting name="NominalCurrent" serializeAs="String">
        <value>0.004</value>
      </setting>
      <setting name="CurrentSourceSeriesResistance" serializeAs="String">
        <value>636</value>
      </setting>
      <setting name="CurrentSourceParallelResistance" serializeAs="String">
        <value>150</value>
      </setting>
    </isr.Core.EngineeringTests.LowTempZeroPressureBridgeTestInfo>

    <!-- CURRENT SOURCE -->
    <isr.Core.EngineeringTests.CurrentSourceTestInfo>
      <!-- CONFIGURATION -->
      <setting name="Exists" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="Verbose" serializeAs="String">
        <value>False</value>
      </setting>
      <setting name="Enabled" serializeAs="String">
        <value>True</value>
      </setting>

      <!-- SOURCE -->
      <setting name="NominalVoltage"  serializeAs="String">
        <value>5.0</value>
      </setting>
      <setting name="SourceResistance"  serializeAs="String">
        <value>1000</value>
      </setting>
      <setting name="LoadResistance"  serializeAs="String">
        <value>700</value>
      </setting>

      <!-- ATTENUATED SOURCE -->
      <setting name="SeriesResistance"  serializeAs="String">
        <value>500</value>
      </setting>
      <setting name="ParallelResistance"  serializeAs="String">
        <value>1100</value>
      </setting>
      <setting name="NominalCurrent"  serializeAs="String">
        <value>0.004</value>
      </setting>
      <setting name="SourceConductance"  serializeAs="String">
        <value>0.001</value>
      </setting>

    </isr.Core.EngineeringTests.CurrentSourceTestInfo>

    <!-- VOLTAGE SOURCE -->
    <isr.Core.EngineeringTests.VoltageSourceTestInfo>
      <!-- CONFIGURATION -->
      <setting name="Exists" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="Verbose" serializeAs="String">
        <value>False</value>
      </setting>
      <setting name="Enabled" serializeAs="String">
        <value>True</value>
      </setting>

      <!-- SOURCE -->
      <setting name="NominalVoltage"  serializeAs="String">
        <value>5.0</value>
      </setting>
      <setting name="SourceResistance"  serializeAs="String">
        <value>1000</value>
      </setting>
      <setting name="LoadResistance"  serializeAs="String">
        <value>700</value>
      </setting>

      <!-- ATTENUATED SOURCE -->
      <setting name="SeriesResistance"  serializeAs="String">
        <value>500</value>
      </setting>
      <setting name="ParallelResistance"  serializeAs="String">
        <value>1100</value>
      </setting>
      <setting name="NominalCurrent"  serializeAs="String">
        <value>0.004</value>
      </setting>
      <setting name="SourceConductance"  serializeAs="String">
        <value>0.001</value>
      </setting>

    </isr.Core.EngineeringTests.VoltageSourceTestInfo>
    
    <!-- POLY FIT -->
    <isr.Core.EngineeringTests.PolyFitTestInfo>
      <!-- CONFIGURATION -->
      <setting name="Exists" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="Verbose" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="All" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="Enabled" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="MinimumElementCount" serializeAs="String">
          <value>3</value>
       </setting>
    </isr.Core.EngineeringTests.PolyFitTestInfo>
  
</userSettings>

</configuration>
