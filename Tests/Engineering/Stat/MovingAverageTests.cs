using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.EngineeringTests
{

    /// <summary> Moving average tests. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-01-27 </para>
    /// </remarks>
    [TestClass()]
    public class MovingAverageTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( EngineeringTests.MovingAverageTests.TestInfo.TimeZoneOffset ) < expectedUpperLimit,
                                $"{nameof( EngineeringTests.TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        /// <summary> The default window length. </summary>
        private const int _DefaultWindowLength = 5;

        /// <summary> Assert moving average. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="count">         Number of. </param>
        /// <param name="movingAverage"> The moving average. </param>
        /// <param name="sample">        The sample. </param>
        private static void AssertMovingAverage( int count, Engineering.MovingAverage movingAverage, Engineering.SampleStatistics sample )
        {
            _ = movingAverage.EvaluateMean();
            _ = sample.EvaluateMean();
            Assert.AreEqual( sample.Minimum, movingAverage.Minimum, $"Minimum after adding {count} values" );
            Assert.AreEqual( sample.Maximum, movingAverage.Maximum, $"Maximum after adding {count} values" );
            Assert.AreEqual( sample.Mean, movingAverage.Mean, $"Mean after adding {count} values" );
        }

        /// <summary> Adds a value. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="rng">           The random number generator. </param>
        /// <param name="queue">         The queue. </param>
        /// <param name="movingAverage"> The moving average. </param>
        private static void AddValue( Random rng, Queue<double> queue, Engineering.MovingAverage movingAverage )
        {
            movingAverage.AddValue( rng.NextDouble() );
            queue.Enqueue( movingAverage.ValuesList.Last() );
            while ( queue.Count > movingAverage.Length )
            {
                _ = queue.Dequeue();
            }
        }

        /// <summary> (Unit Test Method) moving average test multiple times. </summary>
        /// <remarks>
        /// Added because the moving average failed updating the range correctly when a adding a new
        /// value.
        /// </remarks>
        [TestMethod()]
        public void MovingAverageTestMultipleTimes()
        {
            for ( int i = 1; i <= 1000; i++ )
            {
                this.MovingAverageTest();
                ApplianceBase.DoEvents();
                ApplianceBase.Delay( 1 );
            }
        }

        /// <summary> (Unit Test Method) tests moving average. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void MovingAverageTest()
        {
            var movingAverage = new Engineering.MovingAverage( _DefaultWindowLength );
            var rng = new Random( DateTimeOffset.Now.Second );
            var sample = new Engineering.SampleStatistics();
            var queue = new Queue<double>();
            int count = 0;
            count += 1;
            AddValue( rng, queue, movingAverage );
            sample.ClearKnownState();
            sample.AddValues( queue.ToArray() );
            AssertMovingAverage( count, movingAverage, sample );
            count += 1;
            AddValue( rng, queue, movingAverage );
            sample.ClearKnownState();
            sample.AddValues( queue.ToArray() );
            AssertMovingAverage( count, movingAverage, sample );
            count += 1;
            AddValue( rng, queue, movingAverage );
            sample.ClearKnownState();
            sample.AddValues( queue.ToArray() );
            AssertMovingAverage( count, movingAverage, sample );
            count += 1;
            AddValue( rng, queue, movingAverage );
            sample.ClearKnownState();
            sample.AddValues( queue.ToArray() );
            AssertMovingAverage( count, movingAverage, sample );
            count += 1;
            AddValue( rng, queue, movingAverage );
            sample.ClearKnownState();
            sample.AddValues( queue.ToArray() );
            AssertMovingAverage( count, movingAverage, sample );
            count += 1;
            AddValue( rng, queue, movingAverage );
            sample.ClearKnownState();
            sample.AddValues( queue.ToArray() );
            AssertMovingAverage( count, movingAverage, sample );
        }
    }
}
