﻿using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.Core.ModelsTests.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.Core.ModelsTests.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.Core.ModelsTests.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
