
#pragma warning disable IDE1006 // Naming Styles
namespace isr.Core.ModelsTests.My
#pragma warning restore IDE1006 // Naming Styles
{

    /// <summary> Provides assembly information for the class library. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    public sealed partial class MyLibrary
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        private MyLibrary() : base()
        {
        }

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "Core Models Tests";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "Unit Tests for the Core Models Library";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "isr.Core.ModelsTests";
    }
}
