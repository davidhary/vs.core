using System;

using isr.Core.Models;

using Microsoft.VisualStudio.TestTools.UnitTesting;

#pragma warning disable IDE1006 // Naming Styles
namespace isr.Core.ModelsTests
#pragma warning restore IDE1006 // Naming Styles
{

    /// <summary> Event handling tests. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    [TestClass()]
    public class EventHandlingTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Models.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( ModelsTests.EventHandlingTests.TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( ModelsTests.TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " VIEW MODEL PROPERTY CHANGE EVENT HANDLING TESTS "

        /// <summary> A property change. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        private class PropertyChange : ViewModelBase
        {
            /// <summary> The expected value. </summary>
            private string _ExpectedValue;

            /// <summary> Gets or sets the expected value. </summary>
            /// <value> The expected value. </value>
            public string ExpectedValue
            {
                get => this._ExpectedValue;

                set {
                    if ( !string.Equals( value, this.ExpectedValue ) )
                    {
                        this._ExpectedValue = value;
                        this.SyncNotifyPropertyChanged();
                    }
                }
            }

            /// <summary> Gets or sets the actual value. </summary>
            /// <value> The actual value. </value>
            public string ActualValue { get; set; }
        }

        /// <summary> Handles the property changed. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property changed event information. </param>
        private void HandlePropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            if ( !(sender is PropertyChange propertyChangeSender) )
            {
                return;
            }

            switch ( e?.PropertyName ?? "" )
            {
                case nameof( PropertyChange.ExpectedValue ):
                    {
                        propertyChangeSender.ActualValue = propertyChangeSender.ExpectedValue;
                        break;
                    }
            }
        }

        /// <summary> (Unit Test Method) tests property change event handler. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        [TestMethod()]
        public void PropertyChangeHandlingTest()
        {
            var sender = new PropertyChange();
            sender.PropertyChanged += this.HandlePropertyChanged;
            sender.ExpectedValue = nameof( PropertyChange.ExpectedValue );
            Assert.AreEqual( sender.ExpectedValue, sender.ActualValue, "should equal after handling the synchronized event" );
        }

        #endregion

    }
}
