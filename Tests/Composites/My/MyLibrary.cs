﻿
namespace isr.Core.CompositesTests.My
{

    /// <summary> Provides assembly information for the class library. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    internal sealed partial class MyLibrary
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        private MyLibrary() : base()
        {
        }

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "Core Composites Tests";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "Unit Tests for the Core Composites Library";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "isr.Core.CompositesTests";
    }
}