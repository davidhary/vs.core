﻿using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.Core.CompositesTests.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.Core.CompositesTests.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.Core.CompositesTests.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
