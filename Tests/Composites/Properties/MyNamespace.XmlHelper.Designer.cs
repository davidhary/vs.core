// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

// See Compiler::LoadXmlSolutionExtension
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Xml.Linq;

using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.CompositesTests.My
{
    /// <summary>   An internal XML helper. This class cannot be inherited. </summary>
    /// <remarks>   David, 2020-09-23. </remarks>
    [Embedded()]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [System.Runtime.CompilerServices.CompilerGenerated()]
    [System.ComponentModel.EditorBrowsable( System.ComponentModel.EditorBrowsableState.Never )]
    internal sealed class InternalXmlHelper
    {
        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks>   David, 2020-09-23. </remarks>
        [System.ComponentModel.EditorBrowsable( System.ComponentModel.EditorBrowsableState.Never )]
        private InternalXmlHelper()
        {
        }

        /// <summary>   Gets a value. </summary>
        /// <remarks>   David, 2020-09-22. </remarks>
        /// <param name="source">   Source for the. </param>
        /// <returns>   The value. </returns>
        public static string GetValue( IEnumerable<XElement> source )
        {
            foreach ( XElement item in source )
            {
                return item.Value;
            }

            return null;
        }

        /// <summary>   Sets a value. </summary>
        /// <remarks>   David, 2020-09-22. </remarks>
        /// <param name="source">   Source for the. </param>
        /// <param name="value">    The value. </param>
        public static void SetValue( IEnumerable<XElement> source, string value )
        {
            foreach ( XElement item in source )
            {
                item.Value = value;
                break;
            }
        }

        /// <summary>   Gets attribute value. </summary>
        /// <remarks>   David, 2020-09-22. </remarks>
        /// <param name="source">   Source for the. </param>
        /// <param name="name">     The name. </param>
        /// <returns>   The attribute value. </returns>
        public static string GetAttributeValue( IEnumerable<XElement> source, XName name )
        {
            foreach ( XElement item in source )
            {
                return Conversions.ToString( item.Attribute( name ) );
            }

            return null;
        }

        /// <summary>   Sets attribute value. </summary>
        /// <remarks>   David, 2020-09-22. </remarks>
        /// <param name="source">   Source for the. </param>
        /// <param name="name">     The name. </param>
        /// <param name="value">    The value. </param>
        public static void SetAttributeValue( IEnumerable<XElement> source, XName name, string value )
        {
            foreach ( XElement item in source )
            {
                item.SetAttributeValue( name, value );
                break;
            }
        }

        /// <summary>   Gets attribute value. </summary>
        /// <remarks>   David, 2020-09-22. </remarks>
        /// <param name="source">   Source for the. </param>
        /// <param name="name">     The name. </param>
        /// <returns>   The attribute value. </returns>
        public static string GetAttributeValue( XElement source, XName name )
        {
            return Conversions.ToString( source.Attribute( name ) );
        }

        /// <summary>   Sets attribute value. </summary>
        /// <remarks>   David, 2020-09-22. </remarks>
        /// <param name="source">   Source for the. </param>
        /// <param name="name">     The name. </param>
        /// <param name="value">    The value. </param>
        public static void SetAttributeValue( XElement source, XName name, string value )
        {
            source.SetAttributeValue( name, value );
        }

        /// <summary>   Creates an attribute. </summary>
        /// <remarks>   David, 2020-09-22. </remarks>
        /// <param name="name">     The name. </param>
        /// <param name="value">    The value. </param>
        /// <returns>   The new attribute. </returns>
        [System.ComponentModel.EditorBrowsable( System.ComponentModel.EditorBrowsableState.Never )]
        public static XAttribute CreateAttribute( XName name, object value )
        {
            return value is null ? null : new XAttribute( name, value );
        }

        /// <summary>   Creates namespace attribute. </summary>
        /// <remarks>   David, 2020-09-22. </remarks>
        /// <param name="name"> The name. </param>
        /// <param name="ns">   The namespace. </param>
        /// <returns>   The new namespace attribute. </returns>
        [System.ComponentModel.EditorBrowsable( System.ComponentModel.EditorBrowsableState.Never )]
        public static XAttribute CreateNamespaceAttribute( XName name, XNamespace ns )
        {
            var a = new XAttribute( name, ns.NamespaceName );
            a.AddAnnotation( ns );
            return a;
        }

        /// <summary>   Removes the namespace attributes. </summary>
        /// <remarks>   David, 2020-09-22. </remarks>
        /// <param name="inScopePrefixes">  The in scope prefixes. </param>
        /// <param name="inScopeNs">        The in scope ns. </param>
        /// <param name="attributes">       The attributes. </param>
        /// <param name="obj">              The object. </param>
        /// <returns>   An object. </returns>
        [System.ComponentModel.EditorBrowsable( System.ComponentModel.EditorBrowsableState.Never )]
        public static object RemoveNamespaceAttributes( string[] inScopePrefixes, XNamespace[] inScopeNs, List<XAttribute> attributes, object obj )
        {
            if ( obj is object )
            {
                if ( obj is XElement elem )
                {
                    return RemoveNamespaceAttributes( inScopePrefixes, inScopeNs, attributes, elem );
                }
                else
                {
                    if ( obj is IEnumerable elems )
                    {
                        return RemoveNamespaceAttributes( inScopePrefixes, inScopeNs, attributes, elems );
                    }
                }
            }

            return obj;
        }

        /// <summary>   Removes the namespace attributes. </summary>
        /// <remarks>   David, 2020-09-22. </remarks>
        /// <param name="inScopePrefixes">  The in scope prefixes. </param>
        /// <param name="inScopeNs">        The in scope ns. </param>
        /// <param name="attributes">       The attributes. </param>
        /// <param name="obj">              The object. </param>
        /// <returns>   An object. </returns>
        [System.ComponentModel.EditorBrowsable( System.ComponentModel.EditorBrowsableState.Never )]
        public static IEnumerable RemoveNamespaceAttributes( string[] inScopePrefixes, XNamespace[] inScopeNs, List<XAttribute> attributes, IEnumerable obj )
        {
            return obj is object
                ? ( IEnumerable ) (obj is IEnumerable<XElement> elems
                    ? elems.Select( new RemoveNamespaceAttributesClosure( inScopePrefixes, inScopeNs, attributes ).ProcessXElement )
                    : obj.Cast<object>().Select( new RemoveNamespaceAttributesClosure( inScopePrefixes, inScopeNs, attributes ).ProcessObject ))
                : obj;
        }

        /// <summary>   A remove namespace attributes closure. This class cannot be inherited. </summary>
        /// <remarks>   David, 2020-09-22. </remarks>
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.Runtime.CompilerServices.CompilerGenerated()]
        [System.ComponentModel.EditorBrowsable( System.ComponentModel.EditorBrowsableState.Never )]
        private sealed class RemoveNamespaceAttributesClosure
        {
            /// <summary>   The in scope prefixes. </summary>
            private readonly string[] _InScopePrefixes;
            /// <summary>   The in scope ns. </summary>
            private readonly XNamespace[] _InScopeNs;
            /// <summary>   The attributes. </summary>
            private readonly List<XAttribute> _Attributes;

            /// <summary>   Constructor. </summary>
            /// <remarks>   David, 2020-09-22. </remarks>
            /// <param name="inScopePrefixes">  The in scope prefixes. </param>
            /// <param name="inScopeNs">        The in scope ns. </param>
            /// <param name="attributes">       The attributes. </param>
            [System.ComponentModel.EditorBrowsable( System.ComponentModel.EditorBrowsableState.Never )]
            internal RemoveNamespaceAttributesClosure( string[] inScopePrefixes, XNamespace[] inScopeNs, List<XAttribute> attributes )
            {
                this._InScopePrefixes = inScopePrefixes;
                this._InScopeNs = inScopeNs;
                this._Attributes = attributes;
            }

            /// <summary>   Process the x coordinate element described by elem. </summary>
            /// <remarks>   David, 2020-09-22. </remarks>
            /// <param name="elem"> The element. </param>
            /// <returns>   An XElement. </returns>
            [System.ComponentModel.EditorBrowsable( System.ComponentModel.EditorBrowsableState.Never )]
            internal XElement ProcessXElement( XElement elem )
            {
                return RemoveNamespaceAttributes( this._InScopePrefixes, this._InScopeNs, this._Attributes, elem );
            }

            /// <summary>   Process the object described by obj. </summary>
            /// <remarks>   David, 2020-09-23. </remarks>
            /// <param name="obj">  The object. </param>
            /// <returns>   An object. </returns>
            [System.ComponentModel.EditorBrowsable( System.ComponentModel.EditorBrowsableState.Never )]
            internal object ProcessObject( object obj )
            {
                return obj is XElement elem ? RemoveNamespaceAttributes( this._InScopePrefixes, this._InScopeNs, this._Attributes, elem ) : obj;
            }
        }

        /// <summary>   Removes the namespace attributes. </summary>
        /// <remarks>   David, 2020-09-22. </remarks>
        /// <param name="inScopePrefixes">  The in scope prefixes. </param>
        /// <param name="inScopeNs">        The in scope ns. </param>
        /// <param name="attributes">       The attributes. </param>
        /// <param name="e">                An XElement to process. </param>
        /// <returns>   An object. </returns>
        [System.ComponentModel.EditorBrowsable( System.ComponentModel.EditorBrowsableState.Never )]
        public static XElement RemoveNamespaceAttributes( string[] inScopePrefixes, XNamespace[] inScopeNs, List<XAttribute> attributes, XElement e )
        {
            if ( e is object )
            {
                var a = e.FirstAttribute;
                while ( a is object )
                {
                    var nextA = a.NextAttribute;
                    if ( a.IsNamespaceDeclaration )
                    {
                        var ns = a.Annotation<XNamespace>();
                        string prefix = a.Name.LocalName;
                        if ( ns is object )
                        {
                            if ( inScopePrefixes is object && inScopeNs is object )
                            {
                                int lastIndex = inScopePrefixes.Length - 1;
                                for ( int i = 0, loopTo = lastIndex; i <= loopTo; i++ )
                                {
                                    string currentInScopePrefix = inScopePrefixes[i];
                                    var currentInScopeNs = inScopeNs[i];
                                    if ( prefix.Equals( currentInScopePrefix ) )
                                    {
                                        if ( ns == currentInScopeNs )
                                        {
                                            // prefix and namespace match.  Remove the unneeded ns attribute 
                                            a.Remove();
                                        }

                                        // prefix is in scope but refers to something else.  Leave the ns attribute. 
                                        a = null;
                                        break;
                                    }
                                }
                            }

                            if ( a is object )
                            {
                                // Prefix is not in scope 
                                // Now check whether it's going to be in scope because it is in the attributes list 

                                if ( attributes is object )
                                {
                                    int lastIndex = attributes.Count - 1;
                                    for ( int i = 0, loopTo1 = lastIndex; i <= loopTo1; i++ )
                                    {
                                        var currentA = attributes[i];
                                        string currentInScopePrefix = currentA.Name.LocalName;
                                        var currentInScopeNs = currentA.Annotation<XNamespace>();
                                        if ( currentInScopeNs is object )
                                        {
                                            if ( prefix.Equals( currentInScopePrefix ) )
                                            {
                                                if ( ns == currentInScopeNs )
                                                {
                                                    // prefix and namespace match.  Remove the unneeded ns attribute 
                                                    a.Remove();
                                                }

                                                // prefix is in scope but refers to something else.  Leave the ns attribute. 
                                                a = null;
                                                break;
                                            }
                                        }
                                    }
                                }

                                if ( a is object )
                                {
                                    // Prefix is definitely not in scope  
                                    a.Remove();
                                    // namespace is not defined either.  Add this attributes list 
                                    attributes.Add( a );
                                }
                            }
                        }
                    }

                    a = nextA;
                }
            }

            return e;
        }
    }
}
