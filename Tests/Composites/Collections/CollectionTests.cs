using System;
using System.Collections.Specialized;

using isr.Core.Composites;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.CompositesTests
{

    /// <summary> This is a test class for event handling. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    [TestClass()]
    public class CollectionTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Core.My.MyLibrary.Appliance.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{ nameof( TestSite.TimeZoneOffset ) } should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " SYNCHRONIZED COLLECTION TESTS "

        /// <summary> The actual action. </summary>
        private NotifyCollectionChangedAction _ActualAction;

        /// <summary> Handles the collection changed. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Notify collection changed event information. </param>
        private void HandleCollectionChanged( object sender, NotifyCollectionChangedEventArgs e )
        {
            if ( !(sender is SynchronizedObservableCollection<string>) )
            {
                return;
            }

            this._ActualAction = e.Action;
        }

        /// <summary> (Unit Test Method) tests property change event handler. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        [TestMethod()]
        public void CollectionChangeHandlingTest()
        {
            using var sender = new SynchronizedObservableCollection<string>();
            sender.CollectionChanged += this.HandleCollectionChanged;
            sender.Add( $"Item#{sender.Count}" );
            var expectedAction = NotifyCollectionChangedAction.Add;
            Assert.AreEqual( expectedAction, this._ActualAction, "actions should equal after add" );
        }

        #endregion

    }
}
