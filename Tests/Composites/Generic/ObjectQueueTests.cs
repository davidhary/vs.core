using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections;
using isr.Core.Composites;

#pragma warning disable IDE1006 // Naming Styles
namespace isr.Core.CompositesTests
#pragma warning restore IDE1006 // Naming Styles
{
    /// <summary>   (Unit Test Class) an object queue tests. </summary>
    /// <remarks>   David, 2020-09-10. </remarks>
    [TestClass]
    public class ObjectQueueTests
    {

        # region " CONSTRUCTION AND CLEANUP "

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="testContext">  Context for the test. </param>
        [ClassInitialize()]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        public static void Initialize( TestContext testContext )
        {
            _Rnd = new Random();
        }

        /// <summary>   Cleanups this object. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        [ClassCleanup()]
        public static void Cleanup()
        {
        }

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        public ObjectQueueTests()
        {
        }

        /// <summary>   Gets or sets a context for the test. </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary>   Tests initialize. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        [TestInitialize()]
        public void TestInitialize()
        {
        }

        /// <summary>   Tests cleanup. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        [TestCleanup()]
        public void TestCleanup()
        {
        }

        #endregion 

        # region " RANDOM "

        /// <summary>   The random. </summary>
        private static Random _Rnd;

        /// <summary>   Generates a random byte array. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="length">   The length. </param>
        /// <returns>   An array of byte. </returns>
        protected static byte[] GenerateRandomBytes( int length )
        {
            var data = new byte[length];
            _Rnd.NextBytes( data );
            return data;
        }

        /// <summary>   Generates a random objects. </summary>
        /// <remarks>   David, 2020-09-10. </remarks>
        /// <param name="length">   The length. </param>
        /// <returns>   An array of object. </returns>
        protected static object[] GenerateRandomObjects( int length )
        {
            var data = new byte[length];
            _Rnd.NextBytes( data );
            var objects = new object[data.Length];
            Array.Copy( data, objects, data.Length );
            return objects;
        }

        #endregion

        #region " TEST METHODS "

        /// <summary>   (Unit Test Method) empty queue. </summary>
        /// <remarks>   David, 2020-09-10. </remarks>
        [TestMethod()]
        public void EmptyQueue()
        {
            var data = GenerateRandomBytes( 100 );
            var queue = new ObjectQueue( data.Length );
            var objects = new object[data.Length];
            Array.Copy( data, objects, data.Length );
            _ = queue.Enqueue( objects );
            var ret = new object[queue.Count];
            _ = queue.Dequeue( ret );
            CollectionAssert.AreEqual( data, ret );
            Assert.IsTrue( queue.Count == 0 );
        }

        /// <summary>   (Unit Test Method) fill queue. </summary>
        /// <remarks>   David, 2020-09-10. </remarks>
        [TestMethod()]
        public void FillQueue()
        {
            var data = GenerateRandomBytes( 100 );
            var queue = new ObjectQueue( data.Length );
            var objects = new object[data.Length];
            Array.Copy( data, objects, data.Length );
            _ = queue.Enqueue( objects );
            CollectionAssert.AreEqual( data, queue.ToArray() );
        }

        /// <summary>   (Unit Test Method) wrap around queue. </summary>
        /// <remarks>   David, 2020-09-10. </remarks>
        [TestMethod()]
        public void WrapAroundQueue()
        {
            var objects = GenerateRandomObjects( 100 );
            var queue = new ObjectQueue( objects.Length );
            _ = queue.Enqueue( GenerateRandomObjects( objects.Length / 2 ) );
            _ = queue.Enqueue( objects );
            queue.Skip( objects.Length - objects.Length / 2 );
            CollectionAssert.AreEqual( objects, queue.ToArray( objects.Length ) );
        }

        /// <summary>   (Unit Test Method) queue iteration. </summary>
        /// <remarks>   David, 2020-09-10. </remarks>
        /// <param name="queue">    The queue. </param>
        /// <param name="objects">  The objects. </param>
        private static void QueueIteration( ICollection queue, object[] objects )
        {
            int i = 0;
            foreach ( object item in queue )
            {
                Assert.AreEqual( item, objects[i] );
                i++;
            }
        }

        /// <summary>   (Unit Test Method) queue iteration. </summary>
        /// <remarks>   David, 2020-09-10. </remarks>
        /// <param name="objects">  The objects. </param>
        /// <param name="queue">    The queue. </param>
        private static void QueueIteration( object[] objects, IEnumerable queue )
        {
            int i = 0;
            foreach ( object item in queue )
            {
                Assert.AreEqual( item, objects[i] );
                i++;
            }
        }

        /// <summary>   (Unit Test Method) queue iteration. </summary>
        /// <remarks>   David, 2020-09-10. </remarks>
        [TestMethod()]
        public void QueueIteration()
        {
            var objects = GenerateRandomObjects( 100 );
            var queue = new ObjectQueue( objects.Length );
            _ = queue.Enqueue( objects );
            int i = 0;
            foreach ( object item in queue )
            {
                Assert.AreEqual( item, objects[i] );
                i++;
            }
            ObjectQueueTests.QueueIteration( queue, objects );
            ObjectQueueTests.QueueIteration( objects, queue );
        }

        #endregion
    }
}
