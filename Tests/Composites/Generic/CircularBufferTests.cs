using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using isr.Core.Composites;

#pragma warning disable IDE1006 // Naming Styles
namespace isr.Core.CompositesTests
#pragma warning restore IDE1006 // Naming Styles
{
    /// <summary>   (Unit Test Class) a circular buffer tests. </summary>
    /// <remarks>   David, 2020-09-09.
    /// From: https://archive.codeplex.com/?p=circularbuffer </remarks>
    /// <license> (c) 2012 Alex Regueiro.<para>
    /// Licensed under The MIT License. </para><para>
    /// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    /// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    /// NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    /// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    /// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    /// </para> </license>
    [TestClass()]
    public class CircularBufferTests
    {

        # region " CONSTRUCTION AND CLEANUP "

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="testContext">  Context for the test. </param>
        [ClassInitialize()]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        public static void Initialize( TestContext testContext )
        {
            _Rnd = new Random();
        }

        /// <summary>   Cleanups this object. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        [ClassCleanup()]
        public static void Cleanup()
        {
        }

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        public CircularBufferTests()
        {
        }

        /// <summary>   Gets or sets a context for the test. </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary>   Tests initialize. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        [TestInitialize()]
        public void TestInitialize()
        {
        }

        /// <summary>   Tests cleanup. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        [TestCleanup()]
        public void TestCleanup()
        {
        }

        #endregion 

        # region " RANDOM "

        /// <summary>   The random. </summary>
        private static Random _Rnd;

        /// <summary>   Generates a random data. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="length">   The length. </param>
        /// <returns>   An array of byte. </returns>
        protected static byte[] GenerateRandomData( int length )
        {
            var data = new byte[length];
            _Rnd.NextBytes( data );
            return data;
        }

        #endregion 

        # region " TEST METHODS "

        /// <summary>   (Unit Test Method) empty buffer. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        [TestMethod()]
        public void EmptyBuffer()
        {
            var data = GenerateRandomData( 100 );
            var buffer = new CircularBuffer<byte>( data.Length );
            _ = buffer.Put( data );
            var ret = new byte[buffer.Size];
            _ = buffer.Get( ret );
            CollectionAssert.AreEqual( data, ret );
            Assert.IsTrue( buffer.Size == 0 );
        }

        /// <summary>   (Unit Test Method) fill buffer. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        [TestMethod()]
        public void FillBuffer()
        {
            var data = GenerateRandomData( 100 );
            var buffer = new CircularBuffer<byte>( data.Length );
            _ = buffer.Put( data );
            CollectionAssert.AreEqual( data, buffer.ToArray() );
        }

        /// <summary>   (Unit Test Method) wrap around buffer. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        [TestMethod()]
        public void WrapAroundBuffer()
        {
            var data = GenerateRandomData( 100 );
            var buffer = new CircularBuffer<byte>( data.Length, true );
            _ = buffer.Put( GenerateRandomData( data.Length / 2 ) );
            _ = buffer.Put( data );
            buffer.Skip( data.Length - data.Length / 2 );
            CollectionAssert.AreEqual( data, buffer.ToArray() );
        }

        /// <summary>   (Unit Test Method) overflow buffer. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        [TestMethod(), ExpectedException( typeof( InvalidOperationException ) )]
        public void OverflowBuffer()
        {
            var data = GenerateRandomData( 100 );
            var buffer = new CircularBuffer<byte>( data.Length - 1, false );
            _ = buffer.Put( data );
        }

        #endregion 

    }
}
