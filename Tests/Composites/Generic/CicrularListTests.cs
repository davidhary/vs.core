using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections;
using isr.Core.Composites;

#pragma warning disable IDE1006 // Naming Styles
namespace isr.Core.CompositesTests
#pragma warning restore IDE1006 // Naming Styles
{
    /// <summary>   (Unit Test Class) a circular list tests. </summary>
    /// <remarks>   David, 2020-09-10. </remarks>
    [TestClass]
    public class CircularListTests
    {

        # region " CONSTRUCTION AND CLEANUP "

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="testContext">  Context for the test. </param>
        [ClassInitialize()]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        public static void Initialize( TestContext testContext )
        {
            _Rnd = new Random();
        }

        /// <summary>   Cleanups this object. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        [ClassCleanup()]
        public static void Cleanup()
        {
        }

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        public CircularListTests()
        {
        }

        /// <summary>   Gets or sets a context for the test. </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary>   Tests initialize. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        [TestInitialize()]
        public void TestInitialize()
        {
        }

        /// <summary>   Tests cleanup. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        [TestCleanup()]
        public void TestCleanup()
        {
        }

        #endregion 

        # region " RANDOM "

        /// <summary>   The random. </summary>
        private static Random _Rnd;

        /// <summary>   Generates a random byte array. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="length">   The length. </param>
        /// <returns>   An array of byte. </returns>
        protected static byte[] GenerateRandomBytes( int length )
        {
            var bytes = new byte[length];
            _Rnd.NextBytes( bytes );
            return bytes;
        }

        #endregion

        #region " TEST METHODS "

        /// <summary>   (Unit Test Method) fill list. </summary>
        /// <remarks>   David, 2020-09-10. </remarks>
        [TestMethod()]
        public void FillList()
        {
            var data = GenerateRandomBytes( 100 );
            var list = new CircularList<byte>( data.Length ) {
                data
            };
            CollectionAssert.AreEqual( data, list.ToArray() );
        }

        /// <summary>   (Unit Test Method) overflow list. </summary>
        /// <remarks>   David, 2020-09-10. </remarks>
        [TestMethod()]
        public void OverflowList()
        {
            var data = GenerateRandomBytes( 100 );
            var list = new CircularList<byte>( data.Length ) {
                data
            };
            data = GenerateRandomBytes( 100 );
            list.Add( data );
            CollectionAssert.AreEqual( data, list.ToArray() );
        }

        /// <summary>   (Unit Test Method) list iteration. </summary>
        /// <remarks>   David, 2020-09-10. </remarks>
        /// <param name="list">     The list. </param>
        /// <param name="bytes">    The bytes. </param>
        private static void ListIteration( ICollection list, byte[] bytes )
        {
            int i = 0;
            foreach ( byte item in list )
            {
                Assert.AreEqual( item, bytes[i] );
                i++;
            }
        }

        /// <summary>   (Unit Test Method) list iteration. </summary>
        /// <remarks>   David, 2020-09-10. </remarks>
        /// <param name="dummy">    The dummy. </param>
        /// <param name="list">     The list. </param>
        /// <param name="bytes">    The bytes. </param>
        private static void ListIteration( int dummy, IReadOnlyCollection<byte> list, byte[] bytes )
        {
            int i = dummy == 0 ? dummy : 0;
            foreach ( byte item in list )
            {
                Assert.AreEqual( item, bytes[i] );
                i++;
            }
        }

        /// <summary>   (Unit Test Method) list iteration. </summary>
        /// <remarks>   David, 2020-09-10. </remarks>
        /// <param name="bytes">    The bytes. </param>
        /// <param name="list">     The list. </param>
        private static void ListIteration( byte[] bytes, IEnumerable list )
        {
            int i = 0;
            foreach ( byte item in list )
            {
                Assert.AreEqual( item, bytes[i] );
                i++;
            }
        }

        /// <summary>   (Unit Test Method) list iteration. </summary>
        /// <remarks>   David, 2020-09-10. </remarks>
        /// <param name="dummy">    The dummy. </param>
        /// <param name="bytes">    The bytes. </param>
        /// <param name="list">     The list. </param>
        private static void ListIteration( int dummy, byte[] bytes, IEnumerable<byte> list )
        {
            int i = dummy == 0 ? dummy : 0;
            foreach ( byte item in list )
            {
                Assert.AreEqual( item, bytes[i] );
                i++;
            }
        }

        /// <summary>   (Unit Test Method) list iteration. </summary>
        /// <remarks>   David, 2020-09-10. </remarks>
        [TestMethod()]
        public void ListIteration()
        {
            var data = GenerateRandomBytes( 100 );
            var list = new CircularList<byte>( data.Length ) {
                data
            };
            int i = 0;
            foreach ( byte item in list )
            {
                Assert.AreEqual( item, data[i] );
                i++;
            }
            CircularListTests.ListIteration( list, data );
            CircularListTests.ListIteration( data, list );
            CircularListTests.ListIteration( 0, list, data );
            CircularListTests.ListIteration( 0, data, list );
        }

        #endregion
    }
}
