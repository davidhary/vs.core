using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections;
using isr.Core.Composites;

#pragma warning disable IDE1006 // Naming Styles
namespace isr.Core.CompositesTests
#pragma warning restore IDE1006 // Naming Styles
{
    /// <summary>   (Unit Test Class) a generic queue tests. </summary>
    /// <remarks>   David, 2020-09-10. </remarks>
    [TestClass]
    public class GenericQueueTests
    {

        # region " CONSTRUCTION AND CLEANUP "

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="testContext">  Context for the test. </param>
        [ClassInitialize()]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        public static void Initialize( TestContext testContext )
        {
            _Rnd = new Random();
        }

        /// <summary>   Cleanups this object. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        [ClassCleanup()]
        public static void Cleanup()
        {
        }

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        public GenericQueueTests()
        {
        }

        /// <summary>   Gets or sets a context for the test. </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary>   Tests initialize. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        [TestInitialize()]
        public void TestInitialize()
        {
        }

        /// <summary>   Tests cleanup. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        [TestCleanup()]
        public void TestCleanup()
        {
        }

        #endregion 

        # region " RANDOM "

        /// <summary>   The random. </summary>
        private static Random _Rnd;

        /// <summary>   Generates a random byte array. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="length">   The length. </param>
        /// <returns>   An array of byte. </returns>
        protected static byte[] GenerateRandomBytes( int length )
        {
            var bytes = new byte[length];
            _Rnd.NextBytes( bytes );
            return bytes;
        }

        #endregion

        #region " TEST METHODS "

        /// <summary>   (Unit Test Method) empty queue. </summary>
        /// <remarks>   David, 2020-09-10. </remarks>
        [TestMethod()]
        public void EmptyQueue()
        {
            var data = GenerateRandomBytes( 100 );
            var queue = new GenericQueue<byte>( data.Length );
            _ = queue.Enqueue( data );
            var ret = new byte[queue.Count];
            _ = queue.Dequeue( ret );
            CollectionAssert.AreEqual( data, ret );
            Assert.IsTrue( queue.Count == 0 );
        }

        /// <summary>   (Unit Test Method) fill queue. </summary>
        /// <remarks>   David, 2020-09-10. </remarks>
        [TestMethod()]
        public void FillQueue()
        {
            var data = GenerateRandomBytes( 100 );
            var queue = new GenericQueue<byte>( data.Length );
            _ = queue.Enqueue( data );
            CollectionAssert.AreEqual( data, queue.ToArray() );
        }

        /// <summary>   (Unit Test Method) wrap around queue. </summary>
        /// <remarks>   David, 2020-09-10. </remarks>
        [TestMethod()]
        public void WrapAroundQueue()
        {
            var data = GenerateRandomBytes( 100 );
            var queue = new GenericQueue<byte>( data.Length );
            _ = queue.Enqueue( GenerateRandomBytes( data.Length / 2 ) );
            _ = queue.Enqueue( data );
            queue.Skip( data.Length - data.Length / 2 );
            CollectionAssert.AreEqual( data, queue.ToArray( data.Length ) );
        }

        /// <summary>   (Unit Test Method) queue iteration. </summary>
        /// <remarks>   David, 2020-09-10. </remarks>
        /// <param name="queue">    The queue. </param>
        /// <param name="bytes">    The bytes. </param>
        private static void QueueIteration( ICollection queue, byte[] bytes )
        {
            int i = 0;
            foreach ( byte item in queue )
            {
                Assert.AreEqual( item, bytes[i] );
                i++;
            }
        }

        /// <summary>   (Unit Test Method) queue iteration. </summary>
        /// <remarks>   David, 2020-09-10. </remarks>
        /// <param name="dummy">    The dummy. </param>
        /// <param name="queue">    The queue. </param>
        /// <param name="bytes">    The bytes. </param>
        private static void QueueIteration( int dummy, ICollection<byte> queue, byte[] bytes )
        {
            int i = dummy == 0 ? dummy : 0;
            foreach ( byte item in queue )
            {
                Assert.AreEqual( item, bytes[i] );
                i++;
            }
        }

        /// <summary>   (Unit Test Method) queue iteration. </summary>
        /// <remarks>   David, 2020-09-10. </remarks>
        /// <param name="bytes">    The bytes. </param>
        /// <param name="queue">    The queue. </param>
        private static void QueueIteration( byte[] bytes, IEnumerable queue )
        {
            int i = 0;
            foreach ( byte item in queue )
            {
                Assert.AreEqual( item, bytes[i] );
                i++;
            }
        }

        /// <summary>   (Unit Test Method) queue iteration. </summary>
        /// <remarks>   David, 2020-09-10. </remarks>
        /// <param name="dummy">    The dummy. </param>
        /// <param name="bytes">    The bytes. </param>
        /// <param name="queue">    The queue. </param>
        private static void QueueIteration( int dummy, byte[] bytes, IEnumerable<byte> queue )
        {
            int i = dummy == 0 ? dummy : 0;
            foreach ( byte item in queue )
            {
                Assert.AreEqual( item, bytes[i] );
                i++;
            }
        }

        /// <summary>   (Unit Test Method) queue iteration. </summary>
        /// <remarks>   David, 2020-09-10. </remarks>
        [TestMethod()]
        public void QueueIteration()
        {
            var data = GenerateRandomBytes( 100 );
            var queue = new GenericQueue<byte>( data.Length );
            _ = queue.Enqueue( data );
            int i = 0;
            foreach ( byte item in queue )
            {
                Assert.AreEqual( item, data[i] );
                i++;
            }
            GenericQueueTests.QueueIteration( queue, data );
            GenericQueueTests.QueueIteration( data, queue );
            GenericQueueTests.QueueIteration( 0, queue, data );
            GenericQueueTests.QueueIteration( 0, data, queue );
        }

        #endregion
    }
}
