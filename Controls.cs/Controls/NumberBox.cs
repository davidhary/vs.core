using System;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace isr.Core.Controls
{
    /// <summary>   Number box control. </summary>
    /// <license> (c) 2020 Silicon Video. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    /// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    /// NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    /// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    /// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    /// SOFTWARE.</para> </license>
    /// <history date="2020-04-08" by="David" revision=""> Created
    /// https://www.codeproject.com/Articles/5264392/NumberBox-Class-for-Number-Entry-Display
    /// [Silicon Video](https://www.codeproject.com/script/Membership/View.aspx?mid=3452838)
    /// </history>
    public class NumberBox : TextBox
    {

        #region " Constructors "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-04-08. </remarks>
        public NumberBox()
        {
            Leave += this.NumberBox_Leave;
            KeyPress += this.NumberBox_KeyPress;
            KeyDown += this.NumberBox_KeyDown;
            MouseDoubleClick += this.NumberBox_MouseDoubleClick;
            Enter += this.NumberBox_Enter;
        }
        #endregion Constructors

        #region " Properties "

        /// <summary>   The number format information. </summary>
        private readonly NumberFormatInfo _Nfi = new CultureInfo( CultureInfo.CurrentCulture.Name, false ).NumberFormat;

        /// <summary>   Specifies the pattern. </summary>
        private string _Pattern;

        /// <summary>   True if Standard numeric format string is empty. </summary>
        private bool _SnfsEmpty = true;

        /// <summary>   The Standard numeric format string </summary>
        private string _Snfs;

        /// <summary>   Standard Numeric Format String D,E,F,N,X, B for binary. </summary>
        /// <value> The Standard numeric format string </value>
        [Category( "NumberBox" ), Description( "Standard Numeric Format String D,E,F,N,X, B for binary" )]
        public string Snfs
        {
            get => this._Snfs;
            set {
                this._Snfs = value;
                this._SnfsEmpty = string.IsNullOrEmpty( this._Snfs );
                this._Pattern = this.BuildRegularExpression();
                this.LeaveBox();
            }
        }

        /// <summary>   True to prefix empty. </summary>
        private bool _PrefixEmpty = true;

        /// <summary>   The prefix. </summary>
        private string _Prefix;

        /// <summary>   Prefix for X and B formats. </summary>
        /// <value> The prefix. </value>
        [Category( "NumberBox" ), Description( "Prefix for X and B formats" )]
        public string Prefix
        {
            get => this._Prefix;
            set {
                this._Prefix = value;
                this._PrefixEmpty = string.IsNullOrEmpty( this._Prefix );
                this._Pattern = this.BuildRegularExpression();
                this.LeaveBox();
            }
        }

        /// <summary>   True to suffix empty. </summary>
        private bool _SuffixEmpty = true;

        /// <summary>   The suffix. </summary>
        private string _Suffix;

        /// <summary>   Suffix for X and B formats. </summary>
        /// <value> The suffix. </value>
        [Category( "NumberBox" ), Description( "Suffix for X and B formats" )]
        public string Suffix
        {
            get => this._Suffix;
            set {
                this._Suffix = value;
                this._SuffixEmpty = string.IsNullOrEmpty( this._Suffix );
                this._Pattern = this.BuildRegularExpression();
                this.LeaveBox();
            }
        }

        /// <summary>   The maximum value. </summary>
        private double _MaxValue = double.MaxValue;

        /// <summary>   Gets/sets Max Value...double. </summary>
        /// <value> The maximum value. </value>
        [Category( "NumberBox" ), Description( "gets/sets Max Value...double" )]
        public double MaxValue
        {
            get => this._MaxValue;
            set {
                this._MaxValue = value;
                this.LeaveBox();
            }
        }

        /// <summary>   The minimum value. </summary>
        private double _MinValue = double.MinValue;

        /// <summary>   Gets/sets Min Value...double. </summary>
        /// <value> The minimum value. </value>
        [Category( "NumberBox" ), Description( "gets/sets Min Value...double" )]
        public double MinValue
        {
            get => this._MinValue;
            set {
                this._MinValue = value;
                this.LeaveBox();
            }
        }

        /// <summary>   Gets/sets Value As Double. </summary>
        /// <value> The value as double. </value>
        [Category( "NumberBox" ), Description( "gets/sets Value As Double" ), Browsable( true )]
        public double ValueAsDouble
        {
            get => this.GetValue();
            set => this.SetValue( value );
        }

        /// <summary>   Gets/sets Value As Float. </summary>
        /// <value> The value as float. </value>
        [Category( "NumberBox" ), Description( "gets/sets Value As Float" ), Browsable( true )]
        public float ValueAsFloat
        {
            get => ( float ) this.GetValue();
            set => this.SetValue( value );
        }

        /// <summary>   Gets/sets Value As Byte. </summary>
        /// <value> The value as byte. </value>
        [Category( "NumberBox" ), Description( "gets/sets Value As Byte" ), Browsable( true )]
        public byte ValueAsByte
        {
            get => ( byte ) Math.Min( byte.MaxValue, Math.Max( byte.MinValue, this.GetValue() ) );
            set => this.SetValue( value );
        }

        /// <summary>   Gets/sets Value As SByte. </summary>
        /// <value> The value as s byte. </value>
        [Category( "NumberBox" ), Description( "gets/sets Value As SByte" ), Browsable( true )]
        public sbyte ValueAsSByte
        {
            get => ( sbyte ) Math.Min( sbyte.MaxValue, Math.Max( sbyte.MinValue, this.GetValue() ) );
            set => this.SetValue( value );
        }

        /// <summary>   Gets/sets Value As UInt64. </summary>
        /// <value> The value as u int 64. </value>
        [Category( "NumberBox" ), Description( "gets/sets Value As UInt64" ), Browsable( true )]
        public ulong ValueAsUInt64
        {
            get => ( ulong ) Math.Min( ulong.MaxValue, Math.Max( ulong.MinValue, this.GetValue() ) );
            set => this.SetValue( value );
        }

        /// <summary>   Gets/sets Value As UInt32. </summary>
        /// <value> The value as u int 32. </value>
        [Category( "NumberBox" ), Description( "gets/sets Value As UInt32" ), Browsable( true )]
        public uint ValueAsUInt32
        {
            get => ( uint ) Math.Min( uint.MaxValue, Math.Max( uint.MinValue, this.GetValue() ) );
            set => this.SetValue( value );
        }

        /// <summary>   Gets/sets Value As UInt16. </summary>
        /// <value> The value as u int 16. </value>
        [Category( "NumberBox" ), Description( "gets/sets Value As UInt16" ), Browsable( true )]
        public ushort ValueAsUInt16
        {
            get => ( ushort ) Math.Min( ushort.MaxValue, Math.Max( ushort.MinValue, this.GetValue() ) );
            set => this.SetValue( value );
        }

        /// <summary>   Gets/sets Value As Int64. </summary>
        /// <value> The value as int 64. </value>
        [Category( "NumberBox" ), Description( "gets/sets Value As Int64" ), Browsable( true )]
        public long ValueAsInt64
        {
            get => ( long ) Math.Min( long.MaxValue, Math.Max( long.MinValue, this.GetValue() ) );
            set => this.SetValue( value );
        }

        /// <summary>   Gets/sets Value As Int32. </summary>
        /// <value> The value as int 32. </value>
        [Category( "NumberBox" ), Description( "gets/sets Value As Int32" ), Browsable( true )]
        public int ValueAsInt32
        {
            get => ( int ) Math.Min( int.MaxValue, Math.Max( int.MinValue, this.GetValue() ) );
            set => this.SetValue( value );
        }

        /// <summary>   Gets/sets Value As Int16. </summary>
        /// <value> The value as int 16. </value>
        [Category( "NumberBox" ), Description( "gets/sets Value As Int16" ), Browsable( true )]
        public short ValueAsInt16
        {
            get => ( short ) Math.Min( short.MaxValue, Math.Max( short.MinValue, this.GetValue() ) );
            set => this.SetValue( value );
        }

        #endregion Fields

        #region " Methods "

        /// <summary>   Leave box. </summary>
        /// <remarks>   David, 2020-04-08. </remarks>
        public void LeaveBox()
        {
            this.SetValue( Math.Max( this._MinValue, Math.Min( this._MaxValue, this.GetValue() ) ) );
        }

        /// <summary>   Gets the value. </summary>
        /// <remarks>   David, 2020-04-08. </remarks>
        /// <returns>   The value. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        private double GetValue()
        {
            if ( this._SnfsEmpty )
            {
                return 0;
            }

            double x;
            var text = this.RemovePrefixSuffix( this.Text );
            if ( string.IsNullOrEmpty( text ) )
            {
                text = "0";
            }

            try
            {
                x = this._Snfs.StartsWith( "X", StringComparison.OrdinalIgnoreCase )
                    ? Convert.ToUInt64( text, 16 )
                    : this._Snfs.StartsWith( "B", StringComparison.OrdinalIgnoreCase ) ? Convert.ToUInt64( text, 2 ) : Convert.ToDouble( text );
            }
            catch ( Exception )
            {
                x = 0;
            }
            return x;
        }

        /// <summary>   Removes the prefix suffix described by text. </summary>
        /// <remarks>   David, 2020-04-08. </remarks>
        /// <param name="text"> The text. </param>
        /// <returns>   A string. </returns>
        private string RemovePrefixSuffix( string text )
        {
            if ( !this._PrefixEmpty && text.StartsWith( this._Prefix ) )
            {
                text = text.Substring( this._Prefix.Length );
            }

            if ( !this._SuffixEmpty && text.EndsWith( this._Suffix ) )
            {
                text = text.Substring( 0, text.Length - this._Suffix.Length );
            }

            return text;
        }

        /// <summary>   Sets a value. </summary>
        /// <remarks>   David, 2020-04-08. </remarks>
        /// <param name="x">    The x coordinate. </param>
        private void SetValue( double x )
        {
            if ( this._SnfsEmpty )
            {
                return;
            }

            x = Math.Max( this._MinValue, Math.Min( this._MaxValue, x ) );
            if ( this._Snfs.StartsWith( "D", StringComparison.OrdinalIgnoreCase ) )
            {
                if ( x >= 0 )
                {
                    this.Text = (( uint ) x).ToString( this._Snfs, this._Nfi );
                }
                else
                {
                    x = -x;
                    this.Text = @"-" + (( uint ) x).ToString( this._Snfs, this._Nfi );
                }
            }
            else if ( this._Snfs.StartsWith( "X", StringComparison.OrdinalIgnoreCase ) )
            {
                this.Text = (( int ) x).ToString( this._Snfs );
            }
            else if ( this._Snfs.StartsWith( "B", StringComparison.OrdinalIgnoreCase ) )
            {
                var str = Convert.ToString( ( long ) x, 2 );
                var len = LengthParameter( this.Snfs );
                if ( len == 0 )
                {
                    len = 1;
                }

                while ( str.Length < len )
                {
                    str = "0" + str;
                }

                this.Text = str;
            }
            else
            {
                this.Text = x.ToString( this._Snfs, this._Nfi );
            }

            this.Text = this.AddPrefixSuffix( this.Text );
        }

        /// <summary>   Adds a prefix suffix. </summary>
        /// <remarks>   David, 2020-04-08. </remarks>
        /// <param name="text"> The text. </param>
        /// <returns>   A string. </returns>
        private string AddPrefixSuffix( string text )
        {
            if ( !this._PrefixEmpty )
            {
                text = this._Prefix + text;
            }

            if ( !this._SuffixEmpty )
            {
                text += this._Suffix;
            }

            return text;
        }

        /// <summary>   Builds regular expression. </summary>
        /// <remarks>   David, 2020-04-08. </remarks>
        /// <returns>   A string. </returns>
        private string BuildRegularExpression()
        {
            if ( this._SnfsEmpty )
            {
                return string.Empty;
            }

            // start of line anchor
            var str = "^";
            var len = LengthParameter( this._Snfs );

            // add in prefix
            if ( !this._PrefixEmpty )
            {
                str = this.Prefix.Aggregate( str, ( current, c ) => current + ("[" + c + "]") );
            }

            // D4 => -?\d\d?\d?\d?\d?
            if ( this._Snfs.StartsWith( "D", StringComparison.OrdinalIgnoreCase ) )
            {
                str += "-?";
                if ( len == 0 )
                {
                    len = 4;
                }

                for ( var i = 0; i < len; ++i )
                {
                    str += @"\d?";
                }
            }
            // E => ^-?\d*[.,]?\d?\d?\d?\d?\d?\d?[Ee]?[+-]?\d?\d?\d?
            if ( this._Snfs.StartsWith( "E", StringComparison.OrdinalIgnoreCase ) )
            {
                str += @"-?\d*[.,]?";
                if ( len == 0 )
                {
                    len = 6;  // SNFS default length
                }

                for ( var i = 0; i < len; ++i )
                {
                    str += @"\d?";
                }

                str += @"[Ee]?[+-]?\d?\d?\d?";
            }
            if ( this._Snfs.StartsWith( "F", StringComparison.OrdinalIgnoreCase ) )
            {
                str += @"-?\d*[.,]?";
                if ( len == 0 )
                {
                    len = 2;  //SNFS default length
                }

                for ( var i = 0; i < len; ++i )
                {
                    str += @"\d?";
                }
            }
            if ( this._Snfs.StartsWith( "N", StringComparison.OrdinalIgnoreCase ) )
            {
                str += @"-?\d*[., ]?";
                if ( len == 0 )
                {
                    len = 2;  //SNFS default length
                }

                for ( var i = 0; i < len; ++i )
                {
                    str += @"\d?";
                }
            }
            if ( this._Snfs.StartsWith( "X", StringComparison.OrdinalIgnoreCase ) )
            {
                if ( len == 0 )
                {
                    len = 2;  //SNFS default length
                }

                for ( var i = 0; i < len; ++i )
                {
                    str += @"[0-9A-Fa-f]?";
                }
            }
            if ( this._Snfs.StartsWith( "B", StringComparison.OrdinalIgnoreCase ) )
            {
                if ( len == 0 )
                {
                    len = 1;  //SNFS default length
                }

                for ( var i = 0; i < len; ++i )
                {
                    str += @"[0-1]?";
                }
            }

            // add suffix
            if ( !this._SuffixEmpty )
            {
                str = this._Suffix.Aggregate( str, ( current, c ) => current + ("[" + c + "]") );
            }

            // and the end of line anchor
            str += "$";

            return str;
        }

        /// <summary>   Select editable region. </summary>
        /// <remarks>   David, 2020-04-08. </remarks>
        private void SelectEditableRegion()
        {
            this.SelectionStart = this._PrefixEmpty ? 0 : this._Prefix.Length;
            this.SelectionLength = this.Text.Length - this.SelectionStart - (this._SuffixEmpty ? 0 : this._Suffix.Length);
        }

        /// <summary>   Length parameter. </summary>
        /// <remarks>   David, 2020-04-08. </remarks>
        /// <param name="snfs"> The snfs. </param>
        /// <returns>   An int. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        private static int LengthParameter( string snfs )
        {
            try
            {
                return snfs.Length == 1 ? 0 : Convert.ToInt16( snfs.Substring( 1 ) );
            }
            catch ( Exception )
            {
                return 0;
            }
        }

        #endregion Methods

        #region " Events "

        /// <summary>   Event handler. Called by NumberBox for enter events. </summary>
        /// <remarks>   David, 2020-04-08. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void NumberBox_Enter( object sender, EventArgs e )
        {
            this.SelectEditableRegion();
        }

        /// <summary>   Event handler. Called by NumberBox for mouse double click events. </summary>
        /// <remarks>   David, 2020-04-08. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void NumberBox_MouseDoubleClick( object sender, MouseEventArgs e )
        {
            this.SelectEditableRegion();
        }

        /// <summary>   Event handler. Called by NumberBox for leave events. </summary>
        /// <remarks>   David, 2020-04-08. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void NumberBox_Leave( object sender, EventArgs e )
        {
            this.LeaveBox();
        }

        /// <summary>   Event handler. Called by NumberBox for key down events. </summary>
        /// <remarks>   David, 2020-04-08. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Key event information. </param>
        private void NumberBox_KeyDown( object sender, KeyEventArgs e )
        {
            // ReSharper disable once SwitchStatementMissingSomeEnumCasesNoDefault
            switch ( e.KeyCode )
            {
                case Keys.Delete:
                    if ( !this._PrefixEmpty && this.SelectionStart < this._Prefix.Length )
                    {
                        e.Handled = true;
                    }

                    if ( !this._SuffixEmpty && this.SelectionStart > this.Text.Length - this._Suffix.Length - 1 )
                    {
                        e.Handled = true;
                    }

                    this.SelectionLength = 0;
                    break;
                case Keys.Back:
                    if ( this.SelectionLength > 0 )
                    {
                        this.Text = this.Text.Substring( 0, this.SelectionStart ) + this.Text.Substring( this.SelectionStart + this.SelectionLength );
                        this.SelectionStart = (!this._PrefixEmpty) ? this._Prefix.Length : 0;
                        this.SelectionLength = 0;
                        e.Handled = true;
                    }
                    else if ( this.SelectionLength == 0 )
                    {
                        if ( !this._PrefixEmpty && this.SelectionStart > this._Prefix.Length )
                        {
                            var pos = this.SelectionStart - 1;
                            this.Text = this.Text.Substring( 0, this.SelectionStart - 1 ) + this.Text.Substring( this.SelectionStart );
                            this.SelectionStart = pos;
                            e.Handled = true;
                        }
                        else if ( this._PrefixEmpty && this.SelectionStart > 0 )
                        {
                            var pos = this.SelectionStart - 1;
                            this.Text = this.Text.Substring( 0, this.SelectionStart - 1 ) + this.Text.Substring( this.SelectionStart );
                            this.SelectionStart = pos;
                            e.Handled = true;
                        }
                    }
                    break;
                case Keys.Left:
                    if ( !this._PrefixEmpty && this.SelectionStart < this._Prefix.Length + 1 )
                    {
                        e.Handled = true;
                    }

                    this.SelectionLength = 0;
                    break;
                case Keys.Right:
                    if ( !this._SuffixEmpty && this.SelectionStart > this.Text.Length - this._Suffix.Length - 1 )
                    {
                        e.Handled = true;
                    }

                    this.SelectionLength = 0;
                    break;
                case Keys.End:
                    this.SelectionStart = this.Text.Length - (this._SuffixEmpty ? 0 : this._Suffix.Length);
                    e.Handled = true;
                    this.SelectionLength = 0;
                    break;
                case Keys.Home:
                    this.SelectionStart = this._PrefixEmpty ? 0 : this._Prefix.Length;
                    e.Handled = true;
                    this.SelectionLength = 0;
                    break;
            }
        }

        /// <summary>   Event handler. Called by NumberBox for key press events. </summary>
        /// <remarks>   David, 2020-04-08. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Key press event information. </param>
        private void NumberBox_KeyPress( object sender, KeyPressEventArgs e )
        {
            if ( e.Handled )
            {
                return;
            }

            if ( string.IsNullOrEmpty( this.Text ) || this.SelectionLength == this.Text.Length )
            {
                this.Text = this._Prefix + this._Suffix;
                this.SelectionStart = !this._PrefixEmpty ? this._Prefix.Length : 0;
            }

            if ( !this._SuffixEmpty && !this.Text.EndsWith( this._Suffix ) )
            {
                this.Text += this._Suffix;
                this.SelectionStart = this.Text.Length - this._Suffix.Length;
            }

            // insert key into temp string to see if it fits the pattern
            var text = this.SelectionLength > 0
                ? this.Text.Substring( 0, this.SelectionStart ) + e.KeyChar + this.Text.Substring( this.SelectionStart + this.SelectionLength )
                : this.Text.Insert( this.SelectionStart, e.KeyChar.ToString() );

            var regex = new Regex( this._Pattern, RegexOptions.None );
            // if doesn't match pattern then reject the key by returnning e.Handled = true
            e.Handled = !regex.IsMatch( text );
        }

        #endregion Events
    }

}
