
#pragma warning disable IDE1006 // Naming Styles
namespace isr.Core.Controls.My
#pragma warning restore IDE1006 // Naming Styles
{
    /// <summary>   my library. </summary>
    /// <remarks>   David, 2020-03-18. </remarks>
    internal class MyLibrary
    {
        public const string AssemblyTitle = "ISR Core Controls C# Library";
        public const string AssemblyDescription = "ISR Core Controls C# Library";
        public const string AssemblyProduct = "isr.Core.Controls.cs";
        public const string AssemblyConfiguration = "Yet to be tested";
    }
}
