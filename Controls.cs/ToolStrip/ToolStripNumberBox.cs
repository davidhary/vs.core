using System;
using System.Diagnostics;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace isr.Core.Controls
{
    /// <summary> Tool strip number box. </summary>
    /// <license> (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    /// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    /// NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    /// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    /// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    /// SOFTWARE.</para> </license>
    /// <history date="2020-04-08" by="David" revision=""> Created. </history>
    [ToolStripItemDesignerAvailability(ToolStripItemDesignerAvailability.ToolStrip)]
    public class ToolStripNumberBox : ToolStripControlHost, IBindableComponent
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> Call the base constructor passing in a NumberBox instance. </remarks>
        public ToolStripNumberBox() : base(new Controls.NumberBox())
        {
        }

        /// <summary>
        /// Releases the unmanaged resources used by the
        /// <see cref="T:System.Windows.Forms.ToolStripControlHost" /> and optionally releases the
        /// managed resources.
        /// </summary>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (!IsDisposed && disposing)
                {
                    RemoveLeaveEventHandler(Leave);
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        #endregion

        #region " NUMBER BOX "

        /// <summary> Gets the numeric up down control. </summary>
        /// <value> The numeric up down control. </value>
        public Controls.NumberBox NumberBoxControl
        {
            get
            {
                return Control as Controls.NumberBox;
            }
        }

        /// <summary> Standard Numeric Format String D,E,F,N,X, B for binary. </summary>
        /// <value> The Standard numeric format string </value>
        [Category("NumberBox"), Description("Standard Numeric Format String D,E,F,N,X, B for binary")]
        public string Snfs
        {
            get => this.NumberBoxContro.Snfs;
            set => this.NumberBoxContro.Snfs = value;
        }

        /// <summary>   Prefix for X and B formats. </summary>
        /// <value> The prefix. </value>
        [Category("NumberBox"), Description("Prefix for X and B formats")]
        public string Prefix
        {
            get => this.NumberBoxContro.Prefix;
            set => this.NumberBoxContro.Prefix = value;
        }

        /// <summary>   Suffix for X and B formats. </summary>
        /// <value> The suffix. </value>
        [Category("NumberBox"), Description("Suffix for X and B formats")]
        public string Suffix
        {
            get => this.NumberBoxContro.Suffix;
            set => this.NumberBoxContro.Suffix = value;
        }

        /// <summary>   Gets/sets Max Value...double. </summary>
        /// <value> The maximum value. </value>
        [Category("NumberBox"), Description("gets/sets Max Value...double")]
        public double MaxValue
        {
            get => this.NumberBoxContro.MaxValue;
            set => this.NumberBoxContro.MaxValue = value;
        }


        /// <summary>   Gets/sets Min Value...double. </summary>
        /// <value> The minimum value. </value>
        [Category("NumberBox"), Description("gets/sets Min Value...double")]
        public double MinValue
        {
            get => this.NumberBoxContro.MinValue;
            set => this.NumberBoxContro.MinValue = value;
        }

        /// <summary>   Gets/sets Value As Double. </summary>
        /// <value> The value as double. </value>
        [Category("NumberBox"), Description("gets/sets Value As Double"), Browsable(true)]
        public double ValueAsDouble
        {
            get => this.NumberBoxContro.ValueAsDouble;
            set => this.NumberBoxContro.ValueAsDouble = value;
        }

        /// <summary>   Gets/sets Value As Float. </summary>
        /// <value> The value as float. </value>
        [Category("NumberBox"), Description("gets/sets Value As Float"), Browsable(true)]
        public float ValueAsFloat
        {
            get => this.NumberBoxContro.ValueAsFloat;
            set => this.NumberBoxContro.ValueAsFloat = value;
        }

        /// <summary>   Gets/sets Value As Byte. </summary>
        /// <value> The value as byte. </value>
        [Category("NumberBox"), Description("gets/sets Value As Byte"), Browsable(true)]
        public byte ValueAsByte
        {
            get => this.NumberBoxContro.ValueAsByte;
            set => this.NumberBoxContro.ValueAsByte = value;
        }

        /// <summary>   Gets/sets Value As SByte. </summary>
        /// <value> The value as s byte. </value>
        [Category("NumberBox"), Description("gets/sets Value As SByte"), Browsable(true)]
        public sbyte ValueAsSByte
        {
            get => this.NumberBoxContro.ValueAsSByte;
            set => this.NumberBoxContro.ValueAsSByte = value;
        }

        /// <summary>   Gets/sets Value As UInt64. </summary>
        /// <value> The value as u int 64. </value>
        [Category("NumberBox"), Description("gets/sets Value As UInt64"), Browsable(true)]
        public ulong ValueAsUInt64
        {
            get => this.NumberBoxContro.ValueAsUInt64;
            set => this.NumberBoxContro.ValueAsUInt64 = value;
        }

        /// <summary>   Gets/sets Value As UInt32. </summary>
        /// <value> The value as u int 32. </value>
        [Category("NumberBox"), Description("gets/sets Value As UInt32"), Browsable(true)]
        public uint ValueAsUInt32
        {
            get => this.NumberBoxContro.ValueAsUInt32;
            set => this.NumberBoxContro.ValueAsUInt32 = value;
        }

        /// <summary>   Gets/sets Value As UInt16. </summary>
        /// <value> The value as u int 16. </value>
        [Category("NumberBox"), Description("gets/sets Value As UInt16"), Browsable(true)]
        public ushort ValueAsUInt16
        {
            get => this.NumberBoxContro.ValueAsUInt16;
            set => this.NumberBoxContro.ValueAsUInt16 = value;
        }

        /// <summary>   Gets/sets Value As Int64. </summary>
        /// <value> The value as int 64. </value>
        [Category("NumberBox"), Description("gets/sets Value As Int64"), Browsable(true)]
        public long ValueAsInt64
        {
            get => this.NumberBoxContro.ValueAsInt64;
            set => this.NumberBoxContro.ValueAsInt64 = value;
        }

        /// <summary>   Gets/sets Value As Int32. </summary>
        /// <value> The value as int 32. </value>
        [Category("NumberBox"), Description("gets/sets Value As Int32"), Browsable(true)]
        public int ValueAsInt32
        {
            get => this.NumberBoxContro.ValueAsInt32;
            set => this.NumberBoxContro.ValueAsInt32 = value;
        }

        /// <summary>   Gets/sets Value As Int16. </summary>
        /// <value> The value as int 16. </value>
        [Category("NumberBox"), Description("gets/sets Value As Int16"), Browsable(true)]
        public short ValueAsInt16
        {
            get => this.NumberBoxContro.ValueAsInt16;
            set => this.NumberBoxContro.ValueAsInt16 = value;
        }

        /// <summary> Subscribes events from the hosted control. </summary>
        /// <remarks> Subscribe the control events to expose. </remarks>
        /// <param name="control"> The control from which to subscribe events. </param>
        protected override void OnSubscribeControlEvents(Control control)
        {
            if (control is object)
            {

                // Call the base so the base events are connected.
                base.OnSubscribeControlEvents(control);

                // Cast the control to a NumberBox control.
                Controls.NumberBox containedControl = control as Controls.NumberBox;
                if (containedControl is object)
                {
                    // Add the event.
                    containedControl.Leave += this.OnLeave;
                }
            }
        }

        /// <summary> Unsubscribe events from the hosted control. </summary>
        /// <param name="control"> The control from which to unsubscribe events. </param>
        protected override void OnUnsubscribeControlEvents(Control control)
        {

            // Call the base method so the basic events are unsubscribed.
            base.OnUnsubscribeControlEvents(control);

            // Cast the control to a NumberBox control.
            Controls.NumberBox containedControl = control as Controls.NumberBox;
            if (containedControl is object)
            {
                // Remove the event.
                containedControl.Leave -= this.OnLeave;
            }
        }

        /// <summary> Event queue for all listeners interested in Leave events. </summary>
        public event EventHandler<EventArgs> Leave;

        /// <summary> Removes event handler. </summary>
        /// <param name="value"> The handler. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Exception is published")]
        private void RemoveLeaveEventHandler(EventHandler<EventArgs> value)
        {
            foreach (Delegate d in value is null ? (new Delegate[] { }) : value.GetInvocationList())
            {
                try
                {
                    Leave -= (EventHandler<EventArgs>)d;
                }
                catch (Exception ex)
                {
                    Debug.Assert(!Debugger.IsAttached, ex.ToFullBlownString());
                }
            }
        }

        /// <summary> Raises the checked changed event. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information to send to registered event handlers. </param>
        private void OnLeave(object sender, EventArgs e)
        {
            var evt = Leave;
            evt?.Invoke(this, e);
        }

        #endregion

        #region BINDING

        private BindingContext _Context = null;

        /// <summary>
        /// Gets or sets the collection of currency managers for the
        /// <see cref="T:System.Windows.Forms.IBindableComponent" />.
        /// </summary>
        /// <value>
        /// The collection of <see cref="T:System.Windows.Forms.BindingManagerBase" /> objects for this
        /// <see cref="T:System.Windows.Forms.IBindableComponent" />.
        /// </value>
        public BindingContext BindingContext
        {
            get
            {
                if (_Context is null)
                    _Context = new BindingContext();
                return _Context;
            }

            set
            {
                _Context = value;
            }
        }

        private ControlBindingsCollection _Bindings;

        /// <summary>
        /// Gets the collection of data-binding objects for this
        /// <see cref="T:System.Windows.Forms.IBindableComponent" />.
        /// </summary>
        /// <value>
        /// The <see cref="T:System.Windows.Forms.ControlBindingsCollection" /> for this
        /// <see cref="T:System.Windows.Forms.IBindableComponent" />.
        /// </value>
        public ControlBindingsCollection DataBindings
        {
            get
            {
                if (_Bindings is null)
                    _Bindings = new ControlBindingsCollection(this);
                return _Bindings;
            }
        }

        #endregion

    }
}
