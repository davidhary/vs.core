/// <summary> The application's main window. </summary>
/// <remarks> David, 2021-03-12. </remarks>
using System.Drawing;
using System.Windows.Forms;

namespace isr.Core.Tester
{
    /// <summary>   The application's main window. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    public partial class MainForm : Form
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Windows.Forms.Form" /> class.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        public MainForm()
        {
            this.InitializeComponent();
        }
        /// <summary>   The offset. </summary>

        private Point _Offset;
        /// <summary>   True if is top panel dragged, false if not. </summary>
        private bool _IsTopPanelDragged = false;

        /// <summary>   Top panel mouse down. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void TopPanel_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this._IsTopPanelDragged = true;
                var pointStartPosition = this.PointToScreen(new Point(e.X, e.Y));
                this._Offset = new Point()
                {
                    X = this.Location.X - pointStartPosition.X,
                    Y = this.Location.Y - pointStartPosition.Y
                };
            }
            else
            {
                this._IsTopPanelDragged = false;
            }
        }

        /// <summary>   Top panel mouse move. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void TopPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if ( this._IsTopPanelDragged )
            {
                var newPoint = this._TopPanel.PointToScreen(new Point(e.X, e.Y));
                newPoint.Offset( this._Offset );
                this.Location = newPoint;
            }
        }

        /// <summary>   Top panel mouse up. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void TopPanel_MouseUp(object sender, MouseEventArgs e)
        {
            this._IsTopPanelDragged = false;
        }

        /// <summary>   Window text label mouse down. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void WindowTextLabel_MouseDown(object sender, MouseEventArgs e)
        {
            this.TopPanel_MouseDown(sender, e);
        }

        /// <summary>   Window text label mouse move. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void WindowTextLabel_MouseMove(object sender, MouseEventArgs e)
        {
            this.TopPanel_MouseMove(sender, e);
        }

        /// <summary>   Window text label mouse up. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void WindowTextLabel_MouseUp(object sender, MouseEventArgs e)
        {
            this.TopPanel_MouseUp(sender, e);
        }

        /// <summary>   Closes button click. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void CloseButton_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }

        /// <summary>   Minimum button click. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void MinButton_Click(object sender, System.EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        /// <summary>   Opens blue form shaped button click. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void OpenBlueFormShapedButton_Click(object sender, System.EventArgs e)
        {
            (new BlueForm() as BlueForm)?.Show();
        }

        /// <summary>   Opens dark form shaped button click. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void OpenDarkFormShapedButton_Click(object sender, System.EventArgs e)
        {
            (new BlackForm() as BlackForm)?.Show();
        }

        /// <summary>   Opens dashboard shaped button click. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void OpenDashboardShapedButton_Click(object sender, System.EventArgs e)
        {
            (new DashboardForm() as DashboardForm)?.Show();
        }

        /// <summary>   Exit button click. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void ExitButton_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }
    }
}
