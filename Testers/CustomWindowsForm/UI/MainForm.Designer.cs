using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using isr.Core.Controls;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Tester
{
    public partial class MainForm
    {
        /// <summary>
    /// Required designer variable.
    /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && components is object)
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        /// <summary>
        /// Required method for Designer support - do not modify the contents of this method with the
        /// code editor.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        private void InitializeComponent()
        {
            this.@__TopPanel = new System.Windows.Forms.Panel();
            this.@__WindowTextLabel = new System.Windows.Forms.Label();
            this.@__MinButton = new isr.Core.Controls.ZopeButton();
            this.@__CloseButton = new isr.Core.Controls.ZopeButton();
            this._RightPanel = new System.Windows.Forms.Panel();
            this._LeftPanel = new System.Windows.Forms.Panel();
            this._BottomPanel = new System.Windows.Forms.Panel();
            this.@__ExitButton = new isr.Core.Controls.ZopeButton();
            this.@__OpenDashboardShapedButton = new isr.Core.Controls.ShapedButton();
            this.@__OpenDarkFormShapedButton = new isr.Core.Controls.ShapedButton();
            this.@__OpenBlueFormShapedButton = new isr.Core.Controls.ShapedButton();
            this.@__TopPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // __TopPanel
            // 
            this.@__TopPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(100)))), ((int)(((byte)(60)))));
            this.@__TopPanel.Controls.Add(this.@__WindowTextLabel);
            this.@__TopPanel.Controls.Add(this.@__MinButton);
            this.@__TopPanel.Controls.Add(this.@__CloseButton);
            this.@__TopPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.@__TopPanel.Location = new System.Drawing.Point(0, 0);
            this.@__TopPanel.Name = "__TopPanel";
            this.@__TopPanel.Size = new System.Drawing.Size(355, 52);
            this.@__TopPanel.TabIndex = 0;
            this.@__TopPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.TopPanel_MouseDown);
            this.@__TopPanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.TopPanel_MouseMove);
            this.@__TopPanel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.TopPanel_MouseUp);
            // 
            // __WindowTextLabel
            // 
            this.@__WindowTextLabel.AutoSize = true;
            this.@__WindowTextLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.@__WindowTextLabel.ForeColor = System.Drawing.Color.White;
            this.@__WindowTextLabel.Location = new System.Drawing.Point(21, 14);
            this.@__WindowTextLabel.Name = "__WindowTextLabel";
            this.@__WindowTextLabel.Size = new System.Drawing.Size(84, 20);
            this.@__WindowTextLabel.TabIndex = 4;
            this.@__WindowTextLabel.Text = "Main Form";
            this.@__WindowTextLabel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.WindowTextLabel_MouseDown);
            this.@__WindowTextLabel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.WindowTextLabel_MouseMove);
            this.@__WindowTextLabel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.WindowTextLabel_MouseUp);
            // 
            // __MinButton
            // 
            this.@__MinButton.BackColorActive = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(100)))), ((int)(((byte)(60)))));
            this.@__MinButton.BackColorMouseClick = System.Drawing.Color.FromArgb(((int)(((byte)(160)))), ((int)(((byte)(180)))), ((int)(((byte)(200)))));
            this.@__MinButton.BackColorMouseHover = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(80)))), ((int)(((byte)(40)))));
            this.@__MinButton.BackColorNormal = System.Drawing.Color.Teal;
            this.@__MinButton.BorderColor = System.Drawing.Color.Transparent;
            this.@__MinButton.BorderColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.@__MinButton.BorderColorNormal = System.Drawing.Color.Transparent;
            this.@__MinButton.BorderWidth = 2;
            this.@__MinButton.DisplayText = "_";
            this.@__MinButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.@__MinButton.Font = new System.Drawing.Font("Microsoft YaHei UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.@__MinButton.ForeColor = System.Drawing.Color.White;
            this.@__MinButton.Location = new System.Drawing.Point(289, 3);
            this.@__MinButton.MouseColorsEnabled = true;
            this.@__MinButton.Name = "__MinButton";
            this.@__MinButton.Size = new System.Drawing.Size(31, 24);
            this.@__MinButton.TabIndex = 3;
            this.@__MinButton.Text = "_";
            this.@__MinButton.TextLocationLeft = 6;
            this.@__MinButton.TextLocationTop = -20;
            this.@__MinButton.UseVisualStyleBackColor = true;
            this.@__MinButton.Click += new System.EventHandler(this.MinButton_Click);
            // 
            // __CloseButton
            // 
            this.@__CloseButton.BackColorActive = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(100)))), ((int)(((byte)(60)))));
            this.@__CloseButton.BackColorMouseClick = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(150)))), ((int)(((byte)(220)))));
            this.@__CloseButton.BackColorMouseHover = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(80)))), ((int)(((byte)(40)))));
            this.@__CloseButton.BackColorNormal = System.Drawing.Color.Teal;
            this.@__CloseButton.BorderColor = System.Drawing.Color.Transparent;
            this.@__CloseButton.BorderColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.@__CloseButton.BorderColorNormal = System.Drawing.Color.Transparent;
            this.@__CloseButton.BorderWidth = 2;
            this.@__CloseButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.@__CloseButton.DisplayText = "X";
            this.@__CloseButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.@__CloseButton.Font = new System.Drawing.Font("Microsoft YaHei UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.@__CloseButton.ForeColor = System.Drawing.Color.White;
            this.@__CloseButton.Location = new System.Drawing.Point(320, 3);
            this.@__CloseButton.MouseColorsEnabled = true;
            this.@__CloseButton.Name = "__CloseButton";
            this.@__CloseButton.Size = new System.Drawing.Size(31, 24);
            this.@__CloseButton.TabIndex = 2;
            this.@__CloseButton.Text = "X";
            this.@__CloseButton.TextLocationLeft = 6;
            this.@__CloseButton.TextLocationTop = -1;
            this.@__CloseButton.UseVisualStyleBackColor = true;
            this.@__CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // _RightPanel
            // 
            this._RightPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(100)))), ((int)(((byte)(60)))));
            this._RightPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this._RightPanel.Location = new System.Drawing.Point(343, 52);
            this._RightPanel.Name = "_RightPanel";
            this._RightPanel.Size = new System.Drawing.Size(12, 375);
            this._RightPanel.TabIndex = 1;
            // 
            // _LeftPanel
            // 
            this._LeftPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(100)))), ((int)(((byte)(60)))));
            this._LeftPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this._LeftPanel.Location = new System.Drawing.Point(0, 52);
            this._LeftPanel.Name = "_LeftPanel";
            this._LeftPanel.Size = new System.Drawing.Size(12, 375);
            this._LeftPanel.TabIndex = 2;
            // 
            // _BottomPanel
            // 
            this._BottomPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(100)))), ((int)(((byte)(60)))));
            this._BottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._BottomPanel.Location = new System.Drawing.Point(12, 415);
            this._BottomPanel.Name = "_BottomPanel";
            this._BottomPanel.Size = new System.Drawing.Size(331, 12);
            this._BottomPanel.TabIndex = 3;
            // 
            // __ExitButton
            // 
            this.@__ExitButton.BackColorActive = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(100)))), ((int)(((byte)(60)))));
            this.@__ExitButton.BackColorMouseClick = System.Drawing.Color.Green;
            this.@__ExitButton.BackColorMouseHover = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.@__ExitButton.BackColorNormal = System.Drawing.Color.Teal;
            this.@__ExitButton.BorderColor = System.Drawing.Color.Transparent;
            this.@__ExitButton.BorderColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.@__ExitButton.BorderColorNormal = System.Drawing.Color.Transparent;
            this.@__ExitButton.BorderWidth = 2;
            this.@__ExitButton.DisplayText = "Exit";
            this.@__ExitButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.@__ExitButton.Font = new System.Drawing.Font("Microsoft YaHei UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.@__ExitButton.ForeColor = System.Drawing.Color.White;
            this.@__ExitButton.Location = new System.Drawing.Point(227, 367);
            this.@__ExitButton.MouseColorsEnabled = true;
            this.@__ExitButton.Name = "__ExitButton";
            this.@__ExitButton.Size = new System.Drawing.Size(93, 29);
            this.@__ExitButton.TabIndex = 7;
            this.@__ExitButton.Text = "Exit";
            this.@__ExitButton.TextLocationLeft = 26;
            this.@__ExitButton.TextLocationTop = 3;
            this.@__ExitButton.UseVisualStyleBackColor = true;
            this.@__ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // __OpenDashboardShapedButton
            // 
            this.@__OpenDashboardShapedButton.BackColor = System.Drawing.Color.Transparent;
            this.@__OpenDashboardShapedButton.BackEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(20)))), ((int)(((byte)(40)))));
            this.@__OpenDashboardShapedButton.BackEndColorMouseClick = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(100)))), ((int)(((byte)(60)))));
            this.@__OpenDashboardShapedButton.BackEndColorMouseHover = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(40)))), ((int)(((byte)(60)))));
            this.@__OpenDashboardShapedButton.BackEndColorNormal = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(20)))), ((int)(((byte)(40)))));
            this.@__OpenDashboardShapedButton.BackStartColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(40)))), ((int)(((byte)(120)))));
            this.@__OpenDashboardShapedButton.BackStartColorMouseClick = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(40)))));
            this.@__OpenDashboardShapedButton.BackStartColorMouseHover = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(100)))), ((int)(((byte)(180)))));
            this.@__OpenDashboardShapedButton.BackStartColorNormal = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(40)))), ((int)(((byte)(120)))));
            this.@__OpenDashboardShapedButton.BorderColor = System.Drawing.Color.Transparent;
            this.@__OpenDashboardShapedButton.BorderColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.@__OpenDashboardShapedButton.BorderColorNormal = System.Drawing.Color.Transparent;
            this.@__OpenDashboardShapedButton.BorderWidth = 2;
            this.@__OpenDashboardShapedButton.ButtonShape = isr.Core.Controls.ButtonShape.RoundRect;
            this.@__OpenDashboardShapedButton.ButtonText = "Dashboard UI Form";
            this.@__OpenDashboardShapedButton.FlatAppearance.BorderSize = 0;
            this.@__OpenDashboardShapedButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.@__OpenDashboardShapedButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.@__OpenDashboardShapedButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.@__OpenDashboardShapedButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.@__OpenDashboardShapedButton.ForeColor = System.Drawing.Color.White;
            this.@__OpenDashboardShapedButton.GradientAngle = 90;
            this.@__OpenDashboardShapedButton.Location = new System.Drawing.Point(25, 269);
            this.@__OpenDashboardShapedButton.Name = "__OpenDashboardShapedButton";
            this.@__OpenDashboardShapedButton.OpacityEnd = 250;
            this.@__OpenDashboardShapedButton.OpacityStart = 250;
            this.@__OpenDashboardShapedButton.RadiusPercent = 25;
            this.@__OpenDashboardShapedButton.ShowButtonText = true;
            this.@__OpenDashboardShapedButton.Size = new System.Drawing.Size(312, 75);
            this.@__OpenDashboardShapedButton.TabIndex = 6;
            this.@__OpenDashboardShapedButton.Text = "Dashboard UI Form";
            this.@__OpenDashboardShapedButton.TextLocation = new System.Drawing.Point(90, 25);
            this.@__OpenDashboardShapedButton.UseVisualStyleBackColor = false;
            this.@__OpenDashboardShapedButton.Click += new System.EventHandler(this.OpenDashboardShapedButton_Click);
            // 
            // __OpenDarkFormShapedButton
            // 
            this.@__OpenDarkFormShapedButton.BackColor = System.Drawing.Color.Transparent;
            this.@__OpenDarkFormShapedButton.BackEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.@__OpenDarkFormShapedButton.BackEndColorMouseClick = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(100)))), ((int)(((byte)(60)))));
            this.@__OpenDarkFormShapedButton.BackEndColorMouseHover = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.@__OpenDarkFormShapedButton.BackEndColorNormal = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.@__OpenDarkFormShapedButton.BackStartColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.@__OpenDarkFormShapedButton.BackStartColorMouseClick = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(40)))));
            this.@__OpenDarkFormShapedButton.BackStartColorMouseHover = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(150)))), ((int)(((byte)(150)))));
            this.@__OpenDarkFormShapedButton.BackStartColorNormal = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.@__OpenDarkFormShapedButton.BorderColor = System.Drawing.Color.Transparent;
            this.@__OpenDarkFormShapedButton.BorderColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.@__OpenDarkFormShapedButton.BorderColorNormal = System.Drawing.Color.Transparent;
            this.@__OpenDarkFormShapedButton.BorderWidth = 2;
            this.@__OpenDarkFormShapedButton.ButtonShape = isr.Core.Controls.ButtonShape.RoundRect;
            this.@__OpenDarkFormShapedButton.ButtonText = "Dark Custom Form";
            this.@__OpenDarkFormShapedButton.FlatAppearance.BorderSize = 0;
            this.@__OpenDarkFormShapedButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.@__OpenDarkFormShapedButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.@__OpenDarkFormShapedButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.@__OpenDarkFormShapedButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.@__OpenDarkFormShapedButton.ForeColor = System.Drawing.Color.White;
            this.@__OpenDarkFormShapedButton.GradientAngle = 90;
            this.@__OpenDarkFormShapedButton.Location = new System.Drawing.Point(25, 172);
            this.@__OpenDarkFormShapedButton.Name = "__OpenDarkFormShapedButton";
            this.@__OpenDarkFormShapedButton.OpacityEnd = 250;
            this.@__OpenDarkFormShapedButton.OpacityStart = 250;
            this.@__OpenDarkFormShapedButton.RadiusPercent = 25;
            this.@__OpenDarkFormShapedButton.ShowButtonText = true;
            this.@__OpenDarkFormShapedButton.Size = new System.Drawing.Size(312, 75);
            this.@__OpenDarkFormShapedButton.TabIndex = 5;
            this.@__OpenDarkFormShapedButton.Text = "shapedButton2";
            this.@__OpenDarkFormShapedButton.TextLocation = new System.Drawing.Point(90, 25);
            this.@__OpenDarkFormShapedButton.UseVisualStyleBackColor = false;
            this.@__OpenDarkFormShapedButton.Click += new System.EventHandler(this.OpenDarkFormShapedButton_Click);
            // 
            // __OpenBlueFormShapedButton
            // 
            this.@__OpenBlueFormShapedButton.BackColor = System.Drawing.Color.Transparent;
            this.@__OpenBlueFormShapedButton.BackEndColor = System.Drawing.Color.MidnightBlue;
            this.@__OpenBlueFormShapedButton.BackEndColorMouseClick = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(100)))), ((int)(((byte)(60)))));
            this.@__OpenBlueFormShapedButton.BackEndColorMouseHover = System.Drawing.Color.DarkSlateGray;
            this.@__OpenBlueFormShapedButton.BackEndColorNormal = System.Drawing.Color.MidnightBlue;
            this.@__OpenBlueFormShapedButton.BackStartColor = System.Drawing.Color.DodgerBlue;
            this.@__OpenBlueFormShapedButton.BackStartColorMouseClick = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(40)))));
            this.@__OpenBlueFormShapedButton.BackStartColorMouseHover = System.Drawing.Color.Turquoise;
            this.@__OpenBlueFormShapedButton.BackStartColorNormal = System.Drawing.Color.DodgerBlue;
            this.@__OpenBlueFormShapedButton.BorderColor = System.Drawing.Color.Transparent;
            this.@__OpenBlueFormShapedButton.BorderColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.@__OpenBlueFormShapedButton.BorderColorNormal = System.Drawing.Color.Transparent;
            this.@__OpenBlueFormShapedButton.BorderWidth = 2;
            this.@__OpenBlueFormShapedButton.ButtonShape = isr.Core.Controls.ButtonShape.RoundRect;
            this.@__OpenBlueFormShapedButton.ButtonText = "Simple Blue Form";
            this.@__OpenBlueFormShapedButton.FlatAppearance.BorderSize = 0;
            this.@__OpenBlueFormShapedButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.@__OpenBlueFormShapedButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.@__OpenBlueFormShapedButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.@__OpenBlueFormShapedButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.@__OpenBlueFormShapedButton.ForeColor = System.Drawing.Color.White;
            this.@__OpenBlueFormShapedButton.GradientAngle = 90;
            this.@__OpenBlueFormShapedButton.Location = new System.Drawing.Point(25, 77);
            this.@__OpenBlueFormShapedButton.Name = "__OpenBlueFormShapedButton";
            this.@__OpenBlueFormShapedButton.OpacityEnd = 250;
            this.@__OpenBlueFormShapedButton.OpacityStart = 250;
            this.@__OpenBlueFormShapedButton.RadiusPercent = 25;
            this.@__OpenBlueFormShapedButton.ShowButtonText = true;
            this.@__OpenBlueFormShapedButton.Size = new System.Drawing.Size(312, 75);
            this.@__OpenBlueFormShapedButton.TabIndex = 4;
            this.@__OpenBlueFormShapedButton.Text = "Simple Blue Form";
            this.@__OpenBlueFormShapedButton.TextLocation = new System.Drawing.Point(90, 26);
            this.@__OpenBlueFormShapedButton.UseVisualStyleBackColor = false;
            this.@__OpenBlueFormShapedButton.Click += new System.EventHandler(this.OpenBlueFormShapedButton_Click);
            // 
            // MainForm
            // 
            this.AcceptButton = this.@__ExitButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(180)))), ((int)(((byte)(100)))));
            this.CancelButton = this.@__CloseButton;
            this.ClientSize = new System.Drawing.Size(355, 427);
            this.Controls.Add(this.@__ExitButton);
            this.Controls.Add(this.@__OpenDashboardShapedButton);
            this.Controls.Add(this.@__OpenDarkFormShapedButton);
            this.Controls.Add(this.@__OpenBlueFormShapedButton);
            this.Controls.Add(this._BottomPanel);
            this.Controls.Add(this._LeftPanel);
            this.Controls.Add(this._RightPanel);
            this.Controls.Add(this.@__TopPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Main Form";
            this.@__TopPanel.ResumeLayout(false);
            this.@__TopPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        /// <summary>   The top panel. </summary>
        private Panel __TopPanel;

        private Panel _TopPanel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __TopPanel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__TopPanel != null)
                {
                    __TopPanel.MouseDown -= TopPanel_MouseDown;
                    __TopPanel.MouseMove -= TopPanel_MouseMove;
                    __TopPanel.MouseUp -= TopPanel_MouseUp;
                }

                __TopPanel = value;
                if (__TopPanel != null)
                {
                    __TopPanel.MouseDown += TopPanel_MouseDown;
                    __TopPanel.MouseMove += TopPanel_MouseMove;
                    __TopPanel.MouseUp += TopPanel_MouseUp;
                }
            }
        }

        private ZopeButton __MinButton;

        private ZopeButton _MinButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __MinButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__MinButton != null)
                {
                    __MinButton.Click -= MinButton_Click;
                }

                __MinButton = value;
                if (__MinButton != null)
                {
                    __MinButton.Click += MinButton_Click;
                }
            }
        }

        private ZopeButton __CloseButton;

        private ZopeButton _CloseButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __CloseButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__CloseButton != null)
                {
                    __CloseButton.Click -= CloseButton_Click;
                }

                __CloseButton = value;
                if (__CloseButton != null)
                {
                    __CloseButton.Click += CloseButton_Click;
                }
            }
        }

        private Label __WindowTextLabel;

        private Label _WindowTextLabel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __WindowTextLabel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__WindowTextLabel != null)
                {
                    __WindowTextLabel.MouseDown -= WindowTextLabel_MouseDown;
                    __WindowTextLabel.MouseMove -= WindowTextLabel_MouseMove;
                    __WindowTextLabel.MouseUp -= WindowTextLabel_MouseUp;
                }

                __WindowTextLabel = value;
                if (__WindowTextLabel != null)
                {
                    __WindowTextLabel.MouseDown += WindowTextLabel_MouseDown;
                    __WindowTextLabel.MouseMove += WindowTextLabel_MouseMove;
                    __WindowTextLabel.MouseUp += WindowTextLabel_MouseUp;
                }
            }
        }

        private Panel _RightPanel;
        private Panel _LeftPanel;
        private Panel _BottomPanel;
        private ShapedButton __OpenBlueFormShapedButton;

        private ShapedButton _OpenBlueFormShapedButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __OpenBlueFormShapedButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__OpenBlueFormShapedButton != null)
                {
                    __OpenBlueFormShapedButton.Click -= OpenBlueFormShapedButton_Click;
                }

                __OpenBlueFormShapedButton = value;
                if (__OpenBlueFormShapedButton != null)
                {
                    __OpenBlueFormShapedButton.Click += OpenBlueFormShapedButton_Click;
                }
            }
        }

        private ShapedButton __OpenDarkFormShapedButton;

        private ShapedButton _OpenDarkFormShapedButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __OpenDarkFormShapedButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__OpenDarkFormShapedButton != null)
                {
                    __OpenDarkFormShapedButton.Click -= OpenDarkFormShapedButton_Click;
                }

                __OpenDarkFormShapedButton = value;
                if (__OpenDarkFormShapedButton != null)
                {
                    __OpenDarkFormShapedButton.Click += OpenDarkFormShapedButton_Click;
                }
            }
        }

        private ShapedButton __OpenDashboardShapedButton;

        private ShapedButton _OpenDashboardShapedButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __OpenDashboardShapedButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__OpenDashboardShapedButton != null)
                {
                    __OpenDashboardShapedButton.Click -= OpenDashboardShapedButton_Click;
                }

                __OpenDashboardShapedButton = value;
                if (__OpenDashboardShapedButton != null)
                {
                    __OpenDashboardShapedButton.Click += OpenDashboardShapedButton_Click;
                }
            }
        }

        private ZopeButton __ExitButton;

        private ZopeButton _ExitButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ExitButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ExitButton != null)
                {
                    __ExitButton.Click -= ExitButton_Click;
                }

                __ExitButton = value;
                if (__ExitButton != null)
                {
                    __ExitButton.Click += ExitButton_Click;
                }
            }
        }
    }
}
