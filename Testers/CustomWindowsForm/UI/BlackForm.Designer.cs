using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using isr.Core.Controls;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Tester
{
    public partial class BlackForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && components is object)
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            __TopBorderPanel = new Panel();
            __TopBorderPanel.MouseDown += new MouseEventHandler(TopBorderPanel_MouseDown);
            __TopBorderPanel.MouseMove += new MouseEventHandler(TopBorderPanel_MouseMove);
            __TopBorderPanel.MouseUp += new MouseEventHandler(TopBorderPanel_MouseUp);
            __RightBorderPanel = new Panel();
            __RightBorderPanel.MouseDown += new MouseEventHandler(RightPanel_MouseDown);
            __RightBorderPanel.MouseMove += new MouseEventHandler(RightPanel_MouseMove);
            __RightBorderPanel.MouseUp += new MouseEventHandler(RightPanel_MouseUp);
            __LeftBorderPanel = new Panel();
            __LeftBorderPanel.MouseDown += new MouseEventHandler(LeftPanel_MouseDown);
            __LeftBorderPanel.MouseMove += new MouseEventHandler(LeftPanel_MouseMove);
            __LeftBorderPanel.MouseUp += new MouseEventHandler(LeftPanel_MouseUp);
            __BottomBorderPanel = new Panel();
            __BottomBorderPanel.MouseDown += new MouseEventHandler(BottomPanel_MouseDown);
            __BottomBorderPanel.MouseMove += new MouseEventHandler(BottomPanel_MouseMove);
            __BottomBorderPanel.MouseUp += new MouseEventHandler(BottomPanel_MouseUp);
            __TopPanel = new Panel();
            __TopPanel.MouseDown += new MouseEventHandler(TopPanel_MouseDown);
            __TopPanel.MouseMove += new MouseEventHandler(TopPanel_MouseMove);
            __TopPanel.MouseUp += new MouseEventHandler(TopPanel_MouseUp);
            __MinButton = new ZopeButton();
            __MinButton.Click += new System.EventHandler(MinButton_Click);
            __MaxButton = new MinMaxButton();
            __MaxButton.Click += new System.EventHandler(MaxButton_Click);
            __WindowTextLabel = new Label();
            __WindowTextLabel.MouseDown += new MouseEventHandler(WindowTextLabel_MouseDown);
            __WindowTextLabel.MouseMove += new MouseEventHandler(WindowTextLabel_MouseMove);
            __WindowTextLabel.MouseUp += new MouseEventHandler(WindowTextLabel_MouseUp);
            __CloseButton = new ZopeButton();
            __CloseButton.Click += new System.EventHandler(CloseButton_Click);
            _MenuStrip = new ZopeMenuStrip();
            _FileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _NewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _OpenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _ToolStripSeparator1 = new ToolStripSeparator();
            _SaveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _SaveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _ToolStripSeparator2 = new ToolStripSeparator();
            _CloseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _CloseAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _ToolStripSeparator3 = new ToolStripSeparator();
            _PrintToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _PrintPreviewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _ToolStripSeparator4 = new ToolStripSeparator();
            _CloseToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            _EditToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _CutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _CopyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _PasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _ToolStripSeparator5 = new ToolStripSeparator();
            _UnduToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _RedoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _ToolStripSeparator6 = new ToolStripSeparator();
            _FindToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _ReplaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _SelectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _HelpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _HelpContentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _OnlineHelpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _AboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _BuyNowShapedButton = new ShapedButton();
            __BottomBorderRightPanel = new Panel();
            __BottomBorderRightPanel.MouseDown += new MouseEventHandler(RightBottomPanel_1_MouseDown);
            __BottomBorderRightPanel.MouseMove += new MouseEventHandler(RightBottomPanel_1_MouseMove);
            __BottomBorderRightPanel.MouseUp += new MouseEventHandler(RightBottomPanel_1_MouseUp);
            _BottomPanel = new Panel();
            _TimesShapedButton = new ShapedButton();
            _TestShapedButton = new ShapedButton();
            _AddShapedButton = new ShapedButton();
            _ToolsShapedButton = new ShapedButton();
            _ToolTip = new ToolTip(components);
            __RightBorderBottomPanel = new Panel();
            __RightBorderBottomPanel.MouseDown += new MouseEventHandler(RightBottomPanel_2_MouseDown);
            __RightBorderBottomPanel.MouseMove += new MouseEventHandler(RightBottomPanel_2_MouseMove);
            __RightBorderBottomPanel.MouseUp += new MouseEventHandler(RightBottomPanel_2_MouseUp);
            __BottomBorderLeftPanel = new Panel();
            __BottomBorderLeftPanel.MouseDown += new MouseEventHandler(LeftBottomPanel_1_MouseDown);
            __BottomBorderLeftPanel.MouseMove += new MouseEventHandler(LeftBottomPanel_1_MouseMove);
            __BottomBorderLeftPanel.MouseUp += new MouseEventHandler(LeftBottomPanel_1_MouseUp);
            __LeftBorderBottomPanel = new Panel();
            __LeftBorderBottomPanel.MouseDown += new MouseEventHandler(LeftBottomPanel_2_MouseDown);
            __LeftBorderBottomPanel.MouseMove += new MouseEventHandler(LeftBottomPanel_2_MouseMove);
            __LeftBorderBottomPanel.MouseUp += new MouseEventHandler(LeftBottomPanel_2_MouseUp);
            __RightBorderTopPanel = new Panel();
            __RightBorderTopPanel.MouseDown += new MouseEventHandler(RightTopPanel_1_MouseDown);
            __RightBorderTopPanel.MouseMove += new MouseEventHandler(RightTopPanel_1_MouseMove);
            __RightBorderTopPanel.MouseUp += new MouseEventHandler(RightTopPanel_1_MouseUp);
            __TopBorderRightPanel = new Panel();
            __TopBorderRightPanel.MouseDown += new MouseEventHandler(RightTopPanel_2_MouseDown);
            __TopBorderRightPanel.MouseMove += new MouseEventHandler(RightTopPanel_2_MouseMove);
            __TopBorderRightPanel.MouseUp += new MouseEventHandler(RightTopPanel_2_MouseUp);
            __TopBorderLeftPanel = new Panel();
            __TopBorderLeftPanel.MouseDown += new MouseEventHandler(LeftTopPanel_1_MouseDown);
            __TopBorderLeftPanel.MouseMove += new MouseEventHandler(LeftTopPanel_1_MouseMove);
            __TopBorderLeftPanel.MouseUp += new MouseEventHandler(LeftTopPanel_1_MouseUp);
            __LeftBorderTopPanel = new Panel();
            __LeftBorderTopPanel.MouseDown += new MouseEventHandler(LeftTopPanel_2_MouseDown);
            __LeftBorderTopPanel.MouseMove += new MouseEventHandler(LeftTopPanel_2_MouseMove);
            __LeftBorderTopPanel.MouseUp += new MouseEventHandler(LeftTopPanel_2_MouseUp);
            _LeftPanel = new Panel();
            __HelpButton = new ZopeButton();
            __HelpButton.Click += new System.EventHandler(Help_button_Click);
            __RunButton = new ZopeButton();
            __RunButton.Click += new System.EventHandler(Run_button_Click);
            __ViewButton = new ZopeButton();
            __ViewButton.Click += new System.EventHandler(View_button_Click);
            __EditButton = new ZopeButton();
            __EditButton.Click += new System.EventHandler(Edit_button_Click);
            __FileButton = new ZopeButton();
            __FileButton.Click += new System.EventHandler(File_button_Click);
            _SeparatorPanel = new Panel();
            _InformationLabel = new Label();
            __TopPanel.SuspendLayout();
            _MenuStrip.SuspendLayout();
            _BottomPanel.SuspendLayout();
            _LeftPanel.SuspendLayout();
            SuspendLayout();
            // 
            // _TopBorderPanel
            // 
            __TopBorderPanel.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            __TopBorderPanel.BackColor = Color.Black;
            __TopBorderPanel.Cursor = Cursors.SizeNS;
            __TopBorderPanel.Location = new Point(20, 0);
            __TopBorderPanel.Name = "__TopBorderPanel";
            __TopBorderPanel.Size = new Size(690, 2);
            __TopBorderPanel.TabIndex = 0;
            // 
            // _RightBorderPanel
            // 
            __RightBorderPanel.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Right;
            __RightBorderPanel.BackColor = Color.Black;
            __RightBorderPanel.Cursor = Cursors.SizeWE;
            __RightBorderPanel.Location = new Point(728, 22);
            __RightBorderPanel.Name = "__RightBorderPanel";
            __RightBorderPanel.Size = new Size(2, 430);
            __RightBorderPanel.TabIndex = 1;
            // 
            // _LeftBorderPanel
            // 
            __LeftBorderPanel.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left;
            __LeftBorderPanel.BackColor = Color.Black;
            __LeftBorderPanel.Cursor = Cursors.SizeWE;
            __LeftBorderPanel.Location = new Point(0, 20);
            __LeftBorderPanel.Name = "__LeftBorderPanel";
            __LeftBorderPanel.Size = new Size(2, 430);
            __LeftBorderPanel.TabIndex = 2;
            // 
            // _BottomBorderPanel
            // 
            __BottomBorderPanel.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            __BottomBorderPanel.BackColor = Color.Black;
            __BottomBorderPanel.Cursor = Cursors.SizeNS;
            __BottomBorderPanel.Location = new Point(15, 471);
            __BottomBorderPanel.Name = "__BottomBorderPanel";
            __BottomBorderPanel.Size = new Size(692, 2);
            __BottomBorderPanel.TabIndex = 3;
            // 
            // _TopPanel
            // 
            __TopPanel.BackColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)));
            __TopPanel.Controls.Add(__MinButton);
            __TopPanel.Controls.Add(__MaxButton);
            __TopPanel.Controls.Add(__WindowTextLabel);
            __TopPanel.Controls.Add(__CloseButton);
            __TopPanel.Controls.Add(_MenuStrip);
            __TopPanel.Dock = DockStyle.Top;
            __TopPanel.Location = new Point(0, 0);
            __TopPanel.Name = "__TopPanel";
            __TopPanel.Size = new Size(730, 76);
            __TopPanel.TabIndex = 4;
            // 
            // _MinButton
            // 
            __MinButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            __MinButton.BackColorActive = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)));
            __MinButton.DisplayText = "_";
            __MinButton.FlatStyle = FlatStyle.Flat;
            __MinButton.Font = new Font("Microsoft YaHei UI", 20.25f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            __MinButton.ForeColor = Color.White;
            __MinButton.Location = new Point(632, 6);
            __MinButton.BackColorMouseClick = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(60)), Conversions.ToInteger(Conversions.ToByte(60)), Conversions.ToInteger(Conversions.ToByte(160)));
            __MinButton.MouseColorsEnabled = true;
            __MinButton.BackColorMouseHover = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(50)), Conversions.ToInteger(Conversions.ToByte(50)), Conversions.ToInteger(Conversions.ToByte(50)));
            __MinButton.Name = "__MinButton";
            __MinButton.Size = new Size(31, 24);
            __MinButton.TabIndex = 4;
            __MinButton.Text = "_";
            __MinButton.TextLocationLeft = 6;
            __MinButton.TextLocationTop = -20;
            _ToolTip.SetToolTip(__MinButton, "Minimize");
            __MinButton.UseVisualStyleBackColor = true;
            // 
            // _MaxButton
            // 
            __MaxButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            __MaxButton.BusyBackColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)));
            __MaxButton.CustomFormState = CustomFormState.Normal;
            __MaxButton.DisplayText = "_";
            __MaxButton.FlatStyle = FlatStyle.Flat;
            __MaxButton.Font = new Font("Segoe UI", 9.75f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            __MaxButton.ForeColor = Color.White;
            __MaxButton.Location = new Point(663, 6);
            __MaxButton.MouseClickColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(60)), Conversions.ToInteger(Conversions.ToByte(60)), Conversions.ToInteger(Conversions.ToByte(160)));
            __MaxButton.MouseHoverColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(50)), Conversions.ToInteger(Conversions.ToByte(50)), Conversions.ToInteger(Conversions.ToByte(50)));
            __MaxButton.Name = "__MaxButton";
            __MaxButton.Size = new Size(31, 24);
            __MaxButton.TabIndex = 2;
            __MaxButton.Text = "minMaxButton1";
            __MaxButton.TextLocation = new Point(8, 6);
            _ToolTip.SetToolTip(__MaxButton, "Maximize");
            __MaxButton.UseVisualStyleBackColor = true;
            // 
            // _WindowTextLabel
            // 
            __WindowTextLabel.AutoSize = true;
            __WindowTextLabel.Font = new Font("Microsoft Sans Serif", 26.25f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            __WindowTextLabel.ForeColor = Color.White;
            __WindowTextLabel.Location = new Point(28, 21);
            __WindowTextLabel.Name = "__WindowTextLabel";
            __WindowTextLabel.Size = new Size(134, 39);
            __WindowTextLabel.TabIndex = 1;
            __WindowTextLabel.Text = "My App";
            // 
            // _CloseButton
            // 
            __CloseButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            __CloseButton.BackColorActive = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)));
            __CloseButton.DisplayText = "X";
            __CloseButton.FlatStyle = FlatStyle.Flat;
            __CloseButton.Font = new Font("Microsoft YaHei UI", 12.0f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            __CloseButton.ForeColor = Color.White;
            __CloseButton.Location = new Point(694, 6);
            __CloseButton.BackColorMouseClick = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(60)), Conversions.ToInteger(Conversions.ToByte(60)), Conversions.ToInteger(Conversions.ToByte(160)));
            __CloseButton.MouseColorsEnabled = true;
            __CloseButton.BackColorMouseHover = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(50)), Conversions.ToInteger(Conversions.ToByte(50)), Conversions.ToInteger(Conversions.ToByte(50)));
            __CloseButton.Name = "__CloseButton";
            __CloseButton.Size = new Size(31, 24);
            __CloseButton.TabIndex = 0;
            __CloseButton.Text = "X";
            __CloseButton.TextLocationLeft = 6;
            __CloseButton.TextLocationTop = 1;
            _ToolTip.SetToolTip(__CloseButton, "Close");
            __CloseButton.UseVisualStyleBackColor = true;
            // 
            // _MenuStrip
            // 
            _MenuStrip.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            _MenuStrip.Dock = DockStyle.None;
            _MenuStrip.Font = new Font("Segoe UI", 11.25f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _MenuStrip.Items.AddRange(new ToolStripItem[] { _FileToolStripMenuItem, _EditToolStripMenuItem, _HelpToolStripMenuItem });
            _MenuStrip.Location = new Point(421, 32);
            _MenuStrip.Name = "_MenuStrip";
            _MenuStrip.Size = new Size(200, 28);
            _MenuStrip.TabIndex = 19;
            _MenuStrip.Text = "menuStripZ1";
            // 
            // _FileToolStripMenuItem
            // 
            _FileToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { _NewToolStripMenuItem, _OpenToolStripMenuItem, _ToolStripSeparator1, _SaveToolStripMenuItem, _SaveAsToolStripMenuItem, _ToolStripSeparator2, _CloseToolStripMenuItem, _CloseAllToolStripMenuItem, _ToolStripSeparator3, _PrintToolStripMenuItem, _PrintPreviewToolStripMenuItem, _ToolStripSeparator4, _CloseToolStripMenuItem1 });
            _FileToolStripMenuItem.ForeColor = Color.White;
            _FileToolStripMenuItem.Name = "_FileToolStripMenuItem";
            _FileToolStripMenuItem.Size = new Size(60, 24);
            _FileToolStripMenuItem.Text = "  File  ";
            // 
            // _NewToolStripMenuItem
            // 
            _NewToolStripMenuItem.ForeColor = Color.White;
            _NewToolStripMenuItem.Name = "_NewToolStripMenuItem";
            _NewToolStripMenuItem.Size = new Size(240, 24);
            _NewToolStripMenuItem.Text = "New                                 ";
            // 
            // _OpenToolStripMenuItem
            // 
            _OpenToolStripMenuItem.ForeColor = Color.White;
            _OpenToolStripMenuItem.Name = "_OpenToolStripMenuItem";
            _OpenToolStripMenuItem.Size = new Size(240, 24);
            _OpenToolStripMenuItem.Text = "Open";
            // 
            // _ToolStripSeparator1
            // 
            _ToolStripSeparator1.Name = "_ToolStripSeparator1";
            _ToolStripSeparator1.Size = new Size(237, 6);
            // 
            // _SaveToolStripMenuItem
            // 
            _SaveToolStripMenuItem.ForeColor = Color.White;
            _SaveToolStripMenuItem.Name = "_SaveToolStripMenuItem";
            _SaveToolStripMenuItem.Size = new Size(240, 24);
            _SaveToolStripMenuItem.Text = "Save";
            // 
            // _SaveAsToolStripMenuItem
            // 
            _SaveAsToolStripMenuItem.ForeColor = Color.White;
            _SaveAsToolStripMenuItem.Name = "_SaveAsToolStripMenuItem";
            _SaveAsToolStripMenuItem.Size = new Size(240, 24);
            _SaveAsToolStripMenuItem.Text = "Save As";
            // 
            // _ToolStripSeparator2
            // 
            _ToolStripSeparator2.Name = "_ToolStripSeparator2";
            _ToolStripSeparator2.Size = new Size(237, 6);
            // 
            // _CloseToolStripMenuItem
            // 
            _CloseToolStripMenuItem.ForeColor = Color.White;
            _CloseToolStripMenuItem.Name = "_CloseToolStripMenuItem";
            _CloseToolStripMenuItem.Size = new Size(240, 24);
            _CloseToolStripMenuItem.Text = "Close";
            // 
            // _CloseAllToolStripMenuItem
            // 
            _CloseAllToolStripMenuItem.ForeColor = Color.White;
            _CloseAllToolStripMenuItem.Name = "_CloseAllToolStripMenuItem";
            _CloseAllToolStripMenuItem.Size = new Size(240, 24);
            _CloseAllToolStripMenuItem.Text = "Close All";
            // 
            // _ToolStripSeparator3
            // 
            _ToolStripSeparator3.Name = "_ToolStripSeparator3";
            _ToolStripSeparator3.Size = new Size(237, 6);
            // 
            // _PrintToolStripMenuItem
            // 
            _PrintToolStripMenuItem.ForeColor = Color.White;
            _PrintToolStripMenuItem.Name = "_PrintToolStripMenuItem";
            _PrintToolStripMenuItem.Size = new Size(240, 24);
            _PrintToolStripMenuItem.Text = "Print";
            // 
            // _PrintPreviewToolStripMenuItem
            // 
            _PrintPreviewToolStripMenuItem.ForeColor = Color.White;
            _PrintPreviewToolStripMenuItem.Name = "_PrintPreviewToolStripMenuItem";
            _PrintPreviewToolStripMenuItem.Size = new Size(240, 24);
            _PrintPreviewToolStripMenuItem.Text = "Print Preview";
            // 
            // _ToolStripSeparator4
            // 
            _ToolStripSeparator4.Name = "_ToolStripSeparator4";
            _ToolStripSeparator4.Size = new Size(237, 6);
            // 
            // _CloseToolStripMenuItem1
            // 
            _CloseToolStripMenuItem1.ForeColor = Color.White;
            _CloseToolStripMenuItem1.Name = "_CloseToolStripMenuItem1";
            _CloseToolStripMenuItem1.Size = new Size(240, 24);
            _CloseToolStripMenuItem1.Text = "Close";
            // 
            // _EditToolStripMenuItem
            // 
            _EditToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { _CutToolStripMenuItem, _CopyToolStripMenuItem, _PasteToolStripMenuItem, _ToolStripSeparator5, _UnduToolStripMenuItem, _RedoToolStripMenuItem, _ToolStripSeparator6, _FindToolStripMenuItem, _ReplaceToolStripMenuItem, _SelectAllToolStripMenuItem });
            _EditToolStripMenuItem.ForeColor = Color.White;
            _EditToolStripMenuItem.Name = "_EditToolStripMenuItem";
            _EditToolStripMenuItem.Size = new Size(63, 24);
            _EditToolStripMenuItem.Text = "  Edit  ";
            // 
            // _CutToolStripMenuItem
            // 
            _CutToolStripMenuItem.ForeColor = Color.White;
            _CutToolStripMenuItem.Name = "_CutToolStripMenuItem";
            _CutToolStripMenuItem.Size = new Size(216, 24);
            _CutToolStripMenuItem.Text = "Cut                             ";
            // 
            // _CopyToolStripMenuItem
            // 
            _CopyToolStripMenuItem.ForeColor = Color.White;
            _CopyToolStripMenuItem.Name = "_CopyToolStripMenuItem";
            _CopyToolStripMenuItem.Size = new Size(216, 24);
            _CopyToolStripMenuItem.Text = "Copy";
            // 
            // _PasteToolStripMenuItem
            // 
            _PasteToolStripMenuItem.ForeColor = Color.White;
            _PasteToolStripMenuItem.Name = "_PasteToolStripMenuItem";
            _PasteToolStripMenuItem.Size = new Size(216, 24);
            _PasteToolStripMenuItem.Text = "Paste";
            // 
            // _ToolStripSeparator5
            // 
            _ToolStripSeparator5.Name = "_ToolStripSeparator5";
            _ToolStripSeparator5.Size = new Size(213, 6);
            // 
            // _UnduToolStripMenuItem
            // 
            _UnduToolStripMenuItem.ForeColor = Color.White;
            _UnduToolStripMenuItem.Name = "_UnduToolStripMenuItem";
            _UnduToolStripMenuItem.Size = new Size(216, 24);
            _UnduToolStripMenuItem.Text = "Undo";
            // 
            // _RedoToolStripMenuItem
            // 
            _RedoToolStripMenuItem.ForeColor = Color.White;
            _RedoToolStripMenuItem.Name = "_RedoToolStripMenuItem";
            _RedoToolStripMenuItem.Size = new Size(216, 24);
            _RedoToolStripMenuItem.Text = "Redo";
            // 
            // _ToolStripSeparator6
            // 
            _ToolStripSeparator6.Name = "_ToolStripSeparator6";
            _ToolStripSeparator6.Size = new Size(213, 6);
            // 
            // _FindToolStripMenuItem
            // 
            _FindToolStripMenuItem.ForeColor = Color.White;
            _FindToolStripMenuItem.Name = "_FindToolStripMenuItem";
            _FindToolStripMenuItem.Size = new Size(216, 24);
            _FindToolStripMenuItem.Text = "Find";
            // 
            // _ReplaceToolStripMenuItem
            // 
            _ReplaceToolStripMenuItem.ForeColor = Color.White;
            _ReplaceToolStripMenuItem.Name = "_ReplaceToolStripMenuItem";
            _ReplaceToolStripMenuItem.Size = new Size(216, 24);
            _ReplaceToolStripMenuItem.Text = "Replace";
            // 
            // _SelectAllToolStripMenuItem
            // 
            _SelectAllToolStripMenuItem.ForeColor = Color.White;
            _SelectAllToolStripMenuItem.Name = "_SelectAllToolStripMenuItem";
            _SelectAllToolStripMenuItem.Size = new Size(216, 24);
            _SelectAllToolStripMenuItem.Text = "Select All";
            // 
            // _HelpToolStripMenuItem
            // 
            _HelpToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { _HelpContentsToolStripMenuItem, _OnlineHelpToolStripMenuItem, _AboutToolStripMenuItem });
            _HelpToolStripMenuItem.ForeColor = Color.White;
            _HelpToolStripMenuItem.Name = "_HelpToolStripMenuItem";
            _HelpToolStripMenuItem.Size = new Size(69, 24);
            _HelpToolStripMenuItem.Text = "  Help  ";
            // 
            // _HelpContentsToolStripMenuItem
            // 
            _HelpContentsToolStripMenuItem.ForeColor = Color.White;
            _HelpContentsToolStripMenuItem.Name = "_HelpContentsToolStripMenuItem";
            _HelpContentsToolStripMenuItem.Size = new Size(172, 24);
            _HelpContentsToolStripMenuItem.Text = "Help Contents";
            // 
            // _OnlineHelpToolStripMenuItem
            // 
            _OnlineHelpToolStripMenuItem.ForeColor = Color.White;
            _OnlineHelpToolStripMenuItem.Name = "_OnlineHelpToolStripMenuItem";
            _OnlineHelpToolStripMenuItem.Size = new Size(172, 24);
            _OnlineHelpToolStripMenuItem.Text = "Online Help";
            // 
            // _AboutToolStripMenuItem
            // 
            _AboutToolStripMenuItem.ForeColor = Color.White;
            _AboutToolStripMenuItem.Name = "_AboutToolStripMenuItem";
            _AboutToolStripMenuItem.Size = new Size(172, 24);
            _AboutToolStripMenuItem.Text = "About";
            // 
            // _BuyNowShapedButton
            // 
            _BuyNowShapedButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            _BuyNowShapedButton.BackColor = Color.Transparent;
            _BuyNowShapedButton.BorderColor = Color.Transparent;
            _BuyNowShapedButton.BorderWidth = 2;
            _BuyNowShapedButton.ButtonShape = ButtonShape.RoundRect;
            _BuyNowShapedButton.ButtonText = "Buy Now";
            _BuyNowShapedButton.BackEndColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(20)), Conversions.ToInteger(Conversions.ToByte(20)), Conversions.ToInteger(Conversions.ToByte(40)));
            _BuyNowShapedButton.OpacityEnd = 250;
            _BuyNowShapedButton.FlatAppearance.BorderSize = 0;
            _BuyNowShapedButton.FlatAppearance.MouseDownBackColor = Color.Transparent;
            _BuyNowShapedButton.FlatAppearance.MouseOverBackColor = Color.Transparent;
            _BuyNowShapedButton.FlatStyle = FlatStyle.Flat;
            _BuyNowShapedButton.Font = new Font("Microsoft Sans Serif", 12.0f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _BuyNowShapedButton.ForeColor = Color.White;
            _BuyNowShapedButton.GradientAngle = 90;
            _BuyNowShapedButton.Location = new Point(427, 10);
            _BuyNowShapedButton.BackStartColorMouseClick = Color.Black;
            _BuyNowShapedButton.BackEndColorMouseClick = Color.Black;
            _BuyNowShapedButton.BackStartColorMouseHover = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(10)), Conversions.ToInteger(Conversions.ToByte(10)), Conversions.ToInteger(Conversions.ToByte(30)));
            _BuyNowShapedButton.BackEndColorMouseHover = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(20)), Conversions.ToInteger(Conversions.ToByte(20)), Conversions.ToInteger(Conversions.ToByte(80)));
            _BuyNowShapedButton.Name = "_BuyNowShapedButton";
            _BuyNowShapedButton.ShowButtonText = true;
            _BuyNowShapedButton.Size = new Size(145, 54);
            _BuyNowShapedButton.BackStartColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(70)), Conversions.ToInteger(Conversions.ToByte(190)));
            _BuyNowShapedButton.OpacityStart = 250;
            _BuyNowShapedButton.TabIndex = 3;
            _BuyNowShapedButton.Text = "shapedButton2";
            _BuyNowShapedButton.TextLocation = new Point(40, 18);
            _BuyNowShapedButton.UseVisualStyleBackColor = false;
            // 
            // _BottomBorderRightPanel
            // 
            __BottomBorderRightPanel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            __BottomBorderRightPanel.BackColor = Color.Black;
            __BottomBorderRightPanel.Cursor = Cursors.SizeNWSE;
            __BottomBorderRightPanel.Location = new Point(710, 471);
            __BottomBorderRightPanel.Name = "__BottomBorderRightPanel";
            __BottomBorderRightPanel.Size = new Size(19, 2);
            __BottomBorderRightPanel.TabIndex = 5;
            // 
            // _BottomPanel
            // 
            _BottomPanel.BackColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)));
            _BottomPanel.Controls.Add(_TimesShapedButton);
            _BottomPanel.Controls.Add(_BuyNowShapedButton);
            _BottomPanel.Controls.Add(_TestShapedButton);
            _BottomPanel.Controls.Add(_AddShapedButton);
            _BottomPanel.Controls.Add(_ToolsShapedButton);
            _BottomPanel.Dock = DockStyle.Bottom;
            _BottomPanel.Location = new Point(0, 402);
            _BottomPanel.Name = "_BottomPanel";
            _BottomPanel.Size = new Size(730, 71);
            _BottomPanel.TabIndex = 8;
            // 
            // _TimesShapedButton
            // 
            _TimesShapedButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            _TimesShapedButton.BackColor = Color.Transparent;
            _TimesShapedButton.BorderColor = Color.Transparent;
            _TimesShapedButton.BorderWidth = 2;
            _TimesShapedButton.ButtonShape = ButtonShape.Circle;
            _TimesShapedButton.ButtonText = "X";
            _TimesShapedButton.BackEndColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)));
            _TimesShapedButton.OpacityEnd = 250;
            _TimesShapedButton.FlatAppearance.BorderSize = 0;
            _TimesShapedButton.FlatAppearance.MouseDownBackColor = Color.Transparent;
            _TimesShapedButton.FlatAppearance.MouseOverBackColor = Color.Transparent;
            _TimesShapedButton.FlatStyle = FlatStyle.Flat;
            _TimesShapedButton.Font = new Font("Microsoft Sans Serif", 15.75f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _TimesShapedButton.ForeColor = Color.White;
            _TimesShapedButton.GradientAngle = 90;
            _TimesShapedButton.Location = new Point(643, 8);
            _TimesShapedButton.BackStartColorMouseClick = Color.Black;
            _TimesShapedButton.BackEndColorMouseClick = Color.Black;
            _TimesShapedButton.BackStartColorMouseHover = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(150)), Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(0)));
            _TimesShapedButton.BackEndColorMouseHover = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(150)), Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(0)));
            _TimesShapedButton.Name = "_TimesShapedButton";
            _TimesShapedButton.ShowButtonText = true;
            _TimesShapedButton.Size = new Size(75, 55);
            _TimesShapedButton.BackStartColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)));
            _TimesShapedButton.OpacityStart = 250;
            _TimesShapedButton.TabIndex = 9;
            _TimesShapedButton.TextLocation = new Point(28, 16);
            _TimesShapedButton.UseVisualStyleBackColor = false;
            // 
            // _TestShapedButton
            // 
            _TestShapedButton.BackColor = Color.Transparent;
            _TestShapedButton.BorderColor = Color.Transparent;
            _TestShapedButton.BorderWidth = 2;
            _TestShapedButton.ButtonShape = ButtonShape.RoundRect;
            _TestShapedButton.ButtonText = "Test";
            _TestShapedButton.BackEndColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)));
            _TestShapedButton.OpacityEnd = 250;
            _TestShapedButton.FlatAppearance.BorderSize = 0;
            _TestShapedButton.FlatAppearance.MouseDownBackColor = Color.Transparent;
            _TestShapedButton.FlatAppearance.MouseOverBackColor = Color.Transparent;
            _TestShapedButton.FlatStyle = FlatStyle.Flat;
            _TestShapedButton.Font = new Font("Microsoft Sans Serif", 12.0f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _TestShapedButton.ForeColor = Color.White;
            _TestShapedButton.GradientAngle = 90;
            _TestShapedButton.Location = new Point(257, 10);
            _TestShapedButton.BackStartColorMouseClick = Color.Black;
            _TestShapedButton.BackEndColorMouseClick = Color.Black;
            _TestShapedButton.BackStartColorMouseHover = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(80)), Conversions.ToInteger(Conversions.ToByte(80)), Conversions.ToInteger(Conversions.ToByte(80)));
            _TestShapedButton.BackEndColorMouseHover = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(80)), Conversions.ToInteger(Conversions.ToByte(80)), Conversions.ToInteger(Conversions.ToByte(80)));
            _TestShapedButton.Name = "_TestShapedButton";
            _TestShapedButton.ShowButtonText = true;
            _TestShapedButton.Size = new Size(136, 55);
            _TestShapedButton.BackStartColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)));
            _TestShapedButton.OpacityStart = 250;
            _TestShapedButton.TabIndex = 8;
            _TestShapedButton.TextLocation = new Point(46, 18);
            _TestShapedButton.UseVisualStyleBackColor = false;
            // 
            // _AddShapedButton
            // 
            _AddShapedButton.BackColor = Color.Transparent;
            _AddShapedButton.BorderColor = Color.Transparent;
            _AddShapedButton.BorderWidth = 2;
            _AddShapedButton.ButtonShape = ButtonShape.Circle;
            _AddShapedButton.ButtonText = "+";
            _AddShapedButton.BackEndColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)));
            _AddShapedButton.OpacityEnd = 250;
            _AddShapedButton.FlatAppearance.BorderSize = 0;
            _AddShapedButton.FlatAppearance.MouseDownBackColor = Color.Transparent;
            _AddShapedButton.FlatAppearance.MouseOverBackColor = Color.Transparent;
            _AddShapedButton.FlatStyle = FlatStyle.Flat;
            _AddShapedButton.Font = new Font("Microsoft Sans Serif", 27.75f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _AddShapedButton.ForeColor = Color.White;
            _AddShapedButton.GradientAngle = 90;
            _AddShapedButton.Location = new Point(10, 10);
            _AddShapedButton.BackStartColorMouseClick = Color.Black;
            _AddShapedButton.BackEndColorMouseClick = Color.Black;
            _AddShapedButton.BackStartColorMouseHover = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(80)), Conversions.ToInteger(Conversions.ToByte(80)), Conversions.ToInteger(Conversions.ToByte(80)));
            _AddShapedButton.BackEndColorMouseHover = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(80)), Conversions.ToInteger(Conversions.ToByte(80)), Conversions.ToInteger(Conversions.ToByte(80)));
            _AddShapedButton.Name = "_AddShapedButton";
            _AddShapedButton.ShowButtonText = true;
            _AddShapedButton.Size = new Size(59, 55);
            _AddShapedButton.BackStartColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)));
            _AddShapedButton.OpacityStart = 250;
            _AddShapedButton.TabIndex = 6;
            _AddShapedButton.TextLocation = new Point(13, 6);
            _AddShapedButton.UseVisualStyleBackColor = false;
            // 
            // _ToolsShapedButton
            // 
            _ToolsShapedButton.BackColor = Color.Transparent;
            _ToolsShapedButton.BorderColor = Color.Transparent;
            _ToolsShapedButton.BorderWidth = 2;
            _ToolsShapedButton.ButtonShape = ButtonShape.RoundRect;
            _ToolsShapedButton.ButtonText = "Tools";
            _ToolsShapedButton.BackEndColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)));
            _ToolsShapedButton.OpacityEnd = 250;
            _ToolsShapedButton.FlatAppearance.BorderSize = 0;
            _ToolsShapedButton.FlatAppearance.MouseDownBackColor = Color.Transparent;
            _ToolsShapedButton.FlatAppearance.MouseOverBackColor = Color.Transparent;
            _ToolsShapedButton.FlatStyle = FlatStyle.Flat;
            _ToolsShapedButton.Font = new Font("Microsoft Sans Serif", 12.0f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _ToolsShapedButton.ForeColor = Color.White;
            _ToolsShapedButton.GradientAngle = 90;
            _ToolsShapedButton.Location = new Point(96, 10);
            _ToolsShapedButton.BackStartColorMouseClick = Color.Black;
            _ToolsShapedButton.BackEndColorMouseClick = Color.Black;
            _ToolsShapedButton.BackStartColorMouseHover = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(80)), Conversions.ToInteger(Conversions.ToByte(80)), Conversions.ToInteger(Conversions.ToByte(80)));
            _ToolsShapedButton.BackEndColorMouseHover = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(80)), Conversions.ToInteger(Conversions.ToByte(80)), Conversions.ToInteger(Conversions.ToByte(80)));
            _ToolsShapedButton.Name = "_ToolsShapedButton";
            _ToolsShapedButton.ShowButtonText = true;
            _ToolsShapedButton.Size = new Size(136, 55);
            _ToolsShapedButton.BackStartColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)));
            _ToolsShapedButton.OpacityStart = 250;
            _ToolsShapedButton.TabIndex = 7;
            _ToolsShapedButton.TextLocation = new Point(45, 18);
            _ToolsShapedButton.UseVisualStyleBackColor = false;
            // 
            // _RightBorderBottomPanel
            // 
            __RightBorderBottomPanel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            __RightBorderBottomPanel.BackColor = Color.Black;
            __RightBorderBottomPanel.Cursor = Cursors.SizeNWSE;
            __RightBorderBottomPanel.Location = new Point(728, 452);
            __RightBorderBottomPanel.Name = "__RightBorderBottomPanel";
            __RightBorderBottomPanel.Size = new Size(2, 19);
            __RightBorderBottomPanel.TabIndex = 9;
            // 
            // _BottomBorderLeftPanel
            // 
            __BottomBorderLeftPanel.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            __BottomBorderLeftPanel.BackColor = Color.Black;
            __BottomBorderLeftPanel.Cursor = Cursors.SizeNESW;
            __BottomBorderLeftPanel.Location = new Point(0, 471);
            __BottomBorderLeftPanel.Name = "__BottomBorderLeftPanel";
            __BottomBorderLeftPanel.Size = new Size(15, 2);
            __BottomBorderLeftPanel.TabIndex = 10;
            // 
            // _LeftBorderBottomPanel
            // 
            __LeftBorderBottomPanel.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            __LeftBorderBottomPanel.BackColor = Color.Black;
            __LeftBorderBottomPanel.Cursor = Cursors.SizeNESW;
            __LeftBorderBottomPanel.Location = new Point(0, 453);
            __LeftBorderBottomPanel.Name = "__LeftBorderBottomPanel";
            __LeftBorderBottomPanel.Size = new Size(2, 19);
            __LeftBorderBottomPanel.TabIndex = 11;
            // 
            // _RightBorderTopPanel
            // 
            __RightBorderTopPanel.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            __RightBorderTopPanel.BackColor = Color.Black;
            __RightBorderTopPanel.Cursor = Cursors.SizeNESW;
            __RightBorderTopPanel.Location = new Point(728, 2);
            __RightBorderTopPanel.Name = "__RightBorderTopPanel";
            __RightBorderTopPanel.Size = new Size(2, 20);
            __RightBorderTopPanel.TabIndex = 12;
            // 
            // _TopBorderRightPanel
            // 
            __TopBorderRightPanel.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            __TopBorderRightPanel.BackColor = Color.Black;
            __TopBorderRightPanel.Cursor = Cursors.SizeNESW;
            __TopBorderRightPanel.Location = new Point(710, 0);
            __TopBorderRightPanel.Name = "__TopBorderRightPanel";
            __TopBorderRightPanel.Size = new Size(20, 2);
            __TopBorderRightPanel.TabIndex = 13;
            // 
            // _TopBorderLeftPanel
            // 
            __TopBorderLeftPanel.BackColor = Color.Black;
            __TopBorderLeftPanel.Cursor = Cursors.SizeNWSE;
            __TopBorderLeftPanel.Location = new Point(0, 0);
            __TopBorderLeftPanel.Name = "__TopBorderLeftPanel";
            __TopBorderLeftPanel.Size = new Size(20, 2);
            __TopBorderLeftPanel.TabIndex = 14;
            // 
            // _LeftBorderTopPanel
            // 
            __LeftBorderTopPanel.BackColor = Color.Black;
            __LeftBorderTopPanel.Cursor = Cursors.SizeNWSE;
            __LeftBorderTopPanel.Location = new Point(0, 0);
            __LeftBorderTopPanel.Name = "__LeftBorderTopPanel";
            __LeftBorderTopPanel.Size = new Size(2, 20);
            __LeftBorderTopPanel.TabIndex = 15;
            // 
            // _LeftPanel
            // 
            _LeftPanel.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left;
            _LeftPanel.BackColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(60)), Conversions.ToInteger(Conversions.ToByte(60)), Conversions.ToInteger(Conversions.ToByte(60)));
            _LeftPanel.Controls.Add(__HelpButton);
            _LeftPanel.Controls.Add(__RunButton);
            _LeftPanel.Controls.Add(__ViewButton);
            _LeftPanel.Controls.Add(__EditButton);
            _LeftPanel.Controls.Add(__FileButton);
            _LeftPanel.Location = new Point(2, 77);
            _LeftPanel.Name = "_LeftPanel";
            _LeftPanel.Size = new Size(280, 325);
            _LeftPanel.TabIndex = 16;
            // 
            // _HelpButton
            // 
            __HelpButton.BackColorActive = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(60)), Conversions.ToInteger(Conversions.ToByte(60)), Conversions.ToInteger(Conversions.ToByte(60)));
            __HelpButton.DisplayText = "Help";
            __HelpButton.Dock = DockStyle.Top;
            __HelpButton.FlatStyle = FlatStyle.Flat;
            __HelpButton.Font = new Font("Microsoft YaHei UI", 12.0f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            __HelpButton.ForeColor = Color.White;
            __HelpButton.Location = new Point(0, 140);
            __HelpButton.BackColorMouseClick = Color.Black;
            __HelpButton.MouseColorsEnabled = true;
            __HelpButton.BackColorMouseHover = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)));
            __HelpButton.Name = "__HelpButton";
            __HelpButton.Size = new Size(280, 35);
            __HelpButton.TabIndex = 4;
            __HelpButton.Text = "Help";
            __HelpButton.TextLocationLeft = 110;
            __HelpButton.TextLocationTop = 6;
            __HelpButton.UseVisualStyleBackColor = true;
            // 
            // _RunButton
            // 
            __RunButton.BackColorActive = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(60)), Conversions.ToInteger(Conversions.ToByte(60)), Conversions.ToInteger(Conversions.ToByte(60)));
            __RunButton.DisplayText = "Run";
            __RunButton.Dock = DockStyle.Top;
            __RunButton.FlatStyle = FlatStyle.Flat;
            __RunButton.Font = new Font("Microsoft YaHei UI", 12.0f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            __RunButton.ForeColor = Color.White;
            __RunButton.Location = new Point(0, 105);
            __RunButton.BackColorMouseClick = Color.Black;
            __RunButton.MouseColorsEnabled = true;
            __RunButton.BackColorMouseHover = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)));
            __RunButton.Name = "__RunButton";
            __RunButton.Size = new Size(280, 35);
            __RunButton.TabIndex = 3;
            __RunButton.Text = "Run";
            __RunButton.TextLocationLeft = 110;
            __RunButton.TextLocationTop = 6;
            __RunButton.UseVisualStyleBackColor = true;
            // 
            // _ViewButton
            // 
            __ViewButton.BackColorActive = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(60)), Conversions.ToInteger(Conversions.ToByte(60)), Conversions.ToInteger(Conversions.ToByte(60)));
            __ViewButton.DisplayText = "View";
            __ViewButton.Dock = DockStyle.Top;
            __ViewButton.FlatStyle = FlatStyle.Flat;
            __ViewButton.Font = new Font("Microsoft YaHei UI", 12.0f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            __ViewButton.ForeColor = Color.White;
            __ViewButton.Location = new Point(0, 70);
            __ViewButton.BackColorMouseClick = Color.Black;
            __ViewButton.MouseColorsEnabled = true;
            __ViewButton.BackColorMouseHover = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)));
            __ViewButton.Name = "__ViewButton";
            __ViewButton.Size = new Size(280, 35);
            __ViewButton.TabIndex = 2;
            __ViewButton.Text = "View";
            __ViewButton.TextLocationLeft = 110;
            __ViewButton.TextLocationTop = 6;
            __ViewButton.UseVisualStyleBackColor = true;
            // 
            // _EditButton
            // 
            __EditButton.BackColorActive = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(60)), Conversions.ToInteger(Conversions.ToByte(60)), Conversions.ToInteger(Conversions.ToByte(60)));
            __EditButton.DisplayText = "Edit";
            __EditButton.Dock = DockStyle.Top;
            __EditButton.FlatStyle = FlatStyle.Flat;
            __EditButton.Font = new Font("Microsoft YaHei UI", 12.0f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            __EditButton.ForeColor = Color.White;
            __EditButton.Location = new Point(0, 35);
            __EditButton.BackColorMouseClick = Color.Black;
            __EditButton.MouseColorsEnabled = true;
            __EditButton.BackColorMouseHover = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)));
            __EditButton.Name = "__EditButton";
            __EditButton.Size = new Size(280, 35);
            __EditButton.TabIndex = 1;
            __EditButton.Text = "Edit";
            __EditButton.TextLocationLeft = 110;
            __EditButton.TextLocationTop = 6;
            __EditButton.UseVisualStyleBackColor = true;
            // 
            // _FileButton
            // 
            __FileButton.BackColorActive = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(60)), Conversions.ToInteger(Conversions.ToByte(60)), Conversions.ToInteger(Conversions.ToByte(60)));
            __FileButton.DisplayText = "File";
            __FileButton.Dock = DockStyle.Top;
            __FileButton.FlatStyle = FlatStyle.Flat;
            __FileButton.Font = new Font("Microsoft YaHei UI", 12.0f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            __FileButton.ForeColor = Color.White;
            __FileButton.Location = new Point(0, 0);
            __FileButton.BackColorMouseClick = Color.Black;
            __FileButton.MouseColorsEnabled = true;
            __FileButton.BackColorMouseHover = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)));
            __FileButton.Name = "__FileButton";
            __FileButton.Size = new Size(280, 35);
            __FileButton.TabIndex = 0;
            __FileButton.Text = "File";
            __FileButton.TextLocationLeft = 110;
            __FileButton.TextLocationTop = 6;
            __FileButton.UseVisualStyleBackColor = true;
            // 
            // _SeparatorPanel
            // 
            _SeparatorPanel.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left;
            _SeparatorPanel.BackColor = Color.Black;
            _SeparatorPanel.Location = new Point(282, 78);
            _SeparatorPanel.Name = "_SeparatorPanel";
            _SeparatorPanel.Size = new Size(2, 324);
            _SeparatorPanel.TabIndex = 17;
            // 
            // _InformationLabel
            // 
            _InformationLabel.AutoSize = true;
            _InformationLabel.Font = new Font("Microsoft Sans Serif", 14.25f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _InformationLabel.ForeColor = Color.White;
            _InformationLabel.Location = new Point(309, 204);
            _InformationLabel.Name = "_InformationLabel";
            _InformationLabel.Size = new Size(378, 24);
            _InformationLabel.TabIndex = 18;
            _InformationLabel.Text = "Resizable Black colored Custom Form in C#";
            // 
            // BlackForm
            // 
            AutoScaleDimensions = new SizeF(6.0f, 13.0f);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(60)), Conversions.ToInteger(Conversions.ToByte(60)), Conversions.ToInteger(Conversions.ToByte(60)));
            ClientSize = new Size(730, 473);
            Controls.Add(_InformationLabel);
            Controls.Add(_SeparatorPanel);
            Controls.Add(_LeftPanel);
            Controls.Add(__LeftBorderTopPanel);
            Controls.Add(__TopBorderLeftPanel);
            Controls.Add(__TopBorderRightPanel);
            Controls.Add(__RightBorderTopPanel);
            Controls.Add(__LeftBorderBottomPanel);
            Controls.Add(__BottomBorderLeftPanel);
            Controls.Add(__RightBorderBottomPanel);
            Controls.Add(__BottomBorderRightPanel);
            Controls.Add(__BottomBorderPanel);
            Controls.Add(__LeftBorderPanel);
            Controls.Add(__RightBorderPanel);
            Controls.Add(__TopBorderPanel);
            Controls.Add(__TopPanel);
            Controls.Add(_BottomPanel);
            FormBorderStyle = FormBorderStyle.None;
            MainMenuStrip = _MenuStrip;
            Name = "BlackForm";
            Text = "My App";
            __TopPanel.ResumeLayout(false);
            __TopPanel.PerformLayout();
            _MenuStrip.ResumeLayout(false);
            _MenuStrip.PerformLayout();
            _BottomPanel.ResumeLayout(false);
            _LeftPanel.ResumeLayout(false);
            ResumeLayout(false);
            PerformLayout();
        }

        private Panel __TopBorderPanel;

        private Panel _TopBorderPanel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __TopBorderPanel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__TopBorderPanel != null)
                {
                    __TopBorderPanel.MouseDown -= TopBorderPanel_MouseDown;
                    __TopBorderPanel.MouseMove -= TopBorderPanel_MouseMove;
                    __TopBorderPanel.MouseUp -= TopBorderPanel_MouseUp;
                }

                __TopBorderPanel = value;
                if (__TopBorderPanel != null)
                {
                    __TopBorderPanel.MouseDown += TopBorderPanel_MouseDown;
                    __TopBorderPanel.MouseMove += TopBorderPanel_MouseMove;
                    __TopBorderPanel.MouseUp += TopBorderPanel_MouseUp;
                }
            }
        }

        private Panel __RightBorderPanel;

        private Panel _RightBorderPanel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __RightBorderPanel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__RightBorderPanel != null)
                {
                    __RightBorderPanel.MouseDown -= RightPanel_MouseDown;
                    __RightBorderPanel.MouseMove -= RightPanel_MouseMove;
                    __RightBorderPanel.MouseUp -= RightPanel_MouseUp;
                }

                __RightBorderPanel = value;
                if (__RightBorderPanel != null)
                {
                    __RightBorderPanel.MouseDown += RightPanel_MouseDown;
                    __RightBorderPanel.MouseMove += RightPanel_MouseMove;
                    __RightBorderPanel.MouseUp += RightPanel_MouseUp;
                }
            }
        }

        private Panel __LeftBorderPanel;

        private Panel _LeftBorderPanel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __LeftBorderPanel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__LeftBorderPanel != null)
                {
                    __LeftBorderPanel.MouseDown -= LeftPanel_MouseDown;
                    __LeftBorderPanel.MouseMove -= LeftPanel_MouseMove;
                    __LeftBorderPanel.MouseUp -= LeftPanel_MouseUp;
                }

                __LeftBorderPanel = value;
                if (__LeftBorderPanel != null)
                {
                    __LeftBorderPanel.MouseDown += LeftPanel_MouseDown;
                    __LeftBorderPanel.MouseMove += LeftPanel_MouseMove;
                    __LeftBorderPanel.MouseUp += LeftPanel_MouseUp;
                }
            }
        }

        private Panel __BottomBorderPanel;

        private Panel _BottomBorderPanel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __BottomBorderPanel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__BottomBorderPanel != null)
                {
                    __BottomBorderPanel.MouseDown -= BottomPanel_MouseDown;
                    __BottomBorderPanel.MouseMove -= BottomPanel_MouseMove;
                    __BottomBorderPanel.MouseUp -= BottomPanel_MouseUp;
                }

                __BottomBorderPanel = value;
                if (__BottomBorderPanel != null)
                {
                    __BottomBorderPanel.MouseDown += BottomPanel_MouseDown;
                    __BottomBorderPanel.MouseMove += BottomPanel_MouseMove;
                    __BottomBorderPanel.MouseUp += BottomPanel_MouseUp;
                }
            }
        }

        private Panel __TopPanel;

        private Panel _TopPanel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __TopPanel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__TopPanel != null)
                {
                    __TopPanel.MouseDown -= TopPanel_MouseDown;
                    __TopPanel.MouseMove -= TopPanel_MouseMove;
                    __TopPanel.MouseUp -= TopPanel_MouseUp;
                }

                __TopPanel = value;
                if (__TopPanel != null)
                {
                    __TopPanel.MouseDown += TopPanel_MouseDown;
                    __TopPanel.MouseMove += TopPanel_MouseMove;
                    __TopPanel.MouseUp += TopPanel_MouseUp;
                }
            }
        }

        private ZopeButton __CloseButton;

        private ZopeButton _CloseButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __CloseButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__CloseButton != null)
                {
                    __CloseButton.Click -= CloseButton_Click;
                }

                __CloseButton = value;
                if (__CloseButton != null)
                {
                    __CloseButton.Click += CloseButton_Click;
                }
            }
        }

        private Panel __BottomBorderRightPanel;

        private Panel _BottomBorderRightPanel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __BottomBorderRightPanel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__BottomBorderRightPanel != null)
                {
                    __BottomBorderRightPanel.MouseDown -= RightBottomPanel_1_MouseDown;
                    __BottomBorderRightPanel.MouseMove -= RightBottomPanel_1_MouseMove;
                    __BottomBorderRightPanel.MouseUp -= RightBottomPanel_1_MouseUp;
                }

                __BottomBorderRightPanel = value;
                if (__BottomBorderRightPanel != null)
                {
                    __BottomBorderRightPanel.MouseDown += RightBottomPanel_1_MouseDown;
                    __BottomBorderRightPanel.MouseMove += RightBottomPanel_1_MouseMove;
                    __BottomBorderRightPanel.MouseUp += RightBottomPanel_1_MouseUp;
                }
            }
        }

        private ShapedButton _AddShapedButton;
        private Label __WindowTextLabel;

        private Label _WindowTextLabel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __WindowTextLabel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__WindowTextLabel != null)
                {
                    __WindowTextLabel.MouseDown -= WindowTextLabel_MouseDown;
                    __WindowTextLabel.MouseMove -= WindowTextLabel_MouseMove;
                    __WindowTextLabel.MouseUp -= WindowTextLabel_MouseUp;
                }

                __WindowTextLabel = value;
                if (__WindowTextLabel != null)
                {
                    __WindowTextLabel.MouseDown += WindowTextLabel_MouseDown;
                    __WindowTextLabel.MouseMove += WindowTextLabel_MouseMove;
                    __WindowTextLabel.MouseUp += WindowTextLabel_MouseUp;
                }
            }
        }

        private MinMaxButton __MaxButton;

        private MinMaxButton _MaxButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __MaxButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__MaxButton != null)
                {
                    __MaxButton.Click -= MaxButton_Click;
                }

                __MaxButton = value;
                if (__MaxButton != null)
                {
                    __MaxButton.Click += MaxButton_Click;
                }
            }
        }

        private ShapedButton _BuyNowShapedButton;
        private ShapedButton _ToolsShapedButton;
        private Panel _BottomPanel;
        private ZopeButton __MinButton;

        private ZopeButton _MinButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __MinButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__MinButton != null)
                {
                    __MinButton.Click -= MinButton_Click;
                }

                __MinButton = value;
                if (__MinButton != null)
                {
                    __MinButton.Click += MinButton_Click;
                }
            }
        }

        private ToolTip _ToolTip;
        private Panel __RightBorderBottomPanel;

        private Panel _RightBorderBottomPanel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __RightBorderBottomPanel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__RightBorderBottomPanel != null)
                {
                    __RightBorderBottomPanel.MouseDown -= RightBottomPanel_2_MouseDown;
                    __RightBorderBottomPanel.MouseMove -= RightBottomPanel_2_MouseMove;
                    __RightBorderBottomPanel.MouseUp -= RightBottomPanel_2_MouseUp;
                }

                __RightBorderBottomPanel = value;
                if (__RightBorderBottomPanel != null)
                {
                    __RightBorderBottomPanel.MouseDown += RightBottomPanel_2_MouseDown;
                    __RightBorderBottomPanel.MouseMove += RightBottomPanel_2_MouseMove;
                    __RightBorderBottomPanel.MouseUp += RightBottomPanel_2_MouseUp;
                }
            }
        }

        private Panel __BottomBorderLeftPanel;

        private Panel _BottomBorderLeftPanel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __BottomBorderLeftPanel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__BottomBorderLeftPanel != null)
                {
                    __BottomBorderLeftPanel.MouseDown -= LeftBottomPanel_1_MouseDown;
                    __BottomBorderLeftPanel.MouseMove -= LeftBottomPanel_1_MouseMove;
                    __BottomBorderLeftPanel.MouseUp -= LeftBottomPanel_1_MouseUp;
                }

                __BottomBorderLeftPanel = value;
                if (__BottomBorderLeftPanel != null)
                {
                    __BottomBorderLeftPanel.MouseDown += LeftBottomPanel_1_MouseDown;
                    __BottomBorderLeftPanel.MouseMove += LeftBottomPanel_1_MouseMove;
                    __BottomBorderLeftPanel.MouseUp += LeftBottomPanel_1_MouseUp;
                }
            }
        }

        private Panel __LeftBorderBottomPanel;

        private Panel _LeftBorderBottomPanel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __LeftBorderBottomPanel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__LeftBorderBottomPanel != null)
                {
                    __LeftBorderBottomPanel.MouseDown -= LeftBottomPanel_2_MouseDown;
                    __LeftBorderBottomPanel.MouseMove -= LeftBottomPanel_2_MouseMove;
                    __LeftBorderBottomPanel.MouseUp -= LeftBottomPanel_2_MouseUp;
                }

                __LeftBorderBottomPanel = value;
                if (__LeftBorderBottomPanel != null)
                {
                    __LeftBorderBottomPanel.MouseDown += LeftBottomPanel_2_MouseDown;
                    __LeftBorderBottomPanel.MouseMove += LeftBottomPanel_2_MouseMove;
                    __LeftBorderBottomPanel.MouseUp += LeftBottomPanel_2_MouseUp;
                }
            }
        }

        private Panel __RightBorderTopPanel;

        private Panel _RightBorderTopPanel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __RightBorderTopPanel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__RightBorderTopPanel != null)
                {
                    __RightBorderTopPanel.MouseDown -= RightTopPanel_1_MouseDown;
                    __RightBorderTopPanel.MouseMove -= RightTopPanel_1_MouseMove;
                    __RightBorderTopPanel.MouseUp -= RightTopPanel_1_MouseUp;
                }

                __RightBorderTopPanel = value;
                if (__RightBorderTopPanel != null)
                {
                    __RightBorderTopPanel.MouseDown += RightTopPanel_1_MouseDown;
                    __RightBorderTopPanel.MouseMove += RightTopPanel_1_MouseMove;
                    __RightBorderTopPanel.MouseUp += RightTopPanel_1_MouseUp;
                }
            }
        }

        private Panel __TopBorderRightPanel;

        private Panel _TopBorderRightPanel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __TopBorderRightPanel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__TopBorderRightPanel != null)
                {
                    __TopBorderRightPanel.MouseDown -= RightTopPanel_2_MouseDown;
                    __TopBorderRightPanel.MouseMove -= RightTopPanel_2_MouseMove;
                    __TopBorderRightPanel.MouseUp -= RightTopPanel_2_MouseUp;
                }

                __TopBorderRightPanel = value;
                if (__TopBorderRightPanel != null)
                {
                    __TopBorderRightPanel.MouseDown += RightTopPanel_2_MouseDown;
                    __TopBorderRightPanel.MouseMove += RightTopPanel_2_MouseMove;
                    __TopBorderRightPanel.MouseUp += RightTopPanel_2_MouseUp;
                }
            }
        }

        private Panel __TopBorderLeftPanel;

        private Panel _TopBorderLeftPanel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __TopBorderLeftPanel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__TopBorderLeftPanel != null)
                {
                    __TopBorderLeftPanel.MouseDown -= LeftTopPanel_1_MouseDown;
                    __TopBorderLeftPanel.MouseMove -= LeftTopPanel_1_MouseMove;
                    __TopBorderLeftPanel.MouseUp -= LeftTopPanel_1_MouseUp;
                }

                __TopBorderLeftPanel = value;
                if (__TopBorderLeftPanel != null)
                {
                    __TopBorderLeftPanel.MouseDown += LeftTopPanel_1_MouseDown;
                    __TopBorderLeftPanel.MouseMove += LeftTopPanel_1_MouseMove;
                    __TopBorderLeftPanel.MouseUp += LeftTopPanel_1_MouseUp;
                }
            }
        }

        private Panel __LeftBorderTopPanel;

        private Panel _LeftBorderTopPanel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __LeftBorderTopPanel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__LeftBorderTopPanel != null)
                {
                    __LeftBorderTopPanel.MouseDown -= LeftTopPanel_2_MouseDown;
                    __LeftBorderTopPanel.MouseMove -= LeftTopPanel_2_MouseMove;
                    __LeftBorderTopPanel.MouseUp -= LeftTopPanel_2_MouseUp;
                }

                __LeftBorderTopPanel = value;
                if (__LeftBorderTopPanel != null)
                {
                    __LeftBorderTopPanel.MouseDown += LeftTopPanel_2_MouseDown;
                    __LeftBorderTopPanel.MouseMove += LeftTopPanel_2_MouseMove;
                    __LeftBorderTopPanel.MouseUp += LeftTopPanel_2_MouseUp;
                }
            }
        }

        private Panel _LeftPanel;
        private ZopeButton __FileButton;

        private ZopeButton _FileButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __FileButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__FileButton != null)
                {
                    __FileButton.Click -= File_button_Click;
                }

                __FileButton = value;
                if (__FileButton != null)
                {
                    __FileButton.Click += File_button_Click;
                }
            }
        }

        private Panel _SeparatorPanel;
        private ZopeButton __EditButton;

        private ZopeButton _EditButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __EditButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__EditButton != null)
                {
                    __EditButton.Click -= Edit_button_Click;
                }

                __EditButton = value;
                if (__EditButton != null)
                {
                    __EditButton.Click += Edit_button_Click;
                }
            }
        }

        private ZopeButton __ViewButton;

        private ZopeButton _ViewButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ViewButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ViewButton != null)
                {
                    __ViewButton.Click -= View_button_Click;
                }

                __ViewButton = value;
                if (__ViewButton != null)
                {
                    __ViewButton.Click += View_button_Click;
                }
            }
        }

        private ZopeButton __HelpButton;

        private ZopeButton _HelpButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __HelpButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__HelpButton != null)
                {
                    __HelpButton.Click -= Help_button_Click;
                }

                __HelpButton = value;
                if (__HelpButton != null)
                {
                    __HelpButton.Click += Help_button_Click;
                }
            }
        }

        private ZopeButton __RunButton;

        private ZopeButton _RunButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __RunButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__RunButton != null)
                {
                    __RunButton.Click -= Run_button_Click;
                }

                __RunButton = value;
                if (__RunButton != null)
                {
                    __RunButton.Click += Run_button_Click;
                }
            }
        }

        private Label _InformationLabel;
        private ShapedButton _TestShapedButton;
        private ShapedButton _TimesShapedButton;
        private ZopeMenuStrip _MenuStrip;
        private System.Windows.Forms.ToolStripMenuItem _FileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _NewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _OpenToolStripMenuItem;
        private ToolStripSeparator _ToolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem _SaveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _SaveAsToolStripMenuItem;
        private ToolStripSeparator _ToolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem _CloseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _CloseAllToolStripMenuItem;
        private ToolStripSeparator _ToolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem _PrintToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _PrintPreviewToolStripMenuItem;
        private ToolStripSeparator _ToolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem _CloseToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem _EditToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _CutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _CopyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _PasteToolStripMenuItem;
        private ToolStripSeparator _ToolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem _UnduToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _RedoToolStripMenuItem;
        private ToolStripSeparator _ToolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem _FindToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _ReplaceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _SelectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _HelpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _HelpContentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _OnlineHelpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _AboutToolStripMenuItem;
    }
}
