using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using isr.Core.Controls;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Tester
{
    public partial class BlueForm
    {
        /// <summary>
    /// Required designer variable.
    /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && components is object)
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        /// <summary>
        /// Required method for Designer support - do not modify the contents of this method with the
        /// code editor.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            __TopBorderPanel = new Panel();
            __TopBorderPanel.MouseDown += new MouseEventHandler(TopBorderPanel_MouseDown);
            __TopBorderPanel.MouseMove += new MouseEventHandler(TopBorderPanel_MouseMove);
            __TopBorderPanel.MouseUp += new MouseEventHandler(TopBorderPanel_MouseUp);
            __RightBorderPanel = new Panel();
            __RightBorderPanel.MouseDown += new MouseEventHandler(RightPanel_MouseDown);
            __RightBorderPanel.MouseMove += new MouseEventHandler(RightPanel_MouseMove);
            __RightBorderPanel.MouseUp += new MouseEventHandler(RightPanel_MouseUp);
            __LeftBorderPanel = new Panel();
            __LeftBorderPanel.MouseDown += new MouseEventHandler(LeftPanel_MouseDown);
            __LeftBorderPanel.MouseMove += new MouseEventHandler(LeftPanel_MouseMove);
            __LeftBorderPanel.MouseUp += new MouseEventHandler(LeftPanel_MouseUp);
            __BottomBorderPanel = new Panel();
            __BottomBorderPanel.MouseDown += new MouseEventHandler(BottomPanel_MouseDown);
            __BottomBorderPanel.MouseMove += new MouseEventHandler(BottomPanel_MouseMove);
            __BottomBorderPanel.MouseUp += new MouseEventHandler(BottomPanel_MouseUp);
            __TopPanel = new Panel();
            __TopPanel.MouseDown += new MouseEventHandler(TopPanel_MouseDown);
            __TopPanel.MouseMove += new MouseEventHandler(TopPanel_MouseMove);
            __TopPanel.MouseUp += new MouseEventHandler(TopPanel_MouseUp);
            _IconPanel = new Panel();
            _WindowTextLabel = new Label();
            __MaxButton = new MinMaxButton();
            __MaxButton.Click += new System.EventHandler(MaxButton_Click);
            __MinButton = new ZopeButton();
            __MinButton.Click += new System.EventHandler(MinButton_Click);
            __CloseButton = new ZopeButton();
            __CloseButton.Click += new System.EventHandler(CloseButton_Click);
            _ToolTip = new ToolTip(components);
            __TopPanel.SuspendLayout();
            SuspendLayout();
            // 
            // _TopBorderPanel
            // 
            __TopBorderPanel.BackColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(10)), Conversions.ToInteger(Conversions.ToByte(20)), Conversions.ToInteger(Conversions.ToByte(50)));
            __TopBorderPanel.Cursor = Cursors.SizeNS;
            __TopBorderPanel.Dock = DockStyle.Top;
            __TopBorderPanel.Location = new Point(0, 0);
            __TopBorderPanel.Name = "__TopBorderPanel";
            __TopBorderPanel.Size = new Size(684, 2);
            __TopBorderPanel.TabIndex = 0;
            // 
            // _RightBorderPanel
            // 
            __RightBorderPanel.BackColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(10)), Conversions.ToInteger(Conversions.ToByte(20)), Conversions.ToInteger(Conversions.ToByte(50)));
            __RightBorderPanel.Cursor = Cursors.SizeWE;
            __RightBorderPanel.Dock = DockStyle.Right;
            __RightBorderPanel.Location = new Point(682, 2);
            __RightBorderPanel.Name = "__RightBorderPanel";
            __RightBorderPanel.Size = new Size(2, 459);
            __RightBorderPanel.TabIndex = 1;
            // 
            // _LeftBorderPanel
            // 
            __LeftBorderPanel.BackColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(10)), Conversions.ToInteger(Conversions.ToByte(20)), Conversions.ToInteger(Conversions.ToByte(50)));
            __LeftBorderPanel.Cursor = Cursors.SizeWE;
            __LeftBorderPanel.Dock = DockStyle.Left;
            __LeftBorderPanel.Location = new Point(0, 2);
            __LeftBorderPanel.Name = "__LeftBorderPanel";
            __LeftBorderPanel.Size = new Size(2, 459);
            __LeftBorderPanel.TabIndex = 2;
            // 
            // _BottomBorderPanel
            // 
            __BottomBorderPanel.BackColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(10)), Conversions.ToInteger(Conversions.ToByte(20)), Conversions.ToInteger(Conversions.ToByte(50)));
            __BottomBorderPanel.Cursor = Cursors.SizeNS;
            __BottomBorderPanel.Dock = DockStyle.Bottom;
            __BottomBorderPanel.Location = new Point(2, 459);
            __BottomBorderPanel.Name = "__BottomBorderPanel";
            __BottomBorderPanel.Size = new Size(680, 2);
            __BottomBorderPanel.TabIndex = 3;
            // 
            // _TopPanel
            // 
            __TopPanel.BackColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(20)), Conversions.ToInteger(Conversions.ToByte(40)), Conversions.ToInteger(Conversions.ToByte(80)));
            __TopPanel.Controls.Add(_IconPanel);
            __TopPanel.Controls.Add(_WindowTextLabel);
            __TopPanel.Controls.Add(__MaxButton);
            __TopPanel.Controls.Add(__MinButton);
            __TopPanel.Controls.Add(__CloseButton);
            __TopPanel.Dock = DockStyle.Top;
            __TopPanel.Location = new Point(2, 2);
            __TopPanel.Name = "__TopPanel";
            __TopPanel.Size = new Size(680, 35);
            __TopPanel.TabIndex = 4;
            // 
            // _IconPanel
            // 
            _IconPanel.BackgroundImage = My.Resources.Resources.Crazy;
            _IconPanel.Location = new Point(10, 3);
            _IconPanel.Name = "_IconPanel";
            _IconPanel.Size = new Size(26, 26);
            _IconPanel.TabIndex = 5;
            // 
            // _WindowTextLabel
            // 
            _WindowTextLabel.AutoSize = true;
            _WindowTextLabel.Font = new Font("Microsoft Sans Serif", 11.25f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _WindowTextLabel.ForeColor = Color.White;
            _WindowTextLabel.Location = new Point(57, 7);
            _WindowTextLabel.Name = "_WindowTextLabel";
            _WindowTextLabel.Size = new Size(77, 18);
            _WindowTextLabel.TabIndex = 6;
            _WindowTextLabel.Text = "Blue Form";
            // 
            // _MaxButton
            // 
            __MaxButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            __MaxButton.BusyBackColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(20)), Conversions.ToInteger(Conversions.ToByte(40)), Conversions.ToInteger(Conversions.ToByte(80)));
            __MaxButton.CustomFormState = CustomFormState.Normal;
            __MaxButton.DisplayText = "_";
            __MaxButton.FlatStyle = FlatStyle.Flat;
            __MaxButton.Font = new Font("Segoe UI", 9.75f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            __MaxButton.ForeColor = Color.White;
            __MaxButton.Location = new Point(604, 3);
            __MaxButton.MouseClickColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(10)), Conversions.ToInteger(Conversions.ToByte(20)), Conversions.ToInteger(Conversions.ToByte(60)));
            __MaxButton.MouseHoverColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(40)), Conversions.ToInteger(Conversions.ToByte(80)), Conversions.ToInteger(Conversions.ToByte(180)));
            __MaxButton.Name = "__MaxButton";
            __MaxButton.Size = new Size(35, 25);
            __MaxButton.TabIndex = 5;
            __MaxButton.Text = "minMaxButton1";
            __MaxButton.TextLocation = new Point(11, 8);
            _ToolTip.SetToolTip(__MaxButton, "Maximize");
            __MaxButton.UseVisualStyleBackColor = true;
            // 
            // _MinButton
            // 
            __MinButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            __MinButton.BackColorActive = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(20)), Conversions.ToInteger(Conversions.ToByte(40)), Conversions.ToInteger(Conversions.ToByte(80)));
            __MinButton.DisplayText = "_";
            __MinButton.FlatStyle = FlatStyle.Flat;
            __MinButton.Font = new Font("Microsoft YaHei UI", 18.0f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            __MinButton.ForeColor = Color.White;
            __MinButton.Location = new Point(569, 3);
            __MinButton.BackColorMouseClick = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(10)), Conversions.ToInteger(Conversions.ToByte(20)), Conversions.ToInteger(Conversions.ToByte(60)));
            __MinButton.MouseColorsEnabled = true;
            __MinButton.BackColorMouseHover = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(40)), Conversions.ToInteger(Conversions.ToByte(80)), Conversions.ToInteger(Conversions.ToByte(180)));
            __MinButton.Name = "__MinButton";
            __MinButton.Size = new Size(35, 25);
            __MinButton.TabIndex = 1;
            __MinButton.Text = "_";
            __MinButton.TextLocationLeft = 8;
            __MinButton.TextLocationTop = -14;
            _ToolTip.SetToolTip(__MinButton, "Minimize");
            __MinButton.UseVisualStyleBackColor = true;
            // 
            // _CloseButton
            // 
            __CloseButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            __CloseButton.BackColorActive = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(20)), Conversions.ToInteger(Conversions.ToByte(40)), Conversions.ToInteger(Conversions.ToByte(80)));
            __CloseButton.DisplayText = "X";
            __CloseButton.FlatStyle = FlatStyle.Flat;
            __CloseButton.Font = new Font("Microsoft YaHei UI", 11.25f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            __CloseButton.ForeColor = Color.White;
            __CloseButton.Location = new Point(639, 3);
            __CloseButton.BackColorMouseClick = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(150)), Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(0)));
            __CloseButton.MouseColorsEnabled = true;
            __CloseButton.BackColorMouseHover = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(40)), Conversions.ToInteger(Conversions.ToByte(80)), Conversions.ToInteger(Conversions.ToByte(180)));
            __CloseButton.Name = "__CloseButton";
            __CloseButton.Size = new Size(35, 25);
            __CloseButton.TabIndex = 0;
            __CloseButton.Text = "X";
            __CloseButton.TextLocationLeft = 10;
            __CloseButton.TextLocationTop = 4;
            _ToolTip.SetToolTip(__CloseButton, "Close");
            __CloseButton.UseVisualStyleBackColor = true;
            // 
            // BlueForm
            // 
            AutoScaleDimensions = new SizeF(6.0f, 13.0f);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(60)), Conversions.ToInteger(Conversions.ToByte(150)));
            ClientSize = new Size(684, 461);
            Controls.Add(__TopPanel);
            Controls.Add(__BottomBorderPanel);
            Controls.Add(__LeftBorderPanel);
            Controls.Add(__RightBorderPanel);
            Controls.Add(__TopBorderPanel);
            FormBorderStyle = FormBorderStyle.None;
            Name = "BlueForm";
            Text = "Blue Form";
            __TopPanel.ResumeLayout(false);
            __TopPanel.PerformLayout();
            ResumeLayout(false);
        }

        private Panel __TopBorderPanel;

        private Panel _TopBorderPanel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __TopBorderPanel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__TopBorderPanel != null)
                {
                    __TopBorderPanel.MouseDown -= TopBorderPanel_MouseDown;
                    __TopBorderPanel.MouseMove -= TopBorderPanel_MouseMove;
                    __TopBorderPanel.MouseUp -= TopBorderPanel_MouseUp;
                }

                __TopBorderPanel = value;
                if (__TopBorderPanel != null)
                {
                    __TopBorderPanel.MouseDown += TopBorderPanel_MouseDown;
                    __TopBorderPanel.MouseMove += TopBorderPanel_MouseMove;
                    __TopBorderPanel.MouseUp += TopBorderPanel_MouseUp;
                }
            }
        }

        private Panel __RightBorderPanel;

        private Panel _RightBorderPanel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __RightBorderPanel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__RightBorderPanel != null)
                {
                    __RightBorderPanel.MouseDown -= RightPanel_MouseDown;
                    __RightBorderPanel.MouseMove -= RightPanel_MouseMove;
                    __RightBorderPanel.MouseUp -= RightPanel_MouseUp;
                }

                __RightBorderPanel = value;
                if (__RightBorderPanel != null)
                {
                    __RightBorderPanel.MouseDown += RightPanel_MouseDown;
                    __RightBorderPanel.MouseMove += RightPanel_MouseMove;
                    __RightBorderPanel.MouseUp += RightPanel_MouseUp;
                }
            }
        }

        private Panel __LeftBorderPanel;

        private Panel _LeftBorderPanel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __LeftBorderPanel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__LeftBorderPanel != null)
                {
                    __LeftBorderPanel.MouseDown -= LeftPanel_MouseDown;
                    __LeftBorderPanel.MouseMove -= LeftPanel_MouseMove;
                    __LeftBorderPanel.MouseUp -= LeftPanel_MouseUp;
                }

                __LeftBorderPanel = value;
                if (__LeftBorderPanel != null)
                {
                    __LeftBorderPanel.MouseDown += LeftPanel_MouseDown;
                    __LeftBorderPanel.MouseMove += LeftPanel_MouseMove;
                    __LeftBorderPanel.MouseUp += LeftPanel_MouseUp;
                }
            }
        }

        private Panel __BottomBorderPanel;

        private Panel _BottomBorderPanel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __BottomBorderPanel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__BottomBorderPanel != null)
                {
                    __BottomBorderPanel.MouseDown -= BottomPanel_MouseDown;
                    __BottomBorderPanel.MouseMove -= BottomPanel_MouseMove;
                    __BottomBorderPanel.MouseUp -= BottomPanel_MouseUp;
                }

                __BottomBorderPanel = value;
                if (__BottomBorderPanel != null)
                {
                    __BottomBorderPanel.MouseDown += BottomPanel_MouseDown;
                    __BottomBorderPanel.MouseMove += BottomPanel_MouseMove;
                    __BottomBorderPanel.MouseUp += BottomPanel_MouseUp;
                }
            }
        }

        private Panel __TopPanel;

        private Panel _TopPanel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __TopPanel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__TopPanel != null)
                {
                    __TopPanel.MouseDown -= TopPanel_MouseDown;
                    __TopPanel.MouseMove -= TopPanel_MouseMove;
                    __TopPanel.MouseUp -= TopPanel_MouseUp;
                }

                __TopPanel = value;
                if (__TopPanel != null)
                {
                    __TopPanel.MouseDown += TopPanel_MouseDown;
                    __TopPanel.MouseMove += TopPanel_MouseMove;
                    __TopPanel.MouseUp += TopPanel_MouseUp;
                }
            }
        }

        private ZopeButton __CloseButton;

        private ZopeButton _CloseButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __CloseButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__CloseButton != null)
                {
                    __CloseButton.Click -= CloseButton_Click;
                }

                __CloseButton = value;
                if (__CloseButton != null)
                {
                    __CloseButton.Click += CloseButton_Click;
                }
            }
        }

        private MinMaxButton __MaxButton;

        private MinMaxButton _MaxButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __MaxButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__MaxButton != null)
                {
                    __MaxButton.Click -= MaxButton_Click;
                }

                __MaxButton = value;
                if (__MaxButton != null)
                {
                    __MaxButton.Click += MaxButton_Click;
                }
            }
        }

        private ZopeButton __MinButton;

        private ZopeButton _MinButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __MinButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__MinButton != null)
                {
                    __MinButton.Click -= MinButton_Click;
                }

                __MinButton = value;
                if (__MinButton != null)
                {
                    __MinButton.Click += MinButton_Click;
                }
            }
        }

        private ToolTip _ToolTip;
        private Label _WindowTextLabel;
        private Panel _IconPanel;
    }
}
