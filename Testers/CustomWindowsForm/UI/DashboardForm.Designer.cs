using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using isr.Core.Controls;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Tester
{
    public partial class DashboardForm
    {
        /// <summary>
    /// Required designer variable.
    /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && components is object)
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        /// <summary>
        /// Required method for Designer support - do not modify the contents of this method with the
        /// code editor.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        private void InitializeComponent()
        {
            _LeftTopPanel = new Panel();
            _LeftTopPanelLabel = new Label();
            __TopPanel = new Panel();
            __TopPanel.MouseDown += new MouseEventHandler(TopPanel_MouseDown);
            __TopPanel.MouseMove += new MouseEventHandler(TopPanel_MouseMove);
            __TopPanel.MouseUp += new MouseEventHandler(TopPanel_MouseUp);
            __DashboardLabel = new Label();
            __DashboardLabel.MouseDown += new MouseEventHandler(DashboardLabel_mouseDown);
            __DashboardLabel.MouseMove += new MouseEventHandler(DashboardLabel_mouseMove);
            __DashboardLabel.MouseUp += new MouseEventHandler(DashboardLabel_mouseUp);
            _IconPanel = new Panel();
            __MinButton = new ZopeButton();
            __MinButton.Click += new System.EventHandler(MinButton_Click);
            __CloseButton = new ZopeButton();
            __CloseButton.Click += new System.EventHandler(CloseButton_Click);
            _LeftPanel = new Panel();
            _LeftBottomPanel = new Panel();
            _LeftBottomPanelLabel = new Label();
            __SettingsButton = new ZopeButton();
            __SettingsButton.Click += new System.EventHandler(Settings_button_Click);
            __ThemeButton = new ZopeButton();
            __ThemeButton.Click += new System.EventHandler(Theme_button_Click);
            __LayoutButton = new ZopeButton();
            __LayoutButton.Click += new System.EventHandler(Layout_button_Click);
            __PagesButton = new ZopeButton();
            __PagesButton.Click += new System.EventHandler(Pages_button_Click);
            __StatsButton = new ZopeButton();
            __StatsButton.Click += new System.EventHandler(Stats_button_Click);
            __DashboardButton = new ZopeButton();
            __DashboardButton.Click += new System.EventHandler(Dashboard_button_Click);
            _VisitorsPanel = new Panel();
            _MonthVisitorsCountLabel = new Label();
            _MonthVisitorsCountLabelLabel = new Label();
            _TodayVisitorsCountLabel = new Label();
            _TodayVisitorsCountLabelLabel = new Label();
            _VisitorsCountPanel = new Panel();
            _VisitorsCountLabel = new Label();
            _VisitorsCountLabelLabel = new Label();
            _AdminPanel = new Panel();
            _AdminNameLabel = new Label();
            _AdminLabelLabel = new Label();
            _AdminIconPanel = new Panel();
            _StoragePanel = new Panel();
            _TotalStorageLabel = new Label();
            _TotalStorageLabelLabel = new Label();
            _FreeStorageLabel = new Label();
            _FreeStorageLabelLabel = new Label();
            _UsedStorageLabel = new Label();
            _UsedStorageLabelLabel = new Label();
            _StorageTopPanel = new Panel();
            _StoratgeTitleLabel = new Label();
            _ControlPanel = new Panel();
            _ContrlPanelLabel = new Label();
            _DeleteBlogButton = new ZopeButton();
            _ViewBlogButton = new ZopeButton();
            _CampainsPanel = new Panel();
            _CampaignsLabel = new Label();
            _LeftTopPanel.SuspendLayout();
            __TopPanel.SuspendLayout();
            _LeftPanel.SuspendLayout();
            _LeftBottomPanel.SuspendLayout();
            _VisitorsPanel.SuspendLayout();
            _VisitorsCountPanel.SuspendLayout();
            _AdminPanel.SuspendLayout();
            _StoragePanel.SuspendLayout();
            _StorageTopPanel.SuspendLayout();
            _ControlPanel.SuspendLayout();
            _CampainsPanel.SuspendLayout();
            SuspendLayout();
            // 
            // _LeftTopPanel
            // 
            _LeftTopPanel.BackColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(150)), Conversions.ToInteger(Conversions.ToByte(80)));
            _LeftTopPanel.Controls.Add(_LeftTopPanelLabel);
            _LeftTopPanel.Location = new Point(0, 0);
            _LeftTopPanel.Name = "_LeftTopPanel";
            _LeftTopPanel.Size = new Size(245, 60);
            _LeftTopPanel.TabIndex = 0;
            // 
            // _LeftTopPanelLabel
            // 
            _LeftTopPanelLabel.AutoSize = true;
            _LeftTopPanelLabel.Font = new Font("Microsoft Sans Serif", 18.0f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _LeftTopPanelLabel.ForeColor = Color.White;
            _LeftTopPanelLabel.Location = new Point(45, 13);
            _LeftTopPanelLabel.Name = "_LeftTopPanelLabel";
            _LeftTopPanelLabel.Size = new Size(131, 29);
            _LeftTopPanelLabel.TabIndex = 0;
            _LeftTopPanelLabel.Text = "Extreme UI";
            // 
            // _TopPanel
            // 
            __TopPanel.BackColor = Color.White;
            __TopPanel.Controls.Add(__DashboardLabel);
            __TopPanel.Controls.Add(_IconPanel);
            __TopPanel.Controls.Add(__MinButton);
            __TopPanel.Controls.Add(__CloseButton);
            __TopPanel.Location = new Point(245, 0);
            __TopPanel.Name = "__TopPanel";
            __TopPanel.Size = new Size(605, 60);
            __TopPanel.TabIndex = 1;
            // 
            // _DashboardLabel
            // 
            __DashboardLabel.AutoSize = true;
            __DashboardLabel.Font = new Font("Microsoft Sans Serif", 18.0f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            __DashboardLabel.ForeColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(50)), Conversions.ToInteger(Conversions.ToByte(50)), Conversions.ToInteger(Conversions.ToByte(50)));
            __DashboardLabel.Location = new Point(60, 13);
            __DashboardLabel.Name = "__DashboardLabel";
            __DashboardLabel.Size = new Size(131, 29);
            __DashboardLabel.TabIndex = 3;
            __DashboardLabel.Text = "Dashboard";
            // 
            // _IconPanel
            // 
            _IconPanel.BackgroundImage = My.Resources.Resources.home;
            _IconPanel.Location = new Point(6, 13);
            _IconPanel.Name = "_IconPanel";
            _IconPanel.Size = new Size(30, 30);
            _IconPanel.TabIndex = 2;
            // 
            // _MinButton
            // 
            __MinButton.BackColorActive = Color.White;
            __MinButton.DisplayText = "_";
            __MinButton.FlatStyle = FlatStyle.Flat;
            __MinButton.Font = new Font("Microsoft YaHei UI", 20.25f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            __MinButton.ForeColor = Color.Black;
            __MinButton.Location = new Point(540, 4);
            __MinButton.BackColorMouseClick = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(160)), Conversions.ToInteger(Conversions.ToByte(180)), Conversions.ToInteger(Conversions.ToByte(200)));
            __MinButton.MouseColorsEnabled = true;
            __MinButton.BackColorMouseHover = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(100)), Conversions.ToInteger(Conversions.ToByte(150)), Conversions.ToInteger(Conversions.ToByte(220)));
            __MinButton.Name = "__MinButton";
            __MinButton.Size = new Size(31, 24);
            __MinButton.TabIndex = 1;
            __MinButton.Text = "_";
            __MinButton.TextLocationLeft = 6;
            __MinButton.TextLocationTop = -20;
            __MinButton.UseVisualStyleBackColor = true;
            // 
            // _CloseButton
            // 
            __CloseButton.BackColorActive = Color.White;
            __CloseButton.DisplayText = "X";
            __CloseButton.FlatStyle = FlatStyle.Flat;
            __CloseButton.Font = new Font("Microsoft YaHei UI", 14.25f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            __CloseButton.ForeColor = Color.Black;
            __CloseButton.Location = new Point(571, 4);
            __CloseButton.BackColorMouseClick = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(100)), Conversions.ToInteger(Conversions.ToByte(150)), Conversions.ToInteger(Conversions.ToByte(220)));
            __CloseButton.MouseColorsEnabled = true;
            __CloseButton.BackColorMouseHover = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(180)), Conversions.ToInteger(Conversions.ToByte(40)), Conversions.ToInteger(Conversions.ToByte(50)));
            __CloseButton.Name = "__CloseButton";
            __CloseButton.Size = new Size(31, 24);
            __CloseButton.TabIndex = 0;
            __CloseButton.Text = "X";
            __CloseButton.TextLocationLeft = 6;
            __CloseButton.TextLocationTop = -1;
            __CloseButton.UseVisualStyleBackColor = true;
            // 
            // _LeftPanel
            // 
            _LeftPanel.BackColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(40)));
            _LeftPanel.Controls.Add(_LeftBottomPanel);
            _LeftPanel.Controls.Add(__SettingsButton);
            _LeftPanel.Controls.Add(__ThemeButton);
            _LeftPanel.Controls.Add(__LayoutButton);
            _LeftPanel.Controls.Add(__PagesButton);
            _LeftPanel.Controls.Add(__StatsButton);
            _LeftPanel.Controls.Add(__DashboardButton);
            _LeftPanel.Location = new Point(0, 60);
            _LeftPanel.Name = "_LeftPanel";
            _LeftPanel.Size = new Size(245, 440);
            _LeftPanel.TabIndex = 2;
            // 
            // _LeftBottomPanel
            // 
            _LeftBottomPanel.BackColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(20)), Conversions.ToInteger(Conversions.ToByte(20)), Conversions.ToInteger(Conversions.ToByte(30)));
            _LeftBottomPanel.Controls.Add(_LeftBottomPanelLabel);
            _LeftBottomPanel.Location = new Point(0, 380);
            _LeftBottomPanel.Name = "_LeftBottomPanel";
            _LeftBottomPanel.Size = new Size(245, 60);
            _LeftBottomPanel.TabIndex = 6;
            // 
            // _LeftBottomPanelLabel
            // 
            _LeftBottomPanelLabel.AutoSize = true;
            _LeftBottomPanelLabel.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _LeftBottomPanelLabel.ForeColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(200)), Conversions.ToInteger(Conversions.ToByte(200)), Conversions.ToInteger(Conversions.ToByte(200)));
            _LeftBottomPanelLabel.Location = new Point(12, 14);
            _LeftBottomPanelLabel.Name = "_LeftBottomPanelLabel";
            _LeftBottomPanelLabel.Size = new Size(102, 16);
            _LeftBottomPanelLabel.TabIndex = 0;
            _LeftBottomPanelLabel.Text = "Copyright   2016";
            // 
            // _SettingsButton
            // 
            __SettingsButton.BackColorActive = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(40)));
            __SettingsButton.Cursor = Cursors.Hand;
            __SettingsButton.DisplayText = "Settings";
            __SettingsButton.FlatStyle = FlatStyle.Flat;
            __SettingsButton.Font = new Font("Microsoft YaHei UI", 14.25f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            __SettingsButton.ForeColor = Color.White;
            __SettingsButton.Location = new Point(0, 250);
            __SettingsButton.BackColorMouseClick = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(20)), Conversions.ToInteger(Conversions.ToByte(20)), Conversions.ToInteger(Conversions.ToByte(20)));
            __SettingsButton.MouseColorsEnabled = true;
            __SettingsButton.BackColorMouseHover = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(20)), Conversions.ToInteger(Conversions.ToByte(20)), Conversions.ToInteger(Conversions.ToByte(20)));
            __SettingsButton.Name = "__SettingsButton";
            __SettingsButton.Size = new Size(245, 50);
            __SettingsButton.TabIndex = 5;
            __SettingsButton.Text = "Settings";
            __SettingsButton.TextLocationLeft = 80;
            __SettingsButton.TextLocationTop = 10;
            __SettingsButton.UseVisualStyleBackColor = true;
            // 
            // _ThemeButton
            // 
            __ThemeButton.BackColorActive = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(40)));
            __ThemeButton.Cursor = Cursors.Hand;
            __ThemeButton.DisplayText = "Theme";
            __ThemeButton.FlatStyle = FlatStyle.Flat;
            __ThemeButton.Font = new Font("Microsoft YaHei UI", 14.25f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            __ThemeButton.ForeColor = Color.White;
            __ThemeButton.Location = new Point(0, 200);
            __ThemeButton.BackColorMouseClick = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(20)), Conversions.ToInteger(Conversions.ToByte(20)), Conversions.ToInteger(Conversions.ToByte(20)));
            __ThemeButton.MouseColorsEnabled = true;
            __ThemeButton.BackColorMouseHover = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(20)), Conversions.ToInteger(Conversions.ToByte(20)), Conversions.ToInteger(Conversions.ToByte(20)));
            __ThemeButton.Name = "__ThemeButton";
            __ThemeButton.Size = new Size(245, 50);
            __ThemeButton.TabIndex = 4;
            __ThemeButton.Text = "Theme";
            __ThemeButton.TextLocationLeft = 82;
            __ThemeButton.TextLocationTop = 10;
            __ThemeButton.UseVisualStyleBackColor = true;
            // 
            // _LayoutButton
            // 
            __LayoutButton.BackColorActive = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(40)));
            __LayoutButton.Cursor = Cursors.Hand;
            __LayoutButton.DisplayText = "Layout";
            __LayoutButton.FlatStyle = FlatStyle.Flat;
            __LayoutButton.Font = new Font("Microsoft YaHei UI", 14.25f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            __LayoutButton.ForeColor = Color.White;
            __LayoutButton.Location = new Point(0, 150);
            __LayoutButton.BackColorMouseClick = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(20)), Conversions.ToInteger(Conversions.ToByte(20)), Conversions.ToInteger(Conversions.ToByte(20)));
            __LayoutButton.MouseColorsEnabled = true;
            __LayoutButton.BackColorMouseHover = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(20)), Conversions.ToInteger(Conversions.ToByte(20)), Conversions.ToInteger(Conversions.ToByte(20)));
            __LayoutButton.Name = "__LayoutButton";
            __LayoutButton.Size = new Size(245, 50);
            __LayoutButton.TabIndex = 3;
            __LayoutButton.Text = "Layout";
            __LayoutButton.TextLocationLeft = 80;
            __LayoutButton.TextLocationTop = 10;
            __LayoutButton.UseVisualStyleBackColor = true;
            // 
            // _PagesButton
            // 
            __PagesButton.BackColorActive = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(40)));
            __PagesButton.Cursor = Cursors.Hand;
            __PagesButton.DisplayText = "Pages";
            __PagesButton.FlatStyle = FlatStyle.Flat;
            __PagesButton.Font = new Font("Microsoft YaHei UI", 14.25f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            __PagesButton.ForeColor = Color.White;
            __PagesButton.Location = new Point(0, 100);
            __PagesButton.BackColorMouseClick = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(20)), Conversions.ToInteger(Conversions.ToByte(20)), Conversions.ToInteger(Conversions.ToByte(20)));
            __PagesButton.MouseColorsEnabled = true;
            __PagesButton.BackColorMouseHover = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(20)), Conversions.ToInteger(Conversions.ToByte(20)), Conversions.ToInteger(Conversions.ToByte(20)));
            __PagesButton.Name = "__PagesButton";
            __PagesButton.Size = new Size(245, 50);
            __PagesButton.TabIndex = 2;
            __PagesButton.Text = "Pages";
            __PagesButton.TextLocationLeft = 82;
            __PagesButton.TextLocationTop = 10;
            __PagesButton.UseVisualStyleBackColor = true;
            // 
            // _StatsButton
            // 
            __StatsButton.BackColorActive = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(40)));
            __StatsButton.Cursor = Cursors.Hand;
            __StatsButton.DisplayText = "Stats";
            __StatsButton.FlatStyle = FlatStyle.Flat;
            __StatsButton.Font = new Font("Microsoft YaHei UI", 14.25f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            __StatsButton.ForeColor = Color.White;
            __StatsButton.Location = new Point(0, 50);
            __StatsButton.BackColorMouseClick = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(20)), Conversions.ToInteger(Conversions.ToByte(20)), Conversions.ToInteger(Conversions.ToByte(20)));
            __StatsButton.MouseColorsEnabled = true;
            __StatsButton.BackColorMouseHover = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(20)), Conversions.ToInteger(Conversions.ToByte(20)), Conversions.ToInteger(Conversions.ToByte(20)));
            __StatsButton.Name = "__StatsButton";
            __StatsButton.Size = new Size(245, 50);
            __StatsButton.TabIndex = 1;
            __StatsButton.Text = "Stats";
            __StatsButton.TextLocationLeft = 84;
            __StatsButton.TextLocationTop = 10;
            __StatsButton.UseVisualStyleBackColor = true;
            // 
            // _DashboardButton
            // 
            __DashboardButton.BackColorActive = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(40)));
            __DashboardButton.Cursor = Cursors.Hand;
            __DashboardButton.DisplayText = "Dashboard";
            __DashboardButton.FlatStyle = FlatStyle.Flat;
            __DashboardButton.Font = new Font("Microsoft YaHei UI", 14.25f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            __DashboardButton.ForeColor = Color.White;
            __DashboardButton.Location = new Point(0, 0);
            __DashboardButton.BackColorMouseClick = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(20)), Conversions.ToInteger(Conversions.ToByte(20)), Conversions.ToInteger(Conversions.ToByte(20)));
            __DashboardButton.MouseColorsEnabled = true;
            __DashboardButton.BackColorMouseHover = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(20)), Conversions.ToInteger(Conversions.ToByte(20)), Conversions.ToInteger(Conversions.ToByte(20)));
            __DashboardButton.Name = "__DashboardButton";
            __DashboardButton.Size = new Size(245, 50);
            __DashboardButton.TabIndex = 0;
            __DashboardButton.Text = "Dashboard";
            __DashboardButton.TextLocationLeft = 60;
            __DashboardButton.TextLocationTop = 10;
            __DashboardButton.UseVisualStyleBackColor = true;
            // 
            // _VisitorsPanel
            // 
            _VisitorsPanel.BackColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(50)), Conversions.ToInteger(Conversions.ToByte(80)), Conversions.ToInteger(Conversions.ToByte(160)));
            _VisitorsPanel.Controls.Add(_MonthVisitorsCountLabel);
            _VisitorsPanel.Controls.Add(_MonthVisitorsCountLabelLabel);
            _VisitorsPanel.Controls.Add(_TodayVisitorsCountLabel);
            _VisitorsPanel.Controls.Add(_TodayVisitorsCountLabelLabel);
            _VisitorsPanel.Controls.Add(_VisitorsCountPanel);
            _VisitorsPanel.Location = new Point(251, 66);
            _VisitorsPanel.Name = "_VisitorsPanel";
            _VisitorsPanel.Size = new Size(590, 109);
            _VisitorsPanel.TabIndex = 3;
            // 
            // _MonthVisitorsCountLabel
            // 
            _MonthVisitorsCountLabel.AutoSize = true;
            _MonthVisitorsCountLabel.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _MonthVisitorsCountLabel.ForeColor = Color.Azure;
            _MonthVisitorsCountLabel.Location = new Point(457, 61);
            _MonthVisitorsCountLabel.Name = "_MonthVisitorsCountLabel";
            _MonthVisitorsCountLabel.Size = new Size(36, 16);
            _MonthVisitorsCountLabel.TabIndex = 4;
            _MonthVisitorsCountLabel.Text = "1035";
            // 
            // _MonthVisitorsCountLabelLabel
            // 
            _MonthVisitorsCountLabelLabel.AutoSize = true;
            _MonthVisitorsCountLabelLabel.Font = new Font("Microsoft Sans Serif", 12.0f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _MonthVisitorsCountLabelLabel.ForeColor = Color.White;
            _MonthVisitorsCountLabelLabel.Location = new Point(446, 23);
            _MonthVisitorsCountLabelLabel.Name = "_MonthVisitorsCountLabelLabel";
            _MonthVisitorsCountLabelLabel.Size = new Size(64, 20);
            _MonthVisitorsCountLabelLabel.TabIndex = 3;
            _MonthVisitorsCountLabelLabel.Text = "Monthly";
            // 
            // _TodayVisitorsCountLabel
            // 
            _TodayVisitorsCountLabel.AutoSize = true;
            _TodayVisitorsCountLabel.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _TodayVisitorsCountLabel.ForeColor = Color.Azure;
            _TodayVisitorsCountLabel.Location = new Point(285, 61);
            _TodayVisitorsCountLabel.Name = "_TodayVisitorsCountLabel";
            _TodayVisitorsCountLabel.Size = new Size(29, 16);
            _TodayVisitorsCountLabel.TabIndex = 2;
            _TodayVisitorsCountLabel.Text = "568";
            // 
            // _TodayVisitorsCountLabelLabel
            // 
            _TodayVisitorsCountLabelLabel.AutoSize = true;
            _TodayVisitorsCountLabelLabel.Font = new Font("Microsoft Sans Serif", 12.0f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _TodayVisitorsCountLabelLabel.ForeColor = Color.White;
            _TodayVisitorsCountLabelLabel.Location = new Point(274, 23);
            _TodayVisitorsCountLabelLabel.Name = "_TodayVisitorsCountLabelLabel";
            _TodayVisitorsCountLabelLabel.Size = new Size(52, 20);
            _TodayVisitorsCountLabelLabel.TabIndex = 1;
            _TodayVisitorsCountLabelLabel.Text = "Today";
            // 
            // _VisitorsCountPanel
            // 
            _VisitorsCountPanel.BackColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(10)), Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(60)));
            _VisitorsCountPanel.Controls.Add(_VisitorsCountLabel);
            _VisitorsCountPanel.Controls.Add(_VisitorsCountLabelLabel);
            _VisitorsCountPanel.Location = new Point(0, 0);
            _VisitorsCountPanel.Name = "_VisitorsCountPanel";
            _VisitorsCountPanel.Size = new Size(200, 109);
            _VisitorsCountPanel.TabIndex = 0;
            // 
            // _VisitorsCountLabel
            // 
            _VisitorsCountLabel.AutoSize = true;
            _VisitorsCountLabel.Font = new Font("Microsoft Sans Serif", 12.0f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _VisitorsCountLabel.ForeColor = Color.Cyan;
            _VisitorsCountLabel.Location = new Point(67, 61);
            _VisitorsCountLabel.Name = "_VisitorsCountLabel";
            _VisitorsCountLabel.Size = new Size(49, 20);
            _VisitorsCountLabel.TabIndex = 1;
            _VisitorsCountLabel.Text = "5,274";
            // 
            // _VisitorsCountLabelLabel
            // 
            _VisitorsCountLabelLabel.AutoSize = true;
            _VisitorsCountLabelLabel.Font = new Font("Microsoft Sans Serif", 14.25f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _VisitorsCountLabelLabel.ForeColor = Color.White;
            _VisitorsCountLabelLabel.Location = new Point(55, 20);
            _VisitorsCountLabelLabel.Name = "_VisitorsCountLabelLabel";
            _VisitorsCountLabelLabel.Size = new Size(70, 24);
            _VisitorsCountLabelLabel.TabIndex = 0;
            _VisitorsCountLabelLabel.Text = "Visitors";
            // 
            // _AdminPanel
            // 
            _AdminPanel.BackColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(240)), Conversions.ToInteger(Conversions.ToByte(130)), Conversions.ToInteger(Conversions.ToByte(40)));
            _AdminPanel.Controls.Add(_AdminNameLabel);
            _AdminPanel.Controls.Add(_AdminLabelLabel);
            _AdminPanel.Controls.Add(_AdminIconPanel);
            _AdminPanel.Location = new Point(251, 190);
            _AdminPanel.Name = "_AdminPanel";
            _AdminPanel.Size = new Size(341, 130);
            _AdminPanel.TabIndex = 4;
            // 
            // _AdminNameLabel
            // 
            _AdminNameLabel.AutoSize = true;
            _AdminNameLabel.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _AdminNameLabel.ForeColor = Color.White;
            _AdminNameLabel.Location = new Point(195, 69);
            _AdminNameLabel.Name = "_AdminNameLabel";
            _AdminNameLabel.Size = new Size(81, 16);
            _AdminNameLabel.TabIndex = 2;
            _AdminNameLabel.Text = "Pritam Zope";
            // 
            // _AdminLabelLabel
            // 
            _AdminLabelLabel.AutoSize = true;
            _AdminLabelLabel.Font = new Font("Microsoft Sans Serif", 14.25f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _AdminLabelLabel.ForeColor = Color.White;
            _AdminLabelLabel.Location = new Point(202, 19);
            _AdminLabelLabel.Name = "_AdminLabelLabel";
            _AdminLabelLabel.Size = new Size(65, 24);
            _AdminLabelLabel.TabIndex = 1;
            _AdminLabelLabel.Text = "Admin";
            // 
            // _AdminIconPanel
            // 
            _AdminIconPanel.BackgroundImage = My.Resources.Resources.pritam12345;
            _AdminIconPanel.Location = new Point(0, 0);
            _AdminIconPanel.Name = "_AdminIconPanel";
            _AdminIconPanel.Size = new Size(152, 130);
            _AdminIconPanel.TabIndex = 0;
            // 
            // _StoragePanel
            // 
            _StoragePanel.BackColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(200)), Conversions.ToInteger(Conversions.ToByte(40)), Conversions.ToInteger(Conversions.ToByte(30)));
            _StoragePanel.Controls.Add(_TotalStorageLabel);
            _StoragePanel.Controls.Add(_TotalStorageLabelLabel);
            _StoragePanel.Controls.Add(_FreeStorageLabel);
            _StoragePanel.Controls.Add(_FreeStorageLabelLabel);
            _StoragePanel.Controls.Add(_UsedStorageLabel);
            _StoragePanel.Controls.Add(_UsedStorageLabelLabel);
            _StoragePanel.Controls.Add(_StorageTopPanel);
            _StoragePanel.Location = new Point(251, 337);
            _StoragePanel.Name = "_StoragePanel";
            _StoragePanel.Size = new Size(341, 151);
            _StoragePanel.TabIndex = 7;
            // 
            // _TotalStorageLabel
            // 
            _TotalStorageLabel.AutoSize = true;
            _TotalStorageLabel.Font = new Font("Microsoft Sans Serif", 12.0f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _TotalStorageLabel.ForeColor = Color.White;
            _TotalStorageLabel.Location = new Point(268, 103);
            _TotalStorageLabel.Name = "_TotalStorageLabel";
            _TotalStorageLabel.Size = new Size(55, 20);
            _TotalStorageLabel.TabIndex = 6;
            _TotalStorageLabel.Text = "50 GB";
            // 
            // _TotalStorageLabelLabel
            // 
            _TotalStorageLabelLabel.AutoSize = true;
            _TotalStorageLabelLabel.Font = new Font("Microsoft Sans Serif", 12.75f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _TotalStorageLabelLabel.ForeColor = Color.White;
            _TotalStorageLabelLabel.Location = new Point(268, 64);
            _TotalStorageLabelLabel.Name = "_TotalStorageLabelLabel";
            _TotalStorageLabelLabel.Size = new Size(46, 20);
            _TotalStorageLabelLabel.TabIndex = 5;
            _TotalStorageLabelLabel.Text = "Total";
            // 
            // _FreeStorageLabel
            // 
            _FreeStorageLabel.AutoSize = true;
            _FreeStorageLabel.Font = new Font("Microsoft Sans Serif", 12.0f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _FreeStorageLabel.ForeColor = Color.White;
            _FreeStorageLabel.Location = new Point(146, 103);
            _FreeStorageLabel.Name = "_FreeStorageLabel";
            _FreeStorageLabel.Size = new Size(68, 20);
            _FreeStorageLabel.TabIndex = 4;
            _FreeStorageLabel.Text = "26.5 GB";
            // 
            // _FreeStorageLabelLabel
            // 
            _FreeStorageLabelLabel.AutoSize = true;
            _FreeStorageLabelLabel.Font = new Font("Microsoft Sans Serif", 12.75f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _FreeStorageLabelLabel.ForeColor = Color.White;
            _FreeStorageLabelLabel.Location = new Point(157, 64);
            _FreeStorageLabelLabel.Name = "_FreeStorageLabelLabel";
            _FreeStorageLabelLabel.Size = new Size(43, 20);
            _FreeStorageLabelLabel.TabIndex = 3;
            _FreeStorageLabelLabel.Text = "Free";
            // 
            // _UsedStorageLabel
            // 
            _UsedStorageLabel.AutoSize = true;
            _UsedStorageLabel.Font = new Font("Microsoft Sans Serif", 12.0f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _UsedStorageLabel.ForeColor = Color.White;
            _UsedStorageLabel.Location = new Point(27, 103);
            _UsedStorageLabel.Name = "_UsedStorageLabel";
            _UsedStorageLabel.Size = new Size(68, 20);
            _UsedStorageLabel.TabIndex = 2;
            _UsedStorageLabel.Text = "23.5 GB";
            // 
            // _UsedStorageLabelLabel
            // 
            _UsedStorageLabelLabel.AutoSize = true;
            _UsedStorageLabelLabel.Font = new Font("Microsoft Sans Serif", 12.75f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _UsedStorageLabelLabel.ForeColor = Color.White;
            _UsedStorageLabelLabel.Location = new Point(35, 64);
            _UsedStorageLabelLabel.Name = "_UsedStorageLabelLabel";
            _UsedStorageLabelLabel.Size = new Size(48, 20);
            _UsedStorageLabelLabel.TabIndex = 1;
            _UsedStorageLabelLabel.Text = "Used";
            // 
            // _StorageTopPanel
            // 
            _StorageTopPanel.BackColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(120)), Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(0)));
            _StorageTopPanel.Controls.Add(_StoratgeTitleLabel);
            _StorageTopPanel.Location = new Point(0, 0);
            _StorageTopPanel.Name = "_StorageTopPanel";
            _StorageTopPanel.Size = new Size(341, 49);
            _StorageTopPanel.TabIndex = 0;
            // 
            // _StoratgeTitleLabel
            // 
            _StoratgeTitleLabel.AutoSize = true;
            _StoratgeTitleLabel.Font = new Font("Microsoft Sans Serif", 15.0f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _StoratgeTitleLabel.ForeColor = Color.White;
            _StoratgeTitleLabel.Location = new Point(119, 11);
            _StoratgeTitleLabel.Name = "_StoratgeTitleLabel";
            _StoratgeTitleLabel.Size = new Size(81, 25);
            _StoratgeTitleLabel.TabIndex = 0;
            _StoratgeTitleLabel.Text = "Storage";
            // 
            // _ControlPanel
            // 
            _ControlPanel.BackColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(100)), Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(120)));
            _ControlPanel.Controls.Add(_ContrlPanelLabel);
            _ControlPanel.Location = new Point(598, 385);
            _ControlPanel.Name = "_ControlPanel";
            _ControlPanel.Size = new Size(240, 103);
            _ControlPanel.TabIndex = 8;
            // 
            // _ContrlPanelLabel
            // 
            _ContrlPanelLabel.AutoSize = true;
            _ContrlPanelLabel.Font = new Font("Microsoft Sans Serif", 15.75f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _ContrlPanelLabel.ForeColor = Color.White;
            _ContrlPanelLabel.Location = new Point(48, 40);
            _ContrlPanelLabel.Name = "_ContrlPanelLabel";
            _ContrlPanelLabel.Size = new Size(142, 25);
            _ContrlPanelLabel.TabIndex = 0;
            _ContrlPanelLabel.Text = "Control Panel";
            // 
            // _DeleteBlogButton
            // 
            _DeleteBlogButton.BackColorActive = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(180)), Conversions.ToInteger(Conversions.ToByte(50)), Conversions.ToInteger(Conversions.ToByte(60)));
            _DeleteBlogButton.Cursor = Cursors.Hand;
            _DeleteBlogButton.DisplayText = "Delete Blog";
            _DeleteBlogButton.FlatStyle = FlatStyle.Flat;
            _DeleteBlogButton.Font = new Font("Microsoft YaHei UI", 12.75f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            _DeleteBlogButton.ForeColor = Color.White;
            _DeleteBlogButton.Location = new Point(598, 260);
            _DeleteBlogButton.BackColorMouseClick = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(100)), Conversions.ToInteger(Conversions.ToByte(10)), Conversions.ToInteger(Conversions.ToByte(10)));
            _DeleteBlogButton.MouseColorsEnabled = true;
            _DeleteBlogButton.BackColorMouseHover = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(150)), Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(30)));
            _DeleteBlogButton.Name = "_DeleteBlogButton";
            _DeleteBlogButton.Size = new Size(240, 60);
            _DeleteBlogButton.TabIndex = 6;
            _DeleteBlogButton.Text = "Delete Blog";
            _DeleteBlogButton.TextLocationLeft = 65;
            _DeleteBlogButton.TextLocationTop = 16;
            _DeleteBlogButton.UseVisualStyleBackColor = true;
            // 
            // _ViewBlogButton
            // 
            _ViewBlogButton.BackColorActive = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(130)), Conversions.ToInteger(Conversions.ToByte(110)));
            _ViewBlogButton.Cursor = Cursors.Hand;
            _ViewBlogButton.DisplayText = "View Blog";
            _ViewBlogButton.FlatStyle = FlatStyle.Flat;
            _ViewBlogButton.Font = new Font("Microsoft YaHei UI", 14.25f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            _ViewBlogButton.ForeColor = Color.White;
            _ViewBlogButton.Location = new Point(599, 191);
            _ViewBlogButton.BackColorMouseClick = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(10)), Conversions.ToInteger(Conversions.ToByte(80)), Conversions.ToInteger(Conversions.ToByte(50)));
            _ViewBlogButton.MouseColorsEnabled = true;
            _ViewBlogButton.BackColorMouseHover = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(10)), Conversions.ToInteger(Conversions.ToByte(110)), Conversions.ToInteger(Conversions.ToByte(90)));
            _ViewBlogButton.Name = "_ViewBlogButton";
            _ViewBlogButton.Size = new Size(240, 60);
            _ViewBlogButton.TabIndex = 5;
            _ViewBlogButton.Text = "View Blog";
            _ViewBlogButton.TextLocationLeft = 65;
            _ViewBlogButton.TextLocationTop = 16;
            _ViewBlogButton.UseVisualStyleBackColor = true;
            // 
            // _CampainsPanel
            // 
            _CampainsPanel.BackColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(10)), Conversions.ToInteger(Conversions.ToByte(40)), Conversions.ToInteger(Conversions.ToByte(80)));
            _CampainsPanel.Controls.Add(_CampaignsLabel);
            _CampainsPanel.Location = new Point(599, 337);
            _CampainsPanel.Name = "_CampainsPanel";
            _CampainsPanel.Size = new Size(239, 42);
            _CampainsPanel.TabIndex = 9;
            // 
            // _CampaignsLabel
            // 
            _CampaignsLabel.AutoSize = true;
            _CampaignsLabel.Font = new Font("Microsoft Sans Serif", 12.0f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _CampaignsLabel.ForeColor = Color.White;
            _CampaignsLabel.Location = new Point(73, 11);
            _CampaignsLabel.Name = "_CampaignsLabel";
            _CampaignsLabel.Size = new Size(89, 20);
            _CampaignsLabel.TabIndex = 0;
            _CampaignsLabel.Text = "Campaigns";
            // 
            // DashboardForm
            // 
            AutoScaleDimensions = new SizeF(6.0f, 13.0f);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = Color.Gainsboro;
            ClientSize = new Size(873, 515);
            Controls.Add(_CampainsPanel);
            Controls.Add(_ControlPanel);
            Controls.Add(_StoragePanel);
            Controls.Add(_DeleteBlogButton);
            Controls.Add(_ViewBlogButton);
            Controls.Add(_AdminPanel);
            Controls.Add(_VisitorsPanel);
            Controls.Add(_LeftPanel);
            Controls.Add(__TopPanel);
            Controls.Add(_LeftTopPanel);
            FormBorderStyle = FormBorderStyle.None;
            Name = "DashboardForm";
            Text = "Dashboard";
            _LeftTopPanel.ResumeLayout(false);
            _LeftTopPanel.PerformLayout();
            __TopPanel.ResumeLayout(false);
            __TopPanel.PerformLayout();
            _LeftPanel.ResumeLayout(false);
            _LeftBottomPanel.ResumeLayout(false);
            _LeftBottomPanel.PerformLayout();
            _VisitorsPanel.ResumeLayout(false);
            _VisitorsPanel.PerformLayout();
            _VisitorsCountPanel.ResumeLayout(false);
            _VisitorsCountPanel.PerformLayout();
            _AdminPanel.ResumeLayout(false);
            _AdminPanel.PerformLayout();
            _StoragePanel.ResumeLayout(false);
            _StoragePanel.PerformLayout();
            _StorageTopPanel.ResumeLayout(false);
            _StorageTopPanel.PerformLayout();
            _ControlPanel.ResumeLayout(false);
            _ControlPanel.PerformLayout();
            _CampainsPanel.ResumeLayout(false);
            _CampainsPanel.PerformLayout();
            ResumeLayout(false);
        }

        private Panel _LeftTopPanel;
        private Panel __TopPanel;

        private Panel _TopPanel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __TopPanel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__TopPanel != null)
                {
                    __TopPanel.MouseDown -= TopPanel_MouseDown;
                    __TopPanel.MouseMove -= TopPanel_MouseMove;
                    __TopPanel.MouseUp -= TopPanel_MouseUp;
                }

                __TopPanel = value;
                if (__TopPanel != null)
                {
                    __TopPanel.MouseDown += TopPanel_MouseDown;
                    __TopPanel.MouseMove += TopPanel_MouseMove;
                    __TopPanel.MouseUp += TopPanel_MouseUp;
                }
            }
        }

        private ZopeButton __CloseButton;

        private ZopeButton _CloseButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __CloseButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__CloseButton != null)
                {
                    __CloseButton.Click -= CloseButton_Click;
                }

                __CloseButton = value;
                if (__CloseButton != null)
                {
                    __CloseButton.Click += CloseButton_Click;
                }
            }
        }

        private ZopeButton __MinButton;

        private ZopeButton _MinButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __MinButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__MinButton != null)
                {
                    __MinButton.Click -= MinButton_Click;
                }

                __MinButton = value;
                if (__MinButton != null)
                {
                    __MinButton.Click += MinButton_Click;
                }
            }
        }

        private Label _LeftTopPanelLabel;
        private Panel _IconPanel;
        private Label __DashboardLabel;

        private Label _DashboardLabel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __DashboardLabel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__DashboardLabel != null)
                {
                    __DashboardLabel.MouseDown -= DashboardLabel_mouseDown;
                    __DashboardLabel.MouseMove -= DashboardLabel_mouseMove;
                    __DashboardLabel.MouseUp -= DashboardLabel_mouseUp;
                }

                __DashboardLabel = value;
                if (__DashboardLabel != null)
                {
                    __DashboardLabel.MouseDown += DashboardLabel_mouseDown;
                    __DashboardLabel.MouseMove += DashboardLabel_mouseMove;
                    __DashboardLabel.MouseUp += DashboardLabel_mouseUp;
                }
            }
        }

        private Panel _LeftPanel;
        private ZopeButton __DashboardButton;

        private ZopeButton _DashboardButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __DashboardButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__DashboardButton != null)
                {
                    __DashboardButton.Click -= Dashboard_button_Click;
                }

                __DashboardButton = value;
                if (__DashboardButton != null)
                {
                    __DashboardButton.Click += Dashboard_button_Click;
                }
            }
        }

        private ZopeButton __StatsButton;

        private ZopeButton _StatsButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __StatsButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__StatsButton != null)
                {
                    __StatsButton.Click -= Stats_button_Click;
                }

                __StatsButton = value;
                if (__StatsButton != null)
                {
                    __StatsButton.Click += Stats_button_Click;
                }
            }
        }

        private ZopeButton __LayoutButton;

        private ZopeButton _LayoutButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __LayoutButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__LayoutButton != null)
                {
                    __LayoutButton.Click -= Layout_button_Click;
                }

                __LayoutButton = value;
                if (__LayoutButton != null)
                {
                    __LayoutButton.Click += Layout_button_Click;
                }
            }
        }

        private ZopeButton __PagesButton;

        private ZopeButton _PagesButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __PagesButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__PagesButton != null)
                {
                    __PagesButton.Click -= Pages_button_Click;
                }

                __PagesButton = value;
                if (__PagesButton != null)
                {
                    __PagesButton.Click += Pages_button_Click;
                }
            }
        }

        private ZopeButton __SettingsButton;

        private ZopeButton _SettingsButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SettingsButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SettingsButton != null)
                {
                    __SettingsButton.Click -= Settings_button_Click;
                }

                __SettingsButton = value;
                if (__SettingsButton != null)
                {
                    __SettingsButton.Click += Settings_button_Click;
                }
            }
        }

        private ZopeButton __ThemeButton;

        private ZopeButton _ThemeButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ThemeButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ThemeButton != null)
                {
                    __ThemeButton.Click -= Theme_button_Click;
                }

                __ThemeButton = value;
                if (__ThemeButton != null)
                {
                    __ThemeButton.Click += Theme_button_Click;
                }
            }
        }

        private Panel _LeftBottomPanel;
        private Label _LeftBottomPanelLabel;
        private Panel _VisitorsPanel;
        private Panel _VisitorsCountPanel;
        private Label _VisitorsCountLabel;
        private Label _VisitorsCountLabelLabel;
        private Label _MonthVisitorsCountLabel;
        private Label _MonthVisitorsCountLabelLabel;
        private Label _TodayVisitorsCountLabel;
        private Label _TodayVisitorsCountLabelLabel;
        private Panel _AdminPanel;
        private Label _AdminNameLabel;
        private Label _AdminLabelLabel;
        private Panel _AdminIconPanel;
        private ZopeButton _ViewBlogButton;
        private ZopeButton _DeleteBlogButton;
        private Panel _StoragePanel;
        private Label _TotalStorageLabel;
        private Label _TotalStorageLabelLabel;
        private Label _FreeStorageLabel;
        private Label _FreeStorageLabelLabel;
        private Label _UsedStorageLabel;
        private Label _UsedStorageLabelLabel;
        private Panel _StorageTopPanel;
        private Label _StoratgeTitleLabel;
        private Panel _ControlPanel;
        private Label _ContrlPanelLabel;
        private Panel _CampainsPanel;
        private Label _CampaignsLabel;
    }
}
