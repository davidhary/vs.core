﻿/// <summary> Form for viewing the dashboard. </summary>
/// <remarks> David, 2021-03-12. </remarks>
using System.Drawing;
using System.Windows.Forms;

namespace isr.Core.Tester
{
    /// <summary>   Form for viewing the dashboard. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    public partial class DashboardForm : Form
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Windows.Forms.Form" /> class.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        public DashboardForm()
        {

            /// <summary> Dashboard form load. </summary>
    /// <remarks> David, 2021-03-12. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
            base.Load += this.Dashboard_Form_Load;
            this.InitializeComponent();
        }
        /// <summary>   The offset. </summary>

        private Point _Offset;
        /// <summary>   True if is top panel dragged, false if not. </summary>
        private bool _IsTopPanelDragged = false;

        /// <summary>   Event handler. Called by Dashboard_Form for load events. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void Dashboard_Form_Load(object sender, System.EventArgs e)
        {
            this.Dashboard_button_Click(sender, e);
        }

        /// <summary>   Top panel mouse down. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void TopPanel_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this._IsTopPanelDragged = true;
                var pointStartPosition = this.PointToScreen(new Point(e.X, e.Y));
                this._Offset = new Point()
                {
                    X = this.Location.X - (pointStartPosition.X + this._LeftTopPanel.Size.Width),
                    Y = this.Location.Y - pointStartPosition.Y
                };
            }
            else
            {
                this._IsTopPanelDragged = false;
            }
        }

        /// <summary>   Top panel mouse move. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void TopPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if ( this._IsTopPanelDragged )
            {
                var newPoint = this._TopPanel.PointToScreen(new Point(e.X, e.Y));
                newPoint.Offset( this._Offset );
                this.Location = newPoint;
            }
        }

        /// <summary>   Top panel mouse up. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void TopPanel_MouseUp(object sender, MouseEventArgs e)
        {
            this._IsTopPanelDragged = false;
        }

        /// <summary>   Closes button click. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void CloseButton_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }

        /// <summary>   Minimum button click. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void MinButton_Click(object sender, System.EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        /// <summary>   Dashboard label mouse down. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void DashboardLabel_mouseDown(object sender, MouseEventArgs e)
        {
            this.TopPanel_MouseDown(sender, e);
        }

        /// <summary>   Dashboard label mouse move. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void DashboardLabel_mouseMove(object sender, MouseEventArgs e)
        {
            this.TopPanel_MouseMove(sender, e);
        }

        /// <summary>   Dashboard label mouse up. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void DashboardLabel_mouseUp(object sender, MouseEventArgs e)
        {
            this.TopPanel_MouseUp(sender, e);
        }

        /// <summary>   Dashboard button click. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void Dashboard_button_Click(object sender, System.EventArgs e)
        {
            this._DashboardButton.BackColorActive = Color.FromArgb(20, 20, 20);
            this._DashboardButton.MouseColorsEnabled = false;
            this._StatsButton.BackColorActive = Color.FromArgb(30, 30, 40);
            this._PagesButton.BackColorActive = Color.FromArgb(30, 30, 40);
            this._LayoutButton.BackColorActive = Color.FromArgb(30, 30, 40);
            this._ThemeButton.BackColorActive = Color.FromArgb(30, 30, 40);
            this._SettingsButton.BackColorActive = Color.FromArgb(30, 30, 40);
            this._StatsButton.MouseColorsEnabled = true;
            this._PagesButton.MouseColorsEnabled = true;
            this._LayoutButton.MouseColorsEnabled = true;
            this._ThemeButton.MouseColorsEnabled = true;
            this._SettingsButton.MouseColorsEnabled = true;
        }

        /// <summary>   Statistics button click. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void Stats_button_Click(object sender, System.EventArgs e)
        {
            this._StatsButton.BackColorActive = Color.FromArgb(20, 20, 20);
            this._StatsButton.MouseColorsEnabled = false;
            this._DashboardButton.BackColorActive = Color.FromArgb(30, 30, 40);
            this._PagesButton.BackColorActive = Color.FromArgb(30, 30, 40);
            this._LayoutButton.BackColorActive = Color.FromArgb(30, 30, 40);
            this._ThemeButton.BackColorActive = Color.FromArgb(30, 30, 40);
            this._SettingsButton.BackColorActive = Color.FromArgb(30, 30, 40);
            this._DashboardButton.MouseColorsEnabled = true;
            this._PagesButton.MouseColorsEnabled = true;
            this._LayoutButton.MouseColorsEnabled = true;
            this._ThemeButton.MouseColorsEnabled = true;
            this._SettingsButton.MouseColorsEnabled = true;
        }

        /// <summary>   Pages button click. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void Pages_button_Click(object sender, System.EventArgs e)
        {
            this._PagesButton.BackColorActive = Color.FromArgb(20, 20, 20);
            this._PagesButton.MouseColorsEnabled = false;
            this._DashboardButton.BackColorActive = Color.FromArgb(30, 30, 40);
            this._StatsButton.BackColorActive = Color.FromArgb(30, 30, 40);
            this._LayoutButton.BackColorActive = Color.FromArgb(30, 30, 40);
            this._ThemeButton.BackColorActive = Color.FromArgb(30, 30, 40);
            this._SettingsButton.BackColorActive = Color.FromArgb(30, 30, 40);
            this._DashboardButton.MouseColorsEnabled = true;
            this._StatsButton.MouseColorsEnabled = true;
            this._LayoutButton.MouseColorsEnabled = true;
            this._ThemeButton.MouseColorsEnabled = true;
            this._SettingsButton.MouseColorsEnabled = true;
        }

        /// <summary>   Layout button click. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void Layout_button_Click(object sender, System.EventArgs e)
        {
            this._LayoutButton.BackColorActive = Color.FromArgb(20, 20, 20);
            this._LayoutButton.MouseColorsEnabled = false;
            this._DashboardButton.BackColorActive = Color.FromArgb(30, 30, 40);
            this._StatsButton.BackColorActive = Color.FromArgb(30, 30, 40);
            this._PagesButton.BackColorActive = Color.FromArgb(30, 30, 40);
            this._ThemeButton.BackColorActive = Color.FromArgb(30, 30, 40);
            this._SettingsButton.BackColorActive = Color.FromArgb(30, 30, 40);
            this._DashboardButton.MouseColorsEnabled = true;
            this._StatsButton.MouseColorsEnabled = true;
            this._PagesButton.MouseColorsEnabled = true;
            this._ThemeButton.MouseColorsEnabled = true;
            this._SettingsButton.MouseColorsEnabled = true;
        }

        /// <summary>   Theme button click. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void Theme_button_Click(object sender, System.EventArgs e)
        {
            this._ThemeButton.BackColorActive = Color.FromArgb(20, 20, 20);
            this._ThemeButton.MouseColorsEnabled = false;
            this._DashboardButton.BackColorActive = Color.FromArgb(30, 30, 40);
            this._StatsButton.BackColorActive = Color.FromArgb(30, 30, 40);
            this._PagesButton.BackColorActive = Color.FromArgb(30, 30, 40);
            this._LayoutButton.BackColorActive = Color.FromArgb(30, 30, 40);
            this._SettingsButton.BackColorActive = Color.FromArgb(30, 30, 40);
            this._DashboardButton.MouseColorsEnabled = true;
            this._StatsButton.MouseColorsEnabled = true;
            this._PagesButton.MouseColorsEnabled = true;
            this._LayoutButton.MouseColorsEnabled = true;
            this._SettingsButton.MouseColorsEnabled = true;
        }

        /// <summary>   Settings button click. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void Settings_button_Click(object sender, System.EventArgs e)
        {
            this._SettingsButton.BackColorActive = Color.FromArgb(20, 20, 20);
            this._SettingsButton.MouseColorsEnabled = false;
            this._DashboardButton.BackColorActive = Color.FromArgb(30, 30, 40);
            this._StatsButton.BackColorActive = Color.FromArgb(30, 30, 40);
            this._PagesButton.BackColorActive = Color.FromArgb(30, 30, 40);
            this._LayoutButton.BackColorActive = Color.FromArgb(30, 30, 40);
            this._ThemeButton.BackColorActive = Color.FromArgb(30, 30, 40);
            this._DashboardButton.MouseColorsEnabled = true;
            this._StatsButton.MouseColorsEnabled = true;
            this._PagesButton.MouseColorsEnabled = true;
            this._LayoutButton.MouseColorsEnabled = true;
            this._ThemeButton.MouseColorsEnabled = true;
        }
    }
}