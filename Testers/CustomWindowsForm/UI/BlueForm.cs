﻿/// <summary> Default constructor. </summary>
/// <remarks> David, 2021-03-12. </remarks>
using System.Drawing;
using System.Windows.Forms;
using isr.Core.Controls;

namespace isr.Core.Tester
{
    /// <summary>   Form for viewing the blue. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    public partial class BlueForm : Form
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Windows.Forms.Form" /> class.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        public BlueForm()
        {
            this.InitializeComponent();
        }
        /// <summary>   True if is top panel dragged, false if not. </summary>


        private bool _IsTopPanelDragged = false;
        /// <summary>   True if is left panel dragged, false if not. </summary>
        private bool _IsLeftPanelDragged = false;
        /// <summary>   True if is right panel dragged, false if not. </summary>
        private bool _IsRightPanelDragged = false;
        /// <summary>   True if is bottom panel dragged, false if not. </summary>
        private bool _IsBottomPanelDragged = false;
        /// <summary>   True if is top border panel dragged, false if not. </summary>
        private bool _IsTopBorderPanelDragged = false;
        /// <summary>   True if is window maximized, false if not. </summary>
        private bool _IsWindowMaximized = false;
        /// <summary>   The offset. </summary>
        private Point _Offset;
        /// <summary>   Size of the normal window. </summary>
        private Size _NormalWindowSize;
        /// <summary>   The normal window location. </summary>
        private Point _NormalWindowLocation = Point.Empty;

        /// <summary>   Top border panel mouse down. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void TopBorderPanel_MouseDown(object sender, MouseEventArgs e)
        {
            this._IsTopBorderPanelDragged = e.Button == MouseButtons.Left;
        }

        /// <summary>   Top border panel mouse move. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void TopBorderPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Y < this.Location.Y)
            {
                if ( this._IsTopBorderPanelDragged )
                {
                    if ( this.Height < 50)
                    {
                        this.Height = 50;
                        this._IsTopBorderPanelDragged = false;
                    }
                    else
                    {
                        this.Location = new Point( this.Location.X, this.Location.Y + e.Y);
                        this.Height -= e.Y;
                    }
                }
            }
        }

        /// <summary>   Top border panel mouse up. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void TopBorderPanel_MouseUp(object sender, MouseEventArgs e)
        {
            this._IsTopBorderPanelDragged = false;
        }

        /// <summary>   Top panel mouse down. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void TopPanel_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this._IsTopPanelDragged = true;
                var pointStartPosition = this.PointToScreen(new Point(e.X, e.Y));
                this._Offset = new Point()
                {
                    X = this.Location.X - pointStartPosition.X,
                    Y = this.Location.Y - pointStartPosition.Y
                };
            }
            else
            {
                this._IsTopPanelDragged = false;
            }

            if (e.Clicks == 2)
            {
                this._IsTopPanelDragged = false;
                this.MaxButton_Click(sender, e);
            }
        }

        /// <summary>   Top panel mouse move. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void TopPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if ( this._IsTopPanelDragged )
            {
                var newPoint = this._TopPanel.PointToScreen(new Point(e.X, e.Y));
                newPoint.Offset( this._Offset );
                this.Location = newPoint;
                if ( this.Location.X > 2 || this.Location.Y > 2)
                {
                    if ( this.WindowState == FormWindowState.Maximized)
                    {
                        this.Location = this._NormalWindowLocation;
                        this.Size = this._NormalWindowSize;
                        this._ToolTip.SetToolTip( this._MaxButton, "Maximize");
                        this._MaxButton.CustomFormState = CustomFormState.Normal;
                        this._IsWindowMaximized = false;
                    }
                }
            }
        }

        /// <summary>   Top panel mouse up. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void TopPanel_MouseUp(object sender, MouseEventArgs e)
        {
            this._IsTopPanelDragged = false;
            if ( this.Location.Y <= 5)
            {
                if (!this._IsWindowMaximized )
                {
                    this._NormalWindowSize = this.Size;
                    this._NormalWindowLocation = this.Location;
                    var rect = Screen.PrimaryScreen.WorkingArea;
                    this.Location = new Point(0, 0);
                    this.Size = new Size(rect.Width, rect.Height);
                    this._ToolTip.SetToolTip( this._MaxButton, "Restore Down");
                    this._MaxButton.CustomFormState = CustomFormState.Maximize;
                    this._IsWindowMaximized = true;
                }
            }
        }

        /// <summary>   Left panel mouse down. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void LeftPanel_MouseDown(object sender, MouseEventArgs e)
        {
            if ( this.Location.X <= 0 || e.X < 0)
            {
                this._IsLeftPanelDragged = false;
                this.Location = new Point(10, this.Location.Y);
            }
            else
            {
                this._IsLeftPanelDragged = e.Button == MouseButtons.Left;
            }
        }

        /// <summary>   Left panel mouse move. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void LeftPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.X < this.Location.X)
            {
                if ( this._IsLeftPanelDragged )
                {
                    if ( this.Width < 100)
                    {
                        this.Width = 100;
                        this._IsLeftPanelDragged = false;
                    }
                    else
                    {
                        this.Location = new Point( this.Location.X + e.X, this.Location.Y);
                        this.Width -= e.X;
                    }
                }
            }
        }

        /// <summary>   Left panel mouse up. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void LeftPanel_MouseUp(object sender, MouseEventArgs e)
        {
            this._IsLeftPanelDragged = false;
        }

        /// <summary>   Right panel mouse down. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void RightPanel_MouseDown(object sender, MouseEventArgs e)
        {
            this._IsRightPanelDragged = e.Button == MouseButtons.Left;
        }

        /// <summary>   Right panel mouse move. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void RightPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if ( this._IsRightPanelDragged )
            {
                if ( this.Width < 100)
                {
                    this.Width = 100;
                    this._IsRightPanelDragged = false;
                }
                else
                {
                    this.Width += e.X;
                }
            }
        }

        /// <summary>   Right panel mouse up. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void RightPanel_MouseUp(object sender, MouseEventArgs e)
        {
            this._IsRightPanelDragged = false;
        }

        /// <summary>   Bottom panel mouse down. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void BottomPanel_MouseDown(object sender, MouseEventArgs e)
        {
            this._IsBottomPanelDragged = e.Button == MouseButtons.Left;
        }

        /// <summary>   Bottom panel mouse move. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void BottomPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if ( this._IsBottomPanelDragged )
            {
                if ( this.Height < 50)
                {
                    this.Height = 50;
                    this._IsBottomPanelDragged = false;
                }
                else
                {
                    this.Height += e.Y;
                }
            }
        }

        /// <summary>   Bottom panel mouse up. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void BottomPanel_MouseUp(object sender, MouseEventArgs e)
        {
            this._IsBottomPanelDragged = false;
        }

        /// <summary>   Closes button click. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void CloseButton_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }

        /// <summary>   Maximum button click. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void MaxButton_Click(object sender, System.EventArgs e)
        {
            if ( this._IsWindowMaximized )
            {
                this.Location = this._NormalWindowLocation;
                this.Size = this._NormalWindowSize;
                this._ToolTip.SetToolTip( this._MaxButton, "Maximize");
                this._MaxButton.CustomFormState = CustomFormState.Normal;
                this._IsWindowMaximized = false;
            }
            else
            {
                this._NormalWindowSize = this.Size;
                this._NormalWindowLocation = this.Location;
                var rect = Screen.PrimaryScreen.WorkingArea;
                this.Location = new Point(0, 0);
                this.Size = new Size(rect.Width, rect.Height);
                this._ToolTip.SetToolTip( this._MaxButton, "Restore Down");
                this._MaxButton.CustomFormState = CustomFormState.Maximize;
                this._IsWindowMaximized = true;
            }
        }

        /// <summary>   Minimum button click. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void MinButton_Click(object sender, System.EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
    }
}