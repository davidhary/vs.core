using System.Drawing;
using System.Windows.Forms;
using isr.Core.Controls;

namespace isr.Core.Tester
{
    /// <summary>   Form for viewing the black. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    public partial class BlackForm : Form
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Windows.Forms.Form" /> class.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        public BlackForm()
        {
            this.InitializeComponent();
        }
        /// <summary>   True if is top panel dragged, false if not. </summary>

        private bool _IsTopPanelDragged = false;
        /// <summary>   True if is left panel dragged, false if not. </summary>
        private bool _IsLeftPanelDragged = false;
        /// <summary>   True if is right panel dragged, false if not. </summary>
        private bool _IsRightPanelDragged = false;
        /// <summary>   True if is bottom panel dragged, false if not. </summary>
        private bool _IsBottomPanelDragged = false;
        /// <summary>   True if is top border panel dragged, false if not. </summary>
        private bool _IsTopBorderPanelDragged = false;
        /// <summary>   True if is right bottom panel dragged, false if not. </summary>

        private bool _IsRightBottomPanelDragged = false;
        /// <summary>   True if is left bottom panel dragged, false if not. </summary>
        private bool _IsLeftBottomPanelDragged = false;
        /// <summary>   True if is right top panel dragged, false if not. </summary>
        private bool _IsRightTopPanelDragged = false;
        /// <summary>   True if is left top panel dragged, false if not. </summary>
        private bool _IsLeftTopPanelDragged = false;
        /// <summary>   True if is window maximized, false if not. </summary>

        private bool _IsWindowMaximized = false;
        /// <summary>   The offset. </summary>
        private Point _Offset;
        /// <summary>   Size of the normal window. </summary>
        private Size _NormalWindowSize;
        /// <summary>   The normal window location. </summary>
        private Point _NormalWindowLocation = Point.Empty;

        /// <summary>   Top border panel mouse down. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void TopBorderPanel_MouseDown(object sender, MouseEventArgs e)
        {
            this._IsTopBorderPanelDragged = e.Button == MouseButtons.Left;
        }

        /// <summary>   Top border panel mouse move. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void TopBorderPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Y < this.Location.Y)
            {
                if ( this._IsTopBorderPanelDragged )
                {
                    if ( this.Height < 50)
                    {
                        this.Height = 50;
                        this._IsTopBorderPanelDragged = false;
                    }
                    else
                    {
                        this.Location = new Point( this.Location.X, this.Location.Y + e.Y);
                        this.Height -= e.Y;
                    }
                }
            }
        }

        /// <summary>   Top border panel mouse up. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void TopBorderPanel_MouseUp(object sender, MouseEventArgs e)
        {
            this._IsTopBorderPanelDragged = false;
        }

        /// <summary>   Top panel mouse down. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void TopPanel_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this._IsTopPanelDragged = true;
                var pointStartPosition = this.PointToScreen(new Point(e.X, e.Y));
                this._Offset = new Point()
                {
                    X = this.Location.X - pointStartPosition.X,
                    Y = this.Location.Y - pointStartPosition.Y
                };
            }
            else
            {
                this._IsTopPanelDragged = false;
            }

            if (e.Clicks == 2)
            {
                this._IsTopPanelDragged = false;
                this.MaxButton_Click(sender, e);
            }
        }

        /// <summary>   Top panel mouse move. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void TopPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if ( this._IsTopPanelDragged )
            {
                var newPoint = this._TopPanel.PointToScreen(new Point(e.X, e.Y));
                newPoint.Offset( this._Offset );
                this.Location = newPoint;
                if ( this.Location.X > 2 || this.Location.Y > 2)
                {
                    if ( this.WindowState == FormWindowState.Maximized)
                    {
                        this.Location = this._NormalWindowLocation;
                        this.Size = this._NormalWindowSize;
                        this._ToolTip.SetToolTip( this._MaxButton, "Maximize");
                        this._MaxButton.CustomFormState = CustomFormState.Normal;
                        this._IsWindowMaximized = false;
                    }
                }
            }
        }

        /// <summary>   Top panel mouse up. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void TopPanel_MouseUp(object sender, MouseEventArgs e)
        {
            this._IsTopPanelDragged = false;
            if ( this.Location.Y <= 5)
            {
                if (!this._IsWindowMaximized )
                {
                    this._NormalWindowSize = this.Size;
                    this._NormalWindowLocation = this.Location;
                    var rect = Screen.PrimaryScreen.WorkingArea;
                    this.Location = new Point(0, 0);
                    this.Size = new Size(rect.Width, rect.Height);
                    this._ToolTip.SetToolTip( this._MaxButton, "Restore Down");
                    this._MaxButton.CustomFormState = CustomFormState.Maximize;
                    this._IsWindowMaximized = true;
                }
            }
        }

        /// <summary>   Left panel mouse down. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void LeftPanel_MouseDown(object sender, MouseEventArgs e)
        {
            if ( this.Location.X <= 0 || e.X < 0)
            {
                this._IsLeftPanelDragged = false;
                this.Location = new Point(10, this.Location.Y);
            }
            else
            {
                this._IsLeftPanelDragged = e.Button == MouseButtons.Left;
            }
        }

        /// <summary>   Left panel mouse move. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void LeftPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.X < this.Location.X)
            {
                if ( this._IsLeftPanelDragged )
                {
                    if ( this.Width < 100)
                    {
                        this.Width = 100;
                        this._IsLeftPanelDragged = false;
                    }
                    else
                    {
                        this.Location = new Point( this.Location.X + e.X, this.Location.Y);
                        this.Width -= e.X;
                    }
                }
            }
        }

        /// <summary>   Left panel mouse up. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void LeftPanel_MouseUp(object sender, MouseEventArgs e)
        {
            this._IsLeftPanelDragged = false;
        }

        /// <summary>   Right panel mouse down. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void RightPanel_MouseDown(object sender, MouseEventArgs e)
        {
            this._IsRightPanelDragged = e.Button == MouseButtons.Left;
        }

        /// <summary>   Right panel mouse move. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void RightPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if ( this._IsRightPanelDragged )
            {
                if ( this.Width < 100)
                {
                    this.Width = 100;
                    this._IsRightPanelDragged = false;
                }
                else
                {
                    this.Width += e.X;
                }
            }
        }

        /// <summary>   Right panel mouse up. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void RightPanel_MouseUp(object sender, MouseEventArgs e)
        {
            this._IsRightPanelDragged = false;
        }

        /// <summary>   Bottom panel mouse down. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void BottomPanel_MouseDown(object sender, MouseEventArgs e)
        {
            this._IsBottomPanelDragged = e.Button == MouseButtons.Left;
        }

        /// <summary>   Bottom panel mouse move. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void BottomPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if ( this._IsBottomPanelDragged )
            {
                if ( this.Height < 50)
                {
                    this.Height = 50;
                    this._IsBottomPanelDragged = false;
                }
                else
                {
                    this.Height += e.Y;
                }
            }
        }

        /// <summary>   Bottom panel mouse up. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void BottomPanel_MouseUp(object sender, MouseEventArgs e)
        {
            this._IsBottomPanelDragged = false;
        }

        /// <summary>   Minimum button click. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void MinButton_Click(object sender, System.EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        /// <summary>   Maximum button click. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void MaxButton_Click(object sender, System.EventArgs e)
        {
            if ( this._IsWindowMaximized )
            {
                this.Location = this._NormalWindowLocation;
                this.Size = this._NormalWindowSize;
                this._ToolTip.SetToolTip( this._MaxButton, "Maximize");
                this._MaxButton.CustomFormState = CustomFormState.Normal;
                this._IsWindowMaximized = false;
            }
            else
            {
                this._NormalWindowSize = this.Size;
                this._NormalWindowLocation = this.Location;
                var rect = Screen.PrimaryScreen.WorkingArea;
                this.Location = new Point(0, 0);
                this.Size = new Size(rect.Width, rect.Height);
                this._ToolTip.SetToolTip( this._MaxButton, "Restore Down");
                this._MaxButton.CustomFormState = CustomFormState.Maximize;
                this._IsWindowMaximized = true;
            }
        }

        /// <summary>   Closes button click. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void CloseButton_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }

        /// <summary>   Right bottom panel 1 mouse down. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void RightBottomPanel_1_MouseDown(object sender, MouseEventArgs e)
        {
            this._IsRightBottomPanelDragged = true;
        }

        /// <summary>   Right bottom panel 1 mouse move. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void RightBottomPanel_1_MouseMove(object sender, MouseEventArgs e)
        {
            if ( this._IsRightBottomPanelDragged )
            {
                if ( this.Width < 100 || this.Height < 50)
                {
                    this.Width = 100;
                    this.Height = 50;
                    this._IsRightBottomPanelDragged = false;
                }
                else
                {
                    this.Width += e.X;
                    this.Height += e.Y;
                }
            }
        }

        /// <summary>   Right bottom panel 1 mouse up. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void RightBottomPanel_1_MouseUp(object sender, MouseEventArgs e)
        {
            this._IsRightBottomPanelDragged = false;
        }

        /// <summary>   Right bottom panel 2 mouse down. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void RightBottomPanel_2_MouseDown(object sender, MouseEventArgs e)
        {
            this.RightBottomPanel_1_MouseDown(sender, e);
        }

        /// <summary>   Right bottom panel 2 mouse move. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void RightBottomPanel_2_MouseMove(object sender, MouseEventArgs e)
        {
            this.RightBottomPanel_1_MouseMove(sender, e);
        }

        /// <summary>   Right bottom panel 2 mouse up. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void RightBottomPanel_2_MouseUp(object sender, MouseEventArgs e)
        {
            this.RightBottomPanel_1_MouseUp(sender, e);
        }

        /// <summary>   Left bottom panel 1 mouse down. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void LeftBottomPanel_1_MouseDown(object sender, MouseEventArgs e)
        {
            this._IsLeftBottomPanelDragged = true;
        }

        /// <summary>   Left bottom panel 1 mouse move. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void LeftBottomPanel_1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.X < this.Location.X)
            {
                if ( this._IsLeftBottomPanelDragged || this.Height < 50)
                {
                    if ( this.Width < 100)
                    {
                        this.Width = 100;
                        this.Height = 50;
                        this._IsLeftBottomPanelDragged = false;
                    }
                    else
                    {
                        this.Location = new Point( this.Location.X + e.X, this.Location.Y);
                        this.Width -= e.X;
                        this.Height += e.Y;
                    }
                }
            }
        }

        /// <summary>   Left bottom panel 1 mouse up. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void LeftBottomPanel_1_MouseUp(object sender, MouseEventArgs e)
        {
            this._IsLeftBottomPanelDragged = false;
        }

        /// <summary>   Left bottom panel 2 mouse down. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void LeftBottomPanel_2_MouseDown(object sender, MouseEventArgs e)
        {
            this.LeftBottomPanel_1_MouseDown(sender, e);
        }

        /// <summary>   Left bottom panel 2 mouse move. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void LeftBottomPanel_2_MouseMove(object sender, MouseEventArgs e)
        {
            this.LeftBottomPanel_1_MouseMove(sender, e);
        }

        /// <summary>   Left bottom panel 2 mouse up. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void LeftBottomPanel_2_MouseUp(object sender, MouseEventArgs e)
        {
            this.LeftBottomPanel_1_MouseUp(sender, e);
        }

        /// <summary>   Right top panel 1 mouse down. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void RightTopPanel_1_MouseDown(object sender, MouseEventArgs e)
        {
            this._IsRightTopPanelDragged = true;
        }

        /// <summary>   Right top panel 1 mouse move. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void RightTopPanel_1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Y < this.Location.Y || e.X < this.Location.X)
            {
                if ( this._IsRightTopPanelDragged )
                {
                    if ( this.Height < 50 || this.Width < 100)
                    {
                        this.Height = 50;
                        this.Width = 100;
                        this._IsRightTopPanelDragged = false;
                    }
                    else
                    {
                        this.Location = new Point( this.Location.X, this.Location.Y + e.Y);
                        this.Height -= e.Y;
                        this.Width += e.X;
                    }
                }
            }
        }

        /// <summary>   Right top panel 1 mouse up. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void RightTopPanel_1_MouseUp(object sender, MouseEventArgs e)
        {
            this._IsRightTopPanelDragged = false;
        }

        /// <summary>   Right top panel 2 mouse down. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void RightTopPanel_2_MouseDown(object sender, MouseEventArgs e)
        {
            this.RightTopPanel_1_MouseDown(sender, e);
        }

        /// <summary>   Right top panel 2 mouse move. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void RightTopPanel_2_MouseMove(object sender, MouseEventArgs e)
        {
            this.RightTopPanel_1_MouseMove(sender, e);
        }

        /// <summary>   Right top panel 2 mouse up. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void RightTopPanel_2_MouseUp(object sender, MouseEventArgs e)
        {
            this.RightTopPanel_1_MouseUp(sender, e);
        }

        /// <summary>   Left top panel 1 mouse down. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void LeftTopPanel_1_MouseDown(object sender, MouseEventArgs e)
        {
            this._IsLeftTopPanelDragged = true;
        }

        /// <summary>   Left top panel 1 mouse move. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void LeftTopPanel_1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.X < this.Location.X || e.Y < this.Location.Y)
            {
                if ( this._IsLeftTopPanelDragged )
                {
                    if ( this.Width < 100 || this.Height < 50)
                    {
                        this.Width = 100;
                        this.Height = 100;
                        this._IsLeftTopPanelDragged = false;
                    }
                    else
                    {
                        this.Location = new Point( this.Location.X + e.X, this.Location.Y);
                        this.Width -= e.X;
                        this.Location = new Point( this.Location.X, this.Location.Y + e.Y);
                        this.Height -= e.Y;
                    }
                }
            }
        }

        /// <summary>   Left top panel 1 mouse up. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void LeftTopPanel_1_MouseUp(object sender, MouseEventArgs e)
        {
            this._IsLeftTopPanelDragged = false;
        }

        /// <summary>   Left top panel 2 mouse down. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void LeftTopPanel_2_MouseDown(object sender, MouseEventArgs e)
        {
            this.LeftTopPanel_1_MouseDown(sender, e);
        }

        /// <summary>   Left top panel 2 mouse move. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void LeftTopPanel_2_MouseMove(object sender, MouseEventArgs e)
        {
            this.LeftTopPanel_1_MouseMove(sender, e);
        }

        /// <summary>   Left top panel 2 mouse up. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void LeftTopPanel_2_MouseUp(object sender, MouseEventArgs e)
        {
            this.LeftTopPanel_1_MouseUp(sender, e);
        }

        /// <summary>   File button click. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void File_button_Click(object sender, System.EventArgs e)
        {
            this._FileButton.BackColorActive = Color.Black;
            this._FileButton.MouseColorsEnabled = false;
            this._EditButton.BackColorActive = Color.FromArgb(60, 60, 60);
            this._ViewButton.BackColorActive = Color.FromArgb(60, 60, 60);
            this._RunButton.BackColorActive = Color.FromArgb(60, 60, 60);
            this._HelpButton.BackColorActive = Color.FromArgb(60, 60, 60);
            this._EditButton.MouseColorsEnabled = true;
            this._ViewButton.MouseColorsEnabled = true;
            this._RunButton.MouseColorsEnabled = true;
            this._HelpButton.MouseColorsEnabled = true;
        }

        /// <summary>   Edit button click. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void Edit_button_Click(object sender, System.EventArgs e)
        {
            this._EditButton.BackColorActive = Color.Black;
            this._EditButton.MouseColorsEnabled = false;
            this._FileButton.BackColorActive = Color.FromArgb(60, 60, 60);
            this._ViewButton.BackColorActive = Color.FromArgb(60, 60, 60);
            this._RunButton.BackColorActive = Color.FromArgb(60, 60, 60);
            this._HelpButton.BackColorActive = Color.FromArgb(60, 60, 60);
            this._FileButton.MouseColorsEnabled = true;
            this._ViewButton.MouseColorsEnabled = true;
            this._RunButton.MouseColorsEnabled = true;
            this._HelpButton.MouseColorsEnabled = true;
        }

        /// <summary>   View button click. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void View_button_Click(object sender, System.EventArgs e)
        {
            this._ViewButton.BackColorActive = Color.Black;
            this._ViewButton.MouseColorsEnabled = false;
            this._FileButton.BackColorActive = Color.FromArgb(60, 60, 60);
            this._EditButton.BackColorActive = Color.FromArgb(60, 60, 60);
            this._RunButton.BackColorActive = Color.FromArgb(60, 60, 60);
            this._HelpButton.BackColorActive = Color.FromArgb(60, 60, 60);
            this._FileButton.MouseColorsEnabled = true;
            this._EditButton.MouseColorsEnabled = true;
            this._RunButton.MouseColorsEnabled = true;
            this._HelpButton.MouseColorsEnabled = true;
        }

        /// <summary>   Executes the 'button click' operation. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void Run_button_Click(object sender, System.EventArgs e)
        {
            this._RunButton.BackColorActive = Color.Black;
            this._RunButton.MouseColorsEnabled = false;
            this._FileButton.BackColorActive = Color.FromArgb(60, 60, 60);
            this._EditButton.BackColorActive = Color.FromArgb(60, 60, 60);
            this._ViewButton.BackColorActive = Color.FromArgb(60, 60, 60);
            this._HelpButton.BackColorActive = Color.FromArgb(60, 60, 60);
            this._FileButton.MouseColorsEnabled = true;
            this._EditButton.MouseColorsEnabled = true;
            this._ViewButton.MouseColorsEnabled = true;
            this._HelpButton.MouseColorsEnabled = true;
        }

        /// <summary>   Help button click. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void Help_button_Click(object sender, System.EventArgs e)
        {
            this._HelpButton.BackColorActive = Color.Black;
            this._HelpButton.MouseColorsEnabled = false;
            this._FileButton.BackColorActive = Color.FromArgb(60, 60, 60);
            this._EditButton.BackColorActive = Color.FromArgb(60, 60, 60);
            this._ViewButton.BackColorActive = Color.FromArgb(60, 60, 60);
            this._RunButton.BackColorActive = Color.FromArgb(60, 60, 60);
            this._FileButton.MouseColorsEnabled = true;
            this._EditButton.MouseColorsEnabled = true;
            this._ViewButton.MouseColorsEnabled = true;
            this._RunButton.MouseColorsEnabled = true;
        }

        /// <summary>   Window text label mouse down. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void WindowTextLabel_MouseDown(object sender, MouseEventArgs e)
        {
            this.TopPanel_MouseDown(sender, e);
        }

        /// <summary>   Window text label mouse move. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void WindowTextLabel_MouseMove(object sender, MouseEventArgs e)
        {
            this.TopPanel_MouseMove(sender, e);
        }

        /// <summary>   Window text label mouse up. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void WindowTextLabel_MouseUp(object sender, MouseEventArgs e)
        {
            this.TopPanel_MouseUp(sender, e);
        }
    }
}
