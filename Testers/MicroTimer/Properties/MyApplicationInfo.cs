﻿/// namespace: My
/// 
/// summary:   ."

namespace isr.Core.Tester.My
{
    internal partial class MyApplication
    {
        /// <summary> The assembly title. </summary>

        public const string AssemblyTitle = "Micro Timer Demo";
        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "Micro Timer Demo";
        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "Micro.Timer.2019";
    }
}