using System;
using System.Windows.Forms;
using isr.Core.Constructs;

namespace isr.Core.Tester.MicroTimerWinFormsDemo
{

    /// <summary> A form main. </summary>
    /// <remarks> David, 2021-03-12. </remarks>
    public partial class FormMain : Form
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Windows.Forms.Form" /> class.
        /// </summary>
        /// <remarks> David, 2021-03-12. </remarks>
        public FormMain()
        {

            base.FormClosing += this.FormMainFormClosing;
            this.InitializeComponent();

            // Instantiate new MicroTimer and add event handler
            this._MicroTimer = new MicroTimer();
            this._MicroTimer.MicroTimerElapsed += this.OnTimedEvent;
        }

        private readonly MicroTimer _MicroTimer;

        /// <summary> Raises the micro timer event. </summary>
        /// <remarks> David, 2021-03-12. </remarks>
        /// <param name="sender">         Source of the event. </param>
        /// <param name="timerEventArgs"> Event information to send to registered event handlers. </param>
        private void OnTimedEvent(object sender, MicroTimerEventArgs timerEventArgs)
        {
            // Do something small that takes significantly less time than Interval.
            // BeginInvoke executes on the UI thread but this calling thread does not
            // wait for completion before continuing (i.e. it executes asynchronously)
            if ( this.InvokeRequired )
            {
                _ = this.BeginInvoke( ( MethodInvoker ) delegate {
                    // Show the current time in the form's title bar.
                    this.TextBoxElapsedTime.Text = timerEventArgs.ElapsedMicroseconds.ToString( "#,#" );
                } );
                // this converted method did not work: 
                // BeginInvoke( ( MethodInvoker ) () => TextBoxElapsedTime.Text = timerEventArgs.ElapsedMicroseconds.ToString( "#,#" ) );
            }
            else
            {
                this.TextBoxElapsedTime.Text = timerEventArgs.ElapsedMicroseconds.ToString( "#,#" );
            }
        }

        /// <summary> Event handler. Called by buttonStart for click events. </summary>
        /// <remarks> David, 2021-03-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ButtonStartClick(object sender, EventArgs e)
        {

            // Read interval from form
            if ( !long.TryParse( this.TextBoxInterval.Text, out long interval ) )
            {
                return;
            }

            // Set timer interval
            this._MicroTimer.Interval = interval;

            // Ignore event if late by half the interval
            this._MicroTimer.IgnoreEventIfLateBy = interval / 2L;

            // Start timer
            this._MicroTimer.Start();
        }

        /// <summary> Event handler. Called by buttonStop for click events. </summary>
        /// <remarks> David, 2021-03-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ButtonStopClick(object sender, EventArgs e)
        {
            // Stop the timer
            this._MicroTimer.Stop();
        }

        private void FormMainFormClosing(object sender, FormClosingEventArgs e)
        {
            // Stop the timer, wait for up to 1 sec for current event to finish,
            // if it does not finish within this time abort the timer thread
            this._MicroTimer.Stop(TimeSpan.FromMilliseconds(1000d));
            // If Not _microTimer.StopAndWait(TimeSpan.FromSeconds(1000)) Then                _microTimer.Abort()
        }
    }
}
