using System;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace isr.Core.Tester.MicroTimerWinFormsDemo
{
    public partial class FormMain
    {
        /// <summary>
		/// Required designer variable.
		/// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && components is object)
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        /// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            _buttonStart = new Button();
            _buttonStart.Click += new EventHandler(ButtonStartClick);
            _buttonStop = new Button();
            _buttonStop.Click += new EventHandler(ButtonStopClick);
            TextBoxInterval = new TextBox();
            LabelIntervalText = new Label();
            LabelElapsedTimeText = new Label();
            TextBoxElapsedTime = new TextBox();
            SuspendLayout();
            // 
            // buttonStart
            // 
            _buttonStart.Location = new Point(27, 88);
            _buttonStart.Name = "_buttonStart";
            _buttonStart.Size = new Size(102, 23);
            _buttonStart.TabIndex = 0;
            _buttonStart.Text = "Start";
            _buttonStart.UseVisualStyleBackColor = true;
            // Me.buttonStart.Click += New System.EventHandler(Me.ButtonStartClick)
            // 
            // buttonStop
            // 
            _buttonStop.Location = new Point(158, 87);
            _buttonStop.Name = "_buttonStop";
            _buttonStop.Size = new Size(102, 23);
            _buttonStop.TabIndex = 1;
            _buttonStop.Text = "Stop";
            _buttonStop.UseVisualStyleBackColor = true;
            // Me.buttonStop.Click += New System.EventHandler(Me.ButtonStopClick)
            // 
            // TextBoxInterval
            // 
            TextBoxInterval.Location = new Point(158, 20);
            TextBoxInterval.Name = "TextBoxInterval";
            TextBoxInterval.Size = new Size(102, 20);
            TextBoxInterval.TabIndex = 2;
            TextBoxInterval.Text = "1111";
            // 
            // LabelIntervalText
            // 
            LabelIntervalText.AutoSize = true;
            LabelIntervalText.Location = new Point(24, 23);
            LabelIntervalText.Name = "LabelIntervalText";
            LabelIntervalText.Size = new Size(128, 13);
            LabelIntervalText.TabIndex = 3;
            LabelIntervalText.Text = "Timer Interval (micro sec):";
            // 
            // LabelElapsedTimeText
            // 
            LabelElapsedTimeText.AutoSize = true;
            LabelElapsedTimeText.Location = new Point(24, 55);
            LabelElapsedTimeText.Name = "LabelElapsedTimeText";
            LabelElapsedTimeText.Size = new Size(128, 13);
            LabelElapsedTimeText.TabIndex = 5;
            LabelElapsedTimeText.Text = "Elapsed Time (micro sec):";
            // 
            // TextBoxElapsedTime
            // 
            TextBoxElapsedTime.Location = new Point(158, 52);
            TextBoxElapsedTime.Name = "TextBoxElapsedTime";
            TextBoxElapsedTime.ReadOnly = true;
            TextBoxElapsedTime.Size = new Size(102, 20);
            TextBoxElapsedTime.TabIndex = 6;
            // 
            // FormMain
            // 
            AutoScaleDimensions = new SizeF(6f, 13f);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(284, 132);
            Controls.Add(TextBoxElapsedTime);
            Controls.Add(LabelElapsedTimeText);
            Controls.Add(LabelIntervalText);
            Controls.Add(TextBoxInterval);
            Controls.Add(_buttonStop);
            Controls.Add(_buttonStart);
            Name = "FormMain";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Micro Timer Demo";
            // Me.FormClosing += New System.Windows.Forms.FormClosingEventHandler(Me.FormMainFormClosing)
            ResumeLayout(false);
            PerformLayout();
        }

        private Button _buttonStart;

        private Button buttonStart
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _buttonStart;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_buttonStart != null)
                {
                    _buttonStart.Click -= ButtonStartClick;
                }

                _buttonStart = value;
                if (_buttonStart != null)
                {
                    _buttonStart.Click += ButtonStartClick;
                }
            }
        }

        private Button _buttonStop;

        private Button buttonStop
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _buttonStop;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_buttonStop != null)
                {
                    _buttonStop.Click -= ButtonStopClick;
                }

                _buttonStop = value;
                if (_buttonStop != null)
                {
                    _buttonStop.Click += ButtonStopClick;
                }
            }
        }

        private TextBox TextBoxInterval;
        private Label LabelIntervalText;
        private Label LabelElapsedTimeText;
        private TextBox TextBoxElapsedTime;
    }
}
