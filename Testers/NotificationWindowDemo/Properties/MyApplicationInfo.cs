﻿/// namespace: My
/// 
/// summary:   ."

namespace isr.Core.Tester.My
{
    internal partial class MyApplication
    {
        /// <summary> The assembly title. </summary>

        public const string AssemblyTitle = "Notification Window Demo";
        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "Notification Window Demo";
        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "Notification.Window.Demo.2019";
    }
}