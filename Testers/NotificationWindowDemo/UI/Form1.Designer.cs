using System;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace isr.Core.Tester
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses")]
    public partial class Form1
    {
        /// <summary>
    /// Required designer variable.
    /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && components is object)
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        /// <summary>
        /// Required method for Designer support - do not modify the contents of this method with the
        /// code editor.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            __ExitButton = new Button();
            __ExitButton.Click += new EventHandler(ExitButton_Click);
            __ShowButton = new Button();
            __ShowButton.Click += new EventHandler(ShowButton_Click);
            _NotificationTitleTextBoxLabel = new Label();
            _NotificationTextTextBoxLabel = new Label();
            _NotificationTitleTextBox = new TextBox();
            _NotificationTextTextBox = new TextBox();
            _ShowNotificationIconCheckBox = new CheckBox();
            _ShowNotificationCloseButtonCheckBox = new CheckBox();
            _ShowNotificationOptionMenuCheckBox = new CheckBox();
            _NotificationDelayTextBoxLabel = new Label();
            _NotificationAnimationIntervalTextBoxLabel = new Label();
            _NotificationDelayTextBox = new TextBox();
            _NotificationAnimationIntervalTextBox = new TextBox();
            _ShowNotificationGripCheckBox = new CheckBox();
            _NotificationTitlePaddingTextBoxLabel = new Label();
            _NotificationIconPaddingTextBox = new TextBox();
            _NotificationScrollCheckBox = new CheckBox();
            _NotificationContentPaddingTextBox = new TextBox();
            _NotificationIconPaddingTextBoxLabel = new Label();
            _NotificationTitlePaddingTextBox = new TextBox();
            _NotificationContentPaddingTextBoxLabel = new Label();
            _ContextMenuStrip = new ContextMenuStrip(components);
            _AboutToolStripMenuItem = new ToolStripMenuItem();
            _SettingsToolStripMenuItem = new ToolStripMenuItem();
            _ExitToolStripMenuItem = new ToolStripMenuItem();
            _AnimationDurationTextBoxLabel = new Label();
            _AnimationDurationTextBox = new TextBox();
            _PopupNotifier = new Forma.PopupNotifier();
            _ContextMenuStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _ExitButton
            // 
            __ExitButton.Location = new Point(425, 12);
            __ExitButton.Name = "__ExitButton";
            __ExitButton.Size = new Size(75, 23);
            __ExitButton.TabIndex = 0;
            __ExitButton.Text = "Exit";
            __ExitButton.UseVisualStyleBackColor = true;
            // Me._ExitButton.Click += New System.EventHandler(Me.ExitButton_Click)
            // 
            // _ShowButton
            // 
            __ShowButton.Location = new Point(12, 202);
            __ShowButton.Name = "__ShowButton";
            __ShowButton.Size = new Size(75, 23);
            __ShowButton.TabIndex = 1;
            __ShowButton.Text = "Show!";
            __ShowButton.UseVisualStyleBackColor = true;
            // Me._ShowButton.Click += New System.EventHandler(Me._ShowButton_Click)
            // 
            // _NotificationTitleTextBoxLabel
            // 
            _NotificationTitleTextBoxLabel.AutoSize = true;
            _NotificationTitleTextBoxLabel.Location = new Point(12, 17);
            _NotificationTitleTextBoxLabel.Name = "_NotificationTitleTextBoxLabel";
            _NotificationTitleTextBoxLabel.Size = new Size(30, 13);
            _NotificationTitleTextBoxLabel.TabIndex = 2;
            _NotificationTitleTextBoxLabel.Text = "Title:";
            // 
            // _NotificationTextTextBoxLabel
            // 
            _NotificationTextTextBoxLabel.AutoSize = true;
            _NotificationTextTextBoxLabel.Location = new Point(12, 43);
            _NotificationTextTextBoxLabel.Name = "_NotificationTextTextBoxLabel";
            _NotificationTextTextBoxLabel.Size = new Size(31, 13);
            _NotificationTextTextBoxLabel.TabIndex = 3;
            _NotificationTextTextBoxLabel.Text = "Text:";
            // 
            // _NotificationTitleTextBox
            // 
            _NotificationTitleTextBox.Location = new Point(48, 14);
            _NotificationTitleTextBox.Name = "_NotificationTitleTextBox";
            _NotificationTitleTextBox.Size = new Size(372, 20);
            _NotificationTitleTextBox.TabIndex = 4;
            _NotificationTitleTextBox.Text = "Notification Title";
            // 
            // _NotificationTextTextBox
            // 
            _NotificationTextTextBox.Location = new Point(49, 40);
            _NotificationTextTextBox.Name = "_NotificationTextTextBox";
            _NotificationTextTextBox.Size = new Size(371, 20);
            _NotificationTextTextBox.TabIndex = 5;
            _NotificationTextTextBox.Text = "This is the notification text!";
            // 
            // _ShowNotificationIconCheckBox
            // 
            _ShowNotificationIconCheckBox.AutoSize = true;
            _ShowNotificationIconCheckBox.Checked = true;
            _ShowNotificationIconCheckBox.CheckState = CheckState.Checked;
            _ShowNotificationIconCheckBox.Location = new Point(15, 75);
            _ShowNotificationIconCheckBox.Name = "_ShowNotificationIconCheckBox";
            _ShowNotificationIconCheckBox.Size = new Size(76, 17);
            _ShowNotificationIconCheckBox.TabIndex = 6;
            _ShowNotificationIconCheckBox.Text = "Show icon";
            _ShowNotificationIconCheckBox.UseVisualStyleBackColor = true;
            // 
            // _ShowNotificationCloseButtonCheckBox
            // 
            _ShowNotificationCloseButtonCheckBox.AutoSize = true;
            _ShowNotificationCloseButtonCheckBox.Checked = true;
            _ShowNotificationCloseButtonCheckBox.CheckState = CheckState.Checked;
            _ShowNotificationCloseButtonCheckBox.Location = new Point(15, 98);
            _ShowNotificationCloseButtonCheckBox.Name = "_ShowNotificationCloseButtonCheckBox";
            _ShowNotificationCloseButtonCheckBox.Size = new Size(114, 17);
            _ShowNotificationCloseButtonCheckBox.TabIndex = 7;
            _ShowNotificationCloseButtonCheckBox.Text = "Show close button";
            _ShowNotificationCloseButtonCheckBox.UseVisualStyleBackColor = true;
            // 
            // _ShowNotificationOptionMenuCheckBox
            // 
            _ShowNotificationOptionMenuCheckBox.AutoSize = true;
            _ShowNotificationOptionMenuCheckBox.Checked = true;
            _ShowNotificationOptionMenuCheckBox.CheckState = CheckState.Checked;
            _ShowNotificationOptionMenuCheckBox.Location = new Point(15, 121);
            _ShowNotificationOptionMenuCheckBox.Name = "_ShowNotificationOptionMenuCheckBox";
            _ShowNotificationOptionMenuCheckBox.Size = new Size(114, 17);
            _ShowNotificationOptionMenuCheckBox.TabIndex = 8;
            _ShowNotificationOptionMenuCheckBox.Text = "Show option menu";
            _ShowNotificationOptionMenuCheckBox.UseVisualStyleBackColor = true;
            // 
            // _NotificationDelayTextBoxLabel
            // 
            _NotificationDelayTextBoxLabel.AutoSize = true;
            _NotificationDelayTextBoxLabel.Location = new Point(199, 76);
            _NotificationDelayTextBoxLabel.Name = "_NotificationDelayTextBoxLabel";
            _NotificationDelayTextBoxLabel.Size = new Size(59, 13);
            _NotificationDelayTextBoxLabel.TabIndex = 9;
            _NotificationDelayTextBoxLabel.Text = "Delay [ms]:";
            // 
            // _NotificationAnimationIntervalTextBoxLabel
            // 
            _NotificationAnimationIntervalTextBoxLabel.AutoSize = true;
            _NotificationAnimationIntervalTextBoxLabel.Location = new Point(199, 99);
            _NotificationAnimationIntervalTextBoxLabel.Name = "_NotificationAnimationIntervalTextBoxLabel";
            _NotificationAnimationIntervalTextBoxLabel.Size = new Size(115, 13);
            _NotificationAnimationIntervalTextBoxLabel.TabIndex = 10;
            _NotificationAnimationIntervalTextBoxLabel.Text = "Animation interval [ms]:";
            // 
            // _NotificationDelayTextBox
            // 
            _NotificationDelayTextBox.Location = new Point(320, 70);
            _NotificationDelayTextBox.Name = "_NotificationDelayTextBox";
            _NotificationDelayTextBox.Size = new Size(100, 20);
            _NotificationDelayTextBox.TabIndex = 11;
            _NotificationDelayTextBox.Text = "3000";
            // 
            // _NotificationAnimationIntervalTextBox
            // 
            _NotificationAnimationIntervalTextBox.Location = new Point(320, 96);
            _NotificationAnimationIntervalTextBox.Name = "_NotificationAnimationIntervalTextBox";
            _NotificationAnimationIntervalTextBox.Size = new Size(100, 20);
            _NotificationAnimationIntervalTextBox.TabIndex = 12;
            _NotificationAnimationIntervalTextBox.Text = "10";
            // 
            // _ShowNotificationGripCheckBox
            // 
            _ShowNotificationGripCheckBox.AutoSize = true;
            _ShowNotificationGripCheckBox.Checked = true;
            _ShowNotificationGripCheckBox.CheckState = CheckState.Checked;
            _ShowNotificationGripCheckBox.Location = new Point(15, 144);
            _ShowNotificationGripCheckBox.Name = "_ShowNotificationGripCheckBox";
            _ShowNotificationGripCheckBox.Size = new Size(73, 17);
            _ShowNotificationGripCheckBox.TabIndex = 13;
            _ShowNotificationGripCheckBox.Text = "Show grip";
            _ShowNotificationGripCheckBox.UseVisualStyleBackColor = true;
            // 
            // _NotificationTitlePaddingTextBoxLabel
            // 
            _NotificationTitlePaddingTextBoxLabel.AutoSize = true;
            _NotificationTitlePaddingTextBoxLabel.Location = new Point(199, 151);
            _NotificationTitlePaddingTextBoxLabel.Name = "_NotificationTitlePaddingTextBoxLabel";
            _NotificationTitlePaddingTextBoxLabel.Size = new Size(91, 13);
            _NotificationTitlePaddingTextBoxLabel.TabIndex = 14;
            _NotificationTitlePaddingTextBoxLabel.Text = "Title padding [pixels]:";
            // 
            // _NotificationIconPaddingTextBox
            // 
            _NotificationIconPaddingTextBox.Location = new Point(320, 200);
            _NotificationIconPaddingTextBox.Name = "_NotificationIconPaddingTextBox";
            _NotificationIconPaddingTextBox.Size = new Size(100, 20);
            _NotificationIconPaddingTextBox.TabIndex = 15;
            _NotificationIconPaddingTextBox.Text = "0";
            // 
            // _NotificationScrollCheckBox
            // 
            _NotificationScrollCheckBox.AutoSize = true;
            _NotificationScrollCheckBox.Checked = true;
            _NotificationScrollCheckBox.CheckState = CheckState.Checked;
            _NotificationScrollCheckBox.Location = new Point(15, 167);
            _NotificationScrollCheckBox.Name = "_NotificationScrollCheckBox";
            _NotificationScrollCheckBox.Size = new Size(83, 17);
            _NotificationScrollCheckBox.TabIndex = 16;
            _NotificationScrollCheckBox.Text = "Scroll in/out";
            _NotificationScrollCheckBox.UseVisualStyleBackColor = true;
            // 
            // _NotificationContentPaddingTextBox
            // 
            _NotificationContentPaddingTextBox.Location = new Point(320, 174);
            _NotificationContentPaddingTextBox.Name = "_NotificationContentPaddingTextBox";
            _NotificationContentPaddingTextBox.Size = new Size(100, 20);
            _NotificationContentPaddingTextBox.TabIndex = 17;
            _NotificationContentPaddingTextBox.Text = "0";
            // 
            // _NotificationIconPaddingTextBoxLabel
            // 
            _NotificationIconPaddingTextBoxLabel.AutoSize = true;
            _NotificationIconPaddingTextBoxLabel.Location = new Point(199, 203);
            _NotificationIconPaddingTextBoxLabel.Name = "_NotificationIconPaddingTextBoxLabel";
            _NotificationIconPaddingTextBoxLabel.Size = new Size(92, 13);
            _NotificationIconPaddingTextBoxLabel.TabIndex = 18;
            _NotificationIconPaddingTextBoxLabel.Text = "Icon padding [pixels]:";
            // 
            // _NotificationTitlePaddingTextBox
            // 
            _NotificationTitlePaddingTextBox.Location = new Point(320, 148);
            _NotificationTitlePaddingTextBox.Name = "_NotificationTitlePaddingTextBox";
            _NotificationTitlePaddingTextBox.Size = new Size(100, 20);
            _NotificationTitlePaddingTextBox.TabIndex = 19;
            _NotificationTitlePaddingTextBox.Text = "0";
            // 
            // _NotificationContentPaddingTextBoxLabel
            // 
            _NotificationContentPaddingTextBoxLabel.AutoSize = true;
            _NotificationContentPaddingTextBoxLabel.Location = new Point(199, 177);
            _NotificationContentPaddingTextBoxLabel.Name = "_NotificationContentPaddingTextBoxLabel";
            _NotificationContentPaddingTextBoxLabel.Size = new Size(108, 13);
            _NotificationContentPaddingTextBoxLabel.TabIndex = 20;
            _NotificationContentPaddingTextBoxLabel.Text = "Content padding [pixels]:";
            // 
            // _ContextMenuStrip
            // 
            _ContextMenuStrip.Items.AddRange(new ToolStripItem[] { _AboutToolStripMenuItem, _SettingsToolStripMenuItem, _ExitToolStripMenuItem });
            _ContextMenuStrip.Name = "_ContextMenuStrip";
            _ContextMenuStrip.Size = new Size(126, 70);
            // 
            // _AboutToolStripMenuItem
            // 
            _AboutToolStripMenuItem.Name = "_AboutToolStripMenuItem";
            _AboutToolStripMenuItem.Size = new Size(125, 22);
            _AboutToolStripMenuItem.Text = "About...";
            // 
            // _SettingsToolStripMenuItem
            // 
            _SettingsToolStripMenuItem.Name = "_SettingsToolStripMenuItem";
            _SettingsToolStripMenuItem.Size = new Size(125, 22);
            _SettingsToolStripMenuItem.Text = "Settings...";
            // 
            // _ExitToolStripMenuItem
            // 
            _ExitToolStripMenuItem.Name = "_ExitToolStripMenuItem";
            _ExitToolStripMenuItem.Size = new Size(125, 22);
            _ExitToolStripMenuItem.Text = "Exit";
            // 
            // _AnimationDurationTextBoxLabel
            // 
            _AnimationDurationTextBoxLabel.AutoSize = true;
            _AnimationDurationTextBoxLabel.Location = new Point(199, 125);
            _AnimationDurationTextBoxLabel.Name = "_AnimationDurationTextBoxLabel";
            _AnimationDurationTextBoxLabel.Size = new Size(118, 13);
            _AnimationDurationTextBoxLabel.TabIndex = 21;
            _AnimationDurationTextBoxLabel.Text = "Animation Duration [ms]:";
            // 
            // _AnimationDurationTextBox
            // 
            _AnimationDurationTextBox.Location = new Point(320, 122);
            _AnimationDurationTextBox.Name = "_AnimationDurationTextBox";
            _AnimationDurationTextBox.Size = new Size(100, 20);
            _AnimationDurationTextBox.TabIndex = 22;
            _AnimationDurationTextBox.Text = "1000";
            // 
            // _PopupNotifier
            // 
            _PopupNotifier.BodyColor = Color.FromArgb(128, 128, 255);
            _PopupNotifier.ContentFont = new Font("Tahoma", 8.0f);
            _PopupNotifier.ContentText = null;
            _PopupNotifier.GradientPower = 300;
            _PopupNotifier.HeaderHeight = 20;
            _PopupNotifier.Image = null;
            _PopupNotifier.OptionsMenu = _ContextMenuStrip;
            _PopupNotifier.Size = new Size(400, 100);
            _PopupNotifier.TitleFont = new Font("Segoe UI", 9.0f);
            _PopupNotifier.TitleText = null;
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(6.0f, 13.0f);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(512, 237);
            Controls.Add(_NotificationTitlePaddingTextBox);
            Controls.Add(_NotificationContentPaddingTextBoxLabel);
            Controls.Add(_AnimationDurationTextBox);
            Controls.Add(_AnimationDurationTextBoxLabel);
            Controls.Add(_NotificationScrollCheckBox);
            Controls.Add(_NotificationContentPaddingTextBox);
            Controls.Add(_NotificationIconPaddingTextBox);
            Controls.Add(_NotificationIconPaddingTextBoxLabel);
            Controls.Add(_ShowNotificationGripCheckBox);
            Controls.Add(_NotificationTitlePaddingTextBoxLabel);
            Controls.Add(_NotificationDelayTextBox);
            Controls.Add(_NotificationAnimationIntervalTextBoxLabel);
            Controls.Add(_NotificationAnimationIntervalTextBox);
            Controls.Add(_NotificationDelayTextBoxLabel);
            Controls.Add(_ShowNotificationOptionMenuCheckBox);
            Controls.Add(_ShowNotificationCloseButtonCheckBox);
            Controls.Add(_ShowNotificationIconCheckBox);
            Controls.Add(_NotificationTextTextBox);
            Controls.Add(_NotificationTitleTextBox);
            Controls.Add(_NotificationTextTextBoxLabel);
            Controls.Add(_NotificationTitleTextBoxLabel);
            Controls.Add(__ShowButton);
            Controls.Add(__ExitButton);
            FormBorderStyle = FormBorderStyle.FixedDialog;
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "Form1";
            Text = "Notification Window Demo";
            _ContextMenuStrip.ResumeLayout(false);
            ResumeLayout(false);
            PerformLayout();
        }

        private Button __ExitButton;

        private Button _ExitButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ExitButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ExitButton != null)
                {
                    __ExitButton.Click -= ExitButton_Click;
                }

                __ExitButton = value;
                if (__ExitButton != null)
                {
                    __ExitButton.Click += ExitButton_Click;
                }
            }
        }

        private Button __ShowButton;

        private Button _ShowButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ShowButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ShowButton != null)
                {
                    __ShowButton.Click -= ShowButton_Click;
                }

                __ShowButton = value;
                if (__ShowButton != null)
                {
                    __ShowButton.Click += ShowButton_Click;
                }
            }
        }

        private Label _NotificationTitleTextBoxLabel;
        private Label _NotificationTextTextBoxLabel;
        private TextBox _NotificationTitleTextBox;
        private TextBox _NotificationTextTextBox;
        private CheckBox _ShowNotificationIconCheckBox;
        private CheckBox _ShowNotificationCloseButtonCheckBox;
        private CheckBox _ShowNotificationOptionMenuCheckBox;
        private Label _NotificationDelayTextBoxLabel;
        private Label _NotificationAnimationIntervalTextBoxLabel;
        private TextBox _NotificationDelayTextBox;
        private TextBox _NotificationAnimationIntervalTextBox;
        private CheckBox _ShowNotificationGripCheckBox;
        private Label _NotificationTitlePaddingTextBoxLabel;
        private TextBox _NotificationIconPaddingTextBox;
        private CheckBox _NotificationScrollCheckBox;
        private TextBox _NotificationContentPaddingTextBox;
        private Label _NotificationIconPaddingTextBoxLabel;
        private TextBox _NotificationTitlePaddingTextBox;
        private Label _NotificationContentPaddingTextBoxLabel;
        private ContextMenuStrip _ContextMenuStrip;
        private ToolStripMenuItem _AboutToolStripMenuItem;
        private ToolStripMenuItem _SettingsToolStripMenuItem;
        private ToolStripMenuItem _ExitToolStripMenuItem;
        private Label _AnimationDurationTextBoxLabel;
        private TextBox _AnimationDurationTextBox;
        private Forma.PopupNotifier _PopupNotifier;
    }
}
