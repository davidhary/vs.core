/// <summary> A form 1. </summary>
/// <remarks> David, 2021-03-12. </remarks>
// 
// Created/modified in 2011 by Simon Baer
// 
// Licensed under the Code Project Open License (CPOL).
// 
using System;
using System.Windows.Forms;

namespace isr.Core.Tester
{
    public partial class Form1 : Form
    {

        /// <summary>
    /// Initializes a new instance of the <see cref="T:System.Windows.Forms.Form" /> class.
    /// </summary>
    /// <remarks> David, 2021-03-12. </remarks>
        public Form1()
        {
            InitializeComponent();
        }

        /// <summary> Shows the button click. </summary>
    /// <remarks> David, 2021-03-12. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
        private void ShowButton_Click(object sender, EventArgs e)
        {
            _PopupNotifier.TitleText = _NotificationTitleTextBox.Text;
            _PopupNotifier.ContentText = _NotificationTextTextBox.Text;
            _PopupNotifier.ShowCloseButton = _ShowNotificationCloseButtonCheckBox.Checked;
            _PopupNotifier.ShowOptionsButton = _ShowNotificationOptionMenuCheckBox.Checked;
            _PopupNotifier.ShowGrip = _ShowNotificationGripCheckBox.Checked;
            _PopupNotifier.Delay = int.Parse(_NotificationDelayTextBox.Text);
            _PopupNotifier.AnimationInterval = int.Parse(_NotificationAnimationIntervalTextBox.Text);
            _PopupNotifier.AnimationDuration = int.Parse(_AnimationDurationTextBox.Text);
            _PopupNotifier.TitlePadding = new Padding(int.Parse(_NotificationTitlePaddingTextBox.Text));
            _PopupNotifier.ContentPadding = new Padding(int.Parse(_NotificationContentPaddingTextBox.Text));
            _PopupNotifier.ImagePadding = new Padding(int.Parse(_NotificationIconPaddingTextBox.Text));
            _PopupNotifier.Scroll = _NotificationScrollCheckBox.Checked;
            _PopupNotifier.Image = _ShowNotificationIconCheckBox.Checked ? My.Resources.Resources._157_GetPermission_48x48_721 : null;
            _PopupNotifier.Popup();
        }

        /// <summary> Exit button click. </summary>
    /// <remarks> David, 2021-03-12. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
        private void ExitButton_Click(object sender, EventArgs e)
        {
            Close();
            Application.Exit();
        }
    }
}
