using System;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Tester
{
    public partial class Dashboard
    {
        /// <summary>
    /// Required designer variable.
    /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && components is object)
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        /// <summary>
        /// Required method for Designer support - do not modify the contents of this method with the
        /// code editor.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        private void InitializeComponent()
        {
            _ImageButton1 = new Controls.ImageButton();
            _ImageButton1.Click += new EventHandler(ImageButton1_Click);
            _ImageButton3 = new Controls.ImageButton();
            _ImageButton3.Click += new EventHandler(ImageButton3_Click);
            _ImageButton2 = new Controls.ImageButton();
            _ImageButton2.Click += new EventHandler(ImageButton2_Click);
            label1 = new Label();
            _ImageButton4 = new Controls.ImageButton();
            _ImageButton4.Click += new EventHandler(ImageButton4_Click);
            _ImageButton5 = new Controls.ImageButton();
            _ImageButton5.Click += new EventHandler(ImageButton5_Click);
            _ImageButton6 = new Controls.ImageButton();
            _ImageButton6.Click += new EventHandler(ImageButton6_Click);
            _ImageButton7 = new Controls.ImageButton();
            _ImageButton7.Click += new EventHandler(ImageButton7_Click);
            label2 = new Label();
            TraceMessageToolStrip1 = new Forma.TraceMessageToolStrip();
            ((System.ComponentModel.ISupportInitialize)_ImageButton1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_ImageButton3).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_ImageButton2).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_ImageButton4).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_ImageButton5).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_ImageButton6).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_ImageButton7).BeginInit();
            SuspendLayout();
            // 
            // ImageButton1
            // 
            _ImageButton1.DialogResult = DialogResult.None;
            _ImageButton1.DownImage = My.Resources.Resources.ExampleButtonDown;
            _ImageButton1.HoverImage = My.Resources.Resources.ExampleButtonHover;
            _ImageButton1.Location = new Point(61, 61);
            _ImageButton1.Margin = new Padding(3, 4, 3, 4);
            _ImageButton1.Name = "_ImageButton1";
            _ImageButton1.NormalImage = My.Resources.Resources.ExampleButton;
            _ImageButton1.Size = new Size(100, 50);
            _ImageButton1.SizeMode = PictureBoxSizeMode.AutoSize;
            _ImageButton1.TabIndex = 0;
            _ImageButton1.TabStop = false;
            // 
            // ImageButton3
            // 
            _ImageButton3.DialogResult = DialogResult.None;
            _ImageButton3.DownImage = My.Resources.Resources.CUncheckedDown;
            _ImageButton3.HoverImage = My.Resources.Resources.CUncheckedHover;
            _ImageButton3.Location = new Point(105, 154);
            _ImageButton3.Margin = new Padding(3, 4, 3, 4);
            _ImageButton3.Name = "_ImageButton3";
            _ImageButton3.NormalImage = My.Resources.Resources.CUncheckedNormal;
            _ImageButton3.Size = new Size(20, 20);
            _ImageButton3.SizeMode = PictureBoxSizeMode.AutoSize;
            _ImageButton3.TabIndex = 2;
            _ImageButton3.TabStop = false;
            // 
            // ImageButton2
            // 
            _ImageButton2.DialogResult = DialogResult.None;
            _ImageButton2.DownImage = My.Resources.Resources.ExampleButtonDownA;
            _ImageButton2.Font = new Font("Microsoft Sans Serif", 14.25f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _ImageButton2.HoverImage = My.Resources.Resources.ExampleButtonHoverA;
            _ImageButton2.Location = new Point(184, 61);
            _ImageButton2.Margin = new Padding(3, 4, 3, 4);
            _ImageButton2.Name = "_ImageButton2";
            _ImageButton2.NormalImage = My.Resources.Resources.ExampleButtonA;
            _ImageButton2.Size = new Size(100, 50);
            _ImageButton2.SizeMode = PictureBoxSizeMode.AutoSize;
            _ImageButton2.TabIndex = 1;
            _ImageButton2.TabStop = false;
            _ImageButton2.Text = "Example B";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.BackColor = Color.Transparent;
            label1.Location = new Point(126, 160);
            label1.Name = "label1";
            label1.Size = new Size(115, 17);
            label1.TabIndex = 3;
            label1.Text = "Disable click alerts";
            // 
            // ImageButton4
            // 
            _ImageButton4.DialogResult = DialogResult.None;
            _ImageButton4.DownImage = My.Resources.Resources.ExampleButtonDownA;
            _ImageButton4.Font = new Font("Microsoft Sans Serif", 14.25f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _ImageButton4.HoverImage = null;
            _ImageButton4.Location = new Point(308, 61);
            _ImageButton4.Margin = new Padding(3, 4, 3, 4);
            _ImageButton4.Name = "_ImageButton4";
            _ImageButton4.NormalImage = My.Resources.Resources.ExampleButtonA;
            _ImageButton4.Size = new Size(100, 50);
            _ImageButton4.SizeMode = PictureBoxSizeMode.AutoSize;
            _ImageButton4.TabIndex = 4;
            _ImageButton4.TabStop = false;
            _ImageButton4.Text = "Example C";
            // 
            // ImageButton5
            // 
            _ImageButton5.DialogResult = DialogResult.None;
            _ImageButton5.DownImage = null;
            _ImageButton5.Font = new Font("Microsoft Sans Serif", 14.25f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _ImageButton5.HoverImage = My.Resources.Resources.ExampleButtonHoverA;
            _ImageButton5.Location = new Point(432, 61);
            _ImageButton5.Margin = new Padding(3, 4, 3, 4);
            _ImageButton5.Name = "_ImageButton5";
            _ImageButton5.NormalImage = My.Resources.Resources.ExampleButtonA;
            _ImageButton5.Size = new Size(100, 50);
            _ImageButton5.SizeMode = PictureBoxSizeMode.AutoSize;
            _ImageButton5.TabIndex = 5;
            _ImageButton5.TabStop = false;
            _ImageButton5.Text = "Example D";
            // 
            // ImageButton6
            // 
            _ImageButton6.DialogResult = DialogResult.None;
            _ImageButton6.DownImage = null;
            _ImageButton6.Font = new Font("Microsoft Sans Serif", 14.25f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _ImageButton6.HoverImage = null;
            _ImageButton6.Location = new Point(308, 135);
            _ImageButton6.Margin = new Padding(3, 4, 3, 4);
            _ImageButton6.Name = "_ImageButton6";
            _ImageButton6.NormalImage = My.Resources.Resources.ExampleButtonA;
            _ImageButton6.Size = new Size(100, 50);
            _ImageButton6.SizeMode = PictureBoxSizeMode.AutoSize;
            _ImageButton6.TabIndex = 6;
            _ImageButton6.TabStop = false;
            _ImageButton6.Text = "Example E";
            // 
            // ImageButton7
            // 
            _ImageButton7.DialogResult = DialogResult.None;
            _ImageButton7.DownImage = My.Resources.Resources.ExampleButtonDownA;
            _ImageButton7.Font = new Font("Microsoft Sans Serif", 14.25f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _ImageButton7.HoverImage = My.Resources.Resources.ExampleButtonHoverA;
            _ImageButton7.Location = new Point(432, 135);
            _ImageButton7.Margin = new Padding(3, 4, 3, 4);
            _ImageButton7.Name = "_ImageButton7";
            _ImageButton7.NormalImage = null;
            _ImageButton7.Size = new Size(100, 50);
            _ImageButton7.SizeMode = PictureBoxSizeMode.AutoSize;
            _ImageButton7.TabIndex = 7;
            _ImageButton7.TabStop = false;
            _ImageButton7.Text = "Example F";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(66, 126);
            label2.Name = "label2";
            label2.Size = new Size(104, 17);
            label2.TabIndex = 8;
            label2.Text = "Default button ^";
            // 
            // TraceMessageToolStrip1
            // 
            TraceMessageToolStrip1.Font = new Font("Segoe UI", 9.75f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            TraceMessageToolStrip1.GripStyle = ToolStripGripStyle.Hidden;
            TraceMessageToolStrip1.Location = new Point(0, 0);
            TraceMessageToolStrip1.Name = "TraceMessageToolStrip1";
            TraceMessageToolStrip1.Size = new Size(649, 29);
            TraceMessageToolStrip1.Stretch = true;
            TraceMessageToolStrip1.TabIndex = 9;
            TraceMessageToolStrip1.Text = "TraceMessageToolStrip1";
            // 
            // Dashboard
            // 
            AcceptButton = _ImageButton1;
            AutoScaleDimensions = new SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(649, 251);
            Controls.Add(TraceMessageToolStrip1);
            Controls.Add(_ImageButton7);
            Controls.Add(_ImageButton6);
            Controls.Add(_ImageButton5);
            Controls.Add(_ImageButton4);
            Controls.Add(_ImageButton3);
            Controls.Add(label1);
            Controls.Add(_ImageButton2);
            Controls.Add(_ImageButton1);
            Controls.Add(label2);
            Margin = new Padding(3, 4, 3, 4);
            MaximizeBox = false;
            Name = "Dashboard";
            ShowIcon = false;
            Text = "Image Button Demo";
            ((System.ComponentModel.ISupportInitialize)_ImageButton1).EndInit();
            ((System.ComponentModel.ISupportInitialize)_ImageButton3).EndInit();
            ((System.ComponentModel.ISupportInitialize)_ImageButton2).EndInit();
            ((System.ComponentModel.ISupportInitialize)_ImageButton4).EndInit();
            ((System.ComponentModel.ISupportInitialize)_ImageButton5).EndInit();
            ((System.ComponentModel.ISupportInitialize)_ImageButton6).EndInit();
            ((System.ComponentModel.ISupportInitialize)_ImageButton7).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        private Controls.ImageButton _ImageButton1;

        private Controls.ImageButton ImageButton1
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _ImageButton1;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_ImageButton1 != null)
                {
                    _ImageButton1.Click -= ImageButton1_Click;
                }

                _ImageButton1 = value;
                if (_ImageButton1 != null)
                {
                    _ImageButton1.Click += ImageButton1_Click;
                }
            }
        }

        private Controls.ImageButton _ImageButton2;

        private Controls.ImageButton ImageButton2
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _ImageButton2;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_ImageButton2 != null)
                {
                    _ImageButton2.Click -= ImageButton2_Click;
                }

                _ImageButton2 = value;
                if (_ImageButton2 != null)
                {
                    _ImageButton2.Click += ImageButton2_Click;
                }
            }
        }

        private Controls.ImageButton _ImageButton3;

        private Controls.ImageButton ImageButton3
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _ImageButton3;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_ImageButton3 != null)
                {
                    _ImageButton3.Click -= ImageButton3_Click;
                }

                _ImageButton3 = value;
                if (_ImageButton3 != null)
                {
                    _ImageButton3.Click += ImageButton3_Click;
                }
            }
        }

        private Label label1;
        private Controls.ImageButton _ImageButton4;

        private Controls.ImageButton ImageButton4
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _ImageButton4;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_ImageButton4 != null)
                {
                    _ImageButton4.Click -= ImageButton4_Click;
                }

                _ImageButton4 = value;
                if (_ImageButton4 != null)
                {
                    _ImageButton4.Click += ImageButton4_Click;
                }
            }
        }

        private Controls.ImageButton _ImageButton5;

        private Controls.ImageButton ImageButton5
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _ImageButton5;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_ImageButton5 != null)
                {
                    _ImageButton5.Click -= ImageButton5_Click;
                }

                _ImageButton5 = value;
                if (_ImageButton5 != null)
                {
                    _ImageButton5.Click += ImageButton5_Click;
                }
            }
        }

        private Controls.ImageButton _ImageButton6;

        private Controls.ImageButton ImageButton6
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _ImageButton6;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_ImageButton6 != null)
                {
                    _ImageButton6.Click -= ImageButton6_Click;
                }

                _ImageButton6 = value;
                if (_ImageButton6 != null)
                {
                    _ImageButton6.Click += ImageButton6_Click;
                }
            }
        }

        private Controls.ImageButton _ImageButton7;

        private Controls.ImageButton ImageButton7
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _ImageButton7;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_ImageButton7 != null)
                {
                    _ImageButton7.Click -= ImageButton7_Click;
                }

                _ImageButton7 = value;
                if (_ImageButton7 != null)
                {
                    _ImageButton7.Click += ImageButton7_Click;
                }
            }
        }

        private Label label2;
        private Forma.TraceMessageToolStrip TraceMessageToolStrip1;
    }
}
