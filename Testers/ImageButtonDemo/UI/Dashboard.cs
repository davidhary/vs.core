using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using isr.Core.Forma;
using Microsoft.VisualBasic;

namespace isr.Core.Tester
{

    /// <summary>   A dashboard. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    public partial class Dashboard : FormBase
    {
        /// <summary>   True to hide, false to show the alerts. </summary>

        private bool _HideAlerts = false;

        /// <summary>   Specialized default constructor for use only by derived classes. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        public Dashboard()
        {
            this.InitializeComponent();
            this.TraceMessages = new TraceMessagesStack();
            this.TraceMessageToolStrip1.TraceMessagesStack = this.TraceMessages;
        }
        /// <summary>   Identifier for the trace event. </summary>

        private const int _TraceEventId = 0x11;

        /// <summary>   Gets or sets the trace messages. </summary>
        /// <value> The trace messages. </value>
        private TraceMessagesStack TraceMessages { get; set; }

        /// <summary>   Image button 1 click. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void ImageButton1_Click(object sender, EventArgs e)
        {
            if (!this._HideAlerts )
            {
                // MessageBox.Show("Clicked default button.")
                this.TraceMessages.Push(new TraceMessage(TraceEventType.Information, _TraceEventId, "Clicked default button."));
            }
        }

        /// <summary>   Image button 2 click. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void ImageButton2_Click(object sender, EventArgs e)
        {
            if (!this._HideAlerts )
            {
                // MessageBox.Show("Clicked button B.")
                this.TraceMessages.Push(new TraceMessage(TraceEventType.Verbose, _TraceEventId, "Clicked button B"));
            }
        }

        /// <summary>   Image button 3 click. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void ImageButton3_Click(object sender, EventArgs e)
        {
            this._HideAlerts = !this._HideAlerts;
            if ( this._HideAlerts )
            {
                this.ImageButton3.NormalImage = My.Resources.Resources.CCheckedNormal;
                this.ImageButton3.HoverImage = My.Resources.Resources.CCheckedHover;
                this.ImageButton3.DownImage = My.Resources.Resources.CCheckedDown;
            }
            else
            {
                this.ImageButton3.NormalImage = My.Resources.Resources.CUncheckedNormal;
                this.ImageButton3.HoverImage = My.Resources.Resources.CUncheckedHover;
                this.ImageButton3.DownImage = My.Resources.Resources.CUncheckedDown;
            }
        }

        /// <summary>   Image button 4 click. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void ImageButton4_Click(object sender, EventArgs e)
        {
            if (!this._HideAlerts )
            {
                // MessageBox.Show("Clicked button C.")
                this.TraceMessages.Push(new TraceMessage(TraceEventType.Warning, _TraceEventId, "Clicked button C"));
            }
        }

        /// <summary>   Image button 5 click. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void ImageButton5_Click(object sender, EventArgs e)
        {
            if (!this._HideAlerts )
            {
                // MessageBox.Show("Clicked button D.")
                this.TraceMessages.Push(new TraceMessage(TraceEventType.Error, _TraceEventId, "Clicked button D"));
            }
        }

        /// <summary>   Image button 6 click. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void ImageButton6_Click(object sender, EventArgs e)
        {
            if (!this._HideAlerts )
            {
                // MessageBox.Show("Clicked button E.")
                this.TraceMessages.Push(new TraceMessage(TraceEventType.Information, _TraceEventId, "Clicked button E"));
            }
        }

        /// <summary>   Image button 7 click. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void ImageButton7_Click(object sender, EventArgs e)
        {
            if (!this._HideAlerts )
            {
                // MessageBox.Show("Clicked button F.")
                this.TraceMessages.Push(new TraceMessage(TraceEventType.Information, _TraceEventId, "Clicked button F"));
            }
        }

        /// <summary>   Gets or sets the shadow distance. </summary>
        /// <value> The shadow distance. </value>
        public int ShadowDistance { get; set; } = 4;

        /// <summary>   Draws and Fills a Rounded Rectangle and it's accompanying shadow. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="picCanvas">    The picture canvas. </param>
        /// <param name="e">            [in,out] PaintEventArgs object passed in from the Picture box. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Exception is published")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>")]
        private void DrawRndRect(Control picCanvas, ref PaintEventArgs e)
        {
            // I like clean lines so set the smoothing mode to Anti-Alias
            e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;

            // lets create a rectangle that will be centered in the picture box and
            // just under half the size
            var _Rectangle = new Rectangle((int)Math.Round(Conversion.Fix(picCanvas.Width * 0.3d)), (int)Math.Round(Conversion.Fix(picCanvas.Height * 0.3d)), (int)Math.Round(Conversion.Fix(picCanvas.Width * 0.4d)), (int)Math.Round(Conversion.Fix(picCanvas.Height * 0.4d)));

            // create the radius variable and set it equal to 20% the height of the rectangle
            // this will determine the amount of bend at the corners
            float _Radius = (int)Math.Round(Conversion.Fix(_Rectangle.Height * 0.2d));

            // create an x and y variable so that we can reduce the length of our code lines
            float x = _Rectangle.Left;
            float y = _Rectangle.Top;

            // make sure that we have a valid radius, too small and we have a problem
            if (_Radius < 1f)
            {
                _Radius = 1f;
            }

            try
            {
                // Create a graphics path object with the using operator so the framework
                // can clean up the resources for us
                using var _Path = new GraphicsPath();
                // build the rounded rectangle starting at the top line and going around
                // until the line meets itself again
                _Path.AddLine( x + _Radius, y, x + _Rectangle.Width - _Radius * 2f, y );
                _Path.AddArc( x + _Rectangle.Width - _Radius * 2f, y, _Radius * 2f, _Radius * 2f, 270f, 90f );
                _Path.AddLine( x + _Rectangle.Width, y + _Radius, x + _Rectangle.Width, y + _Rectangle.Height - _Radius * 2f );
                _Path.AddArc( x + _Rectangle.Width - _Radius * 2f, y + _Rectangle.Height - _Radius * 2f, _Radius * 2f, _Radius * 2f, 0f, 90f );
                _Path.AddLine( x + _Rectangle.Width - _Radius * 2f, y + _Rectangle.Height, x + _Radius, y + _Rectangle.Height );
                _Path.AddArc( x, y + _Rectangle.Height - _Radius * 2f, _Radius * 2f, _Radius * 2f, 90f, 90f );
                _Path.AddLine( x, y + _Rectangle.Height - _Radius * 2f, x, y + _Radius );
                _Path.AddArc( x, y, _Radius * 2f, _Radius * 2f, 180f, 90f );

                // this is where we create the shadow effect, so we will use a 
                // path gradient brush
                using ( var _Brush = new PathGradientBrush( _Path ) )
                {
                    // set the wrap mode so that the colors will layer themselves
                    // from the outer edge in
                    _Brush.WrapMode = WrapMode.Clamp;

                    // Create a color blend to manage our colors and positions and
                    // since we need 3 colors set the default length to 3

                    // here is the important part of the shadow making process, remember
                    // the clamp mode on the color blend object layers the colors from
                    // the outside to the center so we want our transparent color first
                    // followed by the actual shadow color. Set the shadow color to a 
                    // slightly transparent DimGray, I find that it works best.

                    // our color blend will control the distance of each color layer
                    // we want to set our transparent color to 0 indicating that the 
                    // transparent color should be the outer most color drawn, then
                    // our gray color at about 10% of the distance from the edge

                    var _ColorBlend = new ColorBlend( 3 ) {
                        Colors = new Color[] { Color.Transparent, Color.FromArgb( 180, Color.DimGray ), Color.FromArgb( 180, Color.DimGray ) },
                        Positions = new float[] { 0f, 0.1f, 1.0f }
                    };

                    // assign the color blend to the path gradient brush
                    _Brush.InterpolationColors = _ColorBlend;

                    // fill the shadow with our path gradient brush
                    e.Graphics.FillPath( _Brush, _Path );
                }

                // since the shadow was drawn first we need to move the actual path
                // up and back a little so that we can show the shadow underneath
                // the object. To accomplish this we will create a Matrix Object
                var _Matrix = new Matrix();

                // tell the matrix to move the path up and back the designated distance
                _Matrix.Translate( this.ShadowDistance, this.ShadowDistance );

                // assign the matrix to the graphics path of the rounded rectangle
                _Path.Transform( _Matrix );

                // fill the graphics path first
                using ( var _Brush = new LinearGradientBrush( picCanvas.ClientRectangle, Color.Tomato, Color.MistyRose, LinearGradientMode.Vertical ) )
                {
                    e.Graphics.FillPath( _Brush, _Path );
                }

                // Draw the Graphics path last so that we have cleaner borders
                using var _Pen = new Pen( Color.DimGray, 1.0f );
                e.Graphics.DrawPath( _Pen, _Path );
            }
            catch (Exception ex)
            {
                Debug.WriteLine( this.GetType().Name + ".DrawRndRect() Error: " + ex.Message);
            }
        }

    }
}
