using System;

namespace isr.Core.Tester
{
    public partial class FormSwitch
    {
        /// <summary>
	/// Required designer variable.
	/// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
	/// Clean up any resources being used.
	/// </summary>
	/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && components is object)
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        /// <summary>
        /// Required method for Designer support - do not modify the contents of this method with the
        /// code editor.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        private void InitializeComponent()
        {
            button1 = new System.Windows.Forms.Button();
            button2 = new System.Windows.Forms.Button();
            button3 = new System.Windows.Forms.Button();
            SuspendLayout();
            // 
            // button1
            // 
            button1.Location = new System.Drawing.Point(43, 41);
            button1.Name = "button1";
            button1.Size = new System.Drawing.Size(81, 27);
            button1.TabIndex = 0;
            button1.Text = "Demo 1";
            button1.UseVisualStyleBackColor = true;
            button1.Click += new EventHandler(Button1_Click);
            // 
            // button2
            // 
            button2.Location = new System.Drawing.Point(189, 41);
            button2.Name = "button2";
            button2.Size = new System.Drawing.Size(81, 27);
            button2.TabIndex = 1;
            button2.Text = "Demo 2";
            button2.UseVisualStyleBackColor = true;
            button2.Click += new EventHandler(Button2_Click);
            // 
            // button3
            // 
            button3.Location = new System.Drawing.Point(353, 41);
            button3.Name = "button3";
            button3.Size = new System.Drawing.Size(81, 27);
            button3.TabIndex = 2;
            button3.Text = "Demo 3";
            button3.UseVisualStyleBackColor = true;
            button3.Click += new EventHandler(Button3_Click);
            // 
            // FormSwitch
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(532, 135);
            Controls.Add(button3);
            Controls.Add(button2);
            Controls.Add(button1);
            FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "FormSwitch";
            Text = "Shape Control Demo";
            ResumeLayout(false);
        }

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
    }
}
