using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using isr.Core.Controls;

namespace isr.Core.Tester
{

    /// <summary>   A form 3. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    public partial class Form3 : Form
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Windows.Forms.Form" /> class.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        public Form3()
        {

            /// <summary> Form 3 key down. </summary>
    /// <remarks> David, 2021-03-12. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Key event information. </param>
            base.KeyDown += Form3_KeyDown;

            /// <summary> Form 3 key up. </summary>
    /// <remarks> David, 2021-03-12. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Key event information. </param>
            base.KeyUp += Form3_KeyUp;
            base.Load += Form3_Load;

            /// <summary> Form 3 resize. </summary>
    /// <remarks> David, 2021-03-12. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
            base.Resize += Form3_Resize;
            InitializeComponent();
        }
        /// <summary>   List of controls. </summary>

        private List<ShapeControl> _ControlList = new List<ShapeControl>();
        /// <summary>   The sx. </summary>
        private int _Sx;
        /// <summary>   The sy. </summary>
        private int _Sy;
        /// <summary>   The static i. </summary>

        private int _Static_i = 0;
        /// <summary>   True to control key. </summary>
        private bool _CtrlKey = false;
        /// <summary>   True to alternate key. </summary>
        private bool _AltKey = false;
        /// <summary>   True to plus key. </summary>
        private bool _PlusKey = false;
        /// <summary>   True to minus key. </summary>
        private bool _MinusKey = false;

        /// <summary>   Button add camera click. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void BtnAddCam_Click(object sender, EventArgs e)
        {
            AddCam("");
        }

        /// <summary>   Gets the zero-based index of the next camera. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <returns>   The next camera index. </returns>
        private int GetNextCamIndex()
        {
            if (_ControlList.Count == 0)
            {
                return 1;
            }

            object tempvar;
            tempvar = _ControlList.OrderBy(x => x.Name).ToList();
            _ControlList = (List<ShapeControl>)tempvar;
            _ControlList = _ControlList.OrderBy(x => x.Name.Length).ToList();
            var templist = _ControlList.ToList();
            int count = templist.Count;
            int retval = count + 1;

            // find missing index
            for (int i = 0, loopTo = count - 1; i <= loopTo; i++)
            {
                string ctrlname = templist[i].Name;
                string ctrlindex = ctrlname.Substring(3);
                if (i + 1 != int.Parse(ctrlindex))
                {
                    retval = i + 1;
                    break;
                }
            }

            return retval;
        }

        /// <summary>   Adds a camera. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="caminfo">  The caminfo. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "y")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "x")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        private void AddCam(string caminfo)
        {
            bool bNew = string.IsNullOrEmpty(caminfo);
            string name = string.Empty;
            string tag = string.Empty;
            string tag2 = string.Empty;
            int x = 0;
            int y = 0;
            int w = 0;
            int h = 0;
            int c = 0;
            if (!string.IsNullOrEmpty(caminfo))
            {
                var info = caminfo.Split('|');
                for (int i = 0, loopTo = info.Length - 1; i <= loopTo; i++)
                {
                    var details = info[i].Split('=');
                    switch (details[0] ?? "")
                    {
                        case "name":
                            {
                                name = details[1];
                                break;
                            }

                        case "x":
                            {
                                x = int.Parse(details[1]);
                                break;
                            }

                        case "y":
                            {
                                y = int.Parse(details[1]);
                                break;
                            }

                        case "w":
                            {
                                w = int.Parse(details[1]);
                                break;
                            }

                        case "h":
                            {
                                h = int.Parse(details[1]);
                                break;
                            }

                        case "c":
                            {
                                c = int.Parse(details[1]);
                                break;
                            }

                        case "tag":
                            {
                                tag = details[1];
                                break;
                            }

                        case "tag2":
                            {
                                tag2 = details[1];
                                break;
                            }
                    }
                }
            }
            // ctrllist.Add(ctrl1);
            var ctrl1 = new ShapeControl()
            {
                BackColor = bNew ? Color.FromArgb(126, Color.Red) : Color.FromArgb(c),
                Blink = false,
                BorderColor = Color.FromArgb(0, 255, 255, 255),
                BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid,
                BorderWidth = 3,
                Font = new Font("Arial", 8.0f, FontStyle.Bold),
                Name = bNew ? "cam" + GetNextCamIndex() : name,
                Shape = ShapeType.Ellipse,
                ShapeImage = Properties.Resources.camshape,
                Size = bNew ? new Size(40, 40) : new Size(w, h),
                TabIndex = 0,
                UseGradient = false,
                Vibrate = false,
                Visible = true
            };
            // ctrl1.CenterColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            // ctrllist.Count;
            // ctrl1.SurroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));

            ctrl1.MouseDown += new MouseEventHandler(Ctrl1_MouseDown);
            ctrl1.MouseMove += new MouseEventHandler(Ctrl1_MouseMove);
            ctrl1.MouseDoubleClick += new MouseEventHandler(Ctrl1_MouseDoubleClick);
            ctrl1.MouseHover += new EventHandler(Ctrl1_MouseHover);
            _ControlList.Add(ctrl1);
            int ypos = 50 * _ControlList.Count % Panel1.Height;
            int xpos = 50 * _ControlList.Count / Panel1.Height * 50;
            ctrl1.Location = bNew ? new Point(50 + xpos, ypos - 40) : new Point(50, 50);
            Panel1.Controls.Add(ctrl1);
            ctrl1.Text = "cam";
            ctrl1.Text = bNew ? (string)ctrl1.Name.ToString().Clone() : name;
            ctrl1.BringToFront();
            ctrl1.Tag2 = bNew ? "127.0.0.1:New cam" : tag2;
            // set the color
            if (bNew)
            {
                Ctrl1_MouseDoubleClick(ctrl1, new MouseEventArgs(MouseButtons.Left, 2, 0, 0, 0));
            }

            float dy = ctrl1.Top + ctrl1.Height / 2 - Panel1.Height / 2f;
            float dx = ctrl1.Left + ctrl1.Width / 2 - Panel1.Width / 2f;
            ctrl1.Tag = bNew ? dx + "," + dy + "," + GetNumPixelforImageDisplayed() : tag;
        }

        /// <summary>   Event handler. Called by Ctrl1 for mouse hover events. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void Ctrl1_MouseHover(object sender, EventArgs e)
        {
            ShapeControl ctrl = sender as ShapeControl;
            if (ctrl is null)
                return;
            toolTip1.Show(ctrl.Tag2 + ",(" + ctrl.Left + "," + ctrl.Top + ")", ctrl, 2000);
        }

        /// <summary>   Event handler. Called by Ctrl1 for mouse double click events. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1300:SpecifyMessageBoxOptions")]
        private void Ctrl1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)
            {
                return;
            }

            ShapeControl ctrl = sender as ShapeControl;
            if (ctrl is null)
                return;
            if (e.Button.Equals(MouseButtons.Left))
            {
                if (_PlusKey && !_MinusKey)
                {
                    if (ctrl.Width < 80)
                    {
                        ctrl.Size = new Size(ctrl.Width + 5, ctrl.Height + 5);
                    }

                    _PlusKey = false;
                    return;
                }

                if (_MinusKey && !_PlusKey)
                {
                    if (ctrl.Width > 20)
                    {
                        ctrl.Size = new Size(ctrl.Width - 5, ctrl.Height - 5);
                    }

                    _MinusKey = false;
                    return;
                }

                if (_CtrlKey && !_AltKey)
                {
                    var dr = MessageBox.Show(this, "Delete cam?", "Delete", MessageBoxButtons.OKCancel);
                    if (dr == DialogResult.OK)
                    {
                        _ControlList.Remove(ctrl);
                        Panel1.Controls.Remove(ctrl);
                    }

                    _CtrlKey = false;
                    return;
                }

                if (_AltKey && !_CtrlKey)
                {
                    ctrl.Vibrate = !ctrl.Vibrate;
                    _AltKey = false;
                    return;
                }

                if (_Static_i >= 6)
                {
                    _Static_i = 0;
                }

                switch (_Static_i)
                {
                    case 0:
                        {
                            ctrl.BackColor = Color.FromArgb(126, Color.Red);
                            break;
                        }

                    case 1:
                        {
                            ctrl.BackColor = Color.FromArgb(126, Color.Blue);
                            break;
                        }

                    case 2:
                        {
                            ctrl.BackColor = Color.FromArgb(126, Color.Green);
                            break;
                        }

                    case 3:
                        {
                            ctrl.BackColor = Color.FromArgb(126, Color.Wheat);
                            break;
                        }

                    case 4:
                        {
                            ctrl.BackColor = Color.FromArgb(126, Color.GreenYellow);
                            break;
                        }

                    case 5:
                        {
                            ctrl.BackColor = Color.FromArgb(126, Color.Cyan);
                            break;
                        }
                }

                _Static_i += 1;
            }
        }

        /// <summary>   Event handler. Called by Ctrl1 for mouse down events. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        private void Ctrl1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button.Equals(MouseButtons.Left))
            {
                _Sx = e.X;
                _Sy = e.Y;
            }

            if (e.Button.Equals(MouseButtons.Right))
            {
                var frm = new FormProperty() { Caller = (ShapeControl)sender };
                frm.ShowDialog();
            }
        }

        /// <summary>   Event handler. Called by Ctrl1 for mouse move events. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void Ctrl1_MouseMove(object sender, MouseEventArgs e)
        {
            Control ctrl = sender as Control;
            if (ctrl is null)
                return;
            if (e.Button.Equals(MouseButtons.Left))
            {
                ctrl.Left += e.X - _Sx;
                ctrl.Top += e.Y - _Sy;
                float dy = ctrl.Top + (float)(ctrl.Height / 2) - Panel1.Height / 2f;
                float dx = ctrl.Left + (float)(ctrl.Width / 2) - Panel1.Width / 2f;
                ctrl.Tag = dx + "," + dy + "," + GetNumPixelforImageDisplayed();
            }
        }

        /// <summary>   Event handler. Called by Form3 for key down events. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Key event information. </param>
        private void Form3_KeyDown(object sender, KeyEventArgs e)
        {
            _CtrlKey = e.Control;
            _AltKey = e.Alt;
            if (e.KeyCode == Keys.OemMinus)
            {
                _MinusKey = true;
            }

            if (e.KeyCode == Keys.Oemplus)
            {
                _PlusKey = true;
            }
        }

        /// <summary>   Event handler. Called by Form3 for key up events. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Key event information. </param>
        private void Form3_KeyUp(object sender, KeyEventArgs e)
        {
            _CtrlKey = false;
            _AltKey = false;
            _MinusKey = false;
            _PlusKey = false;
        }

        /// <summary>   Panel 1 mouse double click. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Exception is published")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        private void Panel1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            openFileDialog1.Filter = "Image files (*.bmp;*.jpg;*.gif)|*.bmp;*.jpg;*.gif|All files (*.*)|*.*";
            var dr = openFileDialog1.ShowDialog();
            if (dr == DialogResult.OK)
            {
                try
                {
                    var tempImage = new Bitmap(openFileDialog1.FileName);
                    Panel1.BackgroundImage = new Bitmap(tempImage);
                }
                catch
                {
                }
            }
        }

        /// <summary>   Gets number pixelfor image displayed. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <returns>   The number pixelfor image displayed. </returns>
        private int GetNumPixelforImageDisplayed()
        {
            if (Panel1.BackgroundImage is null)
                return default;
            float panelratio = Panel1.Width / (float)Panel1.Height;
            float imgratio = Panel1.BackgroundImage.Width / (float)Panel1.BackgroundImage.Height;
            float dispwidth;
            float dispheight;
            if (panelratio > imgratio)
            {
                // height limiting
                dispheight = Panel1.Height;
                dispwidth = imgratio * dispheight;
            }
            else
            {
                dispwidth = Panel1.Width;
                dispheight = dispwidth / imgratio;
            }

            // System.Diagnostics.Debug.Print(imgratio +"," + dispwidth + "," + dispheight);

            return (int)Math.Round(Math.Truncate(dispwidth * dispheight));
        }

        /// <summary>   Button 2 click. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "NewMap")]
        private void Button2_Click(object sender, EventArgs e)
        {
            label1.Text = "NewMap_" + Guid.NewGuid().ToString() + ".map";
            Panel1.BackgroundImage = new Bitmap(Panel1.Width, Panel1.Height);
            Graphics.FromImage(Panel1.BackgroundImage).FillRectangle(Brushes.White, new Rectangle(0, 0, Panel1.Width, Panel1.Height));
            Graphics.FromImage(Panel1.BackgroundImage).DrawString("Dbl Click here to insert floor plan..", new Font(FontFamily.GenericSansSerif, 12f), Brushes.Black, 50f, 50f);
            _ControlList.Clear();
            Panel1.Controls.Clear();
        }

        /// <summary>   Button 1 click. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1300:SpecifyMessageBoxOptions")]
        private void Button1_Click(object sender, EventArgs e)
        {
            using (var writer = File.CreateText(label1.Text))
            {
                _ControlList = _ControlList.OrderBy(x => x.Name).ToList();
                _ControlList = _ControlList.OrderBy(x => x.Name.Length).ToList();
                var templist = _ControlList.ToList();
                writer.WriteLine("CAM_COUNT=" + templist.Count);
                for (int i = 0, loopTo = _ControlList.Count - 1; i <= loopTo; i++)
                    writer.WriteLine("name=" + templist[i].Name + "|" + "x=" + templist[i].Left + "|" + "y=" + templist[i].Top + "|" + "w=" + templist[i].Width + "|" + "h=" + templist[i].Height + "|" + "c=" + templist[i].BackColor.ToArgb() + "|" + "tag=" + templist[i].Tag.ToString() + "|" + "tag2=" + templist[i].Tag2.ToString());
            }

            if (Panel1.BackgroundImage is object)
            {
                Panel1.BackgroundImage.Save(label1.Text + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
            }

            MessageBox.Show(label1.Text + " is saved");
        }

        /// <summary>   Form 3 load. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1304:SpecifyCultureInfo", MessageId = "System.Type.InvokeMember(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object,System.Object[])")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "DblClick")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "Ctl")]
        private void Form3_Load(object sender, EventArgs e)
        {
            DoubleBuffered = true;

            // invoke double buffer 
            typeof(Panel).InvokeMember("DoubleBuffered", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.SetProperty, null, Panel1, new object[] { true });
            label2.Text = "On Cam> Right Click:Set Properties, Dbl_Click:Change Color, Ctl+Dbl_Click:Del, Alt+Dbl_Click:Vibrate, Minus+Dbl_Click:Smaller, Plus+Dbl_Click:Larger";
            Button2_Click(null, null);
        }

        /// <summary>   Button import map click. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Exception is published")]
        private void BtnImportMap_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Map files (*.map)|*.map";
            var dr = openFileDialog1.ShowDialog();
            if (dr == DialogResult.OK)
            {
                Button2_Click(null, null);
                label1.Text = openFileDialog1.FileName;
                try
                {
                    using (var reader = File.OpenText(label1.Text))
                    {
                        string s = reader.ReadLine();
                        var info = s.Split('=');
                        for (int i = 0, loopTo = int.Parse(info[1]) - 1; i <= loopTo; i++)
                        {
                            s = reader.ReadLine();
                            AddCam(s);
                        }
                    }

                    if (File.Exists(openFileDialog1.FileName + ".jpg"))
                    {
                        using (var tempImage = new Bitmap(openFileDialog1.FileName + ".jpg"))
                        {
                            Panel1.BackgroundImage = new Bitmap(tempImage);
                        }
                    }


                    // resize


                    UpdateCamPosAfterResize();
                }
                catch
                {
                }
            }
        }

        /// <summary>   Updates the camera position after resize. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        private void UpdateCamPosAfterResize()
        {
            int newarea = GetNumPixelforImageDisplayed();
            for (int i = 0, loopTo = _ControlList.Count - 1; i <= loopTo; i++)
            {
                var info = _ControlList[i].Tag.ToString().Split(',');
                float dx = float.Parse(info[0]);
                float dy = float.Parse(info[1]);
                int area = int.Parse(info[2]);
                // square root of area ratio = linear ratio
                float ratio = (float)Math.Sqrt(newarea / (float)area);
                // get the new offset using the calculated linear ratio
                float newdx = ratio * dx;
                float newdy = ratio * dy;


                // update the new pos for the cam 
                _ControlList[i].Left = (int)Math.Round(Math.Truncate(Panel1.Width / 2 + newdx - _ControlList[i].Width / 2));
                _ControlList[i].Top = (int)Math.Round(Math.Truncate(Panel1.Height / 2 + newdy - _ControlList[i].Height / 2));
            }
        }

        /// <summary>   Event handler. Called by Form3 for resize events. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void Form3_Resize(object sender, EventArgs e)
        {
            Panel1.Visible = false;
            Panel1.Height = ClientSize.Height - 3 * Panel1.Top / 2;
            Panel1.Width = ClientSize.Width - 2 * Panel1.Left;
            label2.Top = Panel1.Top + Panel1.Height + 10;
            UpdateCamPosAfterResize();
            Panel1.Visible = true;
        }
    }
}
