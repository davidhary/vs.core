﻿using System;
using System.Windows.Forms;
using isr.Core.Controls;

namespace isr.Core.Tester
{

    /// <summary>   A form property. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    public partial class FormProperty : Form
    {
        /// <summary>   The caller. </summary>

        private ShapeControl _Caller = null;

        /// <summary>   Gets or sets the caller. </summary>
        /// <value> The caller. </value>
        public ShapeControl Caller
        {
            get
            {
                return _Caller;
            }

            set
            {
                _Caller = value;
                if (value is object)
                {
                    string s = _Caller.Tag2;
                    var info = s.Split(':');
                    textBoxIP.Text = info[0];
                    textBoxNotes.Text = info[1];
                    Text = _Caller.Text + " properties";
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Windows.Forms.Form" /> class.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        public FormProperty()
        {
            InitializeComponent();
        }

        /// <summary>   Event handler. Called by FormProperty for form closing events. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Form closing event information. </param>
        private void FormProperty_FormClosing(object sender, FormClosingEventArgs e)
        {
            // should validate first
            _Caller.Tag2 = textBoxIP.Text + ":" + textBoxNotes.Text;
        }

        /// <summary>   Event handler. Called by FormProperty for activated events. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void FormProperty_Activated(object sender, EventArgs e)
        {
            Location = _Caller.Location;
        }

        /// <summary>   Event handler. Called by TextBoxIP for enter events. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void TextBoxIP_Enter(object sender, EventArgs e)
        {
            textBoxIP.SelectionStart = textBoxIP.Text.Length;
        }
    }
}