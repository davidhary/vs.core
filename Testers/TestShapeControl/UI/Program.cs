﻿/// <summary>
/// Constructor that prevents a default instance of this class from being created.
/// </summary>
/// <remarks> David, 2021-03-12. </remarks>
using System;
using System.Windows.Forms;

namespace isr.Core.Tester
{
    /// <summary>   A program. This class cannot be inherited. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    public sealed class Program
    {

        /// <summary>   Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        private Program()
        {
        }

        /// <summary>   Main entry-point for this application. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="args"> An array of command-line argument strings. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>")]
        [STAThread]
        internal static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var f2 = new FormSwitch();
            // MultiCamForm f1 = new MultiCamForm();
            Application.Run(f2);
        }
    }
}