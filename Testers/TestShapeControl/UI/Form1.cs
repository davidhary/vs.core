using System;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;

namespace isr.Core.Tester
{

    /// <summary>   Summary description for Form1. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    public partial class Form1 : Form
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Windows.Forms.Form" /> class.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public Form1()
        {

            base.Load += Form1_Load;

            // 
            // Required for Windows Form Designer support
            // 
            InitializeComponent();

            // 
            // TODO: Add any constructor code after InitializeComponent call
            // 
            bm = new Bitmap(panel1.Width, panel1.Height);
            var g = Graphics.FromImage(bm);
            g.FillRectangle(Brushes.LightBlue, new Rectangle(0, 0, panel1.Width, panel1.Height));
            using (var sf = new StringFormat() { Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center })
            {
                using (var arialFont = new Font("Arial", 18f, FontStyle.Bold))
                {
                    g.DrawString("THIS IS THE TEST BACKGROUND", arialFont, Brushes.Brown, new Rectangle(new Point(0, 0), new Size(panel1.Width, panel1.Height)), sf);
                }
            }

            panel1.BackgroundImage = bm;
        }

        /// <summary>   Clean up any resources being used. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="disposing">    <see langword="true" /> to release both managed and unmanaged
        ///                             resources; <see langword="false" /> to release only unmanaged
        ///                             resources. </param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components is object)
                {
                    components.Dispose();
                    if (bm is object)
                    {
                        bm.Dispose();
                        bm = null;
                    }
                }
            }

            base.Dispose(disposing);
        }

        /// <summary>   The main entry point for the application. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        [STAThread]
        private static void Main()
        {
            Application.Run(new Form1());
        }

        /// <summary>   Shape control 1 mouse down. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void ShapeControl1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button.Equals(MouseButtons.Left))
            {
                sx = e.X;
                // temp region = ((ShapeControl)sender).Region.Clone();
                // ((ShapeControl)sender).Region = null;
                sy = e.Y;
            }
        }

        /// <summary>   Shape control 1 mouse move. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void ShapeControl1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button.Equals(MouseButtons.Left))
            {
                Control ctrl = sender as Control;
                if (ctrl is object)
                {
                    ctrl.Left += e.X - sx;
                    ctrl.Top += e.Y - sy;
                }
            }
        }

        /// <summary>   Event handler. Called by Form1 for load events. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void Form1_Load(object sender, EventArgs e)
        {
            DoubleBuffered = true;
            typeof(Panel).InvokeMember(nameof(DoubleBuffered), BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.SetProperty, null, panel1, new object[] { true }, System.Globalization.CultureInfo.InvariantCulture);
        }

        /// <summary>   Shape control 4 click. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void ShapeControl4_Click(object sender, EventArgs e)
        {
            shapeControl5.Visible = true;
            Refresh();
            System.Threading.Thread.Sleep(1000);
            shapeControl5.Visible = false;
            Refresh();
        }

        /// <summary>   Shape control 3 click. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void ShapeControl3_Click(object sender, EventArgs e)
        {
            Close();
        }

    }
}
