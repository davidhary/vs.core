using System;
using System.Drawing;
using System.Runtime.CompilerServices;
using isr.Core.Controls;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Tester
{
    public partial class Form1
    {
        private ShapeControl _ShapeControl1;

        private ShapeControl ShapeControl1
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _ShapeControl1;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_ShapeControl1 != null)
                {
                    _ShapeControl1.MouseDown -= ShapeControl1_MouseDown;
                    _ShapeControl1.MouseMove -= ShapeControl1_MouseMove;
                }

                _ShapeControl1 = value;
                if (_ShapeControl1 != null)
                {
                    _ShapeControl1.MouseDown += ShapeControl1_MouseDown;
                    _ShapeControl1.MouseMove += ShapeControl1_MouseMove;
                }
            }
        }
        /// <summary>   Required designer variable. </summary>
        private System.ComponentModel.Container components = null;
        private System.Windows.Forms.Panel panel1;
        private Bitmap bm;
        private int sx;
        private int sy;
        private ShapeControl ShapeControl3;
        private ShapeControl _ShapeControl4;

        private ShapeControl ShapeControl4
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _ShapeControl4;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_ShapeControl4 != null)
                {
                    _ShapeControl4.Click -= ShapeControl4_Click;
                }

                _ShapeControl4 = value;
                if (_ShapeControl4 != null)
                {
                    _ShapeControl4.Click += ShapeControl4_Click;
                }
            }
        }

        private ShapeControl shapeControl5;
        private ShapeControl shapeControl7;
        private ShapeControl shapeControl6;
        private ShapeControl customControl11;
        private ShapeControl shapeControl2;

        /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panel1 = new System.Windows.Forms.Panel();
            this._ShapeControl1 = new isr.Core.Controls.ShapeControl();
            this.shapeControl5 = new isr.Core.Controls.ShapeControl();
            this.customControl11 = new isr.Core.Controls.ShapeControl();
            this.shapeControl7 = new isr.Core.Controls.ShapeControl();
            this._ShapeControl4 = new isr.Core.Controls.ShapeControl();
            this.ShapeControl3 = new isr.Core.Controls.ShapeControl();
            this.shapeControl2 = new isr.Core.Controls.ShapeControl();
            this.shapeControl6 = new isr.Core.Controls.ShapeControl();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this._ShapeControl1);
            this.panel1.Location = new System.Drawing.Point(8, 16);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(387, 320);
            this.panel1.TabIndex = 2;
            // 
            // _ShapeControl1
            // 
            this._ShapeControl1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this._ShapeControl1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._ShapeControl1.Blink = false;
            this._ShapeControl1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(98)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this._ShapeControl1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this._ShapeControl1.BorderWidth = 0;
            this._ShapeControl1.CenterColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(255)))), ((int)(((byte)(253)))), ((int)(((byte)(255)))));
            this._ShapeControl1.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._ShapeControl1.ForeColor = System.Drawing.Color.Yellow;
            this._ShapeControl1.Location = new System.Drawing.Point(55, 39);
            this._ShapeControl1.Name = "_ShapeControl1";
            this._ShapeControl1.Shape = isr.Core.Controls.ShapeType.Diamond;
            this._ShapeControl1.ShapeImage = null;
            this._ShapeControl1.Size = new System.Drawing.Size(131, 143);
            this._ShapeControl1.SurroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this._ShapeControl1.TabIndex = 0;
            this._ShapeControl1.Tag2 = "";
            this._ShapeControl1.Text = "Transparency Test Drag Me Around";
            this._ShapeControl1.UseGradient = true;
            this._ShapeControl1.Vibrate = false;
            this._ShapeControl1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ShapeControl1_MouseDown);
            this._ShapeControl1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ShapeControl1_MouseMove);
            // 
            // shapeControl5
            // 
            this.shapeControl5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(227)))), ((int)(((byte)(251)))), ((int)(((byte)(72)))));
            this.shapeControl5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.shapeControl5.Blink = false;
            this.shapeControl5.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.shapeControl5.BorderStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.shapeControl5.BorderWidth = 3;
            this.shapeControl5.CenterColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.shapeControl5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.shapeControl5.Location = new System.Drawing.Point(419, 64);
            this.shapeControl5.Name = "shapeControl5";
            this.shapeControl5.Shape = isr.Core.Controls.ShapeType.BalloonSE;
            this.shapeControl5.ShapeImage = null;
            this.shapeControl5.Size = new System.Drawing.Size(144, 106);
            this.shapeControl5.SurroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.shapeControl5.TabIndex = 6;
            this.shapeControl5.Tag2 = "";
            this.shapeControl5.Text = "Hello, I am Tommy";
            this.shapeControl5.UseGradient = false;
            this.shapeControl5.Vibrate = false;
            this.shapeControl5.Visible = false;
            // 
            // customControl11
            // 
            this.customControl11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.customControl11.Blink = false;
            this.customControl11.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.customControl11.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this.customControl11.BorderWidth = 0;
            this.customControl11.CenterColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.customControl11.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.customControl11.Location = new System.Drawing.Point(555, 137);
            this.customControl11.Name = "customControl11";
            this.customControl11.Shape = isr.Core.Controls.ShapeType.Ellipse;
            this.customControl11.ShapeImage = null;
            this.customControl11.Size = new System.Drawing.Size(93, 92);
            this.customControl11.SurroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(11)))));
            this.customControl11.TabIndex = 9;
            this.customControl11.Tag2 = "";
            this.customControl11.UseGradient = true;
            this.customControl11.Vibrate = false;
            // 
            // shapeControl7
            // 
            this.shapeControl7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))));
            this.shapeControl7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.shapeControl7.Blink = false;
            this.shapeControl7.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.shapeControl7.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this.shapeControl7.BorderWidth = 1;
            this.shapeControl7.CenterColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.shapeControl7.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.shapeControl7.Location = new System.Drawing.Point(585, 100);
            this.shapeControl7.Name = "shapeControl7";
            this.shapeControl7.Shape = isr.Core.Controls.ShapeType.CustomPolygon;
            this.shapeControl7.ShapeImage = null;
            this.shapeControl7.Size = new System.Drawing.Size(20, 22);
            this.shapeControl7.SurroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.shapeControl7.TabIndex = 8;
            this.shapeControl7.Tag2 = "";
            this.shapeControl7.UseGradient = false;
            this.shapeControl7.Vibrate = false;
            // 
            // _ShapeControl4
            // 
            this._ShapeControl4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(92)))), ((int)(((byte)(159)))), ((int)(((byte)(83)))));
            this._ShapeControl4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._ShapeControl4.Blink = false;
            this._ShapeControl4.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(131)))), ((int)(((byte)(255)))), ((int)(((byte)(4)))));
            this._ShapeControl4.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this._ShapeControl4.BorderWidth = 3;
            this._ShapeControl4.CenterColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this._ShapeControl4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this._ShapeControl4.Location = new System.Drawing.Point(551, 255);
            this._ShapeControl4.Name = "_ShapeControl4";
            this._ShapeControl4.Shape = isr.Core.Controls.ShapeType.RoundedRectangle;
            this._ShapeControl4.ShapeImage = null;
            this._ShapeControl4.Size = new System.Drawing.Size(97, 35);
            this._ShapeControl4.SurroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this._ShapeControl4.TabIndex = 5;
            this._ShapeControl4.Tag2 = "";
            this._ShapeControl4.Text = "Click Me!";
            this._ShapeControl4.UseGradient = false;
            this._ShapeControl4.Vibrate = false;
            this._ShapeControl4.Click += new System.EventHandler(this.ShapeControl4_Click);
            // 
            // ShapeControl3
            // 
            this.ShapeControl3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(92)))), ((int)(((byte)(159)))), ((int)(((byte)(83)))));
            this.ShapeControl3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ShapeControl3.Blink = false;
            this.ShapeControl3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(131)))), ((int)(((byte)(255)))), ((int)(((byte)(4)))));
            this.ShapeControl3.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this.ShapeControl3.BorderWidth = 3;
            this.ShapeControl3.CenterColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.ShapeControl3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.ShapeControl3.Location = new System.Drawing.Point(551, 306);
            this.ShapeControl3.Name = "ShapeControl3";
            this.ShapeControl3.Shape = isr.Core.Controls.ShapeType.RoundedRectangle;
            this.ShapeControl3.ShapeImage = null;
            this.ShapeControl3.Size = new System.Drawing.Size(97, 35);
            this.ShapeControl3.SurroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.ShapeControl3.TabIndex = 4;
            this.ShapeControl3.Tag2 = "";
            this.ShapeControl3.Text = "Close";
            this.ShapeControl3.UseGradient = false;
            this.ShapeControl3.Vibrate = false;
            this.ShapeControl3.Click += new System.EventHandler(this.ShapeControl3_Click);
            // 
            // shapeControl2
            // 
            this.shapeControl2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.shapeControl2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.shapeControl2.Blink = false;
            this.shapeControl2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(118)))), ((int)(((byte)(133)))), ((int)(((byte)(4)))), ((int)(((byte)(9)))));
            this.shapeControl2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this.shapeControl2.BorderWidth = 0;
            this.shapeControl2.CenterColor = System.Drawing.Color.FromArgb(((int)(((byte)(90)))), ((int)(((byte)(163)))), ((int)(((byte)(126)))), ((int)(((byte)(59)))));
            this.shapeControl2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.shapeControl2.Location = new System.Drawing.Point(519, 90);
            this.shapeControl2.Name = "shapeControl2";
            this.shapeControl2.Shape = isr.Core.Controls.ShapeType.TriangleUp;
            this.shapeControl2.ShapeImage = null;
            this.shapeControl2.Size = new System.Drawing.Size(152, 32);
            this.shapeControl2.SurroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.shapeControl2.TabIndex = 3;
            this.shapeControl2.Tag2 = "";
            this.shapeControl2.UseGradient = true;
            this.shapeControl2.Vibrate = false;
            // 
            // shapeControl6
            // 
            this.shapeControl6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(221)))), ((int)(((byte)(152)))), ((int)(((byte)(53)))));
            this.shapeControl6.Blink = false;
            this.shapeControl6.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.shapeControl6.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this.shapeControl6.BorderWidth = 0;
            this.shapeControl6.CenterColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(202)))), ((int)(((byte)(91)))), ((int)(((byte)(171)))));
            this.shapeControl6.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.shapeControl6.Location = new System.Drawing.Point(682, 64);
            this.shapeControl6.Name = "shapeControl6";
            this.shapeControl6.Shape = isr.Core.Controls.ShapeType.Ellipse;
            this.shapeControl6.ShapeImage = ((System.Drawing.Image)(resources.GetObject("shapeControl6.ShapeImage")));
            this.shapeControl6.Size = new System.Drawing.Size(101, 82);
            this.shapeControl6.SurroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(198)))), ((int)(((byte)(74)))), ((int)(((byte)(0)))));
            this.shapeControl6.TabIndex = 9;
            this.shapeControl6.Tag2 = "";
            this.shapeControl6.UseGradient = true;
            this.shapeControl6.Vibrate = false;
            // 
            // Form1
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(810, 372);
            this.ControlBox = false;
            this.Controls.Add(this.shapeControl5);
            this.Controls.Add(this.customControl11);
            this.Controls.Add(this.shapeControl6);
            this.Controls.Add(this.shapeControl7);
            this.Controls.Add(this._ShapeControl4);
            this.Controls.Add(this.ShapeControl3);
            this.Controls.Add(this.shapeControl2);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.Name = "Form1";
            this.Text = "Test Shape Control";
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }
    }
}
