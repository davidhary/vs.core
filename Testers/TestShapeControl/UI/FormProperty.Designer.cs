using System;

namespace isr.Core.Tester
{
    public partial class FormProperty
    {
        /// <summary>
	/// Required designer variable.
	/// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
	/// Clean up any resources being used.
	/// </summary>
	/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && components is object)
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        /// <summary>
        /// Required method for Designer support - do not modify the contents of this method with the
        /// code editor.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        private void InitializeComponent()
        {
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            textBoxNotes = new System.Windows.Forms.TextBox();
            textBoxIP = new System.Windows.Forms.TextBox();
            SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(32, 39);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(17, 13);
            label1.TabIndex = 0;
            label1.Text = "IP";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(32, 81);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(35, 13);
            label2.TabIndex = 1;
            label2.Text = "Notes";
            // 
            // textBoxNotes
            // 
            textBoxNotes.Location = new System.Drawing.Point(73, 81);
            textBoxNotes.MaxLength = 100;
            textBoxNotes.Name = "textBoxNotes";
            textBoxNotes.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            textBoxNotes.Size = new System.Drawing.Size(188, 20);
            textBoxNotes.TabIndex = 1;
            // 
            // textBoxIP
            // 
            textBoxIP.Location = new System.Drawing.Point(73, 39);
            textBoxIP.MaxLength = 15;
            textBoxIP.Name = "textBoxIP";
            textBoxIP.Size = new System.Drawing.Size(188, 20);
            textBoxIP.TabIndex = 0;
            textBoxIP.Enter += new EventHandler(TextBoxIP_Enter);
            // 
            // FormProperty
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(296, 161);
            Controls.Add(textBoxIP);
            Controls.Add(textBoxNotes);
            Controls.Add(label2);
            Controls.Add(label1);
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "FormProperty";
            Text = "Cam Properties";
            Activated += new EventHandler(FormProperty_Activated);
            FormClosing += new System.Windows.Forms.FormClosingEventHandler(FormProperty_FormClosing);
            ResumeLayout(false);
            PerformLayout();
        }

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxNotes;
        private System.Windows.Forms.TextBox textBoxIP;
    }
}
