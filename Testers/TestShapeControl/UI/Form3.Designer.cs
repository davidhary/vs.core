using System;
using System.Runtime.CompilerServices;

namespace isr.Core.Tester
{
    public partial class Form3
    {
        /// <summary>
	/// Required designer variable.
	/// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
	/// Clean up any resources being used.
	/// </summary>
	/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && components is object)
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        /// <summary>
        /// Required method for Designer support - do not modify the contents of this method with the
        /// code editor.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _Panel1 = new System.Windows.Forms.Panel();
            _Panel1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(Panel1_MouseDoubleClick);
            _BtnImportMap = new System.Windows.Forms.Button();
            _BtnImportMap.Click += new EventHandler(BtnImportMap_Click);
            _BtnAddCam = new System.Windows.Forms.Button();
            _BtnAddCam.Click += new EventHandler(BtnAddCam_Click);
            openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            _Button1 = new System.Windows.Forms.Button();
            _Button1.Click += new EventHandler(Button1_Click);
            _Button2 = new System.Windows.Forms.Button();
            _Button2.Click += new EventHandler(Button2_Click);
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            toolTip1 = new System.Windows.Forms.ToolTip(components);
            SuspendLayout();
            // 
            // panel1
            // 
            _Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            _Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            _Panel1.Location = new System.Drawing.Point(24, 98);
            _Panel1.Name = "_panel1";
            _Panel1.Size = new System.Drawing.Size(652, 430);
            _Panel1.TabIndex = 0;
            // 
            // btnImportMap
            // 
            _BtnImportMap.Location = new System.Drawing.Point(24, 12);
            _BtnImportMap.Name = "_btnImportMap";
            _BtnImportMap.Size = new System.Drawing.Size(57, 27);
            _BtnImportMap.TabIndex = 1;
            _BtnImportMap.Text = "Load";
            _BtnImportMap.UseVisualStyleBackColor = true;
            // 
            // btnAddCam
            // 
            _BtnAddCam.Location = new System.Drawing.Point(107, 12);
            _BtnAddCam.Name = "_btnAddCam";
            _BtnAddCam.Size = new System.Drawing.Size(61, 26);
            _BtnAddCam.TabIndex = 2;
            _BtnAddCam.Text = "Add Cam";
            _BtnAddCam.UseVisualStyleBackColor = true;
            // 
            // openFileDialog1
            // 
            openFileDialog1.FileName = "openFileDialog1";
            // 
            // button1
            // 
            _Button1.Location = new System.Drawing.Point(513, 11);
            _Button1.Name = "_button1";
            _Button1.Size = new System.Drawing.Size(56, 27);
            _Button1.TabIndex = 3;
            _Button1.Text = "Save";
            _Button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            _Button2.Location = new System.Drawing.Point(455, 11);
            _Button2.Name = "_button2";
            _Button2.Size = new System.Drawing.Size(52, 27);
            _Button2.TabIndex = 4;
            _Button2.Text = "New";
            _Button2.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(87, 51);
            label1.MaximumSize = new System.Drawing.Size(550, 30);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(35, 13);
            label1.TabIndex = 5;
            label1.Text = "label1";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(24, 542);
            label2.MaximumSize = new System.Drawing.Size(600, 30);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(35, 13);
            label2.TabIndex = 6;
            label2.Text = "label2";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(24, 51);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(57, 13);
            label3.TabIndex = 7;
            label3.Text = "File Name:";
            // 
            // Form3
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(6.0f, 13.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(705, 580);
            Controls.Add(label3);
            Controls.Add(label2);
            Controls.Add(label1);
            Controls.Add(_Button2);
            Controls.Add(_Button1);
            Controls.Add(_BtnAddCam);
            Controls.Add(_BtnImportMap);
            Controls.Add(_Panel1);
            KeyPreview = true;
            MinimumSize = new System.Drawing.Size(640, 480);
            Name = "Form3";
            Text = "Cam Map Designer";
            ResumeLayout(false);
            PerformLayout();
        }

        private System.Windows.Forms.Panel _Panel1;

        private System.Windows.Forms.Panel Panel1
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _Panel1;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_Panel1 != null)
                {
                    _Panel1.MouseDoubleClick -= Panel1_MouseDoubleClick;
                }

                _Panel1 = value;
                if (_Panel1 != null)
                {
                    _Panel1.MouseDoubleClick += Panel1_MouseDoubleClick;
                }
            }
        }

        private System.Windows.Forms.Button _BtnImportMap;

        private System.Windows.Forms.Button BtnImportMap
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _BtnImportMap;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_BtnImportMap != null)
                {
                    _BtnImportMap.Click -= BtnImportMap_Click;
                }

                _BtnImportMap = value;
                if (_BtnImportMap != null)
                {
                    _BtnImportMap.Click += BtnImportMap_Click;
                }
            }
        }

        private System.Windows.Forms.Button _BtnAddCam;

        private System.Windows.Forms.Button BtnAddCam
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _BtnAddCam;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_BtnAddCam != null)
                {
                    _BtnAddCam.Click -= BtnAddCam_Click;
                }

                _BtnAddCam = value;
                if (_BtnAddCam != null)
                {
                    _BtnAddCam.Click += BtnAddCam_Click;
                }
            }
        }

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button _Button1;

        private System.Windows.Forms.Button Button1
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _Button1;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_Button1 != null)
                {
                    _Button1.Click -= Button1_Click;
                }

                _Button1 = value;
                if (_Button1 != null)
                {
                    _Button1.Click += Button1_Click;
                }
            }
        }

        private System.Windows.Forms.Button _Button2;

        private System.Windows.Forms.Button Button2
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _Button2;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_Button2 != null)
                {
                    _Button2.Click -= Button2_Click;
                }

                _Button2 = value;
                if (_Button2 != null)
                {
                    _Button2.Click += Button2_Click;
                }
            }
        }

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}
