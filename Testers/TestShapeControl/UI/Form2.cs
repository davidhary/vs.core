﻿using System;
using System.Windows.Forms;
using isr.Core.Controls;

namespace isr.Core.Tester
{

    /// <summary>   A form 2. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    public partial class Form2 : Form
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Windows.Forms.Form" /> class.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        public Form2()
        {
            InitializeComponent();
        }

        /// <summary>   Check box b checked changed. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void CheckBoxB_CheckedChanged(object sender, EventArgs e)
        {
            string tag = ((Control)sender).Tag.ToString();
            var ctrls = panel1.Controls.Find("customControl1" + tag, false);
            if (ctrls.Length > 0)
            {
                ShapeControl cam = (ShapeControl)ctrls[0];
                cam.Blink = ((System.Windows.Forms.CheckBox)sender).Checked;
            }
        }

        /// <summary>   Check box v checked changed. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void CheckBoxV_CheckedChanged(object sender, EventArgs e)
        {
            string tag = ((Control)sender).Tag.ToString();
            var ctrls = panel1.Controls.Find("customControl1" + tag, false);
            if (ctrls.Length > 0)
            {
                ShapeControl cam = (ShapeControl)ctrls[0];
                cam.Vibrate = ((System.Windows.Forms.CheckBox)sender).Checked;
            }
        }
    }
}