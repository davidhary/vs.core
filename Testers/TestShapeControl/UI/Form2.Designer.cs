using System;
using System.Runtime.CompilerServices;
using isr.Core.Controls;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Tester
{
    public partial class Form2
    {
        private System.ComponentModel.IContainer components = null;

        /// <summary>   Clean up any resources being used. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="disposing">    true if managed resources should be disposed; otherwise, false. </param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && components is object)
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        /// <summary>
        /// Required method for Designer support - do not modify the contents of this method with the
        /// code editor.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            _CheckBox1 = new System.Windows.Forms.CheckBox();
            _CheckBox1.CheckedChanged += new EventHandler(CheckBoxB_CheckedChanged);
            _CheckBox2 = new System.Windows.Forms.CheckBox();
            _CheckBox2.CheckedChanged += new EventHandler(CheckBoxB_CheckedChanged);
            _CheckBox3 = new System.Windows.Forms.CheckBox();
            _CheckBox3.CheckedChanged += new EventHandler(CheckBoxB_CheckedChanged);
            _CheckBox4 = new System.Windows.Forms.CheckBox();
            _CheckBox4.CheckedChanged += new EventHandler(CheckBoxB_CheckedChanged);
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            this.CheckBox5 = new System.Windows.Forms.CheckBox();
            this.CheckBox6 = new System.Windows.Forms.CheckBox();
            this.CheckBox7 = new System.Windows.Forms.CheckBox();
            this.CheckBox8 = new System.Windows.Forms.CheckBox();
            panel1 = new System.Windows.Forms.Panel();
            customControl14 = new ShapeControl();
            customControl13 = new ShapeControl();
            customControl12 = new ShapeControl();
            customControl11 = new ShapeControl();
            panel1.SuspendLayout();
            SuspendLayout();
            // 
            // checkBox1
            // 
            _CheckBox1.AutoSize = true;
            _CheckBox1.Location = new System.Drawing.Point(135, 521);
            _CheckBox1.Name = "_checkBox1";
            _CheckBox1.Size = new System.Drawing.Size(52, 17);
            _CheckBox1.TabIndex = 1;
            _CheckBox1.Tag = "1";
            _CheckBox1.Text = "cam1";
            _CheckBox1.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            _CheckBox2.AutoSize = true;
            _CheckBox2.Location = new System.Drawing.Point(205, 521);
            _CheckBox2.Name = "_checkBox2";
            _CheckBox2.Size = new System.Drawing.Size(52, 17);
            _CheckBox2.TabIndex = 2;
            _CheckBox2.Tag = "2";
            _CheckBox2.Text = "cam2";
            _CheckBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            _CheckBox3.AutoSize = true;
            _CheckBox3.Location = new System.Drawing.Point(135, 544);
            _CheckBox3.Name = "_checkBox3";
            _CheckBox3.Size = new System.Drawing.Size(52, 17);
            _CheckBox3.TabIndex = 3;
            _CheckBox3.Tag = "3";
            _CheckBox3.Text = "cam3";
            _CheckBox3.UseVisualStyleBackColor = true;
            // 
            // checkBox4
            // 
            _CheckBox4.AutoSize = true;
            _CheckBox4.Location = new System.Drawing.Point(205, 544);
            _CheckBox4.Name = "_checkBox4";
            _CheckBox4.Size = new System.Drawing.Size(52, 17);
            _CheckBox4.TabIndex = 4;
            _CheckBox4.Tag = "4";
            _CheckBox4.Text = "cam4";
            _CheckBox4.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(18, 521);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(75, 13);
            label1.TabIndex = 5;
            label1.Text = "Check to blink";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(421, 521);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(85, 13);
            label2.TabIndex = 10;
            label2.Text = "Check to vibrate";
            // 
            // checkBox5
            // 
            this.CheckBox5.AutoSize = true;
            this.CheckBox5.Location = new System.Drawing.Point(608, 544);
            this.CheckBox5.Name = "_checkBox5";
            this.CheckBox5.Size = new System.Drawing.Size(52, 17);
            this.CheckBox5.TabIndex = 9;
            this.CheckBox5.Tag = "4";
            this.CheckBox5.Text = "cam4";
            this.CheckBox5.UseVisualStyleBackColor = true;
            // 
            // checkBox6
            // 
            this.CheckBox6.AutoSize = true;
            this.CheckBox6.Location = new System.Drawing.Point(538, 544);
            this.CheckBox6.Name = "_checkBox6";
            this.CheckBox6.Size = new System.Drawing.Size(52, 17);
            this.CheckBox6.TabIndex = 8;
            this.CheckBox6.Tag = "3";
            this.CheckBox6.Text = "cam3";
            this.CheckBox6.UseVisualStyleBackColor = true;
            // 
            // checkBox7
            // 
            this.CheckBox7.AutoSize = true;
            this.CheckBox7.Location = new System.Drawing.Point(608, 521);
            this.CheckBox7.Name = "_checkBox7";
            this.CheckBox7.Size = new System.Drawing.Size(52, 17);
            this.CheckBox7.TabIndex = 7;
            this.CheckBox7.Tag = "2";
            this.CheckBox7.Text = "cam2";
            this.CheckBox7.UseVisualStyleBackColor = true;
            // 
            // checkBox8
            // 
            this.CheckBox8.AutoSize = true;
            this.CheckBox8.Location = new System.Drawing.Point(538, 521);
            this.CheckBox8.Name = "_checkBox8";
            this.CheckBox8.Size = new System.Drawing.Size(52, 17);
            this.CheckBox8.TabIndex = 6;
            this.CheckBox8.Tag = "1";
            this.CheckBox8.Text = "cam1";
            this.CheckBox8.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            panel1.BackgroundImage = Properties.Resources.SIT_Redhill_Close_3_room_63sqm;
            panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            panel1.Controls.Add(customControl14);
            panel1.Controls.Add(customControl13);
            panel1.Controls.Add(customControl12);
            panel1.Controls.Add(customControl11);
            panel1.Location = new System.Drawing.Point(12, 12);
            panel1.Name = "panel1";
            panel1.Size = new System.Drawing.Size(707, 497);
            panel1.TabIndex = 0;
            // 
            // customControl14
            // 
            customControl14.BackColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(255)), Conversions.ToInteger(Conversions.ToByte(255)));
            customControl14.Blink = false;
            customControl14.BorderColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(255)), Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(0)));
            customControl14.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            customControl14.BorderWidth = 3;
            customControl14.CenterColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(100)), Conversions.ToInteger(Conversions.ToByte(255)), Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(0)));
            customControl14.Font = new System.Drawing.Font("Arial", 8.25f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            customControl14.ForeColor = System.Drawing.SystemColors.ControlText;
            customControl14.Location = new System.Drawing.Point(517, 404);
            customControl14.Name = "customControl14";
            customControl14.Shape = ShapeType.Ellipse;
            customControl14.ShapeImage = null;
            customControl14.Size = new System.Drawing.Size(33, 32);
            customControl14.SurroundColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(100)), Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(255)), Conversions.ToInteger(Conversions.ToByte(255)));
            customControl14.TabIndex = 3;
            customControl14.Tag2 = string.Empty;
            customControl14.Text = "cam4";
            customControl14.UseGradient = false;
            customControl14.Vibrate = false;
            // 
            // customControl13
            // 
            customControl13.BackColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(255)));
            customControl13.Blink = false;
            customControl13.BorderColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(255)), Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(0)));
            customControl13.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            customControl13.BorderWidth = 3;
            customControl13.CenterColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(100)), Conversions.ToInteger(Conversions.ToByte(255)), Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(0)));
            customControl13.Font = new System.Drawing.Font("Arial", 8.25f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            customControl13.ForeColor = System.Drawing.Color.Yellow;
            customControl13.Location = new System.Drawing.Point(302, 386);
            customControl13.Name = "customControl13";
            customControl13.Shape = ShapeType.Ellipse;
            customControl13.ShapeImage = (System.Drawing.Image)resources.GetObject("customControl13.ShapeImage");
            customControl13.Size = new System.Drawing.Size(43, 50);
            customControl13.SurroundColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(100)), Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(255)), Conversions.ToInteger(Conversions.ToByte(255)));
            customControl13.TabIndex = 2;
            customControl13.Tag2 = string.Empty;
            customControl13.Text = "cam3";
            customControl13.UseGradient = false;
            customControl13.Vibrate = false;
            // 
            // customControl12
            // 
            customControl12.BackColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(255)), Conversions.ToInteger(Conversions.ToByte(0)));
            customControl12.Blink = false;
            customControl12.BorderColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(0)));
            customControl12.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            customControl12.BorderWidth = 3;
            customControl12.CenterColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(100)), Conversions.ToInteger(Conversions.ToByte(255)), Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(0)));
            customControl12.Font = new System.Drawing.Font("Arial", 8.25f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            customControl12.ForeColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(192)), Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(0)));
            customControl12.Location = new System.Drawing.Point(517, 205);
            customControl12.Name = "customControl12";
            customControl12.Shape = ShapeType.Ellipse;
            customControl12.ShapeImage = (System.Drawing.Image)resources.GetObject("customControl12.ShapeImage");
            customControl12.Size = new System.Drawing.Size(42, 39);
            customControl12.SurroundColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(100)), Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(255)), Conversions.ToInteger(Conversions.ToByte(255)));
            customControl12.TabIndex = 2;
            customControl12.Tag2 = string.Empty;
            customControl12.Text = "cam2";
            customControl12.UseGradient = false;
            customControl12.Vibrate = false;
            // 
            // customControl11
            // 
            customControl11.BackColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(255)), Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(0)));
            customControl11.Blink = false;
            customControl11.BorderColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(0)));
            customControl11.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            customControl11.BorderWidth = 3;
            customControl11.CenterColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(100)), Conversions.ToInteger(Conversions.ToByte(255)), Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(0)));
            customControl11.Font = new System.Drawing.Font("Arial", 8.25f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            customControl11.ForeColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(128)), Conversions.ToInteger(Conversions.ToByte(255)), Conversions.ToInteger(Conversions.ToByte(255)));
            customControl11.Location = new System.Drawing.Point(266, 192);
            customControl11.Name = "customControl11";
            customControl11.Shape = ShapeType.Ellipse;
            customControl11.ShapeImage = (System.Drawing.Image)resources.GetObject("customControl11.ShapeImage");
            customControl11.Size = new System.Drawing.Size(59, 52);
            customControl11.SurroundColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(100)), Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(255)), Conversions.ToInteger(Conversions.ToByte(255)));
            customControl11.TabIndex = 0;
            customControl11.Tag2 = string.Empty;
            customControl11.Text = " cam1";
            customControl11.UseGradient = false;
            customControl11.Vibrate = false;
            // 
            // Form2
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(6.0f, 13.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            ClientSize = new System.Drawing.Size(747, 578);
            Controls.Add(label2);
            Controls.Add(this.CheckBox5);
            Controls.Add(this.CheckBox6);
            Controls.Add(this.CheckBox7);
            Controls.Add(this.CheckBox8);
            Controls.Add(label1);
            Controls.Add(_CheckBox4);
            Controls.Add(_CheckBox3);
            Controls.Add(_CheckBox2);
            Controls.Add(_CheckBox1);
            Controls.Add(panel1);
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "Form2";
            Text = "Test Blinking and Vibrating";
            panel1.ResumeLayout(false);
            ResumeLayout(false);
            PerformLayout();
        }

        private System.Windows.Forms.Panel panel1;
        private ShapeControl customControl12;
        private ShapeControl customControl11;
        private ShapeControl customControl13;
        private ShapeControl customControl14;
        private System.Windows.Forms.CheckBox _CheckBox1;

        private System.Windows.Forms.CheckBox CheckBox1
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _CheckBox1;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_CheckBox1 != null)
                {
                    _CheckBox1.CheckedChanged -= CheckBoxB_CheckedChanged;
                }

                _CheckBox1 = value;
                if (_CheckBox1 != null)
                {
                    _CheckBox1.CheckedChanged += CheckBoxB_CheckedChanged;
                }
            }
        }

        private System.Windows.Forms.CheckBox _CheckBox2;

        private System.Windows.Forms.CheckBox CheckBox2
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _CheckBox2;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_CheckBox2 != null)
                {
                    _CheckBox2.CheckedChanged -= CheckBoxB_CheckedChanged;
                }

                _CheckBox2 = value;
                if (_CheckBox2 != null)
                {
                    _CheckBox2.CheckedChanged += CheckBoxB_CheckedChanged;
                }
            }
        }

        private System.Windows.Forms.CheckBox _CheckBox3;

        private System.Windows.Forms.CheckBox CheckBox3
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _CheckBox3;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_CheckBox3 != null)
                {
                    _CheckBox3.CheckedChanged -= CheckBoxB_CheckedChanged;
                }

                _CheckBox3 = value;
                if (_CheckBox3 != null)
                {
                    _CheckBox3.CheckedChanged += CheckBoxB_CheckedChanged;
                }
            }
        }

        private System.Windows.Forms.CheckBox _CheckBox4;

        private System.Windows.Forms.CheckBox CheckBox4
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _CheckBox4;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_CheckBox4 != null)
                {
                    _CheckBox4.CheckedChanged -= CheckBoxB_CheckedChanged;
                }

                _CheckBox4 = value;
                if (_CheckBox4 != null)
                {
                    _CheckBox4.CheckedChanged += CheckBoxB_CheckedChanged;
                }
            }
        }

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox CheckBox5;
        private System.Windows.Forms.CheckBox CheckBox6;
        private System.Windows.Forms.CheckBox CheckBox7;
        private System.Windows.Forms.CheckBox CheckBox8;
    }
}
