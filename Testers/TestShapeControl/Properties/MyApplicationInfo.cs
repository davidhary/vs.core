﻿/// namespace: My
/// 
/// summary:   ."

namespace isr.Core.Tester.My
{
    internal partial class MyApplication
    {
        /// <summary>   The assembly title. </summary>

        public const string AssemblyTitle = "Shape Control Tester";
        /// <summary>   Information describing the assembly. </summary>
        public const string AssemblyDescription = "Shape Control Tester";
        /// <summary>   The assembly product. </summary>
        public const string AssemblyProduct = "Shape.Control.Tester.2019";
    }
}