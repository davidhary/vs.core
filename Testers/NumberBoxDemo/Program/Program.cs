﻿using System;
using System.Windows.Forms;

namespace NumberBoxDemo
{
    /// <summary>   A program. </summary>
    /// <remarks>   David, 2020-04-08. </remarks>
    static class Program
    {

        /// <summary>   The main entry point for the application. </summary>
        /// <remarks>   David, 2020-04-08. </remarks>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault( false );
            Application.Run( new Form1() );
        }
    }
}
