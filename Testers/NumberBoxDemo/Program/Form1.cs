using System;
using System.Windows.Forms;

using isr.Core.Controls;

namespace NumberBoxDemo
{
    /// <summary>   A form 1. </summary>
    /// <remarks>   David, 2020-04-08. </remarks>
    public partial class Form1 : Form
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-04-08. </remarks>
        public Form1()
        {
            this.InitializeComponent();
        }

        /// <summary>   Event handler. Called by NumberBox for leave events. </summary>
        /// <remarks>   David, 2020-04-08. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void NumberBox_Leave( object sender, EventArgs e )
        {
            var nb = ( NumberBox ) sender;

            foreach ( var c in this.panel2.Controls )
            {
                if ( c.GetType() == typeof( NumberBox ) )
                {
                    var box = ( NumberBox ) c;
                    if ( box.Snfs.StartsWith( "D" ) )
                    {
                        box.ValueAsInt64 = nb.ValueAsInt64;
                    }

                    if ( box.Snfs.StartsWith( "F" ) )
                    {
                        box.ValueAsFloat = nb.ValueAsFloat;
                    }

                    if ( box.Snfs.StartsWith( "E" ) )
                    {
                        box.ValueAsDouble = nb.ValueAsDouble;
                    }

                    if ( box.Snfs.StartsWith( "N" ) )
                    {
                        box.ValueAsDouble = nb.ValueAsDouble;
                    }

                    if ( box.Snfs.StartsWith( "X" ) )
                    {
                        box.ValueAsInt64 = nb.ValueAsInt64;
                    }

                    if ( box.Snfs.StartsWith( "B" ) )
                    {
                        box.ValueAsByte = nb.ValueAsByte;
                    }
                }
            }
        }
    }
}
