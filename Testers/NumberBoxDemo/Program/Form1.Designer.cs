using isr.Core.Controls;

namespace NumberBoxDemo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.numberBox1 = new NumberBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.numberBox2 = new NumberBox();
            this.label3 = new System.Windows.Forms.Label();
            this.numberBox3 = new NumberBox();
            this.numberBox4 = new NumberBox();
            this.label4 = new System.Windows.Forms.Label();
            this.numberBox5 = new NumberBox();
            this.label5 = new System.Windows.Forms.Label();
            this.numberBox6 = new NumberBox();
            this.label6 = new System.Windows.Forms.Label();
            this.numberBox7 = new NumberBox();
            this.label7 = new System.Windows.Forms.Label();
            this.numberBox8 = new NumberBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.numberBox9 = new NumberBox();
            this.numberBox10 = new NumberBox();
            this.numberBox11 = new NumberBox();
            this.numberBox12 = new NumberBox();
            this.numberBox13 = new NumberBox();
            this.numberBox14 = new NumberBox();
            this.label30 = new System.Windows.Forms.Label();
            this.numberBox15 = new NumberBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.numberBox16 = new NumberBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // numberBox1
            // 
            this.numberBox1.Location = new System.Drawing.Point(49, 85);
            this.numberBox1.MaxValue = 50D;
            this.numberBox1.MinValue = -50D;
            this.numberBox1.Name = "numberBox1";
            this.numberBox1.Prefix = null;
            this.numberBox1.Size = new System.Drawing.Size(63, 20);
            this.numberBox1.Snfs = "F3";
            this.numberBox1.Suffix = null;
            this.numberBox1.TabIndex = 0;
            this.numberBox1.Tag = "2";
            this.numberBox1.Text = "0.000";
            this.numberBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numberBox1.ValueAsByte = ((byte)(0));
            this.numberBox1.ValueAsDouble = 0D;
            this.numberBox1.ValueAsFloat = 0F;
            this.numberBox1.ValueAsInt16 = ((short)(0));
            this.numberBox1.ValueAsInt32 = 0;
            this.numberBox1.ValueAsInt64 = ((long)(0));
            this.numberBox1.ValueAsSByte = ((sbyte)(0));
            this.numberBox1.ValueAsUInt16 = ((ushort)(0));
            this.numberBox1.ValueAsUInt32 = ((uint)(0u));
            this.numberBox1.ValueAsUInt64 = ((ulong)(0ul));
            this.numberBox1.Leave += new System.EventHandler(this.NumberBox_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 88);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "\'F3\'";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 114);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "\'E2\'";
            // 
            // numberBox2
            // 
            this.numberBox2.Location = new System.Drawing.Point(49, 111);
            this.numberBox2.MaxValue = 500D;
            this.numberBox2.MinValue = -500D;
            this.numberBox2.Name = "numberBox2";
            this.numberBox2.Prefix = null;
            this.numberBox2.Size = new System.Drawing.Size(63, 20);
            this.numberBox2.Snfs = "E2";
            this.numberBox2.Suffix = null;
            this.numberBox2.TabIndex = 3;
            this.numberBox2.Tag = "3";
            this.numberBox2.Text = "0.00E+000";
            this.numberBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numberBox2.ValueAsByte = ((byte)(0));
            this.numberBox2.ValueAsDouble = 0D;
            this.numberBox2.ValueAsFloat = 0F;
            this.numberBox2.ValueAsInt16 = ((short)(0));
            this.numberBox2.ValueAsInt32 = 0;
            this.numberBox2.ValueAsInt64 = ((long)(0));
            this.numberBox2.ValueAsSByte = ((sbyte)(0));
            this.numberBox2.ValueAsUInt16 = ((ushort)(0));
            this.numberBox2.ValueAsUInt32 = ((uint)(0u));
            this.numberBox2.ValueAsUInt64 = ((ulong)(0ul));
            this.numberBox2.Leave += new System.EventHandler(this.NumberBox_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "\'D3\'";
            // 
            // numberBox3
            // 
            this.numberBox3.Location = new System.Drawing.Point(49, 59);
            this.numberBox3.MaxValue = 200D;
            this.numberBox3.MinValue = -200D;
            this.numberBox3.Name = "numberBox3";
            this.numberBox3.Prefix = null;
            this.numberBox3.Size = new System.Drawing.Size(63, 20);
            this.numberBox3.Snfs = "D3";
            this.numberBox3.Suffix = null;
            this.numberBox3.TabIndex = 5;
            this.numberBox3.Tag = "1";
            this.numberBox3.Text = "000";
            this.numberBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numberBox3.ValueAsByte = ((byte)(0));
            this.numberBox3.ValueAsDouble = 0D;
            this.numberBox3.ValueAsFloat = 0F;
            this.numberBox3.ValueAsInt16 = ((short)(0));
            this.numberBox3.ValueAsInt32 = 0;
            this.numberBox3.ValueAsInt64 = ((long)(0));
            this.numberBox3.ValueAsSByte = ((sbyte)(0));
            this.numberBox3.ValueAsUInt16 = ((ushort)(0));
            this.numberBox3.ValueAsUInt32 = ((uint)(0u));
            this.numberBox3.ValueAsUInt64 = ((ulong)(0ul));
            this.numberBox3.Leave += new System.EventHandler(this.NumberBox_Leave);
            // 
            // numberBox4
            // 
            this.numberBox4.Location = new System.Drawing.Point(49, 33);
            this.numberBox4.MaxValue = 150D;
            this.numberBox4.MinValue = -150D;
            this.numberBox4.Name = "numberBox4";
            this.numberBox4.Prefix = null;
            this.numberBox4.Size = new System.Drawing.Size(63, 20);
            this.numberBox4.Snfs = "D";
            this.numberBox4.Suffix = null;
            this.numberBox4.TabIndex = 7;
            this.numberBox4.Tag = "0";
            this.numberBox4.Text = "0";
            this.numberBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numberBox4.ValueAsByte = ((byte)(0));
            this.numberBox4.ValueAsDouble = 0D;
            this.numberBox4.ValueAsFloat = 0F;
            this.numberBox4.ValueAsInt16 = ((short)(0));
            this.numberBox4.ValueAsInt32 = 0;
            this.numberBox4.ValueAsInt64 = ((long)(0));
            this.numberBox4.ValueAsSByte = ((sbyte)(0));
            this.numberBox4.ValueAsUInt16 = ((ushort)(0));
            this.numberBox4.ValueAsUInt32 = ((uint)(0u));
            this.numberBox4.ValueAsUInt64 = ((ulong)(0ul));
            this.numberBox4.Leave += new System.EventHandler(this.NumberBox_Leave);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 36);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(19, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "\'D\'";
            // 
            // numberBox5
            // 
            this.numberBox5.Location = new System.Drawing.Point(47, 137);
            this.numberBox5.MaxValue = 25D;
            this.numberBox5.MinValue = -25D;
            this.numberBox5.Name = "numberBox5";
            this.numberBox5.Prefix = null;
            this.numberBox5.Size = new System.Drawing.Size(63, 20);
            this.numberBox5.Snfs = "N";
            this.numberBox5.Suffix = null;
            this.numberBox5.TabIndex = 11;
            this.numberBox5.Tag = "4";
            this.numberBox5.Text = "0.00";
            this.numberBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numberBox5.ValueAsByte = ((byte)(0));
            this.numberBox5.ValueAsDouble = 0D;
            this.numberBox5.ValueAsFloat = 0F;
            this.numberBox5.ValueAsInt16 = ((short)(0));
            this.numberBox5.ValueAsInt32 = 0;
            this.numberBox5.ValueAsInt64 = ((long)(0));
            this.numberBox5.ValueAsSByte = ((sbyte)(0));
            this.numberBox5.ValueAsUInt16 = ((ushort)(0));
            this.numberBox5.ValueAsUInt32 = ((uint)(0u));
            this.numberBox5.ValueAsUInt64 = ((ulong)(0ul));
            this.numberBox5.Leave += new System.EventHandler(this.NumberBox_Leave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(24, 140);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(19, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "\'N\'";
            // 
            // numberBox6
            // 
            this.numberBox6.Location = new System.Drawing.Point(47, 163);
            this.numberBox6.MaxValue = 35D;
            this.numberBox6.MinValue = -35D;
            this.numberBox6.Name = "numberBox6";
            this.numberBox6.Prefix = null;
            this.numberBox6.Size = new System.Drawing.Size(63, 20);
            this.numberBox6.Snfs = "N3";
            this.numberBox6.Suffix = null;
            this.numberBox6.TabIndex = 9;
            this.numberBox6.Tag = "5";
            this.numberBox6.Text = "0.000";
            this.numberBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numberBox6.ValueAsByte = ((byte)(0));
            this.numberBox6.ValueAsDouble = 0D;
            this.numberBox6.ValueAsFloat = 0F;
            this.numberBox6.ValueAsInt16 = ((short)(0));
            this.numberBox6.ValueAsInt32 = 0;
            this.numberBox6.ValueAsInt64 = ((long)(0));
            this.numberBox6.ValueAsSByte = ((sbyte)(0));
            this.numberBox6.ValueAsUInt16 = ((ushort)(0));
            this.numberBox6.ValueAsUInt32 = ((uint)(0u));
            this.numberBox6.ValueAsUInt64 = ((ulong)(0ul));
            this.numberBox6.Leave += new System.EventHandler(this.NumberBox_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 166);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(25, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "\'N3\'";
            // 
            // numberBox7
            // 
            this.numberBox7.Location = new System.Drawing.Point(47, 189);
            this.numberBox7.MaxValue = 1000D;
            this.numberBox7.MinValue = 0D;
            this.numberBox7.Name = "numberBox7";
            this.numberBox7.Prefix = "";
            this.numberBox7.Size = new System.Drawing.Size(63, 20);
            this.numberBox7.Snfs = "X4";
            this.numberBox7.Suffix = null;
            this.numberBox7.TabIndex = 15;
            this.numberBox7.Tag = "6";
            this.numberBox7.Text = "0000";
            this.numberBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numberBox7.ValueAsByte = ((byte)(0));
            this.numberBox7.ValueAsDouble = 0D;
            this.numberBox7.ValueAsFloat = 0F;
            this.numberBox7.ValueAsInt16 = ((short)(0));
            this.numberBox7.ValueAsInt32 = 0;
            this.numberBox7.ValueAsInt64 = ((long)(0));
            this.numberBox7.ValueAsSByte = ((sbyte)(0));
            this.numberBox7.ValueAsUInt16 = ((ushort)(0));
            this.numberBox7.ValueAsUInt32 = ((uint)(0u));
            this.numberBox7.ValueAsUInt64 = ((ulong)(0ul));
            this.numberBox7.Leave += new System.EventHandler(this.NumberBox_Leave);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(19, 192);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(24, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "\'X4\'";
            // 
            // numberBox8
            // 
            this.numberBox8.Location = new System.Drawing.Point(47, 217);
            this.numberBox8.MaxValue = 58D;
            this.numberBox8.MinValue = 0D;
            this.numberBox8.Name = "numberBox8";
            this.numberBox8.Prefix = null;
            this.numberBox8.Size = new System.Drawing.Size(63, 20);
            this.numberBox8.Snfs = "B6";
            this.numberBox8.Suffix = "";
            this.numberBox8.TabIndex = 13;
            this.numberBox8.Tag = "7";
            this.numberBox8.Text = "000000";
            this.numberBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numberBox8.ValueAsByte = ((byte)(0));
            this.numberBox8.ValueAsDouble = 0D;
            this.numberBox8.ValueAsFloat = 0F;
            this.numberBox8.ValueAsInt16 = ((short)(0));
            this.numberBox8.ValueAsInt32 = 0;
            this.numberBox8.ValueAsInt64 = ((long)(0));
            this.numberBox8.ValueAsSByte = ((sbyte)(0));
            this.numberBox8.ValueAsUInt16 = ((ushort)(0));
            this.numberBox8.ValueAsUInt32 = ((uint)(0u));
            this.numberBox8.ValueAsUInt64 = ((ulong)(0ul));
            this.numberBox8.Leave += new System.EventHandler(this.NumberBox_Leave);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(18, 220);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(24, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "\'B6\'";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 14);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "SNFS";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.numberBox4);
            this.panel1.Controls.Add(this.numberBox8);
            this.panel1.Controls.Add(this.numberBox7);
            this.panel1.Controls.Add(this.numberBox6);
            this.panel1.Controls.Add(this.numberBox5);
            this.panel1.Controls.Add(this.numberBox2);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.numberBox3);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.numberBox1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(12, 36);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 254);
            this.panel1.TabIndex = 17;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(129, 220);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(31, 13);
            this.label18.TabIndex = 25;
            this.label18.Text = "0..58";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(129, 192);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(46, 13);
            this.label17.TabIndex = 24;
            this.label17.Text = "0...1000";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(129, 166);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(43, 13);
            this.label16.TabIndex = 23;
            this.label16.Text = "-35...35";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(129, 140);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(43, 13);
            this.label15.TabIndex = 22;
            this.label15.Text = "-25...25";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(129, 114);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(55, 13);
            this.label14.TabIndex = 21;
            this.label14.Text = "-500...500";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(129, 88);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(43, 13);
            this.label13.TabIndex = 20;
            this.label13.Text = "-50...50";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(129, 62);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(40, 13);
            this.label12.TabIndex = 19;
            this.label12.Text = "0...200";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(129, 36);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(55, 13);
            this.label11.TabIndex = 18;
            this.label11.Text = "-150...150";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(129, 14);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(39, 13);
            this.label10.TabIndex = 17;
            this.label10.Text = "Range";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(59, 20);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(74, 13);
            this.label19.TabIndex = 26;
            this.label19.Text = "Write ValueAs";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(277, 20);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(92, 13);
            this.label20.TabIndex = 28;
            this.label20.Text = "ValueAs= Outputs";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.numberBox9);
            this.panel2.Controls.Add(this.numberBox10);
            this.panel2.Controls.Add(this.numberBox11);
            this.panel2.Controls.Add(this.numberBox12);
            this.panel2.Controls.Add(this.numberBox13);
            this.panel2.Controls.Add(this.numberBox14);
            this.panel2.Controls.Add(this.label30);
            this.panel2.Controls.Add(this.numberBox15);
            this.panel2.Controls.Add(this.label31);
            this.panel2.Controls.Add(this.label32);
            this.panel2.Controls.Add(this.label33);
            this.panel2.Controls.Add(this.numberBox16);
            this.panel2.Controls.Add(this.label34);
            this.panel2.Controls.Add(this.label35);
            this.panel2.Controls.Add(this.label36);
            this.panel2.Controls.Add(this.label37);
            this.panel2.Controls.Add(this.label38);
            this.panel2.Location = new System.Drawing.Point(250, 36);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(133, 254);
            this.panel2.TabIndex = 27;
            // 
            // numberBox9
            // 
            this.numberBox9.Location = new System.Drawing.Point(49, 33);
            this.numberBox9.MaxValue = 150D;
            this.numberBox9.MinValue = -150D;
            this.numberBox9.Name = "numberBox9";
            this.numberBox9.Prefix = null;
            this.numberBox9.Size = new System.Drawing.Size(63, 20);
            this.numberBox9.Snfs = "D";
            this.numberBox9.Suffix = null;
            this.numberBox9.TabIndex = 7;
            this.numberBox9.Tag = "0";
            this.numberBox9.Text = "0";
            this.numberBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numberBox9.ValueAsByte = ((byte)(0));
            this.numberBox9.ValueAsDouble = 0D;
            this.numberBox9.ValueAsFloat = 0F;
            this.numberBox9.ValueAsInt16 = ((short)(0));
            this.numberBox9.ValueAsInt32 = 0;
            this.numberBox9.ValueAsInt64 = ((long)(0));
            this.numberBox9.ValueAsSByte = ((sbyte)(0));
            this.numberBox9.ValueAsUInt16 = ((ushort)(0));
            this.numberBox9.ValueAsUInt32 = ((uint)(0u));
            this.numberBox9.ValueAsUInt64 = ((ulong)(0ul));
            // 
            // numberBox10
            // 
            this.numberBox10.Location = new System.Drawing.Point(47, 217);
            this.numberBox10.MaxValue = 58D;
            this.numberBox10.MinValue = 0D;
            this.numberBox10.Name = "numberBox10";
            this.numberBox10.Prefix = null;
            this.numberBox10.Size = new System.Drawing.Size(63, 20);
            this.numberBox10.Snfs = "B6";
            this.numberBox10.Suffix = "b";
            this.numberBox10.TabIndex = 13;
            this.numberBox10.Tag = "0";
            this.numberBox10.Text = "000000b";
            this.numberBox10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numberBox10.ValueAsByte = ((byte)(0));
            this.numberBox10.ValueAsDouble = 0D;
            this.numberBox10.ValueAsFloat = 0F;
            this.numberBox10.ValueAsInt16 = ((short)(0));
            this.numberBox10.ValueAsInt32 = 0;
            this.numberBox10.ValueAsInt64 = ((long)(0));
            this.numberBox10.ValueAsSByte = ((sbyte)(0));
            this.numberBox10.ValueAsUInt16 = ((ushort)(0));
            this.numberBox10.ValueAsUInt32 = ((uint)(0u));
            this.numberBox10.ValueAsUInt64 = ((ulong)(0ul));
            // 
            // numberBox11
            // 
            this.numberBox11.Location = new System.Drawing.Point(47, 189);
            this.numberBox11.MaxValue = 1000D;
            this.numberBox11.MinValue = 0D;
            this.numberBox11.Name = "numberBox11";
            this.numberBox11.Prefix = "0x";
            this.numberBox11.Size = new System.Drawing.Size(63, 20);
            this.numberBox11.Snfs = "X4";
            this.numberBox11.Suffix = "";
            this.numberBox11.TabIndex = 15;
            this.numberBox11.Tag = "0";
            this.numberBox11.Text = "0x0000";
            this.numberBox11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numberBox11.ValueAsByte = ((byte)(0));
            this.numberBox11.ValueAsDouble = 0D;
            this.numberBox11.ValueAsFloat = 0F;
            this.numberBox11.ValueAsInt16 = ((short)(0));
            this.numberBox11.ValueAsInt32 = 0;
            this.numberBox11.ValueAsInt64 = ((long)(0));
            this.numberBox11.ValueAsSByte = ((sbyte)(0));
            this.numberBox11.ValueAsUInt16 = ((ushort)(0));
            this.numberBox11.ValueAsUInt32 = ((uint)(0u));
            this.numberBox11.ValueAsUInt64 = ((ulong)(0ul));
            // 
            // numberBox12
            // 
            this.numberBox12.Location = new System.Drawing.Point(47, 163);
            this.numberBox12.MaxValue = 35D;
            this.numberBox12.MinValue = -35D;
            this.numberBox12.Name = "numberBox12";
            this.numberBox12.Prefix = null;
            this.numberBox12.Size = new System.Drawing.Size(63, 20);
            this.numberBox12.Snfs = "N3";
            this.numberBox12.Suffix = null;
            this.numberBox12.TabIndex = 9;
            this.numberBox12.Tag = "0";
            this.numberBox12.Text = "0.000";
            this.numberBox12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numberBox12.ValueAsByte = ((byte)(0));
            this.numberBox12.ValueAsDouble = 0D;
            this.numberBox12.ValueAsFloat = 0F;
            this.numberBox12.ValueAsInt16 = ((short)(0));
            this.numberBox12.ValueAsInt32 = 0;
            this.numberBox12.ValueAsInt64 = ((long)(0));
            this.numberBox12.ValueAsSByte = ((sbyte)(0));
            this.numberBox12.ValueAsUInt16 = ((ushort)(0));
            this.numberBox12.ValueAsUInt32 = ((uint)(0u));
            this.numberBox12.ValueAsUInt64 = ((ulong)(0ul));
            // 
            // numberBox13
            // 
            this.numberBox13.Location = new System.Drawing.Point(47, 137);
            this.numberBox13.MaxValue = 25D;
            this.numberBox13.MinValue = -25D;
            this.numberBox13.Name = "numberBox13";
            this.numberBox13.Prefix = null;
            this.numberBox13.Size = new System.Drawing.Size(63, 20);
            this.numberBox13.Snfs = "N";
            this.numberBox13.Suffix = null;
            this.numberBox13.TabIndex = 11;
            this.numberBox13.Tag = "0";
            this.numberBox13.Text = "0.00";
            this.numberBox13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numberBox13.ValueAsByte = ((byte)(0));
            this.numberBox13.ValueAsDouble = 0D;
            this.numberBox13.ValueAsFloat = 0F;
            this.numberBox13.ValueAsInt16 = ((short)(0));
            this.numberBox13.ValueAsInt32 = 0;
            this.numberBox13.ValueAsInt64 = ((long)(0));
            this.numberBox13.ValueAsSByte = ((sbyte)(0));
            this.numberBox13.ValueAsUInt16 = ((ushort)(0));
            this.numberBox13.ValueAsUInt32 = ((uint)(0u));
            this.numberBox13.ValueAsUInt64 = ((ulong)(0ul));
            // 
            // numberBox14
            // 
            this.numberBox14.Location = new System.Drawing.Point(49, 111);
            this.numberBox14.MaxValue = 500D;
            this.numberBox14.MinValue = -500D;
            this.numberBox14.Name = "numberBox14";
            this.numberBox14.Prefix = null;
            this.numberBox14.Size = new System.Drawing.Size(63, 20);
            this.numberBox14.Snfs = "E2";
            this.numberBox14.Suffix = null;
            this.numberBox14.TabIndex = 3;
            this.numberBox14.Tag = "0";
            this.numberBox14.Text = "0.00E+000";
            this.numberBox14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numberBox14.ValueAsByte = ((byte)(0));
            this.numberBox14.ValueAsDouble = 0D;
            this.numberBox14.ValueAsFloat = 0F;
            this.numberBox14.ValueAsInt16 = ((short)(0));
            this.numberBox14.ValueAsInt32 = 0;
            this.numberBox14.ValueAsInt64 = ((long)(0));
            this.numberBox14.ValueAsSByte = ((sbyte)(0));
            this.numberBox14.ValueAsUInt16 = ((ushort)(0));
            this.numberBox14.ValueAsUInt32 = ((uint)(0u));
            this.numberBox14.ValueAsUInt64 = ((ulong)(0ul));
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(8, 14);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(35, 13);
            this.label30.TabIndex = 16;
            this.label30.Text = "SNFS";
            // 
            // numberBox15
            // 
            this.numberBox15.Location = new System.Drawing.Point(49, 59);
            this.numberBox15.MaxValue = 200D;
            this.numberBox15.MinValue = -200D;
            this.numberBox15.Name = "numberBox15";
            this.numberBox15.Prefix = null;
            this.numberBox15.Size = new System.Drawing.Size(63, 20);
            this.numberBox15.Snfs = "D3";
            this.numberBox15.Suffix = null;
            this.numberBox15.TabIndex = 5;
            this.numberBox15.Tag = "0";
            this.numberBox15.Text = "000";
            this.numberBox15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numberBox15.ValueAsByte = ((byte)(0));
            this.numberBox15.ValueAsDouble = 0D;
            this.numberBox15.ValueAsFloat = 0F;
            this.numberBox15.ValueAsInt16 = ((short)(0));
            this.numberBox15.ValueAsInt32 = 0;
            this.numberBox15.ValueAsInt64 = ((long)(0));
            this.numberBox15.ValueAsSByte = ((sbyte)(0));
            this.numberBox15.ValueAsUInt16 = ((ushort)(0));
            this.numberBox15.ValueAsUInt32 = ((uint)(0u));
            this.numberBox15.ValueAsUInt64 = ((ulong)(0ul));
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(18, 220);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(24, 13);
            this.label31.TabIndex = 12;
            this.label31.Text = "\'B6\'";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(19, 166);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(25, 13);
            this.label32.TabIndex = 8;
            this.label32.Text = "\'N3\'";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(18, 62);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(25, 13);
            this.label33.TabIndex = 4;
            this.label33.Text = "\'D3\'";
            // 
            // numberBox16
            // 
            this.numberBox16.Location = new System.Drawing.Point(49, 85);
            this.numberBox16.MaxValue = 50D;
            this.numberBox16.MinValue = -50D;
            this.numberBox16.Name = "numberBox16";
            this.numberBox16.Prefix = null;
            this.numberBox16.Size = new System.Drawing.Size(63, 20);
            this.numberBox16.Snfs = "F3";
            this.numberBox16.Suffix = null;
            this.numberBox16.TabIndex = 0;
            this.numberBox16.Tag = "0";
            this.numberBox16.Text = "0.000";
            this.numberBox16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numberBox16.ValueAsByte = ((byte)(0));
            this.numberBox16.ValueAsDouble = 0D;
            this.numberBox16.ValueAsFloat = 0F;
            this.numberBox16.ValueAsInt16 = ((short)(0));
            this.numberBox16.ValueAsInt32 = 0;
            this.numberBox16.ValueAsInt64 = ((long)(0));
            this.numberBox16.ValueAsSByte = ((sbyte)(0));
            this.numberBox16.ValueAsUInt16 = ((ushort)(0));
            this.numberBox16.ValueAsUInt32 = ((uint)(0u));
            this.numberBox16.ValueAsUInt64 = ((ulong)(0ul));
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(19, 114);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(24, 13);
            this.label34.TabIndex = 2;
            this.label34.Text = "\'E2\'";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(23, 36);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(19, 13);
            this.label35.TabIndex = 6;
            this.label35.Text = "\'D\'";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(24, 140);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(19, 13);
            this.label36.TabIndex = 10;
            this.label36.Text = "\'N\'";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(19, 192);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(24, 13);
            this.label37.TabIndex = 14;
            this.label37.Text = "\'X4\'";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(18, 88);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(23, 13);
            this.label38.TabIndex = 1;
            this.label38.Text = "\'F3\'";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 310);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "NumberBox Demo";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private NumberBox numberBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private NumberBox numberBox2;
        private System.Windows.Forms.Label label3;
        private NumberBox numberBox3;
        private NumberBox numberBox4;
        private System.Windows.Forms.Label label4;
        private NumberBox numberBox5;
        private System.Windows.Forms.Label label5;
        private NumberBox numberBox6;
        private System.Windows.Forms.Label label6;
        private NumberBox numberBox7;
        private System.Windows.Forms.Label label7;
        private NumberBox numberBox8;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Panel panel2;
        private NumberBox numberBox9;
        private NumberBox numberBox10;
        private NumberBox numberBox11;
        private NumberBox numberBox12;
        private NumberBox numberBox13;
        private NumberBox numberBox14;
        private System.Windows.Forms.Label label30;
        private NumberBox numberBox15;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private NumberBox numberBox16;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
    }
}

