
#pragma warning disable IDE1006 // Naming Styles
namespace isr.Core.Controls.My
#pragma warning restore IDE1006 // Naming Styles
{
    /// <summary>   my library. </summary>
    /// <remarks>   David, 2020-03-18. </remarks>
    internal class MyAssembly
    {

        /// <summary>   The assembly title. </summary>
        public const string AssemblyTitle = "Number Box Demo";

        /// <summary>   Information describing the assembly. </summary>
        public const string AssemblyDescription = "Number Box Demo";

        /// <summary>   The assembly product. </summary>
        public const string AssemblyProduct = "Number.Box.Demo";

        /// <summary>   The assembly configuration. </summary>
        public const string AssemblyConfiguration = "";
    }
}
