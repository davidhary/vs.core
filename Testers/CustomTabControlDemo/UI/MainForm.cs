﻿/// <summary> The application's main window. </summary>
/// <remarks> David, 2021-03-12. </remarks>

namespace isr.Core.Tester
{
    public partial class MainForm : System.Windows.Forms.Form
    {

        /// <summary>
    /// Initializes a new instance of the <see cref="T:System.Windows.Forms.Form" /> class.
    /// </summary>
    /// <remarks> David, 2021-03-12. </remarks>
        public MainForm()
        {
            InitializeComponent();
        }
    }
}