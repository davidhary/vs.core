﻿// 
// * Created by SharpDevelop.
// * User: mjackson
// * Date: 2010-28-06
// * Time: 13:38
// * 
// * To change this template use Tools | Options | Coding | Edit Standard Headers.
// 


namespace isr.Core.Tester
{
    public partial class MainForm
    {
        /// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components is object)
                {
                    components.Dispose();
                }
            }

            base.Dispose(disposing);
        }

        /// <summary>
    /// This method is required for Windows Forms designer support.
    /// Do not change the method contents inside the source code editor. The Forms designer might
    /// not be able to load this method if it was changed manually.
    /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "googlefavicon")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "bookopen")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "cdmusic")]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            imageList1 = new System.Windows.Forms.ImageList(components);
            contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(components);
            customTabControl1 = new Controls.CustomTabControl();
            tabPage1 = new System.Windows.Forms.TabPage();
            tabPage2 = new System.Windows.Forms.TabPage();
            tabPage3 = new System.Windows.Forms.TabPage();
            tabPage4 = new System.Windows.Forms.TabPage();
            tabPage5 = new System.Windows.Forms.TabPage();
            customTabControl2 = new Controls.CustomTabControl();
            tabPage6 = new System.Windows.Forms.TabPage();
            tabPage7 = new System.Windows.Forms.TabPage();
            tabPage8 = new System.Windows.Forms.TabPage();
            tabPage9 = new System.Windows.Forms.TabPage();
            tabPage10 = new System.Windows.Forms.TabPage();
            customTabControl3 = new Controls.CustomTabControl();
            tabPage11 = new System.Windows.Forms.TabPage();
            tabPage12 = new System.Windows.Forms.TabPage();
            tabPage13 = new System.Windows.Forms.TabPage();
            tabPage14 = new System.Windows.Forms.TabPage();
            tabPage15 = new System.Windows.Forms.TabPage();
            customTabControl4 = new Controls.CustomTabControl();
            tabPage16 = new System.Windows.Forms.TabPage();
            tabPage17 = new System.Windows.Forms.TabPage();
            tabPage18 = new System.Windows.Forms.TabPage();
            tabPage19 = new System.Windows.Forms.TabPage();
            tabPage20 = new System.Windows.Forms.TabPage();
            customTabControl5 = new Controls.CustomTabControl();
            tabPage21 = new System.Windows.Forms.TabPage();
            tabPage22 = new System.Windows.Forms.TabPage();
            tabPage23 = new System.Windows.Forms.TabPage();
            tabPage24 = new System.Windows.Forms.TabPage();
            tabPage25 = new System.Windows.Forms.TabPage();
            customTabControl6 = new Controls.CustomTabControl();
            tabPage26 = new System.Windows.Forms.TabPage();
            tabPage27 = new System.Windows.Forms.TabPage();
            tabPage28 = new System.Windows.Forms.TabPage();
            tabPage29 = new System.Windows.Forms.TabPage();
            tabPage30 = new System.Windows.Forms.TabPage();
            customTabControl7 = new Controls.CustomTabControl();
            tabPage31 = new System.Windows.Forms.TabPage();
            tabPage32 = new System.Windows.Forms.TabPage();
            tabPage33 = new System.Windows.Forms.TabPage();
            tabPage34 = new System.Windows.Forms.TabPage();
            tabPage35 = new System.Windows.Forms.TabPage();
            panel1 = new System.Windows.Forms.Panel();
            customTabControl1.SuspendLayout();
            customTabControl2.SuspendLayout();
            customTabControl3.SuspendLayout();
            customTabControl4.SuspendLayout();
            customTabControl5.SuspendLayout();
            customTabControl6.SuspendLayout();
            customTabControl7.SuspendLayout();
            panel1.SuspendLayout();
            SuspendLayout();
            // 
            // imageList1
            // 
            imageList1.ImageStream = (System.Windows.Forms.ImageListStreamer)resources.GetObject("imageList1.ImageStream");
            imageList1.TransparentColor = System.Drawing.Color.Transparent;
            imageList1.Images.SetKeyName(0, "battery.png");
            imageList1.Images.SetKeyName(1, "book_open.png");
            imageList1.Images.SetKeyName(2, "brush3.png");
            imageList1.Images.SetKeyName(3, "calculator.png");
            imageList1.Images.SetKeyName(4, "cd_music.png");
            imageList1.Images.SetKeyName(5, "Close");
            imageList1.Images.SetKeyName(6, "google_favicon.png");
            // 
            // contextMenuStrip1
            // 
            contextMenuStrip1.Name = "contextMenuStrip1";
            contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // customTabControl1
            // 
            customTabControl1.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            customTabControl1.Controls.Add(tabPage1);
            customTabControl1.Controls.Add(tabPage2);
            customTabControl1.Controls.Add(tabPage3);
            customTabControl1.Controls.Add(tabPage4);
            customTabControl1.Controls.Add(tabPage5);
            // 
            // 
            // 
            customTabControl1.DisplayStyleProvider.BorderColor = System.Drawing.SystemColors.ControlDark;
            customTabControl1.DisplayStyleProvider.BorderColorHot = System.Drawing.SystemColors.ControlDark;
            customTabControl1.DisplayStyleProvider.BorderColorSelected = System.Drawing.Color.FromArgb(127, 157, 185);
            customTabControl1.DisplayStyleProvider.CloserColor = System.Drawing.Color.DarkGray;
            customTabControl1.DisplayStyleProvider.FocusTrack = true;
            customTabControl1.DisplayStyleProvider.HotTrack = true;
            customTabControl1.DisplayStyleProvider.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            customTabControl1.DisplayStyleProvider.Opacity = 1.0f;
            customTabControl1.DisplayStyleProvider.Overlap = 0;
            customTabControl1.DisplayStyleProvider.Padding = new System.Drawing.Point(6, 3);
            customTabControl1.DisplayStyleProvider.Radius = 2;
            customTabControl1.DisplayStyleProvider.ShowTabCloser = false;
            customTabControl1.DisplayStyleProvider.TextColor = System.Drawing.SystemColors.ControlText;
            customTabControl1.DisplayStyleProvider.TextColorDisabled = System.Drawing.SystemColors.ControlDark;
            customTabControl1.DisplayStyleProvider.TextColorSelected = System.Drawing.SystemColors.ControlText;
            customTabControl1.HotTrack = true;
            customTabControl1.ImageList = imageList1;
            customTabControl1.Location = new System.Drawing.Point(12, 12);
            customTabControl1.Name = "customTabControl1";
            customTabControl1.SelectedIndex = 0;
            customTabControl1.Size = new System.Drawing.Size(486, 72);
            customTabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            tabPage1.ImageKey = "(none)";
            tabPage1.Location = new System.Drawing.Point(4, 24);
            tabPage1.Name = "tabPage1";
            tabPage1.Padding = new System.Windows.Forms.Padding(3);
            tabPage1.Size = new System.Drawing.Size(478, 44);
            tabPage1.TabIndex = 0;
            tabPage1.Text = "Page 1";
            tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            tabPage2.ImageKey = "(none)";
            tabPage2.Location = new System.Drawing.Point(4, 24);
            tabPage2.Name = "tabPage2";
            tabPage2.Padding = new System.Windows.Forms.Padding(3);
            tabPage2.Size = new System.Drawing.Size(478, 44);
            tabPage2.TabIndex = 1;
            tabPage2.Text = "Page 2";
            tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            tabPage3.ImageKey = "brush3.png";
            tabPage3.Location = new System.Drawing.Point(4, 24);
            tabPage3.Name = "tabPage3";
            tabPage3.Padding = new System.Windows.Forms.Padding(3);
            tabPage3.Size = new System.Drawing.Size(478, 44);
            tabPage3.TabIndex = 2;
            tabPage3.Text = "Page 3";
            tabPage3.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            tabPage4.ImageKey = "(none)";
            tabPage4.Location = new System.Drawing.Point(4, 24);
            tabPage4.Name = "tabPage4";
            tabPage4.Padding = new System.Windows.Forms.Padding(3);
            tabPage4.Size = new System.Drawing.Size(478, 44);
            tabPage4.TabIndex = 3;
            tabPage4.Text = "Page 4";
            tabPage4.UseVisualStyleBackColor = true;
            // 
            // tabPage5
            // 
            tabPage5.ImageKey = "cd_music.png";
            tabPage5.Location = new System.Drawing.Point(4, 24);
            tabPage5.Name = "tabPage5";
            tabPage5.Size = new System.Drawing.Size(478, 44);
            tabPage5.TabIndex = 4;
            tabPage5.Text = "Page 5";
            tabPage5.UseVisualStyleBackColor = true;
            // 
            // customTabControl2
            // 
            customTabControl2.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            customTabControl2.Controls.Add(tabPage6);
            customTabControl2.Controls.Add(tabPage7);
            customTabControl2.Controls.Add(tabPage8);
            customTabControl2.Controls.Add(tabPage9);
            customTabControl2.Controls.Add(tabPage10);
            customTabControl2.DisplayStyle = Core.Controls.TabStyle.VisualStudio;
            // 
            // 
            // 
            customTabControl2.DisplayStyleProvider.BorderColor = System.Drawing.SystemColors.ControlDark;
            customTabControl2.DisplayStyleProvider.BorderColorHot = System.Drawing.SystemColors.ControlDark;
            customTabControl2.DisplayStyleProvider.BorderColorSelected = System.Drawing.Color.FromArgb(127, 157, 185);
            customTabControl2.DisplayStyleProvider.CloserColor = System.Drawing.Color.DarkGray;
            customTabControl2.DisplayStyleProvider.FocusTrack = false;
            customTabControl2.DisplayStyleProvider.HotTrack = true;
            customTabControl2.DisplayStyleProvider.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            customTabControl2.DisplayStyleProvider.Opacity = 1.0f;
            customTabControl2.DisplayStyleProvider.Overlap = 7;
            customTabControl2.DisplayStyleProvider.Padding = new System.Drawing.Point(14, 1);
            customTabControl2.DisplayStyleProvider.ShowTabCloser = false;
            customTabControl2.DisplayStyleProvider.TextColor = System.Drawing.SystemColors.ControlText;
            customTabControl2.DisplayStyleProvider.TextColorDisabled = System.Drawing.SystemColors.ControlDark;
            customTabControl2.DisplayStyleProvider.TextColorSelected = System.Drawing.SystemColors.ControlText;
            customTabControl2.HotTrack = true;
            customTabControl2.ImageList = imageList1;
            customTabControl2.Location = new System.Drawing.Point(12, 94);
            customTabControl2.Name = "customTabControl2";
            customTabControl2.SelectedIndex = 0;
            customTabControl2.Size = new System.Drawing.Size(486, 72);
            customTabControl2.TabIndex = 2;
            // 
            // tabPage6
            // 
            tabPage6.ImageKey = "(none)";
            tabPage6.Location = new System.Drawing.Point(4, 22);
            tabPage6.Name = "tabPage6";
            tabPage6.Padding = new System.Windows.Forms.Padding(3);
            tabPage6.Size = new System.Drawing.Size(478, 46);
            tabPage6.TabIndex = 0;
            tabPage6.Text = "Page 6";
            tabPage6.UseVisualStyleBackColor = true;
            // 
            // tabPage7
            // 
            tabPage7.ImageKey = "book_open.png";
            tabPage7.Location = new System.Drawing.Point(4, 22);
            tabPage7.Name = "tabPage7";
            tabPage7.Padding = new System.Windows.Forms.Padding(3);
            tabPage7.Size = new System.Drawing.Size(478, 46);
            tabPage7.TabIndex = 1;
            tabPage7.Text = "Page 7";
            tabPage7.UseVisualStyleBackColor = true;
            // 
            // tabPage8
            // 
            tabPage8.ImageKey = "(none)";
            tabPage8.Location = new System.Drawing.Point(4, 22);
            tabPage8.Name = "tabPage8";
            tabPage8.Padding = new System.Windows.Forms.Padding(3);
            tabPage8.Size = new System.Drawing.Size(478, 46);
            tabPage8.TabIndex = 2;
            tabPage8.Text = "Page 8";
            tabPage8.UseVisualStyleBackColor = true;
            // 
            // tabPage9
            // 
            tabPage9.ImageKey = "(none)";
            tabPage9.Location = new System.Drawing.Point(4, 22);
            tabPage9.Name = "tabPage9";
            tabPage9.Padding = new System.Windows.Forms.Padding(3);
            tabPage9.Size = new System.Drawing.Size(478, 46);
            tabPage9.TabIndex = 3;
            tabPage9.Text = "Page 9";
            tabPage9.UseVisualStyleBackColor = true;
            // 
            // tabPage10
            // 
            tabPage10.ImageKey = "cd_music.png";
            tabPage10.Location = new System.Drawing.Point(4, 22);
            tabPage10.Name = "tabPage10";
            tabPage10.Size = new System.Drawing.Size(478, 46);
            tabPage10.TabIndex = 4;
            tabPage10.Text = "Page 10";
            tabPage10.UseVisualStyleBackColor = true;
            // 
            // customTabControl3
            // 
            customTabControl3.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            customTabControl3.Controls.Add(tabPage11);
            customTabControl3.Controls.Add(tabPage12);
            customTabControl3.Controls.Add(tabPage13);
            customTabControl3.Controls.Add(tabPage14);
            customTabControl3.Controls.Add(tabPage15);
            customTabControl3.DisplayStyle = Core.Controls.TabStyle.Rounded;
            // 
            // 
            // 
            customTabControl3.DisplayStyleProvider.BorderColor = System.Drawing.SystemColors.ControlDark;
            customTabControl3.DisplayStyleProvider.BorderColorHot = System.Drawing.SystemColors.ControlDark;
            customTabControl3.DisplayStyleProvider.BorderColorSelected = System.Drawing.Color.FromArgb(127, 157, 185);
            customTabControl3.DisplayStyleProvider.CloserColor = System.Drawing.Color.DarkGray;
            customTabControl3.DisplayStyleProvider.FocusTrack = false;
            customTabControl3.DisplayStyleProvider.HotTrack = true;
            customTabControl3.DisplayStyleProvider.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            customTabControl3.DisplayStyleProvider.Opacity = 1.0f;
            customTabControl3.DisplayStyleProvider.Overlap = 5;
            customTabControl3.DisplayStyleProvider.Padding = new System.Drawing.Point(6, 3);
            customTabControl3.DisplayStyleProvider.Radius = 10;
            customTabControl3.DisplayStyleProvider.ShowTabCloser = false;
            customTabControl3.DisplayStyleProvider.TextColor = System.Drawing.SystemColors.ControlText;
            customTabControl3.DisplayStyleProvider.TextColorDisabled = System.Drawing.SystemColors.ControlDark;
            customTabControl3.DisplayStyleProvider.TextColorSelected = System.Drawing.SystemColors.ControlText;
            customTabControl3.HotTrack = true;
            customTabControl3.ImageList = imageList1;
            customTabControl3.Location = new System.Drawing.Point(12, 328);
            customTabControl3.Name = "customTabControl3";
            customTabControl3.SelectedIndex = 0;
            customTabControl3.Size = new System.Drawing.Size(486, 72);
            customTabControl3.TabIndex = 3;
            // 
            // tabPage11
            // 
            tabPage11.ImageKey = "(none)";
            tabPage11.Location = new System.Drawing.Point(4, 24);
            tabPage11.Name = "tabPage11";
            tabPage11.Padding = new System.Windows.Forms.Padding(3);
            tabPage11.Size = new System.Drawing.Size(478, 44);
            tabPage11.TabIndex = 0;
            tabPage11.Text = "Page 11";
            tabPage11.UseVisualStyleBackColor = true;
            // 
            // tabPage12
            // 
            tabPage12.ImageKey = "(none)";
            tabPage12.Location = new System.Drawing.Point(4, 24);
            tabPage12.Name = "tabPage12";
            tabPage12.Padding = new System.Windows.Forms.Padding(3);
            tabPage12.Size = new System.Drawing.Size(478, 44);
            tabPage12.TabIndex = 1;
            tabPage12.Text = "Page 12";
            tabPage12.UseVisualStyleBackColor = true;
            // 
            // tabPage13
            // 
            tabPage13.ImageKey = "brush3.png";
            tabPage13.Location = new System.Drawing.Point(4, 24);
            tabPage13.Name = "tabPage13";
            tabPage13.Padding = new System.Windows.Forms.Padding(3);
            tabPage13.Size = new System.Drawing.Size(478, 44);
            tabPage13.TabIndex = 2;
            tabPage13.Text = "Page 13";
            tabPage13.UseVisualStyleBackColor = true;
            // 
            // tabPage14
            // 
            tabPage14.ImageKey = "calculator.png";
            tabPage14.Location = new System.Drawing.Point(4, 24);
            tabPage14.Name = "tabPage14";
            tabPage14.Padding = new System.Windows.Forms.Padding(3);
            tabPage14.Size = new System.Drawing.Size(478, 44);
            tabPage14.TabIndex = 3;
            tabPage14.Text = "Page 14";
            tabPage14.UseVisualStyleBackColor = true;
            // 
            // tabPage15
            // 
            tabPage15.ImageKey = "cd_music.png";
            tabPage15.Location = new System.Drawing.Point(4, 24);
            tabPage15.Name = "tabPage15";
            tabPage15.Size = new System.Drawing.Size(478, 44);
            tabPage15.TabIndex = 4;
            tabPage15.Text = "Page 15";
            tabPage15.UseVisualStyleBackColor = true;
            // 
            // customTabControl4
            // 
            customTabControl4.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            customTabControl4.Controls.Add(tabPage16);
            customTabControl4.Controls.Add(tabPage17);
            customTabControl4.Controls.Add(tabPage18);
            customTabControl4.Controls.Add(tabPage19);
            customTabControl4.Controls.Add(tabPage20);
            customTabControl4.DisplayStyle = Core.Controls.TabStyle.Angled;
            // 
            // 
            // 
            customTabControl4.DisplayStyleProvider.BorderColor = System.Drawing.SystemColors.ControlDark;
            customTabControl4.DisplayStyleProvider.BorderColorHot = System.Drawing.SystemColors.ControlDark;
            customTabControl4.DisplayStyleProvider.BorderColorSelected = System.Drawing.Color.FromArgb(127, 157, 185);
            customTabControl4.DisplayStyleProvider.CloserColor = System.Drawing.Color.DarkGray;
            customTabControl4.DisplayStyleProvider.FocusTrack = false;
            customTabControl4.DisplayStyleProvider.HotTrack = true;
            customTabControl4.DisplayStyleProvider.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            customTabControl4.DisplayStyleProvider.Opacity = 1.0f;
            customTabControl4.DisplayStyleProvider.Overlap = 7;
            customTabControl4.DisplayStyleProvider.Padding = new System.Drawing.Point(7, 3);
            customTabControl4.DisplayStyleProvider.Radius = 10;
            customTabControl4.DisplayStyleProvider.ShowTabCloser = false;
            customTabControl4.DisplayStyleProvider.TextColor = System.Drawing.SystemColors.ControlText;
            customTabControl4.DisplayStyleProvider.TextColorDisabled = System.Drawing.SystemColors.ControlDark;
            customTabControl4.DisplayStyleProvider.TextColorSelected = System.Drawing.SystemColors.ControlText;
            customTabControl4.HotTrack = true;
            customTabControl4.ImageList = imageList1;
            customTabControl4.Location = new System.Drawing.Point(12, 406);
            customTabControl4.Name = "customTabControl4";
            customTabControl4.SelectedIndex = 0;
            customTabControl4.Size = new System.Drawing.Size(486, 74);
            customTabControl4.TabIndex = 4;
            // 
            // tabPage16
            // 
            tabPage16.ImageKey = "battery.png";
            tabPage16.Location = new System.Drawing.Point(4, 24);
            tabPage16.Name = "tabPage16";
            tabPage16.Padding = new System.Windows.Forms.Padding(3);
            tabPage16.Size = new System.Drawing.Size(478, 46);
            tabPage16.TabIndex = 0;
            tabPage16.Text = "Page 16";
            tabPage16.UseVisualStyleBackColor = true;
            // 
            // tabPage17
            // 
            tabPage17.ImageKey = "book_open.png";
            tabPage17.Location = new System.Drawing.Point(4, 24);
            tabPage17.Name = "tabPage17";
            tabPage17.Padding = new System.Windows.Forms.Padding(3);
            tabPage17.Size = new System.Drawing.Size(478, 46);
            tabPage17.TabIndex = 1;
            tabPage17.Text = "Page 17";
            tabPage17.UseVisualStyleBackColor = true;
            // 
            // tabPage18
            // 
            tabPage18.ImageKey = "brush3.png";
            tabPage18.Location = new System.Drawing.Point(4, 24);
            tabPage18.Name = "tabPage18";
            tabPage18.Padding = new System.Windows.Forms.Padding(3);
            tabPage18.Size = new System.Drawing.Size(478, 46);
            tabPage18.TabIndex = 2;
            tabPage18.Text = "Page 18";
            tabPage18.UseVisualStyleBackColor = true;
            // 
            // tabPage19
            // 
            tabPage19.ImageKey = "(none)";
            tabPage19.Location = new System.Drawing.Point(4, 24);
            tabPage19.Name = "tabPage19";
            tabPage19.Padding = new System.Windows.Forms.Padding(3);
            tabPage19.Size = new System.Drawing.Size(478, 46);
            tabPage19.TabIndex = 3;
            tabPage19.Text = "Page 19";
            tabPage19.UseVisualStyleBackColor = true;
            // 
            // tabPage20
            // 
            tabPage20.ImageKey = "(none)";
            tabPage20.Location = new System.Drawing.Point(4, 24);
            tabPage20.Name = "tabPage20";
            tabPage20.Size = new System.Drawing.Size(478, 46);
            tabPage20.TabIndex = 4;
            tabPage20.Text = "Page 20";
            tabPage20.UseVisualStyleBackColor = true;
            // 
            // customTabControl5
            // 
            customTabControl5.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            customTabControl5.Controls.Add(tabPage21);
            customTabControl5.Controls.Add(tabPage22);
            customTabControl5.Controls.Add(tabPage23);
            customTabControl5.Controls.Add(tabPage24);
            customTabControl5.Controls.Add(tabPage25);
            customTabControl5.DisplayStyle = Core.Controls.TabStyle.Chrome;
            // 
            // 
            // 
            customTabControl5.DisplayStyleProvider.BorderColor = System.Drawing.SystemColors.ControlDark;
            customTabControl5.DisplayStyleProvider.BorderColorHot = System.Drawing.SystemColors.ControlDark;
            customTabControl5.DisplayStyleProvider.BorderColorSelected = System.Drawing.Color.FromArgb(127, 157, 185);
            customTabControl5.DisplayStyleProvider.CloserColor = System.Drawing.Color.DarkGray;
            customTabControl5.DisplayStyleProvider.CloserColorActive = System.Drawing.Color.White;
            customTabControl5.DisplayStyleProvider.FocusTrack = false;
            customTabControl5.DisplayStyleProvider.HotTrack = true;
            customTabControl5.DisplayStyleProvider.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            customTabControl5.DisplayStyleProvider.Opacity = 1.0f;
            customTabControl5.DisplayStyleProvider.Overlap = 16;
            customTabControl5.DisplayStyleProvider.Padding = new System.Drawing.Point(7, 5);
            customTabControl5.DisplayStyleProvider.Radius = 16;
            customTabControl5.DisplayStyleProvider.ShowTabCloser = true;
            customTabControl5.DisplayStyleProvider.TextColor = System.Drawing.SystemColors.ControlText;
            customTabControl5.DisplayStyleProvider.TextColorDisabled = System.Drawing.SystemColors.ControlDark;
            customTabControl5.DisplayStyleProvider.TextColorSelected = System.Drawing.SystemColors.ControlText;
            customTabControl5.HotTrack = true;
            customTabControl5.ImageList = imageList1;
            customTabControl5.Location = new System.Drawing.Point(12, 172);
            customTabControl5.Name = "customTabControl5";
            customTabControl5.SelectedIndex = 0;
            customTabControl5.Size = new System.Drawing.Size(486, 72);
            customTabControl5.TabIndex = 5;
            // 
            // tabPage21
            // 
            tabPage21.ImageKey = "(none)";
            tabPage21.Location = new System.Drawing.Point(4, 28);
            tabPage21.Name = "tabPage21";
            tabPage21.Padding = new System.Windows.Forms.Padding(3);
            tabPage21.Size = new System.Drawing.Size(478, 40);
            tabPage21.TabIndex = 0;
            tabPage21.Text = "page 21";
            tabPage21.UseVisualStyleBackColor = true;
            // 
            // tabPage22
            // 
            tabPage22.ImageKey = "google_favicon.png";
            tabPage22.Location = new System.Drawing.Point(4, 28);
            tabPage22.Name = "tabPage22";
            tabPage22.Padding = new System.Windows.Forms.Padding(3);
            tabPage22.Size = new System.Drawing.Size(478, 40);
            tabPage22.TabIndex = 1;
            tabPage22.Text = "Page 22";
            tabPage22.UseVisualStyleBackColor = true;
            // 
            // tabPage23
            // 
            tabPage23.ImageKey = "(none)";
            tabPage23.Location = new System.Drawing.Point(4, 28);
            tabPage23.Name = "tabPage23";
            tabPage23.Padding = new System.Windows.Forms.Padding(3);
            tabPage23.Size = new System.Drawing.Size(478, 40);
            tabPage23.TabIndex = 2;
            tabPage23.Text = "Page 23";
            tabPage23.UseVisualStyleBackColor = true;
            // 
            // tabPage24
            // 
            tabPage24.ImageKey = "(none)";
            tabPage24.Location = new System.Drawing.Point(4, 28);
            tabPage24.Name = "tabPage24";
            tabPage24.Padding = new System.Windows.Forms.Padding(3);
            tabPage24.Size = new System.Drawing.Size(478, 40);
            tabPage24.TabIndex = 3;
            tabPage24.Text = "Page 24";
            tabPage24.UseVisualStyleBackColor = true;
            // 
            // tabPage25
            // 
            tabPage25.ImageKey = "google_favicon.png";
            tabPage25.Location = new System.Drawing.Point(4, 28);
            tabPage25.Name = "tabPage25";
            tabPage25.Size = new System.Drawing.Size(478, 40);
            tabPage25.TabIndex = 4;
            tabPage25.Text = "Page 25";
            tabPage25.UseVisualStyleBackColor = true;
            // 
            // customTabControl6
            // 
            customTabControl6.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            customTabControl6.Controls.Add(tabPage26);
            customTabControl6.Controls.Add(tabPage27);
            customTabControl6.Controls.Add(tabPage28);
            customTabControl6.Controls.Add(tabPage29);
            customTabControl6.Controls.Add(tabPage30);
            customTabControl6.DisplayStyle = Core.Controls.TabStyle.IE8;
            // 
            // 
            // 
            customTabControl6.DisplayStyleProvider.BorderColor = System.Drawing.SystemColors.ControlDark;
            customTabControl6.DisplayStyleProvider.BorderColorHot = System.Drawing.SystemColors.ControlDark;
            customTabControl6.DisplayStyleProvider.BorderColorSelected = System.Drawing.Color.FromArgb(127, 157, 185);
            customTabControl6.DisplayStyleProvider.CloserColor = System.Drawing.Color.DarkGray;
            customTabControl6.DisplayStyleProvider.CloserColorActive = System.Drawing.Color.Red;
            customTabControl6.DisplayStyleProvider.FocusTrack = false;
            customTabControl6.DisplayStyleProvider.HotTrack = true;
            customTabControl6.DisplayStyleProvider.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            customTabControl6.DisplayStyleProvider.Opacity = 1.0f;
            customTabControl6.DisplayStyleProvider.Overlap = 0;
            customTabControl6.DisplayStyleProvider.Padding = new System.Drawing.Point(6, 5);
            customTabControl6.DisplayStyleProvider.Radius = 3;
            customTabControl6.DisplayStyleProvider.ShowTabCloser = true;
            customTabControl6.DisplayStyleProvider.TextColor = System.Drawing.SystemColors.ControlText;
            customTabControl6.DisplayStyleProvider.TextColorDisabled = System.Drawing.SystemColors.ControlDark;
            customTabControl6.DisplayStyleProvider.TextColorSelected = System.Drawing.SystemColors.ControlText;
            customTabControl6.HotTrack = true;
            customTabControl6.ImageList = imageList1;
            customTabControl6.Location = new System.Drawing.Point(12, 250);
            customTabControl6.Name = "customTabControl6";
            customTabControl6.SelectedIndex = 0;
            customTabControl6.Size = new System.Drawing.Size(486, 72);
            customTabControl6.TabIndex = 6;
            // 
            // tabPage26
            // 
            tabPage26.ImageKey = "google_favicon.png";
            tabPage26.Location = new System.Drawing.Point(4, 28);
            tabPage26.Name = "tabPage26";
            tabPage26.Padding = new System.Windows.Forms.Padding(3);
            tabPage26.Size = new System.Drawing.Size(478, 40);
            tabPage26.TabIndex = 0;
            tabPage26.Text = "Page 26";
            tabPage26.UseVisualStyleBackColor = true;
            // 
            // tabPage27
            // 
            tabPage27.ImageKey = "google_favicon.png";
            tabPage27.Location = new System.Drawing.Point(4, 28);
            tabPage27.Name = "tabPage27";
            tabPage27.Padding = new System.Windows.Forms.Padding(3);
            tabPage27.Size = new System.Drawing.Size(478, 40);
            tabPage27.TabIndex = 1;
            tabPage27.Text = "Page 27";
            tabPage27.UseVisualStyleBackColor = true;
            // 
            // tabPage28
            // 
            tabPage28.ImageKey = "(none)";
            tabPage28.Location = new System.Drawing.Point(4, 28);
            tabPage28.Name = "tabPage28";
            tabPage28.Padding = new System.Windows.Forms.Padding(3);
            tabPage28.Size = new System.Drawing.Size(478, 40);
            tabPage28.TabIndex = 2;
            tabPage28.Text = "Page 28";
            tabPage28.UseVisualStyleBackColor = true;
            // 
            // tabPage29
            // 
            tabPage29.ImageKey = "(none)";
            tabPage29.Location = new System.Drawing.Point(4, 28);
            tabPage29.Name = "tabPage29";
            tabPage29.Padding = new System.Windows.Forms.Padding(3);
            tabPage29.Size = new System.Drawing.Size(478, 40);
            tabPage29.TabIndex = 3;
            tabPage29.Text = "Page 29";
            tabPage29.UseVisualStyleBackColor = true;
            // 
            // tabPage30
            // 
            tabPage30.ImageKey = "google_favicon.png";
            tabPage30.Location = new System.Drawing.Point(4, 28);
            tabPage30.Name = "tabPage30";
            tabPage30.Size = new System.Drawing.Size(478, 40);
            tabPage30.TabIndex = 4;
            tabPage30.Text = "Page 30";
            tabPage30.UseVisualStyleBackColor = true;
            // 
            // customTabControl7
            // 
            customTabControl7.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            customTabControl7.Controls.Add(tabPage31);
            customTabControl7.Controls.Add(tabPage32);
            customTabControl7.Controls.Add(tabPage33);
            customTabControl7.Controls.Add(tabPage34);
            customTabControl7.Controls.Add(tabPage35);
            customTabControl7.DisplayStyle = Core.Controls.TabStyle.VS2010;
            // 
            // 
            // 
            customTabControl7.DisplayStyleProvider.BorderColor = System.Drawing.Color.Transparent;
            customTabControl7.DisplayStyleProvider.BorderColorHot = System.Drawing.Color.FromArgb(155, 167, 183);
            customTabControl7.DisplayStyleProvider.BorderColorSelected = System.Drawing.Color.FromArgb(127, 157, 185);
            customTabControl7.DisplayStyleProvider.CloserColor = System.Drawing.Color.FromArgb(117, 99, 61);
            customTabControl7.DisplayStyleProvider.FocusTrack = false;
            customTabControl7.DisplayStyleProvider.HotTrack = true;
            customTabControl7.DisplayStyleProvider.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            customTabControl7.DisplayStyleProvider.Opacity = 1.0f;
            customTabControl7.DisplayStyleProvider.Overlap = 0;
            customTabControl7.DisplayStyleProvider.Padding = new System.Drawing.Point(6, 5);
            customTabControl7.DisplayStyleProvider.Radius = 3;
            customTabControl7.DisplayStyleProvider.ShowTabCloser = true;
            customTabControl7.DisplayStyleProvider.TextColor = System.Drawing.Color.White;
            customTabControl7.DisplayStyleProvider.TextColorDisabled = System.Drawing.Color.WhiteSmoke;
            customTabControl7.DisplayStyleProvider.TextColorSelected = System.Drawing.SystemColors.ControlText;
            customTabControl7.HotTrack = true;
            customTabControl7.ImageList = imageList1;
            customTabControl7.Location = new System.Drawing.Point(12, 4);
            customTabControl7.Name = "customTabControl7";
            customTabControl7.SelectedIndex = 0;
            customTabControl7.Size = new System.Drawing.Size(482, 74);
            customTabControl7.TabIndex = 7;
            // 
            // tabPage31
            // 
            tabPage31.ImageKey = "battery.png";
            tabPage31.Location = new System.Drawing.Point(4, 28);
            tabPage31.Name = "tabPage31";
            tabPage31.Padding = new System.Windows.Forms.Padding(3);
            tabPage31.Size = new System.Drawing.Size(474, 42);
            tabPage31.TabIndex = 0;
            tabPage31.Text = "Page 31";
            tabPage31.UseVisualStyleBackColor = true;
            // 
            // tabPage32
            // 
            tabPage32.ImageKey = "book_open.png";
            tabPage32.Location = new System.Drawing.Point(4, 28);
            tabPage32.Name = "tabPage32";
            tabPage32.Padding = new System.Windows.Forms.Padding(3);
            tabPage32.Size = new System.Drawing.Size(484, 42);
            tabPage32.TabIndex = 1;
            tabPage32.Text = "Page32";
            tabPage32.UseVisualStyleBackColor = true;
            // 
            // tabPage33
            // 
            tabPage33.ImageKey = "brush3.png";
            tabPage33.Location = new System.Drawing.Point(4, 28);
            tabPage33.Name = "tabPage33";
            tabPage33.Padding = new System.Windows.Forms.Padding(3);
            tabPage33.Size = new System.Drawing.Size(484, 42);
            tabPage33.TabIndex = 2;
            tabPage33.Text = "Page 33";
            tabPage33.UseVisualStyleBackColor = true;
            // 
            // tabPage34
            // 
            tabPage34.ImageKey = "(none)";
            tabPage34.Location = new System.Drawing.Point(4, 28);
            tabPage34.Name = "tabPage34";
            tabPage34.Padding = new System.Windows.Forms.Padding(3);
            tabPage34.Size = new System.Drawing.Size(484, 42);
            tabPage34.TabIndex = 3;
            tabPage34.Text = "Page 34";
            tabPage34.UseVisualStyleBackColor = true;
            // 
            // tabPage35
            // 
            tabPage35.ImageKey = "(none)";
            tabPage35.Location = new System.Drawing.Point(4, 28);
            tabPage35.Name = "tabPage35";
            tabPage35.Size = new System.Drawing.Size(484, 42);
            tabPage35.TabIndex = 4;
            tabPage35.Text = "Page 35";
            tabPage35.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            panel1.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            panel1.BackColor = System.Drawing.Color.FromArgb(43, 60, 89);
            panel1.Controls.Add(customTabControl7);
            panel1.Location = new System.Drawing.Point(0, 482);
            panel1.Name = "panel1";
            panel1.Size = new System.Drawing.Size(512, 88);
            panel1.TabIndex = 8;
            // 
            // MainForm
            // 
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            BackgroundImage = (System.Drawing.Image)resources.GetObject("$this.BackgroundImage");
            BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            ClientSize = new System.Drawing.Size(510, 570);
            Controls.Add(panel1);
            Controls.Add(customTabControl1);
            Controls.Add(customTabControl2);
            Controls.Add(customTabControl6);
            Controls.Add(customTabControl5);
            Controls.Add(customTabControl3);
            Controls.Add(customTabControl4);
            DoubleBuffered = true;
            MinimumSize = new System.Drawing.Size(526, 521);
            Name = "MainForm";
            Text = "Tab Control Demo";
            customTabControl1.ResumeLayout(false);
            customTabControl2.ResumeLayout(false);
            customTabControl3.ResumeLayout(false);
            customTabControl4.ResumeLayout(false);
            customTabControl5.ResumeLayout(false);
            customTabControl6.ResumeLayout(false);
            customTabControl7.ResumeLayout(false);
            panel1.ResumeLayout(false);
            ResumeLayout(false);
        }

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabPage tabPage35;
        private System.Windows.Forms.TabPage tabPage34;
        private System.Windows.Forms.TabPage tabPage33;
        private System.Windows.Forms.TabPage tabPage32;
        private System.Windows.Forms.TabPage tabPage31;
        private Controls.CustomTabControl customTabControl7;
        private System.Windows.Forms.TabPage tabPage30;
        private System.Windows.Forms.TabPage tabPage29;
        private System.Windows.Forms.TabPage tabPage28;
        private System.Windows.Forms.TabPage tabPage27;
        private System.Windows.Forms.TabPage tabPage26;
        private Controls.CustomTabControl customTabControl6;
        private System.Windows.Forms.TabPage tabPage25;
        private System.Windows.Forms.TabPage tabPage24;
        private System.Windows.Forms.TabPage tabPage23;
        private System.Windows.Forms.TabPage tabPage22;
        private System.Windows.Forms.TabPage tabPage21;
        private Controls.CustomTabControl customTabControl5;
        private System.Windows.Forms.TabPage tabPage20;
        private System.Windows.Forms.TabPage tabPage19;
        private System.Windows.Forms.TabPage tabPage18;
        private System.Windows.Forms.TabPage tabPage17;
        private System.Windows.Forms.TabPage tabPage16;
        private Controls.CustomTabControl customTabControl4;
        private System.Windows.Forms.TabPage tabPage15;
        private System.Windows.Forms.TabPage tabPage14;
        private System.Windows.Forms.TabPage tabPage13;
        private System.Windows.Forms.TabPage tabPage12;
        private System.Windows.Forms.TabPage tabPage11;
        private Controls.CustomTabControl customTabControl3;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.TabPage tabPage6;
        private Controls.CustomTabControl customTabControl2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage1;
        private Controls.CustomTabControl customTabControl1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ImageList imageList1;
    }
}