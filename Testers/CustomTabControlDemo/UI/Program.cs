﻿// 
// * Created by SharpDevelop.
// * User: mjackson
// * Date: 2010-28-06
// * Time: 13:38
// * 
// * To change this template use Tools | Options | Coding | Edit Standard Headers.
// 

using System;
using System.Windows.Forms;

namespace isr.Core.Tester
{

    /// <summary> Class with program entry point. </summary>
/// <remarks> David, 2021-03-12. </remarks>
    internal sealed class Program
    {

        /// <summary> Program entry point. </summary>
	/// <remarks> David, 2021-03-12. </remarks>
        [STAThread]
        internal static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}