﻿using System;
using System.Windows.Forms;

namespace isr.Core.Tester
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            base.Load += Form1_Load;
            this.Shown += Form1_Shown;
            InitializeComponent();
        }

        private void FadeTrackBar_Scroll(object sender, EventArgs e)
        {
            int fadeAlpha = FadeTrackBar.Value;
            CustomProgressBar.Fade = fadeAlpha;
            _StatusStripCustomProgressBar.Fade = fadeAlpha;
            FadeTrackBarlabel.Text = "Fade Alpha: " + fadeAlpha.ToString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            CustomProgressBar.Value = 75;
            StandardProgressBar.Value = 75;
            _StatusStripCustomProgressBar.Value = 75;
            _ToolStripProgressBar.Value = 75;
        }
    }
}