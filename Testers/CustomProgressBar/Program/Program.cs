﻿using System;
using System.Windows.Forms;

namespace isr.Core.Tester
{
    internal sealed class Program
    {
        private Program()
        {
        }

        /// <summary>
    /// The main entry point for the application.
    /// </summary>
        [STAThread]
        public static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}