using System;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Tester
{
    public partial class Form1
    {
        /// <summary>
    /// Required designer variable.
    /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && components is object)
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        /// <summary>
        /// Required method for Designer support - do not modify the contents of this method with the
        /// code editor.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        private void InitializeComponent()
        {
            StandardProgressBar = new ProgressBar();
            _FadeTrackBar = new TrackBar();
            _FadeTrackBar.Scroll += new EventHandler(FadeTrackBar_Scroll);
            StandardProgressBarLabel = new Label();
            CustomProgressBarLabel = new Label();
            FadeTrackBarlabel = new Label();
            CustomProgressBar = new Controls.CustomProgressBar();
            StatusStrip1 = new StatusStrip();
            _ToolStripProgressBar = new ToolStripProgressBar();
            _StatusStripCustomProgressBar = new Controls.StatusStripCustomProgressBar();
            ((System.ComponentModel.ISupportInitialize)_FadeTrackBar).BeginInit();
            StatusStrip1.SuspendLayout();
            SuspendLayout();
            // 
            // StandardProgressBar
            // 
            StandardProgressBar.Location = new Point(133, 24);
            StandardProgressBar.Name = "StandardProgressBar";
            StandardProgressBar.Size = new Size(321, 23);
            StandardProgressBar.TabIndex = 1;
            // 
            // FadeTrackBar
            // 
            _FadeTrackBar.Location = new Point(125, 124);
            _FadeTrackBar.Maximum = 200;
            _FadeTrackBar.Minimum = 100;
            _FadeTrackBar.Name = "_FadeTrackBar";
            _FadeTrackBar.Size = new Size(336, 45);
            _FadeTrackBar.TabIndex = 5;
            _FadeTrackBar.Value = 150;
            // 
            // StandardProgressBarLabel
            // 
            StandardProgressBarLabel.AutoSize = true;
            StandardProgressBarLabel.Location = new Point(19, 29);
            StandardProgressBarLabel.Name = "StandardProgressBarLabel";
            StandardProgressBarLabel.Size = new Size(110, 13);
            StandardProgressBarLabel.TabIndex = 0;
            StandardProgressBarLabel.Text = "Standard Progress Bar";
            // 
            // CustomProgressBarLabel
            // 
            CustomProgressBarLabel.AutoSize = true;
            CustomProgressBarLabel.Location = new Point(19, 76);
            CustomProgressBarLabel.Name = "CustomProgressBarLabel";
            CustomProgressBarLabel.Size = new Size(102, 13);
            CustomProgressBarLabel.TabIndex = 2;
            CustomProgressBarLabel.Text = "Custom Progress Bar";
            // 
            // FadeTrackBarlabel
            // 
            FadeTrackBarlabel.AutoSize = true;
            FadeTrackBarlabel.Location = new Point(19, 123);
            FadeTrackBarlabel.Name = "FadeTrackBarlabel";
            FadeTrackBarlabel.Size = new Size(85, 13);
            FadeTrackBarlabel.TabIndex = 4;
            FadeTrackBarlabel.Text = "Fade Alpha: 150";
            // 
            // CustomProgressBar
            // 
            CustomProgressBar.Font = new Font("Microsoft Sans Serif", 9.0f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            CustomProgressBar.Location = new Point(133, 70);
            CustomProgressBar.Name = "CustomProgressBar";
            CustomProgressBar.Size = new Size(321, 23);
            CustomProgressBar.TabIndex = 3;
            CustomProgressBar.Text = "0 %";
            // 
            // StatusStrip1
            // 
            StatusStrip1.Items.AddRange(new ToolStripItem[] { _ToolStripProgressBar, _StatusStripCustomProgressBar });
            StatusStrip1.Location = new Point(0, 150);
            StatusStrip1.Name = "StatusStrip1";
            StatusStrip1.Size = new Size(484, 29);
            StatusStrip1.TabIndex = 6;
            StatusStrip1.Text = "Status Strip";
            // 
            // _ToolStripProgressBar
            // 
            _ToolStripProgressBar.Name = "_ToolStripProgressBar";
            _ToolStripProgressBar.Size = new Size(100, 23);
            // 
            // _StatusStripCustomProgressBar
            // 
            _StatusStripCustomProgressBar.ForeColor = SystemColors.ControlText;
            _StatusStripCustomProgressBar.Margin = new Padding(1, 3, 1, 3);
            _StatusStripCustomProgressBar.Name = "_StatusStripCustomProgressBar";
            _StatusStripCustomProgressBar.Size = new Size(100, 23);
            _StatusStripCustomProgressBar.Text = "0 %";
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(6.0f, 13.0f);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(484, 179);
            Controls.Add(StatusStrip1);
            Controls.Add(CustomProgressBar);
            Controls.Add(FadeTrackBarlabel);
            Controls.Add(CustomProgressBarLabel);
            Controls.Add(StandardProgressBarLabel);
            Controls.Add(_FadeTrackBar);
            Controls.Add(StandardProgressBar);
            Name = "Form1";
            Text = "Custom Bar Demo";
            ((System.ComponentModel.ISupportInitialize)_FadeTrackBar).EndInit();
            StatusStrip1.ResumeLayout(false);
            StatusStrip1.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        private Label StandardProgressBarLabel;
        private ProgressBar StandardProgressBar;
        private Label CustomProgressBarLabel;
        private Controls.CustomProgressBar CustomProgressBar;
        private Label FadeTrackBarlabel;
        private TrackBar _FadeTrackBar;

        private TrackBar FadeTrackBar
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _FadeTrackBar;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_FadeTrackBar != null)
                {
                    _FadeTrackBar.Scroll -= FadeTrackBar_Scroll;
                }

                _FadeTrackBar = value;
                if (_FadeTrackBar != null)
                {
                    _FadeTrackBar.Scroll += FadeTrackBar_Scroll;
                }
            }
        }

        private StatusStrip StatusStrip1;
        private Controls.StatusStripCustomProgressBar _StatusStripCustomProgressBar;
        private ToolStripProgressBar _ToolStripProgressBar;
    }
}
