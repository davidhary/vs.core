using System;
using System.Runtime.InteropServices;
using System.IO;
using System.ComponentModel;
using System.Security.Permissions;
using System.Security;
using System.Threading;
using Microsoft.Win32.SafeHandles;
using System.Runtime.ConstrainedExecution;

namespace SafeHandleDemo
{
    /// <summary>   my safe file handle. </summary>
    /// <remarks>   David, 2020-05-01.
    /// https://docs.microsoft.com/en-us/dotnet/api/system.runtime.interopservices.safehandle?view=netframework-4.8
    /// when to use a safe handle:
    /// https://stackoverflow.com/questions/155780/safehandle-in-c-sharp
    /// </remarks>
    [SecurityPermission( SecurityAction.InheritanceDemand, UnmanagedCode = true )]
    [SecurityPermission( SecurityAction.Demand, UnmanagedCode = true )]
    internal class MySafeFileHandle : SafeHandleZeroOrMinusOneIsInvalid
    {
        // Create a SafeHandle, informing the base class
        // that this SafeHandle instance "owns" the handle,
        // and therefore SafeHandle should call
        // our ReleaseHandle method when the SafeHandle
        // is no longer in use.
        private MySafeFileHandle() : base( true )
        {
        }
        [ReliabilityContract( Consistency.WillNotCorruptState, Cer.MayFail )]
        protected override bool ReleaseHandle()
        {
            // Here, we must obey all rules for constrained execution regions.
            return NativeMethods.CloseHandle( this.handle );
            // If ReleaseHandle failed, it can be reported via the
            // "releaseHandleFailed" managed debugging assistant (MDA).  This
            // MDA is disabled by default, but can be enabled in a debugger
            // or during testing to diagnose handle corruption problems.
            // We do not throw an exception because most code could not recover
            // from the problem.
        }
    }

    [SuppressUnmanagedCodeSecurity()]
    internal static class NativeMethods
    {
        // Win32 constants for accessing files.
        internal const int GENERIC_READ = unchecked(( int ) 0x80000000);

        // Allocate a file object in the kernel, then return a handle to it.
        [DllImport( "kernel32", SetLastError = true, CharSet = CharSet.Unicode )]
        internal static extern MySafeFileHandle CreateFile( String fileName, int dwDesiredAccess,
                                                           System.IO.FileShare dwShareMode,
                                                           IntPtr securityAttrs_MustBeZero,
                                                           System.IO.FileMode dwCreationDisposition,
                                                           int dwFlagsAndAttributes, IntPtr hTemplateFile_MustBeZero );

        // Use the file handle.
        [DllImport( "kernel32", SetLastError = true )]
        internal static extern int ReadFile( MySafeFileHandle handle, byte[] bytes,
                                             int numBytesToRead, out int numBytesRead, IntPtr overlapped_MustBeZero );

        // Free the kernel's file object (close the file).
        [DllImport( "kernel32", SetLastError = true )]
        [ReliabilityContract( Consistency.WillNotCorruptState, Cer.MayFail )]
        internal static extern bool CloseHandle( IntPtr handle );
    }

    // The MyFileReader class is a sample class that accesses an operating system
    // resource and implements IDisposable. This is useful to show the types of
    // transformation required to make your resource wrapping classes
    // more resilient. Note the Dispose and Finalize implementations.
    // Consider this a simulation of System.IO.FileStream.
    public class MyFileReader : IDisposable
    {
        // _handle is set to null to indicate disposal of this instance.
        private readonly MySafeFileHandle _Handle;

        public MyFileReader( String fileName )
        {
            // Security permission check.
            String fullPath = Path.GetFullPath( fileName );
            new FileIOPermission( FileIOPermissionAccess.Read, fullPath ).Demand();

            // Open a file, and save its handle in _handle.
            // Note that the most optimized code turns into two processor
            // instructions: 1) a call, and 2) moving the return value into
            // the _handle field.  With SafeHandle, the CLR's platform invoke
            // marshaling layer will store the handle into the SafeHandle
            // object in an atomic fashion. There is still the problem
            // that the SafeHandle object may not be stored in _handle, but
            // the real operating system handle value has been safely stored
            // in a critical finalizable object, ensuring against leaking
            // the handle even if there is an asynchronous exception.

            MySafeFileHandle tmpHandle;
            tmpHandle = NativeMethods.CreateFile( fullPath, NativeMethods.GENERIC_READ,
                FileShare.Read, IntPtr.Zero, FileMode.Open, 0, IntPtr.Zero );

            // An Async exception here will cause us to run our finalizer with
            // a null _handle, but MySafeFileHandle's ReleaseHandle code will
            // be invoked to free the handle.

            // This call to Sleep, run from the fault injection code in Main,
            // will help trigger a race. But it will not cause a handle leak
            // because the handle is already stored in a SafeHandle instance.
            // Critical finalization then guarantees that freeing the handle,
            // even during an unexpected AppDomain unload.
            Thread.Sleep( 500 );
            this._Handle = tmpHandle;  // Makes _handle point to a critical finalizable object.

            // Determine if file is opened successfully.
            if ( this._Handle.IsInvalid )
            {
                throw new Win32Exception( Marshal.GetLastWin32Error(), fullPath );
            }
        }

        public void Dispose()  // Follow the Dispose pattern - public non-virtual.
        {
            this.Dispose( true );
            GC.SuppressFinalize( this );
        }

        // No finalizer is needed. The finalizer on SafeHandle
        // will clean up the MySafeFileHandle instance,
        // if it hasn't already been disposed.
        // However, there may be a need for a subclass to
        // introduce a finalizer, so Dispose is properly implemented here.
        [SecurityPermission( SecurityAction.Demand, UnmanagedCode = true )]
        protected virtual void Dispose( bool disposing )
        {
            // Note there are three interesting states here:
            // 1) CreateFile failed, _handle contains an invalid handle
            // 2) We called Dispose already, _handle is closed.
            // 3) _handle is null, due to an async exception before
            //    calling CreateFile. Note that the finalizer runs
            //    if the constructor fails.
            if ( this._Handle != null && !this._Handle.IsInvalid )
            {
                // Free the handle
                this._Handle.Dispose();
            }
            // SafeHandle records the fact that we've called Dispose.
        }

        [SecurityPermission( SecurityAction.Demand, UnmanagedCode = true )]
        public byte[] ReadContents( int length )
        {
            if ( this._Handle.IsInvalid )  // Is the handle disposed?
                throw new ObjectDisposedException( "FileReader is closed" );

            // This sample code will not work for all files.
            byte[] bytes = new byte[length];
            int r = NativeMethods.ReadFile( this._Handle, bytes, length, out int numRead, IntPtr.Zero );
            // Since we removed MyFileReader's finalizer, we no longer need to
            // call GC.KeepAlive here.  Platform invoke will keep the SafeHandle
            // instance alive for the duration of the call.
            if ( r == 0 )
                throw new Win32Exception( Marshal.GetLastWin32Error() );
            if ( numRead < length )
            {
                byte[] newBytes = new byte[numRead];
                Array.Copy( bytes, newBytes, numRead );
                bytes = newBytes;
            }
            return bytes;
        }
    }

    /// <summary>   A program. </summary>
    /// <remarks>
    /// The following code example creates a custom safe handle for an operating system file handle,
    /// deriving from SafeHandleZeroOrMinusOneIsInvalid
    /// https://docs.microsoft.com/en-us/dotnet/api/microsoft.win32.safehandles.safehandlezeroorminusoneisinvalid?view=netframework-4.8
    /// It reads bytes from a file and displays
    /// their hexadecimal values. It also contains a fault testing harness that causes the thread to
    /// abort, but the handle value is freed. When using an IntPtr to represent handles, the handle
    /// is occasionally leaked due to the asynchronous thread abort.
    /// 
    /// You will need a text file in the same folder as the compiled application.Assuming that you
    /// name the application "HexViewer", the command line usage is:
    /// 
    /// HexViewer&lt;filename&gt; -Fault
    /// 
    /// Optionally specify -Fault to intentionally attempt to leak the handle by aborting the thread
    /// in a certain window.Use the Windows Perfmon.exe tool to monitor handle counts while injecting
    /// faults.
    /// https://docs.microsoft.com/en-us/dotnet/api/system.runtime.interopservices.safehandle?view=netframework-4.8
    /// 
    /// </remarks>
    internal static class Program
    {
        // Testing harness that injects faults.
        private static bool _PrintToConsole = false;
        private static bool _WorkerStarted = false;

        private static void Usage()
        {
            Console.WriteLine( "Usage:" );
            // Assumes that application is named HexViwer"
            Console.WriteLine( "HexViewer <fileName> [-fault]" );
            Console.WriteLine( " -fault Runs hex viewer repeatedly, injecting faults." );
        }

        private static void ViewInHex( Object fileName )
        {
            _WorkerStarted = true;
            byte[] bytes;
            using ( MyFileReader reader = new( ( String ) fileName ) )
            {
                bytes = reader.ReadContents( 20 );
            }  // Using block calls Dispose() for us here.

            if ( _PrintToConsole )
            {
                // Print up to 20 bytes.
                int printNBytes = Math.Min( 20, bytes.Length );
                Console.WriteLine( "First {0} bytes of {1} in hex", printNBytes, fileName );
                for ( int i = 0; i < printNBytes; i++ )
                    Console.Write( "{0:x} ", bytes[i] );
                Console.WriteLine();
            }
        }

        private static void Main( string[] args )
        {
            if ( args.Length == 0 || args.Length > 2 ||
                args[0] == "-?" || args[0] == "/?" )
            {
                Usage();
                return;
            }

            String fileName = args[0];
            bool injectFaultMode = args.Length > 1;
            if ( !injectFaultMode )
            {
                _PrintToConsole = true;
                ViewInHex( fileName );
            }
            else
            {
                Console.WriteLine( @"Injecting faults - watch handle count in PERFMON (press CTRL+C when done)
Open Performance monitor; under show select add counter; Select Process; Select Hex Viewer; Select Handle Count for the process." );
                int numIterations = 0;
                while ( true )
                {
                    _WorkerStarted = false;
                    Thread t = new( new ParameterizedThreadStart( ViewInHex ) );
                    t.Start( fileName );
                    Thread.Sleep( 1 );
                    while ( !_WorkerStarted )
                    {
                        Thread.Sleep( 0 );
                    }
                    t.Abort();  // Normal applications should not do this.
                    numIterations++;
                    if ( numIterations % 10 == 0 )
                        GC.Collect();
                    if ( numIterations % 10000 == 0 )
                        Console.WriteLine( numIterations );
                }
            }
        }
    }
}
