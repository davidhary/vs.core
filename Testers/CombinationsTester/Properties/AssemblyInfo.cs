﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Linq Statistics Console Tester")]
[assembly: AssemblyDescription("Linq Statistics Console Tester")]
[assembly: AssemblyProduct("Linq.Statistics.Console.Tester")]
[assembly: CLSCompliant(true)]
[assembly: ComVisible(false)]
