using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace isr.Core.Testers.CombinationsExtensions
{

    /// <summary> Extension methods for combinations. </summary>
    /// <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para> </remarks>
    public static class CombinationsExtensionsMethods
    {

        /// <summary> Generates the combinations. </summary>
        /// <param name="collectionOfSeries"> The collection of series. </param>
        /// <returns> The combinations. </returns>
        /// <remarks>https://stackoverflow.com/questions/545703/combination-of-listlistint</remarks>
        public static List<List<T>> GenerateCombinations<T>(this List<List<T>> collectionOfSeries)
        {
            var generatedCombinations = collectionOfSeries.Take(1).FirstOrDefault().Select(i => (new T[] { i }).ToList()).ToList();
            foreach (List<T> series in collectionOfSeries.Skip(1))
            {
                generatedCombinations = generatedCombinations.Join(series as List<T>, combination => true, i => true, (combination, i) =>
                {
                    var nextLevelCombination = new List<T>(combination) { i };
                    // nextLevelCombination.Add(i)
                    return nextLevelCombination;
                }).ToList();
            }

            return generatedCombinations;
        }
    }
}
