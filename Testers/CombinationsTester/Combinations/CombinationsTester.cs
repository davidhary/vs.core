using System;
using System.Collections.Generic;
using Microsoft.VisualBasic;

namespace isr.Core.Testers
{
    internal class Program
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>")]
        public static void Main(string[] args)
        {
            GenerateCombinationsTest();
        }

        private static void GenerateCombinationsTest()
        {
            var collectionOfSeries = new List<List<int>>() { new List<int>(new[] { 1, 2, 3, 4, 5 }), new List<int>(new[] { 0, 1 }), new List<int>(new[] { 6, 3 }), new List<int>(new[] { 1, 3, 5 }) };
            var combinations = CombinationsExtensions.CombinationsExtensionsMethods.GenerateCombinations<int>(collectionOfSeries);
            Display<int>(combinations);
        }

        private static void Display<T>(List<List<T>> generatedCombinations)
        {
            int index = 0;
            foreach (List<T> generatedCombination in generatedCombinations)
            {
                Console.Write("{0}" + Constants.vbTab + ":", System.Threading.Interlocked.Increment(ref index));
                foreach (T i in generatedCombination)
                    Console.Write("{0,3}", i);
                Console.WriteLine();
            }

            Console.ReadKey();
        }
    }
}
