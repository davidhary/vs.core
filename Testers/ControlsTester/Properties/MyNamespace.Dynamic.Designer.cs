using System;
using System.ComponentModel;
using System.Diagnostics;

namespace ControlsTester.My
{

    internal static partial class MyProject
    {

        internal partial class MyForms
        {
            /// <summary>   The Switchboard. </summary>
            [EditorBrowsable(EditorBrowsableState.Never)]
            public Switchboard _Switchboard;

            /// <summary>   Gets or sets the Switchboard. </summary>
            /// <exception cref="ArgumentException">    Thrown when one or more arguments have unsupported or
            ///                                         illegal values. </exception>
            /// <value> The Switchboard. </value>
            public Switchboard Switchboard
            {
                [DebuggerHidden]
                get
                {
                    _Switchboard = Create__Instance__(_Switchboard);
                    return _Switchboard;
                }

                [DebuggerHidden]
                set
                {
                    if (ReferenceEquals(value, _Switchboard))
                        return;
                    if (value is object)
                        throw new ArgumentException("Property can only be set to Nothing");
                    Dispose__Instance__(ref _Switchboard);
                }
            }

        }
    }
}
