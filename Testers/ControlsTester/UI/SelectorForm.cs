using System;

namespace ControlsTester
{

    /// <summary>   Form for viewing the selector. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-03-06 </para>
    /// </remarks>
    public partial class SelectorForm
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        public SelectorForm()
        {
            this.InitializeComponent();
            this.__ReadOnlyCheckBox.Name = "_ReadOnlyCheckBox";
            this.__ApplyButton.Name = "_ApplyButton";
            this.__SelectorComboBox.Name = "_SelectorComboBox";
            this.__SelectButton.Name = "_SelectButton";
            this.__SelectorNumeric.Name = "_SelectorNumeric";
            this.__ToolStripSelectorComboBox.Name = "_ToolStripSelectorComboBox";
            this.__ToolStripSelectorNumeric.Name = "_ToolStripSelectorNumeric";
        }

        /// <summary>   Reads only check box checked changed. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void ReadOnlyCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            this._SelectorComboBox.ReadOnly = this._ReadOnlyCheckBox.Checked;
            this._SelectorNumeric.ReadOnly = this._ReadOnlyCheckBox.Checked;
            this._ToolStripSelectorComboBox.ReadOnly = this._ReadOnlyCheckBox.Checked;
            this._ToolStripSelectorNumeric.ReadOnly = this._ReadOnlyCheckBox.Checked;
        }

        /// <summary>   Applies the button click. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void ApplyButton_Click( object sender, EventArgs e )
        {
            this._SelectorComboBox.Text = this._EnterNumeric.Text;
            this._SelectorNumeric.Value = this._EnterNumeric.Value;
            this._ToolStripSelectorComboBox.Text = this._EnterNumeric.Text;
            this._ToolStripSelectorNumeric.Value = this._EnterNumeric.Value;
        }

        /// <summary>   Select button click. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void SelectButton_Click( object sender, EventArgs e )
        {
            this._SelectorComboBox.SelectValue();
            this._SelectorNumeric.SelectValue();
            this._ToolStripSelectorComboBox.SelectValue();
            this._ToolStripSelectorNumeric.SelectValue();
        }

        /// <summary>   Selector combo box value selected. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void SelectorComboBox_ValueSelected( object sender, EventArgs e )
        {
            this._SelectedTextBox.Text = this._SelectorComboBox.SelectedText;
        }

        /// <summary>   Selector numeric value selected. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void SelectorNumeric_ValueSelected( object sender, EventArgs e )
        {
            this._SelectedTextBox.Text = this._SelectorNumeric.SelectedText;
        }

        /// <summary>   Tool strip selector combo box value selected. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void ToolStripSelectorComboBox_ValueSelected( object sender, EventArgs e )
        {
            this._SelectedTextBox.Text = this._ToolStripSelectorComboBox.SelectedText;
        }

        /// <summary>   Tool strip selector numeric value selected. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void ToolStripSelectorNumeric_ValueSelected( object sender, EventArgs e )
        {
            this._SelectedTextBox.Text = this._ToolStripSelectorNumeric.SelectedText;
        }

    }
}
