using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

using ControlsTester.ExceptionExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace ControlsTester
{

    /// <summary>   Switches between test panels. </summary>
    /// <remarks>
    /// (c) 2003 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2014-04-05, . based on legacy code. </para>
    /// </remarks>
    public partial class Switchboard
    {

        /// <summary>   Initializes a new instance of this class. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        public Switchboard() : base()
        {

            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.__CancelButton.Name = "_CancelButton";
            this.__ExitButton.Name = "_ExitButton";
            this.__TestButton.Name = "_TestButton";
            this.__OpenButton.Name = "_OpenButton";
            this.__LogOnButton.Name = "_LogOnButton";

            // Add any initialization after the InitializeComponent() call

        }

        /// <summary>   Event handler. Called by form for load events. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void Form_Load( object sender, EventArgs e )
        {
            try
            {


                // Turn on the form hourglass cursor
                this.Cursor = Cursors.WaitCursor;
                this._MessagesTextBox.AddMessage( "Loading..." );

                // instantiate form objects
                this.PopulateTestPanelSelector();

                // set the form caption
                this.Text = Extensions.BuildDefaultCaption( "TESTER" );

                // center the form
                this.CenterToScreen();
            }
            catch
            {
                this._MessagesTextBox.AddMessage( "Exception..." );

                // Use throw without an argument in order to preserve the stack location 
                // where the exception was initially raised.
                throw;
            }
            finally
            {
                this._MessagesTextBox.AddMessage( "Loaded." );
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>   Closes the form and exits the application. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ExitButton_Click( object sender, EventArgs e )
        {
            try
            {
                this.Close();
            }
            catch ( Exception ex )
            {
                ex.Data.Add( "@isr", "Unhandled Exception." );
                _ = MessageBox.Show( ex.ToFullBlownString(), "Unhandled Exception", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly );
            }
        }

        /// <summary>   Cancel button click. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void CancelButton_Click( object sender, EventArgs e )
        {
            this.Close();
        }

        /// <summary>   Tests button click. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void TestButton_Click( object sender, EventArgs e )
        {
            if ( true )
            {
                System.IO.FileInfo fi = new( Application.ExecutablePath );
                Debug.Print( fi.FullName );
            }
        }

        private isr.Core.Controls.MachineLogOn _MachineLogOn;

        /// <summary>   Gets or sets the machine log on. </summary>
        /// <value> The machine log on. </value>
        private isr.Core.Controls.MachineLogOn MachineLogOn
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._MachineLogOn;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._MachineLogOn != null )
                {
                    this._MachineLogOn.PropertyChanged -= this.MachineLogOn_PropertyChanged;
                }

                this._MachineLogOn = value;
                if ( this._MachineLogOn != null )
                {
                    this._MachineLogOn.PropertyChanged += this.MachineLogOn_PropertyChanged;
                }
            }
        }

        /// <summary>   Event handler. Called by MachineLogOn for property changed events. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Property changed event information. </param>
        private void MachineLogOn_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InvokeRequired )
            {
                _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.MachineLogOn_PropertyChanged ), new object[] { sender, e } );
            }

            isr.Core.Controls.MachineLogOn logon = ( isr.Core.Controls.MachineLogOn ) sender;
            if ( logon is object )
            {
                switch ( e.PropertyName ?? "" )
                {
                    case "UserName":
                        {
                            this._MessagesTextBox.AddMessage( $"User name: {logon.UserName}" );
                            break;
                        }

                    case "UserRoles":
                        {
                            string role = string.Empty;
                            foreach ( string s in logon.UserRoles )
                                role = role + "," + s;
                            this._MessagesTextBox.AddMessage( $"User role: {role}" );
                            break;
                        }

                    case "IsAuthenticated":
                        {
                            if ( logon.IsAuthenticated )
                            {
                                this._MessagesTextBox.AddMessage( $"User {logon.UserName} authenticated" );
                            }
                            else
                            {
                                this._MessagesTextBox.AddMessage( "User not authenticated" );
                            }

                            if ( this._Popup is object )
                            {
                                // _popup.Hide()
                            }

                            break;
                        }
                }
            }
        }

        /// <summary>   The popup. </summary>
        private ToolStripDropDown _Popup;

        /// <summary>   Logs on button click. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void LogOnButton_Click( object sender, EventArgs e )
        {
            this.MachineLogOn = new isr.Core.Controls.MachineLogOn();
            if ( true )
            {
                isr.Core.Controls.LogOnControl content = new() { UserLogOn = MachineLogOn };
                content.ShowPopup( this, Point.Add( this._LogOnButton.Parent.Location, new Size( this._LogOnButton.Location.X, this._LogOnButton.Location.Y ) ) );
            }
            else
            {
#pragma warning disable CS0162 // Unreachable code detected
                // logon.Validate("admin", New System.Collections.ObjectModel.ReadOnlyCollection(Of String)(roles))
                List<string> roles = new() { "Administrators" };
                isr.Core.Controls.LogOnControl content = new() { UserLogOn = MachineLogOn, Enabled = true };
                ToolStripControlHost host = new( content ) { Enabled = true, Margin = Padding.Empty, Padding = Padding.Empty };
                this._Popup = new ToolStripDropDown() { Margin = Padding.Empty, Padding = Padding.Empty };
                _ = this._Popup.Items.Add( host );
                this._Popup.Show( this, Point.Add( this._LogOnButton.Parent.Location, new Size( this._LogOnButton.Location.X, this._LogOnButton.Location.Y ) ) );
#pragma warning restore CS0162 // Unreachable code detected
            }
        }

        /// <summary>   Descriptive Enumeration for test forms. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        private enum TestPanel
        {

            /// <summary> An enum constant representing the console control option. </summary>
            [Description( "Console Control" )]
            ConsoleControl,

            /// <summary> An enum constant representing the drop down text box form option. </summary>
            [Description( "Drop Down text Box Control Form" )]
            DropDownTextBoxForm,

            /// <summary> An enum constant representing the engineering up down control form option. </summary>
            [Description( "Engineering Up Down Control Form" )]
            EngineeringUpDownControlForm,

            /// <summary> An enum constant representing the nullable up down control form option. </summary>
            [Description( "Nullable Up Down Control Form" )]
            NullableUpDownControlForm,

            /// <summary> An enum constant representing the rich text box form option. </summary>
            [Description( "Rich Text Box Form" )]
            RichTextBoxForm,

            /// <summary> An enum constant representing the rounded corners controls option. </summary>
            [Description( "Rounded Corners Controls Form" )]
            RoundedCornersControls,

            /// <summary> An enum constant representing the selector form option. </summary>
            [Description( "Selector Form" )]
            SelectorForm,

            /// <summary> An enum constant representing the wizard option. </summary>
            [Description( "Wizard Form" )]
            Wizard,

            /// <summary> An enum constant representing the gnu plot option. </summary>
            [Description( "Gnu Plot Form" )]
            GnuPlot,

            /// <summary> An enum constant representing the folder file explorer option. </summary>
            [Description( "Folder and File Explorer" )]
            FolderFileExplorer,

            /// <summary> An enum constant representing the shell drag drop option. </summary>
            [Description( "Shell Drag and Drop" )]
            ShellDragDrop,

            /// <summary> An enum constant representing the shell explorer option. </summary>
            [Description( "Shell Explorer" )]
            ShellExplorer,

            /// <summary> An enum constant representing the visual styles option. </summary>
            [Description( "Visual Styles" )]
            VisualStyles
        }

        /// <summary>   Open selected items. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void OpenButton_Click( object sender, EventArgs e )
        {
            try
            {
                switch ( ( TestPanel ) Conversions.ToInteger( (( KeyValuePair<Enum, string> ) this._ActionsComboBox.SelectedItem).Key ) )
                {
                    case TestPanel.ConsoleControl:
                        {
                            using ConsoleControlForm myForm = new();
                            _ = myForm.ShowDialog();

                            break;
                        }

                    case TestPanel.EngineeringUpDownControlForm:
                        {
                            using EngineeringUpDownForm myForm = new();
                            _ = myForm.ShowDialog();

                            break;
                        }

                    case TestPanel.NullableUpDownControlForm:
                        {
                            using NullableNumericUpDownForm myForm = new();
                            _ = myForm.ShowDialog();

                            break;
                        }

                    case TestPanel.DropDownTextBoxForm:
                        {
                            using DropDownTextBoxForm myForm = new();
                            _ = myForm.ShowDialog();

                            break;
                        }

                    case TestPanel.RichTextBoxForm:
                        {
                            using isr.Core.Controls.RichTextEditControl control = new();
                            using Font Font = new( "Lucida Console", 9f );
                            control.Font = Font;
                            using isr.Core.Forma.ConsoleForm form = isr.Core.Controls.RichTextEditControl.CreateForm( "Editor", "Edit", control, isr.Core.My.MyLibrary.Logger );
                            form.ShowDialog( null );

                            break;
                        }

                    case TestPanel.RoundedCornersControls:
                        {
                            using RoundedControlsTester myform = new();
                            _ = myform.ShowDialog( null );

                            break;
                        }

                    case TestPanel.SelectorForm:
                        {
                            using SelectorForm myform = new();
                            _ = myform.ShowDialog( null );

                            break;
                        }

                    case TestPanel.Wizard:
                        {
                            using SimpleWizard myform = new();
                            _ = myform.ShowDialog( null );

                            break;
                        }

                    case TestPanel.FolderFileExplorer:
                        {
                            using isr.Core.Forma.ConsoleForm myform = new();
                            using isr.Core.Controls.FileExplorerView ctrl = new();
                            // ctrl.Refresh()
                            ctrl.SelectHome();
                            myform.AddPropertyNotifyControl( ctrl );
                            myform.ShowDialog( null );

                            break;
                        }

                    case TestPanel.ShellDragDrop:
                        {
                            using isr.Core.Forma.ConsoleForm myform = new();
                            using isr.Core.Shell.Forms.DragDropControl ctrl = new();
                            myform.AddUserControl( ctrl );
                            myform.ShowDialog( null );

                            break;
                        }

                    case TestPanel.ShellExplorer:
                        {
                            using isr.Core.Forma.ConsoleForm myform = new();
                            using isr.Core.Shell.Forms.ExplorerControl ctrl = new();
                            myform.AddUserControl( ctrl );
                            myform.ShowDialog( null );

                            break;
                        }

                    case TestPanel.VisualStyles:
                        {
                            using VisualStyleElementViewer myform = new();
                            _ = myform.ShowDialog( null );

                            break;
                        }
                }
            }
            catch ( Exception ex )
            {
                ex.Data.Add( "@isr", "Unhandled Exception." );
                _ = MessageBox.Show( ex.ToFullBlownString(), "Unhandled Exception", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly );
            }
            finally
            {
            }
        }

        /// <summary>   Populates the list of test panels. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        private void PopulateTestPanelSelector()
        {
            this._ActionsComboBox.DataSource = null;
            this._ActionsComboBox.Items.Clear();
            this._ActionsComboBox.DataSource = typeof( TestPanel ).ValueDescriptionPairs();
            this._ActionsComboBox.DisplayMember = nameof( KeyValuePair<Enum, string>.Value );
            this._ActionsComboBox.ValueMember = nameof( KeyValuePair<Enum, string>.Key );
        }

    }

    /// <summary>   An extensions. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    public static class Extensions
    {

        /// <summary>   Adds a message to 'message'. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="box">      The box control. </param>
        /// <param name="message">  The message. </param>
        public static void AddMessage( this TextBox box, string message )
        {
            if ( box is object )
            {
                box.SelectionStart = box.Text.Length;
                box.SelectionLength = 0;
                box.SelectedText = message + Environment.NewLine;
            }
        }

        /// <summary>
        /// Gets the <see cref="DescriptionAttribute"/> of an <see cref="System.Enum"/> type value.
        /// </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="value">    The <see cref="System.Enum"/> type value. </param>
        /// <returns>   A string containing the text of the <see cref="DescriptionAttribute"/>. </returns>
        public static string Description( this Enum value )
        {
            if ( value is null )
                return string.Empty;
            string candidate = value.ToString();
            System.Reflection.FieldInfo fieldInfo = value.GetType().GetField( candidate );
            DescriptionAttribute[] attributes = ( DescriptionAttribute[] ) fieldInfo.GetCustomAttributes( typeof( DescriptionAttribute ), false );
            if ( attributes is object && attributes.Length > 0 )
            {
                candidate = attributes[0].Description;
            }

            return candidate;
        }

        /// <summary>   Gets a Key Value Pair description item. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="value">    The <see cref="System.Enum"/> type value. </param>
        /// <returns>   A list of. </returns>
        public static KeyValuePair<Enum, string> ValueDescriptionPair( this Enum value )
        {
            return new KeyValuePair<Enum, string>( value, value.Description() );
        }

        /// <summary>   Value description pairs. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="type"> The type. </param>
        /// <returns>   An IList. </returns>
        public static IList ValueDescriptionPairs( this Type type )
        {
            ArrayList keyValuePairs = new();
            foreach ( Enum value in Enum.GetValues( type ) )
                _ = keyValuePairs.Add( value.ValueDescriptionPair() );
            return keyValuePairs;
        }

        /// <summary>   Builds the default caption. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="subtitle"> The subtitle. </param>
        /// <returns>   System.String. </returns>
        public static string BuildDefaultCaption( string subtitle )
        {
            System.Text.StringBuilder builder = new();
            _ = builder.Append( My.MyProject.Application.Info.Title );
            _ = builder.Append( " " );
            _ = builder.Append( My.MyProject.Application.Info.Version.ToString() );
            if ( My.MyProject.Application.Info.Version.Major < 1 )
            {
                _ = builder.Append( "." );
                switch ( My.MyProject.Application.Info.Version.Minor )
                {
                    case 0:
                        {
                            _ = builder.Append( "Alpha" );
                            break;
                        }

                    case 1:
                        {
                            _ = builder.Append( "Beta" );
                            break;
                        }

                    case var @case when 2 <= @case && @case <= 8:
                        {
                            _ = builder.Append( $"RC{My.MyProject.Application.Info.Version.Minor - 1}" );
                            break;
                        }

                    default:
                        {
                            _ = builder.Append( "Gold" );
                            break;
                        }
                }
            }

            if ( !string.IsNullOrWhiteSpace( subtitle ) )
            {
                _ = builder.Append( ": " );
                _ = builder.Append( subtitle );
            }

            return builder.ToString();
        }
    }
}
