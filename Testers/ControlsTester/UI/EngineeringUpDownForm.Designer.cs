using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace ControlsTester
{
    [DesignerGenerated()]
    public partial class EngineeringUpDownForm : Form
    {

        /// <summary>
        /// Disposes of the resources (other than memory) used by the
        /// <see cref="T:System.Windows.Forms.Form" />.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="disposing">    <see langword="true" /> to release both managed and unmanaged
        ///                             resources; <see langword="false" /> to release only unmanaged
        ///                             resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components is object)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _DecimalPlacesNumericLabel = new Label();
            __DecimalPlacesNumeric = new NumericUpDown();
            __DecimalPlacesNumeric.ValueChanged += new EventHandler(DecimalPlacesNumeric_ValueChanged);
            __EngineeringScaleComboBox = new ComboBox();
            __EngineeringScaleComboBox.SelectionChangeCommitted += new EventHandler(EngineeringScaleComboBox_SelectionChangeCommitted);
            _EngineeringScaleComboBoxLabel = new Label();
            _UnitTextBoxLabel = new Label();
            __UnitTextBox = new TextBox();
            __UnitTextBox.TextChanged += new EventHandler(UnitTextBox_TextChanged);
            _ToolTip = new ToolTip(components);
            _ScaledValueTextBox = new TextBox();
            __EnterValueNumeric = new isr.Core.Controls.NumericUpDown();
            __EnterValueNumeric.ValueChanged += new EventHandler(EnterValueNumeric_ValueChanged);
            __ReadOnlyCheckBox = new CheckBox();
            __ReadOnlyCheckBox.CheckedChanged += new EventHandler(ReadOnlyCheckBox_CheckedChanged);
            __EditedEngineeringUpDown = new isr.Core.Controls.EngineeringUpDown();
            __EditedEngineeringUpDown.ValueChanged += new EventHandler(EditedEngineeringUpDown_ValueChanged);
            _EnterValueNumericLabel = new Label();
            _EditedEngineeringUpDownLabel = new Label();
            _ScaledValueTextBoxLabel = new Label();
            _NumericUpDown1 = new isr.Core.Controls.NumericUpDown();
            _NumericUpDown1.ValueChanged += new EventHandler(EnterValueNumeric_ValueChanged);
            ((System.ComponentModel.ISupportInitialize)__DecimalPlacesNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)__EnterValueNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)__EditedEngineeringUpDown).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_NumericUpDown1).BeginInit();
            SuspendLayout();
            // 
            // _DecimalPlacesNumericLabel
            // 
            _DecimalPlacesNumericLabel.AutoSize = true;
            _DecimalPlacesNumericLabel.Location = new Point(48, 191);
            _DecimalPlacesNumericLabel.Name = "_DecimalPlacesNumericLabel";
            _DecimalPlacesNumericLabel.Size = new Size(97, 17);
            _DecimalPlacesNumericLabel.TabIndex = 1;
            _DecimalPlacesNumericLabel.Text = "Decimal Places:";
            _DecimalPlacesNumericLabel.TextAlign = ContentAlignment.TopRight;
            // 
            // _DecimalPlacesNumeric
            // 
            __DecimalPlacesNumeric.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            __DecimalPlacesNumeric.Location = new Point(146, 188);
            __DecimalPlacesNumeric.Maximum = new decimal(new int[] { 6, 0, 0, 0 });
            __DecimalPlacesNumeric.Name = "__DecimalPlacesNumeric";
            __DecimalPlacesNumeric.Size = new Size(58, 25);
            __DecimalPlacesNumeric.TabIndex = 2;
            _ToolTip.SetToolTip(__DecimalPlacesNumeric, "Enter decimal places");
            // 
            // _EngineeringScaleComboBox
            // 
            __EngineeringScaleComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            __EngineeringScaleComboBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            __EngineeringScaleComboBox.FormattingEnabled = true;
            __EngineeringScaleComboBox.Location = new Point(147, 150);
            __EngineeringScaleComboBox.Name = "__EngineeringScaleComboBox";
            __EngineeringScaleComboBox.Size = new Size(121, 25);
            __EngineeringScaleComboBox.TabIndex = 3;
            _ToolTip.SetToolTip(__EngineeringScaleComboBox, "Select engineering scale");
            // 
            // _EngineeringScaleComboBoxLabel
            // 
            _EngineeringScaleComboBoxLabel.AutoSize = true;
            _EngineeringScaleComboBoxLabel.Location = new Point(31, 153);
            _EngineeringScaleComboBoxLabel.Name = "_EngineeringScaleComboBoxLabel";
            _EngineeringScaleComboBoxLabel.Size = new Size(114, 17);
            _EngineeringScaleComboBoxLabel.TabIndex = 1;
            _EngineeringScaleComboBoxLabel.Text = "Engineering Scale:";
            _EngineeringScaleComboBoxLabel.TextAlign = ContentAlignment.TopRight;
            // 
            // _UnitTextBoxLabel
            // 
            _UnitTextBoxLabel.AutoSize = true;
            _UnitTextBoxLabel.Location = new Point(111, 118);
            _UnitTextBoxLabel.Name = "_UnitTextBoxLabel";
            _UnitTextBoxLabel.Size = new Size(34, 17);
            _UnitTextBoxLabel.TabIndex = 1;
            _UnitTextBoxLabel.Text = "Unit:";
            _UnitTextBoxLabel.TextAlign = ContentAlignment.TopRight;
            // 
            // _UnitTextBox
            // 
            __UnitTextBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            __UnitTextBox.Location = new Point(147, 114);
            __UnitTextBox.Name = "__UnitTextBox";
            __UnitTextBox.Size = new Size(58, 25);
            __UnitTextBox.TabIndex = 5;
            _ToolTip.SetToolTip(__UnitTextBox, "Enter the unit, e.g., A, V, Ohm.");
            // 
            // _ScaledValueTextBox
            // 
            _ScaledValueTextBox.Location = new Point(147, 73);
            _ScaledValueTextBox.Name = "_ScaledValueTextBox";
            _ScaledValueTextBox.Size = new Size(121, 25);
            _ScaledValueTextBox.TabIndex = 4;
            _ToolTip.SetToolTip(_ScaledValueTextBox, "Scaled value");
            // 
            // _EnterValueNumeric
            // 
            __EnterValueNumeric.Location = new Point(148, 12);
            __EnterValueNumeric.Maximum = new decimal(new int[] { 2000000000, 0, 0, 0 });
            __EnterValueNumeric.Minimum = new decimal(new int[] { 2000000000, 0, 0, (int)-2147483648L });
            __EnterValueNumeric.Name = "__EnterValueNumeric";
            __EnterValueNumeric.ReadOnlyBackColor = Color.Empty;
            __EnterValueNumeric.ReadOnlyForeColor = Color.Empty;
            __EnterValueNumeric.ReadWriteBackColor = Color.Empty;
            __EnterValueNumeric.ReadWriteForeColor = Color.Empty;
            __EnterValueNumeric.Size = new Size(120, 25);
            __EnterValueNumeric.TabIndex = 9;
            _ToolTip.SetToolTip(__EnterValueNumeric, "Set this value");
            // 
            // _ReadOnlyCheckBox
            // 
            __ReadOnlyCheckBox.AutoSize = true;
            __ReadOnlyCheckBox.Location = new Point(146, 228);
            __ReadOnlyCheckBox.Name = "__ReadOnlyCheckBox";
            __ReadOnlyCheckBox.Size = new Size(87, 21);
            __ReadOnlyCheckBox.TabIndex = 8;
            __ReadOnlyCheckBox.Text = "Read Only";
            __ReadOnlyCheckBox.UseVisualStyleBackColor = true;
            // 
            // _EditedEngineeringUpDown
            // 
            __EditedEngineeringUpDown.ForeColor = Color.Black;
            __EditedEngineeringUpDown.Location = new Point(16, 73);
            __EditedEngineeringUpDown.Maximum = new decimal(new int[] { 1000, 0, 0, 0 });
            __EditedEngineeringUpDown.Minimum = new decimal(new int[] { 1000, 0, 0, (int)-2147483648L });
            __EditedEngineeringUpDown.Name = "__EditedEngineeringUpDown";
            __EditedEngineeringUpDown.ReadOnlyBackColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(224)), Conversions.ToInteger(Conversions.ToByte(224)), Conversions.ToInteger(Conversions.ToByte(224)));
            __EditedEngineeringUpDown.ReadOnlyForeColor = Color.Black;
            __EditedEngineeringUpDown.ReadWriteBackColor = Color.Empty;
            __EditedEngineeringUpDown.ReadWriteForeColor = Color.Black;
            __EditedEngineeringUpDown.ScaledValue = new decimal(new int[] { 0, 0, 0, 0 });
            __EditedEngineeringUpDown.Size = new Size(120, 25);
            __EditedEngineeringUpDown.TabIndex = 6;
            __EditedEngineeringUpDown.UnscaledValue = new decimal(new int[] { 0, 0, 0, 0 });
            __EditedEngineeringUpDown.UpDownCursor = Cursors.Hand;
            __EditedEngineeringUpDown.UpDownDisplayMode = isr.Core.Controls.UpDownButtonsDisplayMode.WhenMouseOver;
            // 
            // _EnterValueNumericLabel
            // 
            _EnterValueNumericLabel.AutoSize = true;
            _EnterValueNumericLabel.Location = new Point(10, 16);
            _EnterValueNumericLabel.Name = "_EnterValueNumericLabel";
            _EnterValueNumericLabel.Size = new Size(112, 17);
            _EnterValueNumericLabel.TabIndex = 1;
            _EnterValueNumericLabel.Text = "Enter value to set:";
            _EnterValueNumericLabel.TextAlign = ContentAlignment.TopRight;
            // 
            // _EditedEngineeringUpDownLabel
            // 
            _EditedEngineeringUpDownLabel.AutoSize = true;
            _EditedEngineeringUpDownLabel.Location = new Point(13, 53);
            _EditedEngineeringUpDownLabel.Name = "_EditedEngineeringUpDownLabel";
            _EditedEngineeringUpDownLabel.Size = new Size(69, 17);
            _EditedEngineeringUpDownLabel.TabIndex = 1;
            _EditedEngineeringUpDownLabel.Text = "Edit Value:";
            _EditedEngineeringUpDownLabel.TextAlign = ContentAlignment.TopRight;
            // 
            // _ScaledValueTextBoxLabel
            // 
            _ScaledValueTextBoxLabel.AutoSize = true;
            _ScaledValueTextBoxLabel.Location = new Point(145, 53);
            _ScaledValueTextBoxLabel.Name = "_ScaledValueTextBoxLabel";
            _ScaledValueTextBoxLabel.Size = new Size(104, 17);
            _ScaledValueTextBoxLabel.TabIndex = 1;
            _ScaledValueTextBoxLabel.Text = "Observed Value:";
            _ScaledValueTextBoxLabel.TextAlign = ContentAlignment.TopRight;
            // 
            // NumericUpDown1
            // 
            _NumericUpDown1.Location = new Point(12, 116);
            _NumericUpDown1.Maximum = new decimal(new int[] { 2000000000, 0, 0, 0 });
            _NumericUpDown1.Minimum = new decimal(new int[] { 2000000000, 0, 0, (int)-2147483648L });
            _NumericUpDown1.Name = "_NumericUpDown1";
            _NumericUpDown1.ReadOnlyBackColor = Color.Empty;
            _NumericUpDown1.ReadOnlyForeColor = Color.Empty;
            _NumericUpDown1.ReadWriteBackColor = Color.Empty;
            _NumericUpDown1.ReadWriteForeColor = Color.Empty;
            _NumericUpDown1.Size = new Size(80, 25);
            _NumericUpDown1.TabIndex = 9;
            // 
            // EngineeringUpDownForm
            // 
            AutoScaleDimensions = new SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(291, 257);
            Controls.Add(_NumericUpDown1);
            Controls.Add(__EnterValueNumeric);
            Controls.Add(__ReadOnlyCheckBox);
            Controls.Add(__EditedEngineeringUpDown);
            Controls.Add(_ScaledValueTextBox);
            Controls.Add(_ScaledValueTextBoxLabel);
            Controls.Add(_EditedEngineeringUpDownLabel);
            Controls.Add(_EnterValueNumericLabel);
            Controls.Add(_EngineeringScaleComboBoxLabel);
            Controls.Add(__EngineeringScaleComboBox);
            Controls.Add(_DecimalPlacesNumericLabel);
            Controls.Add(__DecimalPlacesNumeric);
            Controls.Add(_UnitTextBoxLabel);
            Controls.Add(__UnitTextBox);
            Font = new Font("Segoe UI", 9.75f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            FormBorderStyle = FormBorderStyle.FixedDialog;
            Margin = new Padding(3, 4, 3, 4);
            Name = "EngineeringUpDownForm";
            Text = "Engineering Up Down Form";
            ((System.ComponentModel.ISupportInitialize)__DecimalPlacesNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)__EnterValueNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)__EditedEngineeringUpDown).EndInit();
            ((System.ComponentModel.ISupportInitialize)_NumericUpDown1).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        private Label _DecimalPlacesNumericLabel;
        private NumericUpDown __DecimalPlacesNumeric;

        private NumericUpDown _DecimalPlacesNumeric
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __DecimalPlacesNumeric;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__DecimalPlacesNumeric != null)
                {
                    __DecimalPlacesNumeric.ValueChanged -= DecimalPlacesNumeric_ValueChanged;
                }

                __DecimalPlacesNumeric = value;
                if (__DecimalPlacesNumeric != null)
                {
                    __DecimalPlacesNumeric.ValueChanged += DecimalPlacesNumeric_ValueChanged;
                }
            }
        }

        private ComboBox __EngineeringScaleComboBox;

        private ComboBox _EngineeringScaleComboBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __EngineeringScaleComboBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__EngineeringScaleComboBox != null)
                {
                    __EngineeringScaleComboBox.SelectionChangeCommitted -= EngineeringScaleComboBox_SelectionChangeCommitted;
                }

                __EngineeringScaleComboBox = value;
                if (__EngineeringScaleComboBox != null)
                {
                    __EngineeringScaleComboBox.SelectionChangeCommitted += EngineeringScaleComboBox_SelectionChangeCommitted;
                }
            }
        }

        private Label _EngineeringScaleComboBoxLabel;
        private ToolTip _ToolTip;
        private Label _UnitTextBoxLabel;
        private TextBox __UnitTextBox;

        private TextBox _UnitTextBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __UnitTextBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__UnitTextBox != null)
                {
                    __UnitTextBox.TextChanged -= UnitTextBox_TextChanged;
                }

                __UnitTextBox = value;
                if (__UnitTextBox != null)
                {
                    __UnitTextBox.TextChanged += UnitTextBox_TextChanged;
                }
            }
        }

        private isr.Core.Controls.EngineeringUpDown __EditedEngineeringUpDown;

        private isr.Core.Controls.EngineeringUpDown _EditedEngineeringUpDown
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __EditedEngineeringUpDown;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__EditedEngineeringUpDown != null)
                {
                    __EditedEngineeringUpDown.ValueChanged -= EditedEngineeringUpDown_ValueChanged;
                }

                __EditedEngineeringUpDown = value;
                if (__EditedEngineeringUpDown != null)
                {
                    __EditedEngineeringUpDown.ValueChanged += EditedEngineeringUpDown_ValueChanged;
                }
            }
        }

        private TextBox _ScaledValueTextBox;
        private CheckBox __ReadOnlyCheckBox;

        private CheckBox _ReadOnlyCheckBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadOnlyCheckBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadOnlyCheckBox != null)
                {
                    __ReadOnlyCheckBox.CheckedChanged -= ReadOnlyCheckBox_CheckedChanged;
                }

                __ReadOnlyCheckBox = value;
                if (__ReadOnlyCheckBox != null)
                {
                    __ReadOnlyCheckBox.CheckedChanged += ReadOnlyCheckBox_CheckedChanged;
                }
            }
        }

        private isr.Core.Controls.NumericUpDown __EnterValueNumeric;

        private isr.Core.Controls.NumericUpDown _EnterValueNumeric
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __EnterValueNumeric;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__EnterValueNumeric != null)
                {
                    __EnterValueNumeric.ValueChanged -= EnterValueNumeric_ValueChanged;
                }

                __EnterValueNumeric = value;
                if (__EnterValueNumeric != null)
                {
                    __EnterValueNumeric.ValueChanged += EnterValueNumeric_ValueChanged;
                }
            }
        }

        private Label _EnterValueNumericLabel;
        private Label _EditedEngineeringUpDownLabel;
        private Label _ScaledValueTextBoxLabel;
        private isr.Core.Controls.NumericUpDown _NumericUpDown1;

        private isr.Core.Controls.NumericUpDown NumericUpDown1
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _NumericUpDown1;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_NumericUpDown1 != null)
                {
                    _NumericUpDown1.ValueChanged -= EnterValueNumeric_ValueChanged;
                }

                _NumericUpDown1 = value;
                if (_NumericUpDown1 != null)
                {
                    _NumericUpDown1.ValueChanged += EnterValueNumeric_ValueChanged;
                }
            }
        }
    }
}
