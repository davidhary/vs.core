using System;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace ControlsTester
{

    /// <summary>   The application's main form. </summary>
    /// <remarks>
    /// (c) 2012 icemanind. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-07-09 </para><para>
    /// http://www.codeproject.com/Articles/1053951/Console-Control </para>
    /// </remarks>
    public partial class ConsoleControlForm : Form
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Windows.Forms.Form" /> class.
        /// </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        public ConsoleControlForm()
        {

            base.Load += this.Form1_Load;

            base.Paint += this.Form1_Paint;
            this.InitializeComponent();
            string codeBase = System.Reflection.Assembly.GetExecutingAssembly().CodeBase;
            UriBuilder uri = new( codeBase );
            this._CurrentDirectory = System.IO.Path.GetDirectoryName( Uri.UnescapeDataString( uri.Path ) );
        }

        /// <summary>   Clean up any resources being used. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="disposing">    true if managed resources should be disposed; otherwise, false. </param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && this.components is object )
            {
                this.components.Dispose();
            }

            base.Dispose( disposing );
        }

        /// <summary>   A safe native methods. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        private sealed class SafeNativeMethods
        {

            /// <summary>   Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
            /// <remarks>   David, 2020-10-25. </remarks>
            private SafeNativeMethods() : base()
            {
            }

            /// <summary>   Gets the focus. </summary>
            /// <remarks>   David, 2020-10-25. </remarks>
            /// <returns>   The focus. </returns>
            [DllImport( "user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.Winapi )]
            internal static extern IntPtr GetFocus();
        }

        /// <summary>   Pathname of the current directory. </summary>
        private string _CurrentDirectory;

        /// <summary>   Event handler. Called by Form1 for load events. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void Form1_Load( object sender, EventArgs e )
        {
            this._LeftConsoleControl.SetCursorPosition( 0, 0 );
            this._LeftConsoleControl.Write( "Hello World!" );
            this._LeftConsoleControl.SetCursorPosition( 2, 15 );
            this._LeftConsoleControl.Write( "Look at this message!", Color.Yellow, Color.Blue );
            this._CursorColumnTextBox.Text = this._LeftConsoleControl.GetCursorPosition().Column.ToString();
            this._CursorRowTextBox.Text = this._LeftConsoleControl.GetCursorPosition().Row.ToString();
            this._MessageTextBox.Text = "Hello World!";
            this._BackgroundColorPanel.BackColor = this._LeftConsoleControl.CurrentBackgroundColor;
            this._ForegroundColorPanel.BackColor = this._LeftConsoleControl.CurrentForegroundColor;
            this._LeftConsoleControl.AllowInput = false;
            this._RightConsoleControl.SetCursorPosition( 0, 0 );
            this._RightConsoleControl.Write( "Welcome to Iceman Shell!" );
            this._RightConsoleControl.SetCursorPosition( 1, 0 );
            this._RightConsoleControl.Write( $"The current date and time is {DateTimeOffset.Now:F}" );
            this._RightConsoleControl.Write();
            this._RightConsoleControl.Write( "Use the command 'help' if you need help!" );
            this._RightConsoleControl.SetCursorPosition( this._RightConsoleControl.GetCursorPosition().Row + 2, 0 );
            this.ShowPrompt();
            this._RightConsoleControl.LineEntered += this.ConsoleControl2LineEntered;
            this.ActiveControl = this._RightConsoleControl;
        }

        /// <summary>   Shows the prompt. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        private void ShowPrompt()
        {
            this._RightConsoleControl.Write();
            this._RightConsoleControl.Write( "# " );
        }

        /// <summary>   Console control 2 line entered. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Line entered event information. </param>
        private void ConsoleControl2LineEntered( object sender, isr.Core.Controls.LineEnteredEventArgs e )
        {
            if ( e is null )
                return;
            string line = e.Line;
            line = line.Trim();
            int ndx = line.IndexOf( ' ' );
            string cmd = ndx < 0 ? line : line.Substring( 0, ndx );
            string parameters = ndx < 0 ? "" : line.Remove( 0, ndx + 1 );
            switch ( cmd.ToLower() ?? "" )
            {
                case var @case when string.IsNullOrWhiteSpace( @case ):
                    {
                        this.ShowPrompt();
                        break;
                    }

                case "help":
                    {
                        this.ShowHelp();
                        this.ShowPrompt();
                        break;
                    }

                case "exit":
                    {
                        Application.Exit();
                        break;
                    }

                case "ls":
                    {
                        this.DoLsCommand( parameters );
                        break;
                    }

                case "echo":
                    {
                        this.DoEchoCommand( parameters );
                        this.ShowPrompt();
                        break;
                    }

                case "date":
                    {
                        this.DoDateCommand();
                        this.ShowPrompt();
                        break;
                    }

                case "pwd":
                    {
                        this.DoPwdCommand();
                        this.ShowPrompt();
                        break;
                    }

                case "cd":
                    {
                        this.DoCdCommand( parameters );
                        this.ShowPrompt();
                        break;
                    }

                default:
                    {
                        this._RightConsoleControl.Write( "> Unknown Command!" );
                        this._RightConsoleControl.Write();
                        this.ShowPrompt();
                        break;
                    }
            }
        }

        /// <summary>   Button clear console click. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void BtnClearConsole_Click( object sender, EventArgs e )
        {
            this._LeftConsoleControl.Clear();
            this._CursorColumnTextBox.Text = this._LeftConsoleControl.GetCursorPosition().Column.ToString();
            this._CursorRowTextBox.Text = this._LeftConsoleControl.GetCursorPosition().Row.ToString();
        }

        /// <summary>   Button move cursor click. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void BtnMoveCursor_Click( object sender, EventArgs e )
        {
            int row = int.Parse( this._CursorRowTextBox.Text );
            int column = int.Parse( this._CursorColumnTextBox.Text );
            if ( row < 0 || row > 24 )
            {
                _ = MessageBox.Show( "Cursor Row Must be between 0 and 24!" );
                return;
            }

            if ( column < 0 || column > 79 )
            {
                _ = MessageBox.Show( "Cursor Column Must be between 0 and 79!" );
                return;
            }

            this._LeftConsoleControl.SetCursorPosition( row, column );
        }

        /// <summary>   Rb underline checked changed. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void RbUnderline_CheckedChanged( object sender, EventArgs e )
        {
            this.ChangeCursorType();
        }

        /// <summary>   Rb block checked changed. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void RbBlock_CheckedChanged( object sender, EventArgs e )
        {
            this.ChangeCursorType();
        }

        /// <summary>   Rb none checked changed. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void RbNone_CheckedChanged( object sender, EventArgs e )
        {
            this.ChangeCursorType();
        }

        /// <summary>   Change cursor type. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        private void ChangeCursorType()
        {
            if ( this._UnderlineRadioButton.Checked )
            {
                this._LeftConsoleControl.CursorType = isr.Core.Controls.CursorType.Underline;
            }

            if ( this._BlockRadioButton.Checked )
            {
                this._LeftConsoleControl.CursorType = isr.Core.Controls.CursorType.Block;
            }

            if ( this._NoneRadioButton.Checked )
            {
                this._LeftConsoleControl.CursorType = isr.Core.Controls.CursorType.Invisible;
            }
        }

        /// <summary>   Button write message click. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void BtnWriteMessage_Click( object sender, EventArgs e )
        {
            this._LeftConsoleControl.Write( this._MessageTextBox.Text );
        }

        /// <summary>   Pnl background color double click. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void PnlBackgroundColor_DoubleClick( object sender, EventArgs e )
        {
            this._ColorDialog.Color = this._LeftConsoleControl.CurrentBackgroundColor;
            DialogResult dr = this._ColorDialog.ShowDialog();
            if ( dr == DialogResult.OK )
            {
                this._BackgroundColorPanel.BackColor = this._ColorDialog.Color;
                this._LeftConsoleControl.CurrentBackgroundColor = this._ColorDialog.Color;
            }
        }

        /// <summary>   Pnl foreground color double click. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void PnlForegroundColor_DoubleClick( object sender, EventArgs e )
        {
            this._ColorDialog.Color = this._LeftConsoleControl.CurrentForegroundColor;
            DialogResult dr = this._ColorDialog.ShowDialog();
            if ( dr == DialogResult.OK )
            {
                this._ForegroundColorPanel.BackColor = this._ColorDialog.Color;
                this._LeftConsoleControl.CurrentForegroundColor = this._ColorDialog.Color;
            }
        }

        /// <summary>   Searches for the first focused control. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <returns>   The found focused control. </returns>
        private Control FindFocusedControl()
        {
            Control focusedControl = null;
            IntPtr focusedHandle = SafeNativeMethods.GetFocus();
            if ( focusedHandle != IntPtr.Zero )
            {
                focusedControl = FromHandle( focusedHandle );
            }

            return focusedControl;
        }

        /// <summary>   Event handler. Called by Form1 for paint events. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Paint event information. </param>
        private void Form1_Paint( object sender, PaintEventArgs e )
        {
            const int offsetX = 2;
            Bitmap b = new( this.ClientSize.Width, this.ClientSize.Height );
            Graphics g = Graphics.FromImage( b );
            Control c1 = this.FindFocusedControl();
            if ( c1 is object && c1.Name.StartsWith( "consoleControl" ) )
            {
                for ( int i = 1; i <= 5; i++ )
                {
                    Pen p = new( Color.FromArgb( Math.Min( 25 * i * 2, 255 ), 50, 80, 255 ) );
                    g.DrawRectangle( p, c1.Location.X - i, c1.Location.Y - i, c1.Size.Width + i + i, c1.Size.Height + i + i );
                }
            }

            g.DrawLine( new Pen( Color.FromArgb( 200, 200, 200 ) ), 660 + offsetX, 3, 660 + offsetX, this.ClientSize.Height - 5 );
            g.DrawLine( new Pen( Color.FromArgb( 180, 180, 180 ) ), 661 + offsetX, 3, 661 + offsetX, this.ClientSize.Height - 5 );
            g.DrawLine( new Pen( Color.FromArgb( 128, 128, 128 ) ), 662 + offsetX, 3, 662 + offsetX, this.ClientSize.Height - 5 );
            g.DrawLine( new Pen( Color.FromArgb( 108, 108, 108 ) ), 663 + offsetX, 3, 663 + offsetX, this.ClientSize.Height - 5 );
            e.Graphics.DrawImage( b, new Point( 0, 0 ) );
            g.Dispose();
            b.Dispose();
        }

        /// <summary>   Console control 1 enter. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void ConsoleControl1_Enter( object sender, EventArgs e )
        {
            this.Refresh();
        }

        /// <summary>   Console control 1 leave. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void ConsoleControl1_Leave( object sender, EventArgs e )
        {
            this.Refresh();
        }

        /// <summary>   Console control 2 enter. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void ConsoleControl2_Enter( object sender, EventArgs e )
        {
            this.Refresh();
        }

        /// <summary>   Console control 2 leave. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void ConsoleControl2_Leave( object sender, EventArgs e )
        {
            this.Refresh();
        }

        /// <summary>   Shows the help. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        private void ShowHelp()
        {
            this._RightConsoleControl.Write( "IcemanShell is just a 'toy' shell to show off Icemanind's Console Control." );
            this._RightConsoleControl.Write();
            this._RightConsoleControl.Write( "Here is a list of commands:" );
            this._RightConsoleControl.Write();
            this._RightConsoleControl.Write( "(Note that the commands are NOT case sensitive)" );
            this._RightConsoleControl.Write();
            this._RightConsoleControl.Write();
            this._RightConsoleControl.Write( "----------------------------------------------------------------------" );
            this._RightConsoleControl.Write();
            this._RightConsoleControl.Write( "HELP                Shows this help message" );
            this._RightConsoleControl.Write();
            this._RightConsoleControl.Write( "EXIT                Quits the application" );
            this._RightConsoleControl.Write();
            this._RightConsoleControl.Write( "LS <path>           Shows the contents of the specified directory." );
            this._RightConsoleControl.Write();
            this._RightConsoleControl.Write( "ECHO [string]       Echo's the [string] to the console." );
            this._RightConsoleControl.Write();
            this._RightConsoleControl.Write( "DATE                Shows the current date." );
            this._RightConsoleControl.Write();
            this._RightConsoleControl.Write( "PWD                 Shows the current working directory." );
            this._RightConsoleControl.Write();
            this._RightConsoleControl.Write( "CD <path>           Change the current working directory to <path>." );
            this._RightConsoleControl.Write();
        }

        /// <summary>   Executes the ls command operation. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="parameters">   Options for controlling the operation. </param>
        private void DoLsCommand( string parameters )
        {
            string path;
            Color bgColor = this._RightConsoleControl.CurrentBackgroundColor;
            Color fgColor = this._RightConsoleControl.CurrentForegroundColor;
            path = string.IsNullOrEmpty( parameters ) ? this._CurrentDirectory : parameters.Trim( '"' );
            string[] files = System.IO.Directory.GetFiles( path );
            string[] directories = System.IO.Directory.GetDirectories( path );
            this._RightConsoleControl.CurrentForegroundColor = Color.CadetBlue;
            foreach ( string directory in directories.OrderBy( z => z ) )
            {
                string dirName = "[" + System.IO.Path.GetFileName( directory ) + "]";
                this._RightConsoleControl.Write( $"{(dirName.Length > 40 ? Environment.NewLine + dirName + Environment.NewLine : dirName),-40}" );
            }

            foreach ( string file in files.OrderBy( z => z ) )
            {
                string fileName = System.IO.Path.GetFileName( file ) ?? "";
                string extension = System.IO.Path.GetExtension( file ) ?? "";
                switch ( extension.ToLower() ?? "" )
                {
                    case ".exe":
                        {
                            this._RightConsoleControl.CurrentForegroundColor = Color.FromArgb( 100, 255, 100 );
                            break;
                        }

                    case ".dll":
                        {
                            this._RightConsoleControl.CurrentForegroundColor = Color.FromArgb( 10, 165, 10 );
                            break;
                        }

                    default:
                        {
                            this._RightConsoleControl.CurrentForegroundColor = Color.LightGray;
                            break;
                        }
                }

                this._RightConsoleControl.Write( $"{(fileName.Length > 40 ? Environment.NewLine + fileName + Environment.NewLine : fileName),-40}" );
            }

            this._RightConsoleControl.CurrentBackgroundColor = bgColor;
            this._RightConsoleControl.CurrentForegroundColor = fgColor;
            this.ShowPrompt();
        }

        /// <summary>   Executes the echo command operation. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="parameters">   Options for controlling the operation. </param>
        private void DoEchoCommand( string parameters )
        {
            this._RightConsoleControl.Write( parameters );
            this._RightConsoleControl.Write();
        }

        /// <summary>   Executes the date command operation. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        private void DoDateCommand()
        {
            this._RightConsoleControl.Write( $"{DateTimeOffset.Now:ddd MMM d HH:mm:ss K yyyy}" );
            this._RightConsoleControl.Write();
        }

        /// <summary>   Executes the password command operation. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        private void DoPwdCommand()
        {
            this._RightConsoleControl.Write( this._CurrentDirectory );
            this._RightConsoleControl.Write();
        }

        /// <summary>   Executes the CD command operation. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="parameters">   Options for controlling the operation. </param>
        private void DoCdCommand( string parameters )
        {
            parameters = parameters.Trim( '"' );
            if ( parameters.Length >= 2 && parameters[1] == ':' )
            {
                if ( System.IO.Directory.Exists( parameters ) )
                {
                    this._CurrentDirectory = parameters;
                }
                else
                {
                    this._RightConsoleControl.Write( "Invalid Directory ==> " + parameters );
                    this._RightConsoleControl.Write();
                    return;
                }
            }
            else if ( parameters.Length > 0 && parameters[0] == '\\' )
            {
                if ( System.IO.Directory.Exists( System.IO.Path.GetPathRoot( this._CurrentDirectory ) + parameters.Remove( 0, 1 ) ) )
                {
                    this._CurrentDirectory = System.IO.Path.GetPathRoot( this._CurrentDirectory ) + parameters.Remove( 0, 1 );
                }
                else
                {
                    this._RightConsoleControl.Write( "Invalid Directory ==> " + System.IO.Path.GetPathRoot( this._CurrentDirectory ) + parameters.Remove( 0, 1 ) );
                    this._RightConsoleControl.Write();
                    return;
                }
            }
            else if ( parameters.Length > 0 )
            {
                if ( System.IO.Directory.Exists( this._CurrentDirectory + @"\" + parameters ) )
                {
                    this._CurrentDirectory = this._CurrentDirectory + @"\" + parameters;
                }
                else
                {
                    this._RightConsoleControl.Write( "Invalid Directory ==> " + this._CurrentDirectory + parameters );
                    this._RightConsoleControl.Write();
                    return;
                }
            }

            this._CurrentDirectory = this._CurrentDirectory.Replace( @"\\", @"\" );
            this._RightConsoleControl.Write( $"Current Directory is now: {this._CurrentDirectory}" );
            this._RightConsoleControl.Write();
        }
    }
}
