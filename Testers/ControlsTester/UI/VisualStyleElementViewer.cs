
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

using Microsoft.VisualBasic.CompilerServices;

namespace ControlsTester
{

    /// <summary>   A visual style element viewer. </summary>
    /// <remarks>   David, 2020-10-25. </remarks>
    public partial class VisualStyleElementViewer
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Windows.Forms.Form" /> class.
        /// </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        public VisualStyleElementViewer()
        {
            this.InitializeComponent();
            ElementViewer elementViewer1 = new();
            this.Controls.Add( elementViewer1 );
            this.Text = elementViewer1.Text;
            this.Size = new Size( 700, 550 );
        }
    }

    /// <summary>   An element viewer. </summary>
    /// <remarks>   David, 2020-10-25. </remarks>
    public class ElementViewer : UserControl
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Windows.Forms.UserControl" /> class.
        /// </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        public ElementViewer()
        {
            this.TreeView1 = new TreeView();
            this.DomainUpDown1 = new DomainUpDown();

            this.Load += this.ElementViewer_Load;
            this.Location = new Point( 10, 10 );
            this.Size = new Size( 650, 500 );
            this.Text = "VisualStyleElement Viewer";
            this.Font = SystemFonts.IconTitleFont;
            this.BackColor = Color.White;
            this.BorderStyle = BorderStyle.Fixed3D;
            this.AutoSize = true;
        }

        /// <summary>   The element. </summary>
        private VisualStyleElement _Element;

        /// <summary>   The renderer. </summary>
        private VisualStyleRenderer _Renderer;

        /// <summary>   Dictionary of elements. </summary>
        private readonly Dictionary<string, VisualStyleElement> _ElementDictionary = new();

        /// <summary>   The description rectangle. </summary>
        private Rectangle _DescriptionRect;

        /// <summary>   The display rectangle. </summary>
        private Rectangle _DisplayRect;

        /// <summary>   The display rectangle full. </summary>
        private Rectangle _DisplayRectFull;

        /// <summary>   Size of the current true. </summary>
        private Size _CurrentTrueSize = new();

        /// <summary>   Information describing the element. </summary>
        private readonly StringBuilder _ElementDescription = new();

        /// <summary>   The first label. </summary>
        private readonly Label _Label1 = new();
        /// <summary>   The tree view 1 control. </summary>
        private TreeView _TreeView1;

        /// <summary>   Gets or sets the tree view 1. </summary>
        /// <value> The tree view 1. </value>
        private TreeView TreeView1
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._TreeView1;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._TreeView1 != null )
                {
                    this._TreeView1.AfterSelect -= this.TreeView1_AfterSelect;
                }

                this._TreeView1 = value;
                if ( this._TreeView1 != null )
                {
                    this._TreeView1.AfterSelect += this.TreeView1_AfterSelect;
                }
            }
        }

        /// <summary>   The first domain up down. </summary>
        private DomainUpDown _DomainUpDown1;

        /// <summary>   Gets or sets the domain up down 1. </summary>
        /// <value> The domain up down 1. </value>
        private DomainUpDown DomainUpDown1
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._DomainUpDown1;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._DomainUpDown1 != null )
                {
                    this._DomainUpDown1.SelectedItemChanged -= this.DomainUpDown1_SelectedItemChanged;
                }

                this._DomainUpDown1 = value;
                if ( this._DomainUpDown1 != null )
                {
                    this._DomainUpDown1.SelectedItemChanged += this.DomainUpDown1_SelectedItemChanged;
                }
            }
        }

        /// <summary>   True to draw element. </summary>
        private bool _DrawElement = false;

        /// <summary>   Event handler. Called by ElementViewer for load events. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void ElementViewer_Load( object sender, EventArgs e )
        {

            // Make sure the visual styles are enabled before 
            // going any further.
            if ( !Application.RenderWithVisualStyles )
            {
                return;
            }

            this._Label1.Location = new Point( 320, 10 );
            this._Label1.Size = new Size( 300, 60 );
            this._Label1.Text = "Expand the element class nodes in the " + "tree view to access visual style elements. " + "Click an element name to draw the element " + "below. To change the size of a resizable " + "element, use the spin control.";
            this.DomainUpDown1.Location = new Point( 320, 80 );
            this.DomainUpDown1.Size = new Size( 70, 30 );
            this.DomainUpDown1.ReadOnly = true;
            _ = this.DomainUpDown1.Items.Add( ElementSizes.Large );
            _ = this.DomainUpDown1.Items.Add( ElementSizes.Medium );
            _ = this.DomainUpDown1.Items.Add( ElementSizes.TrueSize );
            this.DomainUpDown1.SelectedIndex = 2;
            this.DomainUpDown1.DownButton();
            this._DescriptionRect = new Rectangle( 320, 120, 250, 50 );
            this._DisplayRect = new Rectangle( 320, 160, 0, 0 );
            this._DisplayRectFull = new Rectangle( 320, 160, 300, 200 );

            // Initialize the element and renderer to known good values.
            this._Element = VisualStyleElement.Button.PushButton.Normal;
            this._Renderer = new VisualStyleRenderer( this._Element );
            this.SetupElementCollection();
            this.SetupTreeView();
            this.Controls.AddRange( new Control[] { this.TreeView1, this.DomainUpDown1, this._Label1 } );
        }

        /// <summary>
        /// Sets up the element collection. Use reflection to build a Dictionary of all
        /// VisualStyleElement objects exposed in the System.Windows.Forms.VisualStyles namespace.
        /// </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        private void SetupElementCollection()
        {
            StringBuilder elementName = new();
            VisualStyleElement currentElement;
            object tempObject;

            // Get array of first-level nested types within 
            // VisualStyleElement; these are the element classes.
            Type[] elementClasses = typeof( VisualStyleElement ).GetNestedTypes();
            foreach ( Type elementClass in elementClasses )
            {

                // Get an array of second-level nested types within
                // VisualStyleElement; these are the element parts.
                Type[] elementParts = elementClass.GetNestedTypes();

                // Get the index of the first '+' character in 
                // the full element class name.
                int plusSignIndex = elementClass.FullName.IndexOf( "+" );
                foreach ( Type elementPart in elementParts )
                {

                    // Get an array of Shared property details 
                    // for  the current type. Each of these types have 
                    // properties that return VisualStyleElement objects.

                    // For each property, insert the unique full element   
                    // name and the element into the collection.
                    PropertyInfo[] elementProperties = elementPart.GetProperties( BindingFlags.Static | BindingFlags.Public );
                    foreach ( PropertyInfo elementProperty in elementProperties )
                    {

                        // Get the element.
                        tempObject = elementProperty.GetValue( null, BindingFlags.Static, null, null, null );
                        currentElement = ( VisualStyleElement ) tempObject;

                        // Append the full element name.
                        _ = elementName.Append( elementClass.FullName, plusSignIndex + 1, elementClass.FullName.Length - plusSignIndex - 1 );
                        _ = elementName.Append( "." + elementPart.Name.ToString() + "." + elementProperty.Name );

                        // Add the element and element name to 
                        // the Dictionary.
                        this._ElementDictionary.Add( elementName.ToString(), currentElement );

                        // Clear the element name for the next iteration.
                        _ = elementName.Remove( 0, elementName.Length );
                    }
                }
            }
        }

        /// <summary>   Sets up the tree view. Initialize the tree view with the element names. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        private void SetupTreeView()
        {
            this.TreeView1.Location = new Point( 10, 10 );
            this.TreeView1.Size = new Size( 300, 450 );
            this.TreeView1.BorderStyle = BorderStyle.FixedSingle;
            this.TreeView1.BackColor = Color.WhiteSmoke;
            this.TreeView1.SelectedNode = null;
            this.TreeView1.BeginUpdate();

            // An index into the top-level tree nodes.
            int nodeIndex = 0;

            // An index into the first '.' character in an element name.
            int firstDotIndex;

            // Initialize the element class name to compare 
            // with the class name of the first element 
            // in the Dictionary, and set this name to the first 
            // top-level node.
            StringBuilder compareClassName = new( "Button" );
            _ = this.TreeView1.Nodes.Add( new TreeNode( compareClassName.ToString() ) );

            // The current element class name.
            StringBuilder currentClassName = new();

            // The text for each second-level node.
            StringBuilder nodeText = new();
            foreach ( KeyValuePair<string, VisualStyleElement> entry in this._ElementDictionary )
            {

                // Isolate the class name of the current element.
                firstDotIndex = entry.Key.IndexOf( "." );
                _ = currentClassName.Append( entry.Key, 0, firstDotIndex );

                // Determine whether we need to increment to the next 
                // element class.
                if ( (currentClassName.ToString() ?? "") != (compareClassName.ToString() ?? "") )
                {

                    // Increment the index to the next top-level node 
                    // in the tree view.
                    nodeIndex += 1;

                    // Update the class name to compare with.
                    _ = compareClassName.Remove( 0, compareClassName.Length );
                    _ = compareClassName.Append( entry.Key );
                    _ = compareClassName.Remove( firstDotIndex, compareClassName.Length - firstDotIndex );

                    // Add a new top-level node to the tree view.
                    TreeNode node = new( compareClassName.ToString() );
                    _ = this.TreeView1.Nodes.Add( node );
                }

                // Get the text for the new second-level node.
                _ = nodeText.Append( entry.Key, firstDotIndex + 1, entry.Key.Length - firstDotIndex - 1 );

                // Create and insert the new second-level node.
                TreeNode newNode = new( nodeText.ToString() ) { Name = entry.Key };
                _ = this.TreeView1.Nodes[nodeIndex].Nodes.Add( newNode );
                _ = currentClassName.Remove( 0, currentClassName.Length );
                _ = nodeText.Remove( 0, nodeText.Length );
            }

            this.TreeView1.EndUpdate();
        }

        /// <summary>   Raises the <see cref="E:System.Windows.Forms.Control.Paint" /> event. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="e">    A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
        ///                     event data. </param>
        protected override void OnPaint( PaintEventArgs e )
        {
            base.OnPaint( e );

            // Do nothing further if visual styles are disabled.
            if ( !Application.RenderWithVisualStyles )
            {
                this.Text = "Visual styles are disabled.";
                TextRenderer.DrawText( e.Graphics, this.Text, this.Font, this.Location, this.ForeColor );
                return;
            }

            // Draw the element description.
            TextRenderer.DrawText( e.Graphics, this._ElementDescription.ToString(), this.Font, this._DescriptionRect, this.ForeColor, TextFormatFlags.WordBreak );

            // Draw the element, if an element is selected.
            if ( this._DrawElement )
            {
                this._Renderer.DrawBackground( e.Graphics, this._DisplayRect );
            }
        }

        /// <summary>   Set the element to draw. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Tree view event information. </param>
        private void TreeView1_AfterSelect( object sender, TreeViewEventArgs e )
        {

            // Clear the element description.
            _ = this._ElementDescription.Remove( 0, this._ElementDescription.Length );

            // If the user clicked a first-level node, disable drawing.
            if ( e.Node.Nodes.Count > 0 )
            {
                this._DrawElement = false;
                _ = this._ElementDescription.Append( "No element is selected" );
                this.DomainUpDown1.Enabled = false;
            }

            // The user clicked an element node.
            else
            {
                // Add the element name to the description.
                _ = this._ElementDescription.Append( e.Node.Text );

                // Get the element that corresponds to the selected  
                // node's name.
                string key = e.Node.Name;
                this._Element = this._ElementDictionary[key];

                // Disable resizing if the element is not defined.
                if ( !VisualStyleRenderer.IsElementDefined( this._Element ) )
                {
                    this._DrawElement = false;
                    _ = this._ElementDescription.Append( " is not defined." );
                    this.DomainUpDown1.Enabled = false;
                }
                else
                {
                    // Set the element to the renderer.
                    this._DrawElement = true;
                    this._Renderer.SetParameters( this._Element );
                    _ = this._ElementDescription.Append( " is defined." );

                    // Get the system-defined size of the element.
                    Graphics g = this.CreateGraphics();
                    this._CurrentTrueSize = this._Renderer.GetPartSize( g, ThemeSizeType.True );
                    g.Dispose();
                    this._DisplayRect.Size = this._CurrentTrueSize;
                    this.DomainUpDown1.Enabled = true;
                    this.DomainUpDown1.SelectedIndex = 2;
                }
            }

            this.Invalidate();
        }

        /// <summary>   Event handler. Called by DomainUpDown1 for selected item changed events. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void DomainUpDown1_SelectedItemChanged( object sender, EventArgs e )
        {
            switch ( Conversions.ToInteger( this.DomainUpDown1.SelectedItem ) )
            {
                case ( int ) ElementSizes.TrueSize:
                    {
                        this._DisplayRect.Size = this._CurrentTrueSize;
                        break;
                    }

                case ( int ) ElementSizes.Medium:
                    {
                        this._DisplayRect.Size = new Size( this._DisplayRectFull.Width / 2, this._DisplayRectFull.Height / 2 );
                        break;
                    }

                case ( int ) ElementSizes.Large:
                    {
                        this._DisplayRect.Size = this._DisplayRectFull.Size;
                        break;
                    }
            }

            this.Invalidate();
        }

        /// <summary>   Values that represent element sizes. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        private enum ElementSizes
        {

            /// <summary> An enum constant representing the true size option. </summary>
            TrueSize,

            /// <summary> An enum constant representing the medium option. </summary>
            Medium,

            /// <summary> An enum constant representing the large option. </summary>
            Large
        }
    }
}
