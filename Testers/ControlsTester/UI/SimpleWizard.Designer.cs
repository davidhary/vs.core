﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace ControlsTester
{
    [Microsoft.VisualBasic.CompilerServices.DesignerGenerated()]
    public partial class SimpleWizard
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _LicenseWizardPage = new isr.Core.Controls.WizardPage();
            __AgreeCheckBox = new CheckBox();
            __AgreeCheckBox.CheckedChanged += new EventHandler(CheckIAgree_CheckedChanged);
            _LicenseTextBox = new TextBox();
            _OptionsWizardPage = new isr.Core.Controls.WizardPage();
            _OptionsGroupBox = new GroupBox();
            _SkipOptionRadioButton = new RadioButton();
            _CheckOptionRadioButton = new RadioButton();
            _FinishWizardPage = new isr.Core.Controls.WizardPage();
            __SampleWizard = new isr.Core.Controls.Wizard();
            __SampleWizard.PageChanged += new EventHandler<isr.Core.Controls.PageChangedEventArgs>(SampleWizard_AfterSwitchPages);
            __SampleWizard.PageChanging += new EventHandler<isr.Core.Controls.PageChangingEventArgs>(SampleWizard_BeforeSwitchPages);
            __SampleWizard.Cancel += new System.ComponentModel.CancelEventHandler(SampleWizard_Cancel);
            __SampleWizard.Finish += new EventHandler(SampleWizard_Finish);
            __SampleWizard.Help += new EventHandler(SampleWizard_Help);
            _CheckWizardPage = new isr.Core.Controls.WizardPage();
            _PlaceholderLabel = new Label();
            _ProgressWizardPage = new isr.Core.Controls.WizardPage();
            _ProgressLabel = new Label();
            _LongTaskProgressBar = new ProgressBar();
            _WelcomeWizardPage = new isr.Core.Controls.WizardPage();
            __LongTaskTimer = new Timer(components);
            __LongTaskTimer.Tick += new EventHandler(TimerTask_Tick);
            _LicenseWizardPage.SuspendLayout();
            _OptionsWizardPage.SuspendLayout();
            _OptionsGroupBox.SuspendLayout();
            __SampleWizard.SuspendLayout();
            _CheckWizardPage.SuspendLayout();
            _ProgressWizardPage.SuspendLayout();
            SuspendLayout();
            // 
            // _LicenseWizardPage
            // 
            _LicenseWizardPage.Controls.Add(__AgreeCheckBox);
            _LicenseWizardPage.Controls.Add(_LicenseTextBox);
            _LicenseWizardPage.Description = "Please read the following license agreement and confirm that you agree with all terms and conditions.";
            _LicenseWizardPage.Location = new Point(0, 0);
            _LicenseWizardPage.Name = "_LicenseWizardPage";
            _LicenseWizardPage.Size = new Size(466, 296);
            _LicenseWizardPage.TabIndex = 10;
            _LicenseWizardPage.Title = "License Agreement";
            _LicenseWizardPage.WizardPageStyle = isr.Core.Controls.WizardPageStyle.Standard;
            // 
            // _AgreeCheckBox
            // 
            __AgreeCheckBox.FlatStyle = FlatStyle.System;
            __AgreeCheckBox.Location = new Point(12, 272);
            __AgreeCheckBox.Name = "__AgreeCheckBox";
            __AgreeCheckBox.Size = new Size(288, 16);
            __AgreeCheckBox.TabIndex = 0;
            __AgreeCheckBox.Text = "I agree with this license's terms and conditions.";
            // 
            // _LicenseTextBox
            // 
            _LicenseTextBox.BackColor = SystemColors.Window;
            _LicenseTextBox.Location = new Point(12, 76);
            _LicenseTextBox.Multiline = true;
            _LicenseTextBox.Name = "_LicenseTextBox";
            _LicenseTextBox.ReadOnly = true;
            _LicenseTextBox.Size = new Size(440, 188);
            _LicenseTextBox.TabIndex = 1;
            _LicenseTextBox.Text = "Some long and boring license text...";
            // 
            // _OptionsWizardPage
            // 
            _OptionsWizardPage.Controls.Add(_OptionsGroupBox);
            _OptionsWizardPage.Description = "Please select an option from the available list.";
            _OptionsWizardPage.Location = new Point(0, 0);
            _OptionsWizardPage.Name = "_OptionsWizardPage";
            _OptionsWizardPage.Size = new Size(466, 296);
            _OptionsWizardPage.TabIndex = 11;
            _OptionsWizardPage.Title = "Task Options";
            _OptionsWizardPage.WizardPageStyle = isr.Core.Controls.WizardPageStyle.Standard;
            // 
            // _OptionsGroupBox
            // 
            _OptionsGroupBox.Controls.Add(_SkipOptionRadioButton);
            _OptionsGroupBox.Controls.Add(_CheckOptionRadioButton);
            _OptionsGroupBox.FlatStyle = FlatStyle.System;
            _OptionsGroupBox.Location = new Point(16, 84);
            _OptionsGroupBox.Name = "_OptionsGroupBox";
            _OptionsGroupBox.Size = new Size(436, 112);
            _OptionsGroupBox.TabIndex = 0;
            _OptionsGroupBox.TabStop = false;
            _OptionsGroupBox.Text = "Available Options";
            // 
            // _SkipOptionRadioButton
            // 
            _SkipOptionRadioButton.FlatStyle = FlatStyle.System;
            _SkipOptionRadioButton.Location = new Point(20, 36);
            _SkipOptionRadioButton.Name = "_SkipOptionRadioButton";
            _SkipOptionRadioButton.Size = new Size(260, 20);
            _SkipOptionRadioButton.TabIndex = 0;
            _SkipOptionRadioButton.Text = "Skip any checks and proceed.";
            // 
            // _CheckOptionRadioButton
            // 
            _CheckOptionRadioButton.FlatStyle = FlatStyle.System;
            _CheckOptionRadioButton.Location = new Point(20, 72);
            _CheckOptionRadioButton.Name = "_CheckOptionRadioButton";
            _CheckOptionRadioButton.Size = new Size(260, 20);
            _CheckOptionRadioButton.TabIndex = 1;
            _CheckOptionRadioButton.Text = "Check for something first.";
            // 
            // _FinishWizardPage
            // 
            _FinishWizardPage.Description = "Thank you for using the Sample Wizard." + '\n' + "Press OK to exit.";
            _FinishWizardPage.Location = new Point(0, 0);
            _FinishWizardPage.Name = "_FinishWizardPage";
            _FinishWizardPage.Size = new Size(466, 296);
            _FinishWizardPage.TabIndex = 12;
            _FinishWizardPage.Title = "Sample Wizard has finished";
            _FinishWizardPage.WizardPageStyle = isr.Core.Controls.WizardPageStyle.Finish;
            // 
            // _SampleWizard
            // 
            __SampleWizard.Controls.Add(_FinishWizardPage);
            __SampleWizard.Controls.Add(_ProgressWizardPage);
            __SampleWizard.Controls.Add(_CheckWizardPage);
            __SampleWizard.Controls.Add(_OptionsWizardPage);
            __SampleWizard.Controls.Add(_LicenseWizardPage);
            __SampleWizard.Controls.Add(_WelcomeWizardPage);
            __SampleWizard.Dock = DockStyle.None;
            __SampleWizard.FinishText = "&Finish";
            __SampleWizard.HeaderImage = My.Resources.Resources.HeaderIcon;
            __SampleWizard.HelpVisible = true;
            __SampleWizard.Location = new Point(0, 0);
            __SampleWizard.Name = "__SampleWizard";
            __SampleWizard.Pages.AddRange(new isr.Core.Controls.WizardPage[] { _WelcomeWizardPage, _LicenseWizardPage, _OptionsWizardPage, _CheckWizardPage, _ProgressWizardPage, _FinishWizardPage });
            __SampleWizard.Size = new Size(466, 344);
            __SampleWizard.TabIndex = 0;
            __SampleWizard.WelcomeImage = My.Resources.Resources.WelcomeImage;
            // 
            // _CheckWizardPage
            // 
            _CheckWizardPage.Controls.Add(_PlaceholderLabel);
            _CheckWizardPage.Description = "Please enter required information.";
            _CheckWizardPage.Location = new Point(0, 0);
            _CheckWizardPage.Name = "_CheckWizardPage";
            _CheckWizardPage.Size = new Size(466, 296);
            _CheckWizardPage.TabIndex = 13;
            _CheckWizardPage.Title = "Check Something";
            _CheckWizardPage.WizardPageStyle = isr.Core.Controls.WizardPageStyle.Standard;
            // 
            // _PlaceholderLabel
            // 
            _PlaceholderLabel.ForeColor = Color.Red;
            _PlaceholderLabel.Location = new Point(28, 100);
            _PlaceholderLabel.Name = "_PlaceholderLabel";
            _PlaceholderLabel.Size = new Size(384, 16);
            _PlaceholderLabel.TabIndex = 0;
            _PlaceholderLabel.Text = "Place some validation controls here.";
            // 
            // _ProgressWizardPage
            // 
            _ProgressWizardPage.Controls.Add(_ProgressLabel);
            _ProgressWizardPage.Controls.Add(_LongTaskProgressBar);
            _ProgressWizardPage.Description = "This simulates a long running sample task.";
            _ProgressWizardPage.Location = new Point(0, 0);
            _ProgressWizardPage.Name = "_ProgressWizardPage";
            _ProgressWizardPage.Size = new Size(466, 296);
            _ProgressWizardPage.TabIndex = 10;
            _ProgressWizardPage.Title = "Task Running";
            _ProgressWizardPage.WizardPageStyle = isr.Core.Controls.WizardPageStyle.Standard;
            // 
            // _ProgressLabel
            // 
            _ProgressLabel.Location = new Point(20, 84);
            _ProgressLabel.Name = "_ProgressLabel";
            _ProgressLabel.Size = new Size(252, 16);
            _ProgressLabel.TabIndex = 1;
            _ProgressLabel.Text = "Please wait while the wizard does a long task...";
            // 
            // _LongTaskProgressBar
            // 
            _LongTaskProgressBar.Location = new Point(16, 104);
            _LongTaskProgressBar.Name = "_LongTaskProgressBar";
            _LongTaskProgressBar.Size = new Size(436, 20);
            _LongTaskProgressBar.TabIndex = 0;
            // 
            // _WelcomeWizardPage
            // 
            _WelcomeWizardPage.Description = "This wizard will guide you through the steps of performing a sample task.";
            _WelcomeWizardPage.Location = new Point(0, 0);
            _WelcomeWizardPage.Name = "_WelcomeWizardPage";
            _WelcomeWizardPage.Size = new Size(466, 296);
            _WelcomeWizardPage.TabIndex = 9;
            _WelcomeWizardPage.Title = "Welcome to the Sample  Wizard";
            _WelcomeWizardPage.WizardPageStyle = isr.Core.Controls.WizardPageStyle.Welcome;
            // 
            // _LongTaskTimer
            // 
            // 
            // SimpleWizard
            // 
            AutoScaleBaseSize = new Size(5, 13);
            ClientSize = new Size(468, 338);
            Controls.Add(__SampleWizard);
            FormBorderStyle = FormBorderStyle.FixedDialog;
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "SimpleWizard";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Simple Wizard";
            _LicenseWizardPage.ResumeLayout(false);
            _LicenseWizardPage.PerformLayout();
            _OptionsWizardPage.ResumeLayout(false);
            _OptionsGroupBox.ResumeLayout(false);
            __SampleWizard.ResumeLayout(false);
            _CheckWizardPage.ResumeLayout(false);
            _ProgressWizardPage.ResumeLayout(false);
            ResumeLayout(false);
        }

        private CheckBox __AgreeCheckBox;

        private CheckBox _AgreeCheckBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __AgreeCheckBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__AgreeCheckBox != null)
                {
                    __AgreeCheckBox.CheckedChanged -= CheckIAgree_CheckedChanged;
                }

                __AgreeCheckBox = value;
                if (__AgreeCheckBox != null)
                {
                    __AgreeCheckBox.CheckedChanged += CheckIAgree_CheckedChanged;
                }
            }
        }

        private TextBox _LicenseTextBox;
        private isr.Core.Controls.WizardPage _LicenseWizardPage;
        private isr.Core.Controls.WizardPage _OptionsWizardPage;
        private isr.Core.Controls.WizardPage _FinishWizardPage;
        private isr.Core.Controls.WizardPage _WelcomeWizardPage;
        private isr.Core.Controls.WizardPage _ProgressWizardPage;
        private isr.Core.Controls.Wizard __SampleWizard;

        private isr.Core.Controls.Wizard _SampleWizard
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SampleWizard;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SampleWizard != null)
                {
                    __SampleWizard.PageChanged -= SampleWizard_AfterSwitchPages;
                    __SampleWizard.PageChanging -= SampleWizard_BeforeSwitchPages;
                    __SampleWizard.Cancel -= SampleWizard_Cancel;
                    __SampleWizard.Finish -= SampleWizard_Finish;
                    __SampleWizard.Help -= SampleWizard_Help;
                }

                __SampleWizard = value;
                if (__SampleWizard != null)
                {
                    __SampleWizard.PageChanged += SampleWizard_AfterSwitchPages;
                    __SampleWizard.PageChanging += SampleWizard_BeforeSwitchPages;
                    __SampleWizard.Cancel += SampleWizard_Cancel;
                    __SampleWizard.Finish += SampleWizard_Finish;
                    __SampleWizard.Help += SampleWizard_Help;
                }
            }
        }

        private ProgressBar _LongTaskProgressBar;
        private Label _ProgressLabel;
        private Timer __LongTaskTimer;

        private Timer _LongTaskTimer
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __LongTaskTimer;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__LongTaskTimer != null)
                {
                    __LongTaskTimer.Tick -= TimerTask_Tick;
                }

                __LongTaskTimer = value;
                if (__LongTaskTimer != null)
                {
                    __LongTaskTimer.Tick += TimerTask_Tick;
                }
            }
        }

        private GroupBox _OptionsGroupBox;
        private RadioButton _SkipOptionRadioButton;
        private RadioButton _CheckOptionRadioButton;
        private isr.Core.Controls.WizardPage _CheckWizardPage;
        private Label _PlaceholderLabel;
    }
}