//                            Cristi Potlog                            
//                  Copyright ©2005 - All Rights reserved              
//                                                                     
// THIS SOURCE CODE IS PROVIDED "AS IS" WITH NO WARRANTIES OF ANY KIND,
// EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE       
// WARRANTIES OF DESIGN, MERCHANTIBILITY AND FITNESS FOR A PARTICULAR  
// PURPOSE, NONINFRINGEMENT, OR ARISING FROM A COURSE OF DEALING,      
// USAGE OR TRADE PRACTICE.                                            
//                                                                     
// THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.            
// ------------------------------------------------------------------- 
using System;
using System.Diagnostics;
using System.Windows.Forms;

using isr.Core.Controls;

namespace ControlsTester
{

    /// <summary>   An example for using the CP wizard control. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc., All Rights Reserved <para>
    /// (c) 2005 Cristi Potlog - All Rights Reserved </para><para>
    /// Licensed under the MIT License. </para><para>
    /// David, 2012-09-19, 1.05.4645.  </para><para>
    /// Based on http://www.CodeProject.com/Articles/10808/Cristi-Potlog-s-Wizard-Control-for-NET
    /// </para>
    /// </remarks>
    public partial class SimpleWizard : Form
    {

        /// <summary>   Creates a new instance of the <see cref="SimpleWizard"/> class. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        public SimpleWizard()
        {
            // required for designer support
            this.InitializeComponent();
            this.__AgreeCheckBox.Name = "_AgreeCheckBox";
            this.__SampleWizard.Name = "_SampleWizard";
        }

        /// <summary>
        /// Disposes of the resources (other than memory) used by the
        /// <see cref="T:System.Windows.Forms.Form" />.
        /// </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="disposing">    <see langword="true" /> to release both managed and unmanaged
        ///                             resources; <see langword="false" /> to release only unmanaged
        ///                             resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( disposing )
                {
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        /// <summary>   Starts a sample task simulation. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        private void StartTask()
        {
            // setup wizard page
            this._SampleWizard.BackEnabled = false;
            this._SampleWizard.NextEnabled = false;
            this._LongTaskProgressBar.Value = this._LongTaskProgressBar.Minimum;

            // start timer to simulate a long running task
            this._LongTaskTimer.Enabled = true;
        }

        /// <summary>   Handles the AfterSwitchPages event of the wizard form. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Page changed event information. </param>
        private void SampleWizard_AfterSwitchPages( object sender, PageChangedEventArgs e )
        {

            // check if license page
            if ( ReferenceEquals( this._SampleWizard.NewPage, this._LicenseWizardPage ) )
            {
                // sync next button's state with check box
                this._SampleWizard.NextEnabled = this._AgreeCheckBox.Checked;
            }
            // check if progress page
            else if ( ReferenceEquals( this._SampleWizard.NewPage, this._ProgressWizardPage ) )
            {
                // start the sample task
                this.StartTask();
            }
        }

        /// <summary>   Handles the BeforeSwitchPages event of the wizard form. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Page changing event information. </param>
        private void SampleWizard_BeforeSwitchPages( object sender, PageChangingEventArgs e )
        {

            // check if we're going forward from options page
            if ( ReferenceEquals( this._SampleWizard.OldPage, this._OptionsWizardPage ) && e.NewIndex > e.OldIndex )
            {
                // check if user selected one option
                if ( this._CheckOptionRadioButton.Checked == false && this._SkipOptionRadioButton.Checked == false )
                {
                    // display hint & cancel step
                    _ = MessageBox.Show( "Please chose one of the options presented.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information );
                    e.Cancel = true;
                }
                // check if user chose to skip validation
                else if ( this._SkipOptionRadioButton.Checked )
                {
                    // skip the validation page
                    e.NewIndex += 1;
                }
            }
        }

        /// <summary>   Handles the Cancel event of the wizard form. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Cancel event information. </param>
        private void SampleWizard_Cancel( object sender, System.ComponentModel.CancelEventArgs e )
        {
            // check if task is running
            bool isTaskRunning = this._LongTaskTimer.Enabled;
            // stop the task
            this._LongTaskTimer.Enabled = false;

            // ask user to confirm
            if ( MessageBox.Show( "Are you sure you wand to exit the Sample Wizard?", this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question ) != DialogResult.Yes )
            {
                // cancel closing
                e.Cancel = true;
                // restart the task
                this._LongTaskTimer.Enabled = isTaskRunning;
            }
            else
            {
                // ensure parent form is closed (even when ShowDialog is not used)
                this.DialogResult = DialogResult.Cancel;
                this.Close();
            }
        }

        /// <summary>   Handles the Finish event of the wizard form. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void SampleWizard_Finish( object sender, EventArgs e )
        {
            _ = MessageBox.Show( "The Sample Wizard finished successfully.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information );
            // ensure parent form is closed (even when ShowDialog is not used)
            this.Close();
        }

        /// <summary>   Handles the Help event of the wizard form. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void SampleWizard_Help( object sender, EventArgs e )
        {
            _ = MessageBox.Show( "This is a really cool wizard control!" + Environment.NewLine + ":-)", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information );
        }

        /// <summary>   Handles the CheckedChanged event of checkIAgree. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void CheckIAgree_CheckedChanged( object sender, EventArgs e )
        {
            // sync next button's state with check box
            this._SampleWizard.NextEnabled = this._AgreeCheckBox.Checked;
        }

        /// <summary>   Handles the Tick event of timerTask. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void TimerTask_Tick( object sender, EventArgs e )
        {
            // check if task completed
            if ( this._LongTaskProgressBar.Value == this._LongTaskProgressBar.Maximum )
            {
                // stop the timer & switch to next page
                this._LongTaskTimer.Enabled = false;
                this._SampleWizard.Next();
            }
            else
            {
                // update progress bar
                this._LongTaskProgressBar.PerformStep();
            }
        }

    }
}
