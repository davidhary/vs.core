using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace ControlsTester
{
    [DesignerGenerated()]
    public partial class Switchboard : Form
    {

        /// <summary>   Form overrides dispose to clean up the component list. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="disposing">    <see langword="true" /> to release both managed and unmanaged
        ///                             resources; <see langword="false" /> to release only unmanaged
        ///                             resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components is object)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        /// <summary>   Required by the Windows Form Designer. </summary>
        private System.ComponentModel.IContainer components;

        /// <summary>
        /// NOTE: The following procedure is required by the Windows Form Designer It can be modified
        /// using the Windows Form Designer.  
        /// Do not modify it using the code editor.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(Switchboard));
            __CancelButton = new Button();
            __CancelButton.Click += new EventHandler(CancelButton_Click);
            __ExitButton = new Button();
            __ExitButton.Click += new EventHandler(ExitButton_Click);
            __TestButton = new Button();
            __TestButton.Click += new EventHandler(TestButton_Click);
            _ActionsComboBox = new ComboBox();
            __OpenButton = new Button();
            __OpenButton.Click += new EventHandler(OpenButton_Click);
            _ControlsPanel = new Panel();
            __LogOnButton = new Button();
            __LogOnButton.Click += new EventHandler(LogOnButton_Click);
            _MessagesTextBox = new TextBox();
            _ControlsPanel.SuspendLayout();
            SuspendLayout();
            // 
            // _CancelButton
            // 
            __CancelButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            __CancelButton.DialogResult = DialogResult.Cancel;
            __CancelButton.FlatStyle = FlatStyle.System;
            __CancelButton.Location = new Point(308, 38);
            __CancelButton.Name = "__CancelButton";
            __CancelButton.Size = new Size(60, 23);
            __CancelButton.TabIndex = 7;
            __CancelButton.Text = "&Cancel";
            // 
            // _ExitButton
            // 
            __ExitButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            __ExitButton.DialogResult = DialogResult.OK;
            __ExitButton.FlatStyle = FlatStyle.System;
            __ExitButton.Location = new Point(374, 38);
            __ExitButton.Name = "__ExitButton";
            __ExitButton.Size = new Size(60, 23);
            __ExitButton.TabIndex = 5;
            __ExitButton.Text = "E&xit";
            // 
            // _TestButton
            // 
            __TestButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            __TestButton.FlatStyle = FlatStyle.System;
            __TestButton.Location = new Point(10, 38);
            __TestButton.Name = "__TestButton";
            __TestButton.Size = new Size(60, 23);
            __TestButton.TabIndex = 8;
            __TestButton.Text = "&Test";
            // 
            // _ActionsComboBox
            // 
            _ActionsComboBox.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            _ActionsComboBox.BackColor = SystemColors.Window;
            _ActionsComboBox.Cursor = Cursors.Default;
            _ActionsComboBox.Font = new Font("Arial", 12.0f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            _ActionsComboBox.ForeColor = SystemColors.WindowText;
            _ActionsComboBox.Location = new Point(9, 3);
            _ActionsComboBox.Name = "_ActionsComboBox";
            _ActionsComboBox.RightToLeft = RightToLeft.No;
            _ActionsComboBox.Size = new Size(361, 27);
            _ActionsComboBox.TabIndex = 11;
            _ActionsComboBox.Text = "Select option from the list";
            // 
            // _OpenButton
            // 
            __OpenButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            __OpenButton.Location = new Point(376, 4);
            __OpenButton.Name = "__OpenButton";
            __OpenButton.Size = new Size(58, 24);
            __OpenButton.TabIndex = 12;
            __OpenButton.Text = "&Open...";
            // 
            // _ControlsPanel
            // 
            _ControlsPanel.Controls.Add(_ActionsComboBox);
            _ControlsPanel.Controls.Add(__CancelButton);
            _ControlsPanel.Controls.Add(__ExitButton);
            _ControlsPanel.Controls.Add(__OpenButton);
            _ControlsPanel.Controls.Add(__LogOnButton);
            _ControlsPanel.Controls.Add(__TestButton);
            _ControlsPanel.Dock = DockStyle.Bottom;
            _ControlsPanel.Location = new Point(0, 199);
            _ControlsPanel.Name = "_ControlsPanel";
            _ControlsPanel.Size = new Size(442, 66);
            _ControlsPanel.TabIndex = 15;
            // 
            // _LogOnButton
            // 
            __LogOnButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            __LogOnButton.FlatStyle = FlatStyle.System;
            __LogOnButton.Location = new Point(87, 38);
            __LogOnButton.Name = "__LogOnButton";
            __LogOnButton.Size = new Size(60, 23);
            __LogOnButton.TabIndex = 8;
            __LogOnButton.Text = "&Test";
            // 
            // _MessagesTextBox
            // 
            _MessagesTextBox.BackColor = SystemColors.Info;
            _MessagesTextBox.CausesValidation = false;
            _MessagesTextBox.Dock = DockStyle.Fill;
            _MessagesTextBox.Location = new Point(0, 0);
            _MessagesTextBox.Multiline = true;
            _MessagesTextBox.Name = "_MessagesTextBox";
            _MessagesTextBox.ReadOnly = true;
            _MessagesTextBox.ScrollBars = ScrollBars.Both;
            _MessagesTextBox.Size = new Size(442, 199);
            _MessagesTextBox.TabIndex = 16;
            // 
            // Switchboard
            // 
            AcceptButton = __ExitButton;
            AutoScaleDimensions = new SizeF(6.0f, 13.0f);
            AutoScaleMode = AutoScaleMode.Font;
            CancelButton = __CancelButton;
            ClientSize = new Size(442, 265);
            Controls.Add(_MessagesTextBox);
            Controls.Add(_ControlsPanel);
            Icon = (Icon)resources.GetObject("$this.Icon");
            Name = "Switchboard";
            Text = "Form1";
            _ControlsPanel.ResumeLayout(false);
            Load += new EventHandler(Form_Load);
            ResumeLayout(false);
            PerformLayout();
        }

        /// <summary>   The cancel control. </summary>
        private Button __CancelButton;

        /// <summary>   Gets or sets the cancel button. </summary>
        /// <value> The cancel button. </value>
        private Button _CancelButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __CancelButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__CancelButton != null)
                {
                    __CancelButton.Click -= CancelButton_Click;
                }

                __CancelButton = value;
                if (__CancelButton != null)
                {
                    __CancelButton.Click += CancelButton_Click;
                }
            }
        }

        /// <summary>   The exit control. </summary>
        private Button __ExitButton;

        /// <summary>   Gets or sets the exit button. </summary>
        /// <value> The exit button. </value>
        private Button _ExitButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ExitButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ExitButton != null)
                {
                    __ExitButton.Click -= ExitButton_Click;
                }

                __ExitButton = value;
                if (__ExitButton != null)
                {
                    __ExitButton.Click += ExitButton_Click;
                }
            }
        }

        /// <summary>   The test control. </summary>
        private Button __TestButton;

        /// <summary>   Gets or sets the test button. </summary>
        /// <value> The test button. </value>
        private Button _TestButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __TestButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__TestButton != null)
                {
                    __TestButton.Click -= TestButton_Click;
                }

                __TestButton = value;
                if (__TestButton != null)
                {
                    __TestButton.Click += TestButton_Click;
                }
            }
        }

        /// <summary>   The actions combo box. </summary>
        private ComboBox _ActionsComboBox;
        /// <summary>   The open control. </summary>
        private Button __OpenButton;

        /// <summary>   Gets or sets the open button. </summary>
        /// <value> The open button. </value>
        private Button _OpenButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __OpenButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__OpenButton != null)
                {
                    __OpenButton.Click -= OpenButton_Click;
                }

                __OpenButton = value;
                if (__OpenButton != null)
                {
                    __OpenButton.Click += OpenButton_Click;
                }
            }
        }

        /// <summary>   The controls panel. </summary>
        private Panel _ControlsPanel;
        /// <summary>   The messages control. </summary>
        private TextBox _MessagesTextBox;
        /// <summary>   The log on control. </summary>
        private Button __LogOnButton;

        /// <summary>   Gets or sets the log on button. </summary>
        /// <value> The log on button. </value>
        private Button _LogOnButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __LogOnButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__LogOnButton != null)
                {
                    __LogOnButton.Click -= LogOnButton_Click;
                }

                __LogOnButton = value;
                if (__LogOnButton != null)
                {
                    __LogOnButton.Click += LogOnButton_Click;
                }
            }
        }
    }
}
