using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace ControlsTester
{
    /// <summary>   Form for viewing the selector. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    [DesignerGenerated()]
    public partial class SelectorForm : Form
    {

        /// <summary>
        /// Disposes of the resources (other than memory) used by the
        /// <see cref="T:System.Windows.Forms.Form" />.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="disposing">    <see langword="true" /> to release both managed and unmanaged
        ///                                                         resources; <see langword="false" /> to
        ///                                                         release only unmanaged resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components is object)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        /// <summary>   Required by the Windows Form Designer. </summary>
        private System.ComponentModel.IContainer components;

        /// <summary>
        /// NOTE: The following procedure is required by the Windows Form Designer It can be modified
        /// using the Windows Form Designer.  
        /// Do not modify it using the code editor.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(SelectorForm));
            _ToolTip = new ToolTip(components);
            _SelectedTextBox = new TextBox();
            __ReadOnlyCheckBox = new CheckBox();
            __ReadOnlyCheckBox.CheckedChanged += new EventHandler(ReadOnlyCheckBox_CheckedChanged);
            _EnterValueNumericLabel = new Label();
            _EditedEngineeringUpDownLabel = new Label();
            _ScaledValueTextBoxLabel = new Label();
            __ApplyButton = new Button();
            __ApplyButton.Click += new EventHandler(ApplyButton_Click);
            _EnterValueLabel = new ToolStripTextBox();
            _SelectedValueLabel = new ToolStripTextBox();
            __SelectorComboBox = new isr.Core.Controls.SelectorComboBox();
            __SelectorComboBox.ValueSelected += new EventHandler<EventArgs>(SelectorComboBox_ValueSelected);
            __SelectButton = new Button();
            __SelectButton.Click += new EventHandler(SelectButton_Click);
            _EnterNumeric = new isr.Core.Controls.EngineeringUpDown();
            __SelectorNumeric = new isr.Core.Controls.SelectorNumeric();
            __SelectorNumeric.ValueSelected += new EventHandler<EventArgs>(SelectorNumeric_ValueSelected);
            ToolStrip1 = new ToolStrip();
            __ToolStripSelectorComboBox = new isr.Core.Controls.ToolStripSelectorComboBox();
            __ToolStripSelectorComboBox.ValueSelected += new EventHandler<EventArgs>(ToolStripSelectorComboBox_ValueSelected);
            __ToolStripSelectorNumeric = new isr.Core.Controls.ToolStripSelectorNumeric();
            __ToolStripSelectorNumeric.ValueSelected += new EventHandler<EventArgs>(ToolStripSelectorNumeric_ValueSelected);
            ((System.ComponentModel.ISupportInitialize)_EnterNumeric).BeginInit();
            ToolStrip1.SuspendLayout();
            SuspendLayout();
            // 
            // _SelectedTextBox
            // 
            _SelectedTextBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            _SelectedTextBox.ForeColor = SystemColors.ActiveCaption;
            _SelectedTextBox.Location = new Point(147, 122);
            _SelectedTextBox.Name = "_SelectedTextBox";
            _SelectedTextBox.Size = new Size(121, 25);
            _SelectedTextBox.TabIndex = 4;
            _SelectedTextBox.Text = "ABC";
            _ToolTip.SetToolTip(_SelectedTextBox, "Scaled value");
            // 
            // _ReadOnlyCheckBox
            // 
            __ReadOnlyCheckBox.AutoSize = true;
            __ReadOnlyCheckBox.Location = new Point(16, 171);
            __ReadOnlyCheckBox.Name = "__ReadOnlyCheckBox";
            __ReadOnlyCheckBox.Size = new Size(87, 21);
            __ReadOnlyCheckBox.TabIndex = 8;
            __ReadOnlyCheckBox.Text = "Read Only";
            __ReadOnlyCheckBox.UseVisualStyleBackColor = true;
            // 
            // _EnterValueNumericLabel
            // 
            _EnterValueNumericLabel.AutoSize = true;
            _EnterValueNumericLabel.Location = new Point(10, 65);
            _EnterValueNumericLabel.Name = "_EnterValueNumericLabel";
            _EnterValueNumericLabel.Size = new Size(112, 17);
            _EnterValueNumericLabel.TabIndex = 1;
            _EnterValueNumericLabel.Text = "Enter value to set:";
            _EnterValueNumericLabel.TextAlign = ContentAlignment.TopRight;
            // 
            // _EditedEngineeringUpDownLabel
            // 
            _EditedEngineeringUpDownLabel.AutoSize = true;
            _EditedEngineeringUpDownLabel.Location = new Point(13, 102);
            _EditedEngineeringUpDownLabel.Name = "_EditedEngineeringUpDownLabel";
            _EditedEngineeringUpDownLabel.Size = new Size(69, 17);
            _EditedEngineeringUpDownLabel.TabIndex = 1;
            _EditedEngineeringUpDownLabel.Text = "Edit Value:";
            _EditedEngineeringUpDownLabel.TextAlign = ContentAlignment.TopRight;
            // 
            // _ScaledValueTextBoxLabel
            // 
            _ScaledValueTextBoxLabel.AutoSize = true;
            _ScaledValueTextBoxLabel.Location = new Point(145, 102);
            _ScaledValueTextBoxLabel.Name = "_ScaledValueTextBoxLabel";
            _ScaledValueTextBoxLabel.Size = new Size(104, 17);
            _ScaledValueTextBoxLabel.TabIndex = 1;
            _ScaledValueTextBoxLabel.Text = "Observed Value:";
            _ScaledValueTextBoxLabel.TextAlign = ContentAlignment.TopRight;
            // 
            // _ApplyButton
            // 
            __ApplyButton.Location = new Point(146, 163);
            __ApplyButton.Name = "__ApplyButton";
            __ApplyButton.Size = new Size(122, 35);
            __ApplyButton.TabIndex = 10;
            __ApplyButton.Text = "Apply Edit Value";
            __ApplyButton.UseVisualStyleBackColor = true;
            // 
            // _EnterValueLabel
            // 
            _EnterValueLabel.Name = "_EnterValueLabel";
            _EnterValueLabel.Size = new Size(47, 28);
            _EnterValueLabel.Text = "Entered";
            // 
            // _SelectedValueLabel
            // 
            _SelectedValueLabel.Name = "_SelectedValueLabel";
            _SelectedValueLabel.Size = new Size(51, 28);
            _SelectedValueLabel.Text = "Selected";
            // 
            // _SelectorComboBox
            // 
            __SelectorComboBox.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            __SelectorComboBox.DirtyBackColor = Color.Orange;
            __SelectorComboBox.DirtyForeColor = SystemColors.ActiveCaption;
            __SelectorComboBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            __SelectorComboBox.Location = new Point(311, 122);
            __SelectorComboBox.Margin = new Padding(3, 4, 3, 4);
            __SelectorComboBox.Name = "__SelectorComboBox";
            __SelectorComboBox.SelectedText = null;
            __SelectorComboBox.SelectorIcon = (Image)resources.GetObject("_SelectorComboBox.SelectorIcon");
            __SelectorComboBox.Size = new Size(99, 25);
            __SelectorComboBox.TabIndex = 11;
            // 
            // _SelectButton
            // 
            __SelectButton.Location = new Point(311, 162);
            __SelectButton.Name = "__SelectButton";
            __SelectButton.Size = new Size(75, 36);
            __SelectButton.TabIndex = 12;
            __SelectButton.Text = "Select";
            __SelectButton.UseVisualStyleBackColor = true;
            // 
            // _EnterNumeric
            // 
            _EnterNumeric.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            _EnterNumeric.ForeColor = SystemColors.WindowText;
            _EnterNumeric.Location = new Point(16, 122);
            _EnterNumeric.Maximum = new decimal(new int[] { 1000, 0, 0, 0 });
            _EnterNumeric.Minimum = new decimal(new int[] { 1000, 0, 0, (int)-2147483648L });
            _EnterNumeric.Name = "_EnterNumeric";
            _EnterNumeric.NullValue = new decimal(new int[] { 0, 0, 0, 0 });
            _EnterNumeric.ReadOnlyBackColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(224)), Conversions.ToInteger(Conversions.ToByte(224)), Conversions.ToInteger(Conversions.ToByte(224)));
            _EnterNumeric.ReadOnlyForeColor = Color.Black;
            _EnterNumeric.ReadWriteBackColor = SystemColors.Window;
            _EnterNumeric.ReadWriteForeColor = Color.Black;
            _EnterNumeric.ScaledValue = new decimal(new int[] { 0, 0, 0, 0 });
            _EnterNumeric.Size = new Size(120, 25);
            _EnterNumeric.TabIndex = 6;
            _EnterNumeric.UnscaledValue = new decimal(new int[] { 0, 0, 0, 0 });
            _EnterNumeric.UpDownDisplayMode = isr.Core.Controls.UpDownButtonsDisplayMode.WhenMouseOver;
            _EnterNumeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
            // 
            // _SelectorNumeric
            // 
            __SelectorNumeric.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            __SelectorNumeric.DirtyBackColor = Color.Orange;
            __SelectorNumeric.DirtyForeColor = SystemColors.ActiveCaption;
            __SelectorNumeric.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            __SelectorNumeric.Location = new Point(442, 122);
            __SelectorNumeric.Margin = new Padding(3, 4, 3, 4);
            __SelectorNumeric.Name = "__SelectorNumeric";
            __SelectorNumeric.SelectedText = null;
            __SelectorNumeric.SelectedValue = default;
            __SelectorNumeric.SelectorIcon = (Image)resources.GetObject("_SelectorNumeric.SelectorIcon");
            __SelectorNumeric.Size = new Size(76, 25);
            __SelectorNumeric.TabIndex = 13;
            __SelectorNumeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
            // 
            // ToolStrip1
            // 
            ToolStrip1.Items.AddRange(new ToolStripItem[] { __ToolStripSelectorComboBox, __ToolStripSelectorNumeric });
            ToolStrip1.Location = new Point(0, 0);
            ToolStrip1.Name = "ToolStrip1";
            ToolStrip1.Size = new Size(622, 28);
            ToolStrip1.TabIndex = 14;
            ToolStrip1.Text = "ToolStrip1";
            // 
            // _ToolStripSelectorComboBox
            // 
            __ToolStripSelectorComboBox.AutoSize = false;
            __ToolStripSelectorComboBox.DirtyBackColor = Color.Orange;
            __ToolStripSelectorComboBox.DirtyForeColor = SystemColors.ActiveCaption;
            __ToolStripSelectorComboBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            __ToolStripSelectorComboBox.Name = "__ToolStripSelectorComboBox";
            __ToolStripSelectorComboBox.SelectedText = null;
            __ToolStripSelectorComboBox.SelectorIcon = (Image)resources.GetObject("_ToolStripSelectorComboBox.SelectorIcon");
            __ToolStripSelectorComboBox.Size = new Size(91, 25);
            // 
            // _ToolStripSelectorNumeric
            // 
            __ToolStripSelectorNumeric.AutoSize = false;
            __ToolStripSelectorNumeric.DirtyBackColor = Color.Orange;
            __ToolStripSelectorNumeric.DirtyForeColor = SystemColors.ActiveCaption;
            __ToolStripSelectorNumeric.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            __ToolStripSelectorNumeric.Name = "__ToolStripSelectorNumeric";
            __ToolStripSelectorNumeric.SelectedText = null;
            __ToolStripSelectorNumeric.SelectorIcon = (Image)resources.GetObject("_ToolStripSelectorNumeric.SelectorIcon");
            __ToolStripSelectorNumeric.Size = new Size(74, 25);
            __ToolStripSelectorNumeric.Text = "0";
            __ToolStripSelectorNumeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
            // 
            // SelectorForm
            // 
            AutoScaleDimensions = new SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(622, 263);
            Controls.Add(ToolStrip1);
            Controls.Add(__SelectorNumeric);
            Controls.Add(__SelectButton);
            Controls.Add(__SelectorComboBox);
            Controls.Add(__ApplyButton);
            Controls.Add(__ReadOnlyCheckBox);
            Controls.Add(_EnterNumeric);
            Controls.Add(_SelectedTextBox);
            Controls.Add(_ScaledValueTextBoxLabel);
            Controls.Add(_EditedEngineeringUpDownLabel);
            Controls.Add(_EnterValueNumericLabel);
            Font = new Font("Segoe UI", 9.75f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            FormBorderStyle = FormBorderStyle.FixedDialog;
            Margin = new Padding(3, 4, 3, 4);
            Name = "SelectorForm";
            Text = "Engineering Up Down Form";
            ((System.ComponentModel.ISupportInitialize)_EnterNumeric).EndInit();
            ToolStrip1.ResumeLayout(false);
            ToolStrip1.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        /// <summary>   The tool tip control. </summary>
        private ToolTip _ToolTip;
        /// <summary>   The enter numeric. </summary>
        private isr.Core.Controls.EngineeringUpDown _EnterNumeric;
        /// <summary>   The selected text box. </summary>
        private TextBox _SelectedTextBox;
        /// <summary>   The read only control. </summary>
        private CheckBox __ReadOnlyCheckBox;

        /// <summary>   Gets or sets the read only check box. </summary>
        /// <value> The read only check box. </value>
        private CheckBox _ReadOnlyCheckBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadOnlyCheckBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadOnlyCheckBox != null)
                {
                    __ReadOnlyCheckBox.CheckedChanged -= ReadOnlyCheckBox_CheckedChanged;
                }

                __ReadOnlyCheckBox = value;
                if (__ReadOnlyCheckBox != null)
                {
                    __ReadOnlyCheckBox.CheckedChanged += ReadOnlyCheckBox_CheckedChanged;
                }
            }
        }

        /// <summary>   The enter value numeric label. </summary>
        private Label _EnterValueNumericLabel;
        /// <summary>   The edited engineering up down label. </summary>
        private Label _EditedEngineeringUpDownLabel;
        /// <summary>   The scaled value text box label. </summary>
        private Label _ScaledValueTextBoxLabel;
        /// <summary>   The apply control. </summary>
        private Button __ApplyButton;

        /// <summary>   Gets or sets the apply button. </summary>
        /// <value> The apply button. </value>
        private Button _ApplyButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplyButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplyButton != null)
                {
                    __ApplyButton.Click -= ApplyButton_Click;
                }

                __ApplyButton = value;
                if (__ApplyButton != null)
                {
                    __ApplyButton.Click += ApplyButton_Click;
                }
            }
        }

        /// <summary>   The enter value label. </summary>
        private ToolStripTextBox _EnterValueLabel;
        /// <summary>   The selected value label. </summary>
        private ToolStripTextBox _SelectedValueLabel;
        /// <summary>   The selector combo box. </summary>
        private isr.Core.Controls.SelectorComboBox __SelectorComboBox;

        /// <summary>   Gets or sets the selector combo box. </summary>
        /// <value> The selector combo box. </value>
        private isr.Core.Controls.SelectorComboBox _SelectorComboBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SelectorComboBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SelectorComboBox != null)
                {
                    __SelectorComboBox.ValueSelected -= SelectorComboBox_ValueSelected;
                }

                __SelectorComboBox = value;
                if (__SelectorComboBox != null)
                {
                    __SelectorComboBox.ValueSelected += SelectorComboBox_ValueSelected;
                }
            }
        }

        /// <summary>   The select control. </summary>
        private Button __SelectButton;

        /// <summary>   Gets or sets the select button. </summary>
        /// <value> The select button. </value>
        private Button _SelectButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SelectButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SelectButton != null)
                {
                    __SelectButton.Click -= SelectButton_Click;
                }

                __SelectButton = value;
                if (__SelectButton != null)
                {
                    __SelectButton.Click += SelectButton_Click;
                }
            }
        }

        /// <summary>   The selector numeric. </summary>
        private isr.Core.Controls.SelectorNumeric __SelectorNumeric;

        /// <summary>   Gets or sets the selector numeric. </summary>
        /// <value> The selector numeric. </value>
        private isr.Core.Controls.SelectorNumeric _SelectorNumeric
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SelectorNumeric;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SelectorNumeric != null)
                {
                    __SelectorNumeric.ValueSelected -= SelectorNumeric_ValueSelected;
                }

                __SelectorNumeric = value;
                if (__SelectorNumeric != null)
                {
                    __SelectorNumeric.ValueSelected += SelectorNumeric_ValueSelected;
                }
            }
        }

        /// <summary>   The first tool strip. </summary>
        private ToolStrip ToolStrip1;
        /// <summary>   The tool strip selector combo box. </summary>
        private isr.Core.Controls.ToolStripSelectorComboBox __ToolStripSelectorComboBox;

        /// <summary>   Gets or sets the tool strip selector combo box. </summary>
        /// <value> The tool strip selector combo box. </value>
        private isr.Core.Controls.ToolStripSelectorComboBox _ToolStripSelectorComboBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ToolStripSelectorComboBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ToolStripSelectorComboBox != null)
                {
                    __ToolStripSelectorComboBox.ValueSelected -= ToolStripSelectorComboBox_ValueSelected;
                }

                __ToolStripSelectorComboBox = value;
                if (__ToolStripSelectorComboBox != null)
                {
                    __ToolStripSelectorComboBox.ValueSelected += ToolStripSelectorComboBox_ValueSelected;
                }
            }
        }

        /// <summary>   The tool strip selector numeric. </summary>
        private isr.Core.Controls.ToolStripSelectorNumeric __ToolStripSelectorNumeric;

        /// <summary>   Gets or sets the tool strip selector numeric. </summary>
        /// <value> The tool strip selector numeric. </value>
        private isr.Core.Controls.ToolStripSelectorNumeric _ToolStripSelectorNumeric
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ToolStripSelectorNumeric;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ToolStripSelectorNumeric != null)
                {
                    __ToolStripSelectorNumeric.ValueSelected -= ToolStripSelectorNumeric_ValueSelected;
                }

                __ToolStripSelectorNumeric = value;
                if (__ToolStripSelectorNumeric != null)
                {
                    __ToolStripSelectorNumeric.ValueSelected += ToolStripSelectorNumeric_ValueSelected;
                }
            }
        }
    }
}
