using System;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace ControlsTester
{
    public partial class ConsoleControlForm
    {

        /// <summary>
    /// Required designer variable.
    /// </summary>
        private System.ComponentModel.IContainer components = null;


        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            __ClearConsoleButton = new Button();
            __ClearConsoleButton.Click += new EventHandler(BtnClearConsole_Click);
            _CursorGroupBox = new GroupBox();
            _CursorTypegroupBox = new GroupBox();
            __NoneRadioButton = new RadioButton();
            __NoneRadioButton.CheckedChanged += new EventHandler(RbNone_CheckedChanged);
            __BlockRadioButton = new RadioButton();
            __BlockRadioButton.CheckedChanged += new EventHandler(RbBlock_CheckedChanged);
            __UnderlineRadioButton = new RadioButton();
            __UnderlineRadioButton.CheckedChanged += new EventHandler(RbUnderline_CheckedChanged);
            __MoveCursorButton = new Button();
            __MoveCursorButton.Click += new EventHandler(BtnMoveCursor_Click);
            _CursorColumnTextBox = new TextBox();
            _CursorRowTextBox = new TextBox();
            _CursorColumnTextBoxLabel = new Label();
            _CursorRowTextBoxLabel = new Label();
            _MessageTextBoxLabel = new Label();
            _MessageTextBox = new TextBox();
            __WriteMessageButton = new Button();
            __WriteMessageButton.Click += new EventHandler(BtnWriteMessage_Click);
            _BackgroundColorPanelLabel = new Label();
            __BackgroundColorPanel = new Panel();
            __BackgroundColorPanel.DoubleClick += new EventHandler(PnlBackgroundColor_DoubleClick);
            __ForegroundColorPanel = new Panel();
            __ForegroundColorPanel.DoubleClick += new EventHandler(PnlForegroundColor_DoubleClick);
            _ForegroundColorPanelLabel = new Label();
            _ColorDialog = new ColorDialog();
            __RightConsoleControl = new isr.Core.Controls.ConsoleControl();
            __RightConsoleControl.Enter += new EventHandler(ConsoleControl2_Enter);
            __RightConsoleControl.Leave += new EventHandler(ConsoleControl2_Leave);
            __LeftConsoleControl = new isr.Core.Controls.ConsoleControl();
            __LeftConsoleControl.Enter += new EventHandler(ConsoleControl1_Enter);
            __LeftConsoleControl.Leave += new EventHandler(ConsoleControl1_Leave);
            _CursorGroupBox.SuspendLayout();
            _CursorTypegroupBox.SuspendLayout();
            SuspendLayout();
            // 
            // _ClearConsoleButton
            // 
            __ClearConsoleButton.Font = new Font("Microsoft Sans Serif", 10.0f, FontStyle.Bold, GraphicsUnit.Point, 0);
            __ClearConsoleButton.Location = new Point(11, 388);
            __ClearConsoleButton.Name = "__ClearConsoleButton";
            __ClearConsoleButton.Size = new Size(625, 31);
            __ClearConsoleButton.TabIndex = 1;
            __ClearConsoleButton.Text = "Clear Console Screen";
            __ClearConsoleButton.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            _CursorGroupBox.Controls.Add(_CursorTypegroupBox);
            _CursorGroupBox.Controls.Add(__MoveCursorButton);
            _CursorGroupBox.Controls.Add(_CursorColumnTextBox);
            _CursorGroupBox.Controls.Add(_CursorRowTextBox);
            _CursorGroupBox.Controls.Add(_CursorColumnTextBoxLabel);
            _CursorGroupBox.Controls.Add(_CursorRowTextBoxLabel);
            _CursorGroupBox.Location = new Point(12, 425);
            _CursorGroupBox.Name = "_CursorGroupBox";
            _CursorGroupBox.Size = new Size(226, 157);
            _CursorGroupBox.TabIndex = 2;
            _CursorGroupBox.TabStop = false;
            _CursorGroupBox.Text = "Cursor";
            // 
            // groupBox2
            // 
            _CursorTypegroupBox.Controls.Add(__NoneRadioButton);
            _CursorTypegroupBox.Controls.Add(__BlockRadioButton);
            _CursorTypegroupBox.Controls.Add(__UnderlineRadioButton);
            _CursorTypegroupBox.Location = new Point(13, 95);
            _CursorTypegroupBox.Name = "_CursorTypegroupBox";
            _CursorTypegroupBox.Size = new Size(197, 50);
            _CursorTypegroupBox.TabIndex = 5;
            _CursorTypegroupBox.TabStop = false;
            _CursorTypegroupBox.Text = "Cursor Type";
            // 
            // _NoneRadioButton
            // 
            __NoneRadioButton.AutoSize = true;
            __NoneRadioButton.Location = new Point(138, 21);
            __NoneRadioButton.Name = "__NoneRadioButton";
            __NoneRadioButton.Size = new Size(51, 17);
            __NoneRadioButton.TabIndex = 2;
            __NoneRadioButton.Text = "None";
            __NoneRadioButton.UseVisualStyleBackColor = true;
            // 
            // rbBlock
            // 
            __BlockRadioButton.AutoSize = true;
            __BlockRadioButton.Location = new Point(84, 21);
            __BlockRadioButton.Name = "__BlockRadioButton";
            __BlockRadioButton.Size = new Size(52, 17);
            __BlockRadioButton.TabIndex = 1;
            __BlockRadioButton.Text = "Block";
            __BlockRadioButton.UseVisualStyleBackColor = true;
            // 
            // _UnderlineRadioButton
            // 
            __UnderlineRadioButton.AutoSize = true;
            __UnderlineRadioButton.Checked = true;
            __UnderlineRadioButton.Location = new Point(12, 20);
            __UnderlineRadioButton.Name = "__UnderlineRadioButton";
            __UnderlineRadioButton.Size = new Size(70, 17);
            __UnderlineRadioButton.TabIndex = 0;
            __UnderlineRadioButton.TabStop = true;
            __UnderlineRadioButton.Text = "Underline";
            __UnderlineRadioButton.UseVisualStyleBackColor = true;
            // 
            // _MoveCursorButton
            // 
            __MoveCursorButton.Location = new Point(13, 66);
            __MoveCursorButton.Name = "__MoveCursorButton";
            __MoveCursorButton.Size = new Size(103, 23);
            __MoveCursorButton.TabIndex = 4;
            __MoveCursorButton.Text = "Move Cursor";
            __MoveCursorButton.UseVisualStyleBackColor = true;
            // 
            // _CursorColumnTextBox
            // 
            _CursorColumnTextBox.Location = new Point(85, 43);
            _CursorColumnTextBox.Name = "_CursorColumnTextBox";
            _CursorColumnTextBox.Size = new Size(31, 20);
            _CursorColumnTextBox.TabIndex = 3;
            // 
            // _CursorRowTextBox
            // 
            _CursorRowTextBox.Location = new Point(85, 21);
            _CursorRowTextBox.Name = "_CursorRowTextBox";
            _CursorRowTextBox.Size = new Size(31, 20);
            _CursorRowTextBox.TabIndex = 2;
            // 
            // _CursorColumnTextBoxLabel
            // 
            _CursorColumnTextBoxLabel.AutoSize = true;
            _CursorColumnTextBoxLabel.Location = new Point(10, 46);
            _CursorColumnTextBoxLabel.Name = "_CursorColumnTextBoxLabel";
            _CursorColumnTextBoxLabel.Size = new Size(75, 13);
            _CursorColumnTextBoxLabel.TabIndex = 1;
            _CursorColumnTextBoxLabel.Text = "Column (0-79):";
            // 
            // _CursorRowTextBoxLabel
            // 
            _CursorRowTextBoxLabel.AutoSize = true;
            _CursorRowTextBoxLabel.Location = new Point(22, 24);
            _CursorRowTextBoxLabel.Name = "_CursorRowTextBoxLabel";
            _CursorRowTextBoxLabel.Size = new Size(62, 13);
            _CursorRowTextBoxLabel.TabIndex = 0;
            _CursorRowTextBoxLabel.Text = "Row (0-24):";
            // 
            // _MessageTextBoxLabel
            // 
            _MessageTextBoxLabel.AutoSize = true;
            _MessageTextBoxLabel.Location = new Point(293, 480);
            _MessageTextBoxLabel.Name = "_MessageTextBoxLabel";
            _MessageTextBoxLabel.Size = new Size(31, 13);
            _MessageTextBoxLabel.TabIndex = 3;
            _MessageTextBoxLabel.Text = "Text:";
            // 
            // _MessageTextBox
            // 
            _MessageTextBox.Location = new Point(330, 478);
            _MessageTextBox.Name = "_MessageTextBox";
            _MessageTextBox.Size = new Size(306, 20);
            _MessageTextBox.TabIndex = 4;
            // 
            // _WriteMessageButton
            // 
            __WriteMessageButton.Location = new Point(329, 501);
            __WriteMessageButton.Name = "__WriteMessageButton";
            __WriteMessageButton.Size = new Size(306, 43);
            __WriteMessageButton.TabIndex = 5;
            __WriteMessageButton.Text = "Write Text to Console Screen at Current Cursor Position Using The Default Colors";
            __WriteMessageButton.UseVisualStyleBackColor = true;
            // 
            // _BackgroundColorPanelLabel
            // 
            _BackgroundColorPanelLabel.AutoSize = true;
            _BackgroundColorPanelLabel.Location = new Point(240, 431);
            _BackgroundColorPanelLabel.Name = "_BackgroundColorPanelLabel";
            _BackgroundColorPanelLabel.Size = new Size(95, 13);
            _BackgroundColorPanelLabel.TabIndex = 6;
            _BackgroundColorPanelLabel.Text = "Background Color:";
            // 
            // _BackgroundColorPanel
            // 
            __BackgroundColorPanel.Location = new Point(335, 430);
            __BackgroundColorPanel.Name = "__BackgroundColorPanel";
            __BackgroundColorPanel.Size = new Size(40, 20);
            __BackgroundColorPanel.TabIndex = 7;
            // 
            // _ForegroundColorPanel
            // 
            __ForegroundColorPanel.Location = new Point(335, 453);
            __ForegroundColorPanel.Name = "__ForegroundColorPanel";
            __ForegroundColorPanel.Size = new Size(40, 20);
            __ForegroundColorPanel.TabIndex = 9;
            // 
            // _ForegroundColorPanelLabel
            // 
            _ForegroundColorPanelLabel.AutoSize = true;
            _ForegroundColorPanelLabel.Location = new Point(245, 454);
            _ForegroundColorPanelLabel.Name = "_ForegroundColorPanelLabel";
            _ForegroundColorPanelLabel.Size = new Size(91, 13);
            _ForegroundColorPanelLabel.TabIndex = 8;
            _ForegroundColorPanelLabel.Text = "Foreground Color:";
            // 
            // _ColorDialog
            // 
            _ColorDialog.AnyColor = true;
            _ColorDialog.FullOpen = true;
            // 
            // _RightConsoleControl
            // 
            __RightConsoleControl.AllowInput = true;
            __RightConsoleControl.BackColor = Color.Black;
            __RightConsoleControl.ConsoleBackgroundColor = Color.Black;
            __RightConsoleControl.ConsoleForegroundColor = Color.LightGray;
            __RightConsoleControl.CurrentBackgroundColor = Color.Black;
            __RightConsoleControl.CurrentForegroundColor = Color.LightGray;
            __RightConsoleControl.CursorType = isr.Core.Controls.CursorType.Underline;
            __RightConsoleControl.EchoInput = true;
            __RightConsoleControl.ForeColor = Color.LightGray;
            __RightConsoleControl.Location = new Point(678, 6);
            __RightConsoleControl.Name = "__RightConsoleControl";
            __RightConsoleControl.ShowCursor = true;
            __RightConsoleControl.Size = new Size(646, 377);
            __RightConsoleControl.TabIndex = 10;
            // 
            // _LeftconsoleControl
            // 
            __LeftConsoleControl.AllowInput = true;
            __LeftConsoleControl.BackColor = Color.Black;
            __LeftConsoleControl.ConsoleBackgroundColor = Color.Black;
            __LeftConsoleControl.ConsoleForegroundColor = Color.LightGray;
            __LeftConsoleControl.CurrentBackgroundColor = Color.Black;
            __LeftConsoleControl.CurrentForegroundColor = Color.LightGray;
            __LeftConsoleControl.CursorType = isr.Core.Controls.CursorType.Underline;
            __LeftConsoleControl.EchoInput = true;
            __LeftConsoleControl.ForeColor = Color.LightGray;
            __LeftConsoleControl.Location = new Point(6, 6);
            __LeftConsoleControl.Name = "__LeftconsoleControl";
            __LeftConsoleControl.ShowCursor = true;
            __LeftConsoleControl.Size = new Size(646, 377);
            __LeftConsoleControl.TabIndex = 0;
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(6.0f, 13.0f);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1334, 587);
            Controls.Add(__RightConsoleControl);
            Controls.Add(__ForegroundColorPanel);
            Controls.Add(_ForegroundColorPanelLabel);
            Controls.Add(__BackgroundColorPanel);
            Controls.Add(_BackgroundColorPanelLabel);
            Controls.Add(__WriteMessageButton);
            Controls.Add(_MessageTextBox);
            Controls.Add(_MessageTextBoxLabel);
            Controls.Add(_CursorGroupBox);
            Controls.Add(__ClearConsoleButton);
            Controls.Add(__LeftConsoleControl);
            DoubleBuffered = true;
            FormBorderStyle = FormBorderStyle.FixedSingle;
            MaximizeBox = false;
            Name = "Form1";
            Text = "Console Control Demo";
            _CursorGroupBox.ResumeLayout(false);
            _CursorGroupBox.PerformLayout();
            _CursorTypegroupBox.ResumeLayout(false);
            _CursorTypegroupBox.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        private isr.Core.Controls.ConsoleControl __LeftConsoleControl;

        private isr.Core.Controls.ConsoleControl _LeftConsoleControl
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __LeftConsoleControl;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__LeftConsoleControl != null)
                {
                    __LeftConsoleControl.Enter -= ConsoleControl1_Enter;
                    __LeftConsoleControl.Leave -= ConsoleControl1_Leave;
                }

                __LeftConsoleControl = value;
                if (__LeftConsoleControl != null)
                {
                    __LeftConsoleControl.Enter += ConsoleControl1_Enter;
                    __LeftConsoleControl.Leave += ConsoleControl1_Leave;
                }
            }
        }

        private Button __ClearConsoleButton;

        private Button _ClearConsoleButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ClearConsoleButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ClearConsoleButton != null)
                {
                    __ClearConsoleButton.Click -= BtnClearConsole_Click;
                }

                __ClearConsoleButton = value;
                if (__ClearConsoleButton != null)
                {
                    __ClearConsoleButton.Click += BtnClearConsole_Click;
                }
            }
        }

        private GroupBox _CursorGroupBox;
        private Button __MoveCursorButton;

        private Button _MoveCursorButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __MoveCursorButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__MoveCursorButton != null)
                {
                    __MoveCursorButton.Click -= BtnMoveCursor_Click;
                }

                __MoveCursorButton = value;
                if (__MoveCursorButton != null)
                {
                    __MoveCursorButton.Click += BtnMoveCursor_Click;
                }
            }
        }

        private TextBox _CursorColumnTextBox;
        private TextBox _CursorRowTextBox;
        private Label _CursorColumnTextBoxLabel;
        private Label _CursorRowTextBoxLabel;
        private GroupBox _CursorTypegroupBox;
        private RadioButton __NoneRadioButton;

        private RadioButton _NoneRadioButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __NoneRadioButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__NoneRadioButton != null)
                {
                    __NoneRadioButton.CheckedChanged -= RbNone_CheckedChanged;
                }

                __NoneRadioButton = value;
                if (__NoneRadioButton != null)
                {
                    __NoneRadioButton.CheckedChanged += RbNone_CheckedChanged;
                }
            }
        }

        private RadioButton __BlockRadioButton;

        private RadioButton _BlockRadioButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __BlockRadioButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__BlockRadioButton != null)
                {
                    __BlockRadioButton.CheckedChanged -= RbBlock_CheckedChanged;
                }

                __BlockRadioButton = value;
                if (__BlockRadioButton != null)
                {
                    __BlockRadioButton.CheckedChanged += RbBlock_CheckedChanged;
                }
            }
        }

        private RadioButton __UnderlineRadioButton;

        private RadioButton _UnderlineRadioButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __UnderlineRadioButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__UnderlineRadioButton != null)
                {
                    __UnderlineRadioButton.CheckedChanged -= RbUnderline_CheckedChanged;
                }

                __UnderlineRadioButton = value;
                if (__UnderlineRadioButton != null)
                {
                    __UnderlineRadioButton.CheckedChanged += RbUnderline_CheckedChanged;
                }
            }
        }

        private Label _MessageTextBoxLabel;
        private TextBox _MessageTextBox;
        private Button __WriteMessageButton;

        private Button _WriteMessageButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __WriteMessageButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__WriteMessageButton != null)
                {
                    __WriteMessageButton.Click -= BtnWriteMessage_Click;
                }

                __WriteMessageButton = value;
                if (__WriteMessageButton != null)
                {
                    __WriteMessageButton.Click += BtnWriteMessage_Click;
                }
            }
        }

        private Label _BackgroundColorPanelLabel;
        private Panel __BackgroundColorPanel;

        private Panel _BackgroundColorPanel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __BackgroundColorPanel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__BackgroundColorPanel != null)
                {
                    __BackgroundColorPanel.DoubleClick -= PnlBackgroundColor_DoubleClick;
                }

                __BackgroundColorPanel = value;
                if (__BackgroundColorPanel != null)
                {
                    __BackgroundColorPanel.DoubleClick += PnlBackgroundColor_DoubleClick;
                }
            }
        }

        private Panel __ForegroundColorPanel;

        private Panel _ForegroundColorPanel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ForegroundColorPanel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ForegroundColorPanel != null)
                {
                    __ForegroundColorPanel.DoubleClick -= PnlForegroundColor_DoubleClick;
                }

                __ForegroundColorPanel = value;
                if (__ForegroundColorPanel != null)
                {
                    __ForegroundColorPanel.DoubleClick += PnlForegroundColor_DoubleClick;
                }
            }
        }

        private Label _ForegroundColorPanelLabel;
        private ColorDialog _ColorDialog;
        private isr.Core.Controls.ConsoleControl __RightConsoleControl;

        private isr.Core.Controls.ConsoleControl _RightConsoleControl
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __RightConsoleControl;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__RightConsoleControl != null)
                {
                    __RightConsoleControl.Enter -= ConsoleControl2_Enter;
                    __RightConsoleControl.Leave -= ConsoleControl2_Leave;
                }

                __RightConsoleControl = value;
                if (__RightConsoleControl != null)
                {
                    __RightConsoleControl.Enter += ConsoleControl2_Enter;
                    __RightConsoleControl.Leave += ConsoleControl2_Leave;
                }
            }
        }
    }
}
