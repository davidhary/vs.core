using System;
using System.Windows.Forms;

namespace ControlsTester
{

    /// <summary>   Form for viewing the nullable numeric up down. </summary>
    /// <remarks>   David, 2020-10-25. </remarks>
    public partial class NullableNumericUpDownForm
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        public NullableNumericUpDownForm()
        {
            this.InitializeComponent();
            this.__NullableNumericUpDown.Name = "_NullableNumericUpDown";
            this.__NilifyButton.Name = "_NilifyButton";
        }

        /// <summary>   Event handler. Called by NullableNumericUpDownForm for shown events. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void NullableNumericUpDownForm_Shown( object sender, EventArgs e )
        {
            this._Timer.Interval = 1000;
            this._Timer.Enabled = true;
        }

        /// <summary>   Nullable numeric up down validated. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void NullableNumericUpDown_Validated( object sender, EventArgs e )
        {
            this._ValidatedNullableValueTextBox.Text = this._NullableNumericUpDown.NullableValue.ToString();
            this._ValidatedValueTextBox.Text = this._NullableNumericUpDown.Value.ToString();
            this._NullableTextTextBox.Text = this._NullableNumericUpDown.Text;
        }

        /// <summary>   Nullable numeric up down value changed. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void NullableNumericUpDown_ValueChanged( object sender, EventArgs e )
        {
            this._ChangedNullableValueTextBox.Text = this._NullableNumericUpDown.NullableValue.ToString();
            this._ChangedValueTextBox.Text = this._NullableNumericUpDown.Value.ToString();
            this._NullableTextTextBox.Text = this._NullableNumericUpDown.Text;
        }

        /// <summary>   Nullable numeric up down value decrementing. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Cancel event information. </param>
        private void NullableNumericUpDown_ValueDecrementing( object sender, System.ComponentModel.CancelEventArgs e )
        {
            this._SpinningNullableValueTextBox.Text = this._NullableNumericUpDown.NullableValue.ToString();
            this._SpinningValueTextBox.Text = this._NullableNumericUpDown.Value.ToString();
            this._NullableTextTextBox.Text = this._NullableNumericUpDown.Text;
        }

        /// <summary>   Nullable numeric up down value incrementing. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Cancel event information. </param>
        private void NullableNumericUpDown_ValueIncrementing( object sender, System.ComponentModel.CancelEventArgs e )
        {
            this._SpinningNullableValueTextBox.Text = this._NullableNumericUpDown.NullableValue.ToString();
            this._SpinningValueTextBox.Text = this._NullableNumericUpDown.Value.ToString();
            this._NullableTextTextBox.Text = this._NullableNumericUpDown.Text;
        }

        /// <summary>   Nilify button click. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void NilifyButton_Click( object sender, EventArgs e )
        {
            this._NullableNumericUpDown.NullableValue = new decimal?();
            _ = this._NullableNumericUpDown.Validate();
            Application.DoEvents();
        }

        /// <summary>   Timer tick. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void Timer_Tick( object sender, EventArgs e )
        {
            this._NullableValueTextBox.Text = this._NullableNumericUpDown.NullableValue.ToString();
        }
    }
}
