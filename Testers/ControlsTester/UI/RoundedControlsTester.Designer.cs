using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace ControlsTester
{

    /// <summary>   A rounded controls tester. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    [DesignerGenerated()]
    public partial class RoundedControlsTester : Form
    {

        /// <summary>   Form overrides dispose to clean up the component list. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="disposing">    <see langword="true" /> to release both managed and unmanaged
        ///                             resources; <see langword="false" /> to release only unmanaged
        ///                             resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components is object)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        /// <summary>   Required by the Windows Form Designer. </summary>
        private System.ComponentModel.IContainer components;

        /// <summary>
        /// NOTE: The following procedure is required by the Windows Form Designer It can be modified
        /// using the Windows Form Designer.  
        /// Do not modify it using the code editor.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _Panel = new Panel();
            RoundedPanel1 = new isr.Core.Controls.RoundedPanel();
            _RoundButton = new isr.Core.Controls.RoundedButton();
            _RoundedPanel = new isr.Core.Controls.RoundedPanel();
            TableLayoutPanel1 = new TableLayoutPanel();
            _RoundedLabel = new isr.Core.Controls.RoundedLabel();
            _RoundedPanel.SuspendLayout();
            TableLayoutPanel1.SuspendLayout();
            SuspendLayout();
            // 
            // _Panel
            // 
            _Panel.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            _Panel.BorderStyle = BorderStyle.Fixed3D;
            _Panel.Location = new Point(45, 145);
            _Panel.Name = "_Panel";
            _Panel.Size = new Size(251, 51);
            _Panel.TabIndex = 2;
            // 
            // RoundedPanel1
            // 
            RoundedPanel1.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            RoundedPanel1.BackgroundImageLayout = ImageLayout.None;
            RoundedPanel1.BorderSize = 3;
            RoundedPanel1.BottomLeftRadius = 15;
            RoundedPanel1.BottomRightRadius = 15;
            RoundedPanel1.Location = new Point(46, 205);
            RoundedPanel1.Name = "RoundedPanel1";
            RoundedPanel1.ShadowAngle = 0;
            RoundedPanel1.Size = new Size(251, 49);
            RoundedPanel1.TabIndex = 5;
            RoundedPanel1.TopLeftRadius = 30;
            RoundedPanel1.TopRightRadius = 50;
            // 
            // _RoundButton
            // 
            _RoundButton.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            _RoundButton.BorderSize = 3;
            _RoundButton.BottomLeftRadius = 13;
            _RoundButton.BottomRightRadius = 15;
            _RoundButton.Cursor = Cursors.Default;
            _RoundButton.Location = new Point(177, 85);
            _RoundButton.Name = "_RoundButton";
            _RoundButton.ShadowAngle = 0;
            _RoundButton.Size = new Size(126, 45);
            _RoundButton.TabIndex = 4;
            _RoundButton.Text = "Button";
            _RoundButton.TopLeftRadius = 15;
            _RoundButton.TopRightRadius = 15;
            _RoundButton.UseVisualStyleBackColor = true;
            // 
            // _RoundedPanel
            // 
            _RoundedPanel.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            _RoundedPanel.BackgroundImageLayout = ImageLayout.None;
            _RoundedPanel.BorderSize = 3;
            _RoundedPanel.BottomLeftRadius = 50;
            _RoundedPanel.BottomRightRadius = 50;
            _RoundedPanel.Controls.Add(TableLayoutPanel1);
            _RoundedPanel.Location = new Point(62, 12);
            _RoundedPanel.Name = "_RoundedPanel";
            _RoundedPanel.Padding = new Padding(6);
            _RoundedPanel.ShadowAngle = 0;
            _RoundedPanel.Size = new Size(251, 64);
            _RoundedPanel.TabIndex = 1;
            _RoundedPanel.TopLeftRadius = 30;
            _RoundedPanel.TopRightRadius = 50;
            // 
            // TableLayoutPanel1
            // 
            TableLayoutPanel1.BackColor = Color.Transparent;
            TableLayoutPanel1.ColumnCount = 3;
            TableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 3.0f));
            TableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100.0f));
            TableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 3.0f));
            TableLayoutPanel1.Controls.Add(_RoundedLabel, 1, 1);
            TableLayoutPanel1.Dock = DockStyle.Fill;
            TableLayoutPanel1.Location = new Point(6, 6);
            TableLayoutPanel1.Name = "TableLayoutPanel1";
            TableLayoutPanel1.RowCount = 3;
            TableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 3.0f));
            TableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 100.0f));
            TableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 3.0f));
            TableLayoutPanel1.Size = new Size(239, 52);
            TableLayoutPanel1.TabIndex = 0;
            // 
            // _RoundedLabel
            // 
            _RoundedLabel.BackColor = SystemColors.Control;
            _RoundedLabel.BorderSize = 3;
            _RoundedLabel.BottomLeftRadius = 23;
            _RoundedLabel.BottomRightRadius = 23;
            _RoundedLabel.Dock = DockStyle.Fill;
            _RoundedLabel.Location = new Point(6, 6);
            _RoundedLabel.Margin = new Padding(3);
            _RoundedLabel.Name = "_RoundedLabel";
            _RoundedLabel.ShadowAngle = 0;
            _RoundedLabel.Size = new Size(227, 40);
            _RoundedLabel.TabIndex = 0;
            _RoundedLabel.Text = "RoundedLabel1";
            _RoundedLabel.TextAlign = ContentAlignment.MiddleCenter;
            _RoundedLabel.TopLeftRadius = 23;
            _RoundedLabel.TopRightRadius = 23;
            // 
            // RoundedControlsTester
            // 
            AutoScaleDimensions = new SizeF(6.0f, 13.0f);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(348, 309);
            Controls.Add(RoundedPanel1);
            Controls.Add(_RoundButton);
            Controls.Add(_Panel);
            Controls.Add(_RoundedPanel);
            Name = "RoundedControlsTester";
            Text = "Rounded Controls Tester";
            _RoundedPanel.ResumeLayout(false);
            TableLayoutPanel1.ResumeLayout(false);
            ResumeLayout(false);
        }

        /// <summary>   The panel. </summary>
        private Panel _Panel;
        /// <summary>   The rounded panel. </summary>
        private isr.Core.Controls.RoundedPanel _RoundedPanel;
        /// <summary>   The rounded label. </summary>
        private isr.Core.Controls.RoundedLabel _RoundedLabel;
        /// <summary>   The round button. </summary>
        private isr.Core.Controls.RoundedButton _RoundButton;
        /// <summary>   The first rounded panel. </summary>
        private isr.Core.Controls.RoundedPanel RoundedPanel1;
        /// <summary>   The first table layout panel. </summary>
        private TableLayoutPanel TableLayoutPanel1;
    }
}
