using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace ControlsTester
{
    [DesignerGenerated()]
    public partial class DropDownTextBoxForm : Form
    {

        /// <summary>   Form overrides dispose to clean up the component list. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="disposing">    <see langword="true" /> to release both managed and unmanaged
        ///                             resources; <see langword="false" /> to release only unmanaged
        ///                             resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components is object)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _WinFormsDropDownTextBox = new TextBox();
            _WinFormsDropDownTextBox.MouseClick += new MouseEventHandler(WinFormsTextBox_MouseClick);
            _WinFormsDropDownTextBox.MouseDoubleClick += new MouseEventHandler(WinFormsTextBox_MouseDoubleClick);
            WinFormsTextBox = new TextBox();
            DropDownTextBox = new isr.Core.Controls.DropDownTextBox();
            WinFormDropDownTextBoxLabel = new Label();
            DropDownTextBoxLabel = new Label();
            WinFormsTextBoxLabel = new Label();
            SuspendLayout();
            // 
            // WinFormsDropDownTextBox
            // 
            _WinFormsDropDownTextBox.Location = new Point(12, 105);
            _WinFormsDropDownTextBox.Name = "_WinFormsDropDownTextBox";
            _WinFormsDropDownTextBox.Size = new Size(243, 20);
            _WinFormsDropDownTextBox.TabIndex = 0;
            // 
            // WinFormsTextBox
            // 
            WinFormsTextBox.Location = new Point(12, 33);
            WinFormsTextBox.Name = "WinFormsTextBox";
            WinFormsTextBox.Size = new Size(100, 20);
            WinFormsTextBox.TabIndex = 1;
            // 
            // DropDownTextBox
            // 
            DropDownTextBox.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            DropDownTextBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            DropDownTextBox.Location = new Point(283, 105);
            DropDownTextBox.Margin = new Padding(0);
            DropDownTextBox.Name = "DropDownTextBox";
            DropDownTextBox.Size = new Size(305, 26);
            DropDownTextBox.TabIndex = 2;
            // 
            // WinFormDropDownTextBoxLabel
            // 
            WinFormDropDownTextBoxLabel.Location = new Point(9, 70);
            WinFormDropDownTextBoxLabel.Name = "WinFormDropDownTextBoxLabel";
            WinFormDropDownTextBoxLabel.Size = new Size(256, 32);
            WinFormDropDownTextBoxLabel.TabIndex = 3;
            WinFormDropDownTextBoxLabel.Text = "Windows Forms Text Box with Mouse Drop Effect. Click to drop; double click to res" + "tore";
            // 
            // DropDownTextBoxLabel
            // 
            DropDownTextBoxLabel.AutoSize = true;
            DropDownTextBoxLabel.Location = new Point(286, 88);
            DropDownTextBoxLabel.Name = "DropDownTextBoxLabel";
            DropDownTextBoxLabel.Size = new Size(299, 13);
            DropDownTextBoxLabel.TabIndex = 4;
            DropDownTextBoxLabel.Text = "Custom Drop Down; Click the down arrow to drop and restore.";
            // 
            // WinFormsTextBoxLabel
            // 
            WinFormsTextBoxLabel.AutoSize = true;
            WinFormsTextBoxLabel.Location = new Point(14, 17);
            WinFormsTextBoxLabel.Name = "WinFormsTextBoxLabel";
            WinFormsTextBoxLabel.Size = new Size(102, 13);
            WinFormsTextBoxLabel.TabIndex = 5;
            WinFormsTextBoxLabel.Text = "This text box is fixed";
            // 
            // DropDownTextBoxForm
            // 
            AutoScaleDimensions = new SizeF(6.0f, 13.0f);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(595, 363);
            Controls.Add(WinFormsTextBoxLabel);
            Controls.Add(DropDownTextBoxLabel);
            Controls.Add(WinFormDropDownTextBoxLabel);
            Controls.Add(DropDownTextBox);
            Controls.Add(_WinFormsDropDownTextBox);
            Controls.Add(WinFormsTextBox);
            Name = "DropDownTextBoxForm";
            Text = "Drop Down Text Box Form";
            ResumeLayout(false);
            PerformLayout();
        }

        private TextBox _WinFormsDropDownTextBox;

        private TextBox WinFormsDropDownTextBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _WinFormsDropDownTextBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_WinFormsDropDownTextBox != null)
                {
                    _WinFormsDropDownTextBox.MouseClick -= WinFormsTextBox_MouseClick;
                    _WinFormsDropDownTextBox.MouseDoubleClick -= WinFormsTextBox_MouseDoubleClick;
                }

                _WinFormsDropDownTextBox = value;
                if (_WinFormsDropDownTextBox != null)
                {
                    _WinFormsDropDownTextBox.MouseClick += WinFormsTextBox_MouseClick;
                    _WinFormsDropDownTextBox.MouseDoubleClick += WinFormsTextBox_MouseDoubleClick;
                }
            }
        }

        private TextBox WinFormsTextBox;
        private isr.Core.Controls.DropDownTextBox DropDownTextBox;
        private Label WinFormDropDownTextBoxLabel;
        internal Label WinFormsTextBoxLabel;
        private Label DropDownTextBoxLabel;
    }
}
