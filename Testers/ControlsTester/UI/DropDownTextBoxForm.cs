


using System.Windows.Forms;

namespace ControlsTester
{

    /// <summary>   Form for viewing the drop down text box. </summary>
    /// <remarks>   David, 2020-10-25. </remarks>
    public partial class DropDownTextBoxForm
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        public DropDownTextBoxForm()
        {
            this.InitializeComponent();
            this._WinFormsDropDownTextBox.Name = "WinFormsDropDownTextBox";
        }

        /// <summary>   The height. </summary>
        private int _Height;

        /// <summary>   Win Forms Text Box  mouse click. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void WinFormsTextBox_MouseClick( object sender, MouseEventArgs e )
        {
            if ( sender is TextBox textBox )
            {
                if ( this._Height == 0 )
                {
                    this._Height = textBox.Height;
                }

                textBox.Multiline = true;
                // textBox.Width = 300
                textBox.Height = 200;
                textBox.Refresh();
            }
        }

        /// <summary>   Win Forms Text Box mouse double click. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Mouse event information. </param>
        private void WinFormsTextBox_MouseDoubleClick( object sender, MouseEventArgs e )
        {
            if ( sender is TextBox textBox )
            {
                textBox.Multiline = false;
                // textBox.Width = 300
                textBox.Height = 20;
                textBox.Refresh();
            }
        }
    }
}
