using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace ControlsTester
{
    [DesignerGenerated()]
    public partial class NullableNumericUpDownForm : Form
    {

        /// <summary>   Form overrides dispose to clean up the component list. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="disposing">    <see langword="true" /> to release both managed and unmanaged
        ///                             resources; <see langword="false" /> to release only unmanaged
        ///                             resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components is object)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _ValueTextBoxLabel = new Label();
            _SpinningValueTextBox = new TextBox();
            _ChangedValueTextBoxLabel = new Label();
            _ChangedValueTextBox = new TextBox();
            Label2 = new Label();
            _ValidatedValueTextBox = new TextBox();
            _SpinningNullableValueTextBox = new TextBox();
            _ValidatedNullableValueTextBox = new TextBox();
            _ChangedNullableValueTextBox = new TextBox();
            Label1 = new Label();
            Label3 = new Label();
            __NullableNumericUpDown = new isr.Core.Controls.NullableNumericUpDown();
            __NullableNumericUpDown.Validated += new EventHandler(NullableNumericUpDown_Validated);
            __NullableNumericUpDown.ValueChanged += new EventHandler(NullableNumericUpDown_ValueChanged);
            __NullableNumericUpDown.ValueDecrementing += new EventHandler<System.ComponentModel.CancelEventArgs>(NullableNumericUpDown_ValueDecrementing);
            __NullableNumericUpDown.ValueIncrementing += new EventHandler<System.ComponentModel.CancelEventArgs>(NullableNumericUpDown_ValueIncrementing);
            _NullableTextTextBoxLabel = new Label();
            _NullableTextTextBox = new TextBox();
            __NilifyButton = new Button();
            __NilifyButton.Click += new EventHandler(NilifyButton_Click);
            _NullableValueTextBox = new TextBox();
            __Timer = new Timer(components);
            __Timer.Tick += new EventHandler(Timer_Tick);
            ((System.ComponentModel.ISupportInitialize)__NullableNumericUpDown).BeginInit();
            SuspendLayout();
            // 
            // _ValueTextBoxLabel
            // 
            _ValueTextBoxLabel.AutoSize = true;
            _ValueTextBoxLabel.Location = new Point(40, 75);
            _ValueTextBoxLabel.Name = "_ValueTextBoxLabel";
            _ValueTextBoxLabel.Size = new Size(51, 13);
            _ValueTextBoxLabel.TabIndex = 4;
            _ValueTextBoxLabel.Text = "Spinning:";
            _ValueTextBoxLabel.TextAlign = ContentAlignment.TopRight;
            // 
            // _SpinningValueTextBox
            // 
            _SpinningValueTextBox.Location = new Point(93, 72);
            _SpinningValueTextBox.Name = "_SpinningValueTextBox";
            _SpinningValueTextBox.Size = new Size(100, 20);
            _SpinningValueTextBox.TabIndex = 5;
            // 
            // _ChangedValueTextBoxLabel
            // 
            _ChangedValueTextBoxLabel.AutoSize = true;
            _ChangedValueTextBoxLabel.Location = new Point(38, 113);
            _ChangedValueTextBoxLabel.Name = "_ChangedValueTextBoxLabel";
            _ChangedValueTextBoxLabel.Size = new Size(53, 13);
            _ChangedValueTextBoxLabel.TabIndex = 7;
            _ChangedValueTextBoxLabel.Text = "Changed:";
            _ChangedValueTextBoxLabel.TextAlign = ContentAlignment.TopRight;
            // 
            // _ChangedValueTextBox
            // 
            _ChangedValueTextBox.Location = new Point(93, 110);
            _ChangedValueTextBox.Name = "_ChangedValueTextBox";
            _ChangedValueTextBox.Size = new Size(100, 20);
            _ChangedValueTextBox.TabIndex = 8;
            // 
            // Label2
            // 
            Label2.AutoSize = true;
            Label2.Location = new Point(37, 152);
            Label2.Name = "Label2";
            Label2.Size = new Size(54, 13);
            Label2.TabIndex = 10;
            Label2.Text = "Validated:";
            Label2.TextAlign = ContentAlignment.TopRight;
            // 
            // _ValidatedValueTextBox
            // 
            _ValidatedValueTextBox.Location = new Point(93, 149);
            _ValidatedValueTextBox.Name = "_ValidatedValueTextBox";
            _ValidatedValueTextBox.Size = new Size(100, 20);
            _ValidatedValueTextBox.TabIndex = 11;
            // 
            // _SpinningNullableValueTextBox
            // 
            _SpinningNullableValueTextBox.Location = new Point(199, 72);
            _SpinningNullableValueTextBox.Name = "_SpinningNullableValueTextBox";
            _SpinningNullableValueTextBox.Size = new Size(100, 20);
            _SpinningNullableValueTextBox.TabIndex = 6;
            // 
            // _ValidatedNullableValueTextBox
            // 
            _ValidatedNullableValueTextBox.Location = new Point(199, 149);
            _ValidatedNullableValueTextBox.Name = "_ValidatedNullableValueTextBox";
            _ValidatedNullableValueTextBox.Size = new Size(100, 20);
            _ValidatedNullableValueTextBox.TabIndex = 12;
            // 
            // _ChangedNullableValueTextBox
            // 
            _ChangedNullableValueTextBox.Location = new Point(199, 110);
            _ChangedNullableValueTextBox.Name = "_ChangedNullableValueTextBox";
            _ChangedNullableValueTextBox.Size = new Size(100, 20);
            _ChangedNullableValueTextBox.TabIndex = 9;
            // 
            // Label1
            // 
            Label1.AutoSize = true;
            Label1.Location = new Point(97, 56);
            Label1.Name = "Label1";
            Label1.Size = new Size(37, 13);
            Label1.TabIndex = 2;
            Label1.Text = "Value:";
            Label1.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // Label3
            // 
            Label3.AutoSize = true;
            Label3.Location = new Point(196, 56);
            Label3.Name = "Label3";
            Label3.Size = new Size(78, 13);
            Label3.TabIndex = 3;
            Label3.Text = "Nullable Value:";
            Label3.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // _NullableNumericUpDown
            // 
            __NullableNumericUpDown.Location = new Point(49, 16);
            __NullableNumericUpDown.Maximum = new decimal(new int[] { 100000, 0, 0, 0 });
            __NullableNumericUpDown.Name = "__NullableNumericUpDown";
            __NullableNumericUpDown.ReadOnlyBackColor = Color.Empty;
            __NullableNumericUpDown.ReadOnlyForeColor = Color.Empty;
            __NullableNumericUpDown.ReadWriteBackColor = Color.Empty;
            __NullableNumericUpDown.ReadWriteForeColor = Color.Empty;
            __NullableNumericUpDown.Size = new Size(145, 20);
            __NullableNumericUpDown.TabIndex = 0;
            // 
            // _NullableTextTextBoxLabel
            // 
            _NullableTextTextBoxLabel.AutoSize = true;
            _NullableTextTextBoxLabel.Location = new Point(164, 186);
            _NullableTextTextBoxLabel.Name = "_NullableTextTextBoxLabel";
            _NullableTextTextBoxLabel.Size = new Size(31, 13);
            _NullableTextTextBoxLabel.TabIndex = 13;
            _NullableTextTextBoxLabel.Text = "Text:";
            _NullableTextTextBoxLabel.TextAlign = ContentAlignment.TopRight;
            // 
            // _NullableTextTextBox
            // 
            _NullableTextTextBox.Location = new Point(198, 183);
            _NullableTextTextBox.Name = "_NullableTextTextBox";
            _NullableTextTextBox.Size = new Size(100, 20);
            _NullableTextTextBox.TabIndex = 14;
            // 
            // _NilifyButton
            // 
            __NilifyButton.Location = new Point(223, 16);
            __NilifyButton.Name = "__NilifyButton";
            __NilifyButton.Size = new Size(75, 23);
            __NilifyButton.TabIndex = 1;
            __NilifyButton.Text = "Nilify";
            __NilifyButton.UseVisualStyleBackColor = true;
            // 
            // _NullableValueTextBox
            // 
            _NullableValueTextBox.Location = new Point(302, 18);
            _NullableValueTextBox.Name = "_NullableValueTextBox";
            _NullableValueTextBox.Size = new Size(57, 20);
            _NullableValueTextBox.TabIndex = 14;
            // 
            // _Timer
            // 
            // 
            // NullableNumericUpDownForm
            // 
            AutoScaleDimensions = new SizeF(6.0f, 13.0f);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(371, 261);
            Controls.Add(__NilifyButton);
            Controls.Add(_NullableValueTextBox);
            Controls.Add(_NullableTextTextBox);
            Controls.Add(_ValidatedValueTextBox);
            Controls.Add(_NullableTextTextBoxLabel);
            Controls.Add(Label2);
            Controls.Add(_ChangedNullableValueTextBox);
            Controls.Add(_ChangedValueTextBox);
            Controls.Add(_ChangedValueTextBoxLabel);
            Controls.Add(_ValidatedNullableValueTextBox);
            Controls.Add(_SpinningNullableValueTextBox);
            Controls.Add(_SpinningValueTextBox);
            Controls.Add(Label3);
            Controls.Add(Label1);
            Controls.Add(_ValueTextBoxLabel);
            Controls.Add(__NullableNumericUpDown);
            Name = "NullableNumericUpDownForm";
            Text = "Nullable Numeric Up Down Form";
            ((System.ComponentModel.ISupportInitialize)__NullableNumericUpDown).EndInit();
            Shown += new EventHandler(NullableNumericUpDownForm_Shown);
            ResumeLayout(false);
            PerformLayout();
        }

        private isr.Core.Controls.NullableNumericUpDown __NullableNumericUpDown;

        private isr.Core.Controls.NullableNumericUpDown _NullableNumericUpDown
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __NullableNumericUpDown;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__NullableNumericUpDown != null)
                {
                    __NullableNumericUpDown.Validated -= NullableNumericUpDown_Validated;
                    __NullableNumericUpDown.ValueChanged -= NullableNumericUpDown_ValueChanged;
                    __NullableNumericUpDown.ValueDecrementing -= NullableNumericUpDown_ValueDecrementing;
                    __NullableNumericUpDown.ValueIncrementing -= NullableNumericUpDown_ValueIncrementing;
                }

                __NullableNumericUpDown = value;
                if (__NullableNumericUpDown != null)
                {
                    __NullableNumericUpDown.Validated += NullableNumericUpDown_Validated;
                    __NullableNumericUpDown.ValueChanged += NullableNumericUpDown_ValueChanged;
                    __NullableNumericUpDown.ValueDecrementing += NullableNumericUpDown_ValueDecrementing;
                    __NullableNumericUpDown.ValueIncrementing += NullableNumericUpDown_ValueIncrementing;
                }
            }
        }

        private Label _ValueTextBoxLabel;
        private TextBox _SpinningValueTextBox;
        private Label _ChangedValueTextBoxLabel;
        private TextBox _ChangedValueTextBox;
        private Label Label2;
        private TextBox _ValidatedValueTextBox;
        private TextBox _SpinningNullableValueTextBox;
        private TextBox _ValidatedNullableValueTextBox;
        private TextBox _ChangedNullableValueTextBox;
        private Label Label1;
        private Label Label3;
        private Label _NullableTextTextBoxLabel;
        private TextBox _NullableTextTextBox;
        private Button __NilifyButton;

        private Button _NilifyButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __NilifyButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__NilifyButton != null)
                {
                    __NilifyButton.Click -= NilifyButton_Click;
                }

                __NilifyButton = value;
                if (__NilifyButton != null)
                {
                    __NilifyButton.Click += NilifyButton_Click;
                }
            }
        }

        private TextBox _NullableValueTextBox;
        private Timer __Timer;

        private Timer _Timer
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __Timer;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__Timer != null)
                {
                    __Timer.Tick -= Timer_Tick;
                }

                __Timer = value;
                if (__Timer != null)
                {
                    __Timer.Tick += Timer_Tick;
                }
            }
        }
    }
}
