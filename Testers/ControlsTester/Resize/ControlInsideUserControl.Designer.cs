using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace ControlsTester
{
    [Microsoft.VisualBasic.CompilerServices.DesignerGenerated()]
    public partial class ControlInsideUserControl : UserControl
    {

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="disposing">    <see langword="true" /> to release both managed and unmanaged
        ///                             resources; <see langword="false" /> to release only unmanaged
        ///                             resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components is object)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            TableLayoutPanel1 = new TableLayoutPanel();
            LabelPanelUserControl2 = new LabelPanelUserControl();
            TableLayoutPanel1.SuspendLayout();
            SuspendLayout();
            // 
            // TableLayoutPanel1
            // 
            TableLayoutPanel1.BackColor = SystemColors.ActiveCaption;
            TableLayoutPanel1.ColumnCount = 3;
            TableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 3.0f));
            TableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100.0f));
            TableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 3.0f));
            TableLayoutPanel1.Controls.Add(LabelPanelUserControl2, 1, 1);
            TableLayoutPanel1.Dock = DockStyle.Fill;
            TableLayoutPanel1.Location = new Point(5, 5);
            TableLayoutPanel1.Name = "TableLayoutPanel1";
            TableLayoutPanel1.RowCount = 3;
            TableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 3.0f));
            TableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 100.0f));
            TableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 3.0f));
            TableLayoutPanel1.Size = new Size(294, 78);
            TableLayoutPanel1.TabIndex = 5;
            // 
            // LabelPanelUserControl2
            // 
            LabelPanelUserControl2.BackColor = SystemColors.Info;
            LabelPanelUserControl2.Dock = DockStyle.Fill;
            LabelPanelUserControl2.Location = new Point(6, 6);
            LabelPanelUserControl2.Name = "LabelPanelUserControl2";
            LabelPanelUserControl2.Padding = new Padding(4);
            LabelPanelUserControl2.Size = new Size(282, 66);
            LabelPanelUserControl2.TabIndex = 3;
            // 
            // ControlInsideUserControl1
            // 
            AutoScaleDimensions = new SizeF(6.0f, 13.0f);
            AutoScaleMode = AutoScaleMode.Font;
            BorderStyle = BorderStyle.FixedSingle;
            Controls.Add(TableLayoutPanel1);
            Name = "ControlInsideUserControl1";
            Padding = new Padding(5);
            Size = new Size(304, 88);
            TableLayoutPanel1.ResumeLayout(false);
            ResumeLayout(false);
        }

        private TableLayoutPanel TableLayoutPanel1;
        private LabelPanelUserControl LabelPanelUserControl2;
    }
}
