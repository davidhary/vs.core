using System.Drawing;

namespace ControlsTester
{

    /// <summary>   A label panel user control. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    public partial class LabelPanelUserControl
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        public LabelPanelUserControl()
        {
            this.InitializeComponent();
        }

        /// <summary>   Renders this object. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        public void Render()
        {
            this.BackColor = Color.Green;
        }
    }
}
