using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace ControlsTester
{
    [DesignerGenerated()]
    public partial class ResizeTester : Form
    {

        /// <summary>
        /// Disposes of the resources (other than memory) used by the
        /// <see cref="T:System.Windows.Forms.Form" />.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="disposing">    <see langword="true" /> to release both managed and unmanaged
        ///                             resources; <see langword="false" /> to release only unmanaged
        ///                             resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components is object)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _LabelInsidePanel = new Panel();
            _InsidePaneLabel = new Label();
            _LabelInsidePanelLabel = new Label();
            _PanelLayoutPanel = new TableLayoutPanel();
            Label1 = new Label();
            Panel1 = new Panel();
            Label2 = new Label();
            TableLayoutPanel1 = new TableLayoutPanel();
            LabelPanelUserControl2 = new LabelPanelUserControl();
            LabelPanelUserControl1 = new LabelPanelUserControl();
            ControlInsideUserControl1 = new ControlInsideUserControl();
            _LabelInsidePanel.SuspendLayout();
            _PanelLayoutPanel.SuspendLayout();
            Panel1.SuspendLayout();
            TableLayoutPanel1.SuspendLayout();
            SuspendLayout();
            // 
            // _LabelInsidePanel
            // 
            _LabelInsidePanel.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;

            _LabelInsidePanel.BackColor = SystemColors.ActiveCaption;
            _LabelInsidePanel.BorderStyle = BorderStyle.FixedSingle;
            _LabelInsidePanel.Controls.Add(_InsidePaneLabel);
            _LabelInsidePanel.Location = new Point(11, 33);
            _LabelInsidePanel.Name = "_LabelInsidePanel";
            _LabelInsidePanel.Padding = new Padding(3);
            _LabelInsidePanel.Size = new Size(202, 38);
            _LabelInsidePanel.TabIndex = 0;
            // 
            // _InsidePaneLabel
            // 
            _InsidePaneLabel.BackColor = SystemColors.ButtonHighlight;
            _InsidePaneLabel.Dock = DockStyle.Fill;
            _InsidePaneLabel.Location = new Point(3, 3);
            _InsidePaneLabel.Margin = new Padding(0);
            _InsidePaneLabel.Name = "_InsidePaneLabel";
            _InsidePaneLabel.Size = new Size(194, 30);
            _InsidePaneLabel.TabIndex = 0;
            _InsidePaneLabel.Text = "This label resizes inside the panel";
            _InsidePaneLabel.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // _LabelInsidePanelLabel
            // 
            _LabelInsidePanelLabel.AutoSize = true;
            _LabelInsidePanelLabel.Location = new Point(4, 17);
            _LabelInsidePanelLabel.Name = "_LabelInsidePanelLabel";
            _LabelInsidePanelLabel.Size = new Size(101, 13);
            _LabelInsidePanelLabel.TabIndex = 1;
            _LabelInsidePanelLabel.Text = "Label inside a panel";
            // 
            // _PanelLayoutPanel
            // 
            _PanelLayoutPanel.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;

            _PanelLayoutPanel.ColumnCount = 3;
            _PanelLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 20.0f));
            _PanelLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100.0f));
            _PanelLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 20.0f));
            _PanelLayoutPanel.Controls.Add(Panel1, 1, 1);
            _PanelLayoutPanel.Location = new Point(13, 135);
            _PanelLayoutPanel.Name = "_PanelLayoutPanel";
            _PanelLayoutPanel.RowCount = 3;
            _PanelLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 20.0f));
            _PanelLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 100.0f));
            _PanelLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 20.0f));
            _PanelLayoutPanel.Size = new Size(232, 82);
            _PanelLayoutPanel.TabIndex = 2;
            // 
            // Label1
            // 
            Label1.AutoSize = true;
            Label1.Location = new Point(5, 119);
            Label1.Name = "Label1";
            Label1.Size = new Size(171, 13);
            Label1.TabIndex = 1;
            Label1.Text = "Label inside a panel inside a layout";
            // 
            // Panel1
            // 
            Panel1.BackColor = SystemColors.ActiveCaption;
            Panel1.BorderStyle = BorderStyle.FixedSingle;
            Panel1.Controls.Add(Label2);
            Panel1.Dock = DockStyle.Fill;
            Panel1.Location = new Point(23, 23);
            Panel1.Name = "Panel1";
            Panel1.Padding = new Padding(3);
            Panel1.Size = new Size(186, 36);
            Panel1.TabIndex = 0;
            // 
            // Label2
            // 
            Label2.BackColor = SystemColors.ButtonHighlight;
            Label2.Dock = DockStyle.Fill;
            Label2.Location = new Point(3, 3);
            Label2.Margin = new Padding(0);
            Label2.Name = "Label2";
            Label2.Size = new Size(178, 28);
            Label2.TabIndex = 0;
            Label2.Text = "label inside a panel inside a layout";
            Label2.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // TableLayoutPanel1
            // 
            TableLayoutPanel1.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;

            TableLayoutPanel1.BackColor = SystemColors.ActiveCaption;
            TableLayoutPanel1.ColumnCount = 3;
            TableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 3.0f));
            TableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100.0f));
            TableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 3.0f));
            TableLayoutPanel1.Controls.Add(LabelPanelUserControl2, 1, 1);
            TableLayoutPanel1.Location = new Point(13, 369);
            TableLayoutPanel1.Name = "TableLayoutPanel1";
            TableLayoutPanel1.RowCount = 3;
            TableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 3.0f));
            TableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 100.0f));
            TableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 3.0f));
            TableLayoutPanel1.Size = new Size(200, 59);
            TableLayoutPanel1.TabIndex = 4;
            // 
            // LabelPanelUserControl2
            // 
            LabelPanelUserControl2.BackColor = SystemColors.Info;
            LabelPanelUserControl2.Dock = DockStyle.Fill;
            LabelPanelUserControl2.Location = new Point(6, 6);
            LabelPanelUserControl2.Name = "LabelPanelUserControl2";
            LabelPanelUserControl2.Padding = new Padding(4);
            LabelPanelUserControl2.Size = new Size(188, 47);
            LabelPanelUserControl2.TabIndex = 3;
            // 
            // LabelPanelUserControl1
            // 
            LabelPanelUserControl1.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;

            LabelPanelUserControl1.BackColor = SystemColors.Info;
            LabelPanelUserControl1.Location = new Point(17, 255);
            LabelPanelUserControl1.Name = "LabelPanelUserControl1";
            LabelPanelUserControl1.Padding = new Padding(4);
            LabelPanelUserControl1.Size = new Size(227, 53);
            LabelPanelUserControl1.TabIndex = 3;
            // 
            // ControlInsideUserControl1
            // 
            ControlInsideUserControl1.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;

            ControlInsideUserControl1.BorderStyle = BorderStyle.FixedSingle;
            ControlInsideUserControl1.Location = new Point(14, 472);
            ControlInsideUserControl1.Name = "ControlInsideUserControl1";
            ControlInsideUserControl1.Padding = new Padding(5);
            ControlInsideUserControl1.Size = new Size(199, 88);
            ControlInsideUserControl1.TabIndex = 5;
            // 
            // ResizeTester
            // 
            AutoScaleDimensions = new SizeF(6.0f, 13.0f);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(280, 574);
            Controls.Add(ControlInsideUserControl1);
            Controls.Add(TableLayoutPanel1);
            Controls.Add(LabelPanelUserControl1);
            Controls.Add(_PanelLayoutPanel);
            Controls.Add(Label1);
            Controls.Add(_LabelInsidePanelLabel);
            Controls.Add(_LabelInsidePanel);
            Name = "ResizeTester";
            Text = "ResizeTester";
            _LabelInsidePanel.ResumeLayout(false);
            _PanelLayoutPanel.ResumeLayout(false);
            Panel1.ResumeLayout(false);
            TableLayoutPanel1.ResumeLayout(false);
            DoubleClick += new EventHandler(ResizeTester_DoubleClick);
            ResumeLayout(false);
            PerformLayout();
        }

        private Panel _LabelInsidePanel;
        private Label _InsidePaneLabel;
        private Label _LabelInsidePanelLabel;
        private TableLayoutPanel _PanelLayoutPanel;
        private Panel Panel1;
        private Label Label2;
        private Label Label1;
        private LabelPanelUserControl LabelPanelUserControl1;
        private TableLayoutPanel TableLayoutPanel1;
        private LabelPanelUserControl LabelPanelUserControl2;
        private ControlInsideUserControl ControlInsideUserControl1;
    }
}
