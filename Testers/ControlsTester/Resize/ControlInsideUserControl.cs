
using System.Drawing;

namespace ControlsTester
{

    /// <summary>   A control inside user control. </summary>
    /// <remarks>   David, 2020-10-25. </remarks>
    public partial class ControlInsideUserControl
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        public ControlInsideUserControl()
        {
            this.InitializeComponent();
        }

        /// <summary>   Renders this object. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        public void Render()
        {
            this.BackColor = Color.Red;
            this.LabelPanelUserControl2.Render();
        }
    }
}
