using System;

namespace ControlsTester
{

    /// <summary>   A resize tester. </summary>
    /// <remarks>   David, 2020-10-25. </remarks>
    public partial class ResizeTester
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        public ResizeTester()
        {
            this.InitializeComponent();
        }

        /// <summary>   Resize tester double click. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void ResizeTester_DoubleClick( object sender, EventArgs e )
        {
            this.ControlInsideUserControl1.Render();
        }
    }
}
