using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace ControlsTester
{
    [Microsoft.VisualBasic.CompilerServices.DesignerGenerated()]
    public partial class LabelPanelUserControl : UserControl
    {

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="disposing">    <see langword="true" /> to release both managed and unmanaged
        ///                             resources; <see langword="false" /> to release only unmanaged
        ///                             resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components is object)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _PanelLayoutPanel = new TableLayoutPanel();
            Panel1 = new Panel();
            Label2 = new Label();
            _PanelLayoutPanel.SuspendLayout();
            Panel1.SuspendLayout();
            SuspendLayout();
            // 
            // _PanelLayoutPanel
            // 
            _PanelLayoutPanel.ColumnCount = 3;
            _PanelLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 6.0f));
            _PanelLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100.0f));
            _PanelLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 6.0f));
            _PanelLayoutPanel.Controls.Add(Panel1, 1, 1);
            _PanelLayoutPanel.Dock = DockStyle.Fill;
            _PanelLayoutPanel.Location = new Point(4, 4);
            _PanelLayoutPanel.Name = "_PanelLayoutPanel";
            _PanelLayoutPanel.RowCount = 3;
            _PanelLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 6.0f));
            _PanelLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 100.0f));
            _PanelLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 6.0f));
            _PanelLayoutPanel.Size = new Size(369, 75);
            _PanelLayoutPanel.TabIndex = 3;
            // 
            // Panel1
            // 
            Panel1.BackColor = SystemColors.ActiveCaption;
            Panel1.BorderStyle = BorderStyle.FixedSingle;
            Panel1.Controls.Add(Label2);
            Panel1.Dock = DockStyle.Fill;
            Panel1.Location = new Point(9, 9);
            Panel1.Name = "Panel1";
            Panel1.Padding = new Padding(3);
            Panel1.Size = new Size(351, 57);
            Panel1.TabIndex = 0;
            // 
            // Label2
            // 
            Label2.BackColor = SystemColors.ButtonHighlight;
            Label2.Dock = DockStyle.Fill;
            Label2.Location = new Point(3, 3);
            Label2.Margin = new Padding(0);
            Label2.Name = "Label2";
            Label2.Size = new Size(343, 49);
            Label2.TabIndex = 0;
            Label2.Text = "label inside a panel inside a layout";
            Label2.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // LabelPanelUserControl
            // 
            AutoScaleDimensions = new SizeF(6.0f, 13.0f);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = SystemColors.Info;
            Controls.Add(_PanelLayoutPanel);
            Name = "LabelPanelUserControl";
            Padding = new Padding(4);
            Size = new Size(377, 83);
            _PanelLayoutPanel.ResumeLayout(false);
            Panel1.ResumeLayout(false);
            ResumeLayout(false);
        }

        private TableLayoutPanel _PanelLayoutPanel;
        private Panel Panel1;
        private Label Label2;
    }
}
