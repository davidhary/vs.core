
#pragma warning disable IDE1006 // Naming Styles
namespace isr.Core.Tester
#pragma warning restore IDE1006 // Naming Styles
{

    /// <summary> A dashboard. </summary>
    /// <remarks> David, 2020-09-30. </remarks>
    public partial class Dashboard
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-09-30. </remarks>
        public Dashboard()
        {
            this.InitializeComponent();
        }
    }
}
