using System.Diagnostics;
using System.Windows.Forms;

namespace isr.Core.Tester
{

    /// <summary>
    /// Inherits from the <see cref="isr.Core.Forma.BlueSplash"></see> to provide a splash screen for
    /// the assembly.
    /// </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2015-05-19, x.x.5617 </para>
    /// </remarks>
    public class MySplashScreen : Forma.BlueSplash
    {

        /// <summary> Gets the sentinel indicating if the splash is created. </summary>
        /// <value> <c>True</c> if not nothing or disposed. </value>
        public static bool IsCreated => Instance is object && !Instance.IsDisposed;

        /// <summary> Gets the sentinel indicating if the splash is created and visible. </summary>
        /// <value> <c>True</c> if not nothing or disposed and visible. </value>
        public static bool IsVisible => IsCreated && Instance.Visible;

        /// <summary> The shared instance. </summary>
        /// <value> The instance. </value>
        private static MySplashScreen Instance { get; set; }

        /// <summary> The locking object to enforce thread safety when creating the singleton instance. </summary>
        private static readonly object SyncLocker = new object();

        /// <summary> Creates the instance based on the assembly splash form. </summary>
        /// <remarks> David, 2020-09-30. </remarks>
        /// <param name="value"> The value. </param>
        public static void CreateInstance( Form value )
        {
            if ( value is object && !value.IsDisposed )
            {
                lock ( SyncLocker )
                {
                    Instance = ( MySplashScreen ) value;
                    Instance.TopmostSetter( !Debugger.IsAttached );
                }
            }
        }

        /// <summary> Displays a message on the splash screen. </summary>
        /// <remarks> David, 2020-09-30. </remarks>
        /// <param name="value"> The message. </param>
        public static void SplashMessage( string value )
        {
            if ( IsVisible )
            {
                Instance.DisplayMessage( value );
            }
        }

    }
}
