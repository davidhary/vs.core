using System;
using System.Diagnostics;
using System.Windows.Forms;

using isr.Core.Capsule.ExceptionExtensions;

using Microsoft.VisualBasic.ApplicationServices;


namespace isr.Core.Tester.My
{
    internal partial class MyApplication
    {

        #region " APPLIANCE INSTANCE "

        /// <summary>   The application object provider. </summary>
        private readonly static MyProject.ThreadSafeObjectProvider<isr.Core.Tester.My.Appliance> _AppObjectProvider = new MyProject.ThreadSafeObjectProvider<isr.Core.Tester.My.Appliance>();

        /// <summary>   Gets the implementation of the <see cref="isr.Core.ApplianceBase"/> for this application. </summary>  
        /// <value> The implementation of the <see cref="isr.Core.ApplianceBase"/> for this class application. </value>
        [System.ComponentModel.Design.HelpKeyword( "My.MyApplication.Appliance" )]
        public static isr.Core.Tester.My.Appliance Appliance
        {
            get {
                return _AppObjectProvider.GetInstance;
            }
        }

        /// <summary>   Gets the logger. </summary>
        /// <value> The logger. </value>
        public Logger Logger
        {
            get { return Appliance.Logger; }
        }

        #endregion

        #region " NETWORK EVENTS "

        /// <summary>   Occurs when the network connection is connected or disconnected. </summary>
        /// <remarks>   David, 2020-09-30. </remarks>
        private void HandleNetworkAvailabilityChanged()
        {
            MyProject.Computer.Network.NetworkAvailabilityChanged += HandleNetworkAvailabilityChanged;
        }

        #endregion

        #region " APPLICATION OVERRIDE EVENTS "

        /// <summary>
        /// Sets the visual styles, text display styles, and current principal for the main application
        /// thread (if the application uses Windows authentication), and initializes the splash screen,
        /// if defined. Replaces the default trace listener with the modified listener. Updates the
        /// minimum splash screen display time.
        /// </summary>
        /// <remarks> David, 2020-09-30. </remarks>
        /// <param name="commandLineArgs"> A <see cref="T:System.Collections.ObjectModel.ReadOnlyCollectio
        /// n" /> of String, containing the command-line arguments as
        /// strings for the current application. </param>
        /// <returns>
        /// A <see cref="T:System.Boolean" /> indicating if application startup should continue.
        /// </returns>
        protected override bool OnInitialize( System.Collections.ObjectModel.ReadOnlyCollection<string> commandLineArgs )
        {
            // This creates the logger
            _ = Appliance.Logger;
            return base.OnInitialize( commandLineArgs );
        }

        /// <summary>
        /// When overridden in a derived class, allows a designer to emit code that initializes the
        /// splash screen.
        /// </summary>
        /// <remarks> David, 2020-09-30. </remarks>
        [DebuggerStepThrough()]
        protected override void OnCreateSplashScreen()
        {
            this.CreateSplashScreen() ;
        }

        /// <summary>
        /// Handles the Shutdown event of the MyApplication control. Saves user settings for all related
        /// libraries.
        /// </summary>
        /// <remarks>
        /// This event is not raised if the application terminates abnormally. Application log is set at
        /// verbose level to log shut down operations.
        /// </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnShutdown()
        {
            MyProject.Application.SaveMySettingsOnExit = true;
            // Save library settings here
            this.ProcessShutDown();
            try
            {
                Trace.CorrelationManager.StartLogicalOperation( System.Reflection.MethodBase.GetCurrentMethod().Name );
                if ( MyProject.Application.SaveMySettingsOnExit )
                {
                    _ = this.Logger.TraceEventOverride( TraceEventType.Verbose, TraceEventId, "Saving assembly settings" );
                    Settings.Default.Save();
                }

                this.Logger.Flush();
            }
            catch
            {
            }
            finally
            {
            }

            try
            {
                this.ReleaseSplashScreen();
            }
            catch
            {
            }
            finally
            {
                Trace.CorrelationManager.StopLogicalOperation();
                base.OnShutdown();
            }
        }

        /// <summary> Occurs when the application starts, before the startup form is created. </summary>
        /// <remarks> David, 2020-09-30. </remarks>
        /// <param name="eventArgs"> Startup event information. </param>
        /// <returns>
        /// A <see cref="T:System.Boolean" /> that indicates if the application should continue starting
        /// up.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override bool OnStartup( StartupEventArgs eventArgs )
        {

            if ( eventArgs is null )
            {
                eventArgs = new StartupEventArgs( new System.Collections.ObjectModel.ReadOnlyCollection<string>( Array.Empty<string>() ) );
            }

            string activity = string.Empty;

            // Turn on the screen hourglass
            Cursor.Current = Cursors.AppStarting;
            Application.DoEvents();
            try
            {
                Cursor.Current = Cursors.AppStarting;
                Trace.CorrelationManager.StartLogicalOperation( System.Reflection.MethodBase.GetCurrentMethod().Name );

                activity = $"processing {Appliance.ApplicationBaseTypeLabel()} startup";
                _ = this.Logger.TraceEventOverride( TraceEventType.Verbose, TraceEventId, activity );
                (bool Success, string Details) result = this.ProcessStartup( eventArgs );

                if ( eventArgs.Cancel )
                {

                    // Show the exception message box with three custom buttons.
                    Cursor.Current = Cursors.Default;
                    if ( MyDialogResult.Ok == MyMessageBox.ShowDialogIgnoreExit( $"Failed starting up. Details: {result.Details}.",
                                                                                        "Failed Starting Program", MyMessageBoxIcon.Stop ) )
                    {
                        this.WriteLogEntry( TraceEventType.Error, TraceEventId, $"Application aborted by the user. Details: {result.Details}" );
                        eventArgs.Cancel = true;
                    }
                    else
                    {
                        eventArgs.Cancel = false;
                    }

                    Cursor.Current = Cursors.AppStarting;
                }

                if ( !eventArgs.Cancel )
                {
                    eventArgs.Cancel = !this.TryInitializeKnownState();
                    if ( eventArgs.Cancel )
                    {
                        _ = MyMessageBox.ShowDialogExit( $"Failed initializing application state. Check the program log at '{this.Logger?.FullLogFileName}' for additional information.", "Failed Starting Program", MyMessageBoxIcon.Stop );
                    }
                }

                if ( eventArgs.Cancel )
                {
                    this.WriteLogEntry( TraceEventType.Error, TraceEventId, "Application failed to start up." );
                    this.Logger.Flush();

                    // exit with an error code
                    Environment.Exit( -1 );
                    Application.Exit();
                }
                else if ( this.UserCloseRequested() )
                 {
                    this.WriteLogEntry( TraceEventType.Error, TraceEventId, "User close requested." );
                    this.Logger.Flush();

                    // exit with an error code
                    Environment.Exit( -1 );
                    Application.Exit();
                }
                else
                {
                    this.WriteLogEntry( TraceEventType.Verbose, TraceEventId, "Loading application window..." );
                }
            }
            catch ( Exception ex )
            {
                this.WriteLogEntry( TraceEventType.Error, TraceEventId, "Exception occurred starting application." );
                Cursor.Current = Cursors.Default;
                this.Logger.WriteExceptionDetails( ex, TraceEventId );
                ex.Data.Add( "@isr", "Exception occurred starting this application" );
                if ( MyDialogResult.Abort == MyMessageBox.ShowDialogAbortIgnore( ex ) )
                {
                    // exit with an error code
                    Environment.Exit( -1 );
                    Application.Exit();
                }
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                Trace.CorrelationManager.StopLogicalOperation();
            }

            return base.OnStartup( eventArgs );
        }

        /// <summary>
        /// Occurs when launching a single-instance application and the application is already active.
        /// </summary>
        /// <remarks> David, 2020-09-30. </remarks>
        /// <param name="eventArgs"> Startup next instance event information. </param>
        protected override void OnStartupNextInstance( StartupNextInstanceEventArgs eventArgs )
        {
            this.WriteLogEntry( TraceEventType.Information, TraceEventId, "Application next instant starting." );
            base.OnStartupNextInstance( eventArgs );
        }

        /// <summary>
        /// When overridden in a derived class, allows for code to run when an unhandled exception occurs
        /// in the application.
        /// </summary>
        /// <remarks> David, 2020-09-30. </remarks>
        /// <param name="e"> <see cref="T:Microsoft.VisualBasic.ApplicationServices.UnhandledExceptionEventArgs" />. </param>
        /// <returns>
        /// A <see cref="T:System.Boolean" /> that indicates whether the
        /// <see cref="E:Microsoft.VisualBasic.ApplicationServices.WindowsFormsApplicationBase.UnhandledException" />
        /// event was raised.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override bool OnUnhandledException( Microsoft.VisualBasic.ApplicationServices.UnhandledExceptionEventArgs e )
        {
            string activity = string.Empty;
            bool returnedValue = true;
            if ( e is null )
            {
                Debug.Assert( !Debugger.IsAttached, "Unhandled exception event occurred with event arguments set to nothing." );
                return base.OnUnhandledException( e );
            }

            try
            {
                activity = "flushing the log";
                this.Logger.DefaultFileLogWriter.Flush();
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, $"Exception {activity}", $"Exception {activity};. {ex.ToFullBlownString()}" );
            }

            try
            {
                e.Exception.Data.Add( "@isr", "Unhandled Exception Occurred." );
                this.Logger.WriteExceptionDetails( e.Exception, TraceEventId );
                if ( MyDialogResult.Abort == MyMessageBox.ShowDialogAbortIgnore( e.Exception ) )
                {
                    // exit with an error code
                    Environment.Exit( -1 );
                    Application.Exit();
                }
            }
            catch
            {
                if ( System.Windows.Forms.MessageBox.Show( e.Exception.ToString(), "Unhandled Exception occurred.",
                                                           MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error,
                                                           MessageBoxDefaultButton.Button3, MessageBoxOptions.DefaultDesktopOnly ) == DialogResult.Abort )
                {
                }
            }
            finally
            {
            }

            return returnedValue;
        }

        #endregion

    }

    /// <summary>   Implements the <see cref="isr.Core.ApplianceBase"/> for this application. </summary>
    /// <remarks>   David, 2020-09-29. </remarks>
    [System.ComponentModel.EditorBrowsable( System.ComponentModel.EditorBrowsableState.Never )]
    public class Appliance : isr.Core.ApplianceBase
    {

        #region " CONSTRUCTION "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-09-29. </remarks>
        public Appliance() : base( MyProject.Application )
        {
            // the application info gets by the assembly that reads it for the first time.
            var info = this.Application.Info;
        }

        #endregion

        /// <summary>   Gets the identifier of the trace source. </summary>
        /// <value> The identifier of the trace event. </value>
        public override int TraceEventId => My.MyApplication.TraceEventId;

        /// <summary>   The assembly title. </summary>
        /// <value> The assembly title. </value>
        public override string AssemblyTitle => My.MyApplication.AssemblyTitle;

        /// <summary>   Information describing the assembly. </summary>
        /// <value> Information describing the assembly. </value>
        public override string AssemblyDescription => My.MyApplication.AssemblyDescription;

        /// <summary>   The assembly product. </summary>
        /// <value> The assembly product. </value>
        public override string AssemblyProduct => My.MyApplication.AssemblyProduct;

    }

}
