
using System;
using System.Diagnostics;
using System.Windows.Forms;

using isr.Core.Tester.ExceptionExtensions;

namespace isr.Core.Tester.My
{
    internal partial class MyApplication
    {

        /// <summary> Gets the identifier of the trace source. </summary>
        public const int TraceEventId = ( int ) ProjectTraceEventId.MyBlueSplashScreen;

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "Blue Splash Screen Tester";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "Blue Splash Screen Tester";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "Blue.Splash.Screen.Tester";


        #region " EVENT HANDLING METHODS "

        /// <summary>   Releases the splash. </summary>
        /// <remarks>   David, 2020-09-30. </remarks>
        internal void ReleaseSplashScreen()
        {
            MyProject.Forms.MySplashScreen.Close();
            MyProject.Forms.MySplashScreen.Dispose();
            this.SplashScreen = null;
        }

        /// <summary>   Creates splash screen. </summary>
        /// <remarks>   David, 2020-09-30. </remarks>
        internal void CreateSplashScreen()
        {
            this.WriteLogEntry( TraceEventType.Verbose, MyApplication.TraceEventId, "Creating splash screen" );
            this.MinimumSplashScreenDisplayTime = Settings.Default.SplashMilliseconds;
            this.SplashScreen = MyProject.Forms.MySplashScreen;
            MySplashScreen.CreateInstance( MyProject.Application.SplashScreen );
        }

        /// <summary> Writes a log entry and displays it on the splash screen if exists. </summary>
        /// <remarks> David, 2020-09-30. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="id">        The identifier. </param>
        /// <param name="details">   The details. </param>
        private void WriteLogEntry( TraceEventType eventType, int id, string details )
        {
            (( MySplashScreen ) this.SplashScreen)?.DisplayMessage( details );
            _ = this.Logger.WriteLogEntry( eventType, id, details );
        }

        /// <summary>   Determines if user requested a close. </summary>
        /// <remarks>   David, 2020-09-30. </remarks>
        /// <returns>   True if close requested; false otherwise. </returns>
        internal bool UserCloseRequested()
        {
            return ((( MySplashScreen ) this.SplashScreen)?.IsCloseRequested).GetValueOrDefault( false );
        }


        /// <summary> Instantiates the application to its known state. </summary>
        /// <remarks> David, 2020-09-30. </remarks>
        /// <returns> <c>True</c> if success or <c>False</c> if failed. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private bool TryInitializeKnownState()
        {
            string activity = string.Empty;
            try
            {
                Cursor.Current = Cursors.AppStarting;

                // show status
                activity = Appliance.InDesignMode ? "Application is initializing in design mode." : "Application is initializing in runtime mode.";
                _ = this.Logger.WriteLogEntry( TraceEventType.Verbose, TraceEventId, activity );

                // Apply command line results.
                if ( CommandLineInfo.DevicesEnabled.HasValue )
                {
                    activity = $"command line {(CommandLineInfo.DevicesEnabled.Value ? "allows" : "disallows")} devices";
                    _ = this.Logger.WriteLogEntry( TraceEventType.Information, MyApplication.TraceEventId, activity );
                }

                activity = "handle network availability change";
                this.HandleNetworkAvailabilityChanged();

                return true;
            }
            catch ( Exception ex )
            {

                // Turn off the hourglass
                Cursor.Current = Cursors.Default;
                _ = this.Logger.WriteLogEntry( TraceEventType.Error, MyApplication.TraceEventId, $"Exception {activity};. {ex.ToFullBlownString()}" );
                try
                {
                    this.ReleaseSplashScreen();
                }
                finally
                {
                }

                return false;
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        /// <summary> Processes the shut down. </summary>
        /// <remarks> David, 2020-09-30. </remarks>
        private void ProcessShutDown()
        {
            MyProject.Application.SaveMySettingsOnExit = true;
            if ( MyProject.Application.SaveMySettingsOnExit )
            {
                // Save library settings here
            }
        }


        /// <summary>
        /// Processes the startup. Sets the event arguments
        /// <see cref="Microsoft.VisualBasic.ApplicationServices.StartupEventArgs"/> cancel
        /// value if failed.
        /// </summary>
        /// <remarks> David, 2020-09-30. </remarks>
        /// <param name="e"> The <see cref="Microsoft.VisualBasic.ApplicationServices.StartupEventArgs" />
        /// instance containing the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        private (bool Success, string Details) ProcessStartup( Microsoft.VisualBasic.ApplicationServices.StartupEventArgs e )
        {
            (bool Success, string Details) result = (true, string.Empty);
            if ( !e.Cancel )
            {
                string activity = string.Empty;
                try
                {
                    activity = "creating splash screen instance";
                    MySplashScreen.CreateInstance( MyProject.Application.SplashScreen );

                    activity = "reporting using splash screen";
                    _ = this.Logger.WriteLogEntry( TraceEventType.Verbose, MyApplication.TraceEventId, "Using splash panel" );
                    _ = this.Logger.WriteLogEntry( TraceEventType.Verbose, MyApplication.TraceEventId, "Parsing command line" );

                    activity = "parsing the command line";
                    result = CommandLineInfo.TryParseCommandLine( e.CommandLine );
                }
                catch ( Exception ex )
                {
                    result = (false, $"Exception {activity};. {ex.ToFullBlownString()}");
                    this.Logger.WriteExceptionDetails( ex, TraceEventType.Error, MyApplication.TraceEventId, $"Exception {activity}" );
                }
            }
            e.Cancel = !result.Success;
            return result;
        }

        #endregion

        #region " NETWORK EVENTS "

        /// <summary> Occurs when the network connection is connected or disconnected. </summary>
        /// <remarks> David, 2020-09-30. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Network available event information. </param>
        private void HandleNetworkAvailabilityChanged( object sender, Microsoft.VisualBasic.Devices.NetworkAvailableEventArgs e )
        {
        }

        #endregion

    }
}
