using System;
using System.ComponentModel;
using System.Diagnostics;

namespace isr.Core.Tester.My
{

    internal static partial class MyProject
    {

        internal partial class MyForms
        {
            /// <summary>   The dashboard. </summary>
            [EditorBrowsable(EditorBrowsableState.Never)]
            public Dashboard _Dashboard;

            /// <summary>   Gets or sets the dashboard. </summary>
            /// <exception cref="ArgumentException">    Thrown when one or more arguments have unsupported or
            ///                                         illegal values. </exception>
            /// <value> The dashboard. </value>
            public Dashboard Dashboard
            {
                [DebuggerHidden]
                get
                {
                    _Dashboard = Create__Instance__(_Dashboard);
                    return _Dashboard;
                }

                [DebuggerHidden]
                set
                {
                    if (ReferenceEquals(value, _Dashboard))
                        return;
                    if (value is object)
                        throw new ArgumentException("Property can only be set to Nothing");
                    Dispose__Instance__(ref _Dashboard);
                }
            }

            /// <summary>   my splash screen. </summary>
            [EditorBrowsable(EditorBrowsableState.Never)]
            public MySplashScreen _MySplashScreen;

            /// <summary>   Gets or sets my splash screen. </summary>
            /// <exception cref="ArgumentException">    Thrown when one or more arguments have unsupported or
            ///                                         illegal values. </exception>
            /// <value> my splash screen. </value>
            public MySplashScreen MySplashScreen
            {
                [DebuggerHidden]
                get
                {
                    _MySplashScreen = Create__Instance__(_MySplashScreen);
                    return _MySplashScreen;
                }

                [DebuggerHidden]
                set
                {
                    if (ReferenceEquals(value, _MySplashScreen))
                        return;
                    if (value is object)
                        throw new ArgumentException("Property can only be set to Nothing");
                    Dispose__Instance__(ref _MySplashScreen);
                }
            }
        }
    }
}
