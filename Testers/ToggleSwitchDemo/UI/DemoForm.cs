using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using isr.Core.Controls;

namespace isr.Core.Tester
{

    /// <summary>   Form for viewing the demo. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    public partial class DemoForm : Form
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Windows.Forms.Form" /> class.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        public DemoForm()
        {
            InitializeComponent();
            SetPropertiesForStylesTabSwitches();
            SetPropertiesForPropertiesTabSwitches();
            SetPropertiesForCustomizationsTabSwitches();
        }

        /// <summary>   Sets properties for styles tab switches. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        public void SetPropertiesForStylesTabSwitches()
        {
            // Set the properties for the ToggleSwitches on the "Styles" tab

            MetroStyleToggleSwitch.Style = ToggleSwitchStyle.Metro; // Default
            MetroStyleToggleSwitch.Size = new Size(75, 23);
            IOS5StyleToggleSwitch.Style = ToggleSwitchStyle.Ios5;
            IOS5StyleToggleSwitch.Size = new Size(98, 42);
            IOS5StyleToggleSwitch.OnText = "ON";
            IOS5StyleToggleSwitch.OnFont = new Font(DemoTabControl.Font.FontFamily, 12f, FontStyle.Bold);
            IOS5StyleToggleSwitch.OnForeColor = Color.White;
            IOS5StyleToggleSwitch.OffText = "OFF";
            IOS5StyleToggleSwitch.OffFont = new Font(DemoTabControl.Font.FontFamily, 12f, FontStyle.Bold);
            IOS5StyleToggleSwitch.OffForeColor = Color.FromArgb(141, 123, 141);
            AndroidStyleToggleSwitch.Style = ToggleSwitchStyle.Android;
            AndroidStyleToggleSwitch.Size = new Size(78, 23);
            AndroidStyleToggleSwitch.OnText = "ON";
            AndroidStyleToggleSwitch.OnFont = new Font(DemoTabControl.Font.FontFamily, 8f, FontStyle.Bold);
            AndroidStyleToggleSwitch.OnForeColor = Color.White;
            AndroidStyleToggleSwitch.OffText = "OFF";
            AndroidStyleToggleSwitch.OffFont = new Font(DemoTabControl.Font.FontFamily, 8f, FontStyle.Bold);
            AndroidStyleToggleSwitch.OffForeColor = Color.FromArgb(141, 123, 141);
            BrushedMetalStyleToggleSwitch.Style = ToggleSwitchStyle.BrushedMetal;
            BrushedMetalStyleToggleSwitch.Size = new Size(93, 30);
            BrushedMetalStyleToggleSwitch.OnText = "ON";
            BrushedMetalStyleToggleSwitch.OnFont = new Font(DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold);
            BrushedMetalStyleToggleSwitch.OnForeColor = Color.White;
            BrushedMetalStyleToggleSwitch.OffText = "OFF";
            BrushedMetalStyleToggleSwitch.OffFont = new Font(DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold);
            BrushedMetalStyleToggleSwitch.OffForeColor = Color.White;
            IphoneStyleToggleSwitch.Style = ToggleSwitchStyle.IPhone;
            IphoneStyleToggleSwitch.Size = new Size(93, 30);
            IphoneStyleToggleSwitch.OnText = "ON";
            IphoneStyleToggleSwitch.OnFont = new Font(DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold);
            IphoneStyleToggleSwitch.OnForeColor = Color.White;
            IphoneStyleToggleSwitch.OffText = "OFF";
            IphoneStyleToggleSwitch.OffFont = new Font(DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold);
            IphoneStyleToggleSwitch.OffForeColor = Color.FromArgb(92, 92, 92);
            ModernStyleToggleSwitch.Style = ToggleSwitchStyle.Modern;
            ModernStyleToggleSwitch.Size = new Size(85, 32);
            ModernStyleToggleSwitch.OnText = "ON";
            ModernStyleToggleSwitch.OnFont = new Font(DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold);
            ModernStyleToggleSwitch.OnForeColor = Color.White;
            ModernStyleToggleSwitch.OffText = "OFF";
            ModernStyleToggleSwitch.OffFont = new Font(DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold);
            ModernStyleToggleSwitch.OffForeColor = Color.FromArgb(153, 153, 153);
            CarbonStyleToggleSwitch.Style = ToggleSwitchStyle.Carbon;
            CarbonStyleToggleSwitch.Size = new Size(93, 30);
            CarbonStyleToggleSwitch.OnText = "On";
            CarbonStyleToggleSwitch.OnFont = new Font(DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold);
            CarbonStyleToggleSwitch.OnForeColor = Color.White;
            CarbonStyleToggleSwitch.OffText = "Off";
            CarbonStyleToggleSwitch.OffFont = new Font(DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold);
            CarbonStyleToggleSwitch.OffForeColor = Color.White;
            OSXStyleToggleSwitch.Style = ToggleSwitchStyle.OS10;
            OSXStyleToggleSwitch.Size = new Size(93, 25);
            FancyStyleToggleSwitch.Style = ToggleSwitchStyle.Fancy;
            FancyStyleToggleSwitch.Size = new Size(100, 30);
            FancyStyleToggleSwitch.OnText = "ON";
            FancyStyleToggleSwitch.OnFont = new Font(DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold);
            FancyStyleToggleSwitch.OnForeColor = Color.White;
            FancyStyleToggleSwitch.OffText = "OFF";
            FancyStyleToggleSwitch.OffFont = new Font(DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold);
            FancyStyleToggleSwitch.OffForeColor = Color.White;
            FancyStyleToggleSwitch.ButtonImage = My.Resources.Resources.handle;
        }

        /// <summary>   Sets properties for properties tab switches. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        public void SetPropertiesForPropertiesTabSwitches()
        {
            // Set the properties for the ToggleSwitches on the "(Semi)-Important Properties" tab

            // AllowUserChange example:

            AllowUserChangeToggleSwitch1.AllowUserChange = false;
            AllowUserChangeToggleSwitch2.AllowUserChange = true;

            // Animation example:

            NoAnimationToggleSwitch.Style = ToggleSwitchStyle.Carbon; // Only to provide an interesting look
            NoAnimationToggleSwitch.Size = new Size(93, 30); // Only to provide an interesting look
            NoAnimationToggleSwitch.OnText = "On"; // Only to provide an interesting look
            NoAnimationToggleSwitch.OnFont = new Font(DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold); // Only to provide an interesting look
            NoAnimationToggleSwitch.OnForeColor = Color.White; // Only to provide an interesting look
            NoAnimationToggleSwitch.OffText = "Off"; // Only to provide an interesting look
            NoAnimationToggleSwitch.OffFont = new Font(DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold); // Only to provide an interesting look
            NoAnimationToggleSwitch.OffForeColor = Color.White; // Only to provide an interesting look
            NoAnimationToggleSwitch.UseAnimation = false;
            FastAnimationToggleSwitch.Style = ToggleSwitchStyle.Carbon; // Only to provide an interesting look
            FastAnimationToggleSwitch.Size = new Size(93, 30); // Only to provide an interesting look
            FastAnimationToggleSwitch.OnText = "On"; // Only to provide an interesting look
            FastAnimationToggleSwitch.OnFont = new Font(DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold); // Only to provide an interesting look
            FastAnimationToggleSwitch.OnForeColor = Color.White; // Only to provide an interesting look
            FastAnimationToggleSwitch.OffText = "Off"; // Only to provide an interesting look
            FastAnimationToggleSwitch.OffFont = new Font(DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold); // Only to provide an interesting look
            FastAnimationToggleSwitch.OffForeColor = Color.White; // Only to provide an interesting look
            FastAnimationToggleSwitch.UseAnimation = true; // Default
            FastAnimationToggleSwitch.AnimationInterval = 1; // Default
            FastAnimationToggleSwitch.AnimationStep = 10; // Default
            SlowAnimationToggleSwitch.Style = ToggleSwitchStyle.Carbon; // Only to provide an interesting look
            SlowAnimationToggleSwitch.Size = new Size(93, 30); // Only to provide an interesting look
            SlowAnimationToggleSwitch.OnText = "On"; // Only to provide an interesting look
            SlowAnimationToggleSwitch.OnFont = new Font(DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold); // Only to provide an interesting look
            SlowAnimationToggleSwitch.OnForeColor = Color.White; // Only to provide an interesting look
            SlowAnimationToggleSwitch.OffText = "Off"; // Only to provide an interesting look
            SlowAnimationToggleSwitch.OffFont = new Font(DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold); // Only to provide an interesting look
            SlowAnimationToggleSwitch.OffForeColor = Color.White; // Only to provide an interesting look
            SlowAnimationToggleSwitch.UseAnimation = true; // Default
            SlowAnimationToggleSwitch.AnimationInterval = 10;
            SlowAnimationToggleSwitch.AnimationStep = 1;

            // GrayWhenDisabled example:

            GrayWhenDisabledToggleSwitch1.Style = ToggleSwitchStyle.Fancy; // Only to provide an interesting look
            GrayWhenDisabledToggleSwitch1.Size = new Size(100, 30); // Only to provide an interesting look
            GrayWhenDisabledToggleSwitch1.OnText = "ON"; // Only to provide an interesting look
            GrayWhenDisabledToggleSwitch1.OnFont = new Font(DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold); // Only to provide an interesting look
            GrayWhenDisabledToggleSwitch1.OnForeColor = Color.White; // Only to provide an interesting look
            GrayWhenDisabledToggleSwitch1.OffText = "OFF"; // Only to provide an interesting look
            GrayWhenDisabledToggleSwitch1.OffFont = new Font(DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold); // Only to provide an interesting look
            GrayWhenDisabledToggleSwitch1.OffForeColor = Color.White; // Only to provide an interesting look
            GrayWhenDisabledToggleSwitch1.ButtonImage = My.Resources.Resources.arrowright; // Only to provide an interesting look
            GrayWhenDisabledToggleSwitch1.GrayWhenDisabled = false;
            GrayWhenDisabledToggleSwitch2.Style = ToggleSwitchStyle.Fancy; // Only to provide an interesting look
            GrayWhenDisabledToggleSwitch2.Size = new Size(100, 30); // Only to provide an interesting look
            GrayWhenDisabledToggleSwitch2.OnText = "ON"; // Only to provide an interesting look
            GrayWhenDisabledToggleSwitch2.OnFont = new Font(DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold); // Only to provide an interesting look
            GrayWhenDisabledToggleSwitch2.OnForeColor = Color.White; // Only to provide an interesting look
            GrayWhenDisabledToggleSwitch2.OffText = "OFF"; // Only to provide an interesting look
            GrayWhenDisabledToggleSwitch2.OffFont = new Font(DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold); // Only to provide an interesting look
            GrayWhenDisabledToggleSwitch2.OffForeColor = Color.White; // Only to provide an interesting look
            GrayWhenDisabledToggleSwitch2.ButtonImage = My.Resources.Resources.arrowright; // Only to provide an interesting look
            GrayWhenDisabledToggleSwitch2.GrayWhenDisabled = true; // Default

            // ThresholdPercentage example:

            ThresholdPercentageToggleSwitch.Style = ToggleSwitchStyle.Ios5; // Only to provide an interesting look
            ThresholdPercentageToggleSwitch.Size = new Size(98, 42); // Only to provide an interesting look
            ThresholdPercentageToggleSwitch.OnText = "ON"; // Only to provide an interesting look
            ThresholdPercentageToggleSwitch.OnFont = new Font(DemoTabControl.Font.FontFamily, 12f, FontStyle.Bold); // Only to provide an interesting look
            ThresholdPercentageToggleSwitch.OnForeColor = Color.White; // Only to provide an interesting look
            ThresholdPercentageToggleSwitch.OffText = "OFF"; // Only to provide an interesting look
            ThresholdPercentageToggleSwitch.OffFont = new Font(DemoTabControl.Font.FontFamily, 12f, FontStyle.Bold); // Only to provide an interesting look
            ThresholdPercentageToggleSwitch.OffForeColor = Color.FromArgb(141, 123, 141); // Only to provide an interesting look
            ThresholdPercentageToggleSwitch.ThresholdPercentage = 50; // Default

            // ToggleOnButtonClick & ToggleOnSideClick example:

            ToggleOnClickToggleSwitch.Style = ToggleSwitchStyle.BrushedMetal; // Only to provide an interesting look
            ToggleOnClickToggleSwitch.Size = new Size(93, 30); // Only to provide an interesting look
            ToggleOnClickToggleSwitch.OnText = "ON"; // Only to provide an interesting look
            ToggleOnClickToggleSwitch.OnFont = new Font(DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold); // Only to provide an interesting look
            ToggleOnClickToggleSwitch.OnForeColor = Color.White; // Only to provide an interesting look
            ToggleOnClickToggleSwitch.OffText = "OFF"; // Only to provide an interesting look
            ToggleOnClickToggleSwitch.OffFont = new Font(DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold); // Only to provide an interesting look
            ToggleOnClickToggleSwitch.OffForeColor = Color.White; // Only to provide an interesting look
            ToggleOnClickToggleSwitch.ToggleOnButtonClick = true; // Default
            ToggleOnClickToggleSwitch.ToggleOnSideClick = true; // Default
        }

        /// <summary>   Sets properties for customizations tab switches. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        public void SetPropertiesForCustomizationsTabSwitches()
        {
            // Set the properties for the ToggleSwitches on the "Special Customizations" tab

            // Color customization example, Metro Style ToggleSwitch:

            NormalMetroToggleSwitch.Style = ToggleSwitchStyle.Metro; // Default
            NormalMetroToggleSwitch.Size = new Size(75, 23);
            var customizedMetroRenderer = new ToggleSwitchMetroRenderer()
            {
                LeftSideColor = Color.Red,
                LeftSideColorHovered = Color.FromArgb(210, 0, 0),
                LeftSideColorPressed = Color.FromArgb(190, 0, 0),
                RightSideColor = Color.Yellow,
                RightSideColorHovered = Color.FromArgb(245, 245, 0),
                RightSideColorPressed = Color.FromArgb(235, 235, 0)
            };
            CustomizedMetroToggleSwitch.Style = ToggleSwitchStyle.Metro; // Default
            CustomizedMetroToggleSwitch.Size = new Size(75, 23);
            CustomizedMetroToggleSwitch.SetRenderer(customizedMetroRenderer);

            // Color customization example, IOS5 Style ToggleSwitch:

            NormalIOS5ToggleSwitch.Style = ToggleSwitchStyle.Ios5;
            NormalIOS5ToggleSwitch.Size = new Size(98, 42);
            NormalIOS5ToggleSwitch.OnText = "ON";
            NormalIOS5ToggleSwitch.OnFont = new Font(DemoTabControl.Font.FontFamily, 12f, FontStyle.Bold);
            NormalIOS5ToggleSwitch.OnForeColor = Color.White;
            NormalIOS5ToggleSwitch.OffText = "OFF";
            NormalIOS5ToggleSwitch.OffFont = new Font(DemoTabControl.Font.FontFamily, 12f, FontStyle.Bold);
            NormalIOS5ToggleSwitch.OffForeColor = Color.FromArgb(141, 123, 141);

            // Maybe not the prettiest color scheme in the world - It's just for demonstration :-)
            var customizedIos5Renderer = new ToggleSwitchIos5Renderer()
            {
                LeftSideUpperColor1 = Color.FromArgb(128, 0, 64),
                LeftSideUpperColor2 = Color.FromArgb(180, 0, 90),
                LeftSideLowerColor1 = Color.FromArgb(250, 0, 125),
                LeftSideLowerColor2 = Color.FromArgb(255, 120, 190),
                RightSideUpperColor1 = Color.FromArgb(0, 64, 128),
                RightSideUpperColor2 = Color.FromArgb(0, 90, 180),
                RightSideLowerColor1 = Color.FromArgb(0, 125, 250),
                RightSideLowerColor2 = Color.FromArgb(120, 190, 255),
                ButtonNormalOuterBorderColor = Color.Green,
                ButtonNormalInnerBorderColor = Color.Green,
                ButtonNormalSurfaceColor1 = Color.Red,
                ButtonNormalSurfaceColor2 = Color.Red,
                ButtonHoverOuterBorderColor = Color.Green,
                ButtonHoverInnerBorderColor = Color.Green,
                ButtonHoverSurfaceColor1 = Color.Red,
                ButtonHoverSurfaceColor2 = Color.Red,
                ButtonPressedOuterBorderColor = Color.Green,
                ButtonPressedInnerBorderColor = Color.Green,
                ButtonPressedSurfaceColor1 = Color.Red,
                ButtonPressedSurfaceColor2 = Color.Red
            };
            CustomizedIOS5ToggleSwitch.Style = ToggleSwitchStyle.Ios5;
            CustomizedIOS5ToggleSwitch.Size = new Size(98, 42);
            CustomizedIOS5ToggleSwitch.OnText = "ON";
            CustomizedIOS5ToggleSwitch.OnFont = new Font(DemoTabControl.Font.FontFamily, 12f, FontStyle.Bold);
            CustomizedIOS5ToggleSwitch.OnForeColor = Color.White;
            CustomizedIOS5ToggleSwitch.OffText = "OFF";
            CustomizedIOS5ToggleSwitch.OffFont = new Font(DemoTabControl.Font.FontFamily, 12f, FontStyle.Bold);
            CustomizedIOS5ToggleSwitch.OffForeColor = Color.White; // OBS: Need to change this for text visibility
            CustomizedIOS5ToggleSwitch.SetRenderer(customizedIos5Renderer);

            // Image customization example, Fancy Style ToggleSwitch:

            NormalFancyToggleSwitch.Style = ToggleSwitchStyle.Fancy;
            NormalFancyToggleSwitch.Size = new Size(100, 30);
            NormalFancyToggleSwitch.OnText = "ON";
            NormalFancyToggleSwitch.OnFont = new Font(DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold);
            NormalFancyToggleSwitch.OnForeColor = Color.White;
            NormalFancyToggleSwitch.OffText = "OFF";
            NormalFancyToggleSwitch.OffFont = new Font(DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold);
            NormalFancyToggleSwitch.OffForeColor = Color.White;
            CustomizedFancyToggleSwitch.Style = ToggleSwitchStyle.Fancy;
            CustomizedFancyToggleSwitch.Size = new Size(100, 30);
            CustomizedFancyToggleSwitch.OffButtonImage = My.Resources.Resources.arrowright;
            CustomizedFancyToggleSwitch.OffSideImage = My.Resources.Resources.cross;
            CustomizedFancyToggleSwitch.OnButtonImage = My.Resources.Resources.arrowleft;
            CustomizedFancyToggleSwitch.OnSideImage = My.Resources.Resources.check;

            // Advanced behavior example, Fancy Style ToggleSwitch:

            Color tempColor;
            var customizedFancyRenderer = new ToggleSwitchFancyRenderer();
            tempColor = customizedFancyRenderer.LeftSideBackColor1;
            customizedFancyRenderer.LeftSideBackColor1 = customizedFancyRenderer.RightSideBackColor1;
            customizedFancyRenderer.RightSideBackColor1 = tempColor;
            tempColor = customizedFancyRenderer.LeftSideBackColor2;
            customizedFancyRenderer.LeftSideBackColor2 = customizedFancyRenderer.RightSideBackColor2;
            customizedFancyRenderer.RightSideBackColor2 = tempColor;
            AdvancedBehaviorFancyToggleSwitch.Style = ToggleSwitchStyle.Fancy;
            AdvancedBehaviorFancyToggleSwitch.Size = new Size(150, 30);
            AdvancedBehaviorFancyToggleSwitch.OnText = "Restart";
            AdvancedBehaviorFancyToggleSwitch.OnFont = new Font(DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold);
            AdvancedBehaviorFancyToggleSwitch.OnForeColor = Color.White;
            AdvancedBehaviorFancyToggleSwitch.OffText = "Online";
            AdvancedBehaviorFancyToggleSwitch.OffFont = new Font(DemoTabControl.Font.FontFamily, 10f, FontStyle.Bold);
            AdvancedBehaviorFancyToggleSwitch.OffForeColor = Color.White;
            AdvancedBehaviorFancyToggleSwitch.OffButtonImage = My.Resources.Resources.arrowright;
            AdvancedBehaviorFancyToggleSwitch.UseAnimation = false;
            AdvancedBehaviorFancyToggleSwitch.SetRenderer(customizedFancyRenderer);
            AdvancedBehaviorFancyToggleSwitch.CheckedChanged += AdvancedBehaviorFancyToggleSwitch_CheckedChanged;
            AnimatedGifPictureBox.Parent = AdvancedBehaviorFancyToggleSwitch; // Necessary to get the ToggleSwitch button to show through the picture box' transparent background
        }

        /// <summary>   Allow user change check box checked changed. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void AllowUserChangeCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            AllowUserChangeToggleSwitch1.Checked = AllowUserChangeCheckBox.Checked;
            AllowUserChangeToggleSwitch2.Checked = AllowUserChangeCheckBox.Checked;
        }

        /// <summary>   Gray when disabled check box checked changed. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void GrayWhenDisabledCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            GrayWhenDisabledToggleSwitch1.Enabled = GrayWhenDisabledCheckBox.Checked;
            GrayWhenDisabledToggleSwitch2.Enabled = GrayWhenDisabledCheckBox.Checked;
        }

        /// <summary>   Threshold percentage track bar scroll. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void ThresholdPercentageTrackBar_Scroll(object sender, EventArgs e)
        {
            label15.Text = $"Value = {ThresholdPercentageTrackBar.Value} (Default = 50)";
            ThresholdPercentageToggleSwitch.ThresholdPercentage = ThresholdPercentageTrackBar.Value;
        }

        /// <summary>   Toggle on button click check box checked changed. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void ToggleOnButtonClickCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            ToggleOnClickToggleSwitch.ToggleOnButtonClick = ToggleOnButtonClickCheckBox.Checked;
        }

        /// <summary>   Toggle on side click check box checked changed. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void ToggleOnSideClickCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            ToggleOnClickToggleSwitch.ToggleOnSideClick = ToggleOnSideClickCheckBox.Checked;
        }

        /// <summary>
        /// Event handler. Called by AdvancedBehaviorFancyToggleSwitch for checked changed events.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void AdvancedBehaviorFancyToggleSwitch_CheckedChanged(object sender, EventArgs e)
        {
            if (AdvancedBehaviorFancyToggleSwitch.Checked)
            {
                AdvancedBehaviorFancyToggleSwitch.AllowUserChange = false;
                AdvancedBehaviorFancyToggleSwitch.OnText = "Restarting...";
                PositionAniGifPictureBox();
                AnimatedGifPictureBox.Visible = true;
                if (!SimulateRestartBackgroundWorker.IsBusy)
                {
                    SimulateRestartBackgroundWorker.RunWorkerAsync();
                }
            }
            else
            {
                AdvancedBehaviorFancyToggleSwitch.AllowUserChange = true;
                AdvancedBehaviorFancyToggleSwitch.OnText = "Restart";
            }
        }

        /// <summary>   Position animation GIF picture box. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        private void PositionAniGifPictureBox()
        {
            // Position anigif picturebox

            var buttonRectangle = AdvancedBehaviorFancyToggleSwitch.ButtonRectangle;
            AnimatedGifPictureBox.Height = buttonRectangle.Height - 2;
            AnimatedGifPictureBox.Width = AnimatedGifPictureBox.Height;
            AnimatedGifPictureBox.Left = buttonRectangle.X + (buttonRectangle.Width - AnimatedGifPictureBox.Width) / 2;
            AnimatedGifPictureBox.Top = buttonRectangle.Y + (buttonRectangle.Height - AnimatedGifPictureBox.Height) / 2;
        }

        /// <summary>   Simulate restart background worker do work. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Do work event information. </param>
        private void SimulateRestartBackgroundWorker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            // Simulate restart delay
            Thread.Sleep(1500);
        }

        /// <summary>   Simulate restart background worker run worker completed. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Run worker completed event information. </param>
        private void SimulateRestartBackgroundWorker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            AnimatedGifPictureBox.Visible = false;
            AdvancedBehaviorFancyToggleSwitch.Checked = false;
        }
    }
}
