using System;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace isr.Core.Tester
{
    public partial class DemoForm
    {
        /// <summary>   Required designer variable. </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>   Clean up any resources being used. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="disposing">    true if managed resources should be disposed; otherwise, false. </param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && components is object)
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        /// <summary>
        /// Required method for Designer support - do not modify the contents of this method with the
        /// code editor.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(DemoForm));
            TopPanel = new Panel();
            InfoLabel = new Label();
            DemoTabControl = new TabControl();
            StylesTabPage = new TabPage();
            OSXStyleOnLabel = new Label();
            OSXStyleOffLabel = new Label();
            FancyStyleToggleSwitch = new Controls.ToggleSwitch();
            OSXStyleToggleSwitch = new Controls.ToggleSwitch();
            CarbonStyleToggleSwitch = new Controls.ToggleSwitch();
            ModernStyleToggleSwitch = new Controls.ToggleSwitch();
            IphoneStyleToggleSwitch = new Controls.ToggleSwitch();
            BrushedMetalStyleToggleSwitch = new Controls.ToggleSwitch();
            AndroidStyleToggleSwitch = new Controls.ToggleSwitch();
            IOS5StyleToggleSwitch = new Controls.ToggleSwitch();
            MetroStyleToggleSwitch = new Controls.ToggleSwitch();
            OSXStyleLabel = new Label();
            ModernStyleLabel = new Label();
            IphoneStyleLabel = new Label();
            IOS5StyleLabel = new Label();
            FancyStyleLabel = new Label();
            CarbonStyleLabel = new Label();
            BrushedMetalStyleLabel = new Label();
            AndroidStyleLabel = new Label();
            MetroStyleLabel = new Label();
            SemiImportantPropertiesTabPage = new TabPage();
            label19 = new Label();
            _ToggleOnSideClickCheckBox = new CheckBox();
            _ToggleOnSideClickCheckBox.CheckedChanged += new EventHandler(ToggleOnSideClickCheckBox_CheckedChanged);
            _ToggleOnButtonClickCheckBox = new CheckBox();
            _ToggleOnButtonClickCheckBox.CheckedChanged += new EventHandler(ToggleOnButtonClickCheckBox_CheckedChanged);
            label18 = new Label();
            ToggleOnClickToggleSwitch = new Controls.ToggleSwitch();
            label17 = new Label();
            label16 = new Label();
            label15 = new Label();
            _ThresholdPercentageTrackBar = new TrackBar();
            _ThresholdPercentageTrackBar.Scroll += new EventHandler(ThresholdPercentageTrackBar_Scroll);
            ThresholdPercentageToggleSwitch = new Controls.ToggleSwitch();
            label11 = new Label();
            label12 = new Label();
            _GrayWhenDisabledCheckBox = new CheckBox();
            _GrayWhenDisabledCheckBox.CheckedChanged += new EventHandler(GrayWhenDisabledCheckBox_CheckedChanged);
            label13 = new Label();
            label14 = new Label();
            GrayWhenDisabledToggleSwitch2 = new Controls.ToggleSwitch();
            GrayWhenDisabledToggleSwitch1 = new Controls.ToggleSwitch();
            label10 = new Label();
            label9 = new Label();
            label8 = new Label();
            SlowAnimationToggleSwitch = new Controls.ToggleSwitch();
            label7 = new Label();
            FastAnimationToggleSwitch = new Controls.ToggleSwitch();
            label6 = new Label();
            NoAnimationToggleSwitch = new Controls.ToggleSwitch();
            label5 = new Label();
            label4 = new Label();
            label3 = new Label();
            _AllowUserChangeCheckBox = new CheckBox();
            _AllowUserChangeCheckBox.CheckedChanged += new EventHandler(AllowUserChangeCheckBox_CheckedChanged);
            label2 = new Label();
            label1 = new Label();
            AllowUserChangeToggleSwitch2 = new Controls.ToggleSwitch();
            AllowUserChangeToggleSwitch1 = new Controls.ToggleSwitch();
            AllowUserChangeLabel = new Label();
            SpecialCustomizationsTabPage = new TabPage();
            AnimatedGifPictureBox = new PictureBox();
            AdvancedBehaviorFancyToggleSwitch = new Controls.ToggleSwitch();
            label28 = new Label();
            label26 = new Label();
            label27 = new Label();
            CustomizedFancyToggleSwitch = new Controls.ToggleSwitch();
            NormalFancyToggleSwitch = new Controls.ToggleSwitch();
            label25 = new Label();
            label23 = new Label();
            label24 = new Label();
            CustomizedIOS5ToggleSwitch = new Controls.ToggleSwitch();
            NormalIOS5ToggleSwitch = new Controls.ToggleSwitch();
            label22 = new Label();
            label21 = new Label();
            CustomizedMetroToggleSwitch = new Controls.ToggleSwitch();
            NormalMetroToggleSwitch = new Controls.ToggleSwitch();
            label20 = new Label();
            PlaygroundTabPage = new TabPage();
            PlaygroundToggleSwitch = new Controls.ToggleSwitch();
            PlaygroundPropertyGrid = new PropertyGrid();
            _SimulateRestartBackgroundWorker = new System.ComponentModel.BackgroundWorker();
            _SimulateRestartBackgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(SimulateRestartBackgroundWorker_DoWork);
            _SimulateRestartBackgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(SimulateRestartBackgroundWorker_RunWorkerCompleted);
            TopPanel.SuspendLayout();
            DemoTabControl.SuspendLayout();
            StylesTabPage.SuspendLayout();
            SemiImportantPropertiesTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_ThresholdPercentageTrackBar).BeginInit();
            SpecialCustomizationsTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)AnimatedGifPictureBox).BeginInit();
            PlaygroundTabPage.SuspendLayout();
            SuspendLayout();
            // 
            // TopPanel
            // 
            TopPanel.BackColor = Color.White;
            TopPanel.Controls.Add(InfoLabel);
            TopPanel.Dock = DockStyle.Top;
            TopPanel.Location = new Point(0, 0);
            TopPanel.Name = "TopPanel";
            TopPanel.Size = new Size(878, 100);
            TopPanel.TabIndex = 0;
            // 
            // InfoLabel
            // 
            InfoLabel.Dock = DockStyle.Fill;
            InfoLabel.Location = new Point(0, 0);
            InfoLabel.Name = "InfoLabel";
            InfoLabel.Padding = new Padding(5);
            InfoLabel.Size = new Size(878, 100);
            InfoLabel.TabIndex = 0;
            InfoLabel.Text = resources.GetString("InfoLabel.Text");
            // 
            // DemoTabControl
            // 
            DemoTabControl.Controls.Add(StylesTabPage);
            DemoTabControl.Controls.Add(SemiImportantPropertiesTabPage);
            DemoTabControl.Controls.Add(SpecialCustomizationsTabPage);
            DemoTabControl.Controls.Add(PlaygroundTabPage);
            DemoTabControl.Dock = DockStyle.Fill;
            DemoTabControl.Location = new Point(0, 100);
            DemoTabControl.Name = "DemoTabControl";
            DemoTabControl.SelectedIndex = 0;
            DemoTabControl.Size = new Size(878, 505);
            DemoTabControl.TabIndex = 1;
            // 
            // StylesTabPage
            // 
            StylesTabPage.Controls.Add(OSXStyleOnLabel);
            StylesTabPage.Controls.Add(OSXStyleOffLabel);
            StylesTabPage.Controls.Add(FancyStyleToggleSwitch);
            StylesTabPage.Controls.Add(OSXStyleToggleSwitch);
            StylesTabPage.Controls.Add(CarbonStyleToggleSwitch);
            StylesTabPage.Controls.Add(ModernStyleToggleSwitch);
            StylesTabPage.Controls.Add(IphoneStyleToggleSwitch);
            StylesTabPage.Controls.Add(BrushedMetalStyleToggleSwitch);
            StylesTabPage.Controls.Add(AndroidStyleToggleSwitch);
            StylesTabPage.Controls.Add(IOS5StyleToggleSwitch);
            StylesTabPage.Controls.Add(MetroStyleToggleSwitch);
            StylesTabPage.Controls.Add(OSXStyleLabel);
            StylesTabPage.Controls.Add(ModernStyleLabel);
            StylesTabPage.Controls.Add(IphoneStyleLabel);
            StylesTabPage.Controls.Add(IOS5StyleLabel);
            StylesTabPage.Controls.Add(FancyStyleLabel);
            StylesTabPage.Controls.Add(CarbonStyleLabel);
            StylesTabPage.Controls.Add(BrushedMetalStyleLabel);
            StylesTabPage.Controls.Add(AndroidStyleLabel);
            StylesTabPage.Controls.Add(MetroStyleLabel);
            StylesTabPage.Location = new Point(4, 22);
            StylesTabPage.Name = "StylesTabPage";
            StylesTabPage.Padding = new Padding(3);
            StylesTabPage.Size = new Size(870, 479);
            StylesTabPage.TabIndex = 0;
            StylesTabPage.Text = "Styles";
            StylesTabPage.UseVisualStyleBackColor = true;
            // 
            // OSXStyleOnLabel
            // 
            OSXStyleOnLabel.AutoSize = true;
            OSXStyleOnLabel.Font = new Font("Microsoft Sans Serif", 10.0f, FontStyle.Bold, GraphicsUnit.Point, 0);
            OSXStyleOnLabel.Location = new Point(530, 312);
            OSXStyleOnLabel.Name = "OSXStyleOnLabel";
            OSXStyleOnLabel.Size = new Size(31, 17);
            OSXStyleOnLabel.TabIndex = 19;
            OSXStyleOnLabel.Text = "ON";
            // 
            // OSXStyleOffLabel
            // 
            OSXStyleOffLabel.AutoSize = true;
            OSXStyleOffLabel.Font = new Font("Microsoft Sans Serif", 10.0f, FontStyle.Bold, GraphicsUnit.Point, 0);
            OSXStyleOffLabel.Location = new Point(392, 312);
            OSXStyleOffLabel.Name = "OSXStyleOffLabel";
            OSXStyleOffLabel.Size = new Size(38, 17);
            OSXStyleOffLabel.TabIndex = 18;
            OSXStyleOffLabel.Text = "OFF";
            // 
            // FancyStyleToggleSwitch
            // 
            FancyStyleToggleSwitch.Location = new Point(626, 309);
            FancyStyleToggleSwitch.Name = "FancyStyleToggleSwitch";
            FancyStyleToggleSwitch.OffFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            FancyStyleToggleSwitch.OnFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            FancyStyleToggleSwitch.Size = new Size(50, 19);
            FancyStyleToggleSwitch.TabIndex = 17;
            // 
            // OSXStyleToggleSwitch
            // 
            OSXStyleToggleSwitch.Location = new Point(434, 309);
            OSXStyleToggleSwitch.Name = "OSXStyleToggleSwitch";
            OSXStyleToggleSwitch.OffFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            OSXStyleToggleSwitch.OnFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            OSXStyleToggleSwitch.Size = new Size(50, 19);
            OSXStyleToggleSwitch.TabIndex = 16;
            // 
            // CarbonStyleToggleSwitch
            // 
            CarbonStyleToggleSwitch.Location = new Point(164, 309);
            CarbonStyleToggleSwitch.Name = "CarbonStyleToggleSwitch";
            CarbonStyleToggleSwitch.OffFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            CarbonStyleToggleSwitch.OnFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            CarbonStyleToggleSwitch.Size = new Size(50, 19);
            CarbonStyleToggleSwitch.TabIndex = 15;
            // 
            // ModernStyleToggleSwitch
            // 
            ModernStyleToggleSwitch.Location = new Point(626, 188);
            ModernStyleToggleSwitch.Name = "ModernStyleToggleSwitch";
            ModernStyleToggleSwitch.OffFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            ModernStyleToggleSwitch.OnFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            ModernStyleToggleSwitch.Size = new Size(50, 19);
            ModernStyleToggleSwitch.TabIndex = 14;
            // 
            // IphoneStyleToggleSwitch
            // 
            IphoneStyleToggleSwitch.Location = new Point(395, 188);
            IphoneStyleToggleSwitch.Name = "IphoneStyleToggleSwitch";
            IphoneStyleToggleSwitch.OffFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            IphoneStyleToggleSwitch.OnFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            IphoneStyleToggleSwitch.Size = new Size(50, 19);
            IphoneStyleToggleSwitch.TabIndex = 13;
            // 
            // BrushedMetalStyleToggleSwitch
            // 
            BrushedMetalStyleToggleSwitch.Location = new Point(164, 188);
            BrushedMetalStyleToggleSwitch.Name = "BrushedMetalStyleToggleSwitch";
            BrushedMetalStyleToggleSwitch.OffFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            BrushedMetalStyleToggleSwitch.OnFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            BrushedMetalStyleToggleSwitch.Size = new Size(50, 19);
            BrushedMetalStyleToggleSwitch.TabIndex = 12;
            // 
            // AndroidStyleToggleSwitch
            // 
            AndroidStyleToggleSwitch.Location = new Point(626, 79);
            AndroidStyleToggleSwitch.Name = "AndroidStyleToggleSwitch";
            AndroidStyleToggleSwitch.OffFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            AndroidStyleToggleSwitch.OnFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            AndroidStyleToggleSwitch.Size = new Size(50, 19);
            AndroidStyleToggleSwitch.TabIndex = 11;
            // 
            // IOS5StyleToggleSwitch
            // 
            IOS5StyleToggleSwitch.Location = new Point(395, 79);
            IOS5StyleToggleSwitch.Name = "IOS5StyleToggleSwitch";
            IOS5StyleToggleSwitch.OffFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            IOS5StyleToggleSwitch.OnFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            IOS5StyleToggleSwitch.Size = new Size(50, 19);
            IOS5StyleToggleSwitch.TabIndex = 10;
            // 
            // MetroStyleToggleSwitch
            // 
            MetroStyleToggleSwitch.Location = new Point(164, 79);
            MetroStyleToggleSwitch.Name = "MetroStyleToggleSwitch";
            MetroStyleToggleSwitch.OffFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            MetroStyleToggleSwitch.OnFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            MetroStyleToggleSwitch.Size = new Size(50, 19);
            MetroStyleToggleSwitch.TabIndex = 9;
            // 
            // OSXStyleLabel
            // 
            OSXStyleLabel.AutoSize = true;
            OSXStyleLabel.Location = new Point(392, 280);
            OSXStyleLabel.Name = "OSXStyleLabel";
            OSXStyleLabel.Size = new Size(64, 13);
            OSXStyleLabel.TabIndex = 8;
            OSXStyleLabel.Text = "Style = OSX";
            // 
            // ModernStyleLabel
            // 
            ModernStyleLabel.AutoSize = true;
            ModernStyleLabel.Location = new Point(623, 161);
            ModernStyleLabel.Name = "ModernStyleLabel";
            ModernStyleLabel.Size = new Size(78, 13);
            ModernStyleLabel.TabIndex = 7;
            ModernStyleLabel.Text = "Style = Modern";
            // 
            // IphoneStyleLabel
            // 
            IphoneStyleLabel.AutoSize = true;
            IphoneStyleLabel.Location = new Point(392, 161);
            IphoneStyleLabel.Name = "IphoneStyleLabel";
            IphoneStyleLabel.Size = new Size(75, 13);
            IphoneStyleLabel.TabIndex = 6;
            IphoneStyleLabel.Text = "Style = iPhone";
            // 
            // IOS5StyleLabel
            // 
            IOS5StyleLabel.AutoSize = true;
            IOS5StyleLabel.Location = new Point(392, 52);
            IOS5StyleLabel.Name = "IOS5StyleLabel";
            IOS5StyleLabel.Size = new Size(66, 13);
            IOS5StyleLabel.TabIndex = 5;
            IOS5StyleLabel.Text = "Style = IOS5";
            // 
            // FancyStyleLabel
            // 
            FancyStyleLabel.AutoSize = true;
            FancyStyleLabel.Location = new Point(623, 280);
            FancyStyleLabel.Name = "FancyStyleLabel";
            FancyStyleLabel.Size = new Size(71, 13);
            FancyStyleLabel.TabIndex = 4;
            FancyStyleLabel.Text = "Style = Fancy";
            // 
            // CarbonStyleLabel
            // 
            CarbonStyleLabel.AutoSize = true;
            CarbonStyleLabel.Location = new Point(161, 280);
            CarbonStyleLabel.Name = "CarbonStyleLabel";
            CarbonStyleLabel.Size = new Size(76, 13);
            CarbonStyleLabel.TabIndex = 3;
            CarbonStyleLabel.Text = "Style = Carbon";
            // 
            // BrushedMetalStyleLabel
            // 
            BrushedMetalStyleLabel.AutoSize = true;
            BrushedMetalStyleLabel.Location = new Point(161, 161);
            BrushedMetalStyleLabel.Name = "BrushedMetalStyleLabel";
            BrushedMetalStyleLabel.Size = new Size(107, 13);
            BrushedMetalStyleLabel.TabIndex = 2;
            BrushedMetalStyleLabel.Text = "Style = Brushed Metal";
            // 
            // AndroidStyleLabel
            // 
            AndroidStyleLabel.AutoSize = true;
            AndroidStyleLabel.Location = new Point(623, 52);
            AndroidStyleLabel.Name = "AndroidStyleLabel";
            AndroidStyleLabel.Size = new Size(78, 13);
            AndroidStyleLabel.TabIndex = 1;
            AndroidStyleLabel.Text = "Style = Android";
            // 
            // MetroStyleLabel
            // 
            MetroStyleLabel.AutoSize = true;
            MetroStyleLabel.Location = new Point(161, 52);
            MetroStyleLabel.Name = "MetroStyleLabel";
            MetroStyleLabel.Size = new Size(69, 13);
            MetroStyleLabel.TabIndex = 0;
            MetroStyleLabel.Text = "Style = Metro";
            // 
            // SemiImportantPropertiesTabPage
            // 
            SemiImportantPropertiesTabPage.Controls.Add(label19);
            SemiImportantPropertiesTabPage.Controls.Add(_ToggleOnSideClickCheckBox);
            SemiImportantPropertiesTabPage.Controls.Add(_ToggleOnButtonClickCheckBox);
            SemiImportantPropertiesTabPage.Controls.Add(label18);
            SemiImportantPropertiesTabPage.Controls.Add(ToggleOnClickToggleSwitch);
            SemiImportantPropertiesTabPage.Controls.Add(label17);
            SemiImportantPropertiesTabPage.Controls.Add(label16);
            SemiImportantPropertiesTabPage.Controls.Add(label15);
            SemiImportantPropertiesTabPage.Controls.Add(_ThresholdPercentageTrackBar);
            SemiImportantPropertiesTabPage.Controls.Add(ThresholdPercentageToggleSwitch);
            SemiImportantPropertiesTabPage.Controls.Add(label11);
            SemiImportantPropertiesTabPage.Controls.Add(label12);
            SemiImportantPropertiesTabPage.Controls.Add(_GrayWhenDisabledCheckBox);
            SemiImportantPropertiesTabPage.Controls.Add(label13);
            SemiImportantPropertiesTabPage.Controls.Add(label14);
            SemiImportantPropertiesTabPage.Controls.Add(GrayWhenDisabledToggleSwitch2);
            SemiImportantPropertiesTabPage.Controls.Add(GrayWhenDisabledToggleSwitch1);
            SemiImportantPropertiesTabPage.Controls.Add(label10);
            SemiImportantPropertiesTabPage.Controls.Add(label9);
            SemiImportantPropertiesTabPage.Controls.Add(label8);
            SemiImportantPropertiesTabPage.Controls.Add(SlowAnimationToggleSwitch);
            SemiImportantPropertiesTabPage.Controls.Add(label7);
            SemiImportantPropertiesTabPage.Controls.Add(FastAnimationToggleSwitch);
            SemiImportantPropertiesTabPage.Controls.Add(label6);
            SemiImportantPropertiesTabPage.Controls.Add(NoAnimationToggleSwitch);
            SemiImportantPropertiesTabPage.Controls.Add(label5);
            SemiImportantPropertiesTabPage.Controls.Add(label4);
            SemiImportantPropertiesTabPage.Controls.Add(label3);
            SemiImportantPropertiesTabPage.Controls.Add(_AllowUserChangeCheckBox);
            SemiImportantPropertiesTabPage.Controls.Add(label2);
            SemiImportantPropertiesTabPage.Controls.Add(label1);
            SemiImportantPropertiesTabPage.Controls.Add(AllowUserChangeToggleSwitch2);
            SemiImportantPropertiesTabPage.Controls.Add(AllowUserChangeToggleSwitch1);
            SemiImportantPropertiesTabPage.Controls.Add(AllowUserChangeLabel);
            SemiImportantPropertiesTabPage.Location = new Point(4, 22);
            SemiImportantPropertiesTabPage.Name = "SemiImportantPropertiesTabPage";
            SemiImportantPropertiesTabPage.Padding = new Padding(3);
            SemiImportantPropertiesTabPage.Size = new Size(870, 479);
            SemiImportantPropertiesTabPage.TabIndex = 1;
            SemiImportantPropertiesTabPage.Text = "(Semi)-Important Properties";
            SemiImportantPropertiesTabPage.UseVisualStyleBackColor = true;
            // 
            // label19
            // 
            label19.AutoSize = true;
            label19.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Italic, GraphicsUnit.Point, 0);
            label19.Location = new Point(427, 444);
            label19.Name = "label19";
            label19.Size = new Size(67, 13);
            label19.TabIndex = 42;
            label19.Text = "both or none";
            // 
            // ToggleOnSideClickCheckBox
            // 
            _ToggleOnSideClickCheckBox.AutoSize = true;
            _ToggleOnSideClickCheckBox.Checked = true;
            _ToggleOnSideClickCheckBox.CheckState = CheckState.Checked;
            _ToggleOnSideClickCheckBox.Location = new Point(205, 444);
            _ToggleOnSideClickCheckBox.Name = "_ToggleOnSideClickCheckBox";
            _ToggleOnSideClickCheckBox.Size = new Size(117, 17);
            _ToggleOnSideClickCheckBox.TabIndex = 41;
            _ToggleOnSideClickCheckBox.Text = "Toggle On Side Click";
            _ToggleOnSideClickCheckBox.UseVisualStyleBackColor = true;
            // Me.ToggleOnSideClickCheckBox.CheckedChanged += New System.EventHandler(Me.ToggleOnSideClickCheckBox_CheckedChanged)
            // 
            // ToggleOnButtonClickCheckBox
            // 
            _ToggleOnButtonClickCheckBox.AutoSize = true;
            _ToggleOnButtonClickCheckBox.Checked = true;
            _ToggleOnButtonClickCheckBox.CheckState = CheckState.Checked;
            _ToggleOnButtonClickCheckBox.Location = new Point(205, 421);
            _ToggleOnButtonClickCheckBox.Name = "_ToggleOnButtonClickCheckBox";
            _ToggleOnButtonClickCheckBox.Size = new Size(127, 17);
            _ToggleOnButtonClickCheckBox.TabIndex = 40;
            _ToggleOnButtonClickCheckBox.Text = "Toggle On Button Click";
            _ToggleOnButtonClickCheckBox.UseVisualStyleBackColor = true;
            // Me.ToggleOnButtonClickCheckBox.CheckedChanged += New System.EventHandler(Me.ToggleOnButtonClickCheckBox_CheckedChanged)
            // 
            // label18
            // 
            label18.AutoSize = true;
            label18.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Italic, GraphicsUnit.Point, 0);
            label18.Location = new Point(427, 419);
            label18.Name = "label18";
            label18.Size = new Size(436, 13);
            label18.TabIndex = 39;
            label18.Text = "Determines where you have to click to toggle the switch, on the button, besides t" + "he button,";
            // 
            // ToggleOnClickToggleSwitch
            // 
            ToggleOnClickToggleSwitch.Location = new Point(11, 419);
            ToggleOnClickToggleSwitch.Name = "ToggleOnClickToggleSwitch";
            ToggleOnClickToggleSwitch.OffFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            ToggleOnClickToggleSwitch.OnFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            ToggleOnClickToggleSwitch.Size = new Size(50, 19);
            ToggleOnClickToggleSwitch.TabIndex = 38;
            // 
            // label17
            // 
            label17.AutoSize = true;
            label17.Font = new Font("Microsoft Sans Serif", 10.0f, FontStyle.Underline, GraphicsUnit.Point, 0);
            label17.ForeColor = Color.Gray;
            label17.Location = new Point(8, 384);
            label17.Name = "label17";
            label17.Size = new Size(399, 17);
            label17.TabIndex = 37;
            label17.Text = "Toggle On Button Click (boolean), Toggle On Side Click (boolean)";
            // 
            // label16
            // 
            label16.AutoSize = true;
            label16.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Italic, GraphicsUnit.Point, 0);
            label16.Location = new Point(427, 336);
            label16.Name = "label16";
            label16.Size = new Size(410, 13);
            label16.TabIndex = 36;
            label16.Text = "Determines how far you have to drag the slider button before it snaps to the othe" + "r side";
            // 
            // label15
            // 
            label15.AutoSize = true;
            label15.Location = new Point(266, 336);
            label15.Name = "label15";
            label15.Size = new Size(125, 13);
            label15.TabIndex = 35;
            label15.Text = "Value = 50 (Default = 50)";
            // 
            // ThresholdPercentageTrackBar
            // 
            _ThresholdPercentageTrackBar.BackColor = Color.White;
            _ThresholdPercentageTrackBar.Location = new Point(145, 336);
            _ThresholdPercentageTrackBar.Maximum = 100;
            _ThresholdPercentageTrackBar.Name = "_ThresholdPercentageTrackBar";
            _ThresholdPercentageTrackBar.Size = new Size(104, 45);
            _ThresholdPercentageTrackBar.TabIndex = 34;
            _ThresholdPercentageTrackBar.TickFrequency = 10;
            _ThresholdPercentageTrackBar.Value = 50;
            // Me.ThresholdPercentageTrackBar.Scroll += New System.EventHandler(Me.ThresholdPercentageTrackBar_Scroll)
            // 
            // ThresholdPercentageToggleSwitch
            // 
            ThresholdPercentageToggleSwitch.Location = new Point(11, 336);
            ThresholdPercentageToggleSwitch.Name = "ThresholdPercentageToggleSwitch";
            ThresholdPercentageToggleSwitch.OffFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            ThresholdPercentageToggleSwitch.OnFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            ThresholdPercentageToggleSwitch.Size = new Size(50, 19);
            ThresholdPercentageToggleSwitch.TabIndex = 33;
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Font = new Font("Microsoft Sans Serif", 10.0f, FontStyle.Underline, GraphicsUnit.Point, 0);
            label11.ForeColor = Color.Gray;
            label11.Location = new Point(8, 305);
            label11.Name = "label11";
            label11.Size = new Size(203, 17);
            label11.TabIndex = 32;
            label11.Text = "Threshold Percentage (integer)";
            // 
            // label12
            // 
            label12.AutoSize = true;
            label12.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Italic, GraphicsUnit.Point, 0);
            label12.Location = new Point(427, 235);
            label12.Name = "label12";
            label12.Size = new Size(252, 13);
            label12.TabIndex = 31;
            label12.Text = "Affects the coloring of the control when it is disabled";
            // 
            // GrayWhenDisabledCheckBox
            // 
            _GrayWhenDisabledCheckBox.AutoSize = true;
            _GrayWhenDisabledCheckBox.Checked = true;
            _GrayWhenDisabledCheckBox.CheckState = CheckState.Checked;
            _GrayWhenDisabledCheckBox.Location = new Point(319, 234);
            _GrayWhenDisabledCheckBox.Name = "_GrayWhenDisabledCheckBox";
            _GrayWhenDisabledCheckBox.Size = new Size(65, 17);
            _GrayWhenDisabledCheckBox.TabIndex = 30;
            _GrayWhenDisabledCheckBox.Text = "Enabled";
            _GrayWhenDisabledCheckBox.UseVisualStyleBackColor = true;
            // Me.GrayWhenDisabledCheckBox.CheckedChanged += New System.EventHandler(Me.GrayWhenDisabledCheckBox_CheckedChanged)
            // 
            // label13
            // 
            label13.AutoSize = true;
            label13.Location = new Point(116, 268);
            label13.Name = "label13";
            label13.Size = new Size(172, 13);
            label13.TabIndex = 29;
            label13.Text = "Gray When Disabled = true (Default)";
            // 
            // label14
            // 
            label14.AutoSize = true;
            label14.Location = new Point(116, 234);
            label14.Name = "label14";
            label14.Size = new Size(133, 13);
            label14.TabIndex = 28;
            label14.Text = "Gray When Disabled = false";
            // 
            // GrayWhenDisabledToggleSwitch2
            // 
            GrayWhenDisabledToggleSwitch2.Location = new Point(11, 262);
            GrayWhenDisabledToggleSwitch2.Name = "GrayWhenDisabledToggleSwitch2";
            GrayWhenDisabledToggleSwitch2.OffFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            GrayWhenDisabledToggleSwitch2.OnFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            GrayWhenDisabledToggleSwitch2.Size = new Size(50, 19);
            GrayWhenDisabledToggleSwitch2.TabIndex = 27;
            // 
            // GrayWhenDisabledToggleSwitch1
            // 
            GrayWhenDisabledToggleSwitch1.Location = new Point(11, 228);
            GrayWhenDisabledToggleSwitch1.Name = "GrayWhenDisabledToggleSwitch1";
            GrayWhenDisabledToggleSwitch1.OffFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            GrayWhenDisabledToggleSwitch1.OnFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            GrayWhenDisabledToggleSwitch1.Size = new Size(50, 19);
            GrayWhenDisabledToggleSwitch1.TabIndex = 26;
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.Font = new Font("Microsoft Sans Serif", 10.0f, FontStyle.Underline, GraphicsUnit.Point, 0);
            label10.ForeColor = Color.Gray;
            label10.Location = new Point(8, 194);
            label10.Name = "label10";
            label10.Size = new Size(196, 17);
            label10.TabIndex = 25;
            label10.Text = "Gray When Disabled (boolean)";
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Italic, GraphicsUnit.Point, 0);
            label9.Location = new Point(8, 168);
            label9.Name = "label9";
            label9.Size = new Size(681, 13);
            label9.TabIndex = 24;
            label9.Text = "Different values for the two integer properties will affect the animation speed. " + "Animation is turned off completely using the Use Animation property";
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Location = new Point(621, 136);
            label8.Name = "label8";
            label8.Size = new Size(78, 13);
            label8.TabIndex = 23;
            label8.Text = "Slow animation";
            // 
            // SlowAnimationToggleSwitch
            // 
            SlowAnimationToggleSwitch.Location = new Point(510, 130);
            SlowAnimationToggleSwitch.Name = "SlowAnimationToggleSwitch";
            SlowAnimationToggleSwitch.OffFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            SlowAnimationToggleSwitch.OnFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            SlowAnimationToggleSwitch.Size = new Size(50, 19);
            SlowAnimationToggleSwitch.TabIndex = 22;
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Location = new Point(360, 136);
            label7.Name = "label7";
            label7.Size = new Size(75, 13);
            label7.TabIndex = 21;
            label7.Text = "Fast animation";
            // 
            // FastAnimationToggleSwitch
            // 
            FastAnimationToggleSwitch.Location = new Point(252, 130);
            FastAnimationToggleSwitch.Name = "FastAnimationToggleSwitch";
            FastAnimationToggleSwitch.OffFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            FastAnimationToggleSwitch.OnFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            FastAnimationToggleSwitch.Size = new Size(50, 19);
            FastAnimationToggleSwitch.TabIndex = 20;
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new Point(123, 136);
            label6.Name = "label6";
            label6.Size = new Size(69, 13);
            label6.TabIndex = 19;
            label6.Text = "No animation";
            // 
            // NoAnimationToggleSwitch
            // 
            NoAnimationToggleSwitch.Location = new Point(11, 130);
            NoAnimationToggleSwitch.Name = "NoAnimationToggleSwitch";
            NoAnimationToggleSwitch.OffFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            NoAnimationToggleSwitch.OnFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            NoAnimationToggleSwitch.Size = new Size(50, 19);
            NoAnimationToggleSwitch.TabIndex = 18;
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new Font("Microsoft Sans Serif", 10.0f, FontStyle.Underline, GraphicsUnit.Point, 0);
            label5.ForeColor = Color.Gray;
            label5.Location = new Point(8, 96);
            label5.Name = "label5";
            label5.Size = new Size(491, 17);
            label5.TabIndex = 17;
            label5.Text = "Animation Interval (integer), Animation Step (integer), Use Animation (boolean)";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Italic, GraphicsUnit.Point, 0);
            label4.Location = new Point(427, 61);
            label4.Name = "label4";
            label4.Size = new Size(158, 13);
            label4.TabIndex = 16;
            label4.Text = "you cannot change it in the GUI";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Italic, GraphicsUnit.Point, 0);
            label3.Location = new Point(427, 37);
            label3.Name = "label3";
            label3.Size = new Size(397, 13);
            label3.TabIndex = 15;
            label3.Text = "Notice: You can set the Checked value from code, but if Allow User Change = false,";
            // 
            // AllowUserChangeCheckBox
            // 
            _AllowUserChangeCheckBox.AutoSize = true;
            _AllowUserChangeCheckBox.Location = new Point(288, 37);
            _AllowUserChangeCheckBox.Name = "_AllowUserChangeCheckBox";
            _AllowUserChangeCheckBox.Size = new Size(69, 17);
            _AllowUserChangeCheckBox.TabIndex = 14;
            _AllowUserChangeCheckBox.Text = "Checked";
            _AllowUserChangeCheckBox.UseVisualStyleBackColor = true;
            // Me.AllowUserChangeCheckBox.CheckedChanged += New System.EventHandler(Me.AllowUserChangeCheckBox_CheckedChanged)
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(85, 62);
            label2.Name = "label2";
            label2.Size = new Size(164, 13);
            label2.TabIndex = 13;
            label2.Text = "Allow User Change = true (Default)";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(85, 37);
            label1.Name = "label1";
            label1.Size = new Size(125, 13);
            label1.TabIndex = 12;
            label1.Text = "Allow User Change = false";
            // 
            // AllowUserChangeToggleSwitch2
            // 
            AllowUserChangeToggleSwitch2.Location = new Point(11, 60);
            AllowUserChangeToggleSwitch2.Name = "AllowUserChangeToggleSwitch2";
            AllowUserChangeToggleSwitch2.OffFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            AllowUserChangeToggleSwitch2.OnFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            AllowUserChangeToggleSwitch2.Size = new Size(50, 19);
            AllowUserChangeToggleSwitch2.TabIndex = 11;
            // 
            // AllowUserChangeToggleSwitch1
            // 
            AllowUserChangeToggleSwitch1.Location = new Point(11, 35);
            AllowUserChangeToggleSwitch1.Name = "AllowUserChangeToggleSwitch1";
            AllowUserChangeToggleSwitch1.OffFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            AllowUserChangeToggleSwitch1.OnFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            AllowUserChangeToggleSwitch1.Size = new Size(50, 19);
            AllowUserChangeToggleSwitch1.TabIndex = 10;
            // 
            // AllowUserChangeLabel
            // 
            AllowUserChangeLabel.AutoSize = true;
            AllowUserChangeLabel.Font = new Font("Microsoft Sans Serif", 10.0f, FontStyle.Underline, GraphicsUnit.Point, 0);
            AllowUserChangeLabel.ForeColor = Color.Gray;
            AllowUserChangeLabel.Location = new Point(8, 3);
            AllowUserChangeLabel.Name = "AllowUserChangeLabel";
            AllowUserChangeLabel.Size = new Size(184, 17);
            AllowUserChangeLabel.TabIndex = 0;
            AllowUserChangeLabel.Text = "Allow User Change (boolean)";
            // 
            // SpecialCustomizationsTabPage
            // 
            SpecialCustomizationsTabPage.Controls.Add(AnimatedGifPictureBox);
            SpecialCustomizationsTabPage.Controls.Add(AdvancedBehaviorFancyToggleSwitch);
            SpecialCustomizationsTabPage.Controls.Add(label28);
            SpecialCustomizationsTabPage.Controls.Add(label26);
            SpecialCustomizationsTabPage.Controls.Add(label27);
            SpecialCustomizationsTabPage.Controls.Add(CustomizedFancyToggleSwitch);
            SpecialCustomizationsTabPage.Controls.Add(NormalFancyToggleSwitch);
            SpecialCustomizationsTabPage.Controls.Add(label25);
            SpecialCustomizationsTabPage.Controls.Add(label23);
            SpecialCustomizationsTabPage.Controls.Add(label24);
            SpecialCustomizationsTabPage.Controls.Add(CustomizedIOS5ToggleSwitch);
            SpecialCustomizationsTabPage.Controls.Add(NormalIOS5ToggleSwitch);
            SpecialCustomizationsTabPage.Controls.Add(label22);
            SpecialCustomizationsTabPage.Controls.Add(label21);
            SpecialCustomizationsTabPage.Controls.Add(CustomizedMetroToggleSwitch);
            SpecialCustomizationsTabPage.Controls.Add(NormalMetroToggleSwitch);
            SpecialCustomizationsTabPage.Controls.Add(label20);
            SpecialCustomizationsTabPage.Location = new Point(4, 22);
            SpecialCustomizationsTabPage.Name = "SpecialCustomizationsTabPage";
            SpecialCustomizationsTabPage.Size = new Size(870, 479);
            SpecialCustomizationsTabPage.TabIndex = 2;
            SpecialCustomizationsTabPage.Text = "Special Customizations";
            SpecialCustomizationsTabPage.UseVisualStyleBackColor = true;
            // 
            // AnimatedGifPictureBox
            // 
            AnimatedGifPictureBox.Image = (Image)resources.GetObject("AnimatedGifPictureBox.Image");
            AnimatedGifPictureBox.Location = new Point(171, 355);
            AnimatedGifPictureBox.Name = "AnimatedGifPictureBox";
            AnimatedGifPictureBox.Size = new Size(36, 31);
            AnimatedGifPictureBox.SizeMode = PictureBoxSizeMode.Zoom;
            AnimatedGifPictureBox.TabIndex = 2;
            AnimatedGifPictureBox.TabStop = false;
            // 
            // AdvancedBehaviorFancyToggleSwitch
            // 
            AdvancedBehaviorFancyToggleSwitch.Location = new Point(22, 280);
            AdvancedBehaviorFancyToggleSwitch.Name = "AdvancedBehaviorFancyToggleSwitch";
            AdvancedBehaviorFancyToggleSwitch.OffFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            AdvancedBehaviorFancyToggleSwitch.OnFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            AdvancedBehaviorFancyToggleSwitch.Size = new Size(50, 19);
            AdvancedBehaviorFancyToggleSwitch.TabIndex = 32;
            // 
            // label28
            // 
            label28.AutoSize = true;
            label28.Font = new Font("Microsoft Sans Serif", 10.0f, FontStyle.Underline, GraphicsUnit.Point, 0);
            label28.ForeColor = Color.Gray;
            label28.Location = new Point(19, 237);
            label28.Name = "label28";
            label28.Size = new Size(188, 17);
            label28.TabIndex = 31;
            label28.Text = "Advanced Behavior Example";
            // 
            // label26
            // 
            label26.AutoSize = true;
            label26.Location = new Point(349, 191);
            label26.Name = "label26";
            label26.Size = new Size(61, 13);
            label26.TabIndex = 30;
            label26.Text = "Customized";
            // 
            // label27
            // 
            label27.AutoSize = true;
            label27.Location = new Point(126, 191);
            label27.Name = "label27";
            label27.Size = new Size(40, 13);
            label27.TabIndex = 29;
            label27.Text = "Normal";
            // 
            // CustomizedFancyToggleSwitch
            // 
            CustomizedFancyToggleSwitch.Location = new Point(238, 187);
            CustomizedFancyToggleSwitch.Name = "CustomizedFancyToggleSwitch";
            CustomizedFancyToggleSwitch.OffFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            CustomizedFancyToggleSwitch.OnFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            CustomizedFancyToggleSwitch.Size = new Size(50, 19);
            CustomizedFancyToggleSwitch.TabIndex = 28;
            // 
            // NormalFancyToggleSwitch
            // 
            NormalFancyToggleSwitch.Location = new Point(22, 187);
            NormalFancyToggleSwitch.Name = "NormalFancyToggleSwitch";
            NormalFancyToggleSwitch.OffFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            NormalFancyToggleSwitch.OnFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            NormalFancyToggleSwitch.Size = new Size(50, 19);
            NormalFancyToggleSwitch.TabIndex = 27;
            // 
            // label25
            // 
            label25.AutoSize = true;
            label25.Font = new Font("Microsoft Sans Serif", 10.0f, FontStyle.Underline, GraphicsUnit.Point, 0);
            label25.ForeColor = Color.Gray;
            label25.Location = new Point(19, 148);
            label25.Name = "label25";
            label25.Size = new Size(255, 17);
            label25.TabIndex = 26;
            label25.Text = "Images in the button and the side fields";
            // 
            // label23
            // 
            label23.AutoSize = true;
            label23.Location = new Point(349, 110);
            label23.Name = "label23";
            label23.Size = new Size(61, 13);
            label23.TabIndex = 25;
            label23.Text = "Customized";
            // 
            // label24
            // 
            label24.AutoSize = true;
            label24.Location = new Point(126, 110);
            label24.Name = "label24";
            label24.Size = new Size(40, 13);
            label24.TabIndex = 24;
            label24.Text = "Normal";
            // 
            // CustomizedIOS5ToggleSwitch
            // 
            CustomizedIOS5ToggleSwitch.Location = new Point(238, 93);
            CustomizedIOS5ToggleSwitch.Name = "CustomizedIOS5ToggleSwitch";
            CustomizedIOS5ToggleSwitch.OffFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            CustomizedIOS5ToggleSwitch.OnFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            CustomizedIOS5ToggleSwitch.Size = new Size(50, 19);
            CustomizedIOS5ToggleSwitch.TabIndex = 23;
            // 
            // NormalIOS5ToggleSwitch
            // 
            NormalIOS5ToggleSwitch.Location = new Point(22, 93);
            NormalIOS5ToggleSwitch.Name = "NormalIOS5ToggleSwitch";
            NormalIOS5ToggleSwitch.OffFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            NormalIOS5ToggleSwitch.OnFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            NormalIOS5ToggleSwitch.Size = new Size(50, 19);
            NormalIOS5ToggleSwitch.TabIndex = 22;
            // 
            // label22
            // 
            label22.AutoSize = true;
            label22.Location = new Point(349, 52);
            label22.Name = "label22";
            label22.Size = new Size(61, 13);
            label22.TabIndex = 21;
            label22.Text = "Customized";
            // 
            // label21
            // 
            label21.AutoSize = true;
            label21.Location = new Point(126, 52);
            label21.Name = "label21";
            label21.Size = new Size(40, 13);
            label21.TabIndex = 20;
            label21.Text = "Normal";
            // 
            // CustomizedMetroToggleSwitch
            // 
            CustomizedMetroToggleSwitch.Location = new Point(238, 48);
            CustomizedMetroToggleSwitch.Name = "CustomizedMetroToggleSwitch";
            CustomizedMetroToggleSwitch.OffFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            CustomizedMetroToggleSwitch.OnFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            CustomizedMetroToggleSwitch.Size = new Size(50, 19);
            CustomizedMetroToggleSwitch.TabIndex = 11;
            // 
            // NormalMetroToggleSwitch
            // 
            NormalMetroToggleSwitch.Location = new Point(22, 48);
            NormalMetroToggleSwitch.Name = "NormalMetroToggleSwitch";
            NormalMetroToggleSwitch.OffFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            NormalMetroToggleSwitch.OnFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            NormalMetroToggleSwitch.Size = new Size(50, 19);
            NormalMetroToggleSwitch.TabIndex = 10;
            // 
            // label20
            // 
            label20.AutoSize = true;
            label20.Font = new Font("Microsoft Sans Serif", 10.0f, FontStyle.Underline, GraphicsUnit.Point, 0);
            label20.ForeColor = Color.Gray;
            label20.Location = new Point(19, 14);
            label20.Name = "label20";
            label20.Size = new Size(133, 17);
            label20.TabIndex = 1;
            label20.Text = "Color Customization";
            // 
            // PlaygroundTabPage
            // 
            PlaygroundTabPage.Controls.Add(PlaygroundToggleSwitch);
            PlaygroundTabPage.Controls.Add(PlaygroundPropertyGrid);
            PlaygroundTabPage.Location = new Point(4, 22);
            PlaygroundTabPage.Name = "PlaygroundTabPage";
            PlaygroundTabPage.Size = new Size(870, 479);
            PlaygroundTabPage.TabIndex = 3;
            PlaygroundTabPage.Text = "Playground";
            PlaygroundTabPage.UseVisualStyleBackColor = true;
            // 
            // PlaygroundToggleSwitch
            // 
            PlaygroundToggleSwitch.Location = new Point(51, 42);
            PlaygroundToggleSwitch.Name = "PlaygroundToggleSwitch";
            PlaygroundToggleSwitch.OffFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            PlaygroundToggleSwitch.OnFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            PlaygroundToggleSwitch.Size = new Size(92, 33);
            PlaygroundToggleSwitch.TabIndex = 11;
            // 
            // PlaygroundPropertyGrid
            // 
            PlaygroundPropertyGrid.Dock = DockStyle.Right;
            PlaygroundPropertyGrid.Location = new Point(459, 0);
            PlaygroundPropertyGrid.Name = "PlaygroundPropertyGrid";
            PlaygroundPropertyGrid.SelectedObject = PlaygroundToggleSwitch;
            PlaygroundPropertyGrid.Size = new Size(411, 479);
            PlaygroundPropertyGrid.TabIndex = 0;
            // 
            // SimulateRestartBackgroundWorker
            // 
            // Me.SimulateRestartBackgroundWorker.DoWork += New System.ComponentModel.DoWorkEventHandler(Me.SimulateRestartBackgroundWorker_DoWork)
            // Me.SimulateRestartBackgroundWorker.RunWorkerCompleted += New System.ComponentModel.RunWorkerCompletedEventHandler(Me.SimulateRestartBackgroundWorker_RunWorkerCompleted)
            // 
            // DemoForm
            // 
            AutoScaleDimensions = new SizeF(6.0f, 13.0f);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(878, 605);
            Controls.Add(DemoTabControl);
            Controls.Add(TopPanel);
            FormBorderStyle = FormBorderStyle.FixedDialog;
            Icon = (Icon)resources.GetObject("$this.Icon");
            MaximizeBox = false;
            Name = "DemoForm";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "JCS Toggle Switch Demo Form";
            TopPanel.ResumeLayout(false);
            DemoTabControl.ResumeLayout(false);
            StylesTabPage.ResumeLayout(false);
            StylesTabPage.PerformLayout();
            SemiImportantPropertiesTabPage.ResumeLayout(false);
            SemiImportantPropertiesTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_ThresholdPercentageTrackBar).EndInit();
            SpecialCustomizationsTabPage.ResumeLayout(false);
            SpecialCustomizationsTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)AnimatedGifPictureBox).EndInit();
            PlaygroundTabPage.ResumeLayout(false);
            ResumeLayout(false);
        }

        private Panel TopPanel;
        private Label InfoLabel;
        private TabControl DemoTabControl;
        private TabPage StylesTabPage;
        private TabPage SemiImportantPropertiesTabPage;
        private Label OSXStyleLabel;
        private Label ModernStyleLabel;
        private Label IphoneStyleLabel;
        private Label IOS5StyleLabel;
        private Label FancyStyleLabel;
        private Label CarbonStyleLabel;
        private Label BrushedMetalStyleLabel;
        private Label AndroidStyleLabel;
        private Label MetroStyleLabel;
        private Controls.ToggleSwitch MetroStyleToggleSwitch;
        private Controls.ToggleSwitch FancyStyleToggleSwitch;
        private Controls.ToggleSwitch OSXStyleToggleSwitch;
        private Controls.ToggleSwitch CarbonStyleToggleSwitch;
        private Controls.ToggleSwitch ModernStyleToggleSwitch;
        private Controls.ToggleSwitch IphoneStyleToggleSwitch;
        private Controls.ToggleSwitch BrushedMetalStyleToggleSwitch;
        private Controls.ToggleSwitch AndroidStyleToggleSwitch;
        private Controls.ToggleSwitch IOS5StyleToggleSwitch;
        private Label OSXStyleOffLabel;
        private Label OSXStyleOnLabel;
        private Label AllowUserChangeLabel;
        private Label label2;
        private Label label1;
        private Controls.ToggleSwitch AllowUserChangeToggleSwitch2;
        private Controls.ToggleSwitch AllowUserChangeToggleSwitch1;
        private CheckBox _AllowUserChangeCheckBox;

        private CheckBox AllowUserChangeCheckBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _AllowUserChangeCheckBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_AllowUserChangeCheckBox != null)
                {
                    _AllowUserChangeCheckBox.CheckedChanged -= AllowUserChangeCheckBox_CheckedChanged;
                }

                _AllowUserChangeCheckBox = value;
                if (_AllowUserChangeCheckBox != null)
                {
                    _AllowUserChangeCheckBox.CheckedChanged += AllowUserChangeCheckBox_CheckedChanged;
                }
            }
        }

        private Label label4;
        private Label label3;
        private Label label5;
        private Controls.ToggleSwitch NoAnimationToggleSwitch;
        private Label label6;
        private Label label9;
        private Label label8;
        private Controls.ToggleSwitch SlowAnimationToggleSwitch;
        private Label label7;
        private Controls.ToggleSwitch FastAnimationToggleSwitch;
        private Label label10;
        private Label label12;
        private CheckBox _GrayWhenDisabledCheckBox;

        private CheckBox GrayWhenDisabledCheckBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _GrayWhenDisabledCheckBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_GrayWhenDisabledCheckBox != null)
                {
                    _GrayWhenDisabledCheckBox.CheckedChanged -= GrayWhenDisabledCheckBox_CheckedChanged;
                }

                _GrayWhenDisabledCheckBox = value;
                if (_GrayWhenDisabledCheckBox != null)
                {
                    _GrayWhenDisabledCheckBox.CheckedChanged += GrayWhenDisabledCheckBox_CheckedChanged;
                }
            }
        }

        private Label label13;
        private Label label14;
        private Controls.ToggleSwitch GrayWhenDisabledToggleSwitch2;
        private Controls.ToggleSwitch GrayWhenDisabledToggleSwitch1;
        private Label label11;
        private Controls.ToggleSwitch ThresholdPercentageToggleSwitch;
        private TrackBar _ThresholdPercentageTrackBar;

        private TrackBar ThresholdPercentageTrackBar
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _ThresholdPercentageTrackBar;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_ThresholdPercentageTrackBar != null)
                {
                    _ThresholdPercentageTrackBar.Scroll -= ThresholdPercentageTrackBar_Scroll;
                }

                _ThresholdPercentageTrackBar = value;
                if (_ThresholdPercentageTrackBar != null)
                {
                    _ThresholdPercentageTrackBar.Scroll += ThresholdPercentageTrackBar_Scroll;
                }
            }
        }

        private Label label15;
        private Label label16;
        private Label label17;
        private Controls.ToggleSwitch ToggleOnClickToggleSwitch;
        private Label label18;
        private CheckBox _ToggleOnButtonClickCheckBox;

        private CheckBox ToggleOnButtonClickCheckBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _ToggleOnButtonClickCheckBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_ToggleOnButtonClickCheckBox != null)
                {
                    _ToggleOnButtonClickCheckBox.CheckedChanged -= ToggleOnButtonClickCheckBox_CheckedChanged;
                }

                _ToggleOnButtonClickCheckBox = value;
                if (_ToggleOnButtonClickCheckBox != null)
                {
                    _ToggleOnButtonClickCheckBox.CheckedChanged += ToggleOnButtonClickCheckBox_CheckedChanged;
                }
            }
        }

        private CheckBox _ToggleOnSideClickCheckBox;

        private CheckBox ToggleOnSideClickCheckBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _ToggleOnSideClickCheckBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_ToggleOnSideClickCheckBox != null)
                {
                    _ToggleOnSideClickCheckBox.CheckedChanged -= ToggleOnSideClickCheckBox_CheckedChanged;
                }

                _ToggleOnSideClickCheckBox = value;
                if (_ToggleOnSideClickCheckBox != null)
                {
                    _ToggleOnSideClickCheckBox.CheckedChanged += ToggleOnSideClickCheckBox_CheckedChanged;
                }
            }
        }

        private Label label19;
        private TabPage SpecialCustomizationsTabPage;
        private TabPage PlaygroundTabPage;
        private Controls.ToggleSwitch PlaygroundToggleSwitch;
        private PropertyGrid PlaygroundPropertyGrid;
        private Label label22;
        private Label label21;
        private Controls.ToggleSwitch CustomizedMetroToggleSwitch;
        private Controls.ToggleSwitch NormalMetroToggleSwitch;
        private Label label20;
        private Label label23;
        private Label label24;
        private Controls.ToggleSwitch CustomizedIOS5ToggleSwitch;
        private Controls.ToggleSwitch NormalIOS5ToggleSwitch;
        private Label label26;
        private Label label27;
        private Controls.ToggleSwitch CustomizedFancyToggleSwitch;
        private Controls.ToggleSwitch NormalFancyToggleSwitch;
        private Label label25;
        private Controls.ToggleSwitch AdvancedBehaviorFancyToggleSwitch;
        private Label label28;
        private System.ComponentModel.BackgroundWorker _SimulateRestartBackgroundWorker;

        private System.ComponentModel.BackgroundWorker SimulateRestartBackgroundWorker
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _SimulateRestartBackgroundWorker;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_SimulateRestartBackgroundWorker != null)
                {
                    _SimulateRestartBackgroundWorker.DoWork -= SimulateRestartBackgroundWorker_DoWork;
                    _SimulateRestartBackgroundWorker.RunWorkerCompleted -= SimulateRestartBackgroundWorker_RunWorkerCompleted;
                }

                _SimulateRestartBackgroundWorker = value;
                if (_SimulateRestartBackgroundWorker != null)
                {
                    _SimulateRestartBackgroundWorker.DoWork += SimulateRestartBackgroundWorker_DoWork;
                    _SimulateRestartBackgroundWorker.RunWorkerCompleted += SimulateRestartBackgroundWorker_RunWorkerCompleted;
                }
            }
        }

        private PictureBox AnimatedGifPictureBox;
    }
}
