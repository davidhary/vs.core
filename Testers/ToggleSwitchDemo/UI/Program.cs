﻿/// <summary>
/// Constructor that prevents a default instance of this class from being created.
/// </summary>
/// <remarks> David, 2021-03-12. </remarks>
using System;
using System.Windows.Forms;

namespace isr.Core.Tester
{
    /// <summary>   A program. This class cannot be inherited. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    internal sealed class Program
    {

        /// <summary>   Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        private Program()
        {
        }

        /// <summary>   The main entry point for the application. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        [STAThread]
        public static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new DemoForm());
        }
    }
}