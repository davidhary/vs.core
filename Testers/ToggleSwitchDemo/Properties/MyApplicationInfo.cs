﻿/// namespace: My
/// 
/// summary:   ."

namespace isr.Core.Tester.My
{
    internal partial class MyApplication
    {
        /// <summary>   The assembly title. </summary>

        public const string AssemblyTitle = "Toggle Switch Demo";
        /// <summary>   Information describing the assembly. </summary>
        public const string AssemblyDescription = "Toggle Switch Demo";
        /// <summary>   The assembly product. </summary>
        public const string AssemblyProduct = "Toggle.Switch.Demo.2019";
    }
}