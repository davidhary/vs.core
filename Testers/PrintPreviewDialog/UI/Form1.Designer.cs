using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Tester
{
    [DesignerGenerated()]
    public partial class Form1 : Form
    {

        /// <summary>   Form overrides dispose to clean up the component list. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="disposing">    <see langword="true" /> to release both managed and unmanaged
        ///                             resources; <see langword="false" /> to release only unmanaged
        ///                             resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (components is object)
                    {
                        components.Dispose();
                    }

                    if (_Font is object)
                        _Font.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }


        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "PrintPreviewDialog")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "CoolPrintPreviewDialog")]
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new Container();
            _LongDocumentCheckBox = new CheckBox();
            _CoolCodeLabel = new Label();
            _StandardCodeLabel = new Label();
            __OpenCoolPreviewDialogButton = new Button();
            __OpenCoolPreviewDialogButton.Click += new EventHandler(OpenCoolPreviewDialogButtonClick);
            __OpenStandardPreviewDialogButton = new Button();
            __OpenStandardPreviewDialogButton.Click += new EventHandler(OpenStandardPreviewDialogButtonClick);
            __PrintDocument = new System.Drawing.Printing.PrintDocument();
            __PrintDocument.BeginPrint += new System.Drawing.Printing.PrintEventHandler(PrintDocumentBeginPrint);
            __PrintDocument.EndPrint += new System.Drawing.Printing.PrintEventHandler(PrintDocumentEndPrint);
            __PrintDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(PrintDocumentPrintPage);
            SuspendLayout();
            // 
            // _LongDocumentCheckBox
            // 
            _LongDocumentCheckBox.AutoSize = true;
            _LongDocumentCheckBox.Location = new Point(232, 238);
            _LongDocumentCheckBox.Name = "_LongDocumentCheckBox";
            _LongDocumentCheckBox.Size = new Size(162, 21);
            _LongDocumentCheckBox.TabIndex = 15;
            _LongDocumentCheckBox.Text = "Create Long Document";
            _LongDocumentCheckBox.UseVisualStyleBackColor = true;
            // 
            // _CoolCodeLabel
            // 
            _CoolCodeLabel.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            _CoolCodeLabel.Font = new Font("Courier New", 7.8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _CoolCodeLabel.Location = new Point(229, 124);
            _CoolCodeLabel.Name = "_CoolCodeLabel";
            _CoolCodeLabel.Size = new Size(388, 102);
            _CoolCodeLabel.TabIndex = 13;
            // 
            // _StandardCodeLabel
            // 
            _StandardCodeLabel.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            _StandardCodeLabel.Font = new Font("Courier New", 7.8f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _StandardCodeLabel.Location = new Point(229, 13);
            _StandardCodeLabel.Name = "_StandardCodeLabel";
            _StandardCodeLabel.Size = new Size(388, 102);
            _StandardCodeLabel.TabIndex = 14;
            // 
            // _OpenCoolPreviewDialogButton
            // 
            __OpenCoolPreviewDialogButton.Location = new Point(13, 124);
            __OpenCoolPreviewDialogButton.Margin = new Padding(4);
            __OpenCoolPreviewDialogButton.Name = "__OpenCoolPreviewDialogButton";
            __OpenCoolPreviewDialogButton.Size = new Size(209, 102);
            __OpenCoolPreviewDialogButton.TabIndex = 12;
            __OpenCoolPreviewDialogButton.Text = "Enhanced" + '\r' + '\n' + "CoolPrintPreviewDialog";
            __OpenCoolPreviewDialogButton.UseVisualStyleBackColor = true;
            // 
            // _OpenStandardPreviewDialogButton
            // 
            __OpenStandardPreviewDialogButton.Location = new Point(13, 13);
            __OpenStandardPreviewDialogButton.Margin = new Padding(4);
            __OpenStandardPreviewDialogButton.Name = "__OpenStandardPreviewDialogButton";
            __OpenStandardPreviewDialogButton.Size = new Size(209, 102);
            __OpenStandardPreviewDialogButton.TabIndex = 11;
            __OpenStandardPreviewDialogButton.Text = "Standard" + '\r' + '\n' + "PrintPreviewDialog";
            __OpenStandardPreviewDialogButton.UseVisualStyleBackColor = true;
            // 
            // PrintDocument1
            // 
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(120.0f, 120.0f);
            AutoScaleMode = AutoScaleMode.Dpi;
            ClientSize = new Size(650, 275);
            Controls.Add(_LongDocumentCheckBox);
            Controls.Add(_CoolCodeLabel);
            Controls.Add(_StandardCodeLabel);
            Controls.Add(__OpenCoolPreviewDialogButton);
            Controls.Add(__OpenStandardPreviewDialogButton);
            Name = "Form1";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Compare Print Preview Dialogs";
            ResumeLayout(false);
            PerformLayout();
        }

        private CheckBox _LongDocumentCheckBox;
        private Label _CoolCodeLabel;
        private Label _StandardCodeLabel;
        private Button __OpenCoolPreviewDialogButton;

        private Button _OpenCoolPreviewDialogButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __OpenCoolPreviewDialogButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__OpenCoolPreviewDialogButton != null)
                {
                    __OpenCoolPreviewDialogButton.Click -= OpenCoolPreviewDialogButtonClick;
                }

                __OpenCoolPreviewDialogButton = value;
                if (__OpenCoolPreviewDialogButton != null)
                {
                    __OpenCoolPreviewDialogButton.Click += OpenCoolPreviewDialogButtonClick;
                }
            }
        }

        private Button __OpenStandardPreviewDialogButton;

        private Button _OpenStandardPreviewDialogButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __OpenStandardPreviewDialogButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__OpenStandardPreviewDialogButton != null)
                {
                    __OpenStandardPreviewDialogButton.Click -= OpenStandardPreviewDialogButtonClick;
                }

                __OpenStandardPreviewDialogButton = value;
                if (__OpenStandardPreviewDialogButton != null)
                {
                    __OpenStandardPreviewDialogButton.Click += OpenStandardPreviewDialogButtonClick;
                }
            }
        }

        private System.Drawing.Printing.PrintDocument __PrintDocument;

        private System.Drawing.Printing.PrintDocument _PrintDocument
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __PrintDocument;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__PrintDocument != null)
                {
                    __PrintDocument.BeginPrint -= PrintDocumentBeginPrint;
                    __PrintDocument.EndPrint -= PrintDocumentEndPrint;
                    __PrintDocument.PrintPage -= PrintDocumentPrintPage;
                }

                __PrintDocument = value;
                if (__PrintDocument != null)
                {
                    __PrintDocument.BeginPrint += PrintDocumentBeginPrint;
                    __PrintDocument.EndPrint += PrintDocumentEndPrint;
                    __PrintDocument.PrintPage += PrintDocumentPrintPage;
                }
            }
        }
    }
}
