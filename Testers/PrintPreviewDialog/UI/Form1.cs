using System;
using System.Drawing;
using System.Windows.Forms;

namespace isr.Core.Tester
{

    /// <summary>   Form 1. </summary>
    /// <remarks>
    /// (c) 2009 Bernardo Castilho. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2009-08-05" by="Bernardo Castilho" revision=""&gt;
    /// http://www.codeproject.com/Articles/38758/An-Enhanced-PrintPreviewDialog. </para>
    /// </remarks>
    public partial class Form1
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Windows.Forms.Form" /> class.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        public Form1()
        {

            // This call is required by the designer.
            InitializeComponent();

            // Add any initialization after the InitializeComponent() call.
            _CoolCodeLabel.Text = @"Using dialog As CoolPrintPreviewDialog = New CoolPrintPreviewDialog
    dialog.Document = Me._PrintDocument
    dialog.ShowDialog(Me)
End Using";
            _StandardCodeLabel.Text = @"Using dialog As PrintPreviewDialog = New PrintPreviewDialog
    dialog.Document = Me._PrintDocument
    dialog.ShowDialog(Me)
End Using";
            __OpenCoolPreviewDialogButton.Name = "_OpenCoolPreviewDialogButton";
            __OpenStandardPreviewDialogButton.Name = "_OpenStandardPreviewDialogButton";
        }

        /// <summary>   The font. </summary>
        private readonly Font _Font = new Font("Segoe UI", 14.0f);
        /// <summary>   The page. </summary>
        private int _Page = 0;
        /// <summary>   The start. </summary>
        private DateTimeOffset _Start;

        /// <summary>
        /// Event handler. Called by _OpenStandardPreviewDialogButton for click events. Show standard
        /// print preview.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void OpenStandardPreviewDialogButtonClick(object sender, EventArgs e)
        {
            using (var dialog = new PrintPreviewDialog())
            {
                dialog.Document = _PrintDocument;
                dialog.ShowDialog(this);
            }
        }

        /// <summary>
        /// Event handler. Called by _OpenCoolPreviewDialogButton for click events. Show cool print
        /// preview.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void OpenCoolPreviewDialogButtonClick(object sender, EventArgs e)
        {
            using (var dialog = new Controls.CoolPrintPreviewDialog())
            {
                dialog.Document = _PrintDocument;
                dialog.ShowDialog(this);
            }
        }

        /// <summary>
        /// Event handler. Called by _PrintDocument for begin print events. Render document.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Print event information. </param>
        private void PrintDocumentBeginPrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            _Start = DateTimeOffset.Now;
            _Page = 0;
        }

        /// <summary>   Event handler. Called by _PrintDocument for end print events. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Print event information. </param>
        private void PrintDocumentEndPrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            Console.WriteLine($"Document rendered in {DateTimeOffset.Now.Subtract(_Start).TotalMilliseconds} ms");
        }

        /// <summary>   Event handler. Called by _PrintDocument for print page events. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Print page event information. </param>
        private void PrintDocumentPrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            var rc = e.MarginBounds;
            rc.Height = _Font.Height + 10;
            int i = 0;
            while (true)
            {
                string text = $"line {i + 1} on page {_Page + 1}";
                e.Graphics.DrawString(text, _Font, Brushes.Black, rc);
                rc.Y += rc.Height;
                if (rc.Bottom > e.MarginBounds.Bottom)
                {
                    _Page += 1;
                    e.HasMorePages = _LongDocumentCheckBox.Checked ? _Page < 3000 : _Page < 30;
                    return;
                }

                i += 1;
            }
        }
    }
}
