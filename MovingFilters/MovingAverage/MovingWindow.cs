using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;

namespace isr.Core.MovingFilters
{

    /// <summary> Moving Window filter. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-01-27 </para>
    /// </remarks>
    public class MovingWindow : MovingAverage, ICloneable
    {

        #region " CONSTRUCTOR "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="length"> The length. </param>
        public MovingWindow( int length ) : base( length )
        {
            this.ResetKnownStateThis();
        }

        /// <summary> The cloning constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> The value. </param>
        public MovingWindow( MovingWindow value ) : base( value )
        {
            this.ResetKnownStateThis();
            if ( value is object )
            {
                this.ReadingTimespan = value.ReadingTimespan;
                this.TimeoutInterval = value.TimeoutInterval;
                this.ReadingTimeoutInterval = value.ReadingTimeoutInterval;
                this.UpdateRule = value.UpdateRule;
                this.RelativeWindow = new FilterWindow<double>( value.RelativeWindow );
                this.Status = value.Status;
                this.ConformityRange = new FilterWindow<double>( value.ConformityRange );
                this.OverflowRange = new FilterWindow<double>( value.OverflowRange );
                this.Readings = new ReadingCollection( value.Readings );
                this.MaximumFailureCount = value.MaximumFailureCount;
                this.MaximumConsecutiveFailureCount = value.MaximumConsecutiveFailureCount;
            }
        }

        /// <summary> Creates a new object that is a copy of the current instance. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> A new object that is a copy of this instance. </returns>
        public override object Clone()
        {
            return new MovingWindow( this );
        }

        #endregion

        #region " RESET AND CLEAR "

        /// <summary> Clears values to their known (initial) state. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        private void ClearKnownStateThis()
        {
            this.Readings.Clear();
            this.ReadingTimespan = TimeSpan.Zero;
            this.Status = MovingWindowStatus.None;
        }

        /// <summary> Clears values to their known (initial) state. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        public override void ClearKnownState()
        {
            base.ClearKnownState();
            this.ClearKnownStateThis();
        }

        /// <summary> Resets to known (default/instantiated) state. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        private void ResetKnownStateThis()
        {
            this.OverflowRange = new FilterWindow<double>( double.MinValue, double.MaxValue );
            this.ConformityRange = new FilterWindow<double>( double.MinValue, double.MaxValue );
            this.MaximumFailureCount = 0;
            this.MaximumConsecutiveFailureCount = 0;
            this.Readings = new ReadingCollection();
            this.TimeoutInterval = TimeSpan.MaxValue;
            this.ReadingTimeoutInterval = DefaultReadingTimeoutInterval;
            this.UpdateRule = MovingWindowUpdateRule.None;
            this.RelativeWindow = new FilterWindow<double>( 0, 0 );
            this.ClearKnownStateThis();
        }

        /// <summary> Resets to known (default/instantiated) state. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        public void ResetKnownState()
        {
            this.ResetKnownStateThis();
        }

        #endregion

        #region " TIMEOUT MANAGER "

        /// <summary> Gets the timeout interval. </summary>
        /// <value> The timeout interval. </value>
        public TimeSpan TimeoutInterval { get; set; }

        /// <summary> Query if 'timeoutInterval' is timeout. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> <c>true</c> if timeout; otherwise <c>false</c> </returns>
        public bool IsTimeout()
        {
            return this.Readings.ElapsedTime > this.TimeoutInterval;
        }

        /// <summary> The default reading timeout interval. </summary>
        /// <value> The default reading timeout interval. </value>
        public static TimeSpan DefaultReadingTimeoutInterval { get; set; } = TimeSpan.FromSeconds( 2d );

        /// <summary> Gets the Reading Timeout interval. </summary>
        /// <value> The ReadingTimeout interval. </value>
        public TimeSpan ReadingTimeoutInterval { get; set; }

        #endregion

        #region " RANGE MANAGEMENT "

        /// <summary>
        /// Gets the Conformity range. Values outside this range are
        /// <see cref="ReadingStatus.NonConformal">non conformal</see>, such as RTD resistance that
        /// fall outside the RTD resistance range.
        /// </summary>
        /// <value> The acceptance range. </value>
        public FilterWindow<double> ConformityRange { get; set; }

        /// <summary>
        /// Gets the overflow range. Values not-inside this range are
        /// <see cref="ReadingStatus.Overflow">Overflow</see>, such as an open circuit.
        /// </summary>
        /// <remarks>
        /// This is designed to address overflow condition such as getting an infinity value (open) from
        /// a SCPI instrument.
        /// </remarks>
        /// <value> The overflow range. </value>
        public FilterWindow<double> OverflowRange { get; set; }

        #endregion

        #region " MOVING WINDOW "

        /// <summary> Gets the update rule. </summary>
        /// <value> The update rule. </value>
        public MovingWindowUpdateRule UpdateRule { get; set; }

        /// <summary> Gets the current window around the mean. </summary>
        /// <value> The current window. </value>
        public FilterWindow<double> CurrentWindow
        {
            get {
                double average = this.Mean;
                return new FilterWindow<double>( average + this.RelativeWindow.Min, average + this.RelativeWindow.Max );
            }
        }

        /// <summary> Gets the relative window. </summary>
        /// <value> The relative window. </value>
        public FilterWindow<double> RelativeWindow { get; set; }

        /// <summary> Gets the total number of reading. </summary>
        /// <value> The number of readings. </value>
        public int TotalReadingsCount => this.Readings.Count;

        /// <summary> Gets the number of readings amenable for averaging. </summary>
        /// <value> The number of readings. </value>
        public int AmenableReadingsCount => this.Readings.StatusReadingDictionary[ReadingStatus.Amenable].Count;

        /// <summary> Gets the first reading. </summary>
        /// <value> The first reading. </value>
        public MovingWindowReading FirstReading => this.Readings.FirstReading;

        /// <summary> Gets the last reading. </summary>
        /// <value> The last reading. </value>
        public MovingWindowReading LastReading => this.Readings.LastReading;

        /// <summary> Gets the amenable readings. </summary>
        /// <value> The amenable readings. </value>
        public IList<MovingWindowReading> AmenableReadings => this.Readings.StatusReadingDictionary[ReadingStatus.Amenable];

        /// <summary> Gets the last amenable reading. </summary>
        /// <value> The last amenable reading. </value>
        public MovingWindowReading LastAmenableReading => this.Readings.LastAmenableReading;

        /// <summary> Gets the elapsed time. </summary>
        /// <value> The elapsed time. </value>
        public TimeSpan ElapsedTime => this.Readings.ElapsedTime;

        /// <summary> Gets the elapsed milliseconds. </summary>
        /// <value> The elapsed milliseconds. </value>
        public double ElapsedMilliseconds => this.Readings.ElapsedMilliseconds;

        /// <summary> Gets the reading timespan. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The reading timespan. </value>
        public TimeSpan ReadingTimespan { get; private set; }

        /// <summary> Reads a value. Measures the reading timespan. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="action"> The action. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool ReadValue( Func<double?> action )
        {
            if ( action is null )
            {
                throw new ArgumentNullException( nameof( action ) );
            }

            var readingStopwatch = Stopwatch.StartNew();
            var value = action();
            this.ReadingTimespan = readingStopwatch.Elapsed;
            this.AddValue( value, this.ReadingTimespan );
            return value.HasValue;
        }

        /// <summary> Adds a value. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> The value. </param>
        public override void AddValue( double value )
        {
            this.AddValue( new double?( value ), TimeSpan.Zero );
        }

        /// <summary> Adds a value. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value">           The value. </param>
        /// <param name="readingTimeSpan"> The reading timespan. </param>
        public void AddValue( double? value, TimeSpan readingTimeSpan )
        {
            var reading = new MovingWindowReading( value, this.ParseReadingStatus( value, readingTimeSpan ), this.Count ) { InputTimespan = readingTimeSpan };
            this.Readings.Add( reading );
            this.AddValue( reading );
        }

        /// <summary> Adds a value. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="reading"> The reading. </param>
        private void AddValue( MovingWindowReading reading )
        {
            if ( reading.Status == ReadingStatus.Amenable )
            {
                double value = reading.Value.Value;
                if ( this.Count < this.Length || this.UpdateRule == MovingWindowUpdateRule.None )
                {
                    this.Status = MovingWindowStatus.Filling;
                    base.AddValue( value );
                }
                else
                {
                    this.Status = ParseMovingWindowStatus( this.CurrentWindow, value );
                    // if value is within window and the window is full we are done.
                    if ( this.Status == MovingWindowStatus.Below || this.Status == MovingWindowStatus.Above )
                    {
                        // if not inside the window, drop first value and add this value
                        base.AddValue( value );
                    }
                }
            }
            else
            {
                // if the reading is not amenable, make sure to resume the filling status.
                this.Status = MovingWindowStatus.Filling;
            }
        }

        #endregion

        #region " MOVING WINDOW STATUS "

        /// <summary> Gets the status. </summary>
        /// <value> The status. </value>
        public MovingWindowStatus Status { get; private set; }

        /// <summary> Returns true if the total failures equals or exceeds the maximum allowed. </summary>
        /// <value> The number of hit failures. </value>
        public bool HitFailureCount => this.MaximumFailureCount <= this.FailureCount;

        /// <summary>
        /// Returns true if the number of consecutive total failures equals or exceeds the maximum
        /// allowed.
        /// </summary>
        /// <value> The number of hit consecutive failures. </value>
        public bool HitConsecutiveFailureCount => this.MaximumConsecutiveFailureCount <= this.ConsecutiveFailureCount;

        /// <summary> Query if this moving windows had a failure count out. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> <c>true</c> if failure count out; otherwise <c>false</c> </returns>
        public bool IsFailureCountOut()
        {
            return this.HitConsecutiveFailureCount || this.HitFailureCount;
        }

        /// <summary> Query if the status is a stop status. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> <c>true</c> if stop status; otherwise <c>false</c> </returns>
        public bool IsStopStatus()
        {
            return this.Status == MovingWindowStatus.Within || this.IsTimeout() || this.IsFailureCountOut();
        }

        /// <summary> Parses the moving window status. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="currentWindow"> The current window. </param>
        /// <param name="value">         The value. </param>
        /// <returns> The <see cref="MovingWindowStatus"/>. </returns>
        private static MovingWindowStatus ParseMovingWindowStatus( FilterWindow<double> currentWindow, double value )
        {
            var currentWin = currentWindow;
            return value > currentWin.Max ? MovingWindowStatus.Above : value < currentWin.Min ? MovingWindowStatus.Below : MovingWindowStatus.Within;
        }

        /// <summary> Status annunciation caption. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="movingWindowStatus"> The status. </param>
        /// <returns> A String. </returns>
        public static string StatusAnnunciationCaption( MovingWindowStatus movingWindowStatus )
        {
            return movingWindowStatus == MovingWindowStatus.Above
                ? "high"
                : movingWindowStatus == MovingWindowStatus.Below
                    ? "low"
                    : movingWindowStatus == MovingWindowStatus.Filling
                                    ? "filling"
                                    : movingWindowStatus == MovingWindowStatus.None ? "n/a" : "within";
        }

        #endregion

        #region " READINGS "

        /// <summary>
        /// Gets the maximum number of <see cref="ReadingStatus.Overflow"/> or
        /// <see cref="ReadingStatus.NonConformal"/> or
        /// <see cref="ReadingStatus.Timeout"/> or
        /// <see cref="ReadingStatus.Indeterminable"/> failures allowed. Zero if unlimited.
        /// </summary>
        /// <value> The number of maximum range failures. </value>
        public int MaximumFailureCount { get; set; }

        /// <summary>
        /// Gets the maximum number of consecutive <see cref="ReadingStatus.Overflow"/> or
        /// <see cref="ReadingStatus.NonConformal"/> or
        /// <see cref="ReadingStatus.Timeout"/> or
        /// <see cref="ReadingStatus.Indeterminable"/> failures allowed. Zero if unlimited.
        /// </summary>
        /// <value> The number of maximum range failures. </value>
        public int MaximumConsecutiveFailureCount { get; set; }

        /// <summary> Gets the number of failures. </summary>
        /// <value> The number of failures. </value>
        public int FailureCount => this.Readings.FailureCount;

        /// <summary> Builds failure report. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> A String. </returns>
        public string BuildFailureReport()
        {
            return this.Readings.BuildFailureReport();
        }

        /// <summary> Builds detailed failure report. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> A String. </returns>
        public string BuildDetailedFailureReport()
        {
            return this.Readings.BuildDetailedFailureReport();
        }

        /// <summary> Gets the number of consecutive failures. </summary>
        /// <value> The number of consecutive failures. </value>
        public int ConsecutiveFailureCount => this.Readings.ConsecutiveFailureCount;

        /// <summary> Parse reading status. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value">         The value. </param>
        /// <param name="inputInterval"> The input interval. </param>
        /// <returns> The ReadingStatus. </returns>
        private ReadingStatus ParseReadingStatus( double? value, TimeSpan inputInterval )
        {
            return inputInterval > this.ReadingTimespan
                ? ReadingStatus.Timeout
                : value.HasValue ? this.ParseReadingStatus( value.Value ) : ReadingStatus.Indeterminable;
        }

        /// <summary> Parse reading status. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> The ReadingStatus. </returns>
        private ReadingStatus ParseReadingStatus( double value )
        {
            ReadingStatus result = !this.OverflowRange.Encloses( value )
                ? ReadingStatus.Overflow
                : !this.ConformityRange.Encloses( value ) ? ReadingStatus.NonConformal : ReadingStatus.Amenable;
            return result;
        }

        /// <summary> Query if 'status' has reading. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="status"> The status. </param>
        /// <returns> <c>true</c> if reading; otherwise <c>false</c> </returns>
        public static bool HasReading( MovingWindowStatus status )
        {
            return status == MovingWindowStatus.Above || status == MovingWindowStatus.Below || status == MovingWindowStatus.Within;
        }

        /// <summary> Status annunciation caption. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> A String. </returns>
        public string StatusAnnunciationCaption()
        {
            return StatusAnnunciationCaption( this.LastReading, this.Status );
        }

        /// <summary> Status annunciation caption. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="lastReading"> The last reading. </param>
        /// <returns> A String. </returns>
        public static string StatusAnnunciationCaption( MovingWindowReading lastReading )
        {
            if ( lastReading is null )
            {
                throw new ArgumentNullException( nameof( lastReading ) );
            }

            string result = string.Empty;
            if ( lastReading.Status == ReadingStatus.Amenable )
            {
                result = "amenable";
            }
            else if ( lastReading.Status == ReadingStatus.Indeterminable )
            {
                result = "Indeterminable";
            }
            else if ( lastReading.Status == ReadingStatus.NonConformal )
            {
                result = "Non-Conforming";
            }
            else if ( lastReading.Status == ReadingStatus.Overflow )
            {
                result = "Overflow";
            }
            else if ( lastReading.Status == ReadingStatus.Timeout )
            {
                result = "Device Timeout";
            }

            return result;
        }

        /// <summary> Status annunciation caption. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="lastReading">        The last reading. </param>
        /// <param name="movingWindowStatus"> The status. </param>
        /// <returns> A String. </returns>
        public static string StatusAnnunciationCaption( MovingWindowReading lastReading, MovingWindowStatus movingWindowStatus )
        {
            return lastReading is null
                ? throw new ArgumentNullException( nameof( lastReading ) )
                : lastReading.Status == ReadingStatus.Amenable ? StatusAnnunciationCaption( movingWindowStatus ) : StatusAnnunciationCaption( lastReading );
        }

        /// <summary> Gets the readings. </summary>
        /// <value> The readings. </value>
        private ReadingCollection Readings { get; set; }

        /// <summary> Collection of readings. </summary>
        /// <remarks>
        /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
        /// Licensed under The MIT License.</para><para>
        /// David, 2018-04-26 </para>
        /// </remarks>
        private class ReadingCollection : System.Collections.ObjectModel.Collection<MovingWindowReading>
        {

            /// <summary> Default constructor. </summary>
            /// <remarks> David, 2020-09-23. </remarks>
            public ReadingCollection() : base()
            {
                this.StatusReadingDictionary = new Dictionary<ReadingStatus, StatusReadingList>();
                foreach ( ReadingStatus status in Enum.GetValues( typeof( ReadingStatus ) ) )
                {
                    this.StatusReadingDictionary.Add( status, new StatusReadingList( status ) );
                }
            }

            /// <summary> Constructor. </summary>
            /// <remarks> David, 2020-09-23. </remarks>
            /// <param name="readings"> The readings. </param>
            public ReadingCollection( ReadingCollection readings ) : this()
            {
                foreach ( MovingWindowReading reading in readings )
                {
                    this.Add( new MovingWindowReading( reading ) );
                }
            }

            /// <summary>
            /// Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1" />.
            /// </summary>
            /// <remarks> David, 2020-09-23. </remarks>
            public new void Clear()
            {
                base.Clear();
                foreach ( ReadingStatus status in Enum.GetValues( typeof( ReadingStatus ) ) )
                {
                    this.StatusReadingDictionary[status].Clear();
                }

                this.LastStatus = ReadingStatus.Amenable;
                this.ConsecutiveCount = 0;
            }

            /// <summary> Gets the last reading status. </summary>
            /// <value> The last status. </value>
            public ReadingStatus LastStatus { get; private set; }

            /// <summary> Gets the number of readings with consecutive status. </summary>
            /// <value> The number of readings with consecutive status. </value>
            private int ConsecutiveCount { get; set; }

            /// <summary> Gets the number of consecutive failures. </summary>
            /// <value> The number of consecutive failures. </value>
            public int ConsecutiveFailureCount => this.LastStatus == ReadingStatus.Amenable ? 0 : this.ConsecutiveCount;

            /// <summary>
            /// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
            /// </summary>
            /// <remarks> David, 2020-09-23. </remarks>
            /// <param name="value"> The object to add to the
            /// <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
            public new void Add( MovingWindowReading value )
            {
                base.Add( value );
                if ( this.LastStatus == value.Status )
                {
                    if ( this.ConsecutiveCount == 0 )
                    {
                        this.ConsecutiveCount += 1;
                    }

                    this.ConsecutiveCount += 1;
                }
                else
                {
                    this.ConsecutiveCount = 0;
                }

                this.LastStatus = value.Status;
                // must handle all these cases in order to correctly count consecutive failures.
                foreach ( ReadingStatus status in Enum.GetValues( typeof( ReadingStatus ) ) )
                {
                    this.StatusReadingDictionary[status].Add( value, this.ConsecutiveCount );
                }
            }

            /// <summary> Gets the last reading. </summary>
            /// <value> The last reading. </value>
            public MovingWindowReading FirstReading => this.Any() ? this[0] : new MovingWindowReading( new double?(), ReadingStatus.Amenable, 0 );

            /// <summary> Gets the last reading. </summary>
            /// <value> The last reading. </value>
            public MovingWindowReading LastReading => this.Any() ? this.Last() : new MovingWindowReading( new double?(), ReadingStatus.Amenable, 0 );

            /// <summary> Gets the elapsed time. </summary>
            /// <value> The elapsed time. </value>
            public TimeSpan ElapsedTime => this.Any() ? this.LastReading.Timestamp.Subtract( this.FirstReading.Timestamp ) : TimeSpan.Zero;

            /// <summary> Gets the elapsed milliseconds. </summary>
            /// <value> The elapsed milliseconds. </value>
            public double ElapsedMilliseconds => this.ElapsedTime.Ticks / ( double ) TimeSpan.TicksPerMillisecond;

            /// <summary> Gets the amenable readings. </summary>
            /// <value> The amenable readings. </value>
            public IList<MovingWindowReading> AmenableReadings => this.StatusReadingDictionary[ReadingStatus.Amenable];

            /// <summary> Gets the last amenable reading. </summary>
            /// <value> The last amenable reading. </value>
            public MovingWindowReading LastAmenableReading => this.StatusReadingDictionary[ReadingStatus.Amenable].LastReading;

            /// <summary> Gets a dictionary of status readings. </summary>
            /// <value> A dictionary of status readings. </value>
            public Dictionary<ReadingStatus, StatusReadingList> StatusReadingDictionary { get; private set; }

            /// <summary> Gets the number of failures. </summary>
            /// <value> The number of failures. </value>
            public int FailureCount
            {
                get {
                    int count = 0;
                    foreach ( ReadingStatus status in Enum.GetValues( typeof( ReadingStatus ) ) )
                    {
                        if ( status != ReadingStatus.Amenable )
                        {
                            count += this.StatusReadingDictionary[status].Count;
                        }
                    }

                    return count;
                }
            }

            /// <summary> Appends a line if has value. </summary>
            /// <remarks> David, 2020-09-23. </remarks>
            /// <param name="builder"> The builder. </param>
            /// <param name="value">   The object to add to the
            /// <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
            private static void AppendLineIfHasValue( System.Text.StringBuilder builder, string value )
            {
                if ( !string.IsNullOrWhiteSpace( value ) )
                {
                    _ = builder.AppendLine( value );
                }
            }

            /// <summary> Builds failure report. </summary>
            /// <remarks> David, 2020-09-23. </remarks>
            /// <returns> A String. </returns>
            public string BuildFailureReport()
            {
                var builder = new System.Text.StringBuilder();
                foreach ( ReadingStatus status in Enum.GetValues( typeof( ReadingStatus ) ) )
                {
                    if ( status != ReadingStatus.Amenable )
                    {
                        AppendLineIfHasValue( builder, this.StatusReadingDictionary[status].BuildFailureReport() );
                    }
                }

                return builder.ToString().TrimEnd( Environment.NewLine.ToCharArray() );
            }

            /// <summary> Builds detailed failure report. </summary>
            /// <remarks> David, 2020-09-23. </remarks>
            /// <returns> A String. </returns>
            public string BuildDetailedFailureReport()
            {
                var builder = new System.Text.StringBuilder();
                foreach ( ReadingStatus status in Enum.GetValues( typeof( ReadingStatus ) ) )
                {
                    if ( status != ReadingStatus.Amenable )
                    {
                        AppendLineIfHasValue( builder, this.StatusReadingDictionary[status].BuildDetailedFailureReport() );
                    }
                }

                return builder.ToString().TrimEnd( Environment.NewLine.ToCharArray() );
            }
        }

        /// <summary> List of readings identified by reading status. </summary>
        /// <remarks>
        /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
        /// Licensed under The MIT License.</para><para>
        /// David, 2018-04-26 </para>
        /// </remarks>
        private class StatusReadingList : List<MovingWindowReading>
        {

            /// <summary> Constructor. </summary>
            /// <remarks> David, 2020-09-23. </remarks>
            /// <param name="status"> The status. </param>
            public StatusReadingList( ReadingStatus status ) : base()
            {
                this.Status = status;
            }

            /// <summary> Constructor. </summary>
            /// <remarks> David, 2020-09-23. </remarks>
            /// <param name="readings"> The readings. </param>
            public StatusReadingList( StatusReadingList readings ) : this( readings.Status )
            {
                foreach ( MovingWindowReading reading in readings )
                {
                    this.Add( new MovingWindowReading( reading ) );
                }

                this.ConsecutiveCount = readings.ConsecutiveCount;
            }

            /// <summary>
            /// Removes all elements from the <see cref="T:System.Collections.Generic.List`1" />.
            /// </summary>
            /// <remarks> David, 2020-09-23. </remarks>
            public new void Clear()
            {
                base.Clear();
                this.ConsecutiveCount = 0;
            }

            /// <summary> Gets the status. </summary>
            /// <value> The status. </value>
            public ReadingStatus Status { get; private set; }

            /// <summary> Gets the number of consecutive readings of the <see cref="Status"/>. </summary>
            /// <value> The number of consecutive readings of the <see cref="Status"/>. </value>
            public int ConsecutiveCount { get; private set; }

            /// <summary>
            /// Adds an item to the <see cref="T:System.Collections.Generic.Collection" />.
            /// </summary>
            /// <remarks> David, 2020-09-23. </remarks>
            /// <param name="reading">          The object to add to the
            /// <see cref="T:System.Collections.Generic.Collection" />.
            /// </param>
            /// <param name="consecutiveCount"> The number of consecutive readings of the
            /// <see cref="Status"/>. </param>
            public void Add( MovingWindowReading reading, int consecutiveCount )
            {
                if ( reading.Status == this.Status )
                {
                    this.Add( reading );
                    this.ConsecutiveCount = consecutiveCount;
                }
                else
                {
                    this.ConsecutiveCount = 0;
                }
            }

            /// <summary> Builds failure report. </summary>
            /// <remarks> David, 2020-09-23. </remarks>
            /// <returns> A String. </returns>
            public string BuildFailureReport()
            {
                var builder = new System.Text.StringBuilder();
                if ( this.ConsecutiveCount > 0 )
                {
                    _ = builder.AppendLine( $"{this.Status} {this.ConsecutiveCount} consecutive {(this.Status == ReadingStatus.Amenable ? "readings" : "failures")}" );
                }

                if ( this.Count > 0 )
                {
                    _ = builder.AppendLine( $"{this.Status} {this.Count} {(this.Status == ReadingStatus.Amenable ? "readings" : "failures")}" );
                }

                return builder.ToString().TrimEnd( Environment.NewLine.ToCharArray() );
            }

            /// <summary> Builds detailed failure report. </summary>
            /// <remarks> David, 2020-09-23. </remarks>
            /// <returns> A String. </returns>
            public string BuildDetailedFailureReport()
            {
                var builder = new System.Text.StringBuilder();
                if ( this.ConsecutiveCount > 0 )
                {
                    _ = builder.AppendLine( $"{this.Status} {this.ConsecutiveCount} consecutive failures" );
                }

                if ( this.Count > 0 )
                {
                    _ = builder.AppendLine( $"{this.Status} {this.Count} failures" );
                }

                foreach ( MovingWindowReading reading in this )
                {
                    _ = reading.Value.HasValue
                        ? builder.AppendLine( $"{reading.Value.Value:G4} {this.Status} {reading.Timestamp:HH:mm:ss.fff} {reading.OrdinalNumber}" )
                        : builder.AppendLine( $"{this.Status} {reading.Timestamp:HH:mm:ss.fff} {reading.OrdinalNumber}" );
                }

                return builder.ToString().TrimEnd( Environment.NewLine.ToCharArray() );
            }

            /// <summary> Gets the last reading. </summary>
            /// <value> The last reading. </value>
            public MovingWindowReading LastReading => this.Any() ? this.Last() : new MovingWindowReading( new double?(), ReadingStatus.Amenable, 0 );
        }

        #endregion

        #region " PERCENT PROGRESS "

        /// <summary> Gets the percent progress. This will reset if readings exceed the length. </summary>
        /// <value> The percent progress. </value>
        public int PercentProgress
        {
            get {
                int minimumExpectedCount = this.Length + 1;
                // this will get to 100% when hitting the minimum expected count.
                int count = this.TotalReadingsCount % (minimumExpectedCount + 1);
                return ( int ) (100 * count / ( double ) minimumExpectedCount);
            }
        }

        #endregion

    }

    /// <summary> Values that represent moving window update rules. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    public enum MovingWindowUpdateRule
    {

        /// <summary> An enum constant representing the none option. </summary>
        [Description( "Not stopping" )]
        None,

        /// <summary> An enum constant representing the stop on within window option. </summary>
        [Description( "Stop On Within Window" )]
        StopOnWithinWindow
    }

    /// <summary> Values that represent moving window status. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    public enum MovingWindowStatus
    {

        /// <summary> An enum constant representing the none option. </summary>
        [Description( "Not set" )]
        None,

        /// <summary> An enum constant representing the filling option. </summary>
        [Description( "Filling" )]
        Filling,

        /// <summary> An enum constant representing the within option. </summary>
        [Description( "Within Window" )]
        Within,

        /// <summary> An enum constant representing the above option. </summary>
        [Description( "Above Window" )]
        Above,

        /// <summary> An enum constant representing the below option. </summary>
        [Description( "Below window" )]
        Below
    }

    /// <summary> Values that represent reading status. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    public enum ReadingStatus
    {

        /// <summary> An enum constant representing the amenable option; the reading has a value that can be averaged. </summary>
        [Description( "Amenable" )]
        Amenable,

        /// <summary> An enum constant representing the non conformal option; the reading is outside the conformity range. </summary>
        [Description( "Non Conformal" )]
        NonConformal,

        /// <summary> An enum constant representing the overflow option; the reading is an overflow. </summary>
        [Description( "Overflow" )]
        Overflow,

        /// <summary> An enum constant representing the indeterminable option; for example, a value could not be read from the instrument. </summary>
        [Description( "Indeterminable" )]
        Indeterminable,

        /// <summary> An enum constant representing the Timeout option; for example, a value could not be read from the instrument withing a prescribed time. </summary>
        [Description( "Timeout" )]
        Timeout
    }

    /// <summary> A moving window reading. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-04-26 </para>
    /// </remarks>
    public class MovingWindowReading
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value">         The value. </param>
        /// <param name="status">        The status. </param>
        /// <param name="ordinalNumber"> The ordinal number. </param>
        public MovingWindowReading( double? value, ReadingStatus status, int ordinalNumber ) : base()
        {
            this.Value = value;
            this.Status = status;
            this.OrdinalNumber = ordinalNumber;
            this.Timestamp = DateTimeOffset.Now;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="reading"> The reading. </param>
        public MovingWindowReading( MovingWindowReading reading ) : this( Validated( reading ).Value, reading.Status, reading.OrdinalNumber )
        {
            this.InputTimespan = reading.InputTimespan;
        }

        /// <summary> Validated the given reading. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="reading"> The reading. </param>
        /// <returns> A reading. </returns>
        public static MovingWindowReading Validated( MovingWindowReading reading )
        {
            return reading is null ? throw new ArgumentNullException( nameof( reading ) ) : reading;
        }

        /// <summary> Gets or sets the value. </summary>
        /// <value> The value. </value>
        public double? Value { get; private set; }

        /// <summary> Gets or sets the status. </summary>
        /// <value> The status. </value>
        public ReadingStatus Status { get; private set; }

        /// <summary> Gets or sets the timestamp. </summary>
        /// <value> The timestamp. </value>
        public DateTimeOffset Timestamp { get; private set; }

        /// <summary> Gets or sets the ordinal number. </summary>
        /// <value> The ordinal number. </value>
        public int OrdinalNumber { get; private set; }

        /// <summary> Gets or sets the input timespan. </summary>
        /// <value> The input timespan. </value>
        public TimeSpan InputTimespan { get; set; }
    }
}
