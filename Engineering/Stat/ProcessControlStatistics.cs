using System;

#pragma warning disable IDE1006 // Naming Styles
namespace isr.Core.Engineering
#pragma warning restore IDE1006 // Naming Styles
{

    /// <summary> Process control statistics. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-04-29 </para>
    /// </remarks>
    public class ProcessControlStatistics
    {

        #region " PROCESS CONTROL "

        /// <summary> Gets the sentinel indicating if the process statistics were evaluated. </summary>
        /// <value> <c>True</c> is statistics were evaluated; otherwise <c>False</c> . </value>
        public bool HasValue => this.Sigma > 0d;

        /// <summary> Gets or sets capability ratio (Cp). </summary>
        /// <value> The cp. </value>
        public double Cp { get; set; }

        /// <summary> Gets or sets the capability ratio minimal (Cpk). </summary>
        /// <value> The capability ratio minimal (Cpk). </value>
        public double Cpk { get; set; }

        /// <summary> Gets or sets the capability ratio nominal (Cpm). </summary>
        /// <value> The capability ratio nominal  (Cpm). </value>
        public double Cpm { get; set; }

        /// <summary> Gets or sets the sigma level. DSefaults to 6. </summary>
        /// <value> The sigma level. </value>
        public double SigmaLevel { get; set; } = 6d;

        /// <summary> Gets or sets the sigma. </summary>
        /// <value> The sigma. </value>
        public double Sigma { get; private set; }

        /// <summary> Gets or sets the mean. </summary>
        /// <value> The mean value. </value>
        public double Mean { get; private set; }

        /// <summary> Gets or sets target value. </summary>
        /// <value> The target value. </value>
        public double TargetValue { get; private set; }

        /// <summary> Gets or sets the lower specification limit. </summary>
        /// <value> The lower specification limit. </value>
        public double LowerSpecificationLimit { get; private set; }

        /// <summary> Gets or sets the upper specification limit. </summary>
        /// <value> The upper specification limit. </value>
        public double UpperSpecificationLimit { get; private set; }

        /// <summary> Calculates the process control capability ratios. </summary>
        /// <remarks>
        /// See http://www.mpcps.com/ for confidence intervals for the process control values. The ratio
        /// of sigma to standard deviation is 1.25/0.8 at N=30 at a confidence level of 95% and 1.5,0.75
        /// at 99.5%. N-30 is often used as the minimum sample size for getting decent values.
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="targetValue">        Target value. </param>
        /// <param name="specificationLimit"> The specification limit interval. </param>
        /// <param name="mean">               The sample mean. </param>
        /// <param name="sigma">              The sample sigma. </param>
        public void Evaluate( double targetValue, Interval specificationLimit, double mean, double sigma )
        {
            if ( specificationLimit is null )
            {
                throw new ArgumentNullException( nameof( specificationLimit ) );
            }

            this.Evaluate( targetValue, specificationLimit.LowEndPoint, specificationLimit.HighEndPoint, mean, sigma );
        }

        /// <summary> Calculates the process control capability ratios. </summary>
        /// <remarks> Values are set to <see cref="Double.PositiveInfinity"/> if sigma is zero. </remarks>
        /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="targetValue">             Target value. </param>
        /// <param name="lowerSpecificationLimit"> The lower specification limit. </param>
        /// <param name="upperSpecificationLimit"> The upper specification limit. </param>
        /// <param name="mean">                    The sample mean. </param>
        /// <param name="sigma">                   The sample sigma. </param>
        public void Evaluate( double targetValue, double lowerSpecificationLimit, double upperSpecificationLimit, double mean, double sigma )
        {
            if ( upperSpecificationLimit <= lowerSpecificationLimit )
            {
                throw new ArgumentException( $"USL {upperSpecificationLimit} must be higher than LSL {lowerSpecificationLimit}", nameof( upperSpecificationLimit ) );
            }

            this.Sigma = sigma;
            this.Mean = mean;
            this.TargetValue = targetValue;
            this.LowerSpecificationLimit = lowerSpecificationLimit;
            this.UpperSpecificationLimit = upperSpecificationLimit;
            this.Evaluate();
        }

        /// <summary> Calculates the process control capability ratios. </summary>
        /// <remarks>
        /// See http://www.mpcps.com/ for confidence intervals for the process control values. The ratio
        /// of sigma to standard deviation is 1.25/0.8 at N=30 at a confidence level of 95% and 1.5,0.75
        /// at 99.5%. N-30 is often used as the minimum sample size for getting decent values.
        /// </remarks>
        private void Evaluate()
        {
            if ( this.HasValue )
            {
                double sixSigma = this.SigmaLevel * this.Sigma;
                this.Cp = (this.UpperSpecificationLimit - this.LowerSpecificationLimit) / sixSigma;
                this.Cpk = Math.Min( this.UpperSpecificationLimit - this.Mean, this.Mean - this.LowerSpecificationLimit ) / (0.5d * sixSigma);
                double delta = (this.Mean - this.TargetValue) / this.Sigma;
                this.Cpm = this.Cp / Math.Sqrt( 1d + delta * delta );
            }
        }

        /// <summary> Evaluate cp. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="sigma">                   The sample sigma. </param>
        /// <param name="upperSpecificationLimit"> The upper specification limit. </param>
        /// <param name="lowerSpecificationLimit"> The lower specification limit. </param>
        /// <returns> A Double? </returns>
        public static double EvaluateCp( double sigma, double upperSpecificationLimit, double lowerSpecificationLimit )
        {
            return sigma < 0d
                ? throw new InvalidOperationException( $"Sigma is {sigma}" )
                : EvaluateCp( sigma, upperSpecificationLimit, lowerSpecificationLimit, 6d );
        }

        /// <summary> Evaluate cp. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="sigma">                   The sample sigma. </param>
        /// <param name="upperSpecificationLimit"> The upper specification limit. </param>
        /// <param name="lowerSpecificationLimit"> The lower specification limit. </param>
        /// <param name="sigmaLevel">              The sigma level. </param>
        /// <returns> A Double? </returns>
        public static double EvaluateCp( double sigma, double upperSpecificationLimit, double lowerSpecificationLimit, double sigmaLevel )
        {
            return (upperSpecificationLimit - lowerSpecificationLimit) / (sigmaLevel * sigma);
        }

        /// <summary> Evaluate cpk. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="mean">                    The sample mean. </param>
        /// <param name="sigma">                   The sample sigma. </param>
        /// <param name="upperSpecificationLimit"> The upper specification limit. </param>
        /// <param name="lowerSpecificationLimit"> The lower specification limit. </param>
        /// <param name="sigmaLevel">              The sigma level. </param>
        /// <returns> A Double? </returns>
        public static double EvaluateCpk( double mean, double sigma, double upperSpecificationLimit, double lowerSpecificationLimit, double sigmaLevel )
        {
            return sigma < 0d
                ? throw new InvalidOperationException( $"Sigma is {sigma}" )
                : Math.Min( upperSpecificationLimit - mean, mean - lowerSpecificationLimit ) / (0.5d * sigmaLevel * sigma);
        }

        /// <summary> Evaluate cpk. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="mean">                    The sample mean. </param>
        /// <param name="sigma">                   The sample sigma. </param>
        /// <param name="upperSpecificationLimit"> The upper specification limit. </param>
        /// <param name="lowerSpecificationLimit"> The lower specification limit. </param>
        /// <returns> A Double? </returns>
        public static double EvaluateCpk( double mean, double sigma, double upperSpecificationLimit, double lowerSpecificationLimit )
        {
            return EvaluateCpk( mean, sigma, upperSpecificationLimit, lowerSpecificationLimit, 6d );
        }

        /// <summary> Evaluate cpm. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="mean">                    The sample mean. </param>
        /// <param name="sigma">                   The sample sigma. </param>
        /// <param name="upperSpecificationLimit"> The upper specification limit. </param>
        /// <param name="lowerSpecificationLimit"> The lower specification limit. </param>
        /// <param name="targetValue">             Target value. </param>
        /// <param name="sigmaLevel">              The sigma level. </param>
        /// <returns> A Double? </returns>
        public static double EvaluateCpm( double mean, double sigma, double upperSpecificationLimit, double lowerSpecificationLimit, double targetValue, double sigmaLevel )
        {
            if ( sigma < 0d )
            {
                throw new InvalidOperationException( $"Sigma is {sigma}" );
            }

            double delta = (mean - targetValue) / sigma;
            return (upperSpecificationLimit - lowerSpecificationLimit) / (sigmaLevel * sigma * Math.Sqrt( 1d + delta * delta ));
        }

        /// <summary> Evaluate cpm. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="mean">                    The sample mean. </param>
        /// <param name="sigma">                   The sample sigma. </param>
        /// <param name="upperSpecificationLimit"> The upper specification limit. </param>
        /// <param name="lowerSpecificationLimit"> The lower specification limit. </param>
        /// <param name="targetValue">             Target value. </param>
        /// <returns> A Double? </returns>
        public static double EvaluateCpm( double mean, double sigma, double upperSpecificationLimit, double lowerSpecificationLimit, double targetValue )
        {
            return EvaluateCpm( mean, sigma, upperSpecificationLimit, lowerSpecificationLimit, targetValue, 6d );
        }

        #endregion

    }
}
