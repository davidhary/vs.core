using System;
using System.Collections.Generic;
using System.Linq;

namespace isr.Core.Engineering
{

    /// <summary> Sample statistics. </summary>
    /// <remarks>
    /// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-11-19 </para>
    /// </remarks>
    public class SampleStatistics : ICloneable
    {

        #region " CONSTRUCTOR "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        public SampleStatistics() : base()
        {
            this.ValuesList = new List<double>();
            this.ResetKnownStateThis();
        }

        /// <summary> The cloning constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> The value. </param>
        public SampleStatistics( SampleStatistics value ) : this()
        {
            if ( value is object )
            {
                this.ClearKnownState();
                this._Mean = value.Mean;
                this._Sigma = value.Sigma;
                this._Sum = value.Sum;
                this._SumSquareDeviations = value.SumSquareDeviations;
                this._Maximum = value.Maximum;
                this._Minimum = value.Minimum;
                this.ValuesArray = value.ValuesArray.ToArray();
                this.AddValues( value.ValuesList.ToArray() );
            }
        }

        /// <summary> Creates a new object that is a copy of the current instance. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> A new object that is a copy of this instance. </returns>
        public virtual object Clone()
        {
            return new SampleStatistics( this );
        }

        /// <summary> Clears values to their known (initial) state. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        private void ClearKnownStateThis()
        {
            this.Count = 0;
            this._Mean = 0d;
            this._Sigma = 0d;
            this._Sum = 0d;
            this._SumSquareDeviations = 0d;
            this._Maximum = double.MinValue;
            this._Minimum = double.MaxValue;
            this.ValuesArray = Array.Empty<double>();
            this.ValuesArray = Array.Empty<double>();
            this.ValuesList.Clear();
        }

        /// <summary> Clears values to their known (initial) state. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        public virtual void ClearKnownState()
        {
            this.ClearKnownStateThis();
        }

        /// <summary> Resets to known (default/instantiated) state. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        private void ResetKnownStateThis()
        {
            this.ClearKnownStateThis();
        }

        /// <summary> Resets to known (default/instantiated) state. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        public virtual void ResetKnownState()
        {
            this.ResetKnownStateThis();
        }

        #endregion

        #region " STATISTICS "

        /// <summary> The maximum. </summary>
        private double _Maximum;

        /// <summary> Gets the maximum. </summary>
        /// <value> The maximum value. </value>
        public double Maximum
        {
            get {
                if ( this.RangeRequired )
                {
                    this.EvaluateRange();
                }

                return this._Maximum;
            }
        }

        /// <summary> The minimum. </summary>
        private double _Minimum;

        /// <summary> Gets the minimum. </summary>
        /// <value> The minimum value. </value>
        public double Minimum
        {
            get {
                if ( this.RangeRequired )
                {
                    this.EvaluateRange();
                }

                return this._Minimum;
            }
        }

        /// <summary> The mean. </summary>
        private double _Mean;

        /// <summary> Gets the mean. </summary>
        /// <value> The mean value. </value>
        public double Mean
        {
            get {
                if ( this.MeanRequired )
                {
                    _ = this.EvaluateMean();
                }

                return this._Mean;
            }
        }

        /// <summary> The sigma. </summary>
        private double _Sigma;

        /// <summary> Gets the sigma. </summary>
        /// <value> The sigma. </value>
        public double Sigma
        {
            get {
                if ( this.SigmaRequired )
                {
                    _ = this.EvaluateSigma( this.Mean );
                }

                return this._Sigma;
            }
        }

        /// <summary> Number of. </summary>
        private double _Sum;

        /// <summary> Gets the number of. </summary>
        /// <value> The sum. </value>
        public double Sum
        {
            get {
                if ( this.MeanRequired )
                {
                    _ = this.EvaluateMean();
                }

                return this._Sum;
            }
        }

        /// <summary> The sum square deviations. </summary>
        private double _SumSquareDeviations;

        /// <summary> Gets the sum square deviations. </summary>
        /// <value> The total number of square deviations. </value>
        public double SumSquareDeviations
        {
            get {
                if ( this.SigmaRequired )
                {
                    _ = this.EvaluateSigma( this.Mean );
                }

                return this._SumSquareDeviations;
            }
        }

        /// <summary> Returns true if the sample includes values. </summary>
        /// <value> any. </value>
        public bool Any => this.Count > 0;

        /// <summary> Gets or sets the number of values. </summary>
        /// <value> The count. </value>
        public int Count { get; protected set; }

        /// <summary> Gets the mean required. </summary>
        /// <value> The mean required. </value>
        protected bool MeanRequired { get; set; }

        /// <summary> Gets the statistics required. </summary>
        /// <value> The statistics required. </value>
        protected bool SigmaRequired { get; set; }

        /// <summary> Gets the range required. </summary>
        /// <value> The range required. </value>
        protected bool RangeRequired { get; set; }

        /// <summary> Gets the cast to array required. </summary>
        /// <value> The cast to array required. </value>
        protected bool CastToArrayRequired { get; set; }

        /// <summary>
        /// Gets the values changed indicating the values were either added or removed.
        /// </summary>
        /// <value> The values changed. </value>
        public bool ValuesChanged => this.CastToArrayRequired;

        #endregion

        #region " VALUES "

        /// <summary> Gets or sets an array of values. </summary>
        /// <value> An array of values. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1819:Properties should not return arrays", Justification = "<Pending>" )]
        public double[] ValuesArray { get; private set; }

        /// <summary> Gets or sets the values. </summary>
        /// <value> A list of values. </value>
        public IList<double> ValuesList { get; private set; }

        /// <summary> Adds a value. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> The value. </param>
        public virtual void AddValue( double value )
        {
            this.MeanRequired = true;
            this.SigmaRequired = true;
            this.RangeRequired = true;
            this.CastToArrayRequired = true;
            this.ValuesList.Add( value );
            this.Count += 1;
        }

        /// <summary> Adds the values. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="values"> The values. </param>
        public void AddValues( double[] values )
        {
            if ( values is object )
            {
                this.MeanRequired = true;
                this.SigmaRequired = true;
                this.RangeRequired = true;
                this.CastToArrayRequired = true;
                foreach ( double value in values )
                {
                    this.ValuesList.Add( value );
                    this.Count += 1;
                }
            }
        }

        /// <summary> Removes the value at the index. </summary>
        /// <remarks> David, 2020-09-07. </remarks>
        /// <param name="index"> Zero-based index of the values. </param>
        public virtual void RemoveValueAt( int index )
        {
            this.MeanRequired = true;
            this.SigmaRequired = true;
            this.RangeRequired = true;
            this.CastToArrayRequired = true;
            this.ValuesList.RemoveAt( index );
            this.Count -= 1;
        }

        #endregion

        #region " EVALUATE "

        /// <summary>
        /// Converts the internal <see cref="ValuesList">list of values</see> to an internal
        /// <see cref="ValuesArray">array of values</see>.
        /// </summary>
        /// <remarks>   David, 2020-10-09. </remarks>
        public void CastToArray()
        {
            if ( this.CastToArrayRequired )
            {
                this.ValuesArray = this.ValuesList.ToArray();
                this.CastToArrayRequired = false;
            }
        }

        /// <summary> Evaluate mean. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> A Double. </returns>
        public double EvaluateMean()
        {
            if ( this.CastToArrayRequired )
            {
                this.ValuesArray = this.ValuesList.ToArray();
                this.CastToArrayRequired = false;
            }

            if ( this.MeanRequired )
            {
                this._Sum = 0d;
                this._Mean = 0d;
                if ( this.RangeRequired )
                {
                    if ( this.Any )
                    {
                        this._Minimum = this.ValuesArray[0];
                        this._Maximum = this._Minimum;
                        foreach ( double value in this.ValuesArray )
                        {
                            if ( value > this._Maximum )
                            {
                                this._Maximum = value;
                            }
                            else if ( value < this._Minimum )
                            {
                                this._Minimum = value;
                            }

                            this._Sum += value;
                        }
                    }
                    else
                    {
                        this._Maximum = double.MinValue;
                        this._Minimum = double.MaxValue;
                    }

                    this.RangeRequired = false;
                }
                else if ( this.Any )
                {
                    foreach ( double value in this.ValuesArray )
                    {
                        this._Sum += value;
                    }
                }

                this._Mean = this._Sum / this.Count;
                this.MeanRequired = false;
            }

            return this._Mean;
        }

        /// <summary> Evaluate sigma. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="average"> The average. </param>
        /// <returns> A Double. </returns>
        public double EvaluateSigma( double average )
        {
            if ( this.CastToArrayRequired )
            {
                this.ValuesArray = this.ValuesList.ToArray();
                this.CastToArrayRequired = false;
            }

            if ( this.SigmaRequired )
            {
                this._Sigma = 0d;
                this._SumSquareDeviations = 0d;
                if ( this.Any && this.Count > 1 )
                {
                    foreach ( double v in this.ValuesArray )
                    {
                        this._SumSquareDeviations += (v - average) * (v - average);
                    }

                    this._Sigma = Math.Sqrt( this._SumSquareDeviations / (this.Count - 1) );
                }

                this.SigmaRequired = false;
            }

            return this._Sigma;
        }

        /// <summary> Evaluates correlation coefficient. </summary>
        /// <remarks> Assumes that the function values already exist. </remarks>
        /// <param name="values"> The values. </param>
        /// <returns> The correlation coefficient or coefficient of multiple determination. </returns>
        public double EvaluateCorrelationCoefficient( double[] values )
        {
            var valuesSample = new SampleStatistics();
            valuesSample.AddValues( values );
            double favg = valuesSample.Mean;
            double fssq = valuesSample.SumSquareDeviations;
            double oavg = this.Mean;
            double ossq = this.SumSquareDeviations;
            double ofssq = 0d;
            for ( int i = 0, loopTo = this.Count - 1; i <= loopTo; i++ )
            {
                ofssq += (this.ValuesArray[i] - oavg) * (valuesSample.ValuesArray[i] - favg);
            }

            return ofssq / (Math.Sqrt( fssq ) * Math.Sqrt( ossq ));
        }

        /// <summary> Evaluates this object. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        public void Evaluate()
        {
            _ = this.EvaluateSigma( this.Mean );
            this.EvaluateRange();
        }

        /// <summary> Evaluate range. </summary>
        /// <remarks> David, 2020-09-07. </remarks>
        public void EvaluateRange()
        {
            if ( this.CastToArrayRequired )
            {
                this.ValuesArray = this.ValuesList.ToArray();
                this.CastToArrayRequired = false;
            }

            if ( this.RangeRequired )
            {
                if ( this.Any )
                {
                    this._Minimum = this.ValuesArray[0];
                    this._Maximum = this._Minimum;
                    foreach ( double value in this.ValuesArray )
                    {
                        if ( value > this._Maximum )
                        {
                            this._Maximum = value;
                        }
                        else if ( value < this._Minimum )
                        {
                            this._Minimum = value;
                        }
                    }
                }
                else
                {
                    this._Maximum = double.MinValue;
                    this._Minimum = double.MaxValue;
                }

                this.RangeRequired = false;
            }
        }

        #endregion

    }
}
