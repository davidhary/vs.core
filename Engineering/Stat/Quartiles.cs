using System;
using System.Linq;

namespace isr.Core.Engineering
{

    /// <summary> A quartiles. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-05-10 </para>
    /// </remarks>
    public class Quartiles
    {

        #region " CONSTRUCTOR "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        public Quartiles() : base()
        {
            this.ClearKnownStateThis();
        }

        #endregion

        #region " QUARTILES "

        /// <summary> Clears values to their known (initial) state. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        private void ClearKnownStateThis()
        {
            this._Values = new[] { 0d, 0d, 0d };
        }

        /// <summary> Clears values to their known (initial) state. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        public virtual void ClearKnownState()
        {
            this.ClearKnownStateThis();
        }

        /// <summary> Gets the Median (second quartile). </summary>
        /// <value> The Median value of the  excluding outliers. </value>
        public double Median => this.Values()[1];

        /// <summary> Gets the first quartile. </summary>
        /// <value> The first quartile. </value>
        public double First => this.Values()[0];

        /// <summary> Gets the Second quartile (median). </summary>
        /// <value> The Second quartile (median). </value>
        public double Second => this.Values()[1];

        /// <summary> Gets the Third quartile. </summary>
        /// <value> The Third quartile. </value>
        public double Third => this.Values()[2];

        /// <summary> The values. </summary>
        private double[] _Values;

        /// <summary> Gets the quartiles. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> A Double() array with 1st, 2nd, and 3rd quartile values. </returns>
        public double[] Values()
        {
            return this._Values;
        }

        /// <summary> Gets the quartile range. </summary>
        /// <value> The quartile range. </value>
        public double Range => this.Values()[2] - this.Values()[0];

        /// <summary> Calculates the percentile. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="sortedValues"> The sorted values. </param>
        /// <param name="value">        The percentile value between 0 and 1. </param>
        /// <returns> A Double value representing the percentile. </returns>
        public static double Percentile( double[] sortedValues, double value )
        {
            if ( sortedValues is null )
            {
                throw new ArgumentNullException( nameof( sortedValues ) );
            }

            bool useLibraOfficeStyle = true;
            double realIndex = useLibraOfficeStyle ? value * (sortedValues.Length - 1) : value * sortedValues.Length - 1d;
            if ( realIndex < 0d )
            {
                throw new InvalidOperationException( "Insufficient data for establishing the percentile" );
            }

            int index = ( int ) Math.Floor( realIndex );
            double fraction = realIndex - index;
            return index >= sortedValues.Length - 1 ? sortedValues[sortedValues.Length - 1] : sortedValues[index] + fraction * (sortedValues[index + 1] - sortedValues[index]);
        }

        /// <summary> Calculates the quartiles. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="values"> The values. </param>
        /// <returns> The calculated quartiles. </returns>
        public static double[] CalculateQuartiles( double[] values )
        {
            if ( values is null )
            {
                throw new ArgumentNullException( nameof( values ) );
            }

            if ( values.Count() < 4 )
            {
                throw new InvalidOperationException( "Insufficient data for establishing quartiles" );
            }

            Array.Sort( values );
            return new double[] { Percentile( values, 0.25d ), Percentile( values, 0.5d ), Percentile( values, 0.75d ) };
        }

        /// <summary> Evaluates the quartiles. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="samples"> The samples. </param>
        public void Evaluate( double[] samples )
        {
            this.ClearKnownState();
            this._Values = CalculateQuartiles( samples );
        }

        #endregion

    }
}
