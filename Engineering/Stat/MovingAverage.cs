using System;

namespace isr.Core.Engineering
{

    /// <summary> Moving average. </summary>
    /// <remarks>
    /// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-11-19 </para>
    /// </remarks>
    public class MovingAverage : SampleStatistics, ICloneable
    {

        #region " CONSTRUCTOR "

        /// <summary> The minimum length of the. </summary>
        private const int _MinimumLength = 2;

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="length"> The length. </param>
        public MovingAverage( int length ) : base()
        {
            this.Length = length < _MinimumLength ? _MinimumLength : length;
        }

        /// <summary> The cloning constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> The value. </param>
        public MovingAverage( MovingAverage value ) : base( value )
        {
            this.Length = value is null ? _MinimumLength : value.Length < _MinimumLength ? _MinimumLength : value.Length;
        }

        /// <summary> Creates a new object that is a copy of the current instance. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> A new object that is a copy of this instance. </returns>
        public override object Clone()
        {
            return new MovingAverage( this );
        }

        #endregion

        #region " MOVING AVERAGE "

        /// <summary> Gets or sets the length of the moving average. </summary>
        /// <value> The length. </value>
        public int Length { get; set; }

        /// <summary> Adds a value. </summary>
        /// <remarks>
        /// This method was modified to ensure that the moving value is updated correctly. Test showed
        /// that when the first value was also an extremum value, the previous extremum was retained
        /// indicating that the update range method estimate used the previous values and then the new
        /// value was added. To this end, a new test was added with a large number of test to ensure that
        /// the fix works.
        /// </remarks>
        /// <param name="value"> The value. </param>
        public override void AddValue( double value )
        {
            base.AddValue( value );
            while ( this.Count > this.Length )
            {
                this.RemoveValueAt( 0 );
            }
        }

        #endregion

    }
}
