using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualBasic;

namespace isr.Core.Engineering.EnumerableStats
{

    /// <summary> A methods. </summary>
    /// <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para> </remarks>
    public static class EnumerableStatMethods
    {

        /// <summary> Computes the histogram. </summary>
        /// <remarks>
        /// The first bin at X=<paramref name="lowerBound"/> counts all the values below the lower limit;
        /// <para>
        /// The second bin at X=<paramref name="lowerBound"/> + half the bin width counts the values
        /// higher and equal to the lower limit but lower than the bin width;</para><para>
        /// The next to last bin at X=<paramref name="upperBound"/> - half the bin width counts the
        /// values at the last bin;</para><para>
        /// The last bin at <paramref name="upperBound"/> counts the number of values equal or higher
        /// than the high limit. </para>
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="source">     Source for the. </param>
        /// <param name="lowerBound"> The lower bound. </param>
        /// <param name="upperBound"> The upper bound. </param>
        /// <param name="binCount">   Number of bins. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process histogram direct in this collection.
        /// </returns>
        public static IList<System.Windows.Point> Histogram( this IList<double> source, double lowerBound, double upperBound, int binCount )
        {
            if ( source is null )
            {
                throw new ArgumentNullException( nameof( source ) );
            }

            double binWidth = (upperBound - lowerBound) / binCount;
            double inverseWidth = 1d / binWidth;
            int[] binCounts = ( int[] ) Array.CreateInstance( typeof( int ), binCount + 2 );
            foreach ( double x in source )
            {
                double binValue = inverseWidth * (x - lowerBound);
                int binNumber = binValue < 0d ? 0 : binValue >= binCount ? binCount + 1 : 1 + ( int ) Conversion.Fix( binValue );
                binCounts[binNumber] += 1;
            }

            var histF = new List<System.Windows.Point>();
            int i = 0;
            foreach ( int binValue in binCounts )
            {
                double x = i == 0 ? lowerBound : i == binCounts.Count() - 1 ? upperBound : lowerBound + binWidth * (i - 0.5d);
                histF.Add( new System.Windows.Point( x, binValue ) );
                i += 1;
            }

            return histF;
        }

        /// <summary> Computes the histogram. </summary>
        /// <remarks>
        /// The first bin at X=<paramref name="lowerBound"/> counts all the values below the lower limit;
        /// <para>
        /// The second bin at X=<paramref name="lowerBound"/> + half the bin width counts the values
        /// higher and equal to the lower limit but lower than the bin width;</para><para>
        /// The next to last bin at X=<paramref name="upperBound"/> - half the bin width counts the
        /// values at the last bin;</para><para>
        /// The last bin at <paramref name="upperBound"/> counts the number of values equal or higher
        /// than the high limit. </para>
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="source">     Source for the. </param>
        /// <param name="lowerBound"> The lower bound. </param>
        /// <param name="upperBound"> The upper bound. </param>
        /// <param name="binCount">   Number of bins. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process histogram direct in this collection.
        /// </returns>
        public static IList<System.Windows.Point> Histogram( this double[] source, double lowerBound, double upperBound, int binCount )
        {
            if ( source is null )
            {
                throw new ArgumentNullException( nameof( source ) );
            }

            double binWidth = (upperBound - lowerBound) / binCount;
            double inverseWidth = 1d / binWidth;
            int[] binCounts = ( int[] ) Array.CreateInstance( typeof( int ), binCount + 2 );
            foreach ( double x in source )
            {
                double binValue = inverseWidth * (x - lowerBound);
                int binNumber = binValue < 0d ? 0 : binValue >= binCount ? binCount + 1 : 1 + ( int ) Conversion.Fix( binValue );
                binCounts[binNumber] += 1;
            }

            var histF = new List<System.Windows.Point>();
            int i = 0;
            foreach ( int binValue in binCounts )
            {
                double x = i == 0 ? lowerBound : i == binCounts.Count() - 1 ? upperBound : lowerBound + binWidth * (i - 0.5d);
                histF.Add( new System.Windows.Point( x, binValue ) );
                i += 1;
            }

            return histF;
        }
    }
}
