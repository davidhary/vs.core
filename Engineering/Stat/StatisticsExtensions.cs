using System;

namespace isr.Core.Engineering.StatisticsExtensions
{
    /// <summary>   <see cref="StatisticsExtensions"/> methods. </summary>
    /// <remarks>   David, 2020-09-23. </remarks>
    public static class StatisticsExtensionMethods
    {

        /// <summary> Splits arrays values around the splitting value. </summary>
        /// <remarks>
        /// The function split implements the scanning action. The two scan pointers are used by the
        /// calling program To discover where they crossed.
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="a">              The array values to process. </param>
        /// <param name="splittingValue"> The splitting value. </param>
        /// <param name="leftScanIndex">  [in,out] the left scan index. </param>
        /// <param name="rightScanIndex"> [in,out] the right scan index. </param>
        public static void Split( this double[] a, double splittingValue, ref int leftScanIndex, ref int rightScanIndex )
        {
            if ( a is null )
            {
                throw new ArgumentNullException( nameof( a ) );
            }

            do
            {
                // do the left scan
                while ( a[leftScanIndex] < splittingValue )
                {
                    leftScanIndex += 1;
                }
                // do the right scan
                while ( a[rightScanIndex] > splittingValue )
                {
                    rightScanIndex -= 1;
                }
                // Once the two scans stop, swap values if they are in the wrong part:
                double t = a[leftScanIndex];
                a[leftScanIndex] = a[rightScanIndex];
                a[rightScanIndex] = t;
            }
            // scan until the pointers cross:
            while ( leftScanIndex < rightScanIndex );
        }

        /// <summary>
        /// Determines the median of the given values using a
        /// <see cref="StatisticsExtensions.StatisticsExtensionMethods.Split(double[], double, ref int, ref int)"/> function.
        /// </summary>
        /// <remarks>
        /// https://www.i-programmer.info/babbages-bag/505-quick-median.html?start=1 This method will (on
        /// average) find the median of n elements in a time proportional to 2n - which is much better
        /// than performing a full sort. Notice that this will only give you median if n is odd and it
        /// doesn't work at all if there are duplicate values in the array. If you are interested in
        /// finding percentiles other than the median then all you have to do is change the initial value
        /// of k. The value that you end up with in a[k] is always bigger than the number of element to
        /// its left and smaller than the number of elements to its right.
        /// For example, if you set k=n/3 then you will find a value that divides the array into
        /// 1/3rd, 2/3rds.
        /// Don't bother to use this algorithm unless the number of values is large. For small arrays
        /// almost any method will do and sorting is invariably simpler. Even when it isn't practical,
        /// this quick median method is always beautiful.
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values"> The array values to process. </param>
        /// <returns> The median value. </returns>
        public static int SplitMedian( this double[] values )
        {
            if ( values is null )
            {
                throw new ArgumentNullException( nameof( values ) );
            }

            int n = values.Length;
            if ( n == 1 )
            {
                return 0;
            }
            // This initially sets the scan to the whole of the array and x to the middle value. 
            int leftScanIndex = 0;
            int rightScanIndex = n - 1;
            int k = n / 2;
            int i;
            int j;
            while ( leftScanIndex < rightScanIndex )
            {
                double x = values[k];
                i = leftScanIndex;
                j = rightScanIndex;
                // It then calls split repeatedly on appropriate portions of the array 
                // until the two pointers meet in the middle of the array when the value in a[k] is the median.
                values.Split( x, ref i, ref j );
                if ( j <= k )
                {
                    leftScanIndex = i;
                }

                if ( i >= k )
                {
                    rightScanIndex = j;
                }
            }

            return k;
        }
    }
}
