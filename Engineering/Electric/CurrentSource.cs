using System;

namespace isr.Core.Engineering
{

    /// <summary> A current source. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-09-15 </para>
    /// </remarks>
    public class CurrentSource
    {

        #region " CONSTRUCTION "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="current">     The Current. </param>
        /// <param name="conductance"> The conductance. </param>
        public CurrentSource( double current, double conductance ) : base()
        {
            this.Initialize( current, conductance );
        }

        /// <summary> Cloning constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="currentSource"> The current source. </param>
        public CurrentSource( CurrentSource currentSource ) : this( ValidatedCurrentSource( currentSource ).Current, currentSource.Conductance )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="voltageSource"> The voltage source. </param>
        public CurrentSource( VoltageSource voltageSource ) : this( VoltageSource.ValidatedVoltageSource( voltageSource ).ToCurrentSource() )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="currentSource"> The current source. </param>
        public CurrentSource( AttenuatedCurrentSource currentSource ) : this( AttenuatedCurrentSource.ValidatedCurrentSource( currentSource ).ToCurrentSource() )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="voltageSource"> The voltage source. </param>
        public CurrentSource( AttenuatedVoltageSource voltageSource ) : this( AttenuatedVoltageSource.ValidatedVoltageSource( voltageSource ).ToCurrentSource() )
        {
        }

        /// <summary> Validated current source. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="currentSource"> The current source. </param>
        /// <returns> A CurrentSource. </returns>
        public static CurrentSource ValidatedCurrentSource( CurrentSource currentSource )
        {
            return currentSource is null ? throw new ArgumentNullException( nameof( currentSource ) ) : currentSource;
        }

        /// <summary> Initializes this object. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="current">     The Current. </param>
        /// <param name="conductance"> The conductance. </param>
        private void InitializeThis( double current, double conductance )
        {
            this.Current = current;
            this.Conductance = conductance;
        }

        /// <summary> Initializes this object. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="current">     The Current. </param>
        /// <param name="conductance"> The conductance. </param>
        public void Initialize( double current, double conductance )
        {
            this.InitializeThis( current, conductance );
        }

        /// <summary> Initializes this object. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="currentSource"> The current source. </param>
        public void Initialize( CurrentSource currentSource )
        {
            if ( currentSource is null )
            {
                throw new ArgumentNullException( nameof( currentSource ) );
            }

            this.InitializeThis( currentSource.Current, currentSource.Conductance );
        }

        #endregion

        #region " EQUALS "

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
        /// <see cref="T:System.Object" />. </param>
        /// <returns>
        /// <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />; otherwise, <c>False</c>.
        /// </returns>
        public override bool Equals( object obj )
        {
            return obj is object && ReferenceEquals( this.GetType(), obj.GetType() ) && this.Equals( ( CurrentSource ) obj );
        }

        /// <summary>
        /// Compares two Current Sources. The Current Sources are compared using their Conductances and
        /// Currents.
        /// </summary>
        /// <remarks>
        /// The Current Sources are the same if the have the same Current and Conductance.
        /// </remarks>
        /// <param name="other"> Specifies the other <see cref="CurrentSource">Current Source</see>
        /// to compare for equality with this instance. </param>
        /// <returns> A Boolean data type. </returns>
        public bool Equals( CurrentSource other )
        {
            return !(other is null) && this.Current.Equals( other.Current ) && this.Conductance.Equals( other.Conductance );
        }

        /// <summary>
        /// Compares two Current Sources. The Current Sources are compared using their Conductance and
        /// Current.
        /// </summary>
        /// <remarks>
        /// The Current Sources are the same if the have the same Current and Conductance.
        /// </remarks>
        /// <param name="other">     Specifies the other <see cref="CurrentSource">Current Source</see>
        /// to compare for equality with this instance. </param>
        /// <param name="tolerance"> Specifies the relative tolerance for comparing the two values. The
        /// values are compared based on their end points. </param>
        /// <returns> A Boolean data type. </returns>
        public bool Equals( CurrentSource other, double tolerance )
        {
            return !(other is null) && (this.Equals( other ) ||
                           Math.Abs( this.Current - other.Current ) <= other.Current * tolerance &&
                           Math.Abs( this.Conductance - other.Conductance ) <= other.Conductance * tolerance);
        }

        /// <summary> Implements the operator =. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool Equals( CurrentSource left, CurrentSource right )
        {
            return ReferenceEquals( left, right ) || left is object && left.Equals( right );
        }

        /// <summary>
        /// Compares two Current Sources. The Current Sources are compared using their Conductance and
        /// Current.
        /// </summary>
        /// <remarks>
        /// The Current Sources are the same if the have the same Current and Conductance.
        /// </remarks>
        /// <param name="left">      Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right">     Specifies the right hand side argument of the binary operation. </param>
        /// <param name="tolerance"> Specifies the relative tolerance for comparing the two values. The
        /// values are compared based on their end points. </param>
        /// <returns> A Boolean data type. </returns>
        public static bool Equals( CurrentSource left, CurrentSource right, double tolerance )
        {
            return left is null && right is null || left is object && left.Equals( right, tolerance );
        }

        /// <summary> Implements the operator =. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( CurrentSource left, CurrentSource right )
        {
            return Equals( left, right );
        }

        /// <summary> Implements the operator &lt;&gt;. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( CurrentSource left, CurrentSource right )
        {
            return !Equals( left, right );
        }

        /// <summary> Creates a unique hash code. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> An <see cref="System.Int32">Int32</see> value. </returns>
        public override int GetHashCode()
        {
            return this.Current.GetHashCode() ^ this.Conductance.GetHashCode();
        }

        #endregion

        #region " TO STRING "

        /// <summary> Returns a string that represents the current object. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> A string that represents the current object. </returns>
        public override string ToString()
        {
            return $"{this.Current}:{this.Conductance}";
        }

        /// <summary> Returns a string that represents the current object. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="currentFormat">     The Current format. </param>
        /// <param name="conductanceFormat"> The conductance format. </param>
        /// <returns> A string that represents the current object. </returns>
        public string ToString( string currentFormat, string conductanceFormat )
        {
            return $"{string.Format( currentFormat, this.Current )}:{string.Format( conductanceFormat, this.Conductance )}";
        }

        #endregion

        #region " COMPONENETS "

        /// <summary> Gets or sets the current source Current. This is the current into a short. </summary>
        /// <value> The Current. </value>
        public double Current { get; set; }

        /// <summary> Gets or sets the current source equivalent conductance. </summary>
        /// <value> The equivalent conductance. </value>
        public double Conductance { get; set; }

        #endregion

        #region " OUTPUT "

        /// <summary> Load current. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="loadConductance"> The load conductance. </param>
        /// <returns> A Double. </returns>
        public double LoadCurrent( double loadConductance )
        {
            if ( loadConductance == 0d )
            {
                return 0d;
            }
            else if ( double.IsInfinity( loadConductance ) )
            {
                return this.Current;
            }

            return this.Current * loadConductance / (loadConductance + this.Conductance);
        }

        /// <summary> Returns the load voltage. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="loadResistance"> The load resistance. </param>
        /// <returns> The voltage. </returns>
        public double LoadVoltage( double loadResistance )
        {
            return loadResistance == 0d
                ? 0d
                : double.IsInfinity( loadResistance )
                    ? this.Current / this.Conductance
                    : loadResistance * this.LoadCurrent( 1d / loadResistance );
        }

        #endregion

        #region " VOLTAGE SOURCE CONVERTERS "

        /// <summary> Converts this object to a voltage source. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="current">     The Current. </param>
        /// <param name="conductance"> The resistance. </param>
        /// <returns> This object as a VoltageSource. </returns>
        public static VoltageSource ToVoltageSource( double current, double conductance )
        {
            double resistance = 1d / conductance;
            return new VoltageSource( current * resistance, resistance );
        }

        /// <summary> Initializes this object from the given from voltage source. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="voltage">    The voltage. </param>
        /// <param name="resistance"> The resistance. </param>
        /// <returns> A CurrentSource. </returns>
        public static CurrentSource FromVoltageSource( double voltage, double resistance )
        {
            double conductance = 1d / resistance;
            return new CurrentSource( voltage * conductance, conductance );
        }

        /// <summary> Converts this object to a voltage source. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> This object as a VoltageSource. </returns>
        public VoltageSource ToVoltageSource()
        {
            return ToVoltageSource( this.Current, this.Conductance );
        }

        /// <summary> Initializes this object from the given from voltage source. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="voltageSource"> The voltage source. </param>
        public void FromVoltageSource( VoltageSource voltageSource )
        {
            if ( voltageSource is null )
            {
                throw new ArgumentNullException( nameof( voltageSource ) );
            }

            this.Conductance = 1d / voltageSource.Resistance;
            this.Current = voltageSource.Voltage * this.Conductance;
        }

        #endregion

        #region " ATTENUATED CURRENT SOURCE CONVERTERS "

        /// <summary> Converts this object to an Attenuated current source. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="currentSource"> The current source. </param>
        /// <param name="attenuation">   The attenuation. </param>
        /// <returns> The given data converted to an AttenuatedCurrentSource. </returns>
        public static AttenuatedCurrentSource ToAttenuatedCurrentSource( CurrentSource currentSource, double attenuation )
        {
            return currentSource is null
                ? throw new ArgumentNullException( nameof( currentSource ) )
                : attenuation < AttenuatedVoltageSource.MinimumAttenuation
                ? throw new ArgumentOutOfRangeException( nameof( attenuation ), $"Value must be greater or equal to {AttenuatedCurrentSource.MinimumAttenuation}" )
                : currentSource.ToAttenuatedCurrentSource( attenuation );
        }

        /// <summary> Converts this object to an Attenuated current source. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="attenuation"> The attenuation. </param>
        /// <returns> The given data converted to an AttenuatedCurrentSource. </returns>
        public AttenuatedCurrentSource ToAttenuatedCurrentSource( double attenuation )
        {
            if ( attenuation < AttenuatedCurrentSource.MinimumAttenuation )
            {
                throw new ArgumentOutOfRangeException( nameof( attenuation ), $"Value must be greater or equal to {AttenuatedCurrentSource.MinimumAttenuation}" );
            }

            double conductance = this.Conductance * attenuation;
            double resistance = 0d;
            if ( attenuation > AttenuatedCurrentSource.MinimumAttenuation )
            {
                resistance = 1d / this.Conductance - 1d / conductance;
            }

            return new AttenuatedCurrentSource( this.Current, resistance, conductance );
        }

        /// <summary> Converts this object to an Attenuated current source. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="nominalCurrent"> The nominal current. </param>
        /// <returns> The given data converted to an AttenuatedCurrentSource. </returns>
        public AttenuatedCurrentSource ToAttenuatedCurrentSource( decimal nominalCurrent )
        {
            return this.ToAttenuatedCurrentSource( ( double ) nominalCurrent / this.Current );
        }

        /// <summary> Converts this object to an Attenuated current source. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="currentSource">  The current source. </param>
        /// <param name="nominalCurrent"> The nominal current. </param>
        /// <returns> The given data converted to an AttenuatedCurrentSource. </returns>
        public static AttenuatedCurrentSource ToAttenuatedCurrentSource( CurrentSource currentSource, decimal nominalCurrent )
        {
            return currentSource is null
                ? throw new ArgumentNullException( nameof( currentSource ) )
                : currentSource.ToAttenuatedCurrentSource( nominalCurrent );
        }

        #endregion

    }
}
