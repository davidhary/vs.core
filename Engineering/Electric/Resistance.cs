using System;

namespace isr.Core.Engineering
{

    /// <summary> A resistance implementation of Ohms law. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-06-28 </para>
    /// </remarks>
    public class Resistance
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        public Resistance() : base()
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="resistance"> The resistance or <see cref="Double.NaN"/> if not
        /// <see cref="HasValue"/>. </param>
        public Resistance( Resistance resistance ) : this( Validated( resistance ).Voltage, resistance.Current )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="voltage"> The voltage. </param>
        /// <param name="current"> The current. </param>
        public Resistance( double voltage, double current ) : this()
        {
            this.Current = current;
            this.Voltage = voltage;
        }

        /// <summary> Validated the given resistance. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="resistance"> The resistance or <see cref="Double.NaN"/> if not
        /// <see cref="HasValue"/>. </param>
        /// <returns> A Resistance. </returns>
        public static Resistance Validated( Resistance resistance )
        {
            return resistance is null ? throw new ArgumentNullException( nameof( resistance ) ) : resistance;
        }

        /// <summary> Gets the sentinel indicating if the measure has a non zero current value. </summary>
        /// <value> The sentinel indicating if the measure has a non zero current value. </value>
        public bool HasValue => this.Current != 0d && (this.Voltage == 0d || Math.Sign( this.Voltage ) == Math.Sign( this.Current ));

        /// <summary> Gets the resistance. </summary>
        /// <value> The resistance or <see cref="Double.NaN"/> if not <see cref="HasValue"/>. </value>
        public double ResistanceValue => this.HasValue ? this.Voltage / this.Current : double.NaN;

        /// <summary> Gets or sets the voltage. </summary>
        /// <value> The voltage. </value>
        public double Voltage { get; set; }

        /// <summary> Gets or sets the current. </summary>
        /// <value> The current. </value>
        public double Current { get; set; }

        /// <summary> Query if 'value' is hit compliance. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="value">           True to value. </param>
        /// <param name="limit">           The source limit. </param>
        /// <param name="complianceLimit"> The compliance limit. </param>
        /// <returns> <c>true</c> if hit compliance; otherwise <c>false</c> </returns>
        public static bool IsHitCompliance( double value, double limit, double complianceLimit )
        {
            value = Math.Abs( value );
            return complianceLimit <= 0d
                ? throw new ArgumentException( $"N{nameof( complianceLimit )}={complianceLimit} must be positive" )
                : limit == 0d ? Math.Abs( value ) < complianceLimit : Math.Abs( value / limit - 1d ) < complianceLimit;
        }

        /// <summary> Query if <see cref="Voltage"/> 'value' is hit compliance. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="limit">           The source limit. </param>
        /// <param name="complianceLimit"> The compliance limit. </param>
        /// <returns> <c>true</c> if hit compliance; otherwise <c>false</c> </returns>
        public bool IsHitVoltageCompliance( double limit, double complianceLimit )
        {
            return IsHitCompliance( this.Voltage, limit, complianceLimit );
        }

        /// <summary> Query if 'limit' is hit current compliance. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="limit">           The source limit. </param>
        /// <param name="complianceLimit"> The compliance limit. </param>
        /// <returns> <c>true</c> if hit current compliance; otherwise <c>false</c> </returns>
        public bool IsHitCurrentCompliance( double limit, double complianceLimit )
        {
            return IsHitCompliance( this.Current, limit, complianceLimit );
        }
    }
}
