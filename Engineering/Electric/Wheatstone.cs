using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace isr.Core.Engineering
{

    /// <summary> A Wheatstone bridge. </summary>
    /// <remarks>
    /// Without lose of generality, the Wheatstone bridge is laid out using 4 nodes and 4 edges
    /// arranged in two, left and right arms. Also, the bridge is drawn as a square standing on one
    /// node. The bridge voltage is applied at the top (+) and bottom (-) nodes. The bridge output is
    /// measured at the left (+) and right (-) nodes. <para>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-09-14 </para>
    /// </remarks>
    public class Wheatstone
    {

        #region " CONSTRUCTION "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="topRight">              The top right  resistance value. </param>
        /// <param name="bottomRight">           The bottom right  resistance value. </param>
        /// <param name="bottomLeft">            The bottom left  resistance value. </param>
        /// <param name="topLeft">               The top left  resistance value. </param>
        /// <param name="relativeOffsetEpsilon"> The relative offset epsilon. </param>
        public Wheatstone( double topRight, double bottomRight, double bottomLeft, double topLeft, double relativeOffsetEpsilon ) : base()
        {
            this.Initialize( topRight, bottomRight, bottomLeft, topLeft, relativeOffsetEpsilon );
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="topRight">    The top right  resistance value. </param>
        /// <param name="bottomRight"> The bottom right  resistance value. </param>
        /// <param name="bottomLeft">  The bottom left  resistance value. </param>
        /// <param name="topLeft">     The top left  resistance value. </param>
        public Wheatstone( double topRight, double bottomRight, double bottomLeft, double topLeft ) : this( topRight, bottomRight, bottomLeft, topLeft, DefaultOutputEpsilon )
        {
        }

        /// <summary> Cloning Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="bridge"> The bridge. </param>
        public Wheatstone( Wheatstone bridge ) : this( ValidatedBridge( bridge ).TopRight, ValidatedBridge( bridge ).BottomRight, ValidatedBridge( bridge ).BottomLeft, ValidatedBridge( bridge ).TopLeft, ValidatedBridge( bridge ).OutputEpsilon )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-07-04. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="elements">              The elements. </param>
        /// <param name="layout">                The bridge layout. </param>
        /// <param name="relativeOffsetEpsilon"> The relative offset epsilon. </param>
        public Wheatstone( IEnumerable<double> elements, WheatstoneLayout layout, double relativeOffsetEpsilon ) : base()
        {
            if ( layout == WheatstoneLayout.Clockwise )
            {
                this.Initialize( elements.ElementAtOrDefault( 0 ), elements.ElementAtOrDefault( 1 ), elements.ElementAtOrDefault( 2 ), elements.ElementAtOrDefault( 3 ), relativeOffsetEpsilon );
            }
            else if ( layout == WheatstoneLayout.Counterclockwise )
            {
                this.Initialize( elements.ElementAtOrDefault( 3 ), elements.ElementAtOrDefault( 2 ), elements.ElementAtOrDefault( 1 ), elements.ElementAtOrDefault( 0 ), relativeOffsetEpsilon );
            }
            else if ( layout == WheatstoneLayout.LeftRightTopBottom )
            {
                this.Initialize( elements.ElementAtOrDefault( 1 ), elements.ElementAtOrDefault( 3 ), elements.ElementAtOrDefault( 2 ), elements.ElementAtOrDefault( 0 ), relativeOffsetEpsilon );
            }
            else
            {
                throw new ArgumentOutOfRangeException( nameof( layout ) );
            }
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="elements"> The elements. </param>
        /// <param name="layout">   The bridge layout. </param>
        public Wheatstone( IEnumerable<double> elements, WheatstoneLayout layout ) : this( elements, layout, DefaultOutputEpsilon )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> The value. </param>
        public Wheatstone( double value ) : this( value, value, value, value )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value">          The value. </param>
        /// <param name="relativeOffset"> The relative output offset. </param>
        public Wheatstone( double value, double relativeOffset ) : this( value * (1d + relativeOffset), value * (1d - relativeOffset), value * (1d + relativeOffset), value * (1d - relativeOffset) )
        {
        }

        /// <summary> Validated bridge. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="bridge"> The bridge. </param>
        /// <returns> A Wheatstone. </returns>
        public static Wheatstone ValidatedBridge( Wheatstone bridge )
        {
            return bridge is null ? throw new ArgumentNullException( nameof( bridge ) ) : bridge;
        }

        #endregion

        #region " EQUALS "

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
        /// <see cref="T:System.Object" />. </param>
        /// <returns>
        /// <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />; otherwise, <c>False</c>.
        /// </returns>
        public override bool Equals( object obj )
        {
            return this.Equals( obj as Wheatstone );
        }

        /// <summary>
        /// Compares two bridges. The bridges are compared using their LowerLimits and UpperLimits.
        /// </summary>
        /// <remarks>
        /// The two bridges are the same if the have the same minimum and maximum values.
        /// </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> A Boolean data type. </returns>
        public bool Equals( double value )
        {
            return this.TopRight == value && this.BottomRight == value && this.BottomLeft == value && this.TopLeft == value;
        }

        /// <summary>
        /// Compares two bridges. The bridges are compared using their LowerLimits and UpperLimits.
        /// </summary>
        /// <remarks>
        /// The two bridges are the same if the have the same minimum and maximum values.
        /// </remarks>
        /// <param name="other"> Specifies the other <see cref="Wheatstone">Wheatstone</see>
        /// to compare for equality with this instance. </param>
        /// <returns> A Boolean data type. </returns>
        public bool Equals( Wheatstone other )
        {
            return other is object && this.TopRight == other.TopRight && this.BottomRight == other.BottomRight && this.BottomLeft == other.BottomLeft && this.TopLeft == other.TopLeft;
        }

        /// <summary>
        /// Compares two bridges. The bridges are compared using their LowerLimits and UpperLimits.
        /// </summary>
        /// <remarks>
        /// The two bridges are the same if the have the same minimum and maximum values.
        /// </remarks>
        /// <param name="other">     Specifies the other <see cref="Wheatstone">Wheatstone</see>
        /// to compare for equality with this instance. </param>
        /// <param name="tolerance"> Specifies the relative tolerance for comparing the two values. The
        /// values are compared based on their end points. </param>
        /// <returns> A Boolean data type. </returns>
        public bool Equals( Wheatstone other, double tolerance )
        {
            return !(other is null) && (this.Equals( other )
                || Math.Abs( other.TopRight - this.TopRight ) <= 0.5d * tolerance * (other.TopRight + this.TopRight) && Math.Abs( other.BottomRight - this.BottomRight ) <= 0.5d * tolerance * (other.BottomRight + this.BottomRight) && Math.Abs( other.BottomLeft - this.BottomLeft ) <= 0.5d * tolerance * (other.BottomLeft + this.BottomLeft) && Math.Abs( other.TopLeft - this.TopLeft ) <= 0.5d * tolerance * (other.TopLeft + this.TopLeft));
        }

        /// <summary> Implements the operator =. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool Equals( Wheatstone left, Wheatstone right )
        {
            return ReferenceEquals( left, right ) || left is object && left.Equals( right );
        }

        /// <summary>
        /// Compares two bridges. The bridges are compared using their LowerLimits and UpperLimits.
        /// </summary>
        /// <remarks>
        /// The two bridges are the same if the have the same minimum and maximum values.
        /// </remarks>
        /// <param name="left">      Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right">     Specifies the right hand side argument of the binary operation. </param>
        /// <param name="tolerance"> Specifies the relative tolerance for comparing the two values. The
        /// values are compared based on their end points. </param>
        /// <returns> A Boolean data type. </returns>
        public static bool Equals( Wheatstone left, Wheatstone right, double tolerance )
        {
            return left is null && right is null || left is object && left.Equals( right, tolerance );
        }

        /// <summary> Implements the operator =. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( Wheatstone left, Wheatstone right )
        {
            return Equals( left, right );
        }

        /// <summary> Implements the operator &lt;&gt;. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( Wheatstone left, Wheatstone right )
        {
            return !Equals( left, right );
        }

        /// <summary> Creates a unique hash code. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> An <see cref="System.Int32">Int32</see> value. </returns>
        public override int GetHashCode()
        {
            return this.TopRight.GetHashCode() ^ this.BottomRight.GetHashCode() ^ this.BottomLeft.GetHashCode() ^ this.TopLeft.GetHashCode();
        }

        #endregion

        #region " BRIDGE ELEMENTS "

        /// <summary> Gets or sets the top right resistance value. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The top right  resistance value. </value>
        public double TopRight { get; private set; }

        /// <summary> Gets or sets the bottom right resistance value. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The bottom right  resistance value. </value>
        public double BottomRight { get; private set; }

        /// <summary> Gets or sets the bottom left resistance value. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The bottom left  resistance value. </value>
        public double BottomLeft { get; private set; }

        /// <summary> Gets or sets the top left resistance value. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The top left  resistance value. </value>
        public double TopLeft { get; private set; }

        /// <summary> Select edge. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="edge"> The edge. </param>
        /// <returns> A Double. </returns>
        public double SelectEdge( WheatstoneEdge edge )
        {
            double value = 0d;
            switch ( edge )
            {
                case WheatstoneEdge.TopLeft:
                    {
                        value = this.TopLeft;
                        break;
                    }

                case WheatstoneEdge.TopRight:
                    {
                        value = this.TopRight;
                        break;
                    }

                case WheatstoneEdge.BottomLeft:
                    {
                        value = this.BottomLeft;
                        break;
                    }

                case WheatstoneEdge.BottomRight:
                    {
                        value = this.BottomRight;
                        break;
                    }
            }

            return value;
        }

        /// <summary> Invalid element. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        private static bool InvalidElement( double value )
        {
            return Resistor.IsOpen( value ) || Resistor.IsShort( value );
        }

        /// <summary> Determines if we can invalid bridge. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        private bool InvalidBridge()
        {
            return InvalidElement( this.TopRight ) || InvalidElement( this.BottomRight ) || InvalidElement( this.BottomLeft ) || InvalidElement( this.TopLeft );
        }

        /// <summary> Query if the bridge is valid. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> <c>true</c> if valid; otherwise <c>false</c> </returns>
        public virtual bool IsValid()
        {
            return !this.InvalidBridge();
        }

        /// <summary> Initializes this object. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        private void Initialize()
        {
            this._Output = this.BottomLeft / (this.TopLeft + this.BottomLeft) - this.BottomRight / (this.TopRight + this.BottomRight);
            this.IsOutputBalanced = Math.Abs( this.Output() ) <= this._OutputEpsilon;
            double topRightBottomLeft = this.TopRight * this.BottomLeft;
            double topLeftBottomRight = this.TopLeft * this.BottomRight;
            this.Balance = topRightBottomLeft / topLeftBottomRight;
            this.BalanceDeviation = this.Balance - 1d;
            this.IsBalanced = Math.Abs( this.BalanceDeviation ) <= this._BalanceDeviationEpsilon;
            this.ProductImbalance = topRightBottomLeft - topLeftBottomRight;
            this.BridgeResistance = (this.TopLeft + this.BottomLeft) * (this.TopRight + this.BottomRight) / (this.TopLeft + this.BottomLeft + this.TopRight + this.BottomRight);
        }

        /// <summary> Initializes this object. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="topRight">              The top right  resistance value. </param>
        /// <param name="bottomRight">           The bottom right  resistance value. </param>
        /// <param name="bottomLeft">            The bottom left  resistance value. </param>
        /// <param name="topLeft">               The top left  resistance value. </param>
        /// <param name="relativeOffsetEpsilon"> The relative offset epsilon. </param>
        private void Initialize( double topRight, double bottomRight, double bottomLeft, double topLeft, double relativeOffsetEpsilon )
        {
            this.TopRight = topRight;
            this.BottomRight = bottomRight;
            this.BottomLeft = bottomLeft;
            this.TopLeft = topLeft;
            this._OutputEpsilon = relativeOffsetEpsilon;
            this._BalanceDeviationEpsilon = 4d * relativeOffsetEpsilon;
            this.Initialize();
        }

        /// <summary> Initializes this object. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="bridge"> The bridge. </param>
        public void Initialize( Wheatstone bridge )
        {
            if ( bridge is null )
            {
                throw new ArgumentNullException( nameof( bridge ) );
            }

            this.Initialize( bridge.TopRight, bridge.BottomRight, bridge.BottomLeft, bridge.TopLeft, bridge.OutputEpsilon );
        }

        #endregion

        #region " BRIDGE OUTPUT "

        /// <summary> Gets the bridge output for the specified bridge voltage. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="bridgeVoltage"> The bridge voltage. </param>
        /// <returns> A Double. </returns>
        public double Output( double bridgeVoltage )
        {
            return bridgeVoltage * this.Output();
        }

        /// <summary> Gets the bridge output for the specified bridge voltage. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="voltageSource"> The voltage source. </param>
        /// <returns> A Double. </returns>
        public double Output( VoltageSource voltageSource )
        {
            return voltageSource is null
                ? throw new ArgumentNullException( nameof( voltageSource ) )
                : this.Output( this.BridgeVoltage( voltageSource ) );
        }

        /// <summary> Gets the bridge output for the specified bridge voltage. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="currentSource"> The current source. </param>
        /// <returns> A Double. </returns>
        public double Output( CurrentSource currentSource )
        {
            return currentSource is null
                ? throw new ArgumentNullException( nameof( currentSource ) )
                : this.Output( this.BridgeVoltage( currentSource ) );
        }

        /// <summary> The output. </summary>
        private double _Output;

        /// <summary> Gets the bridge output for a unity input. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> A Double. </returns>
        public double Output()
        {
            return this._Output;
        }

        /// <summary> Gets or sets the default epsilon for assessing the bridge output. </summary>
        /// <value> The default relative offset epsilon. </value>
        public static double DefaultOutputEpsilon { get; set; } = 0.0001d;

        /// <summary> The output epsilon. </summary>
        private double _OutputEpsilon;

        /// <summary> Gets or sets the unity output epsilon. </summary>
        /// <value> The unity output  epsilon. </value>
        public double OutputEpsilon
        {
            get => this._OutputEpsilon;

            set {
                if ( value != this.OutputEpsilon )
                {
                    this._OutputEpsilon = value;
                    this.IsOutputBalanced = Math.Abs( this.Output() ) <= this.OutputEpsilon;
                }
            }
        }

        /// <summary>
        /// Returns true if the absolute bridge <see cref="Engineering.Wheatstone.Output()"/> is lower than the
        /// <see cref="OutputEpsilon"/>.
        /// </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> <c>true</c> if balanced; otherwise <c>false</c> </value>
        public bool IsOutputBalanced { get; private set; }

        #endregion

        #region " BRIDGE VOLTAGE "

        /// <summary> Bridge voltage. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="voltageSource"> The voltage source. </param>
        /// <returns> A Double. </returns>
        public double BridgeVoltage( VoltageSource voltageSource )
        {
            return voltageSource is null
                ? throw new ArgumentNullException( nameof( voltageSource ) )
                : voltageSource.LoadVoltage( this.BridgeResistance );
        }

        /// <summary> Bridge voltage. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="currentSource"> The current source. </param>
        /// <returns> A Double. </returns>
        public double BridgeVoltage( CurrentSource currentSource )
        {
            return currentSource is null
                ? throw new ArgumentNullException( nameof( currentSource ) )
                : currentSource.LoadVoltage( this.BridgeResistance );
        }

        /// <summary> Gets the bridge resistance as see from voltage or current source. </summary>
        /// <value> The bridge resistance. </value>
        public double BridgeResistance { get; private set; }

        #endregion

        #region " BRIDGE IMBALANCE "

        /// <summary> Gets the bridge product imbalance. </summary>
        /// <remarks>
        /// This is the difference between the product of the increasing values (top right and bottom
        /// left) and decreasing value (top left and bottom right).
        /// </remarks>
        /// <value> A Double. </value>
        public double ProductImbalance { get; private set; }

        /// <summary> Gets a measure of the bridge balance. </summary>
        /// <remarks>
        /// The ratio of the Top-Right Left-Bottom diagonal product over the Top-Left Bottom-Right
        /// product. The <see cref="Balance"/> is one if the bridge is balance. When greater than one,
        /// the bridge is said to be positively imbalanced.
        /// </remarks>
        /// <value> The relative imbalance. </value>
        public double Balance { get; private set; }

        /// <summary> Gets the balance deviation. </summary>
        /// <remarks>
        /// This is a measure of the bridge deviation from balance. When zero, the bridge is said to be
        /// balanced. When positive, the bridge is positively imbalanced, which means that its output is
        /// also positive.
        /// </remarks>
        /// <value> The relative deviation. </value>
        public double BalanceDeviation { get; private set; }

        /// <summary> Gets the absolute balance deviation. </summary>
        /// <value> The absolute balance deviation. </value>
        public double AbsoluteBalanceDeviation => Math.Abs( this.BalanceDeviation );

        /// <summary> The balance deviation epsilon. </summary>
        private double _BalanceDeviationEpsilon;

        /// <summary> Gets or sets the balance deviation epsilon. </summary>
        /// <remarks>
        /// the <see cref="BalanceDeviationEpsilon"/> is four times the <see cref="OutputEpsilon"/>
        /// </remarks>
        /// <value> The balance deviation epsilon. </value>
        public double BalanceDeviationEpsilon
        {
            get => this._BalanceDeviationEpsilon;

            set {
                if ( value != this.BalanceDeviationEpsilon || this.IsBalanced != this.CheckBalanced( value ) )
                {
                    this._BalanceDeviationEpsilon = value;
                    this.IsBalanced = this.CheckBalanced( value );
                }
            }
        }

        /// <summary> Check balanced. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="epsilon"> The epsilon. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        private bool CheckBalanced( double epsilon )
        {
            return Math.Abs( this.BalanceDeviation ) <= epsilon;
        }

        /// <summary>
        /// Returns true if the absolute bridge <see cref="BalanceDeviation"/> is lower than the
        /// <see cref="BalanceDeviationEpsilon"/>.
        /// </summary>
        /// <value> <c>true</c> if balanced; otherwise <c>false</c> </value>
        public bool IsBalanced { get; private set; }

        #endregion

        #region " TO COMMA SEPARATED STRING "

        /// <summary> Returns a comma-separated values string. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="values"> The values. </param>
        /// <param name="format"> Describes the format to use. </param>
        /// <returns> The given data converted to a String. </returns>
        private static string ToCommaSeparatedString( IEnumerable<double> values, string format )
        {
            var builder = new System.Text.StringBuilder();
            for ( int i = 0, loopTo = values.Count() - 1; i <= loopTo; i++ )
            {
                _ = builder.Append( $"{values.ElementAtOrDefault( i ).ToString( format )}" );
                if ( i < values.Count() - 1 )
                {
                    _ = builder.Append( "," );
                }
            }

            return builder.ToString();
        }

        /// <summary> Returns a comma-separated values string. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="format"> Describes the format to use. </param>
        /// <returns> The given data converted to a String. </returns>
        public virtual string ToCommaSeparatedString( string format )
        {
            return ToCommaSeparatedString( new double[] { this.TopRight, this.BottomRight, this.BottomLeft, this.TopLeft }, format );
        }

        /// <summary> Returns a comma-separated values string. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> The given data converted to a String. </returns>
        public virtual string ToCommaSeparatedString()
        {
            return this.ToCommaSeparatedString( "G6" );
        }

        /// <summary> Returns a comma-separated header string. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="values"> The values. </param>
        /// <returns> The given data converted to a String. </returns>
        private static string ToCommaSeparatedString( IEnumerable<WheatstoneEdge> values )
        {
            var builder = new System.Text.StringBuilder();
            for ( int i = 0, loopTo = values.Count() - 1; i <= loopTo; i++ )
            {
                _ = builder.Append( $"{values.ElementAtOrDefault( i )}" );
                if ( i < values.Count() - 1 )
                {
                    _ = builder.Append( "," );
                }
            }

            return builder.ToString();
        }

        /// <summary> Comma separated header. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> A String. </returns>
        public static string ToCommaSeparatedHeader()
        {
            return ToCommaSeparatedString( new WheatstoneEdge[] { WheatstoneEdge.TopRight, WheatstoneEdge.BottomRight, WheatstoneEdge.BottomLeft, WheatstoneEdge.TopLeft } );
        }

        #endregion

        #region " TO STRING "

        /// <summary> Gets a string representation of the edges. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="values"> The values. </param>
        /// <returns> A String that represents this object. </returns>
        public static string ToString( IEnumerable<WheatstoneEdge> values )
        {
            var builder = new System.Text.StringBuilder( "[" );
            _ = builder.Append( ToCommaSeparatedString( values ) );
            _ = builder.Append( "]" );
            return builder.ToString();
        }

        /// <summary> Convert this object into a string representation. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="values"> The values. </param>
        /// <param name="format"> Describes the format to use. </param>
        /// <returns> A String that represents this object. </returns>
        public static string ToString( IEnumerable<double> values, string format )
        {
            var builder = new System.Text.StringBuilder( "[" );
            _ = builder.Append( ToCommaSeparatedString( values, format ) );
            _ = builder.Append( "]" );
            return builder.ToString();
        }

        /// <summary> Gets a string representation of the edges. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="format"> Describes the format to use. </param>
        /// <returns> A String that represents this object. </returns>
        public string ToString( string format )
        {
            return ToString( new double[] { this.TopRight, this.BottomRight, this.BottomLeft, this.TopLeft }, format );
        }

        /// <summary> Convert this object into a string representation. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> A String that represents this object. </returns>
        public override string ToString()
        {
            return this.ToString( "G6" );
        }

        #endregion

    }

    /// <summary> A collection of Wheatstone bridges. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-09-25 </para>
    /// </remarks>
    public class WheatstoneCollection : System.Collections.ObjectModel.Collection<Wheatstone>
    {

        /// <summary> Total Imbalance squared. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> A Double. </returns>
        public double TotalImbalanceSquared()
        {
            double result = 0d;
            foreach ( Wheatstone bridge in this )
            {
                double x = bridge.ProductImbalance;
                result += x * x;
            }

            return result;
        }
    }

    /// <summary> Values that represent Wheatstone bridge layout. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    public enum WheatstoneLayout
    {

        /// <summary> An enum constant representing the clockwise option starting at the top right position. </summary>
        [Description( "Clockwise" )]
        Clockwise,

        /// <summary> An enum constant representing the counterclockwise option starting at the top left position. </summary>
        [Description( "Counter Clockwise" )]
        Counterclockwise,

        /// <summary> An enum constant representing the left right top bottom option starting at the top left position. </summary>
        [Description( "Left Right Top Bottom" )]
        LeftRightTopBottom
    }

    /// <summary> Values that represent Wheatstone edges. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    public enum WheatstoneEdge
    {

        /// <summary> An enum constant representing the top right option. </summary>
        [Description( "Top Right" )]
        TopRight,

        /// <summary> An enum constant representing the bottom right option. </summary>
        [Description( "Bottom Right" )]
        BottomRight,

        /// <summary> An enum constant representing the bottom left option. </summary>
        [Description( "Bottom Left" )]
        BottomLeft,

        /// <summary> An enum constant representing the top left option. </summary>
        [Description( "Top Left" )]
        TopLeft
    }
}
