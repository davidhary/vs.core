using System;

namespace isr.Core.Engineering
{

    /// <summary> A conductor. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-09-16 </para>
    /// </remarks>
    public sealed class Conductor
    {

        #region " CONSTRUCTION "

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        private Conductor() : base()
        {
        }

        #endregion

        #region " CONDUCTOR PAIR "

        /// <summary> Returns the equivalent conductance of two conductors in parallel. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="leftValue">  The left value. </param>
        /// <param name="rightValue"> The right value. </param>
        /// <returns> A Double. </returns>
        public static double Parallel( double leftValue, double rightValue )
        {
            return leftValue + rightValue;
        }

        /// <summary> Returns the equivalent conductance of two conductors in series. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="leftValue">  The left value. </param>
        /// <param name="rightValue"> The right value. </param>
        /// <returns> A Double. </returns>
        public static double Series( double leftValue, double rightValue )
        {
            return leftValue * rightValue / (leftValue + rightValue);
        }

        /// <summary> Returns the relative current through the right parallel conductance. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="leftValue">  The left value. </param>
        /// <param name="rightValue"> The right value. </param>
        /// <returns> A Double. </returns>
        public static double OutputCurrent( double leftValue, double rightValue )
        {
            return rightValue / (leftValue + rightValue);
        }

        /// <summary> Returns the relative voltage across the right series conductance. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="leftValue">  The left value. </param>
        /// <param name="rightValue"> The right value. </param>
        /// <returns> A Double. </returns>
        public static double OutputVoltage( double leftValue, double rightValue )
        {
            return leftValue / (leftValue + rightValue);
        }

        #endregion

        #region " PAIR OF CONDUCTORS: WITH VALIDATIONS "

        /// <summary> Gets or sets the open conductance. </summary>
        /// <value> The open conductance. </value>
        public static double OpenConductance { get; set; } = 0d;

        /// <summary> Gets or sets the short conductance. </summary>
        /// <value> The short conductance. </value>
        public static double ShortConductance { get; set; } = double.PositiveInfinity;

        /// <summary> Query if 'value' is open. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> <c>true</c> if open; otherwise <c>false</c> </returns>
        public static bool IsOpen( double value )
        {
            return value == OpenConductance;
        }

        /// <summary> Query if 'value' is short. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> <c>true</c> if short; otherwise <c>false</c> </returns>
        public static bool IsShort( double value )
        {
            return value == ShortConductance;
        }

        /// <summary> Returns the equivalent conductance of two parallel conductors. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="leftValue">  The left value. </param>
        /// <param name="rightValue"> The right value. </param>
        /// <returns> A Double. </returns>
        public static double ToParallel( double leftValue, double rightValue )
        {
            return leftValue < OpenConductance
                ? throw new ArgumentOutOfRangeException( nameof( leftValue ), "value must be non-negative" )
                : rightValue < OpenConductance
                ? throw new ArgumentOutOfRangeException( nameof( rightValue ), "value must be non-negative" )
                : IsOpen( leftValue )
                ? rightValue
                : IsOpen( rightValue )
                    ? leftValue
                    : IsShort( leftValue ) || IsShort( rightValue ) ? ShortConductance : Parallel( leftValue, rightValue );
        }

        /// <summary> returns the equivalent series conductance of two conductors. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="leftValue">  The left value. </param>
        /// <param name="rightValue"> The right value. </param>
        /// <returns> A Double. </returns>
        public static double ToSeries( double leftValue, double rightValue )
        {
            return leftValue < OpenConductance
                ? throw new ArgumentOutOfRangeException( nameof( leftValue ), "value must be non-negative" )
                : rightValue < OpenConductance
                ? throw new ArgumentOutOfRangeException( nameof( rightValue ), "value must be non-negative" )
                : IsShort( leftValue )
                ? rightValue
                : IsShort( rightValue )
                    ? leftValue
                    : IsOpen( leftValue ) || IsOpen( rightValue ) ? OpenConductance : Series( leftValue, rightValue );
        }

        /// <summary> Returns the equivalent conductance of a resistor and conductor in series. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="conductance"> The conductance. </param>
        /// <param name="resistance">  The resistance. </param>
        /// <returns> A Double. </returns>
        public static double SeriesResistor( double conductance, double resistance )
        {
            return conductance < OpenConductance
                ? throw new ArgumentOutOfRangeException( nameof( conductance ), "value must be non-negative" )
                : IsOpen( conductance ) || Resistor.IsOpen( resistance )
                ? OpenConductance
                : IsShort( conductance )
                    ? Resistor.IsShort( resistance ) ? ShortConductance : 1.0d / resistance
                    : Resistor.IsShort( resistance ) ? conductance : ToSeries( conductance, 1d / resistance );
        }

        /// <summary> Converts a conductance to a resistance. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="conductance"> The conductance. </param>
        /// <returns> Conductance as a Double. </returns>
        public static double ToResistance( double conductance )
        {
            return IsOpen( conductance ) ? Resistor.OpenResistance : IsShort( conductance ) ? Resistor.ShortResistance : 1d / conductance;
        }

        /// <summary> Returns the relative current through the right parallel conductance. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <exception cref="InvalidOperationException">   Thrown when the requested operation is
        /// invalid. </exception>
        /// <param name="leftValue">  The left value. </param>
        /// <param name="rightValue"> The right value. </param>
        /// <returns> A Double. </returns>
        public static double ToOutputCurrent( double leftValue, double rightValue )
        {
            if ( leftValue < OpenConductance )
            {
                throw new ArgumentOutOfRangeException( nameof( leftValue ), "value must be non-negative" );
            }

            if ( rightValue < OpenConductance )
            {
                throw new ArgumentOutOfRangeException( nameof( rightValue ), "value must be non-negative" );
            }

            if ( IsShort( rightValue ) )
            {
                if ( IsShort( leftValue ) )
                {
                    // two shorts cannot be used and cause an exception
                    throw new InvalidOperationException( "Unable to assign output current for two parallel shorts." );
                }
                else
                {
                    return 1d;
                }
            }
            else if ( IsShort( leftValue ) )
            {
                return 0d;
            }
            else if ( IsOpen( leftValue ) )
            {
                if ( IsOpen( rightValue ) )
                {
                    // two opens cannot be used and cause an exception
                    throw new InvalidOperationException( "Unable to assign output current for two parallel opens." );
                }
                else
                {
                    return 1d;
                }
            }
            else
            {
                return IsOpen( rightValue )
                    ? IsOpen( leftValue ) ? throw new InvalidOperationException( "Unable to assign output current for two parallel opens." ) : 0d
                    : OutputCurrent( leftValue, rightValue );
            }
        }

        /// <summary> Returns the relative voltage across the right series conductance. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <exception cref="InvalidOperationException">   Thrown when the requested operation is
        /// invalid. </exception>
        /// <param name="leftValue">  The left value. </param>
        /// <param name="rightValue"> The right value. </param>
        /// <returns> The given data converted to a Double. </returns>
        public static double ToOutputVoltage( double leftValue, double rightValue )
        {
            if ( leftValue < OpenConductance )
            {
                throw new ArgumentOutOfRangeException( nameof( leftValue ), "value must be non-negative" );
            }

            if ( rightValue < OpenConductance )
            {
                throw new ArgumentOutOfRangeException( nameof( rightValue ), "value must be non-negative" );
            }

            if ( IsOpen( leftValue ) )
            {
                if ( IsOpen( rightValue ) )
                {
                    // two shorts cannot be used and cause an exception
                    throw new InvalidOperationException( "Unable to assign output voltage for two opens." );
                }
                else
                {
                    // if the left value is zero, all the current goes through it
                    return 0d;
                }
            }
            else if ( IsOpen( rightValue ) )
            {
                // if right value is zero, all the current goes through it.
                return 1d;
            }
            else if ( IsShort( leftValue ) )
            {
                if ( IsShort( rightValue ) )
                {
                    // two shorts cannot be used and cause an exception
                    throw new InvalidOperationException( "Unable to assign output voltage for two shorts." );
                }
                else
                {
                    return 1d;
                }
            }
            else
            {
                return IsShort( rightValue ) ? 0d : OutputVoltage( leftValue, rightValue );
            }
        }

        #endregion

    }
}
