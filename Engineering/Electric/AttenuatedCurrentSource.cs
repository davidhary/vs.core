using System;

namespace isr.Core.Engineering
{

    /// <summary> an Attenuated current source. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-09-15 </para>
    /// </remarks>
    public class AttenuatedCurrentSource : CurrentSource
    {

        #region " CONSTRUCTION "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="shortLoadCurrent">    The short load current. </param>
        /// <param name="seriesResistance">    The series resistance. </param>
        /// <param name="parallelConductance"> The parallel conductance. </param>
        public AttenuatedCurrentSource( double shortLoadCurrent, double seriesResistance, double parallelConductance ) : base( ToCurrentSource( shortLoadCurrent, seriesResistance, parallelConductance ) )
        {
            this.InitializeThis( shortLoadCurrent, seriesResistance, parallelConductance );
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="nominalCurrent">      The nominal Current. </param>
        /// <param name="seriesResistance">    The series resistance. </param>
        /// <param name="parallelConductance"> The parallel conductance. </param>
        public AttenuatedCurrentSource( decimal nominalCurrent, double seriesResistance, double parallelConductance ) : base( ToCurrentSource( nominalCurrent, seriesResistance, parallelConductance ) )
        {
            this.InitializeThis( nominalCurrent, seriesResistance, parallelConductance );
        }

        /// <summary> The cloning Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="currentSource"> The current source. </param>
        public AttenuatedCurrentSource( AttenuatedCurrentSource currentSource ) : this( ValidatedCurrentSource( currentSource ).Current, currentSource.SeriesResistance, currentSource.ParallelConductance )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="nominalCurrent"> The nominal Current. </param>
        /// <param name="resistance">     The resistance. </param>
        /// <param name="attenuation">    The attenuation. </param>
        public AttenuatedCurrentSource( decimal nominalCurrent, double resistance, decimal attenuation ) : this( new CurrentSource( ( double ) nominalCurrent, resistance ), attenuation )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="source">      The current source. </param>
        /// <param name="attenuation"> The attenuation. </param>
        public AttenuatedCurrentSource( CurrentSource source, double attenuation ) : this( ToAttenuatedCurrentSource( ValidatedCurrentSource( source ), attenuation ) )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="source">         The current source. </param>
        /// <param name="nominalCurrent"> The nominal Current. </param>
        public AttenuatedCurrentSource( CurrentSource source, decimal nominalCurrent ) : this( ToAttenuatedCurrentSource( ValidatedCurrentSource( source ), nominalCurrent ) )
        {
        }

        /// <summary> Validated attenuated current source. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="currentSource"> The current source. </param>
        /// <returns> An AttenuatedCurrentSource. </returns>
        public static AttenuatedCurrentSource ValidatedCurrentSource( AttenuatedCurrentSource currentSource )
        {
            return currentSource is null ? throw new ArgumentNullException( nameof( currentSource ) ) : currentSource;
        }

        /// <summary> Initializes this object. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="shortLoadCurrent">    The short load current. </param>
        /// <param name="seriesResistance">    The series resistance. </param>
        /// <param name="parallelConductance"> The parallel conductance. </param>
        private void InitializeThis( double shortLoadCurrent, double seriesResistance, double parallelConductance )
        {
            this._SeriesResistance = seriesResistance;
            this._ParallelConductance = parallelConductance;
            this._Attenuation = ToAttenuation( seriesResistance, parallelConductance );
            this._NominalCurrent = this._Attenuation * shortLoadCurrent;
        }

        /// <summary> Initializes this object. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="nominalCurrent">      The nominal Current. </param>
        /// <param name="seriesResistance">    The series resistance. </param>
        /// <param name="parallelConductance"> The parallel conductance. </param>
        private void InitializeThis( decimal nominalCurrent, double seriesResistance, double parallelConductance )
        {
            this._SeriesResistance = seriesResistance;
            this._ParallelConductance = parallelConductance;
            this._Attenuation = ToAttenuation( seriesResistance, parallelConductance );
            this._NominalCurrent = ( double ) nominalCurrent;
        }

        /// <summary> Initializes this object. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="shortLoadCurrent">    The short load current. </param>
        /// <param name="seriesResistance">    The series resistance. </param>
        /// <param name="parallelConductance"> The parallel conductance. </param>
        public void Initialize( double shortLoadCurrent, double seriesResistance, double parallelConductance )
        {
            this.InitializeThis( shortLoadCurrent, seriesResistance, parallelConductance );
            this.Initialize( shortLoadCurrent / this._Attenuation, ToEquivalentConductance( seriesResistance, parallelConductance ) );
        }

        /// <summary> Initializes this object. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="nominalCurrent">      The nominal Current. </param>
        /// <param name="seriesResistance">    The series resistance. </param>
        /// <param name="parallelConductance"> The parallel conductance. </param>
        public void Initialize( decimal nominalCurrent, double seriesResistance, double parallelConductance )
        {
            this.InitializeThis( nominalCurrent, seriesResistance, parallelConductance );
            this.Initialize( ( double ) nominalCurrent / this._Attenuation, ToEquivalentConductance( seriesResistance, parallelConductance ) );
        }

        /// <summary> Initializes this object. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="currentSource"> The current source. </param>
        /// <param name="attenuation">   The attenuation. The ratio of nominal to equivalent current
        /// source current. </param>
        public void Initialize( CurrentSource currentSource, double attenuation )
        {
            if ( currentSource is null )
            {
                throw new ArgumentNullException( nameof( currentSource ) );
            }

            if ( attenuation < MinimumAttenuation )
            {
                throw new ArgumentOutOfRangeException( nameof( attenuation ), $"must be greater or equal to {MinimumAttenuation}" );
            }

            this._Attenuation = attenuation;
            this._NominalCurrent = currentSource.Current * this.Attenuation;
            this._ParallelConductance = this.Attenuation * currentSource.Conductance;
            this._SeriesResistance = (attenuation - 1d) / this._ParallelConductance;
            this.Initialize( currentSource );
        }

        /// <summary> Initializes this object. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="attenuation"> The attenuation. </param>
        public void Initialize( double attenuation )
        {
            if ( attenuation < MinimumAttenuation )
            {
                throw new ArgumentOutOfRangeException( nameof( attenuation ), $"must be greater or equal to {MinimumAttenuation}" );
            }

            this._Attenuation = attenuation;
            this._NominalCurrent = this.Current * this.Attenuation;
            this._ParallelConductance = this.Attenuation * this.Conductance;
            this._SeriesResistance = (attenuation - 1d) / this._ParallelConductance;
        }

        #endregion

        #region " EQUALS "

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
        /// <see cref="T:System.Object" />. </param>
        /// <returns>
        /// <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />; otherwise, <c>False</c>.
        /// </returns>
        public override bool Equals( object obj )
        {
            return this.Equals( obj as AttenuatedCurrentSource );
        }

        /// <summary>
        /// Compares two Attenuated Current Sources. The Attenuated Current Sources are compared using
        /// their resistances and Currents.
        /// </summary>
        /// <remarks> The balances are the same if the have the same resistances and Currents. </remarks>
        /// <param name="other"> Specifies the other <see cref="AttenuatedCurrentSource">Attenuated
        /// Current Source</see>
        /// to compare for equality with this instance. </param>
        /// <returns> A Boolean data type. </returns>
        public bool Equals( AttenuatedCurrentSource other )
        {
            return other is object &&
                   base.Equals( other ) &&
                   this.NominalCurrent.Equals( other.NominalCurrent ) &&
                   this.SeriesResistance.Equals( other.SeriesResistance ) &&
                   this.ParallelResistance.Equals( other.ParallelResistance );
        }

        /// <summary> Compares two Attenuated Current Sources. </summary>
        /// <remarks> The balances are the same if the have the same resistances and Currents. </remarks>
        /// <param name="other">     Specifies the other <see cref="AttenuatedCurrentSource">Attenuated
        /// Current Source</see>
        /// to compare for equality with this instance. </param>
        /// <param name="tolerance"> Specifies the relative tolerance for comparing the two values. The
        /// values are compared based on their end points. </param>
        /// <returns> A Boolean data type. </returns>
        public bool Equals( AttenuatedCurrentSource other, double tolerance )
        {
            return !(other is null) && (this.Equals( other ) ||
                   base.Equals( other, tolerance ) &&
                   Math.Abs( this.NominalCurrent - other.NominalCurrent ) < other.NominalCurrent * tolerance &&
                   Math.Abs( this.SeriesResistance - other.SeriesResistance ) <= other.SeriesResistance * tolerance &&
                   Math.Abs( this.ParallelResistance - other.ParallelResistance ) <= other.ParallelResistance * tolerance);
        }

        /// <summary> Implements the operator =. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool Equals( AttenuatedCurrentSource left, AttenuatedCurrentSource right )
        {
            return ReferenceEquals( left, right ) || left is object && left.Equals( right );
        }

        /// <summary>
        /// Compares two Attenuated Current Sources. The Attenuated Current Sources are compared using
        /// their resistances and Currents.
        /// </summary>
        /// <remarks> The balances are the same if the have the same values and layout. </remarks>
        /// <param name="left">      Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right">     Specifies the right hand side argument of the binary operation. </param>
        /// <param name="tolerance"> Specifies the relative tolerance for comparing the two values. The
        /// values are compared based on their end points. </param>
        /// <returns> A Boolean data type. </returns>
        public static bool Equals( AttenuatedCurrentSource left, AttenuatedCurrentSource right, double tolerance )
        {
            return left is null && right is null || left is object && left.Equals( right, tolerance );
        }

        /// <summary> Implements the operator =. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( AttenuatedCurrentSource left, AttenuatedCurrentSource right )
        {
            return Equals( left, right );
        }

        /// <summary> Implements the operator &lt;&gt;. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( AttenuatedCurrentSource left, AttenuatedCurrentSource right )
        {
            return !Equals( left, right );
        }

        /// <summary> Creates a unique hash code. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> An <see cref="System.Int32">Int32</see> value. </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode() ^ this.NominalCurrent.GetHashCode() ^ this.SeriesResistance.GetHashCode() ^ this.ParallelResistance.GetHashCode();
        }

        #endregion

        #region " TO STRING "

        /// <summary> Returns a string that represents the current object. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> A string that represents the current object. </returns>
        public override string ToString()
        {
            return $"{this.NominalCurrent}:{this.SeriesResistance}:{this.ParallelConductance}";
        }

        /// <summary> Returns a string that represents the current object. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="currentFormat">    The Current format. </param>
        /// <param name="resistanceFormat"> The resistance format. </param>
        /// <returns> A string that represents the current object. </returns>
        public new string ToString( string currentFormat, string resistanceFormat )
        {
            return $"{string.Format( currentFormat, this.NominalCurrent )}:{string.Format( resistanceFormat, this.SeriesResistance )}:{string.Format( resistanceFormat, this.ParallelResistance )}";
        }

        #endregion

        #region " COMPONENTS "

        /// <summary> The nominal current. </summary>
        private double _NominalCurrent;

        /// <summary> Gets or sets the Current of the attenuated source. </summary>
        /// <value> The nominal Current. </value>
        public double NominalCurrent
        {
            get => this._NominalCurrent;

            set {
                if ( value != this.NominalCurrent )
                {
                    this._NominalCurrent = value;
                    this.Initialize( ( decimal ) this.NominalCurrent, this.SeriesResistance, this.ParallelConductance );
                }
            }
        }

        /// <summary> The series resistance. </summary>
        private double _SeriesResistance;

        /// <summary> Gets or sets the series resistance. </summary>
        /// <value> The series resistance. </value>
        public double SeriesResistance
        {
            get => this._SeriesResistance;

            set {
                if ( value != this.SeriesResistance )
                {
                    this._SeriesResistance = value;
                    this.Initialize( ( decimal ) this.NominalCurrent, this.SeriesResistance, this.ParallelConductance );
                }
            }
        }

        /// <summary> Gets the equivalent resistance. </summary>
        /// <value> The equivalent resistance. </value>
        public double Resistance => Conductor.ToResistance( this.Conductance );

        /// <summary> Gets the parallel resistance. </summary>
        /// <value> The parallel resistance. </value>
        public double ParallelResistance => Conductor.ToResistance( this.ParallelConductance );

        /// <summary> The parallel conductance. </summary>
        private double _ParallelConductance;

        /// <summary> Gets or sets the parallel conductance. </summary>
        /// <value> The parallel conductance. </value>
        public double ParallelConductance
        {
            get => this._ParallelConductance;

            set {
                if ( value != this.ParallelConductance )
                {
                    this._ParallelConductance = value;
                    this.Initialize( ( decimal ) this.NominalCurrent, this.SeriesResistance, this.ParallelConductance );
                }
            }
        }

        /// <summary> The minimum attenuation. This is the minimum ratio between the nominal source current to the equivalent source current over a short. </summary>
        public const double MinimumAttenuation = 1d;

        /// <summary> The attenuation. </summary>
        private double _Attenuation;

        /// <summary>
        /// Gets or sets the attenuation. This is the ratio of the nominal source voltage to the
        /// equivalent source voltage.
        /// </summary>
        /// <value> The attenuation. </value>
        public double Attenuation
        {
            get => this._Attenuation;

            set {
                if ( value != this.Attenuation )
                {
                    this.Initialize( value );
                }
            }
        }

        #endregion

        #region " ATTENUATION AND EQUIVALENT CONDUCTANCE "

        /// <summary> Evaluates attenuation. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="seriesResistance">    The series resistance. </param>
        /// <param name="parallelConductance"> The parallel conductance. </param>
        /// <returns> A Double. </returns>
        public static double ToAttenuation( double seriesResistance, double parallelConductance )
        {
            return seriesResistance < 0d
                ? throw new ArgumentException( $"Value {seriesResistance} must not be negative", nameof( seriesResistance ) )
                : parallelConductance < 0d
                ? throw new ArgumentException( $"Value {parallelConductance} must not be negative", nameof( parallelConductance ) )
                : seriesResistance == 0d || parallelConductance == 0d ? 1d : 1d + seriesResistance * parallelConductance;
        }

        /// <summary> Evaluates attenuation. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="seriesResistance">   The series resistance. </param>
        /// <param name="parallelResistance"> The parallel resistance. </param>
        /// <returns> A Double. </returns>
        public static double ToAttenuation( double seriesResistance, decimal parallelResistance )
        {
            return parallelResistance <= 0m
                ? throw new ArgumentException( $"Value {parallelResistance} must be positive", nameof( parallelResistance ) )
                : AttenuatedVoltageSource.ToAttenuation( seriesResistance, 1m / parallelResistance );
        }

        /// <summary> Evaluates attenuation. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="nominalCurrent">   The nominal current. </param>
        /// <param name="shortLoadCurrent"> The short load current. </param>
        /// <returns> A Double. </returns>
        public static double ToAttenuation( decimal nominalCurrent, double shortLoadCurrent )
        {
            return ( double ) nominalCurrent / shortLoadCurrent;
        }

        /// <summary> Evaluates equivalent Conductance. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="seriesResistance">    The series resistance. </param>
        /// <param name="parallelConductance"> The parallel conductance. </param>
        /// <returns> A Double. </returns>
        public static double ToEquivalentConductance( double seriesResistance, double parallelConductance )
        {
            return seriesResistance < 0d
                ? throw new ArgumentException( $"Value {seriesResistance} must not be negative", nameof( seriesResistance ) )
                : parallelConductance < 0d
                ? throw new ArgumentException( $"Value {parallelConductance} must not be negative", nameof( parallelConductance ) )
                : Conductor.SeriesResistor( parallelConductance, seriesResistance );
        }

        /// <summary> Evaluates equivalent Conductance. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="seriesResistance">   The series resistance. </param>
        /// <param name="parallelResistance"> The parallel resistance. </param>
        /// <returns> A Double. </returns>
        public static double ToEquivalentConductance( double seriesResistance, decimal parallelResistance )
        {
            return parallelResistance <= 0m
                ? throw new ArgumentException( $"Value {parallelResistance} must be positive", nameof( parallelResistance ) )
                : seriesResistance < 0d
                ? throw new ArgumentException( $"Value {seriesResistance} must not be negative", nameof( seriesResistance ) )
                : Resistor.ToConductance( Resistor.ToSeries( seriesResistance, ( double ) parallelResistance ) );
        }

        #endregion

        #region " NOMINAL CURRENT CONVERTERS "

        /// <summary>
        /// Converts an short load current of an attenuated current source to the nominal current of a
        /// non-attenuated current source.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="shortLoadCurrent">    The short load current. </param>
        /// <param name="seriesResistance">    The series resistance. </param>
        /// <param name="parallelConductance"> The parallel conductance. </param>
        /// <returns> The given data converted to a Double. </returns>
        public static double ToNominalCurrent( double shortLoadCurrent, double seriesResistance, double parallelConductance )
        {
            return parallelConductance == 0d || seriesResistance == 0d ? shortLoadCurrent : shortLoadCurrent * ToAttenuation( seriesResistance, parallelConductance );
        }

        /// <summary>
        /// Converts nominal current of a non-attenuated current source to an short load current or the
        /// equivalent attenuated current source.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="nominalCurrent">      The nominal current. </param>
        /// <param name="seriesResistance">    The series resistance. </param>
        /// <param name="parallelConductance"> The parallel conductance. </param>
        /// <returns> The given data converted to a Double. </returns>
        public static double ToShortLoadCurrent( decimal nominalCurrent, double seriesResistance, double parallelConductance )
        {
            return parallelConductance == 0d || seriesResistance == 0d ? ( double ) nominalCurrent : ( double ) nominalCurrent / ToAttenuation( seriesResistance, parallelConductance );
        }

        /// <summary>
        /// Converts nominal current of a non-attenuated current source to an attenuated current source
        /// with the specified seres and parallel resistances.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="nominalCurrent">      The nominal current. </param>
        /// <param name="seriesResistance">    The series resistance. </param>
        /// <param name="parallelConductance"> The parallel conductance. </param>
        /// <returns> The given data converted to an AttenuatedcurrentSource. </returns>
        public static AttenuatedCurrentSource ToAttenuatedCurrentSource( decimal nominalCurrent, double seriesResistance, double parallelConductance )
        {
            return new AttenuatedCurrentSource( ToShortLoadCurrent( nominalCurrent, seriesResistance, parallelConductance ), seriesResistance, parallelConductance );
        }

        #endregion

        #region " CONVERTERS "

        /// <summary>
        /// Build a current source using the short load current and equivalent conductance for the given
        /// resistance and conductance values.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="shortLoadCurrent">    The short load current. </param>
        /// <param name="seriesResistance">    The series resistance. </param>
        /// <param name="parallelConductance"> The parallel conductance. </param>
        /// <returns> The given data converted to a CurrentSource. </returns>
        public static CurrentSource ToCurrentSource( double shortLoadCurrent, double seriesResistance, double parallelConductance )
        {
            return new CurrentSource( shortLoadCurrent, ToEquivalentConductance( seriesResistance, parallelConductance ) );
        }

        /// <summary>
        /// Build a current source using the nominal current and equivalent conductance for the given
        /// resistance and conductance values.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="nominalCurrent">      The nominal Current. </param>
        /// <param name="seriesResistance">    The series resistance. </param>
        /// <param name="parallelConductance"> The parallel conductance. </param>
        /// <returns> The given data converted to a CurrentSource. </returns>
        public static CurrentSource ToCurrentSource( decimal nominalCurrent, double seriesResistance, double parallelConductance )
        {
            return new CurrentSource( ToShortLoadCurrent( nominalCurrent, seriesResistance, parallelConductance ), ToEquivalentConductance( seriesResistance, parallelConductance ) );
        }

        /// <summary> Converts this object to a Current source. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> The given data converted to a CurrentSource. </returns>
        public CurrentSource ToCurrentSource()
        {
            return new CurrentSource( this.Current, this.Conductance );
        }

        /// <summary> From voltage source. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="voltageSource">  The voltage source. </param>
        /// <param name="nominalCurrent"> The nominal Current. </param>
        public void FromVoltageSource( VoltageSource voltageSource, double nominalCurrent )
        {
            if ( voltageSource is null )
            {
                throw new ArgumentNullException( nameof( voltageSource ) );
            }

            double attenuation = nominalCurrent * voltageSource.Resistance / voltageSource.Voltage;
            this.Initialize( new CurrentSource( nominalCurrent, 1d / voltageSource.Resistance ), attenuation );
        }

        /// <summary> From Attenuated voltage source. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="voltageSource">  The voltage source. </param>
        /// <param name="nominalCurrent"> The nominal Current. </param>
        public void FromAttenuatedVoltageSource( AttenuatedVoltageSource voltageSource, double nominalCurrent )
        {
            if ( voltageSource is null )
            {
                throw new ArgumentNullException( nameof( voltageSource ) );
            }

            double attenuation = nominalCurrent * voltageSource.Resistance / voltageSource.Voltage;
            this.Initialize( new CurrentSource( nominalCurrent, 1d / voltageSource.Resistance ), attenuation );
        }

        /// <summary> Converts a nominalVoltage to an Attenuated voltage source. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="nominalVoltage"> The nominal voltage. </param>
        /// <returns> NominalVoltage as an AttenuatedVoltageSource. </returns>
        public AttenuatedVoltageSource ToAttenuatedVoltageSource( double nominalVoltage )
        {
            this.ValidateAttenuatedVoltageSourceConversion( nominalVoltage );
            return this.ToVoltageSource().ToAttenuatedVoltageSource( nominalVoltage / this.ToVoltageSource().Voltage );
        }

        /// <summary>
        /// Validates the attenuated voltage source conversion described by nominalVoltage.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="nominalVoltage"> The nominal voltage. </param>
        public void ValidateAttenuatedVoltageSourceConversion( double nominalVoltage )
        {
            double openLoadVoltage = this.LoadVoltage( Resistor.OpenResistance );
            if ( openLoadVoltage > nominalVoltage )
            {
                throw new InvalidOperationException( $"Current source with an open load voltage of {openLoadVoltage:G4} cannot be converted to a voltage source with a lower nominal voltage of {nominalVoltage:G4}" );
            }
        }

        #endregion

    }
}
