using System;

namespace isr.Core.Engineering
{

    /// <summary> A resistor. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-09-16 </para>
    /// </remarks>
    public sealed class Resistor
    {

        #region " CONSTRUCTION "

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        private Resistor() : base()
        {
        }

        #endregion

        #region " RESISTOR PAIR "

        /// <summary> Returns the equivalent Resistance of two Resistors in parallel. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="leftValue">  The left value. </param>
        /// <param name="rightValue"> The right value. </param>
        /// <returns> A Double. </returns>
        public static double Parallel( double leftValue, double rightValue )
        {
            return leftValue * rightValue / (leftValue + rightValue);
        }

        /// <summary> Returns the equivalent Resistance of two Resistors in series. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="leftValue">  The left value. </param>
        /// <param name="rightValue"> The right value. </param>
        /// <returns> A Double. </returns>
        public static double Series( double leftValue, double rightValue )
        {
            return leftValue + rightValue;
        }

        /// <summary> Returns the relative current through the right parallel Resistance. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="leftValue">  The left value. </param>
        /// <param name="rightValue"> The right value. </param>
        /// <returns> A Double. </returns>
        public static double OutputCurrent( double leftValue, double rightValue )
        {
            return leftValue / (leftValue + rightValue);
        }

        /// <summary> Returns the relative voltage across the right series Resistance. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="leftValue">  The left value. </param>
        /// <param name="rightValue"> The right value. </param>
        /// <returns> A Double. </returns>
        public static double OutputVoltage( double leftValue, double rightValue )
        {
            return rightValue / (leftValue + rightValue);
        }

        #endregion

        #region " PAIR OF RESISTORS: WITH VALIDATIONS "

        /// <summary> Gets or sets the open Resistance. </summary>
        /// <value> The open Resistance. </value>
        public static double OpenResistance { get; set; } = double.PositiveInfinity;

        /// <summary> Gets or sets the short Resistance. </summary>
        /// <value> The short Resistance. </value>
        public static double ShortResistance { get; set; } = 0d;

        /// <summary> Query if 'value' is open. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> <c>true</c> if open; otherwise <c>false</c> </returns>
        public static bool IsOpen( double value )
        {
            return value == OpenResistance;
        }

        /// <summary> Query if 'value' is short. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> <c>true</c> if short; otherwise <c>false</c> </returns>
        public static bool IsShort( double value )
        {
            return value == ShortResistance;
        }

        /// <summary> Returns the equivalent resistance of a series resistor and conductor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="resistance">  The resistance. </param>
        /// <param name="conductance"> The conductance. </param>
        /// <returns> A Double. </returns>
        public static double SeriesConductor( double resistance, double conductance )
        {
            return conductance < Conductor.OpenConductance
                ? throw new ArgumentOutOfRangeException( nameof( conductance ), "value must be non-negative" )
                : Conductor.IsOpen( conductance )
                ? OpenResistance
                : Conductor.IsShort( conductance )
                    ? resistance
                    : IsOpen( resistance ) ? OpenResistance : IsShort( resistance ) ? 1d / conductance : ToSeries( resistance, 1d / conductance );
        }

        /// <summary> Returns the equivalent resistance of applying a shunt conductor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="resistance">  The resistance. </param>
        /// <param name="conductance"> The conductance. </param>
        /// <returns> A Double. </returns>
        public static double ShuntConductor( double resistance, double conductance )
        {
            return conductance < Conductor.OpenConductance
                ? throw new ArgumentOutOfRangeException( nameof( conductance ), "value must be non-negative" )
                : Conductor.IsOpen( conductance )
                ? ToParallel( resistance, OpenResistance )
                : Conductor.IsShort( conductance ) ? ToParallel( resistance, ShortResistance ) : ToParallel( resistance, 1d / conductance );
        }

        /// <summary> Returns the equivalent Resistance of two parallel Resistors. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="leftValue">  The left value. </param>
        /// <param name="rightValue"> The right value. </param>
        /// <returns> A Double. </returns>
        public static double ToParallel( double leftValue, double rightValue )
        {
            return leftValue < ShortResistance
                ? throw new ArgumentOutOfRangeException( nameof( leftValue ), "value must be non-negative" )
                : rightValue < ShortResistance
                ? throw new ArgumentOutOfRangeException( nameof( rightValue ), "value must be non-negative" )
                : IsOpen( leftValue )
                ? rightValue
                : IsOpen( rightValue )
                    ? leftValue
                    : IsShort( leftValue ) || IsShort( rightValue ) ? ShortResistance : Parallel( leftValue, rightValue );
        }

        /// <summary> returns the equivalent series Resistance of two Resistors. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="leftValue">  The left value. </param>
        /// <param name="rightValue"> The right value. </param>
        /// <returns> A Double. </returns>
        public static double ToSeries( double leftValue, double rightValue )
        {
            return leftValue < ShortResistance
                ? throw new ArgumentOutOfRangeException( nameof( leftValue ), "value must be non-negative" )
                : rightValue < ShortResistance
                ? throw new ArgumentOutOfRangeException( nameof( rightValue ), "value must be non-negative" )
                : IsShort( leftValue )
                ? rightValue
                : IsShort( rightValue )
                    ? leftValue
                    : IsOpen( leftValue ) || IsOpen( rightValue ) ? OpenResistance : Series( leftValue, rightValue );
        }

        /// <summary> Converts a resistance to a conductance. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="resistance"> The resistance. </param>
        /// <returns> Resistance as a Double. </returns>
        public static double ToConductance( double resistance )
        {
            return IsOpen( resistance ) ? Conductor.OpenConductance : IsShort( resistance ) ? Conductor.ShortConductance : 1d / resistance;
        }

        /// <summary> Returns the relative current through the right parallel Resistance. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <exception cref="InvalidOperationException">   Thrown when the requested operation is
        /// invalid. </exception>
        /// <param name="leftValue">  The left value. </param>
        /// <param name="rightValue"> The right value. </param>
        /// <returns> A Double. </returns>
        public static double ToOutputCurrent( double leftValue, double rightValue )
        {
            if ( leftValue < ShortResistance )
            {
                throw new ArgumentOutOfRangeException( nameof( leftValue ), "value must be non-negative" );
            }

            if ( rightValue < ShortResistance )
            {
                throw new ArgumentOutOfRangeException( nameof( rightValue ), "value must be non-negative" );
            }

            if ( IsShort( rightValue ) )
            {
                if ( IsShort( leftValue ) )
                {
                    // two shorts cannot be used and cause an exception
                    throw new InvalidOperationException( "Unable to assign output current for two parallel shorts." );
                }
                else
                {
                    return 1d;
                }
            }
            else if ( IsShort( leftValue ) )
            {
                return 0d;
            }
            else if ( IsOpen( leftValue ) )
            {
                if ( IsOpen( rightValue ) )
                {
                    // two opens cannot be used and cause an exception
                    throw new InvalidOperationException( "Unable to assign output current for two parallel opens." );
                }
                else
                {
                    return 1d;
                }
            }
            else
            {
                return IsOpen( rightValue ) ? 0d : OutputCurrent( leftValue, rightValue );
            }
        }

        /// <summary> Returns the relative voltage across the right series Resistance. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <exception cref="InvalidOperationException">   Thrown when the requested operation is
        /// invalid. </exception>
        /// <param name="leftValue">  The left value. </param>
        /// <param name="rightValue"> The right value. </param>
        /// <returns> The given data converted to a Double. </returns>
        public static double ToOutputVoltage( double leftValue, double rightValue )
        {
            if ( leftValue < ShortResistance )
            {
                throw new ArgumentOutOfRangeException( nameof( leftValue ), "value must be non-negative" );
            }

            if ( rightValue < ShortResistance )
            {
                throw new ArgumentOutOfRangeException( nameof( rightValue ), "value must be non-negative" );
            }

            if ( IsOpen( leftValue ) )
            {
                if ( IsOpen( rightValue ) )
                {
                    // two opens cannot be used and cause an exception
                    throw new InvalidOperationException( "Unable to assign output voltage for two opens." );
                }
                else
                {
                    // if the left value is zero, all the current goes through it
                    return 0d;
                }
            }
            else if ( IsOpen( rightValue ) )
            {
                // if right value is zero, all the current goes through it.
                return 1d;
            }
            else if ( IsShort( leftValue ) )
            {
                // if left is open, we have zero
                if ( IsShort( rightValue ) )
                {
                    // two shorts cannot be used and cause an exception
                    throw new InvalidOperationException( "Unable to assign output voltage for two shorts." );
                }
                else
                {
                    return 1d;
                }
            }
            else
            {
                return IsShort( rightValue ) ? 0d : OutputVoltage( leftValue, rightValue );
            }
        }

        #endregion

        #region " STANDARD RESISTOR TABLE "

        /// <summary> Decade value. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="decadePosition"> The decade position. </param>
        /// <param name="decadeCount">    Number of decades. </param>
        /// <returns> A Double. </returns>
        public static double DecadeValue( int decadePosition, int decadeCount )
        {
            return Math.Round( Math.Pow( 10d, decadePosition / ( double ) decadeCount ), ( int ) Math.Floor( Math.Log10( decadeCount ) ) );
        }

        /// <summary> Converts the resistance to a decade value for the specified decade count. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="resistance">  The resistance. </param>
        /// <param name="decadeCount"> Number of decades. </param>
        /// <returns> The decade value for the specified decade count. </returns>
        public static double ToDecadeValue( double resistance, int decadeCount )
        {
            double result;
            int i = ( int ) Math.Round( decadeCount * (Math.Log10( resistance ) - Math.Floor( Math.Log10( resistance ) )) );
            result = DecadeValue( i, decadeCount );
            // result = Math.Round(Math.Pow(10, i / decadeCount), 2)
            return result;
        }

        /// <summary>
        /// Converts the resistance to a standard resistance for the specified decade count.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="resistance">  The resistance. </param>
        /// <param name="decadeCount"> Number of decades. </param>
        /// <returns> The standard resistance for the specified decade count. </returns>
        public static double ToStandardResistance( double resistance, int decadeCount )
        {
            return Math.Pow( 10d, Math.Floor( Math.Log10( resistance ) ) ) * ToDecadeValue( resistance, decadeCount );
        }

        #endregion

    }
}
