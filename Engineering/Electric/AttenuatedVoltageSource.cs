using System;

namespace isr.Core.Engineering
{

    /// <summary> An attenuated voltage source. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-09-15 </para>
    /// </remarks>
    public class AttenuatedVoltageSource : VoltageSource
    {

        #region " CONSTRUCTION "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="openLoadVoltage">     The open load voltage. </param>
        /// <param name="seriesResistance">    The series resistance. </param>
        /// <param name="parallelConductance"> The parallel conductance. </param>
        public AttenuatedVoltageSource( double openLoadVoltage, double seriesResistance, double parallelConductance ) : base( ToVoltageSource( openLoadVoltage, seriesResistance, parallelConductance ) )
        {
            this.InitializeThis( openLoadVoltage, seriesResistance, parallelConductance );
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="nominalVoltage">      The nominal voltage. </param>
        /// <param name="seriesResistance">    The series resistance. </param>
        /// <param name="parallelConductance"> The parallel conductance. </param>
        public AttenuatedVoltageSource( decimal nominalVoltage, double seriesResistance, double parallelConductance ) : base( ToVoltageSource( nominalVoltage, seriesResistance, parallelConductance ) )
        {
            this.InitializeThis( nominalVoltage, seriesResistance, parallelConductance );
        }

        /// <summary> The cloning Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="voltageSource"> The voltage source. </param>
        public AttenuatedVoltageSource( AttenuatedVoltageSource voltageSource ) : this( ValidatedVoltageSource( voltageSource ).Voltage, voltageSource.SeriesResistance, voltageSource.ParallelConductance )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="voltageSource"> The voltage Source. </param>
        /// <param name="attenuation">   The attenuation. The ratio of the nominal voltage to the source
        /// voltage over open load. </param>
        public AttenuatedVoltageSource( VoltageSource voltageSource, double attenuation ) : this( ToAttenuatedVoltageSource( ValidatedVoltageSource( voltageSource ), attenuation ) )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="nominalVoltage"> The nominal voltage. </param>
        /// <param name="resistance">     The resistance. </param>
        /// <param name="attenuation">    The attenuation. The ratio of the nominal voltage to the source
        /// voltage over open load. </param>
        public AttenuatedVoltageSource( decimal nominalVoltage, double resistance, decimal attenuation ) : this( VoltageSource.ToAttenuatedVoltageSource( nominalVoltage, resistance, ( double ) attenuation ) )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="voltageSource">  The voltage Source. </param>
        /// <param name="nominalVoltage"> The nominal voltage. </param>
        public AttenuatedVoltageSource( VoltageSource voltageSource, decimal nominalVoltage ) : this( ToAttenuatedVoltageSource( ValidatedVoltageSource( voltageSource ), nominalVoltage ) )
        {
        }

        /// <summary> Validated Attenuated voltage source. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="voltageSource"> The voltage source. </param>
        /// <returns> An AttenuatedVoltageSource. </returns>
        public static AttenuatedVoltageSource ValidatedVoltageSource( AttenuatedVoltageSource voltageSource )
        {
            return voltageSource is null ? throw new ArgumentNullException( nameof( voltageSource ) ) : voltageSource;
        }

        /// <summary> Initializes the attenuated voltage source using nominal voltage. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="nominalVoltage">      The nominal voltage. </param>
        /// <param name="seriesResistance">    The series resistance. </param>
        /// <param name="parallelConductance"> The parallel conductance. </param>
        private void InitializeThis( decimal nominalVoltage, double seriesResistance, double parallelConductance )
        {
            this._NominalVoltage = ( double ) nominalVoltage;
            this._SeriesResistance = seriesResistance;
            this._ParallelConductance = parallelConductance;
            this._Attenuation = ToAttenuation( seriesResistance, parallelConductance );
        }

        /// <summary> Updates the attenuated voltage source using nominal voltage. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="nominalVoltage">      The nominal voltage. </param>
        /// <param name="seriesResistance">    The series resistance. </param>
        /// <param name="parallelConductance"> The parallel conductance. </param>
        public void Initialize( decimal nominalVoltage, double seriesResistance, double parallelConductance )
        {
            this.InitializeThis( nominalVoltage, seriesResistance, parallelConductance );
            this.Initialize( ( double ) nominalVoltage / this._Attenuation, ToEquivalentResistance( seriesResistance, parallelConductance ) );
        }

        /// <summary> Initializes the attenuated voltage source using open load voltage. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="openLoadVoltage">     The open load voltage. </param>
        /// <param name="seriesResistance">    The series resistance. </param>
        /// <param name="parallelConductance"> The parallel conductance. </param>
        private void InitializeThis( double openLoadVoltage, double seriesResistance, double parallelConductance )
        {
            this._SeriesResistance = seriesResistance;
            this._ParallelConductance = parallelConductance;
            this._Attenuation = ToAttenuation( seriesResistance, parallelConductance );
            this._NominalVoltage = this._Attenuation * openLoadVoltage;
        }

        /// <summary> Updates the attenuated voltage source using open load voltage. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="openLoadVoltage">     The open load voltage. </param>
        /// <param name="seriesResistance">    The series resistance. </param>
        /// <param name="parallelConductance"> The parallel conductance. </param>
        public void Initialize( double openLoadVoltage, double seriesResistance, double parallelConductance )
        {
            this.InitializeThis( openLoadVoltage, seriesResistance, parallelConductance );
            this.Initialize( openLoadVoltage, ToEquivalentResistance( seriesResistance, parallelConductance ) );
        }

        /// <summary> Initializes this object. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="voltageSource"> The voltage source. </param>
        /// <param name="attenuation">   The attenuation. </param>
        public void Initialize( VoltageSource voltageSource, double attenuation )
        {
            if ( voltageSource is null )
            {
                throw new ArgumentNullException( nameof( voltageSource ) );
            }

            if ( attenuation < MinimumAttenuation )
            {
                throw new ArgumentOutOfRangeException( nameof( attenuation ), $"must be greater or equal to {MinimumAttenuation}" );
            }

            this._Attenuation = attenuation;
            this._NominalVoltage = voltageSource.Voltage / this.Attenuation;
            this._SeriesResistance = this.Attenuation * voltageSource.Resistance;
            this._ParallelConductance = (this.Attenuation - 1d) / this.SeriesResistance;
            this.Initialize( voltageSource );
        }

        /// <summary>
        /// Initializes the attenuated voltage source for a new attenuation based on the underly voltage
        /// source open voltage and equivalent resistance.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="attenuation"> The attenuation. The ratio of the nominal to the equivalent
        /// voltage. Must be greater or equal to 1. </param>
        public void Initialize( double attenuation )
        {
            if ( attenuation < MinimumAttenuation )
            {
                throw new ArgumentOutOfRangeException( nameof( attenuation ), $"must be greater or equal to {MinimumAttenuation}" );
            }
            // keeps the open load voltage and equivalent resistance unchanged.
            this._NominalVoltage = attenuation * this.Voltage / this.Attenuation;
            this._Attenuation = attenuation;
            this._SeriesResistance = attenuation * this.Resistance;
            this._ParallelConductance = (attenuation - 1d) / this.SeriesResistance;
        }

        #endregion

        #region " EQUALS "

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
        /// <see cref="T:System.Object" />. </param>
        /// <returns>
        /// <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />; otherwise, <c>False</c>.
        /// </returns>
        public override bool Equals( object obj )
        {
            return this.Equals( obj as AttenuatedVoltageSource );
        }

        /// <summary>
        /// Compares two Attenuated Voltage Sources. The Attenuated Voltage Sources are compared using
        /// their resistances and voltages.
        /// </summary>
        /// <remarks> The balances are the same if the have the same resistances and voltages. </remarks>
        /// <param name="other"> Specifies the other <see cref="AttenuatedVoltageSource">Attenuated
        /// Voltage Source</see>
        /// to compare for equality with this instance. </param>
        /// <returns> A Boolean data type. </returns>
        public bool Equals( AttenuatedVoltageSource other )
        {
            return other is object && base.Equals( other ) && this.NominalVoltage.Equals( other.NominalVoltage ) && this.SeriesResistance.Equals( other.SeriesResistance ) && this.ParallelConductance.Equals( other.ParallelConductance );
        }

        /// <summary> Compares two Attenuated Voltage Sources. </summary>
        /// <remarks> The balances are the same if the have the same resistances and voltages. </remarks>
        /// <param name="other">     Specifies the other <see cref="AttenuatedVoltageSource">Attenuated
        /// Voltage Source</see>
        /// to compare for equality with this instance. </param>
        /// <param name="tolerance"> Specifies the relative tolerance for comparing the two values. The
        /// values are compared based on their end points. </param>
        /// <returns> A Boolean data type. </returns>
        public bool Equals( AttenuatedVoltageSource other, double tolerance )
        {
            return !(other is null) && (this.Equals( other ) ||
                     base.Equals( other, tolerance ) &&
                     Math.Abs( this.NominalVoltage - other.NominalVoltage ) < other.NominalVoltage * tolerance &&
                     Math.Abs( this.SeriesResistance - other.SeriesResistance ) <= other.SeriesResistance * tolerance &&
                     Math.Abs( this.ParallelConductance - other.ParallelConductance ) <= other.ParallelConductance * tolerance);
        }

        /// <summary> Implements the operator =. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool Equals( AttenuatedVoltageSource left, AttenuatedVoltageSource right )
        {
            return ReferenceEquals( left, right ) || left is object && left.Equals( right );
        }

        /// <summary>
        /// Compares two Attenuated Voltage Sources. The Attenuated Voltage Sources are compared using
        /// their resistances and voltages.
        /// </summary>
        /// <remarks> The balances are the same if the have the same values and layout. </remarks>
        /// <param name="left">      Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right">     Specifies the right hand side argument of the binary operation. </param>
        /// <param name="tolerance"> Specifies the relative tolerance for comparing the two values. The
        /// values are compared based on their end points. </param>
        /// <returns> A Boolean data type. </returns>
        public static bool Equals( AttenuatedVoltageSource left, AttenuatedVoltageSource right, double tolerance )
        {
            return left is null && right is null || left is object && left.Equals( right, tolerance );
        }

        /// <summary> Implements the operator =. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( AttenuatedVoltageSource left, AttenuatedVoltageSource right )
        {
            return Equals( left, right );
        }

        /// <summary> Implements the operator &lt;&gt;. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( AttenuatedVoltageSource left, AttenuatedVoltageSource right )
        {
            return !Equals( left, right );
        }

        /// <summary> Creates a unique hash code. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> An <see cref="System.Int32">Int32</see> value. </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode() ^ this.NominalVoltage.GetHashCode() ^ this.SeriesResistance.GetHashCode() ^ this.ParallelConductance.GetHashCode();
        }

        #endregion

        #region " TO STRING "

        /// <summary> Returns a string that represents the current object. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> A string that represents the current object. </returns>
        public override string ToString()
        {
            return $"{this.NominalVoltage}:{this.SeriesResistance}:{this.ParallelConductance}";
        }

        /// <summary> Returns a string that represents the current object. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="voltageFormat">    The voltage format. </param>
        /// <param name="resistanceFormat"> The resistance format. </param>
        /// <returns> A string that represents the current object. </returns>
        public new string ToString( string voltageFormat, string resistanceFormat )
        {
            return $"{string.Format( voltageFormat, this.NominalVoltage )}:{string.Format( resistanceFormat, this.SeriesResistance )}:{string.Format( resistanceFormat, this.ParallelConductance )}";
        }

        #endregion

        #region " COMPONENTS "

        /// <summary> The nominal voltage. </summary>
        private double _NominalVoltage;

        /// <summary> Gets or sets the nominal voltage of the attenuated source. </summary>
        /// <value> The nominal voltage. </value>
        public double NominalVoltage
        {
            get => this._NominalVoltage;

            set {
                if ( value != this.NominalVoltage )
                {
                    this._NominalVoltage = value;
                    this.Initialize( ( decimal ) this.NominalVoltage, this.SeriesResistance, this.ParallelConductance );
                }
            }
        }

        /// <summary> The series resistance. </summary>
        private double _SeriesResistance;

        /// <summary> Gets or sets the series resistance. </summary>
        /// <value> The series resistance. </value>
        public double SeriesResistance
        {
            get => this._SeriesResistance;

            set {
                if ( value != this.SeriesResistance )
                {
                    this._SeriesResistance = value;
                    this.Initialize( ( decimal ) this.NominalVoltage, this.SeriesResistance, this.ParallelConductance );
                }
            }
        }

        /// <summary> Gets the parallel resistance. </summary>
        /// <value> The parallel resistance. </value>
        public double ParallelResistance => Conductor.ToResistance( this.ParallelConductance );

        /// <summary> The parallel conductance. </summary>
        private double _ParallelConductance;

        /// <summary> Gets or sets the parallel conductance. </summary>
        /// <value> The parallel conductance. </value>
        public double ParallelConductance
        {
            get => this._ParallelConductance;

            set {
                if ( value != this.ParallelConductance )
                {
                    this._ParallelConductance = value;
                    this.Initialize( ( decimal ) this.NominalVoltage, this.SeriesResistance, this.ParallelConductance );
                }
            }
        }

        /// <summary> The minimum attenuation. This is the minimum ratio between the nominal voltage source voltage and the equivalent voltage source voltage. </summary>
        public const double MinimumAttenuation = 1d;

        /// <summary> The attenuation. </summary>
        private double _Attenuation;

        /// <summary>
        /// Gets or sets the attenuation. This is the ratio of the nominal source voltage to the output
        /// voltage over open load.
        /// </summary>
        /// <value> The attenuation. </value>
        public double Attenuation
        {
            get => this._Attenuation;

            set {
                if ( value != this.Attenuation )
                {
                    this.Initialize( value );
                }
            }
        }

        #endregion

        #region " ATTENUATION AND EQUIVALENT RESISTANCE "

        /// <summary> Evaluate attenuation. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="seriesResistance">    The series resistance. </param>
        /// <param name="parallelConductance"> The parallel conductance. </param>
        /// <returns> A Double. </returns>
        public static double ToAttenuation( double seriesResistance, double parallelConductance )
        {
            return seriesResistance < 0d
                ? throw new ArgumentException( $"Value {seriesResistance} must not be negative", nameof( seriesResistance ) )
                : parallelConductance < 0d
                ? throw new ArgumentException( $"Value {parallelConductance} must not be negative", nameof( parallelConductance ) )
                : seriesResistance == 0d || parallelConductance == 0d ? 1d : 1d + seriesResistance * parallelConductance;
        }

        /// <summary> Evaluates attenuation. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="seriesResistance">   The series resistance. </param>
        /// <param name="parallelResistance"> The parallel resistance. </param>
        /// <returns> A Double. </returns>
        public static double ToAttenuation( double seriesResistance, decimal parallelResistance )
        {
            return parallelResistance <= 0m
                ? throw new ArgumentException( $"Value {parallelResistance} must be positive", nameof( parallelResistance ) )
                : ToAttenuation( seriesResistance, 1m / parallelResistance );
        }

        /// <summary> Evaluates attenuation. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="nominalVoltage">  The nominal voltage. </param>
        /// <param name="openLoadVoltage"> The open load voltage. </param>
        /// <returns> A Double. </returns>
        public static double ToAttenuation( decimal nominalVoltage, double openLoadVoltage )
        {
            return ( double ) nominalVoltage / openLoadVoltage;
        }

        /// <summary> Evaluates equivalent resistance. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="seriesResistance">    The series resistance. </param>
        /// <param name="parallelConductance"> The parallel conductance. </param>
        /// <returns> A Double. </returns>
        public static double ToEquivalentResistance( double seriesResistance, double parallelConductance )
        {
            return seriesResistance < 0d
                ? throw new ArgumentException( $"Value {seriesResistance} must not be negative", nameof( seriesResistance ) )
                : parallelConductance < 0d
                ? throw new ArgumentException( $"Value {parallelConductance} must not be negative", nameof( parallelConductance ) )
                : Resistor.ShuntConductor( seriesResistance, parallelConductance );
        }

        /// <summary> Evaluates equivalent resistance. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="seriesResistance">   The series resistance. </param>
        /// <param name="parallelResistance"> The parallel resistance. </param>
        /// <returns> A Double. </returns>
        public static double ToEquivalentResistance( double seriesResistance, decimal parallelResistance )
        {
            return parallelResistance <= 0m
                ? throw new ArgumentException( $"Value {parallelResistance} must be positive", nameof( parallelResistance ) )
                : seriesResistance < 0d
                ? throw new ArgumentException( $"Value {seriesResistance} must not be negative", nameof( seriesResistance ) )
                : Resistor.ToParallel( seriesResistance, ( double ) parallelResistance );
        }

        #endregion

        #region " NOMINAL VOLTAGE CONVERTERS "

        /// <summary>
        /// Converts an open load voltage of an attenuated voltage source to the nominal voltage of a non-
        /// attenuated voltage source.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="openLoadVoltage">     The open load voltage. </param>
        /// <param name="seriesResistance">    The series resistance. </param>
        /// <param name="parallelConductance"> The parallel conductance. </param>
        /// <returns> The given data converted to a Double. </returns>
        public static double ToNominalVoltage( double openLoadVoltage, double seriesResistance, double parallelConductance )
        {
            return parallelConductance == 0d || seriesResistance == 0d ? openLoadVoltage : openLoadVoltage * ToAttenuation( seriesResistance, parallelConductance );
        }

        /// <summary>
        /// Converts nominal voltage of a non-attenuated voltage source to an open load voltage or the
        /// equivalent attenuated voltage source.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="nominalVoltage">      The nominal voltage. </param>
        /// <param name="seriesResistance">    The series resistance. </param>
        /// <param name="parallelConductance"> The parallel conductance. </param>
        /// <returns> The given data converted to a Double. </returns>
        public static double ToOpenLoadVoltage( decimal nominalVoltage, double seriesResistance, double parallelConductance )
        {
            return parallelConductance == 0d || seriesResistance == 0d ? ( double ) nominalVoltage : ( double ) nominalVoltage / ToAttenuation( seriesResistance, parallelConductance );
        }

        /// <summary>
        /// Converts nominal voltage of a non-attenuated voltage source to an attenuated voltage source
        /// with the specified seres and parallel resistances.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="nominalVoltage">      The nominal voltage. </param>
        /// <param name="seriesResistance">    The series resistance. </param>
        /// <param name="parallelConductance"> The parallel conductance. </param>
        /// <returns> The given data converted to an AttenuatedVoltageSource. </returns>
        public static new AttenuatedVoltageSource ToAttenuatedVoltageSource( decimal nominalVoltage, double seriesResistance, double parallelConductance )
        {
            return new AttenuatedVoltageSource( ToOpenLoadVoltage( nominalVoltage, seriesResistance, parallelConductance ), seriesResistance, parallelConductance );
        }

        #endregion

        #region " VOLTAGE SOURCE CONVERTERS "

        /// <summary>
        /// Build a voltage source using the open load voltage and equivalent resistance for the given
        /// resistance and conductance values.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="openLoadVoltage">     The open load voltage. </param>
        /// <param name="seriesResistance">    The series resistance. </param>
        /// <param name="parallelConductance"> The parallel conductance. </param>
        /// <returns> The given data converted to a VoltageSource. </returns>
        public static VoltageSource ToVoltageSource( double openLoadVoltage, double seriesResistance, double parallelConductance )
        {
            return new VoltageSource( openLoadVoltage, ToEquivalentResistance( seriesResistance, parallelConductance ) );
        }

        /// <summary>
        /// Build a voltage source using the nominal voltage and equivalent resistance for the given
        /// resistance and conductance values.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="nominalVoltage">      The nominal voltage. </param>
        /// <param name="seriesResistance">    The series resistance. </param>
        /// <param name="parallelConductance"> The parallel conductance. </param>
        /// <returns> The given data converted to a VoltageSource. </returns>
        public static VoltageSource ToVoltageSource( decimal nominalVoltage, double seriesResistance, double parallelConductance )
        {
            return new VoltageSource( ToOpenLoadVoltage( nominalVoltage, seriesResistance, parallelConductance ), ToEquivalentResistance( seriesResistance, parallelConductance ) );
        }

        /// <summary>
        /// Converts the attenuated voltage source to a voltage source with equivalent resistance and
        /// open load voltage.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> The given data converted to a VoltageSource. </returns>
        public VoltageSource ToVoltageSource()
        {
            return new VoltageSource( this.Voltage, this.Resistance );
        }

        /// <summary> Update this object from a voltage source. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="voltageSource">  The voltage source. </param>
        /// <param name="nominalVoltage"> The nominal voltage. </param>
        public void FromVoltageSource( VoltageSource voltageSource, decimal nominalVoltage )
        {
            if ( voltageSource is null )
            {
                throw new ArgumentNullException( nameof( voltageSource ) );
            }

            double attenuation = ToAttenuation( nominalVoltage, voltageSource.Voltage );
            this.Initialize( new VoltageSource( ( double ) nominalVoltage, voltageSource.Resistance ), attenuation );
        }

        #endregion

        #region " CURRENT SOURCE CONVERTERS "

        /// <summary> Updates this object from an Attenuated current source. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="currentSource">  The current source. </param>
        /// <param name="nominalVoltage"> The nominal voltage. </param>
        public void FromAttenuatedCurrentSource( AttenuatedCurrentSource currentSource, decimal nominalVoltage )
        {
            if ( currentSource is null )
            {
                throw new ArgumentNullException( nameof( currentSource ) );
            }

            double voltage = currentSource.Current / currentSource.Conductance;
            double attenuation = ( double ) nominalVoltage / voltage;
            this.Initialize( new VoltageSource( voltage, 1d / currentSource.Conductance ), attenuation );
        }

        /// <summary> Converts a nominalCurrent to an Attenuated current source. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="nominalCurrent"> The nominal current. </param>
        /// <returns> NominalCurrent as an AttenuatedCurrentSource. </returns>
        public AttenuatedCurrentSource ToAttenuatedCurrentSource( double nominalCurrent )
        {
            this.ValidateAttenuatedCurrentSourceConversion( nominalCurrent );
            return this.ToCurrentSource().ToAttenuatedCurrentSource( ( decimal ) nominalCurrent );
        }

        /// <summary>
        /// Validates the attenuated current source conversion described by nominalCurrent.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="nominalCurrent"> The nominal current. </param>
        public void ValidateAttenuatedCurrentSourceConversion( double nominalCurrent )
        {
            double shortLoadCurrent = this.LoadCurrent( Resistor.ShortResistance );
            if ( shortLoadCurrent > nominalCurrent )
            {
                throw new InvalidOperationException( $"Voltage source with an short load current of {shortLoadCurrent:G4} cannot be converted to a current source with a lower nominal current of {nominalCurrent:G4}" );
            }
        }

        #endregion

    }
}
