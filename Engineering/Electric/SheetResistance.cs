using System;

namespace isr.Core.Engineering
{

    /// <summary> A sheet resistance. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-06-28 </para>
    /// </remarks>
    public class SheetResistance : Resistance
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        public SheetResistance() : base()
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="sheetResistance"> The sheet resistance or <see cref="Double.NaN"/> if no value. </param>
        public SheetResistance( SheetResistance sheetResistance ) : base( sheetResistance )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="voltage"> The voltage. </param>
        /// <param name="current"> The current. </param>
        public SheetResistance( double voltage, double current ) : base( voltage, current )
        {
        }

        /// <summary> Gets the sheet resistance. </summary>
        /// <value> The sheet resistance or <see cref="Double.NaN"/> if no value. </value>
        public double SheetResistanceValue => this.HasValue ? ToSheetResistance( this.ResistanceValue ) : double.NaN;

        /// <summary> Gets the Pi over log two. </summary>
        /// <value> The pi over log two. </value>
        private static double PiOverLogTwo { get; set; } = Math.PI / Math.Log( 2d );

        /// <summary> Converts a resistance to a sheet resistance. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="resistance"> The resistance. </param>
        /// <returns> Resistance as a Double. </returns>
        public static double ToSheetResistance( double resistance )
        {
            return resistance * PiOverLogTwo;
        }
    }

    /// <summary> Collection of sheet resistances. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-04-21 </para>
    /// </remarks>
    public class SheetResistanceCollection : System.Collections.ObjectModel.Collection<SheetResistance>
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        public SheetResistanceCollection() : base()
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="sheetResistances"> The sheet resistances. </param>
        public SheetResistanceCollection( SheetResistanceCollection sheetResistances ) : this()
        {
            if ( sheetResistances is object )
            {
                foreach ( SheetResistance sr in sheetResistances )
                {
                    this.Add( new SheetResistance( sr ) );
                }
            }
        }

        /// <summary> Gets the number of sheet resistances. </summary>
        /// <value> The number of sheet resistances. </value>
        public int SheetResistanceCount { get; private set; }

        /// <summary> Gets the sheet resistance. </summary>
        /// <value>
        /// The sheet resistance or <see cref="Double.NaN"/> if <see cref="SheetResistanceCount"/> is
        /// zero.
        /// </value>
        public double? SheetResistance
        {
            get {
                double sum = 0d;
                int count = 0;
                foreach ( SheetResistance sr in this )
                {
                    if ( sr.HasValue )
                    {
                        sum += sr.SheetResistanceValue;
                        count += 1;
                    }
                }

                this.SheetResistanceCount = count;
                return count > 0 ? sum / this.Count : new double?();
            }
        }

        /// <summary> Gets the average measured Voltage. </summary>
        /// <value>
        /// The average measured Voltage or <see cref="Double.NaN"/> if
        /// <see cref="SheetResistanceCount"/> is zero.
        /// </value>
        public double? AverageAbsoluteVoltage
        {
            get {
                double sum = 0d;
                int count = 0;
                foreach ( SheetResistance sr in this )
                {
                    if ( sr.HasValue )
                    {
                        sum += Math.Abs( sr.Voltage );
                        count += 1;
                    }
                }

                return count > 0 ? sum / this.Count : new double?();
            }
        }

        /// <summary> Gets the average source current. </summary>
        /// <value>
        /// The average source current or <see cref="Double.NaN"/> if <see cref="SheetResistanceCount"/>
        /// is zero.
        /// </value>
        public double? AbsoluteCurrent
        {
            get {
                double sum = 0d;
                int count = 0;
                foreach ( SheetResistance sr in this )
                {
                    if ( sr.HasValue )
                    {
                        sum += Math.Abs( sr.Current );
                        count += 1;
                    }
                }

                return count > 0 ? sum / this.Count : new double?();
            }
        }
    }
}
