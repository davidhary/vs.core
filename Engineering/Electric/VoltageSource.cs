using System;

namespace isr.Core.Engineering
{

    /// <summary> A voltage source. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-09-15 </para>
    /// </remarks>
    public class VoltageSource
    {

        #region " CONSTRUCTION "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="voltage">    The voltage. </param>
        /// <param name="resistance"> The resistance. </param>
        public VoltageSource( double voltage, double resistance ) : base()
        {
            this.InitializeThis( voltage, resistance );
        }

        /// <summary> Cloning Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="voltageSource"> The voltage source. </param>
        public VoltageSource( VoltageSource voltageSource ) : this( ValidatedVoltageSource( voltageSource ).Voltage, voltageSource.Resistance )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="currentSource"> The current source. </param>
        public VoltageSource( CurrentSource currentSource ) : this( CurrentSource.ValidatedCurrentSource( currentSource ).ToVoltageSource() )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="voltageSource"> The voltage source. </param>
        public VoltageSource( AttenuatedVoltageSource voltageSource ) : this( AttenuatedVoltageSource.ValidatedVoltageSource( voltageSource ).ToCurrentSource() )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="currentSource"> The current source. </param>
        public VoltageSource( AttenuatedCurrentSource currentSource ) : this( AttenuatedCurrentSource.ValidatedCurrentSource( currentSource ).ToVoltageSource() )
        {
        }

        /// <summary> Validated voltage source. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="voltageSource"> The voltage source. </param>
        /// <returns> A VoltageSource. </returns>
        public static VoltageSource ValidatedVoltageSource( VoltageSource voltageSource )
        {
            return voltageSource is null ? throw new ArgumentNullException( nameof( voltageSource ) ) : voltageSource;
        }

        /// <summary> Initializes this object. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="voltage">    The voltage. </param>
        /// <param name="resistance"> The resistance. </param>
        private void InitializeThis( double voltage, double resistance )
        {
            this.Voltage = voltage;
            this.Resistance = resistance;
        }

        /// <summary> Initialized. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="voltage">    The voltage. </param>
        /// <param name="resistance"> The resistance. </param>
        public void Initialize( double voltage, double resistance )
        {
            this.InitializeThis( voltage, resistance );
        }

        /// <summary> Initialized. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="voltageSource"> The voltage source. </param>
        public void Initialize( VoltageSource voltageSource )
        {
            if ( voltageSource is null )
            {
                throw new ArgumentNullException( nameof( voltageSource ) );
            }

            this.InitializeThis( voltageSource.Voltage, voltageSource.Resistance );
        }

        #endregion

        #region " EQUALS "

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
        /// <see cref="T:System.Object" />. </param>
        /// <returns>
        /// <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />; otherwise, <c>False</c>.
        /// </returns>
        public override bool Equals( object obj )
        {
            return obj is object && ReferenceEquals( this.GetType(), obj.GetType() ) && this.Equals( ( VoltageSource ) obj );
        }

        /// <summary>
        /// Compares two Voltage Sources. The Voltage Sources are compared using their resistances and
        /// voltages.
        /// </summary>
        /// <remarks>
        /// The Voltage Sources are the same if the have the same Voltage and Resistance.
        /// </remarks>
        /// <param name="other"> Specifies the other <see cref="VoltageSource">Voltage Source</see>
        /// to compare for equality with this instance. </param>
        /// <returns> A Boolean data type. </returns>
        public bool Equals( VoltageSource other )
        {
            return !(other is null) && this.Voltage.Equals( other.Voltage ) && this.Resistance.Equals( other.Resistance );
        }

        /// <summary>
        /// Compares two Voltage Sources. The Voltage Sources are compared using their resistance and
        /// voltage.
        /// </summary>
        /// <remarks>
        /// The Voltage Sources are the same if the have the same Voltage and Resistance.
        /// </remarks>
        /// <param name="other">     Specifies the other <see cref="VoltageSource">Voltage Source</see>
        /// to compare for equality with this instance. </param>
        /// <param name="tolerance"> Specifies the relative tolerance for comparing the two values. The
        /// values are compared based on their end points. </param>
        /// <returns> A Boolean data type. </returns>
        public bool Equals( VoltageSource other, double tolerance )
        {
            return !(other is null) && (this.Equals( other ) || Math.Abs( this.Voltage - other.Voltage ) <= other.Voltage * tolerance && Math.Abs( this.Resistance - other.Resistance ) <= other.Resistance * tolerance);
        }

        /// <summary> Implements the operator =. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool Equals( VoltageSource left, VoltageSource right )
        {
            return ReferenceEquals( left, right ) || left is object && left.Equals( right );
        }

        /// <summary>
        /// Compares two Voltage Sources. The Voltage Sources are compared using their resistance and
        /// voltage.
        /// </summary>
        /// <remarks>
        /// The Voltage Sources are the same if the have the same Voltage and Resistance.
        /// </remarks>
        /// <param name="left">      Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right">     Specifies the right hand side argument of the binary operation. </param>
        /// <param name="tolerance"> Specifies the relative tolerance for comparing the two values. The
        /// values are compared based on their end points. </param>
        /// <returns> A Boolean data type. </returns>
        public static bool Equals( VoltageSource left, VoltageSource right, double tolerance )
        {
            return left is null && right is null || left is object && left.Equals( right, tolerance );
        }

        /// <summary> Implements the operator =. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( VoltageSource left, VoltageSource right )
        {
            return Equals( left, right );
        }

        /// <summary> Implements the operator &lt;&gt;. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( VoltageSource left, VoltageSource right )
        {
            return !Equals( left, right );
        }

        /// <summary> Creates a unique hash code. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> An <see cref="System.Int32">Int32</see> value. </returns>
        public override int GetHashCode()
        {
            return this.Voltage.GetHashCode() ^ this.Resistance.GetHashCode();
        }

        #endregion

        #region " TO STRING "

        /// <summary> Returns a string that represents the current object. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> A string that represents the current object. </returns>
        public override string ToString()
        {
            return $"{this.Voltage}:{this.Resistance}";
        }

        /// <summary> Returns a string that represents the current object. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="voltageFormat">    The voltage format. </param>
        /// <param name="resistanceFormat"> The resistance format. </param>
        /// <returns> A string that represents the current object. </returns>
        public string ToString( string voltageFormat, string resistanceFormat )
        {
            return $"{string.Format( voltageFormat, this.Voltage )}:{string.Format( resistanceFormat, this.Resistance )}";
        }

        #endregion

        #region " COMPONENETS "

        /// <summary> Gets or sets the voltage source voltage. This is the open load voltage. </summary>
        /// <value> The voltage. </value>
        public double Voltage { get; set; }

        /// <summary> Gets or sets the voltage source equivalent resistance. </summary>
        /// <value> The resistance. </value>
        public double Resistance { get; set; }

        #endregion

        #region " OUTPUT "

        /// <summary> Returns the load voltage. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="loadResistance"> The load resistance. </param>
        /// <returns> The voltage. </returns>
        public double LoadVoltage( double loadResistance )
        {
            return double.IsInfinity( loadResistance )
                ? this.Voltage
                : loadResistance == 0d ? 0d : loadResistance * this.LoadCurrent( loadResistance );
        }

        /// <summary> Returns the load current. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="loadResistance"> The load resistance. </param>
        /// <returns> The current. </returns>
        public double LoadCurrent( double loadResistance )
        {
            return double.IsInfinity( loadResistance ) ? 0d : this.Voltage / (loadResistance + this.Resistance);
        }

        #endregion

        #region " CONVERTERS "

        /// <summary> Converts this object to a current source. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="voltage">    The voltage. </param>
        /// <param name="resistance"> The resistance. </param>
        /// <returns> This object as a CurrentSource. </returns>
        public static CurrentSource ToCurrentSource( double voltage, double resistance )
        {
            double conductance = 1d / resistance;
            return new CurrentSource( voltage * conductance, conductance );
        }

        /// <summary> Converts this object to a current source. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="voltageSource"> The voltage source. </param>
        /// <returns> This object as a CurrentSource. </returns>
        public static CurrentSource ToCurrentSource( VoltageSource voltageSource )
        {
            return voltageSource is null ? throw new ArgumentNullException( nameof( voltageSource ) ) : voltageSource.ToCurrentSource();
        }

        /// <summary> Converts this object to a current source. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> This object as a CurrentSource. </returns>
        public CurrentSource ToCurrentSource()
        {
            double conductance = 1d / this.Resistance;
            return new CurrentSource( this.Voltage * conductance, conductance );
        }

        /// <summary> Initializes this object from the given from current source. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="current">     The current. </param>
        /// <param name="conductance"> The conductance. </param>
        /// <returns> A VoltageSource. </returns>
        public static VoltageSource FromCurrentSource( double current, double conductance )
        {
            double resistance = 1d / conductance;
            return new VoltageSource( current * resistance, resistance );
        }

        /// <summary> Initializes this object from the given from current source. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="currentSource"> The current source. </param>
        public void FromCurrentSource( CurrentSource currentSource )
        {
            if ( currentSource is null )
            {
                throw new ArgumentNullException( nameof( currentSource ) );
            }

            this.Resistance = 1d / currentSource.Conductance;
            this.Voltage = currentSource.Current * this.Resistance;
        }

        #endregion

        #region " ATTENUATED VOLTAGE SOURCE CONVERTERS "

        /// <summary>
        /// Builds an attenuated voltage source using the voltage source and attenuation.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="voltageSource"> The voltage source. </param>
        /// <param name="attenuation">   The attenuation. </param>
        /// <returns> The given data converted to an AttenuatedVoltageSource. </returns>
        public static AttenuatedVoltageSource ToAttenuatedVoltageSource( VoltageSource voltageSource, double attenuation )
        {
            return voltageSource is null
                ? throw new ArgumentNullException( nameof( voltageSource ) )
                : attenuation < AttenuatedVoltageSource.MinimumAttenuation
                ? throw new ArgumentOutOfRangeException( nameof( attenuation ), $"Value must be greater or equal to {AttenuatedVoltageSource.MinimumAttenuation}" )
                : voltageSource.ToAttenuatedVoltageSource( attenuation );
        }

        /// <summary> Converts this object to an Attenuated voltage source. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="attenuation"> The attenuation. </param>
        /// <returns> The given data converted to an AttenuatedVoltageSource. </returns>
        public AttenuatedVoltageSource ToAttenuatedVoltageSource( double attenuation )
        {
            if ( attenuation < AttenuatedVoltageSource.MinimumAttenuation )
            {
                throw new ArgumentOutOfRangeException( nameof( attenuation ), $"Value must be greater or equal to {AttenuatedVoltageSource.MinimumAttenuation}" );
            }

            double resistance = this.Resistance;
            double seriesResistance = attenuation * resistance;
            double conductance = 0d;
            if ( attenuation > AttenuatedVoltageSource.MinimumAttenuation )
            {
                conductance = (attenuation - 1d) / seriesResistance;
            }

            return new AttenuatedVoltageSource( this.Voltage, seriesResistance, conductance );
        }

        /// <summary>
        /// Converts this voltage source to an attenuated voltage source with the specified nominal
        /// voltage.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="nominalVoltage"> The nominal voltage. </param>
        /// <returns> NominalVoltage as an AttenuatedVoltageSource. </returns>
        public AttenuatedVoltageSource ToAttenuatedVoltageSource( decimal nominalVoltage )
        {
            return this.ToAttenuatedVoltageSource( ( double ) nominalVoltage / this.Voltage );
        }

        /// <summary>
        /// Builds an attenuated voltage source using the voltage source and nominal voltage.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="voltageSource">  The voltage source. </param>
        /// <param name="nominalVoltage"> The nominal voltage. </param>
        /// <returns> The given data converted to an AttenuatedVoltageSource. </returns>
        public static AttenuatedVoltageSource ToAttenuatedVoltageSource( VoltageSource voltageSource, decimal nominalVoltage )
        {
            return voltageSource is null
                ? throw new ArgumentNullException( nameof( voltageSource ) )
                : voltageSource.ToAttenuatedVoltageSource( nominalVoltage );
        }

        /// <summary>
        /// Builds an attenuated voltage source using the nominal voltage, equivalent resistance and
        /// attenuation.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="nominalVoltage"> The nominal voltage. </param>
        /// <param name="resistance">     The resistance. </param>
        /// <param name="attenuation">    The attenuation. The ratio of the nominal voltage to the source
        /// voltage over open load. </param>
        /// <returns> The given data converted to a VoltageSource. </returns>
        public static AttenuatedVoltageSource ToAttenuatedVoltageSource( decimal nominalVoltage, double resistance, double attenuation )
        {
            return new VoltageSource( ( double ) ( decimal ) (( double ) nominalVoltage / attenuation), resistance ).ToAttenuatedVoltageSource( attenuation );
        }


        #endregion

    }
}
