using System;
using System.Collections.Generic;
using System.Linq;

namespace isr.Core.Engineering
{

    /// <summary> Delta Wye Computation class. </summary>
    /// <remarks>
    /// <para>Delta: (a) -- R1 -- (b) -- R3 -- (c) -- R2 -- (a)</para>
    /// <para>Wye: (a) -- Ra -- (x) -- Rb -- (b) (c) -- Rc -- (x) where x is the center. </para> (c)
    /// 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-02-18 </para>
    /// </remarks>
    public sealed class DeltaWye
    {

        #region " CONSTRUCTOR "

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        private DeltaWye() : base()
        {
        }

        #endregion

        #region " CONVERSION FUNCTIONS "

        /// <summary> Delta to Wye. </summary>
        /// <remarks> Ra = R1*R2/Sum(R); Rb = R1*R2/sum(R); Rc = R2*R3/sum(R). </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="values"> The values. </param>
        /// <returns> A Double() </returns>
        public static double[] DeltaToWye( double[] values )
        {
            if ( values is null )
            {
                throw new ArgumentNullException( nameof( values ) );
            }

            if ( values.Count() < 3 )
            {
                throw new InvalidOperationException( "Delta array must include at least three elements" );
            }

            double sum = 0d;
            for ( int i = 0; i <= 2; i++ )
            {
                sum += values[i];
            }

            return sum == 0d
                ? throw new InvalidOperationException( "Delta array must include at least one non-zero value" )
                : (new[] { values[0] * values[1] / sum, values[0] * values[2] / sum, values[1] * values[2] / sum });
        }

        /// <summary> Wye to delta. </summary>
        /// <remarks> R1 = Sum(RxR)/Rc; R2 = sum(RxR)/Rb; Rc =sum(RxR)/Ra. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="values"> The values. </param>
        /// <returns> A Double() </returns>
        public static double[] WyeToDelta( double[] values )
        {
            if ( values is null )
            {
                throw new ArgumentNullException( nameof( values ) );
            }

            if ( values.Count() < 3 )
            {
                throw new InvalidOperationException( "Wye array must include at least three elements" );
            }

            var deltas = new List<double>();
            double sum = values[0] * values[1];
            sum += values[0] * values[2];
            sum += values[1] * values[2];
            for ( int i = 0; i <= 2; i++ )
            {
                if ( values[i] == 0d )
                {
                    throw new InvalidOperationException( $"Wye array must not include zero values--zero at {i}" );
                }
                else
                {
                    deltas.Add( sum / values[2 - i] );
                }
            }

            return deltas.ToArray();
        }

        /// <summary> Delta to measured. </summary>
        /// <remarks> Converts delta circuit values to measured values. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="values"> The values. </param>
        /// <returns> A Double() </returns>
        public static double[] DeltaToMeasured( double[] values )
        {
            if ( values is null )
            {
                throw new ArgumentNullException( nameof( values ) );
            }

            if ( values.Count() < 3 )
            {
                throw new InvalidOperationException( "Delta array must include at least three elements" );
            }
            // convert delta to Wye.
            var wyes = DeltaToWye( values );
            // add the Wye values
            return new[] { wyes[0] + wyes[1], wyes[0] + wyes[2], wyes[1] + wyes[2] };
        }

        /// <summary> Calculates a delta value from measured values. </summary>
        /// <remarks> Calculates the delta loop resistance from the delta measured values. </remarks>
        /// <param name="x"> The x coordinate. </param>
        /// <param name="y"> The y coordinate. </param>
        /// <param name="z"> The z coordinate. </param>
        /// <returns> The calculated delta Wye element. </returns>
        private static double MeasuredToDelta( double x, double y, double z )
        {
            if ( y + z - x == 0d )
            {
                return double.NaN;
            }
            else
            {
                // Return x + (x * x - (y - z) * (y - z)) / (2 * (y + z - x))
                return x + (2d * y * z + x * x - y * y - z * z) / (2d * (y + z - x));
            }
        }

        /// <summary> Calculates a delta value from measured values. </summary>
        /// <remarks> Calculates the delta loop resistances from the delta measured values. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="values"> The values. </param>
        /// <returns> The calculated delta Wye element. </returns>
        public static double[] MeasuredToDelta( double[] values )
        {
            if ( values is null )
            {
                throw new ArgumentNullException( nameof( values ) );
            }

            if ( values.Count() < 3 )
            {
                throw new InvalidOperationException( "Measured delta loop array must include at least three elements" );
            }

            var deltas = new List<double>();
            for ( int i = 0; i <= 2; i++ )
            {
                deltas.Add( MeasuredToDelta( values[i], values[(i + 1) % 3], values[(i + 2) % 3] ) );
            }

            return deltas.ToArray();
        }

        /// <summary> Calculates a delta value from measured values. </summary>
        /// <remarks>
        /// When emulating, the values provided are assumed to be the desired delta values. Therefore,
        /// the values are first converted to measured values. This allows to validate the conversion
        /// equations.
        /// </remarks>
        /// <param name="values">  The values. </param>
        /// <param name="emulate"> true to emulate. </param>
        /// <returns> The calculated delta Wye element. </returns>
        public static double[] MeasuredToDelta( double[] values, bool emulate )
        {
            if ( emulate )
            {
                values = DeltaToMeasured( values );
            }

            return MeasuredToDelta( values );
        }

        #endregion

    }
}
