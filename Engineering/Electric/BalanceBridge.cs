using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace isr.Core.Engineering
{

    /// <summary> A Wheatstone bridge with balancing components. </summary>
    /// <remarks>
    /// Without loss of generality, the balance bridge layout is defined by assigning series and
    /// parallel (shunt) elements to each of the compensated edges of a 'Naked' Wheatstone bridge.
    /// <para>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-09-15 </para>
    /// </remarks>
    public class BalanceBridge : Wheatstone
    {

        #region " CONSTRUCTION "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-05. </remarks>
        /// <param name="nakedBridge">   The naked bridge. </param>
        /// <param name="balanceValues"> The values. </param>
        /// <param name="balanceLayout"> The layout. </param>
        public BalanceBridge( Wheatstone nakedBridge, IEnumerable<double> balanceValues, BalanceLayout balanceLayout ) : base( ToWheatstone( ValidatedBridge( nakedBridge ), ValidatedBalanceValues( balanceValues ), balanceLayout ) )
        {
            this._NakedBridge = ValidatedBridge( nakedBridge );
            this._BridgeBalance = new BridgeBalance( balanceValues, balanceLayout );
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="nakedBridge"> The naked bridge. </param>
        /// <param name="layout">      The layout. </param>
        public BalanceBridge( Wheatstone nakedBridge, BalanceLayout layout ) : this( nakedBridge, new double[] { 0d, 0d, 0d, 0d }, layout )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="nakedBridge">   The naked bridge. </param>
        /// <param name="bridgeBalance"> The bridge balance. </param>
        public BalanceBridge( Wheatstone nakedBridge, BridgeBalance bridgeBalance ) : base( ToWheatstone( ValidatedBridge( nakedBridge ), BridgeBalance.ValidatedBridgeBalance( bridgeBalance ) ) )
        {
            this._NakedBridge = ValidatedBridge( nakedBridge );
            this._BridgeBalance = bridgeBalance;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="bridge"> The bridge. </param>
        public BalanceBridge( BalanceBridge bridge ) : this( new Wheatstone( ValidatedBalanceBridge( bridge ).NakedBridge ), BridgeBalance.ValidatedBridgeBalance( bridge.BridgeBalance ) )
        {
        }

        /// <summary> Makes a deep copy of this object. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> A copy of this object. </returns>
        public BalanceBridge Clone()
        {
            return new BalanceBridge( this );
        }

        /// <summary> Validated balance bridge. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="bridge"> The bridge. </param>
        /// <returns> A BalanceBridge. </returns>
        public static BalanceBridge ValidatedBalanceBridge( BalanceBridge bridge )
        {
            return bridge is null
                ? throw new ArgumentNullException( nameof( bridge ) )
                : bridge.BridgeBalance is null ? throw new ArgumentNullException( nameof( bridge ) ) : bridge;
        }

        /// <summary> Enumerates validated balance values in this collection. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="values"> The values. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process validated balance values in this
        /// collection.
        /// </returns>
        public static IEnumerable<double> ValidatedBalanceValues( IEnumerable<double> values )
        {
            return values is null
                ? throw new ArgumentNullException( nameof( values ) )
                : values.Count() != 4 ? throw new InvalidOperationException( $"{nameof( values )} must have 4 elements" ) : values;
        }

        #endregion

        #region " EQUALS "

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
        /// <see cref="T:System.Object" />. </param>
        /// <returns>
        /// <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />; otherwise, <c>False</c>.
        /// </returns>
        public override bool Equals( object obj )
        {
            return obj is object && ReferenceEquals( this.GetType(), obj.GetType() ) && this.Equals( ( BalanceBridge ) obj );
        }

        /// <summary>
        /// Compares two bridges. The bridges are compared using their LowerLimits and UpperLimits.
        /// </summary>
        /// <remarks> The bridges are the same if the have the same naked bridge and balance. </remarks>
        /// <param name="other"> Specifies the other <see cref="BalanceBridge">Balance Bridge</see>
        /// to compare for equality with this instance. </param>
        /// <returns> A Boolean data type. </returns>
        public bool Equals( BalanceBridge other )
        {
            return !(other is null) && (!(this.NakedBridge is null) && !(other.NakedBridge is null)
                    && (!(this.BridgeBalance is null) && !(other.BridgeBalance is null)
                    && this.NakedBridge.Equals( other.NakedBridge ) && this.BridgeBalance.Equals( other.BridgeBalance )));
        }

        /// <summary>
        /// Compares two bridges. The bridges are compared using their LowerLimits and UpperLimits.
        /// </summary>
        /// <remarks> The bridges are the same if the have the same naked bridge and balance. </remarks>
        /// <param name="other">     Specifies the other <see cref="BalanceBridge">Balance Bridge</see>
        /// to compare for equality with this instance. </param>
        /// <param name="tolerance"> Specifies the relative tolerance for comparing the two values. The
        /// values are compared based on their end points. </param>
        /// <returns> A Boolean data type. </returns>
        public bool Equals( BalanceBridge other, double tolerance )
        {
            return !(other is null) && !(this.NakedBridge is null) && !(other.NakedBridge is null) && (!(this.BridgeBalance is null) && !(other.BridgeBalance is null)
                    && this.BridgeBalance.Equals( other.BridgeBalance, tolerance )
                    && this.NakedBridge.Equals( other.NakedBridge, tolerance ) && Wheatstone.Equals( this, other, tolerance ));
        }

        /// <summary> Implements the operator =. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool Equals( BalanceBridge left, BalanceBridge right )
        {
            return ReferenceEquals( left, right ) || left is object && left.Equals( right );
        }

        /// <summary>
        /// Compares two bridges. The bridges are compared using their LowerLimits and UpperLimits.
        /// </summary>
        /// <remarks> The bridges are the same if the have the same naked bridge and balance. </remarks>
        /// <param name="left">      Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right">     Specifies the right hand side argument of the binary operation. </param>
        /// <param name="tolerance"> Specifies the relative tolerance for comparing the two values. The
        /// values are compared based on their end points. </param>
        /// <returns> A Boolean data type. </returns>
        public static bool Equals( BalanceBridge left, BalanceBridge right, double tolerance )
        {
            return left is null && right is null || left is object && left.Equals( right, tolerance );
        }

        /// <summary> Implements the operator =. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( BalanceBridge left, BalanceBridge right )
        {
            return Equals( left, right );
        }

        /// <summary> Implements the operator &lt;&gt;. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( BalanceBridge left, BalanceBridge right )
        {
            return !Equals( left, right );
        }

        /// <summary> Creates a unique hash code. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> An <see cref="System.Int32">Int32</see> value. </returns>
        public override int GetHashCode()
        {
            return this.NakedBridge.GetHashCode() ^ this.BridgeBalance.GetHashCode();
        }

        #endregion

        #region " CONVERTERS "

        /// <summary> Converts this object to a Wheatstone bridge. </summary>
        /// <remarks> David, 2020-09-05. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="nakedBridge">   The naked bridge. </param>
        /// <param name="balanceValues"> The balance values. </param>
        /// <param name="balanceLayout"> The balance layout. </param>
        /// <returns> The given data converted to a Wheatstone. </returns>
        public static Wheatstone ToWheatstone( Wheatstone nakedBridge, IEnumerable<double> balanceValues, BalanceLayout balanceLayout )
        {
            if ( nakedBridge is null )
            {
                throw new ArgumentNullException( nameof( nakedBridge ) );
            }

            if ( balanceValues is null )
            {
                throw new ArgumentNullException( nameof( balanceValues ) );
            }

            switch ( balanceLayout )
            {
                case BalanceLayout.TopShuntBottomSeries:
                    {
                        return new Wheatstone( Resistor.ShuntConductor( nakedBridge.TopRight, balanceValues.ElementAtOrDefault( ( int ) TopShuntBottomSeriesIndex.TopRightShunt ) ), balanceValues.ElementAtOrDefault( ( int ) TopShuntBottomSeriesIndex.BottomRightSeries ) + nakedBridge.BottomRight, balanceValues.ElementAtOrDefault( ( int ) TopShuntBottomSeriesIndex.BottomLeftSeries ) + nakedBridge.BottomLeft, Resistor.ShuntConductor( nakedBridge.TopLeft, balanceValues.ElementAtOrDefault( ( int ) TopShuntBottomSeriesIndex.TopLeftShunt ) ) );
                    }

                case BalanceLayout.RightShuntRightSeries:
                    {
                        return new Wheatstone( Resistor.ShuntConductor( nakedBridge.TopRight, balanceValues.ElementAtOrDefault( ( int ) RightShuntRightSeriesIndex.TopRightShunt ) ) + balanceValues.ElementAtOrDefault( ( int ) RightShuntRightSeriesIndex.TopRightSeries ), Resistor.ShuntConductor( nakedBridge.BottomRight, balanceValues.ElementAtOrDefault( ( int ) RightShuntRightSeriesIndex.BottomRightShunt ) ) + balanceValues.ElementAtOrDefault( ( int ) RightShuntRightSeriesIndex.BottomRightSeries ), nakedBridge.BottomLeft, nakedBridge.TopLeft );
                    }

                default:
                    {
                        throw new InvalidOperationException( $"Unhandled layout {balanceLayout}" );
                    }
            }
        }

        /// <summary> Converts this object to a Wheatstone bridge. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="nakedBridge">   The naked bridge. </param>
        /// <param name="bridgeBalance"> The bridge balance. </param>
        /// <returns> The given data converted to a Wheatstone. </returns>
        public static Wheatstone ToWheatstone( Wheatstone nakedBridge, BridgeBalance bridgeBalance )
        {
            return nakedBridge is null
                ? throw new ArgumentNullException( nameof( nakedBridge ) )
                : bridgeBalance is null
                ? throw new ArgumentNullException( nameof( bridgeBalance ) )
                : ToWheatstone( nakedBridge, bridgeBalance.Values, bridgeBalance.Layout );
        }

        #endregion

        #region " COMPONENETS "

        /// <summary> The bridge balance. </summary>
        private BridgeBalance _BridgeBalance;

        /// <summary> Gets or sets the bridge balance. </summary>
        /// <value> The bridge balance. </value>
        public BridgeBalance BridgeBalance
        {
            get => this._BridgeBalance;

            set {
                if ( value is object && !value.Equals( this.BridgeBalance ) )
                {
                    this._BridgeBalance = value;
                    this.Initialize( ToWheatstone( this.NakedBridge, this.BridgeBalance ) );
                }
            }
        }

        /// <summary> The naked bridge. </summary>
        private Wheatstone _NakedBridge;

        /// <summary> Gets or sets the naked bridge. </summary>
        /// <value> The naked bridge. </value>
        public Wheatstone NakedBridge
        {
            get => this._NakedBridge;

            set {
                if ( value is object && !value.Equals( this.NakedBridge ) )
                {
                    this._NakedBridge = value;
                    this.Initialize( ToWheatstone( this.NakedBridge, this.BridgeBalance ) );
                }
            }
        }

        /// <summary> Query if the bridge is valid. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> <c>true</c> if valid; otherwise <c>false</c> </returns>
        public override bool IsValid()
        {
            return base.IsValid() && this.BridgeBalance.IsValid();
        }

        #endregion

        #region " COMPENSATORS "

        /// <summary> Computes shunt only compensation for the bridge. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="bridge">        The bridge. </param>
        /// <param name="balanceLayout"> The balance layout. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the shunt compensations in this
        /// collection.
        /// </returns>
        public static IEnumerable<double> ShuntCompensation( Wheatstone bridge, BalanceLayout balanceLayout )
        {
            if ( bridge is null )
            {
                throw new ArgumentNullException( nameof( bridge ) );
            }

            var values = new double[] { 0d, 0d, 0d, 0d };
            if ( balanceLayout == BalanceLayout.TopShuntBottomSeries )
            {
                if ( bridge.ProductImbalance > 0d )
                {
                    values[( int ) TopShuntBottomSeriesIndex.TopRightShunt] = bridge.BalanceDeviation / bridge.TopRight;
                    values[( int ) TopShuntBottomSeriesIndex.TopLeftShunt] = Conductor.OpenConductance;
                }
                else if ( bridge.ProductImbalance < 0d )
                {
                    values[( int ) TopShuntBottomSeriesIndex.TopLeftShunt] = -bridge.BalanceDeviation / (bridge.TopLeft * bridge.Balance);
                    values[( int ) TopShuntBottomSeriesIndex.TopRightShunt] = Conductor.OpenConductance;
                }
                else
                {
                    // open shunts
                    values[( int ) TopShuntBottomSeriesIndex.TopRightShunt] = Conductor.OpenConductance;
                    values[( int ) TopShuntBottomSeriesIndex.TopLeftShunt] = Conductor.OpenConductance;
                }
                // short resistors
                values[( int ) TopShuntBottomSeriesIndex.BottomLeftSeries] = Resistor.ShortResistance;
                values[( int ) TopShuntBottomSeriesIndex.BottomRightSeries] = Resistor.ShortResistance;
            }
            else if ( balanceLayout == BalanceLayout.RightShuntRightSeries )
            {
                if ( bridge.ProductImbalance > 0d )
                {
                    values[( int ) RightShuntRightSeriesIndex.TopRightShunt] = bridge.BalanceDeviation / bridge.TopRight;
                    values[( int ) RightShuntRightSeriesIndex.BottomRightShunt] = Conductor.OpenConductance;
                }
                else if ( bridge.ProductImbalance < 0d )
                {
                    values[( int ) RightShuntRightSeriesIndex.BottomRightShunt] = -bridge.BalanceDeviation / (bridge.TopLeft * bridge.Balance);
                    values[( int ) RightShuntRightSeriesIndex.TopRightShunt] = Conductor.OpenConductance;
                }
                else
                {
                    values[( int ) RightShuntRightSeriesIndex.TopRightSeries] = Conductor.OpenConductance;
                    values[( int ) RightShuntRightSeriesIndex.BottomRightSeries] = Resistor.ShortResistance;
                }
            }

            return values;
        }

        /// <summary> Evaluates the shunt bridge balance. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="bridge">        The bridge. </param>
        /// <param name="balanceLayout"> The layout. </param>
        /// <returns> A BalanceBridge. </returns>
        public static BridgeBalance ShuntBridgeBalance( Wheatstone bridge, BalanceLayout balanceLayout )
        {
            return new BridgeBalance( ShuntCompensation( bridge, balanceLayout ), balanceLayout );
        }

        /// <summary> Evaluates the shunt bridge balance. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> A BalanceBridge. </returns>
        public BridgeBalance ShuntBridgeBalance()
        {
            return ShuntBridgeBalance( this.NakedBridge, this.BridgeBalance.Layout );
        }

        /// <summary> Applies the shunt compensation. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        public void ApplyShuntCompensation()
        {
            this.BridgeBalance = this.ShuntBridgeBalance();
        }

        /// <summary> Derives the series only compensation for the bridge. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="bridge">        The bridge. </param>
        /// <param name="balanceLayout"> The balance layout. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process series compensation in this
        /// collection.
        /// </returns>
        public static IEnumerable<double> SeriesCompensation( Wheatstone bridge, BalanceLayout balanceLayout )
        {
            if ( bridge is null )
            {
                throw new ArgumentNullException( nameof( bridge ) );
            }

            var values = new double[] { 0d, 0d, 0d, 0d };
            if ( balanceLayout == BalanceLayout.TopShuntBottomSeries )
            {
                if ( bridge.BalanceDeviation > 0d )
                {
                    values[( int ) TopShuntBottomSeriesIndex.BottomRightSeries] = bridge.BottomRight * bridge.BalanceDeviation;
                    values[( int ) TopShuntBottomSeriesIndex.BottomLeftSeries] = Resistor.ShortResistance;
                }
                else if ( bridge.BalanceDeviation < 0d )
                {
                    values[( int ) TopShuntBottomSeriesIndex.BottomLeftSeries] = -bridge.BottomLeft * bridge.BalanceDeviation / bridge.Balance;
                    values[( int ) TopShuntBottomSeriesIndex.BottomRightSeries] = Resistor.ShortResistance;
                }
                else
                {
                    // short resistors
                    values[( int ) TopShuntBottomSeriesIndex.BottomRightSeries] = Resistor.ShortResistance;
                    values[( int ) TopShuntBottomSeriesIndex.BottomRightSeries] = Resistor.ShortResistance;
                }
                // open shunts
                values[( int ) TopShuntBottomSeriesIndex.TopRightShunt] = Conductor.OpenConductance;
                values[( int ) TopShuntBottomSeriesIndex.TopLeftShunt] = Conductor.OpenConductance;
            }
            else if ( balanceLayout == BalanceLayout.RightShuntRightSeries )
            {
                if ( bridge.BalanceDeviation > 0d )
                {
                    values[( int ) RightShuntRightSeriesIndex.BottomRightSeries] = bridge.BottomRight * bridge.BalanceDeviation;
                    values[( int ) RightShuntRightSeriesIndex.TopRightSeries] = Resistor.ShortResistance;
                }
                else if ( bridge.BalanceDeviation < 0d )
                {
                    values[( int ) RightShuntRightSeriesIndex.TopRightSeries] = -bridge.BottomLeft * bridge.BalanceDeviation / bridge.Balance;
                    values[( int ) RightShuntRightSeriesIndex.BottomRightSeries] = Resistor.ShortResistance;
                }
                else
                {
                    // short resistors
                    values[( int ) RightShuntRightSeriesIndex.TopRightSeries] = Resistor.ShortResistance;
                    values[( int ) RightShuntRightSeriesIndex.BottomRightSeries] = Resistor.ShortResistance;
                }

                values[( int ) RightShuntRightSeriesIndex.TopRightShunt] = Conductor.OpenConductance;
                values[( int ) RightShuntRightSeriesIndex.BottomRightShunt] = Conductor.OpenConductance;
            }

            return values;
        }

        /// <summary> Series bridge balance. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="bridge">        The bridge. </param>
        /// <param name="balanceLayout"> The layout. </param>
        /// <returns> A BridgeBalance. </returns>
        public static BridgeBalance SeriesBridgeBalance( Wheatstone bridge, BalanceLayout balanceLayout )
        {
            return new BridgeBalance( SeriesCompensation( bridge, balanceLayout ), balanceLayout );
        }

        /// <summary> Series bridge balance. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> A BridgeBalance. </returns>
        public BridgeBalance SeriesBridgeBalance()
        {
            return SeriesBridgeBalance( this.NakedBridge, this.BridgeBalance.Layout );
        }

        /// <summary> Applies the series compensation. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        public void ApplySeriesCompensation()
        {
            this.BridgeBalance = this.SeriesBridgeBalance();
        }

        #endregion

    }


    /// <summary> A bridge balance. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-11-25 </para>
    /// </remarks>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1036:Override methods on comparable types", Justification = "<Pending>" )]
    public class BridgeBalance : IComparable<BridgeBalance>
    {

        #region " CONSTRUCTION "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="values">  The balance values. </param>
        /// <param name="indexes"> The balance Indexes. </param>
        /// <param name="layout">  The balance layout. </param>
        public BridgeBalance( IEnumerable<double> values, IEnumerable<int> indexes, BalanceLayout layout ) : base()
        {
            this.Metadata = new BalanceMetadataCollection( this.Layout );
            this.Values = new List<double>( ValidatedValues( values ) );
            this.Layout = layout;
            this.Indexes = new List<int>( indexes );
            this.InitializeMinimaMaxima();
            this.AbsoluteBalanceDeviation = double.MaxValue;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="indexes"> The balance Indexes. </param>
        /// <param name="layout">  The balance layout. </param>
        public BridgeBalance( IEnumerable<int> indexes, BalanceLayout layout ) : this( new double[] { 0d, 0d, 0d, 0d }, indexes, layout )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-05. </remarks>
        /// <param name="values"> The balance values. </param>
        /// <param name="layout"> The balance layout. </param>
        public BridgeBalance( IEnumerable<double> values, BalanceLayout layout ) : this( values, BalanceMetadataCollection.DefaultIndexes( values, layout ), layout )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="layout"> The balance layout. </param>
        public BridgeBalance( BalanceLayout layout ) : this( new double[] { 0d, 0d, 0d, 0d }, layout )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="bridgeBalance"> The bridge balance. </param>
        public BridgeBalance( BridgeBalance bridgeBalance ) : this( ValidatedValues( ValidatedBridgeBalance( bridgeBalance ).Values ), bridgeBalance.Indexes, bridgeBalance.Layout )
        {
            this.InitializeMinimaMaximaThis( bridgeBalance.Minima, bridgeBalance.Maxima );
            this.AbsoluteBalanceDeviation = bridgeBalance.AbsoluteBalanceDeviation;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="bridgeBalance">            The bridge balance. </param>
        /// <param name="values">                   The balance values. </param>
        /// <param name="absoluteBalanceDeviation"> The absolute balance deviation. </param>
        public BridgeBalance( BridgeBalance bridgeBalance, IList<double> values, double absoluteBalanceDeviation ) : this( ValidatedBridgeBalance( bridgeBalance ) )
        {
            if ( values is null )
            {
                throw new ArgumentNullException( nameof( values ) );
            }

            if ( this.Indexes is null )
            {
                throw new InvalidOperationException( "Balance indexes must be specified if inserting new values" );
            }

            if ( 4 > values.Count && values.Count == this.Indexes.Count )
            {
                // if values are a subset of the set of values then update using the indexes.
                this.InsertValues( bridgeBalance.Indexes, values );
            }
            else
            {
                _ = ValidatedValues( values );
                this.Values = new List<double>( values );
            }

            this.InitializeMinimaMaximaThis( bridgeBalance.Minima, bridgeBalance.Maxima );
            this.AbsoluteBalanceDeviation = absoluteBalanceDeviation;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="bridgeBalance"> The bridge balance. </param>
        /// <param name="values">        The balance values. </param>
        public BridgeBalance( BridgeBalance bridgeBalance, IList<double> values ) : this( ValidatedBridgeBalance( bridgeBalance ), values, double.MaxValue )
        {
        }

        /// <summary> Makes a deep copy of this object. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> A copy of this object. </returns>
        public BridgeBalance Clone()
        {
            return new BridgeBalance( this );
        }

        /// <summary> Enumerates validated balance values in this collection. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="values"> The balance values. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process validated balance values in this
        /// collection.
        /// </returns>
        public static IEnumerable<double> ValidatedValues( IEnumerable<double> values )
        {
            return values is null
                ? throw new ArgumentNullException( nameof( values ) )
                : values.Count() != 4 ? throw new InvalidOperationException( $"{nameof( values )} must have 4 elements" ) : values;
        }

        /// <summary> Validated bridge balance. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="bridgeBalance"> The bridge balance. </param>
        /// <returns> A Bridge Balance. </returns>
        public static BridgeBalance ValidatedBridgeBalance( BridgeBalance bridgeBalance )
        {
            if ( bridgeBalance is null )
            {
                throw new ArgumentNullException( nameof( bridgeBalance ) );
            }

            _ = ValidatedValues( bridgeBalance.Values );
            return bridgeBalance;
        }

        #endregion

        #region " I COMPARABLE "

        /// <summary>
        /// Compares two <see cref="BridgeBalance"/> objects to determine their relative ordering.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="left">  The left. </param>
        /// <param name="right"> The right. </param>
        /// <returns>
        /// Negative if 'left' is less than 'right', 0 if they are equal, or positive if it is greater.
        /// </returns>
        public static int Compare( BridgeBalance left, BridgeBalance right )
        {
            return left is null
                ? throw new ArgumentNullException( nameof( left ) )
                : right is null ? throw new ArgumentNullException( nameof( right ) ) : left.CompareTo( right );
        }

        /// <summary>
        /// Compares this <see cref="BridgeBalance"/> to another to determine their relative ordering
        /// based on the Wheatstone <see cref="AbsoluteBalanceDeviation">Absolute Balance Deviation</see>.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="other"> Another instance to compare. </param>
        /// <returns>
        /// Negative if this object is less than the other, 0 if they are equal, or positive if this is
        /// greater.
        /// </returns>
        public int CompareTo( BridgeBalance other )
        {
            return other is null
                ? throw new ArgumentNullException( nameof( other ) )
                : this.AbsoluteBalanceDeviation.CompareTo( other.AbsoluteBalanceDeviation );
        }

        /// <summary> Cast that converts the given BridgeBalance to a > </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="left">  The left-hand-side of the '-' operator (A BalanceCandidate class) </param>
        /// <param name="right"> The right-hand-side of the '-' operator (A BalanceCandidate class) </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator >( BridgeBalance left, BridgeBalance right )
        {
            return Compare( left, right ) > 0;
        }

        /// <summary> Cast that converts the given BalanceCandidate to a &lt; </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="left">  The left-hand-side of the '-' operator (A BalanceCandidate class) </param>
        /// <param name="right"> The right-hand-side of the '-' operator (A BalanceCandidate class) </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator <( BridgeBalance left, BridgeBalance right )
        {
            return Compare( left, right ) < 0;
        }

        #endregion

        #region " EQUALS "

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
        /// <see cref="T:System.Object" />. </param>
        /// <returns>
        /// <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />; otherwise, <c>False</c>.
        /// </returns>
        public override bool Equals( object obj )
        {
            return this.Equals( obj as BridgeBalance );
        }

        /// <summary>
        /// Compares two bridges. The bridges are compared using their LowerLimits and UpperLimits.
        /// </summary>
        /// <remarks> The balances are the same if the have the same values and layout. </remarks>
        /// <param name="other"> Specifies the other <see cref="BridgeBalance">Bridge Balance</see>
        /// to compare for equality with this instance. </param>
        /// <returns> A Boolean data type. </returns>
        public bool Equals( BridgeBalance other )
        {
            if ( other is null )
            {
                return false;
            }
            else if ( this.Values is null || other.Values is null )
            {
                return false;
            }
            else
            {
                bool affirmative = true;
                for ( int i = 0, loopTo = this.Values.Count - 1; i <= loopTo; i++ )
                {
                    affirmative = affirmative && this.Values[i].Equals( other.Values[i] );
                    if ( !affirmative )
                    {
                        break;
                    }
                }

                return this.Layout == other.Layout && affirmative;
            }
        }

        /// <summary> Compares two bridge balances. </summary>
        /// <remarks> The balances are the same if the have the same values and layout. </remarks>
        /// <param name="other">     Specifies the other <see cref="BridgeBalance">Bridge balance</see>
        /// to compare for equality with this instance. </param>
        /// <param name="tolerance"> Specifies the relative tolerance for comparing the two values. The
        /// values are compared based on their end points. </param>
        /// <returns> A Boolean data type. </returns>
        public bool Equals( BridgeBalance other, double tolerance )
        {
            if ( other is null )
            {
                return false;
            }
            else if ( this.Values is null || other.Values is null )
            {
                return false;
            }
            else if ( this.Values.Count != other.Values.Count )
            {
                return false;
            }
            else if ( this.Equals( other ) )
            {
                return true;
            }
            else
            {
                bool affirmative = true;
                for ( int i = 0, loopTo = this.Values.Count - 1; i <= loopTo; i++ )
                {
                    affirmative = affirmative && Math.Abs( this.Values[i] - other.Values[i] ) <= 0.5d * tolerance * (this.Values[i] + other.Values[i]);
                    if ( !affirmative )
                    {
                        break;
                    }
                }

                return this.Layout == other.Layout && affirmative;
            }
        }

        /// <summary> Implements the operator =. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool Equals( BridgeBalance left, BridgeBalance right )
        {
            return ReferenceEquals( left, right ) || left is object && left.Equals( right );
        }

        /// <summary>
        /// Compares two bridges. The bridges are compared using their LowerLimits and UpperLimits.
        /// </summary>
        /// <remarks> The balances are the same if the have the same values and layout. </remarks>
        /// <param name="left">      Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right">     Specifies the right hand side argument of the binary operation. </param>
        /// <param name="tolerance"> Specifies the relative tolerance for comparing the two values. The
        /// values are compared based on their end points. </param>
        /// <returns> A Boolean data type. </returns>
        public static bool Equals( BridgeBalance left, BridgeBalance right, double tolerance )
        {
            return left is null && right is null || left is object && left.Equals( right, tolerance );
        }

        /// <summary> Implements the operator =. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( BridgeBalance left, BridgeBalance right )
        {
            return Equals( left, right );
        }

        /// <summary> Implements the operator &lt;&gt;. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( BridgeBalance left, BridgeBalance right )
        {
            return !Equals( left, right );
        }

        /// <summary> Creates a unique hash code. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> An <see cref="System.Int32">Int32</see> value. </returns>
        public override int GetHashCode()
        {
            return this.Values.GetHashCode() ^ this.Layout.GetHashCode();
        }

        #endregion

        #region " ELEMENTS "

        /// <summary> Gets the balance layout. </summary>
        /// <value> The balance layout. </value>
        public BalanceLayout Layout { get; private set; }

        /// <summary> Gets the identity. </summary>
        /// <value> The identity. </value>
        public string Identity => BalanceMetadataCollection.ToDelimitedString( this.Metadata.SelectValues( this.Indexes ), "+" );

        /// <summary> Gets or sets the balance elements meta data. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The balance elements meta data. </value>
        public BalanceMetadataCollection Metadata { get; private set; }

        /// <summary> Gets or sets the balance values. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The balance values. </value>
        public IList<double> Values { get; private set; }

        /// <summary> Insert new values using the specified indexes. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="indexes"> The balance Indexes. </param>
        /// <param name="values">  The balance values. </param>
        private void InsertValues( IEnumerable<int> indexes, IList<double> values )
        {
            for ( int i = 0, loopTo = indexes.Count() - 1; i <= loopTo; i++ )
            {
                this.Values[indexes.ElementAtOrDefault( i )] = values[i];
            }
        }

        /// <summary> Gets or sets the indexes of the balance element values and meta data. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The indexes of the balance element values and meta data. </value>
        public IList<int> Indexes { get; set; }

        /// <summary> Shunt index. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> An Integer. </returns>
        public int ShuntIndex()
        {
            return this.Indexes[this.DefaultShuntIndex];
        }

        /// <summary> Gets or sets the default shunt index. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The default shunt index. </value>
        public int DefaultShuntIndex { get; private set; } = 0;

        /// <summary> Looks for the shunt index. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> An Integer. </returns>
        public int FindShuntIndex()
        {
            return this.FindShuntIndex( this.Values, this.Indexes );
        }

        /// <summary> Looks for the shunt index. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="values">  The balance values. </param>
        /// <param name="indexes"> The balance Indexes. </param>
        /// <returns> An Integer. </returns>
        public int FindShuntIndex( IEnumerable<double> values, IEnumerable<int> indexes )
        {
            // bug fixed, default selected as series index instead of shunt index. 
            int result = indexes.ElementAtOrDefault( this.DefaultShuntIndex );
            foreach ( int index in this.Metadata.ShuntIndexes )
            {
                if ( values.ElementAtOrDefault( index ) != 0d )
                {
                    result = index;
                }
            }

            return result;
        }

        /// <summary> Gets the zero-based index of the series resistor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> An Integer. </returns>
        public int SeriesIndex()
        {
            return this.Indexes[this.DefaultSeriesIndex];
        }

        /// <summary> Gets or sets the default series index. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The default series index. </value>
        public int DefaultSeriesIndex { get; private set; } = 1;

        /// <summary> Gets the zero-based index of the series resistor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> The found series index. </returns>
        public int FindSeriesIndex()
        {
            return this.FindSeriesIndex( this.Values, this.Indexes );
        }

        /// <summary> Gets the zero-based index of the series resistor. </summary>
        /// <remarks> David, 2020-09-05. </remarks>
        /// <param name="values">  The balance values. </param>
        /// <param name="indexes"> The balance Indexes. </param>
        /// <returns> The found series index. </returns>
        public int FindSeriesIndex( IEnumerable<double> values, IEnumerable<int> indexes )
        {
            int result = indexes.ElementAtOrDefault( this.DefaultSeriesIndex );
            foreach ( int index in this.Metadata.SeriesIndexes )
            {
                if ( values.ElementAtOrDefault( index ) != 0d )
                {
                    result = index;
                }
            }

            return result;
        }

        /// <summary> Enumerates balance indexes used in the bridge balance. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns>
        /// An enumerator that allows for each to be used to process balance indexes in this collection.
        /// </returns>
        public IEnumerable<int> BalanceIndexes()
        {
            return new List<int>( new int[] { this.ShuntIndex(), this.SeriesIndex() } );
        }

        /// <summary> Enumerates balance values the bridge balance. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns>
        /// An enumerator that allows for each to be used to process balance values in this collection.
        /// </returns>
        public IEnumerable<double> BalanceValues()
        {
            return SelectValues( this.Values, this.BalanceIndexes() );
        }

        /// <summary> Converts a value to an enum. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> Value as a T. </returns>
        public static T ToEnum<T>( int value )
        {
            return ( T ) Enum.Parse( typeof( T ), value.ToString(), true );
        }

        /// <summary> Converts an index to an balance index. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="index"> Zero-based index of the balance values. </param>
        /// <returns> Index as a T. </returns>
        public T ToIndex<T>( int index )
        {
            return ToEnum<T>( this.Indexes[index] );
        }

        /// <summary> Enumerates select values in this collection. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values">  The balance values. </param>
        /// <param name="indexes"> The balance Indexes. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process select values in this collection.
        /// </returns>
        public static IEnumerable<double> SelectValues( IEnumerable<double> values, IEnumerable<int> indexes )
        {
            if ( values is null )
            {
                throw new ArgumentNullException( nameof( values ) );
            }

            if ( indexes is null )
            {
                throw new ArgumentNullException( nameof( indexes ) );
            }

            var l = new List<double>();
            foreach ( int index in indexes )
            {
                l.Add( values.ElementAtOrDefault( index ) );
            }

            return l;
        }

        /// <summary> Query if the bridge balance is valid. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> <c>true</c> if valid; otherwise <c>false</c> </returns>
        public bool IsValid()
        {
            bool affirmative = true;
            for ( int i = 0, loopTo = this.Values.Count - 1; i <= loopTo; i++ )
            {
                affirmative = this.Metadata[i].ElementType == BalanceElementType.Series ? affirmative && !Resistor.IsOpen( this.Values[i] ) : affirmative && !Conductor.IsShort( this.Values[i] );
                if ( !affirmative )
                {
                    break;
                }
            }

            return affirmative;
        }

        /// <summary> Gets or sets the absolute balance deviation. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The absolute balance deviation. </value>
        public double AbsoluteBalanceDeviation { get; set; }

        #endregion

        #region " MAXIMA/MINIMA "

        /// <summary> Gets or sets the maximum values. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The maximum values. </value>
        public IList<double> Maxima { get; set; }

        /// <summary> Gets or sets the minimum values. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The minimum values. </value>
        public IList<double> Minima { get; set; }

        /// <summary> Initializes the minimum and maximum values. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="equivalentBridgeResistance"> The equivalent bridge resistance. </param>
        public void InitializeMinimaMaxima( double equivalentBridgeResistance )
        {
            this.InitializeMinimaMaxima();
            foreach ( int index in this.Indexes )
            {
                if ( this.Metadata[index].ElementType == BalanceElementType.Series )
                {
                    this.Maxima[index] = equivalentBridgeResistance;
                }
                else if ( this.Metadata[index].ElementType == BalanceElementType.Shunt )
                {
                    this.Maxima[index] = 1d / equivalentBridgeResistance;
                }
            }
        }

        /// <summary> Initializes the minimum and maximum values. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        private void InitializeMinimaMaxima()
        {
            this.InitializeMinimaMaximaThis( new double[] { 0d, 0d, 0d, 0d }, new double[] { 0d, 0d, 0d, 0d } );
        }

        /// <summary> Initializes minimum and maximum values. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="minima"> The minimum values. </param>
        /// <param name="maxima"> The maximum values. </param>
        private void InitializeMinimaMaximaThis( IEnumerable<double> minima, IEnumerable<double> maxima )
        {
            this.Minima = new List<double>( minima );
            this.Maxima = new List<double>( maxima );
        }

        /// <summary> Initializes the minimum and maximum values. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="minima"> The minimum values. </param>
        /// <param name="maxima"> The maximum values. </param>
        public void InitializeMinimaMaxima( IEnumerable<double> minima, IEnumerable<double> maxima )
        {
            this.InitializeMinimaMaximaThis( minima, maxima );
        }

        #endregion

        #region " TO COMMA SEPARATED STRING "

        /// <summary> Returns a comma-delimited values string. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="values"> The values. </param>
        /// <param name="format"> Describes the format to use. </param>
        /// <returns> A String that represents this object. </returns>
        public static string ToCommaSeparatedString( IEnumerable<double> values, string format )
        {
            var builder = new System.Text.StringBuilder();
            for ( int i = 0, loopTo = values.Count() - 1; i <= loopTo; i++ )
            {
                _ = builder.Append( $"{values.ElementAtOrDefault( i ).ToString( format )}" );
                if ( i < values.Count() - 1 )
                {
                    _ = builder.Append( "," );
                }
            }

            return builder.ToString();
        }

        /// <summary> Returns a comma-delimited values string. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="format"> Describes the format to use. </param>
        /// <returns> A String that represents this object. </returns>
        public string ToCommaSeparatedString( string format )
        {
            return ToCommaSeparatedString( this.Values, format );
        }

        /// <summary> Returns a comma-delimited values string. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> A String that represents this object. </returns>
        public string ToCommaSeparatedString()
        {
            return this.ToCommaSeparatedString( "G6" );
        }

        #endregion

        #region " TO STRING "

        /// <summary> Convert this object into a string representation. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="values">    The values. </param>
        /// <param name="metadata">  Information describing the meta. </param>
        /// <param name="delimiter"> The delimiter. </param>
        /// <param name="format">    Describes the format to use. </param>
        /// <returns> A String that represents this object. </returns>
        public static string ToString( IEnumerable<double> values, IEnumerable<BalanceElementMetadata> metadata, string delimiter, string format )
        {
            var builder = new System.Text.StringBuilder();
            for ( int i = 0, loopTo = values.Count() - 1; i <= loopTo; i++ )
            {
                _ = builder.Append( $"{metadata.ElementAtOrDefault( i ).Edge} {metadata.ElementAtOrDefault( i ).ElementType} = {values.ElementAtOrDefault( i ).ToString( format )}" );
                if ( i < values.Count() - 1 )
                {
                    _ = builder.Append( delimiter );
                }
            }

            return builder.ToString();
        }

        /// <summary> Convert this object into a string representation. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="delimiter"> The delimiter. </param>
        /// <param name="format">    Describes the format to use. </param>
        /// <returns> A String that represents this object. </returns>
        public string ToString( string delimiter, string format )
        {
            return ToString( this.Values, this.Metadata, delimiter, format );
        }

        /// <summary> Convert this object into a string representation. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="values"> The values. </param>
        /// <param name="format"> Describes the format to use. </param>
        /// <returns> A String that represents this object. </returns>
        public static string ToString( IEnumerable<double> values, string format )
        {
            var builder = new System.Text.StringBuilder( "[" );
            _ = builder.Append( ToCommaSeparatedString( values, format ) );
            _ = builder.Append( "]" );
            return builder.ToString();
        }

        /// <summary> Convert this object into a string representation. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="format"> Describes the format to use. </param>
        /// <returns> A String that represents this object. </returns>
        public string ToString( string format )
        {
            return ToString( this.Values, format );
        }

        /// <summary> Convert this object into a string representation. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> A String that represents this object. </returns>
        public override string ToString()
        {
            return this.ToString( "G6" );
        }

        #endregion

    }

    /// <summary> A collection of Balance Bridges. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-09-25 </para>
    /// </remarks>
    public class BalanceBridgeCollection : System.Collections.ObjectModel.Collection<BalanceBridge>
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        public BalanceBridgeCollection() : base()
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="bridges"> The bridges. </param>
        public BalanceBridgeCollection( BalanceBridgeCollection bridges ) : this()
        {
            if ( bridges is null )
            {
                throw new ArgumentNullException( nameof( bridges ) );
            }

            foreach ( BalanceBridge b in bridges )
            {
                this.Add( new BalanceBridge( b ) );
            }
        }

        /// <summary> Validates the given bridges. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="bridges"> The bridges. </param>
        /// <returns> A BalanceBridgeCollection. </returns>
        public static BalanceBridgeCollection Validate( BalanceBridgeCollection bridges )
        {
            return bridges is null ? throw new ArgumentNullException( nameof( bridges ) ) : bridges;
        }

        #endregion

        #region " BRIDGE BALANCE "

        /// <summary>
        /// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="bridge"> The object to add to the
        /// <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
        public new void Add( BalanceBridge bridge )
        {
            if ( bridge is null )
            {
                throw new ArgumentNullException( nameof( bridge ) );
            }

            base.Add( bridge );
            if ( this._BridgeBalance is null )
            {
                this._BridgeBalance = new BridgeBalance( bridge.BridgeBalance );
            }
        }

        /// <summary> The bridge balance. </summary>
        private BridgeBalance _BridgeBalance;

        /// <summary> Gets or sets the bridge balance. </summary>
        /// <value> The bridge balance. </value>
        public BridgeBalance BridgeBalance
        {
            get => this._BridgeBalance;

            set {
                this._BridgeBalance = value;
                foreach ( BalanceBridge bridge in this )
                {
                    bridge.BridgeBalance = value;
                }
            }
        }
        #endregion

        #region " CALCULATIONS "

        /// <summary> Root sum squares. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values"> The values. </param>
        /// <returns> A Double. </returns>
        public static double RootSumSquares( double[] values )
        {
            if ( values is null )
            {
                throw new ArgumentNullException( nameof( values ) );
            }

            double result = 0d;
            foreach ( double value in values )
            {
                result += value * value;
            }

            result = Math.Sqrt( result );
            return result;
        }

        /// <summary> Root sum squares. </summary>
        /// <remarks> David, 2020-09-05. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values"> The values. </param>
        /// <returns> A Double. </returns>
        public static double RootSumSquares( IEnumerable<double> values )
        {
            if ( values is null )
            {
                throw new ArgumentNullException( nameof( values ) );
            }

            double result = 0d;
            foreach ( double value in values )
            {
                result += value * value;
            }

            result = Math.Sqrt( result );
            return result;
        }

        /// <summary> Enumerates imbalances in this collection. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns>
        /// An enumerator that allows for each to be used to process imbalances in this collection.
        /// </returns>
        public IList<double> Imbalances()
        {
            var l = new List<double>();
            foreach ( Wheatstone bridge in this )
            {
                l.Add( bridge.ProductImbalance );
            }

            return l;
        }

        /// <summary> Enumerates deviations in this collection. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns>
        /// An enumerator that allows for each to be used to process imbalances in this collection.
        /// </returns>
        public IList<double> Deviations()
        {
            var l = new List<double>();
            foreach ( Wheatstone bridge in this )
            {
                l.Add( bridge.BalanceDeviation );
            }

            return l;
        }

        /// <summary> Enumerates Relative Offsets in this collection. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns>
        /// An enumerator that allows for each to be used to process imbalances in this collection.
        /// </returns>
        public IList<double> RelativeOffsets()
        {
            var l = new List<double>();
            foreach ( Wheatstone bridge in this )
            {
                l.Add( bridge.Output() );
            }

            return l;
        }


        #endregion

    }

    /// <summary> Values that represent balance layouts. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    public enum BalanceLayout
    {

        /// <summary> An enum constant representing the top shunt bottom series option where
        /// shunt conductances are connected across the top bridge elements and
        /// resistors are wired in series to the bottom bridge elements. Element order
        /// is Top Right Shunt, Bottom Right Series, Bottom Left Series, Top Left Shunt</summary>
        [Description( "Top shunts bottom series" )]
        TopShuntBottomSeries,

        /// <summary> An enum constant representing the right shunt right series option where
        /// the shunt conductances are applied to the elements of the right arm of
        /// the bridge and a series resistor is added to this parallel pair. . Element order
        /// is Top Right Shunt, Top Right Series, Bottom Right Series, Bottom Right Shunt</summary>
        [Description( "Right shunts right series" )]
        RightShuntRightSeries
    }

    /// <summary> Values that represent top shunt bottom series indexes. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    public enum TopShuntBottomSeriesIndex
    {

        /// <summary> An enum constant representing the top right shunt option. </summary>
        TopRightShunt = 0,

        /// <summary> An enum constant representing the bottom right series option. </summary>
        BottomRightSeries = 1,

        /// <summary> An enum constant representing the bottom left series option. </summary>
        BottomLeftSeries = 2,

        /// <summary> An enum constant representing the top left shunt option. </summary>
        TopLeftShunt = 3
    }

    /// <summary> Values that represent right shunt right series indexes. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    public enum RightShuntRightSeriesIndex
    {

        /// <summary> An enum constant representing the top right shunt option. </summary>
        TopRightShunt = 0,

        /// <summary> An enum constant representing the top right series option. </summary>
        TopRightSeries = 1,

        /// <summary> An enum constant representing the bottom right series option. </summary>
        BottomRightSeries = 2,

        /// <summary> An enum constant representing the bottom right shunt option. </summary>
        BottomRightShunt = 3
    }

    /// <summary> Values that represent element types. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    public enum BalanceElementType
    {

        /// <summary> An enum constant representing the shunt option. </summary>
        Shunt,

        /// <summary> An enum constant representing the series option. </summary>
        Series
    }

    /// <summary> Meta data for a balance element. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public struct BalanceElementMetadata
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="index">       Zero-based index of the. </param>
        /// <param name="edge">        The edge. </param>
        /// <param name="elementType"> The type of the element. </param>
        public BalanceElementMetadata( int index, WheatstoneEdge edge, BalanceElementType elementType )
        {
            this.ElementIndex = index;
            this.Edge = edge;
            this.ElementType = elementType;
        }

        /// <summary> Gets the edge. </summary>
        /// <exception cref="InvalidEnumArgumentException"> Thrown when an Invalid Enum Argument error
        /// condition occurs. </exception>
        /// <value> The edge. </value>
        public WheatstoneEdge Edge { get; set; }

        /// <summary> Gets the type of the element. </summary>
        /// <exception cref="InvalidEnumArgumentException"> Thrown when an Invalid Enum Argument error
        /// condition occurs. </exception>
        /// <value> The type of the element. </value>
        public BalanceElementType ElementType { get; set; }

        /// <summary> Gets the zero-based index of the element. </summary>
        /// <exception cref="InvalidEnumArgumentException"> Thrown when an Invalid Enum Argument error
        /// condition occurs. </exception>
        /// <value> The element index. </value>
        public int ElementIndex { get; set; }

        #region " EQUALS "

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
        /// <see cref="T:System.Object" />. </param>
        /// <returns>
        /// <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />; otherwise, <c>False</c>.
        /// </returns>
        public override bool Equals( object obj )
        {
            return obj is object && (ReferenceEquals( this, obj ) || this.Equals( ( BalanceElementMetadata ) obj ));
        }

        /// <summary> = casting operator. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="left">  The left-hand-side of the '-' operator (A BalanceElementAttribute class) </param>
        /// <param name="right"> The right-hand-side of the '-' operator (A BalanceElementAttribute class) </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( BalanceElementMetadata left, BalanceElementMetadata right )
        {
            return Equals( left, right );
        }

        /// <summary> &lt;&gt; casting operator. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="left">  The left-hand-side of the '-' operator (A BalanceElementAttribute class) </param>
        /// <param name="right"> The right-hand-side of the '-' operator (A BalanceElementAttribute class) </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( BalanceElementMetadata left, BalanceElementMetadata right )
        {
            return !Equals( left, right );
        }

        /// <summary> Indicates whether this instance and a specified object are equal. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="left">  The left-hand-side of the '-' operator (A BalanceElementAttribute class) </param>
        /// <param name="right"> The right-hand-side of the '-' operator (A BalanceElementAttribute class) </param>
        /// <returns>
        /// <c>True</c> if <paramref name="left" /> and <paramref name="right" /> are the same type and
        /// represent the same value; otherwise, <c>False</c>.
        /// </returns>
        public static bool Equals( BalanceElementMetadata left, BalanceElementMetadata right )
        {
            return left.Edge.Equals( right.Edge ) && left.ElementType.Equals( right.ElementType ) && left.ElementIndex.Equals( right.ElementIndex );
        }

        /// <summary>
        /// Returns True if the value of the <paramref name="other"/> equals to the instance value.
        /// </summary>
        /// <remarks> Balance element meta data are the same if the have the same values. </remarks>
        /// <param name="other"> The other
        /// <see cref="BalanceElementMetadata">BalanceElementAttribute</see> to
        /// compare for equality with this instance. </param>
        /// <returns> <c>True</c> if equals; otherwise, <c>False</c>. </returns>
        public bool Equals( BalanceElementMetadata other )
        {
            return Equals( this, other );
        }

        /// <summary> Creates a unique hash code. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> An <see cref="System.Int32">Int32</see> value. </returns>
        public override int GetHashCode()
        {
            return this.Edge.GetHashCode() ^ this.ElementType.GetHashCode() ^ this.ElementIndex.GetHashCode();
        }

        #endregion

    }

    /// <summary> A balance meta data collection. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-11-27 </para>
    /// </remarks>
    public class BalanceMetadataCollection : System.Collections.ObjectModel.KeyedCollection<int, BalanceElementMetadata>
    {

        #region " CONSTRUCTION "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        public BalanceMetadataCollection() : base()
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="InvalidEnumArgumentException"> Thrown when an Invalid Enum Argument error
        /// condition occurs. </exception>
        /// <param name="layout"> The layout. </param>
        public BalanceMetadataCollection( BalanceLayout layout ) : base()
        {
            this.SeriesIndexes = new List<int>();
            this.ShuntIndexes = new List<int>();
            switch ( layout )
            {
                case BalanceLayout.RightShuntRightSeries:
                    {
                        this.Add( new BalanceElementMetadata( ( int ) RightShuntRightSeriesIndex.TopRightShunt, WheatstoneEdge.TopRight, BalanceElementType.Shunt ) );
                        this.ShuntIndexes.Add( this.Count - 1 );
                        this.Add( new BalanceElementMetadata( ( int ) RightShuntRightSeriesIndex.TopRightSeries, WheatstoneEdge.TopRight, BalanceElementType.Series ) );
                        this.SeriesIndexes.Add( this.Count - 1 );
                        this.Add( new BalanceElementMetadata( ( int ) RightShuntRightSeriesIndex.BottomRightSeries, WheatstoneEdge.BottomRight, BalanceElementType.Series ) );
                        this.SeriesIndexes.Add( this.Count - 1 );
                        this.Add( new BalanceElementMetadata( ( int ) RightShuntRightSeriesIndex.BottomRightShunt, WheatstoneEdge.BottomRight, BalanceElementType.Shunt ) );
                        this.ShuntIndexes.Add( this.Count - 1 );
                        break;
                    }

                case BalanceLayout.TopShuntBottomSeries:
                    {
                        this.Add( new BalanceElementMetadata( ( int ) TopShuntBottomSeriesIndex.TopRightShunt, WheatstoneEdge.TopRight, BalanceElementType.Shunt ) );
                        this.ShuntIndexes.Add( this.Count - 1 );
                        this.Add( new BalanceElementMetadata( ( int ) TopShuntBottomSeriesIndex.BottomRightSeries, WheatstoneEdge.BottomRight, BalanceElementType.Series ) );
                        this.SeriesIndexes.Add( this.Count - 1 );
                        this.Add( new BalanceElementMetadata( ( int ) TopShuntBottomSeriesIndex.BottomLeftSeries, WheatstoneEdge.BottomLeft, BalanceElementType.Series ) );
                        this.SeriesIndexes.Add( this.Count - 1 );
                        this.Add( new BalanceElementMetadata( ( int ) TopShuntBottomSeriesIndex.TopLeftShunt, WheatstoneEdge.TopLeft, BalanceElementType.Shunt ) );
                        this.ShuntIndexes.Add( this.Count - 1 );
                        break;
                    }

                default:
                    {
                        throw new InvalidEnumArgumentException( nameof( layout ), ( int ) layout, typeof( BalanceLayout ) );
                    }
            }
        }

        /// <summary>
        /// When implemented in a derived class, extracts the key from the specified element.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="item"> The element from which to extract the key. </param>
        /// <returns> The key for the specified element. </returns>
        protected override int GetKeyForItem( BalanceElementMetadata item )
        {
            return item.ElementIndex;
        }

        /// <summary> Gets the meta data. </summary>
        /// <value> The meta data. </value>
        public static BalanceMetadataCollection Metadata( BalanceLayout layout )
        {
            return new BalanceMetadataCollection( layout );
        }

        /// <summary> Returns the default indexes for the specified values. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="values"> The balance values. </param>
        /// <param name="layout"> The layout. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process default indexes in this collection.
        /// </returns>
        public static IEnumerable<int> DefaultIndexes( IEnumerable<double> values, BalanceLayout layout )
        {
            values = BalanceBridge.ValidatedBalanceValues( values );
            var balanceInfo = Metadata( layout );
            return new List<int>( new int[] { balanceInfo.SuggestShuntIndex( values, balanceInfo.ShuntIndexes[0] ), balanceInfo.SuggestSeriesIndex( values, balanceInfo.SeriesIndexes[0] ) } );
        }

        /// <summary> Returns the default indexes. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="layout"> The layout. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process default indexes in this collection.
        /// </returns>
        public static IEnumerable<int> DefaultIndexes( BalanceLayout layout )
        {
            var balanceInfo = Metadata( layout );
            return new List<int>( new int[] { balanceInfo.ShuntIndexes[0], balanceInfo.SeriesIndexes[0] } );
        }

        #endregion

        #region " INDEXES "

        /// <summary> Gets or sets the series indexes. </summary>
        /// <value> The series indexes. </value>
        public IList<int> SeriesIndexes { get; private set; }

        /// <summary> Gets or sets the shunt indexes. </summary>
        /// <value> The shunt indexes. </value>
        public IList<int> ShuntIndexes { get; private set; }

        #endregion

        #region " SELECT VALUES AND INDICES "

        /// <summary> Enumerates select values in this collection. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values">  The balance values. </param>
        /// <param name="indexes"> The balance Indexes. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process select values in this collection.
        /// </returns>
        public static IEnumerable<BalanceElementMetadata> SelectValues( IEnumerable<BalanceElementMetadata> values, IEnumerable<int> indexes )
        {
            if ( values is null )
            {
                throw new ArgumentNullException( nameof( values ) );
            }

            if ( indexes is null )
            {
                throw new ArgumentNullException( nameof( indexes ) );
            }

            var l = new List<BalanceElementMetadata>();
            foreach ( int index in indexes )
            {
                l.Add( values.ElementAtOrDefault( index ) );
            }

            return l;
        }

        /// <summary> Enumerates select values in this collection. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="indexes"> The balance Indexes. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process select values in this collection.
        /// </returns>
        public IEnumerable<BalanceElementMetadata> SelectValues( IEnumerable<int> indexes )
        {
            return SelectValues( this, indexes );
        }

        /// <summary> Suggests a shunt index. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="layout"> The layout. </param>
        /// <param name="values"> The balance values. </param>
        /// <returns> An Integer. </returns>
        public static int SuggestShuntIndex( BalanceLayout layout, IEnumerable<double> values )
        {
            var BalanceInfo = Metadata( layout );
            return BalanceInfo.SuggestShuntIndex( values, BalanceInfo.ShuntIndexes[0] );
        }

        /// <summary> Suggests a shunt index. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="layout">       The layout. </param>
        /// <param name="values">       The balance values. </param>
        /// <param name="defaultIndex"> The default index. </param>
        /// <returns> An Integer. </returns>
        public static int SuggestShuntIndex( BalanceLayout layout, IEnumerable<double> values, int defaultIndex )
        {
            return Metadata( layout ).SuggestShuntIndex( values, defaultIndex );
        }

        /// <summary> Suggest shunt index. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="values"> The balance values. </param>
        /// <returns> An Integer. </returns>
        public int SuggestShuntIndex( IEnumerable<double> values )
        {
            return this.SuggestShuntIndex( values, this.ShuntIndexes[0] );
        }

        /// <summary> Suggest shunt index. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="values">       The balance values. </param>
        /// <param name="defaultIndex"> The default index. </param>
        /// <returns> An Integer. </returns>
        public int SuggestShuntIndex( IEnumerable<double> values, int defaultIndex )
        {
            // default to the first index
            int shuntIndex = defaultIndex;
            foreach ( int index in this.ShuntIndexes )
            {
                if ( values.ElementAtOrDefault( index ) > 0d )
                {
                    shuntIndex = index;
                }
            }

            return shuntIndex;
        }

        /// <summary> Suggests a Series index. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="layout"> The layout. </param>
        /// <param name="values"> The balance values. </param>
        /// <returns> An Integer. </returns>
        public static int SuggestSeriesIndex( BalanceLayout layout, IEnumerable<double> values )
        {
            var BalanceInfo = Metadata( layout );
            return BalanceInfo.SuggestSeriesIndex( values, BalanceInfo.SeriesIndexes[0] );
        }

        /// <summary> Suggests a Series index. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="layout">       The layout. </param>
        /// <param name="values">       The balance values. </param>
        /// <param name="defaultIndex"> The default index. </param>
        /// <returns> An Integer. </returns>
        public static int SuggestSeriesIndex( BalanceLayout layout, IEnumerable<double> values, int defaultIndex )
        {
            return Metadata( layout ).SuggestSeriesIndex( values, defaultIndex );
        }

        /// <summary> Suggest Series index. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="values"> The balance values. </param>
        /// <returns> An Integer. </returns>
        public int SuggestSeriesIndex( IEnumerable<double> values )
        {
            return this.SuggestSeriesIndex( values, this.SeriesIndexes[0] );
        }

        /// <summary> Suggest Series index. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="values">       The balance values. </param>
        /// <param name="defaultIndex"> The default index. </param>
        /// <returns> An Integer. </returns>
        public int SuggestSeriesIndex( IEnumerable<double> values, int defaultIndex )
        {
            // default to the first index
            int SeriesIndex = defaultIndex;
            foreach ( int index in this.SeriesIndexes )
            {
                if ( values.ElementAtOrDefault( index ) > 0d )
                {
                    SeriesIndex = index;
                }
            }

            return SeriesIndex;
        }



        #endregion

        #region " TO STRING "

        /// <summary> Returns delimited header string. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="metadata">  Information describing the meta. </param>
        /// <param name="delimiter"> The delimiter. </param>
        /// <returns> A String that represents this object. </returns>
        public static string ToDelimitedString( IEnumerable<BalanceElementMetadata> metadata, string delimiter )
        {
            var builder = new System.Text.StringBuilder();
            for ( int i = 0, loopTo = metadata.Count() - 1; i <= loopTo; i++ )
            {
                _ = builder.Append( $"{metadata.ElementAtOrDefault( i ).Edge} {metadata.ElementAtOrDefault( i ).ElementType}" );
                if ( i < metadata.Count() - 1 )
                {
                    _ = builder.Append( delimiter );
                }
            }

            return builder.ToString();
        }

        /// <summary> Returns a comma-delimited header string. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> A String that represents this object. </returns>
        public string ToCommaSeparatedString()
        {
            return ToDelimitedString( this, "," );
        }

        /// <summary> Convert this object into a string representation. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="metadata"> The meta data. </param>
        /// <returns> A String that represents this object. </returns>
        public static string ToString( IEnumerable<BalanceElementMetadata> metadata )
        {
            var builder = new System.Text.StringBuilder( "[" );
            _ = builder.Append( ToDelimitedString( metadata, "," ) );
            _ = builder.Append( "]" );
            return builder.ToString();
        }

        /// <summary> Convert this object into a string representation. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> A String that represents this object. </returns>
        public override string ToString()
        {
            return ToString( this );
        }

        #endregion

    }
}
