using System;

namespace isr.Core.Engineering
{

    /// <summary> A Seebeck measure. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-06-28 </para>
    /// </remarks>
    public class SeebeckMeasure
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        public SeebeckMeasure() : base()
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="voltage">         The voltage. </param>
        /// <param name="lowTemperature">  The low temperature. </param>
        /// <param name="highTemperature"> The high temperature. </param>
        public SeebeckMeasure( double voltage, double lowTemperature, double highTemperature ) : this()
        {
            this.Voltage = voltage;
            this.LowTemperature = lowTemperature;
            this.HighTemperature = highTemperature;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="seebeckMeasure"> The seebeck measure. </param>
        public SeebeckMeasure( SeebeckMeasure seebeckMeasure ) : this( Validated( seebeckMeasure ).Voltage, seebeckMeasure.LowTemperature, seebeckMeasure.HighTemperature )
        {
        }

        /// <summary> Validated the given seebeck measure. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="seebeckMeasure"> The seebeck measure. </param>
        /// <returns> A SeebeckMeasure. </returns>
        public static SeebeckMeasure Validated( SeebeckMeasure seebeckMeasure )
        {
            return seebeckMeasure is null ? throw new ArgumentNullException( nameof( seebeckMeasure ) ) : seebeckMeasure;
        }

        /// <summary> Gets the voltage. Zero if measurement was not made. </summary>
        /// <value> The voltage. </value>
        public double Voltage { get; set; }

        /// <summary> Gets the high temperature. </summary>
        /// <value> The high temperature. </value>
        public double HighTemperature { get; set; }

        /// <summary> Gets the low temperature. </summary>
        /// <value> The low temperature. </value>
        public double LowTemperature { get; set; }

        /// <summary> Gets true if the seebeck measurement temperature difference is positive. </summary>
        /// <value> The indicator of having valid temperatures. </value>
        public bool HasTemperatureDifference => this.HighTemperature - this.LowTemperature != 0d;

        /// <summary> Gets the has measurement. </summary>
        /// <value> The has measurement. </value>
        public bool HasMeasurement => this.Voltage != 0d && this.HasTemperatureDifference;

        /// <summary> Gets the Seebeck Coefficient. </summary>
        /// <value>
        /// The Seebeck Coefficient or <see cref="Double.NaN"/> if temperature difference is not positive.
        /// </value>
        public double SeebeckCoefficient => this.HasMeasurement ? this.Voltage / (this.HighTemperature - this.LowTemperature) : double.NaN;
    }
}
