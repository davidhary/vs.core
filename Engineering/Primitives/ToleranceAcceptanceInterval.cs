using System;

namespace isr.Core.Engineering
{

    /// <summary> A tolerance acceptance interval. </summary>
    /// <remarks>
    /// This class is sealed to ensure that the hash value of its elements is not used
    /// by two instances with different hash value set. (c) 2014 Integrated Scientific
    /// Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-05-19 </para>
    /// </remarks>
    public class ToleranceAcceptanceInterval : AcceptanceInterval
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="code"> The tolerance code. </param>
        public ToleranceAcceptanceInterval( string code ) : base()
        {
            if ( string.IsNullOrWhiteSpace( code ) )
            {
                this.Parsed = false;
            }
            else
            {
                var info = Parse( code );
                this.Parsed = info is object;
                if ( this.Parsed )
                {
                    this.Code = code;
                    this.RelativeInterval = new ToleranceInterval( info.RelativeInterval );
                    this.Caption = info.Caption;
                    this.CompoundCaption = BuildCompoundCaptionFormat( code, this.Caption );
                }
            }
        }

        /// <summary> Constructor for symmetric interval. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="code">      The tolerance code. </param>
        /// <param name="tolerance"> The tolerance interval. </param>
        /// <param name="caption">   The caption. </param>
        public ToleranceAcceptanceInterval( string code, Interval tolerance, string caption ) : base( code, tolerance, caption )
        {
        }

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> The value. </param>
        public ToleranceAcceptanceInterval( AcceptanceInterval value ) : this( value.Code, value.RelativeInterval, value.Caption )
        {
        }

        /// <summary> Constructor for symmetric interval and standard caption. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="code">      The tolerance code. </param>
        /// <param name="tolerance"> The tolerance. </param>
        public ToleranceAcceptanceInterval( string code, Interval tolerance ) : this( code, tolerance, BuildCaption( tolerance.HighEndPoint ) )
        {
        }

        /// <summary> Constructor for symmetric interval and standard caption. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="code">      The tolerance code. </param>
        /// <param name="tolerance"> The tolerance. </param>
        public ToleranceAcceptanceInterval( string code, double tolerance ) : this( code, tolerance, BuildCaption( tolerance ) )
        {
        }

        /// <summary> Constructor for symmetric interval. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="code">      The tolerance code. </param>
        /// <param name="tolerance"> The tolerance. </param>
        /// <param name="caption">   The caption. </param>
        public ToleranceAcceptanceInterval( string code, double tolerance, string caption ) : this( code, new ToleranceInterval( tolerance ), caption )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="lowerTolerance"> The lower tolerance. </param>
        /// <param name="upperTolerance"> The upper tolerance. </param>
        /// <param name="caption">        The caption. </param>
        public ToleranceAcceptanceInterval( double lowerTolerance, double upperTolerance, string caption ) : this( UserCode, new ToleranceInterval( lowerTolerance, upperTolerance ), caption )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="code">           The tolerance code. </param>
        /// <param name="lowerTolerance"> The lower tolerance. </param>
        /// <param name="upperTolerance"> The upper tolerance. </param>
        /// <param name="caption">        The caption. </param>
        public ToleranceAcceptanceInterval( string code, double lowerTolerance, double upperTolerance, string caption ) : this( code, new ToleranceInterval( lowerTolerance, upperTolerance ), caption )
        {
        }

        /// <summary> The clone Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> The value. </param>
        public ToleranceAcceptanceInterval( ToleranceAcceptanceInterval value ) : this( value.Code, value.RelativeInterval, value.Caption )
        {
        }

        /// <summary> Gets the empty value. </summary>
        /// <value> The empty. </value>
        public static ToleranceAcceptanceInterval Empty => new ToleranceAcceptanceInterval( EmptyCode, 0d );

        /// <summary> Gets or sets the caption format. </summary>
        /// <value> The caption format. </value>
        public static string CaptionFormat { get; set; } = "±{0}%";

        /// <summary> Builds a caption. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns>
        /// A String value using the
        /// <see cref="ToleranceAcceptanceInterval.CaptionFormat">caption format</see>.
        /// </returns>
        public static string BuildCaption( double value )
        {
            double scaleFactor = 1d;
            if ( CaptionFormat.Contains( "%}" ) )
            {
                scaleFactor = 1d;
            }
            else if ( CaptionFormat.Contains( "%" ) )
            {
                scaleFactor = 100d;
            }

            return BritishStadnardTcr.BuildCaption( value, scaleFactor, CaptionFormat );
        }

        /// <summary> Builds a caption. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value">       The value. </param>
        /// <param name="scaleFactor"> The scale factor. </param>
        /// <param name="format">      Describes the format to use. </param>
        /// <returns>
        /// A String value using the
        /// <see cref="ToleranceAcceptanceInterval.CaptionFormat">caption format</see>.
        /// </returns>
        public static string BuildCaption( double value, double scaleFactor, string format )
        {
            return string.Format( System.Globalization.CultureInfo.CurrentCulture, format, scaleFactor * value );
        }

        /// <summary> The user tolerance value. </summary>
        /// <value> The user value. </value>
        public static ToleranceAcceptanceInterval UserValue { get; set; } = new ToleranceAcceptanceInterval( "@", 0.99d );

        /// <summary> The unknown tolerance value. </summary>
        /// <value> The unknown value. </value>
        public static ToleranceAcceptanceInterval UnknownValue { get; set; } = new ToleranceAcceptanceInterval( "Z", 0.99d );

        /// <summary> Gets or sets the unknown code. </summary>
        /// <value> The unknown code. </value>
        public static new string UnknownCode
        {
            get => UnknownValue.Code;

            set => UnknownValue = new ToleranceAcceptanceInterval( value, UnknownValue.RelativeInterval );
        }

        /// <summary> Gets or sets the user code. </summary>
        /// <value> The user code. </value>
        public static new string UserCode
        {
            get => UserValue.Code;

            set => UserValue = new ToleranceAcceptanceInterval( value, UserValue.RelativeInterval );
        }

        /// <summary> The dictionary. </summary>
        private static AcceptanceIntervalCollection _Dictionary;

        /// <summary> Gets the <see cref="AcceptanceIntervalCollection">dictionary</see>. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> A Dictionary(Of String, CodedIntervalBase). </returns>
        public static AcceptanceIntervalCollection Dictionary()
        {
            if ( _Dictionary is null || _Dictionary.Count == 0 )
            {
                BuildDictionary();
            }

            return _Dictionary;
        }

        /// <summary> Builds coded interval base dictionary. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="values"> The values. </param>
        public static void BuildDictionary( AcceptanceIntervalCollection values )
        {
            _Dictionary = new AcceptanceIntervalCollection();
            _Dictionary.Populate( values );
        }

        /// <summary> Builds tolerance information dictionary. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        private static void BuildDictionary()
        {
            var dix = new AcceptanceIntervalCollection() { new ToleranceAcceptanceInterval( "A", 0.0005d ), new ToleranceAcceptanceInterval( "B", 0.001d ), new ToleranceAcceptanceInterval( "C", 0.0025d ), new ToleranceAcceptanceInterval( "D", 0.005d ), new ToleranceAcceptanceInterval( "F", 0.01d ), new ToleranceAcceptanceInterval( "G", 0.02d ), new ToleranceAcceptanceInterval( "H", 0.03d ), new ToleranceAcceptanceInterval( "J", 0.05d ), new ToleranceAcceptanceInterval( "K", 0.1d ), new ToleranceAcceptanceInterval( "M", 0.2d ), new ToleranceAcceptanceInterval( "P", 0.0015d ), new ToleranceAcceptanceInterval( "Q", 0.0002d ), new ToleranceAcceptanceInterval( "R", 0.002d ), new ToleranceAcceptanceInterval( "S", 0.00025d ), new ToleranceAcceptanceInterval( "T", 0.0001d ), new ToleranceAcceptanceInterval( "U", 0.0003d ), new ToleranceAcceptanceInterval( "V", 0.00005d ), new ToleranceAcceptanceInterval( "Y", 0.00015d ), UnknownValue, UserValue };
            BuildDictionary( dix );
        }

        /// <summary> Parses. </summary>
        /// <remarks> David, 2020-04-20. </remarks>
        /// <param name="code"> The code. </param>
        /// <returns> A ToleranceAcceptanceInterval. </returns>
        public static ToleranceAcceptanceInterval Parse( string code )
        {
            var acceptanceIntervals = Dictionary();
            return acceptanceIntervals.Contains( code ) ? new ToleranceAcceptanceInterval( acceptanceIntervals[code] ) : null;
        }

        /// <summary> Tries to parse a coded value. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="code">    The code. </param>
        /// <param name="value">   [in,out] The Margin Factor. </param>
        /// <param name="details"> [in,out] The details. </param>
        /// <returns> <c>True</c> if Margin Factor code can be parsed. </returns>
        public static bool TryParse( string code, ref ToleranceAcceptanceInterval value, ref string details )
        {
            var (Success, Value, Details) = TryParse( code );
            value = Value;
            details = Details;
            return Success;
        }

        /// <summary> Tries to parse a coded value. </summary>
        /// <remarks> David, 2020-04-18. </remarks>
        /// <param name="code"> The code. </param>
        /// <returns> <c>True</c> if Margin Factor code can be parsed. </returns>
        public static (bool Success, ToleranceAcceptanceInterval Value, string Details) TryParse( string code )
        {
            var acceptanceIntervals = ToleranceAcceptanceInterval.Dictionary();
            return acceptanceIntervals.Contains( code )
                    ? (true, new ToleranceAcceptanceInterval( acceptanceIntervals[code] ), String.Empty)
                    : (false, ToleranceAcceptanceInterval.Empty, $"{ nameof( ToleranceAcceptanceInterval )}.{ nameof( ToleranceAcceptanceInterval.Code )} '{code}' is unknown");
        }

        /// <summary>
        /// Searches for the first tolerance information matching the specific tolerance.
        /// </summary>
        /// <remarks> David, 2020-04-18. </remarks>
        /// <param name="tolerance"> The tolerance value to find. </param>
        /// <param name="epsilon">   The epsilon. </param>
        /// <returns> The found tolerance information. </returns>
        public static (bool Success, ToleranceAcceptanceInterval Value) Find( double tolerance, double epsilon )
        {
            (bool Success, ToleranceAcceptanceInterval Value) result = (false, Empty);
            foreach ( ToleranceAcceptanceInterval toleranceAcceptInterval in Dictionary() )
            {
                if ( Math.Abs( tolerance - toleranceAcceptInterval.RelativeInterval.HighEndPoint ) <= epsilon )
                {
                    result = (true, toleranceAcceptInterval);
                }
            }

            return result;
        }

        /// <summary> Gets the sentinel indicating if the entered value is Empty defined. </summary>
        /// <value> <c>True</c> if unknown code. </value>
        public override bool IsEmptyCode => string.Equals( this.Code, EmptyCode, StringComparison.OrdinalIgnoreCase );

        /// <summary> Gets the sentinel indicating if the entered code is unknown. </summary>
        /// <value> <c>True</c> if unknown code. </value>
        public override bool IsUnknownCode => string.Equals( this.Code, UnknownCode, StringComparison.OrdinalIgnoreCase );

        /// <summary> Gets the sentinel indicating if the entered value is user defined. </summary>
        /// <value> <c>True</c> if unknown code. </value>
        public override bool IsUserCode => string.Equals( this.Code, UserCode, StringComparison.OrdinalIgnoreCase );
    }
}
