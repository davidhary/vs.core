using System;
using System.Collections.Generic;

namespace isr.Core.Engineering
{

    /// <summary>
    /// British Standard (BS EN 60062) Temperature coefficient acceptance intervals.
    /// </summary>
    /// <remarks>
    /// This class is sealed to ensure that the hash value of its elements is not used
    /// by two instances with different hash value set. (c) 2014 Integrated Scientific
    /// Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-05-19 </para>
    /// </remarks>
    public sealed class BritishStadnardTcrInterval : AcceptanceInterval
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="code"> The temperature coefficient code. </param>
        public BritishStadnardTcrInterval( string code ) : base()
        {
            if ( string.IsNullOrWhiteSpace( code ) )
            {
                this.Parsed = false;
            }
            else
            {
                var info = Parse( code );
                this.Parsed = info is object;
                if ( this.Parsed )
                {
                    this.Code = code;
                    this.RelativeInterval = new TcrInterval( info.RelativeInterval );
                    this.Caption = info.Caption;
                    this.CompoundCaption = BuildCompoundCaptionFormat( code, this.Caption );
                }
            }
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="code">             The temperature coefficient code. </param>
        /// <param name="lowerCoefficient"> The lower Coefficient. </param>
        /// <param name="upperCoefficient"> The upper Coefficient. </param>
        /// <param name="caption">          The caption. </param>
        public BritishStadnardTcrInterval( string code, double lowerCoefficient, double upperCoefficient, string caption ) : base( code, new TcrInterval( lowerCoefficient, upperCoefficient ), caption )
        {
        }

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> The value. </param>
        public BritishStadnardTcrInterval( AcceptanceInterval value ) : this( value.Code, value.RelativeInterval, value.Caption )
        {
        }

        /// <summary> Constructor for symmetric range and standard caption. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="code">        The tolerance code. </param>
        /// <param name="coefficient"> The coefficient. </param>
        public BritishStadnardTcrInterval( string code, Interval coefficient ) : base( code, coefficient, BritishStadnardTcr.BuildCaption( coefficient.HighEndPoint ) )
        {
        }

        /// <summary> Constructor for symmetric range. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="code">        The temperature coefficient code. </param>
        /// <param name="coefficient"> The coefficient. </param>
        /// <param name="caption">     The caption. </param>
        public BritishStadnardTcrInterval( string code, Interval coefficient, string caption ) : base( code, coefficient, caption )
        {
        }

        /// <summary> Constructor for symmetric range and standard caption. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="code">        The tolerance code. </param>
        /// <param name="coefficient"> The coefficient. </param>
        public BritishStadnardTcrInterval( string code, double coefficient ) : this( code, coefficient, BritishStadnardTcr.BuildCaption( coefficient ) )
        {
        }

        /// <summary> Constructor for symmetric range. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="code">        The temperature coefficient code. </param>
        /// <param name="coefficient"> The coefficient. </param>
        /// <param name="caption">     The caption. </param>
        public BritishStadnardTcrInterval( string code, double coefficient, string caption ) : this( code, new ToleranceInterval( coefficient ), caption )
        {
        }

        /// <summary> The clone Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> The value. </param>
        public BritishStadnardTcrInterval( BritishStadnardTcrInterval value ) : this( value.Code, value.RelativeInterval, value.Caption )
        {
        }

        /// <summary> Gets the empty value. </summary>
        /// <value> The empty. </value>
        public static BritishStadnardTcrInterval Empty => new BritishStadnardTcrInterval( EmptyCode, 0d );

        /// <summary> The dictionary. </summary>
        private static AcceptanceIntervalCollection _Dictionary;

        /// <summary> Gets the <see cref="AcceptanceIntervalCollection">dictionary</see>. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> A Dictionary(Of String, CodedIntervalBase). </returns>
        public static AcceptanceIntervalCollection Dictionary()
        {
            if ( _Dictionary is null || _Dictionary.Count == 0 )
            {
                BuildDictionary();
            }

            return _Dictionary;
        }

        /// <summary> Builds coded interval base dictionary. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="values"> The values. </param>
        public static void BuildDictionary( AcceptanceIntervalCollection values )
        {
            _Dictionary = new AcceptanceIntervalCollection();
            _Dictionary.Populate( values );
        }

        /// <summary> Builds Coefficient information dictionary. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        private static void BuildDictionary()
        {
            var dix = new AcceptanceIntervalCollection();
            foreach ( KeyValuePair<string, double> kvp in BritishStadnardTcr.Dictionary() )
            {
                dix.Add( new BritishStadnardTcrInterval( kvp.Key, kvp.Value ) );
            }

            BuildDictionary( dix );
        }

        /// <summary> Parses. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="code"> The code. </param>
        /// <returns> A Temperature Coefficient Info. </returns>
        public static BritishStadnardTcrInterval Parse( string code )
        {
            var acceptanceIndervals = Dictionary();
            return acceptanceIndervals.Contains( code ) ? new BritishStadnardTcrInterval( acceptanceIndervals[code] ) : null;
        }

        /// <summary> Tries to parse a coded value. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="code">    The code. </param>
        /// <param name="value">   [in,out] The Margin Factor. </param>
        /// <param name="details"> [in,out] The details. </param>
        /// <returns> <c>True</c> if Margin Factor code can be parsed. </returns>
        public static bool TryParse( string code, ref BritishStadnardTcrInterval value, ref string details )
        {
            bool affirmative = false;
            var acceptanceIndervals = Dictionary();
            if ( acceptanceIndervals.Contains( code ) )
            {
                value = new BritishStadnardTcrInterval( acceptanceIndervals[code] );
                affirmative = true;
            }
            else
            {
                details = $"{ nameof( BritishStadnardTcrInterval.GetType )}.{ nameof( BritishStadnardTcrInterval.Code )} '{code}' is unknown";
            }

            return affirmative;
        }

        /// <summary> Gets the sentinel indicating if the entered value is Empty defined. </summary>
        /// <value> <c>True</c> if unknown code. </value>
        public override bool IsEmptyCode => string.Equals( this.Code, EmptyCode, StringComparison.OrdinalIgnoreCase );

        /// <summary> Gets the sentinel indicating if the entered code is unknown. </summary>
        /// <value> <c>True</c> if unknown code. </value>
        public override bool IsUnknownCode => string.Equals( this.Code, UnknownCode, StringComparison.OrdinalIgnoreCase );

        /// <summary> Gets the sentinel indicating if the entered value is user defined. </summary>
        /// <value> <c>True</c> if unknown code. </value>
        public override bool IsUserCode => string.Equals( this.Code, UserCode, StringComparison.OrdinalIgnoreCase );
    }
}
