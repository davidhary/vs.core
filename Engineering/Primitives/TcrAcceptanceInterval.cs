using System;

namespace isr.Core.Engineering
{
    /// <summary>
    /// Defines the Temperature Coefficient of Resistance (TCR) acceptance interval.
    /// </summary>
    /// <remarks>
    /// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-09-22 </para>
    /// </remarks>
    public class TcrAcceptanceInterval : AcceptanceInterval
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="code"> The temperature coefficient code. </param>
        public TcrAcceptanceInterval( string code ) : base()
        {
            if ( string.IsNullOrWhiteSpace( code ) )
            {
                this.Parsed = false;
            }
            else
            {
                var info = Parse( code );
                this.Parsed = info is object;
                if ( this.Parsed )
                {
                    this.Code = code;
                    this.RelativeInterval = new TcrInterval( info.RelativeInterval );
                    this.Caption = info.Caption;
                    this.CompoundCaption = BuildCompoundCaptionFormat( code, this.Caption );
                }
            }
        }

        /// <summary> Constructor for symmetric interval. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="code">        The temperature coefficient code. </param>
        /// <param name="coefficient"> The coefficient interval. </param>
        /// <param name="caption">     The caption. </param>
        public TcrAcceptanceInterval( string code, Interval coefficient, string caption ) : base( code, coefficient, caption )
        {
        }

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> The acceptance interval. </param>
        protected TcrAcceptanceInterval( AcceptanceInterval value ) : this( value.Code, value.RelativeInterval.HighEndPoint, value.Caption )
        {
        }

        /// <summary> Constructor for symmetric interval and standard caption. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="code">        The tolerance code. </param>
        /// <param name="coefficient"> The coefficient  interval. </param>
        public TcrAcceptanceInterval( string code, Interval coefficient ) : this( code, coefficient, BuildCaption( coefficient.HighEndPoint ) )
        {
        }

        /// <summary> Constructor for symmetric interval and standard caption. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="code">        The tolerance code. </param>
        /// <param name="coefficient"> The coefficient. </param>
        public TcrAcceptanceInterval( string code, double coefficient ) : this( code, coefficient, BuildCaption( coefficient ) )
        {
        }

        /// <summary> Constructor for symmetric interval. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="code">        The temperature coefficient code. </param>
        /// <param name="coefficient"> The coefficient. </param>
        /// <param name="caption">     The caption. </param>
        public TcrAcceptanceInterval( string code, double coefficient, string caption ) : this( code, new TcrInterval( coefficient ), caption )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="lowerCoefficient"> The lower Coefficient. </param>
        /// <param name="upperCoefficient"> The upper Coefficient. </param>
        public TcrAcceptanceInterval( double lowerCoefficient, double upperCoefficient ) : this( UserCode, new TcrInterval( lowerCoefficient, upperCoefficient ), BuildCaption( lowerCoefficient, upperCoefficient, CaptionFormat ) )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="code">             The temperature coefficient code. </param>
        /// <param name="lowerCoefficient"> The lower Coefficient. </param>
        /// <param name="upperCoefficient"> The upper Coefficient. </param>
        /// <param name="caption">          The caption. </param>
        public TcrAcceptanceInterval( string code, double lowerCoefficient, double upperCoefficient, string caption ) : this( code, new TcrInterval( lowerCoefficient, upperCoefficient ), caption )
        {
        }

        /// <summary> The cloning constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> The value. </param>
        public TcrAcceptanceInterval( TcrAcceptanceInterval value ) : this( value.Code, value.RelativeInterval.LowEndPoint, value.RelativeInterval.HighEndPoint, value.Caption )
        {
        }

        /// <summary> Gets the empty. </summary>
        /// <value> The empty. </value>
        public static TcrAcceptanceInterval Empty => new TcrAcceptanceInterval( EmptyCode, 0d );

        /// <summary> Gets or sets the units caption. </summary>
        /// <value> The units format. </value>
        public static string UnitsCaption { get; set; } = "ppm/°C";

        /// <summary> Gets or sets the caption format. </summary>
        /// <value> The caption format. </value>
        public static string CaptionFormat { get; set; } = "±{0} " + UnitsCaption;

        /// <summary> Builds a caption. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns>
        /// A String value using the
        /// <see cref="TcrAcceptanceInterval.CaptionFormat">caption format</see>.
        /// </returns>
        public static string BuildCaption( double value )
        {
            double scaleFactor = 1d;
            if ( CaptionFormat.Contains( "ppm" ) )
            {
                scaleFactor = 1000000.0d;
            }

            return BuildCaption( value, scaleFactor, CaptionFormat );
        }

        /// <summary> Builds a caption. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value">       The value. </param>
        /// <param name="scaleFactor"> The scale factor. </param>
        /// <param name="format">      Describes the format to use. </param>
        /// <returns>
        /// A String value using the
        /// <see cref="TcrAcceptanceInterval.CaptionFormat">caption format</see>.
        /// </returns>
        public static string BuildCaption( double value, double scaleFactor, string format )
        {
            return string.Format( System.Globalization.CultureInfo.CurrentCulture, format, scaleFactor * value );
        }

        /// <summary> The unknown Coefficient value. </summary>
        /// <value> The unknown value. </value>
        public static TcrAcceptanceInterval UnknownValue { get; set; } = new TcrAcceptanceInterval( "??", 0.00025d );

        /// <summary> Gets or sets the unknown code. </summary>
        /// <value> The unknown code. </value>
        public static new string UnknownCode
        {
            get => UnknownValue.Code;

            set => UnknownValue = new TcrAcceptanceInterval( value, UnknownValue.RelativeInterval );
        }

        /// <summary> The user coefficient value. </summary>
        /// <value> The user value. </value>
        public static TcrAcceptanceInterval UserValue { get; set; } = new TcrAcceptanceInterval( "@@", 0.001d );

        /// <summary> Gets or sets the user code. </summary>
        /// <value> The user code. </value>
        public static new string UserCode
        {
            get => UserValue.Code;

            set => UserValue = new TcrAcceptanceInterval( value, UserValue.RelativeInterval );
        }

        /// <summary> The dictionary. </summary>
        private static AcceptanceIntervalCollection _Dictionary;

        /// <summary> Gets the <see cref="AcceptanceIntervalCollection">dictionary</see>. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> A Dictionary(Of String, CodedIntervalBase). </returns>
        public static AcceptanceIntervalCollection Dictionary()
        {
            if ( _Dictionary is null || _Dictionary.Count == 0 )
            {
                BuildDictionary();
            }

            return _Dictionary;
        }

        /// <summary> Builds coded interval base dictionary. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="values"> The values. </param>
        public static void BuildDictionary( AcceptanceIntervalCollection values )
        {
            _Dictionary = new AcceptanceIntervalCollection();
            _Dictionary.Populate( values );
        }

        /// <summary> Builds Coefficient information dictionary. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        private static void BuildDictionary()
        {
            var dix = new AcceptanceIntervalCollection() { new TcrAcceptanceInterval( "00", -0.000125d, -0.000075d, "-100±25 " + UnitsCaption ), new TcrAcceptanceInterval( "01", 0.0001d ), new TcrAcceptanceInterval( "02", 0.00005d ), new TcrAcceptanceInterval( "03", 0.000025d ), new TcrAcceptanceInterval( "10", 0.00002d ), new TcrAcceptanceInterval( "11", 0.000015d ), new TcrAcceptanceInterval( "12", 0.00001d ), new TcrAcceptanceInterval( "13", 0.000005d ), new TcrAcceptanceInterval( "04", 0.0003d ), new TcrAcceptanceInterval( "05", 0.0001d ), new TcrAcceptanceInterval( "06", 0.00005d ), new TcrAcceptanceInterval( "07", 0.00025d ), new TcrAcceptanceInterval( "14", 0.00002d ), new TcrAcceptanceInterval( "15", 0.000015d ), new TcrAcceptanceInterval( "16", 0.00001d ), new TcrAcceptanceInterval( "17", 0.000005d ), new TcrAcceptanceInterval( "99", 0.00025d ), new TcrAcceptanceInterval( "08", 0.00025d ), UnknownValue }; // Commercial
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                // Commercial
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                // Commercial
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                // Commercial
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                // Commercial
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                // Commercial
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                // Commercial
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                // Screened
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                // Screened
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                // Screened
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                // Screened
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                // Screened
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                // Screened
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                // Screened
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                // Screened
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                // Screened
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                // Commercial
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                // Screened
            BuildDictionary( dix );
        }

        /// <summary> Tries to parse a coded value. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="code">    The code. </param>
        /// <param name="value">   [in,out] The Margin Factor. </param>
        /// <param name="details"> [in,out] The details. </param>
        /// <returns> <c>True</c> if Margin Factor code can be parsed. </returns>
        public static bool TryParse( string code, ref TcrAcceptanceInterval value, ref string details )
        {
            var (Success, Value, Details) = TryParse( code );
            value = Value;
            details = Details;
            return Success;
        }

        /// <summary> Tries to parse a coded value. </summary>
        /// <remarks> David, 2020-04-18. </remarks>
        /// <param name="code"> The code. </param>
        /// <returns> <c>True</c> if Margin Factor code can be parsed. </returns>
        public static (bool Success, TcrAcceptanceInterval Value, string Details) TryParse( string code )
        {
            var acceptanceIntervals = TcrAcceptanceInterval.Dictionary();
            return acceptanceIntervals.Contains( code )
                ? (true, new TcrAcceptanceInterval( acceptanceIntervals[code] ), String.Empty)
                : (false, TcrAcceptanceInterval.Empty, $"{ nameof( TcrAcceptanceInterval ) }.{ nameof( TcrAcceptanceInterval.Code )} '{code}' is unknown");
        }

        /// <summary> Parses. </summary>
        /// <remarks> David, 2020-04-20. </remarks>
        /// <param name="code"> The code. </param>
        /// <returns> A TcrAcceptanceInterval. </returns>
        public static TcrAcceptanceInterval Parse( string code )
        {
            var acceptanceIntervals = Dictionary();
            return acceptanceIntervals.Contains( code ) ? new TcrAcceptanceInterval( acceptanceIntervals[code] ) : null;
        }

        /// <summary> Gets the sentinel indicating if the entered value is Empty defined. </summary>
        /// <value> <c>True</c> if unknown code. </value>
        public override bool IsEmptyCode => string.Equals( this.Code, EmptyCode, StringComparison.OrdinalIgnoreCase );

        /// <summary> Gets the sentinel indicating if the entered code is unknown. </summary>
        /// <value> <c>True</c> if unknown code. </value>
        public override bool IsUnknownCode => string.Equals( this.Code, UnknownCode, StringComparison.OrdinalIgnoreCase );

        /// <summary> Gets the sentinel indicating if the entered value is user defined. </summary>
        /// <value> <c>True</c> if unknown code. </value>
        public override bool IsUserCode => string.Equals( this.Code, UserCode, StringComparison.OrdinalIgnoreCase );
    }
}
