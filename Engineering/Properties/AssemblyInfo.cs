using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.Core.Engineering.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.Core.Engineering.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.Core.Engineering.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]

[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( isr.Core.Engineering.My.MyLibrary.TestAssemblyStrongName )]
