
namespace isr.Core.Engineering.My
{

    /// <summary> Provides assembly information for the class library. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    public sealed partial class MyLibrary
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        private MyLibrary() : base()
        {
        }

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "Core Engineering Library";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "Core Engineering Library";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "Core.Engineering";

        /// <summary> The Strong Name of the test assembly. </summary>
        public const string TestAssemblyStrongName = "isr.Core.EngineeringTests,PublicKey=" + isr.Core.My.SolutionInfo.PublicKey;

    }
}
