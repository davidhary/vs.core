## ISR Core Engineering<sub>&trade;</sub>: Core Engineering Class Library
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Revision History [](#){name=Revision-History}

*5.0.7280 2019-12-07*  
Uses now Services, Model and Constructs created from
agnostic.

*3.3.6655 2018-03-12*  
Moving Window Filter: Adds acceptance range; Keeps
track of indeterminable and range failures.

*3.3.6481 2017-09-29*  
Wheatstone: Adds relative imbalance and deviation.
Calculates compensation shunts and series resistors. Adds unit tests for
compensation.

*3.3.6477 2017-09-27*  
Changes implementation of current and voltage divider
to raise an exception if both values are short or open. Encapsulates the
balance bridge structure to allow testing of alternative balance
elements layouts.

*3.3.6475 2017-09-23*  
Adds element order for creating a Wheatstone bridge.
Adds parallel resistance and conductance pair value.

*3.1.6466 2017-09-14*  
Process control statistics: Adds mean and sigma. Uses
sigma to return the Has value property.

*3.1.6384 2017-06-24*  
Defaults to UTC time.

*3.1.6104 2016-09-17*  
Moving Window: Adds percent completion properties.

*3.0.5866 2016-01-23*  
Updates to .NET 4.6.1

*2.1.5376 2014-09-20*  
Created to hold engineering entities.

\(C\) 2014 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Core Engineering Library](https://bitbucket.org/davidhary/vs.core)  
[Linq Statistics]{.underline}**](http://www.codeproject.com/Articles/42492/Using-LINQ-to-Calculate-Basic-Statistics)
