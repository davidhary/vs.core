using System;

namespace isr.Core.Engineering
{

    /// <summary> A cubic polynomial. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-02-06 </para>
    /// </remarks>
    public class CubicPolynomial
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        public CubicPolynomial() : base()
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> Information describing the polynomial. </param>
        public CubicPolynomial( CubicPolynomial value ) : this()
        {
            if ( value is object )
            {
                this.ConstantCoefficient = value.ConstantCoefficient;
                this.LinearCoefficient = value.LinearCoefficient;
                this.QuadraticCoefficient = value.QuadraticCoefficient;
                this.CubicCoefficient = value.CubicCoefficient;
            }
        }

        /// <summary> Gets or sets the constant coefficient. </summary>
        /// <value> The constant coefficient. </value>
        public double ConstantCoefficient { get; set; }

        /// <summary> Gets or sets the linear coefficient. </summary>
        /// <value> The linear coefficient. </value>
        public double LinearCoefficient { get; set; }

        /// <summary> Gets or sets the quadratic coefficient. </summary>
        /// <value> The quadratic coefficient. </value>
        public double QuadraticCoefficient { get; set; }

        /// <summary> Gets or sets the cubic coefficient. </summary>
        /// <value> The cubic coefficient. </value>
        public double CubicCoefficient { get; set; }

        /// <summary> Evaluates. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> Information describing the polynomial. </param>
        /// <returns> A Double. </returns>
        public double Evaluate( double value )
        {
            return this.ConstantCoefficient + value * (this.LinearCoefficient + value * (this.QuadraticCoefficient + value * this.CubicCoefficient));
        }

        /// <summary> Square root. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> Information describing the polynomial. </param>
        /// <returns> A Double. </returns>
        private double SquareRoot( double value )
        {
            return Math.Sqrt( this.LinearCoefficient * this.LinearCoefficient - 4d * this.QuadraticCoefficient * (this.ConstantCoefficient - value) );
        }

        /// <summary> Calculates the positive quadratic root for the given value. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> Information describing the polynomial. </param>
        /// <returns> A Double. </returns>
        public double PositiveQuadraticRoot( double value )
        {
            return (-this.LinearCoefficient + this.SquareRoot( value )) / (2d * this.QuadraticCoefficient);
        }

        /// <summary> Calculates the positive quadratic root for the given value. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> Information describing the polynomial. </param>
        /// <returns> A Double. </returns>
        public double NegativeQuadraticRoot( double value )
        {
            return (-this.LinearCoefficient - this.SquareRoot( value )) / (2d * this.QuadraticCoefficient);
        }
    }
}
