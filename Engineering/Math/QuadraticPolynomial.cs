using System;
using System.Collections.Generic;
using System.Linq;

namespace isr.Core.Engineering
{

    /// <summary> A quadratic polynomial. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-02-06 </para>
    /// </remarks>
    public class QuadraticPolynomial
    {

        #region " CONSTRUCTION "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        public QuadraticPolynomial() : base()
        {
            this.GoodnessOfFit = double.NaN;
            this.StandardError = double.NaN;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="constantCoefficient">  The constant coefficient. </param>
        /// <param name="linearCoefficient">    The linear coefficient. </param>
        /// <param name="quadraticCoefficient"> The quadratic coefficient. </param>
        public QuadraticPolynomial( double constantCoefficient, double linearCoefficient, double quadraticCoefficient ) : this()
        {
            this.ConstantCoefficient = constantCoefficient;
            this.LinearCoefficient = linearCoefficient;
            this.QuadraticCoefficient = quadraticCoefficient;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> Information describing the polynomial. </param>
        public QuadraticPolynomial( QuadraticPolynomial value ) : this()
        {
            if ( value is object )
            {
                this.ConstantCoefficient = value.ConstantCoefficient;
                this.LinearCoefficient = value.LinearCoefficient;
                this.QuadraticCoefficient = value.QuadraticCoefficient;
            }
        }

        /// <summary> Gets the constant coefficient. </summary>
        /// <value> The constant coefficient. </value>
        public double ConstantCoefficient { get; set; }

        /// <summary> Gets the linear coefficient. </summary>
        /// <value> The linear coefficient. </value>
        public double LinearCoefficient { get; set; }

        /// <summary> Gets the quadratic coefficient. </summary>
        /// <value> The quadratic coefficient. </value>
        public double QuadraticCoefficient { get; set; }

        #endregion

        #region " EVALUATION "

        /// <summary> Evaluates the polynomial at the specified value. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> The value for evaluating the polynomial. </param>
        /// <returns> A Double. </returns>
        public double Evaluate( double value )
        {
            return this.ConstantCoefficient + value * (this.LinearCoefficient + value * this.QuadraticCoefficient);
        }

        /// <summary> Evaluates the slope at the specified value. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> The value for evaluating the derivative. </param>
        /// <returns> A Double. </returns>
        public double Slope( double value )
        {
            return this.LinearCoefficient + 2d * value * this.QuadraticCoefficient;
        }

        #endregion

        #region " ROOTS "

        /// <summary> Square root. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> Information describing the polynomial. </param>
        /// <returns> A Double. </returns>
        private double SquareRoot( double value )
        {
            return Math.Sqrt( this.LinearCoefficient * this.LinearCoefficient - 4d * this.QuadraticCoefficient * (this.ConstantCoefficient - value) );
        }

        /// <summary> Calculates the positive quadratic root for the given value. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> Information describing the polynomial. </param>
        /// <returns> A Double. </returns>
        public double PositiveQuadraticRoot( double value )
        {
            return (-this.LinearCoefficient + this.SquareRoot( value )) / (2d * this.QuadraticCoefficient);
        }

        /// <summary> Calculates the positive quadratic root for the given value. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="value"> Information describing the polynomial. </param>
        /// <returns> A Double. </returns>
        public double NegativeQuadraticRoot( double value )
        {
            return (-this.LinearCoefficient - this.SquareRoot( value )) / (2d * this.QuadraticCoefficient);
        }

        #endregion

        #region " GOODNESS OF FIT "

        /// <summary> Gets the standard error. </summary>
        /// <value> The standard error. </value>
        public double StandardError { get; private set; }

        /// <summary> Gets the goodness of fit (R-Squared). </summary>
        /// <value> The goodness of fit (R-Squared). </value>
        public double GoodnessOfFit { get; private set; }

        /// <summary> Gets the correlation coefficient. </summary>
        /// <value> The correlation coefficient. </value>
        public double CorrelationCoefficient => Math.Sqrt( this.GoodnessOfFit );

        /// <summary> Sum squared deviations. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values"> The values. </param>
        /// <returns> The total number of squared deviations. </returns>
        public double SumSquaredDeviations( IList<System.Windows.Point> values )
        {
            if ( values is null )
            {
                throw new ArgumentNullException( nameof( values ) );
            }

            double result = 0d;
            foreach ( System.Windows.Point p in values )
            {
                double temp = p.Y - this.Evaluate( p.X );
                result += temp * temp;
            }

            return result;
        }

        /// <summary> Sum squared mean deviations. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values"> The values. </param>
        /// <returns> The total number of squared mean deviations. </returns>
        public static double SumSquaredMeanDeviations( IList<System.Windows.Point> values )
        {
            if ( values is null )
            {
                throw new ArgumentNullException( nameof( values ) );
            }

            double mean = 0d;
            double result = 0d;
            if ( values.Any() )
            {
                foreach ( System.Windows.Point p in values )
                {
                    mean += p.Y;
                }

                mean /= values.Count;
                foreach ( System.Windows.Point p in values )
                {
                    double temp = p.Y - mean;
                    result += temp * temp;
                }
            }

            return result;
        }

        /// <summary> Sum squared deviations. </summary>
        /// <remarks> David, 2020-09-05. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="x"> A Double() to process. </param>
        /// <param name="y"> A Double() to process. </param>
        /// <returns> The total number of squared deviations. </returns>
        public double SumSquaredDeviations( double[] x, double[] y )
        {
            if ( x is null )
            {
                throw new ArgumentNullException( nameof( x ) );
            }

            if ( y is null )
            {
                throw new ArgumentNullException( nameof( y ) );
            }

            if ( x.Length != y.Length )
            {
                throw new InvalidOperationException( $"{nameof( x )} {nameof( x.Length )} {x.Length} must equal {nameof( y )} {nameof( y.Length )} {y.Length} " );
            }

            double result = 0d;
            if ( x.Length > 0 )
            {
                double temp;
                for ( int i = 0, loopTo = x.Length - 1; i <= loopTo; i++ )
                {
                    temp = y[i] - this.Evaluate( x[i] );
                    result += temp * temp;
                }
            }

            return result;
        }

        /// <summary> Sum squared mean deviations. </summary>
        /// <remarks> David, 2020-09-05. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="x"> A Double() to process. </param>
        /// <param name="y"> A Double() to process. </param>
        /// <returns> The total number of squared mean deviations. </returns>
        public static double SumSquaredMeanDeviations( double[] x, double[] y )
        {
            if ( x is null )
            {
                throw new ArgumentNullException( nameof( x ) );
            }

            if ( y is null )
            {
                throw new ArgumentNullException( nameof( y ) );
            }

            if ( x.Length != y.Length )
            {
                throw new InvalidOperationException( $"{nameof( x )} {nameof( x.Length )} {x.Length} must equal {nameof( y )} {nameof( y.Length )} {y.Length} " );
            }

            double mean = 0d;
            double result = 0d;
            if ( x.Length > 0 )
            {
                for ( int i = 0, loopTo = x.Length - 1; i <= loopTo; i++ )
                {
                    mean += y[i];
                }

                mean /= x.Length;
                double temp;
                for ( int i = 0, loopTo1 = x.Length - 1; i <= loopTo1; i++ )
                {
                    temp = y[i] - mean;
                    result += temp * temp;
                }
            }

            return result;
        }

        #endregion

        #region " POLY FIT "

        /// <summary> Fits a second order polynomial to the values. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="InvalidOperationException">   Thrown when the requested operation is
        /// invalid. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="values"> The values. </param>
        /// <returns> A Double. </returns>
        private double PolyFitThis( IList<System.Windows.Point> values )
        {
            if ( values is null )
            {
                throw new ArgumentNullException( nameof( values ) );
            }

            if ( !values.Any() )
            {
                throw new InvalidOperationException( "Input is empty." );
            }

            if ( values.Count < 3 )
            {
                throw new ArgumentOutOfRangeException( nameof( values ), "Input is too short; requires at least 3 elements." );
            }

            this.ConstantCoefficient = 0d;
            this.LinearCoefficient = 0d;
            this.QuadraticCoefficient = 0d;

            // setup the normal equations. 
            int dimension = 2;
            var a = new double[dimension + 1, dimension + 1 + 1];
            var coefficients = new double[dimension + 1, dimension + 1];
            var constants = new double[dimension + 1];
            int dp1 = dimension + 1;
            for ( int i = 0, loopTo = dimension; i <= loopTo; i++ )
            {
                for ( int j = 0, loopTo1 = dimension; j <= loopTo1; j++ )
                {
                    int k = i + j;
                    for ( int l = 0, loopTo2 = values.Count - 1; l <= loopTo2; l++ )
                    {
                        a[i, j] += Math.Pow( values[l].X, k );
                    }

                    coefficients[i, j] = a[i, j];
                }

                foreach ( System.Windows.Point p in values )
                {
                    a[i, dp1] += p.Y * Math.Pow( p.X, i );
                }

                constants[i] = a[i, dp1];
            }

            double d = Determinant( coefficients );
            this.GoodnessOfFit = double.NaN;
            this.StandardError = double.NaN;
            if ( Math.Abs( d ) > float.Epsilon )
            {
                var p = CramerRule( coefficients, constants );
                this.ConstantCoefficient = p[0] / d;
                this.LinearCoefficient = p[1] / d;
                this.QuadraticCoefficient = p[2] / d;
                double ssq = this.SumSquaredDeviations( values );
                this.StandardError = Math.Sqrt( ssq / values.Count );
                double ssqdm = SumSquaredMeanDeviations( values );
                if ( ssqdm - ssq > float.Epsilon )
                {
                    // otherwise, correlation coefficient could be negative
                    this.GoodnessOfFit = (ssqdm - ssq) / ssqdm;
                }
                else
                {
                    this.GoodnessOfFit = 0d;
                }
            }

            return this.GoodnessOfFit;
        }

        /// <summary> Polynomial fit. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="values"> The values. </param>
        /// <returns> A Double. </returns>
        public double PolyFit( IList<System.Windows.Point> values )
        {
            if ( values is null )
            {
                throw new ArgumentNullException( nameof( values ) );
            }

            if ( !values.Any() )
            {
                throw new InvalidOperationException( "Input is empty." );
            }

            this.ConstantCoefficient = 0d;
            this.LinearCoefficient = 0d;
            this.QuadraticCoefficient = 0d;
            if ( values.Count == 1 )
            {
                this.ConstantCoefficient = values[0].Y;
                this.GoodnessOfFit = 1d;
                this.StandardError = 0d;
            }
            else if ( values.Count == 2 )
            {
                double dx = values[1].X - values[0].X;
                double dy = values[1].Y - values[0].Y;
                if ( dx > float.Epsilon )
                {
                    this.LinearCoefficient = dy / dx;
                    this.ConstantCoefficient = values[0].Y - values[0].X * this.LinearCoefficient;
                    this.GoodnessOfFit = 1d;
                    this.StandardError = 0d;
                }
                else
                {
                    this.ConstantCoefficient = 0.5d * (values[1].Y + values[0].Y);
                    this.GoodnessOfFit = 0d;
                    this.StandardError = this.ConstantCoefficient;
                }
            }
            else
            {
                _ = this.PolyFitThis( values );
            }

            return this.GoodnessOfFit;
        }

        /// <summary> Fits a second order polynomial to the values. </summary>
        /// <remarks> David, 2020-09-05. </remarks>
        /// <param name="x"> A Double() to process. </param>
        /// <param name="y"> A Double() to process. </param>
        /// <returns> A Double. </returns>
        private double PolyFitThis( double[] x, double[] y )
        {
            this.ConstantCoefficient = 0d;
            this.LinearCoefficient = 0d;
            this.QuadraticCoefficient = 0d;

            // setup the normal equations. 
            int dimension = 2;
            var a = new double[dimension + 1, dimension + 1 + 1];
            var coefficients = new double[dimension + 1, dimension + 1];
            var constants = new double[dimension + 1];
            int dp1 = dimension + 1;
            for ( int i = 0, loopTo = dimension; i <= loopTo; i++ )
            {
                for ( int j = 0, loopTo1 = dimension; j <= loopTo1; j++ )
                {
                    int k = i + j;
                    for ( int l = 0, loopTo2 = x.Length - 1; l <= loopTo2; l++ )
                    {
                        a[i, j] += Math.Pow( x[l], k );
                    }

                    coefficients[i, j] = a[i, j];
                }

                for ( int j = 0, loopTo3 = x.Length - 1; j <= loopTo3; j++ )
                {
                    a[i, dp1] += y[j] * Math.Pow( x[j], i );
                }

                constants[i] = a[i, dp1];
            }

            double d = Determinant( coefficients );
            this.GoodnessOfFit = double.NaN;
            this.StandardError = double.NaN;
            if ( Math.Abs( d ) > float.Epsilon )
            {
                var p = CramerRule( coefficients, constants );
                this.ConstantCoefficient = p[0] / d;
                this.LinearCoefficient = p[1] / d;
                this.QuadraticCoefficient = p[2] / d;
                double ssq = this.SumSquaredDeviations( x, y );
                this.StandardError = Math.Sqrt( ssq / x.Length );
                double ssqdm = SumSquaredMeanDeviations( x, y );
                if ( ssqdm - ssq > float.Epsilon )
                {
                    // otherwise, correlation coefficient could be negative
                    this.GoodnessOfFit = (ssqdm - ssq) / ssqdm;
                }
                else
                {
                    this.GoodnessOfFit = 0d;
                }
            }

            return this.GoodnessOfFit;
        }

        /// <summary> Polynomial fit. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="x"> A Double() to process. </param>
        /// <param name="y"> A Double() to process. </param>
        /// <returns> A Double. </returns>
        public double PolyFit( double[] x, double[] y )
        {
            if ( x is null )
            {
                throw new ArgumentNullException( nameof( x ) );
            }

            if ( y is null )
            {
                throw new ArgumentNullException( nameof( y ) );
            }

            if ( x.Length != y.Length )
            {
                throw new InvalidOperationException( $"{nameof( x )} {nameof( x.Length )} {x.Length} must equal {nameof( y )} {nameof( y.Length )} {y.Length} " );
            }

            this.ConstantCoefficient = 0d;
            this.LinearCoefficient = 0d;
            this.QuadraticCoefficient = 0d;
            if ( x.Length == 1 )
            {
                this.ConstantCoefficient = y[0];
                this.GoodnessOfFit = 1d;
                this.StandardError = 0d;
            }
            else if ( x.Length == 2 )
            {
                double dx = x[1] - x[0];
                double dy = y[1] - y[0];
                if ( dx > float.Epsilon )
                {
                    this.LinearCoefficient = dy / dx;
                    this.ConstantCoefficient = y[0] - x[0] * this.LinearCoefficient;
                    this.GoodnessOfFit = 1d;
                    this.StandardError = 0d;
                }
                else
                {
                    this.ConstantCoefficient = 0.5d * (y[1] + y[0]);
                    this.GoodnessOfFit = 0d;
                    this.StandardError = this.ConstantCoefficient;
                }
            }
            else
            {
                _ = this.PolyFitThis( x, y );
            }

            return this.GoodnessOfFit;
        }

        /// <summary> Builds the matrix for calculating the numerator of the Cramer Rule. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="index">        Zero-based index of the solution variable. </param>
        /// <param name="coefficients"> The coefficients. </param>
        /// <param name="constants">    The constants. </param>
        /// <returns> The matrix array of solution values times the determinant. </returns>
        public static double[,] CramerSubstitution( int index, double[,] coefficients, double[] constants )
        {
            if ( coefficients is null )
            {
                throw new ArgumentNullException( nameof( coefficients ) );
            }

            if ( constants is null )
            {
                throw new ArgumentNullException( nameof( constants ) );
            }

            if ( index < 0 )
            {
                throw new ArgumentOutOfRangeException( nameof( index ), "Must be non-negative" );
            }

            if ( index >= constants.Count() )
            {
                throw new ArgumentOutOfRangeException( nameof( index ), $"Must be less than {constants.Count()}" );
            }

            int d = constants.Count() - 1;
            var a = new double[d + 1, d + 1];
            for ( int col = 0, loopTo = d; col <= loopTo; col++ )
            {
                for ( int row = 0, loopTo1 = d; row <= loopTo1; row++ )
                {
                    a[col, row] = col == index ? constants[row] : coefficients[col, row];
                }
            }

            return a;
        }

        /// <summary> Use Cramer rule to calculate the solution of the linear equations. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="coefficients"> The coefficients. </param>
        /// <param name="constants">    The constants. </param>
        /// <returns> The array of solution values times the determinant. </returns>
        public static double[] CramerRule( double[,] coefficients, double[] constants )
        {
            if ( coefficients is null )
            {
                throw new ArgumentNullException( nameof( coefficients ) );
            }

            if ( constants is null )
            {
                throw new ArgumentNullException( nameof( constants ) );
            }

            var l = new List<double>();
            for ( int Xi = 0, loopTo = constants.Count() - 1; Xi <= loopTo; Xi++ )
            {
                var a = CramerSubstitution( Xi, coefficients, constants );
                l.Add( Determinant( a ) );
            }

            return l.ToArray();
        }

        /// <summary> Determinants. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="matrix"> The matrix values to process. </param>
        /// <returns> A Double. </returns>
        public static double Determinant( double[,] matrix )
        {
            if ( matrix is null )
            {
                throw new ArgumentNullException( nameof( matrix ) );
            }
            // If Not matrix.Any Then Throw New InvalidOperationException("matrix is empty.")
            if ( matrix.GetLength( 0 ) != 3 )
            {
                throw new ArgumentOutOfRangeException( nameof( matrix ), $"First dimension {matrix.GetLength( 0 )} must be 3." );
            }

            if ( matrix.GetLength( 1 ) != 3 )
            {
                throw new ArgumentOutOfRangeException( nameof( matrix ), $"Second dimension {matrix.GetLength( 1 )} must be 3." );
            }

            double result = 0d;
            result += matrix[0, 0] * matrix[1, 1] * matrix[2, 2];
            result -= matrix[0, 0] * matrix[1, 2] * matrix[2, 1];
            result -= matrix[0, 1] * matrix[1, 0] * matrix[2, 2];
            result += matrix[0, 1] * matrix[1, 2] * matrix[2, 0];
            result += matrix[0, 2] * matrix[1, 0] * matrix[2, 1];
            result -= matrix[0, 2] * matrix[1, 1] * matrix[2, 0];
            return result;
        }

        #endregion

    }
}
