using System;
using System.Collections.Generic;
using System.Linq;

namespace isr.Core.Statistics
{
    /// <summary>   A sample skewness. </summary>
    /// <remarks>   David, 2021-03-20. 
    /// <list type="bullet"><listheader>References</listheader><item>
    /// V. Barnet, T.  Lewis, Outliers in Statistical Data. 3rd Edition, 1994. Wiley Series in Probability and Mathematical Statistics. pp. 91-93, 100-101, 312-313.</item><item>
    /// NIST. Engineering Statistics Handbook. 1.3.5.11 Measures of Skewness and Kurtosis. https://www.itl.nist.gov/div898/handbook/eda/section3/eda35b.htm.</item><item>
    ///             </item><item>
    /// </item></list>            
    /// </remarks>
    public class SampleSkewness
    {

        #region " CONSTRUCTOR "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        public SampleSkewness() : base()
        {
            this.Sample = new SampleStatistics();
            this.ClearKnownStateThis();
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="values"> The values. </param>
        public SampleSkewness( double[] values ) : this()
        {
            if ( values is object )
            {
                this.Sample.AddValues( values );
                this.UpdateFences();
            }
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="sample"> The sample. </param>
        public SampleSkewness( SampleStatistics sample ) : this()
        {
            this.Sample = sample;
            this.UpdateFences();
        }

        #endregion

        #region " RESET AND CLEAR "

        /// <summary> Clears skewness values to their known (initial) state. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        private void ClearSkewnessThis()
        {
            this.LowerFence = 0;
            this.UpperFence = this.LowerFence;
            this.FilteredSample = new SampleStatistics();
        }

        /// <summary> Clears values to their known (initial) state. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        private void ClearKnownStateThis()
        {
            this.ClearSkewnessThis();
        }

        /// <summary> Clears values to their known (initial) state. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        public virtual void ClearKnownState()
        {
            this.Sample.ClearKnownState();
            this.ClearKnownStateThis();
        }

        #endregion

        #region " SAMPLE STATISTICS "

        /// <summary> Gets the sample. </summary>
        /// <value> The sample. </value>
        public SampleStatistics Sample { get; private set; }

        /// <summary> Gets the filtered sample with outliers removed. </summary>
        /// <value> The filtered sample. </value>
        public SampleStatistics FilteredSample { get; set; }

        /// <summary> Gets the number of outliers. </summary>
        /// <value> The number of outliers. </value>
        public int OutlierCount => this.Sample.Count - this.FilteredSample.Count;

        #endregion

        #region " FENCES "

        private static (int[] SampleSize, double[] Values) _CriticalValues =
            (new int[] { 5, 10, 15, 20, 25, 30, 40, 50, 60, 70, 80, 90, 100, 200, 500, 1000 },
             new double[] { 1.0, 0.9, 0.8, 0.8, 0.71, 0.66, 0.59, 0.53, 0.49, 0.46, 0.43, 0.41, 0.39, 0.28, 0.18, 0.13 });
        /// <summary>
        /// Critical values for 5% tests of discordancy for one or more outliers in a normal sample using
        /// the sample skewness test statistic. Barnett and Lewis, 1994.
        /// </summary>
        /// <remarks>   David, 2021-03-20. </remarks>
        /// <returns>   A Tuple. </returns>
        public static (int[] SampleSize, double[] Values) CriticalValues()
        {
            return _CriticalValues;
        }

        /// <summary>   Gets or sets the minimum size of the sample. </summary>
        /// <value> The minimum size of the sample. </value>
        public static int MinimumSampleSize { get; set; } = SampleSkewness.CriticalValues().SampleSize[0];

        /// <summary>   Critical value. </summary>
        /// <remarks>   David, 2021-03-20. </remarks>
        /// <param name="sampleSize">   Size of the sample. </param>
        /// <returns>   A double. </returns>
        public static double CriticalValue( int sampleSize )
        {
            if ( sampleSize < SampleSkewness.MinimumSampleSize )
                throw new InvalidOperationException( $"Element count is {sampleSize}. At least {SampleSkewness.MinimumSampleSize} values are required for establishing the critical value" );

            double value = 0;
            (int[] SampleSizes, double[] Values) = CriticalValues();
            if ( sampleSize < SampleSizes[0] )
            {
                return Values[0];
            }
            else if ( sampleSize > SampleSizes[^1] )
                return Values[^1];
            else
            {
                for ( int i = 0; i < SampleSizes.Length - 1; i++ )
                {
                    if ( sampleSize == SampleSizes[i] )
                    {
                        value = Values[i];
                        break;
                    }
                    else if ( sampleSize == SampleSizes[i + 1] )
                    {
                        value = Values[i + 1];
                        break;
                    }
                    else if ( sampleSize > SampleSizes[i] && sampleSize < SampleSizes[i + 1] )
                    {
                        value = Values[i] + (sampleSize - SampleSizes[i]) * (Values[i + 1] - Values[i]) / (SampleSizes[i + 1] - SampleSizes[i]);
                        break;
                    }
                }
            }
            return value;
        }

        /// <summary>
        /// Gets or sets the sample skewness. The computes the Fisher-Pearson coefficient of skewness.
        /// </summary>
        /// <value> The skewness. </value>
        public double Skewness { get; private set; }

        /// <summary>   Gets or sets the sigma. Note that in computing the skewness, the s is computed with N in the denominator rather than N - 1.
        ///  </summary>
        /// <value> The sigma. </value>
        public double Sigma { get; private set; }

        /// <summary> Gets the upper fence. </summary>
        /// <value> The upper fence. </value>
        public double UpperFence { get; set; }

        /// <summary> Gets the lower fence. </summary>
        /// <value> The lower fence. </value>
        public double LowerFence { get; set; }

        /// <summary>   Updates the fences if the sample has values. </summary>
        /// <remarks>   David, 2021-08-19. </remarks>
        public void UpdateFences()
        {
            if ( this.Sample.Count > 0 )
            {
                double criticalValue = SampleSkewness.CriticalValue( this.Sample.Count );
                this.LowerFence = -criticalValue;
                this.UpperFence = criticalValue;
            }
        }

        /// <summary> Gets the first quartile. </summary>
        /// <value> The first quartile. </value>
        public double FirstQuartile => this.QuartileValues()[0];

        /// <summary> Gets the Second quartile (median). </summary>
        /// <value> The Second quartile (median). </value>
        public double SecondQuartile => this.QuartileValues()[1];

        /// <summary> Gets the Third quartile. </summary>
        /// <value> The Third quartile. </value>
        public double ThirdQuartile => this.QuartileValues()[2];

        /// <summary> The values. </summary>
        private double[] _QuartileValues;

        /// <summary> Gets the quartiles. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> A Double() array with 1st, 2nd, and 3rd quartile values. </returns>
        public double[] QuartileValues()
        {
            return this._QuartileValues;
        }

        /// <summary>   Evaluate skewness. </summary>
        /// <remarks>   David, 2021-03-20. </remarks>
        /// <param name="values">   The values. </param>
        /// <returns>   A Tuple. </returns>
        public static (double skewness, double sigma) EvaluateSkewness( double[] values )
        {
            if ( values is not object )
                throw new ArgumentNullException( nameof( values ) );
            if ( values.Length < SampleSkewness.MinimumSampleSize )
                throw new InvalidOperationException( $"Element count is {values.Length}. At least {SampleSkewness.MinimumSampleSize} values are required for establishing the skewness" );
            double sum = 0;
            foreach ( double v in values )
            {
                sum += v;
            }
            return EvaluateSkewness( values, sum / values.Length );
        }

        /// <summary>   Evaluate skewness. </summary>
        /// <remarks>   David, 2021-03-20. </remarks>
        /// <param name="values">   The values. </param>
        /// <param name="mean">     The mean. </param>
        /// <returns>   A Tuple. </returns>
        public static (double skewness, double sigma) EvaluateSkewness( double[] values, double mean )
        {
            if ( values is not object )
                throw new ArgumentNullException( nameof( values ) );
            if ( values.Length < SampleSkewness.MinimumSampleSize )
                throw new InvalidOperationException( $"Element count is {values.Length}. At least {SampleSkewness.MinimumSampleSize} values are required for establishing the skewness" );
            double sumSquares = 0;
            double sumCubes = 0;
            foreach ( double v in values )
            {
                double diff = (v - mean);
                double temp = diff * diff;
                sumSquares += temp;
                sumCubes += diff * temp;
            }
            double sigma = Math.Sqrt( sumSquares / values.Length );
            double skewness = sumCubes / (values.Length * sigma * sigma * sigma);
            return (skewness, sigma);
        }

        /// <summary>
        /// Evaluates the <see cref="Skewness">sample skewness</see>.
        /// </summary>
        /// <remarks>   David, 2020-09-23. </remarks>
        public void Evaluate()
        {
            this.ClearSkewnessThis();
            (double skewness, double sigma) = EvaluateSkewness( this.Sample.ValuesArray(), this.Sample.Mean );
            this.Sigma = sigma;
            this.Skewness = skewness;
        }

        /// <summary>   Searches for the first outliers. </summary>
        /// <remarks>   David, 2021-05-24. </remarks>
        /// <param name="values">   The values. </param>
        /// <returns>   The found outliers. </returns>
        public static (int[] OutlierIndexes, double[] SortedValues) FindOutliers( double[] values )
        {
            double criticalValue = SampleSkewness.CriticalValue( values.Length );
            return SampleSkewness.FindOutliersSorted( values, -criticalValue, criticalValue );
        }

        /// <summary>   Sort and index. </summary>
        /// <remarks>   David, 2021-05-25. </remarks>
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="values">   The values. </param>
        /// <returns>   The sorted and index. </returns>
        public static int[] SortAndIndex<T>( T[] values )
        {
            int i, c = values.Length;
            var keys = new int[c];
            if ( c > 1 )
            {
                for ( i = 0; i < c; i++ )
                    keys[i] = i;

                System.Array.Sort( values, keys /*, ... */);
            }
            return keys;
        }

        /// <summary>   Searches for the first outliers. </summary>
        /// <remarks>   David, 2021-05-25. </remarks>
        /// <param name="values">       The values. </param>
        /// <param name="lowerFence">   The lower fence. </param>
        /// <param name="upperFence">   The upper fence. </param>
        /// <returns>   The indexes of the outliers. </returns>
        public static IEnumerable<int> FindOutliers( double[] values, double lowerFence, double upperFence )
        {
            List<int> outlierIndexes = new();
            double[] sortedFilteredValues = new double[values.Length];
            Array.Copy( values, sortedFilteredValues, values.Length );
            int[] indexes = SortAndIndex( sortedFilteredValues );
            bool done;
            int bottomIndex = 0;
            int topIndex = values.Length - 1;
            do
            {
                (double skewness, double _) = EvaluateSkewness( sortedFilteredValues );
                if ( skewness < lowerFence )
                {
                    outlierIndexes.Add( indexes[bottomIndex] );
                    bottomIndex += 1;
                    double[] temp = new double[sortedFilteredValues.Length - 1];
                    // skip the first value
                    Array.Copy( sortedFilteredValues, 1, temp, 0, sortedFilteredValues.Length - 1 );
                    sortedFilteredValues = temp;
                    done = sortedFilteredValues.Length < SampleSkewness.MinimumSampleSize;
                }
                else if ( skewness > upperFence )
                {
                    outlierIndexes.Add( indexes[topIndex] );
                    topIndex -= 1;
                    double[] temp = new double[sortedFilteredValues.Length - 1];
                    // skip the last value
                    Array.Copy( sortedFilteredValues, 0, temp, 0, sortedFilteredValues.Length - 1 );
                    sortedFilteredValues = temp;
                    done = sortedFilteredValues.Length < SampleSkewness.MinimumSampleSize;
                }
                else
                {
                    done = true;
                }
            } while ( !done );

            return outlierIndexes;
        }


        /// <summary>   Searches for the first outliers. </summary>
        /// <remarks>   David, 2021-03-20. </remarks>
        /// <param name="values">       The values. </param>
        /// <param name="lowerFence">   The lower fence. </param>
        /// <param name="upperFence">   The upper fence. </param>
        /// <returns>   A Tuple ( indexes of outliers in the sorted values, sorted values). </returns>
        public static (int[] OutlierIndexes, double[] SortedValues) FindOutliersSorted( double[] values, double lowerFence, double upperFence )
        {
            List<int> outlierIndexes = new();
            double[] sortedValues = new double[values.Length];
            Array.Copy( values, sortedValues, values.Length );
            Array.Sort( sortedValues );
            double[] filteredValues = sortedValues;
            bool done;
            int bottomIndex = 0;
            int topIndex = values.Length - 1;
            do
            {
                (double skewness, double _) = EvaluateSkewness( filteredValues );
                if ( skewness < lowerFence )
                {
                    outlierIndexes.Add( bottomIndex );
                    bottomIndex += 1;
                    double[] temp = new double[filteredValues.Length - 1];
                    // skip the first value
                    Array.Copy( filteredValues, 1, temp, 0, filteredValues.Length - 1 );
                    filteredValues = temp;
                    done = filteredValues.Length < SampleSkewness.MinimumSampleSize;
                }
                else if ( skewness > upperFence )
                {
                    outlierIndexes.Add( topIndex );
                    topIndex -= 1;
                    double[] temp = new double[filteredValues.Length - 1];
                    // skip the last value
                    Array.Copy( filteredValues, 0, temp, 0, filteredValues.Length - 1 );
                    filteredValues = temp;
                    done = filteredValues.Length < SampleSkewness.MinimumSampleSize;
                }
                else
                {
                    done = true;
                }
            } while ( !done );

            return (outlierIndexes.ToArray(), sortedValues);
        }

        private int[] _OutlierIndexes;
        /// <summary>   Gets the outlier indexes. </summary>
        /// <value> The outlier indexes. </value>
        public int[] OutlierIndexes()
        {
            return this._OutlierIndexes;
        }

        /// <summary>   Filter sample. </summary>
        /// <remarks>
        /// This function is set outside the evaluation to allow recalculation is the fence is updated.
        /// </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="updateFences"> (Optional) True to update fences. </param>
        public void FilterSample( bool updateFences = true )
        {
            if ( updateFences ) this.UpdateFences();
            if ( this.UpperFence - this.LowerFence <= Single.Epsilon )
                throw new InvalidOperationException( $"Upper fence {this.UpperFence} must be higher than the lower fence {this.LowerFence}" );
            if ( this.Sample.ValuesArray() is not object )
                throw new InvalidOperationException( $"Sample data not set for computing skewness" );
            if ( this.Sample.ValuesArray().Length < SampleSkewness.MinimumSampleSize )
                throw new InvalidOperationException( $"Element count is {this.Sample.ValuesArray().Length}. At least {SampleSkewness.MinimumSampleSize} values are required for establishing the skewness" );
            int[] outlierIndexes = FindOutliers( this.Sample.ValuesArray(), this.LowerFence, this.UpperFence ).ToArray();
            this.FilteredSample = new SampleStatistics();
            this.FilteredSample.AddValues( this.Sample.ValuesArray(), outlierIndexes );
            this._OutlierIndexes = outlierIndexes;
            this._QuartileValues = this.FilteredSample.Count > SampleSkewness.MinimumSampleSize
                ? Quartiles.CalculateQuartiles( this.FilteredSample.ValuesArray(), SampleSkewness.MinimumSampleSize )
                : (new[] { 0d, 0d, 0d });
        }

        #endregion

    }
}
