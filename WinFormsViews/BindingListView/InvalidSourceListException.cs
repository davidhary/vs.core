using System;

namespace isr.Core.WinFormsViews
{
    /// <summary> Exception for signaling invalid source list errors. </summary>
    /// <remarks>
    /// (c) 2006 Andrew Davey. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-15, 1.2 </para><para>
    /// https://blogs.warwick.ac.uk/andrewdavey and  </para><para>
    /// https://sourceforge.net/projects/blw/. </para>
    /// </remarks>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Usage", "CA2237:Mark ISerializable types with serializable",
        Justification = "[Serializable] relates essentially just to BinaryFormatter, which usually isn't a good choice." )]
    public class InvalidSourceListException : Exception
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        public InvalidSourceListException() : base( My.Resources.Resources.InvalidSourceList )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="message"> The message. </param>
        public InvalidSourceListException( string message ) : base( message )
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Exception" /> class with a specified
        /// error message and a reference to the inner exception that is the cause of this exception.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="message">        The error message that explains the reason for the exception. </param>
        /// <param name="innerException"> The exception that is the cause of the current exception, or a
        /// null reference (<see langword="Nothing" /> in Visual Basic)
        /// if no inner exception is specified. </param>
        public InvalidSourceListException( string message, Exception innerException ) : base( message, innerException )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="info">    The information. </param>
        /// <param name="context"> The context. </param>
        protected InvalidSourceListException( System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context ) : base( info, context )
        {
        }
    }
}
