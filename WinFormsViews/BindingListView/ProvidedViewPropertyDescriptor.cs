using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace isr.Core.WinFormsViews
{

    /// <summary> Provided view property descriptor. </summary>
    /// <remarks>
    /// (c) 2006 Andrew Davey. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-15, 1.2 </para><para>
    /// https://blogs.warwick.ac.uk/andrewdavey and </para><para>
    /// https://sourceforge.net/projects/blw/.
    /// </para>
    /// </remarks>
    internal class ProvidedViewPropertyDescriptor<T> : PropertyDescriptor
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="name">         The name. </param>
        /// <param name="propertyType"> Type of the property. </param>
        public ProvidedViewPropertyDescriptor( string name, Type propertyType ) : base( name, null )
        {
            this._PropertyType = propertyType;
        }

        /// <summary> Type of the property. </summary>
        private readonly Type _PropertyType;

        /// <summary>
        /// When overridden in a derived class, returns whether resetting an object changes its value.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>

        /// <param name="component"> The component to test for reset capability. </param>
        /// <returns> true if resetting the component changes its value; otherwise, false. </returns>
        public override bool CanResetValue( object component )
        {
            return false;
        }

        /// <summary>
        /// When overridden in a derived class, gets the type of the component this property is bound to.
        /// </summary>
        /// <value>
        /// A <see cref="T:System.Type" /> that represents the type of component this property is bound
        /// to. When the
        /// <see cref="M:System.ComponentModel.PropertyDescriptor.GetValue(System.Object)" /> or
        /// <see cref="M:System.ComponentModel.PropertyDescriptor.SetValue(System.Object,System.Object)" />
        /// methods are invoked, the object specified might be an instance of this type.
        /// </value>
        public override Type ComponentType => typeof( ObjectView<T> );

        /// <summary>
        /// When overridden in a derived class, gets the current value of the property on a component.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="component"> The component with the property for which to retrieve the value. </param>
        /// <returns> The value of a property for a given component. </returns>
        public override object GetValue( object component )
        {
            return component is null
                ? throw new ArgumentNullException( nameof( component ) )
                : this.ComponentType.IsAssignableFrom( component.GetType() )
                ? (component as ObjectView<T>).GetProvidedView( this.Name )
                : throw new ArgumentException( $"Type of {nameof( component )} is not valid.", nameof( component ) );
        }

        /// <summary>
        /// When overridden in a derived class, gets a value indicating whether this property is read-
        /// only.
        /// </summary>
        /// <value> true if the property is read-only; otherwise, false. </value>
        public override bool IsReadOnly => true;

        /// <summary> When overridden in a derived class, gets the type of the property. </summary>
        /// <value> A <see cref="T:System.Type" /> that represents the type of the property. </value>
        public override Type PropertyType => this._PropertyType;

        /// <summary>
        /// When overridden in a derived class, resets the value for this property of the component to
        /// the default value.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="NotSupportedException"> Thrown when the requested operation is not supported. </exception>
        /// <param name="component"> The component with the property value that is to be reset to the
        /// default value. </param>
        public override void ResetValue( object component )
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// When overridden in a derived class, sets the value of the component to a different value.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="NotSupportedException"> Thrown when the requested operation is not supported. </exception>
        /// <param name="component"> The component with the property value that is to be set. </param>
        /// <param name="value">     The new value. </param>
        public override void SetValue( object component, object value )
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// When overridden in a derived class, determines a value indicating whether the value of this
        /// property needs to be persisted.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="component"> The component with the property to be examined for persistence. </param>
        /// <returns> true if the property should be persisted; otherwise, false. </returns>
        public override bool ShouldSerializeValue( object component )
        {
            return false;
        }

        /// <summary> Can provide view of the given property. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="prop"> The property. </param>
        /// <returns> <c>true</c> if we can provide view of; otherwise <c>false</c> </returns>
        public static bool CanProvideViewOf( PropertyDescriptor prop )
        {
            var listTypeDef = typeof( IList<object> ).GetGenericTypeDefinition();
            var propType = prop.PropertyType;
            var args = propType.GetGenericArguments();
            // Is this a generic type, with only one generic parameter.
            if ( args.Length == 1 )
            {
                // Create type IList<T> where T is args[0]
                var listType = listTypeDef.MakeGenericType( args );
                // Check if the property type implements IList<T>
                // but is not an IBindingListView (or better).
                if ( listType.IsAssignableFrom( propType ) && !typeof( IBindingListView ).IsAssignableFrom( propType ) )
                {
                    return true;
                }
            }

            return false;
        }
    }
}
