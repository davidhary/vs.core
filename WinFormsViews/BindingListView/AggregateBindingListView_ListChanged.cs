using System.ComponentModel;

namespace isr.Core.WinFormsViews
{
    public partial class AggregateBindingListView<T>
    {

        #region " LIST CHANGED EVENT HANDLER "

        /// <summary> Removes the List Changed event handlers. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        protected void RemoveListChangedEventHandler()
        {
            this._ListChangedHandlers.RemoveAll();
        }

        /// <summary> The list changed handlers. </summary>
        private readonly NotifyListChangedEventContextCollection _ListChangedHandlers = new NotifyListChangedEventContextCollection();

        /// <summary> Event queue for all listeners interested in List Changed events. </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>

        public event ListChangedEventHandler ListChanged
        {
            add {
                this._ListChangedHandlers.Add( new NotifyListChangedEventContext( value ) );
            }

            remove {
                this._ListChangedHandlers.RemoveValue( value );
            }
        }

        private void OnListChanged( object sender, ListChangedEventArgs e )
        {
            this._ListChangedHandlers.Raise( sender, e );
        }

        #endregion

        #region " NOTIFY "

        /// <summary>
        /// Asynchronously notifies (posts) change on a different thread. Unsafe for cross threading;
        /// fast return of control to the invoking function.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="e"> List Changed event information. </param>
        protected virtual void NotifyListChanged( ListChangedEventArgs e )
        {
            this._ListChangedHandlers.Post( this, e );
        }

        /// <summary>
        /// Asynchronously notifies (posts) change on a different thread. Unsafe for cross threading;
        /// fast return of control to the invoking function.
        /// </summary>
        /// <remarks>
        /// Overhead of this method is 7 to 10 times larger than naked raise event. This has no advantage
        /// even with slow handler functions.
        /// </remarks>
        /// <param name="e"> List Changed event information. </param>
        protected virtual void AsyncNotifyListChanged( ListChangedEventArgs e )
        {
            this._ListChangedHandlers.Post( this, e );
        }

        /// <summary>
        /// Synchronously notifies (send) list change on the caller thread. Safe for cross threading.
        /// </summary>
        /// <remarks>
        /// Overhead of this method is 3 to 4 times larger than naked raise event. This is the best
        /// approach.
        /// </remarks>
        /// <param name="e"> List Changed event information. </param>
        protected virtual void SyncNotifyListChanged( ListChangedEventArgs e )
        {
            this._ListChangedHandlers.Send( this, e );
        }

        #endregion

        #region " LIST CHANGE NOTIFICATIONS "

        /// <summary>
        /// Helper method to build the ListChangedEventArgs needed for the ListChanged event.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="listChangedType"> The type of change that occurred. </param>
        /// <param name="newIndex">        The index of the changed item. </param>
        private void OnListChanged( ListChangedType listChangedType, int newIndex )
        {
            this.NotifyListChanged( new ListChangedEventArgs( listChangedType, newIndex ) );
        }

        /// <summary>
        /// Helper method to build the ListChangedEventArgs needed for the ListChanged event.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="listChangedType"> The type of change that  occurred. </param>
        /// <param name="newIndex">        The index of the item after the change. </param>
        /// <param name="oldIndex">        The index of the item before the change. </param>
        private void OnListChanged( ListChangedType listChangedType, int newIndex, int oldIndex )
        {
            this.NotifyListChanged( new ListChangedEventArgs( listChangedType, newIndex, oldIndex ) );
        }

        /// <summary> Raises the ListChanged event with the given event arguments. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="e"> The ListChangedEventArgs to raise the event with. </param>
        protected virtual void OnListChanged( ListChangedEventArgs e )
        {
            this.NotifyListChanged( e );
        }

        #endregion

    }
}
