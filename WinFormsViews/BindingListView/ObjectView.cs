using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.WinFormsViews
{

    /// <summary>
    /// Serves a wrapper for items being viewed in a <see cref="BindingListView{T}"/>. This class
    /// implements <see cref="INotifyingEditableObject"/> so will raise the necessary events during
    /// the item edit life-cycle.
    /// </summary>
    /// <remarks>
    /// If <typeparamref name="T"/> implements <see cref="System.ComponentModel.IEditableObject"/>
    /// this class will call BeginEdit/CancelEdit/EndEdit on the <typeparamref name="T"/> object as
    /// well. If <typeparamref name="T"/> implements
    /// <see cref="System.ComponentModel.IDataErrorInfo"/> this class will use that implementation as
    /// its own. <para>
    /// (c) 2006 Andrew Davey. All rights reserved.</para><para>&gt;
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-15. Source: </para><para>
    /// https://blogs.warwick.ac.uk/andrewdavey and </para><para>
    /// https://sourceforge.net/projects/blw/. </para>
    /// </remarks>
    public class ObjectView<T> : INotifyingEditableObject, IDataErrorInfo, INotifyPropertyChanged, ICustomTypeDescriptor
    {

        /// <summary>
        /// Creates a new <see cref="ObjectView{T}"/> wrapper for a <typeparamref name="T"/> object.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="object"> Gets the object being edited. </param>
        /// <param name="parent">   The view containing this ObjectView. </param>
        public ObjectView( T @object, AggregateBindingListView<T> parent )
        {
            this._Parent = parent;
            this.Object = @object;
            if ( Conversions.ToBoolean( @object is INotifyPropertyChanged ) )
            {
                (( INotifyPropertyChanged ) @object).PropertyChanged += this.ObjectPropertyChanged;
            }

            if ( typeof( ICustomTypeDescriptor ).IsAssignableFrom( typeof( T ) ) )
            {
                this._IsCustomTypeDescriptor = true;
                this._CustomTypeDescriptor = @object as ICustomTypeDescriptor;
                Debug.Assert( this._CustomTypeDescriptor is object );
            }

            this.ProvidedViews = new Dictionary<string, object>();
            this.CreateProvidedViews();
        }

        /// <summary>
        /// The view containing this ObjectView.
        /// </summary>
        [NonSerialized]
        private readonly AggregateBindingListView<T> _Parent;

        /// <summary>
        /// Flag that signals if we are currently editing the object.
        /// </summary>
        private bool _Editing;

        /// <summary>
        /// Flag set to true if type of T implements ICustomTypeDescriptor
        /// </summary>
        private readonly bool _IsCustomTypeDescriptor;

        /// <summary>
        /// Holds the Object pre-casted ICustomTypeDescriptor (if supported).
        /// </summary>
        private readonly ICustomTypeDescriptor _CustomTypeDescriptor;

        /// <summary>
        /// The actual object being edited.
        /// </summary>
        private T _Object;

        /// <summary>
        /// Gets the object being edited.
        /// </summary>
        public T Object
        {
            get => this._Object;

            private set {
                if ( value is null )
                {
                    throw new ArgumentNullException( nameof( value ), My.Resources.Resources.ObjectCannotBeNull );
                }

                this._Object = value;
            }
        }

        /// <summary>
        /// A collection of BindingListView objects, indexed by name, for views auto-provided for any
        /// generic IList members.
        /// </summary>
        /// <value> The provided views. </value>
        private IDictionary<string, object> ProvidedViews { get; set; }

        /// <summary> Gets provided view. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="name"> The name. </param>
        /// <returns> The provided view. </returns>
        public object GetProvidedView( string name )
        {
            return this.ProvidedViews[name];
        }

        /// <summary> Casts an ObjectView{T} to a T by getting the wrapped T object. </summary>
        /// <param name="value"> The ObjectView{T} to cast to a T. </param>
        /// <returns> The object that is wrapped. </returns>
        public static explicit operator T( ObjectView<T> value )
        {
            return value is null ? ( T ) new object() : value.Object;
        }

        /// <summary> Determines whether the specified object is equal to the current object. </summary>
        /// <param name="obj"> The object to compare with the current object. </param>
        /// <returns>
        /// true if the specified object  is equal to the current object; otherwise, false.
        /// </returns>
        public override bool Equals( object obj )
        {
            return obj is null
                ? false
                : obj is T
                    ? this.Object.Equals( obj )
                    : obj is ObjectView<T> ? this.Object.Equals( (obj as ObjectView<T>).Object ) : base.Equals( obj );
        }

        /// <summary> Serves as the default hash function. </summary>
        /// <returns> A hash code for the current object. </returns>
        public override int GetHashCode()
        {
            return this.Object.GetHashCode();
        }

        /// <summary> Returns a string that represents the current object. </summary>
        /// <returns> A string that represents the current object. </returns>
        public override string ToString()
        {
            return this.Object.ToString();
        }

        /// <summary> Object property changed. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property changed event information. </param>
        private void ObjectPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            // Raise our own event
            this.OnPropertyChanged( sender, new PropertyChangedEventArgs( e.PropertyName ) );
        }

        /// <summary> Should provide view of the given list property. </summary>
        /// <param name="listProp"> The list property. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        private bool ShouldProvideViewOf( PropertyDescriptor listProp )
        {
            return this._Parent.ShouldProvideView( listProp );
        }

        /// <summary> Gets provided view name. </summary>
        /// <param name="listProp"> The list property. </param>
        /// <returns> The provided view name. </returns>
        private string GetProvidedViewName( PropertyDescriptor listProp )
        {
            return this._Parent.GetProvidedViewName( listProp );
        }

        /// <summary> Creates provided views. </summary>
        private void CreateProvidedViews()
        {
            foreach ( PropertyDescriptor prop in (this as ICustomTypeDescriptor).GetProperties() )
            {
                if ( this.ShouldProvideViewOf( prop ) )
                {
                    var view = this._Parent.CreateProvidedView( this, prop );
                    string viewName = this.GetProvidedViewName( prop );
                    this.ProvidedViews.Add( viewName, view );
                }
            }
        }

        #region " INotifyEditableObject Members "

        /// <summary>
        /// Indicates an edit has just begun.
        /// </summary>
        public event EventHandler EditBegun;

        /// <summary>
        /// Indicates the edit was canceled.
        /// </summary>
        public event EventHandler EditCanceled;

        /// <summary>
        /// Indicated the edit was ended.
        /// </summary>
        public event EventHandler EditEnded;

        /// <summary> Executes the edit begun action. </summary>
        protected virtual void OnEditBegun()
        {
            EditBegun?.Invoke( this, EventArgs.Empty );
        }

        /// <summary> Executes the edit canceled action. </summary>
        protected virtual void OnEditCanceled()
        {
            EditCanceled?.Invoke( this, EventArgs.Empty );
        }

        /// <summary> Executes the edit ended action. </summary>
        protected virtual void OnEditEnded()
        {
            EditEnded?.Invoke( this, EventArgs.Empty );
        }

        #endregion

        #region " IEditableObject Members "

        /// <summary> Begins an edit. </summary>
        public void BeginEdit()
        {
            // As per documentation, this method may get called multiple times for a single edit.
            // So we set a flag to only honor the first call.
            if ( !this._Editing )
            {
                this._Editing = true;
                // If possible call the object's BeginEdit() method
                // to let it do what ever it needs e.g. save state
                if ( Conversions.ToBoolean( this.Object is IEditableObject ) )
                {
                    (( IEditableObject ) this.Object).BeginEdit();
                }
                // Raise the EditBegun event.                
                this.OnEditBegun();
            }
        }

        /// <summary> Cancel edit. </summary>
        public void CancelEdit()
        {
            // We can only cancel if currently editing
            if ( this._Editing )
            {
                // If possible call the object's CancelEdit() method
                // to let it do what ever it needs e.g. rollback state
                if ( Conversions.ToBoolean( this.Object is IEditableObject ) )
                {
                    (( IEditableObject ) this.Object).CancelEdit();
                }
                // Raise the EditCancelled event.
                this.OnEditCanceled();
                // No longer editing now.
                this._Editing = false;
            }
        }

        /// <summary> Ends an edit. </summary>
        public void EndEdit()
        {
            // We can only end if currently editing. 
            if ( this._Editing )
            {
                // If possible call the object's EndEdit() method
                // to let it do what ever it needs e.g. commit state
                if ( Conversions.ToBoolean( this.Object is IEditableObject ) )
                {
                    (( IEditableObject ) this.Object).EndEdit();
                }
                // Raise the EditEnded event.
                this.OnEditEnded();
                // No longer editing now.
                this._Editing = false;
            }
        }

        #endregion

        #region " IDataErrorInfo Members "

        /// <summary> Gets the data error information error. </summary>
        /// <remarks>
        /// If the wrapped Object support IDataErrorInfo we forward calls to it. Otherwise, we just
        /// return empty strings that signal "no error".
        /// </remarks>
        /// <value> The i data error information error. </value>
        string IDataErrorInfo.Error => Conversions.ToBoolean( this.Object is IDataErrorInfo ) ? (( IDataErrorInfo ) this.Object).Error : string.Empty;

        /// <summary> Gets the data error information item. </summary>
        /// <value> The i data error information item. </value>
        public string this[string columnName] => Conversions.ToBoolean( this.Object is IDataErrorInfo ) ? (( IDataErrorInfo ) this.Object)[columnName] : string.Empty;

        #endregion

        #region " INotifyPropertyChanged Members "

        /// <summary> Event queue for all listeners interested in PropertyChanged events. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary> Raises the property changed event. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="args">   Event information to send to registered event handlers. </param>
        protected virtual void OnPropertyChanged( object sender, PropertyChangedEventArgs args )
        {
            PropertyChanged?.Invoke( sender, args );
        }

        #endregion

        #region " ICustomTypeDescriptor Members "

        AttributeCollection ICustomTypeDescriptor.GetAttributes()
        {
            return this._IsCustomTypeDescriptor ? this._CustomTypeDescriptor.GetAttributes() : TypeDescriptor.GetAttributes( this.Object );
        }

        string ICustomTypeDescriptor.GetClassName()
        {
            return this._IsCustomTypeDescriptor ? this._CustomTypeDescriptor.GetClassName() : typeof( T ).FullName;
        }

        string ICustomTypeDescriptor.GetComponentName()
        {
            return this._IsCustomTypeDescriptor ? this._CustomTypeDescriptor.GetComponentName() : TypeDescriptor.GetFullComponentName( this.Object );
        }

        TypeConverter ICustomTypeDescriptor.GetConverter()
        {
            return this._IsCustomTypeDescriptor ? this._CustomTypeDescriptor.GetConverter() : TypeDescriptor.GetConverter( this.Object );
        }

        EventDescriptor ICustomTypeDescriptor.GetDefaultEvent()
        {
            return this._IsCustomTypeDescriptor ? this._CustomTypeDescriptor.GetDefaultEvent() : TypeDescriptor.GetDefaultEvent( this.Object );
        }

        PropertyDescriptor ICustomTypeDescriptor.GetDefaultProperty()
        {
            return this._IsCustomTypeDescriptor ? this._CustomTypeDescriptor.GetDefaultProperty() : TypeDescriptor.GetDefaultProperty( this.Object );
        }

        object ICustomTypeDescriptor.GetEditor( Type editorBaseType )
        {
            return this._IsCustomTypeDescriptor ? this._CustomTypeDescriptor.GetEditor( editorBaseType ) : null;
        }

        EventDescriptorCollection ICustomTypeDescriptor.GetEvents( Attribute[] attributes )
        {
            return this._IsCustomTypeDescriptor ? this._CustomTypeDescriptor.GetEvents() : TypeDescriptor.GetEvents( this.Object, attributes );
        }

        EventDescriptorCollection ICustomTypeDescriptor.GetEvents()
        {
            return this._IsCustomTypeDescriptor ? this._CustomTypeDescriptor.GetEvents() : TypeDescriptor.GetEvents( this.Object );
        }

        PropertyDescriptorCollection ICustomTypeDescriptor.GetProperties( Attribute[] attributes )
        {
            return this._IsCustomTypeDescriptor ? this._CustomTypeDescriptor.GetProperties() : TypeDescriptor.GetProperties( this.Object, attributes );
        }

        /// <summary> Custom type descriptor get properties. </summary>
        /// <returns> A PropertyDescriptorCollection. </returns>
        PropertyDescriptorCollection ICustomTypeDescriptor.GetProperties()
        {
            return this._IsCustomTypeDescriptor ? this._CustomTypeDescriptor.GetProperties() : TypeDescriptor.GetProperties( this.Object );
        }

        /// <summary> Custom type descriptor get property owner. </summary>
        /// <param name="pd"> The pd. </param>
        /// <returns> An Object. </returns>
        object ICustomTypeDescriptor.GetPropertyOwner( PropertyDescriptor pd )
        {
            return this._IsCustomTypeDescriptor ? this._CustomTypeDescriptor.GetPropertyOwner( pd ) : this.Object;
        }

        #endregion

    }
}
