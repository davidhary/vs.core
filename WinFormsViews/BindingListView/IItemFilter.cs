using System;

namespace isr.Core.WinFormsViews
{
    /// <summary>
    /// Defines a general method to test it an item should be included in a
    /// <see cref="BindingListView{T}"/>.
    /// </summary>
    /// <remarks>
    /// <typeparam name="T"> The type of item to be filtered. </typeparam> <para>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public interface IItemFilter<T>
    {

        /// <summary> Tests if the item should be included. </summary>
        /// <param name="item"> The item to test. </param>
        /// <returns> True if the item should be included, otherwise false. </returns>
        bool Include( T item );
    }

    /// <summary>
    /// A dummy filter that is used when no filter is needed. It simply includes any and all items
    /// tested.
    /// </summary>
    /// <remarks>
    /// (c) 2006 Andrew Davey. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-15, </para><para>
    /// David, 2018-12-15, 1.2.*. https://blogs.warwick.ac.uk/andrewdavey and
    /// https://sourceforge.net/projects/blw/. </para>
    /// </remarks>
    public class IncludeAllItemFilter<T> : IItemFilter<T>
    {

        /// <summary> Tests if the item should be included. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="item"> The item to test. </param>
        /// <returns> True if the item should be included, otherwise false. </returns>
        public bool Include( T item )
        {
            // All items are to be included.
            // So always return true.
            return true;
        }

        /// <summary> Returns a string that represents the current object. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns> A string that represents the current object. </returns>
        public override string ToString()
        {
            return My.Resources.Resources.NoFilter;
        }

        #region " Singleton Accessor "

        /// <summary> The instance. </summary>
        private static IncludeAllItemFilter<T> _Instance;

        /// <summary> Gets the singleton instance of <see cref="IncludeAllItemFilter{T}"/>. </summary>
        /// <value> The instance. </value>
        public static IncludeAllItemFilter<T> Instance
        {
            get {
                if ( _Instance is null )
                {
                    _Instance = new IncludeAllItemFilter<T>();
                }

                return _Instance;
            }
        }

        #endregion

    }

    /// <summary>
    /// A filter that uses a user-defined <see cref="Predicate{T}"/> to test items for inclusion
    /// in <see cref="BindingListView{T}"/>.
    /// </summary>
    /// <remarks>
    /// David, 2018-12-15, 1.2.*. https://blogs.warwick.ac.uk/andrewdavey and <para>
    /// https://sourceforge.net/projects/blw/. </para><para>
    /// (c) 2006 Andrew Davey. All rights reserved.</para><para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public class PredicateItemFilter<T> : IItemFilter<T>
    {

        /// <summary>
        /// Creates a new <see cref="PredicateItemFilter{T}"/> that uses the specified
        /// <see cref="Predicate{T}"/> and default name.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="includeDelegate"> The <see cref="Predicate{T}"/> used to test items. </param>
        public PredicateItemFilter( Predicate<T> includeDelegate ) : this( includeDelegate, null )
        {
            // The other constructor is called to do the work.
        }

        /// <summary>
        /// Creates a new <see cref="PredicateItemFilter{T}"/> that uses the specified
        /// <see cref="Predicate{T}"/>.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="includeDelegate"> The <see cref="PredicateItemFilter{T}"/> used to test
        /// items. </param>
        /// <param name="name">            The name used for the ToString() return value. </param>
        public PredicateItemFilter( Predicate<T> includeDelegate, string name )
        {
            // We don't allow a null string. Use the default instead.
            this._Name = name ?? this._DefaultName;
            this._IncludeDelegate = includeDelegate is object
                ? includeDelegate
                : throw new ArgumentNullException( nameof( includeDelegate ), My.Resources.Resources.IncludeDelegateCannotBeNull );
        }

        /// <summary> The include delegate. </summary>
        private readonly Predicate<T> _IncludeDelegate;

        /// <summary> The name. </summary>
        private readonly string _Name;

        /// <summary> The default name. </summary>
        private readonly string _DefaultName = My.Resources.Resources.PredicateFilter;

        /// <summary> Tests if the item should be included. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="item"> The item to test. </param>
        /// <returns> True if the item should be included, otherwise false. </returns>
        public bool Include( T item )
        {
            return this._IncludeDelegate( item );
        }

        /// <summary> Returns a string that represents the current object. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns> A string that represents the current object. </returns>
        public override string ToString()
        {
            return this._Name;
        }
    }
}
