
namespace isr.Core.WinFormsViews.My
{

    /// <summary> Provides assembly information for the class library. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    public sealed partial class MyLibrary
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        private MyLibrary() : base()
        {
        }

        /// <summary> Gets the identifier of the trace source. </summary>
        public const int TraceEventId = ( int ) ProjectTraceEventId.WinFormsViews;

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "Core Windows Forms Views Library";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "Core Windows Forms Views Library";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "Core.Windows.Forms.Views";

        /// <summary> Name of the test assembly strong. </summary>
        public const string TestAssemblyStrongName = "isr.Core.WinFormViewsTests,PublicKey=" + isr.Core.My.SolutionInfo.PublicKey;

    }
}
