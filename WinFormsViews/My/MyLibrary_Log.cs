using System;
using System.Diagnostics;
using isr.Core.WinFormsViews.ExceptionExtensions;

namespace isr.Core.WinFormsViews.My
{
    public sealed partial class MyLibrary
    {

        /// <summary> Logs unpublished exception. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="activity">  The activity. </param>
        /// <param name="exception"> The exception. </param>
        public static void LogUnpublishedException(string activity, Exception exception)
        {
            _ = LogUnpublishedMessage( new TraceMessage( TraceEventType.Error, TraceEventId, $"Exception {activity};. {exception.ToFullBlownString()}" ) );
        }

        /// <summary> Assigns a <see cref="Core.Logger"/> to this library. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="value"> The value. </param>
        public static void Apply(Logger value)
        {
            _Logger = value;
        }

        /// <summary> Applies the trace level described by value. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="value"> The value. </param>
        public static void ApplyTraceLogLevel(TraceEventType value)
        {
            TraceLevel = value;
            Logger.ApplyTraceLevel(value);
        }

        /// <summary> Applies the trace level described by value. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        public static void ApplyTraceLogLevel()
        {
            ApplyTraceLogLevel(MyLibrary.TraceLevel);
        }
    }
}
