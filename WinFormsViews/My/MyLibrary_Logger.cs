using System;
using System.Diagnostics;

namespace isr.Core.WinFormsViews.My
{
    public sealed partial class MyLibrary
    {

        #region " LOG "

        private static Logger _Logger;

        /// <summary> Gets the Logger. </summary>
        /// <value> The Logger. </value>
        public static Logger Logger
        {
            get {
                if ( _Logger is null )
                {
                    CreateLogger();
                }

                return _Logger;
            }
        }

        /// <summary>   Gets the application Logger. </summary>
        /// <value> The application Logger. </value>
        public static Microsoft.VisualBasic.Logging.Log ApplicationLog
        {
            get {
                if ( _Logger is null )
                {
                    CreateLogger();
                }

                return MyProject.Application.Log;
            }
        }


        /// <summary> Creates the logger. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        private static void CreateLogger()
        {
            _Logger = null;
            try
            {
                _Logger = Logger.Create(MyProject.Application.Info.ProductName);
                Microsoft.VisualBasic.Logging.FileLogTraceListener listener;
                listener = Logger.ReplaceDefaultTraceListener(true);
                if (!Logger.LogFileExists)
                {
                    _ = Logger.TraceEventOverride( ApplicationInfoTraceMessage() );
                }

                // set the log for the application
                if (!string.Equals(MyProject.Application.Log.DefaultFileLogWriter.FullLogFileName, listener.FullLogFileName, StringComparison.OrdinalIgnoreCase))
                {
                    MyProject.Application.Log.TraceSource.Listeners.Remove(CustomFileLogTraceListener.DefaultFileLogWriterName);
                    _ = MyProject.Application.Log.TraceSource.Listeners.Add( listener );
                    MyProject.Application.Log.TraceSource.Switch.Level = SourceLevels.Verbose;
                }

                // set the trace level.
                ApplyTraceLogLevel();
            }
            catch
            {
                if (_Logger is object)
                {
                    _Logger.Dispose();
                }

                _Logger = null;
                throw;
            }
        }

        /// <summary> Gets the trace level. </summary>
        /// <value> The trace level. </value>
        public static TraceEventType TraceLevel { get; set; } = TraceEventType.Warning;

        #endregion

        #region " IDENTITY "

        /// <summary> Identifies this library. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="talker"> The talker. </param>
        public static void Identify(ITraceMessageTalker talker)
        {
            talker?.IdentifyTalker(IdentityTraceMessage());
        }

        /// <summary> Gets the identity. </summary>
        /// <value> The identity. </value>
        public static string Identity => $"{AssemblyProduct} ID = {TraceEventId:X}";

        /// <summary> Library log trace message. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns> A TraceMessage. </returns>
        public static TraceMessage LogTraceMessage()
        {
            return new TraceMessage(TraceEventType.Information, TraceEventId, $"Log at;. {Logger.FullLogFileName}");
        }

        /// <summary> Gets a message describing the identity trace. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns> A TraceMessage. </returns>
        public static TraceMessage IdentityTraceMessage()
        {
            return IdentityTraceMessage(TraceEventType.Information);
        }

        /// <summary> Gets a message describing the identity trace. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <returns> A TraceMessage. </returns>
        public static TraceMessage IdentityTraceMessage(TraceEventType eventType)
        {
            return new TraceMessage(eventType, TraceEventId, Identity);
        }

        /// <summary> Application information trace message. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns> A TraceMessage. </returns>
        public static TraceMessage ApplicationInfoTraceMessage()
        {
            return new TraceMessage(TraceEventType.Information, TraceEventId, AssemblyExtensions.Methods.BuildProductTimeCaption(MyProject.Application.Info));
        }

        #endregion

        #region " UNPUBLISHED MESSAGES "

        /// <summary> Gets or sets the unpublished identify date. </summary>
        /// <value> The unpublished identify date. </value>
        public static DateTimeOffset UnpublishedIdentifyDate { get; set; }

        /// <summary> Gets or sets the unpublished trace messages. </summary>
        /// <value> The unpublished trace messages. </value>
        public static TraceMessagesQueue UnpublishedTraceMessages { get; private set; } = new TraceMessagesQueue();

        /// <summary> Logs unpublished message. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="message">   The message. </param>
        /// <returns> A String. </returns>
        public static string LogUnpublishedMessage(TraceEventType eventType, string message)
        {
            return LogUnpublishedMessage(eventType, TraceEventId, message);
        }

        /// <summary> Logs unpublished message. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="format">    Describes the format to use. </param>
        /// <param name="args">      A variable-length parameters list containing arguments. </param>
        /// <returns> A String. </returns>
        public static string LogUnpublishedMessage(TraceEventType eventType, string format, params object[] args)
        {
            return LogUnpublishedMessage(eventType, TraceEventId, format, args);
        }

        /// <summary> Logs unpublished message. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="id">        The identifier. </param>
        /// <param name="format">    Describes the format to use. </param>
        /// <param name="args">      A variable-length parameters list containing arguments. </param>
        /// <returns> A String. </returns>
        public static string LogUnpublishedMessage(TraceEventType eventType, int id, string format, params object[] args)
        {
            return LogUnpublishedMessage(new TraceMessage(eventType, id, format, args));
        }

        /// <summary> Logs unpublished message. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="message"> The message. </param>
        /// <returns> A String. </returns>
        public static string LogUnpublishedMessage(TraceMessage message)
        {
            if (message is null)
            {
                return string.Empty;
            }
            else
            {
                if (DateTimeOffset.Now.Date > UnpublishedIdentifyDate)
                {
                    _ = Logger.TraceEventOverride( ApplicationInfoTraceMessage() );
                    _ = Logger.TraceEventOverride( LogTraceMessage() );
                    _ = Logger.TraceEventOverride( IdentityTraceMessage() );
                    My.MyLibrary.UnpublishedIdentifyDate = DateTimeOffset.Now.Date;
                }

                My.MyLibrary.UnpublishedTraceMessages.Enqueue(message);
                _ = Logger.TraceEvent( message );
                return message.Details;
            }
        }

        #endregion

    }
}
