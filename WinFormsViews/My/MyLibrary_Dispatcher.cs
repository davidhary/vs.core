﻿using System;
using System.Windows.Threading;

namespace isr.Core.WinFormsViews.My
{
    public sealed partial class MyLibrary
    {

        /// <summary> Lets Windows process all the messages currently in the message queue. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        public static void DoEvents()
        {
            DispatcherExtensions.Methods.DoEvents(Dispatcher.CurrentDispatcher);
        }

        /// <summary>
        /// Delays operations by the given delay time selecting the delay clock which resolution exceeds
        /// 0.2 times the delay time. T.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="delayMilliseconds"> The delay in milliseconds. </param>
        public static void Delay(double delayMilliseconds)
        {
            Delay(TimeSpanExtensions.Methods.FromMilliseconds(delayMilliseconds));
        }

        /// <summary>
        /// Delays operations by the given delay time selecting the delay clock which resolution exceeds
        /// 0.2 times the delay time. sions.DoEvents(Dispatcher)"/> to release messages currently in the
        /// message queue.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="delayTime"> The delay time. </param>
        public static void Delay(TimeSpan delayTime)
        {
            TimeSpanExtensions.Methods.StartDelayTask(delayTime).Wait();
        }

        /// <summary>
        /// Delays operations by the given delay time selecting the delay clock which resolution exceeds
        /// <paramref name="resolution"/> times the delay time.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="delayTime">  The delay time. </param>
        /// <param name="resolution"> The resolution. </param>
        public static void Delay(TimeSpan delayTime, double resolution)
        {
            TimeSpanExtensions.Methods.StartDelayTask(delayTime, resolution).Wait();
        }

        /// <summary>
        /// Executes the specified delegate on the <see cref="DispatcherPriority.Render"/> priority.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="act"> The act. </param>
        public static void Render(Action act)
        {
            DispatcherExtensions.Methods.Render(Dispatcher.CurrentDispatcher, act);
        }
    }
}