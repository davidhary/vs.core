﻿using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.Core.WinFormsViews.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.Core.WinFormsViews.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.Core.WinFormsViews.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
