Imports System.Collections.Generic
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Text
Imports isr.Core.Shell.ShellWrapper

#Disable Warning CA1036 ' Override methods on comparable types
''' <summary> A shell item. </summary>
''' <remarks> (c) 2012 James D. Parsells. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2019-07-06, 3.0.*. </para></remarks>
Public Class ShellItem
#Enable Warning CA1036 ' Override methods on comparable types
    Implements IDisposable, IComparable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Private Constructor, creates new shell item from the item's parent folder and
    '''  the item's PIDL  relative to that folder.</summary>
    ''' <param name="folder">the folder interface of the parent</param>
    ''' <param name="pidl">the Relative PIDL  of this item</param>
    ''' <param name="parent">the shell item of the parent</param>
    ''' 
    Private Sub New(ByVal folder As IShellFolder, ByVal pidl As IntPtr, ByVal parent As ShellItem)
        If IsNothing(_DesktopBase) Then
            _DesktopBase = New ShellItem() 'This initializes the Desktop folder
        End If
        Me._Parent = parent
        Me._PIDL = ConcatPidls(parent.PIDL, pidl)

        'Get some attributes
        Me.SetUpAttributes(folder, pidl)

        'Set un-fetched value for IconIndex....
        Me._IconIndexNormal = -1
        Me._IconIndexOpen = -1
        'finally, set up my Folder
        If Me._IsFolder Then
            Dim HR As Integer
            HR = folder.BindToObject(pidl, IntPtr.Zero, IID_IShellFolder, Me._Folder)
            If HR <> NOERROR Then
                Marshal.ThrowExceptionForHR(HR)
            End If
        End If
    End Sub

    ''' <summary>
    ''' Private Constructor. Creates shell item of the Desktop
    ''' only used when desktop folder has not been initialized
    ''' </summary>
    Private Sub New()
        If Not IsNothing(_DesktopBase) Then
            Throw New Exception("Attempt to initialize shell item for second time")
        End If

        Dim HR As Integer
        'firstly determine what the local machine calls a "System Folder" and "My Computer"
        Dim tmpPidl As IntPtr
        HR = UnsafeNativeMethods.SHGetSpecialFolderLocation(IntPtr.Zero, CSIDL.DRIVES, tmpPidl)
        Dim shfi As New SHFILEINFO()
        Dim dwflag As Integer = SHGFI.DISPLAYNAME Or
                                SHGFI.TYPENAME Or
                                SHGFI.PIDL
        Dim dwAttr As Integer = 0
        UnsafeNativeMethods.SHGetFileInfo(tmpPidl, dwAttr, shfi, FileInfoSize, dwflag)
        _SystemFolder = shfi.szTypeName
        _MyComputer = shfi.szDisplayName
        Marshal.FreeCoTaskMem(tmpPidl)
        'set OS version info
        _XPorAbove = ShellWrapper.IsXpOrAbove()
        _Win2KOrAbove = ShellWrapper.Is2KOrAbove

        'With That done, now set up Desktop shell item
        Me._Path = "::{" & DesktopGUID.ToString & "}"
        Me._IsFolder = True
        Me._HasSubFolders = True
        Me._IsBrowsable = False
        HR = UnsafeNativeMethods.SHGetDesktopFolder(Me._Folder)
        Me._PIDL = GetSpecialFolderLocation(IntPtr.Zero, CSIDL.DESKTOP)
        dwflag = SHGFI.DISPLAYNAME Or
                 SHGFI.TYPENAME Or
                 SHGFI.SYSICONINDEX Or
                 SHGFI.PIDL
        dwAttr = 0
        Dim H As IntPtr = UnsafeNativeMethods.SHGetFileInfo(Me._PIDL, dwAttr, shfi, FileInfoSize, dwflag)
        Me._DisplayName = shfi.szDisplayName
        Me._TypeName = SystemFolder   'not returned correctly by SHGetFileInfo
        Me._IconIndexNormal = shfi.iIcon
        Me._IconIndexOpen = shfi.iIcon
        Me._HasDispType = True
        Me._IsDropTarget = True
        Me._IsReadOnly = False
        Me._IsReadOnlySetup = True

        'also get local name for "My Documents"
        Dim pchEaten As Integer
        tmpPidl = IntPtr.Zero
        HR = Me._Folder.ParseDisplayName(Nothing, Nothing, "::{450d8fba-ad25-11d0-98a8-0800361b1103}",
                 pchEaten, tmpPidl, Nothing)
        shfi = New SHFILEINFO()
        dwflag = SHGFI.DISPLAYNAME Or
                                SHGFI.TYPENAME Or
                                SHGFI.PIDL
        dwAttr = 0
        UnsafeNativeMethods.SHGetFileInfo(tmpPidl, dwAttr, shfi, FileInfoSize, dwflag)
        _StrMyDocuments = shfi.szDisplayName
        Marshal.FreeCoTaskMem(tmpPidl)
        'this must be done after getting "My Documents" string
        Me._SortFlag = Me.ComputeSortFlag()
        'Set DesktopBase
        _DesktopBase = Me

        _SystemName = Environment.MachineName    '2012-04-14
        ' Lastly, get the Path and shell item of the DesktopDirectory -- useful for DragDrop
        _DeskTopDirectory = New ShellItem(CSIDL.DESKTOPDIRECTORY)
    End Sub

    ''' <summary>Create instance based on a non-desktop CSIDL.
    ''' Will create based on any CSIDL Except the DeskTop CSIDL</summary>
    ''' <param name="id">Value from CSIDL enumeration denoting the folder to create this shell item of.</param>
    ''' 
    Public Sub New(ByVal id As CSIDL)
        If IsNothing(_DesktopBase) Then
            _DesktopBase = New ShellItem() 'This initializes the Desktop folder
        End If
        Dim HR As Integer
        If id = CSIDL.MYDOCUMENTS Then
            Dim pchEaten As Integer
            HR = _DesktopBase._Folder.ParseDisplayName(Nothing, Nothing, "::{450d8fba-ad25-11d0-98a8-0800361b1103}",
                     pchEaten, Me._PIDL, Nothing)
        Else
            HR = UnsafeNativeMethods.SHGetSpecialFolderLocation(IntPtr.Zero, id, Me._PIDL)
        End If
        If HR = NOERROR Then
            Dim pParent As IShellFolder
            Dim relPidl As IntPtr = IntPtr.Zero

            pParent = GetParentOf(Me._PIDL, relPidl)
            'Get the Attributes
            Me.SetUpAttributes(pParent, relPidl)
            'Set un-fetched value for IconIndex....
            Me._IconIndexNormal = -1
            Me._IconIndexOpen = -1
            'finally, set up my Folder
            If Me._IsFolder Then
                HR = pParent.BindToObject(relPidl, IntPtr.Zero, IID_IShellFolder, Me._Folder)
                If HR <> NOERROR Then
                    Marshal.ThrowExceptionForHR(HR)
                End If
            End If
            Marshal.ReleaseComObject(pParent)
            'if PidlCount(m_Pidl) = 1 then relPidl is same as m_Pidl, don't release
            If PidlCount(Me._PIDL) > 1 Then Marshal.FreeCoTaskMem(relPidl)
        Else
            Marshal.ThrowExceptionForHR(HR)
        End If
    End Sub

    ''' <summary>Create a new shell item based on a Path Must be a valid FileSystem Path</summary>
    ''' <param name="path"></param>
    ''' 
    Public Sub New(ByVal path As String)
        If IsNothing(_DesktopBase) Then
            _DesktopBase = New ShellItem() 'This initializes the Desktop folder
        End If
        'Removal of following code allows Path(GUID) of Special FOlders to serve
        '  as a valid Path for shell item creation (part of Calum's refresh code needs this
        'If Not Directory.Exists(path) AndAlso Not File.Exists(path) Then
        '    Throw New Exception("shell item -- Invalid Path specified")
        'End If
        Dim HR As Integer
        HR = _DesktopBase._Folder.ParseDisplayName(0, IntPtr.Zero, path, 0, Me._PIDL, 0)
        If Not HR = NOERROR Then Marshal.ThrowExceptionForHR(HR)
        Dim pParent As IShellFolder
        Dim relPidl As IntPtr = IntPtr.Zero

        pParent = GetParentOf(Me._PIDL, relPidl)

        'Get the Attributes
        Me.SetUpAttributes(pParent, relPidl)
        'Set un-fetched value for IconIndex....
        Me._IconIndexNormal = -1
        Me._IconIndexOpen = -1
        'finally, set up my Folder
        If Me._IsFolder Then
            HR = pParent.BindToObject(relPidl, IntPtr.Zero, IID_IShellFolder, Me._Folder)
            If HR <> NOERROR Then
                Marshal.ThrowExceptionForHR(HR)
            End If
        End If
        Marshal.ReleaseComObject(pParent)
        'if PidlCount(m_Pidl) = 1 then relPidl is same as m_Pidl, don't release
        If PidlCount(Me._PIDL) > 1 Then
            Marshal.FreeCoTaskMem(relPidl)
        End If
    End Sub


    '''<Summary>Given a Byte() containing the PIDL  of the parent
    ''' folder and another Byte() containing the PIDL  of the Item,
    ''' relative to the Folder, Create a shell item for the Item.
    ''' This is of primary use in dealing with "Shell IDList Array" 
    ''' formatted info passed in a Drag Operation
    ''' </Summary>
    Public Sub New(ByVal foldBytes() As Byte, ByVal itemBytes() As Byte)
        Debug.WriteLine("shell item.New(FoldBytes,ItemBytes) Fold len= " & foldBytes.Length & " Item Len = " & itemBytes.Length)
        If IsNothing(_DesktopBase) Then
            _DesktopBase = New ShellItem() 'This initializes the Desktop folder
        End If
        Dim pParent As IShellFolder = MakeFolderFromBytes(foldBytes)
        If IsNothing(pParent) Then
            GoTo XIT    'm_PIDL will = IntPtr.Zero for really bad shell item
        End If
        Dim ipParent As IntPtr = PidlWrapper.BytesToPidl(foldBytes)
        Dim ipItem As IntPtr = PidlWrapper.BytesToPidl(itemBytes)
        If ipParent.Equals(IntPtr.Zero) Or ipItem.Equals(IntPtr.Zero) Then
            GoTo XIT
        End If
        ' Now process just like sub new(folder,pidl,parent) version
        Me._PIDL = ConcatPidls(ipParent, ipItem)

        'Get some attributes
        Me.SetUpAttributes(pParent, ipItem)

        'Set un-fetched value for IconIndex....
        Me._IconIndexNormal = -1
        Me._IconIndexOpen = -1
        'finally, set up my Folder
        If Me._IsFolder Then
            Dim HR As Integer
            HR = pParent.BindToObject(ipItem, IntPtr.Zero, IID_IShellFolder, Me._Folder)
#If DEBUG Then
            If HR <> NOERROR Then
                Marshal.ThrowExceptionForHR(HR)
            End If
#End If
        End If
XIT:    'On any kind of exit, free the allocated memory
#If DEBUG Then
        If Me._PIDL.Equals(IntPtr.Zero) Then
            Debug.WriteLine("shell item.New(FoldBytes,ItemBytes) Failed")
        Else
            Debug.WriteLine("shell item.New(FoldBytes,ItemBytes) Created " & Me.Path)
        End If
#End If
        If Not ipParent.Equals(IntPtr.Zero) Then
            Marshal.FreeCoTaskMem(ipParent)
        End If
        If Not ipItem.Equals(IntPtr.Zero) Then
            Marshal.FreeCoTaskMem(ipItem)
        End If
    End Sub

    ''' <summary> Gets the disposed. </summary>
    ''' <value> The disposed. </value>
    Private Property Disposed As Boolean

    ''' <summary>
    ''' Deallocates CoTaskMem containing m_Pidl and removes reference to m_Folder
    ''' </summary>
    ''' <param name="disposing"></param>
    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        ' Allow your Dispose method to be called multiple times,
        ' but throw an exception if the object has been disposed.
        ' Whenever you do something with this class, 
        ' check to see if it has been disposed.
        Try
            If Not (Me._Disposed) Then
                ' If disposing equals true, dispose all managed 
                ' and unmanaged resources.
                If (disposing) Then
                End If
                ' Release unmanaged resources. If disposing is false,
                ' only the following code is executed. 
                If Not IsNothing(Me._Folder) Then
                    Marshal.ReleaseComObject(Me._Folder)
                End If
                If Not Me._PIDL.Equals(IntPtr.Zero) Then
                    Marshal.FreeCoTaskMem(Me._PIDL)
                End If
            End If
        Finally
            Me._Disposed = True
        End Try
    End Sub

    ' This Finalize method will run only if the 
    ' Dispose method does not get called.
    ' By default, methods are NotOverridable. 
    ' This prevents a derived class from overriding this method.
    ''' <summary>
    ''' Summary of Finalize.
    ''' </summary>
    Protected Overrides Sub Finalize()
        ' Do not re-create Dispose clean-up code here.
        ' Calling Dispose(false) is optimal in terms of
        ' readability and maintainability.
        Me.Dispose(False)
    End Sub

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Me.Dispose(True)
        ' Take yourself off of the finalization queue
        ' to prevent finalization code for this object
        ' from executing a second time.
        GC.SuppressFinalize(Me)
    End Sub

#End Region

#Region " Shared Private Fields"

    'To get My Documents sorted first, we need to know the Locale 
    'specific name of that folder.
    Private Shared _StrMyDocuments As String

    ' The DesktopBase is set up via Sub New() (one time only) and
    '  disposed of only when DesktopBase is finally disposed of
    Private Shared _DesktopBase As ShellItem

    'We can avoid an extra SHGetFileInfo call once this is set up
    Private Shared Property OpenFolderIconIndex As Integer = -1

    ' It is also useful to know if the OS is XP or above.  
    ' Set up in Sub New() to avoid multiple calls to find this info
    Private Shared _XPorAbove As Boolean

    ' Likewise if OS is Win2K or Above
    Private Shared _Win2KOrAbove As Boolean

    ' Keep the local System Name for IsRemote testing
    Private Shared _SystemName As String                              '2012-04-14

    ' Keep list of Drives and their DriveType for IsRemote testing
    Private Shared ReadOnly DriveDict As New Dictionary(Of String, Boolean)   '2012-04-16

#End Region

#Region " INSTANCE PRIVATE FIELDS "

    Private _DisplayName As String = String.Empty
    Private _Path As String
    Private _TypeName As String
    Private _IconIndexNormal As Integer   'index into the System Image list for Normal icon
    Private _IconIndexOpen As Integer 'index into the SystemImage list for Open icon
    Private _IsBrowsable As Boolean
    Private _IsFileSystem As Boolean
    Private _IsFolder As Boolean
    Private _HasSubFolders As Boolean
    Private _IsLink As Boolean
    Private _IsDisk As Boolean
    Private _IsShared As Boolean
    Private _IsHidden As Boolean
    Private _IsNetWorkDrive As Boolean '= False
    Private _IsRemovable As Boolean '= False
    Private _IsReadOnly As Boolean '= False
    'Properties of interest to Drag Operations
    Private _CanMove As Boolean '= False
    Private _CanCopy As Boolean '= False
    Private _CanDelete As Boolean '= False
    Private _CanLink As Boolean '= False
    Private _IsDropTarget As Boolean '= False
    Private _Attributes As FileAttributes  'Added 2011-10-09 'True FileAttributes from FileInfo
    Private _SFGAO_Attributes As SFGAO     'the original, returned from GetAttributesOf Added 2011-10-09 
    Private _IsRemote As Boolean           '2012-04-14

    Private _SortFlag As Integer '= 0 'Used in comparisons

    Private _Directories As ArrayList

    'The following elements are only filled in on demand
    Private _XtrInfo As Boolean '= False
    Private _LastWriteTime As DateTime
    Private _CreationTime As DateTime
    Private _LastAccessTime As DateTime
    Private _Length As Long

    'Indicates whether DisplayName, TypeName, SortFlag have been set up
    Private _HasDispType As Boolean '= False

    'Indicates whether IsReadOnly has been set up
    Private _IsReadOnlySetup As Boolean '= False

#End Region

#Region "   UTILITY FUNCTIONS USED IN CONSTRUCTORS"

    '''<Summary>It is impossible to validate a PIDL  completely since its contents
    ''' are arbitrarily defined by the creating Shell Namespace.  However, it
    ''' is possible to validate the structure of a PIDL.</Summary>
    Public Shared Function IsValidPidl(ByVal b() As Byte) As Boolean
        IsValidPidl = False     'assume failure
        Dim bMax As Integer = b.Length - 1   'max value that index can have
        If bMax < 1 Then Exit Function 'min size is 2 bytes
        Dim cb As Integer = b(0) + (b(1) * 256)
        Dim indx As Integer = 0
        Do While cb > 0
            If (indx + cb + 1) > bMax Then Exit Function 'an error
            indx += cb
            cb = b(indx) + (b(indx + 1) * 256)
        Loop
        ' on fall thru, it is ok as far as we can check
        IsValidPidl = True
    End Function

    ''' <summary> Makes folder from bytes. </summary>
    ''' <param name="b"> A single dimension Byte Array. </param>
    ''' <returns> A ShellWrapper.IShellFolder. </returns>
    Public Shared Function MakeFolderFromBytes(ByVal b As Byte()) As IShellFolder
        MakeFolderFromBytes = Nothing       'get rid of VS2005 warning
        If Not IsValidPidl(b) Then Return Nothing
        If b.Length = 2 AndAlso ((b(0) = 0) And (b(1) = 0)) Then 'this is the desktop
            Return _DesktopBase.Folder
        ElseIf b.Length = 0 Then   'Also indicates the desktop
            Return _DesktopBase.Folder
        Else
            Dim ptr As IntPtr = Marshal.AllocCoTaskMem(b.Length)
            If ptr.Equals(IntPtr.Zero) Then Return Nothing
            Marshal.Copy(b, 0, ptr, b.Length)
            'the next statement assigns a IshellFolder object to the function return, or has an error
            Dim hr As Integer = _DesktopBase.Folder.BindToObject(ptr, IntPtr.Zero, IID_IShellFolder, MakeFolderFromBytes)
            If hr <> 0 Then MakeFolderFromBytes = Nothing
            Marshal.FreeCoTaskMem(ptr)
        End If
    End Function

    '''<Summary>Returns both the IShellFolder interface of the parent folder
    '''  and the relative PIDL  of the input PIDL</Summary>
    '''<remarks>Several internal functions need this information and do not have
    ''' it readily available. GetParentOf serves those functions</remarks>
    Private Shared Function GetParentOf(ByVal pidl As IntPtr, ByRef relPidl As IntPtr) As IShellFolder
        GetParentOf = Nothing     'avoid VB2005 warning
        Dim HR As Integer
        Dim itemCnt As Integer = PidlCount(pidl)
        If itemCnt = 1 Then         'parent is desktop
            HR = UnsafeNativeMethods.SHGetDesktopFolder(GetParentOf)
            relPidl = pidl
        Else
            Dim tmpPidl As IntPtr
            tmpPidl = TrimPidl(pidl, relPidl)
            HR = _DesktopBase._Folder.BindToObject(tmpPidl, IntPtr.Zero, IID_IShellFolder, GetParentOf)
            Marshal.FreeCoTaskMem(tmpPidl)
        End If
        If Not HR = NOERROR Then Marshal.ThrowExceptionForHR(HR)
    End Function

    ''' <summary>Get the base attributes of the folder/file that this shell item represents</summary>
    ''' <param name="folder">Parent Folder of this Item</param>
    ''' <param name="pidl">Relative PIDL  of this Item.</param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SetUpAttributes(ByVal folder As IShellFolder, ByVal pidl As IntPtr)
        Dim attrFlag As SFGAO
        attrFlag = SFGAO.BROWSABLE
        attrFlag = attrFlag Or SFGAO.FILESYSTEM
        'attrFlag = attrFlag Or SFGAO.HASSUBFOLDER   'made into an on-demand attribute
        attrFlag = attrFlag Or SFGAO.FOLDER
        attrFlag = attrFlag Or SFGAO.LINK
        attrFlag = attrFlag Or SFGAO.SHARE
        attrFlag = attrFlag Or SFGAO.HIDDEN
        attrFlag = attrFlag Or SFGAO.REMOVABLE
        'attrFlag = attrFlag Or SFGAO.RDONLY   'made into an on-demand attribute
        attrFlag = attrFlag Or SFGAO.CANCOPY
        attrFlag = attrFlag Or SFGAO.CANDELETE
        attrFlag = attrFlag Or SFGAO.CANLINK
        attrFlag = attrFlag Or SFGAO.CANMOVE
        attrFlag = attrFlag Or SFGAO.DROPTARGET
        'Note: for GetAttributesOf, we must provide an array, in  all cases with 1 element
        Dim aPidl(0) As IntPtr
        aPidl(0) = pidl
        folder.GetAttributesOf(1, aPidl, attrFlag)
        Me._SFGAO_Attributes = attrFlag
        Me._IsBrowsable = CBool(attrFlag And SFGAO.BROWSABLE)
        Me._IsFileSystem = CBool(attrFlag And SFGAO.FILESYSTEM)
        'm_HasSubFolders = CBool(attrFlag And SFGAO.HASSUBFOLDER)  'made into an on-demand attribute
        Me._IsFolder = CBool(attrFlag And SFGAO.FOLDER)
        Me._IsLink = CBool(attrFlag And SFGAO.LINK)
        Me._IsShared = CBool(attrFlag And SFGAO.SHARE)
        Me._IsHidden = CBool(attrFlag And SFGAO.HIDDEN)
        Me._IsRemovable = CBool(attrFlag And SFGAO.REMOVABLE)
        'm_IsReadOnly = CBool(attrFlag And SFGAO.RDONLY)      'made into an on-demand attribute
        Me._CanCopy = CBool(attrFlag And SFGAO.CANCOPY)
        Me._CanDelete = CBool(attrFlag And SFGAO.CANDELETE)
        Me._CanLink = CBool(attrFlag And SFGAO.CANLINK)
        Me._CanMove = CBool(attrFlag And SFGAO.CANMOVE)
        Me._IsDropTarget = CBool(attrFlag And SFGAO.DROPTARGET)

        'Get the Path
        Dim strr As IntPtr = Marshal.AllocCoTaskMem(MAX_PATH * 2 + 4)
        Marshal.WriteInt32(strr, 0, 0)
        Dim buf As New StringBuilder(MAX_PATH)
        Dim itemflags As SHGDN = SHGDN.FORPARSING
        folder.GetDisplayNameOf(pidl, itemflags, strr)
        Dim HR As Integer = UnsafeNativeMethods.StrRetToBuf(strr, pidl, buf, MAX_PATH)
        Marshal.FreeCoTaskMem(strr)     'now done with it
        If HR = NOERROR Then
            Me._Path = buf.ToString
            'check for zip file = folder on xp, leave it a file
            If Me._IsFolder AndAlso Me._IsFileSystem AndAlso _XPorAbove Then
                'Note:meaning of SFGAO.STREAM changed between win2k and winXP
                'Version 20 code
                'If File.Exists(m_Path) Then
                '    m_IsFolder = False
                'End If
                'Version 21 code
                aPidl(0) = pidl
                attrFlag = SFGAO.STREAM
                folder.GetAttributesOf(1, aPidl, attrFlag)
                If 0 <> (attrFlag And SFGAO.STREAM) Then
                    Me._IsFolder = False
                End If
            End If
            If Me._Path.Length = 3 AndAlso Me._Path.Substring(1).Equals(":\") Then
                Me._IsDisk = True
                Try                 '2012-04-16 Entire Try Block
                    Dim disk As New Management.ManagementObject("win32_logicaldisk.deviceid=""" & Me.Path.Substring(0, 2) & """")
                    Me._Length = CType(disk("Size"), Int64)
                    If CType(disk("DriveType"), UInt32).ToString = CStr(4) Then
                        Me._IsNetWorkDrive = True
                        Me._IsRemote = True
                    End If
                Catch ex As Exception
                    'Disconnected Network Drives etc. will generate 
                    'an error here, just assume that it is a network
                    'drive
                    Me._IsNetWorkDrive = True
                    Me._IsRemote = True
                Finally
                    Me._XtrInfo = True
                    If Not DriveDict.ContainsKey(Me._Path) Then
                        DriveDict.Add(Me._Path, Me._IsRemote)
                        Debug.WriteLine(Me._Path & " " & DriveDict(Me._Path).ToString)
                    End If
                End Try
            End If

            'Setup IsRemote             '2012-04-14
            If Not (Me._IsDisk OrElse Me._Path.StartsWith("::")) Then
                Dim ItemRoot As String = IO.Path.GetPathRoot(Me._Path)
                If Not (ItemRoot.Length = 3 AndAlso ItemRoot.Substring(1).Equals(":\")) Then
                    Dim tmp() As String = ItemRoot.Split(New Char() {"\"c, "/"c}, StringSplitOptions.RemoveEmptyEntries)
                    Me._IsRemote = tmp.Length = 0 OrElse Not tmp(0).Equals(_SystemName, StringComparison.InvariantCultureIgnoreCase)
                Else
                    If DriveDict.ContainsKey(ItemRoot) AndAlso DriveDict(ItemRoot) Then Me._IsRemote = True '2012-04-16
                End If
            End If
        Else
            Marshal.ThrowExceptionForHR(HR)
        End If
    End Sub

    ''' <summary> Gets shell item. </summary>
    ''' <param name="path"> . </param>
    ''' <returns> The shell item. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function GetShellItem(ByVal path As String) As ShellItem
        GetShellItem = Nothing    'assume failure
        Dim HR As Integer
        Dim tmpPidl As IntPtr
        HR = GetDeskTop.Folder.ParseDisplayName(0, IntPtr.Zero, path, 0, tmpPidl, 0)
        If HR = 0 Then
            GetShellItem = FindShellItem(tmpPidl)
            If IsNothing(GetShellItem) Then
                Try
                    GetShellItem = New ShellItem(path)
                Catch
                    GetShellItem = Nothing
                End Try
            End If
        End If
        If Not tmpPidl.Equals(IntPtr.Zero) Then
            Marshal.FreeCoTaskMem(tmpPidl)
        End If
    End Function

    ''' <summary> Gets shell item. </summary>
    ''' <param name="id"> Value from CSIDL enumeration denoting the folder to create this shell item of. </param>
    ''' <returns> The shell item. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function GetShellItem(ByVal id As CSIDL) As ShellItem
        GetShellItem = Nothing      'avoid VB2005 Warning
        If id = CSIDL.DESKTOP Then
            Return GetDeskTop()
        End If
        Dim HR As Integer
        Dim tmpPidl As IntPtr
        If id = CSIDL.MYDOCUMENTS Then
            Dim pchEaten As Integer
            HR = GetDeskTop.Folder.ParseDisplayName(Nothing, Nothing, "::{450d8fba-ad25-11d0-98a8-0800361b1103}",
                     pchEaten, tmpPidl, Nothing)
        Else
            HR = UnsafeNativeMethods.SHGetSpecialFolderLocation(IntPtr.Zero, id, tmpPidl)
        End If
        If HR = NOERROR Then
            GetShellItem = FindShellItem(tmpPidl)
            If IsNothing(GetShellItem) Then
                Try
                    GetShellItem = New ShellItem(id)
                Catch
                    GetShellItem = Nothing
                End Try
            End If
        End If
        If Not tmpPidl.Equals(IntPtr.Zero) Then
            Marshal.FreeCoTaskMem(tmpPidl)
        End If
    End Function

    ''' <summary> Gets shell item. </summary>
    ''' <param name="foldBytes"> The fold in bytes. </param>
    ''' <param name="itemBytes"> The item in bytes. </param>
    ''' <returns> The shell item. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function GetShellItem(ByVal foldBytes() As Byte, ByVal itemBytes() As Byte) As ShellItem
        GetShellItem = Nothing    'assume failure
        Dim b() As Byte = PidlWrapper.JoinPidlBytes(foldBytes, itemBytes)
        If IsNothing(b) Then Exit Function 'can do no more with invalid pidl's
        'otherwise do like below, skipping unnecessary validation check
        Dim thisPidl As IntPtr = Marshal.AllocCoTaskMem(b.Length)
        If thisPidl.Equals(IntPtr.Zero) Then Return Nothing
        Marshal.Copy(b, 0, thisPidl, b.Length)
        GetShellItem = FindShellItem(thisPidl)
        Marshal.FreeCoTaskMem(thisPidl)
        If IsNothing(GetShellItem) Then   'didn't find it, make new
            Try
                GetShellItem = New ShellItem(foldBytes, itemBytes)
            Catch

            End Try
        End If
        If GetShellItem.PIDL.Equals(IntPtr.Zero) Then GetShellItem = Nothing
    End Function

    ''' <summary> Searches for the first shell item. </summary>
    ''' <param name="b"> A single dimension Byte Array. </param>
    ''' <returns> The found shell item. </returns>
    Public Shared Function FindShellItem(ByVal b() As Byte) As ShellItem
        If Not IsValidPidl(b) Then Return Nothing
        Dim thisPidl As IntPtr = Marshal.AllocCoTaskMem(b.Length)
        If thisPidl.Equals(IntPtr.Zero) Then Return Nothing
        Marshal.Copy(b, 0, thisPidl, b.Length)
        FindShellItem = FindShellItem(thisPidl)
        Marshal.FreeCoTaskMem(thisPidl)
    End Function

    ''' <summary> Searches for the first shell item. </summary>
    ''' <param name="ptr"> The pointer. </param>
    ''' <returns> The found shell item. </returns>
    Public Shared Function FindShellItem(ByVal ptr As IntPtr) As ShellItem
        FindShellItem = Nothing     'avoid VB2005 Warning
        Dim BaseItem As ShellItem = ShellItem.GetDeskTop
        Dim CSI As ShellItem
        Dim FoundIt As Boolean = False      'True if we found item or an ancestor
        Do Until FoundIt
            For Each CSI In BaseItem.GetDirectories
                If IsAncestorOf(CSI.PIDL, ptr) Then
                    If ShellItem.IsEqual(CSI.PIDL, ptr) Then  'we found the desired item
                        Return CSI
                    Else
                        BaseItem = CSI
                        FoundIt = True
                        Exit For
                    End If
                End If
            Next
            If Not FoundIt Then Return Nothing 'didn't find an ancestor
            'The complication is that the desired item may not be a directory
            If Not IsAncestorOf(BaseItem.PIDL, ptr, True) Then  'Don't have immediate ancestor
                FoundIt = False     'go around again
            Else
                For Each CSI In BaseItem.GetItems
                    If ShellItem.IsEqual(CSI.PIDL, ptr) Then
                        Return CSI
                    End If
                Next
                'fall thru here means it doesn't exist or we can't find it because of funny PIDL  from SHParseDisplayName
                Return Nothing
            End If
            DispatcherExtensions.DoEvents(Windows.Threading.Dispatcher.CurrentDispatcher)
        Loop
    End Function

    ''' <summary>Computes the Sort key of this shell item, based on its attributes</summary>
    Private Function ComputeSortFlag() As Integer
        Dim rVal As Integer = 0
        If Me._IsDisk Then rVal = &H100000
        If Me._TypeName.Equals(SystemFolder) Then
            If Not Me._IsBrowsable Then
                rVal = rVal Or &H10000
                If _StrMyDocuments.Equals(Me._DisplayName) Then
                    rVal = rVal Or &H1
                End If
            Else
                rVal = rVal Or &H1000
            End If
        End If
        If Me._IsFolder Then rVal = rVal Or &H100
        Return rVal
    End Function

    ''' <summary> CompareTo(obj as object) Compares obj to this instance based on SortFlag-- obj must be a shell item</summary>
    ''' <SortOrder>  (low)Disks,non-browsable System Folders, browsable System Folders,  Directories, Files, Nothing (high)</SortOrder>
    ''' <param name="obj"> An object to compare with this instance. </param>
    ''' <returns>
    ''' Negative if this object is less than the other, 0 if they are equal, or positive if this is
    ''' greater.
    ''' </returns>
    Public Overridable Overloads Function CompareTo(ByVal obj As Object) As Integer _
            Implements IComparable.CompareTo
        If IsNothing(obj) Then Return 1 'non-existent is always low
        Dim Other As ShellItem = CType(obj, ShellItem)
        If Not Me._HasDispType Then Me.SetDispType()
        Dim cmp As Integer = Other.SortFlag - Me._SortFlag 'Note the reversal
        Return If(cmp <> 0, cmp, If(Me._IsDisk, String.Compare(Me._Path, Other.Path),
                                                StringLogicalComparer.CompareStrings(Me._DisplayName, Other.DisplayName)))
    End Function

#End Region

#Region " Properties"

#Region "   Shared Properties"

    ' My Computer is also commonly used (though not internally),
    ' so save & expose its name on the current machine
    Public Shared ReadOnly Property MyComputer() As String

    'This class has occasion to refer to the TypeName as reported by
    ' SHGetFileInfo. It needs to compare this to the string
    ' (in English) "System Folder"
    'on non-English systems, we do not know, in the general case,
    ' what the equivalent string is to compare against
    'The following variable is set by Sub New() to the string that
    ' corresponds to "System Folder" on the current machine
    ' Sub New() depends on the existence of My Computer(CSIDL.DRIVES),
    ' to determine what the equivalent string is
    Public Shared ReadOnly Property SystemFolder() As String

    ' DragDrop, possibly among others, needs to know the Path of
    ' the DeskTopDirectory in addition to the Desktop itself
    ' Also need the actual shell item for the DeskTopDirectory, so get it
    Private Shared _DeskTopDirectory As ShellItem

    Public Shared ReadOnly Property DesktopDirectoryPath() As String
        Get
            Return _DeskTopDirectory.Path
        End Get
    End Property

#End Region

#Region "   Normal Properties"

    ''' <summary> The Absolute PIDL for this item (not retained for files). </summary>
    ''' <remarks>
    ''' A pidl is a pointer to an array of item IDs. Each item ID in the list represents a level in
    ''' the shell namespace.
    ''' </remarks>
    ''' <value> The PIDL. </value>
    Public ReadOnly Property PIDL() As IntPtr

    ''' <summary> If item is a folder, contains the Folder interface for this instance. </summary>
    ''' <value> The Folder interface for this instance. </value>
    Public ReadOnly Property Folder() As IShellFolder

    Public ReadOnly Property Path() As String
        Get
            Return Me._Path
        End Get
    End Property

    ' Private _Parent As ShellItem '= Nothing
    Public ReadOnly Property Parent() As ShellItem

    ''' <summary>
    ''' This instance's Shell Attributes as returned by Folder.GetAttributesOf
    ''' </summary>
    ''' <returns>This instance's Shell Attributes as returned by Folder.GetAttributesOf</returns>
    ''' <remarks>Internal use only</remarks>
    Friend ReadOnly Property SFGAO_Attributes() As SFGAO        'Change 2011-10-09
        Get
            Return Me._SFGAO_Attributes
        End Get
    End Property

    Public ReadOnly Property IsBrowsable() As Boolean
        Get
            Return Me._IsBrowsable
        End Get
    End Property

    Public ReadOnly Property IsFileSystem() As Boolean
        Get
            Return Me._IsFileSystem
        End Get
    End Property

    Public ReadOnly Property IsFolder() As Boolean
        Get
            Return Me._IsFolder
        End Get
    End Property

    Private _HasSubFoldersSetup As Boolean
    ''' <summary>
    ''' True if item is a Folder and has sub-Folders
    ''' </summary>
    ''' <returns>True if item is a Folder and has sub-Folders, False otherwise</returns>
    ''' <remarks>Modified to make this attribute behave (with respect to Remote Folders) like XP, even on Vista/Win7.
    ''' That is, any Remote Folder is reported as HasSubFolders = True. Local Folders are tested with the API call.
    ''' On Vista/Win7, Compressed files (e.g., .Zip, .Cab, etc) are considered sub Folders by this Property.
    ''' This behavior is NOT modified to behave like XP.</remarks>
    Public ReadOnly Property HasSubFolders() As Boolean
        Get
            'Return Me.Directories.Length > 0  'new code(2009-12-11) since removed.
            If Me._HasSubFoldersSetup Then
                Return Me._HasSubFolders
            ElseIf Me._IsRemote Then          '2012-04-14
                Me._HasSubFolders = True      '2012-04-14
                Me._HasSubFoldersSetup = True '2012-04-14
            Else
                Dim shfi As New SHFILEINFO With {
                    .dwAttributes = SFGAO.HASSUBFOLDER
                }
                Dim dwflag As SHGFI = SHGFI.PIDL Or
                                        SHGFI.ATTRIBUTES Or
                                        SHGFI.ATTR_SPECIFIED
                Dim dwAttr As Integer = 0
                Dim H As IntPtr = UnsafeNativeMethods.SHGetFileInfo(Me._PIDL, dwAttr, shfi, FileInfoSize, dwflag)
                If H.ToInt32 <> NOERROR AndAlso H.ToInt32 <> 1 Then
                    Marshal.ThrowExceptionForHR(H.ToInt32)
                End If
                Me._HasSubFolders = CBool(shfi.dwAttributes And SFGAO.HASSUBFOLDER)
                Me._SFGAO_Attributes = CType(Me._SFGAO_Attributes Or (shfi.dwAttributes And SFGAO.HASSUBFOLDER), SFGAO)
                Me._HasSubFoldersSetup = True
            End If
            Return Me._HasSubFolders         'old code reverted to (2009-12-12)
        End Get
    End Property
    Public ReadOnly Property IsDisk() As Boolean
        Get
            Return Me._IsDisk
        End Get
    End Property
    Public ReadOnly Property IsLink() As Boolean
        Get
            Return Me._IsLink
        End Get
    End Property
    Public ReadOnly Property IsShared() As Boolean
        Get
            Return Me._IsShared
        End Get
    End Property
    Public ReadOnly Property IsHidden() As Boolean
        Get
            Return Me._IsHidden
        End Get
    End Property
    Public ReadOnly Property IsRemovable() As Boolean
        Get
            Return Me._IsRemovable
        End Get
    End Property
    Public ReadOnly Property IsRemote() As Boolean  '2012-04-14
        Get                                         '2012-04-14
            Return Me._IsRemote                       '2012-04-14
        End Get                                     '2012-04-14
    End Property

#Region "   Drag Ops Properties"
    Public ReadOnly Property CanMove() As Boolean
        Get
            Return Me._CanMove
        End Get
    End Property
    Public ReadOnly Property CanCopy() As Boolean
        Get
            Return Me._CanCopy
        End Get
    End Property
    Public ReadOnly Property CanDelete() As Boolean
        Get
            Return Me._CanDelete
        End Get
    End Property
    Public ReadOnly Property CanLink() As Boolean
        Get
            Return Me._CanLink
        End Get
    End Property
    Public ReadOnly Property IsDropTarget() As Boolean
        Get
            Return Me._IsDropTarget
        End Get
    End Property
#End Region

#End Region

#Region "   Filled on Demand Properties"

#Region "       Filled based on m_HasDispType"
    ''' <summary>
    ''' Set DisplayName, TypeName, and SortFlag when actually needed
    ''' </summary>
    ''' 
    Private Sub SetDispType()
        'Get Display name, Type Name
        Dim shfi As New SHFILEINFO()
        Dim dwflag As Integer = SHGFI.DISPLAYNAME Or
                                SHGFI.TYPENAME Or
                                SHGFI.PIDL
        Dim dwAttr As Integer = 0
        If Me._IsFileSystem And Not Me._IsFolder Then
            dwflag = dwflag Or SHGFI.USEFILEATTRIBUTES
            dwAttr = FILE_ATTRIBUTE_NORMAL
        End If
        Dim H As IntPtr = UnsafeNativeMethods.SHGetFileInfo(Me._PIDL, dwAttr, shfi, FileInfoSize, dwflag)
        Me._DisplayName = shfi.szDisplayName
        Me._TypeName = shfi.szTypeName
        'fix DisplayName
        If String.IsNullOrEmpty(Me._DisplayName) Then
            Me._DisplayName = Me._Path
        End If
        'Fix TypeName
        'If m_IsFolder And m_TypeName.Equals("File") Then
        '    m_TypeName = "File Folder"
        'End If
        Me._SortFlag = Me.ComputeSortFlag()
        Me._HasDispType = True
    End Sub

    Public ReadOnly Property DisplayName() As String
        Get
            If Not Me._HasDispType Then Me.SetDispType()
            Return Me._DisplayName
        End Get
    End Property

    Private ReadOnly Property SortFlag() As Integer
        Get
            If Not Me._HasDispType Then Me.SetDispType()
            Return Me._SortFlag
        End Get
    End Property

    Public ReadOnly Property TypeName() As String
        Get
            If Not Me._HasDispType Then Me.SetDispType()
            Return Me._TypeName
        End Get
    End Property
#End Region

#Region "       IconIndex properties"
    Public ReadOnly Property IconIndexNormal() As Integer
        Get
            If Me._IconIndexNormal < 0 Then
                If Not Me._HasDispType Then Me.SetDispType()
                Dim shfi As New SHFILEINFO()
                Dim dwflag As Integer = SHGFI.PIDL Or
                                        SHGFI.SYSICONINDEX
                Dim dwAttr As Integer = 0
                If Me._IsFileSystem And Not Me._IsFolder Then
                    dwflag = dwflag Or SHGFI.USEFILEATTRIBUTES
                    dwAttr = FILE_ATTRIBUTE_NORMAL
                End If
                Dim H As IntPtr = UnsafeNativeMethods.SHGetFileInfo(Me._PIDL, dwAttr, shfi, FileInfoSize, dwflag)
                Me._IconIndexNormal = shfi.iIcon
            End If
            Return Me._IconIndexNormal
        End Get
    End Property
    ' IconIndexOpen is Filled on demand
    Public ReadOnly Property IconIndexOpen() As Integer
        Get
            If Me._IconIndexOpen < 0 Then
                If Not Me._HasDispType Then Me.SetDispType()
                If Not Me._IsDisk And Me._IsFileSystem And Me._IsFolder Then
                    If OpenFolderIconIndex < 0 Then
                        Dim dwflag As Integer = SHGFI.SYSICONINDEX Or SHGFI.PIDL
                        Dim shfi As New SHFILEINFO()
                        Dim H As IntPtr = UnsafeNativeMethods.SHGetFileInfo(Me._PIDL, 0,
                                          shfi, FileInfoSize,
                                          dwflag Or SHGFI.OPENICON)
                        Me._IconIndexOpen = shfi.iIcon
                        'If m_TypeName.Equals("File Folder") Then
                        '    OpenFolderIconIndex = shfi.iIcon
                        'End If
                    Else
                        Me._IconIndexOpen = OpenFolderIconIndex
                    End If
                Else
                    Me._IconIndexOpen = Me._IconIndexNormal
                End If
            End If
            Return Me._IconIndexOpen
        End Get
    End Property
#End Region

#Region "       FileInfo type Information"

    ''' <summary>
    ''' Obtains information available from FileInfo.
    ''' </summary>
    ''' 
    Private Sub FillDemandInfo()
        If Not Me._IsDisk And Me._IsFileSystem And Not Me._IsFolder Then
            'in this case, it's a file
            If File.Exists(Me._Path) Then
                Dim fi As New FileInfo(Me._Path)
                Me._LastWriteTime = fi.LastWriteTime
                Me._LastAccessTime = fi.LastAccessTime
                Me._CreationTime = fi.CreationTime
                Me._Length = fi.Length
                Me._Attributes = fi.Attributes          'Added 2011-10-09
                Me._XtrInfo = True
            End If
        Else
            If Me._IsFileSystem And Me._IsFolder Then
                If Directory.Exists(Me._Path) Then
                    Dim di As New DirectoryInfo(Me._Path)
                    Me._LastWriteTime = di.LastWriteTime
                    Me._LastAccessTime = di.LastAccessTime
                    Me._CreationTime = di.CreationTime
                    Me._Attributes = di.Attributes          'Added 2011-10-09
                    Me._XtrInfo = True
                End If
            End If
        End If
    End Sub

    Public ReadOnly Property LastWriteTime() As DateTime
        Get
            If Not Me._XtrInfo Then
                Me.FillDemandInfo()
            End If
            Return Me._LastWriteTime
        End Get
    End Property
    Public ReadOnly Property LastAccessTime() As DateTime
        Get
            If Not Me._XtrInfo Then
                Me.FillDemandInfo()
            End If
            Return Me._LastAccessTime
        End Get
    End Property
    Public ReadOnly Property CreationTime() As DateTime
        Get
            If Not Me._XtrInfo Then
                Me.FillDemandInfo()
            End If
            Return Me._CreationTime
        End Get
    End Property
    Public ReadOnly Property Length() As Long
        Get
            If Not Me._XtrInfo Then
                Me.FillDemandInfo()
            End If
            Return Me._Length
        End Get
    End Property
    ''' <summary>
    ''' This instance's Attributes as returned by FileSystemInfo
    ''' </summary>
    ''' <returns>This instance's Attributes as returned by FileSystemInfo</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Attributes() As System.IO.FileAttributes
        Get
            If Not Me._XtrInfo Then
                Me.FillDemandInfo()
            End If
            Return Me._Attributes
        End Get
    End Property
    Public ReadOnly Property IsNetworkDrive() As Boolean
        Get
            If Not Me._XtrInfo Then
                Me.FillDemandInfo()
            End If
            Return Me._IsNetWorkDrive
        End Get
    End Property


    Private _PidlItem As PidlWrapper

    ''' <summary> Holds a byte() representation of _PIDL -- filled when needed. </summary>
    ''' <value> The PIDL wrapper. </value>
    Public ReadOnly Property PidlItem() As PidlWrapper
        Get
            If IsNothing(Me._PidlItem) Then
                Me._PidlItem = New PidlWrapper(Me._PIDL)
            End If
            Return Me._PidlItem
        End Get
    End Property

    '''<Summary>The IsReadOnly attribute causes an annoying access to any floppy drives
    ''' on the system. To postpone this (or avoid, depending on user action),
    ''' the attribute is only queried when asked for</Summary>
    Public ReadOnly Property IsReadOnly() As Boolean
        Get
            If Me._IsReadOnlySetup Then
                Return Me._IsReadOnly
            Else
                Dim shfi As New SHFILEINFO With {
                    .dwAttributes = SFGAO.RDONLY
                }
                Dim dwflag As Integer = SHGFI.PIDL Or
                                        SHGFI.ATTRIBUTES Or
                                        SHGFI.ATTR_SPECIFIED
                Dim dwAttr As Integer = 0
                Dim H As IntPtr = UnsafeNativeMethods.SHGetFileInfo(Me._PIDL, dwAttr, shfi, FileInfoSize, dwflag)
                If H.ToInt32 <> NOERROR AndAlso H.ToInt32 <> 1 Then
                    Marshal.ThrowExceptionForHR(H.ToInt32)
                End If
                Me._IsReadOnly = CBool(shfi.dwAttributes And SFGAO.RDONLY)
                'If m_IsReadOnly Then Debug.WriteLine("IsReadOnly -- " & m_Path)
                Me._IsReadOnlySetup = True
                Return Me._IsReadOnly
            End If
            'If Not m_XtrInfo Then
            '    FillDemandInfo()
            'End If
            'Return m_Attributes And FileAttributes.ReadOnly = FileAttributes.ReadOnly
        End Get
    End Property

    ''' <summary> True once we have system information. </summary>
    ''' <value> True once we have system information. </value>
    Private ReadOnly Property HaveSysInfo As Boolean

    Private _IsSystem As Boolean

    '''<Summary>The IsSystem attribute is seldom used, but required by DragDrop operations.
    ''' Since there is no way of getting ONLY the System attribute without getting
    ''' the RO attribute (which forces a reference to the floppy drive), we pay
    ''' the price of getting its own File/DirectoryInfo for this purpose alone.
    '''</Summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public ReadOnly Property IsSystem() As Boolean
        Get
            ' HaveSysInfo As Boolean   'true once we have gotten this attribute
            ' m_IsSystem As Boolean    'the value of this attribute once we have it
            If Not Me.HaveSysInfo Then
                Try
                    Me._IsSystem = (File.GetAttributes(Me._Path) And FileAttributes.System) = FileAttributes.System
                    Me._HaveSysInfo = True
                Catch ex As Exception
                    Me._HaveSysInfo = True
                End Try
            End If
            Debug.WriteLine("In IsSystem -- Path = " & Me._Path & " IsSystem = " & Me._IsSystem)
            Return Me._IsSystem
        End Get
    End Property

#End Region

#End Region

#End Region

#Region " Public Methods"

#Region "   Shared Public Methods"

    ''' <summary>
    ''' If not initialized, then build DesktopBase
    ''' once done, or if initialized already,
    ''' </summary>
    '''<returns>The DesktopBase shell item representing the desktop</returns>
    ''' 
    Public Shared Function GetDeskTop() As ShellItem
        If IsNothing(_DesktopBase) Then
            _DesktopBase = New ShellItem()
        End If
        Return _DesktopBase
    End Function

    '''<Summary>IsAncestorOf returns True if Shell Item ancestor is an ancestor of Shell Item current
    ''' if OS is Win2K or above, uses the ILIsParent API, otherwise uses the
    ''' cPidl function StartsWith.  This is necessary since ILIsParent in only available
    ''' in Win2K or above systems AND StartsWith fails on some folders on XP systems (most
    ''' obviously some Network Folder Shortcuts, but also Control Panel. Note, StartsWith
    ''' always works on systems prior to XP.
    ''' NOTE: if ancestor and current reference the same Item, both
    ''' methods return True</Summary>
    Public Shared Function IsAncestorOf(ByVal ancestor As ShellItem, ByVal current As ShellItem, Optional ByVal fParent As Boolean = False) As Boolean
        Return IsAncestorOf(ancestor.PIDL, current.PIDL, fParent)
    End Function

    '''<Summary> Compares a candidate Ancestor PIDL  with a Child PIDL  and
    ''' returns True if Ancestor is an ancestor of the child.
    ''' if fParent is True, then only return True if Ancestor is the immediate
    ''' parent of the Child</Summary>
    Public Shared Function IsAncestorOf(ByVal ancestorPidl As IntPtr, ByVal childPidl As IntPtr, Optional ByVal fParent As Boolean = False) As Boolean
        If Is2KOrAbove() Then
            Return UnsafeNativeMethods.ILIsParent(ancestorPidl, childPidl, fParent)
        Else
            Dim Child As New PidlWrapper(childPidl)
            Dim Ancestor As New PidlWrapper(ancestorPidl)
            IsAncestorOf = Child.StartsWith(Ancestor)
            If Not IsAncestorOf Then Exit Function
            If fParent Then ' check for immediate ancestor, if desired
                Dim oAncBytes() As Object = Ancestor.Decompose
                Dim oChildBytes() As Object = Child.Decompose
                If oAncBytes.Length <> (oChildBytes.Length - 1) Then
                    IsAncestorOf = False
                End If
            End If
        End If
    End Function

    '''<Summary>The WalkAllCallBack delegate defines the signature of 
    ''' the routine to be passed to DirWalker
    ''' Usage:  dim d1 as new ShellItem.WalkAllCallBack(address of your routine)
    '''   Callback function receives a shell item for each file and Directory in
    '''   Starting Directory and each sub-directory of this directory and
    '''   each sub-directory of each sub-directory ....
    '''</Summary>
    Public Delegate Function WalkAllCallBack(ByVal info As ShellItem, ByVal userLevel As Integer, ByVal tag As Integer) As Boolean

    '''<Summary>
    ''' AllFolderWalk recursively walks down directories from cStart, calling its
    '''   callback routine, WalkAllCallBack, for each Directory and File encountered, including those in
    '''   cStart.  UserLevel is incremented by 1 for each level of directories that DirWalker
    '''  recurses through.  Tag in an Integer that is simply passed, unmodified to the 
    '''  callback, with each shell item encountered, both File and Directory shell items.
    ''' </Summary>
    ''' <param name="cStart"></param>
    ''' <param name="cback"></param>
    ''' <param name="userLevel"></param>
    ''' <param name="tag"></param>
    Public Shared Function AllFolderWalk(ByVal cStart As ShellItem, ByVal cback As WalkAllCallBack, ByVal userLevel As Integer, ByVal tag As Integer) As Boolean
        If Not IsNothing(cStart) AndAlso cStart.IsFolder Then
            Dim cItem As ShellItem
            'first processes all files in this directory
            For Each cItem In cStart.GetFiles
                If Not cback(cItem, userLevel, tag) Then
                    Return False        'user said stop
                End If
            Next
            'then process all dirs in this directory, recursively
            For Each cItem In cStart.GetDirectories
                If Not cback(cItem, userLevel + 1, tag) Then
                    Return False        'user said stop
                Else
                    If Not AllFolderWalk(cItem, cback, userLevel + 1, tag) Then
                        Return False
                    End If
                End If
            Next
            Return True
        Else        'Invalid call
            Throw New ApplicationException("AllFolderWalk -- Invalid Start Directory")
        End If
    End Function
#End Region

#End Region

#Region "   Public Instance Methods"

#Region "       Equals"
    Public Overloads Function Equals(ByVal other As ShellItem) As Boolean
        Equals = Me.Path.Equals(other.Path)
    End Function
#End Region

#Region "   GetDirectories"
    ''' <summary>
    ''' Returns the Directories of this sub-folder as an ArrayList of Shell Items
    ''' </summary>
    ''' <param name="doRefresh">Optional, default=True, Refresh the directories</param>
    ''' <returns>An ArrayList of Shell Items. May return an empty ArrayList if there are none.</returns>
    ''' <remarks>revised to alway return an up-to-date list unless 
    ''' specifically instructed not to (useful in constructs like:
    ''' if CSI.RefreshDirectories then
    '''     Dirs = CSI.GetDirectories(False)  ' just did a Refresh </remarks>
    Public Function GetDirectories(Optional ByVal doRefresh As Boolean = True) As ArrayList
        If Me._IsFolder Then
            If doRefresh Then
                Me.RefreshDirectories()   ' return an up-to-date List
            ElseIf Me._Directories Is Nothing Then
                Me.RefreshDirectories()
            End If
            Return Me._Directories
        Else 'if it is not a Folder, then return empty arraylist
            Return New ArrayList()
        End If
    End Function

#End Region

#Region "   GetFiles"
    ''' <summary>
    ''' Returns the Files of this sub-folder as an
    '''   ArrayList of Shell Items
    ''' Note: we do not keep the arraylist of files, Generate it each time
    ''' </summary>
    ''' <returns>An ArrayList of Shell Items. May return an empty ArrayList if there are none.</returns>
    ''' 
    Public Function GetFiles() As ArrayList
        Return If(Me._IsFolder, Me.GetContents(SHCONTF.NONFOLDERS Or SHCONTF.INCLUDEHIDDEN), New ArrayList())
    End Function

    ''' <summary>
    ''' Returns the Files of this sub-folder, filtered by a filtering string, as an ArrayList of
    ''' Shell Items. Note: we do not keep the array list of files, Generate it each time.
    ''' </summary>
    ''' <param name="filter"> A filter string (for example: *.Doc) </param>
    ''' <returns>
    ''' An ArrayList of Shell Items. May return an empty ArrayList if there are none.
    ''' </returns>
    Public Function GetFiles(ByVal filter As String) As ArrayList
        If Me._IsFolder Then
            Dim dummy As New ArrayList()
            Dim fileentries() As String
            fileentries = Directory.GetFiles(Me._Path, filter)
            Dim vFile As String
            For Each vFile In fileentries
                dummy.Add(New ShellItem(vFile))
            Next
            Return dummy
        Else
            Return New ArrayList()
        End If
    End Function
#End Region

#Region "   GetItems"
    ''' <summary>
    ''' Returns the Directories and Files of this sub-folder as an
    '''   ArrayList of Shell Items
    ''' Note: we do not keep the arraylist of files, Generate it each time
    ''' </summary>
    ''' <returns>An ArrayList of Shell Items. May return an empty ArrayList if there are none.</returns>
    Public Function GetItems() As ArrayList
        Dim rVal As New ArrayList()
        If Me._IsFolder Then
            rVal.AddRange(Me.GetDirectories)
            rVal.AddRange(Me.GetContents(SHCONTF.NONFOLDERS Or SHCONTF.INCLUDEHIDDEN))
            rVal.Sort()
            Return rVal
        Else
            Return rVal
        End If
    End Function
#End Region

#Region "   GetFileName"
    '''<Summary>GetFileName returns the Full file name of this item.
    '''  Specifically, for a link file (xxx.txt.lnk for example) the
    '''  DisplayName property will return xxx.txt, this method will
    '''  return xxx.txt.lnk.  In most cases this is equivalent of
    '''  System.IO.Path.GetFileName(m_Path).  However, some m_Paths
    '''  actually are GUIDs.  In that case, this routine returns the
    '''  DisplayName</Summary>
    Public Function GetFileName() As String
        Return If(Me._Path.StartsWith("::{"), Me.DisplayName, If(Me._IsDisk, Me._Path.Substring(0, 1), IO.Path.GetFileName(Me._Path)))
    End Function
#End Region

#Region "   ReFreshDirectories"
    '''<Summary> A lower cost way of Refreshing the Directories of this shell item</Summary>
    '''<returns> Returns True if there were any changes</returns>
    Public Function RefreshDirectories() As Boolean
        RefreshDirectories = False      'value unless there were changes
        If Me._IsFolder Then          'if not a folder, then return false
            Dim InvalidDirs As New ArrayList()  'holds shell items of not found dirs
            If IsNothing(Me._Directories) Then
                Me._Directories = Me.GetContents(SHCONTF.FOLDERS Or SHCONTF.INCLUDEHIDDEN)
                RefreshDirectories = True     'changed from unexamined to examined
            Else
                'Get relative PIDLs  from current directory items
                Dim curPidls As ArrayList = Me.GetContents(SHCONTF.FOLDERS Or SHCONTF.INCLUDEHIDDEN, True)
                Dim iptr As IntPtr      'used below
                If curPidls.Count < 1 Then
                    If Me._Directories.Count > 0 Then
                        Me._Directories = New ArrayList() 'nothing there anymore
                        RefreshDirectories = True    'Changed from had some to have none
                    Else    'Empty before, Empty now, do nothing -- just a logic marker
                    End If
                Else    'still has some. Are they the same?
                    If Me._Directories.Count < 1 Then 'didn't have any before, so different
                        Me._Directories = Me.GetContents(SHCONTF.FOLDERS Or SHCONTF.INCLUDEHIDDEN)
                        RefreshDirectories = True     'changed from had none to have some
                    Else    'some before, some now. Same?  This is the complicated part
                        'Firstly, build ArrayLists of Relative Pidls
                        Dim compList As New ArrayList(curPidls)
                        'Since we are only comparing relative PIDLs, build a list of 
                        ' the relative PIDLs  of the old content -- saving repeated building
                        Dim iOld As Integer
                        Dim OldRel(Me._Directories.Count - 1) As IntPtr
                        For iOld = 0 To Me._Directories.Count - 1
                            'GetLastID returns a points into an EXISTING IDLIST -- never release that ptr
                            ' and never release the EXISTING IDLIST before thru with OldRel
                            OldRel(iOld) = GetLastID(CType(Me._Directories(iOld), ShellItem).PIDL)
                        Next
                        Dim iNew As Integer
                        For iOld = 0 To Me._Directories.Count - 1
                            For iNew = 0 To compList.Count - 1
                                If IsEqual(CType(compList(iNew), IntPtr), OldRel(iOld)) Then
                                    compList.RemoveAt(iNew)  'Match, don't look at this one again
                                    GoTo NXTOLD    'content item exists in both
                                End If
                            Next
                            'falling thru here means couldn't find iOld entry
                            InvalidDirs.Add(Me._Directories(iOld)) 'save off the unmatched shell item
                            RefreshDirectories = True
NXTOLD:                 Next
                        'any not found should be removed from m_Directories
                        Dim csi As ShellItem
                        For Each csi In InvalidDirs
                            Me._Directories.Remove(csi)
                        Next
                        'anything remaining in compList is a new entry
                        If compList.Count > 0 Then
                            RefreshDirectories = True
                            For Each iptr In compList   'these are relative PIDLs
                                Try                 'ASUS Fix
                                    Me._Directories.Add(New ShellItem(Me._Folder, iptr, Me))
                                Catch ex As InvalidCastException    'ASUS Fix
                                End Try                             'ASUS Fix
                            Next
                        End If
                        If RefreshDirectories Then 'something added or removed, resort
                            Me._Directories.Sort()
                        End If
                    End If
                    'we obtained some new relative PIDLs  in curPidls, so free them
                    For Each iptr In curPidls
                        Marshal.FreeCoTaskMem(iptr)
                    Next
                End If  'end of content comparison
            End If      'end of IsNothing test
        End If          'end of IsFolder test
    End Function

#End Region

#Region "   ToString"
    ''' <summary>
    ''' Returns the DisplayName as the normal ToString value
    ''' </summary>
    ''' 
    Public Overrides Function ToString() As String
        Return Me._DisplayName
    End Function
#End Region

#Region "   Debug Dumper"
    ''' <summary>
    ''' Summary of DebugDump.
    ''' </summary>
    ''' 
    Public Sub DebugDump()
        Debug.WriteLine("DisplayName = " & Me._DisplayName)
        Debug.WriteLine("PIDL        = " & Me._PIDL.ToString)
        Debug.WriteLine(vbTab & "Path        = " & Me._Path)
        Debug.WriteLine(vbTab & "TypeName    = " & Me.TypeName)
        Debug.WriteLine(vbTab & "iIconNormal = " & Me._IconIndexNormal)
        Debug.WriteLine(vbTab & "iIconSelect = " & Me._IconIndexOpen)
        Debug.WriteLine(vbTab & "IsBrowsable = " & Me._IsBrowsable)
        Debug.WriteLine(vbTab & "IsFileSystem= " & Me._IsFileSystem)
        Debug.WriteLine(vbTab & "IsFolder    = " & Me._IsFolder)
        Debug.WriteLine(vbTab & "IsLink    = " & Me._IsLink)
        Debug.WriteLine(vbTab & "IsDropTarget = " & Me._IsDropTarget)
        Debug.WriteLine(vbTab & "IsReadOnly   = " & Me.IsReadOnly)
        Debug.WriteLine(vbTab & "CanCopy = " & Me.CanCopy)
        Debug.WriteLine(vbTab & "CanLink = " & Me.CanLink)
        Debug.WriteLine(vbTab & "CanMove = " & Me.CanMove)
        Debug.WriteLine(vbTab & "CanDelete = " & Me.CanDelete)
        If Me._IsFolder Then
            If Not IsNothing(Me._Directories) Then
                Debug.WriteLine(vbTab & "Directory Count = " & Me._Directories.Count)
            Else
                Debug.WriteLine(vbTab & "Directory Count Not yet set")
            End If
        End If
    End Sub
#End Region

#End Region

#Region " Private Instance Methods"

    '''<Summary>
    ''' Returns the requested Items of this Folder as an ArrayList of Shell Items
    '''  unless the IntPtrOnly flag is set.  If IntPtrOnly is True, return an
    '''  ArrayList of PIDLs.
    '''</Summary>
    ''' <param name="flags">A set of one or more SHCONTF flags indicating which items to return</param>
    ''' <param name="intPtrOnly">True to suppress generation of Shell Items, returning only an
    '''  ArrayList of IntPtrs to RELATIVE PIDLs  for the contents of this Folder</param>
    Private Function GetContents(ByVal flags As SHCONTF, Optional ByVal intPtrOnly As Boolean = False) As ArrayList
        Dim rVal As New ArrayList()
        Dim HR As Integer
        Dim IEnum As IEnumIDList = Nothing
        'UPDATE: Always get all items. Use flags param only to indicate what the caller wants
        HR = Me._Folder.EnumObjects(0, SHCONTF.INCLUDEHIDDEN Or SHCONTF.FOLDERS Or SHCONTF.NONFOLDERS, IEnum) 'UPDATE
        If HR = NOERROR Then
            Dim item As IntPtr = IntPtr.Zero
            Dim itemCnt As Integer
            HR = IEnum.GetNext(1, item, itemCnt)
            If HR = NOERROR Then
                Do While itemCnt > 0 AndAlso Not item.Equals(IntPtr.Zero)
                    'UPDATE: Changed logic of testing for Compressed(zip, etc) files
                    ' use the flags param to see what user wanted, but always Enumerate thru all items
                    ' Vista and above really obey SHCONTF.FOLDERS and SHCONTF.NONFOLDERS, and we are
                    ' defining compressed files as files rather than Folders as XP and above do
                    Dim ItemIsFolder As Boolean
                    Dim attrFlag As SFGAO = SFGAO.FOLDER Or SFGAO.STREAM
                    Dim aPidl() As IntPtr = {item}
                    Me._Folder.GetAttributesOf(1, aPidl, attrFlag)
                    ItemIsFolder = If(_XPorAbove, CBool(attrFlag And SFGAO.FOLDER) AndAlso Not CBool(attrFlag And SFGAO.STREAM), CBool(attrFlag And SFGAO.FOLDER))
                    If ItemIsFolder And Not CBool(flags And SHCONTF.FOLDERS) Then GoTo SKIPONE 'if folders not wanted then skip this
                    If Not ItemIsFolder And Not CBool(flags And SHCONTF.NONFOLDERS) Then GoTo SKIPONE 'not a folder, if nonfolders not wanted, skip
                    'END UPDATE
                    If intPtrOnly Then   'just relative PIDLs  for fast look, no shell item overhead
                        rVal.Add(PIDLClone(item))   'caller must free
                    Else
                        Try                                     'ASUS Fix
                            rVal.Add(New ShellItem(Me._Folder, item, Me))
                        Catch ex As InvalidCastException        'ASUS Fix
                        End Try                                 'ASUS Fix
                    End If
SKIPONE:            Marshal.FreeCoTaskMem(item) 'if New kept it, it kept a copy
                    item = IntPtr.Zero
                    itemCnt = 0
                    ' Application.DoEvents()
                    Dim unused As Integer = IEnum.GetNext(1, item, itemCnt)
                Loop
            Else
                If HR <> 1 Then GoTo HRError '1 means no more
            End If
        Else : GoTo HRError
        End If
        'Normal Exit
NORMAL: If Not IsNothing(IEnum) Then
            Marshal.ReleaseComObject(IEnum)
        End If
        rVal.TrimToSize()
        Return rVal

        ' Error Exit for all Com errors
HRError:  'not ready disks will return the following error
        'If HR = &HFFFFFFFF800704C7 Then
        '    GoTo NORMAL
        'ElseIf HR = &HFFFFFFFF80070015 Then
        '    GoTo NORMAL
        '    'unavailable net resources will return these
        'ElseIf HR = &HFFFFFFFF80040E96 Or HR = &HFFFFFFFF80040E19 Then
        '    GoTo NORMAL
        'ElseIf HR = &HFFFFFFFF80004001 Then 'Certain "Not Implemented" features will return this
        '    GoTo NORMAL
        'ElseIf HR = &HFFFFFFFF80004005 Then
        '    GoTo NORMAL
        'ElseIf HR = &HFFFFFFFF800704C6 Then
        '    GoTo NORMAL
        'End If
        If Not IsNothing(IEnum) Then Marshal.ReleaseComObject(IEnum)
        '#If Debug Then
        '        Marshal.ThrowExceptionForHR(HR)
        '#End If
        rVal = New ArrayList() 'sometimes it is a non-fatal error,ignored
        GoTo NORMAL
    End Function

    ''' <Summary>
    ''' Get Size in bytes of the first (possibly only)
    '''  SHItem in an ID list.  Note: the full size of
    '''   the item is the sum of the sizes of all SHItems
    '''   in the list!!
    ''' </Summary>
    ''' <param name="pidl"></param>
    Private Shared Function ItemIDSize(ByVal pidl As IntPtr) As Integer
        If Not pidl.Equals(IntPtr.Zero) Then
            Dim b(1) As Byte
            Marshal.Copy(pidl, b, 0, 2)
            Return b(1) * 256 + b(0)
        Else
            Return 0
        End If
    End Function

    ''' <summary>
    ''' computes the actual size of the ItemIDList pointed to by pidl
    ''' </summary>
    ''' <param name="pidl">The PIDL  pointing to an ItemIDList</param>
    '''<returns> Returns actual size of the ItemIDList, less the terminating nulnul</returns> 
    Public Shared Function ItemIDListSize(ByVal pidl As IntPtr) As Integer
        If Not pidl.Equals(IntPtr.Zero) Then
            Dim i As Integer = ItemIDSize(pidl)
            Dim b As Integer = Marshal.ReadByte(pidl, i) + (Marshal.ReadByte(pidl, i + 1) * 256)
            Do While b > 0
                i += b
                b = Marshal.ReadByte(pidl, i) + (Marshal.ReadByte(pidl, i + 1) * 256)
            Loop
            Return i
        Else : Return 0
        End If
    End Function

    ''' <summary>
    ''' Counts the total number of SHItems in input pidl
    ''' </summary>
    ''' <param name="pidl">The PIDL  to obtain the count for</param>
    ''' <returns> Returns the count of SHItems pointed to by pidl</returns> 
    Public Shared Function PidlCount(ByVal pidl As IntPtr) As Integer
        If Not pidl.Equals(IntPtr.Zero) Then
            Dim cnt As Integer = 0
            Dim i As Integer = 0
            Dim b As Integer = Marshal.ReadByte(pidl, i) + (Marshal.ReadByte(pidl, i + 1) * 256)
            Do While b > 0
                cnt += 1
                i += b
                b = Marshal.ReadByte(pidl, i) + (Marshal.ReadByte(pidl, i + 1) * 256)
            Loop
            Return cnt
        Else : Return 0
        End If
    End Function

    '''<Summary>GetLastId -- returns a pointer to the last ITEMID in a valid
    ''' ITEMIDLIST. Returned pointer SHOULD NOT be released since it
    ''' points to place within the original PIDL</Summary>
    '''<returns>IntPtr pointing to last ITEMID in ITEMIDLIST structure,
    ''' Returns IntPtr.Zero if given a null pointer.
    ''' If given a pointer to the Desktop, will return same pointer.</returns>
    '''<remarks>This is what the API ILFindLastID does, however IL... 
    ''' functions are not supported before Win2K.</remarks>
    Public Shared Function GetLastID(ByVal pidl As IntPtr) As IntPtr
        If Not pidl.Equals(IntPtr.Zero) Then
            Dim prev As Integer = 0
            Dim i As Integer = 0
            Dim b As Integer = Marshal.ReadByte(pidl, i) + (Marshal.ReadByte(pidl, i + 1) * 256)
            Do While b > 0
                prev = i
                i += b
                b = Marshal.ReadByte(pidl, i) + (Marshal.ReadByte(pidl, i + 1) * 256)
            Loop
            ' Return New IntPtr(pidl.ToInt32 + prev)
            Return System.IntPtr.Add(pidl, prev)
        Else : Return IntPtr.Zero
        End If
    End Function

    ''' <summary> Decompose PIDL. </summary>
    ''' <param name="pidl"> The PIDL. </param>
    ''' <returns> An IntPtr() </returns>
    Public Shared Function DecomposePIDL(ByVal pidl As IntPtr) As IntPtr()
        Dim lim As Integer = ItemIDListSize(pidl)
        Dim PIDLs(PidlCount(pidl) - 1) As IntPtr
        Dim i As Integer = 0
        Dim curB As Integer

        Do While curB < lim
            Dim thisPtr As IntPtr = New IntPtr(pidl.ToInt32 + curB)
            Dim offSet As Integer = Marshal.ReadByte(thisPtr) + (Marshal.ReadByte(thisPtr, 1) * 256)

            PIDLs(i) = Marshal.AllocCoTaskMem(offSet + 2)
            Dim b(offSet + 1) As Byte
            Marshal.Copy(thisPtr, b, 0, offSet)
            b(offSet) = 0 : b(offSet + 1) = 0
            Marshal.Copy(b, 0, PIDLs(i), offSet + 2)
            'DumpPidl(PIDLs(i))
            curB += offSet
            i += 1
        Loop
        Return PIDLs
    End Function

    ''' <summary> PIDL clone. </summary>
    ''' <param name="pidl"> The PIDL. </param>
    ''' <returns> An IntPtr. </returns>
    Private Shared Function PIDLClone(ByVal pidl As IntPtr) As IntPtr
        Dim cb As Integer = ItemIDListSize(pidl)
        Dim b(cb + 1) As Byte
        Marshal.Copy(pidl, b, 0, cb) 'not including terminating null
        b(cb) = 0 : b(cb + 1) = 0 'force to null
        PIDLClone = Marshal.AllocCoTaskMem(cb + 2)
        Marshal.Copy(b, 0, PIDLClone, cb + 2)
    End Function

    ''' <summary> Query if 'Pidl1' is equal. </summary>
    ''' <param name="pidl1"> The first PIDL. </param>
    ''' <param name="pidl2"> The second PIDL. </param>
    ''' <returns> <c>true</c> if equal; otherwise <c>false</c> </returns>
    Public Shared Function IsEqual(ByVal pidl1 As IntPtr, ByVal pidl2 As IntPtr) As Boolean
        If _Win2KOrAbove Then
            Return UnsafeNativeMethods.ILIsEqual(pidl1, pidl2)
        Else 'do hard way, may not work for some folders on XP
            Dim cb1 As Integer, cb2 As Integer
            cb1 = ItemIDListSize(pidl1)
            cb2 = ItemIDListSize(pidl2)
            If cb1 <> cb2 Then Return False
            Dim lim32 As Integer = cb1 \ 4

            Dim i As Integer
            For i = 0 To lim32 - 1
                If Marshal.ReadInt32(pidl1, i) <> Marshal.ReadInt32(pidl2, i) Then
                    Return False
                End If
            Next
            Dim limB As Integer = cb1 Mod 4
            Dim offset As Integer = lim32 * 4
            For i = 0 To limB - 1
                If Marshal.ReadByte(pidl1, offset + i) <> Marshal.ReadByte(pidl2, offset + i) Then
                    Return False
                End If
            Next
            Return True 'made it to here, so they are equal
        End If
    End Function

    ''' <summary>
    ''' Concatenates the contents of two PIDLs into a new PIDL  (ended by 00)
    ''' allocating CoTaskMem to hold the result,
    ''' placing the concatenation (followed by 00) into the
    ''' allocated Memory,
    ''' and returning an IntPtr pointing to the allocated memory
    ''' </summary>
    ''' <param name="pidl1">IntPtr to a well formed SHItemIDList or IntPtr.Zero</param>
    ''' <param name="pidl2">IntPtr to a well formed SHItemIDList or IntPtr.Zero</param>
    ''' <returns>Returns a points to an ItemIDList containing the 
    '''   concatenation of the two (followed by the req 2 zeros
    '''   Caller must Free this PIDL  when done with it</returns> 
    Public Shared Function ConcatPidls(ByVal pidl1 As IntPtr, ByVal pidl2 As IntPtr) As IntPtr
        Dim cb1 As Integer, cb2 As Integer
        cb1 = ItemIDListSize(pidl1)
        cb2 = ItemIDListSize(pidl2)
        Dim rawCnt As Integer = cb1 + cb2
        If (rawCnt) > 0 Then
            Dim b(rawCnt + 1) As Byte
            If cb1 > 0 Then
                Marshal.Copy(pidl1, b, 0, cb1)
            End If
            If cb2 > 0 Then
                Marshal.Copy(pidl2, b, cb1, cb2)
            End If
            Dim rVal As IntPtr = Marshal.AllocCoTaskMem(cb1 + cb2 + 2)
            b(rawCnt) = 0 : b(rawCnt + 1) = 0
            Marshal.Copy(b, 0, rVal, rawCnt + 2)
            Return rVal
        Else
            Return IntPtr.Zero
        End If
    End Function

    ''' <summary>
    ''' Returns an ItemIDList with the last ItemID trimed off
    '''  This is necessary since I cannot get SHBindToParent to work 
    '''  It's purpose is to generate an ItemIDList for the Parent of a
    '''  Special Folder which can then be processed with DesktopBase.BindToObject,
    '''  yeilding a Folder for the parent of the Special Folder
    '''  It also creates and passes back a RELATIVE PIDL  for this item
    ''' </summary>
    ''' <param name="pidl">A pointer to a well formed ItemIDList. The PIDL  to trim</param>
    ''' <param name="relPidl">BYREF IntPtr which will point to a new relative pidl
    '''        containing the contents of the last ItemID in the ItemIDList
    '''        terminated by the required 2 nulls.</param>
    ''' <returns> an ItemIDList with the last element removed.
    '''  Caller must Free this item when through with it
    '''  Also returns the new relative PIDL  in the 2nd parameter
    '''   Caller must Free this PIDL  as well, when through with it
    '''</returns>
    Public Shared Function TrimPidl(ByVal pidl As IntPtr, ByRef relPidl As IntPtr) As IntPtr
        Dim cb As Integer = ItemIDListSize(pidl)
        Dim b(cb + 1) As Byte
        Marshal.Copy(pidl, b, 0, cb)
        Dim prev As Integer = 0
        Dim i As Integer = b(0) + (b(1) * 256)
        'Do While i < cb AndAlso b(i) <> 0
        Do While i > 0 AndAlso i < cb       'Changed code
            prev = i
            i += b(i) + (b(i + 1) * 256)
        Loop
        If (prev + 1) < cb Then
            'first set up the relative pidl
            b(cb) = 0
            b(cb + 1) = 0
            Dim cb1 As Integer = b(prev) + (b(prev + 1) * 256)
            relPidl = Marshal.AllocCoTaskMem(cb1 + 2)
            Marshal.Copy(b, prev, relPidl, cb1 + 2)
            b(prev) = 0 : b(prev + 1) = 0
            Dim rVal As IntPtr = Marshal.AllocCoTaskMem(prev + 2)
            Marshal.Copy(b, 0, rVal, prev + 2)
            Return rVal
        Else
            Return IntPtr.Zero
        End If
    End Function

    ''' <summary>
    ''' Dumps, to the Debug output, the contents of the memory block pointed to by
    ''' a PIDL. Depends on the internal structure of a PIDL
    ''' </summary>
    ''' <param name="pidl">The IntPtr(a PIDL) pointing to the block to dump</param>
    Public Shared Sub DumpPidl(ByVal pidl As IntPtr)
        Dim cb As Integer = ItemIDListSize(pidl)
        Debug.WriteLine("PIDL " & pidl.ToString & " contains " & cb & " bytes")
        If cb > 0 Then
            Dim b(cb + 1) As Byte
            Marshal.Copy(pidl, b, 0, cb + 1)
            Dim pidlCnt As Integer = 1
            Dim i As Integer = b(0) + (b(1) * 256)
            Dim curB As Integer = 0
            Do While i > 0
                Debug.Write("ItemID #" & pidlCnt & " Length = " & i)
                DumpHex(b, curB, curB + i - 1)
                pidlCnt += 1
                curB += i
                i = b(curB) + (b(curB + 1) * 256)
            Loop
        End If
    End Sub

    '''<Summary>Dump a portion or all of a Byte Array to Debug output</Summary>
    '''<param name = "b">A single dimension Byte Array</param>
    '''<param name = "sPos">Optional start index of area to dump (default = 0)</param>
    '''<param name = "epos">Optional last index position to dump (default = end of array)</param>
    '''<Remarks>
    '''</Remarks>
    Public Shared Sub DumpHex(ByVal b() As Byte,
                            Optional ByVal sPos As Integer = 0,
                            Optional ByVal ePos As Integer = 0)
        If ePos = 0 Then ePos = b.Length - 1
        Dim j As Integer
        Dim curB As Integer = sPos
        Dim sTmp As String
        Dim ch As Char
        Dim SBH As New StringBuilder()
        Dim SBT As New StringBuilder()
        For j = 0 To ePos - sPos
            If j Mod 16 = 0 Then
                Debug.WriteLine(SBH.ToString & SBT.ToString)
                SBH = New StringBuilder() : SBT = New StringBuilder("          ")
                SBH.Append(HexNum(j + sPos, 4) & "). ")
            End If
            sTmp = If(b(curB) < 16, "0" & Hex(b(curB)), Hex(b(curB)))
            SBH.Append(sTmp) : SBH.Append(" ")
            ch = Chr(b(curB))
            If Char.IsControl(ch) Then
                SBT.Append(".")
            Else
                SBT.Append(ch)
            End If
            curB += 1
        Next
        Dim fill As Integer = (j) Mod 16
        If fill <> 0 Then
            SBH.Append(" "c, 48 - (3 * ((j) Mod 16)))
        End If
        Debug.WriteLine(SBH.ToString & SBT.ToString)
    End Sub

    Public Shared Function HexNum(ByVal num As Integer, ByVal nrChrs As Integer) As String
        Dim h As String = Hex(num)
        Dim SB As New StringBuilder()
        Dim i As Integer
        For i = 1 To nrChrs - h.Length
            SB.Append("0")
        Next
        SB.Append(h)
        Return SB.ToString
    End Function

#End Region

End Class

'''<Summary> Pidl Wrapper class contains a Byte() representation of a PIDL  and
''' certain Methods and Properties for comparing one cPidl to another</Summary>
Public Class PidlWrapper
    Implements IEnumerable

#Region "   Private Fields"
#End Region

#Region "   Constructor"
    Public Sub New(ByVal pidl As IntPtr)
        Dim cb As Integer = ShellItem.ItemIDListSize(pidl)
        If cb > 0 Then
            ReDim Me._PidlBytes(cb + 1)
            Marshal.Copy(pidl, Me._PidlBytes, 0, cb)
            'DumpPidl(pidl)
        Else
            ReDim Me._PidlBytes(1)  'This is the DeskTop (we hope)
        End If
        'ensure nulnul
        Me._PidlBytes(Me._PidlBytes.Length - 2) = 0 : Me._PidlBytes(Me._PidlBytes.Length - 1) = 0
        Me._ItemCount = ShellItem.PidlCount(pidl)
    End Sub
#End Region

#Region "   Public Properties"

    ''' <summary> Gets the local copy of the PIDL. </summary>
    ''' <value> The PIDL bytes. </value>
    <CodeAnalysis.SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification:="<Pending>")>
    Public ReadOnly Property PidlBytes() As Byte()

    ''' <summary> Gets the length. </summary>
    ''' <value> The length. </value>
    Public ReadOnly Property Length() As Integer
        Get
            Return Me._PidlBytes.Length
        End Get
    End Property

    ''' <summary> Gets the number of items IDs in this Item ID List (PIDL). </summary>
    ''' <value> The number of items. </value>
    Public ReadOnly Property ItemCount() As Integer

    ' Dim m_OffsetToRelative As Integer 'the index of the start of the last itemID in m_bytes

#End Region

#Region "   Public Instance Methods -- ToPIDL, Decompose, and IsEqual"

    '''<Summary> Copy the contents of a byte() containing a PIDL  to
    '''  CoTaskMemory, returning an IntPtr that points to that mem block
    ''' Assumes that this cPidl is properly terminated, as all New 
    ''' cPidls are.
    ''' Caller must Free the returned IntPtr when done with mem block.
    '''</Summary>
    Public Function ToPIDL() As IntPtr
        ToPIDL = BytesToPidl(Me._PidlBytes)
    End Function

    '''<Summary>Returns an object containing a byte() for each of this cPidl's
    ''' ITEMIDs (individual PIDLS), in order such that obj(0) is
    ''' a byte() containing the bytes of the first ITEMID, etc.
    ''' Each ITEMID is properly terminated with a null
    '''</Summary>
    Public Function Decompose() As Object()
        Dim bArrays(Me.ItemCount - 1) As Object
        Dim eByte As ICPidlEnumerator = CType(Me.GetEnumerator(), ICPidlEnumerator)
        Dim i As Integer
        Do While eByte.MoveNext
            bArrays(i) = eByte.Current
            i += 1
        Loop
        Return bArrays
    End Function

    '''<Summary>Returns True if input cPidl's content exactly match the 
    ''' contents of this instance</Summary>
    Public Function IsEqual(ByVal other As PidlWrapper) As Boolean
        IsEqual = False     'assume not
        If other.Length <> Me.Length Then Exit Function
        Dim ob() As Byte = other.PidlBytes
        Dim i As Integer
        For i = 0 To Me.Length - 1  'note: we look at nulnul also
            If ob(i) <> Me._PidlBytes(i) Then Exit Function
        Next
        Return True         'all equal on fall thru
    End Function
#End Region

#Region "   Public Shared Methods"

#Region "       JoinPidlBytes"
    '''<Summary> Join two byte arrays containing PIDLS, returning a 
    ''' Byte() containing the resultant ITEMIDLIST. Both Byte() must
    ''' be properly terminated (nulnul)
    ''' Returns NOTHING if error
    ''' </Summary>
    Public Shared Function JoinPidlBytes(ByVal b1() As Byte, ByVal b2() As Byte) As Byte()
        If ShellItem.IsValidPidl(b1) And ShellItem.IsValidPidl(b2) Then
            Dim b(b1.Length + b2.Length - 3) As Byte 'allow for leaving off first nulnul
            Array.Copy(b1, b, b1.Length - 2)
            Array.Copy(b2, 0, b, b1.Length - 2, b2.Length)
            Return If(ShellItem.IsValidPidl(b), b, Nothing)
        Else
            Return Nothing
        End If
    End Function
#End Region

#Region "       BytesToPidl"
    '''<Summary> Copy the contents of a byte() containing a PIDL  to
    '''  CoTaskMemory, returning an IntPtr that points to that mem block
    ''' Caller must free the IntPtr when done with it
    '''</Summary>
    Public Shared Function BytesToPidl(ByVal b() As Byte) As IntPtr
        BytesToPidl = IntPtr.Zero       'assume failure
        If ShellItem.IsValidPidl(b) Then
            Dim bLen As Integer = b.Length
            BytesToPidl = Marshal.AllocCoTaskMem(bLen)
            If BytesToPidl.Equals(IntPtr.Zero) Then Exit Function 'another bad error
            Marshal.Copy(b, 0, BytesToPidl, bLen)
        End If
    End Function
#End Region

#Region "       StartsWith"
    '''<Summary>returns True if the beginning of pidlA matches PidlB exactly for pidlB's entire length</Summary>
    Public Shared Function StartsWith(ByVal pidlA As IntPtr, ByVal pidlB As IntPtr) As Boolean
        Return PidlWrapper.StartsWith(New PidlWrapper(pidlA), New PidlWrapper(pidlB))
    End Function

    '''<Summary>returns True if the beginning of A matches B exactly for B's entire length</Summary>
    Public Shared Function StartsWith(ByVal a As PidlWrapper, ByVal b As PidlWrapper) As Boolean
        Return a.StartsWith(b)
    End Function

    '''<Summary>Returns true if the CPidl input parameter exactly matches the
    ''' beginning of this instance of CPidl</Summary>
    Public Function StartsWith(ByVal cp As PidlWrapper) As Boolean
        Dim b() As Byte = cp.PidlBytes
        If b.Length > Me._PidlBytes.Length Then Return False 'input is longer
        Dim i As Integer
        For i = 0 To b.Length - 3 'allow for null null at end of cp.PidlBytes
            If b(i) <> Me._PidlBytes(i) Then Return False
        Next
        Return True
    End Function
#End Region

#End Region

#Region "   GetEnumerator"
    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        Return New ICPidlEnumerator(Me._PidlBytes)
    End Function
#End Region

#Region "   PIDL  enumerator Class"
    Private Class ICPidlEnumerator
        Implements IEnumerator

        Private _SPos As Integer   'the first index in the current PIDL
        Private _EPos As Integer   'the last index in the current PIDL
        Private ReadOnly _Bytes() As Byte   'the local copy of the PIDL
        Public Property NotEmpty As Boolean = False 'the desktop PIDL  is zero length


        Public Sub New(ByVal b() As Byte)
            Me._Bytes = b
            If b.Length > 0 Then Me._NotEmpty = True
            Me._SPos = -1 : Me._EPos = -1
        End Sub

        Public ReadOnly Property Current() As Object Implements System.Collections.IEnumerator.Current
            Get
                If Me._SPos < 0 Then Throw New InvalidOperationException("ICPidlEnumerator --- attempt to get Current with invalidated list")
                Dim b((Me._EPos - Me._SPos) + 2) As Byte    'room for nulnul
                Array.Copy(Me._Bytes, Me._SPos, b, 0, b.Length - 2)
                b(b.Length - 2) = 0 : b(b.Length - 1) = 0 'add nulnul
                Return b
            End Get
        End Property

        Public Function MoveNext() As Boolean Implements System.Collections.IEnumerator.MoveNext
            If Me._NotEmpty Then
                If Me._SPos < 0 Then
                    Me._SPos = 0 : Me._EPos = -1
                Else
                    Me._SPos = Me._EPos + 1
                End If
                If Me._Bytes.Length < Me._SPos + 1 Then Throw New InvalidCastException("Malformed PIDL")
                Dim cb As Integer = Me._Bytes(Me._SPos) + Me._Bytes(Me._SPos + 1) * 256
                If cb = 0 Then
                    Return False 'have passed all back
                Else
                    Me._EPos += cb
                End If
            Else
                Me._SPos = 0 : Me._EPos = 0
                Return False        'in this case, we have exhausted the list of 0 ITEMIDs
            End If
            Return True
        End Function

        Public Sub Reset() Implements System.Collections.IEnumerator.Reset
            Me._SPos = -1 : Me._EPos = -1
        End Sub
    End Class
#End Region

End Class

