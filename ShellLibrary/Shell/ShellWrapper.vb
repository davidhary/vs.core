Imports System.Diagnostics.CodeAnalysis
Imports System.Runtime.InteropServices
Imports System.Text

''' <summary> Access the shell dynamic link library. </summary>
''' <remarks> (c) 2012 James D. Parsells. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2019-07-06, 3.0.*. </para></remarks>
Public NotInheritable Class ShellWrapper

#Region " Shell Constants"
    Public Const MAX_PATH As Integer = 260
    Public Const FILE_ATTRIBUTE_NORMAL As Integer = &H80
    Public Const FILE_ATTRIBUTE_DIRECTORY As Integer = &H10
    Public Const NOERROR As Integer = 0
    Public Const OkayValue As Integer = 0
    Public Const FalseValue As Integer = 1
#End Region

#Region " Shell GUIDs"
    Public Shared IID_IMalloc As New Guid("{00000002-0000-0000-C000-000000000046}")

    Public Shared IID_IShellFolder As New Guid("{000214E6-0000-0000-C000-000000000046}")
    Public Shared IID_IFolderFilterSite As New Guid("{C0A651F5-B48B-11d2-B5ED-006097C686F6}")
    Public Shared IID_IFolderFilter As New Guid("{9CC22886-DC8E-11d2-B1D0-00C04F8EEB3E}")
    Public Shared DesktopGUID As New Guid("{00021400-0000-0000-C000-000000000046}")
    Public Shared CLSID_ShellLink As New Guid("{00021401-0000-0000-C000-000000000046}")
    Public Shared CLSID_InternetShortcut As New Guid("{FBF23B40-E3F0-101B-8488-00AA003E56F8}")
    Public Shared IID_IDropTarget As New Guid("{00000122-0000-0000-C000-000000000046}")
    Public Shared IID_IDataObject As New Guid("{0000010e-0000-0000-C000-000000000046}")

    Private Shared _Shfitmp As SHFILEINFO   'just used for the following
    Public Shared FileInfoSize As Integer = Marshal.SizeOf(_Shfitmp.GetType())


#End Region

#Region "   SHGetFileInfo"

    Public Shared Function GetImageListHandlers() As (SmallImageListHandle As IntPtr, LargeImageListHandle As IntPtr)

        Dim dwFlag As Integer = SHGFI.USEFILEATTRIBUTES Or SHGFI.SYSICONINDEX Or SHGFI.SMALLICON
        Dim shfi As New SHFILEINFO()
        Dim smallImageListHandle As IntPtr = UnsafeNativeMethods.SHGetFileInfo(".txt", FILE_ATTRIBUTE_NORMAL, shfi, FileInfoSize, dwFlag)
        Debug.Assert((Not smallImageListHandle.Equals(IntPtr.Zero)), "Failed to create Image Small ImageList")
        If smallImageListHandle.Equals(IntPtr.Zero) Then Throw New Exception("Failed to create Small ImageList")

        dwFlag = SHGFI.USEFILEATTRIBUTES Or SHGFI.SYSICONINDEX Or SHGFI.LARGEICON
        Dim largeImageListHandle As IntPtr = UnsafeNativeMethods.SHGetFileInfo(".txt", FILE_ATTRIBUTE_NORMAL, shfi, FileInfoSize, dwFlag)
        Debug.Assert((Not largeImageListHandle.Equals(IntPtr.Zero)), "Failed to create Image Large ImageList")
        If largeImageListHandle.Equals(IntPtr.Zero) Then Throw New Exception("Failed to create Large ImageList")
        Return (smallImageListHandle, largeImageListHandle)
    End Function

    Public Shared Function ReplaceIcons(ByVal mutex As Threading.Mutex, pidl As IntPtr, cbFileInfo As Integer,
                                   ByVal smallImageListHandle As IntPtr, ByVal largeImageListHandle As IntPtr,
                                   dwflag As Integer, dwAttr As Integer) As (SmallIconIndex As Integer, LargeIconIndex As Integer)

        'This is the tricky part -- add overlaid Icon to system image list
        '  use of SmallImageList from Calum McLellan
        Dim shfi As New SHFILEINFO
        Dim shfi_small As New SHFILEINFO
        Dim HR As IntPtr
        Dim HR_SMALL As IntPtr
        HR = UnsafeNativeMethods.SHGetFileInfo(pidl, dwAttr, shfi, cbFileInfo, dwflag)
        HR_SMALL = UnsafeNativeMethods.SHGetFileInfo(pidl, dwAttr, shfi_small, cbFileInfo, dwflag Or SHGFI.SMALLICON)
        mutex.WaitOne()
        Dim smallIconIndex As Integer = UnsafeNativeMethods.ImageList_ReplaceIcon(smallImageListHandle, -1, shfi_small.hIcon)
        Debug.Assert(smallIconIndex > -1, "Failed to add overlaid small icon")
        Dim largeIconIndex As Integer = UnsafeNativeMethods.ImageList_ReplaceIcon(largeImageListHandle, -1, shfi.hIcon)
        Debug.Assert(largeIconIndex > -1, "Failed to add overlaid large icon")
        Debug.Assert(smallIconIndex = largeIconIndex, "Small & Large IconIndices are Different")
        mutex.ReleaseMutex()
        UnsafeNativeMethods.DestroyIcon(shfi.hIcon)
        UnsafeNativeMethods.DestroyIcon(shfi_small.hIcon)
        Return (smallIconIndex, largeIconIndex)
    End Function

#End Region

#Region " Public Shared Methods"

#Region "   GetSpecialFolderPath"
    Public Shared Function GetSpecialFolderPath(ByVal hWnd As IntPtr, ByVal csidl As Integer) As String
        Dim res As IntPtr
        Dim ppidl As IntPtr
        ppidl = GetSpecialFolderLocation(hWnd, csidl)
        Dim shfi As New SHFILEINFO()
        Dim uFlags As Integer = SHGFI.PIDL Or SHGFI.DISPLAYNAME Or SHGFI.TYPENAME
        'uFlags = uFlags Or SHGFI.SYSICONINDEX
        Dim dwAttr As Integer = 0
        res = UnsafeNativeMethods.SHGetFileInfo(ppidl, dwAttr, shfi, FileInfoSize, uFlags)
        Marshal.FreeCoTaskMem(ppidl)
        Return shfi.szDisplayName & "  (" & shfi.szTypeName & ")"
    End Function
#End Region

#Region "   GetSpecialFolderLocation"
    Public Shared Function GetSpecialFolderLocation(ByVal hWnd As IntPtr, ByVal csidl As Integer) As IntPtr
        Dim rVal As IntPtr
        Dim res As Integer = UnsafeNativeMethods.SHGetSpecialFolderLocation(hWnd, csidl, rVal)
        Return rVal
    End Function
#End Region

#Region "   IsXpOrAbove and Is2KOrAbove"
    Public Shared Function IsXpOrAbove() As Boolean
        Dim rVal As Boolean = False
        If Environment.OSVersion.Version.Major > 5 Then
            rVal = True
        ElseIf Environment.OSVersion.Version.Major = 5 AndAlso
               Environment.OSVersion.Version.Minor >= 1 Then
            rVal = True
        End If
        'if none of the above tests succeed, then return false
        Return rVal
    End Function
    Public Shared Function Is2KOrAbove() As Boolean
        Return Environment.OSVersion.Version.Major >= 5
    End Function
#End Region

#End Region

End Class

#Region "       Com Interop for IDropTarget"
'    MIDL_INTERFACE("00000122-0000-0000-C000-000000000046")
'IDropTarget : public IUnknown
'{
'public:
'    virtual HRESULT STDMETHODCALLTYPE DragEnter( 
'        /* [unique][in] */ IDataObject *pDataObj,
'        /* [in] */ DWORD grfKeyState,
'        /* [in] */ POINTL pt,
'        /* [out][in] */ DWORD *pdwEffect) = 0;

'    virtual HRESULT STDMETHODCALLTYPE DragOver( 
'        /* [in] */ DWORD grfKeyState,
'        /* [in] */ POINTL pt,
'        /* [out][in] */ DWORD *pdwEffect) = 0;

'    virtual HRESULT STDMETHODCALLTYPE DragLeave( void) = 0;

'    virtual HRESULT STDMETHODCALLTYPE Drop( 
'        /* [unique][in] */ IDataObject *pDataObj,
'        /* [in] */ DWORD grfKeyState,
'        /* [in] */ POINTL pt,
'        /* [out][in] */ DWORD *pdwEffect) = 0;

<ComImportAttribute(), InterfaceType(ComInterfaceType.InterfaceIsIUnknown), Guid("00000122-0000-0000-C000-000000000046")>
Public Interface IDropTarget
    <PreserveSig()>
    Function DragEnter(
                ByVal pDataObj As IntPtr,
                ByVal grfKeyState As Integer,
                ByVal pt As Point,
                ByRef pdwEffect As Integer) _
                As Integer

    <PreserveSig()>
    Function DragOver(
                ByVal grfKeyState As Integer,
                ByVal pt As Point,
                ByRef pdwEffect As Integer) _
                As Integer

    <PreserveSig()>
    Function DragLeave() As Integer

    <PreserveSig()>
    Function DragDrop(
                ByVal pDataObj As IntPtr,
                ByVal grfKeyState As Integer,
                ByVal pt As Point,
                ByRef pdwEffect As Integer) As Integer
End Interface
#End Region

#Region "   COM Interop for IShellFolder"

<ComImportAttribute(), InterfaceTypeAttribute(ComInterfaceType.InterfaceIsIUnknown), Guid("000214E6-0000-0000-C000-000000000046")>
Public Interface IShellFolder
    <PreserveSig()>
    Function ParseDisplayName(
            ByVal hwndOwner As Integer,
            ByVal pbcReserved As IntPtr,
            <MarshalAs(UnmanagedType.LPWStr)>
            ByVal lpszDisplayName As String,
            ByRef pchEaten As Integer,
            ByRef ppidl As IntPtr,
            ByRef pdwAttributes As Integer) As Integer

    <PreserveSig()>
    Function EnumObjects(
            ByVal hwndOwner As Integer,
            <MarshalAs(UnmanagedType.U4)> ByVal _
            grfFlags As SHCONTF,
            ByRef ppenumIDList As IEnumIDList) As Integer

#Disable Warning IDE1006 ' Naming Styles
    <PreserveSig()>
    Function BindToObject(
            ByVal PIDL As IntPtr,
            ByVal pbcReserved As IntPtr,
            ByRef riid As Guid,
            ByRef ppvOut As IShellFolder) As Integer
#Enable Warning IDE1006 ' Naming Styles

#Disable Warning IDE1006 ' Naming Styles
    <PreserveSig()>
    Function BindToStorage(
            ByVal PIDL As IntPtr,
            ByVal pbcReserved As IntPtr,
            ByRef riid As Guid,
            ByVal ppvObj As IntPtr) As Integer
#Enable Warning IDE1006 ' Naming Styles

    <PreserveSig()>
    Function CompareIDs(
            ByVal lParam As IntPtr,
            ByVal pidl1 As IntPtr,
            ByVal pidl2 As IntPtr) As Integer

    <PreserveSig()>
    Function CreateViewObject(
            ByVal hwndOwner As IntPtr,
            ByRef riid As Guid,
            ByRef ppvOut As IntPtr) As Integer ' Update 2011-01-28
    '   ByRef ppvOut As IUnknown) As Integer 'Old Version

    <PreserveSig()>
    Function GetAttributesOf(
            ByVal cidl As Integer,
            <MarshalAs(UnmanagedType.LPArray, SizeParamIndex:=0)>
            ByVal apidl() As IntPtr,
            ByRef rgfInOut As SFGAO) As Integer

    <PreserveSig()>
    Function GetUIObjectOf(
            ByVal hwndOwner As IntPtr,
            ByVal cidl As Integer,
            <MarshalAs(UnmanagedType.LPArray, SizeParamIndex:=0)>
            ByVal apidl() As IntPtr,
            ByRef riid As Guid,
            ByRef prgfInOut As Integer,
            ByRef ppvOut As isr.Core.Shell.IUnknown) As Integer
    'ByRef ppvOut As IDropTarget) As Integer

#Disable Warning IDE1006 ' Naming Styles
    <PreserveSig()>
    Function GetDisplayNameOf(
            ByVal PIDL As IntPtr,
            <MarshalAs(UnmanagedType.U4)>
            ByVal uFlags As SHGDN,
            ByVal lpName As IntPtr) As Integer
#Enable Warning IDE1006 ' Naming Styles

#Disable Warning IDE1006 ' Naming Styles
    <PreserveSig()>
    Function SetNameOf(
            ByVal hwndOwner As Integer,
            ByVal PIDL As IntPtr,
            <MarshalAs(UnmanagedType.LPWStr)> ByVal _
            lpszName As String,
            <MarshalAs(UnmanagedType.U4)> ByVal _
            uFlags As SHCONTF,
            ByRef ppidlOut As IntPtr) As Integer
#Enable Warning IDE1006 ' Naming Styles
End Interface
#End Region

#Region "   Com Interop for IEnumIDList"

<ComImportAttribute(), InterfaceTypeAttribute(ComInterfaceType.InterfaceIsIUnknown), Guid("000214F2-0000-0000-C000-000000000046")>
Public Interface IEnumIDList
    <PreserveSig()>
    Function GetNext(
            ByVal celt As Integer,
            ByRef rgelt As IntPtr,
            ByRef pceltFetched As Integer) As Integer

    <PreserveSig()>
    Function Skip(
            ByVal celt As Integer) As Integer

    <PreserveSig()>
    Function Reset() As Integer

    <PreserveSig()>
    Function Clone(
            ByRef ppenum As IEnumIDList) As Integer
End Interface

#End Region

#Region "   Com Interop for IPersistFile"

<ComImportAttribute(), InterfaceTypeAttribute(ComInterfaceType.InterfaceIsIUnknown), Guid("0000010B-0000-0000-C000-000000000046")>
Public Interface IPersistFile

    'Inherited from I Persist
    Sub GetClassID(
          <Out()> ByRef pClassID As Guid)

    'IPersistFile Interfaces
    <PreserveSig()>
    Function IsDirty() As Integer

    Function Load(
          <MarshalAs(UnmanagedType.LPWStr)> ByVal pszFileName As String,
          ByVal dwMode As Integer) As Integer

    Function Save(
          <MarshalAs(UnmanagedType.LPWStr)> ByVal pszFileName As String,
          <MarshalAs(UnmanagedType.Bool)> ByVal fRemember As Boolean) As Integer

    Function SaveCompleted(
          <MarshalAs(UnmanagedType.LPWStr)> ByVal pszFileName As String) As Integer

    Function GetCurFile(
          <Out(), MarshalAs(UnmanagedType.LPWStr)> ByRef ppszFileName As String) As Integer
End Interface
#End Region

#Region "   Com Interop for IShellLink"
'We define the Ansi version since all Win OSs (95 thru XP) support it

<ComImportAttribute(), InterfaceType(ComInterfaceType.InterfaceIsIUnknown), Guid("000214EE-0000-0000-C000-000000000046")>
Friend Interface IShellLink

    Function GetPath(
          <Out(), MarshalAs(UnmanagedType.LPStr)> ByVal pszFile As StringBuilder,
          ByVal cchMaxPath As Integer,
          <Out()> ByRef pfd As WIN32_FIND_DATA,
          ByVal fFlags As SLGP) As Integer

    Function GetIDList(
          ByRef ppidl As IntPtr) As Integer

#Disable Warning IDE1006 ' Naming Styles
    Function SetIDList(
          ByVal PIDL As IntPtr) As Integer
#Enable Warning IDE1006 ' Naming Styles

    Function GetDescription(
          <Out(), MarshalAs(UnmanagedType.LPStr)> ByVal pszName As StringBuilder,
          ByVal cchMaxName As Integer) As Integer

    Function SetDescription(
          <MarshalAs(UnmanagedType.LPStr)> ByVal pszName As String) As Integer

    Function GetWorkingDirectory(
          <Out(), MarshalAs(UnmanagedType.LPStr)> ByVal pszDir As StringBuilder,
          ByVal cchMaxPath As Integer) As Integer

    Function SetWorkingDirectory(
          <MarshalAs(UnmanagedType.LPStr)> ByVal pszDir As String) As Integer

    Function GetArguments(
          <Out(), MarshalAs(UnmanagedType.LPStr)> ByVal pszArgs As StringBuilder,
          ByVal cchMaxPath As Integer) As Integer

    Function SetArguments(
          <MarshalAs(UnmanagedType.LPStr)> ByVal pszArgs As String) As Integer

    Function GetHotkey(
          ByRef pwHotkey As Short) As Integer

    Function SetHotkey(
          ByVal wHotkey As Short) As Integer

    Function GetShowCmd(
          ByRef piShowCmd As Integer) As Integer

    Function SetShowCmd(
          ByVal iShowCmd As Integer) As Integer

    Function GetIconLocation(
          <Out(), MarshalAs(UnmanagedType.LPStr)>
          ByVal pszIconPath As StringBuilder,
          ByVal cchIconPath As Integer,
          ByRef piIcon As Integer) As Integer

    Function SetIconLocation(
          <MarshalAs(UnmanagedType.LPStr)>
          ByVal pszIconPath As String,
          ByVal iIcon As Integer) As Integer

    Function SetRelativePath(
          <MarshalAs(UnmanagedType.LPStr)>
          ByVal pszPathRel As String,
          ByVal dwReserved As Integer) As Integer

    Function Resolve(
          ByVal hwnd As IntPtr,
          ByVal fFlags As SLR) As Integer

    Function SetPath(
          <MarshalAs(UnmanagedType.LPStr)>
          ByVal pszFile As String) As Integer
End Interface
#End Region

#Region " Shell Enumerations"

#Region " SFGAO"
Public Enum SFGAO
    CANCOPY = &H1                    ' Objects can be copied    
    CANMOVE = &H2                    ' Objects can be moved     
    CANLINK = &H4                    ' Objects can be linked    
    STORAGE = &H8                    ' supports BindToObject(IID_IStorage)
    CANRENAME = &H10                 ' Objects can be renamed
    CANDELETE = &H20                 ' Objects can be deleted
    HASPROPSHEET = &H40              ' Objects have property sheets
    DROPTARGET = &H100               ' Objects are drop target
    CAPABILITYMASK = &H177           ' This flag is a mask for the capability flags.
    ENCRYPTED = &H2000               ' object is encrypted (use alt color)
    ISSLOW = &H4000                  ' 'slow' object
    GHOSTED = &H8000                 ' ghosted icon
    LINK = &H10000                   ' Shortcut (link)
    SHARE = &H20000                  ' shared
    RDONLY = &H40000               ' read-only
    HIDDEN = &H80000                 ' hidden object
    DISPLAYATTRMASK = &HFC000        ' This flag is a mask for the display attributes.
    FILESYSANCESTOR = &H10000000     ' may contain children with FILESYSTEM
    FOLDER = &H20000000              ' support BindToObject(IID_IShellFolder)
    FILESYSTEM = &H40000000          ' is a win32 file system object (file/folder/root)
    HASSUBFOLDER = &H80000000        ' may contain children with FOLDER
    CONTENTSMASK = &H80000000        ' This flag is a mask for the contents attributes.
    VALIDATE = &H1000000             ' invalidate cached information
    REMOVABLE = &H2000000            ' is this removable media?
    COMPRESSED = &H4000000           ' Object is compressed (use alt color)
    BROWSABLE = &H8000000            ' supports IShellFolder but only implements CreateViewObject() (non-folder view)
    NONENUMERATED = &H100000         ' is a non-enumerated object
    NEWCONTENT = &H200000            ' should show bold in explorer tree
    CANMONIKER = &H400000            ' defunct
    HASSTORAGE = &H400000            ' defunct
    STREAM = &H400000                ' supports BindToObject(IID_IStream)
    STORAGEANCESTOR = &H800000       ' may contain children with STORAGE or STREAM
    STORAGECAPMASK = &H70C50008      ' for determining storage capabilities ie for open/save semantics
End Enum
#End Region

#Region " SHGFI"
<Flags()>
Public Enum SHGFI
    ICON = &H100                ' get icon 
    DISPLAYNAME = &H200         ' get display name 
    TYPENAME = &H400            ' get type name 
    ATTRIBUTES = &H800          ' get attributes 
    ICONLOCATION = &H1000       ' get icon location 
    EXETYPE = &H2000            ' return exe type 
    SYSICONINDEX = &H4000       ' get system icon index 
    LINKOVERLAY = &H8000        ' put a link overlay on icon 
    SELECTED = &H10000          ' show icon in selected state 
    ATTR_SPECIFIED = &H20000    ' get only specified attributes 
    LARGEICON = &H0             ' get large icon 
    SMALLICON = &H1             ' get small icon 
    OPENICON = &H2              ' get open icon 
    SHELLICONSIZE = &H4         ' get shell size icon 
    PIDL = &H8                  ' pszPath is a PIDL  
    USEFILEATTRIBUTES = &H10    ' use passed dwFileAttribute 
    ADDOVERLAYS = &H20          ' apply the appropriate overlays
    OVERLAYINDEX = &H40         ' Get the index of the overlay
End Enum
#End Region

#Region " CSIDL"
Public Enum CSIDL As Integer
    DESKTOP = &H0
    INTERNET = &H1
    PROGRAMS = &H2
    CONTROLS = &H3
    PRINTERS = &H4
    PERSONAL = &H5
    FAVORITES = &H6
    STARTUP = &H7
    RECENT = &H8
    SENDTO = &H9
    BITBUCKET = &HA
    STARTMENU = &HB
    MYDOCUMENTS = &HC
    MYMUSIC = &HD
    MYVIDEO = &HE
    DESKTOPDIRECTORY = &H10
    DRIVES = &H11
    NETWORK = &H12
    NETHOOD = &H13
    FONTS = &H14
    TEMPLATES = &H15
    COMMON_STARTMENU = &H16
    COMMON_PROGRAMS = &H17
    COMMON_STARTUP = &H18
    COMMON_DESKTOPDIRECTORY = &H19
    APPDATA = &H1A
    PRINTHOOD = &H1B
    LOCAL_APPDATA = &H1C
    ALTSTARTUP = &H1D
    COMMON_ALTSTARTUP = &H1E
    COMMON_FAVORITES = &H1F
    INTERNET_CACHE = &H20
    COOKIES = &H21
    HISTORY = &H22
    COMMON_APPDATA = &H23
    WINDOWS = &H24
    SYSTEM = &H25
    PROGRAM_FILES = &H26
    MYPICTURES = &H27
    PROFILE = &H28
    SYSTEMX86 = &H29
    PROGRAM_FILESX86 = &H2A
    PROGRAM_FILES_COMMON = &H2B
    PROGRAM_FILES_COMMONX86 = &H2C
    COMMON_TEMPLATES = &H2D
    COMMON_DOCUMENTS = &H2E
    COMMON_ADMINTOOLS = &H2F
    ADMINTOOLS = &H30
    CONNECTIONS = &H31
    COMMON_MUSIC = &H35
    COMMON_PICTURES = &H36
    COMMON_VIDEO = &H37
    RESOURCES = &H38
    RESOURCES_LOCALIZED = &H39
    COMMON_OEM_LINKS = &H3A
    CDBURN_AREA = &H3B
    COMPUTERSNEARME = &H3D
    FLAG_PER_USER_INIT = &H800
    FLAG_NO_ALIAS = &H1000
    FLAG_DONT_VERIFY = &H4000
    FLAG_CREATE = &H8000
    FLAG_MASK = &HFF00
End Enum
#End Region

<CodeAnalysis.SuppressMessage("Design", "CA1027:Mark enums with FlagsAttribute", Justification:="Enum with Zero value must not be flagged")>
Friend Enum E_STRRET : int
    WSTR = &H0          ' Use STRRET.pOleStr
    OFFSET = &H1        ' Use STRRET.uOffset to Ansi
    C_STR = &H2         ' Use STRRET.cStr
End Enum
#End Region

<CodeAnalysis.SuppressMessage("Design", "CA1027:Mark enums with FlagsAttribute", Justification:="Enum with Zero value must not be flagged")>
Public Enum SHCONTF
    EMPTY = 0                      ' used to zero a SHCONTF variable
    FOLDERS = &H20                 ' only want folders enumerated (FOLDER)
    NONFOLDERS = &H40              ' include non folders
    INCLUDEHIDDEN = &H80           ' show items normally hidden
    INIT_ON_FIRST_NEXT = &H100     ' allow EnumObject() to return before validating enum
    NETPRINTERSRCH = &H200         ' hint that client is looking for printers
    SHAREABLE = &H400              ' hint that client is looking sharable resources (remote shares)
    STORAGE = &H800                ' include all items with accessible storage and their ancestors
End Enum

<CodeAnalysis.SuppressMessage("Design", "CA1027:Mark enums with FlagsAttribute", Justification:="Enum with Zero value must not be flagged")>
Public Enum SHGDN
    NORMAL = 0
    INFOLDER = 1
    FORADDRESSBAR = 16384
    FORPARSING = 32768
End Enum

#Region " ILD --- Flags controlling how the Image List item is drawn"
'/// <summary>
'/// Flags controlling how the Image List item is 
'/// drawn
'/// </summary>
'[Flags]	
'   Public Enum ImageListDrawItemConstants : int
'{
'	/// <summary>
'	/// Draw item normally.
'	/// </summary>
'	ILD_NORMAL = 0x0,
'	/// <summary>
'	/// Draw item transparently.
'	/// </summary>
'	ILD_TRANSPARENT = 0x1,
'	/// <summary>
'	/// Draw item blended with 25% of the specified foreground colour
'	/// or the Highlight colour if no foreground colour specified.
'	/// </summary>
'	ILD_BLEND25 = 0x2,
'	/// <summary>
'	/// Draw item blended with 50% of the specified foreground colour
'	/// or the Highlight colour if no foreground colour specified.
'	/// </summary>
'	ILD_SELECTED = 0x4,
'	/// <summary>
'	/// Draw the icon's mask
'	/// </summary>
'	ILD_MASK = 0x10,
'	/// <summary>
'	/// Draw the icon image without using the mask
'	/// </summary>
'	ILD_IMAGE = 0x20,
'	/// <summary>
'	/// Draw the icon using the ROP specified.
'	/// </summary>
'	ILD_ROP = 0x40,
'	/// <summary>
'	/// Preserves the alpha channel in dest. XP only.
'	/// </summary>
'	ILD_PRESERVEALPHA = 0x1000,
'	/// <summary>
'	/// Scale the image to cx, cy instead of clipping it.  XP only.
'	/// </summary>
'	ILD_SCALE = 0x2000,
'	/// <summary>
'	/// Scale the image to the current DPI of the display. XP only.
'	/// </summary>
'	ILD_DPISCALE = 0x4000
'/// <summary>
'/// Flags controlling how the Image List item is 
'/// drawn
'/// </summary>
<CodeAnalysis.SuppressMessage("Design", "CA1027:Mark enums with FlagsAttribute", Justification:="Enum with Zero value must not be flagged")>
Public Enum ILD
    NORMAL = &H0
    TRANSPARENT = &H1
    BLEND25 = &H2
    SELECTED = &H4
    MASK = &H10
    IMAGE = &H20
    ROP = &H40
    PRESERVEALPHA = &H1000
    SCALE = &H2000
    DPISCALE = &H4000
End Enum
#End Region

#Region " ILS --- XP ImageList Draw State options"

<CodeAnalysis.SuppressMessage("Design", "CA1027:Mark enums with FlagsAttribute", Justification:="Enum with Zero value must not be flagged")>
Public Enum ILS
    ''' <summary>
    ''' The image state is not modified.
    ''' </summary>
    NORMAL = (&H0)
    ''' <summary>
    ''' The color for the glow effect is passed to the IImageList::Draw method in the crEffect member of IMAGELISTDRAWPARAMS.  
    ''' </summary>
    GLOW = (&H1)
    ''' <summary>
    ''' The color for the drop shadow effect is passed to the IImageList::Draw method in the crEffect member of IMAGELISTDRAWPARAMS.
    ''' </summary>
    SHADOW = (&H2)
    ''' <summary>
    ''' The amount to increase is indicated by the frame member in the IMAGELISTDRAWPARAMS method.
    ''' </summary>
    SATURATE = (&H4)
    ''' <summary>
    ''' The value of the alpha channel is indicated by the frame member in the IMAGELISTDRAWPARAMS method. 
    ''' The alpha channel can be from 0 to 255 with 0 being completely transparent and 255 being completely opaque.
    ''' </summary>
    ALPHA = (&H8)
End Enum
#End Region

#Region " SLR --- IShellLink.Resolve Flags"
<Flags()>
Friend Enum SLR
    NO_UI = &H1
    ANY_MATCH = &H2
    UPDATE = &H4
    NOUPDATE = &H8
    NOSEARCH = &H10
    NOTRACK = &H20
    NOLINKINFO = &H40
    INVOKE_MSI = &H80
    NO_UI_WITH_MSG_PUMP = &H101
End Enum
#End Region

#Region " SLGP --- IShellLink.GetPath Flags"
<Flags()>
Public Enum SLGP
    SHORTPATH = &H1
    UNCPRIORITY = &H2
    RAWPATH = &H4
End Enum
#End Region

#Region " SHGNLI -- SHGetNewLinkInfo flags"
<Flags()>
Public Enum SHGNLI
    PIDL = 1        'pszLinkTo is a pidl
    PREFIXNAME = 2  'Make name "Shortcut to xxx"
    NOUNIQUE = 4    'don't do the unique name generation
    NOLNK = 8       'don't add ".lnk" extension (Win2k or higher,IE5 or higher)
End Enum

#End Region

#Region " Shell Structures"

#Region "   SHFILEINFO"
'///<Summary>
' SHFILEINFO structure for VB.Net
'///</Summary>
<StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Auto)>
<CodeAnalysis.SuppressMessage("Performance", "CA1815:Override equals and operator equals on value types", Justification:="<Pending>")>
Friend Structure SHFILEINFO
#Disable Warning IDE1006 ' Naming Styles
    <CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2111:PointersShouldNotBeVisible")>
    Public hIcon As IntPtr
    Public iIcon As Integer
    Public dwAttributes As Integer
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=ShellWrapper.MAX_PATH)>
    Public szDisplayName As String
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=80)>
    Public szTypeName As String
#Enable Warning IDE1006 ' Naming Styles
End Structure

#End Region

#Region "   STRRET Structures"
'both of these formats work in main thread, neither in worker thread
'<StructLayout(LayoutKind.Sequential)> _
'Public Structure STRRET
'    Public uType As Integer
'    Public pOle As IntPtr
'End Structure
<StructLayout(LayoutKind.Explicit)>
<CodeAnalysis.SuppressMessage("Performance", "CA1815:Override equals and operator equals on value types", Justification:="<Pending>")>
Friend Structure STRRET
#Disable Warning IDE1006 ' Naming Styles
    <FieldOffset(0)>
    Public uType As Integer   ' One of the STRRET_* values
    <FieldOffset(4)>
    Public pOleStr As Integer ' must be freed by caller of GetDisplayNameOf
    <FieldOffset(4)>
    Public uOffset As Integer ' Offset into SHITEMID
    <FieldOffset(4)>
    Public pStr As Integer    ' NOT USED
#Enable Warning IDE1006 ' Naming Styles
End Structure
#End Region

#Region "   W32_FIND_DATA"

<StructLayoutAttribute(LayoutKind.Sequential, CharSet:=CharSet.Auto)>
<CodeAnalysis.SuppressMessage("Performance", "CA1815:Override equals and operator equals on value types", Justification:="<Pending>")>
Friend Structure WIN32_FIND_DATA
#Disable Warning IDE1006 ' Naming Styles
    Public dwFileAttributes As Integer
    Public ftCreationTime As ComTypes.FILETIME      'update to remove warning messages 2011-02-02
    Public ftLastAccessTime As ComTypes.FILETIME    'update to remove warning messages 2011-02-02
    Public ftLastWriteTime As ComTypes.FILETIME     'update to remove warning messages 2011-02-02
    Public nFileSizeHigh As Integer
    Public nFileSizeLow As Integer
    Public dwReserved0 As Integer
    Public dwReserved1 As Integer
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=ShellWrapper.MAX_PATH)>
    Public cFileName As String
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=14)>
    Public cAlternateFileName As String
#Enable Warning IDE1006 ' Naming Styles
End Structure

#End Region
#End Region

#Region " Shell Interfaces"

#Region "   Com Interop for IUnknown"
'Not needed in .Net - use Marshal Class
<ComImport(), Guid("00000000-0000-0000-C000-000000000046"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)>
Public Interface IUnknown
    <PreserveSig()>
    Function QueryInterface(ByRef riid As Guid, ByRef pVoid As IntPtr) As Integer
    <PreserveSig()>
    Function AddRef() As Integer
    <PreserveSig()>
    Function Release() As Integer
End Interface
#End Region

#Region "   Com Interop for IMalloc"
'Not needed in .Net - use Marshal Class
<ComImport(), Guid("00000002-0000-0000-C000-000000000046"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)>
Friend Interface IMalloc
    ' Allocates a block of memory.
    ' Return value: a pointer to the allocated memory block.
    <PreserveSig()>
    Function Alloc(
                    ByVal cb As Integer) As IntPtr ' Size, in bytes, of the memory block to be allocated.

    ' Changes the size of a previously allocated memory block.
    ' Return value:  Reallocated memory block 
    <PreserveSig()>
    Function Realloc(ByVal pv As IntPtr,
                             ByVal cb As Integer) As IntPtr

    ' Frees a previously allocated block of memory.
    <PreserveSig()>
    Sub Free(ByVal pv As IntPtr) ' Pointer to the memory block to be freed.

    ' This method returns the size (in bytes) of a memory block previously allocated with 
    ' IMalloc::Alloc or IMalloc::Realloc.
    ' Return value: The size of the allocated memory block in bytes 
    <PreserveSig()>
    Function GetSize(ByVal pv As IntPtr) As Integer ' Pointer to the memory block for which the size is requested.

    ' This method determines whether this allocator was used to allocate the specified block of memory.
    ' Return value: 1 - allocated 0 - not allocated by this IMalloc instance. 
    <PreserveSig()>
    Function DidAlloc(
                ByVal pv As IntPtr) As Int16 ' Pointer to the memory block

    ' This method minimizes the heap as much as possible by releasing unused memory to the operating system, 
    ' coalescing adjacent free blocks and committing free pages.
    <PreserveSig()>
    Sub HeapMinimize()
End Interface

#End Region

#Region "   Drag/Drop Interfaces and other declarations"

#Region "   Drag/Drop Enums and Structures"
#Region "       CLIPFORMAT Enum"
Public Enum CF
    TEXT = 1
    BITMAP = 2
    METAFILEPICT = 3
    SYLK = 4
    DIF = 5
    TIFF = 6
    OEMTEXT = 7
    DIB = 8
    PALETTE = 9
    PENDATA = 10
    RIFF = 11
    WAVE = 12
    UNICODETEXT = 13
    ENHMETAFILE = 14
    HDROP = 15
    LOCALE = 16
    MAX = 17
    OWNERDISPLAY = &H80
    DSPTEXT = &H81
    DSPBITMAP = &H82
    DSPMETAFILEPICT = &H83
    DSPENHMETAFILE = &H8E
    PRIVATEFIRST = &H200
    PRIVATELAST = &H2FF
    GDIOBJFIRST = &H300
    GDIOBJLAST = &H3FF
End Enum
#End Region

#Region "       DVASPECT Enum"
<Flags()>
Public Enum DVASPECT
    CONTENT = 1
    THUMBNAIL = 2
    ICON = 4
    DOCPRINT = 8
End Enum
#End Region

#Region "       TYMED Enum"
<Flags()>
Public Enum TYMED
    HGLOBAL = 1
    FILE = 2
    ISTREAM = 4
    ISTORAGE = 8
    GDI = 16
    MFPICT = 32
    ENHMF = 64
    NULL = 0
End Enum
#End Region

#Region "   ADVF Enum"
<Flags()>
Public Enum ADVF
    NODATA = 1
    PRIMEFIRST = 2
    ONLYONCE = 4
    DATAONSTOP = 64
    CACHE_NOHANDLER = 8
    CACHE_FORCEBUILTIN = 16
    CACHE_ONSAVE = 32
End Enum
#End Region

#Region "       FORMATETC Structure"
<StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Auto)>
<CodeAnalysis.SuppressMessage("Performance", "CA1815:Override equals and operator equals on value types", Justification:="<Pending>")>
Public Structure FORMATETC
#Disable Warning IDE1006 ' Naming Styles
    Public cfFormat As CF
    <CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2111:PointersShouldNotBeVisible")>
    Public ptd As IntPtr
    Public dwAspect As DVASPECT
    Public lindex As Integer
    Public Tymd As TYMED
#Enable Warning IDE1006 ' Naming Styles
End Structure
#End Region

#Region "       STGMEDIUM Structure"
<StructLayout(LayoutKind.Sequential)>
<CodeAnalysis.SuppressMessage("Performance", "CA1815:Override equals and operator equals on value types", Justification:="<Pending>")>
Friend Structure STGMEDIUM
#Disable Warning IDE1006 ' Naming Styles
    Public tymed As Integer
    <CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2111:PointersShouldNotBeVisible")>
    Public hGlobal As IntPtr
    <CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2111:PointersShouldNotBeVisible")>
    Public pUnkForRelease As IntPtr
#Enable Warning IDE1006 ' Naming Styles
End Structure
#End Region

#Region "       DROPFILES Structure"
<StructLayout(LayoutKind.Sequential)>
<CodeAnalysis.SuppressMessage("Performance", "CA1815:Override equals and operator equals on value types", Justification:="<Pending>")>
Friend Structure DROPFILES
#Disable Warning IDE1006 ' Naming Styles
    Public pFiles As Integer
    Public pt As Point
    Public fNC As Boolean
    Public fWide As Boolean
#Enable Warning IDE1006 ' Naming Styles
End Structure
#End Region
#End Region

#Region "   Drag/Drop Interfaces"

#Region "   COM Interface for IEumFormatETC"
'    MIDL_INTERFACE("00000103-0000-0000-C000-000000000046")
'IEnumFORMATETC : public IUnknown
'public:
'    virtual /* [local] */ HRESULT STDMETHODCALLTYPE Next( 
'        /* [in] */ ULONG celt,
'        /* [length_is][size_is][out] */ FORMATETC *rgelt,
'        /* [out] */ ULONG *pceltFetched) = 0;

'    virtual HRESULT STDMETHODCALLTYPE Skip( 
'        /* [in] */ ULONG celt) = 0;

'    virtual HRESULT STDMETHODCALLTYPE Reset( void) = 0;

'    virtual HRESULT STDMETHODCALLTYPE Clone( 
'        /* [out] */ IEnumFORMATETC **ppenum) = 0;
<ComImportAttribute(),
      InterfaceTypeAttribute(ComInterfaceType.InterfaceIsIUnknown),
      Guid("00000103-0000-0000-C000-000000000046")>
Friend Interface IEnumFORMATETC

    <PreserveSig()>
    Function GetNext(
            ByVal celt As Integer,
            ByRef rgelt As FORMATETC,
            ByRef pceltFetched As Integer) As Integer

    <PreserveSig()>
    Function Skip(
            ByVal celt As Integer) As Integer

    <PreserveSig()>
    Function Reset() As Integer

    <PreserveSig()>
    Function Clone(
            ByRef ppenum As IEnumFORMATETC) As Integer
End Interface

#End Region

#Region "       Com Interop for IDataObject"
'    MIDL_INTERFACE("0000010e-0000-0000-C000-000000000046")
'IDataObject : public IUnknown
'{
'public:
'    virtual /* [local] */ HRESULT STDMETHODCALLTYPE GetData( 
'        /* [unique][in] */ FORMATETC *pformatetcIn,
'        /* [out] */ STGMEDIUM *pmedium) = 0;

'    virtual /* [local] */ HRESULT STDMETHODCALLTYPE GetDataHere( 
'        /* [unique][in] */ FORMATETC *pformatetc,
'        /* [out][in] */ STGMEDIUM *pmedium) = 0;

'    virtual HRESULT STDMETHODCALLTYPE QueryGetData( 
'        /* [unique][in] */ FORMATETC *pformatetc) = 0;

'    virtual HRESULT STDMETHODCALLTYPE GetCanonicalFormatEtc( 
'        /* [unique][in] */ FORMATETC *pformatectIn,
'        /* [out] */ FORMATETC *pformatetcOut) = 0;

'    virtual /* [local] */ HRESULT STDMETHODCALLTYPE SetData( 
'        /* [unique][in] */ FORMATETC *pformatetc,
'        /* [unique][in] */ STGMEDIUM *pmedium,
'        /* [in] */ BOOL fRelease) = 0;

'    virtual HRESULT STDMETHODCALLTYPE EnumFormatEtc( 
'        /* [in] */ DWORD dwDirection,
'        /* [out] */ IEnumFORMATETC **ppenumFormatEtc) = 0;

'    virtual HRESULT STDMETHODCALLTYPE DAdvise( 
'        /* [in] */ FORMATETC *pformatetc,
'        /* [in] */ DWORD advf,
'        /* [unique][in] */ IAdviseSink *pAdvSink,
'        /* [out] */ DWORD *pdwConnection) = 0;

'    virtual HRESULT STDMETHODCALLTYPE DUnadvise( 
'        /* [in] */ DWORD dwConnection) = 0;

'    virtual HRESULT STDMETHODCALLTYPE EnumDAdvise( 
'        /* [out] */ IEnumSTATDATA **ppenumAdvise) = 0;

'};

<ComImportAttribute(),
      InterfaceTypeAttribute(ComInterfaceType.InterfaceIsIUnknown),
        Guid("0000010e-0000-0000-C000-000000000046")>
Friend Interface IDataObject

    <PreserveSig()>
    Function GetData(ByRef pformatetcIn As FORMATETC, ByRef pmedium As STGMEDIUM) As Integer

    <PreserveSig()>
    Function GetDataHere(ByRef pformatetcIn As FORMATETC, ByRef pmedium As STGMEDIUM) As Integer

    <PreserveSig()>
    Function QueryGetData(ByRef pformatetc As FORMATETC) As Integer

    <PreserveSig()>
    Function GetCanonicalFormatEtc(ByVal pformatetc As FORMATETC,
                                        ByRef pformatetcout As FORMATETC) As Integer
    <PreserveSig()>
    Function SetData(ByRef pformatetcIn As FORMATETC,
                         ByRef pmedium As STGMEDIUM,
                         ByVal frelease As Boolean) As Integer
    <PreserveSig()>
    Function EnumFormatEtc(ByVal dwDirection As Integer,
                               ByRef ppenumFormatEtc As IEnumFORMATETC) As Integer
    <PreserveSig()>
    Function DAdvise(ByRef pformatetc As FORMATETC,
                         ByVal advf As ADVF,
                         ByVal pAdvSink As IAdviseSink,
                         ByRef pdwConnection As Integer) As Integer

    <PreserveSig()>
    Function DUnadvise(ByVal dwConnection As Integer) As Integer

    <PreserveSig()>
    Function EnumDAdvise(ByRef ppenumAdvise As Object) As Integer
End Interface

#End Region

#Region " Com Interop for IAdviseSink "
'    MIDL_INTERFACE("0000010f-0000-0000-C000-000000000046")
'IAdviseSink : public IUnknown
'public:
'    virtual /* [local] */ void STDMETHODCALLTYPE OnDataChange( 
'        /* [unique][in] */ FORMATETC *pFormatetc,
'        /* [unique][in] */ STGMEDIUM *pStgmed) = 0;

'    virtual /* [local] */ void STDMETHODCALLTYPE OnViewChange( 
'        /* [in] */ DWORD dwAspect,
'        /* [in] */ LONG lindex) = 0;

'    virtual /* [local] */ void STDMETHODCALLTYPE OnRename( 
'        /* [in] */ IMoniker *pmk) = 0;

'    virtual /* [local] */ void STDMETHODCALLTYPE OnSave( void) = 0;

'    virtual /* [local] */ void STDMETHODCALLTYPE OnClose( void) = 0;

<ComImportAttribute(), InterfaceTypeAttribute(ComInterfaceType.InterfaceIsIUnknown), Guid("0000010f-0000-0000-C000-000000000046")>
Friend Interface IAdviseSink
    Sub OnDataChange(ByRef pformatetcIn As FORMATETC, ByRef pmedium As STGMEDIUM)
    Sub OnViewChange(ByVal dwAspect As Integer, ByVal lindex As Integer)
    Sub OnRename(ByVal pmk As IntPtr)

    Sub OnSave()

    Sub OnClose()

End Interface
#End Region

#End Region


#End Region
#End Region

