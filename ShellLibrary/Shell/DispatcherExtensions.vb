﻿Imports System.Runtime.CompilerServices
Imports System.Windows.Threading

Namespace DispatcherExtensions

    ''' <summary> Dispatcher extension methods. </summary>
    ''' <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para> </remarks>
    Public Module Methods

        ''' <summary> Executes the events operation. </summary>
        ''' <remarks>
        ''' https://stackoverflow.com/questions/4502037/where-is-the-application-doevents-in-wpf. Well, I
        ''' just hit a case where I start work on a method that runs on the Dispatcher thread, and it
        ''' needs to block without blocking the UI Thread. Turns out that MSDN topic on the Dispatcher
        ''' Push Frame method explains how to implement a DoEvents() based on the Dispatcher itself:
        ''' https://goo.gl/WGFZEU
        ''' <code>
        ''' Imports System.Windows.Threading
        ''' Imports isr.Core.Services.DispatcherExtensions
        ''' Windows.Forms.Application.DoEvents()
        ''' </code>
        ''' </remarks>
        ''' <param name="dispatcher"> The dispatcher. </param>
        <Extension>
        Public Sub DoEvents(ByVal dispatcher As Dispatcher)
            If dispatcher Is Nothing Then Throw New ArgumentNullException(NameOf(dispatcher))
            Dim frame As New DispatcherFrame
            dispatcher.BeginInvoke(DispatcherPriority.Background, New DispatcherOperationCallback(AddressOf Methods.ExitFrame), frame)
            Dispatcher.PushFrame(frame)
        End Sub

        ''' <summary> Executes the events operation before and after a  <see cref="Threading.Tasks.Task"/> delay. </summary>
        ''' <remarks>
        ''' <code>
        ''' Imports System.Windows.Threading
        ''' Imports isr.Core.Services.DispatcherExtensions
        ''' Windows.Forms.Application.DoEvents()(Timespan.FromMilliseconds(10))
        ''' </code>
        ''' </remarks>
        ''' <param name="dispatcher"> The dispatcher. </param>
        ''' <param name="delay">      The delay. </param>
        <Extension>
        Public Sub DoEventsTaskDelay(ByVal dispatcher As Dispatcher, ByVal delay As TimeSpan)
            dispatcher.DoEvents
            Threading.Tasks.Task.Delay(delay).Wait()
            dispatcher.DoEvents
        End Sub

        ''' <summary> Exit frame. </summary>
        ''' <param name="f"> references to the <see cref="DispatcherFrame"/>. </param>
        ''' <returns> An Object. </returns>
        Private Function ExitFrame(f As Object) As Object
            TryCast(f, DispatcherFrame).Continue = False
            Return Nothing
        End Function

    End Module

End Namespace

