Imports System.Runtime.InteropServices
Imports System.Text

''' <summary> An unsafe native methods. </summary>
''' <remarks> David, 2020-01-04 <para>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
''' Licensed under The MIT License.</para> </remarks>
Friend Class UnsafeNativeMethods

    <CodeAnalysis.SuppressMessage("Globalization", "CA2101:Specify marshaling for P/Invoke string arguments", Justification:="<Pending>")>
    Declare Auto Function RegisterClipboardFormat Lib "User32" (ByVal lpszFormat As String) As Integer

    Friend Declare Auto Sub ReleaseStgMedium Lib "ole32.dll" (ByRef pmedium As STGMEDIUM)

    Declare Auto Function RegisterDragDrop Lib "ole32.dll" (ByVal hWnd As IntPtr, ByVal dropTarget As IDropTarget) As Integer

    Declare Auto Function RevokeDragDrop Lib "ole32.dll" (ByVal hWnd As IntPtr) As Integer

    <CodeAnalysis.SuppressMessage("Globalization", "CA2101:Specify marshaling for P/Invoke string arguments", Justification:="<Pending>")>
    Declare Auto Function DragQueryFile Lib "shell32.dll" (ByVal hDrop As IntPtr, ByVal iFile As Integer,
                                                           <MarshalAs(UnmanagedType.LPTStr)> ByVal lpszFile As StringBuilder,
                                                           ByVal cch As Integer) As Integer

    ''' <Summary>Get an IMalloc Interface; Not required for .Net apps, use Marshal class </Summary>
    Declare Auto Function SHGetMalloc Lib "shell32" (ByRef pMalloc As IMalloc) As Integer

    ''' <Summary> Retrieves the IShellFolder interface for the desktop folder, which is the root of the Shell's namespace. </Summary>
    ''' <param name="ppshf"> Receives the IShellFolder interface for the desktop folder </param>
    Declare Auto Function SHGetDesktopFolder Lib "shell32.dll" (ByRef ppshf As IShellFolder) As Integer

    Friend Declare Function SHGetSpecialFolderLocation Lib "Shell32" (ByVal hWndOwner As IntPtr, ByVal csidl As Integer, ByRef ppidl As IntPtr) As Integer
    ' Declare Function SHGetSpecialFolderLocation Lib "Shell32" (ByVal hWndOwner As Integer, ByVal c s i d l As Integer, ByRef p p i d l As IntPtr) As Integer

    ' Accepts a STRRET structure returned by IShellFolder::GetDisplayNameOf that contains or points to a 
    ' string, and then returns that string as a BSTR.
    ' <param>
    '       Pointer to a STRRET structure.
    '       Pointer to an ITEMIDLIST uniquely identifying a file object or subfolder relative
    '       Pointer to a variable of type BSTR that contains the converted string.
    '</param>
    Private Declare Auto Function StrRetToBSTR Lib "shlwapi.dll" (
                ByRef pstr As STRRET,
                ByVal pidl As IntPtr,
                <MarshalAs(UnmanagedType.BStr)>
                ByRef pbstr As String) As Integer

    '<Summary>
    ' Takes a STRRET structure returned by IShellFolder::GetDisplayNameOf, 
    ' converts it to a string, and 
    ' places the result in a buffer. 
    ' <param>
    '       Pointer to a STRRET structure.
    '       Pointer to an ITEMIDLIST uniquely identifying a file object or subfolder relative
    '       Pointer to a Buffer to hold the display name. It will be returned as a null-terminated
    '                   string. If cchBuf is too small, 
    '                   the name will be truncated to fit. 
    '       Size of pszBuf, in characters. 
    '</param>
    '</Summary>
    <CodeAnalysis.SuppressMessage("Globalization", "CA2101:Specify marshaling for P/Invoke string arguments", Justification:="<Pending>")>
    Declare Auto Function StrRetToBuf Lib "shlwapi.dll" (
                        ByVal pstr As IntPtr,
                        ByVal pidl As IntPtr,
                        ByVal pszBuf As StringBuilder,
                        <MarshalAs(UnmanagedType.U4)>
                        ByVal cchBuf As Integer) As Integer

    ''' <Summary> Sends a message to some Window   </Summary>
    Public Declare Auto Function SendMessage Lib "user32" (ByVal hWnd As IntPtr, ByVal wMsg As Integer, ByVal wParam As Integer, ByVal lParam As IntPtr) As Integer

    ''' <Summary> Gets an IconSize from a ImagelistHandle </Summary>
    Declare Function ImageList_GetIconSize Lib "comctl32" (ByVal himl As IntPtr, ByRef cx As Integer, ByRef cy As Integer) As Integer

    Declare Auto Function ImageList_ReplaceIcon Lib "comctl32" (ByVal hImageList As IntPtr, ByVal iconIndex As Integer, ByVal hIcon As IntPtr) As Integer


    Declare Function ImageList_GetIcon Lib "comctl32" (ByVal himl As IntPtr, ByVal i As Integer, ByVal flags As Integer) As IntPtr

    Declare Function ImageList_Draw Lib "comctl32" (
        ByVal hIml As IntPtr,
        ByVal indx As Integer,
        ByVal hdcDst As IntPtr,
        ByVal x As Integer,
        ByVal y As Integer,
        ByVal fStyle As Integer) As Integer

    Declare Function DestroyIcon Lib "user32.dll" (ByVal hIcon As IntPtr) As Integer


    ''' <Summary>
    ''' SHGetFileInfo  - for a given Path as a string
    ''' Retrieves information about an object in the file system, such as a file, a folder, a directory, or a drive root.
    ''' </Summary>
    <CodeAnalysis.SuppressMessage("Globalization", "CA2101:Specify marshaling for P/Invoke string arguments", Justification:="<Pending>")>
    Friend Declare Auto Function SHGetFileInfo Lib "shell32" (
         ByVal pszPath As String,
         ByVal dwFileAttributes As Integer,
         ByRef sfi As SHFILEINFO,
         ByVal cbsfi As Integer,
         ByVal uFlags As Integer) As IntPtr

    ''' <Summary>
    '''  SHGetFileInfo  - for a given ItemIDList as IntPtr
    ''' </Summary>
    Friend Declare Auto Function SHGetFileInfo Lib "shell32" (
             ByVal ppidl As IntPtr,
             ByVal dwFileAttributes As Integer,
             ByRef sfi As SHFILEINFO,
             ByVal cbsfi As Integer,
             ByVal uFlags As Integer) As IntPtr

    '''<Summary>Despite its name, the API returns a filename
    ''' for a link to be copied/created in a Target directory,
    ''' with a specific LinkTarget. It will create a unique name
    ''' unless instructed otherwise (SHGLNI_NOUNIQUE).  And add
    ''' the ".lnk" extension, unless instructed otherwise(SHGLNI.NOLNK)
    '''</Summary>
    <CodeAnalysis.SuppressMessage("Globalization", "CA2101:Specify marshaling for P/Invoke string arguments", Justification:="<Pending>")>
    Friend Declare Ansi Function SHGetNewLinkInfo Lib "shell32" Alias "SHGetNewLinkInfoA" (
            ByVal pszLinkTo As String,
            ByVal pszDir As String,
            <Out(), MarshalAs(UnmanagedType.LPStr)> ByVal pszName As StringBuilder,
            ByRef pfMustCopy As Boolean,
            ByVal uFlags As SHGNLI) As Integer

    '''<Summary> Same function using a PIDL  as the pszLinkTo.
    '''  SHGNLI.PIDL must be set.
    '''</Summary>
    <CodeAnalysis.SuppressMessage("Globalization", "CA2101:Specify marshaling for P/Invoke string arguments", Justification:="<Pending>")>
    Friend Declare Ansi Function SHGetNewLinkInfo Lib "shell32" Alias "SHGetNewLinkInfoA" (
        ByVal pszLinkTo As IntPtr,
        ByVal pszDir As String,
        <Out(), MarshalAs(UnmanagedType.LPStr)> ByVal pszName As StringBuilder,
        ByRef pfMustCopy As Boolean,
        ByVal uFlags As SHGNLI) As Integer

    Declare Auto Function ILIsParent Lib "shell32" Alias "#23" (ByVal pidlParent As IntPtr, ByVal pidlBelow As IntPtr, ByVal fImmediate As Boolean) As Boolean

    Declare Auto Function ILIsEqual Lib "shell32" Alias "#21" (ByVal pidl1 As IntPtr, ByVal pidl2 As IntPtr) As Boolean

End Class
