Imports System.Runtime.CompilerServices
Imports System.ComponentModel
Namespace EnumExtensions
    ''' <summary> Includes extensions for enumerations and enumerated types. </summary>
    ''' <remarks> (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 04/09/2009, 1.1.3386.x. </para><para>
    ''' David, 07/07/2009, 1.2.3475.x"> <para>
    ''' http://www.CodeProject.com/Articles/37921/Enums-Flags-and-CSharp-Oh-my-bad-pun.aspx </para><para>
    ''' Under The Creative Commons Attribution-ShareAlike 2.5 License.</para>
    ''' </para></remarks>
    Public Module Methods

#Region " PRIVATE FUNCTIONS "

        ''' <summary> Converts a value to a long. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> The <see cref="T:System.Enum"/> value. </param>
        ''' <returns> Value as a Long. </returns>
        Private Function ToLong(ByVal value As System.Enum) As Long
            Return Convert.ToInt64(value, Globalization.CultureInfo.CurrentCulture)
        End Function

        ''' <summary> Converts a value to a long. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
        '''                                          illegal values. </exception>
        ''' <param name="value"> The <see cref="T:System.Enum"/> value. </param>
        ''' <returns> Value as a Long. </returns>
        <Extension>
        Public Function ToLong(ByVal value As Object) As Long
            If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
            Try
                Return Convert.ToInt64(value, Globalization.CultureInfo.CurrentCulture)
            Catch e As InvalidCastException
                Throw New ArgumentException($"Cannot convert {value} to long", NameOf(value))
            End Try
        End Function

#End Region

#Region " VALUES: FILTER "

        ''' <summary> Returns a filtered list of this collection. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="values">        The values. </param>
        ''' <param name="inclusionMask"> Specifies a mask for selecting the items to include in the list. </param>
        ''' <param name="exclusionMask"> Specifies a mask for selecting the items to Exclude in the list. </param>
        ''' <returns>
        ''' An enumerator that allows for each to be used to process filter in this collection.
        ''' </returns>
        <Extension()>
        Public Function Filter(Of T)(ByVal values As IEnumerable(Of T), ByVal inclusionMask As T, ByVal exclusionMask As T) As IEnumerable(Of T)
            If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
            Return values.Where(Function(x) (0 <> (ToLong(x) And ToLong(inclusionMask) And Not ToLong(exclusionMask))))
        End Function

        ''' <summary> Returns a filtered list of this collection. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="values">        The values. </param>
        ''' <param name="inclusionMask"> Specifies a mask for selecting the items to include in the list. </param>
        ''' <param name="exclusionMask"> Specifies a mask for selecting the items to Exclude in the list. </param>
        ''' <returns>
        ''' An enumerator that allows for each to be used to process filter in this collection.
        ''' </returns>
        <Extension()>
        Public Function Filter(ByVal values As IEnumerable(Of System.Enum), ByVal inclusionMask As System.Enum, ByVal exclusionMask As System.Enum) As IEnumerable(Of System.Enum)
            If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
            Return values.Where(Function(x) (0 <> (ToLong(x) And ToLong(inclusionMask) And Not ToLong(exclusionMask))))
        End Function

#End Region

#Region " VALUES: INCLUDE FILTER "

        ''' <summary>
        ''' Filters the this collection to include only item keys matching the inclusion mask.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="values">        The values. </param>
        ''' <param name="inclusionMask"> Specifies a mask for selecting the items to include in the list. </param>
        ''' <returns>
        ''' An enumerator that allows for each to be used to process include filter in this collection.
        ''' </returns>
        <Extension()>
        Public Function IncludeFilter(ByVal values As IEnumerable(Of Long), ByVal inclusionMask As Long) As IEnumerable(Of Long)
            If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
            Return values.Where(Function(x) (0 <> (x And inclusionMask)))
        End Function

        ''' <summary>
        ''' Filters the this collection to include only item keys matching the inclusion mask.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="values">        The values. </param>
        ''' <param name="inclusionMask"> Specifies a mask for selecting the items to include in the list. </param>
        ''' <returns>
        ''' An enumerator that allows for each to be used to process include filter in this collection.
        ''' </returns>
        <Extension()>
        Public Function IncludeFilter(ByVal values As IEnumerable(Of System.Enum), ByVal inclusionMask As Long) As IEnumerable(Of System.Enum)
            If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
            Return values.Where(Function(x) (0 <> (ToLong(x) And inclusionMask)))
        End Function

        ''' <summary>
        ''' Filters the this collection to include only item keys matching the inclusion mask.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="values">         The values. </param>
        ''' <param name="includedValues"> The included values. </param>
        ''' <returns>
        ''' An enumerator that allows for each to be used to process include filter in this collection.
        ''' </returns>
        <Extension()>
        Public Function IncludeFilter(ByVal values As IEnumerable(Of System.Enum),
                                      ByVal includedValues As IEnumerable(Of System.Enum)) As IEnumerable(Of System.Enum)
            If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
            If includedValues Is Nothing Then Throw New ArgumentNullException(NameOf(includedValues))
            Return values.Where(Function(x) includedValues.Contains(x)).ToList
        End Function

#End Region

#Region " VALUES: EXCLUDE FILTER "

        ''' <summary>
        ''' Filters the this collection to Exclude item keys matching the exclusion mask.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="values">        The values. </param>
        ''' <param name="exclusionMask"> Specifies a mask for selecting the items to Exclude in the list. </param>
        ''' <returns>
        ''' An enumerator that allows for each to be used to process Exclude filter in this collection.
        ''' </returns>
        <Extension()>
        Public Function ExcludeFilter(ByVal values As IEnumerable(Of Long), ByVal exclusionMask As Long) As IEnumerable(Of Long)
            If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
            Return values.Where(Function(x) (0 = (x And exclusionMask))).ToList
        End Function

        ''' <summary>
        ''' Filters the this collection to Exclude only item keys matching the exclusion mask.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="values">        The values. </param>
        ''' <param name="exclusionMask"> Specifies a mask for selecting the items to Exclude in the list. </param>
        ''' <returns>
        ''' An enumerator that allows for each to be used to process Exclude filter in this collection.
        ''' </returns>
        <Extension()>
        Public Function ExcludeFilter(ByVal values As IEnumerable(Of System.Enum), ByVal exclusionMask As Long) As IEnumerable(Of System.Enum)
            If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
            Return values.Where(Function(x) (0 = (ToLong(x) And exclusionMask))).ToList
        End Function

        ''' <summary>
        ''' Filters the this collection to Exclude only item keys matching the exclusion mask.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="values">         The values. </param>
        ''' <param name="excludedValues"> The Excluded values. </param>
        ''' <returns>
        ''' An enumerator that allows for each to be used to process Exclude filter in this collection.
        ''' </returns>
        <Extension()>
        Public Function ExcludeFilter(ByVal values As IEnumerable(Of System.Enum),
                                      ByVal excludedValues As IEnumerable(Of System.Enum)) As IEnumerable(Of System.Enum)
            If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
            If excludedValues Is Nothing Then Throw New ArgumentNullException(NameOf(excludedValues))
            Return values.Where(Function(x) Not excludedValues.Contains(x)).ToList
        End Function

#End Region

#Region " TO BINDING LIST "

        ''' <summary>
        ''' Converts enumerated
        ''' <see cref="System.Collections.Generic.KeyValuePair(Of System.Enum, String)">key value
        ''' pairs</see> to a binding list.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="pairs"> key value pairs. </param>
        ''' <returns>
        ''' A list of <see cref="T:System.Enum">enumeration</see> and
        ''' <see cref="T:System.String">string</see>.
        ''' </returns>
        <Extension()>
        Public Function ToBindingList(ByVal pairs As IEnumerable(Of System.Collections.Generic.KeyValuePair(Of System.Enum, String))) As BindingList(Of System.Collections.Generic.KeyValuePair(Of System.Enum, String))
            If pairs Is Nothing Then Throw New ArgumentNullException(NameOf(pairs))
            Dim result As New BindingList(Of System.Collections.Generic.KeyValuePair(Of System.Enum, String))
            For Each pair As System.Collections.Generic.KeyValuePair(Of System.Enum, String) In pairs
                result.Add(pair)
            Next
            Return result
        End Function

#End Region

#Region " VALUES: ENUM "

        ''' <summary> Enumerates enum values in this collection. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="enumItem"> An enum item representing the enumeration option. </param>
        ''' <returns>
        ''' An enumerator that allows for each to be used to process enum values in this collection.
        ''' </returns>
        <Extension()>
        Public Function EnumValues(ByVal enumItem As System.Enum) As IEnumerable(Of System.Enum)
            If enumItem Is Nothing Then Throw New ArgumentNullException(NameOf(enumItem))
            Return enumItem.GetType.EnumValues
        End Function

        ''' <summary> Enumerates enum values in this collection. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="enumType"> The <see cref="T:System.Enum"/> type. </param>
        ''' <returns>
        ''' An enumerator that allows for each to be used to process enum values in this collection.
        ''' </returns>
        <Extension()>
        Public Function EnumValues(ByVal enumType As Type) As IEnumerable(Of System.Enum)
            Return [Enum].GetValues(enumType).Cast(Of System.Enum)
        End Function

        ''' <summary> Enumerates enum values in this collection. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="enumType"> The <see cref="T:System.Enum"/> type. </param>
        ''' <returns>
        ''' An enumerator that allows for each to be used to process enum values in this collection.
        ''' </returns>
        <Extension()>
        Public Function EnumValues(Of T)(ByVal enumType As Type) As IEnumerable(Of T)
            Return [Enum].GetValues(enumType).Cast(Of T)
        End Function

        ''' <summary> Enumerates enum values in this collection. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <returns>
        ''' An enumerator that allows for each to be used to process enum values in this collection.
        ''' </returns>
        Public Function EnumValues(Of T)() As IEnumerable(Of T)
            Return [Enum].GetValues(GetType(T)).Cast(Of T)
        End Function

#End Region

#Region " VALUES: LONG "

        ''' <summary> Enumerates values for the Enum type of the supplied Enum constant. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="enumItem"> An enum item representing the enumeration option. </param>
        ''' <returns>
        ''' An enumerator that allows for each to be used to process values in this collection.
        ''' </returns>
        <Extension()>
        Public Function Values(ByVal enumItem As System.Enum) As IEnumerable(Of Long)
            If enumItem Is Nothing Then Throw New ArgumentNullException(NameOf(enumItem))
            Return enumItem.GetType.Values
        End Function

        ''' <summary> Enumerates values in this collection. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="enumType"> The <see cref="T:System.Enum"/> type. </param>
        ''' <returns>
        ''' An enumerator that allows for each to be used to process values in this collection.
        ''' </returns>
        <Extension()>
        Public Function Values(ByVal enumType As Type) As IEnumerable(Of Long)
            Return [Enum].GetValues(enumType).Cast(Of System.Enum).Select(Of Long)(Function(x) Convert.ToInt64(x, Globalization.CultureInfo.CurrentCulture))
        End Function

#End Region

#Region " VALUES : INTEGER "

        ''' <summary> Enumerates values for the Enum type of the supplied Enum constant. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="enumItem"> An enum item representing the enumeration option. </param>
        ''' <returns>
        ''' An enumerator that allows for each to be used to process values in this collection.
        ''' </returns>
        <Extension()>
        Public Function IntegerValues(ByVal enumItem As System.Enum) As IEnumerable(Of Integer)
            If enumItem Is Nothing Then Throw New ArgumentNullException(NameOf(enumItem))
            Return enumItem.GetType.IntegerValues
        End Function

        ''' <summary> Enumerates values in this collection. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="enumType"> The <see cref="T:System.Enum"/> type. </param>
        ''' <returns>
        ''' An enumerator that allows for each to be used to process values in this collection.
        ''' </returns>
        <Extension()>
        Public Function IntegerValues(ByVal enumType As Type) As IEnumerable(Of Integer)
            Return [Enum].GetValues(enumType).Cast(Of System.Enum).Select(Of Integer)(Function(x) Convert.ToInt32(x, Globalization.CultureInfo.CurrentCulture))
        End Function

#End Region

#Region " TO VALUES "

        ''' <summary>
        ''' Converts enumerated
        ''' <see cref="System.Collections.Generic.KeyValuePair(Of System.Enum, String)">key value
        ''' pairs</see> to a <see cref="IEnumerable(Of String)">list</see>
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="pairs"> key value pairs. </param>
        ''' <returns> A list of string values. </returns>
        <Extension()>
        Public Function ToValues(ByVal pairs As IEnumerable(Of System.Collections.Generic.KeyValuePair(Of System.Enum, String))) As IEnumerable(Of String)
            If pairs Is Nothing Then Throw New ArgumentNullException(NameOf(pairs))
            Return pairs.Select(Of String)(Function(x) x.Value)
        End Function

        ''' <summary> Enumerates to keys in this collection. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="pairs"> key value pairs. </param>
        ''' <returns> Pairs as an IEnumerable(Of System.Enum) </returns>
        <Extension()>
        Public Function ToKeys(ByVal pairs As IEnumerable(Of System.Collections.Generic.KeyValuePair(Of System.Enum, String))) As IEnumerable(Of System.Enum)
            If pairs Is Nothing Then Throw New ArgumentNullException(NameOf(pairs))
            Return pairs.Select(Of System.Enum)(Function(x) x.Key)
        End Function

        ''' <summary> Query if 'pairs' contains key. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="pairs"> The pairs. </param>
        ''' <param name="key">   An enum constant representing the key option. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        <Extension()>
        Public Function ContainsKey(ByVal pairs As IEnumerable(Of System.Collections.Generic.KeyValuePair(Of System.Enum, String)),
                                    ByVal key As System.Enum) As Boolean
            If pairs Is Nothing Then Throw New ArgumentNullException(NameOf(pairs))
            Return pairs.ToKeys.Contains(key)
        End Function

        ''' <summary> Select pair. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="pairs">       The pairs. </param>
        ''' <param name="defaultKey">  An enum constant representing the default key option. </param>
        ''' <param name="description"> The description. </param>
        ''' <returns> A KeyValuePair(Of System.Enum, String) </returns>
        <Extension()>
        Public Function SelectPair(ByVal pairs As IEnumerable(Of System.Collections.Generic.KeyValuePair(Of System.Enum, String)),
                                   ByVal defaultKey As System.Enum, ByVal description As String) As KeyValuePair(Of System.Enum, String)
            If pairs Is Nothing Then Throw New ArgumentNullException(NameOf(pairs))
            Dim def As New KeyValuePair(Of System.Enum, String)(defaultKey, defaultKey.Description)
            Dim found As KeyValuePair(Of System.Enum, String) = pairs.FirstOrDefault(Function(x) String.Equals(x.Value, description))
            Return If(String.IsNullOrWhiteSpace(found.Value), def, found)
        End Function

        ''' <summary> Select description. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="enumType">     The <see cref="T:System.Enum"/>
        '''                             type. </param>
        ''' <param name="defaultValue"> An enum constant representing the default value option. </param>
        ''' <param name="description">  The description. </param>
        ''' <returns> A System.Enum. </returns>
        <Extension()>
        Public Function SelectDescription(ByVal enumType As Type, ByVal defaultValue As System.Enum, ByVal description As String) As System.Enum
            Return enumType.ValueDescriptionPairs.SelectPair(defaultValue, description).Key
        End Function

#End Region

#Region " VALUE DESCRIPTION PAIRS "

        ''' <summary> Gets a Key Value Pair description item. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="enumItem"> The <see cref="T:System.Enum">enumeration</see> </param>
        ''' <returns> A list of. </returns>
        <Extension()>
        Public Function ValueDescriptionPair(ByVal enumItem As System.Enum) As System.Collections.Generic.KeyValuePair(Of System.Enum, String)
            If enumItem Is Nothing Then Throw New ArgumentNullException(NameOf(enumItem))
            Return New System.Collections.Generic.KeyValuePair(Of System.Enum, String)(enumItem, enumItem.Description)
        End Function

        ''' <summary> Gets a Key Value Pair description item. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="enumItem"> The <see cref="T:System.Enum">enumeration</see> </param>
        ''' <returns> A list of. </returns>
        <Extension()>
        Public Function ValueDescriptionPair(Of T)(ByVal enumItem As T) As System.Collections.Generic.KeyValuePair(Of T, String)
            If enumItem Is Nothing Then Throw New ArgumentNullException(NameOf(enumItem))
            Return New System.Collections.Generic.KeyValuePair(Of T, String)(enumItem, enumItem.Description)
        End Function

        ''' <summary> Enumerates the value description pairs of the <see cref="T:System.Enum"/> </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="values"> The values. </param>
        ''' <returns>
        ''' An <see cref="IEnumerable"/> containing the enumerated type value (key) and description
        ''' (value) pairs.
        ''' </returns>
        <Extension()>
        Public Function ValueDescriptionPairs(ByVal values As IEnumerable(Of System.Enum)) As IEnumerable(Of System.Collections.Generic.KeyValuePair(Of System.Enum, String))
            If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
            Return values.Select(Of KeyValuePair(Of System.Enum, String))(Function(x) x.ValueDescriptionPair)
        End Function

        ''' <summary> Enumerates the value description pairs of the <see cref="T:System.Enum"/> </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="values"> The values. </param>
        ''' <returns>
        ''' An <see cref="IEnumerable"/> containing the enumerated type value (key) and description
        ''' (value) pairs.
        ''' </returns>
        <Extension()>
        Public Function ValueDescriptionPairs(Of T)(ByVal values As IEnumerable(Of T)) As IEnumerable(Of System.Collections.Generic.KeyValuePair(Of T, String))
            If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
            Return values.Select(Of KeyValuePair(Of T, String))(Function(x) x.ValueDescriptionPair)
        End Function

        ''' <summary> Enumerates the value description pairs of the <see cref="T:System.Enum"/> </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="enumItem"> An enum item representing the enumeration option. </param>
        ''' <returns>
        ''' An <see cref="IEnumerable"/> containing the enumerated type value (key) and description
        ''' (value) pairs.
        ''' </returns>
        <Extension()>
        Public Function ValueDescriptionPairs(ByVal enumItem As System.Enum) As IEnumerable(Of System.Collections.Generic.KeyValuePair(Of System.Enum, String))
            If enumItem Is Nothing Then Throw New ArgumentNullException(NameOf(enumItem))
            Return enumItem.EnumValues.ValueDescriptionPairs
        End Function

        ''' <summary> Enumerates the value description pairs of the <see cref="T:System.Enum"/> </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="enumType"> The <see cref="T:System.Enum"/> type. </param>
        ''' <returns>
        ''' An <see cref="IEnumerable"/> containing the enumerated type value (key) and description
        ''' (value) pairs.
        ''' </returns>
        ''' <example>
        ''' Create an enumeration with descriptions:
        ''' <code>
        '''     Public Enum SimpleEnum2
        '''       [System.ComponentModel.Description("Today")] Today
        '''       [System.ComponentModel.Description("Last 7 days")] Last7
        '''       [System.ComponentModel.Description("Last 14 days")] Last14
        '''       [System.ComponentModel.Description("Last 30 days")] Last30
        '''       [System.ComponentModel.Description("All")] All
        '''     End Enum
        '''     Dim combo As ComboBox = New ComboBox()
        '''     combo.DataSource = GetType(SimpleEnum2).ValueDescriptionPairs().ToList
        '''     combo.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
        '''     combo.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
        ''' </code>
        ''' </example>
        <Extension()>
        Public Function ValueDescriptionPairs(ByVal enumType As Type) As IEnumerable(Of System.Collections.Generic.KeyValuePair(Of System.Enum, String))
            Return enumType.EnumValues.ValueDescriptionPairs
        End Function

#End Region

#Region " DESCRIPTIONS "

        ''' <summary>
        ''' Gets the <see cref="DescriptionAttribute"/> of an <see cref="T:System.Enum"/> type value.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value"> The <see cref="T:System.Enum"/> value. </param>
        ''' <returns> A string containing the text of the <see cref="DescriptionAttribute"/>. </returns>
        <Extension()>
        Public Function Description(ByVal value As System.Enum) As String
            If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
            Dim candidate As String = value.ToString()
            Dim fieldInfo As Reflection.FieldInfo = value.GetType().GetField(candidate)
            Dim attributes As DescriptionAttribute() = CType(fieldInfo.GetCustomAttributes(GetType(DescriptionAttribute), False), DescriptionAttribute())
            If attributes IsNot Nothing AndAlso attributes.Length > 0 Then
                candidate = attributes(0).Description
            End If
            Return candidate
        End Function

        ''' <summary>
        ''' Gets the <see cref="DescriptionAttribute"/> of an <see cref="T:System.Enum"/> type value.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value"> The <see cref="T:System.Enum"/> value. </param>
        ''' <returns> A string containing the text of the <see cref="DescriptionAttribute"/>. </returns>
        <Extension()>
        Public Function Description(Of T)(ByVal value As T) As String
            If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
            Dim candidate As String = value.ToString()
            Dim fieldInfo As Reflection.FieldInfo = value.GetType().GetField(candidate)
            Dim attributes As DescriptionAttribute() = CType(fieldInfo.GetCustomAttributes(GetType(DescriptionAttribute), False), DescriptionAttribute())
            If attributes IsNot Nothing AndAlso attributes.Length > 0 Then
                candidate = attributes(0).Description
            End If
            Return candidate
        End Function

        ''' <summary> Returns the descriptions of an enumeration. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="values"> The values. </param>
        ''' <returns> A list of strings. </returns>
        <Extension()>
        Public Function Descriptions(ByVal values As IEnumerable(Of System.Enum)) As IEnumerable(Of String)
            If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
            Return values.Select(Of String)(Function(x) x.Description)
        End Function

        ''' <summary> Returns the descriptions of an enumeration. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="enumItem"> An enum constant representing the enum type option. </param>
        ''' <returns> A list of strings. </returns>
        <Extension()>
        Public Function Descriptions(ByVal enumItem As System.Enum) As IEnumerable(Of String)
            If enumItem Is Nothing Then Throw New ArgumentNullException(NameOf(enumItem))
            Return enumItem.GetType.EnumValues.Descriptions
        End Function

        ''' <summary> Returns the descriptions of an enumeration. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="enumType"> The <see cref="T:System.Enum"/> type. </param>
        ''' <returns> A list of strings. </returns>
        <Extension()>
        Public Function Descriptions(ByVal enumType As Type) As IEnumerable(Of String)
            Return enumType.EnumValues.Descriptions
        End Function

        ''' <summary> Query if 'type' has description. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="enumType">    The <see cref="T:System.Enum"/>
        '''                            type. </param>
        ''' <param name="description"> The description. </param>
        ''' <returns> <c>true</c> if description; otherwise <c>false</c> </returns>
        <Extension()>
        Public Function HasDescription(ByVal enumType As Type, ByVal description As String) As Boolean
            Return enumType.Descriptions.Contains(description, StringComparer.OrdinalIgnoreCase)
        End Function

#End Region

#Region " NAMES "

        ''' <summary> Gets a Key Value Pair Name item. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value"> The <see cref="T:System.Enum">enumeration</see> item. </param>
        ''' <returns> a value name pair. </returns>
        <Extension()>
        Public Function ValueNamePair(ByVal value As System.Enum) As System.Collections.Generic.KeyValuePair(Of System.Enum, String)
            If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
            Return New System.Collections.Generic.KeyValuePair(Of System.Enum, String)(value, value.ToString)
        End Function

        ''' <summary> Gets a Key Value Pair Name item. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value"> The <see cref="T:System.Enum">enumeration</see> item. </param>
        ''' <returns> a value name pair. </returns>
        <Extension()>
        Public Function ValueNamePair(Of T)(ByVal value As T) As System.Collections.Generic.KeyValuePair(Of T, String)
            If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
            Return New System.Collections.Generic.KeyValuePair(Of T, String)(value, value.ToString)
        End Function

        ''' <summary> Enumerates the value name pairs of the <see cref="T:System.Enum"/> </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="values"> The values. </param>
        ''' <returns>
        ''' An <see cref="IEnumerable"/> containing the enumerated type value (key) and Name (value)
        ''' pairs.
        ''' </returns>
        <Extension()>
        Public Function ValueNamePairs(ByVal values As IEnumerable(Of System.Enum)) As IEnumerable(Of System.Collections.Generic.KeyValuePair(Of System.Enum, String))
            If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
            Return values.Select(Of KeyValuePair(Of System.Enum, String))(Function(x) x.ValueNamePair)
        End Function

        ''' <summary> Enumerates the value name pairs of the <see cref="T:System.Enum"/> </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="values"> The values. </param>
        ''' <returns>
        ''' An <see cref="IEnumerable"/> containing the enumerated type value (key) and Name (value)
        ''' pairs.
        ''' </returns>
        <Extension()>
        Public Function ValueNamePairs(Of T)(ByVal values As IEnumerable(Of T)) As IEnumerable(Of System.Collections.Generic.KeyValuePair(Of T, String))
            If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
            Return values.Select(Of KeyValuePair(Of T, String))(Function(x) x.ValueNamePair)
        End Function

        ''' <summary>
        ''' Converts the <see cref="T:System.Enum"/> type to an <see cref="IEnumerable"/> compatible
        ''' object.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="enumType"> The <see cref="T:System.Enum"/> type. </param>
        ''' <returns>
        ''' An <see cref="IEnumerable"/> containing the enumerated type value (key) and Name (value)
        ''' pairs.
        ''' </returns>
        ''' <example>
        ''' The first step is to add a Name attribute to your Enum.
        ''' <code>
        '''     Public Enum SimpleEnum2
        '''       [System.ComponentModel.Name("Today")] Today
        '''       [System.ComponentModel.Name("Last 7 days")] Last7
        '''       [System.ComponentModel.Name("Last 14 days")] Last14
        '''       [System.ComponentModel.Name("Last 30 days")] Last30
        '''       [System.ComponentModel.Name("All")] All
        '''     End Enum
        '''     Dim combo As ComboBox = New ComboBox()
        '''     combo.DataSource = GetType(SimpleEnum2).ValueNamePairs().ToList
        '''     combo.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
        '''     combo.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
        ''' </code>
        ''' </example>
        <Extension()>
        Public Function ValueNamePairs(ByVal enumType As Type) As IEnumerable(Of System.Collections.Generic.KeyValuePair(Of System.Enum, String))
            Return enumType.EnumValues.ValueNamePairs
        End Function

#End Region

#Region " DELIMITED DESCRIPTION "

        ''' <summary> The start delimiter. </summary>
        Private Const _StartDelimiter As Char = "("c

        ''' <summary> The end delimiter. </summary>
        Private Const _EndDelimiter As Char = ")"c

        ''' <summary> The embedded value format. </summary>
        Private Const _EmbeddedValueFormat As String = _StartDelimiter & "{0}" & _EndDelimiter

        ''' <summary>
        ''' Builds a delimited value. This helps parsing enumerated values that include delimited value
        ''' strings in their descriptions.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <returns> A String. </returns>
        <Extension()>
        Public Function BuildDelimitedValue(ByVal value As String) As String
            Return String.Format(Globalization.CultureInfo.CurrentCulture, _EmbeddedValueFormat, value)
        End Function

        ''' <summary> Returns the description up to the <paramref name="startDelimiter"/> </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">          The value. </param>
        ''' <param name="startDelimiter"> The start delimiter. </param>
        ''' <returns> The description up to the <paramref name="startDelimiter"/> </returns>
        <Extension()>
        Public Function DescriptionUntil(ByVal value As String, ByVal startDelimiter As Char) As String
            If String.IsNullOrWhiteSpace(value) Then
                Return String.Empty
            Else
                Dim startingIndex As Integer = value.IndexOf(startDelimiter)
                Return If(startingIndex > 0, value.Substring(0, startingIndex).TrimEnd, value)
            End If
        End Function

        ''' <summary> Returns the description up to the <see cref="_StartDelimiter"/> </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <returns> The description up to the <see cref="_StartDelimiter"/> </returns>
        <Extension()>
        Public Function DescriptionUntil(ByVal value As String) As String
            Return DescriptionUntil(value, _StartDelimiter)
        End Function

        ''' <summary> Returns the description up to the <paramref name="startDelimiter"/> </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">          The <see cref="T:System.Enum"/> item. </param>
        ''' <param name="startDelimiter"> The start delimiter. </param>
        ''' <returns> The description up to the <paramref name="startDelimiter"/> </returns>
        <Extension()>
        Public Function DescriptionUntil(ByVal value As System.Enum, ByVal startDelimiter As Char) As String
            Return If(value Is Nothing, String.Empty, DescriptionUntil(value.Description, startDelimiter))
        End Function

        ''' <summary> Returns the description up to the <see cref="_StartDelimiter"/> </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> The <see cref="T:System.Enum">enumeration</see> </param>
        ''' <returns> The description up to the <see cref="_StartDelimiter"/> </returns>
        <Extension()>
        Public Function DescriptionUntil(ByVal value As System.Enum) As String
            Return If(value Is Nothing, String.Empty, DescriptionUntil(value, _StartDelimiter))
        End Function

        ''' <summary> Extracts a delimited value. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">          The value. </param>
        ''' <param name="startDelimiter"> The start delimiter. </param>
        ''' <param name="endDelimiter">   The end delimiter. </param>
        ''' <returns> The extracted between. </returns>
        <Extension()>
        Public Function ExtractBetween(ByVal value As String, ByVal startDelimiter As Char, ByVal endDelimiter As Char) As String
            If String.IsNullOrWhiteSpace(value) Then
                Return String.Empty
            Else
                Dim startingIndex As Integer = value.IndexOf(startDelimiter)
                Dim endingIndex As Integer = value.LastIndexOf(endDelimiter)
                Return If(startingIndex > 0 AndAlso endingIndex > startingIndex,
                    value.Substring(startingIndex + 1, endingIndex - startingIndex - 1),
                    value)
            End If
        End Function

        ''' <summary> Extracts a delimited value from value. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <returns> The extracted between. </returns>
        <Extension()>
        Public Function ExtractBetween(ByVal value As String) As String
            Return ExtractBetween(value, _StartDelimiter, _EndDelimiter)
        End Function

        ''' <summary>
        ''' Extracts a delimited value from the description of the relevant enumerated value.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">          The <see cref="T:System.Enum"/> item. </param>
        ''' <param name="startDelimiter"> The start delimiter. </param>
        ''' <param name="endDelimiter">   The end delimiter. </param>
        ''' <returns> The extracted between. </returns>
        <Extension()>
        Public Function ExtractBetween(ByVal value As System.Enum, ByVal startDelimiter As Char, ByVal endDelimiter As Char) As String
            Return If(value Is Nothing, String.Empty, ExtractBetween(value.Description, startDelimiter, endDelimiter))
        End Function

        ''' <summary>
        ''' Extracts a delimited value from the description of the relevant enumerated value.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> The <see cref="T:System.Enum">enumeration</see> </param>
        ''' <returns> The extracted between. </returns>
        <Extension()>
        Public Function ExtractBetween(ByVal value As System.Enum) As String
            Return If(value Is Nothing, String.Empty, ExtractBetween(value, _StartDelimiter, _EndDelimiter))
        End Function

#End Region

#Region " WEBDEV_HB. UNDER LICENSE FROM "

        ''' <summary> Returns true if the specified bits are set. </summary>
        ''' <remarks> David, 06/05/2010, 1.2.3807.x. Returns true if equals (IS). </remarks>
        ''' <param name="enumItem"> The <see cref="T:System.Enum">enumeration</see> </param>
        ''' <param name="value">    Specifies the bit values which to compare. </param>
        ''' <returns> <c>true</c> if set; otherwise <c>false</c> </returns>
        <System.Runtime.CompilerServices.Extension()>
        Public Function IsSet(Of T)(ByVal enumItem As System.Enum, ByVal value As T) As Boolean
            Return Has(enumItem, value)
        End Function

        ''' <summary> Returns true if the enumerated flag has the specified bits. </summary>
        ''' <remarks>
        ''' David, 07/07/2009, 1.2.3475. </para><para>
        ''' http://www.CodeProject.com/Articles/37921/Enums-Flags-and-CSharp-Oh-my-bad-pun.aspx Under The
        ''' Creative Commons Attribution-ShareAlike 2.5 License
        ''' <para>
        ''' David, 06/05/2010, 1.2.3807. </para><para>
        ''' Returns true if equals (IS).
        ''' </para>
        ''' </remarks>
        ''' <param name="enumItem"> The <see cref="T:System.Enum"/> item. </param>
        ''' <param name="value">    Specifies the bit values which to compare. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        <System.Runtime.CompilerServices.Extension()>
        Public Function Has(Of T)(ByVal enumItem As System.Enum, ByVal value As T) As Boolean
            Try
                Dim oldValue As Integer = ConvertToInteger(Of T)(enumItem)
                Dim bitsValue As Integer = ConvertToInteger(Of T)(value)
                Return (oldValue = bitsValue) OrElse ((oldValue And bitsValue) = bitsValue)
            Catch
                Return False
            End Try
        End Function

        ''' <summary>  Returns true if value is only the provided type. </summary>
        ''' <typeparam name="T"></typeparam>
        ''' <param name="enumItem">  The <see cref="T:System.Enum"/> item.</param>
        ''' <param name="value"> Specifies the bit values which to compare.</param>
        ''' <remarks> David, 07/07/2009, 1.2.3475.x. 
        ''' http://www.CodeProject.com/Articles/37921/Enums-Flags-and-CSharp-Oh-my-bad-pun.aspx
        ''' Under The Creative Commons Attribution-ShareAlike 2.5 License. </remarks>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        <System.Runtime.CompilerServices.Extension()>
        Public Function [Is](Of T)(ByVal enumItem As System.Enum, ByVal value As T) As Boolean
            Try
                Dim oldValue As Integer = ConvertToInteger(Of T)(enumItem)
                Dim bitsValue As Integer = ConvertToInteger(value)
                Return oldValue = bitsValue
            Catch
                Return False
            End Try
        End Function

        ''' <summary> Set the specified bits. </summary>
        ''' <typeparam name="T"></typeparam>
        ''' <param name="enumItem">  The <see cref="T:System.Enum"/> item.</param>
        ''' <param name="value">     Specifies the bit values which to add.</param>
        <System.Runtime.CompilerServices.Extension()>
        Public Function [Set](Of T)(ByVal enumItem As System.Enum, ByVal value As T) As T
            Return Add(enumItem, value)
        End Function

        ''' <summary> Appends a value. </summary>
        ''' <remarks>
        ''' David, 07/07/2009, 1.2.3475.x.
        ''' http://www.CodeProject.com/Articles/37921/Enums-Flags-and-CSharp-Oh-my-bad-pun.aspx Under The
        ''' Creative Commons Attribution-ShareAlike 2.5 License.
        ''' </remarks>
        ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        '''                                      illegal values. </exception>
        ''' <param name="enumItem"> The <see cref="T:System.Enum"/> item. </param>
        ''' <param name="value">    Specifies the bit values which to add. </param>
        ''' <returns> A T. </returns>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        <System.Runtime.CompilerServices.Extension()>
        Public Function Add(Of T)(ByVal enumItem As System.Enum, ByVal value As T) As T
            Try
                Dim oldValue As Integer = ConvertToInteger(Of T)(enumItem)
                Dim bitsValue As Integer = ConvertToInteger(value)
                Dim newValue As Integer = oldValue Or bitsValue
                Return CType(CObj(newValue), T)
            Catch ex As Exception
                Throw New ArgumentException($"Could not append value from enumerated type '{GetType(T).Name}'.", ex)
            End Try
        End Function

        ''' <summary> Clears the specified bits. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="enumConstant"> The <see cref="T:System.Enum"/> constant. </param>
        ''' <param name="value">        Specifies the bit values which to remove. </param>
        ''' <returns> A T. </returns>
        <System.Runtime.CompilerServices.Extension()>
        Public Function Clear(Of T)(ByVal enumConstant As System.Enum, ByVal value As T) As T
            Return Remove(enumConstant, value)
        End Function

        ''' <summary> Completely removes the value. </summary>
        ''' <remarks>
        ''' David, 07/07/2009, 1.2.3475.x.
        ''' http://www.CodeProject.com/Articles/37921/Enums-Flags-and-CSharp-Oh-my-bad-pun.aspx Under The
        ''' Creative Commons Attribution-ShareAlike 2.5 License.
        ''' </remarks>
        ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        '''                                      illegal values. </exception>
        ''' <param name="enumItem"> The <see cref="T:System.Enum"/> item. </param>
        ''' <param name="value">    Specifies the bit values which to remove. </param>
        ''' <returns> A T. </returns>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        <System.Runtime.CompilerServices.Extension()>
        Public Function Remove(Of T)(ByVal enumItem As System.Enum, ByVal value As T) As T
            Try
                Dim oldValue As Integer = ConvertToInteger(Of T)(enumItem)
                Dim bitsValue As Integer = ConvertToInteger(value)
                Dim newValue As Integer = oldValue And (Not bitsValue)
                Return CType(CObj(newValue), T)
            Catch ex As Exception
                Throw New ArgumentException($"Could not remove value from enumerated type '{GetType(T).Name}'.", ex)
            End Try
        End Function

        ''' <summary> Converts a value to an integer. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> Specifies the bit values which to compare. </param>
        ''' <returns> The given data converted to an integer. </returns>
        Private Function ConvertToInteger(Of T)(ByVal value As T) As Integer
            Return CInt(CObj(value))
        End Function

        ''' <summary> Converts a type to an integer. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="enumConstant"> The <see cref="T:System.Enum"/> constant. </param>
        ''' <returns> The given data converted to an integer. </returns>
        Private Function ConvertToInteger(Of T)(ByVal enumConstant As System.Enum) As Integer
            Return CInt(CObj(enumConstant))
        End Function

#End Region

#Region " PARSE "

        ''' <summary> Converts a value to an enum. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <returns> Value as a T. </returns>
        <Extension()>
        Public Function ToEnum(Of T)(ByVal value As String) As T
            Return CType([Enum].Parse(GetType(T), value), T)
        End Function

        ''' <summary> Converts a value to an enum. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <returns> Value as a T. </returns>
        <Extension()>
        Public Function ToEnum(Of T)(ByVal value As Integer) As T
            Return CType([Enum].Parse(GetType(T), value.ToString, True), T)
        End Function

        ''' <summary> Converts a value to a nullable enum based on the underlying enum type. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> The <see cref="T:System.Enum"/> value. </param>
        ''' <returns> Value as a Nullable(Of T) </returns>
        <Extension()>
        Public Function ToNullableEnum(Of T As Structure)(ByVal value As Long) As Nullable(Of T)
            Select Case [Enum].GetUnderlyingType(GetType(T))
                Case GetType(Int32), GetType(Integer)
                    Return ToNullableEnumThis(Of T)(CInt(value))
                Case GetType(Int16)
                    Return ToNullableEnumThis(Of T)(CShort(value))
                Case GetType(Int64), GetType(Long)
                    Return ToNullableEnumThis(Of T)(CLng(value))
                Case GetType(Byte)
                    Return ToNullableEnumThis(Of T)(CByte(value))
                Case Else
                    Return ToNullableEnumThis(Of T)(CInt(value))
            End Select
        End Function

        ''' <summary> Converts a value to a nullable enum based on the underlying enum type. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> The <see cref="T:System.Enum"/> value. </param>
        ''' <returns> Value as a Nullable(Of T) </returns>
        <Extension()>
        Public Function ToNullableEnum(Of T As Structure)(ByVal value As Integer) As Nullable(Of T)
            Select Case [Enum].GetUnderlyingType(GetType(T))
                Case GetType(Int32), GetType(Integer)
                    Return ToNullableEnumThis(Of T)(CInt(value))
                Case GetType(Int16)
                    Return ToNullableEnumThis(Of T)(CShort(value))
                Case GetType(Int64), GetType(Long)
                    Return ToNullableEnumThis(Of T)(CLng(value))
                Case GetType(Byte)
                    Return ToNullableEnumThis(Of T)(CByte(value))
                Case Else
                    Return ToNullableEnumThis(Of T)(CInt(value))
            End Select
        End Function

        ''' <summary> Converts long to enum assuming enum underlying type is long. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> The <see cref="T:System.Enum"/> value. </param>
        ''' <returns> The enum converted long. </returns>
        Private Function ToNullableEnumThis(Of T As Structure)(ByVal value As Long) As Nullable(Of T)
            Return If([Enum].IsDefined(GetType(T), value), CType([Enum].ToObject(GetType(T), value), T), New Nullable(Of T))
        End Function

        ''' <summary> Converts integer to enum assuming enum underlying type is integer. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> The <see cref="T:System.Enum"/> value. </param>
        ''' <returns> The enum converted integer. </returns>
        Private Function ToNullableEnumThis(Of T As Structure)(ByVal value As Integer) As Nullable(Of T)
            Return If([Enum].IsDefined(GetType(T), value), CType([Enum].ToObject(GetType(T), value), T), New Nullable(Of T))
        End Function

        ''' <summary> Converts Short to enum assuming enum underlying type is Short. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> The <see cref="T:System.Enum"/> value. </param>
        ''' <returns> The enum converted Short. </returns>
        Private Function ToNullableEnumThis(Of T As Structure)(ByVal value As Short) As Nullable(Of T)
            Return If([Enum].IsDefined(GetType(T), value), CType([Enum].ToObject(GetType(T), value), T), New Nullable(Of T))
        End Function

        ''' <summary> Converts Byte to enum assuming enum underlying type is Byte. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> The <see cref="T:System.Enum"/> value. </param>
        ''' <returns> The enum converted Byte. </returns>
        Private Function ToNullableEnumThis(Of T As Structure)(ByVal value As Byte) As Nullable(Of T)
            Return If([Enum].IsDefined(GetType(T), value), CType([Enum].ToObject(GetType(T), value), T), New Nullable(Of T))
        End Function

#End Region

    End Module
End Namespace
