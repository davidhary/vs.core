Imports System.ComponentModel

''' <summary>
''' A class to encapsulate specific enumerations in cases where speed improvements are desired.
''' Reported 100000 operations time reduced from 3.9 to .57 million ticks. With using a creatable
''' class, improvement increased to 10 fold.
''' </summary>
''' <remarks>
''' (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 3/25/2015 </para><para>
''' David, 05/04/2008 from Stewart and StageSoft 1.2.3411. </para><para>
''' From Strong-Type and Efficient .NET Enums By Hanna GIAT</para><para>
''' http://www.CodeProject.com/KB/cs/efficient_strong_enum.aspx. </para> </para>
''' </remarks>
Public Class EnumExtender(Of T)

    ''' <summary>
    ''' constructor for the class instantiates all elements. With built-in elements, subsequent
    ''' operations are fairly speedy.
    ''' </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    Public Sub New()
        MyBase.New()
        Me._Type = GetType(T)
        Me._Values = System.Enum.GetValues(Me._Type)
        Me._Names = System.Enum.GetNames(Me._Type)

        ' Me._descriptions = isr.Core.Services.EnumExtensions.Descriptions(T)
        Dim values As New System.Collections.Generic.List(Of String)
        For Each value As System.Enum In System.Enum.GetValues(Me._Type)
            values.Add(Me.Description(value))
        Next value
        Me._Descriptions = values.ToArray
        Me._NameValuePairs = New Dictionary(Of String, T)(Me._Names.Count)
        Me._LowerCaseNameValuePairs = New Dictionary(Of String, T)(Me._Names.Count)
        Me._ValueDescriptionPairs = New Dictionary(Of T, String)(Me._Names.Count)
        Me._ValueEnumValuePairs = New Dictionary(Of Integer, T)(Me._Names.Count)
        Me._ValueNamePairs = New Dictionary(Of T, String)(Me._Names.Count)

        Dim index As Integer = 0
        For Each name As String In Me._Names
            Dim enumValue As T = CType(System.Enum.Parse(Me._Type, name), T)
            Me._NameValuePairs.Add(name, enumValue)
            Me._ValueDescriptionPairs.Add(enumValue, Me._Descriptions(index))
            Me._ValueNamePairs.Add(enumValue, name)
            Me._LowerCaseNameValuePairs.Add(name.ToLower(Globalization.CultureInfo.CurrentCulture), enumValue)
            Me._ValueEnumValuePairs.Add(Convert.ToInt32(enumValue, Globalization.CultureInfo.CurrentCulture), enumValue)
            index += 1
        Next name

    End Sub

    ''' <summary>
    ''' Gets the <see cref="DescriptionAttribute"/> of an <see cref="T:System.Enum"/> type value.
    ''' </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="value"> The <see cref="T:System.Enum"/> type value. </param>
    ''' <returns> A string containing the text of the <see cref="DescriptionAttribute"/>. </returns>
    Public Function Description(ByVal value As System.Enum) As String

        If value Is Nothing Then Return String.Empty

        Dim candidate As String = value.ToString()
        Dim fieldInfo As Reflection.FieldInfo = value.GetType().GetField(candidate)
        Dim attributes As DescriptionAttribute() = CType(fieldInfo.GetCustomAttributes(GetType(DescriptionAttribute), False), DescriptionAttribute())

        If attributes IsNot Nothing AndAlso attributes.Length > 0 Then
            candidate = attributes(0).Description
        End If
        Return candidate

    End Function

    ''' <summary> Gets the descriptions. </summary>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <value> The descriptions. </value>
    Public ReadOnly Property Descriptions() As IList(Of String)

    ''' <summary>
    ''' Returns the descriptions of an flags enumerated list masked by the specified value.
    ''' </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="mask"> Allows returning only selected items. </param>
    ''' <returns> The descriptions. </returns>
    Public Function FilteredDescriptions(ByVal mask As Integer) As IList(Of String)
        Dim index As Integer = 0
        Dim values As New System.Collections.Generic.List(Of String)
        For Each value As Object In Me.Values
            If (CInt(value) And mask) <> 0 Then
                values.Add(Me.Descriptions(index))
            End If
            index += 1
        Next value
        Return values
    End Function

    ''' <summary> Gets the Name of the Enum value. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="value"> The value. </param>
    ''' <returns> The name. </returns>
    Public Function Name(ByVal value As Object) As String
        ' We can hold an additional dictionary to map T -> string.
        ' In my specific usage, this usages is rare, so I selected the lower performance option
        If value Is Nothing Then Return String.Empty
        Try
            Dim enumValue As T = CType(value, T)
            For Each pair As KeyValuePair(Of String, T) In Me._NameValuePairs
                ' Dim x As Integer = Convert.ToInt32(pair.Value, Globalization.CultureInfo.CurrentCulture)
                If pair.Value.Equals(enumValue) Then
                    Return pair.Key
                End If
            Next pair
        Catch
            Throw New ArgumentException("Cannot convert " & value.ToString & " to " & Me._Type.ToString(), NameOf(value))
        End Try
        Return Nothing ' should never happen

    End Function

    ''' <summary> Returns the names. </summary>
    ''' <value> The names. </value>
    Public ReadOnly Property Names() As IEnumerable(Of String)

    ''' <summary>
    ''' Returns the Names of an flags enumerated list masked by the specified value.
    ''' </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="mask"> Allows returning only selected items. </param>
    ''' <returns> The names. </returns>
    Public Function FilteredNames(ByVal mask As Integer) As IEnumerable(Of String)
        Dim index As Integer = 0
        Dim values As New System.Collections.Generic.List(Of String)
        For Each value As Object In Me.Values
            If (CInt(value) And mask) <> 0 Then
                values.Add(Me.Names(index))
            End If
            index += 1
        Next value
        Return values
    End Function

    ''' <summary> Returns the array of Enum values. </summary>
    ''' <value> The values. </value>
    Public ReadOnly Property Values() As Array

    ''' <summary>
    ''' Returns the Values of an flags enumerated list masked by the specified value.
    ''' </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="mask"> Allows returning only selected items. </param>
    ''' <returns> The values. </returns>
    Public Function FilteredValues(ByVal mask As Integer) As IEnumerable(Of Integer)
        Dim index As Integer = 0
        Dim v As New System.Collections.Generic.List(Of Integer)
        For Each value As Object In Me.Values
            If (CInt(value) And mask) <> 0 Then
                v.Add(CInt(value))
            End If
            index += 1
        Next value
        Return v
    End Function

    ''' <summary>
    ''' Returns a <see cref="Dictionary(Of T, String)"/> of value and description pairs.
    ''' </summary>
    ''' <remarks>
    ''' The first step is to add a description attribute to your Enum.
    ''' <code>
    ''' Public Enum SimpleEnum2
    ''' [System.ComponentModel.Description("Today")] Today
    ''' [System.ComponentModel.Description("Last 7 days")] Last7
    ''' [System.ComponentModel.Description("Last 14 days")] Last14
    ''' [System.ComponentModel.Description("Last 30 days")] Last30
    ''' [System.ComponentModel.Description("All")] All
    ''' End Enum
    ''' Public SimpleEfficientEnum As New EnumExtender(of SimpleEnum2)
    ''' Dim combo As ComboBox = New ComboBox()
    ''' combo.DataSource = SimpleEfficientEnum.ValueDescriptionPairs(GetType(SimpleEnum2)).ToList
    ''' combo.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
    ''' combo.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
    ''' dim selectedEnum as SimpleEnum2 = SimpleEfficientEnum.ToObjectFromPair(combo.SelectedItem)
    ''' </code>
    ''' </remarks>
    ''' <value>
    ''' A <see cref="Dictionary(Of T, String)"/> containing the enumerated type value and description.
    ''' </value>
    Public ReadOnly Property ValueDescriptionPairs() As IDictionary(Of T, String)

    ''' <summary> Returns a <see cref="Dictionary(Of T, String)"/> of value and Name pairs. </summary>
    ''' <value>
    ''' A <see cref="Dictionary(Of T, String)"/> containing the enumerated type value and Name.
    ''' </value>
    Public ReadOnly Property ValueNamePairs() As IDictionary(Of T, String)

    ''' <summary>
    ''' Gets or sets the type.
    ''' </summary>
    Private ReadOnly _Type As Type

    ''' <summary> Gets the underlying type. </summary>
    ''' <value> The underlying type. </value>
    Public ReadOnly Property UnderlyingType() As Type
        Get
            ' Seems like this is good enough. 
            Return System.Enum.GetUnderlyingType(Me._Type)
        End Get
    End Property

    ''' <summary> Returns True if the enumerated value is defined in this enumeration. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> <c>True</c> if the enumerated value is defined in this enumeration. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function IsDefined(ByVal value As Object) As Boolean
        If value Is Nothing Then Return False
        Try
            Return Me.IsDefined(CInt(Microsoft.VisualBasic.Fix(value)))
        Catch
            Return False
        End Try
    End Function

    ''' <summary> Returns True if the enumerated value is defined in this enumeration. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> <c>True</c> if the enumerated value is defined in this enumeration. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function IsDefined(ByVal value As Integer) As Boolean
        Try
            Return Me._ValueEnumValuePairs.ContainsKey(value)
        Catch
            Return False
        End Try
    End Function

    ''' <summary> Dictionary of name value pairs used for parsing case-sensitive values. </summary>
    ''' <value> The name value pairs. </value>
    Private ReadOnly Property NameValuePairs As IDictionary(Of String, T)

    ''' <summary> Returns the Enum value for the given name. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> Enum value for the given name. </returns>
    Public Function Parse(ByVal value As String) As T
        If String.IsNullOrWhiteSpace(value) Then Return Nothing
        Return Me._NameValuePairs(value) ' Exception will be thrown if the value not found
    End Function

    ''' <summary>
    ''' Will serve us in the Parse method, with ignoreCase == true. It is possible not to hold this
    ''' member and to define a Comparer class - But my main goal here is the performance at runtime.
    ''' </summary>
    ''' <value> The lower case name value pairs. </value>
    Private ReadOnly Property LowerCaseNameValuePairs As IDictionary(Of String, T)

    ''' <summary> Returns the Enum value for the given name. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="value">      The value. </param>
    ''' <param name="ignoreCase"> . </param>
    ''' <returns> Enum value for the given name. </returns>
    Public Function Parse(ByVal value As String, ByVal ignoreCase As Boolean) As T
        If String.IsNullOrWhiteSpace(value) Then Return Nothing
        If ignoreCase Then
            Dim valLower As String = value.ToLower(Globalization.CultureInfo.CurrentCulture)
            Return Me.LowerCaseNameValuePairs(valLower)
        Else
            Return Me.Parse(value)
        End If
    End Function

    ''' <summary> Return a new dictionary excluding the specified values. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="excluded"> The excluded values. </param>
    ''' <returns> An IDictionary(Of T, String) </returns>
    Public Function Exclude(ByVal excluded As T()) As IDictionary(Of T, String)
        Dim v As New Dictionary(Of T, String)
        For Each vv As KeyValuePair(Of T, String) In Me._ValueDescriptionPairs
            If Not excluded.Contains(vv.Key) Then v.Add(vv.Key, vv.Value)
        Next
        Return v
    End Function

#Region " BIT MANIPULATION "

    ''' <summary> Appends a value. </summary>
    ''' <remarks>
    ''' David, 07/07/2009, 1.2.3475.x.
    ''' http://www.CodeProject.com/Articles/37921/Enums-Flags-and-CSharp-Oh-my-bad-pun.aspx Under The
    ''' Creative Commons Attribution-ShareAlike 2.5 License.
    ''' </remarks>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="value"> Specifies the bit values which to add. </param>
    ''' <returns> A T. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function Add(ByVal value As T) As T
        Try
            Dim oldValue As Integer = Me.ToInteger(Me._Type)
            Dim bitsValue As Integer = Me.ToInteger(value)
            Dim newValue As Integer = oldValue Or bitsValue
            Return CType(CObj(newValue), T)
        Catch ex As Exception
            Throw New ArgumentException($"Could not append value from enumerated type '{GetType(T).Name}'.", ex)
        End Try
    End Function

    ''' <summary> Set the specified bits. </summary>
    ''' <param name="value"> Specifies the bit values which to add.</param>
    Public Function [Set](ByVal value As T) As T
        Return Me.Add(value)
    End Function

    ''' <summary> Returns true if the enumerated flag has the specified bits. </summary>
    ''' <remarks>
    ''' David, 07/07/2009, 1.2.3475.x
    ''' http://www.CodeProject.com/Articles/37921/Enums-Flags-and-CSharp-Oh-my-bad-pun.aspx Under The
    ''' Creative Commons Attribution-ShareAlike 2.5 License <para>
    ''' David, 06/05/2010, 1.2.3807.x, Returns true if equals (IS).
    ''' </para>
    ''' </remarks>
    ''' <param name="value"> Specifies the bit values which to compare. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function Has(ByVal value As T) As Boolean
        Try
            Dim oldValue As Integer = Me.ToInteger(Me._Type)
            Dim bitsValue As Integer = Me.ToInteger(value)
            Return (oldValue = bitsValue) OrElse ((oldValue And bitsValue) = bitsValue)
        Catch
            Return False
        End Try
    End Function

    ''' <summary> Returns true if the specified bits are set. </summary>
    ''' <remarks> David, 06/05/2010, 1.2.3807.x Returns true if equals (IS). </remarks>
    ''' <param name="value"> Specifies the bit values which to compare. </param>
    ''' <returns> True if set, false if not. </returns>
    Public Function IsSet(ByVal value As T) As Boolean
        Return Me.Has(value)
    End Function

    ''' <summary>  Returns true if value is only the provided type. </summary>
    ''' <param name="value"> Specifies the bit values which to compare.</param>
    ''' <remarks> David, 07/07/2009, 1.2.3475.x.
    ''' http://www.CodeProject.com/Articles/37921/Enums-Flags-and-CSharp-Oh-my-bad-pun.aspx
    ''' Under The Creative Commons Attribution-ShareAlike 2.5 License </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function [Is](ByVal value As T) As Boolean
        Try
            Dim oldValue As Integer = Me.ToInteger(Me._Type)
            Dim bitsValue As Integer = Me.ToInteger(value)
            Return oldValue = bitsValue
        Catch
            Return False
        End Try
    End Function

    ''' <summary> Clears the specified bits. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="value"> Specifies the bit values which to remove. </param>
    ''' <returns> A T. </returns>
    Public Function Clear(ByVal value As T) As T
        Return Me.Remove(value)
    End Function

    ''' <summary> Completely removes the value. </summary>
    ''' <remarks>
    ''' David, 07/07/2009, 1.2.3475.x
    ''' http://www.CodeProject.com/Articles/37921/Enums-Flags-and-CSharp-Oh-my-bad-pun.aspx Under The
    ''' Creative Commons Attribution-ShareAlike 2.5 License.
    ''' </remarks>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="value"> Specifies the bit values which to remove. </param>
    ''' <returns> A T. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function Remove(ByVal value As T) As T
        Try
            Dim oldValue As Integer = Me.ToInteger(Me._Type)
            Dim bitsValue As Integer = Me.ToInteger(value)
            Dim newValue As Integer = oldValue And (Not bitsValue)
            Return CType(CObj(newValue), T)
        Catch ex As Exception
            Throw New ArgumentException($"Could not remove value from enumerated type '{GetType(T).Name}'.", ex)
        End Try
    End Function


#End Region

#Region " CONVERSIONS "

    ''' <summary> Returns the value converted to Enum. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="value"> The value. </param>
    ''' <returns> value converted to Enum. </returns>
    Public Function ToObject(ByVal value As Object) As T
        If value Is Nothing Then Return Nothing
        Try
            Dim wholeValue As Integer = CInt(Microsoft.VisualBasic.Fix(value))
            Return Me.ToObject(wholeValue)
        Catch e1 As InvalidCastException
            Throw New ArgumentException(String.Format("Cannot convert {0} to integer", value), NameOf(value))
        End Try
        'If an exception is coming from ToObject(value) do not catch it here.
    End Function

    ''' <summary> Converts a value to an integer. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
    '''                                          illegal values. </exception>
    ''' <param name="value"> The value. </param>
    ''' <returns> value as an Integer. </returns>
    Public Function ToInteger(ByVal value As Object) As Integer
        If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
        Try
            Return CInt(Microsoft.VisualBasic.Fix(value))
        Catch e As InvalidCastException
            Throw New ArgumentException($"Cannot convert {value} to integer", NameOf(value))
        End Try
    End Function

    ''' <summary> Will serve us in the <see cref="ToObject(Integer)"/> method. </summary>
    ''' <value> The value enum value pairs. </value>
    Public ReadOnly Property ValueEnumValuePairs As IDictionary(Of Integer, T)

    ''' <summary> Returns the value converted to Enum. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> value converted to Enum. </returns>
    Public Function ToObject(ByVal value As Integer) As T
        Return Me.ValueEnumValuePairs(value)
    End Function

    ''' <summary> Returns the value converted to Enum from the given value name pair. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> converted to Enum from the given value name pair. </returns>
    Public Function ToObjectFromPair(ByVal value As Object) As T
        If value Is Nothing Then Return Nothing
        Return Me.ToObject(CType(value, KeyValuePair(Of T, String)).Key)
    End Function

    ''' <summary> Converts a type to an integer. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="type"> The <see cref="T:System.Enum"/> type. </param>
    ''' <returns> The given data converted to an integer. </returns>
    Public Function ToInteger(ByVal type As System.Enum) As Integer
        Return Me.ToInteger(CObj(type))
    End Function

    ''' <summary> Returns the value converted to Enum from the given value name pair. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="values"> The values. </param>
    ''' <param name="value">  The value. </param>
    ''' <returns> converted to Enum from the given value name pair. </returns>
    Public Function ToObjectFromPair(ByVal values As IDictionary(Of Integer, T), ByVal value As Object) As T
        If value Is Nothing Then Return Nothing
        Return Me.ToObject(values, CType(value, KeyValuePair(Of T, String)).Key)
    End Function

    ''' <summary> Returns the value converted to Enum. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="values"> The values. </param>
    ''' <param name="value">  The value. </param>
    ''' <returns> value converted to Enum. </returns>
    Public Function ToObject(ByVal values As IDictionary(Of Integer, T), ByVal value As Object) As T
        If value Is Nothing Then Return Nothing
        Try
            Dim wholeValue As Integer = CInt(Microsoft.VisualBasic.Fix(value))
            Return Me.ToObject(values, wholeValue)
        Catch e1 As InvalidCastException
            Throw New ArgumentException(String.Format("Cannot convert {0} to integer", value), NameOf(value))
        End Try
        'If an exception is coming from ToObject(value) do not catch it here.
    End Function

    ''' <summary> Returns the value converted to Enum. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="values"> The values. </param>
    ''' <param name="value">  The value. </param>
    ''' <returns> value converted to Enum. </returns>
    Public Function ToObject(ByVal values As IDictionary(Of Integer, T), ByVal value As Integer) As T
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        Return values(value)
    End Function

#End Region

#Region " BINDING LIST "

    ''' <summary> Converts the pairs to a binding list. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="pairs"> The pairs. </param>
    ''' <returns> Pairs as a ComponentModel.BindingList(Of KeyValuePair(Of T, String)) </returns>
    Public Function ToBindingList(ByVal pairs As IDictionary(Of T, String)) As ComponentModel.BindingList(Of KeyValuePair(Of T, String))
        Dim bl As New ComponentModel.BindingList(Of KeyValuePair(Of T, String))
        Me.Populate(bl, pairs)
        Return bl
    End Function

    ''' <summary> Converts the pairs to a binding list. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="pairs"> The pairs. </param>
    ''' <returns> Pairs as a ComponentModel.BindingList(Of KeyValuePair(Of T, String)) </returns>
    Public Function ToBindingList(ByVal pairs As IList) As ComponentModel.BindingList(Of KeyValuePair(Of T, String))
        Dim bl As New ComponentModel.BindingList(Of KeyValuePair(Of T, String))
        Me.Populate(bl, pairs)
        Return bl
    End Function

    ''' <summary> Populates the binding list with key value pairs. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="bindingList"> List of bindings. </param>
    ''' <param name="pairs">       The pairs. </param>
    Public Sub Populate(ByVal bindingList As ComponentModel.BindingList(Of KeyValuePair(Of T, String)), ByVal pairs As IDictionary(Of T, String))
        If bindingList Is Nothing Then Throw New ArgumentNullException(NameOf(bindingList))
        If pairs Is Nothing Then Throw New ArgumentNullException(NameOf(pairs))
        For Each pair As KeyValuePair(Of T, String) In pairs
            bindingList.Add(pair)
        Next
    End Sub

    ''' <summary> Populates the binding list with key value pairs. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="bindingList"> List of bindings. </param>
    ''' <param name="pairs">       The pairs. </param>
    Public Sub Populate(ByVal bindingList As ComponentModel.BindingList(Of KeyValuePair(Of T, String)), ByVal pairs As IList)
        If bindingList Is Nothing Then Throw New ArgumentNullException(NameOf(bindingList))
        If pairs Is Nothing Then Throw New ArgumentNullException(NameOf(pairs))
        For Each pair As KeyValuePair(Of T, String) In pairs
            bindingList.Add(pair)
        Next
    End Sub

#End Region

End Class

