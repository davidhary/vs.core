Imports System.Reflection
Imports System.ComponentModel

''' <summary>
''' Utility class can be used to treat <c>string</c> arrays as <c>Enum</c>'s.  Use this class to
''' parse a string to an Enum or serialize an Enum to a string.  Unlike direct use of an Enum,
''' this class allows the Enum values to be decorated with <c>Description</c> attributes which
''' can be used when serializing the Enum. Modified to user with SCPI enumerations.
''' </summary>
''' <remarks>
''' (c) 2007 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 02/03/2007, 1.0.2590. </para><para>
''' Converted to Visual Basic .NET. By Rudy RIHANI, Code Net.
''' http://www.codeproject.com/Articles/17472/StringEnumerator
''' </para>
''' </remarks>
Public Class StringEnumerator(Of T As Structure)

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="StringEnumerator"/> class. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

#End Region

    ''' <summary>
    ''' Parses the specified value string to Enum type.  If the string can not be parsed then and
    ''' exception is thrown.  This method will first attempt to parse the string by matching it
    ''' directly to the string representation of the Enum value.  If a match is not found, then it
    ''' will attempt to match the Enum against each DescriptionAttribute applied to that Enum (if
    ''' any).
    ''' </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <exception cref="InvalidCastException"> . </exception>
    ''' <param name="value"> The Enum string. </param>
    ''' <returns> An Enum value of type T. </returns>
    Public Function Parse(ByVal value As String) As T
        Dim enumType As Type = GetType(T)
        For Each fi As FieldInfo In enumType.GetFields()
            If String.Compare(fi.Name, value, True, Globalization.CultureInfo.CurrentCulture) = 0 Then
                Return CType(fi.GetValue(Nothing), T)
            Else
                Dim fieldAttributes As Object() = fi.GetCustomAttributes(GetType(DescriptionAttribute), False)
                For Each attr As DescriptionAttribute In fieldAttributes
                    If String.Compare(attr.Description, value, True, Globalization.CultureInfo.CurrentCulture) = 0 Then
                        Return CType(fi.GetValue(Nothing), T)
                    End If
                Next attr
            End If
        Next fi
        Throw New InvalidCastException(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                     "Can't convert {0} to {1}", value, enumType.ToString()))
    End Function

    ''' <summary>
    ''' Parses the specified value string to Enum type.  If the string can not be parsed then and
    ''' exception is thrown.  This method will first attempt to parse the string by matching it
    ''' directly to the string representation of the Enum value.  If a match is not found, then it
    ''' will attempt to match the Enum against each DescriptionAttribute applied to that Enum (if
    ''' any).
    ''' </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <exception cref="InvalidCastException"> . </exception>
    ''' <param name="value"> The Enum string. </param>
    ''' <returns> An Enum value of type T. </returns>
    Public Function ParseContained(ByVal value As String) As T
        Dim enumType As Type = GetType(T)
        For Each fi As FieldInfo In enumType.GetFields()
            If String.Compare(fi.Name, value, True, Globalization.CultureInfo.CurrentCulture) = 0 Then
                Return CType(fi.GetValue(Nothing), T)
            Else
                Dim fieldAttributes As Object() = fi.GetCustomAttributes(GetType(DescriptionAttribute), False)
                For Each attr As DescriptionAttribute In fieldAttributes
                    If attr.Description.Contains(value) Then
                        Return CType(fi.GetValue(Nothing), T)
                    End If
                Next attr
            End If
        Next fi
        Throw New InvalidCastException(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                     "Can't convert {0} to {1}", value, enumType.ToString()))
    End Function

    ''' <summary>
    ''' Parses the specified value string to Enum type.  If the string can not be parsed then
    ''' <c>False</c> is returned. This method will first attempt to parse the string by matching it
    ''' directly to the string representation of the Enum value.  If a match is not found, then it
    ''' will attempt to match the Enum against each DescriptionAttribute applied to that Enum (if
    ''' any).
    ''' </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="value">      The value to parse. </param>
    ''' <param name="enumerator"> [in,out] The enumerator type. </param>
    ''' <returns> <c>True</c> if the string value was successfully parsed. </returns>
    Public Function TryParse(ByVal value As String, ByRef enumerator As T) As Boolean
        If [Enum].TryParse(value, enumerator) Then
            Return True
        Else
            Dim enumType As Type = GetType(T)
            For Each fi As FieldInfo In enumType.GetFields()
                If String.Compare(fi.Name, value, True, Globalization.CultureInfo.CurrentCulture) = 0 Then
                    enumerator = CType(fi.GetValue(Nothing), T)
                    Return True
                Else
                    Dim fieldAttributes As Object() = fi.GetCustomAttributes(GetType(DescriptionAttribute), False)
                    For Each attr As DescriptionAttribute In fieldAttributes
                        If String.Compare(attr.Description, value, True, Globalization.CultureInfo.CurrentCulture) = 0 Then
                            enumerator = CType(fi.GetValue(Nothing), T)
                            Return True
                        End If
                    Next attr
                End If
            Next fi
        End If
        Return False
    End Function

    ''' <summary>
    ''' Uses the enumeration DescriptionAttribute to generate the string translation of the Enum
    ''' value.  If no such attribute has been applied then the method will simply call T.ToString()
    ''' on the enum.
    ''' </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="enumValue"> The Enum value. </param>
    ''' <returns> The string translation of the Enum value. </returns>
    Public Shadows Function ToString(ByVal enumValue As T) As String
        Dim enumType As Type = GetType(T)
        Dim fi As FieldInfo = enumType.GetField(enumValue.ToString())

        'Get the Description attribute that has been applied to this enum
        Dim fieldAttributes As Object() = fi.GetCustomAttributes(GetType(DescriptionAttribute), False)
        If fieldAttributes.Length > 0 Then
            Dim descAttr As DescriptionAttribute = TryCast(fieldAttributes(0), DescriptionAttribute)
            If Not descAttr Is Nothing Then
                Return descAttr.Description
            End If

        End If
        ' Enum does not have Description attribute so we return default string representation.
        Return enumValue.ToString()
    End Function

    ''' <summary>
    ''' Returns an array of strings that represent the values of the enumerator.  If any of the Enum
    ''' values have a <see cref="T:System.ComponentModel.DescriptionAttribute">Description
    ''' Attribute</see>
    ''' applied to them, then the value of the description is used in the List element.
    ''' </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <returns> Array of strings that represent the values of the enumerator. </returns>
    Public Function ToArray() As IEnumerable(Of String)
        Return ToList()
    End Function

    ''' <summary>
    ''' Returns a collection of strings that represent the values of the enumerator.  If any of the
    ''' Enum values have <see cref="T:System.ComponentModel.DescriptionAttribute">Description
    ''' Attribute</see>
    ''' applied to them, then the value of the description is used in the List element.
    ''' </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <returns> A collection that represent the values of the enumerator. </returns>
    Public Function ToReadOnlyCollection() As ObjectModel.ReadOnlyCollection(Of String)
        Return New ObjectModel.ReadOnlyCollection(Of String)(ToList())
    End Function

    ''' <summary>
    ''' Returns a list of strings that represent the values of the enumerator.  If any of the Enum
    ''' values have <see cref="T:System.ComponentModel.DescriptionAttribute">Description
    ''' Attribute</see>
    ''' applied to them, then the value of the description is used in the List element.
    ''' </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <returns> List of strings that represent the values of the enumerator. </returns>
    Private Shared Function ToList() As IList(Of String)
        Dim enumValues As List(Of String) = New List(Of String)()
        Dim enumType As Type = GetType(T)
        For Each fi As FieldInfo In enumType.GetFields()
            If fi.IsSpecialName = False Then
                'Get the Description attribute that has been applied to this enum
                Dim fieldAttributes As Object() = fi.GetCustomAttributes(GetType(DescriptionAttribute), False)
                If fieldAttributes.Length > 0 Then
                    Dim descAttr As DescriptionAttribute = TryCast(fieldAttributes(0), DescriptionAttribute)
                    If Not descAttr Is Nothing Then
                        enumValues.Add(descAttr.Description)
                    End If
                Else
                    'Enum does not have Description attribute so we return default string representation.
                    Dim enumValue As T = CType(fi.GetValue(Nothing), T)
                    enumValues.Add(enumValue.ToString())
                End If
            End If
        Next fi

        Return enumValues

    End Function

    ''' <summary> Returns a dictionary that maps enumeration values to their descriptions. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <returns> A dictionary that maps enumeration values to their descriptions. </returns>
    Public Function ToDictionary() As IDictionary(Of T, String)
        Dim enumDictionary As IDictionary(Of T, String) = New Dictionary(Of T, String)()

        Dim enumType As Type = GetType(T)
        For Each fi As FieldInfo In enumType.GetFields()
            If fi.IsSpecialName = False Then
                Dim enumValue As T = CType(fi.GetValue(Nothing), T)

                'Get the Description attribute that has been applied to this enum
                Dim fieldAttributes As Object() = fi.GetCustomAttributes(GetType(DescriptionAttribute), False)
                If fieldAttributes.Length > 0 Then
                    Dim descAttr As DescriptionAttribute = TryCast(fieldAttributes(0), DescriptionAttribute)
                    If Not descAttr Is Nothing Then
                        enumDictionary.Add(enumValue, descAttr.Description)
                    End If
                Else
                    'Enum does not have Description attribute so we return default string representation.
                    enumDictionary.Add(enumValue, enumValue.ToString())
                End If
            End If
        Next fi

        Return enumDictionary
    End Function

    ''' <summary>
    ''' Returns a list that contains all of the Enum types.  This list can be enumerated against to
    ''' get each Enum value.
    ''' </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <returns> A list that contains all of the Enum types. </returns>
    Public Function GetKeys() As IList(Of T)
        Dim keys As List(Of T) = New List(Of T)()
        Dim enumType As Type = GetType(T)
        For Each fi As FieldInfo In enumType.GetFields()
            If fi.IsSpecialName = False Then
                Dim enumValue As T = CType(fi.GetValue(Nothing), T)
                keys.Add(enumValue)
            End If
        Next fi
        Return keys
    End Function

End Class
