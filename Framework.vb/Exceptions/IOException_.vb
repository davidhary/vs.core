

''' <summary> Handles Application Settings Input Output exceptions. </summary>
''' <remarks> Use This class to handle exceptions when reading or writing settings files.  <para>
''' (c) 2003 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License. </para><para>  
''' David, 1/21/2014,Documented. </para></remarks>
<Serializable()>
Public Class IOException
    Inherits ExceptionBase


#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> The default message. </summary>
    Private Const _DefaultMessage As String = "Failed accessing I/O."

    ''' <summary> Initializes a new instance of the <see cref="IOException" /> class. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    Public Sub New()
        Me.New(_DefaultMessage)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:IOException" /> class with a specified error
    ''' message.
    ''' </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="message"> The message that describes the error. </param>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:IOException" /> class with a specified error
    ''' message and a reference to the inner exception that is the cause of this exception.
    ''' </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="message">        The error message that explains the reason for the exception. </param>
    ''' <param name="innerException"> The exception that is the cause of the current exception, or a
    '''                               null reference (Nothing in Visual Basic) if no inner
    '''                               exception is specified. </param>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="T:IOException" /> class. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="format"> The format. </param>
    ''' <param name="args">   The arguments. </param>
    Public Sub New(ByVal format As String, ByVal ParamArray args() As Object)
        MyBase.New(format, args)
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="T:IOException" /> class. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="innerException"> Specifies the InnerException. </param>
    ''' <param name="format">         Specifies the exception formatting. </param>
    ''' <param name="args">           Specifies the message arguments. </param>
    Public Sub New(ByVal innerException As System.Exception, ByVal format As String, ByVal ParamArray args() As Object)
        MyBase.New(innerException, format, args)
    End Sub

    ''' <summary> Initializes a new instance of the class with serialized data. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="info">    The <see cref="T:System.Runtime.Serialization.SerializationInfo" />
    '''                        that holds the serialized object data about the exception being
    '''                        thrown. </param>
    ''' <param name="context"> The <see cref="T:System.Runtime.Serialization.StreamingContext" />
    '''                        that contains contextual information about the source or destination. 
    ''' </param>
    Protected Sub New(ByVal info As Runtime.Serialization.SerializationInfo, ByVal context As Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
    End Sub

#End Region

#Region " GET OBJECT DATA "

    ''' <summary> Overrides the GetObjectData method to serialize custom values. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="info">    Represents the SerializationInfo of the exception. </param>
    ''' <param name="context"> Represents the context information of the exception. </param>
    <Security.Permissions.SecurityPermission(Security.Permissions.SecurityAction.Demand, SerializationFormatter:=True)>
    Public Overrides Sub GetObjectData(ByVal info As System.Runtime.Serialization.SerializationInfo,
                                       ByVal context As System.Runtime.Serialization.StreamingContext)
        MyBase.GetObjectData(info, context)
    End Sub

#End Region

End Class

