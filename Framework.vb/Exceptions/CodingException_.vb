

''' <summary> Handles a coding exceptions. </summary>
''' <remarks>
''' Use this class to handle exceptions that were caused due to a coding oversight, such as
''' creating a dictionary and failing to update is as new items were added. <para>
''' (c) 2003 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License.</para><para>
''' David, 09/02/2016, 3.0.6089.x. </para></remarks>
<Serializable()>
Public Class CodingException
    Inherits ExceptionBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> The default message. </summary>
    Private Const _DefaultMessage As String = "Coding bug."

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:CodingException" /> class. Uses the internal
    ''' default message.
    ''' </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    Public Sub New()
        MyBase.New(CodingException._DefaultMessage)
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="T:CodingException" /> class. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="message"> The message. </param>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="T:CodingException" /> class. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="message">        The message. </param>
    ''' <param name="innerException"> Specifies the exception that was trapped for throwing this
    '''                               exception. </param>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="T:CodingException" /> class. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="format"> The format. </param>
    ''' <param name="args">   The arguments. </param>
    Public Sub New(ByVal format As String, ByVal ParamArray args() As Object)
        MyBase.New(format, args)
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="T:CodingException" /> class. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="innerException"> Specifies the InnerException. </param>
    ''' <param name="format">         Specifies the exception formatting. </param>
    ''' <param name="args">           Specifies the message arguments. </param>
    Public Sub New(ByVal innerException As System.Exception, ByVal format As String, ByVal ParamArray args() As Object)
        MyBase.New(innerException, format, args)
    End Sub

    ''' <summary> Initializes a new instance of the class with serialized data. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="info">    The <see cref="T:System.Runtime.Serialization.SerializationInfo" />
    '''                        that holds the serialized object data about the exception being
    '''                        thrown. </param>
    ''' <param name="context"> The <see cref="T:System.Runtime.Serialization.StreamingContext" />
    '''                        that contains contextual information about the source or destination. 
    ''' </param>
    Protected Sub New(ByVal info As Runtime.Serialization.SerializationInfo, ByVal context As Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
    End Sub

#End Region

#Region " GET OBJECT DATA "

    ''' <summary> Overrides the GetObjectData method to serialize custom values. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="info">    Represents the SerializationInfo of the exception. </param>
    ''' <param name="context"> Represents the context information of the exception. </param>
    <Security.Permissions.SecurityPermission(Security.Permissions.SecurityAction.Demand, SerializationFormatter:=True)>
    Public Overrides Sub GetObjectData(ByVal info As System.Runtime.Serialization.SerializationInfo,
                                       ByVal context As System.Runtime.Serialization.StreamingContext)
        MyBase.GetObjectData(info, context)
    End Sub

#End Region

End Class

