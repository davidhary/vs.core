Imports System.Runtime.CompilerServices
Namespace NumericExtensions

    ''' <summary> Includes extensions for <see cref="Random">random number generation</see>. </summary>
    ''' <remarks> (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 11/14/2013, 2.0.5066.x. </para></remarks>
    Public Module Methods

#Region " HYPOTENEUSE "

        ''' <summary> Computes the length of a right triangle's hypotenuse. </summary>
        ''' <remarks>
        ''' <para>The length is computed accurately, even in cases where x<sup>2</sup> or y<sup>2</sup>
        ''' would overflow.</para>
        ''' </remarks>
        ''' <param name="value">      The length of one side. </param>
        ''' <param name="orthogonal"> The length of orthogonal side. </param>
        ''' <returns>
        ''' The length of the hypotenuse, square root of (x<sup>2</sup> + y<sup>2</sup>).
        ''' </returns>
        <Extension()>
        Public Function Hypotenuse(ByVal value As Single, ByVal orthogonal As Single) As Double
            Return Hypotenuse(CDbl(value), CDbl(orthogonal))
        End Function

        ''' <summary> Computes the length of a right triangle's hypotenuse. </summary>
        ''' <remarks>
        ''' <para>The length is computed accurately, even in cases where x<sup>2</sup> or y<sup>2</sup>
        ''' would overflow.</para>
        ''' </remarks>
        ''' <param name="value">      The length of one side. </param>
        ''' <param name="orthogonal"> The length of orthogonal side. </param>
        ''' <returns>
        ''' The length of the hypotenuse, square root of (x<sup>2</sup> + y<sup>2</sup>).
        ''' </returns>
        <Extension()>
        Public Function Hypotenuse(ByVal value As Double, ByVal orthogonal As Double) As Double
            If (value = 0.0) AndAlso (orthogonal = 0.0) Then
                Return (0.0)
            Else
                Dim ax As Double = Math.Abs(value)
                Dim ay As Double = Math.Abs(orthogonal)
                If ax > ay Then
                    Dim r As Double = orthogonal / value
                    Return (ax * Math.Sqrt(1.0 + r * r))
                Else
                    Dim r As Double = value / orthogonal
                    Return (ay * Math.Sqrt(1.0 + r * r))
                End If
            End If
        End Function

        ''' <summary> Compute the distance between the array and the zero array. </summary>
        ''' <remarks> David, 2020-09-05. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="polygon"> The sides of the polygon. </param>
        ''' <returns> The distance between the two arrays. </returns>
        <Extension()>
        Public Function Hypotenuse(ByVal polygon As Double()) As Double
            If polygon Is Nothing Then Throw New ArgumentNullException(NameOf(polygon))
            Dim d As Double = 0
            For Each p As Double In polygon
                d += Math.Pow(p, 2D)
            Next
            d = Math.Sqrt(d)
            Return d
        End Function

        ''' <summary> Computes the length of a right triangle's hypotenuse. </summary>
        ''' <remarks>
        ''' <para>The length is computed accurately, even in cases where x<sup>2</sup> or y<sup>2</sup>
        ''' would overflow.</para>
        ''' </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="polygon"> The sides of the polygon. </param>
        ''' <param name="other">   The other polygon. </param>
        ''' <returns>
        ''' The length of the hypotenuse, square root of (x<sup>2</sup> + y<sup>2</sup>).
        ''' </returns>
        <Extension()>
        Public Function Hypotenuse(ByVal polygon As Double(), ByVal other As Double()) As Double
            If polygon Is Nothing Then Throw New ArgumentNullException(NameOf(polygon))
            If other Is Nothing Then Throw New ArgumentNullException(NameOf(other))
            Dim d As Double = 0
            For j As Integer = 0 To polygon.Count - 1
                d += Math.Pow((polygon(j) - other(j)), 2D)
            Next j
            d = Math.Sqrt(d)
            Return d
        End Function

        ''' <summary> Compute the distance between the array and the zero array. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="polygon"> The sides of the polygon. </param>
        ''' <returns> The distance between the two arrays. </returns>
        <Extension()>
        Public Function Hypotenuse(ByVal polygon As IEnumerable(Of Double)) As Double
            If polygon Is Nothing Then Throw New ArgumentNullException(NameOf(polygon))
            Dim d As Double = 0
            For Each p As Double In polygon
                d += Math.Pow(p, 2D)
            Next
            d = Math.Sqrt(d)
            Return d
        End Function

        ''' <summary> Compute the distance between two polygons. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="polygon"> The polygon. </param>
        ''' <param name="other">   The other polygon. </param>
        ''' <returns> The distance between the two arrays. </returns>
        <Extension()>
        Public Function Hypotenuse(ByVal polygon As IEnumerable(Of Double), ByVal other As IEnumerable(Of Double)) As Double
            If polygon Is Nothing Then Throw New ArgumentNullException(NameOf(polygon))
            If other Is Nothing Then Throw New ArgumentNullException(NameOf(other))
            Dim d As Double = 0
            For j As Integer = 0 To polygon.Count - 1
                d += Math.Pow((polygon(j) - other(j)), 2D)
            Next j
            d = Math.Sqrt(d)
            Return d
        End Function

        ''' <summary> Compute the distance between the point and the origin. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="point"> The point. </param>
        ''' <returns> The distance between a point and the origin. </returns>
        <Extension()>
        Public Function Hypotenuse(ByVal point As Drawing.PointF) As Double
            Return Hypotenuse(point.X, point.Y)
        End Function

        ''' <summary> Computes the distance between two points. </summary>
        ''' <remarks>
        ''' <para>The length is computed accurately, even in cases where x<sup>2</sup> or y<sup>2</sup>
        ''' would overflow.</para>
        ''' </remarks>
        ''' <param name="left">  The left. </param>
        ''' <param name="right"> The right. </param>
        ''' <returns>
        ''' The length of the hypotenuse, square root of (x<sup>2</sup> + y<sup>2</sup>).
        ''' </returns>
        <Extension()>
        Public Function Hypotenuse(ByVal left As Drawing.PointF, ByVal right As Drawing.PointF) As Double
            Return Hypotenuse(right.X - left.X, right.Y - left.Y)
        End Function

        ''' <summary> Compute the distance between the point and the origin. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="point"> The point. </param>
        ''' <returns> The distance between a point and the origin. </returns>
        <Extension()>
        Public Function Hypotenuse(ByVal point As System.Windows.Point) As Double
            Return Hypotenuse(point.X, point.Y)
        End Function

        ''' <summary> Computes the distance between two points. </summary>
        ''' <remarks>
        ''' <para>The length is computed accurately, even in cases where x<sup>2</sup> or y<sup>2</sup>
        ''' would overflow.</para>
        ''' </remarks>
        ''' <param name="left">  The left. </param>
        ''' <param name="right"> The right. </param>
        ''' <returns>
        ''' The length of the hypotenuse, square root of (x<sup>2</sup> + y<sup>2</sup>).
        ''' </returns>
        <Extension()>
        Public Function Hypotenuse(ByVal left As System.Windows.Point, ByVal right As System.Windows.Point) As Double
            Return Hypotenuse(right.X - left.X, right.Y - left.Y)
        End Function

#End Region


    End Module

End Namespace
