﻿Imports System.Runtime.CompilerServices
Namespace NumericExtensions
    ''' <summary> Includes extensions for numeric objects. </summary>
    ''' <remarks> (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 04/09/2009, 1.1.3386.x. </para></remarks>
    Public Module ApproximateMethods

#Region " DECIMAL "

        ''' <summary>
        ''' Returns true if the two values are within the specified absolute precision.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        ''' <returns> <c>True</c> if the two values are within the specified absolute precision. </returns>
        <Extension()>
        Public Function Approximates(ByVal value As Decimal, ByVal other As Decimal, ByVal precision As Decimal) As Boolean
            Return other.Equals(value) OrElse Math.Abs(value - other) <= precision
        End Function

        ''' <summary>
        ''' Returns true if the two values are within the specified relative precision.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="tolerance"> relative difference using precision based on the tolerance. </param>
        ''' <returns> <c>True</c> if the two values are within the specified absolute precision. </returns>
        <Extension()>
        Public Function Tolerates(ByVal value As Decimal, ByVal other As Decimal, ByVal tolerance As Decimal) As Boolean
            Return ApproximateMethods.Approximates(value, other, Epsilon(value, other, tolerance))
        End Function

        ''' <summary>
        ''' Returns true if the two values are within the specified relative precision.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">             Value. </param>
        ''' <param name="other">             The other value. </param>
        ''' <param name="significantDigits"> Defines relative precision based on the significant digits of
        '''                                  the
        '''                                  <see cref="Hypotenuse"></see> </param>
        ''' <returns> <c>True</c> if the two values are within the specified relative precision. </returns>
        <Extension()>
        Public Function Approximates(ByVal value As Decimal, ByVal other As Decimal, ByVal significantDigits As Integer) As Boolean
            Return other.Equals(value) OrElse Math.Abs(value - other) < Epsilon(value, other, significantDigits)
        End Function

        ''' <summary>
        ''' Returns the Hypotenuse of the values scaled by the specified significant digits.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">             Value. </param>
        ''' <param name="other">             The other value. </param>
        ''' <param name="significantDigits"> Defines relative precision based on the significant digits of
        '''                                  the
        '''                                  <see cref="Hypotenuse"></see> </param>
        ''' <returns> The Hypotenuse of the values scaled by the specified significant digits. </returns>
        <Extension()>
        Public Function Epsilon(ByVal value As Decimal, ByVal other As Decimal, ByVal significantDigits As Integer) As Decimal
            Return CDec(Hypotenuse(value, other) / 10 ^ (significantDigits - 1))
        End Function

        ''' <summary> Returns the Hypotenuse of the values scaled by the tolerance. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="tolerance"> The tolerance. </param>
        ''' <returns> The Hypotenuse of the values scaled by the specified significant digits. </returns>
        <Extension()>
        Public Function Epsilon(ByVal value As Decimal, ByVal other As Decimal, ByVal tolerance As Decimal) As Decimal
            Return CDec(tolerance * Hypotenuse(value, other))
        End Function

        ''' <summary>
        ''' Returns true if the first value is less than or equal the other within the specified absolute
        ''' precision.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        ''' <returns>
        ''' <c>True</c> if the first value is less than or equal the other within the specified absolute
        ''' precision.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension()>
        Public Function SmallerEquals(ByVal value As Decimal, ByVal other As Decimal, ByVal precision As Decimal) As Boolean
            Return NumericExtensions.Approximates(value, other, precision) OrElse (value < other)
        End Function

        ''' <summary>
        ''' Returns True if the first value is greater than or equal the other within the specified
        ''' absolute precision.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        ''' <returns>
        ''' <c>True</c> if the first value is greater than or equal the other within the specified
        ''' absolute precision.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension()>
        Public Function GreaterEquals(ByVal value As Decimal, ByVal other As Decimal, ByVal precision As Decimal) As Boolean
            Return NumericExtensions.Approximates(value, other, precision) OrElse (value > other)
        End Function

        ''' <summary>
        ''' Returns True if the values are unequal within the specified absolute precision.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        ''' <returns>
        ''' <c>True</c> if the values are unequal within the specified absolute precision.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension()>
        Public Function Differs(ByVal value As Decimal, ByVal other As Decimal, ByVal precision As Decimal) As Boolean
            Return Not NumericExtensions.Approximates(value, other, precision)
        End Function

        ''' <summary>
        ''' Returns True if the first value is less than the other within the specified absolute
        ''' precision.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        ''' <returns>
        ''' <c>True</c> if the first value is less than the other within the specified absolute precision.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension()>
        Public Function SmallerDiffers(ByVal value As Decimal, ByVal other As Decimal, ByVal precision As Decimal) As Boolean
            Return Not value.GreaterEquals(other, precision)
        End Function

        ''' <summary>
        ''' Returns True if the first value is greater than the other within the specified absolute
        ''' precision.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        ''' <returns>
        ''' <c>True</c> if the first value is greater than the other within the specified absolute
        ''' precision.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension()>
        Public Function GreaterDiffers(ByVal value As Decimal, ByVal other As Decimal, ByVal precision As Decimal) As Boolean
            Return Not value.SmallerEquals(other, precision)
        End Function

#End Region

#Region " DECIMAL? "

        ''' <summary>
        ''' Returns true if the absolute difference is less than or equal than
        ''' <paramref name="delta">delta</paramref>.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <param name="other"> The other value. </param>
        ''' <param name="delta"> The delta. </param>
        ''' <returns>
        ''' <c>True</c> if the absolute difference is greater than delta, <c>False</c> otherwise.
        ''' </returns>
        <Extension()>
        Public Function Approximates(ByVal value As Decimal?, ByVal other As Decimal?, ByVal delta As Decimal) As Boolean
            If value Is Nothing Then
                Return other Is Nothing
            ElseIf other Is Nothing Then
                Return False
            Else
                Return NumericExtensions.Approximates(value.Value, other.Value, delta)
            End If
        End Function

        ''' <summary>
        ''' Returns true if the two values are within the specified relative precision.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="tolerance"> relative difference using precision based on the tolerance. </param>
        ''' <returns> <c>True</c> if the two values are within the specified absolute precision. </returns>
        <Extension()>
        Public Function Tolerates(ByVal value As Decimal?, ByVal other As Decimal?, ByVal tolerance As Decimal) As Boolean
            If value Is Nothing Then
                Return other Is Nothing
            ElseIf other Is Nothing Then
                Return False
            Else
                Return ApproximateMethods.Approximates(value, other, Epsilon(value.Value, other.Value, tolerance))
            End If
        End Function

        ''' <summary>
        ''' Returns true if the absolute difference is greater than
        ''' <paramref name="delta">delta</paramref>.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <param name="other"> The other value. </param>
        ''' <param name="delta"> The delta. </param>
        ''' <returns>
        ''' <c>True</c> if the absolute difference is greater than delta, <c>False</c> otherwise.
        ''' </returns>
        <Extension()>
        Public Function Differs(ByVal value As Decimal?, ByVal other As Decimal?, ByVal delta As Decimal) As Boolean
            If value Is Nothing Then
                Return other IsNot Nothing
            ElseIf other Is Nothing Then
                Return True
            Else
                Return value.Value.Differs(other.Value, delta)
            End If
        End Function

#End Region

#Region " DOUBLE "

        ''' <summary>
        ''' Returns True if the two values are within the specified absolute precision.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        ''' <returns> <c>True</c> if the two values are within the specified absolute precision. </returns>
        <Extension()>
        Public Function Approximates(ByVal value As Double, ByVal other As Double, ByVal precision As Double) As Boolean
            Return other.Equals(value) OrElse Math.Abs(value - other) <= precision
        End Function

        ''' <summary>
        ''' Returns true if the two values are within the specified relative precision.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="tolerance"> relative difference using precision based on the tolerance. </param>
        ''' <returns> <c>True</c> if the two values are within the specified absolute precision. </returns>
        <Extension()>
        Public Function Tolerates(ByVal value As Double, ByVal other As Double, ByVal tolerance As Double) As Boolean
            Return ApproximateMethods.Approximates(value, other, Epsilon(value, other, tolerance))
        End Function

        ''' <summary>
        ''' Returns true if the two values are within the specified relative precision.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">             Value. </param>
        ''' <param name="other">             The other value. </param>
        ''' <param name="significantDigits"> Defines relative precision based on the significant digits of
        '''                                  the
        '''                                  <see cref="Hypotenuse"></see> </param>
        ''' <returns>
        ''' Returns true if the two values are within the specified relative precision.
        ''' </returns>
        <Extension()>
        Public Function Approximates(ByVal value As Double, ByVal other As Double, ByVal significantDigits As Integer) As Boolean
            Return other.Equals(value) OrElse Math.Abs(value - other) < Epsilon(value, other, significantDigits)
        End Function

        ''' <summary>
        ''' Returns the Hypotenuse of the values scaled by the specified significant digits.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">             Value. </param>
        ''' <param name="other">             The other value. </param>
        ''' <param name="significantDigits"> Defines relative precision based on the significant digits of
        '''                                  the
        '''                                  <see cref="Hypotenuse"></see> </param>
        ''' <returns> the Hypotenuse of the values scaled by the specified significant digits. </returns>
        <Extension()>
        Public Function Epsilon(ByVal value As Double, ByVal other As Double, ByVal significantDigits As Integer) As Double
            ' Return Hypotenuse(value, other) / If(significantDigits >= 15, 1.0E+16, 10 ^ (significantDigits - 1))
            Return Hypotenuse(value, other) / 10 ^ (significantDigits - 1)
        End Function

        ''' <summary> Returns the Hypotenuse of the values scaled by the tolerance. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="tolerance"> The tolerance. </param>
        ''' <returns> The Hypotenuse of the values scaled by the specified significant digits. </returns>
        <Extension()>
        Public Function Epsilon(ByVal value As Double, ByVal other As Double, ByVal tolerance As Double) As Double
            Return tolerance * Hypotenuse(value, other)
        End Function

        ''' <summary>
        ''' Returns true if the first value is less than or equal the other within the specified absolute
        ''' precision.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        ''' <returns>
        ''' <c>True</c> if the first value is less than or equal the other within the specified absolute
        ''' precision.
        ''' </returns>
        <Extension()>
        Public Function SmallerEquals(ByVal value As Double, ByVal other As Double, ByVal precision As Double) As Boolean
            Return NumericExtensions.Approximates(value, other, precision) OrElse (value < other)
        End Function

        ''' <summary>
        ''' Returns true if the first value is greater than or equal the other within the specified
        ''' absolute precision.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        ''' <returns>
        ''' <c>True</c> if the first value is greater than or equal the other within the specified
        ''' absolute precision.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension()>
        Public Function GreaterEquals(ByVal value As Double, ByVal other As Double, ByVal precision As Double) As Boolean
            Return NumericExtensions.Approximates(value, other, precision) OrElse (value > other)
        End Function

        ''' <summary>
        ''' Returns true if the values are unequal within the specified absolute precision.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        ''' <returns>
        ''' <c>True</c> if the values are unequal within the specified absolute precision.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension()>
        Public Function Differs(ByVal value As Double, ByVal other As Double, ByVal precision As Double) As Boolean
            Return Not NumericExtensions.Approximates(value, other, precision)
        End Function

        ''' <summary>
        ''' Returns true if the first value is less than the other within the specified absolute
        ''' precision.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        ''' <returns>
        ''' <c>True</c> if the first value is less than the other within the specified absolute precision.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension()>
        Public Function SmallerDiffers(ByVal value As Double, ByVal other As Double, ByVal precision As Double) As Boolean
            Return Not value.GreaterEquals(other, precision)
        End Function

        ''' <summary>
        ''' Returns true if the first value is greater than the other within the specified absolute
        ''' precision.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        ''' <returns>
        ''' <c>True</c> if the first value is greater than the other within the specified absolute
        ''' precision.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension()>
        Public Function GreaterDiffers(ByVal value As Double, ByVal other As Double, ByVal precision As Double) As Boolean
            Return Not value.SmallerEquals(other, precision)
        End Function

#End Region

#Region " DOUBLE? "

        ''' <summary>
        ''' Returns true if the absolute difference is less than or equal than
        ''' <paramref name="delta">delta</paramref>.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <param name="other"> The other value. </param>
        ''' <param name="delta"> The delta. </param>
        ''' <returns>
        ''' <c>True</c> if the absolute difference is greater than delta, <c>False</c> otherwise.
        ''' </returns>
        <Extension()>
        Public Function Approximates(ByVal value As Double?, ByVal other As Double?, ByVal delta As Double) As Boolean
            If value Is Nothing Then
                Return other Is Nothing
            ElseIf other Is Nothing Then
                Return False
            Else
                Return NumericExtensions.Approximates(value.Value, other.Value, delta)
            End If
        End Function

        ''' <summary>
        ''' Returns true if the two values are within the specified relative precision.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="tolerance"> relative difference using precision based on the tolerance. </param>
        ''' <returns> <c>True</c> if the two values are within the specified absolute precision. </returns>
        <Extension()>
        Public Function Approximates(ByVal value As Double?, ByVal other As Double?, ByVal tolerance As Single) As Boolean
            If value Is Nothing Then
                Return other Is Nothing
            ElseIf other Is Nothing Then
                Return False
            Else
                Return NumericExtensions.Tolerates(value.Value, other.Value, tolerance)
            End If
        End Function

        ''' <summary>
        ''' Returns true if the absolute difference is greater than
        ''' <paramref name="delta">delta</paramref>.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <param name="other"> The other value. </param>
        ''' <param name="delta"> The delta. </param>
        ''' <returns>
        ''' <c>True</c> if the absolute difference is greater than delta, <c>False</c> otherwise.
        ''' </returns>
        <Extension()>
        Public Function Differs(ByVal value As Double?, ByVal other As Double?, ByVal delta As Double) As Boolean
            If value Is Nothing Then
                Return other IsNot Nothing
            ElseIf other Is Nothing Then
                Return True
            Else
                Return value.Value.Differs(other.Value, delta)
            End If
        End Function

#End Region

#Region " SINGLE "

        ''' <summary>
        ''' Returns true if the values are unequal within the specified absolute precision.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        ''' <returns> <c>True</c> if the values are different; <c>False</c> otherwise. </returns>
        <System.Runtime.CompilerServices.Extension()>
        Public Function Differs(ByVal value As Single, ByVal other As Single, ByVal precision As Single) As Boolean
            Return Not NumericExtensions.Approximates(value, other, precision)
        End Function

        ''' <summary>
        ''' Returns true if the absolute difference is greater than
        ''' <paramref name="delta">delta</paramref>.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <param name="other"> The other value. </param>
        ''' <param name="delta"> The delta. </param>
        ''' <returns>
        ''' <c>True</c> if the absolute difference is greater than delta, <c>False</c> otherwise.
        ''' </returns>
        <Extension()>
        Public Function Differs(ByVal value As Single?, ByVal other As Single?, ByVal delta As Single) As Boolean
            If value Is Nothing Then
                Return other IsNot Nothing
            ElseIf other Is Nothing Then
                Return True
            Else
                Return value.Value.Differs(other.Value, delta)
            End If
        End Function

        ''' <summary>
        ''' Returns true if the two values are within the specified absolute precision.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        ''' <returns> <c>True</c> if the values are close; <c>False</c> otherwise. </returns>
        <Extension()>
        Public Function Approximates(ByVal value As Single, ByVal other As Single, ByVal precision As Single) As Boolean
            Return other.Equals(value) OrElse Math.Abs(value - other) <= precision
        End Function

        ''' <summary>
        ''' Returns true if the two values are within the specified relative precision.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="tolerance"> relative difference using precision based on the tolerance. </param>
        ''' <returns> <c>True</c> if the two values are within the specified absolute precision. </returns>
        <Extension()>
        Public Function Tolerates(ByVal value As Single, ByVal other As Single, ByVal tolerance As Single) As Boolean
            Return ApproximateMethods.Approximates(value, other, Epsilon(value, other, tolerance))
        End Function

        ''' <summary>
        ''' Returns true if the two values are within the specified relative precision.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">             Value. </param>
        ''' <param name="other">             The other value. </param>
        ''' <param name="significantDigits"> Defines relative precision based on the significant digits of
        '''                                  the <see cref="Hypotenuse"></see> </param>
        ''' <returns>
        ''' <c>True</c> if the two values are within the specified relative precision;
        ''' <c>False</c> otherwise.
        ''' </returns>
        <Extension()>
        Public Function Approximates(ByVal value As Single, ByVal other As Single, ByVal significantDigits As Integer) As Boolean
            Return other.Equals(value) OrElse Math.Abs(value - other) < Epsilon(value, other, significantDigits)
        End Function

        ''' <summary>
        ''' Returns the Hypotenuse of the values scaled by the specified significant digits.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">             Value. </param>
        ''' <param name="other">             The other value. </param>
        ''' <param name="significantDigits"> Defines relative precision based on the significant digits of
        '''                                  the <see cref="Hypotenuse"></see> </param>
        ''' <returns> the Hypotenuse of the values scaled by the specified significant digits. </returns>
        <Extension()>
        Public Function Epsilon(ByVal value As Single, ByVal other As Single, ByVal significantDigits As Integer) As Single
            Return CSng(Hypotenuse(value, other) / 10 ^ (significantDigits - 1))
        End Function

        ''' <summary> Returns the Hypotenuse of the values scaled by the tolerance. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="tolerance"> The tolerance. </param>
        ''' <returns> The Hypotenuse of the values scaled by the specified significant digits. </returns>
        <Extension()>
        Public Function Epsilon(ByVal value As Single, ByVal other As Single, ByVal tolerance As Single) As Single
            Return CSng(tolerance * Hypotenuse(value, other))
        End Function

        ''' <summary>
        ''' Returns true if the first value is smaller or equal the other within the specified absolute
        ''' precision.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        ''' <returns>
        ''' <c>True</c> if the first value is smaller or equal the other within the specified absolute
        ''' precision; <c>False</c> otherwise.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension()>
        Public Function SmallerEquals(ByVal value As Single, ByVal other As Single, ByVal precision As Single) As Boolean
            Return NumericExtensions.Approximates(value, other, precision) OrElse (value < other)
        End Function

        ''' <summary>
        ''' Returns true if the first value is greater than or equal the other within the specified
        ''' absolute precision.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        ''' <returns>
        ''' <c>True</c> if the first value is greater than or equal the other within the specified
        ''' absolute precision; <c>False</c> otherwise.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension()>
        Public Function GreaterEquals(ByVal value As Single, ByVal other As Single, ByVal precision As Single) As Boolean
            Return NumericExtensions.Approximates(value, other, precision) OrElse (value > other)
        End Function

        ''' <summary>
        ''' Returns true if the first value is less than the other within the specified absolute
        ''' precision.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        ''' <returns>
        ''' <c>True</c> if the first value is less than the other within the specified absolute precision.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension()>
        Public Function SmallerDiffers(ByVal value As Single, ByVal other As Single, ByVal precision As Single) As Boolean
            Return Not value.GreaterEquals(other, precision)
        End Function

        ''' <summary>
        ''' Returns true if the first value is greater than the other within the specified absolute
        ''' precision.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        ''' <returns>
        ''' <c>True</c> if the first value is greater than the other within the specified absolute
        ''' precision.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension()>
        Public Function GreaterDiffers(ByVal value As Single, ByVal other As Single, ByVal precision As Single) As Boolean
            Return Not value.SmallerEquals(other, precision)
        End Function

#End Region

#Region " POINT "

        ''' <summary>
        ''' Returns True if the two points are within the specified absolute precision.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        ''' <returns> <c>True</c> if the two values are within the specified absolute precision. </returns>
        <Extension()>
        Public Function Approximates(ByVal value As Windows.Point, ByVal other As Windows.Point, ByVal precision As Double) As Boolean
            Return value.Equals(other) OrElse value.Hypotenuse(other) <= precision
        End Function

        ''' <summary>
        ''' Returns true if the two points are within the specified relative precision.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="tolerance"> relative difference using precision based on the tolerance. </param>
        ''' <returns> <c>True</c> if the two values are within the specified relative precision. </returns>
        <Extension()>
        Public Function Tolerates(ByVal value As Windows.Point, ByVal other As Windows.Point, ByVal tolerance As Double) As Boolean
            Return value.Equals(other) OrElse value.Hypotenuse(other) <= tolerance * (value.Hypotenuse + other.Hypotenuse)
        End Function

#End Region

    End Module
End Namespace
