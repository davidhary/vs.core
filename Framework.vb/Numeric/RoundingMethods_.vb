﻿Imports System.Runtime.CompilerServices
Namespace NumericExtensions

    ''' <summary> Includes rounding extension methods. </summary>
    ''' <remarks> (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para> </remarks>
    Public Module RoundingMethods

#Region " DOUBLE "

        ''' <summary>
        ''' Commercial round or round away from zero as implemented using the Excel Round() function.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> value which to use in the calculation. </param>
        ''' <returns> A Double. </returns>
        <Extension()>
        Public Function CommercialRound(ByVal value As Double) As Double
            If value - Math.Floor(value) = 0.5 Then
                value = If(value > 0, value + 0.5, value - 0.5)
            Else
                value = Math.Round(value)
            End If
            Return value
        End Function

        ''' <summary>
        ''' Commercial round or round away from zero as implemented using the Excel Round() function.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">         value which to use in the calculation. </param>
        ''' <param name="decimalPlaces"> The number of decimal places beyond the most significant digit. </param>
        ''' <returns> A Double. </returns>
        <Extension()>
        Public Function CommercialRound(ByVal value As Double, decimalPlaces As Integer) As Double
            Dim decmalScale As Double = Math.Pow(10, decimalPlaces)
            value = CommercialRound(value * decmalScale) / decmalScale
            Return value
        End Function

        ''' <summary> Returns a value that is rounded up to the specified significant digit. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">           value which to use in the calculation. </param>
        ''' <param name="relativeDecimal"> The decimal digit relative to the most significant digit to
        '''                                use. </param>
        ''' <returns> A rounded up value to the specified relative digit. </returns>
        ''' <example>
        ''' Unary.Ceiling(1.123456,2) returns 1.13.<p>
        ''' Unary.Ceiling(-1.123456,2) returns -1.12.</p>
        ''' </example>
        <Extension()>
        Public Function Ceiling(ByVal value As Double, ByVal relativeDecimal As Integer) As Double

            Dim decimalDigitValue As Double

            ' check if the value is zero
            If value = 0 Then

                ' if so, return This value
                Return value

            Else

                ' get the representation of the significant digit
                decimalDigitValue = DecimalValue(value, -relativeDecimal)

                ' get the new value
                Return decimalDigitValue * System.Math.Ceiling(value / decimalDigitValue)

            End If

        End Function

        ''' <summary> Returns the number of decimal places for displaying the auto value. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> A <see cref="T:System.Double">Double</see> value. </param>
        ''' <returns> An Integer. </returns>
        <Extension()>
        Public Function DecimalPlaces(ByVal value As Double) As Integer
            Dim candidate As Integer = 0
            value = Math.IEEERemainder(Math.Abs(value), 1)
            Dim minimum As Double = value / 1000.0 : minimum = If(minimum < 0.0000001, 0.0000001, minimum)
            Do While value > minimum
                candidate += 1
                value = Math.IEEERemainder(10 * value, 1)
            Loop
            Return candidate
        End Function

        ''' <summary> Returns the decimal value of a number. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">            Value which to use in the calculation. </param>
        ''' <param name="decimalLeftShift"> Decimal power by which to shift the digit. </param>
        ''' <returns> The decimal value of the value shifted by the specified decimalPlaces. </returns>
        ''' <example>
        ''' Unary.DecimalValue(1.123,1) returns 10.<p>
        ''' Unary.DecimalValue(11.23,1) returns 100.</p><p>
        ''' Unary.DecimalValue(-1.123,1) returns 10.</p><p>
        ''' Unary.DecimalValue(0.1234,1) returns 1.</p><p>
        ''' Unary.DecimalValue(11.12,0) returns 10.</p>
        ''' </example>
        <Extension()>
        Public Function DecimalValue(ByVal value As Double, ByVal decimalLeftShift As Integer) As Double

            ' check if the value is zero
            If value = 0 Then

                ' if so, return This value
                Return 0

            Else

                ' get the representation of the most significant digit
                Return Math.Pow(10, value.Exponent() + decimalLeftShift)

            End If

        End Function

        ''' <summary>
        ''' Returns a suggested fixed format based requested most significant decimalPlaces.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">         Number to format. </param>
        ''' <param name="decimalPlaces"> The number of decimal places beyond the most significant digit. </param>
        ''' <returns> A fix format string. </returns>
        ''' <example>
        ''' Unary.FixedFormat(1.123,1) returns 0.0.<p>
        ''' Unary.FixedFormat(11.23,1) returns 0.</p><p>
        ''' Unary.FixedFormat(-1.123,1) returns 0.0.</p><p>
        ''' Unary.FixedFormat(0.1234,1) returns 0.0.</p>
        ''' </example>
        <Extension()>
        Public Function FixedFormat(ByVal value As Double, ByVal decimalPlaces As Integer) As String
            decimalPlaces = decimalPlaces - value.Exponent()
            If decimalPlaces > 0 Then
                Return "0." & New String("0"c, decimalPlaces)
            Else
                Return "0"
            End If
        End Function

        ''' <summary> Returns a value that is rounded down to the specified significant digit. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">           value which to use in the calculation. </param>
        ''' <param name="relativeDecimal"> The decimal digit relative to the most significant digit to
        '''                                use. </param>
        ''' <returns> A rounded down value to the specified relative digit. </returns>
        ''' <example>
        ''' Unary.Floor(1.123,1) returns 1.1.<p>
        ''' Unary.Floor(11.23,1) returns 11.</p><p>
        ''' Unary.Floor(-1.123,1) returns -1.3.</p><p>
        ''' Unary.Floor(0.1234,1) returns 0.12.</p><p>
        ''' Unary.Floor(11.12,0) returns 10.</p>
        ''' </example>
        <Extension()>
        Public Function Floor(ByVal value As Double, ByVal relativeDecimal As Integer) As Double

            Dim decimalDigitValue As Double

            If value = 0 Then
                ' if zero, return this value
                Return value
            Else
                ' get the representation of the significant digit
                decimalDigitValue = DecimalValue(value, -relativeDecimal)

                ' get the new value
                Return decimalDigitValue * System.Math.Floor(value / decimalDigitValue)
            End If

        End Function

        ''' <summary> Calculate the modulus (remainder) in a safe manner so that divide by zero errors are
        ''' avoided. </summary>
        ''' <param name="value">    The divisor. </param>
        ''' <param name="dividend"> The dividend. </param>
        ''' <returns> the value of the modulus, or zero for the divide-by-zero case. </returns>
        <Extension()>
        Public Function [Mod](ByVal value As Double, ByVal dividend As Double) As Double
            If dividend = 0 Then
                Return 0
            End If
            Dim temp As Double = value / dividend
            Return dividend * (temp - Math.Floor(temp))
        End Function

#End Region

#Region " SINGLE "

        ''' <summary>
        ''' Commercial round or round away from zero as implemented using the Excel Round() function.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> value which to use in the calculation. </param>
        ''' <returns> A Single. </returns>
        <Extension()>
        Public Function CommercialRound(ByVal value As Single) As Single
            Return CSng(CommercialRound(CDbl(value)))
        End Function

        ''' <summary>
        ''' Commercial round or round away from zero as implemented using the Excel Round() function.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">         value which to use in the calculation. </param>
        ''' <param name="decimalPlaces"> The number of decimal places beyond the most significant digit. </param>
        ''' <returns> A Single. </returns>
        <Extension()>
        Public Function CommercialRound(ByVal value As Single, decimalPlaces As Integer) As Single
            Return CSng(CommercialRound(CDbl(value), decimalPlaces))
        End Function

        ''' <summary> Returns a value that is rounded up to the specified significant digit. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">           value which to use in the calculation. </param>
        ''' <param name="relativeDecimal"> The decimal digit relative to the most significant digit to
        '''                                use. </param>
        ''' <returns> A rounded up value to the specified relative digit. </returns>
        ''' <example>
        ''' Unary.Ceiling(1.123456,2) returns 1.13.<p>
        ''' Unary.Ceiling(-1.123456,2) returns -1.12.</p>
        ''' </example>
        <Extension()>
        Public Function Ceiling(ByVal value As Single, ByVal relativeDecimal As Integer) As Single
            Return CSng(CType(value, Double).Ceiling(relativeDecimal))
        End Function

        ''' <summary> Returns the number of decimal places for displaying the auto value. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> A <see cref="T:System.Single">Single</see> value. </param>
        ''' <returns> An Integer. </returns>
        <Extension()>
        Public Function DecimalPlaces(ByVal value As Single) As Integer
            Return CType(value, Double).DecimalPlaces
        End Function

        ''' <summary> Returns the decimal value of a number. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">            Value which to use in the calculation. </param>
        ''' <param name="decimalLeftShift"> Decimal power by which to shift the digit. </param>
        ''' <returns> The decimal value of the value shifted by the specified decimalPlaces. </returns>
        ''' <example>
        ''' Unary.DecimalValue(1.123,1) returns 10.<p>
        ''' Unary.DecimalValue(11.23,1) returns 100.</p><p>
        ''' Unary.DecimalValue(-1.123,1) returns 10.</p><p>
        ''' Unary.DecimalValue(0.1234,1) returns 1.</p><p>
        ''' Unary.DecimalValue(11.12,0) returns 10.</p>
        ''' </example>
        <Extension()>
        Public Function DecimalValue(ByVal value As Single, ByVal decimalLeftShift As Integer) As Double
            Return CDbl(value).DecimalValue(decimalLeftShift)
        End Function

        ''' <summary>
        ''' Returns a suggested fixed format based requested most significant decimalPlaces.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">         Number to format. </param>
        ''' <param name="decimalPlaces"> The number of decimal places beyond the most significant digit. </param>
        ''' <returns> A fix format string. </returns>
        ''' <example>
        ''' Unary.FixedFormat(1.123,1) returns 0.0.<p>
        ''' Unary.FixedFormat(11.23,1) returns 0.</p><p>
        ''' Unary.FixedFormat(-1.123,1) returns 0.0.</p><p>
        ''' Unary.FixedFormat(0.1234,1) returns 0.0.</p>
        ''' </example>
        <Extension()>
        Public Function FixedFormat(ByVal value As Single, ByVal decimalPlaces As Integer) As String
            Return CDbl(value).FixedFormat(decimalPlaces)
        End Function

        ''' <summary> Returns a value that is rounded down to the specified significant digit. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">           value which to use in the calculation. </param>
        ''' <param name="relativeDecimal"> The decimal digit relative to the most significant digit to
        '''                                use. </param>
        ''' <returns> A rounded down value to the specified relative digit. </returns>
        ''' <example>
        ''' Unary.Floor(1.123,1) returns 1.1.<p>
        ''' Unary.Floor(11.23,1) returns 11.</p><p>
        ''' Unary.Floor(-1.123,1) returns -1.3.</p><p>
        ''' Unary.Floor(0.1234,1) returns 0.12.</p><p>
        ''' Unary.Floor(11.12,0) returns 10.</p>
        ''' </example>
        <Extension()>
        Public Function Floor(ByVal value As Single, ByVal relativeDecimal As Integer) As Double
            Return CDbl(value).Floor(relativeDecimal)
        End Function

        ''' <summary> Calculate the modulus (remainder) in a safe manner so that divide by zero errors are
        ''' avoided. </summary>
        ''' <param name="value">    The divisor. </param>
        ''' <param name="dividend"> The dividend. </param>
        ''' <returns> the value of the modulus, or zero for the divide-by-zero case. </returns>
        <Extension()>
        Public Function [Mod](ByVal value As Single, ByVal dividend As Single) As Double
            Return CType(value, Double).Mod(dividend)
        End Function

#End Region

    End Module

End Namespace
