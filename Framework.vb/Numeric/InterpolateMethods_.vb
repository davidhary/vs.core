﻿Imports System.Runtime.CompilerServices
Namespace NumericExtensions
    Public Module InterpolateMethods

#Region " DOUBLE "

        ''' <summary> Interpolate over the unity range. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">    The abscissa value to interpolate to an ordinate value. </param>
        ''' <param name="minValue"> The minimum range point. </param>
        ''' <param name="maxValue"> The maximum range point. </param>
        ''' <returns>
        ''' Returns the interpolated output over the specified range relative to the value over the unity
        ''' [0,1) range.
        ''' </returns>
        <Extension()>
        Public Function Interpolate(ByVal value As Double, ByVal minValue As Double, ByVal maxValue As Double) As Double
            Return value * (maxValue - minValue) + minValue
        End Function

        ''' <summary> Linearly interpolated value. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">     The abscissa value to interpolate to an ordinate value. </param>
        ''' <param name="minValue">  The first abscissa value. </param>
        ''' <param name="maxValue">  The second abscissa value. </param>
        ''' <param name="minOutput"> The first ordinate value. </param>
        ''' <param name="maxOutput"> The second ordinate value. </param>
        ''' <returns>
        ''' The abscissa value derived using linear interpolation based on a pair of coordinates.
        ''' </returns>
        <Extension()>
        Public Function Interpolate(ByVal value As Double, ByVal minValue As Double, ByVal maxValue As Double, ByVal minOutput As Double, ByVal maxOutput As Double) As Double

            ' check if we have two distinct pairs
            If maxValue = minValue Then
                ' if we have identical value values, return the
                ' average of the ordinate value
                Return 0.5 * (minOutput + maxOutput)
            Else
                Return minOutput + (value - minValue) * (maxOutput - minOutput) / (maxValue - minValue)
            End If

        End Function

        ''' <summary> Linearly interpolated value. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">  The abscissa value to interpolate to an ordinate value. </param>
        ''' <param name="point1"> The first point. </param>
        ''' <param name="point2"> The second point. </param>
        ''' <returns>
        ''' The abscissa value derived using linear interpolation based on a pair of coordinates.
        ''' </returns>
        <Extension()>
        Public Function Interpolate(ByVal value As Double, ByVal point1 As System.Drawing.PointF, ByVal point2 As System.Drawing.PointF) As Double

            ' check if we have two distinct pairs
            If point2.X = point2.X Then
                ' if we have identical X values, return the
                ' average of the ordinate value
                Return 0.5 * (point1.Y + point2.Y)
            Else
                Return point1.Y + (value - point1.X) * (point2.Y - point1.Y) / (point2.X - point1.X)
            End If

        End Function

        ''' <summary> Linearly interpolated value. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">     The abscissa value to interpolate to an ordinate value. </param>
        ''' <param name="rectangle"> The rectangle of coordinates. </param>
        ''' <returns>
        ''' The abscissa value derived using linear interpolation based on a pair of coordinates.
        ''' </returns>
        <Extension()>
        Public Function Interpolate(ByVal value As Double, ByVal rectangle As System.Drawing.RectangleF) As Double

            ' check if we have two distinct pairs
            If rectangle.Width = 0 Then
                ' if we have identical X values, return the
                ' average of the ordinate value
                Return rectangle.Y + 0.5 * rectangle.Height
            Else
                Return rectangle.Y + (value - rectangle.X) * rectangle.Height / rectangle.Width
            End If

        End Function

#End Region

#Region " SINGLE "

        ''' <summary> Interpolate over the unity range. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">    The abscissa value to interpolate to an ordinate value. </param>
        ''' <param name="minValue"> The minimum range point. </param>
        ''' <param name="maxValue"> The maximum range point. </param>
        ''' <returns>
        ''' Returns the interpolated output over the specified range relative to the value over the unity
        ''' [0,1) range.
        ''' </returns>
        <Extension()>
        Public Function Interpolate(ByVal value As Single, ByVal minValue As Single, ByVal maxValue As Single) As Double
            Return value * (maxValue - minValue) + minValue
        End Function

        ''' <summary> Linearly interpolated value. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">     The abscissa value to interpolate to an ordinate value. </param>
        ''' <param name="minValue">  The first abscissa value. </param>
        ''' <param name="maxValue">  The second abscissa value. </param>
        ''' <param name="minOutput"> The first ordinate value. </param>
        ''' <param name="maxOutput"> The second ordinate value. </param>
        ''' <returns>
        ''' The abscissa value derived using linear interpolation based on a pair of coordinates.
        ''' </returns>
        <Extension()>
        Public Function Interpolate(ByVal value As Single, ByVal minValue As Single, ByVal maxValue As Single, ByVal minOutput As Single, ByVal maxOutput As Single) As Double
            Return CType(value, Double).Interpolate(minValue, maxValue, minOutput, maxOutput)
        End Function

        ''' <summary> Linearly interpolated value. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">  The abscissa value to interpolate to an ordinate value. </param>
        ''' <param name="point1"> The first point. </param>
        ''' <param name="point2"> The second point. </param>
        ''' <returns>
        ''' The abscissa value derived using linear interpolation based on a pair of coordinates.
        ''' </returns>
        <Extension()>
        Public Function Interpolate(ByVal value As Single, ByVal point1 As System.Drawing.PointF, ByVal point2 As System.Drawing.PointF) As Double
            Return CType(value, Double).Interpolate(point1, point2)
        End Function

        ''' <summary> Linearly interpolated value. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">     The abscissa value to interpolate to an ordinate value. </param>
        ''' <param name="rectangle"> The rectangle of coordinates. </param>
        ''' <returns>
        ''' The abscissa value derived using linear interpolation based on a pair of coordinates.
        ''' </returns>
        <Extension()>
        Public Function Interpolate(ByVal value As Single, ByVal rectangle As System.Drawing.RectangleF) As Double
            Return CType(value, Double).Interpolate(rectangle)
        End Function


#End Region

    End Module
End Namespace
