﻿Imports System.Runtime.CompilerServices
Namespace NumericExtensions

    ''' <summary> Includes exponent extension methods. </summary>
    ''' <remarks> (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para> </remarks>
    Public Module ExponentMethods

#Region " DOUBLE "

        ''' <summary> Computes e<sup>x</sup>-1. </summary>
        ''' <remarks>
        ''' If x is close to 0, then e<sup>x</sup> is close to 1, and computing e<sup>x</sup>-1 by
        ''' subtracting one from e<sup>x</sup> as computed by the <see cref="Math.Exp" /> function will
        ''' be subject to severe loss of significance due to cancellation. This method maintains full
        ''' precision for all values of x by switching to a series expansion for values of x near zero.
        ''' </remarks>
        ''' <exception cref="InvalidOperationException"> Thrown when operation failed to execute. </exception>
        ''' <param name="value">             The argument. </param>
        ''' <param name="maximumIterations"> The maximum number of iterations of a series for convergence. </param>
        ''' <returns> The value of e<sup>x</sup>-1. </returns>
        <Extension()>
        Public Function ExpMinusOne(ByVal value As Double, ByVal maximumIterations As Integer) As Double
            If Math.Abs(value) < 0.125 Then
                Dim dr As Double = value
                Dim r As Double = dr
                For k As Integer = 2 To maximumIterations - 1
                    Dim r_old As Double = r
                    dr *= value / k
                    r += dr
                    If r = r_old Then
                        Return (r)
                    End If
                Next k
                Throw New InvalidOperationException()
            Else
                Return (Math.Exp(value) - 1.0)
            End If
        End Function

        ''' <summary> Returns the exponent. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> A <see cref="T:System.Double">Double</see> value. </param>
        ''' <returns> The most significant decade of the value. </returns>
        ''' <example>
        ''' NumericExtensions.Exponent(1.12) returns 0.<p>
        ''' Unary.Decade(11.23) returns 1.</p><p>
        ''' NumericExtensions.Exponent(-1.123) returns 0.</p><p>
        ''' NumericExtensions.Exponent(0.1234) returns -1</p>
        ''' </example>
        ''' <seealso cref="Decimal"/>
        <Extension()>
        Public Function Exponent(ByVal value As Double) As Integer

            ' check if the value is zero
            If value = 0 Then

                ' if so, return This value
                Return 0

            Else

                ' get the exponent based on the most significant digit.
                '     Return Convert.ToInt32(System.Math.Floor(System.Math.Log(System.Math.Abs(value))) / System.Math.Log(10.0#))
                Return Convert.ToInt32(System.Math.Floor(System.Math.Abs(value).Log10))
            End If

        End Function

        ''' <summary> Returns the exponent. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">               A <see cref="T:System.Double">Double</see> value. </param>
        ''' <param name="useEngineeringScale"> true to use engineering scale. </param>
        ''' <returns> The most significant decade of the value. </returns>
        ''' <example>
        ''' NumericExtensions.Exponent(11.12) returns 0.<p>
        ''' Unary.Decade(111.23) returns 1.</p><p>
        ''' NumericExtensions.Exponent(1111.123) returns 3.</p><p>
        ''' NumericExtensions.Exponent(-11.12) returns 0.</p><p>
        ''' Unary.Decade(-111.23) returns 1.</p><p>
        ''' NumericExtensions.Exponent(-1111.123) returns 3.</p><p>
        ''' NumericExtensions.Exponent(0.1234) returns 0</p><p>
        ''' NumericExtensions.Exponent(0.01234) returns 0</p><p>
        ''' NumericExtensions.Exponent(0.0001234) returns -3</p><p>
        ''' NumericExtensions.Exponent(-0.1234) returns 0</p><p>
        ''' NumericExtensions.Exponent(-0.01234) returns 0</p><p>
        ''' NumericExtensions.Exponent(-0.001234) returns -3</p><p>
        ''' NumericExtensions.Exponent(-0.0001234) returns -3</p>
        ''' </example>
        <Extension()>
        Public Function Exponent(ByVal value As Double, ByVal useEngineeringScale As Boolean) As Integer

            ' check if the value is zero
            If value = 0 Then

                ' if so, return This value
                Return 0

            Else

                If useEngineeringScale Then
                    ' Use a power of 10 that is a multiple of 3 (engineering scale)
                    Return 3 * (Exponent(value) \ 3)
                Else
                    Return Exponent(value)
                End If

            End If

        End Function

        ''' <summary> Calculate a base 10 logarithm in a safe manner to avoid math exceptions. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> The value for which the logarithm is to be calculated. </param>
        ''' <returns>
        ''' The value of the logarithm, or 0 if the <paramref name="value"/>
        ''' argument was negative or zero.
        ''' </returns>
        <Extension()>
        Public Function Log10(ByVal value As Double) As Double
            Return Math.Log10(If(value < Double.Epsilon, Double.Epsilon, value))
        End Function

        ''' <summary> Raises an argument to an integer power. </summary>
        ''' <remarks>
        ''' <para>Low integer powers can be computed by optimized algorithms much faster than the general
        ''' algorithm for an arbitrary real power employed by
        ''' <see cref="T:System.Math.Pow"/>.</para>
        ''' </remarks>
        ''' <param name="value"> The argument. </param>
        ''' <param name="power"> The power. </param>
        ''' <returns> The value of x<sup>n</sup>. </returns>
        <Extension()>
        Public Function Pow(ByVal value As Double, ByVal power As Integer) As Double

            If power < 0 Then
                Return (1.0 / Pow(value, -power))
            End If

            Select Case power
                Case 0
                    ' we follow convention that 0^0 = 1
                    Return (1.0)
                Case 1
                    Return (value)
                Case 2
                    ' 1 multiply
                    Return (value * value)
                Case 3
                    ' 2 multiplies
                    Return (value * value * value)
                Case 4
                    ' 2 multiplies
                    Dim x2 As Double = value * value
                    Return (x2 * x2)
                Case 5
                    ' 3 multiplies
                    Dim x2 As Double = value * value
                    Return (x2 * x2 * value)
                Case 6
                    ' 3 multiplies
                    Dim x2 As Double = value * value
                    Return (x2 * x2 * x2)
                Case 7
                    ' 4 multiplies
                    Dim x3 As Double = value * value * value
                    Return (x3 * x3 * value)
                Case 8
                    ' 3 multiplies
                    Dim x2 As Double = value * value
                    Dim x4 As Double = x2 * x2
                    Return (x4 * x4)
                Case 9
                    ' 4 multiplies
                    Dim x3 As Double = value * value * value
                    Return (x3 * x3 * x3)
                Case 10
                    ' 4 multiplies
                    Dim x2 As Double = value * value
                    Dim x4 As Double = x2 * x2
                    Return (x4 * x4 * x2)
                Case Else
                    Return (Math.Pow(value, power))
            End Select

        End Function

        ''' <summary> An method for squaring. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <returns> System.Double. </returns>
        <Extension()>
        Public Function Squared(ByVal value As Double) As Double
            Return (value * value)
        End Function

        ''' <summary> An method for cubing. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <returns> System.Double. </returns>
        <Extension()>
        Public Function Cubed(ByVal value As Double) As Double
            Return (value * value * value)
        End Function

#End Region

#Region " SINGLE "

        ''' <summary> Returns the exponent. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> A <see cref="T:System.Single">Single</see> value. </param>
        ''' <returns> The most significant decade of the value. </returns>
        ''' <example>
        ''' NumericExtensions.Exponent(1.12) returns 0.<p>
        ''' Unary.Decade(11.23) returns 1.</p><p>
        ''' NumericExtensions.Exponent(-1.123) returns 0.</p><p>
        ''' NumericExtensions.Exponent(0.1234) returns -1</p>
        ''' </example>
        ''' <seealso cref="Decimal"/>
        <Extension()>
        Public Function Exponent(ByVal value As Single) As Integer
            Return CDbl(value).Exponent
        End Function

        ''' <summary> Returns the exponent. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">               A <see cref="T:System.Single">Single</see> value. </param>
        ''' <param name="useEngineeringScale"> true to use engineering scale. </param>
        ''' <returns> The most significant decade of the value. </returns>
        ''' <example>
        ''' NumericExtensions.Exponent(11.12) returns 0.<p>
        ''' Unary.Decade(111.23) returns 1.</p><p>
        ''' NumericExtensions.Exponent(1111.123) returns 3.</p><p>
        ''' NumericExtensions.Exponent(-11.12) returns 0.</p><p>
        ''' Unary.Decade(-111.23) returns 1.</p><p>
        ''' NumericExtensions.Exponent(-1111.123) returns 3.</p><p>
        ''' NumericExtensions.Exponent(0.1234) returns 0</p><p>
        ''' NumericExtensions.Exponent(0.01234) returns 0</p><p>
        ''' NumericExtensions.Exponent(0.0001234) returns -3</p><p>
        ''' NumericExtensions.Exponent(-0.1234) returns 0</p><p>
        ''' NumericExtensions.Exponent(-0.01234) returns 0</p><p>
        ''' NumericExtensions.Exponent(-0.001234) returns -3</p><p>
        ''' NumericExtensions.Exponent(-0.0001234) returns -3</p>
        ''' </example>
        <Extension()>
        Public Function Exponent(ByVal value As Single, ByVal useEngineeringScale As Boolean) As Integer
            Return CDbl(value).Exponent(useEngineeringScale)
        End Function

        ''' <summary> Calculate a base 10 logarithm in a safe manner to avoid math exceptions. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> The value for which the logarithm is to be calculated. </param>
        ''' <returns>
        ''' The value of the logarithm, or 0 if the <paramref name="value"/>
        ''' argument was negative or zero.
        ''' </returns>
        <Extension()>
        Public Function Log10(ByVal value As Single) As Double
            Return CType(value, Double).Log10()
        End Function

        ''' <summary> Raises an argument to an integer power. </summary>
        ''' <remarks>
        ''' <para>Low integer powers can be computed by optimized algorithms much faster than the general
        ''' algorithm for an arbitrary real power employed by
        ''' <see cref="T:System.Math.Pow"/>.</para>
        ''' </remarks>
        ''' <param name="value"> The argument. </param>
        ''' <param name="power"> The power. </param>
        ''' <returns> The value of x<sup>n</sup>. </returns>
        <Extension()>
        Public Function Pow(ByVal value As Single, ByVal power As Integer) As Double
            Return CType(value, Double).Pow(power)
        End Function

        ''' <summary> An method for squaring. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <returns> System.Double. </returns>
        <Extension()>
        Public Function Squared(ByVal value As Single) As Double
            Return (value * value)
        End Function

        ''' <summary> An method for cubing. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <returns> System.Double. </returns>
        <Extension()>
        Public Function Cubed(ByVal value As Single) As Double
            Return (value * value * value)
        End Function


#End Region

    End Module

End Namespace
