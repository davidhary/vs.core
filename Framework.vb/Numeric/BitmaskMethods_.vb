﻿Imports System.Runtime.CompilerServices
Namespace NumericExtensions

    ''' <summary> Bitmask extension methods. </summary>
    ''' <remarks> (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para> </remarks>
    Public Module BitmaskMethods

        ''' <summary> Is any bit on. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        '''                                      illegal values. </exception>
        ''' <param name="status">  The status. </param>
        ''' <param name="bitmask"> The bit mask. </param>
        ''' <returns> A Boolean. </returns>
        <Extension>
        Public Function IsAnyBitOn(ByVal status As Integer, ByVal bitmask As Integer) As Boolean
            If bitmask <= 0 Then Throw New ArgumentException("Bitmask must be greater than 0", NameOf(bitmask))
            Return 0 <> (status And bitmask)
        End Function

        ''' <summary> Query if all bitmask bits in 'status' are on. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        '''                                      illegal values. </exception>
        ''' <param name="status">  The status. </param>
        ''' <param name="bitmask"> The bit mask. </param>
        ''' <returns> A Boolean. </returns>
        <Extension>
        Public Function AreAllBitsOn(ByVal status As Integer, ByVal bitmask As Integer) As Boolean
            If bitmask <= 0 Then Throw New ArgumentException("Bitmask must be greater than 0", NameOf(bitmask))
            Return bitmask = (status And bitmask)
        End Function

        ''' <summary> Return the masked status value. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        '''                                      illegal values. </exception>
        ''' <param name="status">  The status. </param>
        ''' <param name="bitmask"> The bit mask. </param>
        ''' <returns> An Integer? </returns>
        <Extension>
        Public Function MaskedValue(ByVal status As Integer, ByVal bitmask As Integer) As Integer
            If bitmask <= 0 Then Throw New ArgumentException("Bitmask must be greater than 0", NameOf(bitmask))
            Return status And bitmask
        End Function

        ''' <summary> Is any bit on. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        '''                                      illegal values. </exception>
        ''' <param name="status">  The status. </param>
        ''' <param name="bitmask"> The bit mask. </param>
        ''' <returns> A Boolean. </returns>
        <Extension>
        Public Function IsAnyBitOn(ByVal status As Long, ByVal bitmask As Long) As Boolean
            If bitmask <= 0 Then Throw New ArgumentException("Bitmask must be greater than 0", NameOf(bitmask))
            Return 0 <> (status And bitmask)
        End Function

        ''' <summary> Query if all bitmask bits in 'status' are on. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        '''                                      illegal values. </exception>
        ''' <param name="status">  The status. </param>
        ''' <param name="bitmask"> The bit mask. </param>
        ''' <returns> A Boolean. </returns>
        <Extension>
        Public Function AreAllBitsOn(ByVal status As Long, ByVal bitmask As Long) As Boolean
            If bitmask <= 0 Then Throw New ArgumentException("Bitmask must be greater than 0", NameOf(bitmask))
            Return bitmask = (status And bitmask)
        End Function

        ''' <summary> Return the masked status value. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        '''                                      illegal values. </exception>
        ''' <param name="status">  The status. </param>
        ''' <param name="bitmask"> The bit mask. </param>
        ''' <returns> An Long? </returns>
        <Extension>
        Public Function MaskedValue(ByVal status As Long, ByVal bitmask As Long) As Long
            If bitmask <= 0 Then Throw New ArgumentException("Bitmask must be greater than 0", NameOf(bitmask))
            Return status And bitmask
        End Function

    End Module

End Namespace

