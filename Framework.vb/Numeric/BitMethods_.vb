﻿Imports System.Runtime.CompilerServices
Namespace NumericExtensions

    ''' <summary> Bit extension methods. </summary>
    ''' <remarks> (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para> </remarks>
    Public Module BitMethods

        ''' <summary> Queries if 'status' bit is on. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="status"> The status. </param>
        ''' <param name="bit">    The bit. </param>
        ''' <returns> <c>true</c> if bit; otherwise <c>false</c> </returns>
        <Extension>
        Public Function IsBit(ByVal status As Long, ByVal bit As Integer) As Boolean
            Return (1L And (status >> bit)) = 1L
        End Function

        ''' <summary>
        ''' Toggles the bits specified in the <paramref name="bit"/> based on the
        ''' <paramref name="switch"/>.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="status"> The status. </param>
        ''' <param name="bit">    The value. </param>
        ''' <param name="switch"> True to set; otherwise, clear. </param>
        ''' <returns> A Long. </returns>
        <Extension>
        Public Function Toggle(ByVal status As Long, ByVal bit As Integer, ByVal switch As Boolean) As Long
            status = If(switch, status Or (1L << bit), status And (Not (1L << bit)))
            Return status
        End Function

    End Module

End Namespace

