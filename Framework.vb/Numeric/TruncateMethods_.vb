﻿Imports System.Runtime.CompilerServices
Namespace NumericExtensions
    ''' <summary> A truncate extensions. </summary>
    ''' <remarks> (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para> </remarks>
    Public Module TruncateMethods

#Region " INTEGER "

        ''' <summary> Truncates a value to the desired precision. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> Extended value. </param>
        ''' <returns> An Int32. </returns>
        <Extension()>
        Public Function Truncate(ByVal value As Int32) As Int16

            value = value And UInt16.MaxValue
            If value > Int16.MaxValue Then
                value -= UInt16.MaxValue + 1 ' -65536
            End If
            Return Convert.ToInt16(value)

        End Function

#End Region

#Region " LONG "

        ''' <summary> Truncates a value to the desired precision. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> Extended value. </param>
        ''' <returns> An Int32. </returns>
        <Extension()>
        Public Function Truncate(ByVal value As Int64) As Int32

            value = value And UInt32.MaxValue
            If value > Int32.MaxValue Then
                value -= UInt32.MaxValue + 1
            End If
            Return Convert.ToInt32(value)

        End Function

#End Region

#Region " SHORT "

        ''' <summary> Truncates a value to the desired precision. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> Extended value. </param>
        ''' <returns> A Byte. </returns>
        <Extension()>
        Public Function Truncate(ByVal value As Int16) As Byte

            Dim maxValue As Short = 2S * Byte.MaxValue - 1S
            value = value And maxValue
            If value > Int16.MaxValue Then
                value -= maxValue + 1S ' -65536
            End If
            Return Convert.ToByte(value)

        End Function

#End Region

    End Module

End Namespace
