Imports System.Runtime.CompilerServices
Imports System.ComponentModel
Imports System.Collections.Specialized

Namespace EventHandlerExtensions

    Partial Public Module Methods

        ''' <summary>
        ''' Asynchronously notifies an <see cref="EventHandler(Of System.EventArgs)">Event</see>.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="handler"> The event handler. </param>
        ''' <param name="sender">  The sender of the event. </param>
        <Extension>
        Public Sub AsyncNotify(ByVal handler As EventHandler(Of System.EventArgs), ByVal sender As Object)
            Dim evt As EventHandler(Of System.EventArgs) = handler
            If evt IsNot Nothing Then Methods.AsyncNotifyThis(evt, Methods.CurrentSyncContext, sender, System.EventArgs.Empty)
        End Sub

        ''' <summary>
        ''' Asynchronously notifies an <see cref="EventHandler(Of System.EventArgs)">Event</see>.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="handler"> The handler. </param>
        ''' <param name="context"> The synchronization context. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       T event information. </param>
        <Extension>
        Public Sub AsyncNotify(Of TEventArgs As EventArgs)(ByVal handler As EventHandler(Of TEventArgs), ByVal context As Threading.SynchronizationContext, ByVal sender As Object, ByVal e As TEventArgs)
            Dim evt As EventHandler(Of TEventArgs) = handler
            If evt IsNot Nothing Then Methods.AsyncNotifyThis(evt, context, sender, e)
        End Sub

        ''' <summary>
        ''' Asynchronously notifies an <see cref="EventHandler(Of System.EventArgs)">Event</see>.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="evt">     The handler. </param>
        ''' <param name="context"> The synchronization context. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       T event information. </param>
        Private Sub AsyncNotifyThis(Of TEventArgs As EventArgs)(ByVal evt As EventHandler(Of TEventArgs), ByVal context As Threading.SynchronizationContext, ByVal sender As Object, ByVal e As TEventArgs)
            context.Post(Sub() evt(sender, e), Nothing)
        End Sub

        ''' <summary>
        ''' Asynchronously notifies an <see cref="PropertyChangedEventHandler">Event</see>.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="handler"> The handler. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       T event information. </param>
        <Extension>
        Public Sub AsyncNotify(ByVal handler As PropertyChangedEventHandler, ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
            Dim evt As PropertyChangedEventHandler = handler
            If evt IsNot Nothing Then Methods.AsyncNotifyThis(evt, Methods.CurrentSyncContext, sender, e)
        End Sub

        ''' <summary>
        ''' Asynchronously notifies an <see cref="PropertyChangedEventHandler">Event</see>.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="handler"> The handler. </param>
        ''' <param name="context"> The synchronization context. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       Property changed event information. </param>
        <Extension>
        Public Sub AsyncNotify(ByVal handler As PropertyChangedEventHandler, ByVal context As Threading.SynchronizationContext, ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
            Dim evt As PropertyChangedEventHandler = handler
            If evt IsNot Nothing Then Methods.AsyncNotifyThis(evt, context, sender, e)
        End Sub

        ''' <summary>
        ''' Asynchronously notifies an <see cref="PropertyChangedEventHandler">Event</see>.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="evt">     The handler. </param>
        ''' <param name="context"> The synchronization context. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       Property changed event information. </param>
        Private Sub AsyncNotifyThis(ByVal evt As PropertyChangedEventHandler, ByVal context As Threading.SynchronizationContext, ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
            context.Post(Sub() evt(sender, e), Nothing)
        End Sub

        ''' <summary>
        ''' Asynchronously notifies an <see cref="PropertyChangingEventHandler">Event</see>.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="handler"> The handler. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       T event information. </param>
        <Extension>
        Public Sub AsyncNotify(ByVal handler As PropertyChangingEventHandler, ByVal sender As Object, ByVal e As PropertyChangingEventArgs)
            Dim evt As PropertyChangingEventHandler = handler
            If evt IsNot Nothing Then Methods.AsyncNotifyThis(evt, Methods.CurrentSyncContext, sender, e)
        End Sub

        ''' <summary>
        ''' Asynchronously notifies an <see cref="PropertyChangingEventHandler">Event</see>.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="handler"> The handler. </param>
        ''' <param name="context"> The synchronization context. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       Property Changing event information. </param>
        <Extension>
        Public Sub AsyncNotify(ByVal handler As PropertyChangingEventHandler, ByVal context As Threading.SynchronizationContext, ByVal sender As Object, ByVal e As PropertyChangingEventArgs)
            Dim evt As PropertyChangingEventHandler = handler
            If evt IsNot Nothing Then Methods.AsyncNotifyThis(evt, context, sender, e)
        End Sub

        ''' <summary>
        ''' Asynchronously notifies an <see cref="PropertyChangingEventHandler">Event</see>.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="evt">     The handler. </param>
        ''' <param name="context"> The synchronization context. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       Property Changing event information. </param>
        Private Sub AsyncNotifyThis(ByVal evt As PropertyChangingEventHandler, ByVal context As Threading.SynchronizationContext, ByVal sender As Object, ByVal e As PropertyChangingEventArgs)
            context.Post(Sub() evt(sender, e), Nothing)
        End Sub

        ''' <summary>
        ''' Asynchronously notifies an <see cref="NotifyCollectionChangedEventHandler">Event</see>.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="handler"> The handler. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       T event information. </param>
        <Extension>
        Public Sub AsyncNotify(ByVal handler As NotifyCollectionChangedEventHandler, ByVal sender As Object, ByVal e As NotifyCollectionChangedEventArgs)
            Dim evt As NotifyCollectionChangedEventHandler = handler
            If evt IsNot Nothing Then Methods.AsyncNotifyThis(evt, Methods.CurrentSyncContext, sender, e)
        End Sub

        ''' <summary>
        ''' Asynchronously notifies an <see cref="NotifyCollectionChangedEventHandler">Event</see>.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="handler"> The handler. </param>
        ''' <param name="context"> The synchronization context. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       Property changed event information. </param>
        <Extension>
        Public Sub AsyncNotify(ByVal handler As NotifyCollectionChangedEventHandler, ByVal context As Threading.SynchronizationContext, ByVal sender As Object, ByVal e As NotifyCollectionChangedEventArgs)
            Dim evt As NotifyCollectionChangedEventHandler = handler
            If evt IsNot Nothing Then Methods.AsyncNotifyThis(evt, context, sender, e)
        End Sub

        ''' <summary>
        ''' Asynchronously notifies an <see cref="NotifyCollectionChangedEventHandler">Event</see>.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="evt">     The handler. </param>
        ''' <param name="context"> The synchronization context. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       Property changed event information. </param>
        Private Sub AsyncNotifyThis(ByVal evt As NotifyCollectionChangedEventHandler, ByVal context As Threading.SynchronizationContext, ByVal sender As Object, ByVal e As NotifyCollectionChangedEventArgs)
            context.Post(Sub() evt(sender, e), Nothing)
        End Sub

    End Module

End Namespace

