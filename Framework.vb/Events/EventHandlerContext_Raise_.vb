Imports System.Threading

Partial Public Class EventHandlerContext(Of TEventArgs As EventArgs)

#Region " ACTIVE CONTEXT "

    ''' <summary> Returns the current synchronization context. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <returns> A Threading.SynchronizationContext. </returns>
    Private Shared Function CurrentSyncContext() As Threading.SynchronizationContext
        If Threading.SynchronizationContext.Current Is Nothing Then
            Threading.SynchronizationContext.SetSynchronizationContext(New Threading.SynchronizationContext)
        End If
        Return Threading.SynchronizationContext.Current
    End Function

#Disable Warning IDE0051 ' Remove unused private members

    ''' <summary> Gets a context for the active. </summary>
    ''' <value> The active context. </value>
    Private ReadOnly Property ActiveContext As SynchronizationContext
#Enable Warning IDE0051 ' Remove unused private members
        Get
            Return If(Me.Context, EventHandlerContext(Of TEventArgs).CurrentSyncContext)
        End Get
    End Property

#End Region

#Region " POST or Invoke / SEND or Invoke "

    ''' <summary>
    ''' Asynchronously raises (Posts) the <see cref="EventHandler(Of TEventArgs)"/>. It does all the
    ''' checking to see if the SynchronizationContext is nothing or not, and invokes the delegate
    ''' accordingly. Note that, unlike the synchronous Send action, although the event is posted on
    ''' the synchronization context, cross thread exceptions still occurred, ostensibly, due to the
    ''' changing of the sync context between the time the action was elicited and the time it was
    ''' actually invoked.
    ''' </summary>
    ''' <remarks> David, 7/27/2020. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      event information. </param>
    Public Sub Post(ByVal sender As Object, ByVal e As TEventArgs)
        Dim evt As EventHandler(Of TEventArgs) = Me.Handler
        If evt IsNot Nothing Then
            Dim context As SynchronizationContext = Me.Context
            If context Is Nothing Then
                ' if not UI or active context, invoke the event
                evt.Invoke(sender, e)
            Else
                context.Post(Sub() evt.Invoke(sender, e), Nothing)
            End If
        End If
    End Sub

    ''' <summary>
    ''' Synchronously raises (sends) the <see cref="EventHandler(Of TEventArgs)"/>. It does all the
    ''' checking to see if the SynchronizationContext is nothing or not, and invokes the delegate
    ''' accordingly.
    ''' </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property changed event information. </param>
    Public Sub Send(ByVal sender As Object, ByVal e As TEventArgs)
        Dim evt As EventHandler(Of TEventArgs) = Me.Handler
        If evt IsNot Nothing Then
            Dim context As SynchronizationContext = Me.Context
            If Me.Context Is Nothing Then
                ' if not UI or active context, invoke the event
                evt.Invoke(sender, e)
            Else
                ' if UI or active context, send
                context.Send(Sub() evt.Invoke(sender, e), Nothing)
            End If
        End If
    End Sub

#End Region

End Class

