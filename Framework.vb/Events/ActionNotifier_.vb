Imports System.Threading.Tasks

''' <summary> Action notifier. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 4/26/2017 </para>
''' </remarks>
Public Class ActionNotifier(Of T)

#Region " CONSTRUCTION "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    Public Sub New()
        MyBase.New()
        Me.NotifyAsyncActions = New List(Of Func(Of T, Task))()
        Me.ActionContexts = New ActionContextCollection(Of T)
    End Sub

#End Region

#Region " SYNC NOTIFIERS "

    ''' <summary> Gets or sets the action contexts. </summary>
    ''' <value> The action contexts. </value>
    Private ReadOnly Property ActionContexts As New ActionContextCollection(Of T)

    ''' <summary> Event queue for all listeners interested in Notified events. </summary>
    Public Custom Event Notified As Action(Of T)
        AddHandler(ByVal value As Action(Of T))
            Me.ActionContexts.Add(New ActionContext(Of T)(value))
        End AddHandler
        RemoveHandler(ByVal value As Action(Of T))
            Me.ActionContexts.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(ByVal argument As T)
            Me.ActionContexts.Raise(argument)
        End RaiseEvent
    End Event

    ''' <summary> Asynchronous invoke. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="argument"> The action argument. </param>
    Public Sub AsyncInvoke(ByVal argument As T)
        Me.ActionContexts.AsyncInvoke(argument)
    End Sub

    ''' <summary> Synchronization invoke. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="argument"> The action argument. </param>
    Public Sub SyncInvoke(ByVal argument As T)
        Me.ActionContexts.SyncInvoke(argument)
    End Sub

    ''' <summary>
    ''' Executes the given operation on a different thread, and waits for the result.
    ''' </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="argument"> The action argument. </param>
    Public Sub Invoke(ByVal argument As T)
        Me.ActionContexts.Invoke(argument)
    End Sub

    ''' <summary> Registers an action to invoke. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="action"> The action. </param>
    Public Sub Register(ByVal action As Action(Of T))
        AddHandler Me.Notified, action
    End Sub

    ''' <summary> Unregisters an action. </summary>
    ''' <remarks> David, 2020-08-26. </remarks>
    ''' <param name="action"> The action. </param>
    Public Sub Unregister(ByVal action As Action(Of T))
        RemoveHandler Me.Notified, action
    End Sub

#End Region

#Region " ASYNC TASK NOTIFIERS "

    ''' <summary> Gets or sets the notify asynchronous actions. </summary>
    ''' <value> The notify asynchronous actions. </value>
    Private ReadOnly Property NotifyAsyncActions As New List(Of Func(Of T, Task))()

    ''' <summary> Executes the asynchronous on a different thread, and waits for the result. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="argument"> The action argument. </param>
    ''' <returns> A Task. </returns>
    Public Async Function InvokeAsync(ByVal argument As T) As Task
        Me.Invoke(argument)
        For Each callback As Func(Of T, Task) In Me.NotifyAsyncActions
            Await callback(argument)
        Next callback
    End Function

    ''' <summary> Registers this object. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="action"> The action. </param>
    Public Sub Register(ByVal action As Func(Of T, Task))
        Me.NotifyAsyncActions.Add(action)
    End Sub

#End Region

End Class

