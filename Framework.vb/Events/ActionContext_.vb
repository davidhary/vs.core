Imports System.Threading

''' <summary> Executes actions on a context. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 1/29/2019 </para>
''' </remarks>
Public Class ActionContext(Of T)

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="handler"> Event queue for all listeners interested in Action Handler events. </param>
    Public Sub New(ByVal handler As Action(Of T))
        Me.Handler = handler
        Me.Context = SynchronizationContext.Current
    End Sub

    ''' <summary> Gets the synchronization context. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The context. </value>
    Public ReadOnly Property Context As SynchronizationContext

    ''' <summary> Event queue for all listeners interested in Action Handler events. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The handler. </value>
    Public ReadOnly Property Handler As Action(Of T)

    ''' <summary> Returns the current synchronization context. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown if the current synchronization thread is
    '''                                              null. </exception>
    ''' <returns> A Threading.SynchronizationContext. </returns>
    Private Shared Function CurrentSyncContext() As Threading.SynchronizationContext
        If Threading.SynchronizationContext.Current Is Nothing Then
            Threading.SynchronizationContext.SetSynchronizationContext(New Threading.SynchronizationContext)
        End If
        If Threading.SynchronizationContext.Current Is Nothing Then
            Throw New InvalidOperationException("Current Synchronization Context not set;. Must be set before starting the thread.")
        End If
        Return Threading.SynchronizationContext.Current
    End Function

    ''' <summary> Gets a context for the active. </summary>
    ''' <value> The active context. </value>
    Private ReadOnly Property ActiveContext As SynchronizationContext
        Get
            Return If(Me.Context, ActionContext(Of T).CurrentSyncContext)
        End Get
    End Property

    ''' <summary> Asynchronous invoke. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="argument"> The action argument. </param>
    Public Sub AsyncInvoke(ByVal argument As T)
        Dim evt As Action(Of T) = Me.Handler
        If evt IsNot Nothing Then Me.ActiveContext.Post(Sub() evt(argument), Nothing)
    End Sub

    ''' <summary> Synchronization invoke. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="argument"> The action argument. </param>
    Public Sub SyncInvoke(ByVal argument As T)
        Dim evt As Action(Of T) = Me.Handler
        If evt IsNot Nothing Then Me.ActiveContext.Send(Sub() evt(argument), argument)
    End Sub

    ''' <summary>
    ''' Executes the given operation on a different thread, and waits for the result.
    ''' </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="argument"> The action argument. </param>
    Public Sub Invoke(ByVal argument As T)
        Dim evt As Action(Of T) = Me.Handler
        evt?.Invoke(argument)
    End Sub

End Class

''' <summary> A collection of <see cref="ActionContext(Of T)"/>. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 1/29/2019 </para>
''' </remarks>
Public Class ActionContextCollection(Of T)
    Inherits List(Of ActionContext(Of T))

    ''' <summary> Executes (Sync Invokes) the event handler action. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="argument"> The action argument. </param>
    Public Sub Raise(ByVal argument As T)
        For Each act As ActionContext(Of T) In Me : act?.SyncInvoke(argument) : Next
    End Sub

    ''' <summary> Asynchronous invoke. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="argument"> The action argument. </param>
    Public Sub AsyncInvoke(ByVal argument As T)
        For Each act As ActionContext(Of T) In Me : act?.AsyncInvoke(argument) : Next
    End Sub

    ''' <summary> Synchronization invoke. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="argument"> The action argument. </param>
    Public Sub SyncInvoke(ByVal argument As T)
        For Each act As ActionContext(Of T) In Me : act?.SyncInvoke(argument) : Next
    End Sub

    ''' <summary>
    ''' Executes the given operation on a different thread, and waits for the result.
    ''' </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="argument"> The action argument. </param>
    Public Sub Invoke(ByVal argument As T)
        For Each act As ActionContext(Of T) In Me : act?.Invoke(argument) : Next
    End Sub

    ''' <summary> Looks up a given key to find its associated last index. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> An Integer. </returns>
    Private Function LookupLastIndex(ByVal value As Action(Of T)) As Integer
        Return Me.ToList.FindLastIndex(Function(x) x.Handler.Equals(value))
    End Function

    ''' <summary> Removes the value described by value. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub RemoveValue(ByVal value As Action(Of T))
        If Me.Any Then
            Dim lastIndex As Integer = Me.LookupLastIndex(value)
            If lastIndex >= 0 AndAlso lastIndex < Me.Count AndAlso Me.ElementAt(lastIndex).Handler.Equals(value) Then Me.RemoveAt(lastIndex)
        End If
    End Sub

    ''' <summary> Removes all. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    Public Overloads Sub RemoveAll()
        Do While Me.Count > 0
            Me.RemoveAt(0)
        Loop
    End Sub

End Class
