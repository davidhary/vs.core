Namespace EventHandlerExtensions

    Partial Public Module Methods

        ''' <summary> Returns the current synchronization context. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="InvalidOperationException"> Thrown if the current synchronization thread is
        '''                                              null. </exception>
        ''' <returns> A Threading.SynchronizationContext. </returns>
        Private Function CurrentSyncContext() As Threading.SynchronizationContext
            If Threading.SynchronizationContext.Current Is Nothing Then
                Threading.SynchronizationContext.SetSynchronizationContext(New Threading.SynchronizationContext)
            End If
            If Threading.SynchronizationContext.Current Is Nothing Then
                Throw New InvalidOperationException("Current Synchronization Context not set;. Must be set before starting the thread.")
            End If
            Return Threading.SynchronizationContext.Current
        End Function

    End Module

End Namespace
