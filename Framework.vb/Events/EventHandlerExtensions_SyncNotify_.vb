Imports System.Collections.Specialized
Imports System.ComponentModel
Imports System.Runtime.CompilerServices

Namespace EventHandlerExtensions

    Partial Public Module Methods

        ''' <summary>
        ''' Synchronously notifies an <see cref="EventHandler(Of System.EventArgs)">Event</see>.
        ''' </summary>
        ''' <remarks>
        ''' <para>
        ''' 20190111: Based on new benchmarks.; the dynamic post is best when handling long events. For
        ''' property changes involving minimal front end duration, a simple post will do.</para><para>
        ''' Also, note that calling the Windows Form or Dispatcher (not tested yet) Do Events from within
        ''' the event handler, 'tells' the synchronization context to call the next event on the
        ''' invocation list. </para>
        ''' <para>Test Results posting events to a class and a panel with 10 ms sleep on both
        ''' targets:</para>
        ''' <para>Async Invoke (Post): .15- .24 ms; Regain Control:   .41 -  .58 ms. loop over
        ''' </para>
        ''' <para>Async Invoke (Post): 7.5 - 7.7 ms; Regain Control:   .34 -  .58 ms. Single Sync
        ''' context call. </para>
        ''' <para> Sync invoke (send): 7.7 - 8.5 ms; Regain Control: 10.7 - 11.3 ms. </para>
        ''' <para>Unsafe Invoke (Sync): Invoke: 6.7 - 7.5 ms; Regain Control: 10.2 - 11.2 ms. </para>
        ''' <para>Conclusion: Using safe invoke has little if any negative effects. </para>
        ''' </remarks>
        ''' <param name="handler"> The event handler. </param>
        ''' <param name="sender">  The sender of the event. </param>
        <Extension>
        Public Sub SyncNotify(ByVal handler As EventHandler(Of System.EventArgs), ByVal sender As Object)
            Dim evt As EventHandler(Of EventArgs) = handler
            If evt IsNot Nothing Then Methods.SyncNotifyThis(evt, Methods.CurrentSyncContext, sender, System.EventArgs.Empty)
        End Sub

        ''' <summary>
        ''' Synchronously notifies an <see cref="EventHandler(Of System.EventArgs)">Event</see>.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="handler"> The event handler. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       The arguments for the event. </param>
        <Extension>
        Public Sub SyncNotify(Of TEventArgs As EventArgs)(ByVal handler As EventHandler(Of TEventArgs), ByVal sender As Object, ByVal e As TEventArgs)
            Dim evt As EventHandler(Of TEventArgs) = handler
            If evt IsNot Nothing Then Methods.SyncNotifyThis(evt, Methods.CurrentSyncContext, sender, e)
        End Sub

        ''' <summary>
        ''' Synchronously notifies an <see cref="EventHandler(Of System.EventArgs)">Event</see>.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="handler"> The event handler. </param>
        ''' <param name="context"> The synchronization context. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       The arguments for the event. </param>
        <Extension>
        Public Sub SyncNotify(Of TEventArgs As EventArgs)(ByVal handler As EventHandler(Of TEventArgs), ByVal context As Threading.SynchronizationContext, ByVal sender As Object, ByVal e As TEventArgs)
            Dim evt As EventHandler(Of TEventArgs) = handler
            If evt IsNot Nothing Then Methods.SyncNotifyThis(evt, context, sender, e)
        End Sub

        ''' <summary>
        ''' Synchronously notifies an <see cref="EventHandler(Of System.EventArgs)">Event</see>.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="evt">     The event handler. </param>
        ''' <param name="context"> The synchronization context. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       The arguments for the event. </param>
        Private Sub SyncNotifyThis(Of TEventArgs As EventArgs)(ByVal evt As EventHandler(Of TEventArgs), ByVal context As Threading.SynchronizationContext, ByVal sender As Object, ByVal e As TEventArgs)
            context.Send(Sub() evt(sender, e), Nothing)
            ' same speed as the single call.
            ' For Each d As [Delegate] In evt.GetInvocationList: Methods.CurrentSyncContext.Send(Sub(arguments) d.DynamicInvoke(New Object() {sender, e}), e): Next
        End Sub

        ''' <summary>
        ''' Synchronously notifies an <see cref="PropertyChangedEventHandler">Event</see>.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="handler"> The handler. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       Property Changed event information. </param>
        <Extension>
        Public Sub SyncNotify(ByVal handler As PropertyChangedEventHandler, ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
            Dim evt As PropertyChangedEventHandler = handler
            If evt IsNot Nothing Then Methods.SyncNotifyThis(evt, Methods.CurrentSyncContext, sender, e)
        End Sub

        ''' <summary>
        ''' Synchronously notifies an <see cref="PropertyChangedEventHandler">Event</see>.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="handler"> The handler. </param>
        ''' <param name="context"> The synchronization context. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       Property Changed event information. </param>
        <Extension>
        Public Sub SyncNotify(ByVal handler As PropertyChangedEventHandler, ByVal context As Threading.SynchronizationContext, ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
            Dim evt As PropertyChangedEventHandler = handler
            If evt IsNot Nothing Then Methods.SyncNotifyThis(evt, context, sender, e)
        End Sub

        ''' <summary>
        ''' Synchronously notifies an <see cref="PropertyChangedEventHandler">Event</see>.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="evt">     The handler. </param>
        ''' <param name="context"> The synchronization context. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       Property Changed event information. </param>
        Private Sub SyncNotifyThis(ByVal evt As PropertyChangedEventHandler, ByVal context As Threading.SynchronizationContext, ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
            context.Send(Sub() evt(sender, e), Nothing)
            ' same speed as the single call.
            ' For Each d As [Delegate] In evt.GetInvocationList: Methods.CurrentSyncContext.Send(Sub(arguments) d.DynamicInvoke(New Object() {sender, e}), e): Next
        End Sub

        ''' <summary>
        ''' Synchronously notifies an <see cref="PropertyChangingEventHandler">Event</see>.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="handler"> The handler. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       Property changing event information. </param>
        <Extension>
        Public Sub SyncNotify(ByVal handler As PropertyChangingEventHandler, ByVal sender As Object, ByVal e As PropertyChangingEventArgs)
            Dim evt As PropertyChangingEventHandler = handler
            If evt IsNot Nothing Then Methods.SyncNotifyThis(evt, Methods.CurrentSyncContext, sender, e)
        End Sub

        ''' <summary>
        ''' Synchronously notifies an <see cref="PropertyChangingEventHandler">Event</see>.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="handler"> The handler. </param>
        ''' <param name="context"> The synchronization context. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       Property Changing event information. </param>
        <Extension>
        Public Sub SyncNotify(ByVal handler As PropertyChangingEventHandler, ByVal context As Threading.SynchronizationContext, ByVal sender As Object, ByVal e As PropertyChangingEventArgs)
            Dim evt As PropertyChangingEventHandler = handler
            If evt IsNot Nothing Then Methods.SyncNotifyThis(evt, context, sender, e)
        End Sub

        ''' <summary>
        ''' Synchronously notifies an <see cref="PropertyChangingEventHandler">Event</see>.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="evt">     The handler. </param>
        ''' <param name="context"> The synchronization context. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       Property Changing event information. </param>
        Private Sub SyncNotifyThis(ByVal evt As PropertyChangingEventHandler, ByVal context As Threading.SynchronizationContext, ByVal sender As Object, ByVal e As PropertyChangingEventArgs)
            context.Send(Sub() evt(sender, e), Nothing)
            ' same speed as the single call.
            ' For Each d As [Delegate] In evt.GetInvocationList : Methods.CurrentSyncContext.Send(Sub(arguments) d.DynamicInvoke(New Object() {sender, e}), e) : Next
        End Sub

        ''' <summary>
        ''' Synchronously notifies an <see cref="NotifyCollectionChangedEventHandler">Event</see>.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="handler"> The handler. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       Notify collection changed event information. </param>
        <Extension>
        Public Sub SyncNotify(ByVal handler As NotifyCollectionChangedEventHandler, ByVal sender As Object, ByVal e As NotifyCollectionChangedEventArgs)
            Dim evt As NotifyCollectionChangedEventHandler = handler
            If evt IsNot Nothing Then Methods.SyncNotifyThis(evt, Methods.CurrentSyncContext, sender, e)
        End Sub

        ''' <summary>
        ''' Synchronously notifies an <see cref="NotifyCollectionChangedEventHandler">Event</see>.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="handler"> The handler. </param>
        ''' <param name="context"> The synchronization context. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       Property Changed event information. </param>
        <Extension>
        Public Sub SyncNotify(ByVal handler As NotifyCollectionChangedEventHandler, ByVal context As Threading.SynchronizationContext, ByVal sender As Object, ByVal e As NotifyCollectionChangedEventArgs)
            Dim evt As NotifyCollectionChangedEventHandler = handler
            If evt IsNot Nothing Then Methods.SyncNotifyThis(evt, context, sender, e)
        End Sub

        ''' <summary>
        ''' Synchronously notifies an <see cref="NotifyCollectionChangedEventHandler">Event</see>.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="evt">     The handler. </param>
        ''' <param name="context"> The synchronization context. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       Property Changed event information. </param>
        Private Sub SyncNotifyThis(ByVal evt As NotifyCollectionChangedEventHandler, ByVal context As Threading.SynchronizationContext, ByVal sender As Object, ByVal e As NotifyCollectionChangedEventArgs)
            context.Send(Sub() evt(sender, e), Nothing)
            ' same speed as the single call.
            ' For Each d As [Delegate] In evt.GetInvocationList : Methods.CurrentSyncContext.Send(Sub(arguments) d.DynamicInvoke(New Object() {sender, e}), e) : Next
        End Sub

    End Module

End Namespace

