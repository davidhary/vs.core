Imports System.Threading
Imports System.Threading.Tasks

''' <summary> A generic event handler context. </summary>
''' <remarks>
''' The <see cref="EventHandlerContext"/> and the associated custom event handlers make the
''' generic event fire on the SynchronizationContext of the listening code. <para>
''' The event declaration is modified to a custom event handler. </para><para>
''' As each handler may have a different context, the AddHandler block stores the handler being
''' passed in as well as the SynchronizationContext to be used later when raising the
''' event.</para> <para>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
''' Licensed under The MIT License.</para><para>
''' David, 1/12/2019 </para><para>
''' David, 10/01/2009 from Bill McCarthy </para><para>
''' https://visualstudiomagazine.com/Articles/2009/10/01/Threading-and-the-UI.aspx?Page=2. </para>
''' </remarks>
Public Class EventHandlerContext(Of TEventArgs As EventArgs)

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="handler"> The handler. </param>
    Public Sub New(ByVal handler As EventHandler(Of TEventArgs))
        Me.Handler = handler
        Me.Context = SynchronizationContext.Current
    End Sub

    ''' <summary> Gets or sets the synchronization context. </summary>
    ''' <value> The context. </value>
    Private ReadOnly Property Context As SynchronizationContext

    ''' <summary> Gets or sets the handler. </summary>
    ''' <value> The handler. </value>
    Public ReadOnly Property Handler() As EventHandler(Of TEventArgs)

    ''' <summary>
    ''' Executes the given operation on a different thread, and waits for the result.
    ''' </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property changed event information. </param>
    Public Sub UnsafeInvoke(ByVal sender As Object, ByVal e As TEventArgs)
        Dim evt As EventHandler(Of TEventArgs) = Me.Handler
        evt?.Invoke(sender, e)
    End Sub

    ''' <summary> Executes the asynchronous on a different thread, and waits for the result. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      T event information. </param>
    ''' <returns> A Task. </returns>
    Public Async Function InvokeAsync(ByVal sender As Object, ByVal e As TEventArgs) As Task
        Await Task.Run(Sub() Me.Send(sender, e))
    End Function

End Class

''' <summary>
''' A generic thread-safe collection of <see cref="EventHandlerContext(Of TEventArgs)"/>
''' </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 12/11/2018 </para>
''' </remarks>
Public Class EventHandlerContextCollection(Of TEventArgs As EventArgs)
    Inherits List(Of EventHandlerContext(Of TEventArgs))

    ''' <summary>
    ''' Executes the given operation on a different thread, and waits for the result.
    ''' </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property changed event information. </param>
    Public Sub UnsafeInvoke(ByVal sender As Object, ByVal e As TEventArgs)
        For Each evt As EventHandlerContext(Of TEventArgs) In Me : evt?.UnsafeInvoke(sender, e) : Next
    End Sub

    ''' <summary> Executes the asynchronous on a different thread, and waits for the result. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      T event information. </param>
    ''' <returns> A Task. </returns>
    Public Async Function InvokeAsync(ByVal sender As Object, ByVal e As TEventArgs) As Task
        Await Task.Run(Sub() Me.Send(sender, e))
    End Function

    ''' <summary> Executes (sends) the event handler action synchronously (thread safe). </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property changed event information. </param>
    Public Sub Raise(ByVal sender As Object, ByVal e As TEventArgs)
        For Each evt As EventHandlerContext(Of TEventArgs) In Me : evt?.Send(sender, e) : Next
    End Sub

    ''' <summary> Executes (posts) the event handler action asynchronously (thread unsafe). </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      A T to process. </param>
    Public Sub Post(sender As Object, ByVal e As TEventArgs)
        For Each evt As EventHandlerContext(Of TEventArgs) In Me : evt?.Post(sender, e) : Next
    End Sub

    ''' <summary> Executes (sends) the event handler action synchronously (thread safe). </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      A T to process. </param>
    Public Sub Send(sender As Object, ByVal e As TEventArgs)
        For Each evt As EventHandlerContext(Of TEventArgs) In Me : evt?.Send(sender, e) : Next
    End Sub

    ''' <summary> Looks up a given key to find its associated last index. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> An Integer. </returns>
    Private Function LookupLastIndex(ByVal value As EventHandler(Of TEventArgs)) As Integer
        Return Me.ToList.FindLastIndex(Function(x) x.Handler.Equals(value))
    End Function

    ''' <summary> Removes the value described by value. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub RemoveValue(ByVal value As EventHandler(Of TEventArgs))
        If Me.Any Then
            Dim lastIndex As Integer = Me.LookupLastIndex(value)
            If lastIndex >= 0 AndAlso lastIndex < Me.Count AndAlso Me.ElementAt(lastIndex).Handler.Equals(value) Then Me.RemoveAt(lastIndex)
        End If
    End Sub

    ''' <summary> Removes all. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    Public Overloads Sub RemoveAll()
        Do While Me.Count > 0
            Me.RemoveAt(0)
        Loop
    End Sub

End Class
