﻿Imports Microsoft.Win32
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.ServiceProcess

''' <summary> A service methods. </summary>
''' <remarks>
''' (c) 2018 Clifford Nelson. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 6/13/2018 from Clifford Nelson </para><para>
''' https://www.codeproject.com/Tips/1248302/Methods-to-Help-Working-with-Services.
''' </para>
''' </remarks>
Public NotInheritable Class ServiceMethods



    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    Private Sub New()
        MyBase.New
    End Sub

    ''' <summary> Installs the service. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="installServicePath"> The install service path. </param>
    Public Shared Sub InstallService(ByVal installServicePath As String)
        ServiceMethods.InstallService(installServicePath, False)
    End Sub

    ''' <summary> Uninstall service. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="installServicePath"> The install service path. </param>
    Public Shared Sub UninstallService(ByVal installServicePath As String)
        ServiceMethods.InstallService(installServicePath, True)
    End Sub

    ''' <summary> Gets or sets the filename of the install utility file. </summary>
    ''' <value> The filename of the install utility file. </value>
    Public Shared Property InstallUtilityFileName As String = "InstallUtil.exe"

    ''' <summary> Builds install command. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="installServicePath"> The install service path. </param>
    ''' <param name="uninstall">          True to uninstall. </param>
    ''' <returns> A String. </returns>
    Public Shared Function BuildInstallCommand(ByVal installServicePath As String, ByVal uninstall As Boolean) As String
        Return $"/{installServicePath} {If(uninstall, "u", "i")}"
    End Function

    ''' <summary> Installs the service. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <exception cref="FileNotFoundException">    Thrown when the requested file is not present. </exception>
    ''' <exception cref="OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="installServicePath"> The install service path. </param>
    ''' <param name="uninstall">          True to uninstall. </param>
    Public Shared Sub InstallService(ByVal installServicePath As String, ByVal uninstall As Boolean)
        Dim installUtilPath As String = Path.Combine(RuntimeEnvironment.GetRuntimeDirectory(), ServiceMethods.InstallUtilityFileName)
        If Not File.Exists(installUtilPath) Then
            Throw New FileNotFoundException(String.Format("Install Utility path not found: {0}", installUtilPath))
        End If
        If Not File.Exists(installServicePath) Then
            Throw New FileNotFoundException(String.Format("Install Service path not found: {0}", installServicePath))
        End If

        Dim processStartInfo As New ProcessStartInfo(installUtilPath, ServiceMethods.BuildInstallCommand(installServicePath, uninstall)) With {
                .RedirectStandardOutput = True,
                .RedirectStandardError = True,
                .UseShellExecute = False}

        Using process As New Process()
            process.StartInfo = processStartInfo
            process.Start()
            process.WaitForExit()
            If process.ExitCode = -1 Then Throw New OperationFailedException(process.StandardOutput.ReadToEnd())
        End Using

    End Sub

    ''' <summary> Queries if a given service exists. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="serviceName"> Name of the service. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Shared Function ServiceExists(ByVal serviceName As String) As Boolean
        ' Avoids exceptions this way
        Return ServiceController.GetServices().Any(Function(i) i.ServiceName = serviceName)
    End Function

    ''' <summary> Service start. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <exception cref="OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="serviceName"> Name of the service. </param>
    Public Shared Sub ServiceStart(ByVal serviceName As String)
        Using service As New ServiceController(serviceName)
            Try
                If service.Status = ServiceControllerStatus.Running Then
                    Return
                End If
                service.Start()
                service.WaitForStatus(ServiceControllerStatus.Running)
            Catch ex As Exception
                Throw New OperationFailedException(String.Format("Failed to start the Windows Service {0}", serviceName), ex)
            End Try
        End Using
    End Sub

    ''' <summary> Service stop. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <exception cref="OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="serviceName"> Name of the service. </param>
    Public Shared Sub ServiceStop(ByVal serviceName As String)
        Using service As New ServiceController(serviceName)
            Try
                If service.Status = ServiceControllerStatus.Stopped Then
                    Return
                End If
                service.Stop()
                service.WaitForStatus(ServiceControllerStatus.Stopped)
            Catch ex As Exception
                Throw New OperationFailedException(String.Format("Failed to stop the Windows Service {0}", serviceName), ex)
            End Try
        End Using
    End Sub

    ''' <summary> Service refresh. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <exception cref="OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="serviceName"> Name of the service. </param>
    Public Shared Sub ServiceRefresh(ByVal serviceName As String)
        Using service As New ServiceController(serviceName)
            Try
                If service.Status = ServiceControllerStatus.Running Then
                    service.Stop()
                    service.WaitForStatus(ServiceControllerStatus.Stopped)
                End If

                service.Start()
                service.WaitForStatus(ServiceControllerStatus.Running)
            Catch ex As Exception
                Throw New OperationFailedException(String.Format("Failed to restart the Windows Service {0}", serviceName), ex)
            End Try
        End Using
    End Sub

    ''' <summary> Service path. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <exception cref="OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="serviceName"> Name of the service. </param>
    ''' <returns> A String. </returns>
    Public Shared Function ServicePath(ByVal serviceName As String) As String
        Try
            Dim registryPath As String = "SYSTEM\CurrentControlSet\Services\" & serviceName
            Dim key As RegistryKey = Registry.LocalMachine.OpenSubKey(registryPath)
            Dim value As String = key.GetValue("ImagePath").ToString()
            key.Close()
            Return If(String.IsNullOrEmpty(value.Chars(0)), value.Substring(1, value.Length - 2), value)
        Catch ex As Exception
            Throw New OperationFailedException(String.Format("Error in attempting to get Registry key for service {0}", serviceName), ex)
        End Try
    End Function

    ''' <summary> Service version. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="serviceName"> Name of the service. </param>
    ''' <returns> A FileVersionInfo. </returns>
    Public Shared Function ServiceVersion(ByVal serviceName As String) As FileVersionInfo
        Return FileVersionInfo.GetVersionInfo(ServicePath(serviceName))
    End Function

End Class
