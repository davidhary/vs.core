Imports System.Runtime.InteropServices

''' <summary> The process methods. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 8/15/2019 </para>
''' </remarks>
Public NotInheritable Class ProcessMethods

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Constructor that prevents a default instance of this class from being created.
    ''' </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    Private Sub New()
        MyBase.New
    End Sub

#End Region

#Region " DESIGN MODE "

    ''' <summary>
    ''' Gets a value indicating whether the application is running under the IDE in design mode.
    ''' </summary>
    ''' <value>
    ''' <c>True</c> if the application is running under the IDE in design mode; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Shared ReadOnly Property InDesignMode() As Boolean
        Get
            Return Debugger.IsAttached
        End Get
    End Property

    ''' <summary> List of names of the designer process. </summary>
    Private Shared ReadOnly DesignerProcessNames() As String = {"xdesproc", "devenv"}

    ''' <summary> True to running from visual studio designer? </summary>
    Private Shared _RunningFromVisualStudioDesigner? As Boolean = Nothing

    ''' <summary> <see langword="True"/> if running from visual studio designer. </summary>
    ''' <value> The running from visual studio designer. </value>
    Public Shared ReadOnly Property RunningFromVisualStudioDesigner() As Boolean
        Get
            If Not _RunningFromVisualStudioDesigner.HasValue Then
                Using currentProcess As System.Diagnostics.Process = System.Diagnostics.Process.GetCurrentProcess()
                    ProcessMethods._RunningFromVisualStudioDesigner = DesignerProcessNames.Contains(currentProcess.ProcessName.Trim(), StringComparer.OrdinalIgnoreCase)
                End Using
            End If
            Return _RunningFromVisualStudioDesigner.Value
        End Get
    End Property

#End Region

#Region " CHECK 64 Bits "

    ''' <summary> Query if 'process' is 64 bit. </summary>
    ''' <remarks>
    ''' see https://msdn.microsoft.com/en-us/library/windows/desktop/ms684139%28v=vs.85%29.aspx.
    ''' </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="process"> The process. </param>
    ''' <returns> <c>true</c> if 64 bit; otherwise <c>false</c> </returns>
    Public Shared Function Is64Bit(ByVal process As Process) As Boolean
        If process Is Nothing Then Throw New ArgumentNullException(NameOf(process))
        If Not Environment.Is64BitOperatingSystem Then Return False
        ' if this method is not available in your version of .NET, use GetNativeSystemInfo via P/Invoke instead
        Dim isWow64 As Boolean
        If Not NativeMethods.IsWow64Process(process.Handle, isWow64) Then Throw New InvalidOperationException()
        Return Not isWow64
    End Function

    ''' <summary> Query if 'process' is 64 bit. </summary>
    ''' <remarks>
    ''' see https://msdn.microsoft.com/en-us/library/windows/desktop/ms684139%28v=vs.85%29.aspx.
    ''' </remarks>
    ''' <returns> <c>true</c> if 64 bit; otherwise <c>false</c> </returns>
    Public Shared Function Is64Bit() As Boolean
        Return ProcessMethods.Is64Bit(Process.GetCurrentProcess)
    End Function

    ''' <summary> A native methods. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    Private NotInheritable Class NativeMethods

        ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        Private Sub New()
        End Sub

        ''' <summary> Is wow 64 process. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="process"> [In] a pointer to the process. </param>
        ''' <param name="wow64Process"> [Out] True if WOW 64 process. </param>
        ''' <returns> True if success </returns>
        <DllImport("kernel32.dll", SetLastError:=True, CallingConvention:=CallingConvention.Winapi)>
        Friend Shared Function IsWow64Process(<[In]()> ByVal process As IntPtr, <Out(), MarshalAs(UnmanagedType.Bool)> ByRef wow64Process As Boolean) As <MarshalAs(UnmanagedType.Bool)> Boolean
        End Function
    End Class

#End Region

End Class

