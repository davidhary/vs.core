'           SystemConfiguration.DLL

''' <summary> Information about the program. </summary>
''' <remarks> David, 2020-09-15. </remarks>
Public Class ProgramInfo

#Region " CONSTRUCTION "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    Public Sub New()
        MyBase.New
        Me._Lines = New ObjectModel.Collection(Of ProgramInfoLine)
    End Sub

#End Region

#Region " DISPLAY "

    ''' <summary> Gets or sets the lines. </summary>
    ''' <value> The lines. </value>
    Public ReadOnly Property Lines As ObjectModel.Collection(Of ProgramInfoLine)

    ''' <summary> Clears this object to its blank/initial state. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    Public Sub Clear()
        Me.Lines.Clear()
    End Sub

    ''' <summary> Appends a line. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="text"> The text. </param>
    ''' <param name="font"> The font. </param>
    Public Sub AppendLine(ByVal text As String, ByVal font As Drawing.Font)
        Me.Lines.Add(New ProgramInfoLine(text, font))
    End Sub

    ''' <summary> Updates the display contents. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="headerFont">  The header font. </param>
    ''' <param name="contentFont"> The content font. </param>
    Public Sub BuildContents(ByVal headerFont As Drawing.Font, ByVal contentFont As Drawing.Font)

        Me.Clear()
        Me.AppendLine("Product Name:", headerFont)
        Me.AppendLine(My.Application.Info.ProductName, contentFont)
        Me.AppendLine(String.Empty, contentFont)

        Me.AppendLine("Product Title:", headerFont)
        Me.AppendLine(My.Application.Info.Title, contentFont)
        Me.AppendLine(String.Empty, contentFont)


        Me.AppendLine("Product Name:", headerFont)
        Me.AppendLine(My.Application.Info.AssemblyName, contentFont)
        Me.AppendLine(String.Empty, contentFont)

        Me.AppendLine("Product Version:", headerFont)
        Me.AppendLine(My.Application.Info.Version.ToString, contentFont)
        Me.AppendLine(String.Empty, contentFont)

        Me.AppendLine("Product Folder:", headerFont)
        Me.AppendLine(My.Application.Info.DirectoryPath, contentFont)
        Me.AppendLine(String.Empty, contentFont)

        Me.AppendLine("Program Data Folder:", headerFont)
        Me.AppendLine(My.Computer.FileSystem.SpecialDirectories.AllUsersApplicationData, contentFont)
        Me.AppendLine(String.Empty, contentFont)

        Me.AppendLine("User Program Data Folder:", headerFont)
        Me.AppendLine(My.Computer.FileSystem.SpecialDirectories.CurrentUserApplicationData, contentFont)
        Me.AppendLine(String.Empty, contentFont)

        Dim configuration As System.Configuration.Configuration = Global.System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None)
        Me.AppendLine("All Users Configuration File:", headerFont)
        Me.AppendLine(configuration.FilePath(), contentFont)
        If configuration.Locations.Count > 0 Then
            Me.AppendLine("Configuration Locations:", headerFont)
            Me.AppendLine(configuration.FilePath(), contentFont)
            For Each s As System.Configuration.ConfigurationLocation In configuration.Locations
                Me.AppendLine(s.Path, contentFont)
            Next
        End If
        Me.AppendLine(String.Empty, contentFont)

        configuration = Global.System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.PerUserRoaming)
        Me.AppendLine("User Roaming Configuration File:", headerFont)
        Me.AppendLine(configuration.FilePath(), contentFont)
        If configuration.Locations.Count > 0 Then
            Me.AppendLine("Configuration Locations:", headerFont)
            Me.AppendLine(configuration.FilePath(), contentFont)
            For Each s As System.Configuration.ConfigurationLocation In configuration.Locations
                Me.AppendLine(s.Path, contentFont)
            Next
        End If
        Me.AppendLine(String.Empty, contentFont)

        configuration = Global.System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.PerUserRoamingAndLocal)
        configuration = Global.System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.PerUserRoaming)
        Me.AppendLine("User Local  Configuration File:", headerFont)
        Me.AppendLine(configuration.FilePath(), contentFont)
        If configuration.Locations.Count > 0 Then
            Me.AppendLine("Configuration Locations:", headerFont)
            Me.AppendLine(configuration.FilePath(), contentFont)
            For Each s As System.Configuration.ConfigurationLocation In configuration.Locations
                Me.AppendLine(s.Path, contentFont)
            Next
        End If
        Me.AppendLine(String.Empty, contentFont)

        ' allows the calling application to add data.
        Me.OnProgramInfoRequested(System.EventArgs.Empty)

        Me.AppendLine("Product Copyrights:", headerFont)
        Me.AppendLine(My.Application.Info.Copyright, contentFont)
        Me.AppendLine(String.Empty, contentFont)

    End Sub

#End Region

#Region " PROGRAM INFO REQUESTED "

    ''' <summary> Notifies of the <see cref="ProgramInfoRequested"/> event. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnProgramInfoRequested(ByVal e As System.EventArgs)
        Me.SyncNotifyProgramInfoRequested(e)
    End Sub

    ''' <summary> Removes the  <see cref="ProgramInfoRequested"/> event handlers. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    Protected Sub RemoveProgramInfoRequestedEventHandlers()
        Me._ProgramInfoRequestedEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The  <see cref="ProgramInfoRequested"/> event handlers. </summary>
    Private ReadOnly _ProgramInfoRequestedEventHandlers As New EventHandlerContextCollection(Of System.EventArgs)

    ''' <summary> Event queue for all listeners interested in  <see cref="ProgramInfoRequested"/> events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event ProgramInfoRequested As EventHandler(Of System.EventArgs)
        AddHandler(value As EventHandler(Of System.EventArgs))
            Me._ProgramInfoRequestedEventHandlers.Add(New EventHandlerContext(Of System.EventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of System.EventArgs))
            Me._ProgramInfoRequestedEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As System.EventArgs)
            Me._ProgramInfoRequestedEventHandlers.Send(sender, e)
        End RaiseEvent
    End Event

    ''' <summary>
    ''' Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or invokes the
    ''' <see cref="ProgramInfoRequested">ProgramInfoRequested Event</see>.
    ''' </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Protected Sub SyncNotifyProgramInfoRequested(ByVal e As System.EventArgs)
        Me._ProgramInfoRequestedEventHandlers.Send(Me, e)
    End Sub

#End Region

#Region " REFRESH REQUESTED "

    ''' <summary> Notifies of the <see cref="RefreshRequested"/> event. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    Public Sub NotifyRefreshRequested()
        Me.SyncNotifyRefreshRequested(System.EventArgs.Empty)
    End Sub

    ''' <summary> Removes the<see cref="RefreshRequested"/> event handlers. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    Protected Sub RemoveRefreshRequestedEventHandlers()
        Me._RefreshRequestedEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The <see cref="RefreshRequested"/> event handlers. </summary>
    Private ReadOnly _RefreshRequestedEventHandlers As New EventHandlerContextCollection(Of System.EventArgs)

    ''' <summary> Event queue for all listeners interested in <see cref="RefreshRequested"/> events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event RefreshRequested As EventHandler(Of System.EventArgs)
        AddHandler(value As EventHandler(Of System.EventArgs))
            Me._RefreshRequestedEventHandlers.Add(New EventHandlerContext(Of System.EventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of System.EventArgs))
            Me._RefreshRequestedEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As System.EventArgs)
            Me._RefreshRequestedEventHandlers.Send(sender, e)
        End RaiseEvent
    End Event

    ''' <summary>
    ''' Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or invokes the
    ''' <see cref="RefreshRequested">RefreshRequested Event</see>.
    ''' </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Protected Sub SyncNotifyRefreshRequested(ByVal e As System.EventArgs)
        Me._RefreshRequestedEventHandlers.Send(Me, e)
    End Sub

#End Region

End Class

''' <summary> A program information line. </summary>
''' <remarks> David, 2020-09-15. </remarks>
<CodeAnalysis.SuppressMessage("Performance", "CA1815:Override equals and operator equals on value types", Justification:="<Pending>")>
Public Structure ProgramInfoLine

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="text"> The text. </param>
    ''' <param name="font"> The font. </param>
    Public Sub New(text As String, font As Drawing.Font)
        Me.Text = text
        Me.Font = font
    End Sub

    ''' <summary> Gets or sets the font. </summary>
    ''' <value> The font. </value>
    Public Property Font As Drawing.Font

    ''' <summary> Gets or sets the text. </summary>
    ''' <value> The text. </value>
    Public Property Text As String
End Structure
