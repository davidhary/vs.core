
Partial Public Class MyAssemblyInfo

    ''' <summary> Gets the application information caption with ISO 8601 UTC time, e.g., '2019-09-10 19:27:04Z'. </summary>
    ''' <value> The UTC caption. </value>
    Public ReadOnly Property IsoUniversalTimeCaption As String
        Get
            Return $"{Me.PrefixProcessName}.r.{Me.ProductVersion(4)} {System.DateTimeOffset.Now:u)}"
        End Get
    End Property

    ''' <summary> Gets the GMT caption, e.g., 'Tue, 10 May 2019 19:26:42 GMT'. </summary>
    ''' <value> The GMT caption. </value>
    Public ReadOnly Property GmtCaption As String
        Get
            Return $"{Me.PrefixProcessName}.r.{Me.ProductVersion(4)} {System.DateTimeOffset.Now:r)}"
        End Get
    End Property

    ''' <summary> Gets the application information caption with ISO complete time, e.g., '2019-09-10T12:12:29.7552627-07:00'. </summary>
    ''' <value> The UTC caption. </value>
    Public ReadOnly Property IsoCompleteCaption As String
        Get
            Return $"{Me.PrefixProcessName}.r.{Me.ProductVersion(4)} {DateTimeOffset.Now:o)}"
        End Get
    End Property

    ''' <summary> Gets the application ISO 8691 Short date and time caption, e.g., 2019-09-10T12:24:47-07:00. </summary>
    ''' <value> The time caption. </value>
    Public ReadOnly Property IsoShortCaption As String
        Get
            Return $"{Me.PrefixProcessName}.r.{Me.ProductVersion(4)} {DateTimeOffset.Now:s)}{DateTimeOffset.Now:zzz)}"
        End Get
    End Property

End Class
