Imports System.Runtime.CompilerServices

Imports Microsoft.VisualBasic.ApplicationServices
Namespace AssemblyExtensions

    ''' <summary> Includes extensions for <see cref="T:System.Reflection.Assembly">Assembly</see>
    ''' and <see cref="T:System.Reflection.Assembly">assembly info</see>. </summary>
    ''' <remarks> (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para> </remarks>
    Public Module Methods

        ''' <summary> Returns the assembly public key. </summary>
        ''' <remarks>
        ''' <examples>
        ''' <code>
        ''' Dim pk As String = System.Reflection.Assembly.GetExecutingAssembly.GetPublicKey()
        ''' </code></examples>
        ''' </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="assembly"> The assembly. </param>
        ''' <returns> The public key of the assembly. </returns>
        <Extension()>
        Public Function GetPublicKey(ByVal assembly As System.Reflection.Assembly) As String
            If assembly Is Nothing Then
                Throw New ArgumentNullException(NameOf(assembly))
            End If
            Dim values As Byte() = assembly.GetName.GetPublicKey
            Return values.Aggregate(String.Empty, Function(current As String, value As Byte) (current & value.ToString("X2")))
        End Function

        ''' <summary> Gets public key token. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="assembly"> The assembly. </param>
        ''' <returns> The public key token of the assembly. </returns>
        <Extension()>
        Public Function GetPublicKeyToken(ByVal assembly As System.Reflection.Assembly) As String
            If assembly Is Nothing Then
                Throw New ArgumentNullException(NameOf(assembly))
            End If
            Dim values As Byte() = assembly.GetName.GetPublicKeyToken
            Return values.Aggregate(String.Empty, Function(current As String, value As Byte) (current & value.ToString("X2")))
        End Function

        ''' <summary> Gets the application <see cref="FileVersionInfo">File Version Info.</see> </summary>
        ''' <remarks>
        ''' Use this version information source for the application so that it won't be affected by any
        ''' changes that might occur in the <see cref="My.MyApplication.Info">application info</see> code.
        ''' </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="assembly"> The assembly. </param>
        ''' <returns> The application <see cref="FileVersionInfo">File Version Info</see>. </returns>
        <Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Demand,
                                    Flags:=System.Security.Permissions.SecurityPermissionFlag.UnmanagedCode)>
        <Extension()>
        Public Function GetFileVersionInfo(ByVal assembly As System.Reflection.Assembly) As FileVersionInfo
            If assembly Is Nothing Then
                Throw New ArgumentNullException(NameOf(assembly))
            End If
            Return FileVersionInfo.GetVersionInfo(assembly.Location)
        End Function

        ''' <summary> Gets a unique identifier. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="assembly"> The assembly. </param>
        ''' <returns> The unique identifier. </returns>
        <Extension()>
        Public Function GetGuid(ByVal assembly As System.Reflection.Assembly) As Guid
            If assembly Is Nothing Then Throw New ArgumentNullException(NameOf(assembly))
            Dim attribute As Runtime.InteropServices.GuidAttribute = CType(assembly.GetCustomAttributes(GetType(Runtime.InteropServices.GuidAttribute),
                                                                                                          True)(0), Runtime.InteropServices.GuidAttribute)
            Return New Guid(attribute.Value)
        End Function

        ''' <summary> Directory path. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="assembly"> The assembly. </param>
        ''' <returns> A String. </returns>
        <Extension()>
        Public Function DirectoryPath(ByVal assembly As System.Reflection.Assembly) As String
            If assembly Is Nothing Then Throw New ArgumentNullException(NameOf(assembly))
            Return System.IO.Path.GetDirectoryName(assembly.Location)
        End Function

        ''' <summary> Gets the assembly. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="assemblyInfo"> Information describing the assembly. </param>
        ''' <returns> The application <see cref="T:System.Reflection.Assembly">Assembly</see>. </returns>
        <Extension()>
        Public Function GetAssembly(ByVal assemblyInfo As AssemblyInfo) As System.Reflection.Assembly
            If assemblyInfo Is Nothing Then
                Throw New ArgumentNullException(NameOf(assemblyInfo))
            End If
            Dim value As System.Reflection.Assembly = Nothing
            For Each a As System.Reflection.Assembly In assemblyInfo.LoadedAssemblies
                If a.GetName.Name.Equals(assemblyInfo.AssemblyName) Then
                    value = a
                    Exit For
                End If
            Next
            Return value
        End Function

        ''' <summary> Gets the folder where application files are located. </summary>
        ''' <remarks>
        ''' <examples>
        ''' <code>
        ''' Dim folder As String = My.Application.Info.ApplicationFolder()
        ''' </code></examples>
        ''' </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="assemblyInfo"> Information describing the assembly. </param>
        ''' <returns> The application folder path. </returns>
        <Extension()>
        Public Function ApplicationFolder(ByVal assemblyInfo As AssemblyInfo) As String
            If assemblyInfo Is Nothing Then
                Throw New ArgumentNullException(NameOf(assemblyInfo))
            End If
            Return assemblyInfo.DirectoryPath
        End Function

        ''' <summary> The default version format. </summary>
        Public Const DefaultVersionFormat As String = "{0}.{1:00}.{2:0000}"

        ''' <summary> Gets the product version. </summary>
        ''' <remarks>
        ''' <examples>
        ''' <code>
        ''' Dim version As String = My.Application.Info.ProducVersion("")
        ''' </code></examples>
        ''' </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="assemblyInfo"> Information describing the assembly. </param>
        ''' <param name="format">       The version format. Set to empty to use the
        '''                             <see cref="DefaultVersionFormat">default version format</see>. 
        ''' </param>
        ''' <returns> The application version. </returns>
        <Extension()>
        Public Function ProductVersion(ByVal assemblyInfo As AssemblyInfo, ByVal format As String) As String
            If assemblyInfo Is Nothing Then Throw New ArgumentNullException(NameOf(assemblyInfo))
            If String.IsNullOrWhiteSpace(format) Then format = DefaultVersionFormat
            Return String.Format(Globalization.CultureInfo.CurrentCulture,
                                 format, assemblyInfo.Version.Major, assemblyInfo.Version.Minor, assemblyInfo.Version.Build)
        End Function

#Region " PRODUCT PROCESS CAPTION "

        ''' <summary> Prefix process name to the product name. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="assemblyInfo"> Information describing the assembly. </param>
        ''' <returns> A String. </returns>
        <Extension()>
        Public Function PrefixProcessName(ByVal assemblyInfo As AssemblyInfo) As String
            If assemblyInfo Is Nothing Then Throw New ArgumentNullException(NameOf(assemblyInfo))
            Dim name As String = assemblyInfo.ProductName
            Dim processName As String = Process.GetCurrentProcess().ProcessName
            If Not name.StartsWith(processName, StringComparison.OrdinalIgnoreCase) Then name = $"{processName}.{name}"
            Return name
        End Function

        ''' <summary> Builds product time caption. </summary>
        ''' <remarks>
        ''' <list type="bullet">Use the following format options: <item>
        ''' u - UTC - 2019-09-10 19:27:04Z</item><item>
        ''' r - GMT - Tue, 10 May 2019 19:26:42 GMT</item><item>
        ''' o - ISO - 2019-09-10T12:12:29.7552627-07:00</item><item>
        ''' s - ISO - 2019-09-10T12:24:47</item><item>
        ''' empty   - 9/10/2019 16:57:24 -07:00</item><item>
        ''' s + zzz - 2019-09-10T12:24:47-07:00</item></list>
        ''' </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="assemblyInfo">      Information describing the assembly. </param>
        ''' <param name="versionElements">   The version elements. </param>
        ''' <param name="timeCaptionFormat"> The time caption format. </param>
        ''' <param name="kindFormat">        The kind format. </param>
        ''' <returns> A String. </returns>
        <Extension()>
        Public Function BuildProductTimeCaption(ByVal assemblyInfo As AssemblyInfo,
                                                ByVal versionElements As Integer, ByVal timeCaptionFormat As String, ByVal kindFormat As String) As String
            If assemblyInfo Is Nothing Then Throw New ArgumentNullException(NameOf(assemblyInfo))
            Dim result As String
            If String.IsNullOrWhiteSpace(timeCaptionFormat) Then
                result = $"{assemblyInfo.PrefixProcessName}.r.{assemblyInfo.Version.ToString(versionElements)} {System.DateTimeOffset.Now}"
            ElseIf String.IsNullOrWhiteSpace(kindFormat) Then
                result = $"{assemblyInfo.PrefixProcessName}.r.{assemblyInfo.Version.ToString(versionElements)} {System.DateTimeOffset.Now.ToString(timeCaptionFormat)}"
            Else
                result = $"{assemblyInfo.PrefixProcessName}.r.{assemblyInfo.Version.ToString(versionElements)} {DateTimeOffset.Now.ToString(timeCaptionFormat)}{DateTimeOffset.Now.ToString(kindFormat)}"
            End If
            Return result
        End Function

        ''' <summary>
        ''' Builds product time caption using full version and local time plus kind format.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="assemblyInfo"> Information describing the assembly. </param>
        ''' <returns> A String. </returns>
        <Extension()>
        Public Function BuildProductTimeCaption(ByVal assemblyInfo As AssemblyInfo) As String
            Return assemblyInfo.BuildProductTimeCaption(4, String.Empty, String.Empty)
        End Function

#End Region

    End Module

End Namespace
