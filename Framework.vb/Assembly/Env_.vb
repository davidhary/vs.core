Imports System.IO
Imports System.Text
Imports System.Text.RegularExpressions

''' <summary> An environment. </summary>
''' <remarks>
''' (c) 2019 TONERDO. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 10/16/2019, https://github.com/tonerdo/dotnet-env. </para>
''' </remarks>
Public NotInheritable Class Env

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Constructor that prevents a default instance of this class from being created.
    ''' </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    Private Sub New()
        MyBase.New
    End Sub

    ''' <summary> The default environment file name. </summary>
    Public Const DefaultEnvFileName As String = ".env"

    ''' <summary> Loads the given options. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="lines">   The lines. </param>
    ''' <param name="options"> The options to load. </param>
    Public Shared Sub Load(ByVal lines As String(), ByVal options As EnvLoadOptions)
        If options Is Nothing Then Throw New ArgumentNullException(NameOf(options))
        Dim envFile As EnvironmentVariableDictionary = Parser.Parse(lines, options.TrimWhiteSpace, options.IsEmbeddedHashComment, options.UnescapeQuotedValues)
        envFile.SetEnvironmentVariables(options.ClobberExistingValues)
    End Sub

    ''' <summary> Loads the given options. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="path">    Full pathname of the file. </param>
    ''' <param name="options"> The options to load. </param>
    Public Shared Sub Load(ByVal path As String, ByVal options As EnvLoadOptions)
        If options Is Nothing Then Throw New ArgumentNullException(NameOf(options))
        If (Not options.RequireEnvFile) AndAlso (Not File.Exists(path)) Then
            Return
        End If
        Env.Load(File.ReadAllLines(path), options)
    End Sub

    ''' <summary> Loads the given options. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="file">    The file. </param>
    ''' <param name="options"> The options to load. </param>
    Public Shared Sub Load(ByVal file As Stream, ByVal options As EnvLoadOptions)
        If options Is Nothing Then Throw New ArgumentNullException(NameOf(options))
        Dim lines As New List(Of String)()
        Dim currentLine As String = String.Empty
        Using reader As New StreamReader(file)
            Do While currentLine IsNot Nothing
                currentLine = reader.ReadLine()
                If currentLine IsNot Nothing Then
                    lines.Add(currentLine)
                End If
            Loop
        End Using
        Env.Load(lines.ToArray, options)
    End Sub

    ''' <summary> Loads the given options. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="options"> The options to load. </param>
    Public Shared Sub Load(ByVal options As EnvLoadOptions)
        Env.Load(Path.Combine(Directory.GetCurrentDirectory(), Env.DefaultEnvFileName), options)
    End Sub

#End Region

#Region " RETRIEVE "

    ''' <summary> Retrieves the environment value of the specified key. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="key">          The key. </param>
    ''' <param name="defaultValue"> (Optional) The default value. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function RetrieveValue(ByVal key As String, Optional defaultValue As String = Nothing) As String
        Return If(Environment.GetEnvironmentVariable(key), defaultValue)
    End Function

    ''' <summary> Retrieves the environment value of the specified key. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="key">          The key. </param>
    ''' <param name="defaultValue"> (Optional) The default value. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function RetrieveBoolean(ByVal key As String, Optional ByVal defaultValue As Boolean? = Nothing) As Boolean?
        Dim keyValue As String = Environment.GetEnvironmentVariable(key)
        Dim parsedValue As Boolean = False
        Return If(Boolean.TryParse(keyValue, parsedValue), parsedValue, defaultValue)
    End Function

    ''' <summary> Retrieves the environment value of the specified key. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="key">          The key. </param>
    ''' <param name="defaultValue"> (Optional) The default value. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function RetrieveDouble(ByVal key As String, Optional ByVal defaultValue As Double? = Nothing) As Double?
        Dim keyValue As String = Environment.GetEnvironmentVariable(key)
        Dim parsedValue As Double = 0
        Return If(Double.TryParse(keyValue, parsedValue), parsedValue, defaultValue)
    End Function

    ''' <summary> Retrieves the environment value of the specified key. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="key">          The key. </param>
    ''' <param name="defaultValue"> (Optional) The default value. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function RetrieveInteger(ByVal key As String, Optional ByVal defaultValue As Integer? = Nothing) As Integer?
        Dim keyValue As String = Environment.GetEnvironmentVariable(key)
        Dim parsedValue As Integer = 0
        Return If(Integer.TryParse(keyValue, parsedValue), parsedValue, defaultValue)
    End Function

#End Region

#Region " Dictionary "

    ''' <summary> Dictionary of environment variables. </summary>
    ''' <remarks>
    ''' (c) 2019 TONERDO. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 10/16/2019, https://github.com/tonerdo/dotnet-env. </para>
    ''' </remarks>
    Private Class EnvironmentVariableDictionary
        Inherits Dictionary(Of String, String)

        ''' <summary> Sets environment variables. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="clobberExistingVars"> (Optional) True to clobber existing variables. </param>
        Public Sub SetEnvironmentVariables(Optional ByVal clobberExistingVars As Boolean = True)
            For Each keyValuePair As KeyValuePair(Of String, String) In Me
                If clobberExistingVars OrElse Environment.GetEnvironmentVariable(keyValuePair.Key) Is Nothing Then
                    Environment.SetEnvironmentVariable(keyValuePair.Key, keyValuePair.Value)
                End If
            Next keyValuePair
        End Sub
    End Class

#End Region

#Region " Parser "

    ''' <summary> A parser. </summary>
    ''' <remarks>
    ''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 10/16/2019,  https://github.com/tonerdo/dotnet-env. </para>
    ''' </remarks>
    Private NotInheritable Class Parser

        ''' <summary>
        ''' Constructor that prevents a default instance of this class from being created.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        Private Sub New()
            MyBase.New
        End Sub

        ''' <summary> The export RegEx. </summary>
        Private Shared ReadOnly ExportRegex As New Regex("^\s*export\s+")

        ''' <summary> Query if 'line' is comment. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="line"> The line. </param>
        ''' <returns> <c>true</c> if comment; otherwise <c>false</c> </returns>
        Private Shared Function IsComment(ByVal line As String) As Boolean
            Return line.Trim().StartsWith("#", StringComparison.OrdinalIgnoreCase)
        End Function

        ''' <summary> Removes the inline comment described by line. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="line"> The line. </param>
        ''' <returns> A String. </returns>
        Private Shared Function RemoveInlineComment(ByVal line As String) As String
            Dim pos As Integer = line.IndexOf("#"c)
            Return If(pos >= 0, line.Substring(0, pos), line)
        End Function

        ''' <summary> Removes the export keyword described by line. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="line"> The line. </param>
        ''' <returns> A String. </returns>
        Private Shared Function RemoveExportKeyword(ByVal line As String) As String
            Dim match As Match = Parser.ExportRegex.Match(line)
            Return If(match.Success, line.Substring(match.Length), line)
        End Function

        ''' <summary> Parses. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="lines">                 The lines. </param>
        ''' <param name="trimWhitespace">        (Optional) True to trim whitespace. </param>
        ''' <param name="isEmbeddedHashComment"> (Optional) True if is embedded hash comment, false if
        '''                                      not. </param>
        ''' <param name="unescapeQuotedValues">  (Optional) True to un-escape quoted values. </param>
        ''' <returns> An EnvironmentVariableDictionary. </returns>
        Public Shared Function Parse(ByVal lines() As String, Optional ByVal trimWhitespace As Boolean = True,
                                     Optional ByVal isEmbeddedHashComment As Boolean = True,
                                     Optional ByVal unescapeQuotedValues As Boolean = True) As EnvironmentVariableDictionary
            Dim vars As New EnvironmentVariableDictionary()

            For i As Integer = 0 To lines.Length - 1
                Dim line As String = lines(i)

                ' skip comments
                If IsComment(line) Then
                    Continue For
                End If

                If isEmbeddedHashComment Then
                    line = RemoveInlineComment(line)
                End If

                line = RemoveExportKeyword(line)

                Dim keyValuePair() As String = line.Split(New Char() {"="c}, 2)

                ' skip malformed lines
                If keyValuePair.Length <> 2 Then
                    Continue For
                End If

                If trimWhitespace Then
                    keyValuePair(0) = keyValuePair(0).Trim()
                    keyValuePair(1) = keyValuePair(1).Trim()
                End If

                If unescapeQuotedValues AndAlso IsQuoted(keyValuePair(1)) Then
                    keyValuePair(1) = Unescape(keyValuePair(1).Substring(1, keyValuePair(1).Length - 2))
                End If

                vars.Add(keyValuePair(0), keyValuePair(1))
            Next i

            Return vars
        End Function

        ''' <summary> Query if 's' is quoted. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="s"> The string. </param>
        ''' <returns> <c>true</c> if quoted; otherwise <c>false</c> </returns>
        Private Shared Function IsQuoted(ByVal s As String) As Boolean
            Return s.Length > 1 AndAlso ((String.IsNullOrEmpty(s.Chars(0)) AndAlso String.IsNullOrEmpty(s.Chars(s.Length - 1))) OrElse (s.Chars(0) = "'"c AndAlso s.Chars(s.Length - 1) = "'"c))
        End Function

        ''' <summary> Removes escape notations. </summary>
        ''' <remarks>
        ''' copied from
        ''' https://stackoverflow.com/questions/6629020/evaluate-escaped-string/25471811#25471811.
        ''' </remarks>
        ''' <param name="s"> The string. </param>
        ''' <returns> A String. </returns>
        Private Shared Function Unescape(ByVal s As String) As String
            Dim sb As New StringBuilder()
            Dim r As New Regex("\\[abfnrtv?""'\\]|\\[0-3]?[0-7]{1,2}|\\u[0-9a-fA-F]{4}|\\U[0-9a-fA-F]{8}|.")
            Dim mc As MatchCollection = r.Matches(s, 0)
            For Each m As Match In mc
                If m.Length = 1 Then
                    sb.Append(m.Value)
                Else
                    If m.Value.Chars(1) >= "0"c AndAlso m.Value.Chars(1) <= "7"c Then
                        Dim i As Integer = Convert.ToInt32(m.Value.Substring(1), 8)
                        sb.Append(ChrW(i))
                    ElseIf m.Value.Chars(1) = "u"c Then
                        Dim i As Integer = Convert.ToInt32(m.Value.Substring(2), 16)
                        sb.Append(ChrW(i))
                    ElseIf m.Value.Chars(1) = "U"c Then
                        Dim i As Integer = Convert.ToInt32(m.Value.Substring(2), 16)
                        sb.Append(Char.ConvertFromUtf32(i))
                    Else
                        Select Case m.Value.Chars(1)
                            Case "a"c
                                sb.Append("\a")
                            Case "b"c
                                sb.Append(ControlChars.Back)
                            Case "f"c
                                sb.Append(ControlChars.FormFeed)
                            Case "n"c
                                sb.Append(ControlChars.Lf)
                            Case "r"c
                                sb.Append(ControlChars.Cr)
                            Case "t"c
                                sb.Append(ControlChars.Tab)
                            Case "v"c
                                sb.Append(ControlChars.VerticalTab)
                            Case Else
                                sb.Append(m.Value.Chars(1))
                        End Select
                    End If
                End If
            Next m
            Return sb.ToString()
        End Function
    End Class

#End Region

End Class

''' <summary> The <see cref="Env"/> load options. </summary>
''' <remarks>
''' (c) 2019 TONERDO. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 10/16/2019, https://github.com/tonerdo/dotnet-env. </para>
''' </remarks>
Public Class EnvLoadOptions

    ''' <summary> Gets or sets the trim white space. </summary>
    ''' <value> The trim white space. </value>
    Public ReadOnly Property TrimWhiteSpace As Boolean

    ''' <summary> Gets or sets the is embedded hash comment. </summary>
    ''' <value> The is embedded hash comment. </value>
    Public ReadOnly Property IsEmbeddedHashComment As Boolean

    ''' <summary> Gets or sets the unescape quoted values. </summary>
    ''' <value> The unescape quoted values. </value>
    Public ReadOnly Property UnescapeQuotedValues As Boolean

    ''' <summary> Gets or sets the clobber existing values. </summary>
    ''' <value> The clobber existing values. </value>
    Public ReadOnly Property ClobberExistingValues As Boolean

    ''' <summary> Gets or sets the require environment file. </summary>
    ''' <value> The require environment file. </value>
    Public ReadOnly Property RequireEnvFile As Boolean

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="trimWhiteSpace">        (Optional) True to trim white space. </param>
    ''' <param name="isEmbeddedHashComment"> (Optional) True if is embedded hash comment, false if
    '''                                      not. </param>
    ''' <param name="unescapeQuotedValues">  (Optional) True to unescape quoted values. </param>
    ''' <param name="clobberExistingValues"> (Optional) True to clobber existing values. </param>
    ''' <param name="requireEnvFile">        (Optional) True to require environment file. </param>
    Public Sub New(Optional trimWhiteSpace As Boolean = True, Optional isEmbeddedHashComment As Boolean = True,
                   Optional unescapeQuotedValues As Boolean = True, Optional clobberExistingValues As Boolean = True,
                   Optional requireEnvFile As Boolean = True)
        Me.TrimWhiteSpace = trimWhiteSpace
        Me.IsEmbeddedHashComment = isEmbeddedHashComment
        Me.UnescapeQuotedValues = unescapeQuotedValues
        Me.ClobberExistingValues = clobberExistingValues
        Me.RequireEnvFile = requireEnvFile
    End Sub
End Class

