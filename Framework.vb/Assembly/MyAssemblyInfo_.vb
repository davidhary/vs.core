Imports Microsoft.VisualBasic.ApplicationServices
Public Class MyAssemblyInfo
    Inherits AssemblyInfo

    ''' <summary> Constructor. </summary>
    ''' <param name="assembly"> The assembly. </param>
    Public Sub New(ByVal assembly As System.Reflection.Assembly)
        MyBase.New(assembly)
        Me.Assembly = assembly
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As AssemblyInfo)
        Me.New(AssemblyExtensions.GetAssembly(value))
    End Sub

#Region " EXTENDED INFORMATION "

    ''' <summary> Gets the assembly. </summary>
    ''' <value> The assembly. </value>
    Public Property Assembly As System.Reflection.Assembly

    ''' <summary> Gets the pathname of the application folder. </summary>
    ''' <value> The pathname of the application folder. </value>
    Public ReadOnly Property ApplicationFolder As String
        Get
            Return Me.DirectoryPath
        End Get
    End Property

    ''' <summary> Returns the application file name (with extension). </summary>
    ''' <value> The name of the application. </value>
    Public ReadOnly Property ApplicationFileName() As String
        Get
            Return Me.FileInfo.Name
        End Get
    End Property

    ''' <summary> Returns the application file name (without extension). </summary>
    ''' <value> The name of the application. </value>
    Public ReadOnly Property ApplicationFileTitle() As String
        Get
            Return Me.AssemblyName
        End Get
    End Property

    ''' <summary> Returns the application Title. </summary>
    ''' <value> The Title of the application. </value>
    Public ReadOnly Property ApplicationTitle() As String
        Get
            Return Me.Title
        End Get
    End Property

    Private _FileInfo As System.IO.FileInfo
    ''' <summary> Gets the file info for the assembly file. </summary>
    ''' <value> Information describing the file version. </value>
    Public ReadOnly Property FileInfo As System.IO.FileInfo
        Get
            If Me._FileInfo Is Nothing Then
                Me._FileInfo = New System.IO.FileInfo(Me.Location)
            End If
            Return Me._FileInfo
        End Get
    End Property

    Private _FileVersionInfo As FileVersionInfo
    ''' <summary> Gets information describing the file version. </summary>
    ''' <value> Information describing the file version. </value>
    Public ReadOnly Property FileVersionInfo As FileVersionInfo
        Get
            If Me._FileVersionInfo Is Nothing Then
                Me._FileVersionInfo = AssemblyExtensions.GetFileVersionInfo(Me.Assembly)
            End If
            Return Me._FileVersionInfo
        End Get
    End Property

    ''' <summary> Gets the location. </summary>
    ''' <value> The location. </value>
    Public ReadOnly Property Location As String
        Get
            Return Me.Assembly.Location
        End Get
    End Property

    ''' <summary> Gets the public key. </summary>
    ''' <value> The public key. </value>
    Public ReadOnly Property PublicKey As String
        Get
            Return AssemblyExtensions.GetPublicKey(Me.Assembly)
        End Get
    End Property

    ''' <summary> Gets the public key token. </summary>
    ''' <value> The public key token. </value>
    Public ReadOnly Property PublicKeyToken As String
        Get
            Return AssemblyExtensions.GetPublicKeyToken(Me.Assembly)
        End Get
    End Property

    ''' <summary> Gets the product version. </summary>
    ''' <returns> The product version. </returns>
    Public Function ProductVersion(ByVal format As String) As String
        Return AssemblyExtensions.ProductVersion(Me, format)
    End Function

    ''' <summary> Returns the application product version. </summary>
    ''' <returns> The product version. </returns>
    Public Function ProductVersion(ByVal fieldCount As Integer) As String
        Return Me.Version.ToString(fieldCount)
    End Function

#End Region

#Region " CAPTION "

    ''' <summary> Gets the release candidate prefix. </summary>
    ''' <value> The release candidate prefix. </value>
    Public Property ReleaseCandidatePrefix As String = "RC"

    ''' <summary> Gets the alpha version prefix. </summary>
    ''' <value> The alpha version prefix. </value>
    Public Property AlphaVersionPrefix As String = "Alpha"

    ''' <summary> Gets the beta version prefix. </summary>
    ''' <value> The beta version prefix. </value>
    Public Property BetaVersionPrefix As String = "Beta"

    ''' <summary> Gets the gold version prefix. </summary>
    ''' <value> The gold version prefix. </value>
    Public Property GoldVersionPrefix As String = "Gold"

    ''' <summary> Builds the Application Title caption. </summary>
    ''' <param name="subtitle"> The subtitle. </param>
    ''' <returns> System.String. </returns>
    Private Function BuildCaption(ByVal title As String, ByVal subtitle As String) As String

        Dim builder As New System.Text.StringBuilder
        builder.Append(title)
        builder.Append(" ")
        builder.Append(Me.ProductVersion(3))
        If Me.Version.Major < 1 Then
            builder.Append(".")
            Select Case Me.Version.Minor
                Case 0
                    builder.Append(Me.AlphaVersionPrefix)
                Case 1
                    builder.Append(Me.BetaVersionPrefix)
                Case 2 To 8
                    builder.Append($"{Me.ReleaseCandidatePrefix}{Me.Version.Minor - 1}")
                Case Else
                    builder.Append(Me.GoldVersionPrefix)
            End Select
        End If
        If Not String.IsNullOrWhiteSpace(subtitle) Then
            builder.Append(": ")
            builder.Append(subtitle)
        End If
        Return builder.ToString

    End Function

    ''' <summary> Builds the application caption using the application title. </summary>
    ''' <param name="subtitle"> The subtitle. </param>
    ''' <returns> System.String. </returns>
    Public Function BuildDefaultCaption(ByVal subtitle As String) As String
        Return Me.BuildCaption(Me.Title, subtitle)
    End Function

    ''' <summary> Builds the application caption using the application title. </summary>
    ''' <param name="subtitle"> The subtitle. </param>
    ''' <returns> System.String. </returns>
    Public Function BuildTitleCaption(ByVal subtitle As String) As String
        Return Me.BuildCaption(Me.Title, subtitle)
    End Function

    ''' <summary> Builds the application caption using the application title. </summary>
    ''' <param name="subtitle"> The subtitle. </param>
    ''' <returns> System.String. </returns>
    Public Function BuildDescriptionCaption(ByVal subtitle As String) As String
        Return Me.BuildCaption(Me.Description, subtitle)
    End Function

#End Region

#Region " PRODUCT PROCESS CAPTION "

    ''' <summary> Prefix process name to the product name. </summary>
    ''' <returns> A String. </returns>
    Public Function PrefixProcessName() As String
        Dim name As String = Me.ProductName
        Dim processName As String = Process.GetCurrentProcess().ProcessName
        If Not name.StartsWith(processName, StringComparison.OrdinalIgnoreCase) Then name = $"{processName}.{name}"
        Return name
    End Function

    ''' <summary> Builds product time caption. </summary>
    ''' <param name="versionElements">   The version elements. </param>
    ''' <param name="timeCaptionFormat"> The time caption format. </param>
    ''' <param name="kindFormat">        The kind format. </param>
    ''' <returns> A String. </returns>
    ''' <remarks>
    ''' <list type="bullet">Use the following format options: <item>
    ''' u - UTC - 2019-09-10 19:27:04Z</item><item>
    ''' r - GMT - Tue, 10 May 2019 19:26:42 GMT</item><item>
    ''' o - ISO - 2019-09-10T12:12:29.7552627-07:00</item><item>
    ''' s - ISO - 2019-09-10T12:24:47</item><item>
    ''' empty   - 9/10/2019 16:57:24 -07:00</item><item>
    ''' s + zzz - 2019-09-10T12:24:47-07:00</item></list>
    '''          </remarks>
    Public Function BuildProductTimeCaption(ByVal versionElements As Integer, ByVal timeCaptionFormat As String, ByVal kindFormat As String) As String
        Dim result As String
        If String.IsNullOrWhiteSpace(timeCaptionFormat) Then
            result = $"{Me.PrefixProcessName}.r.{Me.ProductVersion(versionElements)} {System.DateTimeOffset.Now}"
        ElseIf String.IsNullOrWhiteSpace(kindFormat) Then
            result = $"{Me.PrefixProcessName}.r.{Me.ProductVersion(versionElements)} {System.DateTimeOffset.Now.ToString(timeCaptionFormat)}"
        Else
            result = $"{Me.PrefixProcessName}.r.{Me.ProductVersion(versionElements)} {DateTimeOffset.Now.ToString(timeCaptionFormat)}{DateTimeOffset.Now.ToString(kindFormat)}"
        End If
        Return result
    End Function

    ''' <summary> Builds product time caption using full version and local time plus kind format. </summary>
    ''' <returns> A String. </returns>
    Public Function BuildProductTimeCaption() As String
        Return Me.BuildProductTimeCaption(4, String.Empty, String.Empty)
    End Function

#End Region

#Region " APPLICATION DATA FOLDER "

    ''' <summary>
    ''' Gets the application data folder Path. This seems to create the folder if it does not exist.
    ''' </summary>
    ''' <param name="allUsers"> True to select the folder for
    '''                         <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.AllUsersApplicationData"/>
    '''                         all users; otherwise, use using current user; otherwise, use the
    '''                         folder for
    '''                         <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.CurrentUserApplicationData"/> </param>
    ''' <returns> System.String. </returns>
    Public Shared Function OpenApplicationDataFolderPath(ByVal allUsers As Boolean) As String
        Return If(allUsers, My.Computer.FileSystem.SpecialDirectories.AllUsersApplicationData,
                    My.Computer.FileSystem.SpecialDirectories.CurrentUserApplicationData)
    End Function

#End Region

#Region " APPLICATION CONFIGURATION FILE "

    ''' <summary>
    ''' The default configuration extension
    ''' </summary>
    Private ReadOnly _DefaultConfigExtension As String = ".config"

    ''' <summary> Returns the file path of the default application configuration file. The folder is
    ''' created if it does not exists. </summary>
    ''' <param name="allUsers">           True to select the folder for
    '''                                   <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.AllUsersApplicationData"/>
    '''                                   all users; otherwise, use using current user; otherwise,
    '''                                   use the folder for
    '''                                   <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.CurrentUserApplicationData"/> </param>
    ''' <param name="significantVersionElements"> The significant version elements. A value between 1
    ''' and 4 indicating the version level to use. For example, with 1, only the major version of the
    ''' folder name is used; whereas with 4 all version elements are used. Defaults to 4. </param>
    ''' <returns> The configuration file full path. Uses the application folder if this folder is
    ''' writable. Otherwise the <see cref="OpenApplicationConfigFolderPath">application data
    ''' folder</see> is used. System.String. </returns>
    Public Function BuildApplicationConfigFilePath(ByVal allUsers As Boolean, ByVal significantVersionElements As Integer) As String
        Return Me.BuildApplicationConfigFilePath(allUsers, Me._DefaultConfigExtension, significantVersionElements)
    End Function

    ''' <summary>
    ''' Returns the file path of the application configuration file. The folder is created if it does
    ''' not exists.
    ''' </summary>
    ''' <param name="allUsers">                   True to select the folder for
    '''                                           <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.AllUsersApplicationData"/>
    '''                                           all users; otherwise, use using current user;
    '''                                           otherwise, use the folder for
    '''                                           <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.CurrentUserApplicationData"/> </param>
    ''' <param name="extension">                  The extension. </param>
    ''' <param name="significantVersionElements"> The significant version elements. A value between 1
    '''                                           and 4 indicating the version level to use. For
    '''                                           example, with 1, only the major version of the
    '''                                           folder name is used; whereas with 4 all version
    '''                                           elements are used. Defaults to 4. </param>
    ''' <returns>
    ''' The configuration file full path. Uses the application folder if this folder is writable.
    ''' Otherwise the <see cref="OpenApplicationConfigFolderPath">application data folder</see> is
    ''' used. System.String.
    ''' </returns>
    Public Function BuildApplicationConfigFilePath(ByVal allUsers As Boolean, ByVal extension As String, ByVal significantVersionElements As Integer) As String
        Return System.IO.Path.Combine(MyAssemblyInfo.OpenApplicationConfigFolderPath(allUsers, significantVersionElements),
                                      Me.FileInfo.Name & extension)
    End Function

    ''' <summary> Opens the application configuration folder path. </summary>
    ''' <param name="allUsers">                   True to select the folder for
    '''                                           <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.AllUsersApplicationData"/>
    '''                                           all users; otherwise, use using current user;
    '''                                           otherwise, use the folder for
    '''                                           <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.CurrentUserApplicationData"/> </param>
    ''' <param name="significantVersionElements"> The significant version elements. A value between 1
    '''                                           and 4 indicating the version level to use. For
    '''                                           example, with 1, only the major version of the
    '''                                           folder name is used; whereas with 4 all version
    '''                                           elements are used. Defaults to 4. </param>
    ''' <returns>
    ''' The configuration folder full path. Uses the application folder if this folder is writable.
    ''' Otherwise the <see cref="OpenApplicationDataFolderPath">application data folder</see> is
    ''' used. System.String.
    ''' </returns>
    Public Shared Function OpenApplicationConfigFolderPath(ByVal allUsers As Boolean, ByVal significantVersionElements As Integer) As String
        Dim candidatePath As String = My.Application.Info.DirectoryPath
        If Not MyAssemblyInfo.IsFolderWritable(candidatePath) Then
            candidatePath = MyAssemblyInfo.OpenApplicationDataFolderPath(allUsers, significantVersionElements)
        End If
        Return candidatePath
    End Function

#End Region

#Region " APPLICATION DATA FOLDER -- CUSTOMIZED "


    ''' <summary> Returns the Folder Path with the relevant version information elements replaces with
    ''' '0'. Creates the folder if it does not exist. </summary>
    ''' <param name="dataFolderPath">             Name of the data path. </param>
    ''' <param name="significantVersionElements"> The significant version elements. A value between 1
    ''' and 4 indicating the version level to use. For example, with 1, only the major version of the
    ''' folder name is used; whereas with 4 all version elements are used. Defaults to 4. </param>
    ''' <returns> A folder name with the revision numbers (if there) truncated based on the number of
    ''' significant elements. For example, with two significant digits we get: ...major.minor.0.0;
    ''' or. </returns>
    Public Shared Function OpenDataFolderPath(ByVal dataFolderPath As String, ByVal significantVersionElements As Integer) As String
        significantVersionElements = If(significantVersionElements < 1, 1, If(significantVersionElements > 4, 4, significantVersionElements))
        Dim di As New System.IO.DirectoryInfo(MyAssemblyInfo.ReplaceTrailingElements(dataFolderPath, "."c, 4 - significantVersionElements, "0"))
        If Not di.Exists Then
            di.Create()
        End If
        Return di.FullName
    End Function

    ''' <summary>
    ''' Gets the application data folder Path. The folder is created if it does not exist.
    ''' </summary>
    ''' <param name="allUsers">                   True to select the folder for
    '''                                           <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.AllUsersApplicationData"/>
    '''                                           all users; otherwise, use using current user;
    '''                                           otherwise, use the folder for
    '''                                           <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.CurrentUserApplicationData"/> </param>
    ''' <param name="significantVersionElements"> The significant version elements. A value between 1
    '''                                           and 4 indicating the version level to use. For
    '''                                           example, with 1, only the major version of the
    '''                                           folder name is used; whereas with 4 all version
    '''                                           elements are used. Defaults to 4. </param>
    ''' <returns>
    ''' The application data folder for the product. With All Users:<para>
    ''' </para><para>
    ''' In Windows XP: .\ Application Data\&lt;company&gt;\&lt;product name&gt;\major.minor.0.0;</para><para>
    ''' or In Windows 7 or 8: C:\ProgramData\&lt;company&gt;\&lt;product name&gt;\major.minor.0.0</para><para>
    ''' For the Current User:</para><para>
    ''' In Windows XP: .\&lt;user&gt;\documents and settings\AppData\&lt;company&gt;\&lt;product name&gt;\major.minor.0.0; </para><para>
    ''' or In Windows 7 or 8: C:\Users\&lt;user&gt;\AppData\Roaming\\&lt;company&gt;\&lt;product name&gt;\major.minor.0.0.</para><para>
    ''' The version information is based on the Assembly File Version.</para>
    ''' </returns>
    Public Shared Function OpenApplicationDataFolderPath(ByVal allUsers As Boolean, ByVal significantVersionElements As Integer) As String
        Return MyAssemblyInfo.OpenDataFolderPath(MyAssemblyInfo.OpenApplicationDataFolderPath(allUsers), significantVersionElements)
    End Function

#End Region

#Region " HELPER FUNCTIONS "

    ''' <summary> Determines whether the specified folder path is writable. </summary>
    ''' <remarks> Uses a temporary random file name to test if the file can be created. The file is
    ''' deleted thereafter. </remarks>
    ''' <param name="path"> The path. </param>
    ''' <returns> <c>True</c> if the specified path is writable; otherwise, <c>False</c>. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function IsFolderWritable(ByVal path As String) As Boolean
        Dim filePath As String = String.Empty
        Dim affirmative As Boolean = False
        Try
            filePath = System.IO.Path.Combine(path, System.IO.Path.GetRandomFileName())
            Using s As System.IO.FileStream = System.IO.File.Open(filePath, System.IO.FileMode.OpenOrCreate)
            End Using
            affirmative = True
        Catch
        Finally
            ' SS reported an exception from this test possibly indicating that Windows allowed writing the file 
            ' by failed report deletion. Or else, Windows raised another exception type.
            Try
                If System.IO.File.Exists(filePath) Then
                    System.IO.File.Delete(filePath)
                End If
            Catch
            End Try
        End Try
        Return affirmative
    End Function

    ''' <summary> Drops the trailing elements. </summary>
    ''' <param name="value">     The value. </param>
    ''' <param name="delimiter"> The delimiter. </param>
    ''' <param name="count">     The count. </param>
    ''' <returns> System.String. </returns>
    Public Shared Function DropTrailingElements(ByVal value As String, ByVal delimiter As Char, ByVal count As Integer) As String
        If String.IsNullOrWhiteSpace(value) Then
            Return value
        ElseIf String.IsNullOrWhiteSpace(delimiter) Then
            Return value
        Else
            Do While count > 0
                count -= 1
                Dim i As Integer = value.LastIndexOf(delimiter)
                If i > 0 Then
                    value = value.Substring(0, i)
                Else
                    count = 0
                End If
            Loop
            Return value
        End If
    End Function

    ''' <summary> Drops the trailing elements. </summary>
    ''' <param name="value">     The value. </param>
    ''' <param name="delimiter"> The delimiter. </param>
    ''' <param name="count">     The count. </param>
    ''' <param name="replace">   The replace. </param>
    ''' <returns> System.String. </returns>
    Public Shared Function ReplaceTrailingElements(ByVal value As String, ByVal delimiter As Char, ByVal count As Integer, ByVal replace As String) As String
        If String.IsNullOrWhiteSpace(value) Then
            Return value
        ElseIf String.IsNullOrWhiteSpace(delimiter) Then
            Return value
        Else
            Dim replaceCount As Integer = 0
            Do While count > 0
                count -= 1
                Dim i As Integer = value.LastIndexOf(delimiter)
                If i > 0 Then
                    replaceCount += 1
                    value = value.Substring(0, i)
                Else
                    count = 0
                End If
            Loop
            Dim builder As New System.Text.StringBuilder(value)
            If replaceCount > 0 Then
                For i As Integer = 1 To replaceCount
                    builder.Append(delimiter)
                    builder.Append(replace)
                Next
            End If
            Return builder.ToString
        End If
    End Function

#End Region

#Region " TIME ZONE INFO "

    Private Shared _LocalTimeZoneId As String
    ''' <summary> Gets the identifier of the local Time zone. </summary>
    ''' <value> The identifier of the local Time zone. </value>
    Public Shared ReadOnly Property LocalTimeZoneId As String
        Get
            If String.IsNullOrWhiteSpace(MyAssemblyInfo._LocalTimeZoneId) Then MyAssemblyInfo._LocalTimeZoneId = System.TimeZoneInfo.Local.Id
            Return MyAssemblyInfo._LocalTimeZoneId
        End Get
    End Property

    Private Shared _LocalTimeZoneInfo As TimeZoneInfo

    ''' <summary> Gets information describing the local Time zone. </summary>
    ''' <value> Information describing the local Time zone. </value>
    Public Shared ReadOnly Property LocalTimeZoneInfo As TimeZoneInfo
        Get
            If MyAssemblyInfo._LocalTimeZoneInfo Is Nothing Then
                MyAssemblyInfo._LocalTimeZoneInfo = System.TimeZoneInfo.FindSystemTimeZoneById(MyAssemblyInfo.LocalTimeZoneId)
            End If
            Return MyAssemblyInfo._LocalTimeZoneInfo
        End Get
    End Property

#End Region

End Class

