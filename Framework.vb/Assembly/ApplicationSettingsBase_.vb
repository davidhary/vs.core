''' <summary> An application settings base. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2/12/2018 </para>
''' </remarks>
<Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute()>
Public Class ApplicationSettingsBase
    Inherits Global.System.Configuration.ApplicationSettingsBase

#Region " CONVERSIONS "

    ''' <summary> Converts a value to a nullable double. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> Value as a Double? </returns>
    Public Shared Function ToNullableDouble(ByVal value As String) As Double?
        Return If(String.IsNullOrWhiteSpace(value), New Double?, Convert.ToDouble(value))
    End Function

    ''' <summary> Converts a value to a nullable double. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> Value as a Double? </returns>
    Public Shared Function ToNullableDouble(ByVal value As Object) As Double?
        Return If(value Is Nothing, New Double?, Convert.ToDouble(value.ToString))
    End Function

    ''' <summary> Converts a value to a nullable Int32. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> Value as a Int32? </returns>
    Public Shared Function ToNullableInt32(ByVal value As String) As Int32?
        Return If(String.IsNullOrWhiteSpace(value), New Int32?, Convert.ToInt32(value))
    End Function

    ''' <summary> Converts a value to a nullable Int32. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> Value as a Int32? </returns>
    Public Shared Function ToNullableInt32(ByVal value As Object) As Int32?
        Return If(value Is Nothing, New Int32?, Convert.ToInt32(value.ToString))
    End Function

#End Region

#Region " SEPERATEED VALUES SUPPORT "

#Disable Warning CA1819 ' Properties should not return arrays

    ''' <summary> Gets or sets the rows delimiter. </summary>
    ''' <value> The rows delimiter. </value>
    Public Property RowsDelimiter As Char() = Environment.NewLine.ToCharArray
#Enable Warning CA1819 ' Properties should not return arrays

#Disable Warning CA1819 ' Properties should not return arrays

    ''' <summary> Gets or sets the values delimiter. </summary>
    ''' <value> The values delimiter. </value>
    Public Property ValuesDelimiter As Char() = New Char() {"|"c}
#Enable Warning CA1819 ' Properties should not return arrays

    ''' <summary> Splits  the <paramref name="values"/> to array of arrays. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="values">         The values. </param>
    ''' <param name="itemDelimiter">  The item delimiter. </param>
    ''' <param name="groupDelimiter"> The group delimiter. </param>
    ''' <returns> A T()() </returns>
    Public Shared Function Split(Of T)(ByVal values As String, ByVal itemDelimiter As Char(), ByVal groupDelimiter As Char()) As T()()
        Dim results As T()() = Nothing
        If Not String.IsNullOrWhiteSpace(values) Then
            Dim rows As String() = values.Split(groupDelimiter)
            ReDim results(rows.Count - 1)
            Dim i As Integer = 0
            For Each row As String In rows
                Dim l As New List(Of T)
                If Not String.IsNullOrWhiteSpace(row) Then
                    For Each v As String In row.Split(itemDelimiter)
                        l.Add(CType(Convert.ChangeType(v.Trim, GetType(T)), T))
                    Next
                End If
                results(i) = l.ToArray
                i += 1
            Next
        End If
        Return results
    End Function

    ''' <summary>
    ''' Joins the <paramref name="values"/> to a string of values each separated by
    ''' <paramref name="itemDelimiter"/> and then by <paramref name="groupDelimiter"/>
    ''' </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="values">         The values array of arrays which to join. </param>
    ''' <param name="itemDelimiter">  The item delimiter. </param>
    ''' <param name="groupDelimiter"> The group delimiter. </param>
    ''' <returns> A String. </returns>
    Public Shared Function Join(Of T)(ByVal values As T()(), ByVal itemDelimiter As Char(), ByVal groupDelimiter As Char()) As String
        Dim result As New System.Text.StringBuilder
        For Each row As T() In values
            For Each v As T In row
                result.Append($"{v}{itemDelimiter}")
            Next
            result.Append(groupDelimiter)
        Next
        Return result.ToString.TrimEnd(groupDelimiter).TrimEnd(itemDelimiter)
    End Function

#End Region

#Region " BASE PROPERTIES "

    ''' <summary> Gets or sets the application setting <see cref="Boolean"/> value. </summary>
    ''' <param name="name"> (Optional) Name of the caller member. </param>
    ''' <value> The application setting <see cref="Boolean"/> value. </value>
    Protected Property AppSettingBoolean(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Boolean
        Get
            Return CType(Me(name), Boolean)
        End Get
        Set(value As Boolean)
            Me(name) = value
        End Set
    End Property

    ''' <summary> Gets or sets the application setting values. </summary>
    ''' <param name="name"> (Optional) Name of the caller member. </param>
    ''' <value> The application setting values. </value>
    Protected Property AppSettingBooleanValues(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As IEnumerable(Of Boolean)
        Get
            Dim values As String = CStr(MyBase.PropertyValues.Item(name).SerializedValue)
            Dim l As New List(Of Boolean)
            If Not String.IsNullOrWhiteSpace(values) Then
                For Each v As String In values.Split(Me.ValuesDelimiter)
                    l.Add(Convert.ToBoolean(v.Trim))
                Next
            End If
            Return l
        End Get
        Set(value As IEnumerable(Of Boolean))
            Dim builder As New System.Text.StringBuilder
            For Each v As Boolean In value
                builder.Append($"{v}{Me.ValuesDelimiter}")
            Next
            MyBase.PropertyValues.Item(name).SerializedValue = builder.ToString.TrimEnd(Me.ValuesDelimiter)
        End Set
    End Property

    ''' <summary> Gets or sets the application setting <see cref="Byte"/> value. </summary>
    ''' <param name="name"> (Optional) Name of the caller member. </param>
    ''' <value> The application setting <see cref="Byte"/> value. </value>
    Protected Property AppSettingByte(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Byte
        Get
            Return CType(Me(name), Byte)
        End Get
        Set(value As Byte)
            Me(name) = value
        End Set
    End Property

    ''' <summary> Gets or sets the application setting date time offset. </summary>
    ''' <param name="name"> (Optional) Name of the caller member. </param>
    ''' <value> The application setting date time offset. </value>
    Protected Property AppSettingDateTimeOffset(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As DateTimeOffset
        Get
            Return DateTimeOffset.Parse(Me(name).ToString)
        End Get
        Set(value As DateTimeOffset)
            Me(name) = value.ToString
        End Set
    End Property

    ''' <summary> Gets or sets the application setting date time. </summary>
    ''' <param name="name"> (Optional) Name of the caller member. </param>
    ''' <value> The application setting date time. </value>
    Protected Property AppSettingDateTime(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As DateTime
        Get
            Return DateTime.Parse(Me(name).ToString)
        End Get
        Set(value As DateTime)
            Me(name) = value.ToString
        End Set
    End Property

    ''' <summary> Gets or sets the application setting <see cref="Double"/> value. </summary>
    ''' <param name="name"> (Optional) Name of the caller member. </param>
    ''' <value> The application setting <see cref="Double"/> value. </value>
    Protected Property AppSettingDecimal(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Decimal
        Get
            Return CType(Me(name), Decimal)
        End Get
        Set(value As Decimal)
            Me(name) = value
        End Set
    End Property

    ''' <summary> Gets or sets the application setting <see cref="Double"/> value. </summary>
    ''' <param name="name"> (Optional) Name of the caller member. </param>
    ''' <value> The application setting <see cref="Double"/> value. </value>
    Protected Property AppSettingDouble(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Double
        Get
            Return CType(Me(name), Double)
        End Get
        Set(value As Double)
            Me(name) = value
        End Set
    End Property

    ''' <summary> Gets or sets the application setting <see cref="Double?"/> value. </summary>
    ''' <param name="name"> (Optional) Name of the caller member. </param>
    ''' <value> The application setting nullable <see cref="Double?"/> value. </value>
    Protected Property AppSettingNullableDouble(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Double?
        Get
            Return ApplicationSettingsBase.ToNullableDouble(Me(name))
        End Get
        Set(value As Double?)
            Me(name) = If(value, New Object)
        End Set
    End Property

    ''' <summary> Gets or sets the application setting <see cref="Double"/> value. </summary>
    ''' <param name="name"> (Optional) Name of the caller member. </param>
    ''' <value> The application setting <see cref="Double"/> value. </value>
    Protected Property AppSettingDoubleValues(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As IEnumerable(Of Double)
        Get
            Dim l As New List(Of Double)
            Dim values As String = CStr(MyBase.PropertyValues.Item(name).SerializedValue)
            If Not String.IsNullOrWhiteSpace(values) Then
                For Each v As String In values.Split(Me.ValuesDelimiter)
                    l.Add(Convert.ToDouble(v.Trim))
                Next
            End If
            Return l
        End Get
        Set(value As IEnumerable(Of Double))
            Dim builder As New System.Text.StringBuilder
            For Each v As Double In value
                builder.Append($"{v}{Me.ValuesDelimiter}")
            Next
            MyBase.PropertyValues.Item(name).SerializedValue = builder.ToString.TrimEnd(Me.ValuesDelimiter)
        End Set
    End Property

#Disable Warning CA1819 ' Properties should not return arrays

    ''' <summary> Gets or sets the application setting double arrays. </summary>
    ''' <param name="name"> (Optional) Name of the caller member. </param>
    ''' <value> The application setting double arrays. </value>
    Protected Property AppSettingDoubleArrays(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Double()()
#Enable Warning CA1819 ' Properties should not return arrays
        Get
            Return ApplicationSettingsBase.Split(Of Double)(CStr(MyBase.PropertyValues.Item(name).SerializedValue), Me.ValuesDelimiter, Me.RowsDelimiter)
        End Get
        Set(value As Double()())
            MyBase.PropertyValues.Item(name).SerializedValue = ApplicationSettingsBase.Join(value, Me.ValuesDelimiter, Me.RowsDelimiter)
        End Set
    End Property

    ''' <summary> Gets or sets the application setting <see cref="Int32"/> value. </summary>
    ''' <param name="name"> (Optional) Name of the caller member. </param>
    ''' <value> The application setting <see cref="Int32"/> value. </value>
    Protected Property AppSettingInt32(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Integer
        Get
            Return CType(Me(name), Integer)
        End Get
        Set(value As Integer)
            Me(name) = value
        End Set
    End Property

    ''' <summary> Gets or sets the application setting nullable int 32. </summary>
    ''' <param name="name"> (Optional) Name of the caller member. </param>
    ''' <value> The application setting nullable int 32. </value>
    Protected Property AppSettingNullableInt32(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Integer?
        Get
            Return ApplicationSettingsBase.ToNullableInt32(Me(name))
        End Get
        Set(value As Integer?)
            Me(name) = If(value, New Object)
        End Set
    End Property

#Disable Warning CA1819 ' Properties should not return arrays

    ''' <summary> Gets or sets the application setting int 32 arrays. </summary>
    ''' <param name="name"> (Optional) Name of the caller member. </param>
    ''' <value> The application setting int 32 arrays. </value>
    Protected Property AppSettingInt32Arrays(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Int32()()
#Enable Warning CA1819 ' Properties should not return arrays
        Get
            Return ApplicationSettingsBase.Split(Of Int32)(CStr(MyBase.PropertyValues.Item(name).SerializedValue), Me.ValuesDelimiter, Me.RowsDelimiter)
        End Get
        Set(value As Int32()())
            MyBase.PropertyValues.Item(name).SerializedValue = ApplicationSettingsBase.Join(value, Me.ValuesDelimiter, Me.RowsDelimiter)
        End Set
    End Property

    ''' <summary> Gets or sets the application setting <see cref="Int64"/> value. </summary>
    ''' <param name="name"> (Optional) Name of the caller member. </param>
    ''' <value> The application setting <see cref="Int64"/> value. </value>
    Protected Property AppSettingInt64(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Long
        Get
            Return CType(Me(name), Long)
        End Get
        Set(value As Long)
            Me(name) = value
        End Set
    End Property

    ''' <summary> Gets or sets the application setting <see cref="Double"/> value. </summary>
    ''' <param name="name"> (Optional) Name of the caller member. </param>
    ''' <value> The application setting <see cref="Double"/> value. </value>
    Protected Property AppSettingSingle(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Single
        Get
            Return CType(Me(name), Single)
        End Get
        Set(value As Single)
            Me(name) = value
        End Set
    End Property

    ''' <summary> Gets or sets the application setting <see cref="Double"/> value. </summary>
    ''' <param name="name"> (Optional) Name of the caller member. </param>
    ''' <value> The application setting <see cref="Double"/> value. </value>
    Protected Property AppSettingTimeSpan(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As TimeSpan
        Get
            Return TimeSpan.Parse(Me(name).ToString)
        End Get
        Set(value As TimeSpan)
            Me(name) = value.ToString
        End Set
    End Property

    ''' <summary> Gets or sets the application setting value. </summary>
    ''' <param name="name"> (Optional) Name of the caller member. </param>
    ''' <value> The application setting value. </value>
    Protected Property AppSettingValue(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As String
        Get
            Return Me(name).ToString
        End Get
        Set(value As String)
            Me(name) = value
        End Set
    End Property

    ''' <summary> Gets or sets the application setting values. </summary>
    ''' <param name="name"> (Optional) Name of the caller member. </param>
    ''' <value> The application setting values. </value>
    Protected Property AppSettingValues(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As IEnumerable(Of String)
        Get
            Dim values As String = CStr(MyBase.PropertyValues.Item(name).SerializedValue)
            Dim l As New List(Of String)
            If Not String.IsNullOrWhiteSpace(values) Then
                For Each v As String In values.Split(Me.ValuesDelimiter)
                    l.Add(v.Trim)
                Next
            End If
            Return l
        End Get
        Set(value As IEnumerable(Of String))
            Dim builder As New System.Text.StringBuilder
            For Each v As String In value
                builder.Append($"{v}{Me.ValuesDelimiter}")
            Next
            MyBase.PropertyValues.Item(name).SerializedValue = builder.ToString.TrimEnd(Me.ValuesDelimiter)
        End Set
    End Property

    ''' <summary> Gets or sets the application setting values. </summary>
    ''' <param name="name"> (Optional) Name of the caller member. </param>
    ''' <value> The application setting values. </value>
    Protected Property AppSettingValues(ByVal delimiter As Char(), <Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As IEnumerable(Of String)
        Get
            Dim values As String = CStr(MyBase.PropertyValues.Item(name).SerializedValue)
            Dim l As New List(Of String)
            If Not String.IsNullOrWhiteSpace(values) Then
                For Each v As String In values.Split(delimiter)
                    l.Add(v.Trim)
                Next
            End If
            Return l
        End Get
        Set(value As IEnumerable(Of String))
            Dim builder As New System.Text.StringBuilder
            For Each v As String In value
                builder.Append($"{v}{delimiter}")
            Next
            MyBase.PropertyValues.Item(name).SerializedValue = builder.ToString.TrimEnd(delimiter)
        End Set
    End Property

#Disable Warning CA1819 ' Properties should not return arrays

    ''' <summary> Gets or sets the application setting arrays. </summary>
    ''' <param name="name"> (Optional) Name of the caller member. </param>
    ''' <value> The application setting arrays. </value>
    Protected Property AppSettingArrays(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As String()()
#Enable Warning CA1819 ' Properties should not return arrays
        Get
            Return ApplicationSettingsBase.Split(Of String)(CStr(MyBase.PropertyValues.Item(name).SerializedValue), Me.ValuesDelimiter, Me.RowsDelimiter)
        End Get
        Set(value As String()())
            MyBase.PropertyValues.Item(name).SerializedValue = ApplicationSettingsBase.Join(value, Me.ValuesDelimiter, Me.RowsDelimiter)
        End Set
    End Property

    ''' <summary> Application setting enum. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="name"> (Optional) Name of the caller member. </param>
    ''' <returns> An ENUM value. </returns>
    Protected Function AppSettingEnum(Of T)(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As T
        Return CType(System.Enum.Parse(GetType(T), Me(name).ToString), T)
    End Function

    ''' <summary> Gets or sets the application setting <see cref="Object"/>. </summary>
    ''' <param name="name"> (Optional) Name of the caller member. </param>
    ''' <value> The application setting <see cref="Object"/>. </value>
    Protected Property AppSetting(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Object
        Get
            Return Me(name)
        End Get
        Set(value As Object)
            Me(name) = value
        End Set
    End Property

#End Region

End Class

