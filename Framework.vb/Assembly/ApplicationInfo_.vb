'           System.Linq


''' <summary> A sealed class designed to provide application information for the assembly. </summary>
''' <remarks> (c) 2005 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 01/15/2005, 1.0.1841.x. </para></remarks>
Public NotInheritable Class ApplicationInfo

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="ApplicationInfo" /> class. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    Private Sub New()
        MyBase.New()
    End Sub

#End Region

#Region " APPLICATION INFORMATION "

    ''' <summary> Full pathname of the application folder file. </summary>
    Private Shared _ApplicationFolder As String

    ''' <summary> Gets or sets or Sets the folder where application files are located. </summary>
    ''' <value> The application folder path. </value>
    Public Shared Property ApplicationFolder() As String
        Get
            If String.IsNullOrEmpty(ApplicationInfo._ApplicationFolder) Then
                ApplicationInfo._ApplicationFolder = My.Application.Info.DirectoryPath
            End If
            Return ApplicationInfo._ApplicationFolder
        End Get
        Set(ByVal value As String)
            If Not String.IsNullOrEmpty(value) Then
                ApplicationInfo._ApplicationFolder = value
            End If
        End Set
    End Property

    ''' <summary> Gets the application filename. </summary>
    ''' <value> The application filename. </value>
    Public Shared ReadOnly Property FileName As String
        Get
            ' Return $"{My.Application.Info.AssemblyName}.exe"
            Return System.IO.Path.Combine(My.Application.Info.DirectoryPath, $"{My.Application.Info.AssemblyName}.exe")
        End Get
    End Property

    ''' <summary> Returns the application public key. </summary>
    ''' <value> The public key of the application. </value>
    Public Shared ReadOnly Property ApplicationPublicKey() As String
        Get
            Dim values As Byte() = System.Reflection.Assembly.GetExecutingAssembly.GetName.GetPublicKey
            Return values.Aggregate(String.Empty, Function(current As String, value As Byte) (current & value.ToString("X2")))
        End Get
    End Property

    ''' <summary> Returns the application name. </summary>
    ''' <value> The name of the application. </value>
    Public Shared ReadOnly Property ApplicationName() As String
        Get
            Return My.Application.Info.AssemblyName
        End Get
    End Property

    ''' <summary> The application Description. </summary>
    Private Shared _ApplicationDescription As String

    ''' <summary>
    ''' Gets or sets or Sets the application Description.  This is required for setting the error
    ''' sources.
    ''' </summary>
    ''' <value> The application Description. </value>
    Public Shared Property ApplicationDescription() As String
        Get
            If String.IsNullOrEmpty(ApplicationInfo._ApplicationDescription) Then
                ApplicationInfo._ApplicationDescription = My.Application.Info.Description
            End If
            Return ApplicationInfo._ApplicationDescription
        End Get
        Set(ByVal value As String)
            If Not String.IsNullOrEmpty(value) Then
                ApplicationInfo._ApplicationDescription = value
            End If
        End Set
    End Property

    ''' <summary> The application title. </summary>
    Private Shared _ApplicationTitle As String

    ''' <summary>
    ''' Gets or sets or Sets the application title.  This is required for setting the error sources.
    ''' </summary>
    ''' <value> The application title. </value>
    Public Shared Property ApplicationTitle() As String
        Get
            If String.IsNullOrEmpty(ApplicationInfo._ApplicationTitle) Then
                ApplicationInfo._ApplicationTitle = My.Application.Info.Title
            End If
            Return ApplicationInfo._ApplicationTitle
        End Get
        Set(ByVal value As String)
            If Not String.IsNullOrEmpty(value) Then
                ApplicationInfo._ApplicationTitle = value
            End If
        End Set
    End Property

    ''' <summary> The application version. </summary>
    Private Shared _ApplicationVersion As String

    ''' <summary> Gets or sets or Sets the folder where application files are located. </summary>
    ''' <value> The application version. </value>
    Public Shared Property ApplicationVersion() As String
        Get
            If String.IsNullOrEmpty(ApplicationInfo._ApplicationVersion) Then
                ApplicationInfo._ApplicationVersion = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                    "{0}.{1:00}.{2:0000}", My.Application.Info.Version.Major,
                                                    My.Application.Info.Version.Minor, My.Application.Info.Version.Build)
            End If
            Return ApplicationInfo._ApplicationVersion
        End Get
        Set(ByVal value As String)
            If Not String.IsNullOrEmpty(value) Then
                ApplicationInfo._ApplicationVersion = value
            End If
        End Set
    End Property

    ''' <summary> Returns the application file version. </summary>
    ''' <value> The file version of the application. </value>
    Public Shared ReadOnly Property FileVersion() As String
        <Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Demand,
                                    Flags:=System.Security.Permissions.SecurityPermissionFlag.UnmanagedCode)>
        Get
            Return FileVersionInfo.FileVersion
        End Get
    End Property

    ''' <summary> Information describing the file version. </summary>
    Private Shared _FileVersionInfo As FileVersionInfo

    ''' <summary> Gets the application <see cref="FileVersionInfo">File Version Info.</see> </summary>
    ''' <remarks>
    ''' Use this version information source for the application so that it won't be affected by any
    ''' changes that might occur in the <see cref="My.MyApplication.Info">application info</see> code.
    ''' </remarks>
    ''' <value> The application <see cref="FileVersionInfo">File Version Info</see>. </value>
    Public Shared ReadOnly Property FileVersionInfo As FileVersionInfo
        <Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Demand,
                                    Flags:=System.Security.Permissions.SecurityPermissionFlag.UnmanagedCode)>
        Get
            If ApplicationInfo._FileVersionInfo Is Nothing Then
                ApplicationInfo._FileVersionInfo = FileVersionInfo.GetVersionInfo(ApplicationInfo.FileName)
            End If
            Return ApplicationInfo._FileVersionInfo
        End Get
    End Property

    ''' <summary> Returns the application product name. </summary>
    ''' <value> The product name of the application. </value>
    Public Shared ReadOnly Property ProductName() As String
        Get
            Return My.Application.Info.ProductName
        End Get
    End Property

    ''' <summary> Returns the application product version. </summary>
    ''' <value> The product version of the application. </value>
    Public Shared ReadOnly Property ProductVersion() As String
        Get
            Return My.Application.Info.Version.ToString(4)
        End Get
    End Property

#End Region

#Region " APPLICATION CONFIGURATION FILE "

    ''' <summary> Returns the application configuration file name. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="extension"> The extension. </param>
    ''' <returns> System.String. </returns>
    Public Shared Function BuildApplicationConfigFileName(ByVal extension As String) As String
        ' Return String.Format("{0}{1}{2}", My.Application.Info.AssemblyName, ".exe", extension)
        Return $"{My.Application.Info.AssemblyName}.exe{extension}"
    End Function

    ''' <summary>
    ''' Returns the file path of the default application configuration file. The folder is created if
    ''' it does not exists.
    ''' </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="allUsers">                   True to select the folder for
    '''                                           <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.AllUsersApplicationData"/>
    '''                                           all users; otherwise, use using current user;
    '''                                           otherwise, use the folder for
    '''                                           <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.CurrentUserApplicationData"/> 
    ''' </param>
    ''' <param name="significantVersionElements"> The significant version elements. A value between 1
    '''                                           and 4 indicating the version level to use. For
    '''                                           example, with 1, only the major version of the
    '''                                           folder name is used; whereas with 4 all version
    '''                                           elements are used. Defaults to 4. </param>
    ''' <returns>
    ''' The configuration file full path. Uses the application folder if this folder is writable.
    ''' Otherwise the <see cref="OpenApplicationConfigFolderPath">application data folder</see> is
    ''' used. System.String.
    ''' </returns>
    Public Shared Function BuildApplicationConfigFilePath(ByVal allUsers As Boolean, ByVal significantVersionElements As Integer) As String
        Return ApplicationInfo.BuildApplicationConfigFilePath(allUsers, ".config", significantVersionElements)
    End Function

    ''' <summary>
    ''' Returns the file path of the application configuration file. The folder is created if it does
    ''' not exists.
    ''' </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="allUsers">                   True to select the folder for
    '''                                           <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.AllUsersApplicationData"/>
    '''                                           all users; otherwise, use using current user;
    '''                                           otherwise, use the folder for
    '''                                           <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.CurrentUserApplicationData"/> 
    ''' </param>
    ''' <param name="extension">                  The extension. </param>
    ''' <param name="significantVersionElements"> The significant version elements. A value between 1
    '''                                           and 4 indicating the version level to use. For
    '''                                           example, with 1, only the major version of the
    '''                                           folder name is used; whereas with 4 all version
    '''                                           elements are used. Defaults to 4. </param>
    ''' <returns>
    ''' The configuration file full path. Uses the application folder if this folder is writable.
    ''' Otherwise the <see cref="OpenApplicationConfigFolderPath">application data folder</see> is
    ''' used. System.String.
    ''' </returns>
    Public Shared Function BuildApplicationConfigFilePath(ByVal allUsers As Boolean, ByVal extension As String, ByVal significantVersionElements As Integer) As String
        Return System.IO.Path.Combine(ApplicationInfo.OpenApplicationConfigFolderPath(allUsers, significantVersionElements),
                                      ApplicationInfo.BuildApplicationConfigFileName(extension))
    End Function

    ''' <summary> Determines whether the specified folder path is writable. </summary>
    ''' <remarks>
    ''' Uses a temporary random file name to test if the file can be created. The file is deleted
    ''' thereafter.
    ''' </remarks>
    ''' <param name="path"> The path. </param>
    ''' <returns> <c>True</c> if the specified path is writable; otherwise, <c>False</c>. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function IsFolderWritable(ByVal path As String) As Boolean
        Dim filePath As String = String.Empty
        Dim affirmative As Boolean = False
        Try
            filePath = System.IO.Path.Combine(path, System.IO.Path.GetRandomFileName())
            Using s As System.IO.FileStream = System.IO.File.Open(filePath, System.IO.FileMode.OpenOrCreate)
            End Using
            affirmative = True
        Catch
        Finally
            ' SS reported an exception from this test possibly indicating that Windows allowed writing the file 
            ' by failed report deletion. Or else, Windows raised another exception type.
            Try
                If System.IO.File.Exists(filePath) Then
                    System.IO.File.Delete(filePath)
                End If
            Catch
            End Try
        End Try
        Return affirmative
    End Function

    ''' <summary> Opens the application configuration folder path. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="allUsers">                   True to select the folder for
    '''                                           <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.AllUsersApplicationData"/>
    '''                                           all users; otherwise, use using current user;
    '''                                           otherwise, use the folder for
    '''                                           <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.CurrentUserApplicationData"/> 
    ''' </param>
    ''' <param name="significantVersionElements"> The significant version elements. A value between 1
    '''                                           and 4 indicating the version level to use. For
    '''                                           example, with 1, only the major version of the
    '''                                           folder name is used; whereas with 4 all version
    '''                                           elements are used. Defaults to 4. </param>
    ''' <returns>
    ''' The configuration folder full path. Uses the application folder if this folder is writable.
    ''' Otherwise the <see cref="OpenApplicationDataFolderPath">application data folder</see> is
    ''' used. System.String.
    ''' </returns>
    Public Shared Function OpenApplicationConfigFolderPath(ByVal allUsers As Boolean, ByVal significantVersionElements As Integer) As String
        Dim candidatePath As String = My.Application.Info.DirectoryPath
        If Not ApplicationInfo.IsFolderWritable(candidatePath) Then
            candidatePath = ApplicationInfo.OpenApplicationDataFolderPath(allUsers, significantVersionElements)
        End If
        Return candidatePath
    End Function

#End Region

#Region " APPLICATION DATA FOLDER -- CUSTOMIZED "

    ''' <summary> Drops the trailing elements. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="value">     The value. </param>
    ''' <param name="delimiter"> The delimiter. </param>
    ''' <param name="count">     The count. </param>
    ''' <returns> System.String. </returns>
    Public Shared Function DropTrailingElements(ByVal value As String, ByVal delimiter As Char, ByVal count As Integer) As String
        If String.IsNullOrEmpty(value) Then
            Return value
        ElseIf String.IsNullOrEmpty(delimiter) Then
            Return value
        Else
            Do While count > 0
                count -= 1
                Dim i As Integer = value.LastIndexOf(delimiter)
                If i > 0 Then
                    value = value.Substring(0, i)
                Else
                    count = 0
                End If
            Loop
            Return value
        End If
    End Function

    ''' <summary> Drops the trailing elements. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="value">     The value. </param>
    ''' <param name="delimiter"> The delimiter. </param>
    ''' <param name="count">     The count. </param>
    ''' <param name="replace">   The replace. </param>
    ''' <returns> System.String. </returns>
    Public Shared Function ReplaceTrailingElements(ByVal value As String, ByVal delimiter As Char, ByVal count As Integer, ByVal replace As String) As String
        If String.IsNullOrEmpty(value) Then
            Return value
        ElseIf String.IsNullOrEmpty(delimiter) Then
            Return value
        Else
            Dim replaceCount As Integer = 0
            Do While count > 0
                count -= 1
                Dim i As Integer = value.LastIndexOf(delimiter)
                If i > 0 Then
                    replaceCount += 1
                    value = value.Substring(0, i)
                Else
                    count = 0
                End If
            Loop
            Dim builder As New System.Text.StringBuilder(value)
            If replaceCount > 0 Then
                For i As Integer = 1 To replaceCount
                    builder.Append(delimiter)
                    builder.Append(replace)
                Next
            End If
            Return builder.ToString
        End If
    End Function

    ''' <summary>
    ''' Returns the Folder Path with the relevant version information elements replaces with '0'.
    ''' Creates the folder if it does not exist.
    ''' </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="dataFolderPath">             Name of the data path. </param>
    ''' <param name="significantVersionElements"> The significant version elements. A value between 1
    '''                                           and 4 indicating the version level to use. For
    '''                                           example, with 1, only the major version of the
    '''                                           folder name is used; whereas with 4 all version
    '''                                           elements are used. Defaults to 4. </param>
    ''' <returns>
    ''' A folder name with the revision numbers (if there) truncated based on the number of
    ''' significant elements. For example, with two significant digits we get: ...major.minor.0.0;
    ''' or.
    ''' </returns>
    Public Shared Function OpenDataFolderPath(ByVal dataFolderPath As String, ByVal significantVersionElements As Integer) As String
        significantVersionElements = Math.Max(1, Math.Min(4, significantVersionElements))
        Dim di As New System.IO.DirectoryInfo(ApplicationInfo.ReplaceTrailingElements(dataFolderPath, "."c, 4 - significantVersionElements, "0"))
        If Not di.Exists Then
            di.Create()
        End If
        Return di.FullName
    End Function

    ''' <summary>
    ''' Gets the application data folder Path. The folder is created if it does not exist.
    ''' </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="allUsers">                   True to select the folder for
    '''                                           <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.AllUsersApplicationData"/>
    '''                                           all users; otherwise, use using current user;
    '''                                           otherwise, use the folder for
    '''                                           <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.CurrentUserApplicationData"/> 
    ''' </param>
    ''' <param name="significantVersionElements"> The significant version elements. A value between 1
    '''                                           and 4 indicating the version level to use. For
    '''                                           example, with 1, only the major version of the
    '''                                           folder name is used; whereas with 4 all version
    '''                                           elements are used. Defaults to 4. </param>
    ''' <returns>
    ''' The application data folder for the product. With All Users: In Windows XP: .\ Application
    ''' Data\&lt;company&gt;\&lt;product name&gt;\major.minor.0.0; or In Windows 7 or 8: C:\
    ''' ProgramData\&lt;company&gt;\&lt;product name&gt;\major.minor.0.0 For the Current User: In
    ''' Windows XP: .\&lt;user&gt;\documents and settings\AppData\&lt;company&gt;\&lt;product name&gt;
    ''' \major.minor.0.0; or In Windows 7 or 8: C:\Users\&lt;user&gt;\AppData\Roaming\\&lt;company&gt;
    ''' \&lt;product name&gt;\major.minor.0.0.
    ''' 
    ''' The version information is based on the Assembly File Version.
    ''' </returns>
    Public Shared Function OpenApplicationDataFolderPath(ByVal allUsers As Boolean, ByVal significantVersionElements As Integer) As String
        Return ApplicationInfo.OpenDataFolderPath(ApplicationInfo.OpenApplicationDataFolderPath(allUsers),
                                                  significantVersionElements)
    End Function

#End Region

#Region " APPLICATION DATA FOLDER "

    ''' <summary>
    ''' Gets the application data folder Path. This seems to create the folder if it does not exist.
    ''' </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="allUsers"> True to select the folder for
    '''                         <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.AllUsersApplicationData"/>
    '''                         all users; otherwise, use using current user;
    '''                         otherwise, use the folder for
    '''                         <see cref="Microsoft.VisualBasic.MyServices.SpecialDirectoriesProxy.CurrentUserApplicationData"/> 
    ''' </param>
    ''' <returns> System.String. </returns>
    Public Shared Function OpenApplicationDataFolderPath(ByVal allUsers As Boolean) As String
        Return If(allUsers, My.Computer.FileSystem.SpecialDirectories.AllUsersApplicationData,
                            My.Computer.FileSystem.SpecialDirectories.CurrentUserApplicationData)
    End Function

#End Region

#Region " CAPTION "

    ''' <summary> Builds application description caption. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="subtitle"> The subtitle. </param>
    ''' <returns> A String. </returns>
    Public Shared Function BuildApplicationDescriptionCaption(ByVal subtitle As String) As String
        Return ApplicationInfo.BuildCaption(ApplicationInfo.ApplicationDescription, subtitle)
    End Function

    ''' <summary> Builds application title caption. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="subtitle"> The subtitle. </param>
    ''' <returns> A String. </returns>
    Public Shared Function BuildApplicationTitleCaption(ByVal subtitle As String) As String
        Return ApplicationInfo.BuildCaption(ApplicationInfo.ApplicationTitle, subtitle)
    End Function

    ''' <summary> Gets or sets the release candidate prefix. </summary>
    ''' <value> The release candidate prefix. </value>
    Public Shared Property ReleaseCandidatePrefix As String = "RC"

    ''' <summary> Gets or sets the alpha version prefix. </summary>
    ''' <value> The alpha version prefix. </value>
    Public Shared Property AlphaVersionPrefix As String = "Alpha"

    ''' <summary> Gets or sets the beta version prefix. </summary>
    ''' <value> The beta version prefix. </value>
    Public Shared Property BetaVersionPrefix As String = "Beta"

    ''' <summary> Gets or sets the gold version prefix. </summary>
    ''' <value> The gold version prefix. </value>
    Public Shared Property GoldVersionPrefix As String = "Gold"

    ''' <summary> Builds the default caption. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="title">    The title. </param>
    ''' <param name="subtitle"> The subtitle. </param>
    ''' <returns> System.String. </returns>
    Public Shared Function BuildCaption(ByVal title As String, ByVal subtitle As String) As String
        Dim builder As New System.Text.StringBuilder
        builder.Append(title)
        builder.Append(" ")
        builder.Append(ApplicationInfo.ApplicationVersion)
        If My.Application.Info.Version.Major < 1 Then
            builder.Append(".")
            Select Case My.Application.Info.Version.Minor
                Case 0
                    builder.Append(ApplicationInfo.AlphaVersionPrefix)
                Case 1
                    builder.Append(ApplicationInfo.BetaVersionPrefix)
                Case 2 To 8
                    builder.Append($"{ApplicationInfo.ReleaseCandidatePrefix}{My.Application.Info.Version.Minor - 1}")
                Case Else
                    builder.Append(ApplicationInfo.GoldVersionPrefix)
            End Select
        End If
        If Not String.IsNullOrEmpty(subtitle) Then
            builder.Append(": ")
            builder.Append(subtitle)
        End If
        Return builder.ToString
    End Function

    ''' <summary> Gets or sets the simulate suffix. </summary>
    ''' <value> The simulate suffix. </value>
    Public Shared Property SimulateSuffix As String = ".Simulate"

    ''' <summary> Gets or sets the retrieve only suffix. </summary>
    ''' <value> The retrieve only suffix. </value>
    Public Shared Property RetrieveOnlySuffix As String = ".Retrieve Only"

    ''' <summary> Builds the default caption. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="title">           The title. </param>
    ''' <param name="subtitle">        The subtitle. </param>
    ''' <param name="usingDevices">    if set to <c>True</c> using devices. </param>
    ''' <param name="usingSimulation"> if set to <c>True</c> using simulation. </param>
    ''' <returns> System.String. </returns>
    Public Shared Function BuildCaption(ByVal title As String, ByVal subtitle As String, ByVal usingDevices As Boolean, ByVal usingSimulation As Boolean) As String
        ' Set the caption to the Application title
        Dim newCaption As New System.Text.StringBuilder
        newCaption.Append(ApplicationInfo.BuildCaption(title, subtitle))
        If usingDevices Then
            newCaption.Append(".")
            ' builder.Append(".Acquire")
        Else
            If usingSimulation Then
                newCaption.Append(ApplicationInfo.SimulateSuffix)
            Else
                newCaption.Append(ApplicationInfo.RetrieveOnlySuffix)
            End If
        End If
        Return newCaption.ToString
    End Function

#End Region

End Class

