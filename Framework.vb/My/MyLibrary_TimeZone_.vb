Namespace My

    Partial Public NotInheritable Class MyLibrary
        ''' <summary> Identifier for the local time zone. </summary>
        Private Shared _LocalTimeZoneId As String

        ''' <summary> Gets the identifier of the local Time zone. </summary>
        ''' <value> The identifier of the local Time zone. </value>
        Public Shared ReadOnly Property LocalTimeZoneId As String
            Get
                If String.IsNullOrWhiteSpace(MyLibrary._LocalTimeZoneId) Then MyLibrary._LocalTimeZoneId = TimeZoneInfo.Local.Id
                Return MyLibrary._LocalTimeZoneId
            End Get
        End Property
        ''' <summary> Information describing the local time zone. </summary>
        Private Shared _LocalTimeZoneInfo As TimeZoneInfo

        ''' <summary> Gets information describing the local Time zone. </summary>
        ''' <value> Information describing the local Time zone. </value>
        Public Shared ReadOnly Property LocalTimeZoneInfo As TimeZoneInfo
            Get
                If MyLibrary._LocalTimeZoneInfo Is Nothing Then
                    MyLibrary._LocalTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(MyLibrary.LocalTimeZoneId)
                End If
                Return MyLibrary._LocalTimeZoneInfo
            End Get
        End Property

        ''' <summary> Gets or sets the identifier of the Platform Time zone. </summary>
        ''' <value> The identifier of the Platform Time zone. </value>
        Public Shared Property PlatformTimeZoneId As String
        ''' <summary> Information describing the platform time zone. </summary>
        Private Shared _PlatformTimeZoneInfo As TimeZoneInfo

        ''' <summary> Gets or sets information describing the Platform Time zone. </summary>
        ''' <value> Information describing the Platform Time zone. </value>
        Public Shared Property PlatformTimeZoneInfo As TimeZoneInfo
            Get
                If MyLibrary._PlatformTimeZoneInfo Is Nothing Then
                    If String.IsNullOrWhiteSpace(MyLibrary.PlatformTimeZoneId) Then MyLibrary.PlatformTimeZoneId = TimeZoneInfo.Local.Id
                    MyLibrary._PlatformTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(MyLibrary.PlatformTimeZoneId)
                End If
                Return MyLibrary._PlatformTimeZoneInfo
            End Get
            Set(value As TimeZoneInfo)
                MyLibrary._PlatformTimeZoneInfo = value
            End Set
        End Property

        ''' <summary> Gets the platform time now. </summary>
        ''' <value> The platform time now. </value>
        Public Shared ReadOnly Property PlatformTimeNow() As DateTime
            Get
                Return TimeZoneInfo.ConvertTime(DateTime.Now, MyLibrary.PlatformTimeZoneInfo)
            End Get
        End Property

        ''' <summary> Gets the platform time UTC now. </summary>
        ''' <value> The platform time UTC now. </value>
        Public Shared ReadOnly Property PlatformTimeUtcNow() As DateTime
            Get
                Return TimeZoneInfo.ConvertTimeToUtc(DateTime.Now, MyLibrary.PlatformTimeZoneInfo)
            End Get
        End Property

    End Class

End Namespace
