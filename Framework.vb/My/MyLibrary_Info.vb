Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    Partial Public NotInheritable Class MyLibrary

        ''' <summary>
        ''' Constructor that prevents a default instance of this class from being created.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        Private Sub New()
            MyBase.New()
        End Sub
        ''' <summary> The assembly title. </summary>
        Public Const AssemblyTitle As String = "Core Framework Library"
        ''' <summary> Information describing the assembly. </summary>
        Public Const AssemblyDescription As String = "Core Framework Library"
        ''' <summary> The assembly product. </summary>
        Public Const AssemblyProduct As String = "Core.Framework"

    End Class

End Namespace
