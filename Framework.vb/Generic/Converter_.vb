''' <summary> A converter. </summary>
''' <remarks>
''' (c) 2016 Cory Charlton.<para>
''' Licensed under The MIT License. </para><para>  
''' David, 12/12/2016, 3.1.6192 https://github.com/CoryCharlton/CCSWE.Core. </para>
''' </remarks>
Public NotInheritable Class Converter

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    Private Sub New()
    End Sub

    ''' <summary> Converts. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> An Object. </returns>
    Public Shared Function Convert(Of T)(ByVal value As Object) As T
        Return CType(Convert(value, GetType(T)), T)
    End Function

    ''' <summary> Converts. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value"> The value. </param>
    ''' <param name="type">  The type. </param>
    ''' <returns> An Object. </returns>
    Public Shared Function Convert(ByVal value As Object, ByVal type As Type) As Object
        If type Is Nothing Then Throw New ArgumentNullException(NameOf(type))
        If type.IsGenericType AndAlso type.GetGenericTypeDefinition() Is GetType(Nullable(Of )) Then
            If value Is Nothing Then Return Nothing
            type = type.GetGenericArguments()(0)
        End If
        ' Return If(type.IsEnum, If(value Is Nothing, Nothing, If(TypeOf value Is String, System.Enum.Parse(type, CStr(value)), System.Enum.ToObject(type, value))), System.Convert.ChangeType(value, type))
        Return If(type.IsEnum, If(value, If(TypeOf value Is String, System.Enum.Parse(type, CStr(value)), System.Enum.ToObject(type, value))),
                                                                    System.Convert.ChangeType(value, type))
    End Function

    ''' <summary> Convert value. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="input"> The input. </param>
    ''' <returns> The value converted. </returns>
    Public Shared Function ConvertValue(Of T)(ByVal input As Object) As T
        If input Is Nothing Then Return Nothing
        Dim type As System.Type = GetType(T)
        If type.IsGenericType AndAlso type.GetGenericTypeDefinition() Is GetType(Nullable(Of )) Then
            type = type.GetGenericArguments()(0)
        End If
        Return If(type.IsEnum,
            If(TypeOf input Is String,
                CType(System.Enum.Parse(GetType(T), input.ToString().Trim()), T),
                CType(System.Enum.ToObject(type, input), T)),
            CType(System.Convert.ChangeType(input, type), T))
    End Function

    ''' <summary> Safe convert. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="value">        The value. </param>
    ''' <param name="defaultValue"> The default value. </param>
    ''' <returns> A T. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function SafeConvert(Of T)(ByVal value As Object, ByVal defaultValue As T) As T
        Try
            Return Converter.Convert(Of T)(value)
        Catch
            Return defaultValue
        End Try
    End Function
End Class
