﻿Imports System.Runtime.CompilerServices
Namespace HashExtensions
    ''' <summary> Includes hash extensions for <see cref="String">String</see>. </summary>
    ''' <remarks> (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 04/09/2009, 1.1.3386.x. </para></remarks>
    Public Module Methods

#Region " HASH "

        ''' <summary> Truncates a value to the desired precision. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> Extended value. </param>
        ''' <returns> An Int32. </returns>
        <Extension()>
        Public Function Truncate(ByVal value As Int64) As Int32
            value = value And UInt32.MaxValue
            If value > Int32.MaxValue Then
                value -= UInt32.MaxValue + 1
            End If
            Return Convert.ToInt32(value)
        End Function

        ''' <summary>
        ''' Returns the hash code for <paramref name="value">this string</paramref>
        ''' using the algorithm implements in .NET Framework 2.0.
        ''' </summary>
        ''' <remarks>
        ''' From Visual Studio Common Language http://www.orthogonal.com.au/computers/StringHash.htm.
        ''' </remarks>
        ''' <param name="value"> The value. </param>
        ''' <returns> A 32-bit signed integer hash code. </returns>
        <Extension()>
        Public Function GetHashCodeNet2(ByVal value As String) As Integer
            Dim hash As Integer = 5381
            If Not String.IsNullOrWhiteSpace(value) Then
                Dim characters() As Char = value.ToCharArray
                Dim i As Integer = 0
                Dim hash64 As Long
                Do While i < value.Length
                    ' hash = ((hash << 5) + hash) Xor Convert.ToInt32(characters(i))
                    hash64 = (hash << 5I)
                    hash64 += hash
                    hash = hash64.Truncate Xor Convert.ToInt32(characters(i))
                    i += 1
                Loop
            End If
            Return hash
        End Function

        ''' <summary> Converts the value to a Base 64 Hash. </summary>
        ''' <remarks> Uses S.H.A Crypto service provider. </remarks>
        ''' <param name="value"> Specifies the value to convert. </param>
        ''' <returns> The Base 64 Hash. </returns>
        <Extension()>
        Public Function ToBase64Hash(ByVal value As String) As String
            If String.IsNullOrWhiteSpace(value) Then
                Return String.Empty
            End If
            Using algorithm As New Security.Cryptography.SHA1CryptoServiceProvider
                Return ToBase64Hash(value, algorithm)
            End Using
        End Function

        ''' <summary> Converts the value to a Base 64 Hash. </summary>
        ''' <remarks> Uses S.H.A Crypto service provider. </remarks>
        ''' <param name="value">     Specifies the value to convert. </param>
        ''' <param name="algorithm"> Specifies the algorithm for computing the hash. </param>
        ''' <returns> The Base 64 Hash. </returns>
        <Extension()>
        Public Function ToBase64Hash(ByVal value As String, ByVal algorithm As Security.Cryptography.HashAlgorithm) As String

            If String.IsNullOrWhiteSpace(value) Then
                Return String.Empty
            ElseIf algorithm Is Nothing Then
                Return value
            End If

            Dim encoding As New System.Text.UnicodeEncoding

            ' Store the source string in a byte array         
            Dim values() As Byte = encoding.GetBytes(value)

            ' get a S.H.A provider.
            ' Dim provider As New System.Security.Cryptography.SHA1CryptoServiceProvider

            ' Create the hash         
            values = algorithm.ComputeHash(values)

            ' return as a base64 encoded string         
            Return Convert.ToBase64String(values)

        End Function

        ''' <summary> Converts a value to a base 64 string. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="values"> Specifies the value to convert. </param>
        ''' <returns> Value as a String. </returns>
        <Extension()>
        Public Function ToBase64String(ByVal values As Byte()) As String
            Return Convert.ToBase64String(values)
        End Function


#End Region

    End Module
End Namespace
