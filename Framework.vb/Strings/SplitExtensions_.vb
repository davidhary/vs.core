Imports System.Runtime.CompilerServices
Namespace SplitExtensions
    ''' <summary> Includes Split extensions for <see cref="String">String</see>. </summary>
    ''' <remarks> (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 04/09/2009, 1.1.3386.x. </para></remarks>
    Public Module Methods
        ''' <summary> The space. </summary>
        Private Const _Space As String = " "
        ''' <summary> The space character. </summary>
        Private Const _SpaceCharacter As Char = " "c

        ''' <summary>
        ''' Splits the string to words by adding spaces between lower and upper case characters.
        ''' </summary>
        ''' <remarks>
        ''' Timing on I7-2600K 3.4GHz <para>
        ''' CamelCase123 - 0.33ns</para><para>
        ''' </para>
        ''' </remarks>
        ''' <param name="value"> The <c>String</c> value to split. </param>
        ''' <returns> A <c>String</c> of words separated by spaces. </returns>
        <Extension()>
        Public Function SplitWords(ByVal value As String) As String
            If String.IsNullOrWhiteSpace(value) Then
                Return String.Empty
            Else
                Dim isSpace As Boolean = False
                Dim isLowerCase As Boolean = False
                Dim newValue As New System.Text.StringBuilder
                For Each c As Char In value
                    If Not isSpace AndAlso isLowerCase AndAlso Char.IsUpper(c) Then
                        newValue.Append(_Space)
                    End If
                    isSpace = c.Equals(_SpaceCharacter)
                    isLowerCase = Not Char.IsUpper(c)
                    newValue.Append(c)
                Next
                Return newValue.ToString()
            End If
        End Function

        ''' <summary> A code standing for a regular expression element. </summary>
        Private Const _Element As String = "$1"

        ''' <summary> The complete split command finding case as well as numeric and string of upper case characters. </summary>
        ''' <remarks>This will match against each word in Camel and Pascal case strings, while properly handling acronyms and including numbers. <para>
        ''' (^[a-z]+)                               Match against any lower-case letters at the start of the command. </para><para>
        ''' ([0-9]+)                                Match against one Or more consecutive numbers (anywhere in the string, including at the start). </para><para>
        ''' ([A-Z]{1}[a-z]+)                        Match against Title case words (one upper case followed by lower case letters). </para><para>
        ''' ([A-Z]+(?=([A-Z][a-z])|($)|([0-9])))    Match against multiple consecutive upper-case letters, leaving the last upper case letter  
        '''                                         out the match if it Is followed by lower case letters, and including it if it's followed 
        '''                                         by the end of the string or a number. </para><para>
        ''' Timing on I7-2600K 3.4GHz </para><para>
        ''' CamelCase123 - 2.8ns</para><para>
        ''' </para></remarks>
        Private Const _SplitCaseCommand As String = "((^[a-z]+)|([0-9_]+)|([A-Z]{1}[a-z]+)|([A-Z]+(?=([A-Z][a-z])|($)|([0-9_]))))"

        ''' <summary>
        ''' Splits camel or pascal case match against each word in Camel and Pascal case strings, while
        ''' properly handling acronyms and including numbers.
        ''' </summary>
        ''' <remarks>
        ''' Timing on I7-2600K 3.4GHz <para>
        ''' CamelCase123 - 2.6ns</para><para>
        ''' </para>
        ''' </remarks>
        ''' <param name="value"> The <c>String</c> value to split. </param>
        ''' <returns> A String. </returns>
        <Extension()>
        Public Function SplitCase(ByVal value As String) As String
            Return value.SplitCase(_Space)
        End Function

        ''' <summary>
        ''' Splits camel case match against each word in Camel and Pascal case strings, while properly
        ''' handling acronyms and including numbers.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">     The <c>String</c> value to split. </param>
        ''' <param name="delimiter"> The delimited. </param>
        ''' <returns> A String. </returns>
        <Extension()>
        Public Function SplitCase(ByVal value As String, ByVal delimiter As String) As String
            Return Text.RegularExpressions.Regex.Replace(value, _SplitCaseCommand, $"{_Element}{delimiter}",
                                                                Text.RegularExpressions.RegexOptions.Compiled).Trim()
        End Function

        ''' <summary> The complete split command finding case as well as numeric and string of upper case characters needing no trim action. </summary>
        ''' <remarks> See https://www.codeproject.com/articles/108996/splitting-pascal-camel-case-with-regex-enhancement
        ''' With a “Match if prefix is absent” group
        ''' (https://docs.microsoft.com/en-us/dotnet/standard/base-types/regular-expression-language-quick-reference?redirectedfrom=MSDN#grouping_constructs) <parap>
        ''' containing the “beginning of line” character ^ prevents any matches from occurring on the first character,
        ''' which eliminates the need for the String.Trim() call. </parap><parap>
        ''' The formal name for this grouping construct is “Zero-width negative look-behind assertion”, but just think 
        ''' of it as “if you see what’s in here, don’t match the next thing”. </parap><parap>
        ''' This was enhanced by adding split by numbers </parap><parap>
        ''' Timing on I7-2600K 3.4GHz </parap><parap>
        ''' CamelCase123 - 4.8ns</parap><parap>
        ''' </parap> </remarks>
        Private Const _SplitCaseSlowerCommand As String = "(?<!^)([A-Z][a-z]|(?<=[a-z])[^a-z]|(?<=[A-Z])[0-9_])"

        ''' <summary> Splits a Pascal/Camel case string into words. </summary>
        ''' <remarks>
        ''' Timing on I7-2600K 3.4GHz <para>
        ''' CamelCase123 - 4.8ns</para><para>
        ''' </para>
        ''' </remarks>
        ''' <param name="value">     The <c>String</c> value to split. </param>
        ''' <param name="delimiter"> The delimited. </param>
        ''' <returns> A String. </returns>
        <Extension()>
        Public Function SplitCaseSlower(ByVal value As String, ByVal delimiter As String) As String
            Return Text.RegularExpressions.Regex.Replace(value, _SplitCaseSlowerCommand, $"{delimiter}{_Element}",
                                                                Text.RegularExpressions.RegexOptions.Compiled)
        End Function

        ''' <summary> Splits a Pascal/Camel case statement into words. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> The <c>String</c> value to split. </param>
        ''' <returns> A String. </returns>
        <Extension()>
        Public Function SplitCaseSlower(ByVal value As String) As String
            Return value.SplitCaseSlower(_Space)
        End Function

    End Module
End Namespace
