Imports System.Runtime.CompilerServices

Namespace CommaSeparatedValuesExtensions

    ''' <summary> Extension methods for building dictionaries from comma-separated-values. </summary>
    ''' <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para> </remarks>
    Public Module Methods

        ''' <summary> Splits the values into a queue of strings. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="commaSeparatedValues"> The comma separated values. </param>
        ''' <returns> A Queue(Of String) </returns>
        <Extension>
        Public Function SplitQueue(ByVal commaSeparatedValues As String) As Queue(Of String)
            Return If(String.IsNullOrWhiteSpace(commaSeparatedValues),
                New Queue(Of String)(),
                New Queue(Of String)(commaSeparatedValues.Split(","c)))
        End Function

        ''' <summary> Builds a Dictionary(Of Integer, String). </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="keyValuePairs"> The key value pairs comma-separated text. </param>
        ''' <param name="key">           A key argument for permitting overrides. </param>
        ''' <param name="value">         A value argument for permitting overrides. </param>
        ''' <returns> A Dictionary(Of Integer, String) </returns>
        <Extension>
        <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
        Public Function BuildDictionary(ByVal keyValuePairs As String, ByVal key As Integer, ByVal value As String) As Dictionary(Of Integer, String)
            Dim dix As New Dictionary(Of Integer, String)
            Dim values As Queue(Of String) = Methods.SplitQueue(keyValuePairs)
            Do While values.Any
                If Integer.TryParse(values.Dequeue, key) Then
                    If values.Any Then
                        value = values.Dequeue
                        dix.Add(key, value)
                    End If
                End If
            Loop
            Return dix
        End Function

        ''' <summary> Builds a Dictionary(Of string, String). </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="keyValuePairs"> The key value pairs comma-separated text. </param>
        ''' <param name="key">           A key argument for permitting overrides. </param>
        ''' <param name="value">         A value argument for permitting overrides. </param>
        ''' <returns> A Dictionary(Of Integer, String) </returns>
        <Extension>
        <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
        Public Function BuildDictionary(ByVal keyValuePairs As String, ByVal key As String, ByVal value As String) As Dictionary(Of String, String)
            Dim dix As New Dictionary(Of String, String)
            Dim values As Queue(Of String) = Methods.SplitQueue(keyValuePairs)
            Do While values.Any
                key = values.Dequeue
                If values.Any Then
                    value = values.Dequeue
                    dix.Add(key, value)
                End If
            Loop
            Return dix
        End Function

    End Module

End Namespace
