﻿Imports System.Runtime.CompilerServices
Namespace IncludesExtensions
    ''' <summary> Includes 'include' extensions for <see cref="String">String</see>. </summary>
    ''' <remarks> (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 04/09/2009, 1.1.3386.x. </para></remarks>
    Public Module Methods

        ''' <summary> The illegal file characters. </summary>
        Public Const IllegalFileCharacters As String = "/\\:*?""<>|"

        ''' <summary> Returns True if the validated string contains only alpha characters. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="source"> The string to validate. </param>
        ''' <returns>
        ''' <c>True</c> if the validated string contains only alpha; otherwise, <c>False</c>.
        ''' </returns>
        <Extension()>
        Public Function IncludesIllegalFileCharacters(ByVal source As String) As Boolean

            Return source.IncludesCharacters(Methods.IllegalFileCharacters)

        End Function

        ''' <summary> Returns true if the string includes the specified characters. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source">     The string that is being searched. </param>
        ''' <param name="characters"> The characters to search for. </param>
        ''' <returns>
        ''' <c>True</c> if the string includes the specified characters; otherwise, <c>False</c>.
        ''' </returns>
        <Extension()>
        Public Function IncludesCharacters(ByVal source As String, ByVal characters As String) As Boolean

            If source Is Nothing Then
                Throw New ArgumentNullException(NameOf(source))
            End If
            If characters Is Nothing Then
                Throw New ArgumentNullException(NameOf(characters))
            End If

            Dim expression As String = String.Format(System.Globalization.CultureInfo.CurrentCulture, "[{0}]", characters)
            Dim r As System.Text.RegularExpressions.Regex = New System.Text.RegularExpressions.Regex(
                expression, System.Text.RegularExpressions.RegexOptions.IgnoreCase)
            Return r.IsMatch(source)

        End Function

        ''' <summary> Returns true if the string includes any of the specified characters. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source">     The string that is being searched. </param>
        ''' <param name="characters"> The characters to search for. </param>
        ''' <returns> null if it fails, else. </returns>
        <Extension()>
        Public Function IncludesAny(ByVal source As String, ByVal characters As String) As Boolean

            If source Is Nothing Then
                Throw New ArgumentNullException(NameOf(source))
            End If
            If characters Is Nothing Then
                Throw New ArgumentNullException(NameOf(characters))
            End If

            ' 2954: was ^[{0}]+$ 
            Dim expression As String = String.Format(System.Globalization.CultureInfo.CurrentCulture, "[{0}]", characters)
            Dim r As System.Text.RegularExpressions.Regex = New System.Text.RegularExpressions.Regex(
                expression, System.Text.RegularExpressions.RegexOptions.IgnoreCase)
            Return r.IsMatch(source)

        End Function

    End Module
End Namespace
