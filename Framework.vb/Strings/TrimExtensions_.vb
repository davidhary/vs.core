﻿Imports System.Runtime.CompilerServices
Namespace TrimExtensions
    ''' <summary> Includes 'Trim' extensions for <see cref="String">String</see>. </summary>
    ''' <remarks> (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 04/09/2009, 1.1.3386.x. </para></remarks>
    Public Module Methods

#Region " END OF LINE "

        ''' <summary> Trim end line. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> A delimited string of values. </param>
        ''' <returns> A String. </returns>
        <Extension()>
        Public Function TrimEndLine(ByVal value As String) As String
            If String.IsNullOrWhiteSpace(value) Then
                Return String.Empty
            Else
                Return value.TrimEnd(Environment.NewLine.ToCharArray)
            End If
        End Function

#End Region

#Region " SCPI UNIT "

        ''' <summary>
        ''' Remove unit characters from SCPI data. Some instruments append units to the end of the
        ''' fetched values. This methods removes alpha characters as well as the number sign which
        ''' instruments append to the reading number.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> A delimited string of values. </param>
        ''' <returns> The values striped from units. </returns>
        <Extension()>
        Public Function TrimUnits(ByVal value As String) As String
            Return Methods.TrimUnits(value, ",")
        End Function

        ''' <summary>
        ''' Remove unit characters from SCPI data. Some instruments append units to the end of the
        ''' fetched values. This methods removes alpha characters as well as the number sign which
        ''' instruments append to the reading number.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">     A delimited string of values. </param>
        ''' <param name="delimiter"> The delimiter. </param>
        ''' <returns> The values striped from units. </returns>
        <Extension()>
        Public Function TrimUnits(ByVal value As String, ByVal delimiter As String) As String
            Const unitCharacters As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ#"
            Dim dataBuilder As New System.Text.StringBuilder
            If Not String.IsNullOrWhiteSpace(value) Then
                If value.Contains(delimiter) Then
                    For Each dataElement As String In value.Split(","c)
                        If dataBuilder.Length > 0 Then dataBuilder.Append(",")
                        dataBuilder.Append(dataElement.TrimEnd(unitCharacters.ToCharArray))
                    Next
                Else
                    dataBuilder.Append(value.TrimEnd(unitCharacters.ToCharArray))
                End If
            End If
            Return dataBuilder.ToString
        End Function

#End Region

    End Module
End Namespace
