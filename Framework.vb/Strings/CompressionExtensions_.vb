Imports System.Runtime.CompilerServices
Namespace CompressionExtensions
    ''' <summary> Includes Compression extensions for <see cref="String">String</see>. </summary>
    ''' <remarks> (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 04/09/2009, 1.1.3386.x. </para></remarks>
    Public Module Methods

        ''' <summary> Returns a compressed value. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> The string being chopped. </param>
        ''' <returns> Compressed value. </returns>
        <Extension()>
        Public Function Compress(ByVal value As String) As String

            If String.IsNullOrWhiteSpace(value) Then
                Return String.Empty
                Exit Function
            End If

            Dim result As String = String.Empty

            ' Compress the byte array
            Using memoryStream As New System.IO.MemoryStream()

                Using compressedStream As New System.IO.Compression.GZipStream(memoryStream, System.IO.Compression.CompressionMode.Compress)

                    ' Convert the uncompressed string into a byte array
                    Dim values As Byte() = System.Text.Encoding.UTF8.GetBytes(value)
                    compressedStream.Write(values, 0, values.Length)

                    ' Don't FLUSH here - it possibly leads to data loss!
                    compressedStream.Close()

                    Dim compressedValues As Byte() = memoryStream.ToArray()

                    ' Convert the compressed byte array back to a string
                    result = System.Convert.ToBase64String(compressedValues)

                    memoryStream.Close()

                End Using

            End Using

            Return result

        End Function

        ''' <summary> Returns the decompressed string of the value. </summary>
        ''' <remarks>
        ''' David, 04/09/2009, 1.1.3516.x. Bug fix in getting the size. Changed  memoryStream.Length - 5
        ''' to  memoryStream.Length - 4.
        ''' </remarks>
        ''' <param name="value"> The string being chopped. </param>
        ''' <returns> Decompressed value. </returns>
        <Extension()>
        Public Function Decompress(ByVal value As String) As String

            If String.IsNullOrWhiteSpace(value) Then
                Return String.Empty
                Exit Function
            End If

            Dim result As String = String.Empty

            ' Convert the compressed string into a byte array
            Dim compressedValues As Byte() = System.Convert.FromBase64String(value)

            ' Decompress the byte array
            Using memoryStream As New IO.MemoryStream(compressedValues)

                Using compressedStream As New System.IO.Compression.GZipStream(memoryStream, System.IO.Compression.CompressionMode.Decompress)

                    ' it looks like we are getting a bogus size.
                    Dim sizeBytes(3) As Byte
                    memoryStream.Position = memoryStream.Length - 4
                    memoryStream.Read(sizeBytes, 0, 4)

                    Dim outputSize As Int32 = BitConverter.ToInt32(sizeBytes, 0)

                    memoryStream.Position = 0

                    Dim values(outputSize - 1) As Byte

                    compressedStream.Read(values, 0, outputSize)

                    ' Convert the decompressed byte array back to a string
                    result = System.Text.Encoding.UTF8.GetString(values)

                End Using

            End Using

            Return result

        End Function

    End Module
End Namespace
