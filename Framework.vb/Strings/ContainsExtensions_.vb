﻿Imports System.Runtime.CompilerServices
Namespace ContainsExtensions
    ''' <summary> Includes 'Contains' extensions for <see cref="String">String</see>. </summary>
    ''' <remarks> (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 04/09/2009, 1.1.3386.x. </para></remarks>
    Public Module Methods

        ''' <summary> Returns true if the find string is in the search string. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="search">         The item to search. </param>
        ''' <param name="find">           The item to find. </param>
        ''' <param name="comparisonType"> Type of the comparison. </param>
        ''' <returns> <c>True</c> the find string is in the search string. </returns>
        <Extension()>
        Public Function Contains(ByVal search As String, ByVal find As String, ByVal comparisonType As StringComparison) As Boolean
            Return Location(search, find, comparisonType) >= 0
        End Function

        ''' <summary> Returns the location of the find string in the search string. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="search">         The item to search. </param>
        ''' <param name="find">           The item to find. </param>
        ''' <param name="comparisonType"> Type of the comparison. </param>
        ''' <returns> The location of the find string in the search string. </returns>
        <Extension()>
        Public Function Location(ByVal search As String, ByVal find As String, ByVal comparisonType As StringComparison) As Integer
            If String.IsNullOrWhiteSpace(search) Then Return 0
            Return search.IndexOf(find, comparisonType)
        End Function

    End Module
End Namespace
