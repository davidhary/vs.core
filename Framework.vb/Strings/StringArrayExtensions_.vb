﻿Imports System.Runtime.CompilerServices
Namespace ArrayExtensions
    ''' <summary> Includes extensions for string arrays. </summary>
    ''' <remarks> (c) 2011 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 08/27/2011, 1.2.4256.x. </para></remarks>  
    Public Module Methods

        ''' <summary>
        ''' Combines the specified string array using the
        ''' <paramref name="terminator">terminator</paramref>.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">      The string array. </param>
        ''' <param name="terminator"> The terminator. </param>
        ''' <returns> A String. </returns>
        <Extension()>
        Public Function Combine(ByVal value As IEnumerable(Of String), ByVal terminator As String) As String
            If value Is Nothing Then Return String.Empty
            Dim builder As New System.Text.StringBuilder
            For Each s As String In value
                If builder.Length > 0 Then
                    builder.Append(terminator)
                End If
                builder.Append(s)
            Next
            Return builder.ToString
        End Function

    End Module

End Namespace
