﻿Imports System.Runtime.CompilerServices
Namespace ParseExtensions

    ''' <summary> Parse extension methods. </summary>
    ''' <remarks> (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para> </remarks>
    Public Module ParseMethods

#Region " NUMERIC PART "

        ''' <summary> Returns true if the string represents any number. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> Specifies the number candidate. </param>
        ''' <returns> <c>True</c> if the string represents any number; otherwise, <c>False</c>. </returns>
        <Extension()>
        Public Function IsNumber(ByVal value As String) As Boolean
            Return System.Text.RegularExpressions.Regex.IsMatch(value, "^[0-9 ]+$")
        End Function

        ''' <summary>
        ''' Try parse number. An alternative to <see cref="IsNumber(String)"/> for determining is
        ''' <paramref name="value"/> is a number.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> Specifies the number candidate. </param>
        ''' <returns> The (Success As Boolean, Value As Double) </returns>
        <Extension()>
        Public Function TryParseNumber(ByVal value As String) As (Success As Boolean, Value As Double)
            Dim a As Double
            Return (System.Double.TryParse(value, Globalization.NumberStyles.Number Or Globalization.NumberStyles.AllowExponent,
                                           System.Globalization.CultureInfo.CurrentCulture, a),
                                           a)
        End Function

        ''' <summary> Extracts the exponential number described by value. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> Specifies the number candidate. </param>
        ''' <returns> The extracted exponential number. </returns>
        <Extension()>
        Public Function ExtractExponentialNumber(ByVal value As String) As String
            Return System.Text.RegularExpressions.Regex.Match(value, "[-+]?[\d]+[.]?[\d]*([eE][-+]?[\d]+)?").Value
            ' Return System.Text.RegularExpressions.Regex.Match(value, "[-+]?[0-9]+[.]?[0-9]*([eE][-+]?[0-9]+)?").Value
        End Function

        ''' <summary> Return the number part of the ;'value' stripping all non-numeric contents. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> Specifies the number candidate. </param>
        ''' <returns> A string containing the numeric part of the value. </returns>
        <Extension()>
        Public Function ExtractNumberPart(ByVal value As String) As String
            Return System.Text.RegularExpressions.Regex.Match(value, "([+-]?\d+(\.\d+)?)|([+-]?\.\d+)").Value
        End Function

        ''' <summary> Extracts the numeric characters described by value. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> Specifies the number candidate. </param>
        ''' <returns> The extracted numeric characters. </returns>
        <Extension()>
        Public Function ExtractNumericCharacters(ByVal value As String) As String
            Return System.Text.RegularExpressions.Regex.Replace(value, "[^\d]+", String.Empty)
        End Function

#End Region

#Region " PARSE SAFE "

        ''' <summary>
        ''' Returns the numeric value represented by the given String value. If value contains an invalid
        ''' number then a <see cref="Double.NaN"/> is returned.  
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> String containing the double value to convert. </param>
        ''' <returns> A double value. </returns>
        <Extension()>
        Public Function ToDoubleSafe(ByVal value As String) As Double
            Dim result As Double
            Return If(Double.TryParse(value, System.Globalization.NumberStyles.Any, Nothing, result), result, Double.NaN)
        End Function

#End Region

#Region " PARSE ENUM "

        ''' <summary> Parse enum value. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="InvalidCastException"> Thrown when an object cannot be cast to a required
        '''                                         type. </exception>
        ''' <param name="value"> The value. </param>
        ''' <returns> A Nullable(Of. </returns>
        <Extension()>
        Public Function ParseEnumValue(Of T As Structure)(ByVal value As String) As Nullable(Of T)
            If String.IsNullOrWhiteSpace(value) Then
                Return New Nullable(Of T)
            Else
                Dim result As T = Nothing
                If [Enum].TryParse(value, result) Then
                    Return result
                Else
                    Throw New InvalidCastException($"Can't convert {value} to {GetType(T)}")
                End If
            End If
        End Function

#End Region


    End Module

End Namespace
