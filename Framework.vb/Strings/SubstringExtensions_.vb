Imports System.Runtime.CompilerServices
Namespace SubstringExtensions
    ''' <summary> Includes 'Substring' extensions for <see cref="String">String</see>. </summary>
    ''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 11/19/2010, 1.2.3975.x. </para></remarks>
    Public Module Methods

        ''' <summary>
        ''' Returns a substring of the input string taking not of start index and length exceptions.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">      The input string to sub string. </param>
        ''' <param name="startIndex"> The zero based starting index. </param>
        ''' <param name="length">     The total length. </param>
        ''' <returns>
        ''' A substring of the input string taking not of start index and length exceptions.
        ''' </returns>
        <Extension()>
        Public Function SafeSubstring(ByVal value As String, ByVal startIndex As Integer, ByVal length As Integer) As String

            If String.IsNullOrWhiteSpace(value) OrElse length <= 0 Then Return String.Empty

            Dim inputLength As Integer = value.Length
            If startIndex > inputLength Then
                Return String.Empty
            Else
                If startIndex + length > inputLength Then
                    length = inputLength - startIndex
                End If
                Return value.Substring(startIndex, length)
            End If

        End Function

        ''' <summary> Returns the substring after the last occurrence of the specified string. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="source"> The string to substring. </param>
        ''' <param name="search"> The string to search for. </param>
        ''' <returns> The substring after the last occurrence of the specified string. </returns>
        <Extension()>
        Public Function SubstringAfter(ByVal source As String, ByVal search As String) As String

            If String.IsNullOrWhiteSpace(source) Then Return String.Empty
            If String.IsNullOrWhiteSpace(search) Then Return String.Empty

            Dim location As Integer = source.LastIndexOf(search, StringComparison.OrdinalIgnoreCase)
            Return If(location >= 0,
                If(location + search.Length < source.Length, source.Substring(location + search.Length), String.Empty),
                String.Empty)

        End Function

        ''' <summary> Returns the substring before the last occurrence of the specified string. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="source"> The string to substring. </param>
        ''' <param name="search"> The string to search for. </param>
        ''' <returns> The substring before the last occurrence of the specified string. </returns>
        <Extension()>
        Public Function SubstringBefore(ByVal source As String, ByVal search As String) As String

            If String.IsNullOrWhiteSpace(source) Then Return String.Empty
            If String.IsNullOrWhiteSpace(search) Then Return String.Empty

            Dim location As Integer = source.LastIndexOf(search, StringComparison.OrdinalIgnoreCase)
            Return If(location >= 0, source.Substring(0, location), String.Empty)
        End Function

        ''' <summary> Returns the substring after the last occurrence of the specified string. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="source">         The string to substring. </param>
        ''' <param name="startDelimiter"> The start delimiter to search for. </param>
        ''' <param name="endDelimiter">   The end delimiter to search for. </param>
        ''' <returns> The substring after the last occurrence of the specified string. </returns>
        <Extension()>
        Public Function SubstringBetween(ByVal source As String, ByVal startDelimiter As String, ByVal endDelimiter As String) As String

            If String.IsNullOrWhiteSpace(source) Then Return String.Empty
            If String.IsNullOrWhiteSpace(startDelimiter) Then Return String.Empty

            Dim startLocation As Integer = source.LastIndexOf(startDelimiter, StringComparison.OrdinalIgnoreCase) + startDelimiter.Length
            Dim endLocation As Integer = source.LastIndexOf(endDelimiter, StringComparison.OrdinalIgnoreCase)

            Return If(startLocation >= 0 AndAlso startLocation < endLocation,
                source.Substring(startLocation, endLocation - startLocation),
                String.Empty)

        End Function

        ''' <summary>
        ''' Returns a new string consisting of all character preceding the
        ''' <paramref name="startingIndex"/>.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="source">        The string to substring. </param>
        ''' <param name="startingIndex"> Zero-based index of the starting. </param>
        ''' <returns> A String. </returns>
        <Extension()>
        Public Function SafeRemove(ByVal source As String, ByVal startingIndex As Integer) As String
            If String.IsNullOrWhiteSpace(source) Then Return String.Empty
            If startingIndex >= source.Length Then Return source
            Return source.Remove(startingIndex)
        End Function

        ''' <summary>
        ''' Returns a new string in which the specified <paramref name="count"/>"/&gt; number of
        ''' characters are remove starting at <paramref name="startingIndex"/>
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="source">        The string to substring. </param>
        ''' <param name="startingIndex"> Zero-based index of the starting. </param>
        ''' <param name="count">         Number of. </param>
        ''' <returns> A String. </returns>
        <Extension()>
        Public Function SafeRemove(ByVal source As String, ByVal startingIndex As Integer, ByVal count As Integer) As String
            If String.IsNullOrWhiteSpace(source) Then Return String.Empty
            If count = 0 Then Return source
            If startingIndex >= source.Length Then Return source
            If startingIndex + count > source.Length Then Return source.Remove(startingIndex)
            Return source.Remove(startingIndex, count)
        End Function

    End Module
End Namespace
