Imports System.Runtime.CompilerServices
Namespace BinaryWriterExtensions
    ''' <summary> Includes extensions for <see cref="System.IO.BinaryReader">binary reader</see>. </summary>
    ''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 01/23/2014, 2.0.5136.x. based on I/O Library binary reader. </para></remarks>
    Public Module Methods

        ''' <summary> Closes the binary Writer and base stream. </summary>
        ''' <remarks> Use this method to close the instance. </remarks>
        ''' <param name="writer"> The writer. </param>
        <Extension()>
        Public Sub CloseWriter(ByVal writer As System.IO.BinaryWriter)
            If writer IsNot Nothing Then
                writer.Close()
                If writer IsNot Nothing AndAlso writer.BaseStream IsNot Nothing Then
                    writer.BaseStream.Close()
                End If
            End If
        End Sub

        ''' <summary> Opens a stream. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="filePathName"> Specifies the name of the binary file which to read. </param>
        ''' <returns> A System.IO.FileStream. </returns>
        Public Function OpenStream(ByVal filePathName As String) As System.IO.FileStream
            Dim fileStream As System.IO.FileStream = Nothing
            Dim tempFileStream As System.IO.FileStream = Nothing
            If Not String.IsNullOrWhiteSpace(filePathName) Then
                Try
                    tempFileStream = New System.IO.FileStream(filePathName, System.IO.FileMode.Open, System.IO.FileAccess.Write)
                    fileStream = tempFileStream
                Catch
                    tempFileStream?.Dispose()
                    Throw
                End Try
            End If
            Return fileStream
        End Function

        ''' <summary>
        ''' Opens a binary file for Writing and returns a reference to the Writer. The file is
        ''' <see cref="system.IO.FileMode.Open">opened</see> in
        ''' <see cref="system.IO.FileAccess.Write">Write access</see>.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="filePathName"> Specifies the file name. </param>
        ''' <returns> A reference to an open <see cref="IO.BinaryWriter">binary Writer</see>. </returns>
        Public Function OpenWriter(ByVal filePathName As String) As System.IO.BinaryWriter
            If String.IsNullOrWhiteSpace(filePathName) Then Throw New ArgumentNullException(NameOf(filePathName))
            Dim tempWriter As System.IO.BinaryWriter = Nothing
            Dim stream As System.IO.FileStream = Nothing
            Dim writer As System.IO.BinaryWriter
            Try
                stream = Methods.OpenStream(filePathName)
                tempWriter = New System.IO.BinaryWriter(stream)
                writer = tempWriter
            Catch
                stream?.Dispose()
                tempWriter?.Dispose()
                Throw
            End Try
            Return writer

        End Function

#Region " DOUBLE "

        ''' <summary>
        ''' Writes a single-dimension <see cref="Double">double-precision</see>
        ''' array to the values file.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="writer"> The writer. </param>
        ''' <param name="values"> Holds the values to write. </param>
        <Extension()>
        Public Sub Write(ByVal writer As System.IO.BinaryWriter, ByVal values() As Double)

            If writer Is Nothing Then
                Throw New ArgumentNullException(NameOf(writer))
            End If
            If values Is Nothing Then
                Throw New ArgumentNullException(NameOf(values))
            End If

            ' write the values size for verifying size and, indirectly also location, upon read
            writer.Write(Convert.ToInt32(values.Length))

            ' write values to the file
            If values.Any Then
                For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
                    writer.Write(values(i))
                Next i
            End If

        End Sub

        ''' <summary> Writes a double-precision value to the values file. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="writer">   The writer. </param>
        ''' <param name="value">    Specifies the value to write. </param>
        ''' <param name="location"> Specifies the file location. </param>
        <Extension()>
        Public Sub Write(ByVal writer As System.IO.BinaryWriter, ByVal value As Double, ByVal location As Int64)

            If writer Is Nothing Then
                Throw New ArgumentNullException(NameOf(writer))
            End If
            writer.BaseStream.Seek(location, System.IO.SeekOrigin.Begin)
            writer.Write(value)

        End Sub

#End Region

#Region " INT32 "

        ''' <summary>
        ''' Writes a single-dimension <see cref="Int32">Integer</see>
        ''' array to the values file.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="writer"> The writer. </param>
        ''' <param name="values"> Holds the values to write. </param>
        <Extension()>
        Public Sub Write(ByVal writer As System.IO.BinaryWriter, ByVal values() As Int32)

            If writer Is Nothing Then
                Throw New ArgumentNullException(NameOf(writer))
            End If
            If values Is Nothing Then
                Throw New ArgumentNullException(NameOf(values))
            End If

            ' write the values size for verifying size and, indirectly also location, upon read
            writer.Write(Convert.ToInt32(values.Length))

            ' write values to the file
            If values.Any Then
                For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
                    writer.Write(values(i))
                Next i
            End If

        End Sub

        ''' <summary> Writes an Int32 value to the values file. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="writer">   The writer. </param>
        ''' <param name="value">    Specifies the value to write. </param>
        ''' <param name="location"> Specifies the file location. </param>
        <Extension()>
        Public Sub Write(ByVal writer As System.IO.BinaryWriter, ByVal value As Int32, ByVal location As Int64)

            If writer Is Nothing Then
                Throw New ArgumentNullException(NameOf(writer))
            End If
            writer.BaseStream.Seek(location, System.IO.SeekOrigin.Begin)
            writer.Write(value)

        End Sub

#End Region

#Region " INT64 "

        ''' <summary>
        ''' Writes a single-dimension <see cref="Int64">Long Integer</see>
        ''' array to the values file.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="writer"> The writer. </param>
        ''' <param name="values"> Holds the values to write. </param>
        <Extension()>
        Public Sub Write(ByVal writer As System.IO.BinaryWriter, ByVal values() As Int64)

            If writer Is Nothing Then
                Throw New ArgumentNullException(NameOf(writer))
            End If
            If values Is Nothing Then
                Throw New ArgumentNullException(NameOf(values))
            End If

            ' write the values size for verifying size and, indirectly also location, upon read
            writer.Write(Convert.ToInt32(values.Length))

            ' write values to the file
            If values.Any Then
                For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
                    writer.Write(values(i))
                Next i
            End If

        End Sub

        ''' <summary> Writes a Int64 value to the values file. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="writer">   The writer. </param>
        ''' <param name="value">    Specifies the value to write. </param>
        ''' <param name="location"> Specifies the file location. </param>
        <Extension()>
        Public Sub Write(ByVal writer As System.IO.BinaryWriter, ByVal value As Int64, ByVal location As Int64)

            If writer Is Nothing Then
                Throw New ArgumentNullException(NameOf(writer))
            End If
            writer.BaseStream.Seek(location, System.IO.SeekOrigin.Begin)
            writer.Write(value)

        End Sub

#End Region

#Region " SINGLE "

        ''' <summary>
        ''' Writes a single-dimension <see cref="Single">single-precision</see>
        ''' array to the values file.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="writer"> The writer. </param>
        ''' <param name="values"> Holds the values to write. </param>
        <Extension()>
        Public Sub Write(ByVal writer As System.IO.BinaryWriter, ByVal values() As Single)

            If writer Is Nothing Then
                Throw New ArgumentNullException(NameOf(writer))
            End If
            If values Is Nothing Then
                Throw New ArgumentNullException(NameOf(values))
            End If

            ' write the values size for verifying size and, indirectly also location, upon read
            writer.Write(Convert.ToInt32(values.Length))

            ' write values to the file
            If values.Any Then
                For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
                    writer.Write(values(i))
                Next i
            End If

        End Sub

        ''' <summary> Writes a single-precision value to the values file. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="writer">   The writer. </param>
        ''' <param name="value">    Specifies the value to write. </param>
        ''' <param name="location"> Specifies the file location. </param>
        <Extension()>
        Public Sub Write(ByVal writer As System.IO.BinaryWriter, ByVal value As Single, ByVal location As Int64)

            If writer Is Nothing Then
                Throw New ArgumentNullException(NameOf(writer))
            End If
            writer.BaseStream.Seek(location, System.IO.SeekOrigin.Begin)
            writer.Write(value)

        End Sub

#End Region

#Region " STRING "

        ''' <summary>
        ''' Writes a string to the binary file padding it with spaces as necessary to fill the length.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="writer"> The writer. </param>
        ''' <param name="value">  Holds the value to write. </param>
        ''' <param name="length"> The length to write. </param>
        <Extension()>
        Public Sub Write(ByVal writer As System.IO.BinaryWriter, ByVal value As String, ByVal length As Int32)

            If writer Is Nothing Then
                Throw New ArgumentNullException(NameOf(writer))
            End If
            If value Is Nothing Then
                Throw New ArgumentNullException(NameOf(value))
            End If

            ' pad but make sure not to exceed length.
            writer.Write(value.PadRight(length).Substring(0, length))

        End Sub

        ''' <summary> Writes a string value to the values file. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="writer">   The writer. </param>
        ''' <param name="value">    Specifies the value to write. </param>
        ''' <param name="location"> Specifies the file location. </param>
        <Extension()>
        Public Sub Write(ByVal writer As System.IO.BinaryWriter, ByVal value As String, ByVal location As Int64)

            If writer Is Nothing Then
                Throw New ArgumentNullException(NameOf(writer))
            End If
            If value Is Nothing Then
                Throw New ArgumentNullException(NameOf(value))
            End If

            writer.BaseStream.Seek(location, System.IO.SeekOrigin.Begin)
            writer.Write(value)

        End Sub

#End Region

#Region " DATE TIME "

        ''' <summary> Writes a <see cref="Datetime">date time</see> value the file. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="writer"> The writer. </param>
        ''' <param name="value">  Specifies the value to write. </param>
        <Extension()>
        Public Sub Write(ByVal writer As System.IO.BinaryWriter, ByVal value As DateTime)
            If writer Is Nothing Then
                Throw New ArgumentNullException(NameOf(writer))
            End If
            writer.Write(BitConverter.GetBytes(value.ToOADate))
        End Sub

        ''' <summary> Writes a <see cref="Datetime">date time</see> value the file. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="writer">   The writer. </param>
        ''' <param name="value">    Specifies the value to write. </param>
        ''' <param name="location"> Specifies the file location. </param>
        <Extension()>
        Public Sub Write(ByVal writer As System.IO.BinaryWriter, ByVal value As DateTime, ByVal location As Int64)
            If writer Is Nothing Then
                Throw New ArgumentNullException(NameOf(writer))
            End If
            writer.BaseStream.Seek(location, System.IO.SeekOrigin.Begin)
            writer.Write(value)
        End Sub


#End Region

#Region " TIME SPAN "

        ''' <summary> Writes a <see cref="TimeSpan">time span</see> value the file. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="writer"> The writer. </param>
        ''' <param name="value">  Specifies the value to write. </param>
        <Extension()>
        Public Sub Write(ByVal writer As System.IO.BinaryWriter, ByVal value As TimeSpan)
            If writer Is Nothing Then
                Throw New ArgumentNullException(NameOf(writer))
            End If
            writer.Write(value.TotalMilliseconds)
        End Sub

        ''' <summary> Writes a a <see cref="TimeSpan">time span</see> value the file. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="writer">   The writer. </param>
        ''' <param name="value">    Specifies the value to write. </param>
        ''' <param name="location"> Specifies the file location. </param>
        <Extension()>
        Public Sub Write(ByVal writer As System.IO.BinaryWriter, ByVal value As TimeSpan, ByVal location As Int64)
            If writer Is Nothing Then
                Throw New ArgumentNullException(NameOf(writer))
            End If
            writer.BaseStream.Seek(location, System.IO.SeekOrigin.Begin)
            writer.Write(value)
        End Sub

#End Region


    End Module
End Namespace

