Imports System.Text
Imports System.Runtime.CompilerServices

Namespace BitConverterExtensions

    ''' <summary> Bit converter and related methods. </summary>
    ''' <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para> </remarks>
    Public Module Methods

#Region " BIT CONVERTER CONVRSIONS: BIG ENDIAN "

        ''' <summary> Converts the values to a unsigned short using Big Endian byte order. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        '''                                              null. </exception>
        ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        ''' <param name="values"> Source byte array. </param>
        ''' <param name="offset"> Buffer offset. </param>
        ''' <returns> The given data converted to an UInt16. </returns>
        <Extension(), CLSCompliant(False)>
        Public Function BigEndUnsignedShort(ByVal values() As Byte, ByVal offset As Integer) As UInt16
            If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
            Dim byteCount As Integer = 2
            If values.Length < offset + byteCount Then Throw New InvalidOperationException($"{NameOf(values)}({values.Length}) must have at list {byteCount} + {offset} elements")
            Return BitConverter.ToUInt16(values.Skip(offset).Take(byteCount).Reverse.ToArray, 0)
        End Function

        ''' <summary> Converts the values to a single using Big Endian order. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        '''                                              null. </exception>
        ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        ''' <param name="values"> Source byte array. </param>
        ''' <param name="offset"> Buffer offset. </param>
        ''' <returns> The given data converted to a Single. </returns>
        <Extension()>
        Public Function BigEndSingle(ByVal values() As Byte, ByVal offset As Integer) As Single
            If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
            Dim byteCount As Integer = 4
            If values.Length < offset + byteCount Then Throw New InvalidOperationException($"{NameOf(values)}({values.Length}) must have at list {byteCount} + {offset} elements")
            Return BitConverter.ToSingle(values.Skip(offset).Take(byteCount).Reverse.ToArray, 0)
            ' Return ToSingle(values.ToUInt16(offset), values.ToUInt16(offset + 2))
        End Function

        ''' <summary> Converts the values to a single using Big Endian order. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        '''                                              null. </exception>
        ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        ''' <param name="values"> Source byte array. </param>
        ''' <param name="offset"> Buffer offset. </param>
        ''' <returns> The given data converted to a Single. </returns>
        <Extension(), CLSCompliant(False)>
        Public Function BigEndSingle(ByVal values() As UShort, ByVal offset As Integer) As Single
            If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
            Dim wordCount As Integer = 2
            If values.Length < offset + wordCount Then Throw New InvalidOperationException($"{NameOf(values)}({values.Length}) must have at list {wordCount} + {offset} elements")
            Return BitConverter.ToSingle(BitConverter.GetBytes(values(0)).Concat(BitConverter.GetBytes(values(1))).ToArray(), 0)
            ' Return ToSingle(values.ToUInt16(offset), values.ToUInt16(offset + 2))
        End Function

        ''' <summary> Converts the values to a single using Big Endian order. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="highOrderValue"> The high order value. </param>
        ''' <param name="lowOrderValue">  The low order value. </param>
        ''' <returns> The given data converted to a Single. </returns>
        <CLSCompliant(False)>
        Public Function BigEndSingle(ByVal highOrderValue As UShort, ByVal lowOrderValue As UShort) As Single
            Return BitConverter.ToSingle(BitConverter.GetBytes(highOrderValue).Concat(BitConverter.GetBytes(lowOrderValue)).ToArray(), 0)
            ' Return BitConverter.ToSingle(BitConverter.GetBytes(lowOrderValue).Concat(BitConverter.GetBytes(highOrderValue)).ToArray(), 0)
        End Function

        ''' <summary> Converts a value to the bytes using Big Endian order. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> Value to convert. </param>
        ''' <returns> value as a Byte() </returns>
        <Extension()> <CLSCompliant(False)>
        Public Function BigEndBytes(ByVal value As Single) As Byte()
            Return BitConverter.GetBytes(value).Reverse.ToArray
        End Function

        ''' <summary> Converts a value to the bytes using Big Endian order. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> Value to convert. </param>
        ''' <returns> value as a Byte() </returns>
        <Extension()> <CLSCompliant(False)>
        Public Function BigEndBytes(ByVal value As UShort) As Byte()
            Return BitConverter.GetBytes(value).Reverse.ToArray
        End Function

        ''' <summary> Little Endian bytes. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> Value to convert. </param>
        ''' <returns> A Byte() </returns>
        <Extension()> <CLSCompliant(False)>
        Public Function LittleEndBytes(ByVal value As UShort) As Byte()
            Return BitConverter.GetBytes(value).ToArray
        End Function

#End Region

#Region " DIRECT CONVRSIONS: BIG ENDIAN "

        ''' <summary>
        ''' Return an array of bytes from an unsigned 16 bit integer using BIG ENDIAN codification.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> Value to convert. </param>
        ''' <returns> Bytes array. </returns>
        <Extension(), CLSCompliant(False)>
        Public Function BigEndInt8(ByVal value As UShort) As Byte()
            Return New Byte() {CByte(value >> 8), CByte(value And &HFF)}
        End Function

        ''' <summary>
        ''' Return a 16 bit unsigned integer from two bytes according to BIG ENDIAN codification.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="values"> Source byte array. </param>
        ''' <param name="offset"> Buffer offset. </param>
        ''' <returns> Integer returned. </returns>
        <Extension(), CLSCompliant(False)>
        Public Function BigEndUnsignedInt16(ByVal values() As Byte, ByVal offset As Integer) As UInt16
            If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
            Return BigEndUnsignedInt16(values(offset), values(offset + 1))
        End Function

        ''' <summary>
        ''' Return a 16 bit unsigned integer from two bytes according to BIG ENDIAN codification.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="high"> The high byte. </param>
        ''' <param name="low">  The low byte. </param>
        ''' <returns> Integer returned. </returns>
        <CLSCompliant(False)>
        Public Function BigEndUnsignedInt16(ByVal high As Byte, ByVal low As Byte) As UInt16
            Return CUShort((CUShort(high) << 8) Or (low And &HFF))
        End Function

#End Region

#Region " BITS CONVRSIONS "

        ''' <summary> Return a byte from an 8-bit boolean array. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        '''                                              null. </exception>
        ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        ''' <param name="values"> Array of boolean 8 bit. </param>
        ''' <param name="offset"> Array offset. </param>
        ''' <returns> Byte returned. </returns>
        <Extension()>
        Public Function EightBitToByte(ByVal values() As Boolean, ByVal offset As Integer) As Byte
            If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
            If values.Length < 8 Then Throw New InvalidOperationException($"{NameOf(values)}({values.Length}) must have at list 8 elements")
            If values.Length < offset + 7 Then Throw New InvalidOperationException($"{NameOf(values)}({values.Length}) must have at list 7 + {offset} elements")
            Dim ret As Byte = &H0
            For ii As Integer = 0 To 7
                Select Case values(offset + ii)
                    Case True
                        ret = ret Or CByte(1 << ii)

                    Case False
                        ret = ret And CByte(Not (1 << ii))
                End Select
            Next ii
            Return ret
        End Function

#End Region

#Region " ASCII CONVRSIONS "

        ''' <summary> Return an array of bytes coded in ASCII according to Modbus specification. </summary>
        ''' <remarks>
        ''' codification : Byte = 0x5B Codified in two chars   : 0x35 = '5' and 0x42 = 'B' in ASCII The
        ''' returned vector is exactly the double of the introduced one.
        ''' </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="buffer"> Buffer to codify. </param>
        ''' <returns> Buffer codified. </returns>
        <Extension()>
        Public Function ToAsciiBytes(ByVal buffer() As Byte) As Byte()
            If buffer Is Nothing Then Throw New ArgumentNullException(NameOf(buffer))
            Dim chars As New List(Of Char)()
            Dim ii As Integer = 0
            Dim jj As Integer = 0
            Do While ii < buffer.Length * 2
                Dim ch As Char
                Dim val As Byte = CByte(If((ii Mod 2) = 0, buffer(jj) >> 4, buffer(jj) And &HF))
                Select Case val
                    Case &H1
                        ch = "1"c
                    Case &H2
                        ch = "2"c
                    Case &H3
                        ch = "3"c
                    Case &H4
                        ch = "4"c
                    Case &H5
                        ch = "5"c
                    Case &H6
                        ch = "6"c
                    Case &H7
                        ch = "7"c
                    Case &H8
                        ch = "8"c
                    Case &H9
                        ch = "9"c
                    Case &HA
                        ch = "A"c
                    Case &HB
                        ch = "B"c
                    Case &HC
                        ch = "C"c
                    Case &HD
                        ch = "D"c
                    Case &HE
                        ch = "E"c
                    Case &HF
                        ch = "F"c
                    Case Else
                        ch = "0"c
                End Select
                chars.Add(ch)
                If (ii Mod 2) <> 0 Then
                    jj += 1
                End If
                ii += 1
            Loop
            Return Encoding.ASCII.GetBytes(chars.ToArray())
        End Function

        ''' <summary>
        ''' Return a binary buffer from a byte array codified in ASCII according to Modbus specification.
        ''' </summary>
        ''' <remarks>
        ''' codification : Char1 = 0x35 ('5') and Char2 = 0x42 ('B')
        ''' Byte decoded : Byte = 0x5B The returned vector is exactly the half of the introduced one.
        ''' </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="buffer"> ASCII codified buffer. </param>
        ''' <returns> Binary buffer. </returns>
        <Extension()>
        Public Function FromAsciiBytes(ByVal buffer() As Byte) As Byte()
            If buffer Is Nothing Then Throw New ArgumentNullException(NameOf(buffer))
            Dim ret As New List(Of Byte)()
            Dim chars() As Char = Encoding.ASCII.GetChars(buffer)
            Dim bt As Byte = 0
            For ii As Integer = 0 To buffer.Length - 1
                Dim tmp As Byte
                Select Case chars(ii)
                    Case "1"c
                        tmp = &H1
                    Case "2"c
                        tmp = &H2
                    Case "3"c
                        tmp = &H3
                    Case "4"c
                        tmp = &H4
                    Case "5"c
                        tmp = &H5
                    Case "6"c
                        tmp = &H6
                    Case "7"c
                        tmp = &H7
                    Case "8"c
                        tmp = &H8
                    Case "9"c
                        tmp = &H9
                    Case "A"c
                        tmp = &HA
                    Case "B"c
                        tmp = &HB
                    Case "C"c
                        tmp = &HC
                    Case "D"c
                        tmp = &HD
                    Case "E"c
                        tmp = &HE
                    Case "F"c
                        tmp = &HF
                    Case Else
                        tmp = &H0
                End Select
                If ii Mod 2 <> 0 Then
                    bt = bt Or tmp
                    ret.Add(bt)
                    bt = 0
                Else
                    bt = CByte(tmp << 4)
                End If
            Next ii
            Return ret.ToArray()
        End Function

#End Region

    End Module
End Namespace
