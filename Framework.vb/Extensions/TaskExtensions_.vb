Imports System.Runtime.CompilerServices
Imports System.Threading.Tasks

Namespace TaskExtensions

    ''' <summary> Task extension methods. </summary>
    ''' <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para> </remarks>
    Public Module Methods

        ''' <summary> Queries if a task is active. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="task"> The task. </param>
        ''' <returns> <c>true</c> if the action task is active; otherwise <c>false</c> </returns>
        <Extension>
        Public Function IsTaskActive(ByVal task As Task) As Boolean
            Return task IsNot Nothing AndAlso Not (task.IsCanceled OrElse task.IsCompleted OrElse task.IsFaulted)
        End Function

        ''' <summary> Query if task ended. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="task"> The task. </param>
        ''' <returns> <c>true</c> if task ended; otherwise <c>false</c> </returns>
        <Extension>
        Public Function IsTaskEnded(ByVal task As Task) As Boolean
            Return task Is Nothing OrElse (task.IsCanceled OrElse task.IsCompleted OrElse task.IsFaulted)
        End Function

        ''' <summary> Queries if a task is active. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="status"> The status. </param>
        ''' <returns> <c>true</c> if a task is active; otherwise <c>false</c> </returns>
        <Extension>
        Public Function IsTaskActive(ByVal status As TaskStatus) As Boolean
            Return (status = TaskStatus.Created OrElse status = TaskStatus.Running)
        End Function

        ''' <summary> Query if task ended. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="status"> The status. </param>
        ''' <returns> <c>true</c> if task ended; otherwise <c>false</c> </returns>
        <Extension>
        Public Function IsTaskEnded(ByVal status As TaskStatus) As Boolean
            Return (status = TaskStatus.RanToCompletion OrElse status = TaskStatus.Canceled OrElse status = TaskStatus.Faulted)
        End Function

        ''' <summary> Wait task active. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="task">    The task. </param>
        ''' <param name="timeout"> The timeout. </param>
        <Extension>
        Public Sub WaitTaskActive(ByVal task As Task, ByVal timeout As TimeSpan)
            Dim sw As Stopwatch = Stopwatch.StartNew
            Do Until task.IsTaskActive() OrElse sw.Elapsed > timeout
                My.MyLibrary.DoEvents()
            Loop
        End Sub

        ''' <summary> Wait idle. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="task">    The task. </param>
        ''' <param name="timeout"> The timeout. </param>
        Public Sub WaitTaskIdle(ByVal task As Task, ByVal timeout As TimeSpan)
            Dim sw As Stopwatch = Stopwatch.StartNew
            Do Until task.IsTaskEnded() OrElse sw.Elapsed > timeout
                My.MyLibrary.DoEvents()
            Loop
        End Sub

    End Module

End Namespace
