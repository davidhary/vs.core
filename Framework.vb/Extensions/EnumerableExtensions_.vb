﻿Imports System.Runtime.CompilerServices
Namespace EnumerableExtensions

    ''' <summary> Includes extensions for <see cref="Enumerable">Enumerable</see>. </summary>
    ''' <remarks> (c) 2016 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 08/167/2016, 3.0.6072.x. </para></remarks>
    Public Module Methods

        ''' <summary>
        ''' Finds the first index of the element that is lower or equal to the search value where the
        ''' next element is equal or higher then the search value.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="values">      The values. </param>
        ''' <param name="searchValue"> The search value. </param>
        ''' <returns> The index of the highest lower value. </returns>
        <Extension()>
        Public Function BinaryFindBoundingIndex(ByVal values As IEnumerable(Of Double), ByVal searchValue As Double) As Integer
            If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
            Dim result As Integer = -1
            If Not values.Any Then Return result
            Dim first As Integer = 0
            Dim last As Integer = values.Count - 1
            Dim middle As Integer

            ' assume searches failed
            result = first - 1
            Do
                middle = (first + last) \ 2
                If (values(middle) <= searchValue AndAlso values(middle + 1) >= searchValue) Then
                    result = middle
                    Exit Do
                ElseIf values(middle) < searchValue Then
                    first = middle
                Else
                    last = middle
                End If
            Loop Until first >= last
            Return result
        End Function

        ''' <summary> Determines if the two specified arrays have the same values. </summary>
        ''' <remarks>
        ''' <see cref="T:Array"/> or <see cref="T:Arraylist"/> equals methods cannot be used because it
        ''' expects the two entities to be the same for equality.
        ''' </remarks>
        ''' <param name="left">  The left value. </param>
        ''' <param name="right"> The right value. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        <Extension()>
        Public Function ValueEquals(Of T)(ByVal left As IEnumerable(Of T), ByVal right As IEnumerable(Of T)) As Boolean
            Dim result As Boolean = True
            If left Is Nothing Then
                result = right Is Nothing
            ElseIf right Is Nothing Then
                result = False
            Else
                For i As Integer = 0 To left.Count - 1
                    If Not left(i).Equals(right(i)) Then
                        result = False
                        Exit For
                    End If
                Next
            End If
            Return result
        End Function

    End Module

End Namespace
