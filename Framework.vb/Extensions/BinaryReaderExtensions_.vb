﻿Imports System.IO
Imports System.Runtime.CompilerServices
Namespace BinaryReaderExtensions
    ''' <summary> Includes extensions for <see cref="System.IO.BinaryReader">binary reader</see>. </summary>
    ''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 01/23/2014, 2.0.5136.x. based on I/O Library binary reader. </para></remarks>
    Public Module Methods

        ''' <summary> Closes the binary reader and base stream. </summary>
        ''' <remarks> Use this method to close the instance. </remarks>
        ''' <param name="reader"> The reader. </param>
        <Extension()>
        Public Sub CloseReader(ByVal reader As System.IO.BinaryReader)
            If reader IsNot Nothing Then
                reader.Close()
                If reader IsNot Nothing AndAlso reader.BaseStream IsNot Nothing Then
                    reader.BaseStream.Close()
                End If
            End If
        End Sub

        ''' <summary> Opens a stream. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="filePathName"> Specifies the name of the binary file which to read. </param>
        ''' <returns> A System.IO.FileStream. </returns>
        Public Function OpenStream(ByVal filePathName As String) As System.IO.FileStream
            Dim fileStream As System.IO.FileStream = Nothing
            Dim tempFileStream As System.IO.FileStream = Nothing
            If Not String.IsNullOrWhiteSpace(filePathName) Then
                Try
                    tempFileStream = New System.IO.FileStream(filePathName, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                    fileStream = tempFileStream
                Catch
                    tempFileStream?.Dispose()
                    Throw
                End Try
            End If
            Return fileStream
        End Function

        ''' <summary>
        ''' Opens a binary file for reading and returns a reference to the reader. The file is
        ''' <see cref="FileMode.Open">opened</see> in
        ''' <see cref="FileAccess.Read">read access</see>.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <exception cref="FileNotFoundException"> Thrown when the requested file is not present. </exception>
        ''' <param name="filePathName"> Specifies the file name. </param>
        ''' <returns> A reference to an open <see cref="BinaryReader">binary reader</see>. </returns>
        Public Function OpenReader(ByVal filePathName As String) As System.IO.BinaryReader
            If String.IsNullOrWhiteSpace(filePathName) Then Throw New ArgumentNullException(NameOf(filePathName))
            If Not System.IO.File.Exists(filePathName) Then Throw New System.IO.FileNotFoundException("Failed opening a binary reader -- file not found.", filePathName)
            Dim tempReader As System.IO.BinaryReader = Nothing
            Dim stream As System.IO.FileStream = Nothing
            Dim reader As BinaryReader
            Try
                stream = Methods.OpenStream(filePathName)
                tempReader = New System.IO.BinaryReader(stream)
                reader = tempReader
            Catch
                stream?.Dispose()
                tempReader?.Dispose()
                Throw
            End Try
            Return reader
        End Function

#Region " DOUBLE "

        ''' <summary>
        ''' Reads a single-dimension <see cref="Double">double-precision</see>
        ''' array from the data file.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <exception cref="IOException">           Thrown when an IO failure occurred. </exception>
        ''' <param name="reader"> The reader. </param>
        ''' <returns> A <see cref="System.Double">double-precision</see> array. </returns>
        <Extension()>
        Public Function ReadDoubleArray(ByVal reader As System.IO.BinaryReader) As Double()

            If reader Is Nothing Then
                Throw New ArgumentNullException(NameOf(reader))
            End If

            Dim startingPosition As Long = reader.BaseStream.Position

            ' read the stored length
            Dim storedLength As Int32 = reader.ReadInt32

            If storedLength < 0 Then
                Throw New System.IO.IOException(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                              "Program encountered a negative array length of {0}. Possibly the file is corrupt the reader position at {1} is incorrect.",
                                                              storedLength, startingPosition))
            End If

            Return reader.ReadDoubleArray(storedLength)

        End Function

        ''' <summary>
        ''' Reads a single-dimension <see cref="Double">double-precision</see>
        ''' array from the data file.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        '''                                                are null. </exception>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        '''                                                the required range. </exception>
        ''' <param name="reader">       The reader. </param>
        ''' <param name="elementCount"> Specifies the number of data points. </param>
        ''' <returns> A <see cref="Double">double-precision</see> array. </returns>
        <Extension()>
        Public Function ReadDoubleArray(ByVal reader As System.IO.BinaryReader, ByVal elementCount As Int32) As Double()

            If reader Is Nothing Then
                Throw New ArgumentNullException(NameOf(reader))
            End If

            If elementCount < 0 Then
                Dim message As String = "Array size specified as {0} must be non-negative."
                message = String.Format(Globalization.CultureInfo.CurrentCulture, message, elementCount)
                Throw New ArgumentOutOfRangeException(NameOf(elementCount), elementCount, message)
            End If

            If elementCount = 0 Then

                ' return the empty array 
                Dim data() As Double = Array.Empty(Of Double)()
                Return data

            Else
                ' allocate data array
                Dim data(elementCount - 1) As Double

                ' Read the file
                For sampleNumber As Integer = 0 To elementCount - 1
                    data(sampleNumber) = reader.ReadDouble
                Next sampleNumber
                Return data
            End If

        End Function

        ''' <summary>
        ''' Reads a single-dimension <see cref="Double">double-precision</see>
        ''' array from the data file.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        '''                                                are null. </exception>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        '''                                                the required range. </exception>
        ''' <param name="reader">       The reader. </param>
        ''' <param name="elementCount"> Specifies the number of data points. </param>
        ''' <param name="verifyLength"> If true, verifies the length against the given length. </param>
        ''' <returns> A single-dimension <see cref="Double">double-precision</see> array. </returns>
        <Extension()>
        Public Function ReadDoubleArray(ByVal reader As System.IO.BinaryReader, ByVal elementCount As Int32, ByVal verifyLength As Boolean) As Double()

            If reader Is Nothing Then
                Throw New ArgumentNullException(NameOf(reader))
            End If

            If verifyLength Then
                Dim startingPosition As Long = reader.BaseStream.Position
                Dim data() As Double = reader.ReadDoubleArray()
                If data.Length <> elementCount Then
                    Dim message As String = "Data length stored in file of {0} elements does not match the expected data length of {1} elements at {2}."
                    message = String.Format(Globalization.CultureInfo.CurrentCulture, message, data.Length, elementCount, startingPosition)
                    Throw New ArgumentOutOfRangeException(NameOf(elementCount), elementCount, message)
                Else
                    Return data
                End If
            Else
                Return reader.ReadDoubleArray()
            End If

        End Function

        ''' <summary>
        ''' Reads a single-dimension <see cref="Double">double-precision</see>
        ''' array from the data file.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        '''                                                are null. </exception>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        '''                                                the required range. </exception>
        ''' <param name="reader">     The reader. </param>
        ''' <param name="count">      Specifies the number of data points. </param>
        ''' <param name="startIndex"> Specifies the index of the first data point. </param>
        ''' <param name="stepSize">   Specifies the step size between adjacent data points. </param>
        ''' <returns> A single-dimension <see cref="Double">double-precision</see> array. </returns>
        <Extension()>
        Public Function ReadDoubleArray(ByVal reader As System.IO.BinaryReader, ByVal count As Int32, ByVal startIndex As Int32, ByVal stepSize As Int32) As Double()

            If reader Is Nothing Then
                Throw New ArgumentNullException(NameOf(reader))
            End If

            Dim startingPosition As Long = reader.BaseStream.Position

            ' read the stored length
            Dim storedLength As Int32 = reader.ReadInt32

            If storedLength <> count Then
                Dim message As String = "Data length stored in file of {0} elements does not match the expected data length of {1} elements at {2}."
                message = String.Format(Globalization.CultureInfo.CurrentCulture, message, storedLength, count, startingPosition)
                Throw New ArgumentOutOfRangeException(NameOf(count), count, message)
            End If

            ' allocate data array
            Dim data(count - 1) As Double

            ' skip samples to get to the first channel of this sample set.
            If startIndex > 0 Then
                For i As Integer = 1 To startIndex
                    reader.ReadDouble()
                Next
            End If
            ' Read the file
            For sampleNumber As Integer = 0 To count - 1
                data(sampleNumber) = reader.ReadDouble
                If stepSize > 1 Then
                    For i As Integer = 2 To stepSize
                        reader.ReadDouble()
                    Next
                End If
            Next sampleNumber

            Return data

        End Function

        ''' <summary> Reads a double precision value from the data file. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="reader">   The reader. </param>
        ''' <param name="location"> Specifies the file location. </param>
        ''' <returns> A <see cref="System.Double">value</see>. </returns>
        <Extension()>
        Public Function ReadDoubleValue(ByVal reader As System.IO.BinaryReader, ByVal location As Int64) As Double

            If reader Is Nothing Then
                Throw New ArgumentNullException(NameOf(reader))
            End If

            reader.BaseStream.Seek(location, System.IO.SeekOrigin.Begin)
            Return reader.ReadDouble()

        End Function

#End Region

#Region " INT32 "

        ''' <summary>
        ''' Reads a single-dimension <see cref="Int32">Int32-precision</see>
        ''' array from the data file.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <exception cref="IOException">           Thrown when an IO failure occurred. </exception>
        ''' <param name="reader"> The reader. </param>
        ''' <returns> A <see cref="System.Int32">Int32-precision</see> array. </returns>
        <Extension()>
        Public Function ReadInt32Array(ByVal reader As System.IO.BinaryReader) As Int32()

            If reader Is Nothing Then
                Throw New ArgumentNullException(NameOf(reader))
            End If

            Dim startingPosition As Long = reader.BaseStream.Position

            ' read the stored length
            Dim storedLength As Int32 = reader.ReadInt32

            If storedLength < 0 Then
                Throw New System.IO.IOException(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                              "Program encountered a negative array length of {0}. Possibly the file is corrupt the reader position at {1} is incorrect.",
                                                              storedLength, startingPosition))
            End If

            Return reader.ReadInt32Array(storedLength)

        End Function

        ''' <summary>
        ''' Reads a single-dimension <see cref="Int32">Int32-precision</see>
        ''' array from the data file.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        '''                                                are null. </exception>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        '''                                                the required range. </exception>
        ''' <param name="reader">       The reader. </param>
        ''' <param name="elementCount"> Specifies the number of data points. </param>
        ''' <returns> A <see cref="Int32">Int32-precision</see> array. </returns>
        <Extension()>
        Public Function ReadInt32Array(ByVal reader As System.IO.BinaryReader, ByVal elementCount As Int32) As Int32()

            If reader Is Nothing Then
                Throw New ArgumentNullException(NameOf(reader))
            End If

            If elementCount < 0 Then
                Dim message As String = "Array size specified as {0} must be non-negative."
                message = String.Format(Globalization.CultureInfo.CurrentCulture, message, elementCount)
                Throw New ArgumentOutOfRangeException(NameOf(elementCount), elementCount, message)
            End If

            If elementCount = 0 Then

                ' return the empty array 
                Dim data() As Int32 = Array.Empty(Of Integer)()
                Return data

            Else
                ' allocate data array
                Dim data(elementCount - 1) As Int32

                ' Read from the file
                For sampleNumber As Integer = 0 To elementCount - 1
                    data(sampleNumber) = reader.ReadInt32
                Next sampleNumber
                Return data
            End If

        End Function

        ''' <summary>
        ''' Reads a single-dimension <see cref="Int32">Int32-precision</see>
        ''' array from the data file.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        '''                                                are null. </exception>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        '''                                                the required range. </exception>
        ''' <param name="reader">       The reader. </param>
        ''' <param name="elementCount"> Specifies the number of data points. </param>
        ''' <param name="verifyLength"> If true, verifies the length against the given length. </param>
        ''' <returns> A single-dimension <see cref="Int32">Int32-precision</see> array. </returns>
        <Extension()>
        Public Function ReadInt32Array(ByVal reader As System.IO.BinaryReader, ByVal elementCount As Int32, ByVal verifyLength As Boolean) As Int32()

            If reader Is Nothing Then
                Throw New ArgumentNullException(NameOf(reader))
            End If

            If verifyLength Then
                Dim startingPosition As Long = reader.BaseStream.Position
                Dim data() As Int32 = reader.ReadInt32Array()
                If data.Length <> elementCount Then
                    Dim message As String = "Data length stored in file of {0} elements does not match the expected data length of {1} elements at {2}."
                    message = String.Format(Globalization.CultureInfo.CurrentCulture, message, data.Length, elementCount, startingPosition)
                    Throw New ArgumentOutOfRangeException(NameOf(elementCount), elementCount, message)
                Else
                    Return data
                End If
            Else
                Return reader.ReadInt32Array()
            End If

        End Function

        ''' <summary>
        ''' Reads a single-dimension <see cref="Int32">Int32-precision</see>
        ''' array from the data file.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        '''                                                are null. </exception>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        '''                                                the required range. </exception>
        ''' <param name="reader">     The reader. </param>
        ''' <param name="count">      Specifies the number of data points. </param>
        ''' <param name="startIndex"> Specifies the index of the first data point. </param>
        ''' <param name="stepSize">   Specifies the step size between adjacent data points. </param>
        ''' <returns> A single-dimension <see cref="Int32">Int32-precision</see> array. </returns>
        <Extension()>
        Public Function ReadInt32Array(ByVal reader As System.IO.BinaryReader, ByVal count As Int32, ByVal startIndex As Int32, ByVal stepSize As Int32) As Int32()

            If reader Is Nothing Then
                Throw New ArgumentNullException(NameOf(reader))
            End If

            Dim startingPosition As Long = reader.BaseStream.Position

            ' read the stored length
            Dim storedLength As Int32 = reader.ReadInt32

            If storedLength <> count Then
                Dim message As String = "Data length stored in file of {0} elements does not match the expected data length of {1} elements at {2}."
                message = String.Format(Globalization.CultureInfo.CurrentCulture, message, storedLength, count, startingPosition)
                Throw New ArgumentOutOfRangeException(NameOf(count), count, message)
            End If

            ' allocate data array
            Dim data(count - 1) As Int32

            ' skip samples to get to the first channel of this sample set.
            If startIndex > 0 Then
                For i As Integer = 1 To startIndex
                    reader.ReadInt32()
                Next
            End If
            ' Read from the file
            For sampleNumber As Integer = 0 To count - 1
                data(sampleNumber) = reader.ReadInt32
                If stepSize > 1 Then
                    For i As Integer = 2 To stepSize
                        reader.ReadInt32()
                    Next
                End If
            Next sampleNumber

            Return data

        End Function

        ''' <summary> Reads a Int32 precision value from the data file. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="reader">   The reader. </param>
        ''' <param name="location"> Specifies the file location. </param>
        ''' <returns> A <see cref="System.Int32">value</see>. </returns>
        <Extension()>
        Public Function ReadInt32Value(ByVal reader As System.IO.BinaryReader, ByVal location As Int64) As Int32

            If reader Is Nothing Then
                Throw New ArgumentNullException(NameOf(reader))
            End If

            reader.BaseStream.Seek(location, System.IO.SeekOrigin.Begin)
            Return reader.ReadInt32()

        End Function

#End Region

#Region " INT64 "

        ''' <summary>
        ''' Reads a single-dimension <see cref="Int64">Int64-precision</see>
        ''' array from the data file.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <exception cref="IOException">           Thrown when an IO failure occurred. </exception>
        ''' <param name="reader"> The reader. </param>
        ''' <returns> A <see cref="System.Int64">Int64-precision</see> array. </returns>
        <Extension()>
        Public Function ReadInt64Array(ByVal reader As System.IO.BinaryReader) As Int64()

            If reader Is Nothing Then
                Throw New ArgumentNullException(NameOf(reader))
            End If

            Dim startingPosition As Long = reader.BaseStream.Position

            ' read the stored length
            Dim storedLength As Int32 = reader.ReadInt32

            If storedLength < 0 Then
                Throw New System.IO.IOException(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                              "Program encountered a negative array length of {0}. Possibly the file is corrupt the reader position at {1} is incorrect.",
                                                              storedLength, startingPosition))
            End If

            Return reader.ReadInt64Array(storedLength)

        End Function

        ''' <summary>
        ''' Reads a single-dimension <see cref="Int64">Int64-precision</see>
        ''' array from the data file.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        '''                                                are null. </exception>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        '''                                                the required range. </exception>
        ''' <param name="reader">       The reader. </param>
        ''' <param name="elementCount"> Specifies the number of data points. </param>
        ''' <returns> A <see cref="Int64">Int64-precision</see> array. </returns>
        <Extension()>
        Public Function ReadInt64Array(ByVal reader As System.IO.BinaryReader, ByVal elementCount As Int32) As Int64()

            If reader Is Nothing Then
                Throw New ArgumentNullException(NameOf(reader))
            End If

            If elementCount < 0 Then
                Dim message As String = "Array size specified as {0} must be non-negative."
                message = String.Format(Globalization.CultureInfo.CurrentCulture, message, elementCount)
                Throw New ArgumentOutOfRangeException(NameOf(elementCount), elementCount, message)
            End If

            If elementCount = 0 Then

                ' return the empty array 
                Dim data() As Int64 = Array.Empty(Of Long)()
                Return data

            Else
                ' allocate data array
                Dim data(elementCount - 1) As Int64

                ' Read the file
                For sampleNumber As Integer = 0 To elementCount - 1
                    data(sampleNumber) = reader.ReadInt64
                Next sampleNumber
                Return data
            End If

        End Function

        ''' <summary>
        ''' Reads a single-dimension <see cref="Int64">Int64-precision</see>
        ''' array from the data file.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        '''                                                are null. </exception>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        '''                                                the required range. </exception>
        ''' <param name="reader">       The reader. </param>
        ''' <param name="elementCount"> Specifies the number of data points. </param>
        ''' <param name="verifyLength"> If true, verifies the length against the given length. </param>
        ''' <returns> A single-dimension <see cref="Int64">Int64-precision</see> array. </returns>
        <Extension()>
        Public Function ReadInt64Array(ByVal reader As System.IO.BinaryReader, ByVal elementCount As Int32, ByVal verifyLength As Boolean) As Int64()

            If reader Is Nothing Then
                Throw New ArgumentNullException(NameOf(reader))
            End If

            If verifyLength Then
                Dim startingPosition As Long = reader.BaseStream.Position
                Dim data() As Int64 = reader.ReadInt64Array()
                If data.Length <> elementCount Then
                    Dim message As String = "Data length stored in file of {0} elements does not match the expected data length of {1} elements at {2}."
                    message = String.Format(Globalization.CultureInfo.CurrentCulture, message, data.Length, elementCount, startingPosition)
                    Throw New ArgumentOutOfRangeException(NameOf(elementCount), elementCount, message)
                Else
                    Return data
                End If
            Else
                Return reader.ReadInt64Array()
            End If

        End Function

        ''' <summary>
        ''' Reads a single-dimension <see cref="Int64">Int64-precision</see>
        ''' array from the data file.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        '''                                                are null. </exception>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        '''                                                the required range. </exception>
        ''' <param name="reader">     The reader. </param>
        ''' <param name="count">      Specifies the number of data points. </param>
        ''' <param name="startIndex"> Specifies the index of the first data point. </param>
        ''' <param name="stepSize">   Specifies the step size between adjacent data points. </param>
        ''' <returns> A single-dimension <see cref="Int64">Int64-precision</see> array. </returns>
        <Extension()>
        Public Function ReadInt64Array(ByVal reader As System.IO.BinaryReader, ByVal count As Int32, ByVal startIndex As Int32, ByVal stepSize As Int32) As Int64()

            If reader Is Nothing Then
                Throw New ArgumentNullException(NameOf(reader))
            End If

            Dim startingPosition As Long = reader.BaseStream.Position

            ' read the stored length
            Dim storedLength As Int32 = reader.ReadInt32

            If storedLength <> count Then
                Dim message As String = "Data length stored in file of {0} elements does not match the expected data length of {1} elements at {2}."
                message = String.Format(Globalization.CultureInfo.CurrentCulture, message, storedLength, count, startingPosition)
                Throw New ArgumentOutOfRangeException(NameOf(count), count, message)
            End If

            ' allocate data array
            Dim data(count - 1) As Int64

            ' skip samples to get to the first channel of this sample set.
            If startIndex > 0 Then
                For i As Integer = 1 To startIndex
                    reader.ReadInt64()
                Next
            End If
            ' Read the file
            For sampleNumber As Integer = 0 To count - 1
                data(sampleNumber) = reader.ReadInt64
                If stepSize > 1 Then
                    For i As Integer = 2 To stepSize
                        reader.ReadInt64()
                    Next
                End If
            Next sampleNumber

            Return data

        End Function

        ''' <summary> Reads a Int64 precision value from the data file. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="reader">   The reader. </param>
        ''' <param name="location"> Specifies the file location. </param>
        ''' <returns> A <see cref="System.Int64">value</see>. </returns>
        <Extension()>
        Public Function ReadInt64Value(ByVal reader As System.IO.BinaryReader, ByVal location As Int64) As Int64

            If reader Is Nothing Then
                Throw New ArgumentNullException(NameOf(reader))
            End If

            reader.BaseStream.Seek(location, System.IO.SeekOrigin.Begin)
            Return reader.ReadInt64()

        End Function

#End Region

#Region " SINGLE "

        ''' <summary>
        ''' Reads a single-dimension <see cref="Single">Single-precision</see>
        ''' array from the data file.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <exception cref="IOException">           Thrown when an IO failure occurred. </exception>
        ''' <param name="reader"> The reader. </param>
        ''' <returns> A <see cref="System.Single">Single-precision</see> array. </returns>
        <Extension()>
        Public Function ReadSingleArray(ByVal reader As System.IO.BinaryReader) As Single()

            If reader Is Nothing Then
                Throw New ArgumentNullException(NameOf(reader))
            End If

            Dim startingPosition As Long = reader.BaseStream.Position

            ' read the stored length
            Dim storedLength As Int32 = reader.ReadInt32

            If storedLength < 0 Then
                Throw New System.IO.IOException(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                              "Program encountered a negative array length of {0}. Possibly the file is corrupt the reader position at {1} is incorrect.",
                                                              storedLength, startingPosition))
            End If

            Return reader.ReadSingleArray(storedLength)

        End Function

        ''' <summary>
        ''' Reads a single-dimension <see cref="Single">Single-precision</see>
        ''' array from the data file.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        '''                                                are null. </exception>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        '''                                                the required range. </exception>
        ''' <param name="reader">       The reader. </param>
        ''' <param name="elementCount"> Specifies the number of data points. </param>
        ''' <returns> A <see cref="Single">Single-precision</see> array. </returns>
        <Extension()>
        Public Function ReadSingleArray(ByVal reader As System.IO.BinaryReader, ByVal elementCount As Int32) As Single()

            If reader Is Nothing Then
                Throw New ArgumentNullException(NameOf(reader))
            End If

            If elementCount < 0 Then
                Dim message As String = "Array size specified as {0} must be non-negative."
                message = String.Format(Globalization.CultureInfo.CurrentCulture, message, elementCount)
                Throw New ArgumentOutOfRangeException(NameOf(elementCount), elementCount, message)
            End If

            If elementCount = 0 Then

                ' return the empty array 
                Dim data() As Single = Array.Empty(Of Single)()
                Return data

            Else
                ' allocate data array
                Dim data(elementCount - 1) As Single

                ' Read from the file
                For sampleNumber As Integer = 0 To elementCount - 1
                    data(sampleNumber) = reader.ReadSingle
                Next sampleNumber
                Return data
            End If

        End Function

        ''' <summary>
        ''' Reads a single-dimension <see cref="Single">Single-precision</see>
        ''' array from the data file.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        '''                                                are null. </exception>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        '''                                                the required range. </exception>
        ''' <param name="reader">       The reader. </param>
        ''' <param name="elementCount"> Specifies the number of data points. </param>
        ''' <param name="verifyLength"> If true, verifies the length against the given length. </param>
        ''' <returns> A single-dimension <see cref="Single">Single-precision</see> array. </returns>
        <Extension()>
        Public Function ReadSingleArray(ByVal reader As System.IO.BinaryReader, ByVal elementCount As Int32, ByVal verifyLength As Boolean) As Single()

            If reader Is Nothing Then
                Throw New ArgumentNullException(NameOf(reader))
            End If

            If verifyLength Then
                Dim startingPosition As Long = reader.BaseStream.Position
                Dim data() As Single = reader.ReadSingleArray()
                If data.Length <> elementCount Then
                    Dim message As String = "Data length stored in file of {0} elements does not match the expected data length of {1} elements at {2}."
                    message = String.Format(Globalization.CultureInfo.CurrentCulture, message, data.Length, elementCount, startingPosition)
                    Throw New ArgumentOutOfRangeException(NameOf(elementCount), elementCount, message)
                Else
                    Return data
                End If
            Else
                Return reader.ReadSingleArray()
            End If

        End Function

        ''' <summary>
        ''' Reads a single-dimension <see cref="Single">Single-precision</see>
        ''' array from the data file.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        '''                                                are null. </exception>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        '''                                                the required range. </exception>
        ''' <param name="reader">     The reader. </param>
        ''' <param name="count">      Specifies the number of data points. </param>
        ''' <param name="startIndex"> Specifies the index of the first data point. </param>
        ''' <param name="stepSize">   Specifies the step size between adjacent data points. </param>
        ''' <returns> A single-dimension <see cref="Single">Single-precision</see> array. </returns>
        <Extension()>
        Public Function ReadSingleArray(ByVal reader As System.IO.BinaryReader, ByVal count As Int32, ByVal startIndex As Int32, ByVal stepSize As Int32) As Single()

            If reader Is Nothing Then
                Throw New ArgumentNullException(NameOf(reader))
            End If

            Dim startingPosition As Long = reader.BaseStream.Position

            ' read the stored length
            Dim storedLength As Int32 = reader.ReadInt32

            If storedLength <> count Then
                Dim message As String = "Data length stored in file of {0} elements does not match the expected data length of {1} elements at {2}."
                message = String.Format(Globalization.CultureInfo.CurrentCulture, message, storedLength, count, startingPosition)
                Throw New ArgumentOutOfRangeException(NameOf(count), count, message)
            End If

            ' allocate data array
            Dim data(count - 1) As Single

            ' skip samples to get to the first channel of this sample set.
            If startIndex > 0 Then
                For i As Integer = 1 To startIndex
                    reader.ReadSingle()
                Next
            End If
            ' Read the file
            For sampleNumber As Integer = 0 To count - 1
                data(sampleNumber) = reader.ReadSingle
                If stepSize > 1 Then
                    For i As Integer = 2 To stepSize
                        reader.ReadSingle()
                    Next
                End If
            Next sampleNumber

            Return data

        End Function

        ''' <summary> Reads a Single precision value from the data file. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="reader">   The reader. </param>
        ''' <param name="location"> Specifies the file location. </param>
        ''' <returns> A <see cref="System.Single">value</see>. </returns>
        <Extension()>
        Public Function ReadSingleValue(ByVal reader As System.IO.BinaryReader, ByVal location As Int64) As Single

            If reader Is Nothing Then
                Throw New ArgumentNullException(NameOf(reader))
            End If

            reader.BaseStream.Seek(location, System.IO.SeekOrigin.Begin)
            Return reader.ReadSingle()

        End Function

#End Region

#Region " STRING "

        ''' <summary> Reads a string value from the data file. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="reader">   The reader. </param>
        ''' <param name="location"> Specifies the file location. </param>
        ''' <returns> A <see cref="System.String">String</see> value. </returns>
        <Extension()>
        Public Function ReadStringValue(ByVal reader As System.IO.BinaryReader, ByVal location As Int64) As String

            If reader Is Nothing Then
                Throw New ArgumentNullException(NameOf(reader))
            End If

            reader.BaseStream.Seek(location, System.IO.SeekOrigin.Begin)
            Return reader.ReadString()

        End Function

#End Region

#Region " DATE TIME "

        ''' <summary> Reads date time. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="reader"> The reader. </param>
        ''' <returns> The date time. </returns>
        <Extension()>
        Public Function ReadDateTime(ByVal reader As System.IO.BinaryReader) As DateTime

            If reader Is Nothing Then
                Throw New ArgumentNullException(NameOf(reader))
            End If
            Return DateTime.FromOADate(reader.ReadDouble)

        End Function

        ''' <summary> Reads date time. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="reader">   The reader. </param>
        ''' <param name="location"> Specifies the file location. </param>
        ''' <returns> The date time. </returns>
        <Extension()>
        Public Function ReadDateTime(ByVal reader As System.IO.BinaryReader, ByVal location As Int64) As DateTime

            If reader Is Nothing Then
                Throw New ArgumentNullException(NameOf(reader))
            End If
            reader.BaseStream.Seek(location, System.IO.SeekOrigin.Begin)
            Return reader.ReadDateTime()

        End Function

#End Region

#Region " TIME SPAN "

        ''' <summary> Reads a timespan. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="reader"> The reader. </param>
        ''' <returns> The timespan. </returns>
        <Extension()>
        Public Function ReadTimespan(ByVal reader As System.IO.BinaryReader) As TimeSpan

            If reader Is Nothing Then
                Throw New ArgumentNullException(NameOf(reader))
            End If
            Return TimeSpan.FromMilliseconds(reader.ReadDouble)

        End Function

        ''' <summary> Reads a timespan. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="reader">   The reader. </param>
        ''' <param name="location"> Specifies the file location. </param>
        ''' <returns> The timespan. </returns>
        <Extension()>
        Public Function ReadTimespan(ByVal reader As System.IO.BinaryReader, ByVal location As Int64) As TimeSpan

            If reader Is Nothing Then
                Throw New ArgumentNullException(NameOf(reader))
            End If
            reader.BaseStream.Seek(location, System.IO.SeekOrigin.Begin)
            Return reader.ReadTimespan()

        End Function

#End Region

    End Module
End Namespace

