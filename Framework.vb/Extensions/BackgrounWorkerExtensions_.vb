﻿Imports System.Runtime.CompilerServices
Namespace BackgroundWorkerExtensions

    ''' <summary> Includes extensions for <see cref="ArrayList">array lists</see>. </summary>
    ''' <remarks> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 11/07/2014, 2.1.5425.x. </para></remarks>
    Public Module Methods

        ''' <summary> Reports the progress. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="worker">       The worker. </param>
        ''' <param name="userState">    State of the user. </param>
        ''' <param name="currentCount"> Number of currents. </param>
        ''' <param name="totalCount">   Number of totals. </param>
        <Extension()>
        Public Sub ReportProgress(worker As System.ComponentModel.BackgroundWorker, ByVal userState As Object,
                                  ByVal currentCount As Integer, ByVal totalCount As Integer)
            ReportProgress(worker, userState, currentCount, totalCount, TimeSpan.FromSeconds(1))
        End Sub

        ''' <summary> Reports the progress. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="worker">       The worker. </param>
        ''' <param name="userState">    State of the user. </param>
        ''' <param name="currentCount"> Number of currents. </param>
        ''' <param name="totalCount">   Number of totals. </param>
        ''' <param name="period">       The refractory period - progress is reported every so often. </param>
        <Extension()>
        Public Sub ReportProgress(worker As System.ComponentModel.BackgroundWorker, ByVal userState As Object,
                                  ByVal currentCount As Integer, ByVal totalCount As Integer, ByVal period As TimeSpan)
            Static progressLevel As Double = 0
            Static nextTime As DateTimeOffset = DateTimeOffset.Now + period
            If worker Is Nothing Then Return
            ' update every second
            If currentCount = 0 Then
                progressLevel = 0
                worker.ReportProgress(0, userState)
            ElseIf currentCount >= totalCount Then
                worker.ReportProgress(100, userState)
            Else
                Dim progress As Double = 100 * currentCount / totalCount
                If progress > progressLevel AndAlso DateTimeOffset.Now > nextTime Then
                    worker.ReportProgress(CInt(progress), userState)
                    progressLevel += 1
                    nextTime = DateTimeOffset.Now + period
                End If
            End If
        End Sub

    End Module

End Namespace
