Imports System.Runtime.CompilerServices
Namespace EscapeSequencesExtensions
    ''' <summary> Includes Escape Sequences extensions for <see cref="String">String</see>. </summary>
    ''' <remarks> (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 04/09/2009, 1.1.3386.x. </para><para>
    ''' David, 08/28/2015, 2.1.5718.x"> Requires VS 2015. </para></remarks>
    Public Module Methods
        ''' <summary> The new line escape. </summary>
        Public Const NewLineEscape As String = "\n"
        ''' <summary> The new line value. </summary>
        Public Const NewLineValue As Byte = 10
        ''' <summary> The new line character. </summary>
        Public Const NewLineChar As Char = ChrW(Methods.NewLineValue)
        ''' <summary> The return escape. </summary>
        Public Const ReturnEscape As String = "\r"
        ''' <summary> The return value. </summary>
        Public Const ReturnValue As Byte = 13
        ''' <summary> The return character. </summary>
        Public Const ReturnChar As Char = ChrW(Methods.ReturnValue)

        ''' <summary> Gets or sets the escapes. </summary>
        ''' <value> The escapes. </value>
        Private ReadOnly Property Escapes As String() = New String() {Methods.NewLineEscape, Methods.ReturnEscape}

        ''' <summary> Gets or sets the characters. </summary>
        ''' <value> The characters. </value>
        Private ReadOnly Property Characters As Char() = New Char() {Methods.NewLineChar, Methods.ReturnChar}

        ''' <summary> Gets or sets the values. </summary>
        ''' <value> The values. </value>
        Private ReadOnly Property Values As Byte() = New Byte() {Methods.NewLineValue, Methods.ReturnValue}

        ''' <summary> Query if 'value' is escape value. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> Text including escape sequences. </param>
        ''' <returns> <c>true</c> if escape value; otherwise <c>false</c> </returns>
        <Extension()>
        Public Function IsEscapeValue(ByVal value As Byte) As Boolean
            Return Methods.Values.Contains(value)
        End Function

        ''' <summary> Converts a value to an escape sequence. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> Text including escape sequences. </param>
        ''' <returns> Value as a String. </returns>
        <Extension()>
        Public Function ToEscapeSequence(ByVal value As Byte) As String
            Return Methods.Escapes(Array.IndexOf(Methods.Values, value))
        End Function

        ''' <summary> Query if 'value' is escape value. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> Text including escape sequences. </param>
        ''' <returns> <c>true</c> if escape value; otherwise <c>false</c> </returns>
        <Extension()>
        Public Function IsEscapeValue(ByVal value As String) As Boolean
            Return Methods.Escapes.Contains(value)
        End Function

        ''' <summary> Query if 'value' is escape value. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> Text including escape sequences. </param>
        ''' <returns> <c>true</c> if escape value; otherwise <c>false</c> </returns>
        <Extension()>
        Public Function IsEscapeValue(ByVal value As Char) As Boolean
            Return Methods.Characters.Contains(value)
        End Function

        ''' <summary> Enumerates common escape values in the escape sequence. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="escapeSequence"> Text including escape sequence elements. </param>
        ''' <returns>
        ''' An enumerator that allows for each to be used to process common escape values in this
        ''' collection.
        ''' </returns>
        <Extension()>
        Public Function CommonEscapeValues(ByVal escapeSequence As String) As IEnumerable(Of Byte)
            Dim l As New List(Of Byte)
            If Not String.IsNullOrEmpty(escapeSequence) Then
                For i As Integer = 0 To escapeSequence.Length - 1 Step 2
                    Dim s As String = escapeSequence.Substring(i, 2)
                    If Methods.Escapes.Contains(s, StringComparer.OrdinalIgnoreCase) Then
                        l.Add(Methods.Values(Array.IndexOf(Methods.Escapes, s)))
                    End If
                Next
            End If
            Return l
        End Function

        ''' <summary>
        ''' Replaces common escape strings such as <code>'\n'</code> or <code>'\r'</code>with control
        ''' characters such as <code>10</code> and <code>13</code>, respectively.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> Text including escape sequences. </param>
        ''' <returns> String with escape string replaces with control characters. </returns>
        <Extension()>
        Public Function ReplaceCommonEscapeSequences(ByVal value As String) As String
            If Not String.IsNullOrEmpty(value) Then
                For escapeIndex As Integer = 0 To Methods.Escapes.Length - 1
                    ' must assign the escaped values.
                    value = value.Replace(Methods.Escapes(escapeIndex), Methods.Characters(escapeIndex))
                Next
            End If
            Return value
        End Function

        ''' <summary>
        ''' Replaces control characters such as <code>10</code> and <code>13</code> with common escape
        ''' strings such as <code>'\n'</code> or <code>'\r'</code>, respectively.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> Text including control characters. </param>
        ''' <returns> String with control characters replaces with escape codes. </returns>
        <Extension()>
        Public Function InsertCommonEscapeSequences(ByVal value As String) As String
            If Not String.IsNullOrEmpty(value) Then
                For escapeIndex As Integer = 0 To Methods.Escapes.Length - 1
                    ' must assign the replacement value.
                    value = value.Replace(Methods.Characters(escapeIndex), Methods.Escapes(escapeIndex))
                Next
            End If
            Return value
        End Function

        ''' <summary> Remove common escape sequences. </summary>
        ''' <remarks> David, 7/29/2020. </remarks>
        ''' <param name="value"> Text including escape sequences. </param>
        ''' <returns> A String. </returns>
        <Extension()>
        Public Function RemoveCommonEscapeSequences(ByVal value As String) As String
            If Not String.IsNullOrEmpty(value) Then
                For escapeIndex As Integer = 0 To Methods.Escapes.Length - 1
                    value = value.Replace(Methods.Escapes(escapeIndex), String.Empty)
                Next
            End If
            Return value
        End Function

    End Module
End Namespace
