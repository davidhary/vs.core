﻿Imports System.Runtime.CompilerServices
Namespace ArrayListExtensions

    ''' <summary> Includes extensions for <see cref="ArrayList">array lists</see>. </summary>
    ''' <remarks> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 11/07/2014, 2.1.5425.x. </para></remarks>
    Public Module Methods

        ''' <summary> Concatenates. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="values">    The values. </param>
        ''' <param name="delimiter"> The delimiter. </param>
        ''' <returns> A String. </returns>
        <Extension()>
        Public Function Concatenate(ByVal values As ArrayList, ByVal delimiter As String) As String
            Dim builder As New System.Text.StringBuilder
            If values IsNot Nothing Then
                For Each v As String In values
                    If builder.Length > 0 Then
                        builder.Append(delimiter)
                    End If
                    builder.Append(v)
                Next
            End If
            Return builder.ToString()
        End Function

    End Module

End Namespace
