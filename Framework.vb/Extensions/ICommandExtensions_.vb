﻿Imports System.Windows.Input
Namespace ICommandExtensions

    ''' <summary>
    ''' Extensions to the <see cref="System.Windows.Input.ICommand">interface</see>.
    ''' </summary>
    ''' <remarks>
    ''' https://stackoverflow.com/questions/10126968/call-command-from-code-behind.
    ''' (c) 2017 Stefan Vasiljevic. All rights reserved.<para>
    ''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
    ''' Licensed under The MIT License.</para> </remarks>
    Public Module Methods

        ''' <summary>
        ''' Casts the <see cref="Object"/> to an <see cref="ICommand"/> and executes the command if
        ''' enabled (can execute).
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="command"> The command. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function CheckBeginExecute(command As Object) As Boolean
            Dim iCommand As ICommand = TryCast(command, ICommand)
            If iCommand Is Nothing Then
                Return False
            Else
                Return Methods.CheckBeginExecuteCommand(iCommand)
            End If
        End Function

        ''' <summary> Executes the command if enabled (can execute). </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="command"> The command. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function CheckBeginExecute(command As ICommand) As Boolean
            Return Methods.CheckBeginExecuteCommand(command)
        End Function

        ''' <summary> Executes the command if enabled (can execute). </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="command"> The command. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        Private Function CheckBeginExecuteCommand(command As ICommand) As Boolean
            If command Is Nothing Then Throw New ArgumentNullException(NameOf(command))
            Dim canExecute As Boolean = False
            SyncLock command
                canExecute = command.CanExecute(Nothing)
                If canExecute Then
                    command.Execute(Nothing)
                End If
            End SyncLock
            Return canExecute
        End Function

    End Module


End Namespace
