﻿'               Reference to Windows Base DLL
'           C:\My\LIBRARIES\VS\Share\Extensions\TimeSpanExtensions_.vb
Imports System.Threading.Tasks
Imports System.Runtime.CompilerServices
Imports System.Threading

Namespace TimeSpanExtensions

    ''' <summary> Includes extensions for <see cref="TimeSpan"/> calculations. </summary>
    ''' <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para> </remarks>
    Public Module Methods

#Region " EXACT TIMES "

        ''' <summary> Gets or sets the microseconds per tick. </summary>
        ''' <value> The microseconds per tick. </value>
        Public ReadOnly Property MicrosecondsPerTick As Double = 1000000.0R / Stopwatch.Frequency

        ''' <summary> Gets or sets the millisecond per tick. </summary>
        ''' <value> The millisecond per tick. </value>
        Public ReadOnly Property MillisecondsPerTick As Double = 1000.0R / Stopwatch.Frequency

        ''' <summary> Gets or sets the seconds per tick. </summary>
        ''' <value> The seconds per tick. </value>
        Public ReadOnly Property SecondsPerTick As Double = 1.0R / TimeSpan.TicksPerSecond

        ''' <summary> Gets or sets the ticks per microseconds. </summary>
        ''' <value> The ticks per microseconds. </value>
        Public ReadOnly Property TicksPerMicroseconds As Double = 0.001 * TimeSpan.TicksPerMillisecond

        ''' <summary> Converts seconds to time span with tick timespan accuracy. </summary>
        ''' <remarks>
        ''' <code>
        ''' Dim actualTimespan As TimeSpan = TimeSpan.Zero.FromSecondsPrecise(42.042)
        ''' </code>
        ''' </remarks>
        ''' <param name="seconds"> The number of seconds. </param>
        ''' <returns> A TimeSpan. </returns>
        <Extension()>
        Public Function FromSeconds(ByVal seconds As Double) As TimeSpan
            Return TimeSpan.FromTicks(CLng(TimeSpan.TicksPerSecond * seconds))
        End Function

        ''' <summary> Converts a timespan to the seconds with tick timespan accuracy. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="timespan"> The timespan. </param>
        ''' <returns> Timespan as a Double. </returns>
        <Extension()>
        Public Function ToSeconds(ByVal timespan As TimeSpan) As Double
            Return timespan.Ticks * _SecondsPerTick
        End Function

        ''' <summary> Converts milliseconds to time span with tick timespan accuracy. </summary>
        ''' <remarks>
        ''' <code>
        ''' Dim actualTimespan As TimeSpan = TimeSpan.Zero.FromMillisecondsPrecise(42.042)
        ''' </code>
        ''' </remarks>
        ''' <param name="milliseconds"> The number of milliseconds. </param>
        ''' <returns> A TimeSpan. </returns>
        <Extension()>
        Public Function FromMilliseconds(ByVal milliseconds As Double) As TimeSpan
            Return TimeSpan.FromTicks(CLng(TimeSpan.TicksPerMillisecond * milliseconds))
        End Function

        ''' <summary> Converts a timespan to an exact milliseconds with tick timespan accuracy. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="timespan"> The timespan. </param>
        ''' <returns> Timespan as a Double. </returns>
        <Extension()>
        Public Function ToMilliseconds(ByVal timespan As TimeSpan) As Double
            Return timespan.Ticks * _MillisecondsPerTick
        End Function

        ''' <summary> Converts microseconds to time span with tick timespan accuracy. </summary>
        ''' <remarks>
        ''' <code>
        ''' Dim actualTimespan As TimeSpan = TimeSpan.Zero.FromMicroseconds(42.2)
        ''' </code>
        ''' </remarks>
        ''' <param name="microseconds"> The value. </param>
        ''' <returns> A TimeSpan. </returns>
        <Extension()>
        Public Function FromMicroseconds(ByVal microseconds As Double) As TimeSpan
            Return TimeSpan.FromTicks(CLng(_TicksPerMicroseconds * microseconds))
        End Function

        ''' <summary> Converts a timespan to an exact microseconds with tick timespan accuracy. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="timespan"> The timespan. </param>
        ''' <returns> Timespan as a Double. </returns>
        <Extension()>
        Public Function ToMicroseconds(ByVal timespan As TimeSpan) As Double
            Return timespan.Ticks * _MicrosecondsPerTick
        End Function

#End Region

#Region " DELAYS "

        ''' <summary> The system clock resolution. </summary>
        ''' <value> The system clock resolution. </value>
        Public ReadOnly Property SystemClockResolution As TimeSpan = FromMilliseconds(15.6001)

        ''' <summary> The thread clock resolution. </summary>
        ''' <value> The thread clock resolution. </value>
        Public ReadOnly Property ThreadClockResolution As TimeSpan = FromMilliseconds(1)

        ''' <summary> The high resolution clock resolution. </summary>
        ''' <value> The high resolution clock resolution. </value>
        Public ReadOnly Property HighResolutionClockResolution As TimeSpan = FromSeconds(1 / Stopwatch.Frequency)

        ''' <summary>
        ''' Starts a delay task for delaying operations by the given delay time selecting the delay clock
        ''' which resolution exceeds 0.2 times the delay time.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="delayTime"> The delay time. </param>
        ''' <returns> A Task. </returns>
        <Extension>
        Public Function StartDelayTask(ByVal delayTime As TimeSpan) As Task
            ' set expected resolution to one fifth or the requested delay time
            Return StartDelayTask(delayTime, 0.2)
        End Function

        ''' <summary>
        ''' Starts a delay task for delaying operations by the given delay time selecting the delay clock
        ''' which resolution exceeds
        ''' <paramref name="resolution"/> times the delay time.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="delayTime">  The delay time. </param>
        ''' <param name="resolution"> The relative resolution for the selected clock. </param>
        ''' <returns> A Task. </returns>
        <Extension>
        Public Function StartDelayTask(ByVal delayTime As TimeSpan, ByVal resolution As Double) As Task
            ' set expected resolution time
            Dim timeResolution As TimeSpan = FromSeconds(resolution * ToSeconds(delayTime))
            If delayTime <= HighResolutionClockResolution Then
                ' if timespan is zero, run a NOP task
                Return Task.Factory.StartNew(Sub()
                                                 Thread.SpinWait(1)
                                             End Sub)
            ElseIf timeResolution > SystemClockResolution Then
                ' if resolution higher than the system clock, use the system clock
                Return StartSystemClockDelayTask(delayTime)
            ElseIf timeResolution > ThreadClockResolution Then
                Return StartThreadClockDelayTask(delayTime)
            Else
                Return StartHighResolutionClockDelayTask(delayTime)
            End If
        End Function

        ''' <summary>
        ''' Starts a delay task using the system clock timing of about 15.4 ms resolution.
        ''' </summary>
        ''' <remarks>
        ''' Summary: With zero delay the task does not delay. With less than the clock
        '''           resolution, the delays seems random averaging at the clock resolution. With above
        '''           resolution, we get the quantizes time at multiples of the clock resolution<para>
        ''' Q: Removed outliers</para><para>
        ''' TaskDelay(0.000)   0.002±0.013ms  100:[0.0,0.135]</para><para>
        ''' TaskDelay(0.009)   15.238±4.895ms 100:[9.302,20.502]</para><para>
        ''' TaskDelay(0.020)   30.942±1.986ms 100:[25.768,34.789]</para><para>
        ''' TaskDelay(0.000).Q  0.0±0.0ms      73:[0.0,0.0]</para><para>
        ''' TaskDelay(0.009).Q 15.065±4.965ms 100:[9.384,20.46]</para><para>
        ''' TaskDelay(0.020).Q 31.715±1.11ms   87:[28.614,33.27]</para><para>
        ''' </para>
        ''' </remarks>
        ''' <param name="delay"> The delay. </param>
        ''' <returns> A Task. </returns>
        <Extension>
        Public Function StartSystemClockDelayTask(ByVal delay As TimeSpan) As Task
            Return Task.Delay(delay)
        End Function

        ''' <summary> Starts a delay task using the thread clock of 1 ms resolution. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="delay"> The delay. </param>
        ''' <returns> A Task. </returns>
        <Extension>
        Public Function StartThreadClockDelayTask(ByVal delay As TimeSpan) As Task
            Return Task.Factory.StartNew(Sub()
                                             Thread.Sleep(delay)
                                         End Sub)
        End Function

        ''' <summary> A delay task using the system clock timing of about 15.4 ms resolution. </summary>
        ''' <remarks>
        ''' The thread delay seems totally quantized at 1 ms with zero below 1ms.
        ''' <para>Q: Removed outliers </para><para>
        ''' ThreadTask(0.0000)   0.033±0.16ms  100:[0.005,1.607] </para><para>
        ''' ThreadTask(0.0005)   0.027±0.021ms 100:[0.005,0.114] </para><para>
        ''' ThreadTask(0.0015)   1.88±0.174ms  100:[1.334,2.582] </para><para>
        ''' ThreadTask(0.0019)   1.851±0.184ms 100:[1.262,2.339] </para><para>
        ''' ThreadTask(0.0022)   2.862±0.219ms 100:[2.197,3.441] </para><para>
        ''' ThreadTask(0.0000).Q 0.018±0.007ms  91:[0.008,0.042] </para><para>
        ''' ThreadTask(0.0005).Q 0.054±0.047ms  97:[0.009,0.165] </para><para>
        ''' ThreadTask(0.0015).Q 1.877±0.079ms  78:[1.675,2.083] </para><para>
        ''' ThreadTask(0.0019).Q 1.879±0.084ms  85:[1.627,2.078] </para><para>
        ''' ThreadTask(0.0022).Q 2.891±0.082ms  85:[2.647,3.107] </para><para>
        ''' 
        ''' </para>
        ''' </remarks>
        ''' <param name="delay"> The delay. </param>
        ''' <returns> A Task. </returns>
        <Extension>
        Public Function ThreadClockTask(ByVal delay As TimeSpan) As Task
            Return New Task(Sub()
                                Thread.Sleep(delay)
                            End Sub)
        End Function

        ''' <summary>
        ''' Starts a delay task using the high resolution timer of about 0.1 microsecond.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="delay"> The delay. </param>
        ''' <returns> A Task. </returns>
        <Extension>
        Public Function StartHighResolutionClockDelayTask(ByVal delay As TimeSpan) As Task
            Return Task.Factory.StartNew(Sub()
                                             Dim ticks As Long = delay.Ticks
                                             Dim sw As Stopwatch = Stopwatch.StartNew()
                                             Do Until sw.ElapsedTicks >= ticks
                                                 Thread.SpinWait(1)
                                             Loop
                                         End Sub)
        End Function

        ''' <summary> High resolution clock task. </summary>
        ''' <remarks>
        ''' Looks like minimum timespan would be 15 microseconds. at 100 microseconds delay we get actual
        ''' 150 microseconds average. Q: Outliers removed.  <para>
        ''' HighRes(0.00000)   0.031±0.137ms 100:[0.003,1.341] </para><para>
        ''' HighRes(0.00001)   0.036±0.033ms 100[0.014,0.268] </para><para>
        ''' HighRes(0.00010)   0.171±0.033ms 100:[0.126,0.351] </para><para>
        ''' HighRes(0.00000).Q 0.015±0.003ms 84:[0.009,0.023] </para><para>
        ''' HighRes(0.00001).Q 0.025±0.004ms 86:[0.019,0.037] </para><para>
        ''' HighRes(0.00010).Q 0.159±0.008ms 84:[0.143,0.19] </para><para>
        ''' </para>
        ''' </remarks>
        ''' <param name="delay"> The delay. </param>
        ''' <returns> A Task. </returns>
        <Extension>
        Public Function HighResolutionClockTask(ByVal delay As TimeSpan) As Task
            Return New Task(Sub()
                                Dim ticks As Long = delay.Ticks
                                Dim sw As Stopwatch = Stopwatch.StartNew()
                                Do Until sw.ElapsedTicks >= ticks
                                    Thread.SpinWait(1)
                                Loop
                                'sw.Stop()
                                'Dim x As Long = sw.ElapsedTicks
                                'Dim elapsed As Boolean = x >= ticks
                            End Sub)
        End Function

        ''' <summary> NoOp task. </summary>
        ''' <remarks>
        ''' <para>
        ''' NoOp   0.029±0.121ms 100:[0.004,1.211]</para><para>
        ''' NoOp.Q 0.021±0.017ms  88:[0.004,0.069]</para><para>
        ''' </para>
        ''' </remarks>
        ''' <returns> A Task. </returns>
        Public Function NoOpTask() As Task
            Return New Task(Sub()
                            End Sub)
        End Function

        ''' <summary> Spin wait task. </summary>
        ''' <remarks>
        ''' <para>
        ''' q: Removed outliers.</para><para>
        ''' SpinWait(1)   0.039±0.142ms 100:[0.004,1.319]</para><para>
        ''' SpinWait(9)   0.027±0.026ms 100:[0.004,0.174]</para><para>
        ''' SpinWait(1).Q 0.024±0.02ms   99:[0.004,0.093]</para><para>
        ''' SpinWait(9).Q 0.025±0.018ms  98:[0.004,0.077]</para><para>
        ''' </para>
        ''' </remarks>
        ''' <param name="count"> Number of. </param>
        ''' <returns> A Task. </returns>
        Public Function SpinWaitTask(ByVal count As Integer) As Task
            Return New Task(Sub()
                                Thread.SpinWait(count)
                            End Sub)
        End Function

#End Region

    End Module

End Namespace

