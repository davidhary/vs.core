Imports System.Runtime.CompilerServices
Namespace StopwatchExtensions

    ''' <summary> Includes extensions for <see cref="Stopwatch">Stop Watch</see>. </summary>
    ''' <remarks> (c) 2015 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 03/19/2015, 2.0.5556.x. </para></remarks>
    Public Module Methods

        ''' <summary> Gets or sets the microsecond per tick. </summary>
        ''' <value> The microsecond per tick. </value>
        Public ReadOnly Property MicrosecondPerTick As Double = 1000000.0R / Stopwatch.Frequency

        ''' <summary> Elapsed microseconds. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="stopwatch"> The stop watch. </param>
        ''' <returns> A Long. </returns>
        <Extension()>
        Public Function ElapsedMicroseconds(ByVal stopwatch As Stopwatch) As Double
            Return stopwatch.ElapsedTicks * MicrosecondPerTick
        End Function

        ''' <summary> Gets or sets the millisecond per tick. </summary>
        ''' <value> The millisecond per tick. </value>
        Public ReadOnly Property MillisecondPerTick As Double = 1000.0R / Stopwatch.Frequency

        ''' <summary> Elapsed milliseconds. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="stopwatch"> The stop watch. </param>
        ''' <returns> A Double. </returns>
        <Extension()>
        Public Function ElapsedMilliseconds(ByVal stopwatch As Stopwatch) As Double
            Return stopwatch.ElapsedTicks * MillisecondPerTick
        End Function

        ''' <summary> Query if 'stopwatch' is expired. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="stopwatch">       The stop watch. </param>
        ''' <param name="timeoutTimespan"> The value. </param>
        ''' <returns> <c>true</c> if expired; otherwise <c>false</c> </returns>
        <Extension()>
        Public Function IsExpired(ByVal stopwatch As Stopwatch, ByVal timeoutTimespan As TimeSpan) As Boolean
            Return stopwatch IsNot Nothing AndAlso timeoutTimespan > TimeSpan.Zero AndAlso stopwatch.Elapsed > timeoutTimespan
        End Function

    End Module

End Namespace
