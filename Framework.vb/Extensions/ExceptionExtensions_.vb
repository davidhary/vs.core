Imports System.Text
Imports System.Runtime.CompilerServices

Namespace ExceptionExtensions
    ''' <summary> Includes extensions for <see cref="Exception">Exceptions</see>. </summary>
    ''' <remarks> (c) 2017 Marco Bertschi.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 03/30/2017, 3.1.6298.x">
    ''' https://www.codeproject.com/script/Membership/View.aspx?mid=8888914
    ''' https://www.codeproject.com/Tips/1179564/A-Quick-Dirty-Extension-Method-to-Get-the-Full-Exc
    ''' </para></remarks>
    Partial Public Module Methods

#Region " TO FULL BLOWN STRING "

        ''' <summary> Converts this object to a full blown string. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">   The value. </param>
        ''' <param name="addData"> Adds exception data. </param>
        ''' <returns> The given data converted to a String. </returns>
        Public Function ToFullBlownString(ByVal value As System.Exception, addData As Func(Of Exception, Boolean)) As String
            Return Methods.ToFullBlownString(value, Integer.MaxValue, addData)
        End Function

        ''' <summary> Converts this object to a full blown string. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">   The value. </param>
        ''' <param name="level">   The level. </param>
        ''' <param name="addData"> Adds exception data. </param>
        ''' <returns> The given data converted to a String. </returns>
        Public Function ToFullBlownString(ByVal value As System.Exception, ByVal level As Integer, addData As Func(Of Exception, Boolean)) As String
            Dim builder As New StringBuilder()
            Dim counter As Integer = 1
            builder.AppendLine()
            Do While value IsNot Nothing AndAlso counter <= level
                If addData IsNot Nothing Then addData(value)
                Methods.AppendFullBlownString(builder, value, counter)
                value = value.InnerException
                counter += 1
            Loop
            Return builder.ToString().TrimEnd(Environment.NewLine.ToCharArray)
        End Function

        ''' <summary> Appends a full blown string. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="builder"> The builder. </param>
        ''' <param name="value">   The value. </param>
        ''' <param name="prefix">  The prefix. </param>
        Private Sub AppendFullBlownString(ByVal builder As StringBuilder, ByVal value As System.Exception, ByVal prefix As String)
            If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
            If builder Is Nothing Then Throw New ArgumentNullException(NameOf(builder))
            builder.AppendLine($"{prefix}Type: {value.GetType}")
            If Not String.IsNullOrEmpty(value.Message) Then builder.AppendLine($"{prefix}Message: {value.Message}")
            If Not String.IsNullOrEmpty(value.Source) Then builder.AppendLine($"{prefix}Source: {value.Source}")
            If value.TargetSite IsNot Nothing Then builder.AppendLine($"{prefix}Method: {value.TargetSite}")
            Dim userCallStack As String = value.StackTrace.ParseCallStack(0, 0, prefix, True)
            If Not String.IsNullOrEmpty(userCallStack) Then builder.AppendLine(userCallStack)
            If value.Data IsNot Nothing Then
                For Each keyValuePair As System.Collections.DictionaryEntry In value.Data
                    builder.AppendLine($"{prefix} Data: {keyValuePair.Key}: {keyValuePair.Value}")
                Next
            End If
        End Sub

        ''' <summary> Appends a full blown string. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="builder"> The builder. </param>
        ''' <param name="value">   The value. </param>
        ''' <param name="counter"> The counter. </param>
        Private Sub AppendFullBlownString(ByVal builder As StringBuilder, ByVal value As System.Exception, ByVal counter As Integer)
            Methods.AppendFullBlownString(builder, value, $"{counter}-> ")
        End Sub

#End Region

#Region " STACK TRACE "
        ''' <summary> The framework locations. </summary>
        Private ReadOnly FrameworkLocations As String() = New String() {"at System", "at Microsoft"}

        ''' <summary> Returns true if the line is a framework line. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">        The value. </param>
        ''' <param name="locationName"> [in,out] Name of the location. </param>
        ''' <returns> <c>True</c> if the line is a framework line; otherwise, <c>False</c>. </returns>
        <Extension()>
        Public Function IsFrameworkLine(ByVal value As String, ByRef locationName As String) As Boolean
            For Each locationName In Methods.FrameworkLocations
                If Not String.IsNullOrWhiteSpace(value) AndAlso
                    value.TrimStart.StartsWith(locationName, StringComparison.OrdinalIgnoreCase) Then
                    Return True
                End If
            Next
            Return False
        End Function

        ''' <summary> Parses the stack trace and returns the user or full call stack. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="stackTrace">    Normally formatted Stack Trace. </param>
        ''' <param name="skipLines">     Specifies the number of lines to skip. If getting the stack trace
        '''                              from the <see cref="Environment.StackTrace">environment</see>,
        '''                              the first two lines include the environment methods. </param>
        ''' <param name="totalLines">    Specifies the maximum number of lines to include in the trace.
        '''                              Use 0 to include all lines. </param>
        ''' <param name="prefix">        The prefix. </param>
        ''' <param name="userCallStack"> True to user call stack. </param>
        ''' <returns> The full or user call stacks. </returns>
        <Extension()>
        Public Function ParseCallStack(ByVal stackTrace As String, ByVal skipLines As Integer,
                                       ByVal totalLines As Integer, ByVal prefix As String, ByVal userCallStack As Boolean) As String
            Dim userCallStackBuilder As New System.Text.StringBuilder(&HFFFF)
            Dim fullCallStackBuilder As New System.Text.StringBuilder(&HFFFF)
            If String.IsNullOrWhiteSpace(stackTrace) Then
                Return String.Empty
            End If
            Dim callStack As String() = stackTrace.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
            If callStack.Length <= 0 Then
                Return String.Empty
            End If
            Dim isFrameworkBlock As Boolean = False
            Dim lineNumber As Integer = 0
            Dim lineCount As Integer = 0
            For Each line As String In callStack
                If totalLines = 0 OrElse lineCount <= totalLines Then
                    lineNumber += 1
                    If lineNumber > skipLines Then
                        Dim locationName As String = String.Empty
                        Dim searchFor As String = " in "
                        Dim stackLine As String = $"{prefix}{line.Replace(searchFor, $"{Environment.NewLine}{prefix}  {searchFor}")}"
                        If line.IsFrameworkLine(locationName) Then
                            isFrameworkBlock = True
                            fullCallStackBuilder.AppendLine(stackLine)
                        Else
                            If isFrameworkBlock AndAlso Not String.IsNullOrWhiteSpace(locationName) Then
                                ' if previously had external code, append
                                userCallStackBuilder.AppendLine($"{prefix}{locationName}")
                            End If
                            isFrameworkBlock = False
                            fullCallStackBuilder.AppendLine(stackLine)
                            userCallStackBuilder.AppendLine(stackLine)
                            lineCount += 1
                        End If
                    End If
                End If
            Next
            Return If(userCallStack,
                userCallStackBuilder.ToString.TrimEnd(Environment.NewLine.ToCharArray),
                fullCallStackBuilder.ToString.TrimEnd(Environment.NewLine.ToCharArray))
        End Function

#End Region

    End Module
End Namespace
