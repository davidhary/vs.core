﻿Imports System.Runtime.CompilerServices

Namespace ComponentModelExtensions

    ''' <summary> Includes extensions for component model elements. </summary>
    ''' <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 01/14/2019, 3.4.3953.x. </para></remarks>
    Public Module Methods

#Region " INVOKE ACTIONS "

        ''' <summary> Executes the action on a different thread, and waits for the result. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="synchronizer"> Provides a way to synchronously or asynchronously execute a
        '''                             delegate. </param>
        ''' <param name="action">       The action. </param>
        ''' <example> control.InvokeAction(Sub() control.Text = "invoked") </example>
        <Extension()>
        Public Sub InvokeAction(ByVal synchronizer As System.ComponentModel.ISynchronizeInvoke, ByVal action As Action)
            Methods.InvokeAction(synchronizer, action, Nothing)
        End Sub

        ''' <summary> Executes the action on a different thread, and waits for the result. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="synchronizer"> Provides a way to synchronously or asynchronously execute a
        '''                             delegate. </param>
        ''' <param name="action">       The action. </param>
        ''' <param name="args">         The arguments. </param>
        <Extension()>
        Public Sub InvokeAction(ByVal synchronizer As System.ComponentModel.ISynchronizeInvoke, ByVal action As Action, ByVal args As Object())
            If synchronizer IsNot Nothing And action IsNot Nothing Then
                If synchronizer.InvokeRequired Then
                    synchronizer.Invoke(action, args)
                Else
                    action.Invoke()
                End If
            End If
        End Sub

#End Region

    End Module
End Namespace

