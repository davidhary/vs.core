Imports System.Runtime.CompilerServices
Namespace StackTraceExtensions

    ''' <summary> Includes extensions for <see cref="String">String</see> Compacting. </summary>
    ''' <remarks> (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 02/14/2014, 2.0.5158.x"> Based on legacy Call Stack Parser. </para></remarks>
    Public Module Methods

#Region " COMMON "
        ''' <summary> The framework locations. </summary>
        Private ReadOnly FrameworkLocations As String() = New String() {"at System", "at Microsoft"}

        ''' <summary> Returns true if the line is a framework line. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">        The value. </param>
        ''' <param name="locationName"> [in,out] Name of the location. </param>
        ''' <returns> <c>True</c> if the line is a framework line; otherwise, <c>False</c>. </returns>
        <Extension()>
        Public Function IsFrameworkLine(ByVal value As String, ByRef locationName As String) As Boolean
            For Each locationName In Methods.FrameworkLocations
                If Not String.IsNullOrWhiteSpace(value) AndAlso
                    value.TrimStart.StartsWith(locationName, StringComparison.OrdinalIgnoreCase) Then
                    Return True
                End If
            Next
            Return False
        End Function

#End Region

#Region " USE LINES "

        ''' <summary> Parses the stack trace and returns the user or full call stack. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="trace">         The Stack Trace. </param>
        ''' <param name="skipLines">     Specifies the number of lines to skip. If getting the stack trace
        '''                              from the <see cref="Environment.StackTrace">environment</see>,
        '''                              the first two lines include the environment methods. </param>
        ''' <param name="totalLines">    Specifies the maximum number of lines to include in the trace.
        '''                              Use 0 to include all lines. </param>
        ''' <param name="callStackType"> Type of the call stack to return. </param>
        ''' <returns> The full or user call stacks. </returns>
        <Extension()>
        Public Function ParseCallStack(ByVal trace As StackTrace, ByVal skipLines As Integer,
                                       ByVal totalLines As Integer, ByVal callStackType As CallStackType) As String
            Return If(trace Is Nothing, String.Empty, trace.ToString.ParseCallStack(skipLines, totalLines, callStackType))
        End Function

        ''' <summary> Parses the stack trace and returns the user or full call stack. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="stackTrace">    Normally formatted Stack Trace. </param>
        ''' <param name="skipLines">     Specifies the number of lines to skip. If getting the stack trace
        '''                              from the <see cref="Environment.StackTrace">environment</see>,
        '''                              the first two lines include the environment methods. </param>
        ''' <param name="totalLines">    Specifies the maximum number of lines to include in the trace.
        '''                              Use 0 to include all lines. </param>
        ''' <param name="callStackType"> Type of the call stack to return. </param>
        ''' <param name="prefix">        The prefix. </param>
        ''' <returns> The full or user call stacks. </returns>
        <Extension()>
        Public Function ParseCallStack(ByVal stackTrace As String, ByVal skipLines As Integer,
                                       ByVal totalLines As Integer, ByVal callStackType As CallStackType, ByVal prefix As String) As String
            Dim userCallStackBuilder As New System.Text.StringBuilder(&HFFFF)
            Dim fullCallStackBuilder As New System.Text.StringBuilder(&HFFFF)
            If String.IsNullOrWhiteSpace(stackTrace) Then
                Return String.Empty
            End If
            Dim callStack As String() = stackTrace.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
            If callStack.Length <= 0 Then
                Return String.Empty
            End If
            Dim isFrameworkBlock As Boolean = False
            Dim lineNumber As Integer = 0
            Dim lineCount As Integer = 0
            For Each line As String In callStack
                If totalLines = 0 OrElse lineCount <= totalLines Then
                    lineNumber += 1
                    If lineNumber > skipLines Then
                        Dim locationName As String = String.Empty
                        Dim searchFor As String = " in "
                        Dim stackLine As String = $"{prefix}{line.Replace(searchFor, $"{Environment.NewLine}{prefix}  {searchFor}")}"
                        If line.IsFrameworkLine(locationName) Then
                            isFrameworkBlock = True
                            fullCallStackBuilder.AppendLine(stackLine)
                        Else
                            If isFrameworkBlock AndAlso Not String.IsNullOrWhiteSpace(locationName) Then
                                ' if previously had external code, append
                                userCallStackBuilder.AppendLine($"{prefix}{locationName}")
                            End If
                            isFrameworkBlock = False
                            fullCallStackBuilder.AppendLine(stackLine)
                            userCallStackBuilder.AppendLine(stackLine)
                            lineCount += 1
                        End If
                    End If
                End If
            Next
            Return If(CallStackType.UserCallStack = callStackType,
                userCallStackBuilder.ToString.TrimEnd(Environment.NewLine.ToCharArray),
                fullCallStackBuilder.ToString.TrimEnd(Environment.NewLine.ToCharArray))
        End Function
        ''' <summary> The default prefix. </summary>
        Private Const _DefaultPrefix As String = "    s->"

        ''' <summary> Parses the stack trace and returns the user or full call stack. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="stackTrace">    Normally formatted Stack Trace. </param>
        ''' <param name="skipLines">     Specifies the number of lines to skip. If getting the stack trace
        '''                              from the <see cref="Environment.StackTrace">environment</see>,
        '''                              the first two lines include the environment methods. </param>
        ''' <param name="totalLines">    Specifies the maximum number of lines to include in the trace.
        '''                              Use 0 to include all lines. </param>
        ''' <param name="callStackType"> Type of the call stack to return. </param>
        ''' <returns> The full or user call stacks. </returns>
        <Extension()>
        Public Function ParseCallStack(ByVal stackTrace As String, ByVal skipLines As Integer,
                                       ByVal totalLines As Integer, ByVal callStackType As CallStackType) As String
            Return Methods.ParseCallStack(stackTrace, skipLines, totalLines, callStackType, Methods._DefaultPrefix)
        End Function

        ''' <summary> Parses and returns the user call stack. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="stackTrace"> The Stack Trace. </param>
        ''' <param name="skipLines">  Specifies the number of lines to skip. Two lines include the
        '''                           environment methods. </param>
        ''' <param name="totalLines"> Specifies the maximum number of lines to include in the trace. Use 0
        '''                           to include all lines. </param>
        ''' <param name="prefix">     The prefix. </param>
        ''' <returns> The user call stack, which includes no .NET Framework functions. </returns>
        <Extension()>
        Public Function UserCallStack(ByVal stackTrace As String, ByVal skipLines As Integer,
                                       ByVal totalLines As Integer, ByVal prefix As String) As String
            Return If(String.IsNullOrWhiteSpace(stackTrace),
                String.Empty,
                stackTrace.ParseCallStack(skipLines, totalLines, CallStackType.UserCallStack, prefix))
        End Function

        ''' <summary> Parses and returns the user call stack. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="stackTrace"> The Stack Trace. </param>
        ''' <param name="skipLines">  Specifies the number of lines to skip. Two lines include the
        '''                           environment methods. </param>
        ''' <param name="totalLines"> Specifies the maximum number of lines to include in the trace. Use 0
        '''                           to include all lines. </param>
        ''' <returns> The user call stack, which includes no .NET Framework functions. </returns>
        <Extension()>
        Public Function UserCallStack(ByVal stackTrace As String, ByVal skipLines As Integer,
                                       ByVal totalLines As Integer) As String
            Return If(String.IsNullOrWhiteSpace(stackTrace),
                String.Empty,
                stackTrace.ParseCallStack(skipLines, totalLines, CallStackType.UserCallStack, Methods._DefaultPrefix))
        End Function

        ''' <summary> Parses and returns the user call stack. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="stackTrace"> The Stack Trace. </param>
        ''' <param name="prefix">     The prefix. </param>
        ''' <returns> The user call stack, which includes no .NET Framework functions. </returns>
        ''' <example>
        ''' This example returns the entire user call stack skipping no lines.
        ''' <code>
        ''' exception.StackTrace.UserCallStack("1-&gt; ")
        ''' </code>
        ''' </example>
        <Extension()>
        Public Function UserCallStack(ByVal stackTrace As String, ByVal prefix As String) As String
            Return If(String.IsNullOrWhiteSpace(stackTrace), String.Empty, stackTrace.ParseCallStack(0, 0, CallStackType.UserCallStack, prefix))
        End Function

        ''' <summary> Parses and returns the user call stack. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="trace">  The Stack Trace. </param>
        ''' <param name="prefix"> The prefix. </param>
        ''' <returns> The user call stack, which includes no .NET Framework functions. </returns>
        ''' <example>
        ''' This example returns the entire user call stack skipping no lines.
        ''' <code>
        ''' Environment.StackTrace.UserCallStack("1-&gt; ")
        ''' </code>
        ''' </example>
        <Extension()>
        Public Function UserCallStack(ByVal trace As StackTrace, ByVal prefix As String) As String
            Return If(trace Is Nothing, String.Empty, trace.ToString.ParseCallStack(0, 0, CallStackType.UserCallStack, prefix))
        End Function

        ''' <summary> Parses and returns the user call stack. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="trace">     The Stack Trace. </param>
        ''' <param name="skipLines"> Specifies the number of lines to skip. Two lines include the
        '''                          environment methods. </param>
        ''' <param name="prefix">    The prefix. </param>
        ''' <returns> The user call stack, which includes no .NET Framework functions. </returns>
        ''' <example> This example returns the entire user call stack skipping no lines.
        '''           <code>
        '''           Environment.StackTrace.UserCallStack(0,0)
        '''           </code></example>
        ''' <example> This example returns the user call stack of up to 10 lines skipping the first 3 lines
        '''           from the stack frame including file information (file name, line number and
        '''           column number):
        '''           <code>
        '''           New StackFrame(Debugger.IsAttached).UserCallStack(3,10)
        '''           </code></example>
        <Extension()>
        Public Function UserCallStack(ByVal trace As StackTrace, ByVal skipLines As Integer, ByVal prefix As String) As String
            If trace Is Nothing Then Throw New ArgumentNullException(NameOf(trace))
            Return Methods.ParseCallStack(trace.ToString, skipLines, 0, CallStackType.UserCallStack, prefix)
        End Function

        ''' <summary> Parses and returns the user call stack. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="trace">      The Stack Trace. </param>
        ''' <param name="skipLines">  Specifies the number of lines to skip. Two lines include the
        '''                           environment methods. </param>
        ''' <param name="totalLines"> Specifies the maximum number of lines to include in the trace. Use 0
        '''                           to include all lines. </param>
        ''' <param name="prefix">     The prefix. </param>
        ''' <returns> The user call stack, which includes no .NET Framework functions. </returns>
        ''' <example> This example returns the entire user call stack skipping no lines.
        '''           <code>
        '''           Environment.StackTrace.UserCallStack(0,0)
        '''           </code></example>
        ''' <example> This example returns the user call stack of up to 10 lines skipping the first 3 lines
        '''           from the stack frame including file information (file name, line number and
        '''           column number):
        '''           <code>
        '''           New StackFrame(Debugger.IsAttached).UserCallStack(3,10)
        '''           </code></example>
        <Extension()>
        Public Function UserCallStack(ByVal trace As StackTrace, ByVal skipLines As Integer, ByVal totalLines As Integer, ByVal prefix As String) As String
            If trace Is Nothing Then Throw New ArgumentNullException(NameOf(trace))
            Return Methods.ParseCallStack(trace.ToString, skipLines, totalLines, CallStackType.UserCallStack, prefix)
        End Function

        ''' <summary> Parses and returns the user call stack. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="trace">      The Stack Trace. </param>
        ''' <param name="skipLines">  Specifies the number of lines to skip. Two lines include the
        '''                           environment methods. </param>
        ''' <param name="totalLines"> Specifies the maximum number of lines to include in the trace. Use 0
        '''                           to include all lines. </param>
        ''' <returns> The user call stack, which includes no .NET Framework functions. </returns>
        ''' <example> This example returns the entire user call stack skipping no lines.
        '''           <code>
        '''           Environment.StackTrace.UserCallStack(0,0)
        '''           </code></example>
        ''' <example> This example returns the user call stack of up to 10 lines skipping the first 3 lines
        '''           from the stack frame including file information (file name, line number and
        '''           column number):
        '''           <code>
        '''           New StackFrame(Debugger.IsAttached).UserCallStack(3,10)
        '''           </code></example>
        <Extension()>
        Public Function UserCallStack(ByVal trace As StackTrace, ByVal skipLines As Integer, ByVal totalLines As Integer) As String
            Return Methods.ParseCallStack(trace, skipLines, totalLines, CallStackType.UserCallStack)
        End Function

        ''' <summary> Parses and returns the user call stack. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="stackFrame"> The stack frame. </param>
        ''' <param name="skipLines">  The number of lines to  skip. </param>
        ''' <param name="prefix">     The prefix. </param>
        ''' <returns> The user call stack, which includes no .NET Framework functions. </returns>
        ''' <example> This example returns the entire user call stack skipping no lines.
        '''           <code>
        '''           New StackFrame().UserCallStack(0,0)
        '''           </code></example>
        ''' <example> This example returns the user call stack of up to 10 lines skipping the first 3 lines
        '''           from the stack frame including file information (file name, line number and
        '''           column number):
        '''           <code>
        '''           New StackFrame(Debugger.IsAttached).UserCallStack(3,10)
        '''           </code></example>
        <Extension()>
        Public Function UserCallStack(ByVal stackFrame As StackFrame, ByVal skipLines As Integer, ByVal prefix As String) As String
            Return If(stackFrame Is Nothing, String.Empty, New StackTrace(True).UserCallStack(skipLines, 0, prefix))
        End Function

        ''' <summary> Parses and returns the user call stack. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="stackFrame"> The stack frame. </param>
        ''' <param name="skipLines">  The number of lines to  skip. </param>
        ''' <param name="totalLines"> Specifies the maximum number of lines to include in the trace. Use 0
        '''                           to include all lines. </param>
        ''' <param name="prefix">     The prefix. </param>
        ''' <returns> The user call stack, which includes no .NET Framework functions. </returns>
        ''' <example> This example returns the entire user call stack skipping no lines.
        '''           <code>
        '''           New StackFrame().UserCallStack(0,0)
        '''           </code></example>
        ''' <example> This example returns the user call stack of up to 10 lines skipping the first 3 lines
        '''           from the stack frame including file information (file name, line number and
        '''           column number):
        '''           <code>
        '''           New StackFrame(Debugger.IsAttached).UserCallStack(3,10)
        '''           </code></example>
        <Extension()>
        Public Function UserCallStack(ByVal stackFrame As StackFrame, ByVal skipLines As Integer, ByVal totalLines As Integer, ByVal prefix As String) As String
            Return If(stackFrame Is Nothing, String.Empty, New StackTrace(True).UserCallStack(skipLines, totalLines, prefix))
        End Function

        ''' <summary> Parses and returns the user call stack. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="stackFrame"> The stack frame. </param>
        ''' <param name="skipLines">  The number of lines to  skip. </param>
        ''' <param name="totalLines"> Specifies the maximum number of lines to include in the trace. Use 0
        '''                           to include all lines. </param>
        ''' <returns> The user call stack, which includes no .NET Framework functions. </returns>
        ''' <example> This example returns the entire user call stack skipping no lines.
        '''           <code>
        '''           New StackFrame().UserCallStack(0,0)
        '''           </code></example>
        ''' <example> This example returns the user call stack of up to 10 lines skipping the first 3 lines
        '''           from the stack frame including file information (file name, line number and
        '''           column number):
        '''           <code>
        '''           New StackFrame(Debugger.IsAttached).UserCallStack(3,10)
        '''           </code></example>
        <Extension()>
        Public Function UserCallStack(ByVal stackFrame As StackFrame, ByVal skipLines As Integer, ByVal totalLines As Integer) As String
            Return If(stackFrame Is Nothing, String.Empty, New StackTrace(True).UserCallStack(skipLines, totalLines))
        End Function

        ''' <summary> Parses and returns the user call stack. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="exception">  A system exception. </param>
        ''' <param name="skipLines">  The number of lines to  skip. </param>
        ''' <param name="totalLines"> Specifies the maximum number of lines to include in the trace. Use 0
        '''                           to include all lines. </param>
        ''' <returns> The user call stack, which includes no .NET Framework functions. </returns>
        ''' <example> This example returns the entire user call stack skipping no lines.
        '''           <code>
        '''           ex.UserCallStack(0,0)
        '''           </code></example>
        ''' <example> This example returns the user call stack of up to 10 lines skipping the first 3 lines
        '''           from the exception stack trace:
        '''           <code>
        '''           ex.UserCallStack(3,10)
        '''           </code></example>
        <Extension()>
        Public Function UserCallStack(ByVal exception As Exception, ByVal skipLines As Integer, ByVal totalLines As Integer) As String
            Return If(exception Is Nothing, String.Empty, exception.StackTrace.ParseCallStack(skipLines, totalLines, CallStackType.UserCallStack))
        End Function

        ''' <summary> Parses and returns the user call stack. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="exception">  A system exception. </param>
        ''' <param name="skipLines">  Specifies the number of lines to skip. Two lines include the
        '''                           environment methods. </param>
        ''' <param name="totalLines"> Specifies the maximum number of lines to include in the trace. Use 0
        '''                           to include all lines. </param>
        ''' <param name="prefix">     The prefix. </param>
        ''' <returns> The user call stack, which includes no .NET Framework functions. </returns>
        <Extension()>
        Public Function UserCallStack(ByVal exception As Exception, ByVal skipLines As Integer, ByVal totalLines As Integer, ByVal prefix As String) As String
            Return If(exception Is Nothing,
                String.Empty,
                exception.StackTrace.ParseCallStack(skipLines, totalLines, CallStackType.UserCallStack, prefix))
        End Function

        ''' <summary> User call stack. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="exception"> A system exception. </param>
        ''' <returns> The user call stacks. </returns>
        <Extension()>
        Public Function UserCallStack(ByVal exception As Exception) As String
            Return Methods.UserCallStack(exception, 0, 0)
        End Function

        ''' <summary> Parses and returns the user call stack. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="exception"> A system exception. </param>
        ''' <param name="prefix">    The prefix. </param>
        ''' <returns> The user call stack, which includes no .NET Framework functions. </returns>
        <Extension()>
        Public Function UserCallStack(ByVal exception As Exception, ByVal prefix As String) As String
            Return Methods.UserCallStack(exception, 0, 0, prefix)
        End Function

        ''' <summary> Parses and returns the full call stack. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="trace">      The Stack Trace. </param>
        ''' <param name="skipLines">  Specifies the number of lines to skip. Two lines include the
        '''                           environment methods. </param>
        ''' <param name="totalLines"> Specifies the maximum number of lines to include in the trace. Use 0
        '''                           to include all lines. </param>
        ''' <returns> The full call stack includes .NET Framework functions. </returns>
        <Extension()>
        Public Function FullCallStack(ByVal trace As StackTrace, ByVal skipLines As Integer, ByVal totalLines As Integer) As String
            Return Methods.ParseCallStack(trace, skipLines, totalLines, CallStackType.FullCallStack)
        End Function

        ''' <summary> Parses and returns the full call stack. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="stackFrame"> The stack frame. </param>
        ''' <param name="skipLines">  The number of lines to  skip. </param>
        ''' <param name="totalLines"> Specifies the maximum number of lines to include in the trace. Use 0
        '''                           to include all lines. </param>
        ''' <returns> The full call stack includes .NET Framework functions. </returns>
        <Extension()>
        Public Function FullCallStack(ByVal stackFrame As StackFrame, ByVal skipLines As Integer, ByVal totalLines As Integer) As String
            If stackFrame Is Nothing Then
                Return String.Empty
            Else
                ' Return stackFrame.ToString.FullCallStack(skipLines, totalLines)
                Return New StackTrace(True).FullCallStack(skipLines, totalLines)
            End If
        End Function

        ''' <summary> Parses and returns the full call stack. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="exception">  A system exception. </param>
        ''' <param name="skipLines">  The number of lines to  skip. </param>
        ''' <param name="totalLines"> Specifies the maximum number of lines to include in the trace. Use 0
        '''                           to include all lines. </param>
        ''' <returns> The full call stack includes .NET Framework functions. </returns>
        <Extension()>
        Public Function FullCallStack(ByVal exception As Exception, ByVal skipLines As Integer, ByVal totalLines As Integer) As String
            Return If(exception Is Nothing, String.Empty, exception.StackTrace.ParseCallStack(skipLines, totalLines, CallStackType.FullCallStack))
        End Function

#End Region

#Region " USE FRAME "

        ''' <summary> Parses the stack trace into the user and full call stacks. </summary>
        ''' <remarks> Starts with the first call as defined for the stack frame. </remarks>
        ''' <param name="trace">         The Stack Trace. </param>
        ''' <param name="frame">         The Stack Frame. </param>
        ''' <param name="callStackType"> Type of the call stack to return. </param>
        ''' <returns> The full or user call stacks. </returns>
        ''' <example>
        ''' This example returns the entire user call stack starting with the frame stack call and
        ''' skipping all .NET Framework calls.
        ''' <code>
        ''' New StackTrace(True).ParseCallStack(New StackFrame(True), CallStackType.UserCallStack)
        ''' </code>
        ''' </example>
        <Extension()>
        Public Function ParseCallStack(ByVal trace As StackTrace, ByVal frame As StackFrame, ByVal callStackType As CallStackType) As String
            Return If(trace Is Nothing, String.Empty, Methods.ParseCallStack(trace.ToString, frame, callStackType))
        End Function

        ''' <summary> Parses the stack trace into the user and full call stacks. </summary>
        ''' <remarks> Starts with the first call as defined for the stack frame. </remarks>
        ''' <param name="callStack">     The Stack Trace. </param>
        ''' <param name="frame">         The Stack Frame. </param>
        ''' <param name="callStackType"> Type of the call stack to return. </param>
        ''' <returns> The full or user call stacks. </returns>
        <Extension()>
        Public Function ParseCallStack(ByVal callStack As String, ByVal frame As StackFrame, ByVal callStackType As CallStackType) As String
            If frame Is Nothing Then
                Return String.Empty
            End If
            Dim userrCallStackBuilder As New System.Text.StringBuilder(&HFFFF)
            Dim fullCallStackBuilder As New System.Text.StringBuilder(&HFFFF)
            Dim callFrame As String = New Diagnostics.StackTrace(frame).ToString
            If String.IsNullOrWhiteSpace(callStack) Then
                Return String.Empty
            ElseIf String.IsNullOrWhiteSpace(callFrame) Then
                Return String.Empty
            End If
            Dim callFrameLines As String() = callFrame.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
            If callFrameLines.Length <= 0 Then
                Return String.Empty
            Else
                callFrame = callFrameLines(0).Trim.Substring(0, callFrame.IndexOf("in", StringComparison.OrdinalIgnoreCase))
            End If
            Dim callStackLines As String() = callStack.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
            If callStackLines.Length <= 0 Then
                Return String.Empty
            End If
            Dim isFrameworkBlock As Boolean = False
            Dim isCallFrameBlock As Boolean = False
            For Each line As String In callStackLines
                If Not isCallFrameBlock Then
                    ' looking for the call frame block
                    isCallFrameBlock = line.TrimStart.StartsWith(callFrame, StringComparison.OrdinalIgnoreCase)
                End If
                If isCallFrameBlock Then
                    Dim locationName As String = String.Empty
                    If line.IsFrameworkLine(locationName) Then
                        ' locationName = "   " & locationName
                        isFrameworkBlock = True
                        fullCallStackBuilder.AppendLine($"   {line}")
                    Else
                        If isFrameworkBlock Then
                            ' if previously had external code, append
                            userrCallStackBuilder.AppendLine(locationName)
                            isFrameworkBlock = False
                        End If
                        fullCallStackBuilder.AppendLine(line)
                        userrCallStackBuilder.AppendLine(line)
                    End If
                End If
            Next
            Return If(CallStackType.UserCallStack = callStackType,
                userrCallStackBuilder.ToString.TrimEnd(Environment.NewLine.ToCharArray),
                fullCallStackBuilder.ToString.TrimEnd(Environment.NewLine.ToCharArray))
        End Function

        ''' <summary> User call stack. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="stackTrace"> Normally formatted Stack Trace. </param>
        ''' <param name="stackFrame"> The stack frame. </param>
        ''' <returns> The user call stacks. </returns>
        <Extension()>
        Public Function UserCallStack(ByVal stackTrace As StackTrace, ByVal stackFrame As StackFrame) As String
            Return Methods.ParseCallStack(stackTrace, stackFrame, CallStackType.UserCallStack)
        End Function

        ''' <summary> User call stack. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="stackFrame"> The stack frame. </param>
        ''' <returns> The user call stacks. </returns>
        ''' <example>
        ''' This example returns the entire user call stack starting with the frame stack call and
        ''' skipping all .NET Framework calls.
        ''' <code>
        ''' New StackFrame(True).UserCallStack()
        ''' </code>
        ''' </example>
        <Extension()>
        Public Function UserCallStack(ByVal stackFrame As StackFrame) As String
            Return If(stackFrame Is Nothing, String.Empty, New StackTrace(True).UserCallStack(stackFrame))
        End Function

        ''' <summary> User call stack. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="exception">  A system exception. </param>
        ''' <param name="stackFrame"> The stack frame. </param>
        ''' <returns> The user call stacks. </returns>
        <Extension()>
        Public Function UserCallStack(ByVal exception As Exception, ByVal stackFrame As StackFrame) As String
            Return If(exception Is Nothing, String.Empty, exception.StackTrace.ParseCallStack(stackFrame, CallStackType.UserCallStack))
        End Function

        ''' <summary> Full call stack. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="stackTrace"> Normally formatted Stack Trace. </param>
        ''' <param name="stackFrame"> The stack frame. </param>
        ''' <returns> The full call stacks. </returns>
        ''' <example>
        ''' This example returns the entire call stack starting with the frame stack call.
        ''' <code>
        ''' New StackFrame(True).FullCallStack()
        ''' </code>
        ''' </example>
        <Extension()>
        Public Function FullCallStack(ByVal stackTrace As StackTrace, ByVal stackFrame As StackFrame) As String
            Return Methods.ParseCallStack(stackTrace, stackFrame, CallStackType.FullCallStack)
        End Function

        ''' <summary> Full call stack. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="stackFrame"> The stack frame. </param>
        ''' <returns> The full call stacks. </returns>
        <Extension()>
        Public Function FullCallStack(ByVal stackFrame As StackFrame) As String
            Return If(stackFrame Is Nothing, String.Empty, New StackTrace(True).FullCallStack(stackFrame))
        End Function

        ''' <summary> Full call stack. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="exception">  A system exception. </param>
        ''' <param name="stackFrame"> The stack frame. </param>
        ''' <returns> The full call stack. </returns>
        <Extension()>
        Public Function FullCallStack(ByVal exception As Exception, ByVal stackFrame As StackFrame) As String
            Return If(exception Is Nothing, String.Empty, exception.StackTrace.ParseCallStack(stackFrame, CallStackType.FullCallStack))
        End Function

        ''' <summary> Stack line. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="stackFrame"> The stack frame. </param>
        ''' <returns> The call line for the stack frame. </returns>
        <Extension()>
        Public Function StackLine(ByVal stackFrame As StackFrame) As String
            Return New StackTrace(stackFrame).ToString.TrimEnd(Environment.NewLine.ToCharArray)
        End Function

#End Region

    End Module

    ''' <summary> Values that represent CallStackType. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    Public Enum CallStackType
        ''' <summary> No .NET Framework functions. </summary>
        UserCallStack
        ''' <summary> Includes .NET Framework functions. </summary>
        FullCallStack
    End Enum

End Namespace
