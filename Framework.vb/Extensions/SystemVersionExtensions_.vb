﻿Imports System.Runtime.CompilerServices
Namespace SystemVersionExtensions
    ''' <summary> Includes extensions for <see cref="System.Version">System Version</see> Information. </summary>
    ''' <remarks> (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 04/09/2009, 1.1.3386.x. </para></remarks> 
    Public Module Methods

        ''' <summary> The reference date. </summary>
        Private Const referenceDate As String = "1/1/2000"

        ''' <summary> Returns the reference date for the build number. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <returns> A Date. </returns>
        Public Function BuildNumberReferenceDate() As DateTime
            Return Date.Parse(referenceDate)
        End Function

        ''' <summary> The build number of today. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <returns> The build number for today. </returns>
        Public Function BuildNumber() As Integer
            Return CInt(Math.Floor(DateTimeOffset.UtcNow.Subtract(Methods.BuildNumberReferenceDate).TotalDays))
        End Function

        ''' <summary>
        ''' Returns the date corresponding to the
        ''' <see cref="system.version.Build">build number</see>
        ''' which is the day since the <see cref="referenceDate">build number reference date</see> of
        ''' January 1, 2000.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> Specifies the version information. </param>
        ''' <returns> A Date. </returns>
        <Extension()>
        Public Function BuildDate(ByVal value As System.Version) As DateTime
            If value Is Nothing Then Return BuildNumberReferenceDate()
            Return BuildNumberReferenceDate.Add(TimeSpan.FromDays(value.Build))
        End Function

        ''' <summary> Returns a standard version caption for forms. </summary>
        ''' <remarks> Use this function to set the captions for forms. </remarks>
        ''' <param name="value"> Specifies the version information. </param>
        ''' <returns>
        ''' A <see cref="System.String">String</see> data type in the form WW.XX.YYY.ZZZZß or
        ''' WW.XX.YYY.ZZZZa.
        ''' </returns>
        <Extension()>
        Public Function Caption(ByVal value As System.Version) As String

            If value Is Nothing Then Return String.Empty
            ' set the caption using the product name
            Dim builder As New System.Text.StringBuilder
            builder.Append(value.ToString)
            If value.Major < 1 Then
                builder.Append(".")
                Select Case value.Minor
                    Case 0
                        ' builder.Append("Alpha") 
                        builder.Append(Convert.ToChar(&H3B1))
                    Case 1
                        ' builder.Append("Beta")  
                        builder.Append(Convert.ToChar(&H3B2))
                    Case 2 To 8
                        builder.Append(String.Format(System.Globalization.CultureInfo.CurrentCulture, "RC{0}", value.Minor - 1))
                    Case Else
                        builder.Append("Gold")
                End Select
            End If
            Return builder.ToString

        End Function

        ''' <summary>
        ''' Compares the two versions returning 0 if they are equal, 1 if left version is greater (newer)
        ''' than the right version or -1 if the left version is smaller (older) than the newer one.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="leftVersion">  Left hand side version. </param>
        ''' <param name="rightVersion"> The right version. </param>
        ''' <returns>
        ''' Negative if 'leftVersion' is less than 'rightVersion', 0 if they are equal, or positive if it
        ''' is greater.
        ''' </returns>
        <Extension()>
        Public Function Compare(ByVal leftVersion As System.Version, ByVal rightVersion As System.Version) As Integer
            Return Compare(leftVersion, rightVersion, 4)
        End Function

        ''' <summary>
        ''' Compares the two versions returning 0 if they are equal, 1 if left version is greater (newer)
        ''' than the right version or -1 if the left version is smaller (older) than the newer one.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="leftVersion">          Left hand side version. </param>
        ''' <param name="rightVersion">         The right version. </param>
        ''' <param name="comparedElementCount"> The compared element count. Use 1 to compare major, 2 to
        '''                                     include minor, 3 to include build and 4 to include
        '''                                     revision. 1 is the minimum allowed. </param>
        ''' <returns> System.Int32. </returns>
        <Extension()>
        Public Function Compare(ByVal leftVersion As System.Version, ByVal rightVersion As System.Version, ByVal comparedElementCount As Integer) As Integer
            If leftVersion Is Nothing Then Throw New ArgumentNullException(NameOf(leftVersion))
            If rightVersion Is Nothing Then Throw New ArgumentNullException(NameOf(rightVersion))
            Dim result As Integer = leftVersion.Major.CompareTo(rightVersion.Major)
            If result = 0 AndAlso comparedElementCount > 1 Then
                result = leftVersion.Minor.CompareTo(rightVersion.Minor)
                If result = 0 AndAlso comparedElementCount > 2 Then
                    result = leftVersion.Build.CompareTo(rightVersion.Build)
                    If result = 0 AndAlso comparedElementCount > 3 Then
                        result = leftVersion.Revision.CompareTo(rightVersion.Revision)
                    End If
                End If
            End If
            Return result
        End Function

        ''' <summary>
        ''' Compares the two versions returning 0 if they are equal, 1 if left version is greater (newer)
        ''' than the right version or -1 if the left version is smaller (older) than the newer one.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="leftVersion">  Left hand side version. </param>
        ''' <param name="rightVersion"> The right version. </param>
        ''' <returns>
        ''' Negative if 'leftVersion' is less than 'rightVersion', 0 if they are equal, or positive if it
        ''' is greater.
        ''' </returns>
        <Extension()>
        Public Function Compare(ByVal leftVersion As System.Version, ByVal rightVersion As String) As Integer
            Return Compare(leftVersion, rightVersion, 4)
        End Function

        ''' <summary>
        ''' Compares the two versions returning 0 if they are equal, 1 if left version is greater (newer)
        ''' than the right version or -1 if the left version is smaller (older) than the newer one.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
        '''                                          illegal values. </exception>
        ''' <param name="leftVersion">          Left hand side version. </param>
        ''' <param name="rightVersion">         The right version. </param>
        ''' <param name="comparedElementCount"> The compared element count. Use 1 to compare major, 2 to
        '''                                     include minor, 3 to include build and 4 to include
        '''                                     revision. 1 is the minimum allowed. </param>
        ''' <returns>
        ''' Negative if 'leftVersion' is less than 'rightVersion', 0 if they are equal, or positive if it
        ''' is greater.
        ''' </returns>
        <Extension()>
        Public Function Compare(ByVal leftVersion As System.Version, ByVal rightVersion As String, ByVal comparedElementCount As Integer) As Integer
            If leftVersion Is Nothing Then Throw New ArgumentNullException(NameOf(leftVersion))
            If String.IsNullOrWhiteSpace(rightVersion) Then Throw New ArgumentNullException(NameOf(rightVersion))
            If Not rightVersion.HasValidVersionInfo Then
                Throw New ArgumentException("Invalid version information", NameOf(rightVersion))
            End If
            Return Compare(leftVersion, System.Version.Parse(rightVersion), comparedElementCount)
        End Function

        ''' <summary> Determines if the given value has valid version info. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <returns>
        ''' <c>True</c> if [has valid version info] [the specified value]; otherwise,
        ''' <c>False</c>.
        ''' </returns>
        <Extension()>
        Public Function HasValidVersionInfo(ByVal value As String) As Boolean
            Return System.Version.TryParse(value, New System.Version)
        End Function

    End Module
End Namespace
