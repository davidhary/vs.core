Imports System.Runtime.CompilerServices
Namespace ArrayExtensions

    Partial Public Module Methods

        ''' <summary> Converts the jagged array to a single prevision type. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="values"> The values. </param>
        ''' <returns> The given data converted to a single. </returns>
        <Extension()>
        Public Function CastSingle(ByVal values As Double()()) As Single()()
            If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
            Dim len As Integer = values.Length
            Dim result As Single()() = New Single(len - 1)() {}
            For i As Integer = 0 To len - 1
                result(i) = Methods.CastArray(Of Single())(values(i))
            Next
            Return result
        End Function

        ''' <summary> Determines if the two specified arrays have the same values. </summary>
        ''' <remarks>
        ''' <see cref="T:Array"/> or <see cref="T:Arraylist"/> equals methods cannot be used because it
        ''' expects the two entities to be the same for equality.
        ''' </remarks>
        ''' <param name="left">  The left value. </param>
        ''' <param name="right"> The right value. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        <Extension()>
        Public Function ValueEquals(Of T)(ByVal left As T(), ByVal right As T()) As Boolean
            Dim result As Boolean = True
            If left Is Nothing Then
                result = right Is Nothing
            ElseIf right Is Nothing Then
                result = False
            Else
                For i As Integer = 0 To left.Length - 1
                    If Not left(i).Equals(right(i)) Then
                        result = False
                        Exit For
                    End If
                Next
            End If
            Return result
        End Function

        ''' <summary> Casts an array from one type to another. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="InvalidOperationException"> Thrown when the specified type is not an array. </exception>
        ''' <exception cref="ArgumentException">         Thrown when one or more arguments have
        '''                                              unsupported or illegal values. </exception>
        ''' <exception cref="InvalidCastException">      Thrown when an object cannot be cast to a
        '''                                              required type. </exception>
        ''' <param name="source"> The source array to cast. </param>
        ''' <returns> A new array with the correct new target type. </returns>
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId:="GetElementType")>
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId:="TArray")>
        <Extension()>
        Public Function CastArray(Of TArray As Class)(ByVal source As Array) As TArray
            Dim tempResult As Array
            If source Is Nothing Then
                Return Nothing
            End If

            Dim arrayType As Type = GetType(TArray)
            If Not arrayType.IsArray Then Throw New InvalidOperationException($"Type {arrayType} is not a  {GetType(TArray)} array")

            Dim itemType As Type = arrayType.GetElementType()
            If itemType Is Nothing Then
                Throw New ArgumentException($"{NameOf(System.Type.GetElementType)} {itemType} element type for the array couldn't be resolved")
            End If

            Dim lengths(source.Rank - 1) As Integer
            Dim lowerBounds(source.Rank - 1) As Integer
            Dim upperBounds(source.Rank - 1) As Integer
            For i As Integer = 0 To source.Rank - 1
                lengths(i) = source.GetLength(i)
                lowerBounds(i) = source.GetLowerBound(i)
                upperBounds(i) = source.GetUpperBound(i)
            Next i

            'Creates the array
            tempResult = Array.CreateInstance(itemType, lengths, lowerBounds)

            Dim indexes(lengths.Length - 1) As Integer
            Array.Copy(lowerBounds, indexes, lowerBounds.Length)

            Dim pos As Integer = tempResult.Rank - 1
            indexes(pos) -= 1
            pos = CalculateIndexes(indexes, pos, lowerBounds, upperBounds)

            Do While pos >= 0
                tempResult.SetValue(Convert.ChangeType(source.GetValue(indexes), itemType), indexes)
                pos = CalculateIndexes(indexes, pos, lowerBounds, upperBounds)
            Loop
            Dim finalResult As TArray = TryCast(tempResult, TArray)
            If finalResult Is Nothing Then
                Throw New InvalidCastException($"Unable to cast array type {source.GetType()} to an {GetType(TArray)} array")
            End If
            Return finalResult
        End Function

        ''' <summary> Calculate the indexes of the next element to copy. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="indexes">     [in,out] The list of the indexes in the different dimensions for
        '''                            the element to copy. </param>
        ''' <param name="pos">         The current position within the list of indexes. </param>
        ''' <param name="lowerBounds"> The list of lower bounds to use as limit. </param>
        ''' <param name="upperBounds"> The list of upper bounds to use as limit. </param>
        ''' <returns>
        ''' The current position or -1 if the operation failed which means there is no next element to
        ''' copy.
        ''' </returns>
        Friend Function CalculateIndexes(ByRef indexes() As Integer, ByVal pos As Integer, ByVal lowerBounds() As Integer, ByVal upperBounds() As Integer) As Integer
            indexes(pos) += 1
            If indexes(pos) > upperBounds(pos) Then
                indexes(pos) = lowerBounds(pos)
                pos -= 1
                If pos >= 0 Then
                    pos = CalculateIndexes(indexes, pos, lowerBounds, upperBounds)
                    If pos >= 0 Then
                        pos += 1
                    End If
                End If
            End If
            Return pos
        End Function

    End Module

End Namespace
