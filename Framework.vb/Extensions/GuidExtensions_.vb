Imports System.Runtime.CompilerServices
Namespace GuidExtensions

    ''' <summary> GUID extension methods. </summary>
    ''' <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para> </remarks>
    Partial Public Module Methods

        ''' <summary> Creates a new base 64 unique identifier. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> Unique identifier. </param>
        ''' <returns> A String. </returns>
        <Extension()>
        Public Function NewBase64Guid(ByVal value As Guid) As String
            Dim result As String = Convert.ToBase64String(value.ToByteArray)
            result = result.Substring(0, result.Length - 2)
            Return result
        End Function

        ''' <summary> Appends the base 64 string suffix (==). </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value"> The value. </param>
        ''' <returns> Value as a GUID. </returns>
        <Extension()>
        Public Function ToBase64String(ByVal value As String) As String
            If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
            Dim suffix As String = "=="
            If Not value.EndsWith(suffix, StringComparison.OrdinalIgnoreCase) Then value = $"{value}{suffix}"
            Return value
        End Function

        ''' <summary> Initializes this object from the given from base 64 unique identifier. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value"> The value. </param>
        ''' <returns> A GUID. </returns>
        <Extension()>
        Public Function FromBase64Guid(ByVal value As String) As Guid
            If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
            Return New Guid(Convert.FromBase64String(value.ToBase64String))
        End Function

    End Module

End Namespace
