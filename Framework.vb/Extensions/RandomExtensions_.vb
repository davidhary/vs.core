﻿Imports System.Runtime.CompilerServices
Namespace RandomExtensions

    ''' <summary> Includes extensions for <see cref="Random">random number generation</see>. </summary>
    ''' <remarks> (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 11/14/2013, 2.0.5066.x. </para></remarks>
    Public Module Methods

#Region " RANDOM UNIFORM "

        ''' <summary> Generates the next random uniformly-distributed number. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="random"> The pseudo random number generator. </param>
        ''' <param name="min">    The minimum of the solution value. </param>
        ''' <param name="max">    The maximum of the solution value. </param>
        ''' <returns> A random value between the minimum and maximum. </returns>
        <Extension()>
        Public Function NextUniform(ByVal random As Random, ByVal min As Double, ByVal max As Double) As Double
            If random Is Nothing Then Throw New ArgumentNullException(NameOf(random))
            Return (max - min) * random.NextDouble + min
        End Function

        ''' <summary> Generates an array of random numbers-uniformly distributed. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
        '''                                          illegal values. </exception>
        ''' <param name="random"> The pseudo random number generator. </param>
        ''' <param name="length"> The length. </param>
        ''' <param name="min">    The minimum of the solution value. </param>
        ''' <param name="max">    The maximum of the solution value. </param>
        ''' <returns> An array of random numbers. </returns>
        <Extension()>
        Public Function Uniform(ByVal random As Random, ByVal length As Integer, ByVal min As Double, ByVal max As Double) As Double()
            If random Is Nothing Then
                Throw New ArgumentNullException(NameOf(random))
            End If
            If length <= 0 Then
                Throw New ArgumentException("Length must be positive", NameOf(length))
            End If
            Dim values() As Double = New Double(length - 1) {}
            For i As Integer = 0 To values.Length - 1
                values(i) = Methods.NextUniform(random, min, max)
            Next i
            Return values
        End Function

        ''' <summary> Generates an array of random uniformly-distributed number. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
        '''                                          illegal values. </exception>
        ''' <param name="random"> The pseudo random number generator. </param>
        ''' <param name="min">    The minimum. </param>
        ''' <param name="max">    The maximum. </param>
        ''' <returns> An array of random numbers. </returns>
        <Extension()>
        Public Function Uniform(ByVal random As Random, ByVal min() As Double, ByVal max() As Double) As Double()
            If random Is Nothing Then
                Throw New ArgumentNullException(NameOf(random))
            End If
            If min Is Nothing Then
                Throw New ArgumentNullException(NameOf(min))
            ElseIf max Is Nothing Then
                Throw New ArgumentNullException(NameOf(max))
            ElseIf min.Length = 0 Then
                Throw New ArgumentException("Length must be positive", NameOf(min))
            ElseIf max.Length = 0 Then
                Throw New ArgumentException("Length must be positive", NameOf(max))
            ElseIf min.Length <> max.Length Then
                Throw New ArgumentException("Length of inputs must be the same", NameOf(min))
            End If
            Dim values() As Double = New Double(min.Length - 1) {}
            For i As Integer = 0 To values.Length - 1
                values(i) = Methods.NextUniform(random, min(i), max(i))
            Next i
            Return values
        End Function

#End Region

#Region " RANDOM NORMAL "

        ''' <summary> Generates the next random normally-distributed number. </summary>
        ''' <remarks> Uses the Box-Mueller method. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="random"> The pseudo random number generator. </param>
        ''' <returns> A Double. </returns>
        <Extension()>
        Public Function NextNormal(ByVal random As Random) As Double
            If random Is Nothing Then
                Throw New ArgumentNullException(NameOf(random))
            End If

            Dim x, y, r2 As Double
            Do
                x = 2.0 * random.NextDouble() - 1.0
                y = 2.0 * random.NextDouble() - 1.0
                r2 = x * x + y * y
            Loop While (r2 >= 1.0) OrElse (r2 = 0.0)
            Return x * Math.Sqrt(-2.0 * Math.Log(r2) / r2)

        End Function

        ''' <summary> Generates the next random normally-distributed number. </summary>
        ''' <remarks> Uses the Box-Mueller method. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="random"> The pseudo random number generator. </param>
        ''' <param name="mean">   The mean. </param>
        ''' <param name="sigma">  The sigma. </param>
        ''' <returns> A Double. </returns>
        <Extension()>
        Public Function NextNormal(ByVal random As Random, ByVal mean As Double, ByVal sigma As Double) As Double
            If random Is Nothing Then Throw New ArgumentNullException(NameOf(random))
            Return sigma * random.NextNormal + mean
        End Function

        ''' <summary> Generates an array of random normally-distributed numbers. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
        '''                                          illegal values. </exception>
        ''' <param name="random"> The pseudo random number generator. </param>
        ''' <param name="length"> The length. </param>
        ''' <param name="mean">   The mean. </param>
        ''' <param name="sigma">  The sigma. </param>
        ''' <returns> A Double() </returns>
        <Extension()>
        Public Function Normal(ByVal random As Random, ByVal length As Integer, ByVal mean As Double, ByVal sigma As Double) As Double()
            If random Is Nothing Then Throw New ArgumentNullException(NameOf(random))
            If length <= 0 Then Throw New ArgumentException("Length must be positive", NameOf(length))
            Dim values() As Double = New Double(length - 1) {}
            For i As Integer = 0 To values.Length - 1
                values(i) = Methods.NextNormal(random, mean, sigma)
            Next i
            Return values
        End Function

#End Region

    End Module

End Namespace
