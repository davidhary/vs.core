Imports System.Collections.Specialized
Namespace CollectionExtensions

    ''' <summary>
    ''' Contains extension methods for <see cref="NameValueCollection"/>.
    ''' </summary>
    ''' <remarks> (c) 2016 Cory Charlton.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 12/12/2016, 3.1.6192.x"> https://github.com/CoryCharlton/CCSWE.Core. </para></remarks>
    Public Module Methods

#Region " ADD OR REPLACE "

        ''' <summary> Adds or modifies. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="collection"> The <see cref="NameValueCollection"/> to retrieve the value from. </param>
        ''' <param name="key">        The key associated with the value being retrieved. </param>
        ''' <param name="value">      The value being retrieved. </param>
        <System.Runtime.CompilerServices.Extension>
        Public Sub AddOrModify(Of TKey, TValue)(ByVal collection As IDictionary(Of TKey, TValue), ByVal key As TKey, ByVal value As TValue)
            If collection Is Nothing Then Throw New ArgumentNullException(NameOf(collection))
            If collection.ContainsKey(key) Then
                collection(key) = value
            Else
                collection.Add(key, value)
            End If
        End Sub

        ''' <summary> Adds or replaces. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="collection"> The <see cref="NameValueCollection"/> to retrieve the value from. </param>
        ''' <param name="key">        The key associated with the value being retrieved. </param>
        ''' <param name="value">      The value being retrieved. </param>
        <System.Runtime.CompilerServices.Extension>
        Public Sub AddOrReplace(Of TKey, TValue)(ByVal collection As IDictionary(Of TKey, TValue), ByVal key As TKey, ByVal value As TValue)
            If collection Is Nothing Then Throw New ArgumentNullException(NameOf(collection))
            If collection.ContainsKey(key) Then collection.Remove(key)
            collection.Add(key, value)
        End Sub

#End Region

#Region " GENERIC NAME VALUE SELECTORS "

        ''' <summary>
        ''' Gets the value associated with the specified key from the <see cref='NameValueCollection'/>.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="collection"> The <see cref="NameValueCollection"/> to retrieve the value from. </param>
        ''' <param name="key">        The key associated with the value being retrieved. </param>
        ''' <returns> The value associated with the specified key. </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function GetValueAs(Of T)(ByVal collection As NameValueCollection, ByVal key As String) As T
            If collection Is Nothing Then Throw New ArgumentNullException(NameOf(collection))
            If String.IsNullOrWhiteSpace(key) Then Throw New ArgumentNullException(NameOf(key))
            Dim stringValue As String = collection(key)
            Return Converter.ConvertValue(Of T)(stringValue)
        End Function

        ''' <summary>
        ''' Gets the value associated with the specified key from the <see cref='NameValueCollection'/>.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="collection">   The <see cref="NameValueCollection"/> to retrieve the value from. </param>
        ''' <param name="key">          The key associated with the value being retrieved. </param>
        ''' <param name="defaultValue"> The default value to be returned if the specified key does not
        '''                             exist or the cast fails. </param>
        ''' <returns> The value associated with the specified key. </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function GetValueAs(Of T)(ByVal collection As NameValueCollection, ByVal key As String, ByVal defaultValue As T) As T
            If collection Is Nothing Then Throw New ArgumentNullException(NameOf(collection))
            If String.IsNullOrWhiteSpace(key) Then Throw New ArgumentNullException(NameOf(key))
            Dim stringValue As String = collection(key)
            Return If(String.IsNullOrWhiteSpace(stringValue), defaultValue, Converter.SafeConvert(stringValue, defaultValue))
        End Function

        ''' <summary>
        ''' Tries to get the value associated with the specified key from the
        ''' <see cref='NameValueCollection'/>.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="collection"> The <see cref="NameValueCollection"/> to
        '''                           retrieve the value from. </param>
        ''' <param name="key">        The key associated with the value being
        '''                           retrieved. </param>
        ''' <param name="Value">      The value being retrieved. </param>
        ''' <returns> True if the key exists. False is not. </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function TryGetValueAs(Of T)(ByVal collection As NameValueCollection, ByVal key As String, <System.Runtime.InteropServices.Out()> ByRef value As T) As Boolean
            If collection Is Nothing Then Throw New ArgumentNullException(NameOf(collection))
            If String.IsNullOrWhiteSpace(key) Then Throw New ArgumentNullException(NameOf(key))
            Dim result As Boolean = True
            Dim stringValue As String = collection(key)

            Dim defaultValue As T = Nothing
            If String.IsNullOrWhiteSpace(stringValue) Then
                value = defaultValue
                result = False
            Else
                value = Converter.SafeConvert(stringValue, defaultValue)
            End If
            Return result
        End Function

#End Region

#Region " BINARY LOOKUP "

        ''' <summary> Uses a binary search to find the index of the maximum value from. </summary>
        ''' <remarks> Assumes that the value is within the lower and higher bounds of the array. </remarks>
        ''' <param name="orderedValues"> The ordered values. </param>
        ''' <param name="value">         The lookup value. </param>
        ''' <returns>
        ''' An Integer pointing to the highest value lower or equal to the given value.
        ''' </returns>
        Private Function LookupBoundingIndexThis(Of T As {IComparable(Of T)})(ByVal orderedValues As IEnumerable(Of T), ByVal value As T) As Integer
            Dim first As Integer = 0
            Dim last As Integer = orderedValues.Count - 1
            Dim middle As Integer

            Do
                middle = (first + last) \ 2
                Dim currentCompare As Integer = orderedValues(middle).CompareTo(value)
                Dim nextCompare As Integer = orderedValues(middle + 1).CompareTo(value)
                If (currentCompare <= 0) AndAlso (nextCompare >= 0) Then
                    ' the search is done when the two adjacent indexes bind the search value
                    Exit Do
                ElseIf currentCompare < 0 Then
                    ' if the middle is lower than the value, search the high (middle to last) part of the list
                    first = middle
                Else
                    ' if the middle is higher than the value, search the low (first to middle) part of the list
                    last = middle
                End If
            Loop Until first >= last
            Return middle
        End Function

        ''' <summary> Uses a binary search to find the index of the maximum value from. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
        '''                                          illegal values. </exception>
        ''' <param name="orderedValues"> The ordered values. </param>
        ''' <param name="value">         The lookup value. </param>
        ''' <returns>
        ''' The first index if the ratio is equal or lower than the first value; or <para>
        ''' The last index if the ratio is equal or above the last value.</para>
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function LookupBoundingIndex(Of T As {IComparable(Of T)})(ByVal orderedValues As IEnumerable(Of T), ByVal value As T) As Integer
            If orderedValues Is Nothing Then Throw New ArgumentNullException(NameOf(orderedValues))
            If Not orderedValues.Any Then Throw New ArgumentException("Lookup collection is empty")
            If value.CompareTo(orderedValues(0)) <= 0 Then
                Return 0
            ElseIf value.CompareTo(orderedValues(orderedValues.Count - 1)) >= 0 Then
                Return orderedValues.Count - 1
            Else
                Return Methods.LookupBoundingIndexThis(orderedValues, value)
            End If
        End Function

        ''' <summary>
        ''' Uses a binary search to find the index of the maximum value from
        ''' <paramref name="orderedValues"/> lower than or equal the value.
        ''' </summary>
        ''' <remarks> Assumes that the value is within the lower and higher bounds of the array. </remarks>
        ''' <param name="orderedValues"> The ordered values. </param>
        ''' <param name="value">         The ratio. </param>
        ''' <returns>
        ''' An Integer pointing to the highest value lower or equal to the given value.
        ''' </returns>
        Private Function LookupBoundingIndexThis(ByVal orderedValues As IEnumerable(Of Double), ByVal value As Double) As Integer
            Dim first As Integer = 0
            Dim last As Integer = orderedValues.Count - 1
            Dim middle As Integer

            Do
                middle = (first + last) \ 2
                If (orderedValues(middle) <= value AndAlso orderedValues(middle + 1) >= value) Then
                    ' the search is done when the two adjacent indexes bind the search value
                    Exit Do
                ElseIf orderedValues(middle) < value Then
                    ' if the middle is lower than the value, search the high (middle to last) part of the list
                    first = middle
                Else
                    ' if the middle is higher than the value, search the low (first to middle) part of the list
                    last = middle
                End If
            Loop Until first >= last
            Return middle
        End Function

        ''' <summary>
        ''' Uses a binary search to find the index of the maximum value from
        ''' <paramref name="orderedValues"/> lower than or equal the value.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
        '''                                          illegal values. </exception>
        ''' <param name="orderedValues"> The ordered values. </param>
        ''' <param name="value">         The ratio. </param>
        ''' <returns>
        ''' The first index if the ratio is equal or lower than the first value; or <para>
        ''' The last index if the ratio is equal or above the last value.</para>
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function LookupBoundingIndex(ByVal orderedValues As IEnumerable(Of Double), ByVal value As Double) As Integer
            If orderedValues Is Nothing Then Throw New ArgumentNullException(NameOf(orderedValues))
            If Not orderedValues.Any Then Throw New ArgumentException("Lookup collection is empty")
            If value <= orderedValues(0) Then
                Return 0
            ElseIf value >= orderedValues(orderedValues.Count - 1) Then
                Return orderedValues.Count - 1
            Else
                Return Methods.LookupBoundingIndexThis(orderedValues, value)
            End If
        End Function

#End Region

    End Module
End Namespace


