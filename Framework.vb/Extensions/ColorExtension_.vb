Namespace ColorExtensions
    ''' <summary> Graphics extension Methods. </summary>
    ''' <remarks> (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para> </remarks>
    Public Module Methods

        ''' <summary> Converts a <see cref="Drawing.Color"/> to a gray scale. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="originalColor"> The original color. </param>
        ''' <returns> originalColor as a Drawing.Color. </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function ToGrayscale(ByVal originalColor As Drawing.Color) As Drawing.Color
            If originalColor.Equals(Drawing.Color.Transparent) Then
                Return originalColor
            End If

            Dim grayscale As Integer = CInt(Fix((originalColor.R * 0.299) + (originalColor.G * 0.587) + (originalColor.B * 0.114)))
            Return Drawing.Color.FromArgb(grayscale, grayscale, grayscale)
        End Function

    End Module

End Namespace
