﻿Imports System.Runtime.CompilerServices
Namespace NullableExtensions

    ''' <summary> Includes extensions for <see cref="Nullable">nullable</see>. </summary>
    ''' <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para> </remarks>
    Public Module Methods
        ''' <summary> The null value. </summary>
        Public Const NullValue As String = "NULL"

        ''' <summary>
        ''' Convert this object into a string representation that can be saved in the database.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <returns> A String that represents this object. </returns>
        <Extension()>
        Public Function ToDataString(ByVal value As Boolean?) As String
            Return $"{If(value.HasValue, If(value.Value, "1", "0"), NullValue)}"
        End Function

        ''' <summary>
        ''' Convert this object into a string representation that can be saved in the database.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <returns> A String that represents this object. </returns>
        <Extension()>
        Public Function ToDataString(ByVal value As Byte?) As String
            Return $"{If(value.HasValue, value.Value.ToString, NullValue)}"
        End Function

        ''' <summary>
        ''' Convert this object into a string representation that can be saved in the database.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <returns> A String that represents this object. </returns>
        <Extension()>
        Public Function ToDataString(ByVal value As Integer?) As String
            Return $"{If(value.HasValue, value.Value.ToString, NullValue)}"
        End Function

        ''' <summary>
        ''' Convert this object into a string representation that can be saved in the database.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <returns> A String that represents this object. </returns>
        <Extension()>
        Public Function ToDataString(ByVal value As Long?) As String
            Return $"{If(value.HasValue, value.Value.ToString, NullValue)}"
        End Function

        ''' <summary>
        ''' Convert this object into a string representation that can be saved in the database.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <returns> A String that represents this object. </returns>
        <Extension()>
        Public Function ToDataString(ByVal value As Double?) As String
            Return $"{If(value.HasValue, value.Value.ToString, NullValue)}"
        End Function

        ''' <summary>
        ''' Convert this object into a string representation that can be saved in the database.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">  The value. </param>
        ''' <param name="format"> Describes the format to use. </param>
        ''' <returns> A String that represents this object. </returns>
        <Extension()>
        Public Function ToDataString(ByVal value As Double?, ByVal format As String) As String
            Return Methods.ToDataString(value, format, Methods.NullValue)
        End Function

        ''' <summary>
        ''' Convert this object into a string representation that can be saved in the database.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value">     The value. </param>
        ''' <param name="format">    Describes the format to use. </param>
        ''' <param name="nullValue"> The null value. </param>
        ''' <returns> A String that represents this object. </returns>
        <Extension()>
        Public Function ToDataString(ByVal value As Double?, ByVal format As String, ByVal nullValue As String) As String
            Return $"{If(value.HasValue, $"{value.Value.ToString(format)}", nullValue)}"
        End Function

    End Module

End Namespace
