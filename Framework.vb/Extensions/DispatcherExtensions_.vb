Imports System.Runtime.CompilerServices
Imports System.Windows.Threading
Namespace DispatcherExtensions

    ''' <summary> Dispatcher extension methods. </summary>
    ''' <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para> </remarks>
    Public Module Methods

        ''' <summary> Lets Windows process all the messages currently in the message queue. </summary>
        ''' <remarks>
        ''' https://stackoverflow.com/questions/4502037/where-is-the-application-doevents-in-wpf. Well, I
        ''' just hit a case where I start work on a method that runs on the Dispatcher thread, and it
        ''' needs to block without blocking the UI Thread. Turns out that MSDN topic on the Dispatcher
        ''' Push Frame method explains how to implement a DoEvents() based on the Dispatcher itself:
        ''' https://goo.gl/WGFZEU
        ''' <code>
        ''' Imports System.Windows.Threading
        ''' Imports isr.Core.Services.DispatcherExtensions
        ''' My.MyLibrary.DoEvents()
        ''' </code>
        ''' <list type="bullet">Benchmarks<item>
        ''' 1000 iterations of Application Do Events:   7.8,  8.0, 11,  8.4 us </item><item>
        ''' 1000 iterations of Dispatcher Do Events:   57.6, 92.5, 93, 61.6 us </item></list>
        ''' </remarks>
        ''' <param name="dispatcher"> The dispatcher. </param>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        <Extension>
        Public Sub DoEvents(ByVal dispatcher As Dispatcher)
            Dim frame As New DispatcherFrame()
            dispatcher.BeginInvoke(DispatcherPriority.Background, New DispatcherOperationCallback(Function(ByVal f As Object)
                                                                                                      TryCast(f, DispatcherFrame).Continue = False
                                                                                                      Return Nothing
                                                                                                  End Function), frame)
            Dispatcher.PushFrame(frame)
        End Sub

        ''' <summary>
        ''' Executes the specified delegate on the <see cref="DispatcherPriority.Render"/> priority.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="dispatcher"> The dispatcher. </param>
        ''' <param name="act">        The act. </param>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        <Extension>
        Public Sub Render(ByVal dispatcher As Dispatcher, ByVal act As Action)
            If dispatcher Is Nothing Then Throw New ArgumentNullException(NameOf(dispatcher))
            dispatcher.Invoke(DispatcherPriority.Render, act)
        End Sub

        ''' <summary> Waits while the stop watch is running until its elapsed time expires. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="dispatcher"> The dispatcher. </param>
        ''' <param name="stopwatch">  The stop watch. </param>
        ''' <param name="value">      The value. </param>
        ''' <returns> The elapsed <see cref="TimeSpan"/> </returns>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        <Extension()>
        Public Function LetElapse(ByVal dispatcher As Dispatcher, ByVal stopwatch As Stopwatch, ByVal value As TimeSpan) As TimeSpan
            If dispatcher Is Nothing Then Throw New ArgumentNullException(NameOf(dispatcher))
            If stopwatch IsNot Nothing AndAlso value > TimeSpan.Zero AndAlso stopwatch.IsRunning Then
                Do
                    dispatcher.DoEvents
                Loop Until stopwatch.Elapsed > value
                Return stopwatch.Elapsed
            Else
                Return TimeSpan.Zero
            End If
        End Function

    End Module

End Namespace

