﻿Imports System.Runtime.CompilerServices
Namespace FileInfoExtensions
    ''' <summary> Includes extensions for  <see cref="T:System.IO.FileInfo">file info</see>. </summary>
    ''' <remarks> (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 04/09/2009, 1.1.3386.x. </para></remarks>
    Public Module Methods

        ''' <summary> Returns the file size, -2 if path not specified or -1 if file not found. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="path"> The path. </param>
        ''' <returns> System.Int64. </returns>
        Public Function FileSize(ByVal path As String) As Long
            If String.IsNullOrWhiteSpace(path) Then Return 0
            Dim info As System.IO.FileInfo = New System.IO.FileInfo(path)
            Return info.FileSize()
        End Function

        ''' <summary> Returns the file size, -2 if path not specified or -1 if file not found. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <returns>
        ''' -2 if value Is Nothing or the file name is empty; the size if the file exists; otherwise, -1.
        ''' </returns>
        <Extension()>
        Public Function FileSize(ByVal value As System.IO.FileInfo) As Long

            If value Is Nothing Then
                Return -2
            ElseIf value.Exists Then
                Return value.Length
            ElseIf String.IsNullOrWhiteSpace(value.Name) Then
                Return -2
            Else
                Return -1
            End If

        End Function

        ''' <summary> Move to folder. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value">    The value. </param>
        ''' <param name="folder">   Pathname of the folder. </param>
        ''' <param name="override"> true to override. If file exists, move is not done. </param>
        <Extension()>
        Public Sub MoveToFolder(ByVal value As System.IO.FileInfo, ByVal folder As String, ByVal override As Boolean)
            If value Is Nothing Then
                Throw New ArgumentNullException(NameOf(value))
            ElseIf String.IsNullOrWhiteSpace(folder) Then
                Throw New ArgumentNullException(NameOf(folder))
            Else
                Dim destinationFullName As String = System.IO.Path.Combine(folder, value.Name)
                If System.IO.File.Exists(destinationFullName) Then
                    If override Then
                        System.IO.File.Delete(destinationFullName)
                    Else
                        Return
                    End If
                End If
                value.MoveTo(destinationFullName)
            End If
        End Sub

        ''' <summary> Parses file title. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="name">      The name. </param>
        ''' <param name="extension"> The extension. </param>
        ''' <returns> A String. </returns>
        Private Function ParseFileTitle(ByVal name As String, ByVal extension As String) As String
            Return If(String.IsNullOrWhiteSpace(name),
                String.Empty,
                If(String.IsNullOrWhiteSpace(extension),
                    name,
                    name.Substring(0, name.LastIndexOf(extension, StringComparison.OrdinalIgnoreCase))))
        End Function

        ''' <summary> Returns the file name without extension. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <returns> The file name or white space if the file not found. </returns>
        <Extension()>
        Public Function Title(ByVal value As System.IO.FileInfo) As String
            Return If(value Is Nothing, String.Empty, Methods.ParseFileTitle(value.Name, value.Extension))
        End Function

    End Module
End Namespace
