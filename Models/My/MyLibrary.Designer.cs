namespace isr.Core.Models.My
{

    /// <summary>   Implements the <see cref="isr.Core.ApplianceBase"/> for this class library. </summary>
    /// <remarks>   David, 2020-09-29. </remarks>
    [System.ComponentModel.EditorBrowsable( System.ComponentModel.EditorBrowsableState.Never )]
    public class Appliance : isr.Core.ApplianceBase
    {

        #region " CONSTRUCTION "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-09-29. </remarks>
        public Appliance() : base( MyProject.Application )
        {
            // the application info gets by the assembly that reads it for the first time.
            var info = this.Application.Info;
        }

        #endregion

        /// <summary>   Gets the identifier of the trace source. </summary>
        /// <value> The identifier of the trace event. </value>
        public override int TraceEventId => My.MyLibrary.TraceEventId;

        /// <summary>   The assembly title. </summary>
        /// <value> The assembly title. </value>
        public override string AssemblyTitle => My.MyLibrary.AssemblyTitle;

        /// <summary>   Information describing the assembly. </summary>
        /// <value> Information describing the assembly. </value>
        public override string AssemblyDescription => My.MyLibrary.AssemblyDescription;

        /// <summary>   The assembly product. </summary>
        /// <value> The assembly product. </value>
        public override string AssemblyProduct => My.MyLibrary.AssemblyProduct;

    }

    public partial class MyLibrary
    {

        /// <summary>   The application object provider. </summary>
        private readonly static MyProject.ThreadSafeObjectProvider<isr.Core.Models.My.Appliance> _ApplianceObjectProvider = new MyProject.ThreadSafeObjectProvider<isr.Core.Models.My.Appliance>();

        /// <summary>   Gets the implementation of the <see cref="isr.Core.ApplianceBase"/> for this class library. </summary>  
        /// <value> The implementation of the <see cref="isr.Core.ApplianceBase"/> for this class library. </value>
        [System.ComponentModel.Design.HelpKeyword( "My.MyLibrary.Appliance" )]
        public static isr.Core.Models.My.Appliance Appliance
        {
            get {
                return _ApplianceObjectProvider.GetInstance;
            }
        }

        /// <summary>   Gets the logger. </summary>
        /// <value> The logger. </value>
        public static Logger Logger
        {
            get { return Appliance.Logger; }
        }

        /// <summary>   Gets the unpublished trace messages. </summary>
        /// <value> The unpublished trace messages. </value>
        public static TraceMessagesQueue UnpublishedTraceMessages { get => Appliance.UnpublishedTraceMessages; }

    }

}
