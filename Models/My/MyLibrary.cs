
namespace isr.Core.Models.My
{

    /// <summary> Provides assembly information for the class library. </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    public sealed partial class MyLibrary
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        private MyLibrary() : base()
        {
        }

        /// <summary> Gets the identifier of the trace source. </summary>
        public const int TraceEventId = ( int ) ProjectTraceEventId.Models;

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "Core Models Library";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "Core Models Library";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "Core.Models";

        /// <summary> The Strong Name of the test assembly. </summary>
        public const string TestAssemblyStrongName = "isr.Core.ModelTests,PublicKey=" + isr.Core.My.SolutionInfo.PublicKey;

    }
}
