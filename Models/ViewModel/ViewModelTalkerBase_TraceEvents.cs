using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;

#pragma warning disable IDE1006 // Naming Styles
namespace isr.Core.Models
#pragma warning restore IDE1006 // Naming Styles
{
    public partial class ViewModelTalkerBase : ITalker

    {

        /// <summary> List of trace event bindings. </summary>
        private static BindingList<TraceEventType> _TraceEventBidningList;

        /// <summary> Gets a list of trace event bindings. </summary>
        /// <value> A List of trace event bindings. </value>
        public static BindingList<TraceEventType> TraceEventBindingList
        {
            get {
                if ( _TraceEventBidningList is null || !_TraceEventBidningList.Any() )
                {
                    _TraceEventBidningList = new BindingList<TraceEventType>() { TraceEventType.Critical, TraceEventType.Error, TraceEventType.Warning, TraceEventType.Information, TraceEventType.Verbose };
                }

                return _TraceEventBidningList;
            }
        }

        /// <summary> The trace events. </summary>
        private static List<KeyValuePair<TraceEventType, string>> _TraceEvents;

        /// <summary> Gets the trace events. </summary>
        /// <value> The trace events. </value>
        public static IEnumerable<KeyValuePair<TraceEventType, string>> TraceEvents
        {
            get {
                if ( _TraceEvents is null || !_TraceEvents.Any() )
                {
                    _TraceEvents = new List<KeyValuePair<TraceEventType, string>>() { ToTraceEvent( TraceEventType.Critical ), ToTraceEvent( TraceEventType.Error ), ToTraceEvent( TraceEventType.Warning ), ToTraceEvent( TraceEventType.Information ), ToTraceEvent( TraceEventType.Verbose ) };
                }

                return _TraceEvents;
            }
        }

        /// <summary> Converts a value to a trace event. </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> Value as a KeyValuePair(Of TraceEventType, String) </returns>
        public static KeyValuePair<TraceEventType, string> ToTraceEvent( TraceEventType value )
        {
            return new KeyValuePair<TraceEventType, string>( value, value.ToString() );
        }

        /// <summary> The trace log event. </summary>
        private KeyValuePair<TraceEventType, string> _TraceLogEvent;

        /// <summary> Gets or sets the trace log level. </summary>
        /// <value> The trace level. </value>
        public KeyValuePair<TraceEventType, string> TraceLogEvent
        {
            get => this._TraceLogEvent;

            set {
                if ( value.Key != this.TraceLogEvent.Key )
                {
                    this._TraceLogEvent = value;
                    this.TraceLogLevel = value.Key;
                    this.SyncNotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the trace log level. </summary>
        /// <value> The trace level. </value>
        public TraceEventType TraceLogLevel
        {
            get => this.Talker.TraceLogLevel;

            set {
                if ( value != this.TraceLogLevel )
                {
                    this.ApplyTalkerTraceLevel( ListenerType.Logger, value );
                    this.SyncNotifyPropertyChanged();
                    this.TraceLogEvent = ToTraceEvent( value );
                }
            }
        }

        /// <summary> The trace show event. </summary>
        private KeyValuePair<TraceEventType, string> _TraceShowEvent;

        /// <summary> Gets or sets the trace Show level. </summary>
        /// <value> The trace level. </value>
        public KeyValuePair<TraceEventType, string> TraceShowEvent
        {
            get => this._TraceShowEvent;

            set {
                if ( value.Key != this.TraceShowEvent.Key )
                {
                    this._TraceShowEvent = value;
                    this.TraceShowLevel = value.Key;
                    this.SyncNotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the trace Show level. </summary>
        /// <value> The trace level. </value>
        public TraceEventType TraceShowLevel
        {
            get => this.Talker.TraceShowLevel;

            set {
                if ( value != this.TraceShowLevel )
                {
                    this.ApplyTalkerTraceLevel( ListenerType.Display, value );
                    this.SyncNotifyPropertyChanged();
                    this.TraceShowEvent = ToTraceEvent( value );
                }
            }
        }

    }
}
