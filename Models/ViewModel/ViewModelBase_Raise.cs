using System.ComponentModel;

namespace isr.Core.Models
{
    public partial class ViewModelBase
    {

        #region " NOTIFY DEFAUILT: SYNCHRONIZED (USING SEND) "

        /// <summary>
        /// Asynchronously notifies (posts) property change on a different thread. Unsafe for cross
        /// threading; much faster.
        /// </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        /// <param name="e"> Property Changed event information. </param>
        public virtual void NotifyPropertyChanged( PropertyChangedEventArgs e )
        {
            this._PropertyChangedHandlers.Post( this, e );
        }

        /// <summary>
        /// Asynchronously notifies (posts) property change on a different thread. Unsafe for cross
        /// threading; much faster.
        /// </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        /// <param name="name"> (Optional) caller member. </param>
        public void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            if ( !string.IsNullOrWhiteSpace( name ) )
            {
                this.NotifyPropertyChanged( new PropertyChangedEventArgs( name ) );
            }
        }

        #endregion

        #region " ASYNC NOTIFY: POST "

        /// <summary>
        /// Asynchronously notifies (posts) property change on a different thread. Unsafe for cross
        /// threading; much faster.
        /// </summary>
        /// <remarks>
        /// Overhead of this method is 7 to 10 times larger than naked raise event. This has no advantage
        /// even with slow handler functions.
        /// </remarks>
        /// <param name="e"> Property Changed event information. </param>
        public virtual void AsyncNotifyPropertyChanged( PropertyChangedEventArgs e )
        {
            this._PropertyChangedHandlers.Post( this, e );
        }

        /// <summary>
        /// Asynchronously notifies (posts) property change on a different thread. Unsafe for cross
        /// threading; much faster.
        /// </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        /// <param name="name"> (Optional) caller member. </param>
        public void AsyncNotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            if ( !string.IsNullOrWhiteSpace( name ) )
            {
                this.AsyncNotifyPropertyChanged( new PropertyChangedEventArgs( name ) );
            }
        }

        #endregion

        #region " SYNC NOTIFY: SEND "

        /// <summary>
        /// Synchronously notifies (send) property change on a different thread. Safe for cross threading.
        /// </summary>
        /// <remarks>
        /// Overhead of this method is 3 to 4 times larger than naked raise event. This is the best
        /// approach.
        /// </remarks>
        /// <param name="e"> Property Changed event information. </param>
        public virtual void SyncNotifyPropertyChanged( PropertyChangedEventArgs e )
        {
            this._PropertyChangedHandlers.Send( this, e );
        }

        /// <summary>
        /// Synchronously notifies (send) property change on a different thread. Safe for cross threading.
        /// </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        /// <param name="name"> (Optional) caller member. </param>
        public void SyncNotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            if ( !string.IsNullOrWhiteSpace( name ) )
            {
                this.SyncNotifyPropertyChanged( new PropertyChangedEventArgs( name ) );
            }
        }

        #endregion

        #region " RAISE (SENDS) "

        /// <summary>
        /// Synchronously notifies property change on the synchronization thread using 'Send' to prevent
        /// cross thread exceptions.
        /// </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        /// <param name="name"> (Optional) caller member. </param>
        public void RaisePropertyChanged( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            if ( !string.IsNullOrWhiteSpace( name ) )
            {
                this.RaisePropertyChanged( new PropertyChangedEventArgs( name ) );
            }
        }

        /// <summary>
        /// Synchronously notifies property change on the synchronization thread using 'Send' to prevent
        /// cross thread exceptions.
        /// </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        public virtual void RaisePropertyChanged( PropertyChangedEventArgs e )
        {
            this.OnPropertyChanged( this, e );
        }

        #endregion

    }
}
