using System;

using isr.Core.Models.ExceptionExtensions;

using Microsoft.VisualBasic;

namespace isr.Core.Models
{

    /// <summary> A resource Opener view model base class. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-11 </para>
    /// </remarks>
    public abstract class OpenerViewModel : ViewModelTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Specialized default constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        protected OpenerViewModel() : base()
        {
            this.NewThis();
        }

        /// <summary> Specialized default constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        /// <param name="talker"> The talker. </param>
        protected OpenerViewModel( ITraceMessageTalker talker ) : base( talker )
        {
            this.NewThis();
        }

        /// <summary> News this object. </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        private void NewThis()
        {
            this.OpenedImage = My.Resources.Resources.Opened;
            this.ClosedImage = My.Resources.Resources.Closed;
            this._ClearImage = My.Resources.Resources.Clear;
            this._Openable = true;
            this._Clearable = true;
        }

        #endregion

        #region " CLEARABLE "

        /// <summary> True if clearable. </summary>
        private bool _Clearable;

        /// <summary>
        /// Gets or sets the value indicating if the clear button is visible and can be enabled. An item
        /// can be cleared only if it is Opened.
        /// </summary>
        /// <value> The clearable. </value>
        public bool Clearable
        {
            get => this._Clearable;

            set {
                if ( !this.Clearable.Equals( value ) )
                {
                    this._Clearable = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The clear tool tip. </summary>
        private string _ClearToolTip;

        /// <summary> Gets or sets the Clear tool tip. </summary>
        /// <value> The Clear tool tip. </value>
        public string ClearToolTip
        {
            get => this._ClearToolTip;

            set {
                if ( !string.Equals( value, this.ClearToolTip ) )
                {
                    this._ClearToolTip = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The clear image. </summary>
        private System.Drawing.Image _ClearImage;

        /// <summary> Gets or sets the Clear Image. </summary>
        /// <value> The Clear tool tip. </value>
        public System.Drawing.Image ClearImage
        {
            get => this._ClearImage;

            set {
                if ( !Equals( value, this.ClearImage ) )
                {
                    this._ClearImage = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " OPENABLE "

        /// <summary> True if openable. </summary>
        private bool _Openable;

        /// <summary>
        /// Gets or sets the value indicating if the Open button is visible and can be enabled. An item
        /// can be Opened only if it is selected.
        /// </summary>
        /// <value> The Openable. </value>
        public bool Openable
        {
            get => this._Openable;

            set {
                if ( !this.Openable.Equals( value ) )
                {
                    this._Openable = value;
                    this.NotifyPropertyChanged();
                }

                this.OpenEnabled = this.Openable && !string.IsNullOrWhiteSpace( this.ValidatedResourceName );
            }
        }

        /// <summary> True to enable, false to disable the open. </summary>
        private bool _OpenEnabled;

        /// <summary> Gets or sets Open Enabled state. </summary>
        /// <value> The Open enabled. </value>
        public bool OpenEnabled
        {
            get => this._OpenEnabled;

            set {
                this._OpenEnabled = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> True to enable, false to disable the validation. </summary>
        private bool _ValidationEnabled;

        /// <summary> Gets or sets Validation Enabled state. </summary>
        /// <remarks>
        /// Validation is disabled by default to facilitate resource selection in case of resource
        /// manager mismatch between VISA implementations.
        /// </remarks>
        /// <value> The Validation enabled. </value>
        public bool ValidationEnabled
        {
            get => this._ValidationEnabled;

            set {
                this._ValidationEnabled = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> Name of the validated resource. </summary>
        private string _ValidatedResourceName;

        /// <summary> Returns the validated resource name. </summary>
        /// <value> The name of the validated resource. </value>
        public string ValidatedResourceName
        {
            get => this._ValidatedResourceName;

            set {
                if ( string.IsNullOrWhiteSpace( value ) )
                {
                    value = string.Empty;
                }

                value = value.Trim();
                if ( !string.Equals( value, this.ValidatedResourceName, StringComparison.OrdinalIgnoreCase ) )
                {
                    this._ValidatedResourceName = value;
                    this.NotifyPropertyChanged();
                }

                this.CandidateResourceNameValidated = !string.IsNullOrWhiteSpace( this.ValidatedResourceName );
                this.OpenEnabled = this.Openable && this.CandidateResourceNameValidated;
            }
        }

        /// <summary> True if candidated resource name validated. </summary>
        private bool _CandidatedResourceNameValidated;

        /// <summary> Gets or sets the candidate resource name validated. </summary>
        /// <value> The candidate resource name validated. </value>
        public virtual bool CandidateResourceNameValidated
        {
            get => this._CandidatedResourceNameValidated;

            set {
                this._CandidatedResourceNameValidated = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> The open tool tip. </summary>
        private string _OpenToolTip;

        /// <summary> Gets or sets the Open tool tip. </summary>
        /// <value> The Open tool tip. </value>
        public string OpenToolTip
        {
            get => this._OpenToolTip;

            set {
                if ( !string.Equals( value, this.OpenToolTip ) )
                {
                    this._OpenToolTip = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " OPEN FIELDS "

        /// <summary> Gets the Opened image. </summary>
        /// <value> The Opened image. </value>
        public System.Drawing.Image OpenedImage { get; set; }

        /// <summary> Gets the Closed image. </summary>
        /// <value> The Closed image. </value>
        public System.Drawing.Image ClosedImage { get; set; }

        /// <summary> Gets the Open image. </summary>
        /// <value> The Open image. </value>
        public System.Drawing.Image OpenImage => this.IsOpen ? this.OpenedImage : this.ClosedImage;

        /// <summary> Gets the Open status. </summary>
        /// <value> The Open status. </value>
        public abstract bool IsOpen { get; }

        /// <summary> Gets the close status. </summary>
        /// <value> The Close status. </value>
        public bool IsClose => !this.IsOpen;

        #endregion

        #region " NAME  "

        /// <summary> Gets the name of the designated resource. </summary>
        /// <value> The name of the designated resource. </value>
        public string DesignatedResourceName => this.CandidateResourceNameValidated ? this.ValidatedResourceName : this.CandidateResourceName;

        /// <summary> Name of the candidate resource. </summary>
        private string _CandidateResourceName;

        /// <summary> Gets or sets the name of the candidate resource. </summary>
        /// <value> The name of the candidate resource. </value>
        public virtual string CandidateResourceName
        {
            get => this._CandidateResourceName;

            set {
                if ( value is null )
                {
                    value = string.Empty;
                }

                if ( !string.Equals( this.CandidateResourceName, value ) )
                {
                    this._CandidateResourceName = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Name of the open resource. </summary>
        private string _OpenResourceName;

        /// <summary> Gets or sets the name of the open resource. </summary>
        /// <value> The name of the open resource. </value>
        public virtual string OpenResourceName
        {
            get => this._OpenResourceName;

            set {
                if ( value is null )
                {
                    value = string.Empty;
                }

                if ( !string.Equals( this.OpenResourceName, value ) )
                {
                    this._OpenResourceName = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " TITLE "

        /// <summary> The candidate resource title. </summary>
        private string _CandidateResourceTitle;

        /// <summary> Gets or sets the candidate resource title. </summary>
        /// <value> The candidate resource title. </value>
        public virtual string CandidateResourceTitle
        {
            get => this._CandidateResourceTitle;

            set {
                if ( value is null )
                {
                    value = string.Empty;
                }

                if ( !string.Equals( value, this.CandidateResourceTitle ) )
                {
                    this._CandidateResourceTitle = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The open resource title. </summary>
        private string _OpenResourceTitle;

        /// <summary> Gets or sets a short title for the device. </summary>
        /// <value> The short title of the device. </value>
        public virtual string OpenResourceTitle
        {
            get => this._OpenResourceTitle;

            set {
                if ( value is null )
                {
                    value = string.Empty;
                }

                if ( !string.Equals( this.OpenResourceTitle, value ) )
                {
                    this._OpenResourceTitle = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The resource title caption. </summary>
        private string _ResourceTitleCaption;

        /// <summary> Gets or sets the Title caption. </summary>
        /// <value> The Title caption. </value>
        public virtual string ResourceTitleCaption
        {
            get => this._ResourceTitleCaption;

            set {
                if ( !string.Equals( this.ResourceTitleCaption, value ) )
                {
                    this._ResourceTitleCaption = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " CAPTION "

        /// <summary> The resource closed caption. </summary>
        private string _ResourceClosedCaption;

        /// <summary> Gets or sets the default resource name closed caption. </summary>
        /// <value> The resource closed caption. </value>
        public virtual string ResourceClosedCaption
        {
            get {
                if ( string.IsNullOrEmpty( this._ResourceClosedCaption ) )
                {
                    this._ResourceClosedCaption = "<closed>";
                }

                return this._ResourceClosedCaption;
            }

            set {
                if ( !string.Equals( this.ResourceClosedCaption, value ) )
                {
                    this._ResourceClosedCaption = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The resource name caption. </summary>
        private string _ResourceNameCaption;

        /// <summary> Gets or sets the resource name caption. </summary>
        /// <value>
        /// The <see cref="OpenResourceName"/> or <see cref="CandidateResourceName"/> resource names.
        /// </value>
        public virtual string ResourceNameCaption
        {
            get => this._ResourceNameCaption;

            set {
                if ( value is null )
                {
                    value = string.Empty;
                }

                if ( !string.Equals( this.ResourceNameCaption, value ) )
                {
                    this._ResourceNameCaption = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " OPENING "

        /// <summary> Allows taking actions before opening. </summary>
        /// <remarks>
        /// This override should occur as the first call of the overriding method. After this call, the
        /// parent class adds the subsystems.
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void OnOpening( System.ComponentModel.CancelEventArgs e )
        {
            if ( e is null )
            {
                throw new ArgumentNullException( nameof( e ) );
            }

            if ( !e.Cancel )
            {
                this.IsInitialized = false;
                this.SyncNotifyOpening( e );
            }
        }

        /// <summary> Removes the Opening event handlers. </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        protected void RemoveOpeningEventHandlers()
        {
            this._OpeningEventHandlers?.RemoveAll();
        }

        /// <summary> The Opening event handlers. </summary>
        private readonly EventHandlerContextCollection<System.ComponentModel.CancelEventArgs> _OpeningEventHandlers = new EventHandlerContextCollection<System.ComponentModel.CancelEventArgs>();

        /// <summary> Event queue for all listeners interested in Opening events. </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event EventHandler<System.ComponentModel.CancelEventArgs> Opening
        {
            add {
                this._OpeningEventHandlers.Add( new EventHandlerContext<System.ComponentModel.CancelEventArgs>( value ) );
            }

            remove {
                this._OpeningEventHandlers.RemoveValue( value );
            }
        }

        private void OnOpening( object sender, System.ComponentModel.CancelEventArgs e )
        {
            this._OpeningEventHandlers.Send( sender, e );
        }

        /// <summary>
        /// Safely and synchronously sends or invokes the <see cref="Opening">Opening Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        /// <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
        protected void SyncNotifyOpening( System.ComponentModel.CancelEventArgs e )
        {
            this._OpeningEventHandlers.Send( this, e );
        }

        #endregion

        #region " OPENED "

        /// <summary> Notifies an open changed. </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        public virtual void NotifyOpenChanged()
        {
            this.OpenToolTip = $"Click to {(this.IsOpen ? "Close" : "Open")}";
            this.NotifyPropertyChanged( nameof( isr.Core.Models.OpenerViewModel.IsOpen ) );
            this.NotifyPropertyChanged( nameof( isr.Core.Models.OpenerViewModel.IsClose ) );
            this.NotifyPropertyChanged( nameof( isr.Core.Models.OpenerViewModel.OpenImage ) );
            if ( this.IsOpen )
            {
                this.ResourceTitleCaption = this.OpenResourceTitle;
                this.CandidateResourceTitle = this.OpenResourceTitle;
                this.ResourceNameCaption = $"{this.OpenResourceTitle}.{this.OpenResourceName}";
            }
            else
            {
                this.ResourceTitleCaption = $"{this.OpenResourceTitle}.{this.ResourceClosedCaption}";
                this.ResourceNameCaption = $"{this.OpenResourceTitle}.{this.OpenResourceName}.{this.ResourceClosedCaption}";
            }
        }

        /// <summary> Notifies of the opened event. </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void OnOpened( EventArgs e )
        {
            this.NotifyOpenChanged();
            this.SyncNotifyOpened( e );
        }

        /// <summary> Removes the Opened event handlers. </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        protected void RemoveOpenedEventHandlers()
        {
            this._OpenedEventHandlers?.RemoveAll();
        }

        /// <summary> The Opened event handlers. </summary>
        private readonly EventHandlerContextCollection<EventArgs> _OpenedEventHandlers = new EventHandlerContextCollection<EventArgs>();

        /// <summary> Event queue for all listeners interested in Opened events. </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event EventHandler<EventArgs> Opened
        {
            add {
                this._OpenedEventHandlers.Add( new EventHandlerContext<EventArgs>( value ) );
            }

            remove {
                this._OpenedEventHandlers.RemoveValue( value );
            }
        }

        private void OnOpened( object sender, EventArgs e )
        {
            this._OpenedEventHandlers.Send( sender, e );
        }

        /// <summary>
        /// Safely and synchronously Sends or invokes the <see cref="Opened">Opened Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        /// <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
        protected void SyncNotifyOpened( EventArgs e )
        {
            this._OpenedEventHandlers.Send( this, e );
        }

        #endregion

        #region " CLOSING "

        /// <summary> Allows taking actions before closing. </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void OnClosing( System.ComponentModel.CancelEventArgs e )
        {
            if ( e is null )
            {
                throw new ArgumentNullException( nameof( e ) );
            }

            if ( !e.Cancel )
            {
                this.IsInitialized = false;
                this.SyncNotifyClosing( e );
            }
        }

        /// <summary> Removes the Closing event handlers. </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        protected void RemoveClosingEventHandlers()
        {
            this._ClosingEventHandlers?.RemoveAll();
        }

        /// <summary> The Closing event handlers. </summary>
        private readonly EventHandlerContextCollection<System.ComponentModel.CancelEventArgs> _ClosingEventHandlers = new EventHandlerContextCollection<System.ComponentModel.CancelEventArgs>();

        /// <summary> Event queue for all listeners interested in Closing events. </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event EventHandler<System.ComponentModel.CancelEventArgs> Closing
        {
            add {
                this._ClosingEventHandlers.Add( new EventHandlerContext<System.ComponentModel.CancelEventArgs>( value ) );
            }

            remove {
                this._ClosingEventHandlers.RemoveValue( value );
            }
        }

        private void OnClosing( object sender, System.ComponentModel.CancelEventArgs e )
        {
            this._ClosingEventHandlers.Send( sender, e );
        }

        /// <summary>
        /// Safely and synchronously Sends or invokes the <see cref="Closing">Closing Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        /// <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
        protected void SyncNotifyClosing( System.ComponentModel.CancelEventArgs e )
        {
            this._ClosingEventHandlers.Send( this, e );
        }

        #endregion

        #region " CLOSED "

        /// <summary> Notifies of the closed event. </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void OnClosed( EventArgs e )
        {
            this.NotifyOpenChanged();
            this.SyncNotifyClosed( e );
        }

        /// <summary> Removes the Closed event handlers. </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        protected void RemoveClosedEventHandlers()
        {
            this._ClosedEventHandlers?.RemoveAll();
        }

        /// <summary> The Closed event handlers. </summary>
        private readonly EventHandlerContextCollection<EventArgs> _ClosedEventHandlers = new EventHandlerContextCollection<EventArgs>();

        /// <summary> Event queue for all listeners interested in Closed events. </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event EventHandler<EventArgs> Closed
        {
            add {
                this._ClosedEventHandlers.Add( new EventHandlerContext<EventArgs>( value ) );
            }

            remove {
                this._ClosedEventHandlers.RemoveValue( value );
            }
        }

        private void OnClosed( object sender, EventArgs e )
        {
            this._ClosedEventHandlers.Send( sender, e );
        }

        /// <summary>
        /// Safely and synchronously Sends or invokes the <see cref="Closed">Closed Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        /// <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
        protected void SyncNotifyClosed( EventArgs e )
        {
            this._ClosedEventHandlers.Send( this, e );
        }

        #endregion

        #region " INITIALIZING "

        /// <summary> Allows taking actions before Initializing. </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void OnInitializing( System.ComponentModel.CancelEventArgs e )
        {
            if ( e is null )
            {
                throw new ArgumentNullException( nameof( e ) );
            }

            if ( e is object && !e.Cancel )
            {
                this.SyncNotifyInitializing( e );
            }
        }

        /// <summary> Removes the Initializing event handlers. </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        protected void RemoveInitializingEventHandlers()
        {
            this._InitializingEventHandlers?.RemoveAll();
        }

        /// <summary> The Initializing event handlers. </summary>
        private readonly EventHandlerContextCollection<System.ComponentModel.CancelEventArgs> _InitializingEventHandlers = new EventHandlerContextCollection<System.ComponentModel.CancelEventArgs>();

        /// <summary> Event queue for all listeners interested in Initializing events. </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event EventHandler<System.ComponentModel.CancelEventArgs> Initializing
        {
            add {
                this._InitializingEventHandlers.Add( new EventHandlerContext<System.ComponentModel.CancelEventArgs>( value ) );
            }

            remove {
                this._InitializingEventHandlers.RemoveValue( value );
            }
        }

        private void OnInitializing( object sender, System.ComponentModel.CancelEventArgs e )
        {
            this._InitializingEventHandlers.Send( sender, e );
        }

        /// <summary>
        /// Safely and synchronously Sends or invokes the <see cref="Initializing">Initializing Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        /// <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
        protected void SyncNotifyInitializing( System.ComponentModel.CancelEventArgs e )
        {
            this._InitializingEventHandlers.Send( this, e );
        }

        #endregion

        #region " INITIALIZED "

        /// <summary> True if is initialized, false if not. </summary>
        private bool _IsInitialized;

        /// <summary>
        /// Gets or sets the Initialized sentinel of the device. The device is ready after it is
        /// initialized.
        /// </summary>
        /// <value> <c>True</c> if hardware device is Initialized; <c>False</c> otherwise. </value>
        public virtual bool IsInitialized
        {
            get => this._IsInitialized;

            set {
                if ( !this.IsInitialized.Equals( value ) )
                {
                    this._IsInitialized = value;
                    this.NotifyPropertyChanged();
                    _ = this.PublishInfo( $"{this.ResourceNameCaption} {Interaction.IIf( this.IsInitialized, "initialized", "not ready" )}" );
                }
            }
        }

        /// <summary> Notifies of the Initialized event. </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void OnInitialized( EventArgs e )
        {
            this.IsInitialized = true;
            this.SyncNotifyInitialized( e );
        }

        /// <summary> Removes the Initialized event handlers. </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        protected void RemoveInitializedEventHandlers()
        {
            this._InitializedEventHandlers?.RemoveAll();
        }

        /// <summary> The Initialized event handlers. </summary>
        private readonly EventHandlerContextCollection<EventArgs> _InitializedEventHandlers = new EventHandlerContextCollection<EventArgs>();

        /// <summary> Event queue for all listeners interested in Initialized events. </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event EventHandler<EventArgs> Initialized
        {
            add {
                this._InitializedEventHandlers.Add( new EventHandlerContext<EventArgs>( value ) );
            }

            remove {
                this._InitializedEventHandlers.RemoveValue( value );
            }
        }

        private void OnInitialized( object sender, EventArgs e )
        {
            this._InitializedEventHandlers.Send( sender, e );
        }

        /// <summary>
        /// Safely and synchronously Sends or invokes the <see cref="Initialized">Initialized Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        /// <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
        protected void SyncNotifyInitialized( EventArgs e )
        {
            this._InitializedEventHandlers.Send( this, e );
        }

        #endregion

        #region " IDENTITY "

        /// <summary> The identity. </summary>
        private string _Identity = string.Empty;

        /// <summary> Gets or sets the Identity. </summary>
        /// <value> The Identity. </value>
        public string Identity
        {
            get => this._Identity;

            set {
                if ( !string.Equals( this.Identity, value ) )
                {
                    this._Identity = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " COMMANDS "

        /// <summary> Opens a resource. </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        /// <param name="resourceName">  The name of the resource. </param>
        /// <param name="resourceTitle"> The resource title. </param>
        public virtual void OpenResource( string resourceName, string resourceTitle )
        {
            if ( this.IsOpen )
            {
                this.ValidatedResourceName = resourceName;
                this.CandidateResourceNameValidated = string.Equals( this.ValidatedResourceName, this.CandidateResourceName, StringComparison.OrdinalIgnoreCase );
                this.OpenResourceName = resourceName;
                this.OpenResourceTitle = resourceTitle;
                this.CandidateResourceTitle = resourceTitle;
            }
        }

        /// <summary> Attempts to open resource from the given data. </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="resourceName">  The name of the resource. </param>
        /// <param name="resourceTitle"> The resource title. </param>
        /// <param name="e">             Action event information. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public virtual bool TryOpen( string resourceName, string resourceTitle, ActionEventArgs e )
        {
            if ( e is null )
            {
                throw new ArgumentNullException( nameof( e ) );
            }

            string activity = string.Empty;
            try
            {
                activity = $"opening {resourceTitle}:{resourceName}";
                _ = this.PublishInfo( $"{activity};. " );
                this.OpenResource( resourceName, resourceTitle );
            }
            catch ( Exception ex )
            {
                e.RegisterError( $"Exception {activity};. {ex.ToFullBlownString()}" );
                _ = this.Publish( e );
            }
            finally
            {
                if ( !e.Failed && !this.IsOpen )
                {
                    e.RegisterError( $"{activity} failed; resource not signaled as open" );
                }
            }

            return !e.Failed;
        }

        /// <summary> Attempts to open resource from the given data. </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        /// <param name="e"> Action event information. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public virtual bool TryOpen( ActionEventArgs e )
        {
            return this.TryOpen( this.ValidatedResourceName, this.CandidateResourceTitle, e );
        }

        /// <summary> Closes the resource. </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        public virtual void CloseResource()
        {
        }

        /// <summary> Attempts to close resource from the given data. </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Action event information. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public virtual bool TryClose( ActionEventArgs e )
        {
            if ( e is null )
            {
                throw new ArgumentNullException( nameof( e ) );
            }

            string activity = string.Empty;
            try
            {
                activity = $"Closing {this.ResourceNameCaption}";
                _ = this.PublishInfo( $"{activity};. " );
                this.CloseResource();
            }
            catch ( Exception ex )
            {
                e.RegisterError( $"Exception {activity};. {ex.ToFullBlownString()}" );
                _ = this.Publish( e );
            }
            finally
            {
                if ( !e.Failed && this.IsOpen )
                {
                    e.RegisterError( $"{activity} failed; resource not signaled as Close" );
                }
            }

            return !e.Failed;
        }

        /// <summary> Applies default settings and clears the resource active state. </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        public abstract void ClearActiveState();

        /// <summary> Attempts to clear active state. </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Action event information. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public virtual bool TryClearActiveState( ActionEventArgs e )
        {
            if ( e is null )
            {
                throw new ArgumentNullException( nameof( e ) );
            }

            string activity = string.Empty;
            try
            {
                activity = $"Clearing {this.ResourceNameCaption} active state";
                _ = this.PublishInfo( $"{activity};. " );
                this.ClearActiveState();
            }
            catch ( Exception ex )
            {
                e.RegisterError( $"Exception {activity};. {ex.ToFullBlownString()}" );
                _ = this.Publish( e );
            }

            return !e.Failed;
        }

        #endregion

    }
}
