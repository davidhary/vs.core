using System;
using System.Collections.Generic;
using System.Linq;

using isr.Core.Models.ExceptionExtensions;

namespace isr.Core.Models
{

    /// <summary> A resource selector view model base class. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-11 </para>
    /// </remarks>
    public abstract class SelectorViewModel : ViewModelTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Specialized default constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        protected SelectorViewModel() : base()
        {
            this.NewThis();
        }

        /// <summary> Specialized default constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        /// <param name="talker"> The talker. </param>
        protected SelectorViewModel( ITraceMessageTalker talker ) : base( talker )
        {
            this.NewThis();
        }

        /// <summary> Creates a new this. </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        private void NewThis()
        {
            this.ResourceNames = new System.ComponentModel.BindingList<string>();
            this._SearchImage = My.Resources.Resources.Find;
            this._IsOpen = false;
            this._Searchable = true;
        }

        #endregion

        #region " SEARCHABLE "

        /// <summary> True if searchable. </summary>
        private bool _Searchable;

        /// <summary>
        /// Gets or sets the condition determining if the control can be searchable. The elements can be
        /// searched only if not open.
        /// </summary>
        /// <value> The searchable. </value>
        public bool Searchable
        {
            get => this._Searchable;

            set {
                if ( !this.Searchable.Equals( value ) )
                {
                    this._Searchable = value;
                    this.SyncNotifyPropertyChanged();
                }
                this.SyncNotifyPropertyChanged( nameof( isr.Core.Models.SelectorViewModel.SearchEnabled ) );
            }
        }

        /// <summary> Gets search enabled state. </summary>
        /// <value> The search enabled state. </value>
        public bool SearchEnabled => this.Searchable && !this.IsOpen;

        /// <summary> True if is open, false if not. </summary>
        private bool _IsOpen;

        /// <summary> Gets or sets the open status. </summary>
        /// <remarks> the search should be disabled when the resource is open. </remarks>
        /// <value> The open status. </value>
        public bool IsOpen
        {
            get => this._IsOpen;

            set {
                if ( this.IsOpen != value )
                {
                    this._IsOpen = value;
                    this.SyncNotifyPropertyChanged();
                }
                this.SyncNotifyPropertyChanged( nameof( isr.Core.Models.SelectorViewModel.SearchEnabled ) );
            }
        }

        /// <summary> The search tool tip. </summary>
        private string _SearchToolTip;

        /// <summary> Gets or sets the Search tool tip. </summary>
        /// <value> The Search tool tip. </value>
        public string SearchToolTip
        {
            get => this._SearchToolTip;

            set {
                if ( !string.Equals( value, this.SearchToolTip ) )
                {
                    this._SearchToolTip = value;
                    this.SyncNotifyPropertyChanged();
                }
            }
        }

        /// <summary> The search image. </summary>
        private System.Drawing.Image _SearchImage;

        /// <summary> Gets or sets the Search Image. </summary>
        /// <value> The Search tool tip. </value>
        public System.Drawing.Image SearchImage
        {
            get => this._SearchImage;

            set {
                if ( !Equals( value, this.SearchImage ) )
                {
                    this._SearchImage = value;
                    this.SyncNotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " RESOURCE NAME "

        /// <summary> Name of the candidate resource. </summary>
        private string _CandidateResourceName;

        /// <summary> Gets or sets the name of the candidate resource. </summary>
        /// <value> The name of the candidate resource. </value>
        public string CandidateResourceName
        {
            get => this._CandidateResourceName;

            set {
                if ( string.IsNullOrWhiteSpace( value ) )
                {
                    value = string.Empty;
                }

                value = value.Trim();
                if ( !string.Equals( value, this.CandidateResourceName, StringComparison.OrdinalIgnoreCase ) )
                {
                    this._CandidateResourceName = value;
                    this.SyncNotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets the resource found tool tip. </summary>
        /// <value> The resource found tool tip. </value>
        public string ResourceFoundToolTip { get; set; } = "Resource found";

        /// <summary> Gets the resource not found tool tip. </summary>
        /// <value> The resource not found tool tip. </value>
        public string ResourceNotFoundToolTip { get; set; } = "Resource not found";

        /// <summary> Gets existence status of the resource. </summary>
        /// <value> The resource name exists. </value>
        public bool SelectedResourceExists => !string.IsNullOrWhiteSpace( this.ValidatedResourceName );

        /// <summary> Name of the validated resource. </summary>
        private string _ValidatedResourceName;

        /// <summary> Returns the validated resource name. </summary>
        /// <value> The name of the validated resource. </value>
        public string ValidatedResourceName
        {
            get => this._ValidatedResourceName;

            set {
                if ( string.IsNullOrWhiteSpace( value ) )
                {
                    value = string.Empty;
                }

                value = value.Trim();
                if ( !string.Equals( value, this.ValidatedResourceName, StringComparison.OrdinalIgnoreCase ) )
                {
                    this._ValidatedResourceName = value;
                    this.SyncNotifyPropertyChanged();
                    this.SearchToolTip = string.IsNullOrWhiteSpace( this.ValidatedResourceName ) ? this.ResourceFoundToolTip : this.ResourceNotFoundToolTip;
                }
            }
        }

        /// <summary> Queries resource exists. </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        /// <param name="resourceName"> Name of the resource. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public abstract bool QueryResourceExists( string resourceName );

        #endregion

        #region " VALIDATE RESOURCE  "

        /// <summary> True to enable, false to disable the validation. </summary>
        private bool _ValidationEnabled;

        /// <summary> Gets or sets Validation Enabled state. </summary>
        /// <remarks>
        /// Validation is disabled by default to facilitate resource selection in case of resource
        /// manager mismatch between VISA implementations.
        /// </remarks>
        /// <value> The Validation enabled. </value>
        public bool ValidationEnabled
        {
            get => this._ValidationEnabled;

            set {
                this._ValidationEnabled = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> Attempts to validate (e.g., check existence) the resource by name. </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        /// <param name="resourceName"> Name of the resource. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public abstract (bool Success, string Details) TryValidateResource( string resourceName );

        /// <summary> Used to notify of start of the validation of resource name. </summary>
        /// <remarks> David, 2020-07-20. </remarks>
        /// <param name="resourceName"> The name of the resource. </param>
        /// <param name="e">            Event information to send to registered event handlers. </param>
        protected virtual void OnValidatingResourceName( string resourceName, System.ComponentModel.CancelEventArgs e )
        {
            _ = this.PublishInfo( $"Validating {resourceName};. " );
        }

        /// <summary> Used to notify of validation of resource name. </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        /// <param name="resourceName"> The name of the resource. </param>
        /// <param name="e">            Action event information. </param>
        protected virtual void OnResourceNameValidated( string resourceName, EventArgs e )
        {
            _ = this.PublishInfo( $"{resourceName} validated;. " );
        }

        /// <summary> Attempts to validate resource name from the given data. </summary>
        /// <remarks> David, 2020-07-20. </remarks>
        /// <param name="resourceName">        The name of the resource. </param>
        /// <param name="applyEnabledFilters"> True to apply filters if enabled. </param>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public (bool Success, string Details) TryValidateResourceName( string resourceName, bool applyEnabledFilters )
        {
            (bool Success, string Details) result = (true, string.Empty);
            string activity = string.Empty;
            try
            {
                activity = this.PublishVerbose( $"Selecting resource name; checking conditions" );
                if ( string.IsNullOrWhiteSpace( resourceName ) )
                {
                    result = (false, $"{activity} canceled because name is empty");
                }
                else
                {
                    var args = new System.ComponentModel.CancelEventArgs();
                    activity = this.PublishVerbose( "Notifying 'Selecting' resource name" );
                    this.OnValidatingResourceName( resourceName, args );
                    if ( args.Cancel )
                    {
                        activity = this.PublishVerbose( "'Selecting' canceled" );
                    }
                    else
                    {
                        activity = this.PublishVerbose( "enumerating resource names" );
                        _ = this.EnumerateResources( applyEnabledFilters );
                        activity = this.PublishVerbose( $"trying to select resource '{resourceName}'" );
                        result = this.TryValidateResource( resourceName );
                        if ( result.Success )
                        {
                            this.OnResourceNameValidated( resourceName, EventArgs.Empty );
                        }
                    }
                }
            }
            catch ( Exception ex )
            {
                result = (false, $"Exception {activity};. {ex.ToFullBlownString()}");
            }
            finally
            {
            }

            if ( !result.Success )
            {
                _ = this.PublishWarning( result.Details );
            }

            return result;
        }

        #endregion

        #region " ENUMERATE RESOURCES "

        /// <summary> True if has resources, false if not. </summary>
        private bool _HasResources;

        /// <summary> Gets or sets the sentinel indication if resources were enumerated. </summary>
        /// <value> The has resources. </value>
        public bool HasResources
        {
            get => this._HasResources;

            set {
                if ( !this.HasResources.Equals( value ) )
                {
                    this._HasResources = value;
                    // using sync notification is required when using unit tests; otherwise,
                    // actions invoked by the event may not occur.
                    this.SyncNotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the resource names. </summary>
        /// <value> The resource names. </value>
        public System.ComponentModel.BindingList<string> ResourceNames { get; private set; }

        /// <summary> Enumerate resource names. </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        /// <param name="names"> The names. </param>
        /// <returns>
        /// A =<see cref="System.ComponentModel.BindingList{T}">binding list</see>
        /// </returns>
        public System.ComponentModel.BindingList<string> EnumerateResources( string[] names )
        {
            // suspend binding while building the resource names
            this.ResourceNames.RaiseListChangedEvents = false;
            this.ResourceNames.Clear();
            if ( names?.Any() == true )
            {
                foreach ( string name in names )
                {
                    this.ResourceNames.Add( name );
                }
            }
            // resume and reset binding 
            this.ResourceNames.RaiseListChangedEvents = true;
            this.ResourceNames.ResetBindings();
            this.SyncNotifyPropertyChanged( nameof( isr.Core.Models.SelectorViewModel.ResourceNames ) );
            this.HasResources = this.ResourceNames.Any();
            return this.ResourceNames;
        }

        /// <summary> Enumerate resource names. </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        /// <param name="names"> The names. </param>
        /// <returns>
        /// A =<see cref="System.ComponentModel.BindingList{T}">binding list</see>
        /// </returns>
        public System.ComponentModel.BindingList<string> EnumerateResources( IEnumerable<string> names )
        {
            // suspend binding while building the resource names
            this.ResourceNames.RaiseListChangedEvents = false;
            this.ResourceNames.Clear();
            if ( names?.Any() == true )
            {
                foreach ( string name in names )
                {
                    this.ResourceNames.Add( name );
                }
            }
            // resume and reset binding 
            this.ResourceNames.RaiseListChangedEvents = true;
            this.ResourceNames.ResetBindings();
            this.SyncNotifyPropertyChanged( nameof( isr.Core.Models.SelectorViewModel.ResourceNames ) );
            this.HasResources = this.ResourceNames.Any();
            return this.ResourceNames;
        }

        /// <summary> Enumerate resource names. </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        /// <param name="names"> The names. </param>
        /// <returns>
        /// A =<see cref="System.ComponentModel.BindingList{T}">binding list</see>
        /// </returns>
        public System.ComponentModel.BindingList<string> EnumerateResources( IList<string> names )
        {
            // suspend binding while building the resource names
            this.ResourceNames.RaiseListChangedEvents = false;
            this.ResourceNames.Clear();
            if ( names?.Any() == true )
            {
                foreach ( string name in names )
                {
                    this.ResourceNames.Add( name );
                }
            }
            // resume and reset binding 
            this.ResourceNames.RaiseListChangedEvents = true;
            this.ResourceNames.ResetBindings();
            this.SyncNotifyPropertyChanged( nameof( isr.Core.Models.SelectorViewModel.ResourceNames ) );
            this.HasResources = this.ResourceNames.Any();
            return this.ResourceNames;
        }

        /// <summary> Enumerate resource names. </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        /// <param name="applyEnabledFilters"> True to apply filters if enabled. </param>
        /// <returns> A list of <see cref="System.String"/>. </returns>
        public abstract System.ComponentModel.BindingList<string> EnumerateResources( bool applyEnabledFilters );

        #endregion

        #region " VALIDATE RESOURCE NAME ASYNC TASK "

        /// <summary> Gets or sets the default resource name validation task timeout. </summary>
        /// <value> The default resource name validation timeout. </value>
        public static TimeSpan DefaultResourceNameValidationTaskTimeout { get; set; } = TimeSpan.FromSeconds( 5d );

        /// <summary> Gets or sets the resource name validation tasker. </summary>
        /// <value> The resource name validation tasker. </value>
        public Tasker<(bool Success, string Details)> ResourceNameValidationTasker { get; private set; }

        /// <summary> Starts task validating resource name. </summary>
        /// <remarks> David, 2020-07-20. </remarks>
        /// <param name="resourceName">        The name of the resource. </param>
        /// <param name="applyEnabledFilters"> True to apply filters if enabled. </param>
        public void StartTaskValidatingResourceName( string resourceName, bool applyEnabledFilters )
        {
            this.ResourceNameValidationTasker = new Tasker<(bool Success, string Details)>();
            this.ResourceNameValidationTasker.StartAction( () => this.TryValidateResourceName( resourceName, applyEnabledFilters ) );
        }

        /// <summary> Await resource name selection. </summary>
        /// <remarks> David, 2020-08-05. </remarks>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        public (bool Success, string Details) AwaitResourceNameSelection()
        {
            try
            {
                var (Status, Result) = this.ResourceNameValidationTasker.AwaitCompletion( DefaultResourceNameValidationTaskTimeout );
                return System.Threading.Tasks.TaskStatus.RanToCompletion == Status ?
                            (true, "buffer streaming completed") :
                            (false, $"resource name selection task existed with a {Status} status");
            }
            catch ( Exception ex )
            {
                return (false, ex.ToFullBlownString());
            }
        }

        #endregion

    }
}
