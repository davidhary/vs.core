using System;

namespace isr.Core.Models
{
    /// <summary> Comparable, equatable and formattable value Notifier. </summary>
    /// <remarks>
    /// This single-value class can be used to raise property change events. This could be used, for
    /// example, to report a formattable progress. <para>
    /// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-10-10 </para>
    /// </remarks>
    public class ValueNotifier<T> : ViewModelBase where T : IComparable<T>, IEquatable<T>, IFormattable
    {

        /// <summary> Default constructor. </summary>   
        /// <remarks> David, 2020-09-21. </remarks>
        public ValueNotifier() : base()
        {
        }

        /// <summary> The value. </summary>
        private T _Value;

        /// <summary> Gets or sets the value. </summary>
        /// <value> The value. </value>
        public T Value
        {
            get => this._Value;

            set {
                if ( !value.Equals( this.Value ) )
                {
                    this._Value = value;
                    this.AsyncNotifyPropertyChanged();
                    ApplianceBase.DoEvents();
                }
            }
        }

    }
}
