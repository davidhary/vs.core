
namespace isr.Core.Models
{
    /// <summary> Defines the contract that must be implemented by View Models. </summary>
    /// <remarks>
    /// <list type="bullet">View Model Notify implementation: <item>
    /// Notify Function calls Synchronization Context Post;</item><item>
    /// Async Notify Function calls Synchronization Context Post;</item><item>
    /// Sync Notify function calls Synchronization Context Send;</item><item>
    /// Raise event (custom implementation) calls Synchronization Context Post;</item><item>
    /// Notify is used as the default in property set functions.</item></list> <para>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-10. Created from property notifiers
    /// </para>
    /// </remarks>
    public abstract partial class ViewModelTalkerBase : ViewModelBase
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModelTalkerBase" /> class.
        /// </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        protected ViewModelTalkerBase() : base()
        {
            this._Talker = new TraceMessageTalker();
            this._IsAssignedTalker = false;
        }

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        /// <param name="talker"> The talker. </param>
        protected ViewModelTalkerBase( ITraceMessageTalker talker ) : this()
        {
            this._Talker = talker;
            this._IsAssignedTalker = true;
        }

    }
}
