using System.Collections.Specialized;
using System.ComponentModel;

namespace isr.Core.Models
{

    /// <summary> A collection changed base. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-05-22 </para>
    /// </remarks>
    public class CollectionChangedBase : ViewModelBase, INotifyCollectionChanged, INotifyPropertyChanged
    {

        /// <summary> Removes the Collection Changed event handlers. </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        protected void RemoveCollectionChangedEventHandler()
        {
            this.CollectionChangedHandlers.RemoveAll();
        }

        /// <summary> The collection changed handlers. </summary>
#pragma warning disable IDE1006 // Naming Styles
        private readonly NotifyCollectionChangedEventContextCollection CollectionChangedHandlers = new NotifyCollectionChangedEventContextCollection();
#pragma warning restore IDE1006 // Naming Styles

        /// <summary> Event queue for all listeners interested in Collection Changed events. </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event NotifyCollectionChangedEventHandler CollectionChanged
        {
            add {
                this.CollectionChangedHandlers.Add( new NotifyCollectionChangedEventContext( value ) );
            }

            remove {
                this.CollectionChangedHandlers.RemoveValue( value );
            }
        }

        private void OnCollectionChanged( object sender, NotifyCollectionChangedEventArgs e )
        {
            this.CollectionChangedHandlers.Raise( sender, e );
        }

        #region " NOTIFY "

        /// <summary>
        /// Asynchronously notifies (posts) collection change on a different thread. Unsafe for cross
        /// threading; fast return of control to the invoking function.
        /// </summary>
        /// <remarks> David, 2020-09-21. </remarks>
        /// <param name="e"> Collection Changed event information. </param>
        protected virtual void NotifyCollectionChanged( NotifyCollectionChangedEventArgs e )
        {
            this.CollectionChangedHandlers.Post( this, e );
        }

        /// <summary>
        /// Asynchronously notifies (posts) collection change on a different thread. Unsafe for cross
        /// threading; fast return of control to the invoking function.
        /// </summary>
        /// <remarks>
        /// Overhead of this method is 7 to 10 times larger than naked raise event. This has no advantage
        /// even with slow handler functions.
        /// </remarks>
        /// <param name="e"> Collection Changed event information. </param>
        protected virtual void AsyncNotifyCollectionChanged( NotifyCollectionChangedEventArgs e )
        {
            this.CollectionChangedHandlers.Post( this, e );
        }

        /// <summary>
        /// Synchronously notifies (send) collection change on a different thread. Safe for cross
        /// threading.
        /// </summary>
        /// <remarks>
        /// Overhead of this method is 3 to 4 times larger than naked raise event. This is the best
        /// approach.
        /// </remarks>
        /// <param name="e"> Collection Changed event information. </param>
        protected virtual void SyncNotifyCollectionChanged( NotifyCollectionChangedEventArgs e )
        {
            this.CollectionChangedHandlers.Send( this, e );
        }

        #endregion

    }
}
