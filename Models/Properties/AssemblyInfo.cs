using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.Core.Models.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.Core.Models.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.Core.Models.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]

[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( isr.Core.Models.My.MyLibrary.TestAssemblyStrongName )]
