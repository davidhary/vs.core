Imports isr.Core.LinqStatistics.EnumerableStats

''' <summary> A variance tests. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 4/13/2018 </para>
''' </remarks>
<TestClass>
Public Class VarianceTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext
    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

    ''' <summary> (Unit Test Method) variable double. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod>
    Public Sub VarDouble()
        Dim source As IEnumerable(Of Double) = TestData.GetDoubles()

        Dim result As Double = source.Variance()

        Assert.AreEqual(result, 4.1091666666666669, Double.Epsilon)
    End Sub

    ''' <summary> (Unit Test Method) variable nullable double. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod>
    Public Sub VarNullableDouble()
        Dim source As IEnumerable(Of Double?) = TestData.GetNullableDoubles()

        Dim result? As Double = source.Variance()

        Assert.AreEqual(CDbl(result), 4.1091666666666669, Double.Epsilon)
    End Sub

    ''' <summary> (Unit Test Method) variable int. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod>
    Public Sub VarInt()
        Dim source As IEnumerable(Of Integer) = TestData.GetInts()

        Dim result As Double = source.Variance()

        Assert.AreEqual(result, 2.9166666666666665, Double.Epsilon)
    End Sub

    ''' <summary> (Unit Test Method) variable nullable int. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod>
    Public Sub VarNullableInt()
        Dim source As IEnumerable(Of Integer?) = TestData.GetNullableInts()

        Dim result? As Double = source.Variance()

        Assert.AreEqual(CDbl(result), 2.9166666666666665, Double.Epsilon)
    End Sub

    ''' <summary> (Unit Test Method) variable p double. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod>
    Public Sub VarPDouble()
        Dim source As IEnumerable(Of Double) = TestData.GetDoubles()

        Dim result As Double = source.PopulationVariance()

        Assert.AreEqual(result, 3.081875, Double.Epsilon)
    End Sub

    ''' <summary> (Unit Test Method) variable p nullable double. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod>
    Public Sub VarPNullableDouble()
        Dim source As IEnumerable(Of Double?) = TestData.GetNullableDoubles()

        Dim result? As Double = source.PopulationVariance()

        Assert.AreEqual(CDbl(result), 3.081875, Double.Epsilon)
    End Sub

    ''' <summary> (Unit Test Method) variable p int. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId:="PInt")>
    <TestMethod>
    Public Sub VarPInt()
        Dim source As IEnumerable(Of Integer) = TestData.GetInts()

        Dim result As Double = source.PopulationVariance()

        Assert.AreEqual(result, 2.1875, Double.Epsilon)
    End Sub

    ''' <summary> (Unit Test Method) variable p nullable int. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod>
    Public Sub VarPNullableInt()
        Dim source As IEnumerable(Of Integer?) = TestData.GetNullableInts()

        Dim result? As Double = source.PopulationVariance()

        Assert.AreEqual(CDbl(result), 2.1875, Double.Epsilon)
    End Sub

End Class
