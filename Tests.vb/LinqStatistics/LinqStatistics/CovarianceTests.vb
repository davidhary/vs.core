Imports isr.Core.LinqStatistics.EnumerableStats

''' <summary> A covariance tests. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 4/13/2018 </para>
''' </remarks>
<TestClass>
Public Class CovarianceTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext
    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

    ''' <summary> (Unit Test Method) covariances this object. </summary>
    ''' <remarks>
    ''' Covariance 1.4486ms Using IEnumerable from Double()
    ''' Covariance 0.0017ms Using IEnumerable from Double()
    ''' Covariance 0.0393ms Using IList.
    ''' </remarks>
    <TestMethod>
    Public Sub Covariance()
        Dim source As IEnumerable(Of Double) = TestData.GetDoubles()
        Dim other As IEnumerable(Of Double) = TestData.GetDoubles()
        Dim sw As Stopwatch = Stopwatch.StartNew
        Dim result As Double = source.Covariance(other)
        Dim elapsed As TimeSpan = sw.Elapsed
        TestInfo.TraceMessage($"{NameOf(CovarianceTests.Covariance)} {elapsed.TotalMilliseconds}ms Using {NameOf(IEnumerable(Of Double))}")
        Assert.AreEqual(result, 3.081875, Double.Epsilon)

        sw.Restart()
        result = source.Covariance(other)
        elapsed = sw.Elapsed
        TestInfo.TraceMessage($"{NameOf(CovarianceTests.Covariance)} {elapsed.TotalMilliseconds}ms Using {NameOf(IEnumerable(Of Double))} from Double()")
        Assert.AreEqual(result, 3.081875, Double.Epsilon)

        Dim sourceList As IList(Of Double) = TestData.GetDoubles().ToList
        Dim otherList As IList(Of Double) = TestData.GetDoubles().ToList
        sw.Restart()
        result = sourceList.Covariance(otherList)
        elapsed = sw.Elapsed
        TestInfo.TraceMessage($"{NameOf(CovarianceTests.Covariance)} {elapsed.TotalMilliseconds}ms Using {NameOf(IList(Of Double))}")
        Assert.AreEqual(result, 3.081875, Double.Epsilon)
    End Sub

    ''' <summary> (Unit Test Method) covariance 1. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod>
    Public Sub Covariance1()
        Dim source As IEnumerable(Of Double) = TestData.GetDoubles()
        Dim other As IEnumerable(Of Double) = TestData.GetInts().Select(Function(x) CDbl(x))
        Dim result As Double = source.Covariance(other)
        Assert.AreEqual(result, 2.59375, Double.Epsilon)
    End Sub

    ''' <summary> (Unit Test Method) pearson identity. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod>
    Public Sub PearsonIdentity()
        Dim source As IEnumerable(Of Double) = TestData.GetDoubles()
        Dim other As IEnumerable(Of Double) = TestData.GetDoubles()
        Dim result As Double = source.Pearson(other)
        Assert.AreEqual(result, 1.0, Double.Epsilon)
    End Sub

    ''' <summary> (Unit Test Method) pearson 1. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod>
    Public Sub Pearson1()
        Dim source As IEnumerable(Of Double) = TestData.GetDoubles()
        Dim other As IEnumerable(Of Double) = TestData.GetInts().Select(Function(x) CDbl(x))
        Dim result As Double = source.Pearson(other)
        Assert.AreEqual(result, 0.998956491208287, 0.000000000000001)
    End Sub
End Class
