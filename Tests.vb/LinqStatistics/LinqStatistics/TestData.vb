Imports isr.Core.RandomExtensions

''' <summary>
''' Constructor that prevents a default instance of this class from being created.
''' </summary>
''' <remarks> David, 2020-09-23. </remarks>
Friend NotInheritable Class TestData

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Private Sub New()
    End Sub

    ''' <summary> Gets the ints in this collection. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the ints in this collection.
    ''' </returns>
    Public Shared Function GetInts() As IEnumerable(Of Integer)
        Return New Integer() {2, 3, 4, 6}
    End Function

    ''' <summary> Gets the nullable ints in this collection. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the nullable ints in this collection.
    ''' </returns>
    Public Shared Function GetNullableInts() As IEnumerable(Of Integer?)
        Return New Integer?() {2, Nothing, 3, 4, 6}
    End Function

    ''' <summary> Gets the doubles in this collection. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the doubles in this collection.
    ''' </returns>
    Public Shared Function GetDoubles() As IEnumerable(Of Double)
        Return New Double() {2.1, 3.5, 4.6, 6.9}
    End Function

    ''' <summary> Gets the nullable doubles in this collection. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the nullable doubles in this
    ''' collection.
    ''' </returns>
    Public Shared Function GetNullableDoubles() As IEnumerable(Of Double?)
        Return New Double?() {2.1, 3.5, Nothing, 4.6, 6.9}
    End Function

    ''' <summary> Gets the floats in this collection. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the floats in this collection.
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared Function GetFloats() As IEnumerable(Of Single)
        Return New Single() {2.1F, 3.5F, 4.6F, 6.9F}
    End Function

    ''' <summary> Gets the random normal doubles in this collection. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="count"> Number of. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the random normal doubles in this
    ''' collection.
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared Function GetRandomNormalDoubles(ByVal count As Integer) As IEnumerable(Of Double)
        Return TestData.GetRandomNormalDoubles(New Random(), count)
    End Function

    ''' <summary> Gets the random normal doubles in this collection. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="seed">  The seed. </param>
    ''' <param name="count"> Number of. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the random normal doubles in this
    ''' collection.
    ''' </returns>
    Public Shared Function GetRandomNormalDoubles(ByVal seed As Integer, ByVal count As Integer) As IEnumerable(Of Double)
        Return TestData.GetRandomNormalDoubles(New Random(seed), count)
    End Function

    ''' <summary> Gets the random normal doubles in this collection. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="rnd">   The random. </param>
    ''' <param name="count"> Number of. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the random normal doubles in this
    ''' collection.
    ''' </returns>
    Public Shared Function GetRandomNormalDoubles(rnd As Random, ByVal count As Integer) As IEnumerable(Of Double)
        Dim l As New List(Of Double)
        For i As Integer = 1 To count
            l.Add(rnd.NextNormal)
        Next
        Return l.ToArray
    End Function
End Class
