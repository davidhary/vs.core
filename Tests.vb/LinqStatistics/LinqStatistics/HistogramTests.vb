﻿Imports System.Windows

Imports isr.Core.LinqStatistics.EnumerableStats

''' <summary> Summary description for HistogramTests. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 4/13/2018 </para>
''' </remarks>
<TestClass>
Public Class HistogramTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext
    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

    ''' <summary> (Unit Test Method) tests histogram. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId:="dummyResult")>
    <TestMethod>
    Public Sub HistogramTest()
        Dim useLinq As Boolean = False
        ' use a fixed seed to get a predictable random.
        Dim source As IEnumerable(Of Double) = TestData.GetRandomNormalDoubles(1, 10000)
        Dim lowerLimit As Double = -2
        Dim upperLimit As Double = 2
        Dim count As Integer = 20
        Dim binWidth As Double = (upperLimit - lowerLimit) / count
        Dim sw As Stopwatch = Stopwatch.StartNew
        Dim result As IEnumerable(Of Point)
        Dim linqSpeed As Long
        Dim directSpeed As Long

        If useLinq Then
            result = source.Histogram(lowerLimit, upperLimit, count)
            linqSpeed = sw.ElapsedTicks
            sw.Restart()
            Dim dummyResult As IEnumerable(Of Point) = source.HistogramDirect(lowerLimit, upperLimit, count)
            directSpeed = sw.ElapsedTicks
        Else
            Dim dummyResult As IEnumerable(Of Point) = source.Histogram(lowerLimit, upperLimit, count)
            linqSpeed = sw.ElapsedTicks
            sw.Restart()
            result = source.HistogramDirect(lowerLimit, upperLimit, count)
            directSpeed = sw.ElapsedTicks
        End If
        ' Linq speed 33235 could be over three times longer than direct 9143 speed.
        Assert.IsTrue(directSpeed < linqSpeed, $"Expected direct speed {directSpeed} to be lower than linq speed {linqSpeed}")

        ' count test: There are two extra bins above the high and below the low limits.
        Assert.AreEqual(result.Count, count + 2)

        ' abscissa range test: First bin is at the low limit; last is at the high limit.
        Assert.AreEqual(lowerLimit, result(0).X, 0.1 * binWidth)
        Assert.AreEqual(upperLimit, result(count + 1).X, 0.1 * binWidth)

        ' Second bin is at half the bin width past the low limit; last off by half bin width of the last bin.
        Assert.AreEqual(lowerLimit + 0.5 * binWidth, result(1).X, 0.1 * binWidth)
        Assert.AreEqual(upperLimit - 0.5 * binWidth, result(count).X, 0.1 * binWidth)

        ' expected value assuming random returns the same values each time.
        Dim expectedLowCount As Integer = 208
        Assert.AreEqual(expectedLowCount, CInt(result(0).Y))

        Dim expectedHighCount As Integer = 230
        Assert.AreEqual(expectedHighCount, CInt(result(count + 1).Y))
    End Sub

End Class

