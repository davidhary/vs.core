﻿Imports isr.Core.LinqStatistics.EnumerableStats

''' <summary> A population standard deviation tests. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 4/13/2018 </para>
''' </remarks>
<TestClass>
Public Class PopulationStandardDeviationTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext
    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

    ''' <summary> (Unit Test Method) sigma double. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod>
    Public Sub SigmaDouble()
        Dim source As IEnumerable(Of Double) = TestData.GetDoubles()

        Dim result As Double = source.PopulationStandardDeviation()

        Assert.AreEqual(1.75552698640608, result, 0.000000000001)
    End Sub

    ''' <summary> (Unit Test Method) sigma nullable double. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod>
    Public Sub SigmaNullableDouble()
        Dim source As IEnumerable(Of Double?) = TestData.GetNullableDoubles()

        Dim result? As Double = source.PopulationStandardDeviation()

        Assert.AreEqual(1.75552698640608, CDbl(result), 0.000000000001)
    End Sub

    ''' <summary> (Unit Test Method) sigma int. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod>
    Public Sub SigmaInt()
        Dim source As IEnumerable(Of Integer) = TestData.GetInts()

        Dim result As Double = source.PopulationStandardDeviation()

        Assert.AreEqual(1.4790199457749, result, 0.000000000001)
    End Sub

    ''' <summary> (Unit Test Method) sigma nullable int. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod>
    Public Sub SigmaNullableInt()
        Dim source As IEnumerable(Of Integer?) = TestData.GetNullableInts()

        Dim result? As Double = source.PopulationStandardDeviation()

        Assert.AreEqual(1.4790199457749, CDbl(result), 0.000000000001)
    End Sub

    ''' <summary> (Unit Test Method) sigma population double. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod>
    Public Sub SigmaPopulationDouble()
        Dim source As IEnumerable(Of Double) = TestData.GetDoubles()

        Dim result As Double = source.PopulationStandardDeviation()

        Assert.AreEqual(1.7555269864060763, result, 0.000000000001)
    End Sub

    ''' <summary> (Unit Test Method) sigma population nullable double. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod>
    Public Sub SigmaPopulationNullableDouble()
        Dim source As IEnumerable(Of Double?) = TestData.GetNullableDoubles()

        Dim result? As Double = source.PopulationStandardDeviation()

        Assert.AreEqual(1.7555269864060763, CDbl(result), 0.000000000001)
    End Sub

    ''' <summary> (Unit Test Method) sigma population int. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod>
    Public Sub SigmaPopulationInt()
        Dim source As IEnumerable(Of Integer) = TestData.GetInts()

        Dim result As Double = source.PopulationStandardDeviation()

        Assert.AreEqual(1.479019945774904, result, 0.000000000001)
    End Sub

    ''' <summary> (Unit Test Method) sigma population nullable int. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod>
    Public Sub SigmaPopulationNullableInt()
        Dim source As IEnumerable(Of Integer?) = TestData.GetNullableInts()

        Dim result? As Double = source.PopulationStandardDeviation()

        Assert.AreEqual(1.479019945774904, CDbl(result), 0.000000000001)
    End Sub
End Class
