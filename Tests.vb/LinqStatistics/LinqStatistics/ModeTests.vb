﻿Imports isr.Core.LinqStatistics.EnumerableStats

''' <summary> A mode tests. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 4/13/2018 </para>
''' </remarks>
<TestClass>
Public Class ModeTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext
    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

    ''' <summary> (Unit Test Method) mode uniform. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod>
    Public Sub ModeUniform()
        Dim source As IEnumerable(Of Integer) = New Integer() {1, 1, 1}

        Dim result? As Integer = source.Mode()
        Assert.IsTrue(result.GetValueOrDefault(0) = 1)
    End Sub

    ''' <summary> (Unit Test Method) mode none. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod>
    Public Sub ModeNone()
        Dim source As IEnumerable(Of Integer) = New Integer() {1, 2, 3}

        Dim result? As Integer = source.Mode()
        Assert.IsFalse(result.HasValue)
    End Sub

    ''' <summary> (Unit Test Method) mode one. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod>
    Public Sub ModeOne()
        Dim source As IEnumerable(Of Integer) = New Integer() {1, 2, 2, 3}

        Dim result? As Integer = source.Mode()
        Assert.IsTrue(result.GetValueOrDefault(0) = 2)
    End Sub

    ''' <summary> (Unit Test Method) mode two. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod>
    Public Sub ModeTwo()
        Dim source As IEnumerable(Of Integer) = New Integer() {1, 2, 2, 3, 3, 3}

        Dim result? As Integer = source.Mode()
        Assert.IsTrue(result.GetValueOrDefault(0) = 3)
    End Sub

    ''' <summary> (Unit Test Method) mode three. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod>
    Public Sub ModeThree()
        Dim source As IEnumerable(Of Integer) = New Integer() {1, 2, 2, 3, 3}

        Dim result? As Integer = source.Mode()
        Assert.IsTrue(result.GetValueOrDefault(0) = 2)
    End Sub

    ''' <summary> (Unit Test Method) mode nullable. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod>
    Public Sub ModeNullable()
        Dim source As IEnumerable(Of Integer?) = New Integer?() {1, 3, 2, 3, Nothing, 2, 3}

        Dim result? As Integer = source.Mode()
        Assert.IsTrue(result.GetValueOrDefault(0) = 3)
    End Sub

    ''' <summary> (Unit Test Method) mode multiple. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod>
    Public Sub ModeMultiple()
        Dim source As IEnumerable(Of Integer) = New Integer() {1, 3, 2, 2, 3}

        Dim result As IEnumerable(Of Integer) = source.Modes()
        Assert.IsTrue(result.SequenceEqual(New Integer() {2, 3}))
    End Sub

    ''' <summary> (Unit Test Method) modes multiple 2. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod>
    Public Sub ModesMultiple2()
        Dim source As IEnumerable(Of Integer) = New Integer() {1, 3, 2, 2, 3, 3}

        Dim result As IEnumerable(Of Integer) = source.Modes()
        Assert.IsTrue(result.SequenceEqual(New Integer() {3, 2}))
    End Sub

    ''' <summary> (Unit Test Method) modes multiple nullable. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod>
    Public Sub ModesMultipleNullable()
        Dim source As IEnumerable(Of Integer?) = New Integer?() {1, 2, Nothing, 2, 3, 3, 3}

        Dim result As IEnumerable(Of Integer) = source.Modes()
        Assert.IsTrue(result.SequenceEqual(New Integer() {3, 2}))
    End Sub

    ''' <summary> (Unit Test Method) modes single value. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod>
    Public Sub ModesSingleValue()
        Dim source As IEnumerable(Of Integer) = New Integer() {1}

        Dim result? As Integer = source.Mode()
        Assert.IsTrue(result Is Nothing)
    End Sub
End Class
