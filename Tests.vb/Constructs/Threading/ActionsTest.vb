Imports System.Threading

Imports isr.Core.Constructs

''' <summary> Functions tests. </summary>
''' <remarks>
''' David, 2/3/2019, https://github.com/polterguy/lizzie/. <para>
''' Copyright (c) 2018 Thomas Hansen - thomas@gaiasoul.com </para><para>
''' Licensed under The MIT License.</para>
''' </remarks>
<TestClass()>
Public Class ActionsTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.Services.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext
    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " EXECUTE TESTS "

    ''' <summary> (Unit Test Method) executes this object. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    <TestMethod()>
    Public Sub Execute()
        Dim result As String = String.Empty
        Dim actions As New Actions From {
                Sub() result &= "foo",
                Sub() result &= "bar"
            }
        actions.Execute()
        Assert.AreEqual("foobar", result)
    End Sub

    ''' <summary> (Unit Test Method) executes the arguments operation. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    <TestMethod()>
    Public Sub ExecuteArgs()
        Dim sequence As New Actions(Of Mutable(Of String)) From {
                Sub(arg) arg.Value &= "foo",
                Sub(arg) arg.Value &= "bar"
            }

        Dim mutable As New Mutable(Of String)("initial_")
        sequence.Execute(mutable)
        Assert.AreEqual("initial_foobar", mutable.Value)
    End Sub

    ''' <summary> Executes the parallel operation. </summary>
    ''' <remarks>
    ''' Creates two lambdas that run in parallel. If you look carefully at this code, you’ll notice
    ''' that the result of the evaluation doesn’t assume that any one of my lambdas is being executed
    ''' before the other. The order of execution is not explicitly specified, and these lambdas are
    ''' being executed on separate threads. This is because the Actions class in Figure 3lets you add
    ''' delegates to it, so you can later decide if you want to execute the delegates in parallel or
    ''' sequentially.
    ''' </remarks>
    ''' <param name="initialValue"> The initial value. </param>
    ''' <param name="oneValue">     The one value. </param>
    ''' <param name="otherValue">   The other value. </param>
    Private Shared Sub ExecuteParallel(ByVal initialValue As String, ByVal oneValue As String, ByVal otherValue As String)
        Dim result As String = String.Empty
        Using sync As New Synchronizer(Of String, String, String)(initialValue)
            Dim actions As New Actions From {Sub() sync.Assign(Function(res) res & oneValue),
                                                 Sub() sync.Assign(Function(res) res & otherValue)}
            actions.ExecuteParallel()
            sync.Read(Sub(val As String) result = val)
        End Using
        Assert.AreEqual(True, $"{initialValue}{oneValue}{otherValue}" = result OrElse result = $"{initialValue}{otherValue}{oneValue}")
    End Sub

    ''' <summary> (Unit Test Method) executes the parallel test 1 operation. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    <TestMethod()>
    Public Sub ExecuteParallelTest1()
        ActionsTests.ExecuteParallel("initial_", "foot", "bar")
        Using sync As New Synchronizer(Of String, String, String)("initial_")
            Dim actions As New Actions From {
                    Sub() sync.Assign(Function(res) res & "foo"),
                    Sub() sync.Assign(Function(res) res & "bar")
                }
            actions.ExecuteParallel()
            Dim result As String = Nothing
            sync.Read(Sub(val As String) result = val)
            Assert.AreEqual(True, "initial_foobar" = result OrElse result = "initial_barfoo")
        End Using
    End Sub

    ''' <summary> (Unit Test Method) executes the parallel test 2 operation. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    <TestMethod()>
    Public Sub ExecuteParallelTest2()
        Dim result As String = String.Empty
        Dim wait As New ManualResetEvent(False)
        Dim actions As New Actions From {
                Sub()
                    result &= "foo"
                    wait.Set()
                End Sub,
                Sub()
                    wait.WaitOne()
                    result &= "bar"
                End Sub
            }
        actions.ExecuteParallel()
        Assert.AreEqual("foobar", result)
    End Sub

    ''' <summary> (Unit Test Method) executes the parallel test 3 operation. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    <TestMethod()>
    Public Sub ExecuteParallelTest3()
        Dim result As String = String.Empty
        Dim wait As New ManualResetEvent(False)

        Dim actions As New Actions From {
                Sub()
                    wait.WaitOne()
                    result &= "foo"
                End Sub,
                Sub()
                    result &= "bar"
                    wait.Set()
                End Sub
            }

        actions.ExecuteParallel()
        Assert.AreEqual("barfoo", result)
    End Sub

    ''' <summary> (Unit Test Method) executes the parallel unblocked operation. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    <TestMethod()>
    Public Sub ExecuteParallelUnblocked()
        Dim result As String = String.Empty
        Dim waits As ManualResetEvent() = New ManualResetEvent() {New ManualResetEvent(False),
                                                                      New ManualResetEvent(False),
                                                                      New ManualResetEvent(False)}
        Dim actions As New Actions From {
                Sub()
                    result &= "1"
                    waits(0).Set()
                End Sub,
                Sub()
                    result &= "2"
                    waits(1).Set()
                End Sub,
                Sub()
                    result &= "3"
                    waits(2).Set()
                End Sub
            }

        actions.ExecuteParallelUnblocked()
        For Each e As ManualResetEvent In waits
            e.WaitOne()
        Next
        ' this fails on STA threads: WaitHandle.WaitAll(waits)
        Dim outcome As Boolean = result = "123" OrElse result = "132" OrElse
                                     result = "231" OrElse result = "213" OrElse
                                     result = "312" OrElse result = "321"
        Assert.AreEqual(True, outcome, $"{result} unexpected")
    End Sub

    ''' <summary> (Unit Test Method) executes the parallel unblocked argument operation. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    <TestMethod()>
    Public Sub ExecuteParallelUnblockedArg()
        Dim waits As EventWaitHandle() = New EventWaitHandle() {New EventWaitHandle(False, EventResetMode.ManualReset), New EventWaitHandle(False, EventResetMode.ManualReset), New EventWaitHandle(False, EventResetMode.ManualReset)}
        Dim actions As New Actions(Of Synchronizer(Of String)) From {
                Sub(input As Synchronizer(Of String))
                    waits(1).WaitOne()
                    input.Assign(Function(current) current & "1")
                    waits(0).Set()
                End Sub,
                Sub(input As Synchronizer(Of String))
                    waits(2).WaitOne()
                    input.Assign(Function(current) current & "2")
                    waits(1).Set()
                End Sub,
                Sub(input As Synchronizer(Of String))
                    input.Assign(Function(current) current & "3")
                    waits(2).Set()
                End Sub
            }
        Using sync As New Synchronizer(Of String)("initial_")
            actions.ExecuteParallelUnblocked(sync)
            For Each e As EventWaitHandle In waits
                e.WaitOne()
            Next
            ' this fails on STA threads: WaitHandle.WaitAll(waits)
            Dim res As String = Nothing
            sync.Read(Sub(val As String) res = val)
            Assert.AreEqual("initial_321", res)
        End Using
    End Sub
#End Region

End Class
