Imports isr.Core.Constructs

''' <summary> Functions tests. </summary>
''' <remarks>
''' David, 2/3/2019, https://github.com/polterguy/lizzie/. <para>
''' Copyright (c) 2018 Thomas Hansen - thomas@gaiasoul.com </para><para>
''' Licensed under The MIT License.</para>
''' </remarks>
<TestClass()>
Public Class FunctionsTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.Services.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext
    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " EVALUATE TESTS "

    ''' <summary> (Unit Test Method) evaluates this object. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    <TestMethod()>
    Public Sub Evaluate()
        Dim functions As New Functions(Of String) From {
                Function() "1",
                Function() "2",
                Function() "3"
            }

        Dim result As String = String.Empty
        For Each idx As String In functions.Evaluate()
            result &= idx
        Next idx
        Assert.AreEqual("123", result)
    End Sub

    ''' <summary> (Unit Test Method) evaluate parallel. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    <TestMethod()>
    Public Sub EvaluateParallel()
        Dim functions As New Functions(Of String) From {
                Function() "1",
                Function() "2",
                Function() "3"
            }

        Using sync As New Synchronizer(Of String)("")
            For Each idx As String In functions.EvaluateParallel()
                sync.Assign(Function(input) input & idx)
            Next idx
            Dim outcome As Boolean = False
            Dim result As String = String.Empty
            sync.Read(Sub(x) result = x)
            outcome = (result = "123") OrElse (result = "132") OrElse
                          (result = "231") OrElse (result = "213") OrElse
                          (result = "312") OrElse (result = "321")
            Assert.IsTrue(outcome)
        End Using
    End Sub
#End Region

End Class
