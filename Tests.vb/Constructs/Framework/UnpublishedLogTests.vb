﻿Imports isr.Core.Services.DispatcherExtensions
'''<summary>
'''This is a test class for unpublished messages
'''</summary>
<TestClass()>
Public Class UnpublishedLogTests

#Region " CONSTRUCTION and CLEANUP "

	''' <summary> My class initialize. </summary>
	''' <param name="testContext"> Gets or sets the test context which provides information about
	'''                            and functionality for the current test run. </param>
	''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
	<CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
	<ClassInitialize(), CLSCompliant(False)>
	Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
		Try
			_TestSite = New TestSite
			_TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
			_TestSite.AddTraceMessagesQueue(isr.VI.My.MyLibrary.UnpublishedTraceMessages)
			_TestSite.InitializeTraceListener()
		Catch
			' cleanup to meet strong guarantees
			Try
				MyClassCleanup()
			Finally
			End Try
			Throw
		End Try
	End Sub

	''' <summary> My class cleanup. </summary>
	''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
	<ClassCleanup()>
	Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
	End Sub

	''' <summary> Initializes before each test runs. </summary>
	<TestInitialize()> Public Sub MyTestInitialize()
		' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

	''' <summary> Cleans up after each test has run. </summary>
	<TestCleanup()> Public Sub MyTestCleanup()
		TestInfo.AssertMessageQueue()
	End Sub

	'''<summary>
	'''Gets or sets the test context which provides
	'''information about and functionality for the current test run.
	'''</summary>
	Public Property TestContext() As TestContext

	Private Shared _TestSite As TestSite

	''' <summary> Gets information describing the test. </summary>
	''' <value> Information describing the test. </value>
	Private Shared ReadOnly Property TestInfo() As TestSite
		Get
			Return _TestSite
		End Get
	End Property

#End Region

#Region " UNPUBLISHED LOG TESTS  "

	''' <summary> Tests logging unpublished messages. </summary>
	''' <param name="expected"> The expected. </param>
	Public Shared Sub TestLoggingUnpublishedMessages(ByVal expected As TraceEventType)

		Dim payload As String = "Message 1"
		Dim expectedMessage As New isr.Core.Services.TraceMessage(expected, isr.Core.Services.My.MyLibrary.TraceEventId, payload)

		Dim initialLogFileSize As Long = isr.Core.Services.My.MyLibrary.MyLog.FileSize

		Dim actualLogFileName As String = isr.Core.Services.My.MyLibrary.MyLog.FullLogFileName
		Assert.IsFalse(String.IsNullOrWhiteSpace(actualLogFileName), $"Tests if log file name '{actualLogFileName}' is empty")

		' clear trace message log
		isr.Core.Services.My.MyLibrary.UnpublishedTraceMessages.DequeueContent()

		' check content
		Dim actualContents As String = isr.Core.Services.My.MyLibrary.UnpublishedTraceMessages.DequeueContent()
		Assert.IsTrue(String.IsNullOrWhiteSpace(actualContents), "Unpublished messages should clear")

		' log an unpublished trace message to the library.
		isr.Core.Services.My.MyLibrary.LogUnpublishedMessage(expectedMessage)

		Dim actualMessage As isr.Core.Services.TraceMessage = isr.Core.Services.My.MyLibrary.UnpublishedTraceMessages.TryDequeue
		Assert.AreEqual(expectedMessage.Id, actualMessage.Id, " Message trace events identities match")

		isr.Core.Services.My.MyLibrary.MyLog.FlushMessages()

		' this was modified from using the stopwatch; needs to be tested.
		isr.Core.Services.My.MyLibrary.DoEvents()
		isr.Core.Services.My.MyLibrary.Delay(100)
		Dim newLogFileSize As Long = isr.Core.Services.My.MyLibrary.MyLog.FileSize
		Assert.IsTrue(newLogFileSize > initialLogFileSize, $"New file Size {newLogFileSize} > initial file size {initialLogFileSize}")


	End Sub

	''' <summary> (Unit Test Method) tests log unpublished messages. </summary>
	<TestMethod()>
	Public Sub LogUnpublishedMessagesTest()
		TestLoggingUnpublishedMessages(TraceEventType.Warning)
	End Sub
#End Region

End Class
