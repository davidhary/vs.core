﻿Imports isr.Core.Services
''' <summary> Action Event Arguments tests. </summary>
''' <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 4/8/2018 </para></remarks>
<TestClass()>
Public Class ActionEventArgsTests

#Region " CONSTRUCTION and CLEANUP "

	''' <summary> My class initialize. </summary>
	''' <param name="testContext"> Gets or sets the test context which provides information about
	'''                            and functionality for the current test run. </param>
	''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
	<CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
	<ClassInitialize(), CLSCompliant(False)>
	Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
		Try
			_TestSite = New TestSite
			_TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
			_TestSite.AddTraceMessagesQueue(isr.VI.My.MyLibrary.UnpublishedTraceMessages)
			_TestSite.InitializeTraceListener()
		Catch
			' cleanup to meet strong guarantees
			Try
				MyClassCleanup()
			Finally
			End Try
			Throw
		End Try
	End Sub

	''' <summary> My class cleanup. </summary>
	''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
	<ClassCleanup()>
	Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
	End Sub

	''' <summary> Initializes before each test runs. </summary>
	<TestInitialize()> Public Sub MyTestInitialize()
		' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

	''' <summary> Cleans up after each test has run. </summary>
	<TestCleanup()> Public Sub MyTestCleanup()
		TestInfo.AssertMessageQueue()
	End Sub

	'''<summary>
	'''Gets or sets the test context which provides
	'''information about and functionality for the current test run.
	'''</summary>
	Public Property TestContext() As TestContext

	Private Shared _TestSite As TestSite

	''' <summary> Gets information describing the test. </summary>
	''' <value> Information describing the test. </value>
	Private Shared ReadOnly Property TestInfo() As TestSite
		Get
			Return _TestSite
		End Get
	End Property

#End Region

#Region " ACTION EVENT ARGMENTS TESTS "

	<TestMethod()>
	Public Sub ActionEventTest()
		Dim args As ActionEventArgs = Nothing
		args = New ActionEventArgs(TraceEventType.Error)
		Dim message As String = "Information"
		Dim eventType As TraceEventType = TraceEventType.Information
		args.RegisterOutcomeEvent(eventType, message)
		Assert.IsFalse(args.Failed, $"Action {args.Details} at {eventType} failed")
		Assert.AreEqual(eventType, args.OutcomeEvent, $"Action {args.Details} event unexpected")
		Assert.IsTrue(args.HasDetails, $"Action {args.Details} at {eventType} reported not having details")

		eventType = TraceEventType.Error
		message = "Error"
		args.RegisterOutcomeEvent(eventType, message)
		Assert.IsTrue(args.Failed, $"Action {args.Details} at {eventType} did not failed")
		Assert.AreEqual(eventType, args.OutcomeEvent, $"Action {args.Details} event unexpected")

		eventType = TraceEventType.Critical
		message = "Critical"
		args.RegisterOutcomeEvent(eventType, message)
		Assert.IsTrue(args.Failed, $"Action {args.Details} at {eventType} did not failed")
		Assert.AreEqual(eventType, args.OutcomeEvent, $"Action {args.Details} event unexpected")
	End Sub

	<TestMethod()>
	Public Sub ActionEventCancellationTest()
		Dim args As ActionEventArgs = Nothing
		args = New ActionEventArgs(TraceEventType.Error)
		Dim message As String = "Cancel"
		args.RegisterFailure(message)
		Assert.IsTrue(args.Failed, $"Action {args.Details} failed to cancel")
	End Sub

#End Region

End Class
