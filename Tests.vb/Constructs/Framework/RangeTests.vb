Imports isr.Core.Constructs

''' <summary> tests of equalities. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 10/10/2017 </para>
''' </remarks>
<TestClass()>
Public Class RangeTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.Services.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext
    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " EQUALITY TESTS "

    ''' <summary> Query if 'value' is empty. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> <c>true</c> if empty; otherwise <c>false</c> </returns>
    Public Shared Function IsEmpty(ByVal value As String) As Boolean
        Return String.IsNullOrEmpty(value) AndAlso Not value Is Nothing
    End Function

    ''' <summary> Information getter. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> A String. </returns>
    Public Shared Function InfoGetter(ByVal value As String) As String
        Dim result As String

        If value Is Nothing Then
            result = "null"
        ElseIf String.IsNullOrEmpty(value) Then
            result = "empty"
        ElseIf String.IsNullOrWhiteSpace(value) Then
            result = "white"
        Else
            result = value
        End If
        Return result
    End Function

    ''' <summary> (Unit Test Method) tests string equality. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    Public Shared Sub EqualityTest(ByVal left As String, ByVal right As String)
        Assert.IsTrue(left = right, $"1. Left.{InfoGetter(left)} == Right.{InfoGetter(right)}")
        Assert.IsTrue(left = left, $"2. Left.{InfoGetter(left)} == Left.{InfoGetter(left)}")
        Assert.IsTrue(String.Equals(left, right), $"3. String.Equals(Left.{InfoGetter(left)}, Right.{InfoGetter(right)})")
        Assert.IsTrue(String.Equals(left, left), $"4. String.Equals(Left.{InfoGetter(left)}, left.{InfoGetter(left)})")
        Assert.IsTrue(Object.ReferenceEquals(left, right), $"5. Object.ReferenceEquals(Left.{InfoGetter(left)}, Right.{InfoGetter(right)})")
        Assert.IsTrue(Object.ReferenceEquals(left, left), $"6. Object.ReferenceEquals(Left.{InfoGetter(left)}, left.{InfoGetter(left)})")
        Assert.IsTrue(CObj(left) Is CObj(right), $"7. (Object)Left.{InfoGetter(left)} Is (Object)Right.{InfoGetter(right)}")
        Assert.IsTrue(CObj(left) Is CObj(left), $"8. (Object)Left.{InfoGetter(left)} Is (Object)left.{InfoGetter(left)}")
    End Sub

    ''' <summary> (Unit Test Method) tests string equality. </summary>
    ''' <remarks>
    ''' Results:
    ''' <code>
    '''   TRUE: String.Empty == String.Empty
    '''   TRUE: String.Equals(String.Empty, String.Empty)
    '''   TRUE: String.ReferenceEquals(String.Empty, String.Empty)
    '''   TRUE: String.Empty Is String.Empty
    ''' 
    '''   TRUE: null == null
    '''   TRUE: String.Equals(null, null)
    '''   TRUE: String.ReferenceEquals(null, null)
    '''   TRUE: null Is null
    ''' 
    '''   TRUE: "" == String.Empty
    '''   TRUE: String.Equals("", "")
    '''   TRUE: String.ReferenceEquals("", "")
    '''   TRUE: "" Is ""
    ''' 
    '''   TRUE: String.Empty == String.Empty
    '''   TRUE: String.Equals(String.Empty, "")
    '''   TRUE: String.ReferenceEquals(String.Empty, "")
    '''   TRUE: String.Empty Is ""
    ''' 
    '''   TRUE: "a" == "a"
    '''   TRUE: String.Equals("a", "a")
    '''   TRUE: String.ReferenceEquals("a", "a")
    '''   TRUE: "a" Is "a"
    ''' </code>
    ''' Conclusions: (1) Nothing Equals Nothing and Empty equals "" (note below that Empty == Nothing
    ''' but not equals Nothing )
    ''' </remarks>
    <TestMethod()>
    Public Sub StringEqualityTest()
        Dim left As String = String.Empty
        Dim right As String = String.Empty
        EqualityTest(left, right)
        left = Nothing
        right = Nothing
        EqualityTest(left, right)
        left = String.Empty
        right = String.Empty
        EqualityTest(left, right)
        left = String.Empty
        right = String.Empty
        EqualityTest(left, right)
        left = "a"
        right = "a"
        EqualityTest(left, right)
    End Sub

    ''' <summary> (Unit Test Method) tests string inequality. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    Public Shared Sub InequalityTest(ByVal left As String, ByVal right As String)
        Assert.IsFalse(String.Equals(left, right), $"2. !String.Equals(Left.{InfoGetter(left)}, Right.{InfoGetter(right)})")
        Assert.IsFalse(Object.ReferenceEquals(left, right), $"3. !Object.ReferenceEquals(Left.{InfoGetter(left)}, Right.{InfoGetter(right)})")
        Assert.IsFalse(CObj(left) Is CObj(right), $"4. !(Object)Left.{InfoGetter(left)} Is (Object)Right.{InfoGetter(right)}")
    End Sub

    ''' <summary> (Unit Test Method) tests string equality. </summary>
    ''' <remarks>
    ''' Results:
    ''' <code>
    '''   TRUE: String.Empty = null
    '''   FALSE: String.Equals(String.Empty, null)
    '''   FALSE: String.ReferenceEquals(String.Empty, String.Empty)
    '''   FALSE: String.Empty Is String.Empty
    ''' 
    '''   TRUE: null == String.Empty
    '''   FALSE: String.Equals(null, "")
    '''   FALSE: String.ReferenceEquals(null, "")
    '''   FALSE: null Is ""
    ''' 
    '''   FALSE: "a" == String.Empty
    '''   FALSE: String.Equals("a", "")
    '''   FASLE: String.ReferenceEquals("a", "")
    '''   FALSE: "a" Is ""
    ''' 
    '''   FALSE: "a" == "b"
    '''   FALSE: String.Equals("a", "b")
    '''   FALSE: String.ReferenceEquals("a", "b")
    '''   FALSE: "a" Is "b"
    ''' </code>
    ''' Conclusions: (1) String.Equals() correct; except if (2) Nothing == Empty but Not("" =
    ''' Nothing)  !!!!
    ''' </remarks>
    <TestMethod()>
    Public Sub StringInequalityTest()
        Dim left As String = String.Empty
        Dim right As String = Nothing
        Assert.IsTrue(left = right, $"1. Left.{InfoGetter(left)} == Right.{InfoGetter(right)}")
        InequalityTest(left, right)
        left = Nothing
        right = String.Empty
        Assert.IsTrue(left = right, $"1. Left.{InfoGetter(left)} != Right.{InfoGetter(right)}")
        InequalityTest(left, right)
        left = "a"
        right = String.Empty
        Assert.IsFalse(left = right, $"1. Left.{InfoGetter(left)} != Right.{InfoGetter(right)}")
        InequalityTest(left, right)
        left = "a"
        right = "b"
        Assert.IsFalse(left = right, $"1. Left.{InfoGetter(left)} != Right.{InfoGetter(right)}")
        InequalityTest(left, right)
    End Sub

    ''' <summary> Information getter. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> A String. </returns>
    Public Shared Function InfoGetter(ByVal value As IEquatable(Of TimeSpan)) As String
        Return If(value Is Nothing, "null", value.ToString)
    End Function

    ''' <summary> (Unit Test Method) tests string equality. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    Public Shared Sub EqualityTest(ByVal left As IEquatable(Of TimeSpan), ByVal right As IEquatable(Of TimeSpan))
        Assert.IsTrue(Object.Equals(left, right), $"1. Object.Equals(Left.{InfoGetter(left)}, Right.{InfoGetter(right)})")
        Assert.IsTrue(Object.Equals(left, left), $"2. Object.Equals(Left.{InfoGetter(left)}, left.{InfoGetter(left)})")
        Assert.IsFalse(Object.ReferenceEquals(left, right), $"3. Object.ReferenceEquals(Left.{InfoGetter(left)}, Right.{InfoGetter(right)})")
        Assert.IsTrue(Object.ReferenceEquals(left, left), $"4. Object.ReferenceEquals(Left.{InfoGetter(left)}, left.{InfoGetter(left)})")
        Assert.IsFalse(CObj(left) Is CObj(right), $"5. (Object)Left.{InfoGetter(left)} Is (Object)Right.{InfoGetter(right)}")
        Assert.IsTrue(CObj(left) Is CObj(left), $"6. (Object)Left.{InfoGetter(left)} Is (Object)left.{InfoGetter(left)}")
    End Sub

    ''' <summary> (Unit Test Method) tests string inequality. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    Public Shared Sub InequalityTest(ByVal left As IEquatable(Of TimeSpan), ByVal right As IEquatable(Of TimeSpan))
        Assert.IsFalse(Object.Equals(left, right), $"1. !Object.Equals(Left.{InfoGetter(left)}, Right.{InfoGetter(right)})")
        Assert.IsFalse(Object.ReferenceEquals(left, right), $"2. !Object.ReferenceEquals(Left.{InfoGetter(left)}, Right.{InfoGetter(right)})")
        Assert.IsFalse(CObj(left) Is CObj(right), $"3. !(Object)Left.{InfoGetter(left)} Is (Object)Right.{InfoGetter(right)}")
    End Sub

    ''' <summary> (Unit Test Method) tests Structure equality. </summary>
    ''' <remarks>
    ''' Results:
    ''' <code>
    '''  The follwoing applies to timespan value, nothing or zero and from zero seconds
    '''   TRUE: TimeSpan.Zero == TimeSpan.Zero
    '''   TRUE: Object.Equals(TimeSpan.Zero, TimeSpan.Zero)
    '''   FALSE: Object.ReferenceEquals(left, right)
    '''   TRUE: Object.ReferenceEquals(left, left)
    '''   FALSE: left Is right
    '''   TRUE: left Is left
    '''   TRUE: TimeSpan.Zero == TimeSpan = nothing
    ''' </code>
    ''' Conclusions: (1) Nothing (Null) objects are equal. (2) Time span = nothing is the same as
    ''' Timespan.Zero.
    ''' </remarks>
    <TestMethod()>
    Public Sub StructureEqualityTest()
        Dim left As TimeSpan = TimeSpan.Zero
        Dim right As TimeSpan = TimeSpan.Zero
        Assert.IsTrue(left = right, $"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
        EqualityTest(left, right)
        left = Nothing
        right = Nothing
        Assert.IsTrue(left = right, $"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
        EqualityTest(left, right)
        left = TimeSpan.Zero
        right = TimeSpan.FromSeconds(0)
        Assert.IsTrue(left = right, $"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
        EqualityTest(left, right)
        left = TimeSpan.Zero
        right = Nothing
        Assert.IsTrue(left = right, $"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
        EqualityTest(left, right)
    End Sub

    ''' <summary> (Unit Test Method) tests structure inequality. </summary>
    ''' <remarks>
    ''' Results:
    ''' <code>
    '''  The follwoing applies to timespan value, nothing or zero and from zero seconds
    '''   FALSE: TimeSpan.One == TimeSpan.Zero or nothing
    '''   FALSE: Object.Equals(TimeSpan.One, TimeSpan.Zero or nothing)
    '''   FALSE: Object.ReferenceEquals(left, right)
    '''   FALSE: left Is right
    '''   TRUE: TimeSpan.Zero == TimeSpan nothing
    ''' </code>
    ''' Conclusions: (1) Nothing (Null) objects are equal. (2) Time span = nothing is the same as
    ''' Timespan.Zero.
    ''' </remarks>
    <TestMethod()>
    Public Sub StructureInequalityTest()
        Dim left As TimeSpan = TimeSpan.FromSeconds(1)
        Dim right As TimeSpan = Nothing
        Assert.IsFalse(left = right, $"a. Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
        InequalityTest(left, right)
        left = TimeSpan.FromSeconds(1)
        right = TimeSpan.Zero
        Assert.IsFalse(left = right, $"b. Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
        InequalityTest(left, right)
    End Sub

    ''' <summary> Information getter. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> A String. </returns>
    Public Shared Function InfoGetter(ByVal value As Object) As String
        Return If(value Is Nothing, "null", value.ToString)
    End Function

    ''' <summary> (Unit Test Method) tests string equality. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    Public Shared Sub EqualityTest(ByVal left As Object, ByVal right As Object)
        Assert.IsTrue(Object.Equals(left, right), $"1. Object.Equals(Left.{InfoGetter(left)}, Right.{InfoGetter(right)})")
        Assert.IsTrue(Object.Equals(left, left), $"2. Object.Equals(Left.{InfoGetter(left)}, left.{InfoGetter(left)})")
        Assert.IsTrue(Object.ReferenceEquals(left, left), $"3. Object.ReferenceEquals(Left.{InfoGetter(left)}, left.{InfoGetter(left)})")
        Assert.IsTrue(CObj(left) Is CObj(left), $"4. (Object)Left.{InfoGetter(left)} Is (Object)left.{InfoGetter(left)}")
    End Sub

    ''' <summary> (Unit Test Method) tests string inequality. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    Public Shared Sub InequalityTest(ByVal left As Object, ByVal right As Object)
        Assert.IsFalse(Object.Equals(left, right), $"1. !Object.Equals(Left.{InfoGetter(left)}, Right.{InfoGetter(right)})")
        Assert.IsFalse(Object.ReferenceEquals(left, right), $"2. !Object.ReferenceEquals(Left.{InfoGetter(left)}, Right.{InfoGetter(right)})")
        Assert.IsFalse(CObj(left) Is CObj(right), $"3. !(Object)Left.{InfoGetter(left)} Is (Object)Right.{InfoGetter(right)}")
    End Sub

    ''' <summary> (Unit Test Method) tests object equality. </summary>
    ''' <remarks>
    ''' Results:
    ''' <code>
    '''  The follwoing applies to version nothing or zero
    '''   TRUE: Version(0,0) == Version(0,0)
    '''   TRUE: Object.Equals(Version(0,0), Version(0,0))
    '''   FALSE: Object.ReferenceEquals(left, right)
    '''   TRUE: Object.ReferenceEquals(left(null), right(null))
    '''   TRUE: Object.ReferenceEquals(left, left)
    '''   FALSE: left Is right
    '''   TRUE: left(null) Is right(null)
    '''   TRUE: left Is left
    '''   TRUE: TimeSpan.Zero == TimeSpan = nothing
    ''' </code>
    ''' Conclusions: Nothing (Null) objects are equal.
    ''' </remarks>
    <TestMethod()>
    Public Sub VersionEqualityTest()
        Dim unused As Version = New Version(0, 0)
        Dim unused1 As Version = New Version(0, 0)
        Dim left As Version = New Version(0, 0)
        Dim right As Version = New Version(0, 0)
        Assert.IsTrue(left = right, $"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
        Assert.IsFalse(Object.ReferenceEquals(left, right), $"a. Object.ReferenceEquals(Left.{InfoGetter(left)}, Right.{InfoGetter(right)})")
        Assert.IsFalse(CObj(left) Is CObj(right), $"b. (Object)Left.{InfoGetter(left)} Is (Object)Right.{InfoGetter(right)}")
        EqualityTest(left, right)
        left = Nothing
        right = Nothing
        TestInfo.TraceMessage($"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
        Assert.IsTrue(left = right, $"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
        Assert.IsTrue(Object.ReferenceEquals(left, right), $"a. Object.ReferenceEquals(Left.{InfoGetter(left)}, Right.{InfoGetter(right)})")
        Assert.IsTrue(CObj(left) Is CObj(right), $"b. (Object)Left.{InfoGetter(left)} Is (Object)Right.{InfoGetter(right)}")
        EqualityTest(left, right)
    End Sub

    ''' <summary> (Unit Test Method) tests object inequality. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    <TestMethod()>
    Public Sub VersionInequalityTest()
        Dim unused As Version = New Version(0, 0)
        Dim unused1 As Version = New Version(0, 0)
        Dim left As Version = New Version(0, 0)
        Dim right As Version = New Version(0, 1)
        TestInfo.TraceMessage($"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
        Assert.IsFalse(left = right, $"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
        InequalityTest(left, right)
        left = New Version(0, 0)
        right = Nothing
        TestInfo.TraceMessage($"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
        Assert.IsFalse(left = right, $"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
        InequalityTest(left, right)
    End Sub

    ''' <summary> (Unit Test Method) tests nullable equality. </summary>
    ''' <remarks>
    ''' Results:
    ''' <code>
    '''   NULL: New Integer? == New Integer?
    '''   TRUE: Object.Equals(left(New Integer?) , right(New Integer?))
    '''   TRUE: Object.ReferenceEquals(left(New Integer?) , right(New Integer?))
    '''   TRUE: Object.ReferenceEquals(left(New Integer?) , left(New Integer?))
    '''   TRUE: left(New Integer?) Is right(New Integer?)
    '''   TRUE: left(New Integer?) Is left(New Integer?)
    ''' 
    '''   TRUE: New Integer?(1) == New Integer?(1)
    '''   TRUE: Object.Equals(left(New Integer?(1)) , right(New Integer?(1)))
    '''   FALSE: Object.ReferenceEquals(left(New Integer?(1)) , right(New Integer?(1)))
    '''   TRUE: Object.ReferenceEquals(left(New Integer?(1)) , left(New Integer?(1)))
    '''   FALSE: left(New Integer?(1)) Is right(New Integer?(1))
    '''   TRUE: left(New Integer?(1)) Is left(New Integer?(1))
    ''' 
    '''   NULL: Integer?(null) == Integer?(null)
    '''   TRUE: Object.Equals(left(Integer?(null)) , right(Integer?(null)))
    '''   TRUE: Object.ReferenceEquals(left(Integer?(null)) , right(Integer?(null)))
    '''   TRUE: Object.ReferenceEquals(left(Integer?(null)) , left(Integer?(null)))
    '''   TRUE: left(Integer?(null)) Is right(Integer?(null))
    '''   TRUE: left(Integer?(null)) Is left(Integer?(null))
    ''' 
    '''   NULL: New Integer? == Integer?(null)
    '''   TRUE: Object.Equals(left(New Integer?) , right(Integer?(null)))
    '''   TRUE: Object.ReferenceEquals(left(New Integer?) , right(Integer?(null)))
    '''   TRUE: Object.ReferenceEquals(left(New Integer?) , left(New Integer?))
    '''   TRUE: left(New Integer?) Is right(Integer?(null))
    '''   TRUE: left(New Integer?) Is left(New Integer?)
    ''' </code>
    ''' Conclusions: Nothing (Null) objects are equal but equality operations yield nothing.
    ''' </remarks>
    <TestMethod()>
    Public Sub NullableEqualityTest()
        Dim unused As New Integer?
        Dim unused1 As New Integer?
        Dim left As Integer? = New Integer?
        Dim right As Integer? = New Integer?
        Assert.IsTrue((left = right) Is Nothing, $"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
        Assert.IsTrue(Object.ReferenceEquals(left, right), $"a. Object.ReferenceEquals(Left.{InfoGetter(left)}, Right.{InfoGetter(right)})")
        Assert.IsTrue(CObj(left) Is CObj(right), $"b. (Object)Left.{InfoGetter(left)} Is (Object)Right.{InfoGetter(right)}")
        EqualityTest(left, right)
        left = New Integer?(1)
        right = New Integer?(1)
        Assert.IsTrue((left = right).Value, $"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
        Assert.IsFalse(Object.ReferenceEquals(left, right), $"a. Object.ReferenceEquals(Left.{InfoGetter(left)}, Right.{InfoGetter(right)})")
        Assert.IsFalse(CObj(left) Is CObj(right), $"b. (Object)Left.{InfoGetter(left)} Is (Object)Right.{InfoGetter(right)}")
        EqualityTest(left, right)
        left = Nothing
        right = Nothing
        Assert.IsTrue((left = right) Is Nothing, $"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
        Assert.IsTrue(Object.ReferenceEquals(left, right), $"a. Object.ReferenceEquals(Left.{InfoGetter(left)}, Right.{InfoGetter(right)})")
        Assert.IsTrue(CObj(left) Is CObj(right), $"b. (Object)Left.{InfoGetter(left)} Is (Object)Right.{InfoGetter(right)}")
        EqualityTest(left, right)
        left = New Integer?
        right = Nothing
        Assert.IsTrue((left = right) Is Nothing, $"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
        Assert.IsTrue(Object.ReferenceEquals(left, right), $"a. Object.ReferenceEquals(Left.{InfoGetter(left)}, Right.{InfoGetter(right)})")
        Assert.IsTrue(CObj(left) Is CObj(right), $"b. (Object)Left.{InfoGetter(left)} Is (Object)Right.{InfoGetter(right)}")
        EqualityTest(left, right)
    End Sub

    ''' <summary> (Unit Test Method) tests nullable inequality. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    <TestMethod()>
    Public Sub NullableInequalityTest()
        Dim unused As New Integer?
        Dim unused1 As New Integer?
        Dim left As Integer? = New Integer?(0)
        Dim right As Integer? = New Integer?
        Assert.IsTrue((left = right) Is Nothing, $"Left.{InfoGetter(left)} == right.{InfoGetter(right)} Is Not defined = Nothing")
        InequalityTest(left, right)
        left = New Integer?(0)
        right = Nothing
        Assert.IsTrue((left = right) Is Nothing, $"Left.{InfoGetter(left)} == right.{InfoGetter(right)} Is Not defined = Nothing")
        InequalityTest(left, right)
    End Sub

#End Region

#Region " RANGE TESTS "

    ''' <summary> (Unit Test Method) tests range equality. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    <TestMethod()>
    Public Sub RangeEqualityTest()
        Dim unused As RangeR = New RangeR(0, 0)
        Dim unused1 As RangeR = New RangeR(0, 0)
        Dim left As RangeR = New RangeR(0, 0)
        Dim right As RangeR = New RangeR(0, 0)
        Assert.IsTrue(left = right, $"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
        Assert.IsFalse(Object.ReferenceEquals(left, right), $"a. Object.ReferenceEquals(Left.{InfoGetter(left)}, Right.{InfoGetter(right)})")
        Assert.IsFalse(CObj(left) Is CObj(right), $"b. (Object)Left.{InfoGetter(left)} Is (Object)Right.{InfoGetter(right)}")
        EqualityTest(left, right)
        left = Nothing
        right = Nothing
        TestInfo.TraceMessage($"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
        Assert.IsTrue(left = right, $"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
        Assert.IsTrue(Object.ReferenceEquals(left, right), $"a. Object.ReferenceEquals(Left.{InfoGetter(left)}, Right.{InfoGetter(right)})")
        Assert.IsTrue(CObj(left) Is CObj(right), $"b. (Object)Left.{InfoGetter(left)} Is (Object)Right.{InfoGetter(right)}")
        EqualityTest(left, right)
    End Sub

    ''' <summary> (Unit Test Method) tests object inequality. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    <TestMethod()>
    Public Sub RangeInequalityTest()
        Dim unused As RangeR = New RangeR(0, 0)
        Dim unused1 As RangeR = New RangeR(0, 0)
        Dim left As RangeR = New RangeR(0, 0)
        Dim right As RangeR = New RangeR(1, 0)
        TestInfo.TraceMessage($"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
        Assert.IsFalse(left = right, $"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
        InequalityTest(left, right)
        left = New RangeR(0, 0)
        right = Nothing
        TestInfo.TraceMessage($"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
        Assert.IsFalse(left = right, $"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
        InequalityTest(left, right)
    End Sub

#End Region

End Class
