Imports isr.Core.Constructs

''' <summary> tests of collections. </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 05/22/2020 </para>
''' </remarks>
<TestClass()>
Public Class CollectionTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.Services.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()

    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext
    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " LIST DICTIONARY  "

    ''' <summary> Assert contains. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="dict">         The dictionary. </param>
    ''' <param name="keyValuePair"> The key value pair. </param>
    Private Shared Sub AssertContains(ByVal dict As ListDictionary(Of String, Integer), ByVal keyValuePair As KeyValuePair(Of String, Integer))
        Assert.AreEqual(keyValuePair.Value, dict(keyValuePair.Key))
        Assert.IsTrue(dict.ContainsKey(keyValuePair.Key))
        Assert.IsTrue(dict.Keys.Contains(keyValuePair.Key))
        Assert.IsTrue(dict.Values.Contains(keyValuePair.Value))
    End Sub

    ''' <summary> Assert first. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="dict">         The dictionary. </param>
    ''' <param name="keyValuePair"> The key value pair. </param>
    Private Shared Sub AssertFirst(ByVal dict As ListDictionary(Of String, Integer), ByVal keyValuePair As KeyValuePair(Of String, Integer))
        Assert.AreEqual(keyValuePair.Key, dict.FirstKey)
        Assert.AreEqual(keyValuePair.Key, dict.Entries.First().Key)
        Assert.AreEqual(keyValuePair.Value, dict.Entries.First().Value)
    End Sub

    ''' <summary> Assert last. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="dict">         The dictionary. </param>
    ''' <param name="keyValuePair"> The key value pair. </param>
    Private Shared Sub AssertLast(ByVal dict As ListDictionary(Of String, Integer), ByVal keyValuePair As KeyValuePair(Of String, Integer))
        Assert.AreEqual(keyValuePair.Key, dict.LastKey)
        Assert.AreEqual(keyValuePair.Key, dict.Entries.Last().Key)
        Assert.AreEqual(keyValuePair.Value, dict.Entries.Last().Value)
    End Sub

    ''' <summary> (Unit Test Method) tests list dictionary. </summary>
    ''' <remarks> David, 2020-05-22. </remarks>
    <TestMethod()>
    Public Sub ListDictionaryTest()

        Dim dict As ListDictionary(Of String, Integer) = New ListDictionary(Of String, Integer)()
        Dim count As Integer = 0
        Dim firstKeyValuePair As New KeyValuePair(Of String, Integer)("foo", 33)
        dict(firstKeyValuePair.Key) = firstKeyValuePair.Value : count += 1

        Dim middleKeyValuePair As New KeyValuePair(Of String, Integer)("art", 23)
        dict(middleKeyValuePair.Key) = middleKeyValuePair.Value : count += 1
        Assert.AreEqual(count, dict.Count)

        Dim lastKeyValuePair As New KeyValuePair(Of String, Integer)("bar", 13)
        dict(lastKeyValuePair.Key) = lastKeyValuePair.Value : count += 1
        Assert.AreEqual(count, dict.Count)


        CollectionTests.AssertContains(dict, firstKeyValuePair)
        CollectionTests.AssertFirst(dict, firstKeyValuePair)

        CollectionTests.AssertContains(dict, middleKeyValuePair)

        CollectionTests.AssertContains(dict, lastKeyValuePair)
        CollectionTests.AssertLast(dict, lastKeyValuePair)

        Assert.IsTrue(Not dict.ContainsKey("bar1"))

        ' move first to last
        'firstKeyValuePair = New KeyValuePair(Of String, Integer)(middleKeyValuePair.Key, middleKeyValuePair.Value)
        'middleKeyValuePair = New KeyValuePair(Of String, Integer)(lastKeyValuePair.Key, lastKeyValuePair.Value)
        'lastKeyValuePair = New KeyValuePair(Of String, Integer)("foo", 42)

        firstKeyValuePair = New KeyValuePair(Of String, Integer)("foo", 42)
        dict(firstKeyValuePair.Key) = firstKeyValuePair.Value

        CollectionTests.AssertContains(dict, firstKeyValuePair)
        CollectionTests.AssertFirst(dict, firstKeyValuePair)

        CollectionTests.AssertContains(dict, middleKeyValuePair)

        CollectionTests.AssertContains(dict, lastKeyValuePair)
        CollectionTests.AssertLast(dict, lastKeyValuePair)

    End Sub

#End Region

End Class

#If OldCode Then
ListDictionary<string, int> dict = new ListDictionary<string, int>();

dict["foo"] = 1;
dict["bar"] = 2;

Assert.AreEqual(1, dict["foo"]);
Assert.AreEqual(2, dict["bar"]);

Assert.AreEqual(2, dict.Count);

Assert.AreEqual("foo", dict.FirstKey);

Assert.AreEqual("foo", dict.Entries.First().Key);
Assert.AreEqual(1, dict.Entries.First().Value);

Assert.AreEqual("bar", dict.Entries.Last().Key);
Assert.AreEqual(2, dict.Entries.Last().Value);

Assert.IsTrue(dict.Keys.Contains("foo"));
Assert.IsTrue(dict.Values.Contains(1));

Assert.IsTrue(dict.Keys.Contains("bar"));
Assert.IsTrue(dict.Values.Contains(2));

Assert.IsTrue(dict.ContainsKey("foo"));
Assert.IsTrue(dict.ContainsKey("bar"));
Assert.IsTrue(!dict.ContainsKey("blet"));

dict["foo"] = 42;
Assert.AreEqual(42, dict["foo"]);

#End If
