﻿Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    Partial Friend NotInheritable Class MyLibrary

        ''' <summary> Constructor that prevents a default instance of this class from being created. </summary>
        Private Sub New()
            MyBase.New()
        End Sub

        Public Const AssemblyTitle As String = "Core Capsule Tests"
        Public Const AssemblyDescription As String = "Unit Tests for the Core Capsule Library"
        Public Const AssemblyProduct As String = "isr.Core.CapsuleTests"

    End Class

End Namespace

