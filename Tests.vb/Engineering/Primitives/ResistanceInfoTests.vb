﻿Imports isr.Core.Engineering

''' <summary>
''' This is a test class for ResistanceInfoTest and is intended to contain all ResistanceInfoTest
''' Unit Tests.
''' </summary>
''' <remarks> David, 2020-09-23. </remarks>
<TestClass()>
Public Class ResistanceInfoTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext
    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

    ''' <summary> A test for TryParse. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="resistanceCode">     The resistance code. </param>
    ''' <param name="resistanceExpected"> The resistance expected. </param>
    ''' <param name="expected">           True if expected. </param>
    Public Shared Sub TryParse(ByVal resistanceCode As String, ByVal resistanceExpected As Double, ByVal expected As Boolean)
        Dim resistance As Double = 0.0!
        Dim details As String = String.Empty
        Dim actual As Boolean
        actual = ResistanceInfo.TryParse(resistanceCode, resistance, details)
        Assert.AreEqual(resistanceExpected, resistance, details)
        Assert.AreEqual(expected, actual, details)
    End Sub

    ''' <summary> A test for TryParse. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    Public Sub TryParseTest()
        ResistanceInfoTests.TryParse("121", 120, True)
        ResistanceInfoTests.TryParse("230", 23, True)
        ResistanceInfoTests.TryParse("1000", 100, True)
        ResistanceInfoTests.TryParse("2R67", 2.67, True)
        ResistanceInfoTests.TryParse("2.67", 2.67, True)
        ResistanceInfoTests.TryParse("12K6675", 12667.5, True)
        ResistanceInfoTests.TryParse("100R5", 100.5, True)
        ResistanceInfoTests.TryParse("R0015", 0.0015, True)
        ResistanceInfoTests.TryParse("100R", 100, True)
    End Sub

End Class
