Imports isr.Core.EngineeringTests.RandomExtensions

Partial Friend Class TestSite

    ''' <summary> Generates normally distributed doubles. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="seed">  The seed. </param>
    ''' <param name="count"> Number of values to generate. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the random normal doubles in this
    ''' collection.
    ''' </returns>
    Public Shared Function GenerateRandomNormals(ByVal seed As Integer, ByVal count As Integer) As IList(Of Double)
        Return TestSite.GenerateRandomNormals(New Random(seed), count)
    End Function

    ''' <summary> Generates normally distributed doubles. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="generator"> The random number generator. </param>
    ''' <param name="count">     Number of values to generate. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the random normal doubles in this
    ''' collection.
    ''' </returns>
    Public Shared Function GenerateRandomNormals(ByVal generator As Random, ByVal count As Integer) As IList(Of Double)
        Dim l As New List(Of Double)
        For i As Integer = 1 To count
            l.Add(generator.NextNormal)
        Next
        Return l.ToArray
    End Function

End Class

