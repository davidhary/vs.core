﻿Imports isr.Core.Engineering

''' <summary> A current source unit tests. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/18/2017 </para>
''' </remarks>
<TestClass()>
Public Class CurrentSourceTests

#Region " SHARED CONSTANTS "

    ''' <summary> Gets or sets the current source current. </summary>
    ''' <value> The current. </value>
    Public Shared Property Current As Double = 0.004

    ''' <summary> Gets or sets the conductance of the current source. </summary>
    ''' <value> The conductance. </value>
    Public Shared Property Conductance As Double = 0.001

    ''' <summary> Gets or sets the load resistance. </summary>
    ''' <value> The load resistance. </value>
    Public Shared Property LoadResistance As Double = 500

    ''' <summary> Gets or sets the voltage of the voltage source. </summary>
    ''' <value> The voltage. </value>
    Public Shared Property Voltage As Double = 5

    ''' <summary> Gets or sets the resistance of the voltage source. </summary>
    ''' <value> The resistance. </value>
    Public Shared Property Resistance As Double = 1000

    ''' <summary> Gets or sets the short resistance. </summary>
    ''' <value> The short resistance. </value>
    Public Shared Property ShortResistance As Double = 0

    ''' <summary> Gets or sets the open resistance. </summary>
    ''' <value> The short resistance. </value>
    Public Shared Property OpenResistance As Double = Double.PositiveInfinity

    ''' <summary> Gets or sets the short conductance. </summary>
    ''' <value> The short conductance. </value>
    Public Shared Property ShortConductance As Double = Double.PositiveInfinity

    ''' <summary> Gets or sets the open conductance. </summary>
    ''' <value> The open conductance. </value>
    Public Shared Property OpenConductance As Double = 0

    ''' <summary> The current change scale. </summary>
    ''' <value> The current change scale. </value>
    Public Shared Property CurrentChangeScale As Double = 1.5

    ''' <summary> The conductance change scale. </summary>
    ''' <value> The conductance change scale. </value>
    Public Shared Property ConductanceChangeScale As Double = 0.8

    ''' <summary> Gets or sets the current to voltage level epsilon. </summary>
    ''' <value> The current to voltage level epsilon. </value>
    Public Shared Property CurrentToVoltageLevelEpsilon As Double = 0.000000001

    ''' <summary> Gets or sets the current to voltage resistance epsilon. </summary>
    ''' <value> The current to voltage resistance epsilon. </value>
    Public Shared Property CurrentToVoltageResistanceEpsilon As Double = 0.000000001

    ''' <summary> Gets or sets the current to voltage current level epsilon. </summary>
    ''' <value> The current to voltage current level epsilon. </value>
    Public Shared Property CurrentToVoltageCurrentLevelEpsilon As Double = 0.000000001

    ''' <summary> Gets or sets the current to voltage conductance epsilon. </summary>
    ''' <value> The current to voltage conductance epsilon. </value>
    Public Shared Property CurrentToVoltageConductanceEpsilon As Double = 0.000000001

#End Region

#Region " CONSTUCTOR TESTS "

    ''' <summary> (Unit Test Method) tests build current source. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub BuildCurrentSourceTest()

        Dim source As CurrentSource = New CurrentSource(CurrentSourceTests.Current, CurrentSourceTests.Conductance)

        ' test source Current
        Dim actualValue As Double = source.Current
        Dim expectedValue As Double = CurrentSourceTests.Current
        Assert.AreEqual(expectedValue, actualValue)

        ' test source conductance
        actualValue = source.Conductance
        expectedValue = CurrentSourceTests.Conductance
        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    ''' <summary> (Unit Test Method) tests current to voltage source. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub CurrentToVoltageSourceTest()

        ' create current source
        Dim currentSource As CurrentSource = New CurrentSource(VoltageSourceTests.Current, VoltageSourceTests.Conductance)

        ' convert to voltage source
        Dim source As VoltageSource = currentSource.ToVoltageSource

        ' test source voltage
        Dim actualValue As Double = source.Voltage
        Dim expectedValue As Double = currentSource.Current / currentSource.Conductance
        Assert.AreEqual(expectedValue, actualValue, CurrentSourceTests.CurrentToVoltageLevelEpsilon)

        ' test source resistance
        actualValue = source.Resistance
        expectedValue = 1 / currentSource.Conductance
        Assert.AreEqual(expectedValue, actualValue, CurrentSourceTests.CurrentToVoltageResistanceEpsilon)

        Dim finalCurrentSource As CurrentSource = source.ToCurrentSource
        Assert.AreEqual(currentSource.Current, finalCurrentSource.Current, CurrentSourceTests.CurrentToVoltageCurrentLevelEpsilon)
        Assert.AreEqual(currentSource.Conductance, finalCurrentSource.Conductance, CurrentSourceTests.CurrentToVoltageConductanceEpsilon)

    End Sub

    ''' <summary> (Unit Test Method) tests null voltage source exception. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    <ExpectedException(GetType(ArgumentNullException), "Null voltage source inappropriately allowed")>
    Public Sub NullVoltageSourceExceptionTest()

        ' null voltage source
        Dim voltageSource As VoltageSource = Nothing

        ' convert to current source
        Dim currentSource As CurrentSource = New CurrentSource(CurrentSourceTests.Current, CurrentSourceTests.Conductance)
        currentSource.FromVoltageSource(voltageSource)

    End Sub

#End Region

#Region " CURRENT TESTS "

    ''' <summary> (Unit Test Method) tests current source. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub CurrentSourceTest()

        Dim source As CurrentSource = New CurrentSource(CurrentSourceTests.Current, CurrentSourceTests.Conductance)

        ' test load current
        Dim actualValue As Double = source.LoadCurrent(1 / CurrentSourceTests.LoadResistance)
        Dim expectedValue As Double = CurrentSourceTests.Current * Resistor.Parallel(1 / CurrentSourceTests.Conductance,
                                                                   CurrentSourceTests.LoadResistance) /
                                                                   CurrentSourceTests.LoadResistance
        Assert.AreEqual(expectedValue, actualValue)


        ' test open voltage
        actualValue = source.LoadVoltage(CurrentSourceTests.OpenResistance)
        expectedValue = CurrentSourceTests.Current / CurrentSourceTests.Conductance
        Assert.AreEqual(expectedValue, actualValue)

        ' test short voltage
        actualValue = source.LoadVoltage(CurrentSourceTests.ShortResistance)
        expectedValue = 0
        Assert.AreEqual(expectedValue, actualValue)

        ' test load voltage
        actualValue = source.LoadVoltage(CurrentSourceTests.LoadResistance)
        expectedValue = CurrentSourceTests.Current * Resistor.Parallel(1 / CurrentSourceTests.Conductance,
                                                                                         CurrentSourceTests.LoadResistance)
        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    ''' <summary> (Unit Test Method) tests current source change. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub CurrentSourceChangeTest()

        Dim source As CurrentSource = New CurrentSource(CurrentSourceTests.Current, CurrentSourceTests.Conductance)

        source.Current *= CurrentChangeScale
        ' test open voltage
        Dim actualValue As Double = source.LoadVoltage(CurrentSourceTests.OpenResistance)
        Dim expectedValue As Double = CurrentChangeScale * CurrentSourceTests.Current / CurrentSourceTests.Conductance
        Assert.AreEqual(expectedValue, actualValue)

        ' test short voltage
        actualValue = source.LoadVoltage(CurrentSourceTests.ShortResistance)
        expectedValue = 0
        Assert.AreEqual(expectedValue, actualValue)

        ' test load voltage
        actualValue = source.LoadVoltage(CurrentSourceTests.LoadResistance)
        expectedValue = CurrentChangeScale * CurrentSourceTests.Current *
                                        Resistor.Parallel(1 / CurrentSourceTests.Conductance, CurrentSourceTests.LoadResistance)
        Assert.AreEqual(expectedValue, actualValue)

        ' test load current
        actualValue = source.LoadCurrent(1 / CurrentSourceTests.LoadResistance)
        expectedValue = CurrentChangeScale * CurrentSourceTests.Current *
                                        Resistor.Parallel(1 / CurrentSourceTests.Conductance,
                                                          CurrentSourceTests.LoadResistance) / CurrentSourceTests.LoadResistance
        Assert.AreEqual(expectedValue, actualValue)

        source.Current /= CurrentChangeScale
        source.Conductance *= ConductanceChangeScale

        ' test open voltage
        actualValue = source.LoadVoltage(Double.PositiveInfinity)
        expectedValue = CurrentSourceTests.Current / (ConductanceChangeScale * CurrentSourceTests.Conductance)
        Assert.AreEqual(expectedValue, actualValue)

        ' test short voltage
        actualValue = source.LoadVoltage(CurrentSourceTests.ShortResistance)
        expectedValue = 0
        Assert.AreEqual(expectedValue, actualValue)

        ' test load voltage
        actualValue = source.LoadVoltage(CurrentSourceTests.LoadResistance)
        expectedValue = CurrentSourceTests.Current * Resistor.Parallel(1 / (ConductanceChangeScale * CurrentSourceTests.Conductance),
                                                                                         CurrentSourceTests.LoadResistance)
        Assert.AreEqual(expectedValue, actualValue)

        ' test load current
        actualValue = source.LoadCurrent(1 / CurrentSourceTests.LoadResistance)
        expectedValue = CurrentSourceTests.Current * Resistor.Parallel(1 / (ConductanceChangeScale * CurrentSourceTests.Conductance),
                                                                                         CurrentSourceTests.LoadResistance) /
                                                                                         CurrentSourceTests.LoadResistance
        Assert.AreEqual(expectedValue, actualValue)

    End Sub

#End Region

End Class
