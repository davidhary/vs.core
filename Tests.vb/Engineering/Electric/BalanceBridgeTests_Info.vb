Imports isr.Core.Engineering

''' <summary> Provides bridges for testing. </summary>
''' <remarks> David, 2020-09-23. </remarks>
Friend NotInheritable Class TestBridges

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Private Sub New()
        MyBase.New
    End Sub

    ''' <summary> Gets the element epsilon. </summary>
    ''' <value> The element epsilon. </value>
    Public Shared Property ElementEpsilon As Double = 0.000001

    ''' <summary> Gets the output voltage epsilon. </summary>
    ''' <value> The output voltage epsilon. </value>
    Public Shared Property OutputVoltageEpsilon As Double = 0.0001

    ''' <summary> Gets the full scale voltage epsilon. </summary>
    ''' <value> The full scale voltage epsilon. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared Property FullScaleVoltageEpsilon As Double = 0.0001

    ''' <summary> Gets source resistance tolerance. </summary>
    ''' <value> The source resistance tolerance. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared Property SourceResistanceTolerance As Double = 0.001
    ''' <summary> The low temporary zero pressure bridge. </summary>
    Private Shared _LowTempZeroPressureBridge As SampleBridge

    ''' <summary> Gets the low temporary zero pressure bridge. </summary>
    ''' <value> The low temporary zero pressure bridge. </value>
    Public Shared ReadOnly Property LowTempZeroPressureBridge As SampleBridge
        Get
            ' 636 this is calculated based on 5v, 0.004 v and the above resistances
            If _LowTempZeroPressureBridge Is Nothing Then
                _LowTempZeroPressureBridge = New SampleBridge(New Double() {369.59, 363.04, 357.69, 377.06}, WheatstoneLayout.Counterclockwise) With {
                    .SupplyVoltage = 5.02058,
                    .SupplyVoltageDrop = 3.67312,
                    .OutputVoltage = 0.011728
                }
                _LowTempZeroPressureBridge.BalanceBridge = New BalanceBridge(_LowTempZeroPressureBridge.Bridge, New Double() {Conductor.OpenConductance, 10.3, Resistor.ShortResistance, 1 / 52357}, BalanceLayout.TopShuntBottomSeries)
                _LowTempZeroPressureBridge.EquivalentResistance = 782
                _LowTempZeroPressureBridge.RelativeOffsetEpsilon = 0.005
                _LowTempZeroPressureBridge.NominalVoltage = 5
                _LowTempZeroPressureBridge.VoltageSourceSeriesResistance = 1538
                _LowTempZeroPressureBridge.VoltageSourceParallelResistance = 1593
                _LowTempZeroPressureBridge.NominalFullScaleVoltage = 0.02
                _LowTempZeroPressureBridge.NominalCurrent = 0.004
                _LowTempZeroPressureBridge.CurrentSourceSeriesResistance = 636
                _LowTempZeroPressureBridge.CurrentSourceParallelResistance = 150
            End If
            Return _LowTempZeroPressureBridge
        End Get
    End Property
    ''' <summary> The low temporary full scale pressure bridge. </summary>
    Private Shared _LowTempFullScalePressureBridge As SampleBridge

    ''' <summary> Gets the low temporary full scale pressure bridge. </summary>
    ''' <value> The low temporary full scale pressure bridge. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared ReadOnly Property LowTempFullScalePressureBridge As SampleBridge
        Get
            ' 636 this is calculated based on 5v, 0.004 v and the above resistances
            If _LowTempFullScalePressureBridge Is Nothing Then
                _LowTempFullScalePressureBridge = New SampleBridge(New Double() {381.78, 356.88, 369.62, 370.89}, WheatstoneLayout.Counterclockwise) With {
                    .SupplyVoltage = 5.02075,
                    .SupplyVoltageDrop = 3.66533,
                    .OutputVoltage = 0.02001
                }
                _LowTempFullScalePressureBridge.BalanceBridge = New BalanceBridge(_LowTempFullScalePressureBridge.Bridge, New Double() {Conductor.OpenConductance, 10.3, Resistor.ShortResistance, 1 / 52357}, BalanceLayout.TopShuntBottomSeries)
                _LowTempFullScalePressureBridge.EquivalentResistance = 782
                _LowTempFullScalePressureBridge.RelativeOffsetEpsilon = 0.005
                _LowTempFullScalePressureBridge.NominalVoltage = 5
                _LowTempFullScalePressureBridge.VoltageSourceSeriesResistance = 1538
                _LowTempFullScalePressureBridge.VoltageSourceParallelResistance = 1593
                _LowTempFullScalePressureBridge.NominalFullScaleVoltage = 0.02
                _LowTempFullScalePressureBridge.NominalCurrent = 0.004
                _LowTempFullScalePressureBridge.CurrentSourceSeriesResistance = 636
                _LowTempFullScalePressureBridge.CurrentSourceParallelResistance = 150
            End If
            Return _LowTempFullScalePressureBridge
        End Get
    End Property
    ''' <summary> The high temporary zero pressure bridge. </summary>
    Private Shared _HighTempZeroPressureBridge As SampleBridge

    ''' <summary> Gets the high temporary zero pressure bridge. </summary>
    ''' <value> The high temporary zero pressure bridge. </value>
    Public Shared ReadOnly Property HighTempZeroPressureBridge As SampleBridge
        Get
            If _HighTempZeroPressureBridge Is Nothing Then
                _HighTempZeroPressureBridge = New SampleBridge(New Double() {563.79, 563.19, 556.32, 573.16}, WheatstoneLayout.Counterclockwise) With {
                    .SupplyVoltage = 5.02155,
                    .SupplyVoltageDrop = 3.21047,
                    .OutputVoltage = 0.013019 ' possibly a transcription error: 0.011728
                    }

                _HighTempZeroPressureBridge.BalanceBridge = New BalanceBridge(_HighTempZeroPressureBridge.Bridge, New Double() {Conductor.OpenConductance, 10.3, Resistor.ShortResistance, 1 / 52357}, BalanceLayout.TopShuntBottomSeries)
                _HighTempZeroPressureBridge.EquivalentResistance = 782
                _HighTempZeroPressureBridge.RelativeOffsetEpsilon = 0.006

                _HighTempZeroPressureBridge.NominalVoltage = 5
                _HighTempZeroPressureBridge.VoltageSourceSeriesResistance = 1538
                _HighTempZeroPressureBridge.VoltageSourceParallelResistance = 1593
                _HighTempZeroPressureBridge.NominalFullScaleVoltage = 0.02
                _HighTempZeroPressureBridge.NominalCurrent = 0.004
                ' 636 this is calculated based on 5v, 0.004 v and the above resistances
                _HighTempZeroPressureBridge.CurrentSourceSeriesResistance = 636
                _HighTempZeroPressureBridge.CurrentSourceParallelResistance = 150
            End If
            Return _HighTempZeroPressureBridge
        End Get
    End Property
    ''' <summary> The high temporary full scale pressure bridge. </summary>
    Private Shared _HighTempFullScalePressureBridge As SampleBridge

    ''' <summary> Gets the high temporary full scale pressure bridge. </summary>
    ''' <value> The high temporary full scale pressure bridge. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared ReadOnly Property HighTempFullScalePressureBridge As SampleBridge
        Get
            If _HighTempFullScalePressureBridge Is Nothing Then
                _HighTempFullScalePressureBridge = New SampleBridge(New Double() {577.21, 555.43, 569.81, 565.23}, WheatstoneLayout.Counterclockwise) With {
                    .SupplyVoltage = 5.02152,
                    .SupplyVoltageDrop = 3.20471,
                    .OutputVoltage = 0.02001
                }

                _HighTempFullScalePressureBridge.BalanceBridge = New BalanceBridge(_HighTempFullScalePressureBridge.Bridge, New Double() {Conductor.OpenConductance, 10.3, Resistor.ShortResistance, 1 / 52357}, BalanceLayout.TopShuntBottomSeries)
                _HighTempFullScalePressureBridge.EquivalentResistance = 782
                _HighTempFullScalePressureBridge.RelativeOffsetEpsilon = 0.005

                _HighTempFullScalePressureBridge.NominalVoltage = 5
                _HighTempFullScalePressureBridge.VoltageSourceSeriesResistance = 1538
                _HighTempFullScalePressureBridge.VoltageSourceParallelResistance = 1593
                _HighTempFullScalePressureBridge.NominalFullScaleVoltage = 0.02
                _HighTempFullScalePressureBridge.NominalCurrent = 0.004
                ' 636 this is calculated based on 5v, 0.004 v and the above resistances
                _HighTempFullScalePressureBridge.CurrentSourceSeriesResistance = 636
                _HighTempFullScalePressureBridge.CurrentSourceParallelResistance = 150
            End If
            Return _HighTempFullScalePressureBridge
        End Get
    End Property

End Class
