Imports isr.Core.Engineering

''' <summary> Unit tests for the attenuated voltage source. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/18/2017 </para>
''' </remarks>
<TestClass()>
Public Class AttenuatedVoltageSourceTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        Assert.IsTrue(VoltageSourceTestInfo.Get.Exists, $"{GetType(VoltageSourceTestInfo)} settings should exist")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext
    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " CONSTRUCTION TESTS "

    ''' <summary> (Unit Test Method) tests building an attenuated voltage source. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub BuildTest()

        Dim attenuation As Double = AttenuatedVoltageSource.ToAttenuation(VoltageSourceTestInfo.Get.SeriesResistance, VoltageSourceTestInfo.Get.ParallelConductance)
        Dim equivalenctResistance As Double = Resistor.Parallel(VoltageSourceTestInfo.Get.SeriesResistance, VoltageSourceTestInfo.Get.ParallelResistance)

        ' construct a voltage source 
        Dim voltageSource As VoltageSource

        Dim attenuatedSource As AttenuatedVoltageSource = Nothing
        Dim doubleEpsilon As Double = 0.0000000001
        Dim attenuatedSourceTarget As String = String.Empty
        Dim sourceTarget As String = String.Empty

        For i As Integer = 1 To 3
            Select Case i
                Case 1
                    attenuatedSourceTarget = "nominal attenuated voltage source"
                    attenuatedSource = New AttenuatedVoltageSource(CDec(VoltageSourceTestInfo.Get.NominalVoltage), VoltageSourceTestInfo.Get.SeriesResistance, VoltageSourceTestInfo.Get.ParallelConductance)
                    sourceTarget = "voltage source from nominal"
                Case 2
                    attenuatedSourceTarget = "equivalent attenuated voltage source"
                    attenuatedSource = New AttenuatedVoltageSource(CDbl(VoltageSourceTestInfo.Get.NominalVoltage / attenuation), VoltageSourceTestInfo.Get.SeriesResistance, VoltageSourceTestInfo.Get.ParallelConductance)
                    sourceTarget = "voltage source from equivalent"
                Case 3
                    attenuatedSourceTarget = "converted attenuated voltage source"
                    voltageSource = New VoltageSource(VoltageSourceTestInfo.Get.NominalVoltage / attenuation, equivalenctResistance)
                    attenuatedSource = VoltageSource.ToAttenuatedVoltageSource(voltageSource, attenuation)
                    sourceTarget = "voltage source from converted"
            End Select
            voltageSource = attenuatedSource.ToVoltageSource

            ' test the attenuated voltage source
            Dim target As String = attenuatedSourceTarget

            Dim actualValue As Double = attenuatedSource.NominalVoltage
            Dim expectedValue As Double = VoltageSourceTestInfo.Get.NominalVoltage
            Dim outcome As String = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            Dim item As String = "nominal voltage"
            Dim message As String = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

            target = attenuatedSourceTarget
            actualValue = attenuatedSource.Voltage
            expectedValue = VoltageSourceTestInfo.Get.NominalVoltage / attenuation
            outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            item = "open load voltage"
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

            target = sourceTarget
            actualValue = voltageSource.Voltage
            expectedValue = VoltageSourceTestInfo.Get.NominalVoltage / attenuation
            item = "open load voltage"
            outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

            target = attenuatedSourceTarget
            actualValue = attenuatedSource.Resistance
            expectedValue = equivalenctResistance
            item = "equivalent resistance"
            outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

            target = sourceTarget
            actualValue = voltageSource.Resistance
            expectedValue = equivalenctResistance
            item = "equivalent resistance"
            outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

            target = attenuatedSourceTarget
            actualValue = attenuatedSource.SeriesResistance
            expectedValue = VoltageSourceTestInfo.Get.SeriesResistance
            outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            item = "series resistance"
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

            target = attenuatedSourceTarget
            actualValue = attenuatedSource.Attenuation
            expectedValue = attenuation
            item = "attenuation"
            outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

            target = attenuatedSourceTarget
            actualValue = 1 / attenuatedSource.ParallelConductance
            expectedValue = VoltageSourceTestInfo.Get.ParallelResistance
            item = "parallel resistance"
            outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

        Next


    End Sub

    ''' <summary>
    ''' (Unit Test Method) tests exception for converting a voltage source to an attenuated voltage
    ''' source with invalid attenuation.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    <ExpectedException(GetType(ArgumentOutOfRangeException), "Attenuation smaller than 1 inappropriately allowed")>
    Public Sub ConvertAttenuationExceptionTest()
        Dim attenuation As Double = AttenuatedVoltageSource.MinimumAttenuation - 0.01
        Dim voltageSource As VoltageSource = New VoltageSource(VoltageSourceTestInfo.Get.NominalVoltage, VoltageSourceTestInfo.Get.SourceResistance)
        Dim source As AttenuatedVoltageSource = VoltageSource.ToAttenuatedVoltageSource(voltageSource, attenuation)
    End Sub

    ''' <summary>
    ''' (Unit Test Method) tests exception for converting to an attenuated source using a null source.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    <ExpectedException(GetType(ArgumentNullException), "Conversion with null source allowed")>
    Public Sub ConvertNothingExceptionTest()
        Dim attenuation As Double = AttenuatedVoltageSource.MinimumAttenuation + 0.01
        Dim voltageSource As VoltageSource = Nothing
        Dim source As AttenuatedVoltageSource = VoltageSource.ToAttenuatedVoltageSource(voltageSource, attenuation)

    End Sub

#End Region

#Region " LOAD AND PROPERTIES TESTS "

    ''' <summary> (Unit Test Method) tests loading an attenuated voltage source. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    Public Sub LoadTest()

        Dim source As AttenuatedVoltageSource = New AttenuatedVoltageSource(CDec(VoltageSourceTestInfo.Get.NominalVoltage), VoltageSourceTestInfo.Get.SeriesResistance, VoltageSourceTestInfo.Get.ParallelConductance)

        Dim doubleEpsilon As Double = 0.0000000001

        ' test the attenuated voltage source
        Dim target As String = "attenuated voltage source"

        ' test open load voltage
        Dim item As String = "open load voltage"
        Dim actualValue As Double = source.LoadVoltage(Resistor.OpenResistance)
        Dim expectedValue As Double = CDec(VoltageSourceTestInfo.Get.NominalVoltage) / source.Attenuation
        Dim outcome As String = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
        Dim message As String = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
        Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
        TestInfo.VerboseMessage(message)

        ' test short load voltage
        item = "short load voltage"
        actualValue = source.LoadVoltage(0)
        expectedValue = 0
        outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
        Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
        TestInfo.VerboseMessage(message)

        ' test load voltage
        item = "load voltage"
        actualValue = source.LoadVoltage(VoltageSourceTestInfo.Get.LoadResistance)
        expectedValue = source.NominalVoltage * Resistor.OutputVoltage(VoltageSourceTestInfo.Get.SeriesResistance, Resistor.Parallel(VoltageSourceTestInfo.Get.LoadResistance,
                                                                                                                    VoltageSourceTestInfo.Get.ParallelResistance))
        outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
        Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
        TestInfo.VerboseMessage(message)


        ' test load current
        item = "load current"
        actualValue = source.LoadCurrent(VoltageSourceTestInfo.Get.LoadResistance)
        expectedValue = source.NominalVoltage * Resistor.OutputVoltage(VoltageSourceTestInfo.Get.SeriesResistance, Resistor.Parallel(VoltageSourceTestInfo.Get.LoadResistance,
                                                                                                                     VoltageSourceTestInfo.Get.ParallelResistance)) /
                                                                                                                     VoltageSourceTestInfo.Get.LoadResistance
        outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
        Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
        TestInfo.VerboseMessage(message)

    End Sub

    ''' <summary>
    ''' (Unit Test Method) tests changing properties of an attenuated voltage source.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    Public Sub PropertiesChangeTest()

        ' test changing Attenuated voltage source properties
        Dim source As AttenuatedVoltageSource = New AttenuatedVoltageSource(CDec(VoltageSourceTestInfo.Get.NominalVoltage),
                                                                                VoltageSourceTestInfo.Get.SeriesResistance,
                                                                                VoltageSourceTestInfo.Get.ParallelConductance)
        Dim doubleEpsilon As Double = 0.0000000001

        ' test the attenuated voltage source
        Dim target As String = "attenuated voltage source"

        Dim voltageGain As Double = 1.5

        ' test open voltage
        Dim item As String = $"open voltage {voltageGain}={NameOf(voltageGain)}"
        source.NominalVoltage *= voltageGain
        Dim actualValue As Double = source.LoadVoltage(Resistor.OpenResistance)
        Dim expectedValue As Double = voltageGain * VoltageSourceTestInfo.Get.NominalVoltage / source.Attenuation
        Dim outcome As String = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
        Dim message As String = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
        Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
        TestInfo.VerboseMessage(message)

        ' test short voltage
        item = $"short voltage {voltageGain}={NameOf(voltageGain)}"
        actualValue = source.LoadVoltage(0)
        expectedValue = 0
        outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
        Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
        TestInfo.VerboseMessage(message)

        ' test load voltage
        item = $"load voltage {voltageGain}={NameOf(voltageGain)}"
        actualValue = source.LoadVoltage(VoltageSourceTestInfo.Get.LoadResistance)
        expectedValue = voltageGain * VoltageSourceTestInfo.Get.NominalVoltage * Resistor.OutputVoltage(VoltageSourceTestInfo.Get.SeriesResistance, Resistor.Parallel(VoltageSourceTestInfo.Get.LoadResistance,
                                                                                                                    VoltageSourceTestInfo.Get.ParallelResistance))
        outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
        Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
        TestInfo.VerboseMessage(message)

        ' test load current
        item = $"load current {voltageGain}={NameOf(voltageGain)}"
        actualValue = source.LoadCurrent(VoltageSourceTestInfo.Get.LoadResistance)
        expectedValue = voltageGain * VoltageSourceTestInfo.Get.NominalVoltage * Resistor.OutputVoltage(VoltageSourceTestInfo.Get.SeriesResistance, Resistor.Parallel(VoltageSourceTestInfo.Get.LoadResistance,
                                                                                                                     VoltageSourceTestInfo.Get.ParallelResistance)) / VoltageSourceTestInfo.Get.LoadResistance
        outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
        Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
        TestInfo.VerboseMessage(message)

        source.NominalVoltage /= voltageGain
        Dim seriesResistanceGain As Double = 1.2
        source.SeriesResistance *= seriesResistanceGain

        item = $"open voltage {seriesResistanceGain}={NameOf(seriesResistanceGain)}"
        actualValue = source.LoadVoltage(Resistor.OpenResistance)
        expectedValue = VoltageSourceTestInfo.Get.NominalVoltage / source.Attenuation
        outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
        Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
        TestInfo.VerboseMessage(message)

        ' test short voltage
        item = $"short voltage {seriesResistanceGain}={NameOf(seriesResistanceGain)}"
        actualValue = source.LoadVoltage(0)
        expectedValue = 0
        outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
        Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
        TestInfo.VerboseMessage(message)

        ' test load voltage
        item = $"load voltage {seriesResistanceGain}={NameOf(seriesResistanceGain)}"
        actualValue = source.LoadVoltage(VoltageSourceTestInfo.Get.LoadResistance)
        expectedValue = VoltageSourceTestInfo.Get.NominalVoltage * Resistor.OutputVoltage(seriesResistanceGain * VoltageSourceTestInfo.Get.SeriesResistance,
                                                                                       Resistor.Parallel(VoltageSourceTestInfo.Get.LoadResistance,
                                                                                                         VoltageSourceTestInfo.Get.ParallelResistance))
        outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
        Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
        TestInfo.VerboseMessage(message)

        ' test load current
        Dim unused As Double = source.LoadCurrent(VoltageSourceTestInfo.Get.LoadResistance)
        Dim unused1 As Double = VoltageSourceTestInfo.Get.NominalVoltage *
                                    Resistor.OutputVoltage(seriesResistanceGain * VoltageSourceTestInfo.Get.SeriesResistance,
                                                           Resistor.Parallel(VoltageSourceTestInfo.Get.LoadResistance, VoltageSourceTestInfo.Get.ParallelResistance)) /
                                                           VoltageSourceTestInfo.Get.LoadResistance

        item = $"load current {seriesResistanceGain}={NameOf(seriesResistanceGain)}"
        actualValue = source.LoadCurrent(VoltageSourceTestInfo.Get.LoadResistance)
        expectedValue = VoltageSourceTestInfo.Get.NominalVoltage * Resistor.OutputVoltage(seriesResistanceGain * VoltageSourceTestInfo.Get.SeriesResistance,
                                                                         Resistor.Parallel(VoltageSourceTestInfo.Get.LoadResistance, VoltageSourceTestInfo.Get.ParallelResistance)) /
                                                                         VoltageSourceTestInfo.Get.LoadResistance
        outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
        Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
        TestInfo.VerboseMessage(message)

        Dim parallelConductanceGain As Double = 1.2
        source.SeriesResistance /= seriesResistanceGain
        source.ParallelConductance *= parallelConductanceGain

        ' test open voltage
        item = $"open voltage {parallelConductanceGain}={NameOf(parallelConductanceGain)}"
        actualValue = source.LoadVoltage(Resistor.OpenResistance)
        expectedValue = VoltageSourceTestInfo.Get.NominalVoltage / source.Attenuation
        outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
        Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
        TestInfo.VerboseMessage(message)

        ' test short voltage
        item = $"short voltage {parallelConductanceGain}={NameOf(parallelConductanceGain)}"
        actualValue = source.LoadVoltage(0)
        expectedValue = 0
        outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
        Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
        TestInfo.VerboseMessage(message)

        ' test load voltage
        item = $"load voltage {parallelConductanceGain}={NameOf(parallelConductanceGain)}"
        actualValue = source.LoadVoltage(VoltageSourceTestInfo.Get.LoadResistance)
        expectedValue = VoltageSourceTestInfo.Get.NominalVoltage * Resistor.OutputVoltage(VoltageSourceTestInfo.Get.SeriesResistance,
                                                                                       Resistor.Parallel(VoltageSourceTestInfo.Get.LoadResistance,
                                                                                                         VoltageSourceTestInfo.Get.ParallelResistance /
                                                                                                         parallelConductanceGain))
        outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
        Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
        TestInfo.VerboseMessage(message)

        ' test load current
        item = $"load current {parallelConductanceGain}={NameOf(parallelConductanceGain)}"
        actualValue = source.LoadCurrent(VoltageSourceTestInfo.Get.LoadResistance)
        expectedValue = VoltageSourceTestInfo.Get.NominalVoltage * Resistor.OutputVoltage(VoltageSourceTestInfo.Get.SeriesResistance,
                                                                         Resistor.Parallel(VoltageSourceTestInfo.Get.LoadResistance,
                                                                                           VoltageSourceTestInfo.Get.ParallelResistance / parallelConductanceGain)) /
                                                                                           VoltageSourceTestInfo.Get.LoadResistance
        outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
        Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
        TestInfo.VerboseMessage(message)

        Dim attenuationChange As Double = 1.2
        source.ParallelConductance /= parallelConductanceGain
        Dim newAttenuation As Double = source.Attenuation * attenuationChange
        Dim seriesR As Double = newAttenuation * source.Resistance
        Dim parallelR As Double = seriesR / (newAttenuation - 1)
        Dim newNominal As Double = newAttenuation * VoltageSourceTestInfo.Get.NominalVoltage / source.Attenuation
        source.Attenuation *= attenuationChange

        ' test open voltage
        item = $"open voltage {attenuationChange}={NameOf(attenuationChange)}"
        actualValue = source.LoadVoltage(Resistor.OpenResistance)
        expectedValue = newNominal / source.Attenuation
        outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
        Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
        TestInfo.VerboseMessage(message)

        ' test short voltage
        item = $"short voltage {attenuationChange}={NameOf(attenuationChange)}"
        actualValue = source.LoadVoltage(0)
        expectedValue = 0
        outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
        Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
        TestInfo.VerboseMessage(message)

        ' test load voltage
        item = $"load voltage {attenuationChange}={NameOf(attenuationChange)}"
        actualValue = source.LoadVoltage(VoltageSourceTestInfo.Get.LoadResistance)
        expectedValue = newNominal * Resistor.OutputVoltage(seriesR, Resistor.Parallel(VoltageSourceTestInfo.Get.LoadResistance, parallelR))
        outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
        Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
        TestInfo.VerboseMessage(message)

        ' test load current
        item = $"load current {attenuationChange}={NameOf(attenuationChange)}"
        actualValue = source.LoadCurrent(VoltageSourceTestInfo.Get.LoadResistance)
        expectedValue = newNominal * Resistor.OutputVoltage(seriesR, Resistor.Parallel(VoltageSourceTestInfo.Get.LoadResistance, parallelR)) / VoltageSourceTestInfo.Get.LoadResistance
        outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
        Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
        TestInfo.VerboseMessage(message)

    End Sub

#End Region

End Class

