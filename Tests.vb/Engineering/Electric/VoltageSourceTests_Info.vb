Imports isr.Core.Engineering

''' <summary> A VoltageSource Test Info. </summary>
''' <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2/12/2018 </para></remarks>
<Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute(),
     Global.System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "16.7.0.0"),
     Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>
Friend Class VoltageSourceTestInfo
    Inherits isr.Core.ApplicationSettingsBase

#Region " SINGLETON "

    ''' <summary>
    ''' Initializes an instance of the <see cref="T:System.Configuration.ApplicationSettingsBase" />
    ''' class to its default state.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Private Sub New()
        MyBase.New
    End Sub

    ''' <summary>
    ''' Gets the locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    ''' <value> The sync locker. </value>
    Private Shared Property _SyncLocker As New Object

    ''' <summary> Gets the instance. </summary>
    ''' <value> The instance. </value>
    Private Shared Property _Instance As VoltageSourceTestInfo

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    Public Shared Function [Get]() As VoltageSourceTestInfo
        If _Instance Is Nothing Then
            SyncLock _SyncLocker
                _Instance = CType(Global.System.Configuration.ApplicationSettingsBase.Synchronized(New VoltageSourceTestInfo()), VoltageSourceTestInfo)
            End SyncLock
        End If
        Return _Instance
    End Function

    ''' <summary> Returns true if an instance of the class was created and not disposed. </summary>
    ''' <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
    Public Shared ReadOnly Property Instantiated() As Boolean
        Get
            SyncLock _SyncLocker
                Return _Instance IsNot Nothing
            End SyncLock
        End Get
    End Property

#End Region

#Region " SETTINGS EDITORS  EXCLUDED "

    ''' <summary> Opens the settings editor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Public Shared Sub OpenSettingsEditor()
        isr.Core.WindowsForms.EditConfiguration($"{GetType(VoltageSourceTestInfo)} Editor", VoltageSourceTestInfo.Get)
    End Sub

#End Region

#Region " CONFIGURATION INFORMATION "

    ''' <summary> Returns true if test settings exist. </summary>
    ''' <value> <c>True</c> if testing settings exit. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Property Exists As Boolean
        Get
            Return Me.AppSettingGetter(False)
        End Get
        Set
            Me.AppSettingSetter(Value)
        End Set
    End Property

    ''' <summary> Returns true to output test messages at the verbose level. </summary>
    ''' <value> The verbose messaging level. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("False")>
    Public Property Verbose As Boolean
        Get
            Return Me.AppSettingGetter(False)
        End Get
        Set
            Me.AppSettingSetter(Value)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the sentinel indicating of all data are to be used for a test.
    ''' </summary>
    ''' <value> <c>true</c> if all data are to be used for a test; otherwise <c>false</c>. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Property All As Boolean
        Get
            Return Me.AppSettingGetter(False)
        End Get
        Set
            Me.AppSettingSetter(Value)
        End Set
    End Property

    ''' <summary> True if the test set is enabled. </summary>
    ''' <value> The enabled option. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Property Enabled As Boolean
        Get
            Return Me.AppSettingGetter(False)
        End Get
        Set
            Me.AppSettingSetter(Value)
        End Set
    End Property

#End Region

#Region " VOLTAGE SOURCE TEST SETTINGS "

    ''' <summary> Gets or sets the nominal voltage. </summary>
    ''' <value> The nominal voltage. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("5.0")>
    Public Property NominalVoltage As Double
        Get
            Return Me.AppSettingGetter(Double.NaN)
        End Get
        Set(value As Double)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets source resistance. </summary>
    ''' <value> The source resistance. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("1000")>
    Public Property SourceResistance As Double
        Get
            Return Me.AppSettingGetter(Double.NaN)
        End Get
        Set(value As Double)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the load resistance. </summary>
    ''' <value> The load resistance. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("700")>
    Public Property LoadResistance As Double
        Get
            Return Me.AppSettingGetter(Double.NaN)
        End Get
        Set(value As Double)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets the load conductance. </summary>
    ''' <value> The load conductance. </value>
    Public ReadOnly Property LoadConductance As Double
        Get
            Return Resistor.ToConductance(Me.LoadResistance)
        End Get
    End Property

    ''' <summary> Gets or sets the nominal current. </summary>
    ''' <value> The nominal current. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("0.004")>
    Public Property NominalCurrent As Double
        Get
            Return Me.AppSettingGetter(Double.NaN)
        End Get
        Set(value As Double)
            Me.AppSettingSetter(value)
        End Set
    End Property

#End Region

#Region " ATTENUATED VOLTAGE SOURCE TEST SETTINGS "

    ''' <summary> Gets or sets the series resistance. </summary>
    ''' <value> The series resistance. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("500")>
    Public Property SeriesResistance As Double
        Get
            Return Me.AppSettingGetter(Double.NaN)
        End Get
        Set(value As Double)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the parallel resistance. </summary>
    ''' <value> The parallel resistance. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("1100")>
    Public Property ParallelResistance As Double
        Get
            Return Me.AppSettingGetter(Double.NaN)
        End Get
        Set(value As Double)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets the parallel conductance. </summary>
    ''' <value> The parallel conductance. </value>
    Public ReadOnly Property ParallelConductance As Double
        Get
            Return Resistor.ToConductance(Me.ParallelResistance)
        End Get
    End Property

#End Region

End Class
