﻿Imports isr.Core.Engineering

''' <summary> A conductance unit tests. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/16/2017 </para>
''' </remarks>
<TestClass()>
Public Class ConductorTests

#Region " TEST INPUTS "

    ''' <summary> Gets or sets the left conductance. </summary>
    ''' <value> The left conductance. </value>
    Public Shared Property LeftConductance As Double = 0.001

    ''' <summary> Gets or sets the right conductance. </summary>
    ''' <value> The right conductance. </value>
    Public Shared Property RightConductance As Double = 0.002

    ''' <summary> Gets or sets the load conductance. </summary>
    ''' <value> The load conductance. </value>
    Public Shared Property LoadConductance As Double = 0.004

    ''' <summary> Gets or sets the negative conductance. </summary>
    ''' <value> The negative conductance. </value>
    Public Shared Property NegativeConductance As Double = -0.001

#End Region

#Region " PARALLEL TESTS "

    ''' <summary> (Unit Test Method) tests parallel. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub ParallelTest()

        Dim actualValue As Double = Conductor.ToParallel(ConductorTests.LeftConductance, ConductorTests.RightConductance)
        Dim expectedValue As Double = ConductorTests.LeftConductance + ConductorTests.RightConductance
        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    ''' <summary> (Unit Test Method) tests parallel short. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub ParallelShortTest()

        Dim actualValue As Double = Conductor.ToParallel(Conductor.ShortConductance, ConductorTests.RightConductance)
        Dim expectedValue As Double = Conductor.ShortConductance
        Assert.AreEqual(expectedValue, actualValue)

        actualValue = Conductor.ToParallel(ConductorTests.LeftConductance, Conductor.ShortConductance)
        expectedValue = Conductor.ShortConductance
        Assert.AreEqual(expectedValue, actualValue)

        actualValue = Conductor.ToParallel(Conductor.ShortConductance, Conductor.ShortConductance)
        expectedValue = Conductor.ShortConductance
        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    ''' <summary> (Unit Test Method) tests parallel open. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub ParallelOpenTest()

        Dim actualValue As Double = Conductor.ToParallel(Conductor.OpenConductance, ConductorTests.RightConductance)
        Dim expectedValue As Double = ConductorTests.RightConductance
        Assert.AreEqual(expectedValue, actualValue)

        actualValue = Conductor.ToParallel(ConductorTests.LeftConductance, Conductor.OpenConductance)
        expectedValue = ConductorTests.LeftConductance
        Assert.AreEqual(expectedValue, actualValue)

        actualValue = Conductor.ToParallel(Conductor.OpenConductance, Conductor.OpenConductance)
        expectedValue = Conductor.OpenConductance
        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    ''' <summary> (Unit Test Method) tests parallel negative left conductance exception. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    <ExpectedException(GetType(ArgumentOutOfRangeException), "Negative conductance inappropriately allowed")>
    Public Sub ParallelNegativeLeftConductanceExceptionTest()
        Conductor.ToParallel(ConductorTests.NegativeConductance, ConductorTests.RightConductance)
    End Sub

    ''' <summary> (Unit Test Method) tests parallel negative right conductance exception. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    <ExpectedException(GetType(ArgumentOutOfRangeException), "Negative conductance inappropriately allowed")>
    Public Sub ParallelNegativeRightConductanceExceptionTest()
        Conductor.ToParallel(ConductorTests.LeftConductance, ConductorTests.NegativeConductance)
    End Sub

#End Region

#Region " SERIES TESTS "

    ''' <summary> (Unit Test Method) tests series. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub SeriesTest()
        Dim actualValue As Double = Conductor.ToSeries(ConductorTests.LeftConductance, ConductorTests.RightConductance)
        Dim expectedValue As Double = 1 / (1 / ConductorTests.LeftConductance + 1 / ConductorTests.RightConductance)
        Assert.AreEqual(expectedValue, actualValue)
    End Sub

    ''' <summary> (Unit Test Method) tests series short. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub SeriesShortTest()
        Dim actualValue As Double = Conductor.ToSeries(Conductor.ShortConductance, ConductorTests.RightConductance)
        Dim expectedValue As Double = ConductorTests.RightConductance
        Assert.AreEqual(expectedValue, actualValue)
        actualValue = Conductor.ToSeries(ConductorTests.LeftConductance, Conductor.ShortConductance)
        expectedValue = ConductorTests.LeftConductance
        Assert.AreEqual(expectedValue, actualValue)
        actualValue = Conductor.ToSeries(Conductor.ShortConductance, Conductor.ShortConductance)
        expectedValue = Conductor.ShortConductance
        Assert.AreEqual(expectedValue, actualValue)
    End Sub

    ''' <summary> (Unit Test Method) tests series open. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub SeriesOpenTest()
        Dim actualValue As Double = Conductor.ToSeries(Conductor.OpenConductance, ConductorTests.RightConductance)
        Dim expectedValue As Double = Conductor.OpenConductance
        Assert.AreEqual(expectedValue, actualValue)
        actualValue = Conductor.ToSeries(ConductorTests.LeftConductance, Conductor.OpenConductance)
        expectedValue = Conductor.OpenConductance
        Assert.AreEqual(expectedValue, actualValue)
        actualValue = Conductor.ToSeries(Conductor.OpenConductance, Conductor.OpenConductance)
        expectedValue = Conductor.OpenConductance
        Assert.AreEqual(expectedValue, actualValue)
    End Sub

    ''' <summary> (Unit Test Method) tests series negative left conductance exception. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    <ExpectedException(GetType(ArgumentOutOfRangeException), "Negative conductance inappropriately allowed")>
    Public Sub SeriesNegativeLeftConductanceExceptionTest()
        Conductor.ToSeries(ConductorTests.NegativeConductance, ConductorTests.RightConductance)
    End Sub

    ''' <summary> (Unit Test Method) tests series negative right conductance exception. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    <ExpectedException(GetType(ArgumentOutOfRangeException), "Negative conductance inappropriately allowed")>
    Public Sub SeriesNegativeRightConductanceExceptionTest()
        Conductor.ToSeries(ConductorTests.LeftConductance, ConductorTests.NegativeConductance)
    End Sub

#End Region

#Region " OUTPUT CURRENT TESTS "

    ''' <summary> (Unit Test Method) tests output current. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub OutputCurrentTest()
        Dim actualValue As Double = Conductor.ToOutputCurrent(ConductorTests.LeftConductance, ConductorTests.RightConductance)
        Dim expectedValue As Double = ConductorTests.RightConductance /
                                            Conductor.Parallel(ConductorTests.LeftConductance, ConductorTests.RightConductance)
        Assert.AreEqual(expectedValue, actualValue)
    End Sub

    ''' <summary> (Unit Test Method) tests output current short. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub OutputCurrentShortTest()
        Dim actualValue As Double = Conductor.ToOutputCurrent(Conductor.ShortConductance, ConductorTests.RightConductance)
        Dim expectedValue As Double = 0
        Assert.AreEqual(expectedValue, actualValue)
        actualValue = Conductor.ToOutputCurrent(ConductorTests.LeftConductance, Conductor.ShortConductance)
        expectedValue = 1
        Assert.AreEqual(expectedValue, actualValue)
    End Sub

    ''' <summary> (Unit Test Method) tests output current short exception. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    <ExpectedException(GetType(InvalidOperationException), "Two shorts inappropriately allowed")>
    Public Sub OutputCurrentShortsExceptionTest()
        Conductor.ToOutputCurrent(Conductor.ShortConductance, Conductor.ShortConductance)
    End Sub

    ''' <summary> (Unit Test Method) tests output current open. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub OutputCurrentOpenTest()
        Dim actualValue As Double = Conductor.ToOutputCurrent(Conductor.OpenConductance, ConductorTests.RightConductance)
        Dim expectedValue As Double = 1
        Assert.AreEqual(expectedValue, actualValue)
        actualValue = Conductor.ToOutputCurrent(ConductorTests.LeftConductance, Conductor.OpenConductance)
        expectedValue = 0
        Assert.AreEqual(expectedValue, actualValue)
    End Sub

    ''' <summary> (Unit Test Method) tests output current open exception. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    <ExpectedException(GetType(InvalidOperationException), "Two opens inappropriately allowed")>
    Public Sub OutputCurrentOpensExceptionTest()
        Conductor.ToOutputCurrent(Conductor.OpenConductance, Conductor.OpenConductance)
    End Sub

    ''' <summary>
    ''' (Unit Test Method) tests output current negative left conductance exception.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    <ExpectedException(GetType(ArgumentOutOfRangeException), "Negative conductance inappropriately allowed")>
    Public Sub OutputCurrentNegativeLeftConductanceExceptionTest()
        Conductor.ToOutputCurrent(ConductorTests.NegativeConductance, ConductorTests.RightConductance)
    End Sub

    ''' <summary>
    ''' (Unit Test Method) tests output current negative right conductance exception.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    <ExpectedException(GetType(ArgumentOutOfRangeException), "Negative conductance inappropriately allowed")>
    Public Sub OutputCurrentNegativeRightConductanceExceptionTest()
        Conductor.ToOutputCurrent(ConductorTests.LeftConductance, ConductorTests.NegativeConductance)
    End Sub

#End Region

#Region " OUTPUT VOLTAGE TESTS "

    ''' <summary> (Unit Test Method) tests output voltage. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub OutputVoltageTest()

        Dim actualValue As Double = Conductor.ToOutputVoltage(ConductorTests.LeftConductance, ConductorTests.RightConductance)
        Dim expectedValue As Double = (1 / ConductorTests.RightConductance) *
                                            Conductor.ToSeries(ConductorTests.LeftConductance, ConductorTests.RightConductance)
        Assert.AreEqual(expectedValue, actualValue)
    End Sub

    ''' <summary> (Unit Test Method) tests output voltage short. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub OutputVoltageShortTest()

        Dim actualValue As Double = Conductor.ToOutputVoltage(Conductor.ShortConductance, ConductorTests.RightConductance)
        Dim expectedValue As Double = 1
        Assert.AreEqual(expectedValue, actualValue)

        actualValue = Conductor.ToOutputVoltage(ConductorTests.LeftConductance, Conductor.ShortConductance)
        expectedValue = 0
        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    ''' <summary> (Unit Test Method) tests output voltage shorts exception. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    <ExpectedException(GetType(InvalidOperationException), "Two shorts inappropriately allowed")>
    Public Sub OutputVoltageShortsExceptionTest()
        Conductor.ToOutputVoltage(Conductor.ShortConductance, Conductor.ShortConductance)
    End Sub

    ''' <summary> (Unit Test Method) tests output voltage open. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub OutputVoltageOpenTest()

        Dim actualValue As Double = Conductor.ToOutputVoltage(Conductor.OpenConductance, ConductorTests.RightConductance)
        Dim expectedValue As Double = 0
        Assert.AreEqual(expectedValue, actualValue)

        actualValue = Conductor.ToOutputVoltage(ConductorTests.LeftConductance, Conductor.OpenConductance)
        expectedValue = 1
        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    ''' <summary> (Unit Test Method) tests output voltage opens exception. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    <ExpectedException(GetType(InvalidOperationException), "Two opens inappropriately allowed")>
    Public Sub OutputVoltageOpensExceptionTest()
        Conductor.ToOutputVoltage(Conductor.OpenConductance, Conductor.OpenConductance)
    End Sub

    ''' <summary>
    ''' (Unit Test Method) tests output voltage negative left conductance exception.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    <ExpectedException(GetType(ArgumentOutOfRangeException), "Negative conductance inappropriately allowed")>
    Public Sub OutputVoltageNegativeLeftConductanceExceptionTest()
        Conductor.ToOutputVoltage(ConductorTests.NegativeConductance, ConductorTests.RightConductance)
    End Sub

    ''' <summary>
    ''' (Unit Test Method) tests output voltage negative right conductance exception.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    <ExpectedException(GetType(ArgumentOutOfRangeException), "Negative conductance inappropriately allowed")>
    Public Sub OutputVoltageNegativeRightConductanceExceptionTest()
        Conductor.ToOutputVoltage(ConductorTests.LeftConductance, ConductorTests.NegativeConductance)
    End Sub

#End Region

End Class
