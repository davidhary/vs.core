Imports isr.Core.Engineering

''' <summary> Unit tests for the attenuated current source. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 12/12/2017 </para>
''' </remarks>
<TestClass()>
Public Class AttenuatedCurrentSourceTests

    #Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        Assert.IsTrue(CurrentSourceTestInfo.Get.Exists, $"{GetType(CurrentSourceTestInfo)} settings should exist")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext
    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

    #End Region

    #Region " CONSTRUCTION TESTS "

    ''' <summary> (Unit Test Method) tests build. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    Public Sub BuildTest()

        Dim attenuation As Double = AttenuatedCurrentSource.ToAttenuation(CurrentSourceTestInfo.Get.SeriesResistance, CurrentSourceTestInfo.Get.ParallelConductance)
        Dim equivalenctConductance As Double = Conductor.SeriesResistor(CurrentSourceTestInfo.Get.ParallelConductance, CurrentSourceTestInfo.Get.SeriesResistance)


        ' construct a current source 
        Dim unused As CurrentSource = New CurrentSource(CurrentSourceTestInfo.Get.NominalCurrent, equivalenctConductance)

        Dim attenuatedSource As AttenuatedCurrentSource = Nothing
        Dim doubleEpsilon As Double = 0.0000000001
        Dim attenuatedSourcetarget As String = String.Empty
        Dim sourceTarget As String = String.Empty

        For i As Integer = 1 To 3

            Dim currentSource As CurrentSource
            Select Case i
                Case 1
                    attenuatedSourcetarget = "nominal attenuated current source"
                    attenuatedSource = New AttenuatedCurrentSource(CDec(CurrentSourceTestInfo.Get.NominalCurrent),
                                                                   CurrentSourceTestInfo.Get.SeriesResistance, CurrentSourceTestInfo.Get.ParallelConductance)
                    sourceTarget = "current source from nominal"
                Case 2
                    attenuatedSourcetarget = "equivalent attenuated current source"
                    attenuatedSource = New AttenuatedCurrentSource(CDbl(CurrentSourceTestInfo.Get.NominalCurrent / attenuation),
                                                                   CurrentSourceTestInfo.Get.SeriesResistance, CurrentSourceTestInfo.Get.ParallelConductance)
                    sourceTarget = "current source from equivalent"
                Case 3
                    attenuatedSourcetarget = "converted attenuated current source"
                    currentSource = New CurrentSource(CurrentSourceTestInfo.Get.NominalCurrent / attenuation, equivalenctConductance)
                    attenuatedSource = CurrentSource.ToAttenuatedCurrentSource(currentSource, attenuation)
                    sourceTarget = "current source from converted"
            End Select
            currentSource = attenuatedSource.ToCurrentSource

            ' test the attenuated voltage source
            Dim target As String = attenuatedSourcetarget

            Dim actualValue As Double = attenuatedSource.NominalCurrent
            Dim expectedValue As Double = CurrentSourceTestInfo.Get.NominalCurrent
            Dim outcome As String = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            Dim item As String = "nominal current"
            Dim message As String = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

            target = attenuatedSourcetarget
            actualValue = attenuatedSource.Current
            expectedValue = CurrentSourceTestInfo.Get.NominalCurrent / attenuation
            outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            item = "short load current"
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

            target = sourceTarget
            actualValue = currentSource.Current
            expectedValue = CurrentSourceTestInfo.Get.NominalCurrent / attenuation
            outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            item = "short load current"
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

            target = attenuatedSourcetarget
            actualValue = attenuatedSource.Conductance
            expectedValue = equivalenctConductance
            item = "equivalent conductance"
            outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

            target = sourceTarget
            actualValue = attenuatedSource.Conductance
            expectedValue = equivalenctConductance
            item = "equivalent conductance"
            outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

            target = attenuatedSourcetarget
            actualValue = attenuatedSource.SeriesResistance
            expectedValue = CurrentSourceTestInfo.Get.SeriesResistance
            outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            item = "series resistance"
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

            target = attenuatedSourcetarget
            actualValue = attenuatedSource.Attenuation
            expectedValue = attenuation
            item = "attenuation"
            outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

            target = attenuatedSourcetarget
            actualValue = attenuatedSource.ParallelConductance
            expectedValue = CurrentSourceTestInfo.Get.ParallelConductance
            item = "parallel conductance"
            outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

        Next

    End Sub

    #End Region

    #Region " EXCEPTION CONVERSION TESTS "

    ''' <summary>
    ''' (Unit Test Method) tests exception for converting a current source to an attenuated current
    ''' source with invalid attenuation.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    <ExpectedException(GetType(ArgumentOutOfRangeException), "Attenuation smaller than 1 inappropriately allowed")>
    Public Sub ConvertAttenuationExceptionTest()
        Dim attenuation As Double = AttenuatedVoltageSource.MinimumAttenuation - 0.01
        Dim currentSource As CurrentSource = New CurrentSource(CurrentSourceTestInfo.Get.NominalCurrent, CurrentSourceTestInfo.Get.SourceConductance)
        Dim source As AttenuatedCurrentSource = CurrentSource.ToAttenuatedCurrentSource(currentSource, attenuation)
    End Sub

    ''' <summary>
    ''' (Unit Test Method) tests exception for converting to an attenuated source using a null source.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    <ExpectedException(GetType(ArgumentNullException), "Conversion with null source allowed")>
    Public Sub ConvertNothingExceptionTest()
        Dim attenuation As Double = AttenuatedVoltageSource.MinimumAttenuation + 0.01
        Dim currentSource As CurrentSource = Nothing
        Dim source As AttenuatedCurrentSource = CurrentSource.ToAttenuatedCurrentSource(currentSource, attenuation)
    End Sub

    #End Region

    #Region " LOAD AND PROPERTY CHANGE TESTS "

    ''' <summary> (Unit Test Method) tests loading an attenuated current source. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    Public Sub LoadTest()

        Dim source As AttenuatedCurrentSource = New AttenuatedCurrentSource(CDec(CurrentSourceTestInfo.Get.NominalCurrent),
                                                                                CurrentSourceTestInfo.Get.SeriesResistance,
                                                                                CurrentSourceTestInfo.Get.ParallelConductance)

        Dim doubleEpsilon As Double = 0.0000000001

        ' test the attenuated current source
        Dim target As String = "attenuated current source"

        ' test open current
        Dim item As String = "open load current"
        Dim actualValue As Double = source.LoadCurrent(Conductor.OpenConductance)
        Dim expectedValue As Double = 0
        Dim outcome As String = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
        Dim message As String = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
        Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
        TestInfo.VerboseMessage(message)

        item = "short load current"
        actualValue = source.LoadCurrent(Conductor.ShortConductance)
        expectedValue = CDec(CurrentSourceTestInfo.Get.NominalCurrent) / source.Attenuation
        outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
        Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
        TestInfo.VerboseMessage(message)

        ' test short voltage
        item = "short voltage"
        actualValue = source.LoadVoltage(0)
        expectedValue = 0
        outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
        Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
        TestInfo.VerboseMessage(message)

        ' test load current
        item = "load current"
        actualValue = source.LoadCurrent(Resistor.ToConductance(CurrentSourceTestInfo.Get.LoadResistance))
        expectedValue = CurrentSourceTestInfo.Get.NominalCurrent * Conductor.OutputCurrent(CurrentSourceTestInfo.Get.ParallelConductance,
                                                                        Resistor.ToConductance(Resistor.ToSeries(CurrentSourceTestInfo.Get.LoadResistance,
                                                                                                                 CurrentSourceTestInfo.Get.SeriesResistance)))
        outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
        Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
        TestInfo.VerboseMessage(message)

        ' test load voltage
        item = "load voltage"
        actualValue = source.LoadVoltage(CurrentSourceTestInfo.Get.LoadResistance)
        expectedValue = CurrentSourceTestInfo.Get.NominalCurrent * CurrentSourceTestInfo.Get.LoadResistance *
                                Conductor.OutputCurrent(CurrentSourceTestInfo.Get.ParallelConductance,
                                                        Resistor.ToConductance(Resistor.ToSeries(CurrentSourceTestInfo.Get.LoadResistance,
                                                                                                 CurrentSourceTestInfo.Get.SeriesResistance)))
        outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
        Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
        TestInfo.VerboseMessage(message)

    End Sub

    ''' <summary>
    ''' (Unit Test Method) tests changing properties of an attenuated voltage source.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    Public Sub PropertiesChangeTest()

        ' test changing Attenuated voltage source properties
        Dim source As AttenuatedCurrentSource = New AttenuatedCurrentSource(CDec(CurrentSourceTestInfo.Get.NominalCurrent),
                                                                                CurrentSourceTestInfo.Get.SeriesResistance,
                                                                                CurrentSourceTestInfo.Get.ParallelConductance)
        Dim doubleEpsilon As Double = 0.0000000001

        ' test the attenuated voltage source
        Dim target As String = "attenuated current source"

        Dim currentGain As Double = 1.5

        ' test shot load current
        Dim item As String = $"short load current {currentGain}={NameOf(currentGain)}"
        source.NominalCurrent *= currentGain
        Dim actualValue As Double = source.LoadCurrent(Conductor.ShortConductance)
        Dim expectedValue As Double = currentGain * CurrentSourceTestInfo.Get.NominalCurrent / source.Attenuation
        Dim outcome As String = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
        Dim message As String = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
        Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
        TestInfo.VerboseMessage(message)

        ' test open current
        item = $"open current {currentGain}={NameOf(currentGain)}"
        actualValue = source.LoadCurrent(Conductor.OpenConductance)
        expectedValue = 0
        outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
        Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
        TestInfo.VerboseMessage(message)

        ' test load current
        item = $"load current {currentGain}={NameOf(currentGain)}"
        actualValue = source.LoadCurrent(Resistor.ToConductance(CurrentSourceTestInfo.Get.LoadResistance))
        expectedValue = currentGain * CurrentSourceTestInfo.Get.NominalCurrent * Conductor.OutputCurrent(CurrentSourceTestInfo.Get.ParallelConductance,
                                                                        Resistor.ToConductance(Resistor.ToSeries(CurrentSourceTestInfo.Get.LoadResistance,
                                                                                                                 CurrentSourceTestInfo.Get.SeriesResistance)))
        outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
        Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
        TestInfo.VerboseMessage(message)

        source.NominalCurrent /= currentGain
        Dim seriesResistanceGain As Double = 1.2
        source.SeriesResistance *= seriesResistanceGain

        item = $"short load current {seriesResistanceGain}={NameOf(seriesResistanceGain)}"
        actualValue = source.LoadCurrent(Conductor.ShortConductance)
        expectedValue = CurrentSourceTestInfo.Get.NominalCurrent / source.Attenuation
        outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
        Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
        TestInfo.VerboseMessage(message)

        ' test short current
        item = $"open load current {seriesResistanceGain}={NameOf(seriesResistanceGain)}"
        actualValue = source.LoadCurrent(Conductor.OpenConductance)
        expectedValue = 0
        outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
        Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
        TestInfo.VerboseMessage(message)

        ' test load current
        item = $"load current{seriesResistanceGain}={NameOf(seriesResistanceGain)}"
        actualValue = source.LoadCurrent(Resistor.ToConductance(CurrentSourceTestInfo.Get.LoadResistance))
        expectedValue = CurrentSourceTestInfo.Get.NominalCurrent * Conductor.OutputCurrent(CurrentSourceTestInfo.Get.ParallelConductance,
                                                                       Resistor.ToConductance(Resistor.ToSeries(CurrentSourceTestInfo.Get.LoadResistance,
                                                                                                                seriesResistanceGain * CurrentSourceTestInfo.Get.SeriesResistance)))
        outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
        Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
        TestInfo.VerboseMessage(message)

        ' test load voltage
        item = $"load voltage {seriesResistanceGain}={NameOf(seriesResistanceGain)}"
        actualValue = source.LoadVoltage(CurrentSourceTestInfo.Get.LoadResistance)
        expectedValue = CurrentSourceTestInfo.Get.LoadResistance * CurrentSourceTestInfo.Get.NominalCurrent *
                        Conductor.OutputCurrent(CurrentSourceTestInfo.Get.ParallelConductance,
                                                                       Resistor.ToConductance(Resistor.ToSeries(CurrentSourceTestInfo.Get.LoadResistance,
                                                                                                                seriesResistanceGain * CurrentSourceTestInfo.Get.SeriesResistance)))
        outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
        Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
        TestInfo.VerboseMessage(message)

        Dim parallelConductanceGain As Double = 1.2
        source.SeriesResistance /= seriesResistanceGain
        source.ParallelConductance *= parallelConductanceGain

        ' test short current
        item = $"Short load current {parallelConductanceGain}={NameOf(parallelConductanceGain)}"
        actualValue = source.LoadCurrent(Conductor.ShortConductance)
        expectedValue = CurrentSourceTestInfo.Get.NominalCurrent / source.Attenuation
        outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
        Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
        TestInfo.VerboseMessage(message)

        ' test open current
        item = $"open load current {parallelConductanceGain}={NameOf(parallelConductanceGain)}"
        actualValue = source.LoadCurrent(Conductor.OpenConductance)
        expectedValue = 0
        outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
        Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
        TestInfo.VerboseMessage(message)

        ' test load voltage
        item = $"load voltage {parallelConductanceGain}={NameOf(parallelConductanceGain)}"
        actualValue = source.LoadVoltage(CurrentSourceTestInfo.Get.LoadResistance)
        expectedValue = CurrentSourceTestInfo.Get.LoadResistance * CurrentSourceTestInfo.Get.NominalCurrent *
                        Conductor.OutputCurrent(parallelConductanceGain * CurrentSourceTestInfo.Get.ParallelConductance,
                                                                       Resistor.ToConductance(Resistor.ToSeries(CurrentSourceTestInfo.Get.LoadResistance,
                                                                                                                CurrentSourceTestInfo.Get.SeriesResistance)))
        outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {actualValue}"
        Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
        TestInfo.VerboseMessage(message)

        ' test load current
        item = $"load current {parallelConductanceGain}={NameOf(parallelConductanceGain)}"
        actualValue = source.LoadCurrent(Resistor.ToConductance(CurrentSourceTestInfo.Get.LoadResistance))
        expectedValue = CurrentSourceTestInfo.Get.NominalCurrent *
                        Conductor.OutputCurrent(parallelConductanceGain * CurrentSourceTestInfo.Get.ParallelConductance,
                                                                       Resistor.ToConductance(Resistor.ToSeries(CurrentSourceTestInfo.Get.LoadResistance,
                                                                                                                CurrentSourceTestInfo.Get.SeriesResistance)))
        outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {actualValue}"
        Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
        TestInfo.VerboseMessage(message)

        Dim attenuationChange As Double = 1.2
        source.ParallelConductance /= parallelConductanceGain
        Dim newAttenuation As Double = source.Attenuation * attenuationChange
        Dim parallelR As Double = Conductor.ToResistance(source.Conductance * newAttenuation)
        Dim seriesR As Double = 1 / source.Conductance - parallelR
        Dim newNominal As Double = newAttenuation * CurrentSourceTestInfo.Get.NominalCurrent / source.Attenuation
        source.Attenuation *= attenuationChange

        ' test short current
        item = $"short load current {attenuationChange}={NameOf(attenuationChange)}"
        actualValue = source.LoadCurrent(Conductor.ShortConductance)
        expectedValue = newNominal / source.Attenuation
        outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
        Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
        TestInfo.VerboseMessage(message)

        ' test open load current
        item = $"open load current {attenuationChange}={NameOf(attenuationChange)}"
        actualValue = source.LoadCurrent(Conductor.OpenConductance)
        expectedValue = 0
        outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
        Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
        TestInfo.VerboseMessage(message)

        ' test load voltage
        item = $"load voltage {attenuationChange}={NameOf(attenuationChange)}"
        actualValue = source.LoadVoltage(CurrentSourceTestInfo.Get.LoadResistance)
        expectedValue = CurrentSourceTestInfo.Get.LoadResistance * newNominal * Conductor.OutputCurrent(Resistor.ToConductance(parallelR),
                                                                                                             Resistor.ToConductance(Resistor.ToSeries(CurrentSourceTestInfo.Get.LoadResistance, seriesR)))
        outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {actualValue}"
        Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
        TestInfo.VerboseMessage(message)

        ' test load current
        item = $"load current {attenuationChange}={NameOf(attenuationChange)}"
        actualValue = source.LoadCurrent(Resistor.ToConductance(CurrentSourceTestInfo.Get.LoadResistance))
        expectedValue = newNominal * Conductor.OutputCurrent(Resistor.ToConductance(parallelR), Resistor.ToConductance(Resistor.ToSeries(CurrentSourceTestInfo.Get.LoadResistance, seriesR)))
        outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {actualValue}"
        Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
        TestInfo.VerboseMessage(message)

    End Sub

    #End Region

End Class

