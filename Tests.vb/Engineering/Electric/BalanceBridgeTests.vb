Imports isr.Core.Engineering

''' <summary> A balance bridge unit tests. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/23/2017 </para>
''' </remarks>
<TestClass()>
Public Class BalanceBridgeTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext
    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " CONSTRUCTION TESTS "

    ''' <summary> (Unit Test Method) tests build balance bridge. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub BuildBalanceBridgeTest()

        Dim sampleBridge As SampleBridge = TestBridges.LowTempZeroPressureBridge.Clone
        Dim bridge As BalanceBridge = sampleBridge.BalanceBridge

        ' test expected values
        Dim expectedResistance As Double = 0
        If bridge.BridgeBalance.Layout = BalanceLayout.TopShuntBottomSeries Then
            expectedResistance = Resistor.ShuntConductor(bridge.NakedBridge.TopRight, bridge.BridgeBalance.Values(0))
        ElseIf bridge.BridgeBalance.Layout = BalanceLayout.RightShuntRightSeries Then
            expectedResistance = Resistor.ShuntConductor(bridge.NakedBridge.TopRight, bridge.BridgeBalance.Values(0)) + bridge.BridgeBalance.Values(1)
        End If
        Dim actualResistance As Double = bridge.TopRight
        Assert.AreEqual(expectedResistance, actualResistance, TestBridges.ElementEpsilon)

        If bridge.BridgeBalance.Layout = BalanceLayout.TopShuntBottomSeries Then
            expectedResistance = Resistor.ToSeries(bridge.NakedBridge.BottomRight, bridge.BridgeBalance.Values(1))
        ElseIf bridge.BridgeBalance.Layout = BalanceLayout.RightShuntRightSeries Then
            expectedResistance = Resistor.ShuntConductor(bridge.NakedBridge.BottomRight, bridge.BridgeBalance.Values(3)) + bridge.BridgeBalance.Values(2)
        End If
        actualResistance = bridge.BottomRight
        Assert.AreEqual(expectedResistance, actualResistance, TestBridges.ElementEpsilon)

        If bridge.BridgeBalance.Layout = BalanceLayout.TopShuntBottomSeries Then
            expectedResistance = Resistor.ShuntConductor(bridge.NakedBridge.TopLeft, bridge.BridgeBalance.Values(3))
        ElseIf bridge.BridgeBalance.Layout = BalanceLayout.RightShuntRightSeries Then
            expectedResistance = bridge.NakedBridge.TopLeft
        End If
        actualResistance = bridge.TopLeft
        Assert.AreEqual(expectedResistance, actualResistance, TestBridges.ElementEpsilon)

        If bridge.BridgeBalance.Layout = BalanceLayout.TopShuntBottomSeries Then
            expectedResistance = Resistor.ToSeries(bridge.NakedBridge.BottomLeft, bridge.BridgeBalance.Values(2))
        ElseIf bridge.BridgeBalance.Layout = BalanceLayout.RightShuntRightSeries Then
            expectedResistance = bridge.NakedBridge.BottomLeft
        End If
        actualResistance = bridge.BottomLeft
        Assert.AreEqual(expectedResistance, actualResistance, TestBridges.ElementEpsilon)

    End Sub

    ''' <summary> (Unit Test Method) tests build balance bridge. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub BuildInitialBalanceBridgeTest()

        Dim sampleBridge As SampleBridge = TestBridges.LowTempZeroPressureBridge.Clone
        Dim bridge As BalanceBridge = New BalanceBridge(sampleBridge.Bridge, sampleBridge.BalanceBridge.BridgeBalance.Layout)

        ' test expected values
        Dim expectedResistance As Double = 0
        If bridge.BridgeBalance.Layout = BalanceLayout.TopShuntBottomSeries Then
            expectedResistance = Resistor.ShuntConductor(bridge.NakedBridge.TopRight, bridge.BridgeBalance.Values(0))
        ElseIf bridge.BridgeBalance.Layout = BalanceLayout.RightShuntRightSeries Then
            expectedResistance = Resistor.ShuntConductor(bridge.NakedBridge.TopRight, bridge.BridgeBalance.Values(0)) + bridge.BridgeBalance.Values(1)
        End If
        Dim actualResistance As Double = bridge.TopRight
        Assert.AreEqual(expectedResistance, actualResistance, TestBridges.ElementEpsilon)

        If bridge.BridgeBalance.Layout = BalanceLayout.TopShuntBottomSeries Then
            expectedResistance = Resistor.ToSeries(bridge.NakedBridge.BottomRight, bridge.BridgeBalance.Values(1))
        ElseIf bridge.BridgeBalance.Layout = BalanceLayout.RightShuntRightSeries Then
            expectedResistance = Resistor.ShuntConductor(bridge.NakedBridge.BottomRight, bridge.BridgeBalance.Values(3)) + bridge.BridgeBalance.Values(2)
        End If
        actualResistance = bridge.BottomRight
        Assert.AreEqual(expectedResistance, actualResistance, TestBridges.ElementEpsilon)

        If bridge.BridgeBalance.Layout = BalanceLayout.TopShuntBottomSeries Then
            expectedResistance = Resistor.ShuntConductor(bridge.NakedBridge.TopLeft, bridge.BridgeBalance.Values(3))
        ElseIf bridge.BridgeBalance.Layout = BalanceLayout.RightShuntRightSeries Then
            expectedResistance = bridge.NakedBridge.TopLeft
        End If
        actualResistance = bridge.TopLeft
        Assert.AreEqual(expectedResistance, actualResistance, TestBridges.ElementEpsilon)

        If bridge.BridgeBalance.Layout = BalanceLayout.TopShuntBottomSeries Then
            expectedResistance = Resistor.ToSeries(bridge.NakedBridge.BottomLeft, bridge.BridgeBalance.Values(2))
        ElseIf bridge.BridgeBalance.Layout = BalanceLayout.RightShuntRightSeries Then
            expectedResistance = bridge.NakedBridge.BottomLeft
        End If
        actualResistance = bridge.BottomLeft
        Assert.AreEqual(expectedResistance, actualResistance, TestBridges.ElementEpsilon)

    End Sub

    ''' <summary> (Unit Test Method) tests build invalid bridge. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    Public Sub BuildInvalidBridgeTest()

        Dim sampleBridge As SampleBridge = TestBridges.LowTempZeroPressureBridge.Clone
        Dim bridge As BalanceBridge = sampleBridge.BalanceBridge

        Dim actualValidity As Boolean = False
        Dim expectedValidity As Boolean = False
        Assert.AreEqual(expectedValidity, actualValidity)

        actualValidity = bridge.IsValid
        expectedValidity = True
        Assert.AreEqual(expectedValidity, actualValidity)

        bridge.BridgeBalance = If(bridge.BridgeBalance.Layout = BalanceLayout.TopShuntBottomSeries,
            New BridgeBalance(New Double() {Conductor.OpenConductance, 10.3, Resistor.ShortResistance, 1 / 52357}, bridge.BridgeBalance.Layout),
            New BridgeBalance(New Double() {Conductor.OpenConductance, 10.3, Resistor.ShortResistance, 1 / 52357}, bridge.BridgeBalance.Layout))
        actualValidity = bridge.IsValid
        expectedValidity = True
        Assert.AreEqual(expectedValidity, actualValidity)

        bridge.BridgeBalance = If(bridge.BridgeBalance.Layout = BalanceLayout.TopShuntBottomSeries,
            New BridgeBalance(New Double() {Conductor.ShortConductance, 10.3, Resistor.ShortResistance, 1 / 52357}, bridge.BridgeBalance.Layout),
            New BridgeBalance(New Double() {Conductor.ShortConductance, 10.3, Resistor.ShortResistance, 1 / 52357}, bridge.BridgeBalance.Layout))
        actualValidity = bridge.IsValid
        expectedValidity = False
        Assert.AreEqual(expectedValidity, actualValidity)

        bridge.BridgeBalance = If(bridge.BridgeBalance.Layout = BalanceLayout.TopShuntBottomSeries,
            New BridgeBalance(New Double() {Conductor.OpenConductance, 10.3, Resistor.ShortResistance, 1 / 52357}, bridge.BridgeBalance.Layout),
            New BridgeBalance(New Double() {Conductor.OpenConductance, 10.3, Resistor.ShortResistance, 1 / 52357}, bridge.BridgeBalance.Layout))
        actualValidity = bridge.IsValid
        expectedValidity = True
        Assert.AreEqual(expectedValidity, actualValidity)

        bridge.BridgeBalance = If(bridge.BridgeBalance.Layout = BalanceLayout.TopShuntBottomSeries,
            New BridgeBalance(New Double() {Conductor.OpenConductance, 10.3, Resistor.OpenResistance, 1 / 52357}, bridge.BridgeBalance.Layout),
            New BridgeBalance(New Double() {Conductor.OpenConductance, 10.3, Resistor.OpenResistance, 1 / 52357}, bridge.BridgeBalance.Layout))
        actualValidity = bridge.IsValid
        expectedValidity = False
        Assert.AreEqual(expectedValidity, actualValidity)

        bridge.BridgeBalance = If(bridge.BridgeBalance.Layout = BalanceLayout.TopShuntBottomSeries,
            New BridgeBalance(New Double() {Conductor.OpenConductance, 10.3, Resistor.ShortResistance, 1 / 52357}, bridge.BridgeBalance.Layout),
            New BridgeBalance(New Double() {Conductor.OpenConductance, 10.3, Resistor.ShortResistance, 1 / 52357}, bridge.BridgeBalance.Layout))
        actualValidity = bridge.IsValid
        expectedValidity = True
        Assert.AreEqual(expectedValidity, actualValidity)


        bridge.BridgeBalance = If(bridge.BridgeBalance.Layout = BalanceLayout.TopShuntBottomSeries,
            New BridgeBalance(New Double() {Conductor.OpenConductance, Resistor.OpenResistance, Resistor.ShortResistance, 1 / 52357}, bridge.BridgeBalance.Layout),
            New BridgeBalance(New Double() {Conductor.OpenConductance, Resistor.OpenResistance, Resistor.ShortResistance, 1 / 52357}, bridge.BridgeBalance.Layout))
        actualValidity = bridge.IsValid
        expectedValidity = False
        Assert.AreEqual(expectedValidity, actualValidity)

        bridge.BridgeBalance = If(bridge.BridgeBalance.Layout = BalanceLayout.TopShuntBottomSeries,
            New BridgeBalance(New Double() {Conductor.OpenConductance, 10.3, Resistor.ShortResistance, 1 / 52357}, bridge.BridgeBalance.Layout),
            New BridgeBalance(New Double() {Conductor.OpenConductance, 10.3, Resistor.ShortResistance, 1 / 52357}, bridge.BridgeBalance.Layout))
        actualValidity = bridge.IsValid
        expectedValidity = True
        Assert.AreEqual(expectedValidity, actualValidity)

        bridge.BridgeBalance = If(bridge.BridgeBalance.Layout = BalanceLayout.TopShuntBottomSeries,
            New BridgeBalance(New Double() {Conductor.OpenConductance, 10.3, Resistor.ShortResistance, Conductor.ShortConductance}, bridge.BridgeBalance.Layout),
            New BridgeBalance(New Double() {Conductor.OpenConductance, 10.3, Resistor.ShortResistance, Conductor.ShortConductance}, bridge.BridgeBalance.Layout))
        actualValidity = bridge.IsValid
        expectedValidity = False
        Assert.AreEqual(expectedValidity, actualValidity)

        bridge.BridgeBalance = If(bridge.BridgeBalance.Layout = BalanceLayout.TopShuntBottomSeries,
            New BridgeBalance(New Double() {Conductor.OpenConductance, 10.3, Resistor.ShortResistance, 1 / 52357}, bridge.BridgeBalance.Layout),
            New BridgeBalance(New Double() {Conductor.OpenConductance, 10.3, Resistor.ShortResistance, 1 / 52357}, bridge.BridgeBalance.Layout))
        actualValidity = bridge.IsValid
        expectedValidity = True
        Assert.AreEqual(expectedValidity, actualValidity)

    End Sub

    ''' <summary> (Unit Test Method) tests null bridge exception. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    <ExpectedException(GetType(ArgumentNullException), "Null bridge inappropriately allowed")>
    Public Sub NullBridgeExceptionTest()
        Dim wheatstone As Wheatstone = Nothing '  New Wheatstone(New Double() {369.59, 363.04, 357.69, 377.06}, WheatstoneLayout.Counterclockwise)
        ' try build the bridge using a null bridge; should issue the expected exception
        Dim bridge As BalanceBridge = New BalanceBridge(wheatstone,
                                                        New Double() {Conductor.OpenConductance, 10.3, Resistor.ShortResistance, 1 / 52357},
                                                        BalanceLayout.TopShuntBottomSeries)
    End Sub

    ''' <summary> (Unit Test Method) tests null values exception. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    <ExpectedException(GetType(ArgumentNullException), "Null bridge values inappropriately allowed")>
    Public Sub NullValuesExceptionTest()
        Dim wheatstone As Wheatstone = New Wheatstone(New Double() {369.59, 363.04, 357.69, 377.06}, WheatstoneLayout.Counterclockwise)
        ' try build the bridge using a null bridge; should issue the expected exception
        Dim bridge As BalanceBridge = New BalanceBridge(wheatstone,
                                                        Nothing,
                                                        BalanceLayout.TopShuntBottomSeries)
    End Sub

    ''' <summary> (Unit Test Method) tests invalid values exception. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    <ExpectedException(GetType(InvalidOperationException), "Invalid balance values inappropriately allowed")>
    Public Sub InvalidValuesExceptionTest()
        Dim wheatstone As Wheatstone = New Wheatstone(New Double() {369.59, 363.04, 357.69, 377.06}, WheatstoneLayout.Counterclockwise)
        ' try build the bridge using a null bridge; should issue the expected exception
        Dim bridge As BalanceBridge = New BalanceBridge(wheatstone,
                                                        New Double() {Conductor.OpenConductance, 10.3},
                                                        BalanceLayout.TopShuntBottomSeries)
    End Sub

#End Region

#Region " OUTPUT TESTS "

    ''' <summary> (Unit Test Method) tests low temporary zero pressure output. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub LowTempZeroPressureOutputTest()

        Dim sampleBridge As SampleBridge = TestBridges.LowTempZeroPressureBridge
        Dim bridge As BalanceBridge = sampleBridge.BalanceBridge

        Dim relativeOffset As Double = sampleBridge.OutputVoltage / sampleBridge.BridgeVoltage
        Dim expectedRelativeOffset As Double = bridge.NakedBridge.Output
        Assert.AreEqual(expectedRelativeOffset, relativeOffset, bridge.OutputEpsilon)

        relativeOffset = bridge.Output
        expectedRelativeOffset = 0
        Assert.AreEqual(expectedRelativeOffset, relativeOffset, sampleBridge.RelativeOffsetEpsilon)

        Dim expectedBalance As Boolean = Math.Abs(bridge.Output) < bridge.OutputEpsilon
        Dim actualBalance As Boolean = bridge.IsOutputBalanced()
        Assert.AreEqual(expectedBalance, actualBalance)

        Dim outputVoltage As Double = bridge.NakedBridge.Output(sampleBridge.BridgeVoltage)
        Dim expectedVoltage As Double = sampleBridge.OutputVoltage
        Assert.AreEqual(expectedVoltage, outputVoltage, TestBridges.OutputVoltageEpsilon)

    End Sub

    ''' <summary> (Unit Test Method) tests high temporary zero pressure output. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub HighTempZeroPressureOutputTest()

        Dim sampleBridge As SampleBridge = TestBridges.HighTempZeroPressureBridge
        Dim bridge As BalanceBridge = sampleBridge.BalanceBridge

        Dim relativeOffset As Double = sampleBridge.OutputVoltage / sampleBridge.BridgeVoltage
        Dim expectedRelativeOffset As Double = bridge.NakedBridge.Output
        Assert.AreEqual(expectedRelativeOffset, relativeOffset, bridge.OutputEpsilon)

        relativeOffset = bridge.Output
        expectedRelativeOffset = 0
        Assert.AreEqual(expectedRelativeOffset, relativeOffset, sampleBridge.RelativeOffsetEpsilon)

        Dim expectedBalance As Boolean = Math.Abs(bridge.Output) < bridge.OutputEpsilon
        Dim actualBalance As Boolean = bridge.IsOutputBalanced()
        Assert.AreEqual(expectedBalance, actualBalance)

        Dim outputVoltage As Double = bridge.NakedBridge.Output(sampleBridge.BridgeVoltage)
        Dim expectedVoltage As Double = sampleBridge.OutputVoltage
        Assert.AreEqual(expectedVoltage, outputVoltage, TestBridges.OutputVoltageEpsilon)

    End Sub

    ''' <summary> (Unit Test Method) tests balance bridge change. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub BalanceBridgeChangeTest()

        Dim sampleBridge As SampleBridge = TestBridges.HighTempZeroPressureBridge.Clone
        Dim bridge As BalanceBridge = sampleBridge.BalanceBridge

        sampleBridge = TestBridges.LowTempZeroPressureBridge
        bridge.NakedBridge = sampleBridge.Bridge

        Dim relativeOffset As Double = sampleBridge.OutputVoltage / sampleBridge.BridgeVoltage
        Dim expectedRelativeOffset As Double = bridge.NakedBridge.Output
        Assert.AreEqual(expectedRelativeOffset, relativeOffset, bridge.OutputEpsilon)

        relativeOffset = bridge.Output
        expectedRelativeOffset = 0
        Assert.AreEqual(expectedRelativeOffset, relativeOffset, sampleBridge.RelativeOffsetEpsilon)

        Dim expectedBalance As Boolean = Math.Abs(bridge.Output) < bridge.OutputEpsilon
        Dim actualBalance As Boolean = bridge.IsOutputBalanced()
        Assert.AreEqual(expectedBalance, actualBalance)

        Dim outputVoltage As Double = bridge.NakedBridge.Output(sampleBridge.BridgeVoltage)
        Dim expectedVoltage As Double = sampleBridge.OutputVoltage
        Assert.AreEqual(expectedVoltage, outputVoltage, TestBridges.OutputVoltageEpsilon)

    End Sub

    ''' <summary> (Unit Test Method) tests balance bridge values no change. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub BalanceBridgeValuesNoChangeTest()

        Dim sampleBridge As SampleBridge = TestBridges.HighTempZeroPressureBridge.Clone
        Dim bridge As BalanceBridge = sampleBridge.BalanceBridge

        bridge.BridgeBalance = New BridgeBalance(New Double() {Conductor.OpenConductance, 10.3, Resistor.ShortResistance, 1 / 52357}, bridge.BridgeBalance.Layout)
        Dim relativeOffset As Double = sampleBridge.OutputVoltage / sampleBridge.BridgeVoltage
        Dim expectedRelativeOffset As Double = bridge.NakedBridge.Output
        Assert.AreEqual(expectedRelativeOffset, relativeOffset, bridge.OutputEpsilon)

        relativeOffset = bridge.Output
        expectedRelativeOffset = 0
        Assert.AreEqual(expectedRelativeOffset, relativeOffset, sampleBridge.RelativeOffsetEpsilon)

        Dim expectedBalance As Boolean = Math.Abs(bridge.Output) < bridge.OutputEpsilon
        Dim actualBalance As Boolean = bridge.IsOutputBalanced()
        Assert.AreEqual(expectedBalance, actualBalance)

        Dim outputVoltage As Double = bridge.NakedBridge.Output(sampleBridge.BridgeVoltage)
        Dim expectedVoltage As Double = sampleBridge.OutputVoltage
        Assert.AreEqual(expectedVoltage, outputVoltage, TestBridges.OutputVoltageEpsilon)

    End Sub

    ''' <summary> (Unit Test Method) tests balance bridge values change. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub BalanceBridgeValuesChangeTest()

        Dim sampleBridge As SampleBridge = TestBridges.HighTempZeroPressureBridge.Clone
        Dim bridge As BalanceBridge = sampleBridge.BalanceBridge

        bridge.BridgeBalance = New BridgeBalance(New Double() {Conductor.OpenConductance, 11.5, Resistor.ShortResistance, 1 / 52357}, bridge.BridgeBalance.Layout)
        Dim relativeOffset As Double = sampleBridge.OutputVoltage / sampleBridge.BridgeVoltage
        Dim expectedRelativeOffset As Double = bridge.NakedBridge.Output
        Assert.AreEqual(expectedRelativeOffset, relativeOffset, 0.001 * bridge.OutputEpsilon)

        relativeOffset = bridge.Output
        expectedRelativeOffset = 0
        Assert.AreEqual(expectedRelativeOffset, relativeOffset, sampleBridge.RelativeOffsetEpsilon)

        Dim expectedBalance As Boolean = Math.Abs(bridge.Output) < bridge.OutputEpsilon
        Dim actualBalance As Boolean = bridge.IsOutputBalanced()
        Assert.AreEqual(expectedBalance, actualBalance)

        Dim outputVoltage As Double = bridge.NakedBridge.Output(sampleBridge.BridgeVoltage)
        Dim expectedVoltage As Double = sampleBridge.OutputVoltage
        Assert.AreEqual(expectedVoltage, outputVoltage, 0.001 * TestBridges.OutputVoltageEpsilon)

    End Sub

#End Region

#Region " COMPENSATION TESTS "

    ''' <summary> Validates the compensation. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="sampleBridge"> The sample bridge. </param>
    ''' <param name="bridge">       The bridge. </param>
    Private Shared Sub ValidateCompensation(ByVal sampleBridge As SampleBridge, ByVal bridge As BalanceBridge)
        Dim relativeOffset As Double = bridge.Output
        Dim expectedRelativeOffset As Double = 0
        Assert.AreEqual(expectedRelativeOffset, relativeOffset, bridge.OutputEpsilon)

        Dim expectedBalance As Boolean = Math.Abs(bridge.Output) < bridge.OutputEpsilon
        Dim actualBalance As Boolean = bridge.IsOutputBalanced()
        Assert.AreEqual(expectedBalance, actualBalance)

        Dim outputVoltage As Double = bridge.Output(sampleBridge.BridgeVoltage)
        Dim expectedVoltage As Double = 0
        Assert.AreEqual(expectedVoltage, outputVoltage, TestBridges.OutputVoltageEpsilon)
    End Sub

    ''' <summary> (Unit Test Method) tests low temporary zero pressure shunt compensation. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub LowTempZeroPressureShuntCompensationTest()

        Dim sampleBridge As SampleBridge = TestBridges.LowTempZeroPressureBridge.Clone
        Dim bridge As BalanceBridge = sampleBridge.BalanceBridge

        ' apply shunt balance
        bridge.ApplyShuntCompensation()
        BalanceBridgeTests.ValidateCompensation(sampleBridge, bridge)

    End Sub

    ''' <summary> (Unit Test Method) tests low temporary zero pressure Series compensation. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub LowTempZeroPressureSeriesCompensationTest()

        Dim sampleBridge As SampleBridge = TestBridges.LowTempZeroPressureBridge.Clone
        Dim bridge As BalanceBridge = sampleBridge.BalanceBridge

        ' apply shunt balance
        bridge.ApplySeriesCompensation()
        BalanceBridgeTests.ValidateCompensation(sampleBridge, bridge)

    End Sub

    ''' <summary> (Unit Test Method) tests high temporary zero pressure shunt compensation. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub HighTempZeroPressureShuntCompensationTest()

        Dim sampleBridge As SampleBridge = TestBridges.HighTempZeroPressureBridge.Clone
        Dim bridge As BalanceBridge = sampleBridge.BalanceBridge

        ' apply shunt balance
        bridge.ApplyShuntCompensation()
        BalanceBridgeTests.ValidateCompensation(sampleBridge, bridge)

    End Sub

    ''' <summary> (Unit Test Method) tests high temporary zero pressure series compensation. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub HighTempZeroPressureSeriesCompensationTest()

        Dim sampleBridge As SampleBridge = TestBridges.HighTempZeroPressureBridge.Clone
        Dim bridge As BalanceBridge = sampleBridge.BalanceBridge

        ' apply Series balance
        bridge.ApplySeriesCompensation()
        BalanceBridgeTests.ValidateCompensation(sampleBridge, bridge)

    End Sub

#End Region

End Class

#Region " SAMPLE BRIDGE "

''' <summary> A sample bridge. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/23/2017 </para>
''' </remarks>
Public Class SampleBridge

    ''' <summary> Constructor using counter clock wise notation. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="elements">         The Wheatstone Bridge Elements. </param>
    ''' <param name="wheatstoneLayout"> The Wheatstone Bridge layout. </param>
    Public Sub New(ByVal elements As IEnumerable(Of Double), ByVal wheatstoneLayout As WheatstoneLayout)
        MyBase.New
        Me.Bridge = New Wheatstone(elements, wheatstoneLayout)
    End Sub

    ''' <summary> Validates the sample bridge described by value. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value"> The value. </param>
    ''' <returns> A SampleBridge. </returns>
    Private Shared Function ValidateSampleBridge(ByVal value As SampleBridge) As SampleBridge
        If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
        Return value
    End Function

    ''' <summary> Cloning Constructor. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As SampleBridge)
        MyBase.New
        Me.Bridge = New Wheatstone(SampleBridge.ValidateSampleBridge(value).Bridge)
        Me.BalanceBridge = New BalanceBridge(value.BalanceBridge)
        Me.SupplyVoltage = value.SupplyVoltage
        Me.SupplyVoltageDrop = value.SupplyVoltage
        Me.SupplyVoltageDrop = value.SupplyVoltageDrop
        Me.OutputVoltage = value.OutputVoltage
        Me.WheatstoneLayout = value.WheatstoneLayout
        Me.EquivalentResistance = value.EquivalentResistance
        Me.RelativeOffsetEpsilon = value.RelativeOffsetEpsilon

        Me.NominalVoltage = value.NominalVoltage
        Me.VoltageSourceSeriesResistance = value.VoltageSourceSeriesResistance
        Me.VoltageSourceParallelResistance = value.VoltageSourceParallelResistance
        Me.NominalFullScaleVoltage = value.NominalFullScaleVoltage
        Me.NominalCurrent = value.NominalCurrent
        Me.CurrentSourceSeriesResistance = value.CurrentSourceSeriesResistance
        Me.CurrentSourceParallelResistance = value.CurrentSourceParallelResistance
    End Sub

    ''' <summary> Makes a deep copy of this object. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> A copy of this object. </returns>
    Public Function Clone() As SampleBridge
        Return New SampleBridge(Me)
    End Function

    ''' <summary> Gets the bridge. </summary>
    ''' <value> The bridge. </value>
    Public Property Bridge As Wheatstone

    ''' <summary> Gets the supply voltage. </summary>
    ''' <value> The supply voltage. </value>
    Public Property SupplyVoltage As Double = 5.02058

    ''' <summary> Gets the supply voltage drop. </summary>
    ''' <value> The supply voltage drop. </value>
    Public Property SupplyVoltageDrop As Double = 3.67312

    ''' <summary> Gets the bridge voltage. </summary>
    ''' <value> The bridge voltage. </value>
    Public ReadOnly Property BridgeVoltage As Double
        Get
            Return Me.SupplyVoltage - Me.SupplyVoltageDrop
        End Get
    End Property

    ''' <summary> Gets or sets the output voltage. </summary>
    ''' <value> The output voltage. </value>
    Public Property OutputVoltage As Double = 0.011728

    ''' <summary> Gets or sets the wheatstone layout. </summary>
    ''' <value> The wheatstone layout. </value>
    Public Property WheatstoneLayout As WheatstoneLayout = WheatstoneLayout.Clockwise

    ''' <summary> Gets or sets the equivalent resistance. </summary>
    ''' <value> The equivalent resistance. </value>
    Public Property EquivalentResistance As Double = 782

    ''' <summary> Gets or sets the relative offset epsilon. </summary>
    ''' <value> The relative offset epsilon. </value>
    Public Property RelativeOffsetEpsilon As Double = 0.005

    ''' <summary> Gets or sets the nominal voltage. </summary>
    ''' <value> The nominal voltage. </value>
    Public Property NominalVoltage As Double = 5

    ''' <summary> Gets or sets the voltage source series resistance. </summary>
    ''' <value> The voltage source series resistance. </value>
    Public Property VoltageSourceSeriesResistance As Double = 1538

    ''' <summary> Gets or sets the voltage source parallel resistance. </summary>
    ''' <value> The voltage source parallel resistance. </value>
    Public Property VoltageSourceParallelResistance As Double = 1593

    ''' <summary> Gets or sets the nominal full scale voltage. </summary>
    ''' <value> The nominal full scale voltage. </value>
    Public Property NominalFullScaleVoltage As Double = 0.02

    ''' <summary> Gets or sets the nominal current. </summary>
    ''' <value> The nominal current. </value>
    Public Property NominalCurrent As Double = 0.004

    ''' <summary> Gets or sets the current source series resistance. </summary>
    ''' <value> The current source series resistance. </value>
    ''' <remarks>  636 this is calculated based on 5v, 0.004 v and the above resistances, original value is 632. </remarks>
    Public Property CurrentSourceSeriesResistance As Double = 636

    ''' <summary> Gets or sets the current source parallel resistance. </summary>
    ''' <value> The current source parallel resistance. </value>
    Public Property CurrentSourceParallelResistance As Double = 150

    ''' <summary> Gets or sets the balance bridge. </summary>
    ''' <value> The balance bridge. </value>
    Public Property BalanceBridge As BalanceBridge

    End Class

#End Region

