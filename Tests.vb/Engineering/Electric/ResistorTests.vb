﻿Imports isr.Core.Engineering

''' <summary> A resistance unit tests. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/16/2017 </para>
''' </remarks>
<TestClass()>
Public Class ResistorTests

#Region " TEST INPUTS "

    ''' <summary> Gets or sets the left resistance. </summary>
    ''' <value> The left resistance. </value>
    Public Shared Property LeftResistance As Double = 1000

    ''' <summary> Gets or sets the right resistance. </summary>
    ''' <value> The right resistance. </value>
    Public Shared Property RightResistance As Double = 500

    ''' <summary> Gets or sets the load resistance. </summary>
    ''' <value> The load resistance. </value>
    Public Shared Property LoadResistance As Double = 250

    ''' <summary> Gets or sets the negative resistance. </summary>
    ''' <value> The negative resistance. </value>
    Public Shared Property NegativeResistance As Double = -100

#End Region

#Region " PARALLEL TESTS "

    ''' <summary> (Unit Test Method) tests parallel. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub ParallelTest()

        Dim actualValue As Double = Resistor.ToParallel(ResistorTests.LeftResistance, ResistorTests.RightResistance)
        Dim expectedValue As Double = 1 / ((1 / ResistorTests.LeftResistance) + (1 / ResistorTests.RightResistance))
        Assert.AreEqual(expectedValue, actualValue)

        actualValue = Resistor.ShuntConductor(ResistorTests.LeftResistance, 1 / ResistorTests.RightResistance)
        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    ''' <summary> (Unit Test Method) tests parallel short. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub ParallelShortTest()

        Dim actualValue As Double = Resistor.ToParallel(Resistor.ShortResistance, ResistorTests.RightResistance)
        Dim expectedValue As Double = Resistor.ShortResistance
        Assert.AreEqual(expectedValue, actualValue)

        actualValue = Resistor.ToParallel(ResistorTests.LeftResistance, Resistor.ShortResistance)
        expectedValue = Resistor.ShortResistance
        Assert.AreEqual(expectedValue, actualValue)

        actualValue = Resistor.ToParallel(Resistor.ShortResistance, Resistor.ShortResistance)
        expectedValue = Resistor.ShortResistance
        Assert.AreEqual(expectedValue, actualValue)

        actualValue = Resistor.ShuntConductor(ResistorTests.LeftResistance, Conductor.ShortConductance)
        expectedValue = Resistor.ShortResistance
        Assert.AreEqual(expectedValue, actualValue)

        actualValue = Resistor.ShuntConductor(Resistor.ShortResistance, Conductor.ShortConductance)
        expectedValue = Resistor.ShortResistance
        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    ''' <summary> (Unit Test Method) tests parallel open. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub ParallelOpenTest()

        Dim actualValue As Double = Resistor.ToParallel(Resistor.OpenResistance, ResistorTests.RightResistance)
        Dim expectedValue As Double = ResistorTests.RightResistance
        Assert.AreEqual(expectedValue, actualValue)

        actualValue = Resistor.ToParallel(ResistorTests.LeftResistance, Resistor.OpenResistance)
        expectedValue = ResistorTests.LeftResistance
        Assert.AreEqual(expectedValue, actualValue)

        actualValue = Resistor.ToParallel(Resistor.OpenResistance, Resistor.OpenResistance)
        expectedValue = Resistor.OpenResistance
        Assert.AreEqual(expectedValue, actualValue)

        actualValue = Resistor.ShuntConductor(ResistorTests.LeftResistance, Conductor.OpenConductance)
        expectedValue = ResistorTests.LeftResistance
        Assert.AreEqual(expectedValue, actualValue)

        actualValue = Resistor.ShuntConductor(Resistor.OpenResistance, Conductor.OpenConductance)
        expectedValue = Resistor.OpenResistance
        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    ''' <summary> (Unit Test Method) tests parallel negative left resistance exception. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    <ExpectedException(GetType(ArgumentOutOfRangeException), "Negative Resistance inappropriately allowed")>
    Public Sub ParallelNegativeLeftResistanceExceptionTest()
        Resistor.ToParallel(ResistorTests.NegativeResistance, ResistorTests.RightResistance)
    End Sub

    ''' <summary> (Unit Test Method) tests parallel negative right resistance exception. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    <ExpectedException(GetType(ArgumentOutOfRangeException), "Negative Resistance inappropriately allowed")>
    Public Sub ParallelNegativeRightResistanceExceptionTest()
        Resistor.ToParallel(ResistorTests.LeftResistance, ResistorTests.NegativeResistance)
    End Sub

#End Region

#Region " SERIES TESTS "

    ''' <summary> (Unit Test Method) tests series. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub SeriesTest()
        Dim actualValue As Double = Resistor.ToSeries(ResistorTests.LeftResistance, ResistorTests.RightResistance)
        Dim expectedValue As Double = ResistorTests.LeftResistance + ResistorTests.RightResistance
        Assert.AreEqual(expectedValue, actualValue)
    End Sub

    ''' <summary> (Unit Test Method) tests series short. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub SeriesShortTest()
        Dim actualValue As Double = Resistor.ToSeries(Resistor.ShortResistance, ResistorTests.RightResistance)
        Dim expectedValue As Double = ResistorTests.RightResistance
        Assert.AreEqual(expectedValue, actualValue)
        actualValue = Resistor.ToSeries(ResistorTests.LeftResistance, Resistor.ShortResistance)
        expectedValue = ResistorTests.LeftResistance
        Assert.AreEqual(expectedValue, actualValue)
        actualValue = Resistor.ToSeries(Resistor.ShortResistance, Resistor.ShortResistance)
        expectedValue = Resistor.ShortResistance
        Assert.AreEqual(expectedValue, actualValue)
    End Sub

    ''' <summary> (Unit Test Method) tests series open. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub SeriesOpenTest()
        Dim actualValue As Double = Resistor.ToSeries(Resistor.OpenResistance, ResistorTests.RightResistance)
        Dim expectedValue As Double = Resistor.OpenResistance
        Assert.AreEqual(expectedValue, actualValue)
        actualValue = Resistor.ToSeries(ResistorTests.LeftResistance, Resistor.OpenResistance)
        expectedValue = Resistor.OpenResistance
        Assert.AreEqual(expectedValue, actualValue)
        actualValue = Resistor.ToSeries(Resistor.OpenResistance, Resistor.OpenResistance)
        expectedValue = Resistor.OpenResistance
        Assert.AreEqual(expectedValue, actualValue)
    End Sub

    ''' <summary> (Unit Test Method) tests series negative left resistance exception. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    <ExpectedException(GetType(ArgumentOutOfRangeException), "Negative Resistance inappropriately allowed")>
    Public Sub SeriesNegativeLeftResistanceExceptionTest()
        Resistor.ToSeries(ResistorTests.NegativeResistance, ResistorTests.RightResistance)
    End Sub

    ''' <summary> (Unit Test Method) tests series negative right resistance exception. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    <ExpectedException(GetType(ArgumentOutOfRangeException), "Negative Resistance inappropriately allowed")>
    Public Sub SeriesNegativeRightResistanceExceptionTest()
        Resistor.ToSeries(ResistorTests.LeftResistance, ResistorTests.NegativeResistance)
    End Sub

#End Region

#Region " OUTPUT CURRENT TESTS "

    ''' <summary> (Unit Test Method) tests output current. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub OutputCurrentTest()
        Dim actualValue As Double = Resistor.ToOutputCurrent(ResistorTests.LeftResistance, ResistorTests.RightResistance)
        Dim expectedValue As Double = Resistor.Parallel(ResistorTests.LeftResistance, ResistorTests.RightResistance) /
                                                           ResistorTests.RightResistance
        Assert.AreEqual(expectedValue, actualValue)
    End Sub

    ''' <summary> (Unit Test Method) tests output current short. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub OutputCurrentShortTest()
        Dim actualValue As Double = Resistor.ToOutputCurrent(Resistor.ShortResistance, ResistorTests.RightResistance)
        Dim expectedValue As Double = 0
        Assert.AreEqual(expectedValue, actualValue)
        actualValue = Resistor.ToOutputCurrent(ResistorTests.LeftResistance, Resistor.ShortResistance)
        expectedValue = 1
        Assert.AreEqual(expectedValue, actualValue)
    End Sub

    ''' <summary> (Unit Test Method) tests output current short exception. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    <ExpectedException(GetType(InvalidOperationException), "Two shorts inappropriately allowed")>
    Public Sub OutputCurrentShortExceptionTest()
        Resistor.ToOutputCurrent(Resistor.ShortResistance, Resistor.ShortResistance)
    End Sub

    ''' <summary> (Unit Test Method) tests output current open. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub OutputCurrentOpenTest()
        Dim actualValue As Double = Resistor.ToOutputCurrent(Resistor.OpenResistance, ResistorTests.RightResistance)
        Dim expectedValue As Double = 1
        Assert.AreEqual(expectedValue, actualValue)
        actualValue = Resistor.ToOutputCurrent(ResistorTests.LeftResistance, Resistor.OpenResistance)
        expectedValue = 0
        Assert.AreEqual(expectedValue, actualValue)
    End Sub

    ''' <summary> (Unit Test Method) tests output current opens exception. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    <ExpectedException(GetType(InvalidOperationException), "Two opens inappropriately allowed")>
    Public Sub OutputCurrentOpensExceptionTest()
        Resistor.ToOutputCurrent(Resistor.OpenResistance, Resistor.OpenResistance)
    End Sub

    ''' <summary>
    ''' (Unit Test Method) tests output current negative left resistance exception.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    <ExpectedException(GetType(ArgumentOutOfRangeException), "Negative Resistance inappropriately allowed")>
    Public Sub OutputCurrentNegativeLeftResistanceExceptionTest()
        Resistor.ToOutputCurrent(ResistorTests.NegativeResistance, ResistorTests.RightResistance)
    End Sub

    ''' <summary>
    ''' (Unit Test Method) tests output current negative right resistance exception.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    <ExpectedException(GetType(ArgumentOutOfRangeException), "Negative Resistance inappropriately allowed")>
    Public Sub OutputCurrentNegativeRightResistanceExceptionTest()
        Resistor.ToOutputCurrent(ResistorTests.LeftResistance, ResistorTests.NegativeResistance)
    End Sub

#End Region

#Region " OUTPUT VOLTAGE TESTS "

    ''' <summary> (Unit Test Method) tests output voltage. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub OutputVoltageTest()
        Dim actualValue As Double = Resistor.ToOutputVoltage(ResistorTests.LeftResistance, ResistorTests.RightResistance)
        Dim expectedValue As Double = ResistorTests.RightResistance /
                                           Resistor.ToSeries(ResistorTests.LeftResistance, ResistorTests.RightResistance)
        Assert.AreEqual(expectedValue, actualValue)
    End Sub

    ''' <summary> (Unit Test Method) tests output voltage short. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub OutputVoltageShortTest()

        Dim actualValue As Double = Resistor.ToOutputVoltage(Resistor.ShortResistance, ResistorTests.RightResistance)
        Dim expectedValue As Double = 1
        Assert.AreEqual(expectedValue, actualValue, $"Resistor.ToOutputVoltage({Resistor.ShortResistance},{ResistorTests.RightResistance})")

        actualValue = Resistor.ToOutputVoltage(ResistorTests.LeftResistance, Resistor.ShortResistance)
        expectedValue = 0
        Assert.AreEqual(expectedValue, actualValue, $"Resistor.ToOutputVoltage({ResistorTests.LeftResistance},{Resistor.ShortResistance})")

    End Sub

    ''' <summary> (Unit Test Method) tests output voltage shorts exception. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    <ExpectedException(GetType(InvalidOperationException), "Two Shorts inappropriately allowed")>
    Public Sub OutputVoltageShortsExceptionTest()
        Resistor.ToOutputVoltage(Resistor.ShortResistance, Resistor.ShortResistance)
    End Sub

    ''' <summary> (Unit Test Method) tests output voltage open. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub OutputVoltageOpenTest()

        Dim actualValue As Double = Resistor.ToOutputVoltage(Resistor.OpenResistance, ResistorTests.RightResistance)
        Dim expectedValue As Double = 0
        Assert.AreEqual(expectedValue, actualValue, $"Resistor.ToOutputVoltage({Resistor.OpenResistance},{ResistorTests.RightResistance})")

        actualValue = Resistor.ToOutputVoltage(ResistorTests.LeftResistance, Resistor.OpenResistance)
        expectedValue = 1
        Assert.AreEqual(expectedValue, actualValue, $"Resistor.ToOutputVoltage({ResistorTests.LeftResistance},{Resistor.OpenResistance})")

    End Sub

    ''' <summary> (Unit Test Method) tests output voltage opens exception. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    <ExpectedException(GetType(InvalidOperationException), "Two Opens inappropriately allowed")>
    Public Sub OutputVoltageOpensExceptionTest()
        Resistor.ToOutputVoltage(Resistor.OpenResistance, Resistor.OpenResistance)
    End Sub

    ''' <summary>
    ''' (Unit Test Method) tests output voltage negative left resistance exception.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    <ExpectedException(GetType(ArgumentOutOfRangeException), "Negative Resistance inappropriately allowed")>
    Public Sub OutputVoltageNegativeLeftResistanceExceptionTest()
        Resistor.ToOutputVoltage(ResistorTests.NegativeResistance, ResistorTests.RightResistance)
    End Sub

    ''' <summary>
    ''' (Unit Test Method) tests output voltage negative right resistance exception.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    <ExpectedException(GetType(ArgumentOutOfRangeException), "Negative Resistance inappropriately allowed")>
    Public Sub OutputVoltageNegativeRightResistanceExceptionTest()
        Resistor.ToOutputVoltage(ResistorTests.LeftResistance, ResistorTests.NegativeResistance)
    End Sub

#End Region

#Region " STANDARD RESISTANCE TESTS "

    ''' <summary> Tests resistance decade value. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="resistance">          The resistance. </param>
    ''' <param name="decadeCount">         Number of decades. </param>
    ''' <param name="expectedDecadeValue"> The expected decade value. </param>
    Public Shared Sub TestResistanceDecadeValue(ByVal resistance As Double, ByVal decadeCount As Integer, ByVal expectedDecadeValue As Double)
        Dim actualDecadeValue As Double = Resistor.ToDecadeValue(resistance, decadeCount)
        Assert.AreEqual(expectedDecadeValue, actualDecadeValue, $"Decade value for standard {decadeCount} for resistance {resistance}")
    End Sub

    ''' <summary> (Unit Test Method) tests resistance decade value. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    Public Sub ResistanceDecadeValueTest()
        ResistorTests.TestResistanceDecadeValue(0.1011, 192, 1.01)
        ResistorTests.TestResistanceDecadeValue(1.011, 192, 1.01)
        ResistorTests.TestResistanceDecadeValue(10.11, 192, 1.01)
        ResistorTests.TestResistanceDecadeValue(1011, 192, 1.01)
        ResistorTests.TestResistanceDecadeValue(2494, 192, 2.49)
        ResistorTests.TestResistanceDecadeValue(249.4, 192, 2.49)
        ResistorTests.TestResistanceDecadeValue(3340, 192, 3.36)
        ResistorTests.TestResistanceDecadeValue(3331, 192, 3.32)
        ResistorTests.TestResistanceDecadeValue(594, 192, 5.97)
    End Sub

    ''' <summary> Tests standard resistance. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="resistance">         The resistance. </param>
    ''' <param name="decadeCount">        Number of decades. </param>
    ''' <param name="expectedResistance"> The expected resistance. </param>
    Public Shared Sub TestStandardResistance(ByVal resistance As Double, ByVal decadeCount As Integer, ByVal expectedResistance As Double)
        Dim actualResistance As Double = Resistor.ToStandardResistance(resistance, decadeCount)
        Assert.AreEqual(expectedResistance, actualResistance, $"Standard resistance {decadeCount} for resistance {resistance}")
    End Sub

    ''' <summary> Gets or sets the standard resistor list 192. </summary>
    ''' <value> The standard resistor list 192. </value>
    Private Shared ReadOnly Property StandardResistorList192 As List(Of Double)

    ''' <summary> Enumerates standard resistors 192 in this collection. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process standard resistors 192 in this
    ''' collection.
    ''' </returns>
    Public Shared Function StandardResistors192() As IEnumerable(Of Double)
        If ResistorTests.StandardResistorList192 Is Nothing Then
            ResistorTests._StandardResistorList192 = New List(Of Double)
            ResistorTests.StandardResistorList192.AddRange(New Double() {1.0, 1.01, 1.02, 1.04, 1.05, 1.06, 1.07, 1.09, 1.1, 1.11, 1.13, 1.14})
            ResistorTests.StandardResistorList192.AddRange(New Double() {1.15, 1.17, 1.18, 1.2, 1.21, 1.23, 1.24, 1.26, 1.27, 1.29, 1.3, 1.32})
            ResistorTests.StandardResistorList192.AddRange(New Double() {1.33, 1.35, 1.37, 1.38, 1.4, 1.42, 1.43, 1.45, 1.47, 1.49, 1.5, 1.52})
            ResistorTests.StandardResistorList192.AddRange(New Double() {1.54, 1.56, 1.58, 1.6, 1.62, 1.64, 1.65, 1.67, 1.69, 1.72, 1.74, 1.76})
            ResistorTests.StandardResistorList192.AddRange(New Double() {1.78, 1.8, 1.82, 1.84, 1.87, 1.89, 1.91, 1.93, 1.96, 1.98, 2.0, 2.03})
            ResistorTests.StandardResistorList192.AddRange(New Double() {2.05, 2.08, 2.1, 2.13, 2.15, 2.18, 2.21, 2.23, 2.26, 2.29, 2.32, 2.34})
            ResistorTests.StandardResistorList192.AddRange(New Double() {2.37, 2.4, 2.43, 2.46, 2.49, 2.52, 2.55, 2.58, 2.61, 2.64, 2.67, 2.71})
            ResistorTests.StandardResistorList192.AddRange(New Double() {2.74, 2.77, 2.8, 2.84, 2.87, 2.91, 2.94, 2.98, 3.01, 3.05, 3.09, 3.12})
            ResistorTests.StandardResistorList192.AddRange(New Double() {3.16, 3.2, 3.24, 3.28, 3.32, 3.36, 3.4, 3.44, 3.48, 3.52, 3.57, 3.61})
            ResistorTests.StandardResistorList192.AddRange(New Double() {3.65, 3.7, 3.74, 3.79, 3.83, 3.88, 3.92, 3.97, 4.02, 4.07, 4.12, 4.17})
            ResistorTests.StandardResistorList192.AddRange(New Double() {4.22, 4.27, 4.32, 4.37, 4.42, 4.48, 4.53, 4.59, 4.64, 4.7, 4.75, 4.81})
            ResistorTests.StandardResistorList192.AddRange(New Double() {4.87, 4.93, 4.99, 5.05, 5.11, 5.17, 5.23, 5.3, 5.36, 5.42, 5.49, 5.56})
            ResistorTests.StandardResistorList192.AddRange(New Double() {5.62, 5.69, 5.76, 5.83, 5.9, 5.97, 6.04, 6.12, 6.19, 6.26, 6.34, 6.42})
            ResistorTests.StandardResistorList192.AddRange(New Double() {6.49, 6.57, 6.65, 6.73, 6.81, 6.9, 6.98, 7.06, 7.15, 7.23, 7.32, 7.41})
            ResistorTests.StandardResistorList192.AddRange(New Double() {7.5, 7.59, 7.68, 7.77, 7.87, 7.96, 8.06, 8.16, 8.25, 8.35, 8.45, 8.56})
            ResistorTests.StandardResistorList192.AddRange(New Double() {8.66, 8.76, 8.87, 8.98, 9.09, 9.19, 9.31, 9.42, 9.53, 9.65, 9.76, 9.88})
        End If
        Return ResistorTests.StandardResistorList192
    End Function

    ''' <summary> (Unit Test Method) tests decade value. </summary>
    ''' <remarks>
    ''' Found three errors in the original Omega table. One error in the resistor guide web site.
    ''' </remarks>
    <TestMethod()>
    Public Sub DecadeValueTest()
        For i As Integer = 0 To ResistorTests.StandardResistors192.Count - 1
            Assert.AreEqual(ResistorTests.StandardResistors192(i), Resistor.DecadeValue(i, ResistorTests.StandardResistors192.Count),
                                $"Decade value for {i}/{ResistorTests.StandardResistors192.Count}")
        Next
    End Sub

    ''' <summary> (Unit Test Method) tests standard resistance. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    Public Sub StandardResistanceTest()
        ResistorTests.TestStandardResistance(0.1011, 192, 0.101)
        ResistorTests.TestStandardResistance(1.011, 192, 1.01)
        ResistorTests.TestStandardResistance(10.11, 192, 10.1)
        ResistorTests.TestStandardResistance(10.16, 192, 10.1)
        ResistorTests.TestStandardResistance(1011, 192, 1010)
        ResistorTests.TestStandardResistance(1016, 192, 1010)
        ResistorTests.TestStandardResistance(594, 192, 597)
        ResistorTests.TestStandardResistance(906, 192, 909)
        ResistorTests.TestStandardResistance(9060, 192, 9090)
        ResistorTests.TestStandardResistance(90600, 192, 90900)
    End Sub

#End Region

End Class
