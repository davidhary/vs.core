Imports isr.Core.Engineering

''' <summary> Unit tests for testing attenuated voltage and current sources. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/18/2017 </para>
''' </remarks>
<TestClass()>
Public Class AttenuatedSourceTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        Assert.IsTrue(VoltageSourceTestInfo.Get.Exists, $"{GetType(VoltageSourceTestInfo)} settings should exist")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext
    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " CONVERSION TESTS "

    ''' <summary> (Unit Test Method) tests convert attenuated current source. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    Public Sub ConvertAttenuatedCurrentSourceTest()

        Dim attenuatedCurrentSource As AttenuatedCurrentSource = New AttenuatedCurrentSource(CDec(CurrentSourceTestInfo.Get.NominalCurrent),
                                                                                             CurrentSourceTestInfo.Get.SeriesResistance,
                                                                                                 CurrentSourceTestInfo.Get.ParallelConductance)

        ' create converted voltage sources using alternative conversions
        Dim sources As New List(Of AttenuatedVoltageSource)
        Dim descriptions As New List(Of String)
        sources.Add(attenuatedCurrentSource.ToAttenuatedVoltageSource(CurrentSourceTestInfo.Get.NominalVoltage))
        descriptions.Add("Voltage source from attenuated current source")
        sources.Add(New VoltageSource(attenuatedCurrentSource).ToAttenuatedVoltageSource(CDec(CurrentSourceTestInfo.Get.NominalVoltage)))
        descriptions.Add("Voltage source from voltage source")
        sources.Add(attenuatedCurrentSource.ToVoltageSource.ToAttenuatedVoltageSource(CDec(CurrentSourceTestInfo.Get.NominalVoltage)))
        descriptions.Add("Voltage source from current source")

        Dim doubleEpsilon As Double = 0.0000000001

        Dim source As AttenuatedVoltageSource
        For i As Integer = 0 To sources.Count - 1
            source = sources(i)
            Dim description As String = descriptions(i)
            Dim item As String = $"nominal voltage"
            Dim actualValue As Double = source.NominalVoltage
            Dim expectedValue As Double = CurrentSourceTestInfo.Get.NominalVoltage
            Dim outcome As String = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            Dim message As String = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

            ' the two sources have identical output resistance
            item = $"equivalent resistance"
            actualValue = source.Resistance
            expectedValue = Conductor.ToResistance(attenuatedCurrentSource.Conductance)
            outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

            ' the sources have the same open load voltage
            item = $"open load voltage"
            actualValue = source.LoadVoltage(Resistor.OpenResistance)
            expectedValue = attenuatedCurrentSource.LoadVoltage(Resistor.OpenResistance)
            outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

            ' the sources have the same short load voltage
            item = $"short load voltage"
            actualValue = source.LoadVoltage(Resistor.ShortResistance)
            expectedValue = attenuatedCurrentSource.LoadVoltage(Resistor.ShortResistance)
            outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

            ' the sources have the same load voltage
            item = $"load voltage"
            actualValue = source.LoadVoltage(CurrentSourceTestInfo.Get.LoadResistance)
            expectedValue = attenuatedCurrentSource.LoadVoltage(CurrentSourceTestInfo.Get.LoadResistance)
            outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

        Next

    End Sub

    ''' <summary> (Unit Test Method) tests convert attenuated voltage source. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    Public Sub ConvertAttenuatedVoltageSourceTest()

        Dim mimnimumImpedance As Double = CurrentSourceTestInfo.Get.NominalVoltage / CurrentSourceTestInfo.Get.NominalCurrent
        Dim impedance As Double = isr.Core.Engineering.AttenuatedVoltageSource.ToEquivalentResistance(CurrentSourceTestInfo.Get.SeriesResistance,
                                                                                                          CurrentSourceTestInfo.Get.ParallelConductance)
        Dim minImpedanceFactor As Double = 2 * mimnimumImpedance / impedance
        ' make sure that the voltage source impedance is high enough for conversion from 5v to 4 ma.
        Dim attenuatedVoltageSource As AttenuatedVoltageSource = New AttenuatedVoltageSource(CDec(CurrentSourceTestInfo.Get.NominalVoltage),
                                                                                             minImpedanceFactor * CurrentSourceTestInfo.Get.SeriesResistance,
                                                                                             minImpedanceFactor * CurrentSourceTestInfo.Get.ParallelConductance)

        ' create converted current sources using alternative conversions
        Dim sources As New List(Of AttenuatedCurrentSource)
        Dim descriptions As New List(Of String)
        sources.Add(attenuatedVoltageSource.ToAttenuatedCurrentSource(CurrentSourceTestInfo.Get.NominalCurrent))
        descriptions.Add("Current source from attenuated voltage source")
        sources.Add(New CurrentSource(attenuatedVoltageSource).ToAttenuatedCurrentSource(CDec(CurrentSourceTestInfo.Get.NominalCurrent)))
        descriptions.Add("Current source from current source")
        sources.Add(attenuatedVoltageSource.ToCurrentSource.ToAttenuatedCurrentSource(CDec(CurrentSourceTestInfo.Get.NominalCurrent)))
        descriptions.Add("Current source from voltage source")

        Dim doubleEpsilon As Double = 0.0000000001

        Dim source As AttenuatedCurrentSource
        For i As Integer = 0 To sources.Count - 1
            source = sources(i)
            Dim description As String = descriptions(i)
            Dim item As String = $"nominal current"
            Dim actualValue As Double = source.NominalCurrent
            Dim expectedValue As Double = CurrentSourceTestInfo.Get.NominalCurrent
            Dim outcome As String = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            Dim message As String = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

            ' the two sources have identical output resistance
            item = $"equivalent resistance"
            actualValue = Conductor.ToResistance(source.Conductance)
            expectedValue = attenuatedVoltageSource.Resistance
            outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

            ' the sources have the same short load current
            item = $"short load current"
            actualValue = source.LoadCurrent(Conductor.ShortConductance)
            expectedValue = attenuatedVoltageSource.LoadCurrent(Resistor.ShortResistance)
            outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

            ' the sources have the same open load current
            item = $"open load current"
            actualValue = source.LoadCurrent(Conductor.OpenConductance)
            expectedValue = attenuatedVoltageSource.LoadCurrent(Resistor.OpenResistance)
            outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

            ' the sources have the same load current
            item = $"load current"
            actualValue = source.LoadCurrent(Resistor.ToConductance(CurrentSourceTestInfo.Get.LoadResistance))
            expectedValue = attenuatedVoltageSource.LoadCurrent(CurrentSourceTestInfo.Get.LoadResistance)
            outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

        Next

    End Sub

#End Region

#Region " EXCEPTION CONVERSION TESTS "

    ''' <summary> (Unit Test Method) tests low nominal voltage source exception. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    <ExpectedException(GetType(InvalidOperationException), "Conversion from an attenuated current source to an attenuated voltage source with low nominal allowed")>
    Public Sub LowNominalVoltageSourceExceptionTest()
        Dim currentSource As AttenuatedCurrentSource = New AttenuatedCurrentSource(CDec(CurrentSourceTestInfo.Get.NominalCurrent),
                                                                                       CurrentSourceTestInfo.Get.SeriesResistance,
                                                                                       CurrentSourceTestInfo.Get.ParallelConductance)
        Dim source As AttenuatedVoltageSource = currentSource.ToAttenuatedVoltageSource(0.99 * currentSource.LoadVoltage(Resistor.OpenResistance))
    End Sub

    ''' <summary> (Unit Test Method) tests low nominal current source exception. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    <ExpectedException(GetType(InvalidOperationException), "Conversion from an attenuated voltage source to an attenuated current source with low nominal current allowed")>
    Public Sub LowNominalCurrentSourceExceptionTest()
        Dim voltageSource As AttenuatedVoltageSource = New AttenuatedVoltageSource(CDec(CurrentSourceTestInfo.Get.NominalVoltage),
                                                                                       CurrentSourceTestInfo.Get.SeriesResistance,
                                                                                       CurrentSourceTestInfo.Get.ParallelResistance)
        Dim source As AttenuatedCurrentSource = voltageSource.ToAttenuatedCurrentSource(0.99 * voltageSource.LoadCurrent(Resistor.ShortResistance))
    End Sub

    ''' <summary> (Unit Test Method) tests null attenuated current source exception. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    <ExpectedException(GetType(ArgumentNullException), "Null Attenuated Current Source inappropriately allowed")>
    Public Sub NullAttenuatedCurrentSourceExceptionTest()
        Dim currentSource As AttenuatedCurrentSource = Nothing
        Dim source As AttenuatedVoltageSource = New AttenuatedVoltageSource(CDec(CurrentSourceTestInfo.Get.NominalVoltage),
                                                                                CurrentSourceTestInfo.Get.SeriesResistance,
                                                                                CurrentSourceTestInfo.Get.ParallelConductance)
        source.FromAttenuatedCurrentSource(currentSource, CDec(CurrentSourceTestInfo.Get.NominalVoltage))
    End Sub

    ''' <summary> (Unit Test Method) tests null attenuated voltage source exception. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    <ExpectedException(GetType(ArgumentNullException), "Null Attenuated Voltage Source inappropriately allowed")>
    Public Sub NullAttenuatedVoltageSourceExceptionTest()
        Dim VoltageSource As AttenuatedVoltageSource = Nothing
        Dim source As AttenuatedCurrentSource = New AttenuatedCurrentSource(CDec(CurrentSourceTestInfo.Get.NominalCurrent),
                                                                                CurrentSourceTestInfo.Get.SeriesResistance,
                                                                                CurrentSourceTestInfo.Get.ParallelConductance)
        source.FromAttenuatedVoltageSource(VoltageSource, CDec(CurrentSourceTestInfo.Get.NominalCurrent))
    End Sub


#End Region

End Class
