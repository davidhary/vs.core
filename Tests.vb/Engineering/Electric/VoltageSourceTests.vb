﻿Imports isr.Core.Engineering

''' <summary> A voltage source unit tests. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/18/2017 </para>
''' </remarks>
<TestClass()>
Public Class VoltageSourceTests

#Region " TEST INPUTS "

    ''' <summary> Gets or sets the voltage of the voltage source. </summary>
    ''' <value> The voltage. </value>
    Public Shared Property Voltage As Double = 5

    ''' <summary> Gets or sets the resistance of the voltage source. </summary>
    ''' <value> The resistance. </value>
    Public Shared Property Resistance As Double = 1000

    ''' <summary> The load resistance. </summary>
    ''' <value> The load resistance. </value>
    Public Shared Property LoadResistance As Double = 500

    ''' <summary> Gets or sets the open resistance. </summary>
    ''' <value> The short resistance. </value>
    Public Shared Property OpenResistance As Double = Resistor.OpenResistance

    ''' <summary> Gets or sets the current of the current source. </summary>
    ''' <value> The current. </value>
    Public Shared Property Current As Double = 0.004

    ''' <summary> Gets or sets the conductance or the current source. </summary>
    ''' <value> The conductance. </value>
    Public Shared Property Conductance As Double = 0.001

    ''' <summary> The voltage change scale. </summary>
    ''' <value> The voltage change scale. </value>
    Public Shared Property VoltageChangeScale As Double = 1.5

    ''' <summary> The resistance change scale. </summary>
    ''' <value> The resistance change scale. </value>
    Public Shared Property ResistanceChangeScale As Double = 0.8

    ''' <summary> Gets or sets the load voltage epsilon. </summary>
    ''' <value> The load voltage epsilon. </value>
    Public Shared Property LoadVoltageEpsilon As Double = 0.000000001

    ''' <summary> Gets or sets the voltage to current level epsilon. </summary>
    ''' <value> The voltage to current level epsilon. </value>
    Public Shared Property VoltageToCurrentLevelEpsilon As Double = 0.000000001

    ''' <summary> Gets or sets the voltage to current conductance epsilon. </summary>
    ''' <value> The voltage to current conductance epsilon. </value>
    Public Shared Property VoltageToCurrentConductanceEpsilon As Double = 0.000000001

    ''' <summary> Gets or sets the voltage to current voltage level epsilon. </summary>
    ''' <value> The voltage to current voltage level epsilon. </value>
    Public Shared Property VoltageToCurrentVoltageLevelEpsilon As Double = 0.000000001

    ''' <summary> Gets or sets the voltage to current resistance epsilon. </summary>
    ''' <value> The voltage to current resistance epsilon. </value>
    Public Shared Property VoltageToCurrentResistanceEpsilon As Double = 0.000000001

#End Region

#Region " CONSTUCTOR TESTS "

    ''' <summary> (Unit Test Method) tests build voltage source. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub BuildVoltageSourceTest()

        Dim source As VoltageSource = New VoltageSource(VoltageSourceTests.Voltage, VoltageSourceTests.Resistance)

        ' test source voltage
        Dim actualValue As Double = source.Voltage
        Dim expectedValue As Double = VoltageSourceTests.Voltage
        Assert.AreEqual(expectedValue, actualValue)

        ' test source resistance
        actualValue = source.Resistance
        expectedValue = VoltageSourceTests.Resistance
        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    ''' <summary> (Unit Test Method) converts this object to a current source test. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub ConvertToCurrentSourceTest()

        ' build voltage source
        Dim voltageSource As VoltageSource = New VoltageSource(CurrentSourceTests.Voltage, CurrentSourceTests.Resistance)

        ' Convert to current source
        Dim source As CurrentSource = voltageSource.ToCurrentSource

        ' test source Current
        Dim actualValue As Double = source.Current
        Dim expectedValue As Double = voltageSource.Voltage / voltageSource.Resistance
        Assert.AreEqual(expectedValue, actualValue, VoltageSourceTests.VoltageToCurrentLevelEpsilon)

        ' test source resistance
        actualValue = source.Conductance
        expectedValue = 1 / voltageSource.Resistance
        Assert.AreEqual(expectedValue, actualValue, VoltageSourceTests.VoltageToCurrentConductanceEpsilon)

        Dim finalVoltageSource As VoltageSource = source.ToVoltageSource
        Assert.AreEqual(voltageSource.Voltage, finalVoltageSource.Voltage, VoltageSourceTests.VoltageToCurrentVoltageLevelEpsilon)
        Assert.AreEqual(voltageSource.Resistance, finalVoltageSource.Resistance, VoltageSourceTests.VoltageToCurrentResistanceEpsilon)
    End Sub

    ''' <summary> (Unit Test Method) tests null current source exception. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    <ExpectedException(GetType(ArgumentNullException), "Null current source inappropriately allowed")>
    Public Sub NullCurrentSourceExceptionTest()

        ' use a null current source
        Dim currentSource As CurrentSource = Nothing

        ' convert to voltage source
        Dim source As VoltageSource = New VoltageSource(VoltageSourceTests.Voltage, VoltageSourceTests.Resistance)

        ' should throw the expected exception
        source.FromCurrentSource(currentSource)

    End Sub

#End Region

#Region " VOLTAGE TESTS "

    ''' <summary> (Unit Test Method) tests voltage source. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub VoltageSourceTest()

        Dim source As VoltageSource = New VoltageSource(VoltageSourceTests.Voltage, VoltageSourceTests.Resistance)

        ' test open voltage
        Dim actualValue As Double = source.LoadVoltage(Resistor.OpenResistance)
        Dim expectedValue As Double = VoltageSourceTests.Voltage
        Assert.AreEqual(expectedValue, actualValue)

        ' test short voltage
        actualValue = source.LoadVoltage(Resistor.ShortResistance)
        expectedValue = 0
        Assert.AreEqual(expectedValue, actualValue)

        ' test load voltage
        actualValue = source.LoadVoltage(VoltageSourceTests.LoadResistance)
        expectedValue = VoltageSourceTests.Voltage * Resistor.OutputVoltage(VoltageSourceTests.Resistance, VoltageSourceTests.LoadResistance)
        Assert.AreEqual(expectedValue, actualValue, VoltageSourceTests.LoadVoltageEpsilon)

        ' test load current
        actualValue = source.LoadCurrent(VoltageSourceTests.LoadResistance)
        expectedValue = VoltageSourceTests.Voltage / Resistor.Series(VoltageSourceTests.Resistance, VoltageSourceTests.LoadResistance)
        Assert.AreEqual(expectedValue, actualValue)

        ' test open voltage
        actualValue = source.LoadVoltage(VoltageSourceTests.OpenResistance)
        expectedValue = source.Voltage
        Assert.AreEqual(expectedValue, actualValue)


    End Sub

    ''' <summary> (Unit Test Method) tests voltage source change. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()> Public Sub VoltageSourceChangeTest()

        Dim source As VoltageSource = New VoltageSource(VoltageSourceTests.Voltage, VoltageSourceTests.Resistance)
        source.Voltage *= VoltageSourceTests.VoltageChangeScale

        ' test open voltage
        Dim actualValue As Double = source.LoadVoltage(Resistor.OpenResistance)
        Dim expectedValue As Double = VoltageChangeScale * VoltageSourceTests.Voltage
        Assert.AreEqual(expectedValue, actualValue)

        ' test short voltage
        actualValue = source.LoadVoltage(Resistor.ShortResistance)
        expectedValue = 0
        Assert.AreEqual(expectedValue, actualValue)

        ' test load voltage
        actualValue = source.LoadVoltage(VoltageSourceTests.LoadResistance)
        expectedValue = VoltageChangeScale * VoltageSourceTests.Voltage *
                            Resistor.OutputVoltage(VoltageSourceTests.Resistance, VoltageSourceTests.LoadResistance)
        Assert.AreEqual(expectedValue, actualValue)

        ' test load current
        actualValue = source.LoadCurrent(VoltageSourceTests.LoadResistance)
        expectedValue = VoltageChangeScale * VoltageSourceTests.Voltage / Resistor.Series(VoltageSourceTests.Resistance, VoltageSourceTests.LoadResistance)
        Assert.AreEqual(expectedValue, actualValue)

        source.Voltage /= VoltageChangeScale
        source.Resistance *= VoltageSourceTests.ResistanceChangeScale

        ' test open voltage
        actualValue = source.LoadVoltage(VoltageSourceTests.OpenResistance)
        expectedValue = source.Voltage
        Assert.AreEqual(expectedValue, actualValue)

        ' test short voltage
        actualValue = source.LoadVoltage(0)
        expectedValue = 0
        Assert.AreEqual(expectedValue, actualValue)

        ' test load voltage
        actualValue = source.LoadVoltage(VoltageSourceTests.LoadResistance)
        expectedValue = VoltageSourceTests.Voltage * Resistor.OutputVoltage(ResistanceChangeScale * VoltageSourceTests.Resistance, VoltageSourceTests.LoadResistance)
        Assert.AreEqual(expectedValue, actualValue)

        ' test load current
        actualValue = source.LoadCurrent(VoltageSourceTests.LoadResistance)
        expectedValue = VoltageSourceTests.Voltage / Resistor.Series(ResistanceChangeScale * VoltageSourceTests.Resistance, VoltageSourceTests.LoadResistance)
        Assert.AreEqual(expectedValue, actualValue)

    End Sub

#End Region

End Class
