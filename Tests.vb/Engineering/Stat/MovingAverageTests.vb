''' <summary> Moving average tests. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 1/27/2016 </para>
''' </remarks>
<TestClass()>
Public Class MovingAverageTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext
    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region
    ''' <summary> The default window length. </summary>
    Private Const _DefaultWindowLength As Integer = 5

    ''' <summary> Assert moving average. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="count">         Number of. </param>
    ''' <param name="movingAverage"> The moving average. </param>
    ''' <param name="sample">        The sample. </param>
    Private Shared Sub AssertMovingAverage(ByVal count As Integer, ByVal movingAverage As Core.Engineering.MovingAverage, ByVal sample As Core.Engineering.SampleStatistics)
        movingAverage.EvaluateMean()
        sample.EvaluateMean()
        Assert.AreEqual(sample.Minimum, movingAverage.Minimum, $"Minimum after adding {count} values")
        Assert.AreEqual(sample.Maximum, movingAverage.Maximum, $"Maximum after adding {count} values")
        Assert.AreEqual(sample.Mean, movingAverage.Mean, $"Mean after adding {count} values")
    End Sub

    ''' <summary> Adds a value. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="rng">           The random number generator. </param>
    ''' <param name="queue">         The queue. </param>
    ''' <param name="movingAverage"> The moving average. </param>
    Private Shared Sub AddValue(ByVal rng As Random, ByVal queue As Queue(Of Double), ByVal movingAverage As Core.Engineering.MovingAverage)
        movingAverage.AddValue(rng.NextDouble)
        queue.Enqueue(movingAverage.ValuesList.Last)
        Do While queue.Count > movingAverage.Length
            queue.Dequeue()
        Loop
    End Sub

    ''' <summary> (Unit Test Method) moving average test multiple times. </summary>
    ''' <remarks>
    ''' Added because the moving average failed updating the range correctly when a adding a new
    ''' value.
    ''' </remarks>
    <TestMethod()>
    Public Sub MovingAverageTestMultipleTimes()
        For i As Integer = 1 To 1000
            Me.MovingAverageTest()
            isr.Core.My.MyLibrary.DoEvents()
            Threading.Thread.Sleep(1)
        Next
    End Sub

    ''' <summary> (Unit Test Method) tests moving average. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    Public Sub MovingAverageTest()
        Dim movingAverage As New isr.Core.Engineering.MovingAverage(MovingAverageTests._DefaultWindowLength)
        Dim rng As New Random(DateTimeOffset.Now.Second)
        Dim sample As New isr.Core.Engineering.SampleStatistics()
        Dim queue As New Queue(Of Double)
        Dim count As Integer = 0
        count += 1
        MovingAverageTests.AddValue(rng, queue, movingAverage)
        sample.ClearKnownState() : sample.AddValues(queue.ToArray)
        MovingAverageTests.AssertMovingAverage(count, movingAverage, sample)
        count += 1
        MovingAverageTests.AddValue(rng, queue, movingAverage)
        sample.ClearKnownState() : sample.AddValues(queue.ToArray)
        MovingAverageTests.AssertMovingAverage(count, movingAverage, sample)
        count += 1
        MovingAverageTests.AddValue(rng, queue, movingAverage)
        sample.ClearKnownState() : sample.AddValues(queue.ToArray)
        MovingAverageTests.AssertMovingAverage(count, movingAverage, sample)
        count += 1
        MovingAverageTests.AddValue(rng, queue, movingAverage)
        sample.ClearKnownState() : sample.AddValues(queue.ToArray)
        MovingAverageTests.AssertMovingAverage(count, movingAverage, sample)
        count += 1
        MovingAverageTests.AddValue(rng, queue, movingAverage)
        sample.ClearKnownState() : sample.AddValues(queue.ToArray)
        MovingAverageTests.AssertMovingAverage(count, movingAverage, sample)
        count += 1
        MovingAverageTests.AddValue(rng, queue, movingAverage)
        sample.ClearKnownState() : sample.AddValues(queue.ToArray)
        MovingAverageTests.AssertMovingAverage(count, movingAverage, sample)

    End Sub

End Class
