Imports isr.Core.TimeSpanExtensions
Namespace ExtensionsTests

    ''' <summary> Time Span extensions tests. </summary>
    ''' <remarks>
    ''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 3/14/2018 </para>
    ''' </remarks>
    <TestClass()>
    Public Class TimeSpanExtensionsTests

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <remarks>
        ''' Use ClassInitialize to run code before running the first test in the class.
        ''' </remarks>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
        <ClassInitialize(), CLSCompliant(False)>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                _TestSite = New TestSite
                _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
                _TestSite.AddTraceMessagesQueue(isr.Core.My.MyLibrary.UnpublishedTraceMessages)
                _TestSite.InitializeTraceListener()

            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
            _TestSite?.Dispose()
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        ''' <remarks> David, 2020-09-23. </remarks>
        <TestInitialize()> Public Sub MyTestInitialize()
            ' assert reading of test settings from the configuration file.
            Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
            Dim expectedUpperLimit As Double = 12
            Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
            TestInfo.ClearMessageQueue()
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        ''' <remarks> David, 2020-09-23. </remarks>
        <TestCleanup()> Public Sub MyTestCleanup()
            TestInfo.AssertMessageQueue()
        End Sub

        ''' <summary>
        ''' Gets the test context which provides information about and functionality for the current test
        ''' run.
        ''' </summary>
        ''' <value> The test context. </value>
        Public Property TestContext() As TestContext
        ''' <summary> The test site. </summary>
        Private Shared _TestSite As TestSite

        ''' <summary> Gets information describing the test. </summary>
        ''' <value> Information describing the test. </value>
        Private Shared ReadOnly Property TestInfo() As TestSite
            Get
                If _TestSite Is Nothing Then
                    _TestSite = New TestSite
                    _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
                    _TestSite.AddTraceMessagesQueue(isr.Core.My.MyLibrary.UnpublishedTraceMessages)
                End If
                Return _TestSite
            End Get
        End Property

#End Region

#Region " TIMESPAN DELAY "

        ''' <summary> Values that represent task options. </summary>
        ''' <remarks> David, 2020-09-23. </remarks>
        Private Enum TaskOptions
            ''' <summary> An enum constant representing the no Operation option. </summary>
            NoOp
            ''' <summary> An enum constant representing the spin wait option. </summary>
            SpinWait
            ''' <summary> An enum constant representing the high Resource option. </summary>
            HighRes
            ''' <summary> An enum constant representing the thread task option. </summary>
            ThreadTask
            ''' <summary> An enum constant representing the task delay option. </summary>
            TaskDelay
        End Enum

        ''' <summary> Estimates task time. </summary>
        ''' <remarks> David, 2020-09-23. </remarks>
        ''' <param name="taskOption"> The task option. </param>
        ''' <param name="spinCount">  Number of spins. </param>
        ''' <param name="delay">      The delay. </param>
        ''' <param name="count">      Number of. </param>
        ''' <returns> A (Mean As Double, Sigma As Double) </returns>
        Private Shared Function EstimateTaskTime(ByVal taskOption As TaskOptions, ByVal spinCount As Integer,
                                                 ByVal delay As TimeSpan, ByVal count As Integer) As isr.Core.Engineering.SampleStatistics
            Dim stat As New isr.Core.Engineering.SampleStatistics
            Dim sw As Stopwatch = Stopwatch.StartNew
            Dim task As Threading.Tasks.Task
            For i As Integer = 1 To count
                isr.Core.My.MyLibrary.DoEvents()
                Select Case taskOption
                    Case TaskOptions.NoOp
                        sw.Restart()
                        task = TimeSpanExtensions.Methods.NoOpTask
                        task.Start()
                        task.Wait()
                        sw.Stop()
                    Case TaskOptions.SpinWait
                        sw.Restart()
                        task = TimeSpanExtensions.Methods.SpinWaitTask(spinCount)
                        task.Start()
                        task.Wait()
                        sw.Stop()
                    Case TaskOptions.TaskDelay
                        sw.Restart()
                        Threading.Tasks.Task.Delay(delay)
                        sw.Stop()
                    Case TaskOptions.ThreadTask
                        sw.Restart()
                        task = TimeSpanExtensions.Methods.ThreadClockTask(delay)
                        task.Start()
                        task.Wait()
                        sw.Stop()
                    Case TaskOptions.HighRes
                        sw.Restart()
                        task = TimeSpanExtensions.Methods.HighResolutionClockTask(delay)
                        task.Start()
                        task.Wait()
                        sw.Stop()
                End Select
                stat.AddValue(sw.Elapsed.ToMilliseconds)
            Next
            stat.Evaluate()
            Return stat
        End Function

        ''' <summary> Reports. </summary>
        ''' <remarks> David, 2020-09-23. </remarks>
        ''' <param name="name"> The name. </param>
        ''' <param name="stat"> The stat. </param>
        ''' <returns> A String. </returns>
        Private Shared Function Report(ByVal name As String, ByVal stat As isr.Core.Engineering.SampleStatistics) As String
            Return $"{name} {stat.Mean:0.0##}±{stat.Sigma:0.0##}ms {stat.Count}:[{stat.Minimum:0.0##},{stat.Maximum:0.0##}]"
        End Function

        ''' <summary> Tests timespan system clock delay statistics. </summary>
        ''' <remarks> David, 2020-09-23. </remarks>
        ''' <param name="taskOption">     The task option. </param>
        ''' <param name="removeOutliers"> True to remove outliers. </param>
        ''' <param name="count">          Number of. </param>
        Private Shared Sub TestTimespanSystemClockDelayStatistics(ByVal taskOption As TaskOptions, ByVal removeOutliers As Boolean, ByVal count As Integer)
            Dim spinCount As Integer = 1
            Dim delay As TimeSpan = TimeSpan.Zero
            Dim message As String
            Dim expectedMean As Double
            Dim actualMean As Double
            Dim stat As Engineering.SampleStatistics
            Dim qrt As Engineering.SampleQuartiles

            If taskOption = TaskOptions.NoOp Then
                stat = EstimateTaskTime(taskOption, spinCount, delay, count)
                If removeOutliers Then
                    expectedMean = 0.08 ' 0.021±0.017ms 88[0.004,0.069]
                    qrt = New isr.Core.Engineering.SampleQuartiles(stat)
                    qrt.Evaluate()
                    actualMean = qrt.FilteredSample.Mean
                    message = Report($"{taskOption}.Q".ToString, qrt.FilteredSample)
                Else
                    expectedMean = 0.5 ' 0.029±0.121ms 100:[0.004,1.211]
                    actualMean = stat.Mean
                    message = Report($"{taskOption}".ToString, stat)
                End If
                TestInfo.VerboseMessage(message)
                Assert.IsTrue(actualMean < expectedMean, $"Actual mean {actualMean:0.000}ms should be lower then the expected mean {expectedMean:0.000}ms: {message}")
            End If

            If taskOption = TaskOptions.SpinWait Then
                spinCount = 1
                delay = TimeSpan.Zero
                stat = EstimateTaskTime(taskOption, spinCount, delay, count)
                If removeOutliers Then
                    expectedMean = 0.09 ' SpinWait(1).Q 0.024±0.02ms 99: [0.004,0.093]
                    qrt = New isr.Core.Engineering.SampleQuartiles(stat)
                    qrt.Evaluate()
                    actualMean = qrt.FilteredSample.Mean
                    message = Report($"{taskOption}({spinCount}).Q", qrt.FilteredSample)
                Else
                    expectedMean = 0.5 ' SpinWait(1) 0.039±0.142ms 100:[0.004,1.319]
                    actualMean = stat.Mean
                    message = Report($"{taskOption}({spinCount})", stat)
                End If
                TestInfo.VerboseMessage(message)
                Assert.IsTrue(actualMean < expectedMean, $"Actual mean {actualMean:0.000}ms should be lower then the expected mean {expectedMean:0.000}ms: {message}")

                spinCount = 9
                delay = TimeSpan.Zero
                stat = EstimateTaskTime(taskOption, spinCount, delay, count)
                If removeOutliers Then
                    expectedMean = 0.09 ' SpinWait(9).Q 0.025±0.018ms 98: [0.004,0.077]
                    qrt = New isr.Core.Engineering.SampleQuartiles(stat)
                    qrt.Evaluate()
                    actualMean = qrt.FilteredSample.Mean
                    message = Report($"{taskOption}({spinCount}).Q", qrt.FilteredSample)
                Else
                    expectedMean = 0.5 ' SpinWait(9) 0.027±0.026ms 100:[0.004,0.174]
                    actualMean = stat.Mean
                    message = Report($"{taskOption}({spinCount})", stat)
                End If
                TestInfo.VerboseMessage(message)
                Assert.IsTrue(actualMean < expectedMean, $"Actual mean {actualMean:0.000}ms should be lower then the expected mean {expectedMean:0.000}ms: {message}")
            End If

            If taskOption = TaskOptions.TaskDelay Then
                spinCount = 1
                delay = TimeSpan.Zero
                stat = EstimateTaskTime(taskOption, spinCount, delay, count)
                If removeOutliers Then
                    expectedMean = 0.005 ' TaskDelay(0.000).Q 0.0±0.0ms 73: [0.0,0.0]
                    qrt = New isr.Core.Engineering.SampleQuartiles(stat)
                    qrt.Evaluate()
                    actualMean = qrt.FilteredSample.Mean
                    message = Report($"{taskOption}({delay:s\.fff}).Q", qrt.FilteredSample)
                Else
                    expectedMean = 0.05 ' TaskDelay(0.000) 0.002±0.013ms 100:[0.0,0.135]
                    actualMean = stat.Mean
                    message = Report($"{taskOption}({delay:s\.fff})", stat)
                End If
                TestInfo.VerboseMessage(message)
                Assert.IsTrue(actualMean < expectedMean, $"Actual mean {actualMean:0.000}ms should be lower then the expected mean {expectedMean:0.000}ms: {message}")

                taskOption = TaskOptions.TaskDelay
                spinCount = 1
                delay = (9.0).FromMilliseconds
                stat = EstimateTaskTime(taskOption, spinCount, delay, count)
                If removeOutliers Then
                    expectedMean = 20 ' TaskDelay(0.009).Q 15.065±4.965ms 100: [9.384,20.46]
                    qrt = New isr.Core.Engineering.SampleQuartiles(stat)
                    qrt.Evaluate()
                    actualMean = qrt.FilteredSample.Mean
                    message = Report($"{taskOption}({delay:s\.fff}).Q", qrt.FilteredSample)
                Else
                    expectedMean = 20 ' TaskDelay(0.009) 15.238±4.895ms 100:[9.302,20.502]
                    actualMean = stat.Mean
                    message = Report($"{taskOption}({delay:s\.fff})", stat)
                End If
                TestInfo.VerboseMessage(message)
                Assert.IsTrue(actualMean < expectedMean, $"Actual mean {actualMean:0.000}ms should be lower then the expected mean {expectedMean:0.000}ms: {message}")

                taskOption = TaskOptions.TaskDelay
                spinCount = 1
                delay = (20.0).FromMilliseconds
                stat = EstimateTaskTime(taskOption, spinCount, delay, count)
                If removeOutliers Then
                    expectedMean = 35 ' TaskDelay(0.02).Q 31.715±1.11ms 87: [28.614,33.27]
                    qrt = New isr.Core.Engineering.SampleQuartiles(stat)
                    qrt.Evaluate()
                    actualMean = qrt.FilteredSample.Mean
                    message = Report($"{taskOption}({delay:s\.fff}).Q", qrt.FilteredSample)
                Else
                    expectedMean = 35 ' TaskDelay(0.02) 30.942±1.986ms 100:[25.768,34.789]
                    actualMean = stat.Mean
                    message = Report($"{taskOption}({delay:s\.fff})", stat)
                End If
                TestInfo.VerboseMessage(message)
                Assert.IsTrue(actualMean < expectedMean, $"Actual mean {actualMean:0.000}ms should be lower then the expected mean {expectedMean:0.000}ms: {message}")
            End If

            If taskOption = TaskOptions.ThreadTask Then
                spinCount = 1
                delay = TimeSpan.Zero
                stat = EstimateTaskTime(taskOption, spinCount, delay, count)
                If removeOutliers Then
                    expectedMean = 0.08 ' ThreadTask(0.0000).Q 0.018±0.007ms 91: [0.008,0.042]
                    qrt = New isr.Core.Engineering.SampleQuartiles(stat)
                    qrt.Evaluate()
                    actualMean = qrt.FilteredSample.Mean
                    message = Report($"{taskOption}({delay:s\.ffff}).Q", qrt.FilteredSample)
                Else
                    expectedMean = 0.6 ' ThreadTask(0.0000) 0.033±0.16ms 100:[0.005,1.607]
                    actualMean = stat.Mean
                    message = Report($"{taskOption}({delay:s\.ffff})", stat)
                End If
                TestInfo.VerboseMessage(message)
                Assert.IsTrue(actualMean < expectedMean, $"Actual mean {actualMean:0.000}ms should be lower then the expected mean {expectedMean:0.000}ms: {message}")

                taskOption = TaskOptions.ThreadTask
                spinCount = 1
                delay = (0.5).FromMilliseconds
                stat = EstimateTaskTime(taskOption, spinCount, delay, count)
                If removeOutliers Then
                    expectedMean = 0.2 ' ThreadTask(0.0005).Q 0.054±0.047ms 97: [0.009,0.165]
                    qrt = New isr.Core.Engineering.SampleQuartiles(stat)
                    qrt.Evaluate()
                    actualMean = qrt.FilteredSample.Mean
                    message = Report($"{taskOption}({delay:s\.ffff}).Q", qrt.FilteredSample)
                Else
                    expectedMean = 0.5 ' ThreadTask(0.0005) 0.027±0.021ms 100:[0.005,0.114]
                    actualMean = stat.Mean
                    message = Report($"{taskOption}({delay:s\.ffff})", stat)
                End If
                TestInfo.VerboseMessage(message)
                Assert.IsTrue(actualMean < expectedMean, $"Actual mean {actualMean:0.000}ms should be lower then the expected mean {expectedMean:0.000}ms: {message}")

                spinCount = 1
                delay = (1.5).FromMilliseconds
                stat = EstimateTaskTime(taskOption, spinCount, delay, count)
                If removeOutliers Then
                    expectedMean = 2.5 ' ThreadTask(0.0015).Q 1.877±0.079ms 78: [1.675,2.083]
                    qrt = New isr.Core.Engineering.SampleQuartiles(stat)
                    qrt.Evaluate()
                    actualMean = qrt.FilteredSample.Mean
                    message = Report($"{taskOption}({delay:s\.ffff}).Q", qrt.FilteredSample)
                Else
                    expectedMean = 2.5 ' ThreadTask(0.0015) 1.88±0.174ms 100:[1.334,2.582]
                    actualMean = stat.Mean
                    message = Report($"{taskOption}({delay:s\.ffff})", stat)
                End If
                TestInfo.VerboseMessage(message)
                Assert.IsTrue(actualMean < expectedMean, $"Actual mean {actualMean:0.000}ms should be lower then the expected mean {expectedMean:0.000}ms: {message}")

                spinCount = 1
                delay = (1.9).FromMilliseconds
                stat = EstimateTaskTime(taskOption, spinCount, delay, count)
                If removeOutliers Then
                    expectedMean = 2.5 ' ThreadTask(0.0019).Q 1.879±0.084ms 85: [1.627,2.078]
                    qrt = New isr.Core.Engineering.SampleQuartiles(stat)
                    qrt.Evaluate()
                    message = Report($"{taskOption}({delay:s\.ffff}).Q", qrt.FilteredSample)
                Else
                    expectedMean = 2.5 ' ThreadTask(0.0019) 1.851±0.184ms 100:[1.262,2.339]
                    actualMean = stat.Mean
                    message = Report($"{taskOption}({delay:s\.ffff})", stat)
                End If
                TestInfo.VerboseMessage(message)
                Assert.IsTrue(actualMean < expectedMean, $"Actual mean {actualMean:0.000}ms should be lower then the expected mean {expectedMean:0.000}ms: {message}")

                spinCount = 1
                delay = (2.2).FromMilliseconds
                stat = EstimateTaskTime(taskOption, spinCount, delay, count)
                If removeOutliers Then
                    expectedMean = 3.5 ' ThreadTask(0.0022).Q 2.891±0.082ms 85: [2.647,3.107]
                    qrt = New isr.Core.Engineering.SampleQuartiles(stat)
                    qrt.Evaluate()
                    actualMean = qrt.FilteredSample.Mean
                    message = Report($"{taskOption}({delay:s\.ffff}).Q", qrt.FilteredSample)
                Else
                    expectedMean = 4.5 ' ThreadTask(0.0022) 2.862±0.219ms 100:[2.197,3.441]
                    actualMean = stat.Mean
                    message = Report($"{taskOption}({delay:s\.ffff})", stat)
                End If
                TestInfo.VerboseMessage(message)
                Assert.IsTrue(actualMean < expectedMean, $"Actual mean {actualMean:0.000}ms should be lower then the expected mean {expectedMean:0.000}ms: {message}")
            End If

            If taskOption = TaskOptions.HighRes Then

                spinCount = 1
                delay = TimeSpan.Zero
                stat = EstimateTaskTime(taskOption, spinCount, delay, count)
                If removeOutliers Then
                    expectedMean = 0.05 ' HighRes(0.00000).Q 0.015±0.003ms 84: [0.009,0.023]
                    qrt = New isr.Core.Engineering.SampleQuartiles(stat)
                    qrt.Evaluate()
                    actualMean = qrt.FilteredSample.Mean
                    message = Report($"{taskOption}({delay:s\.fffff}).Q", qrt.FilteredSample)
                Else
                    expectedMean = 0.5 ' HighRes(0.00000) 0.031±0.137ms 100:[0.003,1.341]
                    actualMean = stat.Mean
                    message = Report($"{taskOption}({delay:s\.fffff})", stat)
                End If
                TestInfo.VerboseMessage(message)
                Assert.IsTrue(actualMean < expectedMean, $"Actual mean {actualMean:0.000}ms should be lower then the expected mean {expectedMean:0.000}ms: {message}")

                spinCount = 1
                delay = (10.0).FromMicroseconds
                stat = EstimateTaskTime(taskOption, spinCount, delay, count)
                If removeOutliers Then
                    expectedMean = 0.09 ' HighRes(0.00001).Q 0.025±0.004ms 86: [0.019,0.037] 
                    qrt = New isr.Core.Engineering.SampleQuartiles(stat)
                    qrt.Evaluate()
                    actualMean = qrt.FilteredSample.Mean
                    message = Report($"{taskOption}({delay:s\.fffff}).Q", qrt.FilteredSample)
                Else
                    expectedMean = 1.5 ' HighRes(0.00001) 0.036±0.033ms 100:[0.014,0.268]
                    actualMean = stat.Mean
                    message = Report($"{taskOption}({delay:s\.fffff})", stat)
                End If
                TestInfo.VerboseMessage(message)
                Assert.IsTrue(actualMean < expectedMean, $"Actual mean {actualMean:0.000}ms should be lower then the expected mean {expectedMean:0.000}ms: {message}")

                spinCount = 1
                delay = (100.0).FromMicroseconds
                stat = EstimateTaskTime(taskOption, spinCount, delay, count)
                If removeOutliers Then
                    expectedMean = 0.2 ' HighRes(0.0001).Q 0.159±0.008ms 84: [0.143,0.19] 
                    qrt = New isr.Core.Engineering.SampleQuartiles(stat)
                    qrt.Evaluate()
                    actualMean = qrt.FilteredSample.Mean
                    message = Report($"{taskOption}({delay:s\.fffff}).Q", qrt.FilteredSample)
                Else
                    expectedMean = 0.3 ' HighRes(0.0001) 0.171±0.033ms 100:[0.126,0.351]
                    actualMean = stat.Mean
                    message = Report($"{taskOption}({delay:s\.fffff})", stat)
                End If
                TestInfo.VerboseMessage(message)
                Assert.IsTrue(actualMean < expectedMean, $"Actual mean {actualMean:0.000}ms should be lower then the expected mean {expectedMean:0.000}ms: {message}")

            End If
        End Sub

        ''' <summary> (Unit Test Method) tests timespan system clock delay statistics. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        <TestMethod()>
        Public Sub TimespanSystemClockDelayNoOpStatisticsTest()
            TestTimespanSystemClockDelayStatistics(TaskOptions.NoOp, False, 20)
            TestTimespanSystemClockDelayStatistics(TaskOptions.NoOp, True, 20)
        End Sub

        ''' <summary> (Unit Test Method) tests timespan system clock delay spin wait statistics. </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        <TestMethod()>
        Public Sub TimespanSystemClockDelaySpinWaitStatisticsTest()
            TestTimespanSystemClockDelayStatistics(TaskOptions.SpinWait, False, 20)
            TestTimespanSystemClockDelayStatistics(TaskOptions.SpinWait, True, 20)
        End Sub

        ''' <summary>
        ''' (Unit Test Method) tests timespan system clock delay task delay statistics.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        <TestMethod()>
        Public Sub TimespanSystemClockDelayTaskDelayStatisticsTest()
            TestTimespanSystemClockDelayStatistics(TaskOptions.TaskDelay, False, 20)
            TestTimespanSystemClockDelayStatistics(TaskOptions.TaskDelay, True, 20)
        End Sub

        ''' <summary>
        ''' (Unit Test Method) tests timespan system clock delay thread task statistics.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        <TestMethod()>
        Public Sub TimespanSystemClockDelayThreadTaskStatisticsTest()
            TestTimespanSystemClockDelayStatistics(TaskOptions.ThreadTask, False, 20)
            TestTimespanSystemClockDelayStatistics(TaskOptions.ThreadTask, True, 20)
        End Sub

        ''' <summary>
        ''' (Unit Test Method) tests timespan system clock delay high resource statistics.
        ''' </summary>
        ''' <remarks> David, 2020-09-15. </remarks>
        <TestMethod()>
        Public Sub TimespanSystemClockDelayHighResStatisticsTest()
            TestTimespanSystemClockDelayStatistics(TaskOptions.HighRes, False, 20)
            TestTimespanSystemClockDelayStatistics(TaskOptions.HighRes, True, 20)
        End Sub


#End Region


    End Class
End Namespace
