''' <summary> Linear fit tests. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2/19/2019 </para>
''' </remarks>
<TestClass()>
Public Class LinearFitTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext
    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

    ''' <summary> (Unit Test Method) builds determinant test method. </summary>
    ''' <remarks>
    ''' Uses data from C:\My\LIBRARIES\VS\Core\Core\Tests\Engineering\Math\PolyFitTests.vb.
    ''' </remarks>
    <TestMethod()>
    Public Sub LinearFitTest()
        Dim linFit As New isr.Core.Engineering.LinearFit
        Dim height As Double() = New Double() {1.47, 1.5, 1.52, 1.55, 1.57, 1.6, 1.63, 1.65, 1.68, 1.7, 1.73, 1.75, 1.78, 1.8, 1.83}
        Dim mass As Double() = New Double() {52.21, 53.12, 54.48, 55.84, 57.2, 58.57, 59.93, 61.29, 63.11, 64.47, 66.28, 68.1, 69.92, 72.19, 74.46}
        Dim points As New List(Of System.Windows.Point)
        For i As Integer = 0 To height.Count - 1
            points.Add(New System.Windows.Point(height(i), mass(i)))
        Next
        Dim slope As Double = 61.272
        Dim intercept As Double = -39.062
        Dim correlationCoefficient As Double = 0.9945
        Dim goodness As Double = correlationCoefficient * correlationCoefficient
        Dim linearStandardError As Double = 1.775
        Dim constantStandardError As Double = 2.938
        linFit.DoFit(height, mass)
        Assert.AreEqual(slope, linFit.LinearCoefficient, 0.001, "Slope")
        Assert.AreEqual(intercept, linFit.ConstantCoefficient, 0.001, "intercept")
        Assert.AreEqual(goodness, linFit.GoodnessOfFit, 0.001, "Goodness of fit")
        Assert.AreEqual(linearStandardError, linFit.LinearCoefficientStandardError, 0.001, "Linear coefficient Standard error")
        Assert.AreEqual(constantStandardError, linFit.ConstantCoefficientStandardError, 0.001, "Constant coefficient Standard error")
        linFit.DoFit(points)
        Assert.AreEqual(slope, linFit.LinearCoefficient, 0.001, "Slope")
        Assert.AreEqual(intercept, linFit.ConstantCoefficient, 0.001, "intercept")
        Assert.AreEqual(goodness, linFit.GoodnessOfFit, 0.001, "Goodness of fit")
        Assert.AreEqual(linearStandardError, linFit.LinearCoefficientStandardError, 0.001, "Linear coefficient Standard error")
        Assert.AreEqual(constantStandardError, linFit.ConstantCoefficientStandardError, 0.001, "Constant coefficient Standard error")
    End Sub

End Class
