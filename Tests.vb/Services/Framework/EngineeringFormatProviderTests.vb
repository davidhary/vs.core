﻿Imports isr.Core.Services

''' <summary>
''' This is a test class for EngineeringFormatProviderTest and is intended to contain all
''' EngineeringFormatProviderTest Unit Tests.
''' </summary>
''' <remarks> David, 2020-09-18. </remarks>
<TestClass()>
Public Class EngineeringFormatProviderTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.Services.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext
    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " ENGINEERING FORMAT PROVIDER TESTS "

    ''' <summary> A test for Formatting. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <param name="value">                   The value. </param>
    ''' <param name="format">                  Describes the format to use. </param>
    ''' <param name="usingThousandsSeparator"> true to using thousands separator. </param>
    ''' <param name="expectedValue">           The expected value. </param>
    Public Shared Sub CustomFormatPiTest(ByVal value As Double, ByVal format As String, ByVal usingThousandsSeparator As Boolean, ByVal expectedValue As String)
        Dim target As EngineeringFormatProvider = New EngineeringFormatProvider With {
            .UsingThousandsSeparator = usingThousandsSeparator
        }
        Dim actual As String = String.Format(target, format, value)
        Assert.AreEqual(expectedValue, actual)
    End Sub

    ''' <summary> A test for Formatting. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <param name="value">         The value. </param>
    ''' <param name="format">        Describes the format to use. </param>
    ''' <param name="expectedValue"> The expected value. </param>
    Public Shared Sub CustomFormatPiTest(ByVal value As Double, ByVal format As String, ByVal expectedValue As String)
        Dim target As IFormatProvider = New EngineeringFormatProvider
        Dim actual As String = String.Format(target, format, value)
        Assert.AreEqual(expectedValue, actual)
    End Sub

    ''' <summary> A test for Formatting PI. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod()>
    Public Sub CustomFormatPiTest()
        CustomFormatPiTest(Math.PI, "{0}", "3.142")
    End Sub

    ''' <summary> A test for Formatting 0.1 * PI. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod()>
    Public Sub CustomFormatDeciPiTest()
        CustomFormatPiTest(0.1 * Math.PI, "{0}", "0.3142")
    End Sub

    ''' <summary> A test for Formatting 0.01 * PI. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod()>
    Public Sub CustomFormatCentiPiTest()
        CustomFormatPiTest(0.01 * Math.PI, "{0}", "31.42e-03")
    End Sub

    ''' <summary> A test for Formatting 0.001 * PI. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod()>
    Public Sub CustomFormatMilliPiTest()
        CustomFormatPiTest(0.001 * Math.PI, "{0}", "3.142e-03")
    End Sub

    ''' <summary> A test for Formatting 0.000001 * PI. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod()>
    Public Sub CustomFormatMicroPiTest()
        CustomFormatPiTest(0.000001 * Math.PI, "{0}", "3.142e-06")
    End Sub

    ''' <summary> A test for Formatting 1000 * PI. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod()>
    Public Sub CustomFormatKiloPiTest()
        CustomFormatPiTest(1000 * Math.PI, "{0}", "3.142e+03")
    End Sub

    ''' <summary> A test for Formatting 1000000 * PI. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod()>
    Public Sub CustomFormatMegaPiTest()
        CustomFormatPiTest(1000000 * Math.PI, "{0}", "3.142e+06")
    End Sub

    ''' <summary> A test for Formatting 1000 * PI. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod()>
    Public Sub CustomFormatKiloWithSeparatorPiTest()
        CustomFormatPiTest(1000 * Math.PI, "{0}", True, "3,142")
    End Sub

    ''' <summary> A test for Formatting 10000 * PI. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod()>
    Public Sub CustomFormatDekaWithSeparatorPiTest()
        CustomFormatPiTest(10000 * Math.PI, "{0}", True, "31,416")
    End Sub

    ''' <summary> A test for Formatting 100000 * PI. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod()>
    Public Sub CustomFormatHectoWithSeparatorPiTest()
        CustomFormatPiTest(100000 * Math.PI, "{0}", True, "314,159")
    End Sub

    ''' <summary> A test for Formatting 1000000 * PI. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod()>
    Public Sub CustomFormatMegaWithSeparatorPiTest()
        CustomFormatPiTest(1000000 * Math.PI, "{0}", True, "3.142e+06")
    End Sub
#End Region

End Class
