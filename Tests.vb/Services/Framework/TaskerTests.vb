Imports System.ComponentModel
Imports System.Data.SqlTypes
Imports System.Diagnostics.Eventing.Reader
Imports System.Threading
Imports System.Threading.Tasks

Imports isr.Core.Services

''' <summary> A tasker tests. </summary>
''' <remarks> David, 2020-09-18. </remarks>
<TestClass()>
Public Class TaskerTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.Services.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext
    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " UNPUBLISHED LOG TESTS  "

    ''' <summary> Assert tasker exception. </summary>
    ''' <remarks> David, 8/6/2020. </remarks>
    ''' <exception cref="OperationCanceledException"> Thrown when an Operation Canceled error condition
    '''                                               occurs. </exception>
    Public Shared Sub AssertTaskerException()
        Dim tasker As New Tasker
        ' task should throw an exception
        Dim activity As String = ""
        activity = "Task started"
        Try
            activity = "Task action"
            tasker.StartAction(Sub()
                                   Throw New OperationCanceledException("thrown from withing the task action")
                               End Sub)
            activity = "Task awaiting task idle"
            Assert.IsTrue(tasker.AwaitTaskIdle(TimeSpan.FromMilliseconds(200)), $"{NameOf(Services.Tasker.AwaitTaskIdle)} should timeout")
        Catch ex As Exception
            Console.Out.WriteLine($"thrown from {activity}")
            Throw New OperationCanceledException($"thrown from {activity}")
        End Try
    End Sub

    ''' <summary> Assert handler tasker exception event. </summary>
    ''' <remarks> David, 8/6/2020. </remarks>
    Public Shared Sub AssertHandlerTaskerExceptionEvent()
        Dim startTime As DateTime
        Dim captureTimestamp As TimeSpan
        Dim exceptionTimestamp As TimeSpan
        Dim actionExitTimestamp As TimeSpan = TimeSpan.Zero
        Dim taskDoneTimestamp As TimeSpan
        Dim activity As String = ""
        Dim isFaulted As Boolean
        Dim exception As Exception = Nothing
        Dim tasker As New Tasker
        AddHandler tasker.AsyncCompleted, Sub(sender As Object, e As AsyncCompletedEventArgs)
                                              isFaulted = e.Error IsNot Nothing
                                              exception = e.Error
                                              captureTimestamp = DateTime.Now.Subtract(startTime)
                                          End Sub
        ' task should throw an exception to be captured by the tasker before the wait occurred.
        activity = "Task started"
        activity = "Task action"
        startTime = DateTime.Now
        tasker.StartAction(Sub()
                               exceptionTimestamp = DateTime.Now.Subtract(startTime)
                               Throw New OperationCanceledException("thrown from withing the task action")
                               Threading.Thread.Sleep(200)
                               actionExitTimestamp = DateTime.Now.Subtract(startTime)
                           End Sub)
        activity = "Task awaiting task idle"
        Assert.IsTrue(tasker.AwaitTaskIdle(TimeSpan.FromMilliseconds(400)), $"{NameOf(Services.Tasker.AwaitTaskIdle)} should not timeout")
        Assert.IsTrue(isFaulted, $"{NameOf(Services.Tasker.ActionTask)}.{NameOf(Threading.Tasks.Task.IsFaulted)} should match")
        Assert.IsNotNull(exception, $"{NameOf(AsyncCompletedEventArgs)}.{NameOf(AsyncCompletedEventArgs.Error)} should not be null")
        Assert.IsNotNull(TryCast(exception.InnerException, OperationCanceledException), $"{NameOf(AsyncCompletedEventArgs)}.{NameOf(AsyncCompletedEventArgs.Error)} should be {NameOf(OperationCanceledException)}")
        Assert.IsFalse(tasker.ActionTask.IsFaulted, $"{NameOf(Services.Tasker.ActionTask)}.{NameOf(Threading.Tasks.Task.IsFaulted)} should match")
        taskDoneTimestamp = DateTime.Now.Subtract(startTime)
        ' Timestamps: Exception 0; Capture: 1.3379; action exit 0; Done 1.3379
        Console.Out.WriteLine($"Timestamps: Exception {exceptionTimestamp.TotalMilliseconds}; Capture: {captureTimestamp.TotalMilliseconds}; action exit {actionExitTimestamp.TotalMilliseconds}; Done {taskDoneTimestamp.TotalMilliseconds}")
    End Sub

    ''' <summary> Assert tasker. </summary>
    ''' <remarks> David, 2020-07-17. </remarks>
    Public Shared Sub AssertTasker()

        Dim tasker As New Tasker
        Dim timeout As TimeSpan = TimeSpan.FromMilliseconds(100)

        Dim delay As TimeSpan = timeout.Subtract(TimeSpan.FromMilliseconds(50))
        tasker.StartAction(Sub()
                               Thread.Sleep(delay)
                           End Sub)
        Assert.IsTrue(tasker.AwaitTaskIdle(timeout), $"{NameOf(Services.Tasker.AwaitTaskIdle)} should not timeout")

        delay = timeout.Add(TimeSpan.FromMilliseconds(50))
        tasker.StartAction(Sub()
                               Thread.Sleep(delay)
                           End Sub)
        Assert.IsFalse(tasker.AwaitTaskIdle(timeout), $"{NameOf(Services.Tasker.AwaitTaskIdle)} should timeout")

        ' MyAssertProperty.MyAssert.Throws(Of OperationCanceledException)(AddressOf AssertTaskerException)
        AssertHandlerTaskerExceptionEvent()

    End Sub

    ''' <summary> (Unit Test Method) tests tasker. </summary>
    ''' <remarks> David, 2020-07-17. </remarks>
    <TestMethod()>
    Public Sub TaskerTest()
        AssertTasker()
    End Sub

    ''' <summary> Assert tasker of tuple. </summary>
    ''' <remarks> David, 2020-07-17. </remarks>
    Public Shared Sub AssertTaskerOfTuple()

        Dim tasker As New Tasker(Of (Success As Boolean, Details As String))

        Dim timeout As TimeSpan = TimeSpan.FromMilliseconds(100)

        Dim delay As TimeSpan = timeout.Subtract(TimeSpan.FromMilliseconds(50))
        Dim expectedResult As (Success As Boolean, Details As String) = (True, "Success")
        tasker.StartAction(Function()
                               Thread.Sleep(delay)
                               Return expectedResult
                           End Function)
        Assert.IsTrue(tasker.AwaitTaskIdle(timeout), $"{NameOf(Services.Tasker.AwaitTaskIdle)} should not timeout")
        Dim result As (Status As TaskStatus, Result As (Success As Boolean, Details As String)) = tasker.AwaitCompletion(timeout)
        Assert.AreEqual(TaskStatus.RanToCompletion, result.Status, $"{NameOf(Tasks.TaskStatus)} should match")
        Assert.AreEqual(expectedResult.Details, result.Result.Details, $"{NameOf(Services.Tasker(Of (Success As Boolean, Details As String)))} Result should match")

        expectedResult = (False, "Failure")
        tasker.StartAction(Function()
                               Thread.Sleep(delay)
                               Return expectedResult
                           End Function)
        result = tasker.AwaitCompletion(timeout)
        Assert.AreEqual(TaskStatus.RanToCompletion, result.Status, $"{NameOf(Tasks.TaskStatus)} should match")
        Assert.AreEqual(expectedResult.Details, result.Result.Details, $"{NameOf(Services.Tasker(Of (Success As Boolean, Details As String)))} Result should match")

    End Sub

    ''' <summary> (Unit Test Method) tests tasker of tuple. </summary>
    ''' <remarks> David, 2020-07-17. </remarks>
    <TestMethod()>
    Public Sub TaskerOfTupleTest()
        AssertTaskerOfTuple()
    End Sub

#End Region



End Class
