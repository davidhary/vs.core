Imports System.Data.SqlTypes
Imports System.IO

Imports isr.Core.FileInfoExtensions
Imports isr.Core.Services
Imports isr.Core.Services.VisualBasicLoggingExtensions

''' <summary> This is a test class for unpublished messages. </summary>
''' <remarks> David, 2020-09-18. </remarks>
<TestClass()>
Public Class UnpublishedLogTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.Services.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext
    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " UNPUBLISHED LOG TESTS  "

    ''' <summary> Tests logging unpublished messages. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <param name="expected">                           The expected. </param>
    ''' <param name="checkUnpublishedMessageLogFileSize"> True to check unpublished message log file
    '''                                                   size. </param>
    Public Shared Sub TestLoggingUnpublishedMessages(ByVal expected As TraceEventType, ByVal checkUnpublishedMessageLogFileSize As Boolean)

        Dim payload As String = "Message 1"
        Dim expectedMessage As New isr.Core.Services.TraceMessage(expected, isr.Core.Services.My.MyLibrary.TraceEventId, payload)
        Dim logger As MyLog = isr.Core.Services.My.MyLibrary.MyLog

        Dim initialLogFileSize As Long = logger.FileSize

        Dim actualLogFileName As String = logger.FullLogFileName
        Assert.IsFalse(String.IsNullOrWhiteSpace(actualLogFileName), $"Tests if log file name '{actualLogFileName}' is empty")

        Dim fi As New FileInfo(actualLogFileName)

        ' clear trace message log
        isr.Core.Services.My.MyLibrary.UnpublishedTraceMessages.DequeueContent()

        ' check content
        Dim actualContents As String = isr.Core.Services.My.MyLibrary.UnpublishedTraceMessages.DequeueContent()
        Assert.IsTrue(String.IsNullOrWhiteSpace(actualContents), "Unpublished messages should clear")

        ' log an unpublished trace message to the library.
        isr.Core.Services.My.MyLibrary.LogUnpublishedMessage(expectedMessage)

        Dim actualMessage As isr.Core.Services.TraceMessage = isr.Core.Services.My.MyLibrary.UnpublishedTraceMessages.TryPeek
        Assert.AreEqual(expectedMessage.Id, actualMessage.Id, " Message trace events identities match")

        Dim expectedAutoFlush As Boolean = False
        Dim actualAutoFlush As Boolean = logger.FileLogTraceListener.AutoFlush
        Assert.AreEqual(expectedAutoFlush, actualAutoFlush, "initial auto flash should match")
        Assert.AreEqual(expectedAutoFlush, logger.TraceSource.DefaultFileLogWriter.AutoFlush, "initial auto flash should match")

        logger.FileLogTraceListener.AutoFlush = False
        expectedAutoFlush = False
        actualAutoFlush = logger.FileLogTraceListener.AutoFlush
        Assert.AreEqual(expectedAutoFlush, actualAutoFlush, "changed auto flash should match")
        Assert.AreEqual(expectedAutoFlush, logger.TraceSource.DefaultFileLogWriter.AutoFlush, "initial auto flash should match")

        logger.FlushMessages()

        If checkUnpublishedMessageLogFileSize Then
            ' Under C#, the file size changed only after the test engine completes.
            isr.Core.Services.My.MyLibrary.DoEvents()
            isr.Core.Services.My.MyLibrary.Delay(50)
            logger.Flush()
            isr.Core.Services.My.MyLibrary.Delay(100)
            logger.FileLogTraceListener.Flush()
            isr.Core.Services.My.MyLibrary.Delay(100)
            Dim newLogFileSize As Long
#If False Then
            ' this is the wrong path!
            ' the log extensions are incorrect!
            fi = New FileInfo(logger.DefaultFileLogWriterFilePath)
            newLogFileSize = fi.FileSize()
#ElseIf False Then
            fi.Refresh()
            newLogFileSize = fi.FileSize()
#Else
            newLogFileSize = logger.FileSize()
#End If
            Assert.IsTrue(newLogFileSize > initialLogFileSize, $"New file Size {newLogFileSize} > initial file size {initialLogFileSize}")
        End If


    End Sub

    ''' <summary> (Unit Test Method) tests log unpublished messages. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod()>
    Public Sub LogUnpublishedMessagesTest()
        TestLoggingUnpublishedMessages(TraceEventType.Warning, My.Settings.CheckUnpublishedMessageLogFileSize)
    End Sub

    ''' <summary> my logger. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    Private Class MyLogger
        Inherits Microsoft.VisualBasic.Logging.Log

    End Class

    ''' <summary> Tests logging unpublished messages. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <param name="expected"> The expected. </param>
    Public Shared Sub TestLoggingUnpublishedMessages(ByVal expected As TraceEventType)

        Dim payload As String = "Message 1"
        Dim expectedMessage As New isr.Core.Services.TraceMessage(expected, isr.Core.Services.My.MyLibrary.TraceEventId, payload)
        Dim logger As MyLog = isr.Core.Services.My.MyLibrary.MyLog

        Dim initialLogFileSize As Long = logger.FileSize

        Dim actualLogFileName As String = logger.FullLogFileName
        Assert.IsFalse(String.IsNullOrWhiteSpace(actualLogFileName), $"Tests if log file name '{actualLogFileName}' is empty")

        Dim fi As New FileInfo(actualLogFileName)

        Dim expectedAutoFlush As Boolean = False
        Dim actualAutoFlush As Boolean = logger.DefaultFileLogWriter.AutoFlush
        Assert.AreEqual(expectedAutoFlush, actualAutoFlush, "initial auto flash should match")

        expectedAutoFlush = False
        logger.DefaultFileLogWriter.AutoFlush = expectedAutoFlush
        actualAutoFlush = logger.DefaultFileLogWriter.AutoFlush
        Assert.AreEqual(expectedAutoFlush, actualAutoFlush, "new auto flash should match")

        ' initialLogFileSize = fi.FileSize()
        logger.WriteEntry($"message {Date.Now:o}", TraceEventType.Error, 1)
        isr.Core.Services.My.MyLibrary.Delay(200)
        logger.DefaultFileLogWriter.Flush()
        ' logger.DefaultFileLogWriter.Close()
        isr.Core.Services.My.MyLibrary.Delay(200)
        fi.Refresh()
        Dim newLogFileSize As Long = fi.FileSize()
        Assert.IsTrue(newLogFileSize > initialLogFileSize, $"New file Size {newLogFileSize} > initial file size {initialLogFileSize}")

    End Sub

    ''' <summary> Assert logging. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    Public Shared Sub AssertLogging()

        Dim logger As Microsoft.VisualBasic.Logging.Log = isr.Core.Services.My.MyLibrary.ApplicationLog
        Dim fi As New FileInfo(logger.DefaultFileLogWriterFilePath)

        Dim expectedAutoFlush As Boolean = False
        Dim actualAutoFlush As Boolean = logger.DefaultFileLogWriter.AutoFlush
        Assert.AreEqual(expectedAutoFlush, actualAutoFlush, "initial auto flash should match")

        expectedAutoFlush = False
        logger.DefaultFileLogWriter.AutoFlush = expectedAutoFlush
        actualAutoFlush = logger.DefaultFileLogWriter.AutoFlush
        Assert.AreEqual(expectedAutoFlush, actualAutoFlush, "new auto flash should match")

        Dim initialLogFileSize As Long = fi.FileSize()
        logger.WriteEntry($"message {Date.Now:o}")
        logger.WriteEntry($"message {Date.Now:o}", TraceEventType.Error, 1)
        isr.Core.Services.My.MyLibrary.Delay(200)
        logger.DefaultFileLogWriter.Flush()
        ' logger.DefaultFileLogWriter.Close()
        isr.Core.Services.My.MyLibrary.Delay(200)
        fi.Refresh()
        Dim newLogFileSize As Long = fi.FileSize()
        Assert.IsTrue(newLogFileSize > initialLogFileSize, $"New file Size {newLogFileSize} > initial file size {initialLogFileSize}")


    End Sub

    ''' <summary> (Unit Test Method) tests application log. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod()>
    Public Sub ApplicationLogTest()
        ' the naked logger (with the default file logger works are expected.
        ' See what needs to be done to get the replaced logger to work correctly and then write a new logger.
        ' possibly, make it part of the Framework and then write extension methods to add the trace messages.
        AssertLogging()
        ' TestLoggingUnpublishedMessages(TraceEventType.Warning)
    End Sub


#End Region

End Class
