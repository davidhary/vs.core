'           isr.Core.Services
Imports isr.Core.Services

''' <summary> This is a test class for event handling. </summary>
''' <remarks> David, 2020-09-18. </remarks>
<TestClass()>
Public Class EventHandlingTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.Services.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext
    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " EVENT HANDLER CONTEXT OF SYSTEM EVENT ARGS TEST "

    ''' <summary> Handles the refresh requested. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub HandleRefreshRequested(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim programInfoSender As ProgramInfo = TryCast(sender, ProgramInfo)
        If programInfoSender Is Nothing Then Return
        programInfoSender.AppendLine($"Line #{programInfoSender.Lines.Count}",
                                         New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)))
    End Sub

    ''' <summary> (Unit Test Method) tests event handler context. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod()>
    Public Sub EventHandlerContextTest()
        Dim sender As New ProgramInfo
        AddHandler sender.RefreshRequested, AddressOf Me.HandleRefreshRequested
        Dim expectedLineCount As Integer = sender.Lines.Count + 1
        sender.NotifyRefreshRequested()
        Assert.AreEqual(expectedLineCount, sender.Lines.Count, "line count should equal after handling the refresh requested event")
    End Sub

#End Region

#Region " ADDING NEW TEST "

    ''' <summary> Handles the adding new. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Adding new event information. </param>
    Private Sub HandleAddingNew(ByVal sender As Object, ByVal e As ComponentModel.AddingNewEventArgs)
        Dim collectionSender As BindingListView(Of String) = TryCast(sender, BindingListView(Of String))
        If collectionSender Is Nothing Then Return
        e.NewObject = NameOf(e.NewObject)
    End Sub

    ''' <summary> (Unit Test Method) tests binding list adding new handling. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod()>
    Public Sub BindingListAddingNewHandlingTest()
        Dim l As New List(Of String)
        Using sender As New BindingListView(Of String)(l)
            AddHandler sender.AddingNew, AddressOf Me.HandleAddingNew
            Dim expectedNewItemsCount As Integer = 0
            Assert.AreEqual(expectedNewItemsCount, sender.NewItemsList.Count, "expects no new items")
            l.Add($"Item #{l.Count + 1}")
            expectedNewItemsCount += 1
            Assert.AreEqual(expectedNewItemsCount, sender.NewItemsList.Count, "expects new items after adding one item")
        End Using
    End Sub

#End Region

#Region " ACTION NOTIFY TEST "
    ''' <summary> The actual notified value. </summary>
    Private _ActualNotifiedValue As String = String.Empty

    ''' <summary> Handles the action notify described by value. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub HandleActionNotify(ByVal value As String)
        Me._ActualNotifiedValue = value
    End Sub

    ''' <summary> Registers the action. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <param name="sender">             Source of the event. </param>
    ''' <param name="engineFailedAction"> The engine failed action. </param>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    Private Sub RegisterAction(sender As ActionNotifier(Of String), ByVal engineFailedAction As Action(Of String))
        sender.Register(engineFailedAction)
    End Sub

    ''' <summary> (Unit Test Method) tests action notifier handling. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod()>
    Public Sub ActionNotifierHandlingTest()
        Dim sender As New ActionNotifier(Of String)
        Me.RegisterAction(sender, AddressOf Me.HandleActionNotify)
        Dim expectedNotificationValue As String = $"{NameOf(ActionNotifier(Of String).SyncInvoke)}"
        sender.SyncInvoke(expectedNotificationValue)
        Assert.AreEqual(expectedNotificationValue, Me._ActualNotifiedValue, "expects a notification value")
    End Sub

#End Region

End Class
