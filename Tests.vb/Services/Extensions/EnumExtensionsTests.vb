Imports isr.Core.Services
Imports isr.Core.EnumExtensions
Namespace ExtensionsTests

    ''' <summary>
    ''' This is a test class for EnumExtensionsTest and is intended to contain all EnumExtensionsTest
    ''' Unit Tests.
    ''' </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestClass()>
    Public Class EnumExtensionsTests

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <remarks>
        ''' Use ClassInitialize to run code before running the first test in the class.
        ''' </remarks>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
        <ClassInitialize(), CLSCompliant(False)>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                _TestSite = New TestSite
                _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
                _TestSite.AddTraceMessagesQueue(isr.Core.Services.My.MyLibrary.UnpublishedTraceMessages)
                _TestSite.InitializeTraceListener()

            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
            _TestSite?.Dispose()
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestInitialize()> Public Sub MyTestInitialize()
            ' assert reading of test settings from the configuration file.
            Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
            Dim expectedUpperLimit As Double = 12
            Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
            TestInfo.ClearMessageQueue()
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestCleanup()> Public Sub MyTestCleanup()
            TestInfo.AssertMessageQueue()
        End Sub

        ''' <summary>
        ''' Gets the test context which provides information about and functionality for the current test
        ''' run.
        ''' </summary>
        ''' <value> The test context. </value>
        Public Property TestContext() As TestContext
        ''' <summary> The test site. </summary>
        Private Shared _TestSite As TestSite

        ''' <summary> Gets information describing the test. </summary>
        ''' <value> Information describing the test. </value>
        Private Shared ReadOnly Property TestInfo() As TestSite
            Get
                If _TestSite Is Nothing Then
                    _TestSite = New TestSite
                    _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
                    _TestSite.AddTraceMessagesQueue(isr.Core.Services.My.MyLibrary.UnpublishedTraceMessages)
                End If
                Return _TestSite
            End Get
        End Property

#End Region

#Region " ENUM EXTENSION TEST: VALUES "

        ''' <summary> (Unit Test Method) tests enum values include. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestMethod()>
        Public Sub EnumValuesIncludeTest()
            Dim allValues As IEnumerable(Of System.Enum) = GetType(System.Diagnostics.TraceEventType).EnumValues
            Assert.IsTrue(allValues.Any, $"{GetType(TraceEventType)}.EnumValues(of System.Enum) has values")
            Assert.AreEqual([Enum].GetValues(GetType(TraceEventType)).Length, allValues.Count, $"{GetType(TraceEventType)} count matches the extension result")
            Assert.IsTrue(allValues.Contains(TraceEventType.Critical), $"{TraceEventType.Critical} is included in the enum values of {GetType(TraceEventType)}")
            Dim allLongs As IEnumerable(Of Long) = GetType(TraceEventType).Values
            Assert.IsTrue(allLongs.Any, $"{GetType(TraceEventType)}.EnumValues(of Long) has values")
            Assert.AreEqual([Enum].GetValues(GetType(TraceEventType)).Length, allValues.Count, $"{GetType(TraceEventType)}.EnumValues(of Long) count matches the extension result")

            Dim includeMask As Long = TraceEventType.Critical Or TraceEventType.Error
            Dim filteredValues As IEnumerable(Of System.Enum) = allValues.IncludeFilter(includeMask)
            Dim expectedCount As Integer = 2
            Assert.IsTrue(filteredValues.Any, $"{GetType(TraceEventType)} filtered Values has values")
            Assert.AreEqual(expectedCount, filteredValues.Count, $"{GetType(TraceEventType)} filtered values has expected count")
            Assert.IsTrue(filteredValues.Contains(TraceEventType.Critical), $"{TraceEventType.Critical} is included in the filtered values of {GetType(TraceEventType)}")
            Assert.IsFalse(filteredValues.Contains(TraceEventType.Information), $"{TraceEventType.Information} is not included in the filtered values of {GetType(TraceEventType)}")

            Dim filteredLongs As IEnumerable(Of Long) = allLongs.IncludeFilter(includeMask)
            Assert.IsTrue(filteredLongs.Any, $"{GetType(TraceEventType)} filtered Longs has Longs")
            Assert.AreEqual(expectedCount, filteredLongs.Count, $"{GetType(TraceEventType)} filtered Longs has expected count")
            Assert.IsTrue(filteredLongs.Contains(TraceEventType.Critical), $"{TraceEventType.Critical} is included in the filtered Longs of {GetType(TraceEventType)}")
            Assert.IsFalse(filteredLongs.Contains(TraceEventType.Information), $"{TraceEventType.Information} is not included in the filtered Longs of {GetType(TraceEventType)}")

            filteredValues = allValues.IncludeFilter({TraceEventType.Critical, TraceEventType.Error})
            expectedCount = 2
            Assert.IsTrue(filteredValues.Any, $"{GetType(TraceEventType)} filtered Values has list of values")
            Assert.AreEqual(expectedCount, filteredValues.Count, $"{GetType(TraceEventType)} filtered lists of values has expected count")
            Assert.IsTrue(filteredValues.Contains(TraceEventType.Critical), $"{TraceEventType.Critical} is included in the filtered list of values of {GetType(TraceEventType)}")
            Assert.IsFalse(filteredValues.Contains(TraceEventType.Information), $"{TraceEventType.Information} is not included in the filtered list of values of {GetType(TraceEventType)}")

        End Sub

        ''' <summary> (Unit Test Method) tests enum values exclude. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestMethod()>
        Public Sub EnumValuesExcludeTest()
            Dim allValues As IEnumerable(Of System.Enum) = GetType(System.Diagnostics.TraceEventType).EnumValues
            Assert.IsTrue(allValues.Any, $"{GetType(TraceEventType)}.EnumValues(of System.Enum) has values")
            Assert.AreEqual([Enum].GetValues(GetType(TraceEventType)).Length, allValues.Count, $"{GetType(TraceEventType)} count matches the extension result")
            Assert.IsTrue(allValues.Contains(TraceEventType.Critical), $"{TraceEventType.Critical} is Excluded in the enum values of {GetType(TraceEventType)}")
            Dim allLongs As IEnumerable(Of Long) = GetType(TraceEventType).Values
            Assert.IsTrue(allLongs.Any, $"{GetType(TraceEventType)}.EnumValues(of Long) has values")
            Assert.AreEqual([Enum].GetValues(GetType(TraceEventType)).Length, allValues.Count, $"{GetType(TraceEventType)}.EnumValues(of Long) count matches the extension result")

            Dim excludeMask As Long = TraceEventType.Critical Or TraceEventType.Error
            Dim filteredValues As IEnumerable(Of System.Enum) = allValues.ExcludeFilter(excludeMask)
            Dim expectedCount As Integer = allValues.Count - 2
            Assert.IsTrue(filteredValues.Any, $"{GetType(TraceEventType)} filtered Values has values")
            Assert.AreEqual(expectedCount, filteredValues.Count, $"{GetType(TraceEventType)} filtered values has expected count")
            Assert.IsFalse(filteredValues.Contains(TraceEventType.Critical), $"{TraceEventType.Critical} is Excluded in the filtered values of {GetType(TraceEventType)}")
            Assert.IsTrue(filteredValues.Contains(TraceEventType.Information), $"{TraceEventType.Information} is not Excluded in the filtered values of {GetType(TraceEventType)}")

            Dim filteredLongs As IEnumerable(Of Long) = allLongs.ExcludeFilter(excludeMask)
            Assert.IsTrue(filteredLongs.Any, $"{GetType(TraceEventType)} filtered Longs has Longs")
            Assert.AreEqual(expectedCount, filteredLongs.Count, $"{GetType(TraceEventType)} filtered Longs has expected count")
            Assert.IsFalse(filteredLongs.Contains(TraceEventType.Critical), $"{TraceEventType.Critical} is Excluded in the filtered Longs of {GetType(TraceEventType)}")
            Assert.IsTrue(filteredLongs.Contains(TraceEventType.Information), $"{TraceEventType.Information} is not Excluded in the filtered Longs of {GetType(TraceEventType)}")

            filteredValues = allValues.ExcludeFilter({TraceEventType.Critical, TraceEventType.Error})
            expectedCount = allValues.Count - 2
            Assert.IsTrue(filteredValues.Any, $"{GetType(TraceEventType)} filtered Values has list of values")
            Assert.AreEqual(expectedCount, filteredValues.Count, $"{GetType(TraceEventType)} filtered lists of values has expected count")
            Assert.IsFalse(filteredValues.Contains(TraceEventType.Critical), $"{TraceEventType.Critical} is Excluded in the filtered list of values of {GetType(TraceEventType)}")
            Assert.IsTrue(filteredValues.Contains(TraceEventType.Information), $"{TraceEventType.Information} is not Excluded in the filtered list of values of {GetType(TraceEventType)}")

            allValues = TraceEventType.Critical.EnumValues
            Assert.IsTrue(allValues.Any, $"{GetType(TraceEventType)}.EnumValues(of System.Enum) has values")
            Assert.AreEqual([Enum].GetValues(GetType(TraceEventType)).Length, allValues.Count, $"{GetType(TraceEventType)} count matches the extension result")
            Assert.IsTrue(allValues.Contains(TraceEventType.Critical), $"{TraceEventType.Critical} is Excluded in the enum values of {GetType(TraceEventType)}")

            Dim inclusionMask As TraceEventType = TraceEventType.Critical Or TraceEventType.Error Or TraceEventType.Information Or TraceEventType.Warning
            Dim exclusionMask As TraceEventType = TraceEventType.Verbose
            expectedCount = 4

            filteredValues = TraceEventType.Critical.EnumValues.Filter(inclusionMask, exclusionMask)
            Assert.IsTrue(filteredValues.Any, $"{GetType(TraceEventType)} Filtered Enum Values has values")
            Assert.AreEqual(expectedCount, filteredValues.Count, $"{GetType(TraceEventType)} filtered count matches the expected count")
            Assert.IsTrue(filteredValues.Contains(TraceEventType.Critical), $"{TraceEventType.Critical} is included in the filtered")
            Assert.IsFalse(filteredValues.Contains(TraceEventType.Verbose), $"{TraceEventType.Verbose} is Excluded in the filtered")

        End Sub

        ''' <summary> (Unit Test Method) tests value descriptions. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestMethod()>
        Public Sub ValueDescriptionsTest()
            Dim pairs As IEnumerable(Of KeyValuePair(Of System.Enum, String)) = GetType(TraceEventType).ValueDescriptionPairs
            Dim expectedValue As TraceEventType = TraceEventType.Critical
            Assert.IsTrue(pairs.Any, $"{GetType(TraceEventType)}.{NameOf(Methods.ValueDescriptionPairs)} has values")
            Assert.IsTrue(pairs.ContainsKey(expectedValue), $"{GetType(TraceEventType)} contains {expectedValue}")
            Dim pair As KeyValuePair(Of System.Enum, String) = pairs.SelectPair(expectedValue, expectedValue.Description)
            Assert.AreEqual(pair.Key, expectedValue, $"{GetType(TraceEventType)} found {expectedValue}")
            Dim defaultValue As TraceEventType = TraceEventType.Error
            Dim description As String = "incorrect Description"
            pair = pairs.SelectPair(defaultValue, "incorrect Description")
            Assert.AreEqual(defaultValue, pair.Key, $"{GetType(TraceEventType)} found {defaultValue} due to {description}")

            pairs = GetType(TraceEventType).ValueNamePairs
            expectedValue = TraceEventType.Critical
            Assert.IsTrue(pairs.Any, $"{GetType(TraceEventType)}.{NameOf(Methods.ValueDescriptionPairs)} has value names")
            Assert.IsTrue(pairs.ContainsKey(expectedValue), $"{GetType(TraceEventType)} value names contains {expectedValue}")

            Dim descriptions As IEnumerable(Of String) = TraceEventType.Critical.Descriptions
            Assert.IsTrue(descriptions.Any, $"{GetType(TraceEventType)}.{NameOf(descriptions)} has description")
            Dim expectedDescription As String = TraceEventType.Critical.Description
            Assert.IsTrue(descriptions.Contains(expectedDescription), $"{GetType(TraceEventType)} value descriptions contains {expectedDescription}")

            Dim names As IEnumerable(Of String) = pairs.ToValues
            Assert.IsTrue(names.Any, $"{GetType(TraceEventType)}.{NameOf(names)} has name")
            Dim expectedname As String = TraceEventType.Critical.ToString
            Assert.IsTrue(names.Contains(expectedname), $"{GetType(TraceEventType)} value names contains {expectedname}")
        End Sub


#End Region

#Region " ENUM EXTENSION TESTS "

        ''' <summary> A test for GetDescription. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestMethod()>
        Public Sub GetDescriptionTest()
            Dim value As MyDialogResult = MyDialogResult.Abort
            Dim expected As String = MyDialogResult.Abort.ToString
            Dim actual As String
            actual = value.Description
            Assert.AreEqual(expected, actual)
            Assert.AreEqual(expected, value.Description())
        End Sub

#End Region

    End Class
End Namespace
