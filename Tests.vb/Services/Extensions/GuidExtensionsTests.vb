Imports isr.Core.GuidExtensions
Namespace ExtensionsTests

    ''' <summary> GUID extensions tests. </summary>
    ''' <remarks>
    ''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 3/14/2018 </para>
    ''' </remarks>
    <TestClass()>
    Public Class GuidExtensionsTests

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <remarks>
        ''' Use ClassInitialize to run code before running the first test in the class.
        ''' </remarks>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
        <ClassInitialize(), CLSCompliant(False)>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                _TestSite = New TestSite
                _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
                _TestSite.AddTraceMessagesQueue(isr.Core.Services.My.MyLibrary.UnpublishedTraceMessages)
                _TestSite.InitializeTraceListener()

            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
            _TestSite?.Dispose()
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestInitialize()> Public Sub MyTestInitialize()
            ' assert reading of test settings from the configuration file.
            Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
            Dim expectedUpperLimit As Double = 12
            Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
            TestInfo.ClearMessageQueue()
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestCleanup()> Public Sub MyTestCleanup()
            TestInfo.AssertMessageQueue()
        End Sub

        ''' <summary>
        ''' Gets the test context which provides information about and functionality for the current test
        ''' run.
        ''' </summary>
        ''' <value> The test context. </value>
        Public Property TestContext() As TestContext
        ''' <summary> The test site. </summary>
        Private Shared _TestSite As TestSite

        ''' <summary> Gets information describing the test. </summary>
        ''' <value> Information describing the test. </value>
        Private Shared ReadOnly Property TestInfo() As TestSite
            Get
                If _TestSite Is Nothing Then
                    _TestSite = New TestSite
                    _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
                    _TestSite.AddTraceMessagesQueue(isr.Core.Services.My.MyLibrary.UnpublishedTraceMessages)
                End If
                Return _TestSite
            End Get
        End Property

#End Region

#Region " GUID EXTENSION TESTS "

        ''' <summary> (Unit Test Method) tests base 64 unique identifier. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestMethod()>
        Public Sub Base64GuidTest()
            Dim newGuid As Guid = Guid.NewGuid
            Dim base64Guid As String = Convert.ToBase64String(newGuid.ToByteArray)
            Dim fromBase64 As Guid = New Guid(Convert.FromBase64String(base64Guid))
            Assert.AreEqual(newGuid, fromBase64, "Converting from GUID to base 64 and back")
            Dim ExtBase64Guid As String = newGuid.NewBase64Guid()
            Assert.AreEqual(base64Guid, ExtBase64Guid.ToBase64String, "Comparing conversion and extension")
            fromBase64 = base64Guid.FromBase64Guid
            Assert.AreEqual(newGuid, fromBase64, "Extension conversion from GUID to base 64 and back")
        End Sub

#End Region

    End Class
End Namespace
