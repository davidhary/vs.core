Imports isr.Core.TimeSpanExtensions
Namespace ExtensionsTests

    ''' <summary> Time Span extensions tests. </summary>
    ''' <remarks>
    ''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 3/14/2018 </para>
    ''' </remarks>
    <TestClass()>
    Public Class TimeSpanExtensionsTests

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <remarks>
        ''' Use ClassInitialize to run code before running the first test in the class.
        ''' </remarks>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
        <ClassInitialize(), CLSCompliant(False)>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                _TestSite = New TestSite
                _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
                _TestSite.AddTraceMessagesQueue(isr.Core.Services.My.MyLibrary.UnpublishedTraceMessages)
                _TestSite.InitializeTraceListener()

            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
            _TestSite?.Dispose()
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestInitialize()> Public Sub MyTestInitialize()
            ' assert reading of test settings from the configuration file.
            Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
            Dim expectedUpperLimit As Double = 12
            Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
            TestInfo.ClearMessageQueue()
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestCleanup()> Public Sub MyTestCleanup()
            TestInfo.AssertMessageQueue()
        End Sub

        ''' <summary>
        ''' Gets the test context which provides information about and functionality for the current test
        ''' run.
        ''' </summary>
        ''' <value> The test context. </value>
        Public Property TestContext() As TestContext
        ''' <summary> The test site. </summary>
        Private Shared _TestSite As TestSite

        ''' <summary> Gets information describing the test. </summary>
        ''' <value> Information describing the test. </value>
        Private Shared ReadOnly Property TestInfo() As TestSite
            Get
                If _TestSite Is Nothing Then
                    _TestSite = New TestSite
                    _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
                    _TestSite.AddTraceMessagesQueue(isr.Core.Services.My.MyLibrary.UnpublishedTraceMessages)
                End If
                Return _TestSite
            End Get
        End Property

#End Region

#Region " TIMESPAN TIME "

        ''' <summary> (Unit Test Method) tests exact time span. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestMethod()>
        Public Sub ExactTimeSpanTest()
            Dim expectedTimespan As TimeSpan
            Dim actualTimespan As TimeSpan
            Dim expectedMilliseconds As Double
            Dim actualMilliseconds As Double
            Dim expectedMicroseconds As Double
            Dim actualMicroseconds As Double
            Dim expectedSeconds As Double
            Dim actualSeconds As Double
            Dim ticksEpsilon As Long = 2

            Dim stepSize As Long = 7
            Dim ticks As Long = 1
            Do While ticks < 100000

                expectedSeconds = ticks * TimeSpanExtensions.Methods.SecondsPerTick
                expectedTimespan = TimeSpan.FromTicks(ticks)
                actualTimespan = expectedSeconds.FromSeconds
                Asserts.Get.AreEqual(expectedTimespan, actualTimespan, TimeSpan.FromTicks(1),
                                            $"Expected timespan from ticks should equal precise time span from seconds within {TimeSpan.FromTicks(1)}")
                Assert.AreEqual(ticks, actualTimespan.Ticks, ticksEpsilon, $"Ticks of timespan converted to Seconds should match withing {ticksEpsilon } tick(s)")
                Assert.AreEqual(expectedSeconds, actualTimespan.ToSeconds, 2 * TimeSpanExtensions.Methods.SecondsPerTick, $"Timespan converted to seconds should match")
                actualSeconds = actualTimespan.ToSeconds
                Assert.AreEqual(expectedSeconds, actualSeconds, 2 * TimeSpanExtensions.Methods.SecondsPerTick,
                                            $"Timespan seconds should match within {2 * TimeSpanExtensions.Methods.SecondsPerTick}s")

                expectedMilliseconds = ticks * TimeSpanExtensions.Methods.MillisecondsPerTick
                expectedTimespan = TimeSpan.FromTicks(ticks)
                actualTimespan = expectedMilliseconds.FromMilliseconds
                Asserts.Get.AreEqual(expectedTimespan, actualTimespan, TimeSpan.FromTicks(1),
                                     $"Expected timespan from ticks should equal precise time span from milliseconds within {TimeSpan.FromTicks(1)}")
                Assert.AreEqual(ticks, actualTimespan.Ticks, ticksEpsilon, $"Ticks of timespan converted to milliseconds should match withing {ticksEpsilon } tick(s)")
                Assert.AreEqual(0.001 * expectedMilliseconds, actualTimespan.ToSeconds, TimeSpanExtensions.Methods.SecondsPerTick, $"Timespan converted to seconds should match")
                actualMilliseconds = actualTimespan.ToMilliseconds
                Assert.AreEqual(expectedMilliseconds, actualMilliseconds, TimeSpanExtensions.Methods.MillisecondsPerTick,
                                        $"Timespan milliseconds should match within {TimeSpanExtensions.Methods.MillisecondsPerTick}ms")

                expectedMicroseconds = ticks * TimeSpanExtensions.Methods.MicrosecondsPerTick
                expectedTimespan = TimeSpan.FromTicks(ticks)
                actualTimespan = expectedMicroseconds.FromMicroseconds
                Asserts.Get.AreEqual(expectedTimespan, actualTimespan, TimeSpan.FromTicks(1),
                                            $"Expected timespan from ticks should equal precise time span from Microseconds within {TimeSpan.FromTicks(1)}")
                Assert.AreEqual(ticks, actualTimespan.Ticks, ticksEpsilon, $"Ticks of timespan converted to microseconds should match withing {ticksEpsilon } tick(s)")
                Assert.AreEqual(0.000001 * expectedMicroseconds, actualTimespan.ToSeconds, TimeSpanExtensions.Methods.SecondsPerTick, $"Timespan converted to seconds should match")
                actualMicroseconds = actualTimespan.ToMicroseconds
                Assert.AreEqual(expectedMicroseconds, actualMicroseconds, TimeSpanExtensions.Methods.MicrosecondsPerTick,
                                        $"Timespan Microseconds should match within {TimeSpanExtensions.Methods.MicrosecondsPerTick}µs")

                expectedTimespan = actualMilliseconds.FromMilliseconds
                actualTimespan = (1000 * actualMilliseconds).FromMicroseconds
                Asserts.Get.AreEqual(expectedTimespan, actualTimespan, TimeSpan.FromTicks(2),
                                        $"Time spans converted from Milliseconds and Microseconds should match within {TimeSpan.FromTicks(2)}")

                expectedTimespan = actualMilliseconds.FromMilliseconds
                actualTimespan = (0.001 * actualMilliseconds).FromSeconds
                Asserts.Get.AreEqual(expectedTimespan, actualTimespan, TimeSpan.FromTicks(2),
                                        $"Time spans converted from Milliseconds and seconds should match within {TimeSpan.FromTicks(2)}")

                ' Test timespan subtraction and additions
                expectedTimespan = (2 * actualMilliseconds).FromMilliseconds
                actualTimespan = actualTimespan.Add(actualTimespan)
                Asserts.Get.AreEqual(expectedTimespan, actualTimespan, TimeSpan.FromTicks(2),
                                        $"Added time spans converted from Milliseconds should match within {TimeSpan.FromTicks(2)}")

                expectedTimespan = (actualMilliseconds).FromMilliseconds
                actualTimespan = actualTimespan.Subtract(expectedTimespan)
                Asserts.Get.AreEqual(expectedTimespan, actualTimespan, TimeSpan.FromTicks(2),
                                        $"Subtracted time spans converted from Milliseconds should match within {TimeSpan.FromTicks(2)}")

                expectedTimespan = TimeSpan.Zero
                actualTimespan = actualTimespan.Subtract(actualTimespan)
                Asserts.Get.AreEqual(expectedTimespan, actualTimespan, TimeSpan.Zero,
                                        $"Self subtracted time spans should be {TimeSpan.Zero}")

                Select Case ticks
                    Case < 100
                        stepSize = 7
                    Case < 1000
                        stepSize = 71
                    Case < 10000
                        stepSize = 711
                    Case < 100000
                        stepSize = 7101
                End Select
                ticks += stepSize
            Loop

        End Sub

#End Region

    End Class
End Namespace
