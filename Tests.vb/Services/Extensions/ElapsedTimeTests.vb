Imports isr.Core.TimeSpanExtensions
Namespace ExtensionsTests

    ''' <summary> Tests for time span calculations. </summary>
    ''' <remarks>
    ''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 3/14/2018 </para>
    ''' </remarks>
    <TestClass()>
    Public Class ElapsedTimeTests

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <remarks>
        ''' Use ClassInitialize to run code before running the first test in the class.
        ''' </remarks>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
        <ClassInitialize(), CLSCompliant(False)>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                _TestSite = New TestSite
                _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
                _TestSite.AddTraceMessagesQueue(isr.Core.Services.My.MyLibrary.UnpublishedTraceMessages)
                _TestSite.InitializeTraceListener()

            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
            _TestSite?.Dispose()
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestInitialize()> Public Sub MyTestInitialize()
            ' assert reading of test settings from the configuration file.
            Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
            Dim expectedUpperLimit As Double = 12
            Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
            TestInfo.ClearMessageQueue()
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestCleanup()> Public Sub MyTestCleanup()
            TestInfo.AssertMessageQueue()
        End Sub

        ''' <summary>
        ''' Gets the test context which provides information about and functionality for the current test
        ''' run.
        ''' </summary>
        ''' <value> The test context. </value>
        Public Property TestContext() As TestContext
        ''' <summary> The test site. </summary>
        Private Shared _TestSite As TestSite

        ''' <summary> Gets information describing the test. </summary>
        ''' <value> Information describing the test. </value>
        Private Shared ReadOnly Property TestInfo() As TestSite
            Get
                If _TestSite Is Nothing Then
                    _TestSite = New TestSite
                    _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
                    _TestSite.AddTraceMessagesQueue(isr.Core.Services.My.MyLibrary.UnpublishedTraceMessages)
                End If
                Return _TestSite
            End Get
        End Property

#End Region

#Region " ELAPSED TIME TESTS "

        ''' <summary> Measure sleep time. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        ''' <param name="sleepTime">       The sleep time. </param>
        ''' <param name="firstStopWatch">  The first stop watch. </param>
        ''' <param name="secondStopwatch"> The second stopwatch. </param>
        ''' <returns>
        ''' A (FirstStartTime As DateTime, FirstEndTime As DateTime, SecondStartTime As DateTime,
        ''' SecondEndTime As DateTime)
        ''' </returns>
        Private Shared Function MeasureSleepTime(ByVal sleepTime As TimeSpan,
                                          ByVal firstStopWatch As Stopwatch,
                                          ByVal secondStopwatch As Stopwatch) As (FirstStartTime As DateTime, FirstEndTime As DateTime,
                                                                                          SecondStartTime As DateTime, SecondEndTime As DateTime)
            Dim unused As DateTime = DateTime.Now
            Dim unused1 As DateTime = DateTime.Now
            Dim unused2 As DateTime = DateTime.Now
            Dim unused3 As DateTime = DateTime.Now
            Dim firstStartTime As Date = DateTime.Now
            Dim secondStartTime As Date = DateTime.Now
            firstStopWatch.Restart()
            secondStopwatch.Restart()
            Threading.Thread.Sleep(sleepTime)
            secondStopwatch.Stop()
            firstStopWatch.Stop()
            Dim secondEndTime As Date = DateTime.Now
            Dim firstEndTime As Date = DateTime.Now
            Return (firstStartTime, firstEndTime, secondStartTime, secondEndTime)
        End Function

        ''' <summary> Measure do events time. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        ''' <param name="doEventsCount">   Number of do events. </param>
        ''' <param name="firstStopWatch">  The first stop watch. </param>
        ''' <param name="secondStopwatch"> The second stopwatch. </param>
        ''' <returns>
        ''' A (FirstStartTime As DateTime, FirstEndTime As DateTime, SecondStartTime As DateTime,
        ''' SecondEndTime As DateTime)
        ''' </returns>
        Private Shared Function MeasureDoEventsTime(ByVal doEventsCount As Integer,
                                          ByVal firstStopWatch As Stopwatch,
                                          ByVal secondStopwatch As Stopwatch) As (FirstStartTime As DateTime, FirstEndTime As DateTime,
                                                                                  SecondStartTime As DateTime, SecondEndTime As DateTime)
            Dim unused As DateTime = DateTime.Now
            Dim unused1 As DateTime = DateTime.Now
            Dim unused2 As DateTime = DateTime.Now
            Dim unused3 As DateTime = DateTime.Now
            Dim firstStartTime As Date = DateTime.Now
            Dim secondStartTime As Date = DateTime.Now
            firstStopWatch.Restart()
            secondStopwatch.Restart()
            If doEventsCount = 1 Then
                isr.Core.Services.My.MyLibrary.DoEvents()
            Else
                For i As Integer = 1 To doEventsCount
                    isr.Core.Services.My.MyLibrary.DoEvents()
                Next
            End If
            secondStopwatch.Stop()
            firstStopWatch.Stop()
            Dim secondEndTime As Date = DateTime.Now
            Dim firstEndTime As Date = DateTime.Now
            Return (firstStartTime, firstEndTime, secondStartTime, secondEndTime)
        End Function

        ''' <summary> Measure spin wait time. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        ''' <param name="spinWaitCount">   Number of spin waits. </param>
        ''' <param name="firstStopWatch">  The first stop watch. </param>
        ''' <param name="secondStopwatch"> The second stopwatch. </param>
        ''' <returns>
        ''' A (FirstStartTime As DateTime, FirstEndTime As DateTime, SecondStartTime As DateTime,
        ''' SecondEndTime As DateTime)
        ''' </returns>
        Private Shared Function MeasureSpinWaitTime(ByVal spinWaitCount As Integer,
                                          ByVal firstStopWatch As Stopwatch,
                                          ByVal secondStopwatch As Stopwatch) As (FirstStartTime As DateTime, FirstEndTime As DateTime,
                                                                                  SecondStartTime As DateTime, SecondEndTime As DateTime)
            Dim unused As DateTime = DateTime.Now
            Dim unused1 As DateTime = DateTime.Now
            Dim unused2 As DateTime = DateTime.Now
            Dim unused3 As DateTime = DateTime.Now
            Dim firstStartTime As Date = DateTime.Now
            Dim secondStartTime As Date = DateTime.Now
            firstStopWatch.Restart()
            secondStopwatch.Restart()
            Threading.Thread.SpinWait(spinWaitCount)
            secondStopwatch.Stop()
            firstStopWatch.Stop()
            Dim secondEndTime As Date = DateTime.Now
            Dim firstEndTime As Date = DateTime.Now
            Return (firstStartTime, firstEndTime, secondStartTime, secondEndTime)
        End Function

        ''' <summary> Measure thread yield wait time. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        ''' <param name="firstStopWatch">  The first stop watch. </param>
        ''' <param name="secondStopwatch"> The second stopwatch. </param>
        ''' <returns>
        ''' A (FirstStartTime As DateTime, FirstEndTime As DateTime, SecondStartTime As DateTime,
        ''' SecondEndTime As DateTime)
        ''' </returns>
        Private Shared Function MeasureThreadYieldWaitTime(ByVal firstStopWatch As Stopwatch,
                                                           ByVal secondStopwatch As Stopwatch) As (FirstStartTime As DateTime, FirstEndTime As DateTime,
                                                           SecondStartTime As DateTime, SecondEndTime As DateTime)
            Dim unused As DateTime = DateTime.Now
            Dim unused1 As DateTime = DateTime.Now
            Dim unused2 As DateTime = DateTime.Now
            Dim unused3 As DateTime = DateTime.Now
            Dim firstStartTime As Date = DateTime.Now
            Dim secondStartTime As Date = DateTime.Now
            firstStopWatch.Restart()
            secondStopwatch.Restart()
            Threading.Thread.Yield()
            secondStopwatch.Stop()
            firstStopWatch.Stop()
            Dim secondEndTime As Date = DateTime.Now
            Dim firstEndTime As Date = DateTime.Now
            Return (firstStartTime, firstEndTime, secondStartTime, secondEndTime)
        End Function

        ''' <summary> Assert stopwatch measurements. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        ''' <param name="name">            The name. </param>
        ''' <param name="firstStopwatch">  The first stopwatch. </param>
        ''' <param name="secondStopwatch"> The second stopwatch. </param>
        Private Shared Sub AssertStopwatchMeasurements(ByVal name As String, ByVal firstStopwatch As Stopwatch, ByVal secondStopwatch As Stopwatch)
            Assert.IsTrue(firstStopwatch.ElapsedTicks > 0, $"{name}: Ticks of second stopwatch {secondStopwatch.ElapsedTicks} should be greater than zero")
            Assert.IsTrue(secondStopwatch.ElapsedTicks > 0, $"{name}: Ticks of second stopwatch {secondStopwatch.ElapsedTicks} should be greater than zero")
            Assert.AreEqual(firstStopwatch.ElapsedTicks, TimeSpan.FromTicks(firstStopwatch.ElapsedTicks).Ticks,
                            $"{name}: First stopwatch time span from ticks to ticks should match")
            Assert.AreEqual(secondStopwatch.ElapsedTicks, TimeSpan.FromTicks(secondStopwatch.ElapsedTicks).Ticks,
                            $"{name}: stopwatch time span from ticks to ticks should match")
            Assert.IsTrue(secondStopwatch.ElapsedTicks <= firstStopwatch.ElapsedTicks,
                          $"{name}: Ticks of second stopwatch {secondStopwatch.ElapsedTicks} should be <= first stop watch {firstStopwatch.ElapsedTicks}")
            Assert.AreEqual(firstStopwatch.ElapsedTicks, firstStopwatch.Elapsed.Ticks,
                            $"{name}: First stopwatch elapsed ticks should match elapsed time")
            Assert.AreEqual(secondStopwatch.ElapsedTicks, secondStopwatch.Elapsed.Ticks,
                            $"{name}: Second stopwatch elapsed ticks should match elapsed time")
            Assert.AreEqual(firstStopwatch.ElapsedTicks - secondStopwatch.ElapsedTicks,
                            firstStopwatch.Elapsed.Subtract(secondStopwatch.Elapsed).Ticks,
                            $"{name}: Tick difference must match timespan difference in ticks")
        End Sub

        ''' <summary> Asserts date time measurements. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        ''' <param name="name">    The name. </param>
        ''' <param name="outcome"> The outcome. </param>
        Private Shared Sub AssertDatetimeMeasurements(ByVal name As String,
                                                      ByVal outcome As (FirstStartTime As DateTime, FirstEndTime As DateTime, SecondStartTime As DateTime, SecondEndTime As DateTime))
            If String.IsNullOrEmpty(name) Then
                outcome.FirstEndTime.AddTicks(0)
            End If
        End Sub

        ''' <summary> (Unit Test Method) tests date time resolution. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestMethod()>
        Public Sub DateTimeResolutionTest()
            Dim startTime As DateTime = DateTime.Now
            Dim endTime As DateTime = startTime.Add(TimeSpan.FromTicks(1))
            Assert.AreEqual(1L, endTime.Ticks - startTime.Ticks, $"Adding one tick timespan should add one tick to the date time")
            Assert.AreEqual(TimeSpan.FromTicks(1), (1.0 / Stopwatch.Frequency).FromSeconds,
                            $"Time span tick resolution should match the stopwatch frequency {(1.0 / Stopwatch.Frequency)}")
        End Sub

        ''' <summary> (Unit Test Method) tests measure elapsed time. </summary>
        ''' <remarks>
        ''' This measurement asserts only the stop watch measurements because of the following:<para>
        ''' https://stackoverflow.com/questions/307582/how-frequent-is-datetime-now-updated-or-is-there-a-more-precise-api-to-get-the
        ''' </para><para>
        ''' The problem with DateTime when dealing with milliseconds isn't due to the DateTime class at
        ''' all, but rather, has to do with CPU ticks and thread slices. Essentially, when an operation
        ''' is paused by the scheduler to allow other threads to execute, it must wait at a minimum of 1
        ''' time slice before resuming which is around 15ms on modern Windows OSes. Therefore, any
        ''' attempt to pause for less than this 15ms precision will lead to unexpected results.</para>
        ''' </remarks>
        <TestMethod()>
        Public Sub MeasureElapsedTimeTest()
            Dim firstStopwatch As New Stopwatch
            Dim secondStopwatch As New Stopwatch
            Dim outcome As (FirstStartTime As DateTime, FirstEndTime As DateTime, SecondStartTime As DateTime, SecondEndTime As DateTime)
            Dim sleepTimespan As TimeSpan = TimeSpan.FromMilliseconds(1)
            outcome = MeasureSleepTime(sleepTimespan, firstStopwatch, secondStopwatch)
            AssertStopwatchMeasurements($"Sleep {sleepTimespan.ToMilliseconds}", firstStopwatch, secondStopwatch)
            AssertDatetimeMeasurements($"Sleep {sleepTimespan.ToMilliseconds}", outcome)

            Dim doEventsCount As Integer = 1
            Dim unused As (FirstStartTime As Date, FirstEndTime As Date, SecondStartTime As Date, SecondEndTime As Date) = MeasureDoEventsTime(doEventsCount, firstStopwatch, secondStopwatch)
            ' Do Events 1 first stopwatch elapsed ticks 723877
            ' Do Events 1 second stopwatch elapsed ticks 723876
            TestInfo.VerboseMessage($"Do Events {doEventsCount} first stopwatch elapsed ticks {firstStopwatch.ElapsedTicks}")
            TestInfo.VerboseMessage($"Do Events {doEventsCount} second stopwatch elapsed ticks {secondStopwatch.ElapsedTicks}")
            outcome = MeasureDoEventsTime(doEventsCount, firstStopwatch, secondStopwatch)
            ' Do Events 1 first stopwatch elapsed ticks 2621
            ' Do Events 1 first stopwatch elapsed ticks 2621
            ' Do Events 1 second stopwatch elapsed ticks 2619
            TestInfo.VerboseMessage($"Do Events {doEventsCount} first stopwatch elapsed ticks {firstStopwatch.ElapsedTicks}")
            TestInfo.VerboseMessage($"Do Events {doEventsCount} second stopwatch elapsed ticks {secondStopwatch.ElapsedTicks}")
            AssertStopwatchMeasurements($"Do Events {doEventsCount}", firstStopwatch, secondStopwatch)
            AssertDatetimeMeasurements($"Do Events {doEventsCount}", outcome)

            outcome = MeasureThreadYieldWaitTime(firstStopwatch, secondStopwatch)
            AssertStopwatchMeasurements($"Thread Yield", firstStopwatch, secondStopwatch)
            ' Thread Yield first stopwatch elapsed ticks 57, 32
            ' Thread Yield second stopwatch elapsed ticks 55, 31
            TestInfo.VerboseMessage($"Thread Yield first stopwatch elapsed ticks {firstStopwatch.ElapsedTicks}")
            TestInfo.VerboseMessage($"Thread Yield second stopwatch elapsed ticks {secondStopwatch.ElapsedTicks}")
            AssertDatetimeMeasurements($"Thread Yield", outcome)

            Dim spinWaitCount As Integer = 10
            outcome = MeasureSpinWaitTime(spinWaitCount, firstStopwatch, secondStopwatch)
            AssertStopwatchMeasurements($"Spin wait {spinWaitCount}", firstStopwatch, secondStopwatch)
            ' Spin wait 10 first stopwatch elapsed ticks 7 = 0.7 micro seconds
            ' Spin wait 10 second stopwatch elapsed ticks 6
            TestInfo.VerboseMessage($"Spin wait {spinWaitCount} first stopwatch elapsed ticks {firstStopwatch.ElapsedTicks}")
            TestInfo.VerboseMessage($"Spin wait {spinWaitCount} second stopwatch elapsed ticks {secondStopwatch.ElapsedTicks}")
            AssertDatetimeMeasurements($"Spin wait {spinWaitCount}", outcome)
        End Sub

#End Region

    End Class
End Namespace
