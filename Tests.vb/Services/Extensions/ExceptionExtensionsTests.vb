Namespace ExtensionsTests

    ''' <summary> An exception extensions tests. </summary>
    ''' <remarks>
    ''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 1/26/2018 </para>
    ''' </remarks>
    <TestClass()>
    Public Class ExceptionExtensionsTests

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <remarks>
        ''' Use ClassInitialize to run code before running the first test in the class.
        ''' </remarks>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
        <ClassInitialize(), CLSCompliant(False)>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                _TestSite = New TestSite
                _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
                _TestSite.AddTraceMessagesQueue(isr.Core.Services.My.MyLibrary.UnpublishedTraceMessages)
                _TestSite.InitializeTraceListener()

            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
            _TestSite?.Dispose()
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestInitialize()> Public Sub MyTestInitialize()
            ' assert reading of test settings from the configuration file.
            Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
            Dim expectedUpperLimit As Double = 12
            Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
            TestInfo.ClearMessageQueue()
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestCleanup()> Public Sub MyTestCleanup()
            TestInfo.AssertMessageQueue()
        End Sub

        ''' <summary>
        ''' Gets the test context which provides information about and functionality for the current test
        ''' run.
        ''' </summary>
        ''' <value> The test context. </value>
        Public Property TestContext() As TestContext
        ''' <summary> The test site. </summary>
        Private Shared _TestSite As TestSite

        ''' <summary> Gets information describing the test. </summary>
        ''' <value> Information describing the test. </value>
        Private Shared ReadOnly Property TestInfo() As TestSite
            Get
                If _TestSite Is Nothing Then
                    _TestSite = New TestSite
                    _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
                    _TestSite.AddTraceMessagesQueue(isr.Core.Services.My.MyLibrary.UnpublishedTraceMessages)
                End If
                Return _TestSite
            End Get
        End Property

#End Region

#Region " TO FULL BLOWN STRING "

        ''' <summary> Throw argument exception. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        '''                                      illegal values. </exception>
        ''' <param name="version"> The version. </param>
        <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
        Private Shared Sub ThrowArgumentException(ByVal version As Version)
            Throw New ArgumentException("Forced argument exception", NameOf(version))
        End Sub

        ''' <summary> (Unit Test Method) tests full blown exception. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        <TestMethod()>
        Public Sub FullBlownExceptionTest()
            Dim version As Version = New Version(0, 0, 0, 0)

            Dim expectedKey As String = "0-Name"
            Dim expectedValue As String = NameOf(version)
            Try
                ExceptionExtensionsTests.ThrowArgumentException(version)
            Catch ex As ArgumentException
                Dim result As String = ExceptionExtensions.Methods.ToFullBlownString(ex)
                ' this checks the exception data.
                Assert.IsTrue(ex.Data.Contains(expectedKey), $"Data contains key {expectedKey}")
                Assert.AreEqual(expectedValue, CStr(ex.Data.Item(expectedKey)), $"Data value of {expectedKey}")
                ' this checks the stack trace
                Dim fullText As String = ExceptionExtensions.Methods.ToFullBlownString(ex)
                Dim searchFor As String = NameOf(ExceptionExtensionsTests.FullBlownExceptionTest)
                Assert.IsTrue(fullText.Contains(searchFor), $"{fullText} contains {searchFor}")
            Catch ex As Exception
                Assert.Fail("Expected exception not caught")
            End Try
        End Sub


#End Region

    End Class
End Namespace
