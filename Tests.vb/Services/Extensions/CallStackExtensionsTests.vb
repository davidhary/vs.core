Imports isr.Core.StackTraceExtensions
Namespace ExtensionsTests

    ''' <summary>
    ''' This is a test class for ExtensionsTest and is intended to contain all ExtensionsTest Unit
    ''' Tests.
    ''' </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestClass()>
    Public Class CallStackExtensionsTests

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <remarks>
        ''' Use ClassInitialize to run code before running the first test in the class.
        ''' </remarks>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
        <ClassInitialize(), CLSCompliant(False)>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                _TestSite = New TestSite
                _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
                _TestSite.AddTraceMessagesQueue(isr.Core.Services.My.MyLibrary.UnpublishedTraceMessages)
                _TestSite.InitializeTraceListener()

            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
            _TestSite?.Dispose()
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestInitialize()> Public Sub MyTestInitialize()
            ' assert reading of test settings from the configuration file.
            Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
            Dim expectedUpperLimit As Double = 12
            Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
            TestInfo.ClearMessageQueue()
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestCleanup()> Public Sub MyTestCleanup()
            TestInfo.AssertMessageQueue()
        End Sub

        ''' <summary>
        ''' Gets the test context which provides information about and functionality for the current test
        ''' run.
        ''' </summary>
        ''' <value> The test context. </value>
        Public Property TestContext() As TestContext
        ''' <summary> The test site. </summary>
        Private Shared _TestSite As TestSite

        ''' <summary> Gets information describing the test. </summary>
        ''' <value> Information describing the test. </value>
        Private Shared ReadOnly Property TestInfo() As TestSite
            Get
                If _TestSite Is Nothing Then
                    _TestSite = New TestSite
                    _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
                    _TestSite.AddTraceMessagesQueue(isr.Core.Services.My.MyLibrary.UnpublishedTraceMessages)
                End If
                Return _TestSite
            End Get
        End Property

#End Region

#Region " CALL STACK EXTENSION TESTS "

        ''' <summary> A test for ParseCallStack for user. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestMethod()>
        Public Sub ParseUserCallStackFrameTest()
            Dim trace As StackTrace = New StackTrace(True)
            Dim frame As StackFrame = New StackFrame(True)
            Dim callStackType As CallStackType = CallStackType.UserCallStack
            Dim expected As String = "Tests.ParseUserCallStackFrameTest() in "
            Dim actual As String = trace.ParseCallStack(frame, callStackType).Trim
            Assert.IsTrue(actual.Contains(expected), $"Actual trace 
{actual}
contains 
{expected}")
        End Sub

        ''' <summary> A test for ParseCallStack for user. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestMethod()>
        Public Sub ParseFullCallStackFrameTest()
            Dim trace As StackTrace = New StackTrace(True)
            Dim frame As StackFrame = New StackFrame(True)
            Dim callStackType As CallStackType = CallStackType.FullCallStack
            Dim expected As String = String.Empty
            Dim actual As String
            actual = trace.ParseCallStack(frame, callStackType)
            Assert.AreNotEqual(expected, actual)
        End Sub

        ''' <summary> A test for ParseCallStack. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestMethod()>
        Public Sub ParseUserCallStackTest()
            Dim trace As StackTrace = New StackTrace(True)
            Dim skipLines As Integer = 0
            Dim totalLines As Integer = 0
            Dim callStackType As CallStackType = CallStackType.UserCallStack
            Dim expected As String = "s->   at isr."
            Dim actual As String = trace.ParseCallStack(skipLines, totalLines, callStackType).Trim
            Assert.IsTrue(actual.StartsWith(expected, StringComparison.OrdinalIgnoreCase), $"Trace results Actual: { actual.Substring(0, expected.Length)} Starts with {expected}")
        End Sub

        ''' <summary> A test for ParseCallStack. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestMethod()>
        Public Sub ParseFullCallStackTest()
            Dim trace As StackTrace = New StackTrace(True)
            Dim skipLines As Integer = 0
            Dim totalLines As Integer = 0
            Dim callStackType As CallStackType = CallStackType.FullCallStack
            Dim expected As String = String.Empty
            Dim actual As String
            actual = trace.ParseCallStack(skipLines, totalLines, callStackType)
            Assert.AreNotEqual(expected, actual)
        End Sub
#End Region

    End Class
End Namespace
