Namespace ExtensionsTests

    ''' <summary> Combinations tests. </summary>
    ''' <remarks>
    ''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 2/18/2019. </para>
    ''' </remarks>
    <TestClass()>
    Public Class CombinationsTests

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <remarks>
        ''' Use ClassInitialize to run code before running the first test in the class.
        ''' </remarks>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
        <ClassInitialize(), CLSCompliant(False)>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                _TestSite = New TestSite
                _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
                _TestSite.AddTraceMessagesQueue(isr.Core.Services.My.MyLibrary.UnpublishedTraceMessages)
                _TestSite.InitializeTraceListener()

            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
            _TestSite?.Dispose()
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestInitialize()> Public Sub MyTestInitialize()
            ' assert reading of test settings from the configuration file.
            Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
            Dim expectedUpperLimit As Double = 12
            Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
            TestInfo.ClearMessageQueue()
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestCleanup()> Public Sub MyTestCleanup()
            TestInfo.AssertMessageQueue()
        End Sub

        ''' <summary>
        ''' Gets the test context which provides information about and functionality for the current test
        ''' run.
        ''' </summary>
        ''' <value> The test context. </value>
        Public Property TestContext() As TestContext
        ''' <summary> The test site. </summary>
        Private Shared _TestSite As TestSite

        ''' <summary> Gets information describing the test. </summary>
        ''' <value> Information describing the test. </value>
        Private Shared ReadOnly Property TestInfo() As TestSite
            Get
                If _TestSite Is Nothing Then
                    _TestSite = New TestSite
                    _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
                    _TestSite.AddTraceMessagesQueue(isr.Core.Services.My.MyLibrary.UnpublishedTraceMessages)
                End If
                Return _TestSite
            End Get
        End Property

#End Region

#Region " COMBINATION TEST "

        ''' <summary> Builds the given values. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        ''' <param name="values"> The values. </param>
        ''' <returns> A String. </returns>
        Private Shared Function Build(Of T)(ByVal values As List(Of T)) As String
            Dim builder As New System.Text.StringBuilder()
            For Each i As T In values
                builder.AppendFormat("{0,3}", i)
            Next
            Return builder.ToString
        End Function

        ''' <summary> (Unit Test Method) tests combination. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestMethod()>
        Public Sub CombinationTest()
            Dim collectionOfSeries As New List(Of List(Of Integer)) From {New List(Of Integer)({1, 2, 3, 4, 5}),
                                                                      New List(Of Integer)({0, 1}),
                                                                      New List(Of Integer)({6, 3}),
                                                                      New List(Of Integer)({1, 3, 5})}
            Dim combinations As List(Of List(Of Integer)) = CombinationsExtensions.Methods.GenerateCombinations(Of Integer)(collectionOfSeries)
            Dim expectedList As New List(Of Integer)({1, 0, 6, 1})
            Dim actualList As List(Of Integer) = combinations.First
            Assert.IsTrue(expectedList.SequenceEqual(actualList))
            expectedList = New List(Of Integer)({1, 0, 6, 3})
            actualList = combinations(1)
            Assert.IsTrue(expectedList.SequenceEqual(actualList), $"actual: [{Build(actualList)}] <> expected: [{CombinationsTests.Build(actualList)}]")
            expectedList = New List(Of Integer)({5, 1, 3, 5})
            actualList = combinations.Last
            Assert.IsTrue(expectedList.SequenceEqual(actualList), $"actual: [{Build(actualList)}] <> expected: [{CombinationsTests.Build(actualList)}]")
        End Sub

#End Region

    End Class
End Namespace
