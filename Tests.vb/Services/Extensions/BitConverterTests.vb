Imports isr.Core.BitConverterExtensions
Namespace ExtensionsTests

    ''' <summary> A bit converter tests. </summary>
    ''' <remarks>
    ''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 3/8/2018 </para>
    ''' </remarks>
    <TestClass()>
    Public Class BitConverterTests

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <remarks>
        ''' Use ClassInitialize to run code before running the first test in the class.
        ''' </remarks>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
        <ClassInitialize(), CLSCompliant(False)>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                _TestSite = New TestSite
                _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
                _TestSite.AddTraceMessagesQueue(isr.Core.Services.My.MyLibrary.UnpublishedTraceMessages)
                _TestSite.InitializeTraceListener()

            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
            _TestSite?.Dispose()
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestInitialize()> Public Sub MyTestInitialize()
            ' assert reading of test settings from the configuration file.
            Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
            Dim expectedUpperLimit As Double = 12
            Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
            TestInfo.ClearMessageQueue()
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestCleanup()> Public Sub MyTestCleanup()
            TestInfo.AssertMessageQueue()
        End Sub

        ''' <summary>
        ''' Gets the test context which provides information about and functionality for the current test
        ''' run.
        ''' </summary>
        ''' <value> The test context. </value>
        Public Property TestContext() As TestContext
        ''' <summary> The test site. </summary>
        Private Shared _TestSite As TestSite

        ''' <summary> Gets information describing the test. </summary>
        ''' <value> Information describing the test. </value>
        Private Shared ReadOnly Property TestInfo() As TestSite
            Get
                If _TestSite Is Nothing Then
                    _TestSite = New TestSite
                    _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
                    _TestSite.AddTraceMessagesQueue(isr.Core.Services.My.MyLibrary.UnpublishedTraceMessages)
                End If
                Return _TestSite
            End Get
        End Property

#End Region

#Region " BIT CONVERTER TESTS "

        ''' <summary> (Unit Test Method) tests true if little endian. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestMethod()>
        Public Sub TrueIfLittleEndianTest()
            Dim isLittleEndian As Boolean = True
            Dim expected As Boolean = isLittleEndian
            Dim actual As Boolean = BitConverter.IsLittleEndian
            Assert.AreEqual(expected, actual)
        End Sub

        ''' <summary> (Unit Test Method) tests byte converter short. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestMethod()>
        Public Sub ByteConverterShortTest()
            Dim value As UShort = 1234
            Dim values As Byte() = value.BigEndInt8
            Dim expected As UShort = values.BigEndUnsignedShort(0)
            Dim actual As UShort = values.BigEndUnsignedInt16(0)
            Assert.AreEqual(value, expected)
            Assert.AreEqual(expected, actual)
        End Sub

        ''' <summary> (Unit Test Method) tests bit converter short. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestMethod()>
        Public Sub BitConverterShortTest()
            Dim value As UShort = 1234
            Dim values As Byte() = value.BigEndBytes
            Dim actual As UShort = values.BigEndUnsignedInt16(0)
            Dim expected As UShort = values.BigEndUnsignedShort(0)
            Assert.AreEqual(value, expected)
            Assert.AreEqual(expected, actual)
        End Sub

        ''' <summary> (Unit Test Method) tests bit converter single. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestMethod()>
        Public Sub BitConverterSingleTest()
            Dim expected As Single = 1234.567
            Dim values As Byte() = expected.BigEndBytes
            Dim actual As Single = values.BigEndSingle(0)
            Assert.AreEqual(expected, actual)
        End Sub

        ''' <summary> (Unit Test Method) tests bit converter endianess. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestMethod()>
        Public Sub BitConverterEndianessTest()
            Dim value As UShort = 1234
            Dim bigEndBytes As Byte() = value.BigEndBytes
            Dim bigEndInt8 As Byte() = value.BigEndInt8
            Dim actual As Byte = bigEndBytes(0)
            Dim expected As Byte = bigEndInt8(0)
            Assert.AreEqual(expected, actual)
            actual = bigEndBytes(1)
            expected = bigEndInt8(1)
            Assert.AreEqual(expected, actual)
            Dim littleBytes As Byte() = BitConverter.GetBytes(value)
            actual = littleBytes(0)
            expected = bigEndBytes(1)
            Assert.AreEqual(expected, actual)
            actual = littleBytes(1)
            expected = bigEndBytes(0)
            Assert.AreEqual(expected, actual)
        End Sub
#End Region


    End Class
End Namespace
