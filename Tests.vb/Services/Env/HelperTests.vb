
''' <summary> An environment helper tests. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 10/16/2019 </para>
''' </remarks>
<TestClass()>
Public Class EnvHelperTests

    ''' <summary> The variable not present key. </summary>
    Private Const _VariableNotPresentKey As String = NameOf(_VariableNotPresentKey)

    ''' <summary> (Unit Test Method) tests retrieve string value. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod>
    Public Sub RetrieveStringValueTest()
        Dim key As String = "A_STRING"
        Dim value As String = "This is a string"

        Environment.SetEnvironmentVariable(key, value)

        Assert.AreEqual(value, Env.RetrieveValue(key))
        Assert.AreEqual(Nothing, Env.RetrieveValue(_VariableNotPresentKey))
        Assert.AreEqual("none", Env.RetrieveValue(_VariableNotPresentKey, "none"))
    End Sub

    ''' <summary> (Unit Test Method) tests retrieve boolean value. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod>
    Public Sub RetrieveBooleanValueTest()
        Dim key1 As String = "TRUE_VALUE"
        Dim value1 As String = "true"
        Dim key2 As String = "FALSE_VALUE"
        Dim value2 As String = "false"

        Environment.SetEnvironmentVariable(key1, value1)
        Environment.SetEnvironmentVariable(key2, value2)

        Assert.AreEqual(True, Env.RetrieveBoolean(key1).GetValueOrDefault(False), $"{key1} should be {value1}")
        Assert.AreEqual(False, Env.RetrieveBoolean(key2), $"{key2} should be {value2}")
        Assert.AreEqual(Nothing, Env.RetrieveBoolean(_VariableNotPresentKey), $"{_VariableNotPresentKey} should be missing and default to {Nothing}")
        Assert.AreEqual(True, Env.RetrieveBoolean(_VariableNotPresentKey, True), $"{_VariableNotPresentKey} should be missing and was set to default {True}")
    End Sub

    ''' <summary> (Unit Test Method) tests retrieve integer value. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod>
    Public Sub RetrieveIntegerValueTest()
        Dim key1 As String = "ONE_STRING"
        Dim value1 As String = "1"

        Environment.SetEnvironmentVariable(key1, value1)

        Assert.AreEqual(1, Env.RetrieveInteger(key1))
        Assert.AreEqual(Nothing, Env.RetrieveInteger(_VariableNotPresentKey))
        Assert.AreEqual(-1, Env.RetrieveInteger(_VariableNotPresentKey, -1))
    End Sub

    ''' <summary> (Unit Test Method) tests retrieve double value. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod>
    Public Sub RetrieveDoubleValueTest()
        Dim key1 As String = "ONE_POINT_TWO_STRING"
        Dim value1 As String = "1.2"
        Dim key2 As String = "ONE_POINT_TWO_STRING_WITH_COMMA"
        Dim value2 As String = "1,2"

        Environment.SetEnvironmentVariable(key1, value1)
        Environment.SetEnvironmentVariable(key2, value2)

        Assert.AreEqual(1.2, Env.RetrieveDouble(key1))
        Assert.AreEqual(12.0R, Env.RetrieveDouble(key2))
        Assert.AreEqual(Nothing, Env.RetrieveDouble(_VariableNotPresentKey))
        Assert.AreEqual(-1.2, Env.RetrieveDouble(_VariableNotPresentKey, -1.2))
    End Sub
End Class
