Imports System.IO
Imports System.Runtime.InteropServices

''' <summary> An environment tests. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 10/16/2019 </para>
''' </remarks>
<TestClass()>
Public Class EnvTests

    ''' <summary> Query if this object is windows. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <returns> <c>true</c> if windows; otherwise <c>false</c> </returns>
    Private Shared Function IsWindows() As Boolean
        Return RuntimeInformation.IsOSPlatform(OSPlatform.Windows)
    End Function

    ''' <summary> (Unit Test Method) tests is windows. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod>
    Public Sub IsWindowsTest()
        Assert.AreEqual(String.Equals(My.Computer.Info.OSPlatform, "Win32NT", StringComparison.OrdinalIgnoreCase),
                            IsWindows(), $"expected {My.Computer.Info.OSPlatform}")
    End Sub

    ''' <summary> (Unit Test Method) tests load. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod>
    Public Sub LoadTest()
        Env.Load("./env/.env1", New EnvLoadOptions)
        Assert.AreEqual("Toni", Environment.GetEnvironmentVariable("NAME"))
        ' unfortunately .NET removes empty env vars -- there can NEVER be an empty string env var value
        '  https://msdn.microsoft.com/en-us/library/z46c489x(v=vs.110).aspx#Remarks
        Assert.AreEqual(Nothing, Environment.GetEnvironmentVariable("EMPTY"))
        Assert.AreEqual("'", Environment.GetEnvironmentVariable("QUOTE"))
        Assert.AreEqual("https://github.com/tonerdo", Environment.GetEnvironmentVariable("URL"))
        Assert.AreEqual("user=test;password=secret", Environment.GetEnvironmentVariable("CONNECTION"))
        Assert.AreEqual("leading and trailing white space", Environment.GetEnvironmentVariable("WHITEBOTH"))
        Assert.AreEqual("""SPECIAL STUFF---\nLONG-BASE64\ignore""slash""", Environment.GetEnvironmentVariable("SSL_CERT"))
    End Sub

    ''' <summary> (Unit Test Method) tests load path. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod>
    Public Sub LoadPathTest()
        Env.Load("./env/.env2", New EnvLoadOptions)
        Assert.AreEqual("127.0.0.1", Environment.GetEnvironmentVariable("IP"))
        Assert.AreEqual("8080", Environment.GetEnvironmentVariable("PORT"))
        Assert.AreEqual("example.com", Environment.GetEnvironmentVariable("DOMAIN"))
        Assert.AreEqual("some text export other text", Environment.GetEnvironmentVariable("EMBEDEXPORT"))
        Assert.AreEqual(Nothing, Environment.GetEnvironmentVariable("COMMENTLEAD"))
        Assert.AreEqual("leading white space followed by comment", Environment.GetEnvironmentVariable("WHITELEAD"))
        Assert.AreEqual("® 🚀 日本", Environment.GetEnvironmentVariable("UNICODE"))
    End Sub

    ''' <summary> (Unit Test Method) tests load stream. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod>
    Public Sub LoadStreamTest()
        Using stream As FileStream = File.OpenRead("./env/.env1")
            Env.Load(stream, New EnvLoadOptions)
        End Using
        Assert.AreEqual("Toni", Environment.GetEnvironmentVariable("NAME"))
        ' unfortunately .NET removes empty env vars -- there can NEVER be an empty string env var value
        '  https://msdn.microsoft.com/en-us/library/z46c489x(v=vs.110).aspx#Remarks
        Assert.AreEqual(Nothing, Environment.GetEnvironmentVariable("EMPTY"))
        Assert.AreEqual("'", Environment.GetEnvironmentVariable("QUOTE"))
        Assert.AreEqual("https://github.com/tonerdo", Environment.GetEnvironmentVariable("URL"))
        Assert.AreEqual("user=test;password=secret", Environment.GetEnvironmentVariable("CONNECTION"))
        Assert.AreEqual("leading and trailing white space", Environment.GetEnvironmentVariable("WHITEBOTH"))
        Assert.AreEqual("""SPECIAL STUFF---\nLONG-BASE64\ignore""slash""", Environment.GetEnvironmentVariable("SSL_CERT"))
    End Sub

    ''' <summary> (Unit Test Method) tests load lines. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod>
    Public Sub LoadLinesTest()
        Env.Load(File.ReadAllLines("./env/.env1"), New EnvLoadOptions)
        Assert.AreEqual("Toni", Environment.GetEnvironmentVariable("NAME"))
        ' unfortunately .NET removes empty env vars -- there can NEVER be an empty string env var value
        '  https://msdn.microsoft.com/en-us/library/z46c489x(v=vs.110).aspx#Remarks
        Assert.AreEqual(Nothing, Environment.GetEnvironmentVariable("EMPTY"))
        Assert.AreEqual("'", Environment.GetEnvironmentVariable("QUOTE"))
        Assert.AreEqual("https://github.com/tonerdo", Environment.GetEnvironmentVariable("URL"))
        Assert.AreEqual("user=test;password=secret", Environment.GetEnvironmentVariable("CONNECTION"))
        Assert.AreEqual("leading and trailing white space", Environment.GetEnvironmentVariable("WHITEBOTH"))
        Assert.AreEqual("""SPECIAL STUFF---\nLONG-BASE64\ignore""slash""", Environment.GetEnvironmentVariable("SSL_CERT"))
    End Sub

    ''' <summary> (Unit Test Method) tests load arguments. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod>
    Public Sub LoadArgsTest()
        Env.Load("./env/.env1", New EnvLoadOptions(trimWhiteSpace:=True))
        Assert.AreEqual("leading and trailing white space", Environment.GetEnvironmentVariable("WHITEBOTH"))
        Env.Load("./env/.env1", New EnvLoadOptions(False))
        Assert.AreEqual("  leading and trailing white space   ", Environment.GetEnvironmentVariable("  WHITEBOTH  "))
        Env.Load("./env/.env1", New EnvLoadOptions(True, True))
        Assert.AreEqual("Google", Environment.GetEnvironmentVariable("PASSWORD"))
        Env.Load("./env/.env1", New EnvLoadOptions(True, False))
        Assert.AreEqual("Google#Facebook", Environment.GetEnvironmentVariable("PASSWORD"))
        Env.Load("./env/.env1", New EnvLoadOptions(True, True, True))
        ' Assert.AreEqual("SPECIAL STUFF---" & vbLf & "LONG-BASE64\ignore""slash", Environment.GetEnvironmentVariable("SSL_CERT"))
        Assert.AreEqual("""SPECIAL STUFF---\nLONG-BASE64\ignore""slash""", Environment.GetEnvironmentVariable("SSL_CERT"))
        Env.Load("./env/.env1", New EnvLoadOptions(True, True, False))
        Assert.AreEqual("""SPECIAL STUFF---\nLONG-BASE64\ignore""slash""", Environment.GetEnvironmentVariable("SSL_CERT"))
    End Sub

    ''' <summary> (Unit Test Method) tests load path arguments. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod>
    Public Sub LoadPathArgsTest()
        Env.Load("./env/.env2", New EnvLoadOptions(True, True))
        Assert.AreEqual("leading white space followed by comment", Environment.GetEnvironmentVariable("WHITELEAD"))
        Env.Load("./env/.env2", New EnvLoadOptions(False, True))
        Assert.AreEqual("  leading white space followed by comment  ", Environment.GetEnvironmentVariable("WHITELEAD"))
        Env.Load("./env/.env2", New EnvLoadOptions(True, False))
        Assert.AreEqual("leading white space followed by comment  # comment", Environment.GetEnvironmentVariable("WHITELEAD"))
        Env.Load("./env/.env2", New EnvLoadOptions(False, False))
        Assert.AreEqual("  leading white space followed by comment  # comment", Environment.GetEnvironmentVariable("WHITELEAD"))
        Env.Load("./env/.env2", New EnvLoadOptions(False, False, True))
        Assert.AreEqual("® 🚀 日本", Environment.GetEnvironmentVariable("UNICODE"))
        Env.Load("./env/.env2", New EnvLoadOptions(False, False, False))
        Assert.AreEqual("'\u00ae \U0001F680 日本'", Environment.GetEnvironmentVariable("UNICODE"))
    End Sub

    ''' <summary> (Unit Test Method) tests load no clobber. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod>
    Public Sub LoadNoClobberTest()
        Dim expected As String = "totally the original value"
        Environment.SetEnvironmentVariable("URL", expected)
        Env.Load("./env/.env1", New EnvLoadOptions(False, False, False, False))
        Assert.AreEqual(expected, Environment.GetEnvironmentVariable("URL"))

        Environment.SetEnvironmentVariable("URL", "i'm going to be overwritten")
        Env.Load("./env/.env1", New EnvLoadOptions(False, False, False, True))
        Assert.AreEqual("https://github.com/tonerdo", Environment.GetEnvironmentVariable("URL"))
    End Sub

    ''' <summary> (Unit Test Method) tests load no require environment. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod>
    Public Sub LoadNoRequireEnvTest()
        Dim expected As String = "totally the original value"
        Environment.SetEnvironmentVariable("URL", expected)
        ' this env file Does Not Exist
        Env.Load("./env/.envDNE", New EnvLoadOptions(requireEnvFile:=False))
        Assert.AreEqual(expected, Environment.GetEnvironmentVariable("URL"))
        ' it didn't throw an exception and crash for a missing file
        MyAssert.Throws(Of System.IO.FileNotFoundException)(Sub() Env.Load("./.envDNE", New EnvLoadOptions(requireEnvFile:=True)))
    End Sub

    ''' <summary> (Unit Test Method) tests load operating system casing. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod>
    Public Sub LoadOsCasingTest()
        Environment.SetEnvironmentVariable("CASING", "neither")
        Env.Load("./env/.env3", New EnvLoadOptions(clobberExistingValues:=False))
        Assert.AreEqual(If(IsWindows(), "neither", "lower"), Environment.GetEnvironmentVariable("casing"))
        Assert.AreEqual("neither", Environment.GetEnvironmentVariable("CASING"))

        Env.Load("./env/.env3", New EnvLoadOptions(clobberExistingValues:=True))
        Assert.AreEqual("lower", Environment.GetEnvironmentVariable("casing"))
        Assert.AreEqual(If(IsWindows(), "lower", "neither"), Environment.GetEnvironmentVariable("CASING"))

        Environment.SetEnvironmentVariable("CASING", Nothing)
        Environment.SetEnvironmentVariable("casing", "neither")
        Env.Load("./env/.env3", New EnvLoadOptions(clobberExistingValues:=False))
        Assert.AreEqual("neither", Environment.GetEnvironmentVariable("casing"))
        Assert.AreEqual(If(IsWindows(), "neither", Nothing), Environment.GetEnvironmentVariable("CASING"))

        Env.Load("./env/.env3", New EnvLoadOptions(clobberExistingValues:=True))
        Assert.AreEqual("lower", Environment.GetEnvironmentVariable("casing"))
        Assert.AreEqual(If(IsWindows(), "lower", Nothing), Environment.GetEnvironmentVariable("CASING"))
    End Sub

End Class
