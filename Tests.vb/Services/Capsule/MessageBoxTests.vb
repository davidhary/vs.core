Imports isr.Core.Services

''' <summary> SQL Message Box tests. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 4/13/2018 </para>
''' </remarks>
<TestClass()>
Public Class MessageBoxTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.Services.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext
    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

    ''' <summary> Shows the dialog abort ignore. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <exception cref="DivideByZeroException"> Thrown when an attempt is made to divide a number by
    '''                                          zero. </exception>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Sub ShowDialogException()
        Try
            Throw New DivideByZeroException()
        Catch ex As Exception
            Dim expected As MyDialogResult = MyDialogResult.Ok
            Dim actual As MyDialogResult
            ex.Data.Add("@isr", "Testing exception message")
            actual = MyMessageBox.ShowDialog(ex)
            Assert.AreEqual(expected, actual)
        End Try
    End Sub

    ''' <summary> (Unit Test Method) tests show exception dialog. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod(), TestCategory("UI")>
    Public Sub ShowExceptionDialogTest()
        Dim oThread As New Threading.Thread(New Threading.ThreadStart(AddressOf ShowDialogException))
        oThread.Start()
        ' a long delay was required...
        isr.Core.Services.My.MyLibrary.Delay(2000)
        isr.Core.Services.WindowsForms.SendWait("{ENTER}")
        ' This tabbed into another application.  isr.Core.Services.WindowsForms.SendWait("%{TAB}{ENTER}")
        oThread.Join()
    End Sub

    ''' <summary> Shows the dialog abort ignore. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <exception cref="DivideByZeroException"> Thrown when an attempt is made to divide a number by
    '''                                          zero. </exception>
    Public Shared Sub ShowDialogAbortIgnore()
        Try
            Throw New DivideByZeroException()
        Catch ex As DivideByZeroException
            ex.Data.Add("@isr", "Testing exception message")
            Dim result As MyDialogResult = isr.Core.Services.MyMessageBox.ShowDialogAbortIgnore(ex, MyMessageBoxIcon.Error)
            Assert.IsTrue(result = MyDialogResult.Abort OrElse result = MyDialogResult.Ignore,
                              $"{result} expected {MyDialogResult.Abort} or {MyDialogResult.Ignore}")
        End Try
    End Sub

    ''' <summary> Tests showing dialog with abort and ignore buttons. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod(), TestCategory("UI")>
    Public Sub ShowDialogAbortIgnoreTest()
        Dim oThread As New Threading.Thread(New Threading.ThreadStart(AddressOf ShowDialogAbortIgnore))
        oThread.Start()
        ' a long delay was required...
        isr.Core.Services.My.MyLibrary.Delay(2000)
        isr.Core.Services.WindowsForms.SendWait("{ENTER}")
        ' This tabbed into another application.  isr.Core.Services.WindowsForms.SendWait("%{TAB}{ENTER}")
        oThread.Join()
    End Sub

End Class
