Imports isr.Core.SplitExtensions
Imports isr.Core.EscapeSequencesExtensions
Namespace ExtensionsTests

    ''' <summary> String extensions tests. </summary>
    ''' <remarks>
    ''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 3/14/2018 </para>
    ''' </remarks>
    <TestClass()>
    Public Class StringExtensionsTests

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <remarks>
        ''' Use ClassInitialize to run code before running the first test in the class.
        ''' </remarks>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
        <ClassInitialize(), CLSCompliant(False)>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                _TestSite = New TestSite
                _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
                _TestSite.AddTraceMessagesQueue(isr.Core.Services.My.MyLibrary.UnpublishedTraceMessages)
                _TestSite.InitializeTraceListener()

            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
            _TestSite?.Dispose()
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestInitialize()> Public Sub MyTestInitialize()
            ' assert reading of test settings from the configuration file.
            Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
            Dim expectedUpperLimit As Double = 12
            Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
            TestInfo.ClearMessageQueue()
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestCleanup()> Public Sub MyTestCleanup()
            TestInfo.AssertMessageQueue()
        End Sub

        ''' <summary>
        ''' Gets the test context which provides information about and functionality for the current test
        ''' run.
        ''' </summary>
        ''' <value> The test context. </value>
        Public Property TestContext() As TestContext
        ''' <summary> The test site. </summary>
        Private Shared _TestSite As TestSite

        ''' <summary> Gets information describing the test. </summary>
        ''' <value> Information describing the test. </value>
        Private Shared ReadOnly Property TestInfo() As TestSite
            Get
                If _TestSite Is Nothing Then
                    _TestSite = New TestSite
                    _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
                    _TestSite.AddTraceMessagesQueue(isr.Core.Services.My.MyLibrary.UnpublishedTraceMessages)
                End If
                Return _TestSite
            End Get
        End Property

#End Region

#Region " SPLIT EXTENSION TESTS "

        ''' <summary> (Unit Test Method) tests base 64 unique identifier. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestMethod()>
        Public Sub SplitWordsTest()
            Dim input As String = "CamelCase"
            Dim expected As String = "Camel Case"
            Dim actual As String = input.SplitWords
            Assert.AreEqual(expected, actual, $"Split {input}")
            input = "CamelCASE"
            expected = "Camel CASE"
            actual = input.SplitWords
            Assert.AreEqual(expected, actual, $"Split {input}")
            input = "CamelCase123"
            expected = "Camel Case123"
            actual = input.SplitWords
            Assert.AreEqual(expected, actual, $"Split {input}")
            input = "CamelIPv4"
            expected = "Camel IPv4"
            actual = input.SplitWords
            Assert.AreEqual(expected, actual, $"Split {input}")
        End Sub

        ''' <summary> (Unit Test Method) tests base 64 unique identifier. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestMethod()>
        Public Sub SplitTitleCaseTest()
            Dim input As String = "CamelCase"
            Dim expected As String = "Camel Case"
            Dim actual As String = input.SplitCase

            Assert.AreEqual(expected, actual, $"Split {input}")
            input = "CamelCase123"
            expected = "Camel Case 123"
            actual = input.SplitCase
            Assert.AreEqual(expected, actual, $"Split {input}")

            input = "LocalIPV4"
            expected = "Local IPV 4"
            actual = input.SplitCase
            Assert.AreEqual(expected, actual, $"Split {input}")

            input = "LocalREMOTE"
            expected = "Local REMOTE"
            actual = input.SplitCase
            Assert.AreEqual(expected, actual, $"Split {input}")

            input = "LocalIPv4"
            expected = "Local I Pv 4"
            actual = input.SplitCase
            Assert.AreEqual(expected, actual, $"Split {input}")

            input = "ExpandCamelCaseAPIDescriptorPHP3_5_4Version3_21Beta"
            expected = "Expand Camel Case API Descriptor PHP 3_5_4 Version 3_21 Beta"
            actual = input.SplitCase
            Assert.AreEqual(expected, actual, $"Split {input}")

        End Sub

        ''' <summary> (Unit Test Method) tests split case. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestMethod()>
        Public Sub SplitCaseTest()

            Dim input As String = "CamelCase"
            Dim expected As String = "Camel Case"
            Dim actual As String = input.SplitCaseSlower
            Assert.AreEqual(expected, actual, $"Split {input}")

            input = "CamelCase123"
            expected = "Camel Case 123"
            actual = input.SplitCaseSlower
            Assert.AreEqual(expected, actual, $"Split {input}")

            input = "LocalIPV4"
            expected = "Local IPV 4"
            actual = input.SplitCaseSlower
            Assert.AreEqual(expected, actual, $"Split {input}")

            input = "LocalREMOTE"
            expected = "Local REMOTE"
            actual = input.SplitCaseSlower
            Assert.AreEqual(expected, actual, $"Split {input}")

            input = "LocalIPv4"
            expected = "Local I Pv 4"
            actual = input.SplitCaseSlower
            Assert.AreEqual(expected, actual, $"Split {input}")

            input = "ExpandCamelCaseAPIDescriptorPHP3_5_4Version3_21Beta"
            expected = "Expand Camel Case API Descriptor PHP 3_5_4 Version 3_21 Beta"
            actual = input.SplitCaseSlower
            Assert.AreEqual(expected, actual, $"Split {input}")
        End Sub

        ''' <summary> (Unit Test Method) tests split benchmark. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestMethod()>
        Public Sub SplitBenchmarkTest()
            Dim sw As Stopwatch
            Dim toleranceFactor As Integer = 2
            Dim count As Integer = 1000
            Select Case count
                Case Is > 100000
                    toleranceFactor = 2
                Case Is > 10000
                    toleranceFactor = 3
                Case Is > 1000
                    toleranceFactor = 5
                Case Else
                    toleranceFactor = 40
            End Select

            Dim input As String = "CamelCase123"
            Dim expectedMillisecondDuration As Double = 0.00034
            Dim expectedDuration As TimeSpan = TimeSpan.FromTicks(CLng(toleranceFactor * count * TimeSpan.TicksPerMillisecond * expectedMillisecondDuration))
            sw = Stopwatch.StartNew
            Dim output As String
            For i As Integer = 1 To count
                output = input.SplitWords
            Next
            Dim actualDuration As TimeSpan = sw.Elapsed
            Assert.IsTrue(actualDuration < expectedDuration,
                          $"{NameOf(SplitExtensions.Methods.SplitWords)} duration {actualDuration.TotalMilliseconds}ms should be lower than {expectedDuration.TotalMilliseconds}ms for {count} iterations")

            expectedMillisecondDuration = 0.0028
            expectedDuration = TimeSpan.FromTicks(CLng(toleranceFactor * count * TimeSpan.TicksPerMillisecond * expectedMillisecondDuration))
            sw = Stopwatch.StartNew
            For i As Integer = 1 To count
                output = input.SplitCase
            Next
            actualDuration = sw.Elapsed
            Assert.IsTrue(actualDuration < expectedDuration,
                          $"{NameOf(SplitExtensions.Methods.SplitCase)} duration {actualDuration.TotalMilliseconds}ms should be lower than {expectedDuration.TotalMilliseconds}ms for {count} iterations")

            expectedMillisecondDuration = 0.0048
            expectedDuration = TimeSpan.FromTicks(CLng(toleranceFactor * count * TimeSpan.TicksPerMillisecond * expectedMillisecondDuration))
            sw = Stopwatch.StartNew
            For i As Integer = 1 To count
                output = input.SplitCaseSlower
            Next
            actualDuration = sw.Elapsed
            Assert.IsTrue(actualDuration < expectedDuration,
                          $"{NameOf(SplitExtensions.Methods.SplitCaseSlower)} duration {actualDuration.TotalMilliseconds}ms should be lower than {expectedDuration.TotalMilliseconds}ms for {count} iterations")

        End Sub


#End Region

#Region " ESCAPE SEQUENCE TESTS "

        ''' <summary> (Unit Test Method) tests escape sequence. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestMethod>
        Public Sub EscapeSequenceTest()
            Dim IdentityQueryCommand As String = "*IDN?"
            Dim escapedValue As String = $"{IdentityQueryCommand}{EscapeSequencesExtensions.Methods.NewLineEscape}"
            Dim expcetedValue As String = $"{IdentityQueryCommand}{EscapeSequencesExtensions.Methods.NewLineChar}"
            Dim actualValue As String = escapedValue.Replace(EscapeSequencesExtensions.Methods.NewLineEscape,
                                                             ChrW(EscapeSequencesExtensions.Methods.NewLineValue))
            Assert.AreEqual(expcetedValue, actualValue, $"Failed replacing escape value in {escapedValue}")
            actualValue = escapedValue.ReplaceCommonEscapeSequences()
            Assert.AreEqual(expcetedValue, actualValue, $"Extension failed replacing escape value in {escapedValue}")
            actualValue = actualValue.InsertCommonEscapeSequences()
            Assert.AreEqual(escapedValue, actualValue, $"Extension failed inserting escape value")
        End Sub

#End Region

#Region " PARSE "

        ''' <summary> (Unit Test Method) extracts the numeric characters test. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestMethod>
        Public Sub ExtractNumericCharactersTest()
            Dim compoundValue As String = ";123.456A"
            Dim expectedValue As String = "123456"
            Dim actualValue As String = ParseExtensions.Methods.ExtractNumericCharacters(compoundValue)
            Assert.AreEqual(expectedValue, actualValue, $"Should extract numeric characters from {compoundValue}")
        End Sub

        ''' <summary> (Unit Test Method) extracts the number test. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestMethod>
        Public Sub ExtractNumberTest()
            Dim rnd As New Random(DateTime.Now.Millisecond)
            Dim power As Double
            Dim value As Double
            Dim compoundValue As String
            Dim expectedValue As String
            Dim actualValue As String

            For i As Integer = 1 To 50
                power = rnd.Next Mod 10
                value = (rnd.NextDouble - 0.5) * rnd.NextDouble * Math.Pow(power, 10)
                compoundValue = $";{value}A"
                expectedValue = value.ToString
                actualValue = ParseExtensions.Methods.ExtractExponentialNumber(compoundValue)
                ' actualValue = ParseExtensions.Methods.ExtractNumberPart(compoundValue)
                Assert.AreEqual(expectedValue, actualValue, $"test #{i} should extract the number part from {compoundValue}")
            Next
        End Sub

        ''' <summary> (Unit Test Method) extracts the exponential number test. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        <TestMethod>
        Public Sub ExtractExponentialNumberTest()
            Dim rnd As New Random(DateTime.Now.Millisecond)
            Dim exponenet As Double
            Dim value As Double
            Dim candidateValue As String
            Dim compoundValue As String
            Dim expectedValue As String
            Dim actualValue As String

            For i As Integer = 1 To 50
                exponenet = (rnd.Next Mod 100) + 1 ' - 50
                value = (rnd.NextDouble - 0.5) * rnd.NextDouble
                candidateValue = $"A{value}"
                If candidateValue.Contains("E") Then
                    compoundValue = candidateValue
                    expectedValue = $"{value}"
                Else
                    compoundValue = $"A{value}E{exponenet}z"
                    expectedValue = $"{value}E{exponenet}"
                End If
                actualValue = ParseExtensions.Methods.ExtractExponentialNumber(compoundValue)
                Assert.AreEqual(expectedValue, actualValue, $"test #{i} should extract the number part from {compoundValue}")
            Next
        End Sub


#End Region

    End Class
End Namespace
