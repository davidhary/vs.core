Imports isr.Core.Services

''' <summary> Test site base class. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2/12/2018 </para>
''' </remarks>
Partial Public MustInherit Class TestSiteBase
    Inherits ApplicationSettingsBase
    Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="TestSiteBase" /> class. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    Protected Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Me.Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary>
    ''' Gets the dispose status sentinel of the base class.  This applies to the derived class
    ''' provided proper implementation.
    ''' </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> <c>True</c> if disposed; otherwise, <c>False</c>. </value>
    Protected Property IsDisposed() As Boolean

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks>
    ''' Executes in two distinct scenarios as determined by its disposing parameter:<para>
    ''' If True, the method has been called directly or indirectly by a user's code--managed and
    ''' unmanaged resources can be disposed.</para><para>
    ''' If False, the method has been called by the runtime from inside the finalizer and you should
    ''' not reference other objects--only unmanaged resources can be disposed.</para>
    ''' </remarks>
    ''' <param name="disposing"> <c>True</c> if this method releases both managed and unmanaged
    '''                                                   resources;
    '''                                                   False if this method releases only unmanaged
    '''                                                   resources. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed Then
                If disposing Then
                    If Me._TraceMessagesQueueListener IsNot Nothing Then
                        Me._TraceMessagesQueueListener.Dispose()
                    End If
                    Me._TraceMessagesQueueListener = Nothing
                End If
                Me._TraceMessagesQueues.Clear()
            End If
        Finally
            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True
        End Try
    End Sub

#End Region

#Region " VALIDATIONS "

    ''' <summary> Validated test context. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    ''' <returns> A TestContext. </returns>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <CLSCompliant(False)>
    Public Function ValidatedTestContext(ByVal testContext As TestContext) As TestContext
        If testContext Is Nothing Then Throw New ArgumentNullException(NameOf(testContext))
        Return testContext
    End Function

#End Region

#Region " TRACE "

    ''' <summary> Initializes the trace listener. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Sub InitializeTraceListener()
        Me.ReplaceTraceListener()
        Console.Out.WriteLine(My.Application.Log.DefaultFileLogWriter.FullLogFileName)
    End Sub

    ''' <summary> Replace trace listener. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Sub ReplaceTraceListener()
        My.Application.Log.TraceSource.Listeners.Remove(DefaultFileLogTraceListener.DefaultFileLogWriterName)
        My.Application.Log.TraceSource.Listeners.Add(DefaultFileLogTraceListener.CreateListener(False))
        My.Application.Log.TraceSource.Switch.Level = SourceLevels.Verbose
    End Sub

    ''' <summary> Trace message. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <param name="args">   A variable-length parameters list containing arguments. </param>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Sub TraceMessage(ByVal format As String, ByVal ParamArray args() As Object)
        Me.TraceMessage(String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
    End Sub

    ''' <summary> Trace message. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <param name="message"> The message. </param>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Sub TraceMessage(ByVal message As String)
        My.Application.Log.WriteEntry(message)
        'System.Diagnostics.Debug.WriteLine(message)
        Console.Out.WriteLine(message)
    End Sub

    ''' <summary> Verbose message. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <param name="args">   A variable-length parameters list containing arguments. </param>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Sub VerboseMessage(ByVal format As String, ByVal ParamArray args() As Object)
        If Me.Verbose Then Me.TraceMessage(format, args)
    End Sub

    ''' <summary> Trace elapsed time. </summary>
    ''' <remarks> David, 7/10/2020. </remarks>
    ''' <param name="startTime"> The start time. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> Current <see cref="DateTime"/>. </returns>
    Public Function TraceElapsedTime(ByVal startTime As Date, ByVal activity As String) As Date
        Me.TraceMessage($"{activity} {DateTime.Now.Subtract(startTime).TotalMilliseconds}ms")
        Return Date.Now
    End Function

#End Region

#Region " TRACE MESSAGES QUEUE "
    ''' <summary> The trace messages queue listener. </summary>
    Private _TraceMessagesQueueListener As TraceMessagesQueueListener

    ''' <summary> Gets the trace message queue listener. </summary>
    ''' <value> The trace message queue listener. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public ReadOnly Property TraceMessagesQueueListener As TraceMessagesQueueListener
        Get
            If Me._TraceMessagesQueueListener Is Nothing Then
                Me._TraceMessagesQueueListener = New TraceMessagesQueueListener
                Me._TraceMessagesQueueListener.ApplyTraceLevel(TraceEventType.Warning)
            End If
            Return Me._TraceMessagesQueueListener
        End Get
    End Property

    ''' <summary> Assert message. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <param name="traceMessage"> Message describing the trace. </param>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Sub AssertMessage(ByVal traceMessage As TraceMessage)
        If traceMessage Is Nothing Then
        ElseIf traceMessage.EventType = TraceEventType.Warning Then
            Me.TraceMessage($"Warning published: {traceMessage}")
        ElseIf traceMessage.EventType = TraceEventType.Error Then
            Assert.Fail($"Error published: {traceMessage}")
        End If
    End Sub

    ''' <summary> Assert message queue. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="queue"> The queue listener. </param>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Sub AssertMessageQueue(ByVal queue As TraceMessagesQueue)
        If queue Is Nothing Then Throw New ArgumentNullException(NameOf(queue))
        Do While Not queue.IsEmpty
            Me.AssertMessage(queue.TryDequeue)
        Loop
    End Sub

#End Region

#Region " TRACE MESSAGES QUEUE COLLECTION "
    ''' <summary> The trace messages queues. </summary>
    Private _TraceMessagesQueues As TraceMessageQueueCollection

    ''' <summary> Gets the collection of trace messages queues. </summary>
    ''' <value> The trace messages queues. </value>
    Public ReadOnly Property TraceMessagesQueues As TraceMessageQueueCollection
        Get
            If Me._TraceMessagesQueues Is Nothing Then
                Me._TraceMessagesQueues = New TraceMessageQueueCollection
            End If
            Return Me._TraceMessagesQueues
        End Get
    End Property

    ''' <summary> Adds a trace messages queue. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <param name="queue"> The queue listener. </param>
    Public Sub AddTraceMessagesQueue(ByVal queue As isr.Core.Services.TraceMessagesQueue)
        Me.TraceMessagesQueues.Add(queue)
    End Sub

    ''' <summary> Assert message queue. THis clears the queues. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    Public Sub AssertMessageQueue()
        Me.TraceMessagesQueues.AssertMessageQueue(Me)
    End Sub

    ''' <summary> Clears the message queue. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <returns> A String. </returns>
    Public Function ClearMessageQueue() As String
        Return Me.TraceMessagesQueues.ClearMessageQueue()
    End Function

#End Region

#Region " APPLICATION DOMAIN DATA DIRECTORY "

    ''' <summary> The name of the data directory application domain property. </summary>
    Public Const ApplicationDomainDataDirectoryPropertyName As String = "DataDirectory"

    ''' <summary> Modify application domain data directory path. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    Public Sub ModifyApplicationDomainDataDirectoryPath()
        Me.ModifyApplicationDomainDataDirectoryPath(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location))
    End Sub

    ''' <summary> Modify application domain data directory path. </summary>
    ''' <remarks>
    ''' https://stackoverflow.com/questions/1833640/connection-string-with-relative-path-to-the-database-file.
    ''' </remarks>
    ''' <param name="path"> Full pathname of the file. </param>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    Public Sub ModifyApplicationDomainDataDirectoryPath(ByVal path As String)
        AppDomain.CurrentDomain.SetData(TestSiteBase.ApplicationDomainDataDirectoryPropertyName, path)
    End Sub

    ''' <summary> Reads application domain data directory path. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <returns> The application domain data directory path. </returns>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    Public Function ReadApplicationDomainDataDirectoryPath() As String
        Dim value As String = TryCast(AppDomain.CurrentDomain.GetData(TestSiteBase.ApplicationDomainDataDirectoryPropertyName), String)
        Return If(value, String.Empty)
    End Function

#End Region

End Class

''' <summary> Collection of trace message queues. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 4/10/2018 </para>
''' </remarks>
Partial Public Class TraceMessageQueueCollection
    Inherits ObjectModel.Collection(Of TraceMessagesQueue)

    ''' <summary> Query if this object has queued messages. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <returns> <c>true</c> if queued messages; otherwise <c>false</c> </returns>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Function HasQueuedMessages() As Boolean
        Dim result As Boolean = False
        For Each traceMessageQueue As TraceMessagesQueue In Me
            result = result AndAlso traceMessageQueue.Any
        Next
        Return result
    End Function

    ''' <summary> Count queued messages. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <returns> The total number of queued messages. </returns>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Function CountQueuedMessages() As Integer
        Dim result As Integer = 0
        For Each traceMessageQueue As TraceMessagesQueue In Me
            result += traceMessageQueue.Count
        Next
        Return result
    End Function

    ''' <summary> Assert message queue. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <param name="testSite"> The test site. </param>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Sub AssertMessageQueue(ByVal testSite As TestSiteBase)
        For Each traceMessageQueue As TraceMessagesQueue In Me
            testSite?.AssertMessageQueue(traceMessageQueue)
        Next
    End Sub

    ''' <summary> Appends a line. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <param name="builder"> The builder. </param>
    ''' <param name="value">   The value. </param>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private Shared Sub AppendLine(ByVal builder As System.Text.StringBuilder, ByVal value As String)
        If Not String.IsNullOrWhiteSpace(value) Then builder.AppendLine(value)
    End Sub

    ''' <summary> Clears the message queue. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <returns> A String. </returns>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Function ClearMessageQueue() As String
        Dim builder As New System.Text.StringBuilder
        For Each traceMessageQueue As TraceMessagesQueue In Me
            TraceMessageQueueCollection.AppendLine(builder, traceMessageQueue.DequeueContent())
        Next
        Return builder.ToString
    End Function
End Class

