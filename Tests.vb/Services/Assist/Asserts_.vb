''' <summary> Assert. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 12/12/2016,  </para><para>
''' https://stackoverflow.com/questions/933613/how-do-i-use-assert-to-verify-that-an-exception-has-been-thrown
''' </para>
''' </remarks>
Public NotInheritable Class Asserts

#Region " SINGLETON "

    ''' <summary>
    ''' Constructor that prevents a default instance of this class from being created.
    ''' </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    Private Sub New()
        MyBase.New
    End Sub

    ''' <summary>
    ''' Gets the locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    ''' <value> The sync locker. </value>
    Private Shared Property SyncLocker As New Object

    ''' <summary> Gets the instance. </summary>
    ''' <value> The instance. </value>
    Private Shared Property Instance As Asserts

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    Public Shared Function [Get]() As Asserts
        If Instance Is Nothing Then
            SyncLock SyncLocker
                Instance = New Asserts()
            End SyncLock
        End If
        Return Instance
    End Function

    ''' <summary> Returns true if an instance of the class was created and not disposed. </summary>
    ''' <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
    Public Shared ReadOnly Property Instantiated() As Boolean
        Get
            SyncLock SyncLocker
                Return Instance IsNot Nothing
            End SyncLock
        End Get
    End Property

#End Region

#Region " TIME SPAN "

    ''' <summary> Are equal. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <exception cref="AssertFailedException"> Thrown when an Assert Failed error condition occurs. </exception>
    ''' <param name="expected"> The expected. </param>
    ''' <param name="actual">   The actual. </param>
    ''' <param name="delta">    The delta. </param>
    ''' <param name="message">  The message. </param>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    Public Sub AreEqual(ByVal expected As TimeSpan, ByVal actual As TimeSpan, ByVal delta As TimeSpan, ByVal message As String)
        If actual < expected.Subtract(delta) OrElse actual > expected.Add(delta) Then
            Throw New AssertFailedException($"Expected {expected} <> actual {actual} by more than {delta}; {message}")
        End If
    End Sub

    ''' <summary> Are equal. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <param name="expected"> The expected. </param>
    ''' <param name="actual">   The actual. </param>
    ''' <param name="delta">    The delta. </param>
    ''' <param name="format">   Describes the format to use. </param>
    ''' <param name="args">     A variable-length parameters list containing arguments. </param>
    Public Sub AreEqual(ByVal expected As TimeSpan, ByVal actual As TimeSpan, ByVal delta As TimeSpan, ByVal format As String, ParamArray args() As Object)
        Me.AreEqual(expected, actual, delta, String.Format(format, args))
    End Sub

#End Region

#Region " DATE TIME "

    ''' <summary> Are equal. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <exception cref="AssertFailedException"> Thrown when an Assert Failed error condition occurs. </exception>
    ''' <param name="expected"> The expected. </param>
    ''' <param name="actual">   The actual. </param>
    ''' <param name="delta">    The delta. </param>
    ''' <param name="message">  The message. </param>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    Public Sub AreEqual(ByVal expected As DateTime, ByVal actual As DateTime, ByVal delta As TimeSpan, ByVal message As String)
        If actual < expected.Subtract(delta) OrElse actual > expected.Add(delta) Then
            Throw New AssertFailedException($"Expected {expected} > actual {actual} by {expected.Subtract(actual).Subtract(delta)} over {delta}; {message}")
        End If
    End Sub

    ''' <summary> Are equal. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <param name="expected"> The expected. </param>
    ''' <param name="actual">   The actual. </param>
    ''' <param name="delta">    The delta. </param>
    ''' <param name="format">   Describes the format to use. </param>
    ''' <param name="args">     A variable-length parameters list containing arguments. </param>
    Public Sub AreEqual(ByVal expected As DateTime, ByVal actual As DateTime, ByVal delta As TimeSpan, ByVal format As String, ParamArray args() As Object)
        Me.AreEqual(expected, actual, delta, String.Format(format, args))
    End Sub

#End Region

#Region " DATE TIME OFFSET "

    ''' <summary> Are equal. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <exception cref="AssertFailedException"> Thrown when an Assert Failed error condition occurs. </exception>
    ''' <param name="expected"> The expected. </param>
    ''' <param name="actual">   The actual. </param>
    ''' <param name="delta">    The delta. </param>
    ''' <param name="message">  The message. </param>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    Public Sub AreEqual(ByVal expected As DateTimeOffset, ByVal actual As DateTimeOffset, ByVal delta As TimeSpan, ByVal message As String)
        If actual < expected.Subtract(delta) OrElse actual > expected.Add(delta) Then
            Throw New AssertFailedException($"Expected {expected:o} <> actual {actual:o} by more than {delta}; {message}")
        End If
    End Sub

    ''' <summary> Are equal. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <param name="expected"> The expected. </param>
    ''' <param name="actual">   The actual. </param>
    ''' <param name="delta">    The delta. </param>
    ''' <param name="format">   Describes the format to use. </param>
    ''' <param name="args">     A variable-length parameters list containing arguments. </param>
    Public Sub AreEqual(ByVal expected As DateTimeOffset, ByVal actual As DateTimeOffset, ByVal delta As TimeSpan, ByVal format As String, ParamArray args() As Object)
        Me.AreEqual(expected, actual, delta, String.Format(format, args))
    End Sub

#End Region

#Region " IS SINGLE "

    ''' <summary> Is single. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <param name="items"> The items. </param>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    Public Sub IsSingle(ByVal items As IEnumerable(Of Object))
        Assert.IsTrue(items IsNot Nothing)
        Assert.IsTrue(items.Any)
        Assert.AreEqual(1, items.Count)
    End Sub

    ''' <summary> Is single. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <param name="items">  The items. </param>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <param name="args">   A variable-length parameters list containing arguments. </param>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    Public Sub IsSingle(ByVal items As IEnumerable(Of Object), ByVal format As String, ParamArray args() As Object)
        Assert.IsTrue(items IsNot Nothing, format, args)
        Assert.IsTrue(items.Any, format, args)
        Assert.AreEqual(1, items.Count, format, args)
    End Sub

#End Region

#Region " IS EMPY "

    ''' <summary> Is single. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <param name="items"> The items. </param>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    Public Sub IsEmpty(ByVal items As IEnumerable(Of Object))
        If items IsNot Nothing Then Assert.IsFalse(items.Any)
    End Sub

    ''' <summary> Is single. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <param name="items">  The items. </param>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <param name="args">   A variable-length parameters list containing arguments. </param>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    Public Sub IsEmpty(ByVal items As IEnumerable(Of Object), ByVal format As String, ParamArray args() As Object)
        If items IsNot Nothing Then Assert.IsFalse(items.Any, format, args)
    End Sub

#End Region

#Region " THROWS "

    ''' <summary> Throws an exception to verify that an exception has been throw. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <exception cref="AssertFailedException"> Thrown when an Assert Failed error condition occurs. </exception>
    ''' <param name="func"> The function. </param>
    ''' <returns> A T. </returns>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    Public Function Throws(Of T As Exception)(ByVal func As Action) As T
        Dim exceptionThrown As Boolean = False
        Dim result As T = Nothing
        Try
            func?.Invoke()
        Catch ex As T
            exceptionThrown = True
            result = ex
        End Try
        If Not exceptionThrown Then
            Throw New AssertFailedException($"An exception of type {GetType(T)} was expected, but not thrown")
        End If
        Return result
    End Function

    ''' <summary> Throws an exception to verify that an exception has been throw. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <exception cref="AssertFailedException"> Thrown when an Assert Failed error condition occurs. </exception>
    ''' <param name="func">    The function. </param>
    ''' <param name="message"> The message. </param>
    ''' <returns> A T. </returns>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    Public Function Throws(Of T As Exception)(ByVal func As Action, ByVal message As String) As T
        Dim exceptionThrown As Boolean = False
        Dim result As T = Nothing
        Try
            func?.Invoke()
        Catch ex As T
            exceptionThrown = True
            result = ex
        End Try
        If Not exceptionThrown Then
            Throw New AssertFailedException($"An exception of type {GetType(T)} was expected, but not thrown; {message}")
        End If
        Return result
    End Function

    ''' <summary> Throws an exception to verify that an exception has been throw. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <param name="func">   The function. </param>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <param name="args">   A variable-length parameters list containing arguments. </param>
    ''' <returns> A T. </returns>
    Public Function Throws(Of T As Exception)(ByVal func As Action, ByVal format As String, ByVal ParamArray args() As Object) As T
        Return Me.Throws(Of T)(func, String.Format(format, args))
    End Function
#End Region

End Class

Public Module MyAssertProperty

    ''' <summary> Gets my assert. </summary>
    ''' <value> my assert. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public ReadOnly Property MyAssert() As Asserts
        Get
            Return Asserts.Get
        End Get
    End Property
End Module
