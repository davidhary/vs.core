Imports System.Data
Imports System.Data.Common
Imports Microsoft.VisualStudio.TestTools.UnitTesting

Partial Friend Class TestSite

#Region " DATA VALIDATIONS "

    ''' <summary> Validated data row. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    ''' <returns> A DataRow. </returns>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared Function ValidatedDataRow(ByVal testContext As TestContext) As DataRow
        If testContext Is Nothing Then Throw New ArgumentNullException(NameOf(testContext))
        Return TestSite.ValidatedDataRow(testContext.DataRow)
    End Function

    ''' <summary> Validated data row. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="dataRow"> The data row. </param>
    ''' <returns> A DataRow. </returns>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared Function ValidatedDataRow(ByVal dataRow As DataRow) As DataRow
        If dataRow Is Nothing Then Throw New ArgumentNullException(NameOf(dataRow))
        Return dataRow
    End Function

    ''' <summary> Validated connection. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    ''' <returns> A DbConnection. </returns>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared Function ValidatedConnection(ByVal testContext As TestContext) As DbConnection
        If testContext Is Nothing Then Throw New ArgumentNullException(NameOf(testContext))
        Return TestSite.Validatedconnection(testContext.DataConnection)
    End Function

    ''' <summary> Validated connection. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> A connection. </returns>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared Function Validatedconnection(ByVal connection As DbConnection) As DbConnection
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Return connection
    End Function

#End Region

End Class
