
Partial Public MustInherit Class TestSiteBase

#Region " TEST SITE CONFIGURATION "

    ''' <summary> Returns true if test settings exist. </summary>
    ''' <remarks> Default value set to false to test reading of the test settings. </remarks>
    ''' <value> <c>True</c> if testing settings exit. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("False")>
    Public Overridable Property Exists As Boolean
        Get
            Return Me.AppSettingGetter(False)
        End Get
        Set(value As Boolean)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Returns true to output test messages at the verbose level. </summary>
    ''' <value> The verbose messaging level. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("False")>
    Public Overridable Property Verbose As Boolean
        Get
            Return Me.AppSettingGetter(False)
        End Get
        Set(value As Boolean)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Returns true to enable this device. </summary>
    ''' <value> The device enable option. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Overridable Property Enabled As Boolean
        Get
            Return Me.AppSettingGetter(False)
        End Get
        Set(value As Boolean)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets all. </summary>
    ''' <value> all. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Overridable Property All As Boolean
        Get
            Return Me.AppSettingGetter(False)
        End Get
        Set(value As Boolean)
            Me.AppSettingSetter(value)
        End Set
    End Property

#End Region

#Region " TEST SITE LOCATION IDENTIFICATION "

    ''' <summary> Gets or sets the candidate time zones of the test site. </summary>
    ''' <value> The candidate time zones of the test site. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("Pacific Standard Time|Central Standard Time")>
    Public Overridable Property TimeZones As String
        Get
            Return Me.AppSettingGetter(String.Empty)
        End Get
        Set(value As String)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the time zone offset of the test sites. </summary>
    ''' <value> The time zone offsets. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("-8|-6")>
    Public Overridable Property TimeZoneOffsets As String
        Get
            Return Me.AppSettingGetter(String.Empty)
        End Get
        Set(value As String)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the Title of the resource. </summary>
    ''' <value> The Title of the resource. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("192.168|10.1")>
    Public Overridable Property IPv4Prefixes As String
        Get
            Return Me.AppSettingGetter(String.Empty)
        End Get
        Set(value As String)
            Me.AppSettingSetter(value)
        End Set
    End Property
    ''' <summary> The time zone. </summary>
    Private _TimeZone As String

    ''' <summary> Gets the time zone of the test site. </summary>
    ''' <value> The time zone of the test site. </value>
    Public ReadOnly Property TimeZone As String
        Get
            If String.IsNullOrWhiteSpace(Me._TimeZone) Then
                Me._TimeZone = Me.TimeZones.Split(MyBase.ValuesDelimiter)(Me.HostInfoIndex)
            End If
            Return Me._TimeZone
        End Get
    End Property
    ''' <summary> The time zone offset. </summary>
    Private _TimeZoneOffset As Double = Double.MinValue

    ''' <summary> Gets the time zone offset of the test site. </summary>
    ''' <value> The time zone offset of the test site. </value>
    Public ReadOnly Property TimeZoneOffset As Double
        Get
            If Me._TimeZoneOffset = Double.MinValue Then
                Me._TimeZoneOffset = Double.Parse(Me.TimeZoneOffsets.Split(MyBase.ValuesDelimiter)(Me.HostInfoIndex))
            End If
            Return Me._TimeZoneOffset
        End Get
    End Property

    ''' <summary> Gets the host name of the test site. </summary>
    ''' <value> The host name of the test site. </value>
    Public ReadOnly Property HostName As String
    ''' <summary> The host address. </summary>
    Private _HostAddress As Net.IPAddress

    ''' <summary> Gets the IP address of the test site. </summary>
    ''' <value> The IP address of the test site. </value>
    Public ReadOnly Property HostAddress As Net.IPAddress
        Get
            If Me._HostAddress Is Nothing Then
                Me._HostName = System.Net.Dns.GetHostName()
                For Each value As Net.IPAddress In System.Net.Dns.GetHostEntry(Me.HostName).AddressList
                    If value.AddressFamily = Net.Sockets.AddressFamily.InterNetwork Then
                        Me._HostAddress = value
                        Exit For
                    End If
                Next
            End If
            Return Me._HostAddress
        End Get
    End Property
    ''' <summary> Zero-based index of the host information. </summary>
    Private _HostInfoIndex As Integer = -1

    ''' <summary> Gets the index into the host information strings. </summary>
    ''' <value> The index into the host information strings. </value>
    Public ReadOnly Property HostInfoIndex As Integer
        Get
            If Me._HostInfoIndex < 0 Then
                Dim ip As String = Me.HostAddress.ToString
                Dim i As Integer = -1
                For Each value As String In Me.IPv4Prefixes.Split(MyBase.ValuesDelimiter)
                    i += 1
                    If ip.StartsWith(value, StringComparison.OrdinalIgnoreCase) Then
                        Me._HostInfoIndex = i
                        Exit For
                    End If
                Next
            End If
            Return Me._HostInfoIndex
        End Get
    End Property

#End Region

End Class

